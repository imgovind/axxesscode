<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
<div class="wrapper main">
	<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PatientHistoryForm" })) { %>
	<%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
	<%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
	<%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
	<%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
	<%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
	<%= Html.Hidden("categoryType", AssessmentCategory.PatientHistory.ToString())%>
	<%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
	<%  Html.RenderPartial("Action", Model); %>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
		        <legend>Physician Visit</legend>
				<div class="column">
				    <div class="row">
    					<label for="<%= Model.TypeName %>_GenericLastVisitDate" class="fl">Last Physician Visit Date</label>
	    				<div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericLastVisitDate" value="<%= data.AnswerOrEmptyString("GenericLastVisitDate").IsValidDate() ? data.AnswerOrEmptyString("GenericLastVisitDate") : string.Empty %>"  id="<%= Model.TypeName %>_GenericLastVisitDate" /></div>
	    			</div>
				</div>
			</fieldset>
			<fieldset class="loc485">
				<legend>Allergies (Locator #17)</legend>
	    <%  var allergyProfile = (Model != null && Model.AllergyProfile.IsNotNullOrEmpty()) ? Model.AllergyProfile.ToObject<AllergyProfile>() : new AllergyProfile(); %>
				<div class="wide-column">
				    <div class="row">
				        <ul class="buttons ac">
	    <%  if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
	                        <li><a onclick="Allergy.New('<%= allergyProfile.Id %>','<%= Model.TypeName %>')">Add Allergy</a></li>
	    <%  } %>
	                        <li><a onclick="Allergy.Refresh('<%= allergyProfile.Id %>','<%= Model.TypeName %>')">Refresh Allergies</a></li>
	                    </ul>
	                    <div id="<%= Model.TypeName %>_AllergyProfileList">
	    <%  allergyProfile.Type = Model.TypeName.ToString(); %>
	    <%  Html.RenderPartial("~/Views/AllergyProfile/List.ascx",allergyProfile); %>
	                    </div>
	                </div>
	            </div>
	        </fieldset>
	        <fieldset class="loc485">
				<legend>Vital Sign Parameters</legend>
				<div class="column">
					<div class="row">
						<label class="fl">Temperature</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericTempGreaterThan", data.AnswerOrEmptyString("GenericTempGreaterThan"), new { @id = Model.TypeName + "_GenericTempGreaterThan", @status = "(485 Locator 21) Orders, Temperature Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericTempLessThan", data.AnswerOrEmptyString("GenericTempLessThan"), new { @id = Model.TypeName + "_GenericTempLessThan", @status = "(485 Locator 21) Orders, Temperature Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Pulse</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericPulseGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericPulseGreaterThan", data.AnswerOrEmptyString("GenericPulseGreaterThan"), new { @id = Model.TypeName + "_GenericPulseGreaterThan", @status = "(485 Locator 21) Orders, Pulse Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericPulseLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericPulseLessThan", data.AnswerOrEmptyString("GenericPulseLessThan"), new { @id = Model.TypeName + "_GenericPulseLessThan", @status = "(485 Locator 21) Orders, Pulse Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Respirations</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericRespirationGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericRespirationGreaterThan", data.AnswerOrEmptyString("GenericRespirationGreaterThan"), new { @id = Model.TypeName + "_GenericRespirationGreaterThan", @status = "(485 Locator 21) Orders, Respiration Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericRespirationLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericRespirationLessThan", data.AnswerOrEmptyString("GenericRespirationLessThan"), new { @id = Model.TypeName + "_GenericRespirationLessThan", @status = "(485 Locator 21) Orders, Respiration Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Systolic BP</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericSystolicBPGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericSystolicBPGreaterThan", data.AnswerOrEmptyString("GenericSystolicBPGreaterThan"), new { @id = Model.TypeName + "_GenericSystolicBPGreaterThan", @status = "(485 Locator 21) Orders, Systolic Blood Pressure Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericSystolicBPLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericSystolicBPLessThan", data.AnswerOrEmptyString("GenericSystolicBPLessThan"), new { @id = Model.TypeName + "_GenericRespirationGreaterThan", @status = "(485 Locator 21) Orders, Systolic Blood Pressure Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Diastolic BP</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericDiastolicBPGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericDiastolicBPGreaterThan", data.AnswerOrEmptyString("GenericDiastolicBPGreaterThan"), new { @id = Model.TypeName + "_GenericDiastolicBPGreaterThan", @status = "(485 Locator 21) Orders, Diastolic Blood Pressure Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericDiastolicBPLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericDiastolicBPLessThan", data.AnswerOrEmptyString("GenericDiastolicBPLessThan"), new { @id = Model.TypeName + "_GenericDiastolicBPLessThan", @status = "(485 Locator 21) Orders, Diastolic Blood Pressure Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">O<sub>2</sub> Saturation</label>
						<div class="fr">
							<label for="<%= Model.TypeName %>_Generic02SatLessThan">less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_Generic02SatLessThan", data.AnswerOrEmptyString("Generic02SatLessThan"), new { @id = Model.TypeName + "_Generic02SatLessThan", @status = "(485 Locator 21) Orders, Oxygen Saturation Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Fasting blood sugar</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericFastingBloodSugarGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericFastingBloodSugarGreaterThan", data.AnswerOrEmptyString("GenericFastingBloodSugarGreaterThan"), new { @id = Model.TypeName + "_GenericFastingBloodSugarGreaterThan", @status = "(485 Locator 21) Orders, Fasting Blood Sugar Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericFastingBloodSugarLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericFastingBloodSugarLessThan", data.AnswerOrEmptyString("GenericFastingBloodSugarLessThan"), new { @id = Model.TypeName + "_GenericFastingBloodSugarLessThan", @status = "(485 Locator 21) Orders, Fasting Blood Sugar Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Random blood sugar</label>
						<div class="fr ar">
							<label for="<%= Model.TypeName %>_GenericRandomBloddSugarGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericRandomBloddSugarGreaterThan", data.AnswerOrEmptyString("GenericRandomBloddSugarGreaterThan"), new { @id = Model.TypeName + "_GenericRandomBloddSugarGreaterThan", @status = "(485 Locator 21) Orders, Random Blood Sugar Maximum", @class = "shorter" })%>
							<br />
							<label for="<%= Model.TypeName %>_GenericRandomBloodSugarLessThan">or less than (&#60;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericRandomBloodSugarLessThan", data.AnswerOrEmptyString("GenericRandomBloodSugarLessThan"), new { @id = Model.TypeName + "_GenericRandomBloodSugarLessThan", @status = "(485 Locator 21) Orders, Random Blood Sugar Minimum", @class = "shorter" })%>
						</div>
					</div>
					<div class="row">
						<label class="fl">Weight Gain/Loss</label>
						<div class="fr">
							<label for="<%= Model.TypeName %>_GenericWeightGreaterThan">greater than (&#62;)</label>
							<%= Html.TextBox(Model.TypeName + "_GenericWeightGreaterThan", data.AnswerOrEmptyString("GenericWeightGreaterThan"), new { @id = Model.TypeName + "_GenericWeightGreaterThan", @status = "(485 Locator 21) Orders, Weight Change Maximum", @class = "shorter" })%>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>Immunizations</legend>
				<div class="column">
					<div class="row">
						<label class="fl">Pneumonia</label>
						<%= Html.Hidden(Model.TypeName + "_485Pnemonia", " ", new { @id = Model.TypeName + "_485PnemoniaHidden" })%>
						<div class="fr">
							<%  var Pnemonia = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "" },
								    new SelectListItem { Text = "Yes", Value = "Yes" },
								    new SelectListItem { Text = "No", Value = "No"},
								    new SelectListItem { Text = "Unknown", Value = "UK" }
								}, "Value", "Text", data.AnswerOrEmptyString("485Pnemonia")); %>
							<%= Html.DropDownList(Model.TypeName + "_485Pnemonia", Pnemonia, new { @class = "shorter" })%>
							<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485PnemoniaDate" value="<%= data.AnswerOrEmptyString("485PnemoniaDate") %>" id="<%= Model.TypeName %>_485PnemoniaDate" />
						</div>
					</div>
					<div class="row">
						<label class="fl">Flu</label>
						<%= Html.Hidden(Model.TypeName + "_485Flu")%>
						<div class="fr">
							<%  var Flu = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "" },
								    new SelectListItem { Text = "Yes", Value = "Yes" },
								    new SelectListItem { Text = "No", Value = "No"},
								    new SelectListItem { Text = "Unknown", Value = "UK" }
								}, "Value", "Text", data.AnswerOrEmptyString("485Flu")); %>
							<%= Html.DropDownList(Model.TypeName + "_485Flu", Flu, new { @class = "shorter" })%>
							<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485FluDate" value="<%= data.AnswerOrEmptyString("485FluDate") %>" id="<%= Model.TypeName %>_485FluDate" />
						</div>
					</div>
					<div class="row">
						<label class="fl">TB</label>
						<%= Html.Hidden(Model.TypeName + "_485TB")%>
						<div class="fr">
							<%  var TB = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "" },
								    new SelectListItem { Text = "Yes", Value = "Yes" },
								    new SelectListItem { Text = "No", Value = "No"},
								    new SelectListItem { Text = "Unknown", Value = "UK" }
								}, "Value", "Text", data.AnswerOrEmptyString("485TB")); %>
							<%= Html.DropDownList(Model.TypeName + "_485TB", TB, new { @class = "shorter" })%>
							<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485TBDate" value="<%= data.AnswerOrEmptyString("485TBDate") %>" id="<%= Model.TypeName %>_485TBDate" />
						</div>
					</div>
					<div class="row">
						<label class="fl">TB Exposure</label>
						<%= Html.Hidden(Model.TypeName + "_485TBExposure")%>
						<div class="fr">
							<%  var TBExposure = new SelectList(new[] {
								    new SelectListItem { Text = "", Value = "" },
								    new SelectListItem { Text = "Yes", Value = "Yes" },
								    new SelectListItem { Text = "No", Value = "No"},
								    new SelectListItem { Text = "Unknown", Value = "UK" }
								}, "Value", "Text", data.AnswerOrEmptyString("485TBExposure")); %>
							<%= Html.DropDownList(Model.TypeName + "_485TBExposure", TBExposure, new { @class = "shorter" })%>
							<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485TBExposureDate" value="<%= data.AnswerOrEmptyString("485TBExposureDate") %>" id="<%= Model.TypeName %>_485TBExposureDate" />
						</div>
					</div>
					<div class="row ac">Additional Immunizations</div>
					<div class="row">
						<div class="fl"><%= Html.TextBox(Model.TypeName + "_485AdditionalImmunization1Name", data.AnswerOrEmptyString("485AdditionalImmunization1Name"), new { @id = Model.TypeName + "_485AdditionalImmunization1Name" }) %></div>
						<%= Html.Hidden(Model.TypeName + "_485AdditionalImmunization1")%>
						<div class="fr">
							<%  var AdditionalImmunization1 = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "" },
								new SelectListItem { Text = "Yes", Value = "Yes" },
								new SelectListItem { Text = "No", Value = "No"},
								new SelectListItem { Text = "Unknown", Value = "UK" }
								}, "Value", "Text", data.AnswerOrEmptyString("485AdditionalImmunization1")); %>
							<%= Html.DropDownList(Model.TypeName + "_485AdditionalImmunization1", AdditionalImmunization1, new { @class = "shorter" })%>
							<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485AdditionalImmunization1Date" value="<%= data.AnswerOrEmptyString("485AdditionalImmunization1Date") %>" id="<%= Model.TypeName %>_485AdditionalImmunization1Date" />
						</div>
					</div>
					<div class="row">
						<div class="fl"><%= Html.TextBox(Model.TypeName + "_485AdditionalImmunization2Name", data.AnswerOrEmptyString("485AdditionalImmunization2Name"), new { @id = Model.TypeName + "_485AdditionalImmunization2Name" }) %></div>
						<%= Html.Hidden(Model.TypeName + "_485AdditionalImmunization2")%>
						<div class="fr">
							<%  var AdditionalImmunization2 = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "" },
								new SelectListItem { Text = "Yes", Value = "Yes" },
								new SelectListItem { Text = "No", Value = "No"},
								new SelectListItem { Text = "Unknown", Value = "UK" }
								}, "Value", "Text", data.AnswerOrEmptyString("485AdditionalImmunization2")); %>
							<%= Html.DropDownList(Model.TypeName + "_485AdditionalImmunization2", AdditionalImmunization2, new { @class = "shorter" })%>
							<input type="text" class="date-picker short" name="<%= Model.TypeName %>_485AdditionalImmunization2Date" value="<%= data.AnswerOrEmptyString("485AdditionalImmunization2Date") %>" id="<%= Model.TypeName %>_485AdditionalImmunization2Date" />
						</div>
					</div>
					<div class="row">
						<label for="<%= Model.TypeName %>_485ImmunizationComments">Comments</label>
						<div class="ac"><%= Html.TextArea(Model.TypeName + "_485ImmunizationComments", data.AnswerOrEmptyString("485ImmunizationComments"), new { @id = Model.TypeName + "_485ImmunizationComments" }) %></div>
					</div>
				</div>
			</fieldset>
			<fieldset class="vital-signs">
				<legend>Vital Signs</legend>
				<%  Html.RenderPartial("~/Views/VitalSign/Content.ascx", Model != null && Model.VitalSignLog != null ? Model.VitalSignLog : new VitalSignLog(true)); %>
			</fieldset>
		</div>
    	<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4) { %>
		<div>
        	<fieldset class="oasis">
		        <legend>Inpatient Discharges</legend>
        		<div class="column">
			        <div class="row no-input" id="<%= Model.TypeName %>_M1000">
				        <label>
				            <a class="green" onclick="Oasis.Help('M1000')" status="More Information about M1000">(M1000)</a>
				            From which of the following Inpatient Facilities was the patient discharged during the past 14 days?
				        </label>
				        <ul class="checkgroup one-wide">
                            <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesLTC", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesLTCHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesLTC", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").Equals("1"), "1", "Long-term nursing facility (NF)", new { @class = "M1000" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesSNF", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesSNFHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesSNF", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").Equals("1"), "2", "Skilled nursing facility (SNF/TCU)", new { @class = "M1000" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesIPPS", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesIPPSHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesIPPS", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").Equals("1"), "3", "Short-stay acute hospital (IPPS)", new { @class = "M1000" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesLTCH", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesLTCHHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesLTCH", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").Equals("1"), "4", "Long-term care hospital (LTCH)", new { @class = "M1000" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesIRF", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesIRFHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesIRF", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").Equals("1"), "5", "Inpatient rehabilitation hospital or unit (IRF)", new { @class = "M1000" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesPhych", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesPhychHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesPhych", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").Equals("1"), "6", "Psychiatric hospital or unit", new { @class = "M1000" })%>
					        <li class="option">
						        <div class="wrapper" status="(OASIS M1000) Recent Inpatient Discharges, Other">
							        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesOTHR", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesOTHRHidden" })%>
							        <%= string.Format("<input id='{0}_M1000InpatientFacilitiesOTHR' name='{0}_M1000InpatientFacilitiesOTHR' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").Equals("1").ToChecked()) %>
							        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesOTHR">
							            <span class="fl">7 &#8211;</span>
							            <span class="margin">Other</span>
							        </label>
						        </div>
						        <div id="<%= Model.TypeName %>_M1000InpatientFacilitiesOTHRMore" class="more">
							        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesOther" class="fl">Specify</label>
							        <div class="fr oasis"><%= Html.TextBox(Model.TypeName + "_M1000InpatientFacilitiesOther", data.AnswerOrEmptyString("M1000InpatientFacilitiesOther"), new { @id = Model.TypeName + "_M1000InpatientFacilitiesOther", @status = "(OASIS M1000) Recent Inpatient Discharges, Specify Other" })%></div>
							        <div class="clr"></div>
						        </div>
					        </li>
				            <%= Html.CheckgroupOption(Model.TypeName + "_M1000InpatientFacilitiesNone", "1", data.AnswerOrEmptyString("M1000InpatientFacilitiesNone").Equals("1"), "8", "Patient was not discharged from an inpatient facility", new { @class = "M1000" })%>
				        </ul>
				        <div class="fr oasis">
					        <div class="button oasis-help"><a onclick="Oasis.Help('M1000')" status="More Information about M1000">?</a></div>
				        </div>
			        </div>
		        </div>
        	</fieldset>
        	<fieldset class="oasis">
        	    <legend>Discharges Date</legend>
        	    <div class="column">
        	        <div class="row" id="<%= Model.TypeName %>_M1005">
        	            <label for="<%= Model.TypeName %>_M1005InpatientDischargeDate" class="fl">
        	                <a class="green" onclick="Oasis.Help('M1005')" status="More Information about M1005">(M1005)</a>
        	                Inpatient Discharge Date (most recent)
        	            </label>
        	            <div class="fr oasis">
        	                <input type="text" class="date-picker" name="<%= Model.TypeName %>_M1005InpatientDischargeDate" value="<%= data.AnswerOrEmptyString("M1005InpatientDischargeDate") %>" id="<%= Model.TypeName %>_M1005InpatientDischargeDate" status="(OASIS M1005) Inpatient Discharge Date" />
        	                <%= Html.Hidden(Model.TypeName + "_M1005InpatientDischargeDateUnknown", " ", new { @id = Model.TypeName + "_M1005InpatientDischargeDateUnknownHidden" })%>
        	                <div class="button oasis-help"><a onclick="Oasis.Help('M1005')" status="More Information about M1005">?</a></div>
        	            </div>
        	            <div class="clr"></div>
        	            <div class="fr">
        	                <ul class="checkgroup one-wide">
        	                    <%= Html.CheckgroupOption(Model.TypeName + "_M1005InpatientDischargeDateUnknown", "1", data.AnswerOrEmptyString("M1005InpatientDischargeDateUnknown").Equals("1"), "UK", "Unknown") %>
        	                </ul>
        	            </div>
        	        </div>
        	    </div>
        	</fieldset>
            <fieldset class="oasis">
                <legend>Inpatient Procedure</legend>
                <div class="column">
                    <div class="row no-input" id="<%= Model.TypeName %>_M1012">
                        <label>
                            <a class="green" onclick="Oasis.Help('M1012')" status="More Information about M1012">(M1012)</a>
                            List each Inpatient Procedure and the associated ICD-9-C M procedure code relevant to the plan of care.
                        </label>
                        <ul>
                            <li>
                                <span class="fl">a.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure1", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure1"), new { @class = "procedureDiagnosis", @status = "(OASIS M1012) Inpatient Procedure", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode1", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode1"), new { @class = "procedureICD shorter", @status = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">b.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure2", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure2"), new { @class = "procedureDiagnosis", @status = "(OASIS M1012) Inpatient Procedure", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode2", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode2"), new { @class = "procedureICD shorter", @status = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">c.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure3", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure3"), new { @class = "procedureDiagnosis", @status = "(OASIS M1012) Inpatient Procedure", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode3", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode3"), new { @class = "procedureICD shorter", @status = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">d.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure4", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure4"), new { @class = "procedureDiagnosis", @status = "(OASIS M1012) Inpatient Procedure", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode4", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode4"), new { @class = "procedureICD shorter", @status = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                        </ul>
                        <div class="fr oasis">
                            <div class="button oasis-help"><a onclick="Oasis.Help('M1012')" status="More Information about M1012">?</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="checkgroup two-wide">
                            <%= Html.Hidden(Model.TypeName + "_M1012InpatientFacilityProcedureCodeNotApplicable", " ", new { @id = Model.TypeName + "_M1012InpatientFacilityProcedureCodeNotApplicableHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1012InpatientFacilityProcedureCodeNotApplicable", "1", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCodeNotApplicable").Equals("1"), "NA", "Not applicable")%>
                            <%= Html.Hidden(Model.TypeName + "_M1012InpatientFacilityProcedureCodeUnknown", " ", new { @id = Model.TypeName + "_M1012InpatientFacilityProcedureCodeUnknownHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1012InpatientFacilityProcedureCodeUnknown", "1", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCodeUnknown").Equals("1"), "UK", "Unknown")%>
						</ul>
					</div>
				</div>
			</fieldset>
        </div>
        <div>
	        <fieldset class="oasis">
	            <legend>Diagnoses Requiring Change</legend>
	            <div class="column">
	                <div class="row no-input" id="<%= Model.TypeName %>_M1016">
	                    <label>
	                        <a status="More Information about M1016" class="green" onclick="Oasis.Help('M1016')">(M1016)</a>
	                        Diagnoses Requiring Medical or Treatment Regimen Change Within Past 14 Days: List the patient&#8217;s Medical Diagnoses and ICD-9-C M codes at the level of highest specificity for those conditions requiring changed medical or treatment regimen within the past 14 days.<br />
	                        <em>(No Surgical, E-codes, or V-codes)</em>
	                    </label>
	                    <ul>
	                        <li>
	                            <span class="fl">a.</span>
	                            <span class="margin">
	                                <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis1", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis1"), new { @class = "diagnosis", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change", @placeholder = "Diagnosis" })%>
	                                <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode1", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode1"), new { @class = "icd shorter", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code", @placeholder = "ICD-9" })%>
	                            </span>
	                        </li>
	                        <li>
	                            <span class="fl">b.</span>
	                            <span class="margin">
	                                <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis2", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis2"), new { @class = "diagnosis", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change", @placeholder = "Diagnosis" })%>
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode2", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode2"), new { @class = "icd shorter", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code", @placeholder = "ICD-9" })%>
						        </span>
					        </li>
					        <li>
						        <span class="fl">c.</span>
						        <span class="margin">
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis3", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis3"), new { @class = "diagnosis", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change", @placeholder = "Diagnosis" })%>
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode3", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode3"), new { @class = "icd shorter", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code", @placeholder = "ICD-9" })%>
						        </span>
					        </li>
					        <li>
						        <span class="fl">d.</span>
						        <span class="margin">
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis4", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis4"), new { @class = "diagnosis", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change", @placeholder = "Diagnosis" })%>
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode4", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode4"), new { @class = "icd shorter", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code", @placeholder = "ICD-9" })%>
						        </span>
					        </li>
					        <li>
						        <span class="fl">e.</span>
						        <span class="margin">
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis5", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis5"), new { @class = "diagnosis", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change", @placeholder = "Diagnosis" })%>
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode5", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode5"), new { @class = "icd shorter", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code", @placeholder = "ICD-9" })%>
						        </span>
					        </li>
					        <li>
						        <span class="fl">f.</span>
						        <span class="margin">
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis6", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis6"), new { @class = "diagnosis", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change", @placeholder = "Diagnosis" })%>
						            <%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode6", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode6"), new { @class = "icd shorter", @status = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code", @placeholder = "ICD-9" })%>
						        </span>
					        </li>
				        </ul>
				        <div class="fr oasis">
					        <div class="button oasis-help"><a onclick="Oasis.Help('M1016')" status="More Information about M1016">?</a></div>
				        </div>
			        </div>
			        <div class="row">
			            <ul class="checkgroup one-wide">
			                <%= Html.CheckgroupOption(Model.TypeName + "_M1016MedicalRegimenDiagnosisNotApplicable", "1", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisNotApplicable").Equals("1"), "NA", "Not applicable<br /><em>(No medical or treatment regimen changes within the past 14 days)</em>") %>
			            </ul>
			        </div>
			    </div>
			</fieldset>
            <fieldset class="oasis">
                <legend>Inpatient Diagnosis</legend>
                <div class="column">
                    <div class="row no-input" id="<%= Model.TypeName %>_M1010">
                        <label>
                            <a class="green" onclick="Oasis.Help('M1010')" status="More Information about M1010">(M1010)</a>
                            List each Inpatient Diagnosis and ICD-9-C M code at the level of highest specificity for only those conditions treated during an inpatient stay within the last 14 days<br />
                            <em>(no E-codes, or V-codes)</em>
                        </label>
                        <ul>
                            <li>
                                <span class="fl">a.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis1", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis1"), new { @class = "diagnosis", @status = "(OASIS M1010) Inpatient Diagnosis", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode1", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode1"), new { @class = "icd shorter", @status = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">b.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis2", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis2"), new { @class = "diagnosis", @status = "(OASIS M1010) Inpatient Diagnosis", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode2", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode2"), new { @class = "icd shorter", @status = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">c.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis3", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis3"), new { @class = "diagnosis", @status = "(OASIS M1010) Inpatient Diagnosis", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode3", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode3"), new { @class = "icd shorter", @status = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">d.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis4", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis4"), new { @class = "diagnosis", @status = "(OASIS M1010) Inpatient Diagnosis", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode4", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode4"), new { @class = "icd shorter", @status = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
                            </li>
                            <li>
                                <span class="fl">e.</span>
                                <span class="margin">
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis5", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis5"), new { @class = "diagnosis", @status = "(OASIS M1010) Inpatient Diagnosis", @placeholder = "Diagnosis" })%>
                                    <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode5", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode5"), new { @class = "icd shorter", @status = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                                </span>
					        </li>
					        <li>
					            <span class="fl">f.</span>
                                <span class="margin">
    					            <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis6", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis6"), new { @class = "diagnosis", @status = "(OASIS M1010) Inpatient Diagnosis", @placeholder = "Diagnosis" })%>
	    				            <%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode6", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode6"), new { @class = "icd shorter", @status = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code", @placeholder = "ICD-9" })%>
	    				        </span>
					        </li>
					    </ul>
					    <div class="fr oasis">
					        <div class="button oasis-help"><a onclick="Oasis.Help('M1010')" status="More Information about M1010">?</a></div>
					    </div>
					</div>
				</div>
			</fieldset>
        </div>
          <%  } %>
    </div>
    	<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4) { %>
	<fieldset class="oasis">
		<legend>Conditions Prior to Medical or Treatment Regimen Change</legend>
		<div class="wide-column">
			<div class="row no-input" id="<%= Model.TypeName %>_M1018">
				<label>
				    <a status="More Information about M1018" class="green" onclick="Oasis.Help('M1018')">(M1018)</a>
				    Conditions Prior to Medical or Treatment Regimen Change or Inpatient Stay Within Past 14 Days: If this patient experienced an inpatient facility discharge or change in medical or treatment regimen within the past 14 days, indicate any conditions which existed prior to the inpatient stay or change in medical or treatment regime.
				</label>
				<ul class="checkgroup two-wide">
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUI", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUIHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUI", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenUI").Equals("1"), "1", "Urinary incontinence", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenCATH", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenCATHHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenCATH", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenCATH").Equals("1"), "2", "Indwelling/suprapubic catheter", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenPain", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenPainHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenPain", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenPain").Equals("1"), "3", "Intractable pain", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDECSN", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDECSNHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDECSN", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenDECSN").Equals("1"), "4", "Impaired decision-making", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptive", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptiveHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptive", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenDisruptive").Equals("1"), "5", "Disruptive or socially inappropriate behavior", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenMemLoss", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenMemLossHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenMemLoss", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenMemLoss").Equals("1"), "6", "Memory loss to the extent that supervision required", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptive", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptiveHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenNone", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenNone").Equals("1"), "7", "None of the above", new { @class = "M1018" })%>
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUK", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUKHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUK", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenUK").Equals("1"), "UK", "Unknown", new { @class = "M1018" })%>
				</ul>
				<ul class="checkgroup one-wide">
					<%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenNA", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenNAHidden" })%>
				    <%= Html.CheckgroupOption(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenNA", "1", data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenNA").Equals("1"), "NA", "No inpatient facility discharge and no change in medical or treatment regimen in past 14 days", new { @class = "M1018" })%>
				</ul>
				<div class="fr oasis">
					<div class="button oasis-help"><a onclick="Oasis.Help('M1018')" status="More Information about M1018">?</a></div>
				</div>
			</div>
		</div>
	</fieldset>
	    <%  } %>
    <fieldset class="oasis">
        <legend>Diagnoses, Symptom Control, and Payment Diagnoses</legend>
        <div class="wide-column">
            <div class="row">
                <div class="instructions">
                    <p>
                        List each diagnosis for which the patient is receiving home care (Column 1) and enter its ICD-9-C M code at the
                        level of highest specificity (no surgical/procedure codes) (Column 2). Diagnoses are listed in the order that best
                        reflect the seriousness of each condition and support the disciplines and services provided. Rate the degree of
                        symptom control for each condition (Column 2). Choose one value that represents the degree of symptom control
                        appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes (for M1022 only) may be used. ICD-9-C M
                        sequencing requirements must be followed if multiple coding is indicated for any diagnoses. If a V-code is 
                        eported in place of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and 4) may be
                        completed. A case mix diagnosis is a diagnosis that determines the Medicare P P S case mix group. Do not assign
                        symptom control ratings for V- or E-codes.
                    </p>
                    <strong>Code each row according to the following directions for each column:</strong>
                    <ul>
                        <li id="<%= Model.TypeName %>_M1020">
                            <span class="fl">Column 1:</span>
                            <p>Enter the description of the diagnosis.</p>
                        </li>
                        <li id="<%= Model.TypeName %>_M1022">
                            <span class="fl">Column 2:</span>
                            <p>Enter the ICD-9-C M code for the diagnosis described in Column 1;</p>
                            <ul>
                                <li>Rate the degree of symptom control for the condition listed in Column 1 using thefollowing scale:</li>
                                <li>
                                    <span class="fl">0.</span>
                                    <p>Asymptomatic, no treatment needed at this time</p>
                                </li>
                                <li>
                                    <span class="fl">1.</span>
                                    <p>Symptoms well controlled with current therapy</p>
                                </li>
                                <li>
                                    <span class="fl">2.</span>
                                    <p>Symptoms controlled with difficulty, affecting daily functioning; patient needsongoing monitoring</p>
                                </li>
                                <li>
                                    <span class="fl">3.</span>
                                    <p>Symptoms poorly controlled; patient needs frequent adjustment in treatment anddose monitoring</p>
                                </li>
                                <li>
                                    <span class="fl">4.</span>
                                    <p>
                                        Symptoms poorly controlled; history of re-hospitalizations Note that in Column2 the rating for
                                        symptom control of each diagnosis should not be used to determinethe sequencing of the diagnoses
                                        listed in Column 1. These are separate items andsequencing may not coincide. Sequencing of
                                        diagnoses should reflect the seriousnessof each condition and support the disciplines and services
                                        provided.
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li id="<%= Model.TypeName %>_M1024">
                            <span class="fl">Column 3:</span>
                            <p>
                                (OPTIONAL) If a V-code is assigned to any row in Column 2, in place ofa case mix diagnosis, it may be
                                necessary to complete optional item M1024 PaymentDiagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.
                            </p>
                        </li>
                        <li>
                            <span class="fl">Column 4:</span>
                            <p>
                                (OPTIONAL) If a V-code in Column 2 is reported in place of a case mixdiagnosis that requires multiple
                                diagnosis codes under ICD-9-C M coding guidelines,enter the diagnosis descriptions and the ICD-9-C M
                                codes in the same row in Columns 3 and 4. For example, if the case mix diagnosis is a manifestation code,
                                record the diagnosis description and ICD-9-C M code for the underlying condition in Column 3 of that row
                                and the diagnosis description and ICD-9-C M code for the manifestation in Column 4 of that row. Otherwise,
                                leave Column 4 blank in that row.
                            </p>
                        </li>
                    </ul>
                </div>
                <ul id="<%= Model.TypeName %>_OasisDiagnoses" class="oasis-diagnoses" data="<%= Model.DiagnosisDataJson %>"></ul>
            </div>
        </div>
    </fieldset>
	    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
	<div class="inline-fieldset two-wide">
		<div>
	    <%  } %>
		    <fieldset class="loc485">
		        <legend>Surgical Procedure (Locator #12)</legend>
		        <div class="wide-column">
		            <div class="row narrower">
		                <label class="fl">Surgical Procedure</label>
		                <div class="fr">
                            <%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureDescription1", data.AnswerOrEmptyString("485SurgicalProcedureDescription1"), new { @class = "procedureDiagnosis short", @id = Model.TypeName + "_485SurgicalProcedureDescription1", @status = "(485 Locator 12) Surgical Procedure", @placeholder = "Procedure" })%>
                            <%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureCode1", data.AnswerOrEmptyString("485SurgicalProcedureCode1"), new { @class = "procedureICD shorter", @id = Model.TypeName + "_485SurgicalProcedureCode1", @status = "(485 Locator 12) Surgical Procedure, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                            <br />
                            <input type="text" class="date-picker" name="<%= Model.TypeName %>_485SurgicalProcedureCode1Date" value="<%= data.AnswerOrEmptyString("485SurgicalProcedureCode1Date") %>" id="<%= Model.TypeName %>_485SurgicalProcedureCode1Date" status="(485 Locator 12) Surgical Procedure, Date" />
                        </div>
                    </div>
		            <div class="row narrower">
		                <label class="fl">Surgical Procedure</label>
		                <div class="fr">
                            <%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureDescription2", data.AnswerOrEmptyString("485SurgicalProcedureDescription2"), new { @class = "procedureDiagnosis short", @id = Model.TypeName + "_485SurgicalProcedureDescription2", @status = "(485 Locator 12) Surgical Procedure", @placeholder = "Procedure" })%>
                            <%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureCode2", data.AnswerOrEmptyString("485SurgicalProcedureCode2"), new { @class = "procedureICD shorter", @id = Model.TypeName + "_485SurgicalProcedureCode2", @status = "(485 Locator 12) Surgical Procedure, ICD-9-C M Code", @placeholder = "ICD-9" })%>
                            <br />
                            <input type="text" class="date-picker" name="<%= Model.TypeName %>_485SurgicalProcedureCode2Date" value="<%= data.AnswerOrEmptyString("485SurgicalProcedureCode2Date") %>" id="<%= Model.TypeName %>_485SurgicalProcedureCode2Date" status="(485 Locator 12) Surgical Procedure, Date" />
                        </div>
                    </div>
                </div>
            </fieldset>
	    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
	    </div>
	    <div>
	        <fieldset class="oasis">
	            <legend>Therapies at Home</legend>
	            <div class="column">
	                <div class="row" id="<%= Model.TypeName %>_M1030">
	                    <label>
	                        <a status="More Information about M1030" class="green" onclick="Oasis.Help('M1030')">(M1030)</a>
				            Therapies the patient receives at home
				        </label>
				        <ul class="checkgroup one-wide">
                            <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesInfusion", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesInfusionHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1030HomeTherapiesInfusion", "1", data.AnswerOrEmptyString("M1030HomeTherapiesInfusion").Equals("1"), "1", "Intravenous or infusion therapy (excludes TPN)", new { @class = "M1030" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesParNutrition", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesParNutritionHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1030HomeTherapiesParNutrition", "1", data.AnswerOrEmptyString("M1030HomeTherapiesParNutrition").Equals("1"), "2", "Parenteral nutrition (TPN or lipids)", new { @class = "M1030" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesEntNutrition", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesEntNutritionHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1030HomeTherapiesEntNutrition", "1", data.AnswerOrEmptyString("M1030HomeTherapiesEntNutrition").Equals("1"), "3", "Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)", new { @class = "M1030" })%>
                            <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesNone", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesNoneHidden" })%>
                            <%= Html.CheckgroupOption(Model.TypeName + "_M1030HomeTherapiesNone", "1", data.AnswerOrEmptyString("M1030HomeTherapiesNone").Equals("1"), "4", "None of the above", new { @class = "M1030" })%>
					    </ul>
				        <div class="fr oasis">
				            <div class="button oasis-help"><a onclick="Oasis.Help('M1030')" status="More Information about M1030">?</a></div>
				        </div>
				    </div>
				</div>
			</fieldset>
		</div>
	</div>
	    <%  } %>
	<%  Html.RenderPartial("Action", Model); %>
	<%  } %>
<%  } %>
</div>