<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5 && Model.AssessmentTypeNum.ToInteger() != 8) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "FromAgencyPainForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName)%>
    <%= Html.Hidden("categoryType", "EmergentCare")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <fieldset class="oasis">
        <legend>Emergent Care</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M2300">
                <label>
                    <a status="More Information about M2300" class="green" onclick="Oasis.Help('M2300')">(M2300)</a>
                    Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2300EmergentCare", "", new { @id = Model.TypeName + "_M2300EmergentCareHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2300EmergentCare", "00", data.AnswerOrEmptyString("M2300EmergentCare").Equals("00"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2300EmergentCare", "01", data.AnswerOrEmptyString("M2300EmergentCare").Equals("01"), "1", "Yes, used hospital emergency department WITHOUT hospital admission") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2300EmergentCare", "02", data.AnswerOrEmptyString("M2300EmergentCare").Equals("02"), "2", "Yes, used hospital emergency department WITH hospital admission") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2300EmergentCare", "UK", data.AnswerOrEmptyString("M2300EmergentCare").Equals("UK"), "UK", "Unknown") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2300')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2310">
                <label>
                    <a status="More Information about M2310" class="green" onclick="Oasis.Help('M2310')">(M2310)</a>
                    Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)?
                </label>
                <ul class="checkgroup">
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareMed", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareMedHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareMed", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareMed").Equals("1"), "1", "Improper medication administration, medication side effects, toxicity, anaphylaxis")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareFall", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareFallHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareFall", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareFall").Equals("1"), "2", "Injury caused by fall")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareResInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareResInfHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareResInf", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareResInf").Equals("1"), "3", "Respiratory infection (e.g., pneumonia, bronchitis)")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareOtherResInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareOtherResInfHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareOtherResInf", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareOtherResInf").Equals("1"), "4", "Other respiratory problem")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareHeartFail", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareHeartFailHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareHeartFail", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareHeartFail").Equals("1"), "5", "Heart failure (e.g., fluid overload)")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareCardiac", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareCardiacHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareCardiac", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareCardiac").Equals("1"), "6", "Cardiac dysrhythmia (irregular heartbeat)")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareMyocardial", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareMyocardialHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareMyocardial", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareMyocardial").Equals("1"), "7", "Myocardial infarction or chest pain")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareHeartDisease", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareHeartDiseaseHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareHeartDisease", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareHeartDisease").Equals("1"), "8", "Other heart disease")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareStroke", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareStrokeHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareStroke", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareStroke").Equals("1"), "9", "Stroke (CVA) or TIA")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareHypo", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareHypoHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareHypo", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareHypo").Equals("1"), "10", "Hypo/Hyperglycemia, diabetes out of control")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareGI", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareGIHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareGI", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareGI").Equals("1"), "11", "GI bleeding, obstruction, constipation, impaction")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareDehMal", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareDehMalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareDehMal", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareDehMal").Equals("1"), "12", "Dehydration, malnutrition")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareUrinaryInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareUrinaryInfHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareUrinaryInf", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareUrinaryInf").Equals("1"), "13", "Urinary tract infection")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareIV", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareIVHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareIV", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareIV").Equals("1"), "14", "IV catheter-related infection or complication")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareWoundInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareWoundInfHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareWoundInf", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareWoundInf").Equals("1"), "15", "Wound infection or deterioration")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareUncontrolledPain", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareUncontrolledPainHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareUncontrolledPain", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareUncontrolledPain").Equals("1"), "16", "Uncontrolled pain")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareMental", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareMentalHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareMental", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareMental").Equals("1"), "17", "Acute mental/behavioral health problem")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareDVT", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareDVTHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareDVT", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareDVT").Equals("1"), "18", "Deep vein thrombosis, pulmonary embolus")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareOther", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareOtherHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareOther", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareOther").Equals("1"), "19", "Other than above reasons")%>
                    <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareUK", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareUKHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2310ReasonForEmergentCareUK", "1", data.AnswerOrEmptyString("M2310ReasonForEmergentCareUK").Equals("1"), "UK", "Reason unknown")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2310')">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <% Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>