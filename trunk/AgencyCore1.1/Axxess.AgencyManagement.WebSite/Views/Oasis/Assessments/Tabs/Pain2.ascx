﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PainForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Pain.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Pain Scale</legend>
        <div class="wide-column">
            <div class="row ac">
                <label for="<%= Model.TypeName %>_GenericIntensityOfPain">Pain Intensity</label>
                <div>
                    <%  var painIntensity = new SelectList(new[] {
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5 = Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", Model.VitalSignLog.Pain.IsNull() ? "0" : Model.VitalSignLog.Pain.ToString());%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIntensityOfPain", painIntensity, new { @id = Model.TypeName + "_GenericIntensityOfPain", @title = "(Optional) Pain Intensity", @class = "pain-picker" })%>
                </div>
                <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPainOnSetDate" class="fl">Onset Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericPainOnSetDate" value="<%= data.AnswerOrEmptyString("GenericPainOnSetDate") %>" id="<%= Model.TypeName %>_GenericPainOnSetDate" status="(Optional) Onset Date" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericLocationOfPain" class="fl">Primary Site</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericLocationOfPain", data.AnswerOrEmptyString("GenericLocationOfPain"), new { @id = Model.TypeName + "_GenericLocationOfPain", @maxlength = "80", @status = "(Optional) Pain Location" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDurationOfPain" class="fl">Duration</label>
                <div class="fr">
                    <%  var duration = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Continuous", Value = "1" },
                            new SelectListItem { Text = "Intermittent", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericDurationOfPain", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericDurationOfPain", duration, new { @id = Model.TypeName + "_GenericDurationOfPain", @status = "(Optional) Pain Duration" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericQualityOfPain" class="fl">Description</label>
                <div class="fr">
                    <%  var painDescription = new SelectList(new[] { 
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Aching", Value = "1" },
                            new SelectListItem { Text = "Throbbing", Value = "2" },
                            new SelectListItem { Text = "Burning", Value = "3" },
                            new SelectListItem { Text = "Sharp", Value = "4" },
                            new SelectListItem { Text = "Tender", Value = "5" },
                            new SelectListItem { Text = "Other", Value = "6" }
                        } , "Value", "Text", data.AnswerOrDefault("GenericQualityOfPain", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericQualityOfPain", painDescription, new { @id = Model.TypeName + "_GenericQualityOfPain", @status = "(Optional) Pain Description" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedicationEffectiveness" class="strong">Current Pain Management Effectiveness</label>
                <div class="fr">
                    <%  var currentPainManagementEffectiveness = new SelectList(new[] {
                            new SelectListItem { Text = "N/A", Value = "0" },
                            new SelectListItem { Text = "Effective", Value = "1" },
                            new SelectListItem { Text = "Not Effective", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericMedicationEffectiveness", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericMedicationEffectiveness", currentPainManagementEffectiveness, new { @id = Model.TypeName + "_GenericMedicationEffectiveness", @status = "(Optional) Pain Management Effectiveness" })%>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWhatMakesPainBetter">What makes pain better</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericWhatMakesPainBetter", data.AnswerOrEmptyString("GenericWhatMakesPainBetter"), new { @id = Model.TypeName + "_GenericWhatMakesPainBetter", @status = "(Optional) Pain Improvement" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWhatMakesPainWorse">What makes pain worse</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericWhatMakesPainWorse", data.AnswerOrEmptyString("GenericWhatMakesPainWorse"), new { @id = Model.TypeName + "_GenericWhatMakesPainWorse", @status = "(Optional) Pain Exacerbation" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPatientPainGoal">Patient&#8217;s pain goal</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericPatientPainGoal", data.AnswerOrEmptyString("GenericPatientPainGoal"), new { @id = Model.TypeName + "_GenericPatientPainGoal", @status = "(Optional) Pain Goal" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPatientPainComment">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericPatientPainComment", data.AnswerOrEmptyString("GenericPatientPainComment"), new { @id = Model.TypeName + "_GenericPatientPainComment", @status = "(Optional) Pain Comments" })%></div>
            </div>
        </div>
    </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Pain Assessment</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1240">
                <label>
                    <a status="More Information about M1240" class="green" onclick="Oasis.Help('M1240')">(M1240)</a>
                    Has this patient had a formal Pain Assessment using a standardized pain assessment tool (appropriate to the patient&#8217;s ability to communicate the severity of pain)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1240FormalPainAssessment", "", new { @id = Model.TypeName + "_M1240FormalPainAssessmentHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1240FormalPainAssessment", "00", data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("00"), "0", "No standardized assessment conducted") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1240FormalPainAssessment", "01", data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("01"), "1", "Yes, and it does not indicate severe pain") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1240FormalPainAssessment", "02", data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("02"), "2", "Yes, and it indicates severe pain") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1240')" title="More Information about M1240">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum != "11" && Model.AssessmentTypeNum != "14") { %>
    <fieldset class="oasis">
        <legend>Pain Frequency</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1242">
                <label>
                    <a status="More Information about M1242" class="green" onclick="Oasis.Help('M1242')">(M1242)</a>
                    Frequency of Pain Interfering with patient&#8217;s activity or movement
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1242PainInterferingFrequency", "", new { @id = Model.TypeName + "_M1242PainInterferingFrequencyHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1242PainInterferingFrequency", "00", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("00"), "0", "Patient has no pain")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1242PainInterferingFrequency", "01", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("01"), "1", "Patient has pain that does not interfere with activity or movement")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1242PainInterferingFrequency", "02", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("02"), "2", "Less often than daily")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1242PainInterferingFrequency", "03", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("03"), "3", "Daily, but not constantly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1242PainInterferingFrequency", "04", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("04"), "4", "All of the time")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1242')" title="More Information about M1242">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Pain.ascx", Model); %>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>