<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SuppliesWorksheetForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id", @class = "input" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid", @class = "input" })%>
    <%= Html.Hidden(Model.TypeName + "_ServiceId", (int)Model.Service, new { @id = Model.TypeName + "_ServiceId", @class = "input" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "SuppliesWorksheet")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Supply Worksheet</legend>
        <div class="wide-column">
            <div class="row">
                <div class="fr">
                   <div class="ancillary-button"><a class="add-supply-button">Add Supply</a></div>
                </div>
            </div>
            <div class="row">
            <% var suppliesList = Model.Supply.ToObject<List<Supply>>(); %>
                <%=  Html.Telerik().Grid<Supply>(suppliesList).Name(Model.TypeName + "_SupplyGrid").HtmlAttributes(new { @class = "position-relative" })
                        .Columns(columns => {
                            columns.Bound(s => s.Description);
                            columns.Bound(s => s.Quantity).Width(95);
                            columns.Bound(e => e.DateForEdit).Title("Date").Format("{0:MM/dd/yyyy}").Width(120);
                            columns.Bound(s => s.UniqueIdentifier).Template(s => "<a class=\"edit link\">Edit</a><a class=\"delete link\">Delete</a>").ClientTemplate("<a class=\"edit link\">Edit</a><a class=\"delete link\">Delete</a>").Title("Action").HtmlAttributes(new { @class = "action" }).Width(100);
                            columns.Bound(s => s.UniqueIdentifier).Hidden(true).HtmlAttributes(new { @class = "uid" });
                        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Supply", "Oasis", new { patientId = Model.PatientId, eventId = Model.Id }).OperationMode(GridOperationMode.Client))
                        .ClientEvents(events => events.OnDataBound("U.OnTGridDataBound").OnDataBinding("U.OnDataBindingStopAction").OnError("U.OnTGridError"))
                        .NoRecordsTemplate("No Supplies Found").Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Supplies</legend>
        <%  string[] supplies = data.AnswerArray("485Supplies"); %>
        <%= Html.Hidden(Model.TypeName + "_485Supplies", "", new { @id = Model.TypeName + "_485SuppliesHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "1", supplies.Contains("1"), "ABDs") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "2", supplies.Contains("2"), "Ace Wrap") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "3", supplies.Contains("3"), "Alcohol Pads") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "4", supplies.Contains("4"), "Chux/Underpads") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "5", supplies.Contains("5"), "Diabetic Supplies") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "6", supplies.Contains("6"), "Drainage Bag") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "7", supplies.Contains("7"), "Dressing Supplies") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "8", supplies.Contains("8"), "Duoderm") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "9", supplies.Contains("9"), "Exam Gloves") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "10", supplies.Contains("10"), "Foley Catheter") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "11", supplies.Contains("11"), "Gauze Pads") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "12", supplies.Contains("12"), "Insertion Kit") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "13", supplies.Contains("13"), "Irrigation Set") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "14", supplies.Contains("14"), "Irrigation Solution") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "15", supplies.Contains("15"), "Kerlix Rolls") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "16", supplies.Contains("16"), "Leg Bag") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "17", supplies.Contains("17"), "Needles") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "18", supplies.Contains("18"), "NG Tube") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "19", supplies.Contains("19"), "Probe Covers") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "20", supplies.Contains("20"), "Sharps Container") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "21", supplies.Contains("21"), "Sterile Gloves") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "22", supplies.Contains("22"), "Syringe") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Supplies", "23", supplies.Contains("23"), "Tape") %>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SuppliesComment">Other</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485SuppliesComment", data.AnswerOrEmptyString("485SuppliesComment"), new { @id = Model.TypeName + "_485SuppliesComment", @title = "(Optional) Other Supplies" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>DME (Locator #14)</legend>
        <%  string[] dME = data.AnswerArray("485DME"); %>
        <%= Html.Hidden(Model.TypeName + "_485DME", "", new { @id = Model.TypeName + "_485DMEHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "1", dME.Contains("1"), "Bedside Commode")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "2", dME.Contains("2"), "Cane")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "3", dME.Contains("3"), "Elevated Toilet Seat")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "4", dME.Contains("4"), "Grab Bars")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "5", dME.Contains("5"), "Hospital Bed")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "6", dME.Contains("6"), "Nebulizer")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "7", dME.Contains("7"), "Oxygen")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "8", dME.Contains("8"), "Tub/Shower Bench")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "9", dME.Contains("9"), "Walker")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DME", "10", dME.Contains("10"), "Wheelchair")%>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485DMEComments">Other</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485DMEComments", data.AnswerOrEmptyString("485DMEComments"), new { @id = Model.TypeName + "_485DMEComments", @title = "(485 Locator 14) DME, Other" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Provider</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDMEProviderName" class="fl">Name</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDMEProviderName", data.AnswerOrEmptyString("GenericDMEProviderName"), new { @id = Model.TypeName + "_GenericDMEProviderName", @maxlength = "40", @title = "(Optional) DME Provider, Name" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDMEProviderPhone" class="fl">Phone Number</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDMEProviderPhone", data.AnswerOrEmptyString("GenericDMEProviderPhone"), new { @id = Model.TypeName + "_GenericDMEProviderPhone", @maxlength = "12", @title = "(Optional) DME Provider, Phone" })%></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDMESuppliesProvided">DME/Supplies Provided</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericDMESuppliesProvided", data.AnswerOrEmptyString("GenericDMESuppliesProvided"), new { @id = Model.TypeName + "_GenericDMESuppliesProvided", @title = "(Optional) DME Provider, Supplies Provided" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>