﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum != "08") { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "MedicationForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %> 
    <%= Html.Hidden("categoryType", AssessmentCategory.Medications.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <div id="<%= Model.TypeName %>_medications">
        <%  var medicationProfile = (Model != null && Model.MedicationProfile.IsNotNullOrEmpty()) ? Model.MedicationProfile.ToObject<MedicationProfile>() : new MedicationProfile(); %>
        <%  Html.RenderPartial("/Views/Patient/MedicationProfile/ProfileGrid.ascx", new OasisMedicationProfileViewData { Id = medicationProfile.Id, AssessmentType = Model.TypeName, Profile = medicationProfile, Service = ViewData.GetEnum<AgencyServices>("Service") }); %>
    </div>
    <%  } %>
    <fieldset>
        <legend>Medication Administration Record</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecTime" class="fl">Time</label>
                <div class="fr"><input type="text" class="time-picker" name="<%= Model.TypeName %>_GenericMedRecTime" value="<%= data.AnswerOrEmptyString("GenericMedRecTime") %>" id="<%= Model.TypeName %>_GenericMedRecTime" title="(Optional) Medication Administration Record, Time" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecMedication" class="fl">Medication</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecMedication", data.AnswerOrEmptyString("GenericMedRecMedication"), new { @id = Model.TypeName + "_GenericMedRecMedication", @maxlength = "30", @status = "(Optional) Medication Administration Record, Medication" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecDose" class="fl">Dose</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecDose", data.AnswerOrEmptyString("GenericMedRecDose"), new { @id = Model.TypeName + "_GenericMedRecDose", @maxlength = "30", @status = "(Optional) Medication Administration Record, Dose" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecRoute" class="fl">Route</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecRoute", data.AnswerOrEmptyString("GenericMedRecRoute"), new { @id = Model.TypeName + "_GenericMedRecRoute", @maxlength = "30", @status = "(Optional) Medication Administration Record, Route" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecFrequency" class="fl">Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecFrequency", data.AnswerOrEmptyString("GenericMedRecFrequency"), new { @id = Model.TypeName + "_GenericMedRecFrequency", @maxlength = "30", @status = "(Optional) Medication Administration Record, Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecPRN" class="fl">PRN Reason</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecPRN", data.AnswerOrEmptyString("GenericMedRecPRN"), new { @id = Model.TypeName + "_GenericMedRecPRN", @maxlength = "30", @status = "(Optional) Medication Administration Record, PRN" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecLocation" class="fl">Location</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecLocation", data.AnswerOrEmptyString("GenericMedRecLocation"), new { @id = Model.TypeName + "_GenericMedRecLocation", @maxlength = "30", @status = "(Optional) Medication Administration Record, Location" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecResponse" class="fl">Patient Response</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericMedRecResponse", data.AnswerOrEmptyString("GenericMedRecResponse"), new { @id = Model.TypeName + "_GenericMedRecResponse", @maxlength = "30", @status = "(Optional) Medication Administration Record, Response" })%></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecComments">Comment</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericMedRecComments", data.AnswerOrEmptyString("GenericMedRecComments"), new { @id = Model.TypeName + "_GenericMedRecComments", @status = "(Optional) Medication Administration Record, Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Medications</legend>
        <div class="wide-column">
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2000">
                <label>
                    <a status="More Information about M2000" class="green" onclick="Oasis.Help('M2000')">(M2000)</a>
                    Drug Regimen Review: Does a complete drug regimen review indicate potential clinically significant medication issues, e.g., drug reactions, ineffective drug therapy, side effects, drug interactions, duplicate therapy, omissions, dosage errors, or noncompliance?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2000DrugRegimenReview", "", new { @id = Model.TypeName + "_M2000DrugRegimenReviewHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2000DrugRegimenReview", "00", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("00"), "0", "Not assessed/reviewed") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2000DrugRegimenReview", "01", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("01"), "1", "No problems found during review") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2000DrugRegimenReview", "02", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("02"), "2", "Problems found during review") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2000DrugRegimenReview", "NA", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("NA"), "NA", "Patient is not taking any medications") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2000')" status="More Information about M2000">?</a></div>
                </div>
            </div>
            <div class="row no-input" id="<%= Model.TypeName %>_M2002">
                <label>
                    <a status="More Information about M2000" class="green" onclick="Oasis.Help('M2002')">(M2002)</a>
                    Medication Follow-up: Was a physician or the physician&#8212;designee contacted within one calendar day to resolve clinically significant medication issues, including reconciliation?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2002MedicationFollowup", "", new { @id = Model.TypeName + "_M2002MedicationFollowupHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2002MedicationFollowup", "0", data.AnswerOrEmptyString("M2002MedicationFollowup").Equals("0"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2002MedicationFollowup", "1", data.AnswerOrEmptyString("M2002MedicationFollowup").Equals("1"), "1", "Yes")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2002')" status="More Information about M2000">?</a></div>
                </div>
            </div>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2004">
                <label>
                    <a class="green" onclick="Oasis.Help('M2004')">(M2004)</a>
                    Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2004MedicationIntervention", "", new { @id = Model.TypeName + "_M2004MedicationInterventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2004MedicationIntervention", "00", data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2004MedicationIntervention", "01", data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("01"), "1", "Yes")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2004MedicationIntervention", "NA", data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("NA"), "NA", "No clinically significant medication issues identified since the previous OASIS assessment")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2004')">?</a></div>
                </div>
            </div>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2010">
                <label>
                    <a status="More Information about M2000" class="green" onclick="Oasis.Help('M2010')">(M2010)</a>
                    Patient/Caregiver High Risk Drug Education: Has the patient/caregiver received instruction on special precautions for all high-risk medications (such as hypoglycemics, anticoagulants, etc.) and how and when to report problems that may occur?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "", new { @id = Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducationHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "00", data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "01", data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("01"), "1", "Yes")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "NA", data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("NA"), "NA", "Patient not taking any high risk drugs OR patient/caregiver fully knowledgeable about special precautions associated with all high-risk medications")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2010')" status="More Information about M2010">?</a></div>
                </div>
            </div>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2015">
                <label>
                    <a class="green" onclick="Oasis.Help('M2015')">(M2015)</a>
                    Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "", new { @id = Model.TypeName + "_M2015PatientOrCaregiverDrugEducationInterventionHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "00", data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "01", data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("01"), "1", "Yes")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "NA", data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("NA"), "NA", "Patient not taking any drugs")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2015')">?</a></div>
                </div>
            </div>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2020">
                <label>
                    <a status="More Information about M2020" class="green" onclick="Oasis.Help('M2020')">(M2020)</a>
                    Management of Oral Medications: Patient&#8217;s current ability to prepare and take all oral medications reliably and safely, including administration of the correct dosage at the appropriate times/intervals. Excludes injectable and IV medications.
                    <em>(NOTE: This refers to ability, not compliance or willingness)</em>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2020ManagementOfOralMedications", "", new { @id = Model.TypeName + "_M2020ManagementOfOralMedicationsHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2020ManagementOfOralMedications", "00", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("00"), "0", "Able to independently take the correct oral medication(s) and proper dosage(s) at the correct times.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2020ManagementOfOralMedications", "01", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("01"), "1", "Able to take medication(s) at the correct times if:<ul><li><span class=\"fl\">(a)</span><span class=\"margin\">individual dosages are prepared in advance by another person; OR</span></li><li><span class=\"fl\">(b)</span><span class=\"margin\">another person develops a drug diary or chart.</span></li></ul>")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2020ManagementOfOralMedications", "02", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("02"), "2", "Able to take medication(s) at the correct times if given reminders by another person at the appropriate times")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2020ManagementOfOralMedications", "03", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("03"), "3", "Unable to take medication unless administered by another person.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2020ManagementOfOralMedications", "NA", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("NA"), "NA", "No oral medications prescribed.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2020')" title="More Information about M2020">?</a></div>
                </div>
            </div>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 6 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2030">
                <label>
                    <a status="More Information about M2030" class="green" onclick="Oasis.Help('M2030')">(M2030)</a>
                    Management of Injectable Medications: Patient&#8217;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2030ManagementOfInjectableMedications", "", new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedicationsHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2030ManagementOfInjectableMedications", "00", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("00"), "0", "Able to independently take the correct medication(s) and proper dosage(s) at the correct times.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2030ManagementOfInjectableMedications", "01", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("01"), "1", "Able to take injectable medication(s) at the correct times if:<ul><li><span class=\"fl\">(a)</span><span class=\"margin\">individual syringes are prepared in advance by another person; OR</span></li><li><span class=\"fl\">(b)</span><span class=\"margin\">another person develops a drug diary or chart.</span></li></ul>")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2030ManagementOfInjectableMedications", "02", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("02"), "2", "Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2030ManagementOfInjectableMedications", "03", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("03"), "3", "Unable to take injectable medication unless administered by another person.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2030ManagementOfInjectableMedications", "NA", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("NA"), "NA", "No injectable medications prescribed.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2030')" title="More Information about M2030">?</a></div>
                </div>
            </div>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M2040">
                <label>
                    <a status="More Information about M2040" class="green" onclick="Oasis.Help('M2040')">(M2040)</a>
                    Prior Medication Management: Indicate the patient&#8217;s usual ability with managing oral and injectable medications prior to this current illness, exacerbation, or injury. Check only one box in each row.
                </label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_M2040PriorMedicationOral">a. Oral medications</label>
                <div class="fr">
                    <%  var M2040PriorMedicationOral = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "0 - Independent", Value = "00" },
                            new SelectListItem { Text = "1 - Needed Some Help", Value = "01" },
                            new SelectListItem { Text = "2 - Dependent", Value = "02" },
                            new SelectListItem { Text = "NA - Not Applicable", Value = "NA" }
                        }, "Value", "Text", data.AnswerOrEmptyString("M2040PriorMedicationOral"));%>
                    <%= Html.DropDownList(Model.TypeName + "_M2040PriorMedicationOral", M2040PriorMedicationOral, new { @id = Model.TypeName + "_M2040PriorMedicationOral", @status = "(OASIS M2040) Prior Medication Management, Oral medications" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl" for="<%= Model.TypeName %>_M2040PriorMedicationInject">b. Injectable medications</label>
                <div class="fr">
                    <%  var M2040PriorMedicationInject = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "0 - Independent", Value = "00" },
                            new SelectListItem { Text = "1 - Needed Some Help", Value = "01" },
                            new SelectListItem { Text = "2 - Dependent", Value = "02" },
                            new SelectListItem { Text = "NA - Not Applicable", Value = "NA" }
                        }, "Value", "Text", data.AnswerOrEmptyString("M2040PriorMedicationInject"));%>
                    <%= Html.DropDownList(Model.TypeName + "_M2040PriorMedicationInject", M2040PriorMedicationInject, new { @id = Model.TypeName + "_M2040PriorMedicationInject", @status = "(OASIS M2040) Prior Medication Management, Injectable medications" })%>
                </div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row no-input" id="Div1">
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2040')" status="More Information about M2040">?</a></div>
                </div>
            </div>
        <%  } %>
        </div>
    </fieldset>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Medication.ascx", Model); %>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>