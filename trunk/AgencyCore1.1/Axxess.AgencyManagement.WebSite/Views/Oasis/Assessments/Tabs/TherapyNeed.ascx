<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "TherapyNeedForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.TherapyNeed.ToString())%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
    <fieldset class="oasis">
        <legend>Therapy Need</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M2200">
                <label for="<%= Model.TypeName %>_M2200NumberOfTherapyNeed">
                    <a status="More Information about M2200" class="green" onclick="Oasis.Help('M2200')">(M2200)</a>
                    Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?
                </label>
            </div>
            <div id="<%= Model.TypeName %>_M2200Right" class="row no-input">
                <label for="<%= Model.TypeName %>_M2200NumberOfTherapyNeed" class="fl">
                    Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).<br />
                    <em>(Enter zero [&#8220;000&#8221;] if no therapy visits indicated)</em>
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M2200NumberOfTherapyNeed", data.AnswerOrEmptyString("M2200NumberOfTherapyNeed"), new { @id = Model.TypeName + "_M2200NumberOfTherapyNeed", @class = "shorter numeric", @maxlength = "3", @status = "(OASIS M2200) Therapy Need, Number of Visits" })%>
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2200')" status="More Information about M2200">?</a></div>
                </div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_M2200TherapyNeedNA", "1", data.AnswerOrEmptyString("M2200TherapyNeedNA").Equals("1"), "NA", "Not Applicable: No case mix group defined by this assessment.") %>
                </ul>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Plan of Care Synopsis</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M2250">
                <label>
                    <a class="green" onclick="Oasis.Help('M2250')" status="More Information about M2250">(M2250)</a>
                    Plan of Care Synopsis, Does the physician-ordered plan of care include the following:
                </label>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">a.</span>
                    <span class="margin">Patient-specific parameters for notifying physician of changes in vital signs or other clinical findings</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250PatientParameters", "", new { @id = Model.TypeName + "_M2250PatientParametersHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PatientParameters", "00", data.AnswerOrEmptyString("M2250PatientParameters").Equals("00"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PatientParameters", "01", data.AnswerOrEmptyString("M2250PatientParameters").Equals("01"), "1", "Yes") %>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PatientParameters", "NA", data.AnswerOrEmptyString("M2250PatientParameters").Equals("NA"), "NA", "Physician has chosen not to establish patient-specific parameters for this patient. Agency will use standardized clinical guidelines accessible for all care providers to reference") %>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">b.</span>
                    <span class="margin">Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250DiabeticFoot", "", new { @id = Model.TypeName + "_M2250DiabeticFootHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250DiabeticFoot", "00", data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250DiabeticFoot", "01", data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250DiabeticFoot", "NA", data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("NA"), "NA", "Patient is not diabetic or is bilateral amputee")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">c.</span>
                    <span class="margin">Falls prevention interventions</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250FallsPrevention", "", new { @id = Model.TypeName + "_M2250FallsPreventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250FallsPrevention", "00", data.AnswerOrEmptyString("M2250FallsPrevention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250FallsPrevention", "01", data.AnswerOrEmptyString("M2250FallsPrevention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250FallsPrevention", "NA", data.AnswerOrEmptyString("M2250FallsPrevention").Equals("NA"), "NA", "Patient is not assessed to be at risk for falls")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">d.</span>
                    <span class="margin">Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250DepressionPrevention", "", new { @id = Model.TypeName + "_M2250DepressionPreventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250DepressionPrevention", "00", data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250DepressionPrevention", "01", data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250DepressionPrevention", "NA", data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("NA"), "NA", "Patient has no diagnosis or symptoms of depression")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">e.</span>
                    <span class="margin">Intervention(s) to monitor and mitigate pain</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "", new { @id = Model.TypeName + "_M2250MonitorMitigatePainInterventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "00", data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "01", data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "NA", data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("NA"), "NA", "No pain identified")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">f.</span>
                    <span class="margin">Intervention(s) to prevent pressure ulcers</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250PressureUlcerIntervention", "", new { @id = Model.TypeName + "_M2250PressureUlcerInterventionHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PressureUlcerIntervention", "00", data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PressureUlcerIntervention", "01", data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PressureUlcerIntervention", "NA", data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("NA"), "NA", "Patient is not assessed to be at risk for pressure ulcers")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>
                    <span class="fl">g.</span>
                    <span class="margin">Pressure ulcer treatment based on principles of moist wound healing OR order for treatment based on moist wound healing has been requested from physician</span>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2250PressureUlcerTreatment", "", new { @id = Model.TypeName + "_M2250PressureUlcerTreatmentHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PressureUlcerTreatment", "00", data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("00"), "0", "No")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PressureUlcerTreatment", "01", data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("01"), "1", "Yes")%>
                </ul>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2250PressureUlcerTreatment", "NA", data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("NA"), "NA", "Patient has no pressure ulcers with need for moist wound healing")%>
                </ul>
            </div>
            <div class="row no-input">
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2250')" status="More Information about M2250">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/TherapyNeed.ascx", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() == 5) { %>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNarrativeTemplate">Narrative</label>
                <div class="template-text">
                    <%=Html.ToggleTemplates(Model.TypeName + "_GenericDischargeNarrativeTemplate")%>
                    <%= Html.TextArea(Model.TypeName + "_GenericDischargeNarrative", data.AnswerOrEmptyString("GenericDischargeNarrative"), new { @id = Model.TypeName + "_GenericDischargeNarrative", @status = "(Optional) Narrative" })%>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div> 
<%  } %>