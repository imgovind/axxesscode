﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<div class="wrapper main"> 
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SupportiveAssistanceForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.SupportiveAssistance.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Supportive Assistance:</legend>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericSupportiveAssistanceName">Names of organizations providing assistance</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_GenericSupportiveAssistanceName", data.AnswerOrEmptyString("GenericSupportiveAssistanceName"), new { @id = Model.Type + "_GenericSupportiveAssistanceName" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>