﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() != 5 || Model.AssessmentTypeNum.ToInteger() != 8) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "CardiacForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Cardiac.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
    <fieldset class="oasis">
        <legend>Symptoms in Heart Failure Patients</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M1500">
                <label>
                    <a status="More Information about M1500" class="green" onclick="Oasis.Help('M1500')">(M1500)</a>
                    Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1500HeartFailureSymptons", "", new { @id = Model.TypeName + "_M1500HeartFailureSymptonsHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1500HeartFailureSymptons", "00", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("00"), "0", "No") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1500HeartFailureSymptons", "01", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("01"), "1", "Yes") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1500HeartFailureSymptons", "02", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("02"), "2", "Not assessed") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1500HeartFailureSymptons", "NA", data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("NA"), "NA", "Patient does not have diagnosis of heart failure") %>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1500')">?</a></div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1510">
                <label>
                    <a class="green" onclick="Oasis.Help('M1510')">(M1510)</a>
                    Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond?
                </label>
                <ul class="checkgroup one-wide">
                    <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupNoAction", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupNoActionHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1510HeartFailureFollowupNoAction", "0", data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("0"), "0", "No action taken")%>
                    <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupPhysicianCon", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupPhysicianConHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1510HeartFailureFollowupPhysicianCon", "0", data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("1"), "1", "Patient&#8217;s physician (or other primary care practitioner) contacted the same day")%>
                    <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupAdvisedEmg", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupAdvisedEmgHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1510HeartFailureFollowupAdvisedEmg", "0", data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("1"), "1", "Patient advised to get emergency treatment (e.g., call 911 or go to emergency room)")%>
                    <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupParameters", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupParametersHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1510HeartFailureFollowupParameters", "0", data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("1"), "1", "Implemented physician-ordered patient-specific established parameters for treatment")%>
                    <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupInterventions", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupInterventionsHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1510HeartFailureFollowupInterventions", "0", data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("1"), "1", "Patient education or other clinical interventions")%>
                    <%= Html.Hidden(Model.TypeName + "_M1510HeartFailureFollowupChange", "", new { @id = Model.TypeName + "_M1510HeartFailureFollowupChangeHidden" })%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_M1510HeartFailureFollowupChange", "0", data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("1"), "1", "Obtained change in care plan orders (e.g., increased monitoring by agency, change in visit frequency, telehealth, etc.)")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1510');">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <fieldset>
        <legend>Cardiovascular</legend>
        <%  string[] genericCardioVascular = data.AnswerArray("GenericCardioVascular"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericCardioVascular", "", new { @id = Model.TypeName + "_GenericCardioVascularHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericCardioVascular", "1", genericCardioVascular.Contains("1"), "WNL (Within Normal Limits)") %>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Heart Rhythm">
                            <%= string.Format("<input id='{0}_GenericCardioVascular2' name='{0}_GenericCardioVascular' value='2' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("2").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular2">Heart Rhythm</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericHeartSoundsType" class="fl">Specify</label>
                            <div class="fr">
                                <%  var heartRhythm = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Regular/WNL", Value = "1" },
                                        new SelectListItem { Text = "Tachycardia", Value = "2" },
                                        new SelectListItem { Text = "Bradycardia", Value = "3" },
                                        new SelectListItem { Text = "Arrhythmia/ Dysrhythmia", Value = "4" },
                                        new SelectListItem { Text = "Other", Value = "5" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericHeartSoundsType", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericHeartSoundsType", heartRhythm, new { @id = Model.TypeName + "_GenericHeartSoundsType", @status = "(Optional) Cardiovascular, Heart Rhythm" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Cap Refill">
                            <%= string.Format("<input id='{0}_GenericCardioVascular3' name='{0}_GenericCardioVascular' value='3' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("3").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular3">Cap Refill</label>
                        </div>
                        <div class="more ac">
                            <%  var GenericCapRefillLessThan3 = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "" },
                                    new SelectListItem { Text = "Less than (<)", Value = "1" },
                                    new SelectListItem { Text = "Greater than (>)", Value = "0" }
                                }, "Value", "Text", data.AnswerOrEmptyString("GenericCapRefillLessThan3"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericCapRefillLessThan3", GenericCapRefillLessThan3, new { @id = Model.TypeName + "_GenericCapRefillLessThan3", @status = "(Optional) Cardiovascular, Cap Refill Less Than 3 Seconds" })%>
                            3 sec
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Pulses">
                            <%= string.Format("<input id='{0}_GenericCardioVascular4' name='{0}_GenericCardioVascular' value='4' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("4").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular4">Pulses</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericCardiovascularRadial" class="fl">Radial</label>
                            <div class="fr">
                                <%  var radialPulse = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "1+ Weak", Value = "1" },
                                        new SelectListItem { Text = "2+ Normal", Value = "2" },
                                        new SelectListItem { Text = "3+ Strong", Value = "3" },
                                        new SelectListItem { Text = "4+ Bounding", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularRadial", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCardiovascularRadial", radialPulse, new { @id = Model.TypeName + "_GenericCardiovascularRadial", @status = "(Optional) Cardiovascular, Radial Pulse", @class = "short" })%>
                                <%  var GenericCardiovascularRadialPosition = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                        new SelectListItem { Text = "Left", Value = "1" },
                                        new SelectListItem { Text = "Right", Value = "2" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericCardiovascularRadialPosition"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCardiovascularRadialPosition", GenericCardiovascularRadialPosition, new { @id = Model.TypeName + "_GenericCardiovascularRadialPosition", @status = "(Optional) Cardiovascular, Radial Pulse", @class = "shorter" })%>
                            </div>
                            <div class="clr"></div>
                            <label for="<%= Model.TypeName %>_GenericCardiovascularPedal" class="fl">Pedal</label>
                            <div class="fr">
                                <%  var pedalPulse = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "1+ Weak", Value = "1" },
                                        new SelectListItem { Text = "2+ Normal", Value = "2" },
                                        new SelectListItem { Text = "3+ Strong", Value = "3" },
                                        new SelectListItem { Text = "4+ Bounding", Value = "4" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularPedal", "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCardiovascularPedal", pedalPulse, new { @id = Model.TypeName + "_GenericCardiovascularPedal", @status = "(Optional) Cardiovascular, Pedal Pulse", @class = "short" })%>
                                <%  var GenericCardiovascularPedalPosition = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                        new SelectListItem { Text = "Left", Value = "1" },
                                        new SelectListItem { Text = "Right", Value = "2" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericCardiovascularPedalPosition"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericCardiovascularPedalPosition", GenericCardiovascularPedalPosition, new { @id = Model.TypeName + "_GenericCardiovascularPedalPosition", @status = "(Optional) Cardiovascular, Pedal Pulse", @class = "shorter" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Edema">
                            <%= string.Format("<input id='{0}_GenericCardioVascular5' name='{0}_GenericCardioVascular' value='5' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("5").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular5">Edema</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericEdemaLocation" class="fl">Location</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericEdemaLocation", data.AnswerOrEmptyString("GenericEdemaLocation"), new { @id = Model.TypeName + "_GenericEdemaLocation", @maxlength = "50", @status = "(Optional) Cardiovascular, Edema Location" })%></div>
                            <div class="clr"></div>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Cardiovascular, Pitting Edema">
                                        <%  string[] genericPittingEdemaType = data.AnswerArray("GenericPittingEdemaType"); %>
                                        <%= Html.Hidden(Model.TypeName + "_GenericPittingEdemaType", "", new { @id = Model.TypeName + "_GenericPittingEdemaTypeHidden" })%>
                                        <%= string.Format("<input id='{0}_GenericPittingEdemaType1' name='{0}_GenericPittingEdemaType' value='1' type='checkbox' {1} />", Model.TypeName, genericPittingEdemaType.Contains("1").ToChecked()) %>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericPittingEdemaType1">Pitting</label>
                                            <div id="<%= Model.TypeName %>_GenericPittingEdemaType1More" class="fr">
                                                <%  var pitting = new SelectList(new[] {
                                                        new SelectListItem { Text = "", Value = "0" },
                                                        new SelectListItem { Text = "1+", Value = "1" },
                                                        new SelectListItem { Text = "2+", Value = "2" },
                                                        new SelectListItem { Text = "3+", Value = "3" },
                                                        new SelectListItem { Text = "4+", Value = "4" }
                                                    }, "Value", "Text", data.AnswerOrDefault("GenericEdemaPitting", "0"));%>
                                                <%= Html.DropDownList(Model.TypeName + "_GenericEdemaPitting", pitting, new { @id = Model.TypeName + "_GenericEdemaPitting", @status = "(Optional) Cardiovascular, Pitting Edema Severity" })%>
                                            </div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Cardiovascular, Nonpitting Edema">
                                        <%= string.Format("<input id='{0}_GenericPittingEdemaType2' name='{0}_GenericPittingEdemaType' value='2' type='checkbox' {1} />", Model.TypeName, genericPittingEdemaType.Contains("2").ToChecked())%>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericPittingEdemaType2">Nonpitting</label>
                                            <div id="<%= Model.TypeName %>_GenericPittingEdemaType2More" class="fr">
                                                <%  var nonPitting = new SelectList(new[] {
                                                        new SelectListItem { Text = "", Value = "0" },
                                                        new SelectListItem { Text = "N/A", Value = "1" },
                                                        new SelectListItem { Text = "Mild", Value = "2" },
                                                        new SelectListItem { Text = "Moderate", Value = "3" },
                                                        new SelectListItem { Text = "Severe", Value = "4" }
                                                    }, "Value", "Text", data.AnswerOrDefault("GenericEdemaNonPitting", "0"));%>
                                                <%= Html.DropDownList(Model.TypeName + "_GenericEdemaNonPitting", nonPitting, new { @id = Model.TypeName + "_GenericEdemaNonPitting", @status = "(Optional) Cardiovascular, Nonpitting Edema Severity", @class = "short" })%>
                                                <%  var GenericEdemaNonPittingPosition = new SelectList(new[] {
                                                        new SelectListItem { Text = "", Value = "" },
                                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                                        new SelectListItem { Text = "Left", Value = "1" },
                                                        new SelectListItem { Text = "Right", Value = "2" }
                                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericEdemaNonPittingPosition"));%>
                                                <%= Html.DropDownList(Model.TypeName + "_GenericEdemaNonPittingPosition", GenericEdemaNonPittingPosition, new { @id = Model.TypeName + "_GenericEdemaNonPittingPosition", @status = "(Optional) Cardiovascular, Nonpitting Edema", @class = "shorter" })%>
                                            </div>
                                        </span>
                                        <div class="clr"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Dizziness">
                            <%= string.Format("<input id='{0}_GenericCardioVascular6' name='{0}_GenericCardioVascular' value='6' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("6").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular6">Dizziness</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericDizzinessDesc" class="fl">Describe</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericDizzinessDesc", data.AnswerOrEmptyString("GenericDizzinessDesc"), new { @id = Model.TypeName + "_GenericDizzinessDesc", @maxlength = "20", @status = "(Optional) Cardiovascular, Dizziness" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Chest Pain">
                            <%= string.Format("<input id='{0}_GenericCardioVascular7' name='{0}_GenericCardioVascular' value='7' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("7").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular7">Chest Pain</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericChestPainDesc" class="fl">Describe</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericChestPainDesc", data.AnswerOrEmptyString("GenericChestPainDesc"), new { @id = Model.TypeName + "_GenericChestPainDesc", @maxlength = "20", @status = "(Optional) Cardiovascular, Chest Pain" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Cardiovascular, Neck Vein Distention">
                            <%= string.Format("<input id='{0}_GenericCardioVascular8' name='{0}_GenericCardioVascular' value='8' type='checkbox' {1} />", Model.TypeName, genericCardioVascular.Contains("8").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericCardioVascular8">Neck Vein Distention</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericChestPainDesc" class="fl">Describe</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNeckVeinDistentionDesc", data.AnswerOrEmptyString("GenericNeckVeinDistentionDesc"), new { @id = Model.TypeName + "_GenericNeckVeinDistentionDesc", @maxlength = "20", @status = "(Optional) Cardiovascular, Neck Vein Distention" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPacemakerDate" class="fl">Pacemaker Insertion Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericPacemakerDate" value="<%= data.AnswerOrEmptyString("GenericPacemakerDate") %>" id="<%= Model.TypeName %>_GenericPacemakerDate" title="(Optional) Cardiovascular, Pacemaker Insertion Date" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPacemakerDate" class="fl">AICD Insertion Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericAICDDate" value="<%= data.AnswerOrEmptyString("GenericAICDDate") %>" id="<%= Model.TypeName %>_GenericAICDDate" title="(Optional) Cardiovascular, AICD Insertion Date" /></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericAICDDate">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericCardiovascularComments", data.AnswerOrEmptyString("GenericCardiovascularComments"), new { @id = Model.TypeName + "_GenericCardiovascularComments", @status = "(Optional) Cardiovascular Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <% Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>