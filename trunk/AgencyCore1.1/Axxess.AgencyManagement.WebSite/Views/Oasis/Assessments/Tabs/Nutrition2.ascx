﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "NutritionForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Nutrition.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Nutrition</legend>
        <%  string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
        <%  string[] genericProblem = data.AnswerArray("GenericProblem"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNutrition", "", new { @id = Model.TypeName + "_GenericNutritionHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Within Normal Limits">
                            <%= string.Format("<input id='{0}_GenericNutrition1' name='{0}_GenericNutrition' value='1' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition1">WNL (Within Normal Limits)</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Dysphagia">
                            <%= string.Format("<input id='{0}_GenericNutrition2' name='{0}_GenericNutrition' value='2' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition2">Dysphagia</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Appetite">
                            <%= string.Format("<input id='{0}_GenericNutrition3' name='{0}_GenericNutrition' value='3' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition3">Appetite</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Weight">
                            <%= string.Format("<input id='{0}_GenericNutrition4' name='{0}_GenericNutrition' value='4' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition4">Weight Change</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionWeightGainLoss" class="fl">Specify</label>
                            <div class="fr">
                                <%  var GenericNutritionWeightGainLoss = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Loss", Value = "Loss" },
                                        new SelectListItem { Text = "Gain ", Value = "Gain" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericNutritionWeightGainLoss"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNutritionWeightGainLoss", GenericNutritionWeightGainLoss, new { @id = Model.TypeName + "_GenericNutritionWeightGainLoss", @status = "(Optional) Nutrition, Weight Loss/Gain" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Diet">
                            <%= string.Format("<input id='{0}_GenericNutrition5' name='{0}_GenericNutrition' value='5' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("5").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition5">Diet</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionDietAdequate" class="fl">Adequate?</label>
                            <div class="fr">
                                <%  var GenericNutritionDietAdequate = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Adequate", Value = "Adequate" },
                                        new SelectListItem { Text = "Inadequate ", Value = "Inadequate" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericNutritionDietAdequate"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericNutritionDietAdequate", GenericNutritionDietAdequate, new { @id = Model.TypeName + "_GenericNutritionDietAdequate", @status = "(Optional) Nutrition, Adequate Diet" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Diet Type">
                            <%= string.Format("<input id='{0}_GenericNutrition9' name='{0}_GenericNutrition' value='9' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("9").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition9">Diet Type</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionDietType" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNutritionDietType", data.AnswerOrEmptyString("GenericNutritionDietType"), new { @id = Model.TypeName + "_GenericNutritionDietType", @maxlength = "100", @title = "(Optional) Nutrition, Diet Type" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding">
                            <%= string.Format("<input id='{0}_GenericNutrition6' name='{0}_GenericNutrition' value='6' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("6").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition6">Enteral Feeding</label>
                        </div>
                        <div class="more">
                            <%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionEnteralFeeding", "", new { @id = Model.TypeName + "_GenericNutritionEnteralFeedingHidden" })%>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding, NG">
                                        <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding1' name='{0}_GenericNutritionEnteralFeeding' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("1").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding1">NG</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding, PEG">
                                        <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding2' name='{0}_GenericNutritionEnteralFeeding' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("2").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding2">PEG</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper" status="(Optional) Nutrition, Enteral Feeding, Dobhoff">
                                        <%= string.Format("<input title='' id='{0}_GenericNutritionEnteralFeeding3' name='{0}_GenericNutritionEnteralFeeding' value='3' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("3").ToChecked()) %>
                                        <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding3">Dobhoff</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Tube Placement Checked">
                            <%= string.Format("<input id='{0}_GenericNutrition7' name='{0}_GenericNutrition' value='7' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("7").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition7">Tube Placement Checked</label>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nutrition, Residual Checked">
                            <%= string.Format("<input id='{0}_GenericNutrition8' name='{0}_GenericNutrition' value='8' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("8").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_GenericNutrition8" class="inline-radio">Residual Checked</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount" class="fl">Amount</label>
                            <div class="fr">
                                <%= Html.TextBox(Model.TypeName + "_GenericNutritionResidualCheckedAmount", data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount"), new { @id = Model.TypeName + "_GenericNutritionResidualCheckedAmount", @class = "shorter numeric", @maxlength = "5", @title = "(Optional) Nutrition, Residual Checked Amount" })%>
                                <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount">ml</label>
                            </div>
                            <div class="clr"></div>
                            <ul class="checkgroup one-wide">
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem1' name='{0}_GenericProblem' value='1' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("1").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericProblem1">Throat problems?</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem2' name='{0}_GenericProblem' value='2' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("2").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericProblem2">Sore throat?</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem3' name='{0}_GenericProblem' value='3' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("3").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericProblem3">Dentures?</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem4' name='{0}_GenericProblem' value='4' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("4").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericProblem4">Hoarseness?</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem5' name='{0}_GenericProblem' value='5' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("5").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericProblem5">Dental problems?</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem6' name='{0}_GenericProblem' value='6' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("6").ToChecked())%>
                                        <label for="<%= Model.TypeName %>_GenericProblem6">Problems chewing?</label>
                                    </div>
                                </li>
                                <li class="option">
                                    <div class="wrapper">
                                        <%= string.Format("<input id='{0}_GenericProblem7' name='{0}_GenericProblem' value='7' type='checkbox' {1} />", Model.TypeName, genericProblem.Contains("7").ToChecked())%>
                                        <span>
                                            <label for="<%= Model.TypeName %>_GenericProblem7">Other</label>
                                            <div id="<%= Model.TypeName %>_GenericProblem7More" class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNutritionOther", data.AnswerOrEmptyString("GenericNutritionOther"), new { @id = Model.TypeName + "_GenericNutritionOther", @maxlength = "100" })%></div>
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNutritionComments">Comments</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNutritionComments", data.AnswerOrEmptyString("GenericNutritionComments"), 10, 50, new { @id = Model.TypeName + "_GenericNutritionCommentsComments", @title = "(Optional) Nutrition Comments" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
<%  } %>
</div>