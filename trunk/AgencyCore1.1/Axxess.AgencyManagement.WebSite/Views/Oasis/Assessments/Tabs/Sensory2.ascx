﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SensoryStatusForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.Sensory.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Eyes</legend>
        <%  string[] eyes = data.AnswerArray("GenericEyes"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericEyes", "", new { @id = Model.TypeName + "_GenericEyesHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "1", eyes.Contains("1"), "WNL (Within Normal Limits)")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "2", eyes.Contains("2"), "Glasses")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "3", eyes.Contains("3"), "Contacts Left")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "4", eyes.Contains("4"), "Contacts Right")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "5", eyes.Contains("5"), "Blurred Vision")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "6", eyes.Contains("6"), "Glaucoma")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "7", eyes.Contains("7"), "Cataracts")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEyes", "8", eyes.Contains("8"), "Macular Degeneration")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Eyes, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericEyes13' name='{0}_GenericEyes' value='13' {1} />", Model.TypeName, eyes.Contains("13").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericEyes13">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericEyesOtherDetails">Specify</label>
                            <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericEyesOtherDetails", data.AnswerOrEmptyString("GenericEyesOtherDetails"), new { @id = Model.TypeName + "_GenericEyesOtherDetails", @maxlength = "200", @status = "(Optional) Eyes, Specify Other", @class = "short" })%></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row narrower">
                <label for="<%= Model.TypeName %>_GenericEyesLastEyeExamDate" class="fl">Date of Last Eye Exam</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericEyesLastEyeExamDate" value="<%= data.AnswerOrEmptyString("GenericEyesLastEyeExamDate") %>" id="<%= Model.TypeName %>_GenericEyesLastEyeExamDate" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Ears</legend>
        <%  string[] ears = data.AnswerArray("GenericEars"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericEars", "", new { @id = Model.TypeName + "_GenericEarsHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEars", "1", ears.Contains("1"), "WNL (Within Normal Limits)")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEars", "3", ears.Contains("3"), "Deaf")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEars", "4", ears.Contains("4"), "Drainage")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericEars", "5", ears.Contains("5"), "Pain")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Ears, Hearing Impaired">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericEars2' name='{0}_GenericEars' value='2' {1} />", Model.TypeName, ears.Contains("2").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericEars2">Hearing Impaired</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericEarsHearingImpairedPosition" class="fl">Position</label>
                            <div class="fr">
                                <%  var GenericEarsHearingImpairedPosition = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                        new SelectListItem { Text = "Left", Value = "1" },
                                        new SelectListItem { Text = "Right", Value = "2" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericEarsHearingImpairedPosition", GenericEarsHearingImpairedPosition, new { @id = Model.TypeName + "_GenericEarsHearingImpairedPosition" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Ears, Hearing Aids">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericEars6' name='{0}_GenericEars' value='6' {1} />", Model.TypeName, ears.Contains("6").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericEars6">Hearing Aids</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericEarsHearingAidsPosition" class="fl">Position</label>
                            <div class="fr">
                                <%  var GenericEarsHearingAidsPosition = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "" },
                                        new SelectListItem { Text = "Bilateral", Value = "0" },
                                        new SelectListItem { Text = "Left", Value = "1" },
                                        new SelectListItem { Text = "Right", Value = "2" }
                                    }, "Value", "Text", data.AnswerOrEmptyString("GenericEarsHearingAidsPosition")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericEarsHearingAidsPosition", GenericEarsHearingAidsPosition, new { @id = Model.TypeName + "_GenericEarsHearingAidsPosition" })%>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Ears, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericEars7' name='{0}_GenericEars' value='7' {1} />", Model.TypeName, ears.Contains("7").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericEars7">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericEarsOtherDetails">Specify</label>
                            <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericEarsOtherDetails", data.AnswerOrEmptyString("GenericEarsOtherDetails"), new { @id = Model.TypeName + "_GenericEarsOtherDetails", @maxlength = "200", @status = "(Optional) Ears, Specify Other", @class = "short" })%></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nose</legend>
        <%  string[] nose = data.AnswerArray("GenericNose"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNose", "", new { @id = Model.TypeName + "_GenericNoseHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNose", "1", nose.Contains("1"), "WNL (Within Normal Limits)")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNose", "2", nose.Contains("2"), "Congestion")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericNose", "3", nose.Contains("3"), "Loss of Smell")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nose, Nose Bleeds">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericNose4' name='{0}_GenericNose' value='4' {1} />", Model.TypeName, nose.Contains("4").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNose4">Nose Bleeds</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNoseBleedsFrequency" class="fl">How Often</label>
                            <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericNoseBleedsFrequency", data.AnswerOrEmptyString("GenericNoseBleedsFrequency"), new { @class = "oe", @id = Model.TypeName + "_GenericNoseBleedsFrequency", @maxlength = "10", @status = "(Optional) " })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Nose, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericNose5' name='{0}_GenericNose' value='5' {1} />", Model.TypeName, nose.Contains("5").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericNose5">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericNoseOtherDetails">Specify</label>
                            <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericNoseOtherDetails", data.AnswerOrEmptyString("GenericNoseOtherDetails"), new { @id = Model.TypeName + "_GenericNoseOtherDetails", @maxlength = "200", @status = "(Optional) Nose, Specify Other", @class = "short" })%></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Throat</legend>
        <%  string[] throat = data.AnswerArray("GenericThroat"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericThroat", "", new { @id = Model.TypeName + "_GenericThroatHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericThroat", "1", throat.Contains("1"), "WNL (Within Normal Limits)")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericThroat", "2", throat.Contains("2"), "Sore throat")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_GenericThroat", "3", throat.Contains("3"), "Hoarseness")%>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Throat, Other">
                            <%= string.Format("<input type='checkbox' id='{0}_GenericThroat4' name='{0}_GenericThroat' value='4' {1} />", Model.TypeName, throat.Contains("4").ToChecked())%>
                            <label for="<%= Model.TypeName %>_GenericThroat4">Other</label>
                        </div>
                        <div class="more">
                            <label for="<%= Model.TypeName %>_GenericThroatOther">Specify</label>
                            <div class="ac"><%= Html.TextArea(Model.TypeName + "_GenericThroatOther", data.AnswerOrEmptyString("GenericThroatOther"), new { @id = Model.TypeName + "_GenericThroatOther", @maxlength = "200", @status = "(Optional) Throat, Specify Other", @class = "short" })%></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
    <fieldset class="oasis">
        <legend>Vision</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1200">
                <label>
                    <a status="More Information about M1200" class="green" onclick="Oasis.Help('M1200')">(M1200)</a>
                    Vision (with corrective lenses if the patient usually wears them)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1200Vision", "", new { @id = Model.TypeName + "_M1200VisionHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1200Vision", "00", data.AnswerOrEmptyString("M1200Vision").Equals("00"), "0", "Normal vision: sees adequately in most situations; can see medication labels, newsprint.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1200Vision", "01", data.AnswerOrEmptyString("M1200Vision").Equals("01"), "1", "Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&#8217;s length.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1200Vision", "02", data.AnswerOrEmptyString("M1200Vision").Equals("02"), "2", "Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1200')" status="More Information about M1200">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Hearing</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M1210">
                <label>
                    <a status="More Information about M1210" class="green" onclick="Oasis.Help('M1210')">(M1210)</a>
                    Ability to hear (with hearing aid or hearing appliance if normally used)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1210Hearing", "", new { @id = Model.TypeName + "_M1210HearingHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1210Hearing", "00", data.AnswerOrEmptyString("M1210Hearing").Equals("00"), "0", "Adequate: hears normal conversation without difficulty.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1210Hearing", "01", data.AnswerOrEmptyString("M1210Hearing").Equals("01"), "1", "Mildly to Moderately Impaired: difficulty hearing in some environments or speaker may need to increase volume or speak distinctly.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1210Hearing", "02", data.AnswerOrEmptyString("M1210Hearing").Equals("02"), "2", "Severely Impaired: absence of useful hearing.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1210Hearing", "UK", data.AnswerOrEmptyString("M1210Hearing").Equals("UK"), "UK", "Unable to assess hearing.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1210')" status="More Information about M1210">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Speech/Verbal</legend>
        <div class="wide-column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1220">
                <label>
                    <a status="More Information about M1220" class="green" onclick="Oasis.Help('M1220')">(M1220)</a>
                    Understanding of Verbal Content in patient&#8217;s own language (with hearing aid or device if used)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1220VerbalContent", "", new { @id = Model.TypeName + "_M1220VerbalContentHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1220VerbalContent", "00", data.AnswerOrEmptyString("M1220VerbalContent").Equals("00"), "0", "Understands: clear comprehension without cues or repetitions.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1220VerbalContent", "01", data.AnswerOrEmptyString("M1220VerbalContent").Equals("01"), "1", "Usually Understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1220VerbalContent", "02", data.AnswerOrEmptyString("M1220VerbalContent").Equals("02"), "2", "Sometimes Understands: understands only basic conversations or simple, direct phrases. Frequently requires cues to understand.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1220VerbalContent", "03", data.AnswerOrEmptyString("M1220VerbalContent").Equals("03"), "3", "Rarely/Never Understands.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1220VerbalContent", "UK", data.AnswerOrEmptyString("M1220VerbalContent").Equals("UK"), "UK", "Unable to assess understanding.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1220')" status="More Information about M1220">?</a></div>
                </div>
            </div>
            <%  } %>
            <div class="row no-input" id="<%= Model.TypeName %>_M1230">
                <label>
                    <a status="More Information about M1230" class="green" onclick="Oasis.Help('M1230')">(M1230)</a>
                    Speech and Oral (Verbal) Expression of Language (in patient&#8217;s own language)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1230SpeechAndOral", "", new { @id = Model.TypeName + "_M1230SpeechAndOralHidden" })%>
                <ul class="checkgroup one-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1230SpeechAndOral", "00", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("00"), "0", "Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1230SpeechAndOral", "01", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("01"), "1", "Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1230SpeechAndOral", "02", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("02"), "2", "Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1230SpeechAndOral", "03", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("03"), "3", "Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases.")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1230SpeechAndOral", "04", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("04"), "4", "Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M1230SpeechAndOral", "05", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("05"), "5", "Patient nonresponsive or unable to speak.")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M1230')" title="More Information about M1230">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Sensory.ascx", Model); %>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>