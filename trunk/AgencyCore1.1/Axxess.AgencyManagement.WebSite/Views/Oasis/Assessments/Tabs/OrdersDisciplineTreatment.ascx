<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>"  %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5||(Model.Version==2 && Model.AssessmentTypeNum.ToInteger() % 10==9)) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "OrdersDisciplineTreatmentForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", AssessmentCategory.OrdersDisciplineTreatment.ToString())%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <%  Html.RenderPartial("Action", Model); %>
    <fieldset>
        <legend>Orders for Discipline &#38; Treatments</legend>
        <%  if ((AgencyServices)ViewData["Service"] != AgencyServices.PrivateDuty) { %>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_BlankMasterCalendar"></div>
            <div class="row ac">
                <div class="button"><a class="oasis-master-calendar">Show Calendar</a></div>
            </div>
        </div>
        <%  } %>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485SNFrequency" class="fl">SN Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485SNFrequency", data.AnswerOrEmptyString("485SNFrequency"), new { @id = Model.TypeName + "_485SNFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, SN Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485PTFrequency" class="fl">PT Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485PTFrequency", data.AnswerOrEmptyString("485PTFrequency"), new { @id = Model.TypeName + "_485PTFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, PT Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485OTFrequency" class="fl">OT Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485OTFrequency", data.AnswerOrEmptyString("485OTFrequency"), new { @id = Model.TypeName + "_485OTFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, OT Frequency" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485STFrequency" class="fl">ST Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485STFrequency", data.AnswerOrEmptyString("485STFrequency"), new { @id = Model.TypeName + "_485STFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, ST Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485MSWFrequency" class="fl">MSW Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485MSWFrequency", data.AnswerOrEmptyString("485MSWFrequency"), new { @id = Model.TypeName + "_485MSWFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, MSW Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485HHAFrequency" class="fl">HHA Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485HHAFrequency", data.AnswerOrEmptyString("485HHAFrequency"), new { @id = Model.TypeName + "_485HHAFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, HHA Frequency" })%></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485OrdersDisciplineInterventionComments">Additional Orders</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.TypeName + "_485OrdersDisciplineInterventionTemplates") %>
                    <%= Html.TextArea(Model.TypeName + "_485OrdersDisciplineInterventionComments", data.AnswerOrEmptyString("485OrdersDisciplineInterventionComments"), new { @id = Model.TypeName + "_485OrdersDisciplineInterventionComments", @title = "(485 Locator 21) Orders" }) %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Conclusions</legend>
        <%  string[] conclusions = data.AnswerArray("485Conclusions"); %>
        <%= Html.Hidden(Model.TypeName + "_485Conclusions", "", new { @id = Model.TypeName + "_485ConclusionsHidden" })%>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Conclusions", "1", conclusions.Contains("1"), "Skilled Intervention Needed") %>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Conclusions", "2", conclusions.Contains("2"), "Skilled Instruction Needed")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485Conclusions", "3", conclusions.Contains("3"), "No Skilled Service Needed")%>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485ConclusionOther">Other</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485ConclusionOther", data.AnswerOrEmptyString("485ConclusionOther"), new { @id = Model.TypeName + "_485ConclusionOther", @title = "(Optional) Other Conclusions" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Rehabilitation Potential</legend>
        <%  string[] rehabilitationPotential = data.AnswerArray("485RehabilitationPotential"); %>
        <%= Html.Hidden(Model.TypeName + "_485RehabilitationPotential", "", new { @id = Model.TypeName + "_485RehabilitationPotentialHidden" })%>
        <div class="wide-column">
            <div class="row">
                <label>Rehabilitation potential for stated goals</label>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485RehabilitationPotential", "1", rehabilitationPotential.Contains("1"), "Good")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485RehabilitationPotential", "2", rehabilitationPotential.Contains("2"), "Fair")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485RehabilitationPotential", "3", rehabilitationPotential.Contains("3"), "Poor")%>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485AchieveGoalsComments">Other rehabilitation potential</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.TypeName + "_485AchieveGoalsTemplates") %>
                    <%= Html.TextArea(Model.TypeName + "_485AchieveGoalsComments", data.AnswerOrEmptyString("485AchieveGoalsComments"), new { @id = Model.TypeName + "_485AchieveGoalsComments", @title = "(Optional) Other Rehab Potentials" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Discharge Plans</legend>
        <%  string[] dischargePlans = data.AnswerArray("485DischargePlans"); %>
        <%= Html.Hidden(Model.TypeName + "_485DischargePlans", "", new { @id = Model.TypeName + "_485DischargePlansHidden" })%>
        <div class="wide-column">
            <div class="row">
                <label>Patient to be discharged to the care of</label>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DischargePlans", "1", dischargePlans.Contains("1"), "Physician")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DischargePlans", "2", dischargePlans.Contains("2"), "Caregiver")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DischargePlans", "3", dischargePlans.Contains("3"), "Self care")%>
               </ul>
            </div>
            <div class="row">
                <label class="strong">Discharge Plans</label>
                <%  string[] dischargePlansReason = data.AnswerArray("485DischargePlansReason"); %>
                <%= Html.Hidden(Model.TypeName + "_485DischargePlansReason", "", new { @id = Model.TypeName + "_485DischargePlansReasonHidden" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DischargePlansReason", "1", dischargePlansReason.Contains("1"), "Discharge when caregiver willing and able to manage all aspects of patient&#8217;s care.")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DischargePlansReason", "2", dischargePlansReason.Contains("2"), "Discharge when goals met.")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485DischargePlansReason", "3", dischargePlansReason.Contains("3"), "Discharge when wound(s) healed.")%>
                </ul>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485DischargePlanComments">Additional discharge plans</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates(Model.TypeName + "_485DischargePlanTemplates") %>
                    <%= Html.TextArea(Model.TypeName + "_485DischargePlanComments", data.AnswerOrEmptyString("485DischargePlanComments"), new { @id = Model.TypeName + "_485DischargePlanComments", @title = "(Optional) Other Discharge Plans" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide-column">
        <%  if (Model.AssessmentTypeNum.ToInteger() == 1) { %>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInitialSummaryComments">Initial Summary of Patient History</label>
                <div class="template-text">
                    <%=Html.ToggleTemplates(Model.TypeName + "_485SkilledInitialSummaryTemplate")%>
                    <%= Html.TextArea(Model.TypeName + "_485SkilledInitialSummaryComments", data.AnswerOrEmptyString("485SkilledInitialSummaryComments"), new { @id = Model.TypeName + "_485SkilledInitialSummaryComments", @title = "(Optional) Narrative" })%>
                </div>
            </div>
        <%  } %>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInterventionComments">Skilled Intervention/Teaching</label>
                <div class="template-text">
                    <%=Html.ToggleTemplates(Model.TypeName + "_485SkilledInterventionTemplate")%>
                    <%= Html.TextArea(Model.TypeName + "_485SkilledInterventionComments", data.AnswerOrEmptyString("485SkilledInterventionComments"), new { @id = Model.TypeName + "_485SkilledInterventionComments", @style = "height:150px", @title = "(Optional) Narrative" })%>
                </div>
            </div>
            <div class="row">
                <%  string[] iResponse = data.AnswerArray("485SIResponse"); %>
                <%= Html.Hidden(Model.TypeName + "_485SIResponse", "", new { @id = Model.TypeName + "_485SIResponseHidden" })%>
                <ul class="checkgroup two-wide">
                    <li class="option">
                        <div class="wrapper" status="(Optional) Narrative, Verbalizes Understanding">
                            <%= string.Format("<input id='{0}_485SIResponse1' name='{0}_485SIResponse' value='1' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("1").ToChecked()) %>
                            <span>
                                <label for="<%= Model.TypeName %>_485SIResponse1">Verbalizes</label>
                                <%= Html.TextBox(Model.TypeName + "_485SIVerbalizedUnderstandingPercent", data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPercent"), new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingPercent" })%>
                                <label for="<%= Model.TypeName %>_485SIResponse1">understanding of teaching.</label>
                            </span>
                        </div>
                        <div class="more">
                            <ul class="checkgroup two-wide">
                                <%= Html.Hidden(Model.TypeName + "_485SIVerbalizedUnderstandingPT", "", new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingPTHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485SIVerbalizedUnderstandingPT", "1", data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPT").Equals("1"), "PT")%>
                                <%= Html.Hidden(Model.TypeName + "_485SIVerbalizedUnderstandingCG", "", new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingCGHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485SIVerbalizedUnderstandingCG", "1", data.AnswerOrEmptyString("485SIVerbalizedUnderstandingCG").Equals("1"), "CG")%>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Narrative, Needs Further Teaching">
                            <%= string.Format("<input id='{0}_485SIResponse2' name='{0}_485SIResponse' value='2' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("2").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIResponse2">Needs further teaching.</label>
                        </div>
                        <div class="more">
                            <ul class="checkgroup two-wide">
                                <%= Html.Hidden(Model.TypeName + "_485NEEDSFURTHERTEACHINGPT", "", new { @id = Model.TypeName + "_485NEEDSFURTHERTEACHINGPTHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485NEEDSFURTHERTEACHINGPT", "1", data.AnswerOrEmptyString("485NEEDSFURTHERTEACHINGPT").Equals("1"), "PT")%>
                                <%= Html.Hidden(Model.TypeName + "_485NEEDSFURTHERTEACHINGCG", "", new { @id = Model.TypeName + "_485NEEDSFURTHERTEACHINGCGHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485NEEDSFURTHERTEACHINGCG", "1", data.AnswerOrEmptyString("485NEEDSFURTHERTEACHINGCG").Equals("1"), "CG")%>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Narrative, Able to Return Demonstration">
                            <%= string.Format("<input id='{0}_485SIResponse3' name='{0}_485SIResponse' value='3' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("3").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIResponse3">Able to return correct demonstration of procedure.</label>
                        </div>
                        <div class="more">
                            <ul class="checkgroup two-wide">
                                <%= Html.Hidden(Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPT", "", new { @id = Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPTHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPT", "1", data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDUREPT").Equals("1"), "PT")%>
                                <%= Html.Hidden(Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECG", "", new { @id = Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECGHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECG", "1", data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDURECG").Equals("1"), "CG")%>
                            </ul>
                        </div>
                    </li>
                    <li class="option">
                        <div class="wrapper" status="(Optional) Narrative, Unable to Return Demonstration">
                            <%= string.Format("<input id='{0}_485SIResponse4' name='{0}_485SIResponse' value='4' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("4").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIResponse4">Unable to return correct demonstration of procedure.</label>
                        </div>
                        <div class="more">
                            <ul class="checkgroup two-wide">
                                <%= Html.Hidden(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPT", "", new { @id = Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPTHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPT", "1", data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDUREPT").Equals("1"), "PT")%>
                                <%= Html.Hidden(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECG", "", new { @id = Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECGHidden" })%>
                                <%= Html.CheckgroupOption(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECG", "1", data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDURECG").Equals("1"), "CG")%>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Coordination of Care</legend>
        <div class="wide-column">
            <div class="row">
                <label>Conferenced With</label>
                <%  string[] conferencedWithName = data.AnswerArray("485ConferencedWith"); %>
                <%= Html.Hidden(Model.TypeName + "_485ConferencedWith", "", new { @id = Model.TypeName + "_485ConferencedWithHidden" })%>
                <ul class="checkgroup seven-wide">
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "MD", conferencedWithName.Contains("MD"), "MD")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "SN", conferencedWithName.Contains("SN"), "SN")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "PT", conferencedWithName.Contains("PT"), "PT")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "OT", conferencedWithName.Contains("OT"), "OT")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "ST", conferencedWithName.Contains("ST"), "ST")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "MSW", conferencedWithName.Contains("MSW"), "MSW")%>
                    <%= Html.CheckgroupOption(Model.TypeName + "_485ConferencedWith", "HHA", conferencedWithName.Contains("HHA"), "HHA")%>
                </ul>
            </div>
            <div class="row narrower">
                <label for="<%= Model.TypeName %>_485ConferencedWithName" class="fl">Name</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485ConferencedWithName", data.AnswerOrEmptyString("485ConferencedWithName"), new { @id = Model.TypeName + "_485ConferencedWithName", @maxlength = "30", @title = "(Optional) Coordination of Care Name" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInterventionRegarding">Regarding</label>
                <div class="ac"><%= Html.TextArea(Model.TypeName + "_485SkilledInterventionRegarding", data.AnswerOrEmptyString("485SkilledInterventionRegarding"), new { @id = Model.TypeName + "_485SkilledInterventionRegarding", @title = "(Optional) Coordination of Care Regarding" })%></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>