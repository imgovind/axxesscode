<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "ManagementForm" })) { %>
        <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
        <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
        <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
        <%= Html.Hidden("assessment", Model.TypeName) %>
        <%= Html.Hidden("categoryType", AssessmentCategory.CareManagement.ToString())%>
        <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
        <%  Html.RenderPartial("Action", Model); %>
    <fieldset class="oasis">
        <legend>Types and Sources of Assistance</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M2100">
                <label>
                    <a class="green" onclick="Oasis.Help('M2100')" status="More Information about M2100">(M2100)</a>
                    Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed.
                    <em>(Check only one box in each row)</em>
                </label>
            </div>
            <div class="row no-input">
                <label>a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100ADLAssistance", "", new { @id = Model.TypeName + "_M2100ADLAssistanceHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ADLAssistance", "00", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("00"), "0", "No assistance needed in this area") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ADLAssistance", "01", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("01"), "1", "Caregiver(s) currently provide assistance") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ADLAssistance", "02", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ADLAssistance", "03", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("03"), "3", "Caregiver(s) not likely to provide assistance") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ADLAssistance", "04", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance") %>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ADLAssistance", "05", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available") %>
                </ul>
            </div>
            <div class="row no-input">
                <label>b. IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100IADLAssistance", "", new { @id = Model.TypeName + "_M2100IADLAssistanceHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100IADLAssistance", "00", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("00"), "0", "No assistance needed in this area")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100IADLAssistance", "01", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("01"), "1", "Caregiver(s) currently provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100IADLAssistance", "02", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100IADLAssistance", "03", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("03"), "3", "Caregiver(s) not likely to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100IADLAssistance", "04", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100IADLAssistance", "05", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>c. Medication administration (e.g., oral, inhaled or injectable)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100MedicationAdministration", "", new { @id = Model.TypeName + "_M2100MedicationAdministrationHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicationAdministration", "00", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("00"), "0", "No assistance needed in this area")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicationAdministration", "01", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("01"), "1", "Caregiver(s) currently provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicationAdministration", "02", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicationAdministration", "03", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("03"), "3", "Caregiver(s) not likely to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicationAdministration", "04", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicationAdministration", "05", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>d. Medical procedures/ treatments (e.g., changing wound dressing)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100MedicalProcedures", "", new { @id = Model.TypeName + "_M2100MedicalProceduresHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicalProcedures", "00", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("00"), "0", "No assistance needed in this area")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicalProcedures", "01", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("01"), "1", "Caregiver(s) currently provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicalProcedures", "02", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicalProcedures", "03", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("03"), "3", "Caregiver(s) not likely to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicalProcedures", "04", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100MedicalProcedures", "05", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>e. Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100ManagementOfEquipment", "", new { @id = Model.TypeName + "_M2100ManagementOfEquipmentHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ManagementOfEquipment", "00", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("00"), "0", "No assistance needed in this area")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ManagementOfEquipment", "01", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("01"), "1", "Caregiver(s) currently provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ManagementOfEquipment", "02", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ManagementOfEquipment", "03", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("03"), "3", "Caregiver(s) not likely to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ManagementOfEquipment", "04", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100ManagementOfEquipment", "05", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>f. Supervision and safety (e.g., due to cognitive impairment)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100SupervisionAndSafety", "", new { @id = Model.TypeName + "_M2100SupervisionAndSafetyHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100SupervisionAndSafety", "00", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("00"), "0", "No assistance needed in this area")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100SupervisionAndSafety", "01", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("01"), "1", "Caregiver(s) currently provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100SupervisionAndSafety", "02", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100SupervisionAndSafety", "03", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("03"), "3", "Caregiver(s) not likely to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100SupervisionAndSafety", "04", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100SupervisionAndSafety", "05", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available")%>
                </ul>
            </div>
            <div class="row no-input">
                <label>g. Advocacy or facilitation of patient&#8217;s participation in appropriate medical care (includes transportation to or from appointments)</label>
                <%= Html.Hidden(Model.TypeName + "_M2100FacilitationPatientParticipation", "", new { @id = Model.TypeName + "_M2100FacilitationPatientParticipationHidden" })%>
                <ul class="checkgroup two-wide">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100FacilitationPatientParticipation", "00", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("00"), "0", "No assistance needed in this area")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100FacilitationPatientParticipation", "01", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("01"), "1", "Caregiver(s) currently provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100FacilitationPatientParticipation", "02", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("02"), "2", "Caregiver(s) need training/ supportive services to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100FacilitationPatientParticipation", "03", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("03"), "3", "Caregiver(s) not likely to provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100FacilitationPatientParticipation", "04", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("04"), "4", "Unclear if Caregiver(s) will provide assistance")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2100FacilitationPatientParticipation", "05", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("05"), "5", "Assistance needed, but no Caregiver(s) available")%>
                </ul>
            </div>
            <div class="row">
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2100')" title="More Information about M2100">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>ADL or IADL Assistance</legend>
        <div class="wide-column">
            <div class="row no-input" id="<%= Model.TypeName %>_M2110">
                <label>
                    <a status="More Information about M2110" class="green" onclick="Oasis.Help('M2110')">(M2110)</a>
                    How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "", new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance" })%>
                <ul class="checkgroup">
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "01", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("01"), "1", "At least daily")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "02", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("02"), "2", "Three or more times per week")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "03", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("03"), "3", "One to two times per week")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "04", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("04"), "4", "Received, but less often than weekly")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "05", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("05"), "5", "No assistance received")%>
                    <%= Html.CheckgroupRadioOption(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "UK", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("UK"), "UK", "Unknown")%>
                </ul>
                <div class="fr oasis">
                    <div class="button oasis-help"><a onclick="Oasis.Help('M2110')" status="More Information about M2110">?</a></div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  Html.RenderPartial("Action", Model); %>
    <%  } %>
</div>
<%  } %>