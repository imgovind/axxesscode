<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] integumentaryInterventions = data.AnswerArray("485IntegumentaryInterventions"); %>
<%  string[] integumentaryGoals = data.AnswerArray("485IntegumentaryGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485IntegumentaryInterventions", "", new { @id = Model.TypeName + "_485IntegumentaryInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions1' name='{0}_485IntegumentaryInterventions' value='1' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("1").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions1">SN to instruct</label>
                            <%  var instructTurningRepositionPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructTurningRepositionPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructTurningRepositionPerson", instructTurningRepositionPerson) %>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions1">on turning/repositioning every 2 hours.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions2' name='{0}_485IntegumentaryInterventions' value='2' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("2").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions2">SN to instruct the</label>
                            <%  var instructReduceFrictionPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructReduceFrictionPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructReduceFrictionPerson", instructReduceFrictionPerson) %>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions2">on methods to reduce friction and shear.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions3' name='{0}_485IntegumentaryInterventions' value='3' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions3">SN to instruct the</label>
                            <%  var instructPadBonyProminencesPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructPadBonyProminencesPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructPadBonyProminencesPerson", instructPadBonyProminencesPerson) %>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions3">to pad all bony prominences.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions4' name='{0}_485IntegumentaryInterventions' value='4' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions4">SN to perform/instruct on wound care as follows</label>
                            <%= Html.TextArea(Model.TypeName + "_485InstructWoundCarePersonFrequency", data.AnswerOrEmptyString("485InstructWoundCarePersonFrequency"), new { @id = Model.TypeName + "_485InstructWoundCarePersonFrequency" }) %>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryInterventions", "5", integumentaryInterventions.Contains("5"), "SN to assess skin for breakdown every visit.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions6' name='{0}_485IntegumentaryInterventions' value='6' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("6").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions6">SN to instruct the</label>
                            <%  var instructSignsWoundInfectionPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructSignsWoundInfectionPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructSignsWoundInfectionPerson", instructSignsWoundInfectionPerson) %>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions6">on signs/symptoms of wound infection to report to physician, to include increased temp &#62; 100.5, chills, increase in drainage, foul odor, redness, pain and any other significant changes.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryInterventions", "7", integumentaryInterventions.Contains("7"), "May discontinue wound care when wound(s) have healed.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryInterventions", "8", integumentaryInterventions.Contains("8"), "SN to assess wound for S&#38;S of infection, healing status, wound deterioration, and complications.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryInterventions", "9", integumentaryInterventions.Contains("9"), "SN to perform/instruct on incision/suture site care &#38; dressing.")%>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions10' name='{0}_485IntegumentaryInterventions' value='10' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("10").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions10">Therapist to instruct</label>
                            <%  var integumentaryTurningRepositionTherapyPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485IntegumentaryTurningRepositionTherapyPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485IntegumentaryTurningRepositionTherapyPerson", integumentaryTurningRepositionTherapyPerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions10">on turning/repositioning every 2 hours.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions11' name='{0}_485IntegumentaryInterventions' value='11' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("11").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions11">Therapist to instruct the</label>
                            <%  var integumentaryFloatHeelsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485IntegumentaryFloatHeelsPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485IntegumentaryFloatHeelsPerson", integumentaryFloatHeelsPerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions11">to float heels.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions12' name='{0}_485IntegumentaryInterventions' value='12' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("12").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions12">Therapist to instruct the</label>
                            <%  var integumentaryReduceFrictionSheerPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485IntegumentaryReduceFrictionSheerPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485IntegumentaryReduceFrictionSheerPerson", integumentaryReduceFrictionSheerPerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions12">on methods to reduce friction and shear.</label>
                        </span>
                    </div>
                </li>
    <%  if (Model.Discipline == "PT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions13' name='{0}_485IntegumentaryInterventions' value='13' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("13").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions13">Therapist to instruct the</label>
                            <%  var integumentaryMoistureBarrierPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485IntegumentaryMoistureBarrierPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485IntegumentaryMoistureBarrierPerson", integumentaryMoistureBarrierPerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions13">on proper use of moisture barrier</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructMoistureBarrier", data.AnswerOrEmptyString("485InstructMoistureBarrier"), new { @id = Model.TypeName + "_485InstructMoistureBarrier", @class = "shorter" }) %>.
                        </span>
                    </div>
                </li>
    <%  } %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryInterventions14' name='{0}_485IntegumentaryInterventions' value='14' type='checkbox' {1} />", Model.TypeName, integumentaryInterventions.Contains("14").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions14">Therapist to instruct the</label>
                            <%  var integumentaryPadBonyPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485IntegumentaryPadBonyPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485IntegumentaryPadBonyPerson", integumentaryPadBonyPerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryInterventions14">to pad all bony prominences.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IntegumentaryInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485IntegumentaryOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485IntegumentaryInterventionComments", data.AnswerOrEmptyString("485IntegumentaryInterventionComments"), new { @id = Model.TypeName + "_485IntegumentaryInterventionComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485IntegumentaryGoals", "", new { @id = Model.TypeName + "_485IntegumentaryGoalsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryGoals", "1", integumentaryGoals.Contains("1"), "Wound(s) will heal without complication by the end of the episode")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryGoals", "2", integumentaryGoals.Contains("2"), "Wound(s) will be free from signs and symptoms of infection during 60 day episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485IntegumentaryGoals3' name='{0}_485IntegumentaryGoals' value='3' type='checkbox' {1} />", Model.TypeName, integumentaryGoals.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryGoals3">Wound(s) will decrease in size by</label>
                            <%= Html.TextBox(Model.TypeName + "_485WoundSizeDecreasePercent", data.AnswerOrEmptyString("485WoundSizeDecreasePercent"), new { @id = Model.TypeName + "_485WoundSizeDecreasePercent", @class = "shorter", @maxlength = "4" }) %>
                            <label for="<%= Model.TypeName %>_485IntegumentaryGoals3">% by the end of the episode.</label>
                        </span>
                    </div>
                </li>
<%  } %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryGoals", "4", integumentaryGoals.Contains("4"), "Patient skin integrity will remain intact during this episode.")%>
<%  if (Model.Discipline == "Nursing") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485IntegumentaryGoals5' name='{0}_485IntegumentaryGoals' value='5' type='checkbox' {1} />", Model.TypeName, integumentaryGoals.Contains("5").ToChecked()) %>
                        <span>
                            <%  var demonstrateSterileDressingTechniquePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485DemonstrateSterileDressingTechniquePerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485DemonstrateSterileDressingTechniquePerson", demonstrateSterileDressingTechniquePerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryGoals5">will demonstrate understanding of changing</label>
                            <%= Html.TextBox(Model.TypeName + "_485DemonstrateSterileDressingTechniqueType", data.AnswerOrEmptyString("485DemonstrateSterileDressingTechniqueType"), new { @id = Model.TypeName + "_485DemonstrateSterileDressingTechniqueType", @class = "shorter", @maxlength = "10" })%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryGoals5">dressing using sterile technique.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IntegumentaryGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485IntegumentaryGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485IntegumentaryGoalComments", data.AnswerOrEmptyString("485IntegumentaryGoalComments"), new { @id = Model.TypeName + "_485IntegumentaryGoalComments", @title = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>