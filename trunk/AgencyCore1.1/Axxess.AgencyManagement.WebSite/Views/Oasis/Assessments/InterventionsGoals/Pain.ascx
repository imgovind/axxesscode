<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] painInterventions = data.AnswerArray("485PainInterventions"); %>
<%  string[] painGoals = data.AnswerArray("485PainGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485PainInterventions", "", new { @id = Model.TypeName + "_485PainInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "1", painInterventions.Contains("1"), "SN to assess pain level and effectiveness of pain medications and current pain management therapy every visit.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "2", painInterventions.Contains("2"), "SN to instruct patient to take pain medication before pain becomes severe to achieve better pain control.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "3", painInterventions.Contains("3"), "SN to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions4' name='{0}_485PainInterventions' value='4' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485PainInterventions4">SN to report to physician if patient experiences pain level not acceptable to patient, pain level greater than </label>
                            <%= Html.TextBox(Model.TypeName + "_485PainTooGreatLevel", data.AnswerOrEmptyString("485PainTooGreatLevel"), new { @id = Model.TypeName + "_485PainTooGreatLevel", @class = "shorter", @maxlength = "10" }) %>
                            <label for="<%= Model.TypeName %>_485PainInterventions5">, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.</label>
                        </span>
                    </div>
                </li>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "5", painInterventions.Contains("5"), "Therapist to assess pain level and effectiveness of pain medications and current pain management therapy every visit.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "6", painInterventions.Contains("6"), "Therapist to instruct patient to take pain medication before pain becomes severe to achieve better pain control.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "7", painInterventions.Contains("7"), "Therapist to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainInterventions", "8", painInterventions.Contains("8"), "Therapist to assess patient&#8217;s willingness to take pain medications and/or barriers to compliance, e.g., patient is unable to tolerate side effects such as drowsiness, dizziness, constipation.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions9' name='{0}_485PainInterventions' value='9' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("9").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485PainInterventions9">Therapist to report to physician if patient experiences pain level not acceptable to patient, pain level greater than</label>
                            <%= Html.TextBox(Model.TypeName + "_485PainInterventionsPainLevelGreaterThan", data.AnswerOrEmptyString("485PainInterventionsPainLevelGreaterThan"), new { @id = Model.TypeName + "_485PainInterventionsPainLevelGreaterThan", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485PainInterventions9">, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485PainInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485PainOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485PainInterventionComments", data.AnswerOrEmptyString("485PainInterventionComments"), new { @id = Model.TypeName + "_485PainInterventionComments", @status = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485PainGoals", "", new { @id = Model.TypeName + "_485PainGoalsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainGoals", "1", painGoals.Contains("1"), "Patient will verbalize understanding of proper use of pain medication by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainGoals", "2", painGoals.Contains("2"), "PT/CG will verbalize knowledge of pain medication regimen and pain relief measures by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485PainGoals", "3", painGoals.Contains("3"), "Patient will have absence or control of pain as evidenced by optimal mobility and activity necessary for functioning and performing ADLs by the end of the episode.")%>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals4' name='{0}_485PainGoals' value='4' type='checkbox' {1} />", Model.TypeName, painGoals.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485PainGoals4">Patient will verbalize understanding of proper use of pain medication by</label>
                            <%= Html.TextBox(Model.TypeName + "_485PainGoalsMedication", data.AnswerOrEmptyString("485PainGoalsMedication"), new { @id = Model.TypeName + "_485PainGoalsMedication", @class = "shorter" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals5' name='{0}_485PainGoals' value='5' type='checkbox' {1} />", Model.TypeName, painGoals.Contains("5").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485PainGoals5">Patient will achieve pain level less than</label>
                            <%= Html.TextBox(Model.TypeName + "_485PainGoalsPainLevelLessThan", data.AnswerOrEmptyString("485PainGoalsPainLevelLessThan"), new { @id = Model.TypeName + "_485PainGoalsPainLevelLessThan", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485PainGoals5">within</label>
                            <%= Html.TextBox(Model.TypeName + "_485PainGoalsWithinWeeks", data.AnswerOrEmptyString("485PainGoalsWithinWeeks"), new { @id = Model.TypeName + "_485PainGoalsWithinWeeks", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485PainGoals5">weeks.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485PainGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485PainGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485PainGoalComments", data.AnswerOrEmptyString("485PainGoalComments"), new { @id = Model.TypeName + "_485PainGoalComments", @status = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>