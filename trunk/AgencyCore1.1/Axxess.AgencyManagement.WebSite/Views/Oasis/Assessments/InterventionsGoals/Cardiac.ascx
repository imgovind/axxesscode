<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] cardiacStatusInterventions = data.AnswerArray("485CardiacStatusInterventions"); %>
<%  string[] cardiacStatusGoals = data.AnswerArray("485CardiacStatusGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485CardiacStatusInterventions", "", new { @id = Model.TypeName + "_485CardiacStatusInterventionsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusInterventions", "1", cardiacStatusInterventions.Contains("1"), "SN to instruct on daily/weekly weights and recordings.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusInterventions", "2", cardiacStatusInterventions.Contains("2"), "SN to perform weekly weights.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485CardiacStatusInterventions3' name='{0}_485CardiacStatusInterventions' value='3' type='checkbox' {1} />", Model.TypeName, cardiacStatusInterventions.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions3">SN to instruct on application of</label>
                            <%  var applicationWrap = new SelectList(new[] {
                                    new SelectListItem { Text = "Ted Hose", Value = "Ted Hose" },
                                    new SelectListItem { Text = "Ace Wraps", Value = "Ace Wraps" }
                                }, "Value", "Text", data.AnswerOrDefault("485ApplicationWrap", "Ted Hose"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485ApplicationWrap", applicationWrap, new { @class = "short" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485CardiacStatusInterventions4' name='{0}_485CardiacStatusInterventions' value='4' type='checkbox' {1} />", Model.TypeName, cardiacStatusInterventions.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions4">SN to instruct patient on daily weight self-monitoring program, and to report weight</label>
                            <%  var WeightSelfMonitorGain = new SelectList(new[] {
                                    new SelectListItem { Text = "Gain", Value = "1" },
                                    new SelectListItem { Text = "Loss", Value = "0" }
                                }, "Value", "Text", data.AnswerOrDefault("485WeightSelfMonitorGain", "1")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485WeightSelfMonitorGain", WeightSelfMonitorGain, new { @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions4">of</label>
                            <%= Html.TextBox(Model.TypeName + "_485WeightSelfMonitorGainDay", data.AnswerOrEmptyString("485WeightSelfMonitorGainDay"), new { @id = Model.TypeName + "_485WeightSelfMonitorGainDay", @class = "shorter", @maxlength = "3" })%>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions4">lbs/ day,</label>
                            <%= Html.TextBox(Model.TypeName + "_485WeightSelfMonitorGainWeek", data.AnswerOrEmptyString("485WeightSelfMonitorGainWeek"), new { @id = Model.TypeName + "_485WeightSelfMonitorGainWeek", @class = "shorter", @maxlength = "3" })%>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions4">lbs/week.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusInterventions", "5", cardiacStatusInterventions.Contains("5"), "SN to assess patient&#8217;s weight log every visit.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485CardiacStatusInterventions6' name='{0}_485CardiacStatusInterventions' value='6' type='checkbox' {1} />", Model.TypeName, cardiacStatusInterventions.Contains("6").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions6">SN to instruct the</label>
                            <%  var instructRecognizeCardiacDysfunctionPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructRecognizeCardiacDysfunctionPerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructRecognizeCardiacDysfunctionPerson", instructRecognizeCardiacDysfunctionPerson) %>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions6">on measures to recognize cardiac dysfunction and relieve complications.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusInterventions", "7", cardiacStatusInterventions.Contains("7"), "SN to instruct patient on measures to detect and alleviate edema.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusInterventions", "8", cardiacStatusInterventions.Contains("8"), "SN to instruct patient when (s)he starts feeling chest pain, tightness, or squeezing in the chest to take nitroglycerin. Patient may take nitroglycerin one time every 5 minutes. If no relief after 3 doses, call 911.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusInterventions", "9", cardiacStatusInterventions.Contains("9"), "SN to instruct the patient the following symptoms could be signs of a heart attack: chest discomfort, discomfort in one or both arms, back, neck, jaw, stomach, shortness of breath, cold sweat, nausea, or dizziness. Instruct patient on signs and symptoms that necessitate calling 911.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485CardiacStatusInterventions10' name='{0}_485CardiacStatusInterventions' value='10' type='checkbox' {1} />", Model.TypeName, cardiacStatusInterventions.Contains("10").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions10">No blood pressure or venipuncture in</label>
                            <%= Html.TextBox(Model.TypeName + "_485NoBloodPressureArm", data.AnswerOrEmptyString("485NoBloodPressureArm"), new { @id = Model.TypeName + "_485NoBloodPressureArm", @class = "shorter", @maxlength = "10" }) %>
                            <label for="<%= Model.TypeName %>_485CardiacStatusInterventions10">arm.</label>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485CardiacInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485CardiacOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485CardiacInterventionComments", data.AnswerOrEmptyString("485CardiacInterventionComments"), new { @id = Model.TypeName + "_485CardiacInterventionComments", @status = "(485 Locator 21) Orders" }) %>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485CardiacStatusGoals", "", new { @id = Model.TypeName + "_485CardiacStatusGoalsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485CardiacStatusGoals1' name='{0}_485CardiacStatusGoals' value='1' type='checkbox' {1} />", Model.TypeName, cardiacStatusGoals.Contains("1").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals1">Patient weight will be maintained between</label>
                            <%= Html.TextBox(Model.TypeName + "_485WeightMaintainedMin", data.AnswerOrEmptyString("485WeightMaintainedMin"), new { @id = Model.TypeName + "_485WeightMaintainedMin", @class = "shorter", @maxlength = "10" })%>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals1">lbs and</label>
                            <%= Html.TextBox(Model.TypeName + "_485WeightMaintainedMax", data.AnswerOrEmptyString("485WeightMaintainedMax"), new { @id = Model.TypeName + "_485WeightMaintainedMax", @class = "shorter", @maxlength = "10" })%>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals1">lbs during the episode.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485CardiacStatusGoals", "2", cardiacStatusGoals.Contains("2"), "Patient will remain free from chest pain, or chest pain will be relieved with nitroglycerin, during the episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485CardiacStatusGoals3' name='{0}_485CardiacStatusGoals' value='3' type='checkbox' {1} />", Model.TypeName, cardiacStatusGoals.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals3">The</label>
                            <%  var verbalizeCardiacSymptomsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485VerbalizeCardiacSymptomsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485VerbalizeCardiacSymptomsPerson", verbalizeCardiacSymptomsPerson) %>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals3">will verbalize understanding of symptoms of cardiac complications and when to call 911 by</label>
                            <%= Html.TextBox(Model.TypeName + "_485VerbalizeCardiacSymptomsDate", data.AnswerOrEmptyString("485VerbalizeCardiacSymptomsDate"), new { @id = Model.TypeName + "_485VerbalizeCardiacSymptomsDate", @class = "shorter", @maxlength = "10" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485CardiacStatusGoals4' name='{0}_485CardiacStatusGoals' value='4' type='checkbox' {1} />", Model.TypeName, cardiacStatusGoals.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals4">The</label>
                            <%  var verbalizeEdemaRelieverPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485VerbalizeEdemaRelieverPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485VerbalizeEdemaRelieverPerson", verbalizeEdemaRelieverPerson) %>
                            <label for="<%= Model.TypeName %>_485CardiacStatusGoals4">will verbalize and demonstrate edema-relieving measures by the episode.</label>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485CardiacGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485CardiacGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485CardiacGoalComments", data.AnswerOrEmptyString("485CardiacGoalComments"), new { @id = Model.TypeName + "_485CardiacGoalComments", @status = "(485 Locator 22) Goals" }) %>
            </div>
        </div>
    </div>
</fieldset>