<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] sensoryStatusIntervention = data.AnswerArray("485SensoryStatusIntervention"); %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485SensoryStatusIntervention", "", new { @id = Model.TypeName + "_485SensoryStatusInterventionHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485SensoryStatusIntervention", "1", sensoryStatusIntervention.Contains("1"), "ST to evaluate.")%>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485SensoryStatusInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485SensoryStatusOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485SensoryStatusInterventionComments", data.AnswerOrEmptyString("485SensoryStatusInterventionComments"), new { @id = Model.TypeName + "_485SensoryStatusInterventionComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="loc485">
    <legend>Goals</legend>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.TypeName %>_485SensoryStatusGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485SensoryStatusGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485SensoryStatusGoalComments", data.AnswerOrEmptyString("485SensoryStatusGoalComments"), new { @id = Model.TypeName + "_485SensoryStatusGoalComments", @title = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>