<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] eliminationInterventions = data.AnswerArray("485EliminationInterventions"); %>
<%  string[] eliminationGoals = data.AnswerArray("485EliminationGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485EliminationInterventions", "", new { @id = Model.TypeName + "_485EliminationInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "1", eliminationInterventions.Contains("1"), "SN to instruct on establishing bowel regimen.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "9", eliminationInterventions.Contains("9"), "SN to instruct on establishing bladder regimen.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "2", eliminationInterventions.Contains("2"), "SN to instruct on application of appliance, care and storage of equipment and disposal of used supplies.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "3", eliminationInterventions.Contains("3"), "SN to instruct on care of stoma, surrounding skin and use of skin barrier.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "4", eliminationInterventions.Contains("4"), "SN to instruct on foley care, skin and perineal care, proper handling and storage of supplies.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "5", eliminationInterventions.Contains("5"), "SN to instruct on adequate hydration, proper handling and maintenance of drainage bag.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions6' name='{0}_485EliminationInterventions' value='6' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("6").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485EliminationInterventions6">SN to change catheter every month and PRN using sterile technique</label>
                            <%= Html.TextBox(Model.TypeName + "_485EliminationFoleyCatheterType", data.AnswerOrEmptyString("485EliminationFoleyCatheterType"), new { @id = Model.TypeName + "_485EliminationFoleyCatheterType", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485EliminationInterventions6">Fr. catheter.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationInterventions", "7", eliminationInterventions.Contains("7"), "SN to instruct on intermittent catheterizations.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions8' name='{0}_485EliminationInterventions' value='8' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("8").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485EliminationInterventions8">SN to perform intermittent catheterization every</label>
                            <%= Html.TextBox(Model.TypeName + "_485EliminationCatheterizationNumber", data.AnswerOrEmptyString("485EliminationCatheterizationNumber"), new { @id = Model.TypeName + "_485EliminationCatheterizationNumber", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485EliminationInterventions8">&#38; PRN using sterile technique.</label>
                        </span>
                    </div>
                </li>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EliminationInterventions10' name='{0}_485EliminationInterventions' value='8' type='checkbox' {1} />", Model.TypeName, eliminationInterventions.Contains("8").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485EliminationInterventions10">No blood pressure in</label>
                            <%= Html.TextBox(Model.TypeName + "_485EliminationBloodPressureArm", data.AnswerOrEmptyString("485EliminationBloodPressureArm"), new { @id = Model.TypeName + "_485EliminationBloodPressureArm", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485EliminationInterventions10">arm.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485EliminationOrderTemplates">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485CardiacOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485EliminationInterventionComments", data.AnswerOrEmptyString("485EliminationInterventionComments"), new { @id = Model.TypeName + "_485EliminationInterventionComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485EliminationGoals", "", new { @id = Model.TypeName + "_485EliminationGoalsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationGoals", "1", eliminationGoals.Contains("1"), "Foley will remain patent during this episode and patient will be free of signs and symptoms of UTI.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationGoals", "2", eliminationGoals.Contains("2"), "Suprapubic tube will remain patent during this episode and patient will be free of signs and symptoms of UTI.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationGoals", "3", eliminationGoals.Contains("3"), "Patient will be without signs/symptoms of UTI (pain, foul odor, cloudy or blood-tinged urine and fever) during this episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='' id='{0}_485EliminationGoals4' name='{0}_485EliminationGoals' value='4' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485EliminationGoals4">The</label>
                            <%  var eliminationOstomyManagementIndependent = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485EliminationOstomyManagementIndependent", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485EliminationOstomyManagementIndependent", eliminationOstomyManagementIndependent)%>
                            <label for="<%= Model.TypeName %>_485EliminationGoals4">will be independent in ostomy management by</label>
                             <%= Html.TextBox(Model.TypeName + "_485EliminationOstomyManagementIndependentDate", data.AnswerOrEmptyString("485EliminationOstomyManagementIndependentDate"), new { @id = Model.TypeName + "_485EliminationOstomyManagementIndependentDate", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationGoals", "5", eliminationGoals.Contains("5"), "Patient will be free from signs and symptoms of constipation during the episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='' id='{0}_485EliminationGoals6' name='{0}_485EliminationGoals' value='6' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("6").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485EliminationGoals6">The</label>
                            <%  var eliminationAcidFoodVerbalizedGivenTo = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485EliminationAcidFoodVerbalizedGivenTo", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485EliminationAcidFoodVerbalizedGivenTo", eliminationAcidFoodVerbalizedGivenTo)%>
                            <label for="<%= Model.TypeName %>_485EliminationGoals6">will be independent in ostomy management by</label>
                             <%= Html.TextBox(Model.TypeName + "_485EliminationAcidFoodVerbalizedGivenToDate", data.AnswerOrEmptyString("485EliminationAcidFoodVerbalizedGivenToDate"), new { @id = Model.TypeName + "_485EliminationAcidFoodVerbalizedGivenToDate", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='' id='{0}_485EliminationGoals7' name='{0}_485EliminationGoals' value='7' type='checkbox' {1} />", Model.TypeName, eliminationGoals.Contains("7").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485EliminationGoals7">Patient will verbalize understanding not to eat 4 hours before bedtime to reduce acid reflux/indigestion by</label>
                             <%= Html.TextBox(Model.TypeName + "_485EliminationEatingTimeVerbalizedGivenDate", data.AnswerOrEmptyString("485EliminationEatingTimeVerbalizedGivenDate"), new { @id = Model.TypeName + "_485EliminationEatingTimeVerbalizedGivenDate", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485EliminationGoals", "8", eliminationGoals.Contains("8"), "Patient will not develop any signs and symptoms of dehydration during the episode.")%>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485EliminationGoalsComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485EliminationGoalsTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485EliminationGoalsComments", data.AnswerOrEmptyString("485EliminationGoalsComments"), new { @id = Model.TypeName + "_485EliminationGoalsComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>