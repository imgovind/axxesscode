<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] respiratoryInterventions = data.AnswerArray("485RespiratoryInterventions"); %>
<%  string[] respiratoryGoals = data.AnswerArray("485RespiratoryGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485RespiratoryInterventions", "", new { @id = Model.TypeName + "_485RespiratoryInterventionsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions1' name='{0}_485RespiratoryInterventions' value='1' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("1") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions1">SN to instruct the</label>
                            <%  var instructNebulizerUsePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructNebulizerUsePerson", "Patient/Caregiver")); %>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructNebulizerUsePerson", instructNebulizerUsePerson) %>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions1">on proper use of nebulizer/inhaler, and assess return demonstration.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions2' name='{0}_485RespiratoryInterventions' value='2' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("2") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions2">SN to assess O<sub>2</sub> saturation on room air (freq)</label>
                            <%= Html.TextBox(Model.TypeName + "_485AssessOxySaturationFrequency", data.AnswerOrEmptyString("485AssessOxySaturationFrequency"), new { @id = Model.TypeName + "_485AssessOxySaturationFrequency", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions3' name='{0}_485RespiratoryInterventions' value='3' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("3") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions3">SN to assess O<sub>2</sub> saturation on O<sub>2</sub> @</label>
                            <%= Html.TextBox(Model.TypeName + "_485AssessOxySatOnOxyAt", data.AnswerOrEmptyString("485AssessOxySatOnOxyAt"), new { @id = Model.TypeName + "_485AssessOxySatOnOxyAt", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions3">LPM/</label>
                            <%= Html.TextBox(Model.TypeName + "_485AssessOxySatLPM", data.AnswerOrEmptyString("485AssessOxySatLPM"), new { @id = Model.TypeName + "_485AssessOxySatLPM", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions3">(freq)</label>
                            <%= Html.TextBox(Model.TypeName + "_485AssessOxySatOnOxyFrequency", data.AnswerOrEmptyString("485AssessOxySatOnOxyFrequency"), new { @id = Model.TypeName + "_485AssessOxySatOnOxyFrequency", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions4' name='{0}_485RespiratoryInterventions' value='4' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("4") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions4">SN to instruct the</label>
                            <%  var instructSobFactorsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructSobFactorsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructSobFactorsPerson", instructSobFactorsPerson)%>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions4">on factors that contribute to SOB.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions5' name='{0}_485RespiratoryInterventions' value='5' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("5") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions5">SN to instruct the</label>
                            <%  var instructAvoidSmokingPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructAvoidSmokingPerson","Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructAvoidSmokingPerson", instructAvoidSmokingPerson)%>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions5">to avoid smoking or allowing people to smoke in patient&#8217;s home. Instruct patient to avoid irritants/allergens known to increase SOB.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryInterventions", "6", respiratoryInterventions.Contains("6"), "SN to instruct patient on energy conserving measures including frequent rest periods, small frequent meals, avoiding large meals/overeating, controlling stress.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryInterventions", "7", respiratoryInterventions.Contains("7"), "SN to instruct caregiver on proper suctioning technique.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions8' name='{0}_485RespiratoryInterventions' value='8' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("8") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions8">SN to instruct the </label>
                            <%  var recognizePulmonaryDysfunctionPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485RecognizePulmonaryDysfunctionPerson","Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485RecognizePulmonaryDysfunctionPerson", recognizePulmonaryDysfunctionPerson)%>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions8">on methods to recognize pulmonary dysfunction and relieve complications.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions9' name='{0}_485RespiratoryInterventions' value='9' type='checkbox' {1} />", Model.TypeName, respiratoryInterventions != null && respiratoryInterventions.Contains("9") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions9">Report to physician O<sub>2</sub> saturation less than</label>
                            <%= Html.TextBox(Model.TypeName + "_485OxySaturationLessThanPercent", data.AnswerOrEmptyString("485OxySaturationLessThanPercent"), new { @id = Model.TypeName + "_485OxySaturationLessThanPercent", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485RespiratoryInterventions9">%.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryInterventions", "10", respiratoryInterventions.Contains("10"), "SN to assess/instruct on signs &#38; symptoms of pulmonary complications.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryInterventions", "11", respiratoryInterventions.Contains("11"), "SN to instruct on all aspects of trach care and equipment. SN to instruct PT/CG on emergency procedures for complications and dislodgment of tube.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryInterventions", "12", respiratoryInterventions.Contains("12"), "SN to perform trach care using sterile technique (i.e. suctioning, dressing change).")%>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RespiratoryInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485RespiratoryOrderTemplates")%>
                <%= Html.TextArea(Model.TypeName + "_485RespiratoryInterventionComments", data.ContainsKey("485RespiratoryInterventionComments") ? data["485RespiratoryInterventionComments"].Answer : "", new { @id = Model.TypeName + "_485RespiratoryInterventionComments", @status = "(485 Locator 21) Orders" }) %>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485RespiratoryGoals", "", new { @id = Model.TypeName + "_485RespiratoryGoalsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryGoals", "1", respiratoryGoals.Contains("1"), "Respiratory status will improve with reduced shortness of breath and improved lung sounds by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryGoals", "2", respiratoryGoals.Contains("2"), "PT/CG will verbalize and demonstrate correct use and care of oxygen and equipment by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RespiratoryGoals", "3", respiratoryGoals.Contains("3"), "Patient will be free from signs and symptoms of respiratory distress during the episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals4' name='{0}_485RespiratoryGoals' value='4' type='checkbox' {1} />", Model.TypeName, respiratoryGoals != null && respiratoryGoals.Contains("4") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryGoals4">Patient and caregiver will verbalize an understanding of factors that contribute to shortness of breath by</label>
                            <%= Html.TextBox(Model.TypeName + "_485VerbalizeFactorsSobDate", data.AnswerOrEmptyString("485VerbalizeFactorsSobDate"), new { @id = Model.TypeName + "_485VerbalizeFactorsSobDate", @class = "shorter", @maxlength = "10" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals5' name='{0}_485RespiratoryGoals' value='5' type='checkbox' {1} />", Model.TypeName, respiratoryGoals != null && respiratoryGoals.Contains("5") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryGoals5">Patient will verbalize an understanding of energy conserving measures by</label>
                            <%= Html.TextBox(Model.TypeName + "_485VerbalizeEnergyConserveDate", data.AnswerOrEmptyString("485VerbalizeEnergyConserveDate"), new { @id = Model.TypeName + "_485VerbalizeEnergyConserveDate", @class = "shorter", @maxlength = "10" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals6' name='{0}_485RespiratoryGoals' value='6' type='checkbox' {1} />", Model.TypeName, respiratoryGoals != null && respiratoryGoals.Contains("6") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryGoals6">The</label>
                            <%  var verbalizeSafeOxyManagementPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485VerbalizeSafeOxyManagementPerson","Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485VerbalizeSafeOxyManagementPerson", verbalizeSafeOxyManagementPerson)%>
                            <label for="<%= Model.TypeName %>_485RespiratoryGoals6">will verbalize and demonstrate safe management of oxygen by</label>
                            <%= Html.TextBox(Model.TypeName + "_485VerbalizeSafeOxyManagementPersonDate", data.AnswerOrEmptyString("485VerbalizeSafeOxyManagementPersonDate"), new { @id = Model.TypeName + "_485VerbalizeSafeOxyManagementPersonDate", @class = "shorter", @maxlength = "10" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals7' name='{0}_485RespiratoryGoals' value='7' type='checkbox' {1} />", Model.TypeName, respiratoryGoals != null && respiratoryGoals.Contains("7") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485RespiratoryGoals7">Patient will return demonstrate proper use of nebulizer treatment by </label>
                            <%= Html.TextBox(Model.TypeName + "_485DemonstrateNebulizerUseDate", data.AnswerOrEmptyString("485DemonstrateNebulizerUseDate"), new { @id = Model.TypeName + "_485DemonstrateNebulizerUseDate", @class = "shorter", @maxlength = "10" })%>.
                        </span>
                    </div>
                </li>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RespiratoryGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485RespiratoryGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485RespiratoryGoalComments", data.ContainsKey("485RespiratoryGoalComments") ? data["485RespiratoryGoalComments"].Answer : "", new { @id = Model.TypeName + "_485RespiratoryGoalComments", @status = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>