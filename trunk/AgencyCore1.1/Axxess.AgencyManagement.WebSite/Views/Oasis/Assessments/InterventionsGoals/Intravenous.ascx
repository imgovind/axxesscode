<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  if (Model.Discipline == "Nursing") { %>
    <%  var data = Model.ToDictionary(); %>
    <%  string[] integumentaryIVInterventions = data.AnswerArray("485IntegumentaryIVInterventions"); %>
    <%  string[] integumentaryIVGoals = data.AnswerArray("485IntegumentaryIVGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485IntegumentaryIVInterventions", "", new { @id = Model.TypeName + "_485IntegumentaryIVInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions1' name='{0}_485IntegumentaryIVInterventions' value='1' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("1").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions1">SN to instruct the</label>
                            <%  var instructInfectionSignsSymptomsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructInfectionSignsSymptomsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructInfectionSignsSymptomsPerson", instructInfectionSignsSymptomsPerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions1">on signs and symptoms of infection and infiltration.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions2' name='{0}_485IntegumentaryIVInterventions' value='2' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("2").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions2">SN to change central line dressing every</label>
                            <%= Html.TextBox(Model.TypeName + "_485ChangeCentralLineEvery", data.AnswerOrEmptyString("485ChangeCentralLineEvery"), new { @id = Model.TypeName + "_485ChangeCentralLineEvery", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions2">using sterile technique.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions3' name='{0}_485IntegumentaryIVInterventions' value='3' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions3">SN to instruct the</label>
                            <%  var instructChangeCentralLinePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructChangeCentralLinePerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructChangeCentralLinePerson", instructChangeCentralLinePerson)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions3">to change central line dressing every</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructChangeCentralLineEvery", data.AnswerOrEmptyString("485InstructChangeCentralLineEvery"), new { @id = Model.TypeName + "_485InstructChangeCentralLineEvery", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions3">using sterile technique.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485IntegumentaryIVInterventions4' name='{0}_485IntegumentaryIVInterventions' value='4' type='checkbox' {1} />", Model.TypeName, integumentaryIVInterventions.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions4">SN to change</label>
                            <%= Html.TextBox(Model.TypeName + "_485ChangePortDressingType", data.AnswerOrEmptyString("485ChangePortDressingType"), new { @id = Model.TypeName + "_485ChangePortDressingType", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventions4">port dressing using sterile technique every</label>
                            <%= Html.TextBox(Model.TypeName + "_485ChangePortDressingEvery", data.AnswerOrEmptyString("485ChangePortDressingEvery"), new { @id = Model.TypeName + "_485ChangePortDressingEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IntegumentaryIVInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485IntegumentaryIVOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485IntegumentaryIVInterventionComments", data.AnswerOrEmptyString("485IntegumentaryIVInterventionComments"), new { @id = Model.TypeName + "_485IntegumentaryIVInterventionComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485IntegumentaryIVGoals", "", new { @id = Model.TypeName + "_485IntegumentaryIVGoalsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485IntegumentaryIVGoals", "1", integumentaryIVGoals.Contains("1"), "IV will remain patent and free from signs and symptoms of infection.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485IntegumentaryIVGoals2' type='checkbox' name='{0}_485IntegumentaryIVGoals' value='2' {1} />", Model.TypeName, integumentaryIVGoals.Contains("2").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals2">The</label>
                            <%  var instructChangeDress = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                                }, "Value", "Text", data.AnswerOrDefault("485InstructChangeDress", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructChangeDress", instructChangeDress)%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals2">will demonstrate understanding of changing</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructChangeDressSterile", data.AnswerOrEmptyString("485InstructChangeDressSterile"), new { @id = Model.TypeName + "_485InstructChangeDressSterile", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485IntegumentaryIVGoals2">dressing using sterile technique.</label>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IntegumentaryIVGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485IntegumentaryIVGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485IntegumentaryIVGoalComments", data.AnswerOrEmptyString("485IntegumentaryIVGoalComments"), new { @id = Model.TypeName + "_485IntegumentaryIVGoalComments", @title = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>
<%  } %>