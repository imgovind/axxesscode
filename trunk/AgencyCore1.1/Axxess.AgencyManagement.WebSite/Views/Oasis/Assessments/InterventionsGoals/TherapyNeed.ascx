<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  if (Model.Discipline == "Nursing") { %>
    <%  var data = Model.ToDictionary(); %>
    <%  string[] therapyInterventions = data.AnswerArray("485TherapyInterventions"); %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485TherapyInterventions", "", new { @id = Model.TypeName + "_485TherapyInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485TherapyInterventions", "1", therapyInterventions.Contains("1"), "Physical therapist to evaluate and submit plan of treatment.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485TherapyInterventions", "2", therapyInterventions.Contains("2"), "Occupational therapist to evaluate and submit plan of treatment.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485TherapyInterventions", "3", therapyInterventions.Contains("3"), "Speech therapist to evaluate and submit plan of treatment.")%>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485TherapyComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485TherapyOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485TherapyComments", data.AnswerOrEmptyString("485TherapyComments"), new { @id = Model.TypeName + "_485TherapyComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<%  } %>