<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  if (Model.Discipline == "Nursing") { %>
    <%  var data = Model.ToDictionary(); %>
    <%  string[] supportiveAssistanceInterventions = data.AnswerArray("485SupportiveAssistanceInterventions"); %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485SupportiveAssistanceInterventions", " ", new { @id = Model.TypeName + "_485SupportiveAssistanceInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485SupportiveAssistanceInterventions", "1", supportiveAssistanceInterventions.Contains("1"), "MSW to assess psychosocial needs, environment and assist with community referrals and resources.")%>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485SupportiveAssistanceComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485SupportiveAssistanceOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485SupportiveAssistanceComments", data.AnswerOrEmptyString("485SupportiveAssistanceComments"), new { @id = Model.TypeName + "_485SupportiveAssistanceComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="loc485">
    <legend>Goals</legend>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.TypeName %>_485SupportiveAssistanceGoalsComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485SupportiveAssistanceGoalsTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485SupportiveAssistanceGoalsComments", data.AnswerOrEmptyString("485SupportiveAssistanceGoalsComments"), new { @id = Model.TypeName + "_485SupportiveAssistanceGoalsComments", @title = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>
<%  } %>