<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] nursingInterventions = data.AnswerArray("485NursingInterventions"); %>
<%  string[] nursingGoals = data.AnswerArray("485NursingGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485NursingInterventions" value="" />
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingInterventions", "1", nursingInterventions.Contains("1"), "Physical therapy to evaluate.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingInterventions", "2", nursingInterventions.Contains("2"), "Occupational therapy to evaluate.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingInterventions", "3", nursingInterventions.Contains("3"), "SN to assess/instruct on pain management, proper body mechanics and safety measures.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingInterventions", "4", nursingInterventions.Contains("4"), "SN to assess for patient adherence to appropriate activity levels.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingInterventions", "5", nursingInterventions.Contains("5"), "SN to assess patient&#8217;s compliance with home exercise program.") %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions6' name='{0}_485NursingInterventions' value='6' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("6").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485NursingInterventions6">SN to instruct the</label>
                            <%  var instructRomExcercisePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.ContainsKey("485InstructRomExcercisePerson") && data["485InstructRomExcercisePerson"].Answer != "" ? data["485InstructRomExcercisePerson"].Answer : "Patient/Caregiver");%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructRomExcercisePerson", instructRomExcercisePerson)%>
                            <label for="<%= Model.TypeName %>_485NursingInterventions6">on proper ROM exercises and body alignment techniques.</label>
                        </span>
                    </div>
                </li>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NursingInterventions7' name='{0}_485NursingInterventions' value='7' type='checkbox' {1} />", Model.TypeName, nursingInterventions.Contains("7").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485NursingInterventions7">HHA (freq)</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructHhaFreq", data.AnswerOrEmptyString("485InstructHhaFreq"), new { @id = Model.TypeName + "_485InstructHhaFreq", @class = "shorter" })%>
                            <label for="<%= Model.TypeName %>_485NursingInterventions7">assistance with ADLs/IADLs.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485ADLComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485ADLOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485ADLComments", data.ContainsKey("485ADLComments") ? data["485ADLComments"].Answer : "", new { @id = Model.TypeName + "_485ADLComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485NursingGoals" id="<%= Model.TypeName %>_485EstablishHomeExercisePT" />
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingGoals", "1", nursingGoals.Contains("1"), "Patient will have increased mobility, self care, endurance, ROM and decreased pain by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingGoals", "2", nursingGoals.Contains("2"), "Patient will maintain optimal joint function, increased mobility and independence in ADL&#8217;s by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingGoals", "3", nursingGoals.Contains("3"), "Patient&#8217;s strength, endurance and mobility will be improved.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NursingGoals4' name='{0}_485NursingGoals' value='4' type='checkbox' {1} />", Model.TypeName, nursingGoals != null && nursingGoals.Contains("4") ? "checked='checked'" : "")%>
                        <span>
                            <label for="<%= Model.TypeName %>_485NursingGoals4">The</label>
                            <%  var demonstrateROMExcercisePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.ContainsKey("485DemonstrateROMExcercisePerson") && data["485DemonstrateROMExcercisePerson"].Answer != "" ? data["485DemonstrateROMExcercisePerson"].Answer : "Patient/Caregiver");%>
                            <%= Html.DropDownList(Model.TypeName + "_485DemonstrateROMExcercisePerson", demonstrateROMExcercisePerson)%>
                            <label for="<%= Model.TypeName %>_485NursingGoals4">will demonstrate proper ROM exercise and body alignment techniques.</label>
                        </span>
                    </div>
                </li>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485NursingGoals", "5", nursingGoals.Contains("5"), "Patient&#8217;s ADL/IADL needs will be met with assistance of HHA.")%>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485ADLGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485ADLGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485ADLGoalComments", data.ContainsKey("485ADLGoalComments") ? data["485ADLGoalComments"].Answer : "", new { @id = Model.TypeName + "_485ADLGoalComments", @title = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>