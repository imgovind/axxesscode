<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] behaviorInterventions = data.AnswerArray("485BehaviorInterventions"); %>
<%  string[] behaviorGoals = data.AnswerArray("485BehaviorGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485BehaviorInterventions", " ", new { @id = Model.TypeName + "_485BehaviorInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorInterventions", "1", behaviorInterventions.Contains("1"), "SN to notify physician this patient was screened for depression using PHQ-2 scale and meets criteria for further evaluation for depression.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorInterventions", "2", behaviorInterventions.Contains("2"), "SN to perform a neurological assessment each visit.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorInterventions", "3", behaviorInterventions.Contains("3"), "SN to assess/instruct on seizure disorder signs &#38; symptoms and appropriate actions during seizure activity.") %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions4' name='{0}_485BehaviorInterventions' value='4' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("4").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions4">SN to instruct the</label>
                            <%  var instructSeizurePrecautionPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructSeizurePrecautionPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructSeizurePrecautionPerson", instructSeizurePrecautionPerson)%>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions4">on seizure precautions.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorInterventions", "5", behaviorInterventions.Contains("5"), "SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.") %>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorInterventions", "7", behaviorInterventions.Contains("7"), "Notify SN or Physician that this patient was screened for depression using the PHQ-2 scale and meets criteria for further evaluation for depression.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorInterventions", "8", behaviorInterventions.Contains("8"), "SN to evaluate patient for signs and symptoms of depression.") %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions9' name='{0}_485BehaviorInterventions' value='9' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("9").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions9">MSW</label>
                            <%  var MSWProviderServicesVisits = new SelectList(new[] {
                                    new SelectListItem { Text = "1-2", Value = "1" },
                                    new SelectListItem { Text = "Specify", Value = "0"}
                                }, "Value", "Text", data.AnswerOrDefault("485MSWProviderServicesVisits", "1"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485MSWProviderServicesVisits", MSWProviderServicesVisits, new { @class = "short" })%>
                            <%= Html.TextBox(Model.TypeName + "_485MSWProviderServicesVisitsAmount", data.AnswerOrEmptyString("485MSWProviderServicesVisitsAmount"), new { @id = Model.TypeName + "_485MSWProviderServicesVisitsAmount", @class = "shorter", @maxlength = "5" })%>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions9">visits, every 60 days for provider services.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions10' name='{0}_485BehaviorInterventions' value='10' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("10").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions10">MSW</label>
                            <%  var MSWLongTermPlanningVisits = new SelectList(new[] {
                                    new SelectListItem { Text = "1-2", Value = "1" },
                                    new SelectListItem { Text = "Specify", Value = "0"}
                                }, "Value", "Text", data.AnswerOrDefault("485MSWLongTermPlanningVisits", "1"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485MSWLongTermPlanningVisits", MSWLongTermPlanningVisits, new { @class = "short" })%>
                            <%= Html.TextBox(Model.TypeName + "_485MSWLongTermPlanningVisitsAmount", data.AnswerOrEmptyString("485MSWLongTermPlanningVisitsAmount"), new { @id = Model.TypeName + "_485MSWLongTermPlanningVisitsAmount", @class = "shorter", @maxlength = "5" })%>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions10">visits, every 60 days for long term planning.</label>
                        </span>
                    </div>
                </li>
<%  } %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485BehaviorInterventions6' name='{0}_485BehaviorInterventions' value='6' type='checkbox' {1} />", Model.TypeName, behaviorInterventions.Contains("6").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions6">MSW</label>
                            <%  var MSWCommunityAssistanceVisits = new SelectList(new[] {
                                    new SelectListItem { Text = "1-2", Value = "1" },
                                    new SelectListItem { Text = "Specify", Value = "0"}
                                }, "Value", "Text", data.AnswerOrDefault("485MSWCommunityAssistanceVisits", "1"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485MSWCommunityAssistanceVisits", MSWCommunityAssistanceVisits, new { @class = "short" })%>
                            <%= Html.TextBox(Model.TypeName + "_485MSWCommunityAssistanceVisitAmount", data.AnswerOrEmptyString("485MSWCommunityAssistanceVisitAmount"), new { @id = Model.TypeName + "_485MSWCommunityAssistanceVisitAmount", @class = "shorter", @maxlength = "5" })%>
                            <label for="<%= Model.TypeName %>_485BehaviorInterventions6">visits, every 60 days for community resource assistance.</label>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485BehaviorComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485BehaviorOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485BehaviorComments", data.AnswerOrEmptyString("485BehaviorComments"), new { @id = Model.TypeName + "_485BehaviorComments", @status = "(485 Locator 21) Orders" }) %>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485BehaviorGoals", " ", new { @id = Model.TypeName + "_485BehaviorGoalsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "1", behaviorGoals.Contains("1"), "Neuro status will be within normal limits and free of S&#38;S of complications or further deterioration.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "2", behaviorGoals.Contains("2"), "Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "3", behaviorGoals.Contains("3"), "Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "4", behaviorGoals.Contains("4"), "Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "5", behaviorGoals.Contains("5"), "Patient will remain free from increased confusion during the episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485BehaviorGoals6' name='{0}_485BehaviorGoals' value='6' type='checkbox' {1} />", Model.TypeName, behaviorGoals.Contains("6").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485BehaviorGoals6">The</label>
                            <%  var verbalizeSeizurePrecautionsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485VerbalizeSeizurePrecautionsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485VerbalizeSeizurePrecautionsPerson", verbalizeSeizurePrecautionsPerson)%>
                            <label for="<%= Model.TypeName %>_485BehaviorGoals6">will verbalize understanding of seizure precautions.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "7", behaviorGoals.Contains("7"), "Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.")%>
<%  } %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485BehaviorGoals", "8", behaviorGoals.Contains("8"), "Patient&#8217;s community resource needs will be met with assistance of social worker.")%>
            </ul>
        </div>        
        <div class="row">
            <label for="<%= Model.TypeName %>_485BehaviorGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485BehaviorGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485BehaviorGoalComments", data.AnswerOrEmptyString("485BehaviorGoalComments"), new { @id = Model.TypeName + "_485BehaviorGoalComments", @status = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>