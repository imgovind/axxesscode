<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] medicationInterventions = data.AnswerArray("485MedicationInterventions"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485MedicationInterventions", "", new { @id = Model.TypeName + "_485MedicationInterventionsHidden" }) %>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485MedicationInterventions", "1", medicationInterventions.Contains("1"), "SN to assess patient filling medication box to determine if patient is preparing correctly.") %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485MedicationInterventions", "2", medicationInterventions.Contains("2"), "SN to assess caregiver filling medication box to determine if caregiver is preparing correctly.") %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions3' type='checkbox' name='{0}_485MedicationInterventions' value='3' {1} />", Model.TypeName, medicationInterventions.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions3">SN to determine if the</label>
                            <%  var determineFrequencEachMedPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485DetermineFrequencEachMedPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485DetermineFrequencEachMedPerson", determineFrequencEachMedPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions3">is able to identify the correct dose, route, and frequency of each medication.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions4' type='checkbox' name='{0}_485MedicationInterventions' value='4' {1} />", Model.TypeName, medicationInterventions.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions4">SN to assess if the</label>
                            <%  var assessIndicationEachMedPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485AssessIndicationEachMedPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485AssessIndicationEachMedPerson", assessIndicationEachMedPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions4">can verbalize an understanding of the indication for each medication.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485MedicationInterventions", "5", medicationInterventions.Contains("5"), "SN to establish reminders to alert patient to take medications at correct times.") %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions6' type='checkbox' name='{0}_485MedicationInterventions' value='6' {1} />", Model.TypeName, medicationInterventions.Contains("6").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions6">SN to assess the</label>
                            <%  var assessAdminInjectMedsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485AssessAdminInjectMedsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485AssessAdminInjectMedsPerson", assessAdminInjectMedsPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions6">administering injectable medications to determine if proper technique is utilized.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions7' type='checkbox' name='{0}_485MedicationInterventions' value='7' {1} />", Model.TypeName, medicationInterventions.Contains("7").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions7">SN to instruct the</label>
                            <%  var medicationHighRiskPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructHighRiskMedsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructHighRiskMedsPerson", medicationHighRiskPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions7">on precautions for high risk medications, such as, hypoglycemics, anticoagulants/antiplatelets, sedative hypnotics, narcotics, antiarrhythmics, antineoplastics, skeletal muscle relaxants.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions8' type='checkbox' name='{0}_485MedicationInterventions' value='8' {1} />", Model.TypeName, medicationInterventions.Contains("8").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions8">SN to administer IV</label>
                            <%= Html.TextBox(Model.TypeName + "_485AdministerIVType", data.AnswerOrEmptyString("485AdministerIVType"), new { @id = Model.TypeName + "_485AdministerIVType", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions8">at rate of</label>
                            <%= Html.TextBox(Model.TypeName + "_485AdministerIVRate", data.AnswerOrEmptyString("485AdministerIVRate"), new { @id = Model.TypeName + "_485AdministerIVRate", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions8">via</label>
                            <%= Html.TextBox(Model.TypeName + "_485AdministerIVVia", data.AnswerOrEmptyString("485AdministerIVVia"), new { @id = Model.TypeName + "_485AdministerIVVia", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions8">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485AdministerIVEvery", data.AnswerOrEmptyString("485AdministerIVEvery"), new { @id = Model.TypeName + "_485AdministerIVEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions9' type='checkbox' name='{0}_485MedicationInterventions' value='9' {1} />", Model.TypeName, medicationInterventions.Contains("9").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions9">SN to instruct the</label>
                            <%  var instructAdministerIVPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructAdministerIVPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructAdministerIVPerson", instructAdministerIVPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions9">to administer IV at rate of</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructAdministerIVRate", data.AnswerOrEmptyString("485InstructAdministerIVRate"), new { @id = Model.TypeName + "_485InstructAdministerIVRate", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions9">via</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructAdministerIVVia", data.AnswerOrEmptyString("485InstructAdministerIVVia"), new { @id = Model.TypeName + "_485InstructAdministerIVVia", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions9">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructAdministerIVEvery", data.AnswerOrEmptyString("485InstructAdministerIVEvery"), new { @id = Model.TypeName + "_485InstructAdministerIVEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions10' type='checkbox' name='{0}_485MedicationInterventions' value='10' {1} />", Model.TypeName, medicationInterventions.Contains("10").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions10">SN to flush peripheral IV with</label>
                            <%= Html.TextBox(Model.TypeName + "_485FlushPeripheralIVWith", data.AnswerOrEmptyString("485FlushPeripheralIVWith"), new { @id = Model.TypeName + "_485FlushPeripheralIVWith", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions10">cc of</label>
                            <%= Html.TextBox(Model.TypeName + "_485FlushPeripheralIVOf", data.AnswerOrEmptyString("485FlushPeripheralIVOf"), new { @id = Model.TypeName + "_485FlushPeripheralIVOf", @maxlength = "50" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions10">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485FlushPeripheralIVEvery", data.AnswerOrEmptyString("485FlushPeripheralIVEvery"), new { @id = Model.TypeName + "_485FlushPeripheralIVEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions11' type='checkbox' name='{0}_485MedicationInterventions' value='11' {1} />", Model.TypeName, medicationInterventions.Contains("11").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions11">SN to instruct the</label>
                            <%  var instructFlushPerpheralIVPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructFlushPerpheralIVPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructFlushPerpheralIVPerson", instructFlushPerpheralIVPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions11">to flush peripheral IV with</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructFlushPerpheralIVWith", data.AnswerOrEmptyString("485InstructFlushPerpheralIVWith"), new { @id = Model.TypeName + "_485InstructFlushPerpheralIVWith", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions11">cc of</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructFlushPerpheralIVOf", data.AnswerOrEmptyString("485InstructFlushPerpheralIVOf"), new { @id = Model.TypeName + "_485InstructFlushPerpheralIVOf", @maxlength = "50" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions11">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructFlushPerpheralIVEvery", data.AnswerOrEmptyString("485InstructFlushPerpheralIVEvery"), new { @id = Model.TypeName + "_485InstructFlushPerpheralIVEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions12' type='checkbox' name='{0}_485MedicationInterventions' value='12' {1} />", Model.TypeName, medicationInterventions.Contains("12").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions12">SN to flush central line with</label>
                            <%= Html.TextBox(Model.TypeName + "_485FlushCentralLineWith", data.AnswerOrEmptyString("485FlushCentralLineWith"), new { @id = Model.TypeName + "_485FlushCentralLineWith", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions12">cc of</label>
                            <%= Html.TextBox(Model.TypeName + "_485FlushCentralLineOf", data.AnswerOrEmptyString("485FlushCentralLineOf"), new { @id = Model.TypeName + "_485FlushCentralLineOf", @maxlength = "50" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions12">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485FlushCentralLineEvery", data.AnswerOrEmptyString("485FlushCentralLineEvery"), new { @id = Model.TypeName + "_485FlushCentralLineEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions13' type='checkbox' name='{0}_485MedicationInterventions' value='13' {1} />", Model.TypeName, medicationInterventions.Contains("13").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions13">SN to instruct</label>
                            <%  var instructFlushCentralLinePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructFlushCentralLinePerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructFlushCentralLinePerson", instructFlushCentralLinePerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions13">to flush central line with</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructFlushCentralLineWith", data.AnswerOrEmptyString("485InstructFlushCentralLineWith"), new { @id = Model.TypeName + "_485InstructFlushCentralLineWith", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions13">cc of</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructFlushCentralLineOf", data.AnswerOrEmptyString("485InstructFlushCentralLineOf"), new { @id = Model.TypeName + "_485InstructFlushCentralLineOf", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions13">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructFlushCentralLineEvery", data.AnswerOrEmptyString("485InstructFlushCentralLineEvery"), new { @id = Model.TypeName + "_485InstructFlushCentralLineEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions14' type='checkbox' name='{0}_485MedicationInterventions' value='14' {1} />", Model.TypeName, medicationInterventions.Contains("14").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions14">SN to access</label>
                            <%= Html.TextBox(Model.TypeName + "_485AccessPortType", data.AnswerOrEmptyString("485AccessPortType"), new { @id = Model.TypeName + "_485AccessPortType", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions14">port every</label>
                            <%= Html.TextBox(Model.TypeName + "_485AccessPortTypeEvery", data.AnswerOrEmptyString("485AccessPortTypeEvery"), new { @id = Model.TypeName + "_485AccessPortTypeEvery", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions14">and flush with</label>
                            <%= Html.TextBox(Model.TypeName + "_485AccessPortTypeWith", data.AnswerOrEmptyString("485AccessPortTypeWith"), new { @id = Model.TypeName + "_485AccessPortTypeWith", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions14">cc of</label>
                            <%= Html.TextBox(Model.TypeName + "_485AccessPortTypeOf", data.AnswerOrEmptyString("485AccessPortTypeOf"), new { @id = Model.TypeName + "_485AccessPortTypeOf", @class = "shorter", @maxlength = "15" })%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions14">every</label>
                            <%= Html.TextBox(Model.TypeName + "_485AccessPortTypeFrequency", data.AnswerOrEmptyString("485AccessPortTypeFrequency"), new { @id = Model.TypeName + "_485AccessPortTypeFrequency", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions15' type='checkbox' name='{0}_485MedicationInterventions' value='15' {1} />", Model.TypeName, medicationInterventions.Contains("15").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions15">SN to change IV tubing every</label>
                            <%= Html.TextBox(Model.TypeName + "_485ChangeIVTubingEvery", data.AnswerOrEmptyString("485ChangeIVTubingEvery"), new { @id = Model.TypeName + "_485ChangeIVTubingEvery", @class = "shorter", @maxlength = "15" })%>.
                        </span>
                    </div>
                </li>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485MedicationInterventions16' type='checkbox' name='{0}_485MedicationInterventions' value='7' {1} />", Model.TypeName, medicationInterventions.Contains("7").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions16">SN to evaluate due to exhibited</label>
                            <%  var medicationRegimenKnowledgePerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485MedicationRegimenKnowledgePerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485MedicationRegimenKnowledgePerson", medicationRegimenKnowledgePerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationInterventions16">medication regimen knowledge deficits.</label>
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485MedicationInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%=Html.ToggleTemplates(Model.TypeName + "_485MedicationInterventionTemplates")%>
                <%= Html.TextArea(Model.TypeName + "_485MedicationInterventionComments", data.AnswerOrEmptyString("485MedicationInterventionComments"), new { @id = Model.TypeName + "_485MedicationInterventionComments", @status = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<%  string[] medicationGoals = data.AnswerArray("485MedicationGoals"); %>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485MedicationGoals", "", new { @id = Model.TypeName + "_485MedicationGoalsHidden" })%>
    <div class="wide-column">
 <%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485MedicationGoals", "1", medicationGoals.Contains("1"), "Patient will remain free of adverse medication reactions during the episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485MedicationGoals2' type='checkbox' name='{0}_485MedicationGoals' value='2' {1} />", Model.TypeName, medicationGoals.Contains("2").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationGoals2">The</label>
                            <%  var verbalizeMedRegimenUnderstandingPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485VerbalizeMedRegimenUnderstandingPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485VerbalizeMedRegimenUnderstandingPerson", verbalizeMedRegimenUnderstandingPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationGoals2">will verbalize understanding of medication regimen, dose, route, frequency, indications, and side effects by</label>
                            <%= Html.TextBox(Model.TypeName + "_485VerbalizeMedRegimenUnderstandingDate", data.AnswerOrEmptyString("485VerbalizeMedRegimenUnderstandingDate"), new { @id = Model.TypeName + "_485VerbalizeMedRegimenUnderstandingDate", @class = "shorter", @maxlength = "10" })%>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485MedicationGoals3' type='checkbox' name='{0}_485MedicationGoals' value='3' {1} />", Model.TypeName, medicationGoals.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485MedicationGoals3">The</label>
                            <%  var demonstratePeripheralIVLineFlushPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485DemonstratePeripheralIVLineFlushPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485DemonstratePeripheralIVLineFlushPerson", demonstratePeripheralIVLineFlushPerson)%>
                            <label for="<%= Model.TypeName %>_485MedicationGoals3">will demonstrate understanding of flushing peripheral IV line.</label>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485MedicationGoalComments">Additional Goals</label>
            <div class="template-text">
                <%=Html.ToggleTemplates(Model.TypeName + "_485MedicationGoalTemplates")%>
                <%= Html.TextArea(Model.TypeName + "_485MedicationGoalComments", data.AnswerOrEmptyString("485MedicationGoalComments"), new { @id = Model.TypeName + "_485MedicationGoalComments", @status = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>