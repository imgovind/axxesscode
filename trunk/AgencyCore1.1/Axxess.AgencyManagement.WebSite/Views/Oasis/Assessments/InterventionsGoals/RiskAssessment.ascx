<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] riskAssessmentIntervention = data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer != "" ? data["485RiskAssessmentIntervention"].Answer.Split(',') : null; %>
<%  string[] riskAssessmentGoals = data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer != "" ? data["485RiskAssessmentGoals"].Answer.Split(',') : null; %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485RiskAssessmentIntervention" value=" " />
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485RiskAssessmentIntervention", "1", riskAssessmentIntervention.Contains("1"), "SN to assist patient to obtain ERS button.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RiskAssessmentIntervention", "2", riskAssessmentIntervention.Contains("2"), "SN to develop individualized emergency plan with patient.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485RiskAssessmentIntervention", "3", riskAssessmentIntervention.Contains("3"), "SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.")%>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RiskInterventionComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485RiskInterventionTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485RiskInterventionComments", data.ContainsKey("485RiskInterventionComments") ? data["485RiskInterventionComments"].Answer : "", new { @id = Model.TypeName + "_485RiskInterventionComments", @status = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485RiskAssessmentGoals" value=" " />
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485RiskAssessmentGoals", "1", riskAssessmentGoals.Contains("1"), "The patient will have no hospitalizations during the episode.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RiskAssessmentGoals2' name='{0}_485RiskAssessmentGoals' value='2' type='checkbox' {1} />", Model.TypeName, riskAssessmentGoals != null && riskAssessmentGoals.Contains("2") ? "checked='checked'" : "")%>
                        <span>
                            <label for="StartOfCare_485RiskAssessmentGoals2">The</label>
                            <%  var verbalizeEmergencyPlanPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer != "" ? data["485VerbalizeEmergencyPlanPerson"].Answer : "Patient/Caregiver"); %>
                            <%= Html.DropDownList(Model.TypeName + "_485VerbalizeEmergencyPlanPerson", verbalizeEmergencyPlanPerson) %>
                            <label for="<%= Model.TypeName %>_485RiskAssessmentGoals2">will verbalize understanding of individualized emergency plan by the end of the episode.</label>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RiskGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485RiskGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485RiskGoalComments", data.ContainsKey("485RiskGoalComments") ? data["485RiskGoalComments"].Answer : "", new { @id = Model.TypeName + "_485RiskGoalComments", @status = "(485 Locator 22) Goals" }) %>
            </div>
        </div>
    </div>
</fieldset>