<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] instructInterventions = data.AnswerArray("485InstructInterventions"); %>
<%  string[] instructGoals = data.AnswerArray("485InstructGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485InstructInterventions" value=" " />
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
<%  if (Model.Discipline == "Nursing") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "1", instructInterventions.Contains("1"), "SN to instruct patient to wear proper footwear when ambulating.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "2", instructInterventions.Contains("2"), "SN to instruct patient to use prescribed assistive device when ambulating.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "3", instructInterventions.Contains("3"), "SN to instruct patient to change positions slowly.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions4' name='{0}_485InstructInterventions' value='4' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("4").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions4">SN to instruct the</label>
                            <%  var instructRemoveClutterPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.ContainsKey("485InstructRemoveClutterPerson") && data["485InstructRemoveClutterPerson"].Answer != "" ? data["485InstructRemoveClutterPerson"].Answer : "Patient/Caregiver");%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructRemoveClutterPerson", instructRemoveClutterPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions4">to remove clutter from patient&#8217;s path such as clothes, books, shoes, electrical cords, or other items that may cause patient to trip.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions5' name='{0}_485InstructInterventions' value='5' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("5").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions5">SN to instruct the</label>
                            <%  var instructContactForFallPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.ContainsKey("485InstructContactForFallPerson") && data["485InstructContactForFallPerson"].Answer != "" ? data["485InstructContactForFallPerson"].Answer : "Patient/Caregiver");%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructContactForFallPerson", instructContactForFallPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions5">to contact agency to report any fall with or without minor injury and to call 911 for fall resulting in serious injury or causing severe pain or immobility.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "6", instructInterventions.Contains("6"), "HHA to assist with ADL&#8217;s &#38; IADL&#8217;s per HHA care plan.")%>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "7", instructInterventions.Contains("7"), "Therapist to instruct the patient to wear proper footwear when ambulating.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "8", instructInterventions.Contains("8"), "Therapist to instruct the patient to used prescribed assistive device when ambulating.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "9", instructInterventions.Contains("9"), "Therapist to instruct the patient to change positions slowly.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions10' name='{0}_485InstructInterventions' value='10' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("10").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions10">Therapist to instruct the</label>
                            <%  var instructRemoveRugsPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructRemoveRugsPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructContactForFallPerson", instructRemoveRugsPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions10">to remove throw rugs or use double-sided tape to secure rug in place.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions11' name='{0}_485InstructInterventions' value='11' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("11").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions11">Therapist to instruct the</label>
                            <%  var instructRemoveClutterTherapyPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructContactForFallPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructRemoveClutterTherapyPerson", instructRemoveClutterTherapyPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions11">to remove clutter from patient&#8217;s path such as clothes, books, shoes, electrical cords, or other items that may cause patient to trip.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions12' name='{0}_485InstructInterventions' value='12' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("12").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions12">Therapist to instruct the</label>
                            <%  var instructContactAgencyPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructContactAgencyPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructContactAgencyPerson", instructContactAgencyPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions12">to contact agency for increased dizziness or problems with balance.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "13", instructInterventions.Contains("13"), "Therapist to instruct the patient to use non-skid mats in tub/shower.")%>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions14' name='{0}_485InstructInterventions' value='14' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("14").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions14">Therapist to instruct the</label>
                            <%  var instructAdequateLightingPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructAdequateLightingPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructAdequateLightingPerson", instructAdequateLightingPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions14">on importance of adequate lighting in patient area.</label>
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485InstructInterventions15' name='{0}_485InstructInterventions' value='15' type='checkbox' {1} />", Model.TypeName, instructInterventions.Contains("15").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructInterventions15">Therapist to instruct the</label>
                            <%  var instructContactForFallTherapyPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructContactForFallTherapyPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructContactForFallTherapyPerson", instructContactForFallTherapyPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructInterventions15">to contact agency to report any fall with or without minor injury and to call 911 for fall resulting in serious injury or causing severe pain or immobility.</label>
                        </span>
                    </div>
                </li>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructInterventions", "16", instructInterventions.Contains("16"), "Therapist to request Physical Therapy Evaluation order from physician.")%>
<%  } %>
            </ul>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485IADLComments">Additional Orders</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485IADLOrderTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485IADLComments", data.ContainsKey("485IADLComments") ? data["485IADLComments"].Answer : "", new { @id = Model.TypeName + "_485IADLComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <input type="hidden" name="<%= Model.TypeName %>_485InstructGoals" value=" " />
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructGoals", "1", instructGoals.Contains("1"), "The patient will be free from falls during the episode.")%>
                <%= Html.CheckgroupOption(Model.TypeName + "_485InstructGoals", "2", instructGoals.Contains("2"), "The patient will be free from injury during the episode.")%>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485InstructGoals3' name='{0}_485InstructGoals' value='3' type='checkbox' {1} />", Model.TypeName, instructGoals.Contains("3").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructGoals3">The</label>
                            <%  var instructRemoveClutterGoalPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructRemoveClutterGoalPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructRemoveClutterGoalPerson", instructRemoveClutterGoalPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructGoals3">will remove all clutter from patient&#8217;s path, such as clothes, books, shoes, electrical cords, and other items, that may cause patient to trip by</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructRemoveClutterGoalDate", data.AnswerOrEmptyString("485InstructRemoveClutterGoalDate"), new { @id = Model.TypeName + "_485InstructRemoveClutterGoalDate", @class = "shorter" }) %>.
                        </span>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485InstructGoals4' name='{0}_485InstructGoals' value='4' type='checkbox' {1} />", Model.TypeName, instructGoals.Contains("4").ToChecked())%>
                        <span>
                            <label for="<%= Model.TypeName %>_485InstructGoals4">The</label>
                            <%  var instructRemoveRugsGoalPerson = new SelectList(new[] {
                                    new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                    new SelectListItem { Text = "Patient", Value = "Patient" },
                                    new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                                }, "Value", "Text", data.AnswerOrDefault("485InstructRemoveRugsGoalPerson", "Patient/Caregiver"));%>
                            <%= Html.DropDownList(Model.TypeName + "_485InstructRemoveRugsGoalPerson", instructRemoveRugsGoalPerson)%>
                            <label for="<%= Model.TypeName %>_485InstructGoals4">will remove throw rugs or secure them with double-sided tape by</label>
                            <%= Html.TextBox(Model.TypeName + "_485InstructRemoveRugsGoalDate", data.AnswerOrEmptyString("485InstructRemoveRugsGoalDate"), new { @id = Model.TypeName + "_485InstructRemoveRugsGoalDate", @class = "shorter" }) %>.
                        </span>
                    </div>
                </li>
<%  } %>
            </ul>
        </div>    
        <div class="row">
            <label for="<%= Model.TypeName %>_485IADLGoalComments">Additional Goals</label>
            <div class="template-text">
                <%= Html.ToggleTemplates(Model.TypeName + "_485IADLGoalTemplates") %>
                <%= Html.TextArea(Model.TypeName + "_485IADLGoalComments", data.ContainsKey("485IADLGoalComments") ? data["485IADLGoalComments"].Answer : "", new { @id = Model.TypeName + "_485IADLGoalComments", @title = "(485 Locator 22) Goals" })%>
            </div>
        </div>
    </div>
</fieldset>