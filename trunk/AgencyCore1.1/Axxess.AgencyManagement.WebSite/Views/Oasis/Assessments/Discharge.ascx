﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); var area = Model.Service.ToArea(); %>
<span class="wintitle">OASIS-C Discharge from Agency | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
     <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
     <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
     <%= Html.Hidden("AssessmentType", Model.TypeName, new { @class = "tabinput" })%>
     <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' name='{1}' area='{2}' tooltip='M0010 &#8211; M0150'>Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString(), area)%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1040 &#8211; M1050'>Risk Assessment</a>", area, AssessmentCategory.RiskAssessment.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1230'>Sensory Status</a>", area, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1242'>Pain</a>", area, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1306 &#8211; M1350'>Integumentary Status</a>", area, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1400 &#8211; M1410'>Respiratory Status</a>", area, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1500 &#8211; M1510'> Cardiac</a>", area, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1600 &#8211; M1620'>Elimination Status</a>", area, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1700 &#8211; M1750'>Neuro/Behavioral</a>", area, AssessmentCategory.NeuroBehavioral.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1800 &#8211; M1890'>ADL/IADLs</a>", area, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2004 &#8211; M2030'>Medications</a>", area, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2100 &#8211; M2110'>Care Management</a>", area, AssessmentCategory.CareManagement.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2300 &#8211; M2310'>Emergent Care</a>", area, AssessmentCategory.EmergentCare.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M0903 &#8211; M0906<br />M2400 &#8211; M2420'>Discharge</a>", area, AssessmentCategory.TransferDischargeDeath.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="tab-content">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
</div>