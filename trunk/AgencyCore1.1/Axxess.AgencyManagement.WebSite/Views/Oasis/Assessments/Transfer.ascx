﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); var area = Model.Service.ToArea(); %>
<span class="wintitle">OASIS-C Transfer, Not Discharged | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
     <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
     <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
     <%= Html.Hidden("AssessmentType", Model.TypeName, new { @class = "tabinput" })%>
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' name='{1}' area='{2}' tooltip='M0010 &#8211; M0150'>Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString(), area)%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1040 &#8211; M1055'>Risk Assessment</a>", area, AssessmentCategory.RiskAssessment.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1500 &#8211; M1510'>Cardiac</a>", area, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2004 &#8211; M2015'>Medications</a>", area, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2300 &#8211; M2310'>Emergent Care</a>", area, AssessmentCategory.EmergentCare.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M0903 &#8211; M0906<br />M2400 &#8211; M2440'>Transfer</a>", area, AssessmentCategory.TransferDischargeDeath.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="tab-content">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
</div>