﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); 
	var area = Model.Service.ToArea(); %>
<span class="wintitle">Non-OASIS Discharge | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty )%></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
     <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
     <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
     <%= Html.Hidden("AssessmentType", Model.TypeName, new { @class = "tabinput" })%>
     <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' name='{1}' area='{2}' >Demographics</a>", Model.TypeName, AssessmentCategory.Demographics.ToString(), area)%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Risk Assessment</a>", area, AssessmentCategory.RiskAssessment.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Sensory Status</a>", area, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Pain</a>", area, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Integumentary Status</a>", area, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Respiratory Status</a>", area, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Cardiac</a>", area, AssessmentCategory.Cardiac.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Elimination Status</a>", area, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Neuro/Behavioral</a>", area, AssessmentCategory.NeuroBehavioral.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>ADL/IADLs</a>", area, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Medications</a>", area, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Care Management</a>", area, AssessmentCategory.CareManagement.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Emergent Care</a>", area, AssessmentCategory.EmergentCare.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}'>Discharge</a>", area, AssessmentCategory.TransferDischargeDeath.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="tab-content">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
</div>