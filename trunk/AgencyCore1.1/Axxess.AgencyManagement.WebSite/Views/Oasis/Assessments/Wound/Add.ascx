<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<WoundViewData<Question>>" %>
<% var area = ViewContext.RouteData.DataTokens["area"]; %>
<div class="wrapper main"> 
    <%  using (Html.BeginForm("WoundUpdate", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.Type + "WoundCareForm" })) { %>
    <%= Html.Hidden("Id", Model.EventId) %>
    <%= Html.Hidden("Number", Model.Position.Number) %>
    <%= Html.Hidden("Action", "Add") %>
    <%= Html.Hidden("PatientGuid", Model.PatientId) %>
    <%= Html.Hidden("Number", Model.Position.Number) %>
    <%= Html.Hidden("X", Model.Position.x) %>
    <%= Html.Hidden("Y", Model.Position.y) %>
    <fieldset>
        <legend>Wound <%= Model.Position.Number %></legend>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericLocation<%= Model.Position.Number %>" class="fl">Location</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericLocation" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericLocation" + Model.Position.Number), new { @id = Model.Type + "_GenericLocation" + Model.Position.Number, @maxlength = "30", @class = "required", @status = "(Optional) Wound Location" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericOnsetDate<%= Model.Position.Number %>" class="fl">Onset Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_GenericOnsetDate<%= Model.Position.Number %>" value="<%= Model.Data.AnswerOrEmptyString("GenericOnsetDate" + Model.Position.Number) %>" id="<%= Model.Type %>_GenericOnsetDate<%= Model.Position.Number %>" title="(Optional) Wound Onset Date" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericWoundType<%= Model.Position.Number %>" class="fl">Wound Type</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericWoundType" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericWoundType" + Model.Position.Number), new { @id = Model.Type + "_GenericWoundType" + Model.Position.Number, @class = "WoundType", @maxlength = "30", @status = "(Optional) Wound Type" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericPressureUlcerStage<%= Model.Position.Number %>" class="fl">Pressure Ulcer Stage</label>
                <div class="fr">
                    <%  var pressureUlcerStage = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "I", Value = "I" },
                            new SelectListItem { Text = "II", Value = "II" },
                            new SelectListItem { Text = "III", Value = "III" },
                            new SelectListItem { Text = "IV", Value = "IV" },
                            new SelectListItem { Text = "Unstageable", Value = "Unstageable" }
                        }, "Value", "Text", Model.Data.AnswerOrDefault("GenericPressureUlcerStage" + Model.Position.Number, "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericPressureUlcerStage" + Model.Position.Number, pressureUlcerStage, new { @id = Model.Type + "_GenericPressureUlcerStage" + Model.Position.Number, @status = "(Optional) Pressure Ulcer Stage" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Measurements</label>
                <div class="fr ar">
                    <label for="<%= Model.Type %>_GenericMeasurementLength<%= Model.Position.Number %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericMeasurementLength" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericMeasurementLength" + Model.Position.Number), new { @id = Model.Type + "_GenericMeasurementLength" + Model.Position.Number, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Length" })%>
                    cm<br />
                    <label for="<%= Model.Type %>_GenericMeasurementWidth<%= Model.Position.Number %>"><em>Width</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericMeasurementWidth" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericMeasurementWidth" + Model.Position.Number), new { @id = Model.Type + "_GenericMeasurementWidth" + Model.Position.Number, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Width" })%>
                    cm<br />
                    <label for="<%= Model.Type %>_GenericMeasurementDepth<%= Model.Position.Number %>"><em>Depth</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericMeasurementDepth" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericMeasurementDepth" + Model.Position.Number), new { @id = Model.Type + "_GenericMeasurementDepth" + Model.Position.Number, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Wound Measurements, Depth" })%>
                    cm
                </div>
            </div>
            <div class="row">
                <label class="fl">Wound Bed</label>
                <div class="fr ar">
                    <label for="<%= Model.Type %>_GenericWoundBedGranulation<%= Model.Position.Number %>"><em>Granulation</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericWoundBedGranulation" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericWoundBedGranulation" + Model.Position.Number), new { @id = Model.Type + "_GenericWoundBedGranulation" + Model.Position.Number, @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Granulation" })%>
                    %<br />
                    <label for="<%= Model.Type %>_GenericWoundBedSlough<%= Model.Position.Number %>"><em>Slough</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericWoundBedSlough" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericWoundBedSlough" + Model.Position.Number), new { @id = Model.Type + "_GenericWoundBedSlough" + Model.Position.Number, @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Slough" })%>
                    %<br />
                    <label for="<%= Model.Type %>_GenericWoundBedEschar<%= Model.Position.Number %>"><em>Eschar</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericWoundBedEschar" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericWoundBedEschar" + Model.Position.Number), new { @id = Model.Type + "_GenericWoundBedEschar" + Model.Position.Number, @class = "shorter numeric", @maxlength = "5", @status = "(Optional) Wound Bed, Eschar" })%>
                    %
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericSurroundingTissue<%= Model.Position.Number %>" class="fl">Surrounding Tissue</label>
                <div class="fr">
                    <%  var surroundingTissue = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Pink", Value = "Pink" },
                            new SelectListItem { Text = "Dry", Value = "Dry" },
                            new SelectListItem { Text = "Pale", Value = "Pale" },
                            new SelectListItem { Text = "Moist", Value = "Moist" },
                            new SelectListItem { Text = "Excoriated", Value = "Excoriated" },
                            new SelectListItem { Text = "Calloused", Value = "Calloused" },
                            new SelectListItem { Text = "Normal", Value = "Normal" }
                        }, "Value", "Text", Model.Data.AnswerOrDefault("GenericSurroundingTissue" + Model.Position.Number, "0"));%>
                    <%= Html.DropDownList(Model.Type + "_GenericSurroundingTissue" + Model.Position.Number, surroundingTissue, new { @id = Model.Type + "_GenericSurroundingTissue" + Model.Position.Number, @status = "(Optional) Wound Surrounding Tissue" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericDrainage<%= Model.Position.Number %>" class="fl">Drainage</label>
                <div class="fr">
                    <%  var drainage = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Serous", Value = "Serous" },
                            new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" },
                            new SelectListItem { Text = "Sanguineous", Value = "Sanguineous" },
                            new SelectListItem { Text = "Purulent", Value = "Purulent" },
                            new SelectListItem { Text = "None", Value = "None" }
                        }, "Value", "Text", Model.Data.AnswerOrDefault("GenericDrainage" + Model.Position.Number, "0"));%>
                    <%= Html.DropDownList(Model.Type + "_GenericDrainage" + Model.Position.Number, drainage, new { @id = Model.Type + "_GenericDrainage" + Model.Position.Number, @status = "(Optional) Wound Drainage" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericDrainageAmount<%= Model.Position.Number %>" class="fl">Drainage Amount</label>
                <div class="fr">
                    <%  var drainageAmount = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Minimal", Value = "Minimal" },
                            new SelectListItem { Text = "Moderate", Value = "Moderate" },
                            new SelectListItem { Text = "Heavy", Value = "Heavy" },
                            new SelectListItem { Text = "None", Value = "None" }
                        }, "Value", "Text", Model.Data.AnswerOrDefault("GenericDrainageAmount" + Model.Position.Number, "0"));%>
                    <%= Html.DropDownList(Model.Type + "_GenericDrainageAmount" + Model.Position.Number, drainageAmount, new { @id = Model.Type + "_GenericDrainageAmount" + Model.Position.Number, @status = "(Optional) Wound Drainage Amount" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericOdor<%= Model.Position.Number %>" class="fl">Odor</label>
                <div class="fr">
                    <%  var odor = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" }
                        }, "Value", "Text", Model.Data.AnswerOrDefault("GenericOdor" + Model.Position.Number, "0"));%>
                    <%= Html.DropDownList(Model.Type + "_GenericOdor" + Model.Position.Number, odor, new { @id = Model.Type + "_GenericOdor" + Model.Position.Number, @status = "(Optional) Wound Odor" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Tunneling</label>
                <div class="fr ar">
                    <label for="<%= Model.Type %>_GenericTunnelingLength<%= Model.Position.Number %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericTunnelingLength" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericTunnelingLength" + Model.Position.Number), new { @id = Model.Type + "_GenericTunnelingLength" + Model.Position.Number, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Tunneling Length" })%>
                    cm<br />
                    <label for="<%= Model.Type %>_GenericTunnelingTime<%= Model.Position.Number %>"><em>Time</em></label>
                    <%  var tunnelingTime = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", Model.Data.AnswerOrEmptyString("GenericTunnelingTime" + Model.Position.Number));%>
                    <%= Html.DropDownList(Model.Type + "_GenericTunnelingTime" + Model.Position.Number, tunnelingTime, new { @id = Model.Type + "_GenericTunnelingTime" + Model.Position.Number, @class = "shorter", @status = "(Optional) Tunneling Time" })%>
                    o&#8217;clock
                </div>
            </div>
            <div class="row">
                <label class="fl">Undermining</label>
                <div class="fr ar">
                    <label for="<%= Model.Type %>_GenericUnderminingLength<%= Model.Position.Number %>"><em>Length</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericUnderminingLength" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericUnderminingLength" + Model.Position.Number), new { @id = Model.Type + "_GenericUnderminingLength" + Model.Position.Number, @class = "shorter floatnum", @maxlength = "5", @status = "(Optional) Undermining Length" })%>
                    cm<br />
                    <label for="<%= Model.Type %>_GenericUnderminingTimeFrom<%= Model.Position.Number %>"><em>Time</em></label>
                    <%  var underminingTimeFrom = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", Model.Data.AnswerOrEmptyString("GenericUnderminingTimeFrom" + Model.Position.Number));%>
                    <%= Html.DropDownList(Model.Type + "_GenericUnderminingTimeFrom" + Model.Position.Number, underminingTimeFrom, new { @id = Model.Type + "_GenericUnderminingTimeFrom" + Model.Position.Number, @class = "shorter", @status = "(Optional) Undermining Time Start" })%>
                    to
                    <%  var underminingTimeTo = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" },
                            new SelectListItem { Text = "11", Value = "11" },
                            new SelectListItem { Text = "12", Value = "12" }
                        }, "Value", "Text", Model.Data.AnswerOrEmptyString("GenericUnderminingTimeTo" + Model.Position.Number));%>
                    <%= Html.DropDownList(Model.Type + "_GenericUnderminingTimeTo" + Model.Position.Number, underminingTimeTo, new { @id = Model.Type + "_GenericUnderminingTimeTo" + Model.Position.Number, @class = "shorter", @status = "(Optional) Undermining Time End" })%>
                    o&#8217;clock
                </div>
            </div>
            <div class="row">
                <label class="fl">Device</label>
                <div class="fr ar">
                    <label for="<%= Model.Type %>_GenericDeviceType<%= Model.Position.Number %>"><em>Type</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericDeviceType" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericDeviceType" + Model.Position.Number), new { @id = Model.Type + "_GenericDeviceType" + Model.Position.Number, @class = "short DeviceType", @maxlength = "30", @status = "(Optional) Device Type " })%>
                    <br />
                    <label for="<%= Model.Type %>_GenericDeviceSetting<%= Model.Position.Number %>"><em>Setting</em></label>
                    <%= Html.TextBox(Model.Type + "_GenericDeviceSetting" + Model.Position.Number, Model.Data.AnswerOrEmptyString("GenericDeviceSetting" + Model.Position.Number), new { @id = Model.Type + "_GenericDeviceSetting" + Model.Position.Number, @class = "short", @maxlength = "30", @status = "(Optional) Device Setting " })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericUploadFile<%= Model.Position.Number %>" class="fl">Attachment</label>
                <div class="fr">
                    <input type="file" name="<%= Model.Type %>_GenericUploadFile<%= Model.Position.Number %>" value="Upload" title="(Optional) Photo of Wound" />
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
</div>