﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); var area = Model.Service.ToArea(); %>
<span class="wintitle">OASIS-C Follow-up | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
     <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
     <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
     <%= Html.Hidden("AssessmentType", Model.TypeName, new { @class = "tabinput" })%>
    <ul class="vertical-tab-list strong">
        <li><%= string.Format("<a href='#{0}_{1}' name='{1}' area='{2}' tooltip='M0010 &#8211; M0150'>Clinical Record Items</a>", Model.TypeName, AssessmentCategory.Demographics.ToString(), area)%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1020 &#8211; M1030'>History &#38; Diagnoses</a>", area, AssessmentCategory.PatientHistory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1200'>Sensory Status</a>", area, AssessmentCategory.Sensory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1242'>Pain</a>", area, AssessmentCategory.Pain.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1306 &#8211; M1350'>Integumentary Status</a>", area, AssessmentCategory.Integumentary.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1400'>Respiratory Status</a>", area, AssessmentCategory.Respiratory.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1610 &#8211; M1630'>Elimination Status</a>", area, AssessmentCategory.Elimination.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M1810 &#8211; M1860'>ADL/IADLs</a>", area, AssessmentCategory.AdlIadl.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2030'>Medications</a>", area, AssessmentCategory.Medications.ToString())%></li>
        <li><%= string.Format("<a href='{0}/Oasis/Category' name='{1}' tooltip='M2200'>Therapy Need &#38; Plan of Care</a>", area, AssessmentCategory.TherapyNeed.ToString())%></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="tab-content">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
</div>