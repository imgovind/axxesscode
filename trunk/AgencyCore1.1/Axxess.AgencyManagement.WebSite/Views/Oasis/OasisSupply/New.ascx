﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NoteSupplyNewViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddSupply", "Oasis", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newOasisSupplyForm", @class = "mainform" })) { %>
    <%= Html.Hidden("EventId", Model.EventId, new { @default = Model.EventId })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @default = Model.PatientId })%>
    <%= Html.Hidden("UniqueIdentifier", Guid.Empty, new { @default = Guid.Empty.ToString() })%>
    <%= Html.Hidden("Code", string.Empty)%>
    <%= Html.Hidden("UnitCost", string.Empty)%>
    <%= Html.Hidden("RevenueCode", string.Empty)%>
    <fieldset>
        <legend>New Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", string.Empty, new { @class = "required long" })%></div>
            </div>
            <div class="row">
                <label class="fl">Date</label>
                <div class="fr"><%= Html.DatePicker("DateForEdit", DateTime.Today.ToZeroFilled(), true, new { @default = DateTime.Today.ToZeroFilled() }) %></div>
            </div>
            <div class="row">
                <label class="fl">Quantity</label>
                <div class="fr"><%= Html.TextBox("Quantity", "1", new { @class = "required numeric", @default = "1" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Close</a></li>
        <li><a class="save clear">Save &#38; Add Another</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
