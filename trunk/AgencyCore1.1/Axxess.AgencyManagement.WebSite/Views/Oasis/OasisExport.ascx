﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentExportViewData>" %>
<span class="wintitle">OASIS Export | <%= Current.AgencyName %></span>
<%  if (Model.ViewListPermissions != AgencyServices.None) { %>
    <%  string pagename = "OasisExport"; %>
<div class="wrapper main blue">
	<%  using (Html.BeginForm("Generate", "Oasis", FormMethod.Post, new { @id = pagename+ "_GenerateOasisForm" })) { %>
		<%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%> 
	<ul class="fr buttons ac">
	    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
	    <li><a url="Export/OasisExport" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li><br />
		<%  } %>
		<li><a url="Oasis/Export" class="grid-refresh">Refresh</a></li>
	</ul>
	<fieldset class="grid-controls ac">
	    <div class="filter">
	        <input type="hidden" name="ServiceId" class="input" value="<%= (int)Model.Service %>" />
	        <label for="<%= pagename %>_BranchId">Branch</label>
	        <%= Html.BranchOnlyList("BranchId",Model.LocationId.ToString(), (int)Model.Service, new { @id = pagename + "_BranchId", @class = "location input" })%>
	    </div>
	    <div class="filter">
	        <label for="<%= pagename %>_PaymentSources">Payment Source</label>
			<select id="<%= pagename %>_PaymentSources" name="PaymentSources" class="multiple-payment" multiple="multiple">
			    <option value="0">None; No Charge</option>
			    <option value="1" selected="selected">Medicare (Traditional)</option>
			    <option value="2">Medicare (HMO/Managed Care)</option>
			    <option value="3">Medicaid (Traditional)</option>
			    <option value="4">Medicaid (HMO/Managed Care)</option>
			    <option value="5">Workers' Compensation</option>
			    <option value="6">Title Programs (Title III, V, XX)</option>
			    <option value="7">Other Government (CHAMPUS, VA, etc)</option>
			    <option value="8">Private Insurance</option>
			    <option value="9">Private HMO/Managed Care</option>
			    <option value="10">Self-pay</option>
			    <option value="11">Unknown</option>
			    <option value="12">Other</option>
			</select>
		</div>
	</fieldset>  
	<div id="<%=pagename %>GridContainer">
		<%= Html.Hidden("SortParams", string.Format("{0}-{1}", Model.SortColumn, Model.SortDirection), new { @id = pagename + "_SortParams", @class = "input" })%>  
		<%  Html.RenderPartial("Content/OasisExport", Model); %>
	</div>
	<%  } %>
</div>
<%  } %>