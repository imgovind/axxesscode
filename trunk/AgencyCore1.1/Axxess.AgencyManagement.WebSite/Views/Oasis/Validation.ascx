﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ValidationInfoViewData>" %>
<div class="wrapper main note">
<%  using (Html.BeginForm("SubmitOnly", "Oasis", FormMethod.Post, new { @id = "oasisValidationForm" })) { %>
    <%= Html.Hidden("PatientId", Model.PatientId)%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("Id", Model.AssessmentId)%>
    <%= Html.Hidden("Service", Model.Service.ToString()) %>
    <%= Html.Hidden("OasisValidationType", Model.AssessmentType)%>
    <%  if (Model.ValidationErrors != null && Model.ValidationErrors.Count > 0) { %>
    <div class="acore-grid">
        <ul>
            <li class="ac">
                <h3>
                    You have
                    <%= Model.ValidationErrors.Count(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL") %>
                    error<%= Model.ValidationErrors.Count(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL") != 1 ? "s" : string.Empty%>
                    and
                    <%= Model.ValidationErrors.Count(e => e.ErrorType == "WARNING")%>
                    warning<%= Model.ValidationErrors.Count(e => e.ErrorType == "WARNING") != 1 ? "s" : string.Empty%>
                </h3>
            </li>
        </ul>
        <ol class="dc oasis-validation">
        <%  foreach (var data in Model.ValidationErrors) { %>
            <li class="<%= data.ErrorType != "ERROR" && data.ErrorType != "FATAL" ? "warning" : string.Empty %>">
                <a class="error-anchor no-border" question="<%= data.ErrorDup.Substring(0, 5) %>" version="<%= Model.Version %>">
                    <%= data.ErrorType == "ERROR" || data.ErrorType == "FATAL" ? "<span class='img icon32 error fl'></span>" : "<span class='img icon32 exclamation fl'></span>" %>
                    <h3><%= data.ErrorDup != "M0001" ? data.ErrorDup : string.Empty %></h3>
                    <span class="description"><%= data.Description %></span>
                    <div class="clr"></div>
                </a>
            </li>
        <%  } %>
        </ol>
    </div>
    <%  } %>
    <%  if (Model.IsErrorFree) { %>
        <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() || Model.AssessmentType == AssessmentType.Recertification.ToString() || Model.AssessmentType == AssessmentType.FollowUp.ToString()) { %>
    <fieldset>
        <legend>OASIS Information</legend>
        <div class="column">
            <div class="row no-input">
                <label class="fl">HIPPS Code</label>
                <div class="fr"><%= Model.HIPPSCODE %></div>
            </div>
            <div class="row no-input">
                <label class="fl">OASIS Claim Matching Key</label>
                <div class="fr"><%= Model.HIPPSKEY %></div>
            </div>
        </div>
        <div class="column">
            <div class="row no-input">
                <label class="fl">HHRG Code</label>
                <div class="fr"><%= Model.HHRG %></div>
            </div>
            <div class="row no-input">
                <label class="fl">Episode Payment Rate</label>
                <div class="fr"><%= Model.StandardPaymentRate != 0 ? string.Format("${0}", Math.Round(Model.StandardPaymentRate, 2).ToString()) : string.Empty %></div>
            </div>
        </div>
    </fieldset>
        <%  } %>
        <%  if (Model.Status != ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() && Model.Status != ((int)ScheduleStatus.OasisCompletedExportReady).ToString()) { %>
    <fieldset>
        <legend>Signature</legend>
            <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() || Model.AssessmentType == AssessmentType.Recertification.ToString() || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() || Model.AssessmentType == AssessmentType.FollowUp.ToString()) { %>
        <div class="column">
            <div class="row">
                <label for="<%= Model.AssessmentType%>_TimeIn" class="fl">Time In</label>
                <div class="fr"><%= Html.TextBox("TimeIn", Model.TimeIn, new { @id = Model.AssessmentType + "_TimeIn", @class = "time-picker required" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.AssessmentType%>_TimeOut" class="fl">Time Out</label>
                <div class="fr"><%= Html.TextBox("TimeOut", Model.TimeOut, new { @id = Model.AssessmentType + "_TimeOut", @class = "time-picker required" }) %></div>
            </div>
        </div>
            <%  } %>
            <%  if (Model.AssessmentType == AssessmentType.Discharge.ToString() || Model.AssessmentType == AssessmentType.Death.ToString()) { %>
        <div class="column">
            <div class="row">
                <label for="<%= Model.AssessmentType%>_TimeIn" class="fl">Time In</label>
                <div class="fr"><%= Html.TextBox("TimeIn", Model.TimeIn, new { @id = Model.AssessmentType + "_TimeIn", @class = "time-picker" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.AssessmentType%>_TimeOut" class="fl">Time Out</label>
                <div class="fr"><%= Html.TextBox("TimeOut", Model.TimeOut, new { @id = Model.AssessmentType + "_TimeOut", @class = "time-picker" }) %></div>
            </div>
        </div>
            <%  } %>
        <div class="column">
            <div class="row">
                <label for="<%= Model.AssessmentType%>_ValidationClinician" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("ValidationClinician", string.Empty, new { @id = Model.AssessmentType + "_ValidationClinician", @class = "required" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.AssessmentType%>_ValidationSignatureDate" class="fl">Date</label>
                <div class="fr"><%= String.Format("<input type='text' id='{0}_ValidationSignatureDate' name='ValidationSignatureDate' class='date-picker required' mindate='{1}' maxdate='{2}' />", Model.AssessmentType, Model.EpisodeStartDate.ToShortDateString(), Model.EpisodeEndDate.ToShortDateString()) %></div>
            </div>
        </div>
    </fieldset>
        <%  } else { %>
    <fieldset>
        <legend>Validation Successful</legend>
        <div class="wide-column">
            <div class="row">
                <p>
                    The OASIS Validation Check was successful and returned no errors. Click the Exit button to
                    return to the OASIS assessment to approve or return to the clinician.
                </p>
                <p>
                    <em>
                        Please note that this document has been signed and completed already. To sign this
                        document, please reopen it from the Patient Charts or the Schedule Center.
                    </em>
                </p>
            </div>
        </div>
    </fieldset>
        <%  } %>
    <%  } %>
    <ul class="buttons ac">
        <li class="red"><a class="home-health-gold">OASIS Scrubber</a></li>
        <li><a class="oasis-export-file">OASIS File</a></li>
    <%  if (ViewData["pps"] == "true") { %>
        <li><a class="pps-plus-export">PPSPlus Export</a></li>
    <%  } %>
        <li><a class="close">Return to OASIS</a></li>
    <%  if (Model.IsErrorFree) { %>
        <li><a class="save close">Complete</a></li>
    <%  } %>
    </ul>
<%  } %>
</div>