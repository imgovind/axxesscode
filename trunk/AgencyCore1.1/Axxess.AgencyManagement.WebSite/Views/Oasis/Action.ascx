﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var area = Model.Service.ToArea(); %>
<ul class="buttons ac">
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'Save')">Save</a></li>
<%  if (Model.IsLastTab) { %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'CheckError',function() { Oasis.Validation.Load('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>',<%= Model.ShowOasisVendorButton.ToString().ToLower() %>,'<%= area %>')})">Save &#38; Check for Errors</a></li>
    <% } else { %>
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveComplete',function() { Oasis.NonOasisSignature('<%= Model.Id %>','<%= Model.PatientId %>','<%= area %>')})">Save &#38; Complete</a></li>
    <% } %>
<%  } else { %>
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveContinue',function(){})">Save &#38; Continue</a></li>
<%  } %>
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'SaveExit',function(){})">Save &#38; Exit</a></li>
<%  var hintText=new List<string>(); %>
<%  if (Model.IsUserCanApprove) { %>
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'Approve',function(){})">Approve</a></li>
    <% hintText.Add("Approve"); %>
<%  } %>
 <%  if (Model.IsUserCanReturn){ %>
    <li><a onclick="Oasis.Assessment.Return('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>',$(this), '<%= area %>')">Return</a></li>
    <% hintText.Add("Return"); %>
<%  }  %>
<%  if (Model.AssessmentTypeNum.ToInteger() < 10 && !Model.IsLastTab) { %>
    <li><a onclick="Oasis.Assessment.FormSubmit('<%= Model.TypeName %>',$(this),'CheckError',function() { Oasis.Validation.Load('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>',<%= Model.ShowOasisVendorButton.ToString().ToLower() %>,'<%= area %>')})">Check for Errors</a></li>
<%  } %>
</ul>
<%  if (Model.IsUserCanApprove){ %>
<p class="ac"><em><%= string.Format("Note: The {0} button(s) will not save your current changes. Click the Save button first.", hintText.ToArray().Join(" and "))%></em></p>
<%  } %>