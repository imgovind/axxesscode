﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<QAViewData>" %>
<span class="wintitle">Quality Assurance (QA) Center | <%= Current.AgencyName %></span>
<% if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None)
   { %>
<% var pagename = "CaseManagement"; %>
<% var area = Model.Service.ToArea();
    %>
<div class="wrapper main blue">
	<%=Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%> 
    <div class="fr ac">
    <% if (Model.ExportPermissions != AgencyServices.None)
            { %>
        <div class="button"><a area="<%=area %>" service="<%=(int)Model.ExportPermissions %>"  class="export servicepermission" url="/Export/QAScheduleList">Excel Export</a></div>
         <br />
          <%} %>
        <div class="button"><a   class="grid-refresh" area="<%=area %>" url="/QA/Content">Refresh</a></div>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter ignore-grid-binding">
            <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pagename, new { @class = "service input", @id = pagename + "_ServiceId" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.AvailableService, new { @id = pagename + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_Status">Status</label>
            <select id="<%= pagename %>_Status" name="Status">
                <option value="0">All</option>
                <option value="1" selected>Active</option>
                <option value="2">Discharged</option>
            </select>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_StartDate">Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-7), false, new { @id = pagename + "_StartDate", @class = "short" })%>
            <label for="<%= pagename %>_EndDate">&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, false, new { @id = pagename + "_EndDate", @class = "short" })%>
        </div>
        <ul class="buttons ac">
            <li><a class="group-button" grouping="PatientName">Group By Patient</a></li>
            <li><a class="group-button" grouping="EventDate">Group By Date</a></li>
            <li><a class="group-button" grouping="TaskName">Group By Task</a></li>
            <li><a class="group-button" grouping="UserName">Group By Clinician</a></li>
        </ul>
    </fieldset>
     <form method="post" action="/QA/Process" class="mainform">
    <div id="<%= pagename %>GridContainer" init="Agency.QA.InitGridContent"><% Html.RenderPartial("Content", Model); %></div>
   
    <%  if (!Current.IsAgencyFrozen) { %>
        <div class="abs bottom blue wrapper">
            <ul class="buttons ac">
                <li><a class="approve-button servicepermission" service="<%=(int)Model.ApprovePermissions %>" commandtype="Approve">Approve Selected</a></li>
                <li><a class="return-button servicepermission" service="<%=(int)Model.ReturnPermissions %>" commandtype="Return">Return Selected</a></li>
            </ul>
        </div>
    <%  } %>
    
    </form>
</div>
<%} %>