﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<QaCenterViewData>" %>
<%  if (Model.Patient != null) { %>
    <%--<div class="top">
        <%  Html.RenderPartial("/Views/Agency/QA/Info.ascx", Model.Patient); %>
    </div>
    <div class="bottom">
        <%  Html.RenderPartial("/Views/Agency/QA/Activities.ascx", Model.Activities); %>
    </div>--%>
    <%= Html.Hidden("QACenter_PatientId", Model.Patient.Id) %>
<%  } else { %>
    <div class="abs center">No Documents found</div>
<%  } %>
