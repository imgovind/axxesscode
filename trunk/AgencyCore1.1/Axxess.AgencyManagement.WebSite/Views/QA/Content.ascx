﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<QAViewData>" %>
<%  if (Model.AvailableService.Has(Model.Service)) { %>
<%= Html.Filter("GroupName", Model.GroupName, new { @class = "grouping" })%>
<%= Html.Filter("SortParams", string.Format("{0}-{1}",Model.SortColumn,Model.SortDirection), new { @class = "sort-parameters" })%>
<%  var pagename = "CaseManagement"; %>
<%  var actions = new List<string>(); %>
<%  if (Model.ApprovePermissions.Has(Model.Service)) actions.Add("approve"); %>
<%  if (Model.ReturnPermissions.Has(Model.Service)) actions.Add("return"); %>
<%  if (Model.IsUserCanEditApproved) actions.Add("edit"); %>
<%  if (Model.IsUserCanPrint) actions.Add("print"); %>
<%  var actionClass = actions.ToArray().Join(" "); %>
<%  var isLink = actions.Count > 0; %>
<%= Html.Telerik().Grid(Model.Activities).HtmlAttributes(new { @class = "qa-activity-grid button-bottom-bar" }).Name(pagename + "_Grid").Columns(columns => {
        if (!Current.IsAgencyFrozen && (Model.ApprovePermissions.Has(Model.Service) || Model.ReturnPermissions.Has(Model.Service))) columns.Bound(s => s.EventId).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}|{1}|{2}|{3}'/>", s.EpisodeId, s.PatientId, s.EventId, s.Task)).Title("").Width(50).HtmlAttributes(new { @class = "centered-unpadded-cell" }).Sortable(false);
        columns.Bound(s => s.PatientName);
        columns.Bound(s => s.EventDate).Width(100).Format("{0:MM/dd/yyyy}");
        columns.Bound(s => s.TaskName).Template(s => isLink? string.Format("<a class=\"qalink {0} {1} {2}\" >{3}</a>", s.Group, actionClass, s.IsPOCNeeded ? "poc" : string.Empty, s.TaskName):s.TaskName).Title("Task").Title("Task").Width(250);
        columns.Bound(s => s.Status).Width(200).Sortable(false);
        columns.Bound(s => s.RedNote).Title(" ").Width(30).Template(s => s.RedNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 red note\">{0}</a>", s.RedNote) : string.Empty).Sortable(false).Visible(Model.IsUserCanSeeStickyNote);
        columns.Bound(s => s.YellowNote).Title(" ").Width(30).Template(s => s.YellowNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\">{0}</a>", s.YellowNote) : string.Empty).Sortable(false).Visible(Model.IsUserCanSeeStickyNote);
        columns.Bound(s => s.BlueNote).Title(" ").Width(30).Template(s => s.BlueNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 blue note\">{0}</a>", s.BlueNote) : string.Empty).Sortable(false).Visible(Model.IsUserCanSeeStickyNote);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(150);
        columns.Bound(s => s.EventId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "id" }).Sortable(false).Hidden();
		columns.Bound(s => s.PatientId).HeaderHtmlAttributes(new { style = "font-size:0;" }).Width(0).HtmlAttributes(new { style = "font-size:0;", @class = "pid" }).Sortable(false).Hidden();
		columns.Bound(s => s.EventId).Template(s => Model.Service.ToString()).HeaderHtmlAttributes(new { style = "font-size:0;" }).Width(0).HtmlAttributes(new { style = "font-size:0;", @class = "service" }).Sortable(false).Hidden();
		columns.Bound(s => s.Type).Template(s => s.Type).HeaderHtmlAttributes(new { style = "font-size:0;" }).Width(0).HtmlAttributes(new { style = "font-size:0;", @class = "type" }).Sortable(false).Hidden();
    }).Groupable(settings => settings.Groups(groups => {
        if (Model.GroupName == "PatientName") groups.Add(s => s.PatientName);
        else if (Model.GroupName == "EventDate") groups.Add(s => s.EventDate);
        else if (Model.GroupName == "TaskName") groups.Add(s => s.TaskName);
        else if (Model.GroupName == "UserName") groups.Add(s => s.UserName);
        else groups.Add(s => s.EventDate);
    })).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = Model.SortColumn;
        var sortDirection = Model.SortDirection;
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "TaskName") {
            if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        }
    })).NoRecordsTemplate("No records to display.").Scrollable().Footer(false) %>
<%  } %>
