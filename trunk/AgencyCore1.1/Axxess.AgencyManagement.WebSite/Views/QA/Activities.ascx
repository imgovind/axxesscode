﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<%  Html.Telerik().Grid(Model).Name("QACenterActivityGrid").Columns(columns => {
        columns.Bound(s => s.EventDate).Title("Scheduled Date").Width(100);
        columns.Bound(s => s.EpisodeRange).Title("Episode").Width(170);
        columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).Title("Task").Title("Task");
        columns.Bound(s => s.Status).Width(200);
        columns.Bound(s => s.RedNote).Title(" ").Width(35).Template(s => s.RedNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 red note\" tooltip=\"{0}\"> </a>", s.RedNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.YellowNote).Title(" ").Width(35).Template(s => s.YellowNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\" tooltip=\"{0}\"></a>", s.YellowNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.BlueNote).Title(" ").Width(35).Template(s => s.BlueNote.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 blue note\" tooltip=\"{0}\"></a>", s.BlueNote) : string.Empty).Sortable(false);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(150);
    }).Sortable(s => s.Enabled(false)).Scrollable().Footer(false).Render(); %>
