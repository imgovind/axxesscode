﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%= Model.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 blue note\">{0}</a>", Model) : string.Empty %>