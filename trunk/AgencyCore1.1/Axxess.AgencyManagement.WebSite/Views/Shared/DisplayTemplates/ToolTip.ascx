﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%= Model.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\">{0}</a>", Model) : string.Empty %>