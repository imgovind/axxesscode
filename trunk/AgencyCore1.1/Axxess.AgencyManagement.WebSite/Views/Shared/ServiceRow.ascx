﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceViewData>" %>
<% if((Model.LimiterService & Current.AcessibleServices).IsAlone()) { %>
    <%= Html.Hidden("Service", (int)(Model.LimiterService & Current.AcessibleServices), new { @id = "LimitedService" })%>
<% } else { %>
    <%= Html.LimitedServicesRow("Service", "Service", Model.SelectedService, Model.LimiterService, new { @id = "LimitedService" })%>
<% } %>

