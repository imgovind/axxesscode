﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdvancedArrayIdViewData>" %>
<% if(Model.HasOwnFieldSet) { %>
	<fieldset>
		<legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span><span class="required-red">*</span></legend>
		<div class="wide-column">
<% } %>
<div class="row">
	<% if(!Model.HasOwnFieldSet) { %>
    <label><span class="green">(M0150)</span> Payment Source</label>
    <% } %>
    <ul class="checkgroup <%= Model.CheckGroupWidth %>">
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceNone" <%= Model.HasValue("0").ToChecked() %> type="checkbox" value="0" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceNone">None; No Charge for Current Services</label>
            </div>
        </li>
        <% if(Model.NamePrefix == "Pd") { %>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceMedicare" <%= Model.HasValue("1").ToChecked() %> type="checkbox" value="1" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceMedicare">Medicare (Traditional Fee-for-Service)</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceMedicareHmo" <%= Model.HasValue("2").ToChecked() %> type="checkbox" value="2" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceMedicareHmo">Medicare (HMO/Managed Care)</label>
            </div>
        </li>
        <% } %>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceMedicaid" <%= Model.HasValue("3").ToChecked() %> type="checkbox" value="3" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceMedicaid">Medicaid (Traditional Fee-for-Service)</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceMedicaidHmo" <%= Model.HasValue("4").ToChecked() %> type="checkbox" value="4" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceMedicaidHmo">Medicaid (HMO/Managed Care)</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceWorkers" <%= Model.HasValue("5").ToChecked() %> type="checkbox" value="5" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceWorkers">Workers&#8217; Compensation</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceTitleProgram" <%= Model.HasValue("6").ToChecked() %> type="checkbox" value="6" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceTitleProgram">Title Programs (e.g., Titile III, V, or XX)</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceOtherGovernment" <%= Model.HasValue("7").ToChecked() %> type="checkbox" value="7" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceOtherGovernment">Other Government (e.g., CHAMPUS, VA, etc.)</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourcePrivate" <%= Model.HasValue("8").ToChecked() %> type="checkbox" value="8" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourcePrivate">Private Insurance</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourcePrivateHmo" <%= Model.HasValue("9").ToChecked() %> type="checkbox" value="9" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourcePrivateHmo">Private HMO/Managed Care</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceSelf" <%= Model.HasValue("10").ToChecked() %> type="checkbox" value="10" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceSelf">Self-Pay</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input id="<%= Model.IdPrefix %>PaymentSourceUnknown" <%= Model.HasValue("11").ToChecked() %> type="checkbox" value="11" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSourceUnknown">Unknown</label>
            </div>
        </li>
        <li class="option">
            <div class="wrapper">
                <input type="checkbox" id="<%= Model.IdPrefix %>PaymentSource" <%= Model.HasValue("12").ToChecked() %> value="12" name="<%= Model.NamePrefix %>PaymentSources" class="required" />
                <label for="<%= Model.IdPrefix %>PaymentSource">Other</label>
            </div>
            <div class="more">
                <label for="<%= Model.IdPrefix %>OtherPaymentSource" class="fl">Specify</label>
                <div class="fr"><%= Html.TextBox(Model.NamePrefix + "OtherPaymentSource", "", new { @id = Model.IdPrefix + "OtherPaymentSource" }) %></div>
                <div class="clr"></div>
            </div>
        </li>
    </ul>
</div>

<% if(Model.HasOwnFieldSet) { %>
		</div>
	</fieldset>
<% } %>