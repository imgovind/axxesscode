﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ValueIdViewData<int>>" %>
<fieldset>
    <legend>Emergency Triage</legend>
    <div class="column">
        <div class="row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton("Triage", "1", Model.Value == 1, new { @id = Model.IdPrefix + "_Triage1", @class = "required" })%>
                        <label for="<%= Model.IdPrefix %>_Triage1">
                            <span class="num strong">1 &#8211;</span>
                            <strong>Life threatening</strong>
                            (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.
                        </label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton("Triage", "2", Model.Value == 2, new { @id = Model.IdPrefix + "_Triage2", @class = "required" })%>
                        <label for="<%= Model.IdPrefix %>_Triage2">
                            <span class="num strong">2 &#8211;</span>
                            <strong>Not life threatening but would suffer severe adverse effects</strong>
                            from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)
                        </label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton("Triage", "3", Model.Value == 3, new { @id = Model.IdPrefix + "_Triage3", @class = "required" })%>
                        <label for="<%= Model.IdPrefix %>_Triage3">
                            <span class="num strong">3 &#8211;</span>
                            <strong>Visits could be postponed 24-48</strong>
                            hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)
                        </label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <%= Html.RadioButton("Triage", "4", Model.Value == 4, new { @id = Model.IdPrefix + "_Triage4", @class = "required" })%>
                        <label for="<%= Model.IdPrefix %>_Triage4">
                            <span class="num strong">4 &#8211;</span>
                            <strong>Visits could be postponed 72-96</strong>
                            hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10-14 days, routine catheter changes)
                        </label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>
