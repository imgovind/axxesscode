﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ArrayIdViewData>" %>
<fieldset>
	<legend>DME Needed <span class="light">(Optional)</span></legend>
	<input type="hidden" value="" name="DMECollection" />
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection0" type="checkbox" value="0" name="DMECollection" <%= Model.HasValue("0").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection0">Bedside Commode</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection1" type="checkbox" value="1" name="DMECollection" <%= Model.HasValue("1").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection1">Cane</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection2" type="checkbox" value="2" name="DMECollection" <%= Model.HasValue("2").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection2">Elevated Toilet Seat</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection3" type="checkbox" value="3" name="DMECollection" <%= Model.HasValue("3").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection3">Grab Bars</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection4" type="checkbox" value="4" name="DMECollection" <%= Model.HasValue("4").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection4">Hospital Bed</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection5" type="checkbox" value="5" name="DMECollection" <%= Model.HasValue("5").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection5">Nebulizer</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection6" type="checkbox" value="6" name="DMECollection" <%= Model.HasValue("6").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection6">Oxygen</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection7" type="checkbox" value="7" name="DMECollection" <%= Model.HasValue("7").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection7">Tub/Shower Bench</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection8" type="checkbox" value="8" name="DMECollection" <%= Model.HasValue("8").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection8">Walker</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection9" type="checkbox" value="9" name="DMECollection" <%= Model.HasValue("9").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection9">Wheelchair</label>
					</div>
				</li>
				<li class="option">
					<div class="wrapper">
						<input id="<%= Model.IdPrefix %>_DMECollection10" type="checkbox" value="10" name="DMECollection" <%= Model.HasValue("10").ToChecked() %> />
						<label for="<%= Model.IdPrefix %>_DMECollection10">Other</label>
					</div>
					<div class="more">
						<label for="<%= Model.IdPrefix %>_OtherDME" class="fl">Specify</label>
						<div class="fr"><%= Html.TextBox("OtherDME", Model.OtherValue, new { @id = Model.IdPrefix + "_OtherDME" })%></div>
						<div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</fieldset>

