﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ArrayIdViewData>" %>
<fieldset>
    <legend><span class="green">(M0140)</span> Race/Ethnicity<span class="required-red">*</span></legend>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup two-wide">
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceAmericanIndian" <%= Model.HasValue("0").ToChecked() %> type="checkbox" value="0" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceAmericanIndian">American Indian or Alaska Native</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceAsian" <%= Model.HasValue("1").ToChecked() %> type="checkbox" value="1" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceAsian">Asian</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceBlack" <%= Model.HasValue("2").ToChecked() %> type="checkbox" value="2" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceBlack">Black or African-American</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceHispanic" <%= Model.HasValue("3").ToChecked() %> type="checkbox" value="3" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceHispanic">Hispanic or Latino</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceHawaiian" <%= Model.HasValue("4").ToChecked() %> type="checkbox" value="4" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceHawaiian">Native Hawaiian or Pacific Islander</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceWhite" <%= Model.HasValue("5").ToChecked() %> type="checkbox" value="5" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceWhite">White</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id="<%= Model.IdPrefix %>_RaceUnknown" <%= Model.HasValue("6").ToChecked() %> type="checkbox" value="6" name="EthnicRaces" class="required" />
                        <label for="<%= Model.IdPrefix %>_RaceUnknown">Unknown</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>
