﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientInsuranceInfoViewData>" %>
<%  var action = string.Format("{0}_{1}{2}", Model.ActionType, Model.IdPrefix, Model.InsuranceType); %>
<div class="sub row">
    <label for="<%= action %>HealthPlanId" class="fl">Health Plan Id</label>
    <div class="fr"><%= Html.TextBox(string.Format("{0}{1}HealthPlanId", Model.NamePrefix, Model.InsuranceType), Model.HealthPlanId, new { @id = action + "HealthPlanId", @class = "healthplanid required short more" })%></div>
</div>
<div class="sub row">
    <label for="<%= action %>GroupName" class="fl">Group Name</label>
    <div class="fr"><%= Html.TextBox(string.Format("{0}{1}GroupName", Model.NamePrefix, Model.InsuranceType), Model.GroupName, new { @id = action + "GroupName", @class = "short more" })%></div>
</div>
<div class="sub row">
    <label for="<%= action %>GroupId" class="fl">Group Id</label>
    <div class="fr"><%= Html.TextBox(string.Format("{0}{1}GroupId", Model.NamePrefix, Model.InsuranceType), Model.GroupId, new { @id = action + "GroupId", @class = "short more" })%></div>
</div>
<div class="sub row">
    <label for="<%= action %>Relationship" class="fl">Relationship to Patient</label>
    <div class="fr"><%= Html.InsuranceRelationships(string.Format("{0}{1}Relationship", Model.NamePrefix, Model.InsuranceType), Model.Relationship, new { @id = action + "Relationship", @class = "short" })%></div>
</div>