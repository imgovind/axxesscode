﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdvancedArrayIdViewData>" %>
<fieldset>
    <legend>Services Required <span class="light">(Optional)</span></legend>
    <input type="hidden" value="" name="ServicesRequiredCollection" />
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup <%= Model.CheckGroupWidth %>">
                <li class="option">
                    <div class="wrapper">
                        <input id ="<%= Model.IdPrefix %>_ServicesRequiredCollection0" type="checkbox" value="0" name="ServicesRequiredCollection" <%= Model.HasValue("0").ToChecked() %> />
                        <label for="<%= Model.IdPrefix %>_ServicesRequiredCollection0">SN</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id ="<%= Model.IdPrefix %>_ServicesRequiredCollection1" type="checkbox" value="1" name="ServicesRequiredCollection" <%= Model.HasValue("1").ToChecked() %> />
                        <label for="<%= Model.IdPrefix %>_ServicesRequiredCollection1">HHA</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id ="<%= Model.IdPrefix %>_ServicesRequiredCollection2" type="checkbox" value="2" name="ServicesRequiredCollection" <%= Model.HasValue("2").ToChecked() %> />
                        <label for="<%= Model.IdPrefix %>_ServicesRequiredCollection2">PT</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id ="<%= Model.IdPrefix %>_ServicesRequiredCollection3" type="checkbox" value="3" name="ServicesRequiredCollection" <%= Model.HasValue("3").ToChecked() %> />
                        <label for="<%= Model.IdPrefix %>_ServicesRequiredCollection3">OT</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id ="<%= Model.IdPrefix %>_ServicesRequiredCollection4" type="checkbox" value="4" name="ServicesRequiredCollection" <%= Model.HasValue("4").ToChecked() %> />
                        <label for="<%= Model.IdPrefix %>_ServicesRequiredCollection4">ST</label>
                    </div>
                </li>
                <li class="option">
                    <div class="wrapper">
                        <input id ="<%= Model.IdPrefix %>_ServicesRequiredCollection5" type="checkbox" value="5" name="ServicesRequiredCollection" <%= Model.HasValue("5").ToChecked() %> />
                        <label for="<%= Model.IdPrefix %>_ServicesRequiredCollection5">MSW</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>

