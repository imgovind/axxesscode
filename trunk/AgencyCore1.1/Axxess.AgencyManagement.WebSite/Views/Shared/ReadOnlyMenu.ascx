﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop({
    RemoteContent: <%= AppSettings.GetRemoteContent %>,
    Menu: [
        { Name: "Home", Id: "home", IconClass: "home-menu" },
        { Name: "My Account", Id: "account", Parent: "home" },
        { Name: "Create", Id: "create", IconClass: "create-menu" },
        { Name: "New", Id: "createnew", Parent: "create" },
        { Name: "Patients", Id: "patients", IconClass: "patients-menu" },
        { Name: "Help", Id: "help", IconClass: "help-menu" }
    ],
    MenuLinks: [
        { Name: "Training Manual", Href: '<%= string.Format("http://axxessweb.com/kb/?u={0}", SessionStore.SessionId) %>', Parent: "training" },
        { Name: "Discussion Forum", Href: "/Forum", Parent: "support" },
        { Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "support" },
        { Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "support" },
        { Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "help" },
    ],
    Windows: [
        // -- Home Menu -->
        { Name: "Edit Profile", Id: "EditProfile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "account", VersitileHeight: true },
        { Name: "Reset Signature", Id: "ForgotSignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "account", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Width: 500, VersitileHeight: true, MaxOnMobile: false },
        { Name: "Dashboard", MenuName: "My Dashboard", Id: "Dashboard", Url: "Home/Dashboard", OnLoad: Widgets.Init, Menu: "home", VersitileHeight: true },
        { Name: "My Messages", Id: "Inbox", Url: "Message/Inbox", OnLoad: Message.InitInbox, Menu: "home" },
        // -- Create Menu (Some Duplicated to Admin Menu) -->
        { Name: "Compose Message", MenuName: "Message", Id: "NewMessage", Url: "Message/New", OnLoad: Message.InitCompose, Menu: [ "createnew", "adminadd" ] },
        // -- Patient Menu -->
        { Name: "Patient Charts", Id: "PatientCharts", Url: "Patient/Charts/1/<%= Guid.Empty %>", OnLoad: Patient.Charts.Init, Input: { Service: "<%= 0 %>" }, Menu: "patients", MinHeight: 450 },
        // -- Help Menu -->
        { Name: "After-Hours Support", Id: "afterhourssupport", Url: "Agency/AfterHoursSupport", Menu: "help", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "300px", Width: "600px" },
        { Name: "Join GoToMeeting", Id: "GoToMeeting", Url: "User/GoToMeeting", Menu: "help" },
        { Name: "Recent Software Updates", Id: "RecentUpdates", Url: "Home/Updates", Menu: "help" },
        // -- Windows not found in the menus -->
        { Name: "Allergy Profile", Id: "allergyprofile", Url: "Patient/AllergyProfile" },
        { Name: "Medicare Eligibility Reports", Id: "medicareeligibilitylist", Url: "Patient/MedicareEligibilityList" },
        { Name: "Medication Profile SnapShot", Id: "medicationprofilesnapshot", Url: "Patient/MedicationProfileSnapShot", OnLoad: Medication.InitSnapshot },
        { Name: "Medication Profile", Id: "medicationprofile", Url: "Patient/MedicationHistoryView" },
        { Name: "Signed Medication Profiles", Id: "medicationprofilesnapshothistory", Url: "Patient/MedicationHistorySnapshotView" },
        { Name: "Authorization List", Id: "listauthorizations" },
        { Name: "Communication Notes", Id: "patientcommunicationnoteslist" },
        { Name: "Patient Orders History", Id: "patientordershistory" },
        { Name: "Patient 60 Day Summary", Id: "patientsixtydaysummary" },
        { Name: "Patient Directions", Id: "PatientMap", Url: "Patient/Map", OnLoad: Patient.InitMaps },
        { Name: "Patient Vital Signs Charts", Id: "PatientVitalSigns", Url: "Patient/VitalSignsCharts", OnLoad: Patient.VitalSigns.Init }
    ]
})
</script>

