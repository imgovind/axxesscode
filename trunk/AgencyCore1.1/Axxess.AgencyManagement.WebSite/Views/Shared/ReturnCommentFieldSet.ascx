﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NoteTopViewData>" %>
<%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
<fieldset class="return-alert">
    <div class="wide-column">
        <div class="row">
            <span class="img icon32 error fl"></span>
            <p>
                This document has been returned by a member of your QA Team.  Please review the reasons for the return and
                make appropriate changes.
            </p>
        </div>
         <ul class="buttons ac">
            <li class="red"><%= Html.ReturnCommentsLink(Model.Service, Model.Id, Model.EpisodeId, Model.PatientId, false)%></li>
        </ul>
    </div>
</fieldset>
<%  } %>