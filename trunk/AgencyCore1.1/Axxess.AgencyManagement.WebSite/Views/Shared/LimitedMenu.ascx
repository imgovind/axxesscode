﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop({
    RemoteContent: <%= AppSettings.GetRemoteContent %>,
    Menu: [
        { Name: "Home", Id: "home", IconClass: "home-menu" },
        { Name: "My Account", Id: "account", Parent: "home" },
        { Name: "Create", Id: "create", IconClass: "create-menu" },
        { Name: "New", Id: "createnew", Parent: "create" },
        { Name: "View", Id: "view", IconClass: "view-menu" },
<%  if (Current.HasRight(Permissions.ViewLists)) { %>
        { Name: "Lists", Id: "viewlist", Parent: "view" },
<%  } %>
        { Name: "Patients", Id: "patients", IconClass: "patients-menu" },
        { Name: "Help", Id: "help", IconClass: "help-menu" }
    ],
    MenuLinks: [
        { Name: "Training Manual", Href: '<%= string.Format("http://axxessweb.com/kb/?u={0}", SessionStore.SessionId) %>', Parent: "training" },
        { Name: "Discussion Forum", Href: "/Forum", Parent: "support" },
        { Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "support" },
        { Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "support" },
        { Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "help" },
    ],
    Windows: [
        // -- Home Menu -->
        { Name: "Edit Profile", Id: "EditProfile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "account", VersitileHeight: true },
        { Name: "Reset Signature", Id: "ForgotSignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "account", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Width: 500, VersitileHeight: true, MaxOnMobile: false },
        { Name: "Dashboard", MenuName: "My Dashboard", Id: "Dashboard", Url: "Home/Dashboard", OnLoad: Widgets.Init, Menu: "home", VersitileHeight: true },
        { Name: "My Messages", Id: "Inbox", Url: "Message/Inbox", OnLoad: Message.InitInbox, Menu: "home" },
        // -- Create Menu (Some Duplicated to Admin Menu) -->
        { Name: "New Referral", MenuName: "Referral", Id: "newreferral", Url: "Referral/New", OnLoad: Referral.InitNew, Menu: [ "createnew" ] },
        { Name: "Compose Message", MenuName: "Message", Id: "NewMessage", Url: "Message/New", OnLoad: Message.InitCompose, Menu: [ "createnew", "adminadd" ] },
        // -- View Menu (Some Duplicated to Admin Menu) -->
        { Name: "List Referrals", MenuName: "Referrals", Id: "listreferrals", Url: "Referral/List", Menu: [ "viewlist", "adminlist" ] },
        // -- Patient Menu -->
        { Name: "Existing Referrals", Id: "existingreferrals", Url: "Referral/List", Menu: "patients" ,OnLoad:U.ListInitClientRebind },
        // -- Help Menu -->
        { Name: "After-Hours Support", Id: "afterhourssupport", Url: "Agency/AfterHoursSupport", Menu: "help", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "300px", Width: "600px" },
        { Name: "Join GoToMeeting", Id: "GoToMeeting", Url: "User/GoToMeeting", Menu: "help" },
        { Name: "Recent Software Updates", Id: "RecentUpdates", Url: "Home/Updates", Menu: "help" },
        // -- Windows not found in the menus -->
        { Name: "Edit Referral", Id: "editreferral", OnLoad: Referral.InitEdit }
    ]
})
</script>

