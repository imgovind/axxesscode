﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MenuElement>>" %>
<%--<%  var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
    var jsonString = serializer.Serialize(Model);
    var json = Model.ToJson();  %>--%>
<script type="text/javascript">
	$("body").AcoreDesktop({
		ApplicationServices: <%= (int)Current.AcessibleServices %>,
		RemoteContent: <%= AppSettings.GetRemoteContent %>,
		ReadOnly: <%= Current.IsAgencyFrozen.ToString().ToLower() %>,
		Menu: [
			{ Name: "Home", Id: "home", IconClass: "home-menu" },
			<%  if (!Current.IsAgencyFrozen) { %>
			{ Name: "My Account", Id: "account", Parent: "home" },
			{ Name: "Home Health", Id: "HHhome", Parent: "home" },
			{ Name: "Private Duty", Id: "PDhome", Parent: "home" },
			{ Name: "Create", Id: "create", IconClass: "create-menu" },
			{ Name: "New", Id: "createnew", Parent: "create" },
			{ Name: "Home Health", Id: "HHcreatenew", Parent: "createnew" },
			{ Name: "Private Duty", Id: "PDcreatenew", Parent: "createnew" },
			<%  } %>
			{ Name: "View", Id: "view", IconClass: "view-menu" },
			{ Name: "Home Health", Id: "HHview", Parent: "view" },
			{ Name: "Orders Management", Id: "ordersmanagement", Parent: "view" },
			{ Name: "Lists", Id: "viewlist", Parent: "view" },
			{ Name: "Patients", Id: "patients", IconClass: "patients-menu" },
			{ Name: "Schedule", Id: "schedule", IconClass: "schedule-menu" },
			{ Name: "Home Health", Id: "HHschedule", Parent: "schedule" },
			{ Name: "Private Duty", Id: "PDschedule", Parent: "schedule" },
			{ Name: "Billing", Id: "billing", IconClass: "billing-menu" },
			{ Name: "Home Health", Id: "HHbilling", Parent: "billing" },
			{ Name: "Medicare/Medicare HMO", Id: "medicareclaim", Parent: "HHbilling" },
			{ Name: "Managed Care/Other Insurances", Id: "managedcareclaim", Parent: "HHbilling" },
			{ Name: "Private Duty", Id: "PDbilling", Parent: "billing" },
			{ Name: "Admin", Id: "admin", IconClass: "admin-menu" },
			<%  if (!Current.IsAgencyFrozen) { %>
			{ Name: "New", Id: "adminadd", Parent: "admin" },
			{ Name: "Home Health", Id: "HHadminadd", Parent: "adminadd" },
			{ Name: "Private Duty", Id: "PDadminadd", Parent: "adminadd" },
			<%  } %>
			{ Name: "Lists", Id: "adminlist", Parent: "admin" },
			{ Name: "Reports", Id: "reports", IconClass: "reports-menu" },
			{ Name: "Help", Id: "help", IconClass: "help-menu" },
			{ Name: "Training", Id: "training", Parent: "help" },
			{ Name: "Support", Id: "support", Parent: "help" }
		],
		MenuLinks: [
			{ Name: "Training Manual", Href: '<%= string.Format("http://axxessweb.com/kb/?u={0}", SessionStore.SessionId) %>', Parent: "training" },
			{ Name: "Free Webinars", Href: '<%= string.Format("http://axxessweb.com/webinars/?u={0}", SessionStore.SessionId) %>', Parent: "training" },
			{ Name: "Discussion Forum", Href: "/Forum", Parent: "support" },
			{ Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "support" },
			{ Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "support" },
			{ Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "help" },
			{ Name: "Follow us on LinkedIn", Href: "http://www.linkedin.com/company/axxess-consult", Parent: "help" },
		],
		Windows: [ <% for (int i = 0; i < Model.Count; i++) { %>
		   <%= Model[i].Json %><%= i + 1 < Model.Count ? "," : string.Empty %>
	    <% } %>
		]

})
</script>

