﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NoteTopViewData>" %>
<tr>
    <th colspan="<%= Model.ColWidth %>">
        <%= Model.Title %>
<%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
		<%= Html.ReturnCommentsLink(Model.Service, Model.Id, Model.EpisodeId, Model.PatientId, true)%>
<%  } %>
    </th>
</tr>
<%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
<tr>
    <td colspan="<%= Model.ColWidth %>" class="return-alert">
        <div>
            <span class="img icon32 error fl"></span>
            <p>
                This document has been returned by a member of your QA Team.  Please review the reasons for the return and
                make appropriate changes.
            </p>
            <ul class="buttons ac">
                <li class="red"><%= Html.ReturnCommentsLink(Model.Service, Model.Id, Model.EpisodeId, Model.PatientId, false)%></li>
            </ul>
        </div>
    </td>
</tr>
<%  } %>