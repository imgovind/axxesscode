﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Allergy>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateAllergy", "AllergyProfile", FormMethod.Post, new { @id = "editAllergyForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Allergy_Id" })%>
    <%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "Edit_Allergy_ProfileId" })%>
    <fieldset class="newallergy">
        <legend>Edit Allergy</legend>
        <div class="wide-column">
            <div class="row">
	            <label for="Edit_Allergy_Name" class="fl">Name</label>
	            <div class="fr"><%= Html.TextBox("Name", Model != null && Model.Name.IsNotNullOrEmpty() ? Model.Name : string.Empty, new { @id = "Edit_Allergy_Name", @class = "long required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
	            <label for="Edit_Allergy_Type" class="fl">Type</label>
	            <div class="fr">
		            <%= Html.TextBox("Type", Model != null && Model.Type.IsNotNullOrEmpty() ? Model.Type : string.Empty, new { @id = "Edit_Allergy_Type", @class = "fr", @maxlength = "50" })%>
		            <div class="clr"></div>
					<em>(e.g. Medication, Food, Animal, Plants, Environmental)</em>
	            </div>
            </div>
         </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>