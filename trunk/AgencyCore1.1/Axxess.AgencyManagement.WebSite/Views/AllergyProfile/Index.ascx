﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfileViewData>" %>
<span class="wintitle">Allergy Profile | <%= Model.PatientProfile.DisplayName%></span>
<div class="wrapper main note">
    <%= Html.Hidden("Id", Model.AllergyProfile.Id, new {@id = "allergyProfileId" }) %>
    <%= Html.Hidden("PatientId", Model.AllergyProfile.PatientId, new { @id = "AllergyProfile_PatientId" }) %>
	<fieldset>
		<legend>Allergy Profile</legend>
		<div class="column">
			<div class="row no-input">
				<label class="fl">Patient Name</label>
				<div class="fr"><%= Model.PatientProfile.DisplayName%></div>
			</div>
		</div>
		<div class="column">
			<div class="row no-input">
				<label class="fl">Physician</label>
				<div class="fr"><%= Model.PhysicianDisplayName%></div>
			</div>
		</div>
	</fieldset>
    <ul class="buttons fl ac">
<%  if (!Current.IfOnlyRole(AgencyRoles.Auditor) && Model.IsCanUserAdd) { %>
        <li><a class="link" onclick="Allergy.New('<%= Model.AllergyProfile.Id %>')">Add Allergy</a></li>
<%  } %>
        <li><a class="link" onclick="Allergy.Refresh('<%= Model.AllergyProfile.Id %>','AllergyProfile')">Refresh Allergies</a></li>
<%  if (Model.IsCanUserPrint) { %>
        <li><a class="link" onclick="Acore.OpenPrintView({ Url: '<%= ViewData.GetEnum<AgencyServices>("Service").ToArea() %>/AllergyProfile/Print/<%=Model.AllergyProfile.PatientId %>', PdfUrl: '<%= ViewData.GetEnum<AgencyServices>("Service").ToArea() %>/AllergyProfile/Pdf', PdfData: { 'id': '<%=Model.AllergyProfile.PatientId %>' } })">Print Allergy Profile</a></li>
<%  } %>
    </ul>
    <div class="clr"></div>
    <div id="AllergyProfile_AllergyProfileList"><% Html.RenderPartial("List", Model.AllergyProfile); %></div>
</div>