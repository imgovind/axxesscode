﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfile>" %>
<%  var allergies = Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies.ToObject<List<Allergy>>().OrderBy(a => a.Name).ToList() : new List<Allergy>(); %>

<div id="<%= Model.Type %>_active" class="acore-grid">
    <ul>
        <li class="ac"><h3>Active Allergies</h3></li>
        <li>
            <span class="grid-third">Name</span>
            <span class="grid-third">Type</span>
            <% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && (Model.IsUserCanEdit || Model.IsUserCanDelete)){ %>
            <span>Action</span>
			<% } %>
        </li>
    </ul>
<%  var activeAllergies = allergies.Where(a => !a.IsDeprecated).ToList(); %>
<%  if (activeAllergies.IsNotNullOrEmpty()) { %>
    <ol>
    <%  foreach (var allergy in activeAllergies) { %>
        <li>
            <span class="grid-third"><%= allergy.Name %></span>
            <span class="grid-third"><%= allergy.Type %></span>
            <span>
				<% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
					<% if (Model.IsUserCanEdit){%><a class="link" onclick="Allergy.Edit('<%=Model.Id %>','<%= allergy.Id %>','<%= Model.Type %>')">Edit</a><% } %>
					<% if (Model.IsUserCanDelete){%><a class="link" onclick="Allergy.Delete('<%=Model.Id %>','<%= allergy.Id %>','<%= Model.Type %>')">Delete</a><% } %>
				<% } %>
            </span>
        </li>
    <% } %>
    </ol>
<%  } else { %>
	<ol>
		<li class="no-hover">
			<h4>No Active Allergies</h4>
		</li>
	</ol>
<%  } %>
</div>
<div id="<%= Model.Type %>_inactive" class="acore-grid">
    <ul>
        <li class="ac"><h3>Deleted Allergies</h3></li>
        <li>
            <span class="grid-third">Name</span>
            <span class="grid-third">Type</span>
            <% if (!Current.IfOnlyRole(AgencyRoles.Auditor) && (Model.IsUserCanEdit || Model.IsUserCanRestore)) { %>
            <span>Action</span>
            <% } %>
        </li>
    </ul>
<%  var inactiveAllergies = allergies.Where(a => a.IsDeprecated).ToList(); %>
<%  if (inactiveAllergies.IsNotNullOrEmpty()) { %>
    <ol class="dc">
    <% foreach (var allergy in inactiveAllergies) { %>
        <li>
            <span class="grid-third"><%= allergy.Name %></span>
            <span class="grid-third"><%= allergy.Type %></span>
            <span>
				<% if (!Current.IfOnlyRole(AgencyRoles.Auditor)) { %>
					<% if (Model.IsUserCanEdit){ %><a class="link" onclick="Allergy.Edit('<%=Model.Id %>','<%= allergy.Id %>','<%= Model.Type %>')">Edit</a><% } %>
					<% if (Model.IsUserCanRestore){ %><a class="link" onclick="Allergy.Restore('<%=Model.Id %>','<%= allergy.Id %>','<%= Model.Type %>')">Restore</a><% } %>
				<%  } %>
            </span>
        </li>
    <%  } %>
    </ol>
<%  } else { %>
	<ol>
		<li class="no-hover">
			<h4>No Deleted Allergies</h4>
		</li>
	</ol>
<%  } %>
</div>