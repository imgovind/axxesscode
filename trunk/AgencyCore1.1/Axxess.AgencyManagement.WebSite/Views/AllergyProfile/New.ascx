﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddAllergy", "AllergyProfile", FormMethod.Post, new { @id = "newAllergyForm" })) { %>
    <%= Html.Hidden("ProfileId", Model, new { @default = Model.ToString(), @id = "New_Allergy_ProfileId" })%>
    <fieldset class="newallergy">
        <legend>New Allergy</legend>
        <div class="wide-column">
            <div class="row">
	            <label for="New_Allergy_Name" class="fl">Name</label>
				<div class="fr"><%= Html.TextBox("Name", "", new { @id = "New_Allergy_Name", @class = "long required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
	            <label for="New_Allergy_Type" class="fl">Type</label>
				<div class="fr">
					<%= Html.TextBox("Type", "", new { @id = "New_Allergy_Type", @class = "fr", @maxlength = "50" })%>
					<div class="clr"></div>
					<em>(e.g. Medication, Food, Animal, Plants, Environmental)</em>
				</div>
            </div>
         </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="save clear">Save &#38; Add Another</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>