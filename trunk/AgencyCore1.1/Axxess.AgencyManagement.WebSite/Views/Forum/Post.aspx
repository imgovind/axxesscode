<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Forum.Master" Inherits="System.Web.Mvc.ViewPage<OpenForum.Core.ViewModels.PostViewModel>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%= (Model.IncludeDefaultStyles) ? ForumViewHelper.GetDefaultStyles() : string.Empty %>
<%  if (Model.IncludeValidationSummary) Html.ValidationSummary(); %>
<%  Html.BeginForm(); %>
<label>Title</label>
<div><%= Html.TextBox("title", Model.Post.Title, new { @class = "openforum_textbox" }) %></div>
<label>Question</label>
<div id="wysiwyg"><%= Html.TextArea("body", Model.Post.Body, new { @class = "openforum_textarea" }) %></div>
<input id="submit" type="submit" value="Submit" class="hidden" />
<%= Html.Hidden("submit", "Submit") %>
<%= Html.Hidden("id", Model.Post.Id) %>
<ul class="post_controls">
    <li class="first"><a onclick="$('#body').val(CKEDITOR.instances['body'].getData());$('#submit').click()">Submit</a></li>
    <li><%= Html.ActionLink("Cancel", "Index") %></li>
</ul>
<%  Html.EndForm(); %>
<script type="text/javascript">
    var editor = CKEDITOR.replace('body', {
        resize_enabled: false,
        removePlugins: 'elementspath',
        uiColor: '#85a4d5',
        toolbar: [
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
            { name: 'color', items: ['TextColor', 'BGColor'] },
            { name: 'styles', items: ['Font', 'FontSize'] },
            { name: 'list', items: ['NumberedList', 'BulletedList', 'Outdent', 'Indent'] },
            { name: 'links', items: ['Link', 'Unlink'] },
            { name: 'spell', items: ['SpellChecker', 'Scayt'] }
        ]
    });
</script>
</asp:Content>