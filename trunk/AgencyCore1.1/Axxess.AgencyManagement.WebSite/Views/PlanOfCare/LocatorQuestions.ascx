﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCare>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] dME = data.AnswerArray("485DME"); %>
<%  string[] supplies = data.AnswerArray("485Supplies"); %>
<%  string[] safetyMeasure = data.AnswerArray("485SafetyMeasures"); %>
<%  string[] functionLimitations = data.AnswerArray("485FunctionLimitations"); %>
<%  string[] activitiesPermitted = data.AnswerArray("485ActivitiesPermitted"); %>
<%  string[] mentalStatus = data.AnswerArray("485MentalStatus"); %>
<fieldset>
	<legend>Medications (Locator #10): Dose/Frequency/Route (N)ew (C)hanged</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="buttons fl"><li><a id="Edit_485LatestMedications">Get Medications from Profile</a></li></ul>
		    <div class="fr">
		        <div class="ancillary-button"><a onclick="Medication.Profile('<%= Model.PatientId %>','<%= ViewData["Service"] %>')">Add/Edit Medications</a></div>
            </div>
		</div>
		<div class="row ac"><%= Html.TextArea("485Medications", data.ContainsKey("485Medications") && data["485Medications"].Answer.IsNotNullOrEmpty() ? data["485Medications"].Answer : string.Empty, new { @id = "Edit_485_Medications" })%></div>
	</div>
</fieldset>
<fieldset>
	<legend>Principal Diagnosis (Locator #11)/Other Pertinent Diagnosis (Locator #13)</legend>
	<ul class="oasis_diagnoses cms485_diagnoses"></ul>
</fieldset>
<fieldset>
	<legend>Surgical Procedure (Locator #12)</legend>
	<div class="column">
		<div class="row">
		    <label class="fl">Surgical Procedure</label>
		    <div class="fr">
                <%= Html.TextBox("485SurgicalProcedureDescription1", data.AnswerOrEmptyString("485SurgicalProcedureDescription1"), new { @class = "procedureDiagnosis short", @id = "Edit_485SurgicalProcedureDescription1", @placeholder = "Procedure" })%>
                <%= Html.TextBox("485SurgicalProcedureCode1", data.AnswerOrEmptyString("485SurgicalProcedureCode1"), new { @class = "procedureICD shorter", @id = "Edit_485SurgicalProcedureCode1", @placeholder = "ICD-9" })%>
                <br />
                <%= Html.DatePicker("485SurgicalProcedureCode1Date", data.AnswerOrMinDate("485SurgicalProcedureCode1Date"), false, new { id = "Edit_485_SurgicalProcedureCode1Date" })%>
            </div>
        </div>
    </div>
	<div class="column">
		<div class="row">
            <label class="fl">Surgical Procedure</label>
            <div class="fr">
                <%= Html.TextBox("485SurgicalProcedureDescription2", data.AnswerOrEmptyString("485SurgicalProcedureDescription2"), new { @class = "procedureDiagnosis short", @id = "Edit_485SurgicalProcedureDescription2", @placeholder = "Procedure" })%>
                <%= Html.TextBox("485SurgicalProcedureCode2", data.AnswerOrEmptyString("485SurgicalProcedureCode2"), new { @class = "procedureICD shorter", @id = "Edit_485SurgicalProcedureCode2", @placeholder = "ICD-9" })%>
                <br />
                <%= Html.DatePicker("485SurgicalProcedureCode2Date", data.AnswerOrMinDate("485SurgicalProcedureCode2Date"), false, new { id = "Edit_485SurgicalProcedureCode2Date" }) %>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
	<legend>DME and Supplies (Locator #14)</legend>
	<div class="wide-column">
		<div class="row">
			<label>DME</label>
            <input type="hidden" name="485DME" value="" />
            <div class="checkgroup four-wide">
                <%= Html.CheckgroupOption("485DME", "Edit_485DME1", "1", dME.Contains("1"), "Bedside Commode", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME2", "2", dME.Contains("2"), "Cane", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME3", "3", dME.Contains("3"), "Elevated Toilet Seat", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME4", "4", dME.Contains("4"), "Grab Bars", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME5", "5", dME.Contains("5"), "Hospital Bed", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME6", "6", dME.Contains("6"), "Nebulizer", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME7", "7", dME.Contains("7"), "Oxygen", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME8", "8", dME.Contains("8"), "Tub/Shower Bench", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME9", "9", dME.Contains("9"), "Walker", null)%>
                <%= Html.CheckgroupOption("485DME", "Edit_485DME10", "10", dME.Contains("10"), "Wheelchair", null)%>
            </div>
		</div>
		<div class="row">
			<label for="485DMEComments">Other</label>
			<div class="ac">
				<%= Html.TextArea("485DMEComments", data.ContainsKey("485DMEComments") ? data["485DMEComments"].Answer : "", new { @id = "Edit_485DMEComments" })%>
			</div>
		</div>
		<div class="row">
			<label>Supplies</label>
			<input type="hidden" name="485Supplies" value="" />
			<%  var supplyNames = new string[] {
			        "ABDs","Ace Wrap","Alcohol Pads","Chux/Underpads","Diabetic Supplies","Drainage Bag",
                    "Dressing Supplies","Duoderm","Exam Gloves","Foley Catheter","Gauze Pads","Insertion Kit",
                    "Irrigation Set","Irrigation Solution","Kerlix Rolls","Leg Bag","Needles","NG Tube","Probe Covers",
                    "Sharps Container","Sterile Gloves","Syringe","Tape" }; %>
			<%= Html.CheckgroupOptions("485Supplies", "Edit_485Supplies", supplyNames, supplies, null)%>
		</div>
		<div class="row">
            <label for="485SuppliesComment">Other</label>
            <div class="ac">
				<%= Html.TextArea("485SuppliesComment", data.ContainsKey("485SuppliesComment") ? data["485SuppliesComment"].Answer : "", new { @id = "Edit_485SuppliesComment", @style = "height: 90px; font-size: 16px;" })%>
			</div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Safety Measures (Locator #15)</legend>
	<div class="wide-column">
		<div class="row">
		    <%  var safetyNames = new string[] {
                    "Anticoagulant Precautions","Emergency Plan Developed","Fall Precautions","Keep Pathway Clear",
			        "Keep Side Rails Up","Neutropenic Precautions","O2 Precautions","Proper Position During Meals",
                    "Safety in ADLs","Seizure Precautions","Sharps Safety","Slow Position Change",
                    "Standard Precautions/ Infection Control","Support During Transfer and Ambulation",
                    "Use of Assistive Devices","Instructed on safe utilities management","Instructed on mobility safety",
                    "Instructed on DME &#38; electrical safety","Instructed on sharps container",
                    "Instructed on medical gas","Instructed on disaster/ emergency plan","Instructed on safety measures",
                    "Instructed on proper handling of biohazard waste" }; %>
			<%= Html.CheckgroupOptions("485SafetyMeasures", "Edit_485SafetyMeasures", safetyNames, safetyMeasure, null) %>
		</div>
		<div class="row">
			<label for="485SafetyMeasuresComments">Other</label>
			<div class="ac"><%= Html.TextArea("485OtherSafetyMeasures", data.AnswerOrEmptyString("485OtherSafetyMeasures"), new { @id = "Edit_485OtherSafetyMeasures" })%></div>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Nutritional Req. (Locator #16)</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
				<%= Html.ToggleTemplates("485NutritionTemplates")%>
				<%= Html.TextArea("485NutritionalReqs", data.AnswerOrEmptyString("485NutritionalReqs"), new { @id = "Edit_485NutritionalReqs" })%>
			</div>
        </div>
    </div>
</fieldset>
<fieldset>
	<legend>Allergies (Locator #17)</legend>
    <div class="wide-column">
		<div class="row ac">
			<div class="button"><a id="Edit_485LatestAllergies">Get Allergies from Profile</a></div>
        </div>
		<div class="row ac">
		    <%= Html.TextArea("485AllergiesDescription", data.AnswerOrEmptyString("485AllergiesDescription").IsNotNullOrEmpty() ? data["485AllergiesDescription"].Answer : Model.Allergies, new { @id = "Edit_485AllergiesDescription" })%>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Functional Limitations (Locator #18.A)</legend>
	<div class="wide-column">
		<div class="row">
			<ul class="checkgroup">
				<input type="hidden" name="485FunctionLimitations" value="" />
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations1", "1", functionLimitations.Contains("1"), "Amputation", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations5", "5", functionLimitations.Contains("5"), "Paralysis", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations9", "9", functionLimitations.Contains("9"), "Legally Blind", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations2", "2", functionLimitations.Contains("2"), "Bowel/Bladder Incontinence", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations6", "6", functionLimitations.Contains("6"), "Endurance", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitationsA", "A", functionLimitations.Contains("A"), "Dyspnea with Minimal Exertion", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations3", "3", functionLimitations.Contains("3"), "Contracture", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations7", "7", functionLimitations.Contains("7"), "Ambulation", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations4", "4", functionLimitations.Contains("4"), "Hearing", null)%>
				<%= Html.CheckgroupOption("485FunctionLimitations", "Edit_485FunctionLimitations8", "8", functionLimitations.Contains("8"), "Speech", null)%>
			</ul>
		</div>
		<div class="row">
		    <ul class="checkgroup one-wide">
		        <li class="option">
		            <div class="wrapper">
                        <input id="Edit_485FunctionLimitationsB" name="485FunctionLimitations" value="B" type="checkbox" <%= functionLimitations.Contains("B").ToChecked() %> />
                        <label for="Edit_485FunctionLimitationsB">Other</label>
		            </div>
		            <div class="more">
		                <label for="">Specify</label>
		                <div class="ac"><%= Html.TextArea("485FunctionLimitationsOther", data.AnswerOrEmptyString("485FunctionLimitationsOther"), new { @id = "Edit_485FunctionLimitationsOther" })%></div>
		            </div>
		        </li>
		    </ul>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend>Activities Permitted (Locator #18.B)</legend>
    <div class="wide-column">
        <div class="row">
			<input type="hidden" name="485ActivitiesPermitted" value="" />
            <ul class="checkgroup">
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted1", "1", activitiesPermitted.Contains("1"), "Complete bed rest", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted3", "3", activitiesPermitted.Contains("3"), "Up as tolerated", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted5", "5", activitiesPermitted.Contains("5"), "Exercise prescribed", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted7", "7", activitiesPermitted.Contains("7"), "Independent at home", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted9", "9", activitiesPermitted.Contains("9"), "Cane", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermittedB", "B", activitiesPermitted.Contains("B"), "Walker", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted2", "2", activitiesPermitted.Contains("2"), "Bed rest with BRP", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted4", "4", activitiesPermitted.Contains("4"), "Transfer bed-chair", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted6", "6", activitiesPermitted.Contains("6"), "Partial weight bearing", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermitted8", "8", activitiesPermitted.Contains("8"), "Crutches", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermittedA", "A", activitiesPermitted.Contains("A"), "Wheelchair", null)%>
				<%= Html.CheckgroupOption("485ActivitiesPermitted", "Edit_485ActivitiesPermittedC", "C", activitiesPermitted.Contains("C"), "No Restrictions", null)%>
            </ul>
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <input id="Edit_485ActivitiesPermittedD" name="485ActivitiesPermitted" value="D" type="checkbox" <%= activitiesPermitted.Contains("D").ToChecked() %>/>
                        <label for="Edit_485ActivitiesPermittedD">Other</label>
                    </div>
                    <div class="more">
                        <label for="Edit_485ActivitiesPermittedOther">Specify</label>
                        <div class="ac"><%= Html.TextArea("485ActivitiesPermittedOther", data.AnswerOrEmptyString("485ActivitiesPermittedOther"), new { @id = "Edit_485ActivitiesPermittedOther" })%></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>
<fieldset>
	<legend>Mental Status (Locator #19)</legend>
    <div class="wide-column">
        <div class="row">
			<input type="hidden" name="485MentalStatus" value="" />
            <ul class="checkgroup">
			    <%= Html.CheckgroupOptions("485MentalStatus", "Edit_485MentalStatus", new string[] { "Oriented", "Comatose", "Forgetful", "Depressed", "Disoriented", "Lethargic", "Agitated" }, mentalStatus, null)%>
			</ul>
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <input id="Edit_485MentalStatus8" name="485MentalStatus" value="8" type="checkbox" <%= mentalStatus.Contains("8").ToChecked() %> />
                        <label for="Edit_485MentalStatus8">Other</label>
                    </div>
                    <div class="more">
                        <label for="Edit_485MentalStatusOther">Other</label>
                        <div class="ac"><%= Html.TextArea("485MentalStatusOther", data.AnswerOrEmptyString("485MentalStatusOther"), new { @id = "Edit_485MentalStatusOther" })%></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Prognosis (Locator #20)</legend>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup five-wide">
                <%  var prognosis = data.AnswerOrEmptyString("485Prognosis"); %>
                <%= Html.CheckgroupOption("485Prognosis", "Edit_485PrognosisGuarded", "Guarded", prognosis == "Guarded", true, "Guarded", null, null) %>
                <%= Html.CheckgroupOption("485Prognosis", "Edit_485PrognosisPoor", "Poor", prognosis == "Poor", true, "Poor", null, null) %>
                <%= Html.CheckgroupOption("485Prognosis", "Edit_485PrognosisFair", "Fair", prognosis == "Fair", true, "Fair", null, null) %>
                <%= Html.CheckgroupOption("485Prognosis", "Edit_485PrognosisGood", "Good", prognosis == "Good", true, "Good", null, null) %>
                <%= Html.CheckgroupOption("485Prognosis", "Edit_485PrognosisExcellent", "Excellent", prognosis == "Excellent", true, "Excellent", null, null) %>
            </ul>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Orders for Discipline &#38; Treatments (Specify Amount/Frequency/Duration) (#21)</legend>
    <div class="wide-column">
        <div class="row">
            <div class="template-text">
				<%= Html.ToggleTemplates("485InterventionsTemplates") %>
				<%= Html.TextArea("485Interventions", data.AnswerOrEmptyString("485Interventions").Unclean(), new { @id = "Edit_485Interventions" }) %>
			</div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Goals/Rehabilitation Potential/Discharge Plans (#22)</legend>
    <div class="wide-column">
        <div class="row">
			<div class="template-text">
				<%= Html.ToggleTemplates("485GoalsTemplates")%>
				<%= Html.TextArea("485Goals", data.AnswerOrEmptyString("485Goals").Unclean(), new { @id = "Edit_485Goals" })%>
			</div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Electronic Signature(#23)</legend>
	<div class="column">
        <div class="row">
            <label for="Edit_485ClinicianSignature" class="fl">Staff Signature</label>
            <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "Edit_485ClinicianSignature" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <% Model.SignatureDate = DateTime.Today; %>
            <label for="Edit_485SignatureDate" class="fl">Date</label>
            <div class="fr"><input type="text" class="date-picker" name="SignatureDate" value="<%= DateTime.Today.ToZeroFilled() %>" id="Edit_485SignatureDate" /></div>
        </div>
    </div>         
    <div class="column">
	    <div class="row">
			<% if(!Model.IsStandAlone){ %>
			<label for="Edit_485VerbalSocDate" class="fl">Verbal SOC Date</label>
			<div class="fr"><input type="text" class="date-picker" name="M0102PhysicianOrderedDate" value="<%= data.AnswerOrEmptyString("M0102PhysicianOrderedDate") %>" id="Edit_485VerbalSocDate" /></div>
			<% } %>
		</div>
    </div>
</fieldset>
<script type="text/javascript">
    $("#window_HCFA485 .oasis_diagnoses").DiagnosesList({
    <%  if (data != null && data.ContainsKey("M1020PrimaryDiagnosis") && data["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) { %>
        _M1020PrimaryDiagnosis: "<%= data["M1020PrimaryDiagnosis"].Answer.EscapeApostrophe() %>",
        _M1020ICD9M: "<%= data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer.EscapeApostrophe() : "" %>",
        _485ExacerbationOrOnsetPrimaryDiagnosis: "<%= data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer.EscapeApostrophe() : "" %>",
        _M1020PrimaryDiagnosisDate: "<%= data.ContainsKey("M1020PrimaryDiagnosisDate") ? data["M1020PrimaryDiagnosisDate"].Answer.EscapeApostrophe() : "" %>",
    <%  } %>
    <%  for (int i = 1; i < 26; i++) { %>
        <%  if (data != null && data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty()) { %>
        _M1022PrimaryDiagnosis<%= i %>: "<%= data["M1022PrimaryDiagnosis" + i].Answer.EscapeApostrophe() %>",
        _M1022ICD9M<%= i %>: "<%= data.ContainsKey("M1022ICD9M" + i) ? data["M1022ICD9M" + i].Answer.EscapeApostrophe() : "" %>",
        _485ExacerbationOrOnsetPrimaryDiagnosis<%= i %>: "<%= data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.EscapeApostrophe() : "" %>",
        _M1022PrimaryDiagnosis<%= i %>Date: "<%= data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? data["M1022PrimaryDiagnosis" + i + "Date"].Answer.EscapeApostrophe() : "" %>",
    <%  } %>
<%  } %>
        Assessment: "Edit485"
    })
</script>