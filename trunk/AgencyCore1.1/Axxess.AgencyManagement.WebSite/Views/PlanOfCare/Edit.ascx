﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCareViewData>" %>
<%if(Model!=null && Model.PlanOfCare!=null){ %>
<span class="wintitle">Edit <%= Model.PlanOfCare.IsStandAlone ? "Plan of Treatment/Care" : (Model.PlanOfCare.IsNonOasis ? "Non-OASIS Plan of Care" : "485 - Plan of Care (From Assessment)")%> | <%= Model.PatientData.Get("PatientName") %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Save", "PlanOfCare", FormMethod.Post, new { @id = "edit485Form" })) { %>
    <%= Html.Hidden("Id", Model.PlanOfCare.Id, new { @id = "Edit_485_Id" })%>
    <%= Html.Hidden("EpisodeId", Model.PlanOfCare.EpisodeId, new { @id = "Edit_485_EpisodeId" })%>
    <%= Html.Hidden("PatientId", Model.PlanOfCare.PatientId, new { @id = "Edit_485_PatientId" })%>
    <%  if (Model.IsUserCanSeeSticky) { %>
	<%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.PlanOfCare.Id, Model.PlanOfCare.PatientId, Model.PlanOfCare.EpisodeId, Model.PlanOfCare.StatusComment, Model.Service))%>
	<%  } %>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Patient</legend>
				<div class="wide-column">
				    <div class="row no-input">
				        <label class="fl">Name</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientName") %></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Policy Number</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientPolicyNumber")%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Gender</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientGender")%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Date of Birth</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientDoB")%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Start of Care Date</label>
                        <div class="fr"><%= Model.PlanOfCare.SOC.IsValid() ? Model.PlanOfCare.SOC.ToZeroFilled() : string.Empty %></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Medical Record No</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientIdNumber")%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Certifcation Period</label>
                        <div class="fr"><%= Html.PatientEpisodes((int)Model.Service, "GenericEpisodeAssociated", Model.PlanOfCare.EpisodeId.ToString(), Model.PlanOfCare.PatientId, new { @id = "Edit_485_EpisodeList", @class = "required not-zero" })%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Address</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientAddressFirstRow")%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">&#160;</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientAddressSecondRow")%></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Phone</label>
                        <div class="fr"><%= Model.PatientData.Get("PatientPhone")%></div>
                    </div>
    <%  if (Model.IsUserCanEditPatient){ %>
                    <div class="row">
                        <div class="fr">
                            <div class="ancillary-button"><a onclick="Patient.Demographic.Edit.Open('<%= Model.PlanOfCare.PatientId %>','<%= Model.Service.ToArea() %>')">Edit Patient</a></div>
                        </div>
                    </div>
	<%  } %>
                </div>
			</fieldset>
		    <fieldset>
		        <legend>OASIS Refresh</legend>
		        <div class="wide-column">
	<%  if (!Model.PlanOfCare.IsStandAlone && Model.PlanOfCare.IsLinkedToAssessment) { %>
						<div class="row">
							Click on the button below to refresh this page from the OASIS Assessment associated with this episode.
						</div>
						<div class="row ac">
							<div class="button"><a id="Edit_485_RefreshData">Refresh Data</a></div>
						</div>
	<%  } else { %>
						<div class="row">
							This Plan of Care is not associated to an OASIS
						</div>
	<%  } %>
                </div>
            </fieldset>
		</div>
		<div>
			<fieldset>
                <legend>Agency</legend>
                    <div class="wide-column">
                        <div class="row no-input">
                            <label class="fl">Name</label>
                            <div class="fr"><%= Model.DisplayData.Get("AgencyName") %></div>
                        </div>
                        <div class="row no-input">
                            <label class="fl">Provider No</label>
                            <div class="fr"><%= Model.DisplayData.Get("AgencyMedicareProviderNumber") %></div>
                        </div>
                        <div class="row no-input">
                            <label class="fl">Address</label>
                            <div class="fr"><%= Model.DisplayData.Get("AgencyAddressFirstRow") %></div>
                        </div>
                        <div class="row no-input">
                            <label class="fl">&#160;</label>
                            <div class="fr"><%= Model.DisplayData.Get("AgencyAddressSecondRow") %></div>
                        </div>
                        <div class="row no-input">
                            <label class="fl">Phone</label>
                            <div class="fr"><%= Model.DisplayData.Get("AgencyPhone") %></div>
                        </div>
                        <div class="row no-input">
                            <label class="fl">Fax</label>
                            <div class="fr"><%= Model.DisplayData.Get("AgencyFax") %></div>
                        </div>
                    </div>
                </fieldset>
			<fieldset>
			    <legend>Physician</legend>
				<div class="wide-column">
				    <div class="row">
					    <label for="PhysicianId" class="fl">Select Physician</label>
						<div class="fr"><%= Html.TextBox("PhysicianId", Model.PlanOfCare.PhysicianId, new { @id = "Edit_485_PhysicianId", @class = "physician-picker required" })%></div>
						<div class="clr"></div>
    <%  if(Model.IsUserCanAddPhysicain) { %>
						<div class="fr ancillary-button al"><a onclick="Physician.New(true)">New Physician</a></div>
	<%  } %>
					</div>
				</div>
			</fieldset>
	<%  if (Model.IsUserCanLoadPrevious) { %>
            <fieldset>
                <legend>Duplicate from Previous</legend>
                <div class="wide-column">
                    <div class="row">
                        <label class="fl">Select Previous Plan of Care</label>
                        <div class="fr"><%= Html.PreviousCarePlans((int)Model.Service, Model.PlanOfCare.PatientId, Model.PlanOfCare.Id, (DisciplineTasks)Model.PlanOfCare.DisciplineTask, Model.PlanOfCare.Created, new { @id = "Previous_485" })%></div>
                    </div>
                    <div class="row ac">
                        <div class="button"><a id="Previous_485_Button">Load Previous Plan of Care</a></div>
                    </div>
                </div>
            </fieldset>
	<%  } %>
	    </div>
    </div> 
    <% ViewData["Service"] = Model.Service; %>
    <div id="planofCareContentId"><% Html.RenderPartial("LocatorQuestions", Model.PlanOfCare, ViewData); %></div>
    <%= Html.Hidden("Status", Model.PlanOfCare.Status, new { @id = "Edit_485_Status" })%>
    <%= Html.Hidden("SaveStatus", 110, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 115, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save">Save</a></li>
        <li><a class="save close">Save &#38; Close</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>
<script type="text/javascript">
    $("#Previous_485_Button").on("click", function() {
        var previousAssessmentArray = $("#Previous_485").val().split('_');
        if (previousAssessmentArray.length > 1) Acore.Confirm({
            Message: "Are you sure you want to load this plan of care?",
            Yes: function() {
                var episodeId = previousAssessmentArray[1],
                    patientId = $("#Edit_485_PatientId").val(),
                    eventId = previousAssessmentArray[0];
                $("#planofCareContentId").Load("PlanofCare/Content", {
                    episodeId: episodeId,
                    patientId: patientId,
                    eventId: eventId
                }, function(result) {
                    U.Growl("Loading of the 485 has completed", "success");
                })
            }
        });
        else U.Growl("Please select a previous assessment first", "error");
    });
</script>
<%} else{%>
<%} %>
