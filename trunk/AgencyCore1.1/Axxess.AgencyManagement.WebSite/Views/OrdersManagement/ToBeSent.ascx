﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OrderGridViewData>" %>
<span class="wintitle"> Orders To Be Sent | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  string pagename = "OrdersToBeSent"; %>
    <%  var area = Model.Service.ToArea();%>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%> 
    <ul class="fr buttons ac">
    <%  if (!Current.IsAgencyFrozen && Model.SentPermissions != AgencyServices.None) { %>
        <li><a area="<%= area %>" service="<%=(int)Model.SentPermissions %>" class="send-electronically servicepermission">Send Electronically</a></li><br />
    <%  } %>
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a area="<%= area %>" service="<%=(int)Model.ExportPermissions %>" url="/Export/OrdersToBeSent" class="export servicepermission">Excel Export</a></li><br />
    <%  } %>
        <li><a area="<%= area %>" url="/OrdersManagement/ToBeSentContent" class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter ignore-grid-binding"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pagename, new { @class = "service", @id = pagename + "_ServiceId" })%></div>
        <div class="filter">
            <label for="<%= pagename %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.AvailableService, new { @id = pagename + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_StartDate">Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-59), true, new { id = pagename + "_StartDate", @class = "short " })%>
            <label for="<%= pagename %>_EndDate">&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, true, new { id = pagename + "_EndDate", @class = "short" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_SendType">Filter by</label>
            <select id="<%= pagename %>_SendType" class="SendAutomatically" name="SendAutomatically">
                <option value="true">Electronic Orders</option>
                <option value="false">Manual Orders (Fax, Mail, etc)</option>
            </select>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
        <%  Html.Telerik().Grid<Order>().HtmlAttributes(new { @class = "grid-container aggregated" }).Name(pagename + "_Grid").Columns(columns => {
                columns.Bound(o => o.Type).Title(" ").ClientTemplate("<input type='checkbox' class='OrdersToBeSent' name='<#= Type #>' value='<#= Id #>_<#= PatientId #>_<#= EpisodeId #>' />").Width(35).Sortable(false).HeaderHtmlAttributes(new { @class = "checkbox", @service = (int)Model.SentPermissions }).Visible(!Current.IsAgencyFrozen && Model.SentPermissions != AgencyServices.None);
                columns.Bound(o => o.Number).Title("Order").Width(80);
                columns.Bound(o => o.PatientName).Title("Patient").Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
                columns.Bound(o => o.Text).Title("Type");
                columns.Bound(o => o.PhysicianName).Title("Physician");
                columns.Bound(o => o.CreatedDate).ClientTemplate("<#= U.FormatGridDate(CreatedDate) #>").Title("Order Date").Width(100);
                columns.Bound(o => o.Id).Title(" ").ClientTemplate("<# if (CanUserPrint) { #><a class='<#= DocumentType #> img icon16 print'></a><# } #>").Width(35).Sortable(false);
            }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("ToBeSentContent", "OrdersManagement", new { BranchId = Model.Id, SendAutomatically = true, StartDate = DateTime.Now.AddDays(-59).ToShortDateString(), EndDate = DateTime.Now.ToShortDateString() })).NoRecordsTemplate("No Orders found.").ClientEvents(events => events
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("Agency.OrdersManagement.OrderToBeSentOnDataBound")
                .OnRowDataBound("Agency.OrdersManagement.OrdersPrintableOnRowDataBound")
                .OnError("U.OnTGridError")
            ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Render(); %>
    </div>
</div>
<%  } %>