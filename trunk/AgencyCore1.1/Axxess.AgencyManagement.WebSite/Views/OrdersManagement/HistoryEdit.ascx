﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Edit", "OrdersManagement", FormMethod.Post, new { @id = "updateOrderHistry" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Update <%= Model.Text %></legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Sent Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="SendDate" value="<%= Model.SendDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.SendDate.ToShortDateString() %>" /></div>
            </div>
            <div class="row">
                <label class="fl">Received Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReceivedDate" value="<%= Model.ReceivedDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.ReceivedDate.ToShortDateString() %>" /></div>
            </div>
            <div class="row">
                <label class="fl">Physician Signature Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="PhysicianSignatureDate" value="<%= Model.PhysicianSignatureDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.PhysicianSignatureDate.ToShortDateString() %>" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>