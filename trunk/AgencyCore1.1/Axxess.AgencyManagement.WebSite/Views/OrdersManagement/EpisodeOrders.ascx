﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<span class="wintitle">Episode Orders | <%= Model.DisplayName %> | <%= Model.StartDate.ToShortDateString() %>&#8211;<%= Model.EndDate.ToShortDateString()%></span>
<div class="wrapper main blue">
	<%= Html.Hidden("PatientId", Model.PatientId, new { @id =  "EpisodeOrders_PatientId" })%>
	<%= Html.Hidden("EpisodeId", Model.Id, new { @id = "EpisodeOrders_EpisodeId" })%>
	<%= Html.Telerik().Grid<Order>().Name("EpisodeOrders_Grid").HtmlAttributes(new { @class = "bottom-bar" }).DataKeys(keys => keys.Add(o => o.Id).RouteKey("id")).Columns(columns => {
			columns.Bound(o => o.Number).Title("Order Number").Width(110).Sortable(false).ReadOnly();
			columns.Bound(o => o.Text).Title("Type").Sortable(false).ReadOnly();
			columns.Bound(o => o.StatusName).Title("Status").Sortable(false).ReadOnly();
			columns.Bound(o => o.PhysicianName).Title("Physician").Width(150).Sortable(false).ReadOnly();
			columns.Bound(o => o.CreatedDate).ClientTemplate("<#= U.FormatGridDate(CreatedDate) #>").Title("Order Date").Width(90).Sortable(true).ReadOnly();
			columns.Bound(o => o.SendDate).ClientTemplate("<#= U.FormatGridDate(SendDate) #>").Title("Sent Date").Width(90).Sortable(false);
			columns.Bound(o => o.ReceivedDate).ClientTemplate("<#= U.FormatGridDate(ReceivedDate) #>").Title("Received Date").Width(100).Sortable(false);
			columns.Bound(o => o.Id).ClientTemplate("<a class=\"img icon16 print\"/>").Title(" ").Width(30).Sortable(false);
		}).DataBinding(dataBinding => dataBinding.Ajax().Select("EpisodeOrdersContent", "OrdersManagement", new { episodeId = Model.Id, patientId = Model.PatientId })).ClientEvents(c => c
            .OnDataBinding("U.OnTGridDataBinding")
            .OnRowDataBound("Agency.OrdersManagement.OrdersPrintableOnRowDataBound")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
	    ).NoRecordsTemplate("No Orders found for this episode.").Scrollable() %>
</div>
