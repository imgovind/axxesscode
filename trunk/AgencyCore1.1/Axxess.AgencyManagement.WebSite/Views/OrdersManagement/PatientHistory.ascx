﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Patient Order History | <%= Model.DisplayName %></span>
<%  var pageName = "PatientOrderHistory"; %>
<%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%>  
	<ul class="buttons fr ac">
		<li><a class="grid-refresh" area="<%=area %>" url="/OrdersManagement/PatientHistoryGrid">Refresh</a></li><br />
<%  if (Model.ExportPermissions != AgencyServices.None){%>
		<li><a class="export servicepermission" service="<%=(int)Model.ExportPermissions %>"  area="<%=area %>" url="/Export/PatientOrderHistory">Excel Export</a></li>
<%  } %>
	</ul>
	<fieldset class="ac grid-controls">
		<div class="filter ignore-grid-binding">
		 <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" })%>
		</div>
		<div class="filter">
			<label for="<%= pageName%>_StartDate">Date Range</label>
			<%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-59), false, new { @id = pageName + "_StartDate", @class = "short" })%>
            <label>&#8211;</label>
			<%= Html.DatePicker("EndDate", DateTime.Now, false, new { @id = pageName + "_EndDate", @class = "short" })%>
		</div>
		<div class="filter"><%= Html.Hidden("PatientId", Model.Id, new { @id = pageName+"_PatientId" }) %></div>
	</fieldset>
    <%= Html.Telerik().Grid<Order>().Name(pageName + "_Grid").Columns(columns => {
            columns.Bound(o => o.Number).Title("Order Number").Width(110).Sortable(false);
            columns.Bound(o => o.Text).Title("Type").Sortable(false);
            columns.Bound(o => o.StatusName).Title("Status").Sortable(false);
            columns.Bound(o => o.PhysicianName).Title("Physician").Width(150).Sortable(false);
		    columns.Bound(o => o.PhysicianAccess).ClientTemplate("<# if (PhysicianAccess) #>Yes<# else #>No").Title("Electronic").Width(80);
            columns.Bound(o => o.CreatedDate).ClientTemplate("<#= U.FormatGridDate(CreatedDate) #>").Title("Order Date").Width(90);
            columns.Bound(o => o.SendDate).ClientTemplate("<#= U.FormatGridDate(SendDate) #>").Title("Sent Date").Width(90).Sortable(false);
            columns.Bound(o => o.ReceivedDate).ClientTemplate("<#= U.FormatGridDate(ReceivedDate) #>").Title("Received Date").Width(100).Sortable(false);
            columns.Bound(o => o.CanUserPrint).HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<# if (CanUserPrint && DocumentType) { #><a class='img icon16 print <#= DocumentType #>'></a><# } #>").Title(" ").Width(30).Sortable(false);
            columns.Bound(o => o.CanUserDelete).ClientTemplate("<# if (CanUserDelete) { #><a class='link delete' docType='<#= DocumentType #>'>Delete</a><# } #>").Title(" ").Width(50).Sortable(false);
        }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("PatientHistoryGrid", "OrdersManagement", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).ClientEvents(events => events
		    .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("Patient.Orders.OnDataBound")
            .OnRowDataBound("Patient.Orders.OnRowDataBound")
		    .OnError("U.OnTGridError")
        ).NoRecordsTemplate("No Orders found.").Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>