﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle"> Order History | <%= Current.AgencyName %></span>
<%  string pagename = "OrdersHistory"; %>
<%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue" >
    <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%> 
    <ul class="buttons fr">
<%  if (Model.ExportPermissions != AgencyServices.None)
    { %>
	    <li><a area="<%= area %>" service="<%=(int)Model.ExportPermissions %>"   url="/Export/OrdersHistory" class="export servicepermission">Excel Export</a></li><br />
<%  } %>
        <li><a area="<%= area %>" url="/OrdersManagement/HistoryList" class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
		 <div class="filter ignore-grid-binding">
            <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pagename, new { @class = "service input", @id = pagename + "_ServiceId" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename%>_BranchId">Branch</label>
          <%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.AvailableService, new { @id = pagename + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename%>_StartDate">Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-59), true, new { id = pagename + "_StartDate", @class = "short" })%>
            <label>&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, true, new { id = pagename + "_EndDate", @class = "short" })%>
        </div>
    </fieldset>    
    <div id="<%= pagename %>GridContainer">
		<%= Html.Telerik().Grid<Order>().Name(pagename + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
				columns.Bound(o => o.Number).Title("Order").Width(70).ReadOnly();
				columns.Bound(o => o.PatientName).Title("Patient").Width(155).ReadOnly().Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
				columns.Bound(o => o.Text).Width(145).Title("Type").ReadOnly();
				columns.Bound(o => o.PhysicianName).Title("Physician").Width(145).ReadOnly();
				columns.Bound(o => o.PhysicianAccess).ClientTemplate("<# if (PhysicianAccess) #>Yes<# else #>No").Title("Electronic").Width(65).ReadOnly();
				columns.Bound(o => o.CreatedDate).Width(80).ClientTemplate("<#= U.FormatGridDate(CreatedDate) #>").Title("Order Date").ReadOnly();
				columns.Bound(o => o.SendDate).Width(70).ClientTemplate("<#= U.FormatGridDate(SendDate) #>").Title("Sent Date");
				columns.Bound(o => o.ReceivedDate).Width(85).ClientTemplate("<#= U.FormatGridDate(ReceivedDate) #>").Title("Received Date");
				columns.Bound(o => o.PhysicianSignatureDate).Width(85).ClientTemplate("<#= U.FormatGridDate(PhysicianSignatureDate) #>").Title("MD Sign Date");
				columns.Bound(o => o.Id).Width(50).Sortable(false).ClientTemplate("<# if (CanUserEdit) #><a class=\"link\" onclick=\"Agency.OrdersManagement.EditOrder('<#=Id#>','<#=PatientId#>','<#=EpisodeId#>','<#=Type#>','<#=Service #>')\">Edit</a>").Title("Action").Hidden(Model.EditPermissions == AgencyServices.None).Visible(!Current.IsAgencyFrozen);
			}).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("HistoryList", "OrdersManagement", new {  BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).NoRecordsTemplate("No Orders found.").ClientEvents(events => events
				.OnDataBinding("U.OnTGridDataBinding")
				.OnDataBound("Agency.OrdersManagement.OrdersOnDataBound")
				.OnError("U.OnTGridError")
		    ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
	</div>
</div>
