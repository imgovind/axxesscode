﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("MarkAsReturned", "OrdersManagement", FormMethod.Post, new { area = Model.Service.ToArea(), @class = "mainform", @id = "receiveOrder" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Mark <%= Model.Text %> As Received</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Received Date</label>
                <div class="fr"><%= Html.DatePicker("ReceivedDate", Model.ReceivedDate <= DateTime.MinValue ? DateTime.Now : Model.ReceivedDate, true)  %></div>
            </div>
            <div class="row">
                <label class="fl">Physician Signature Date</label>
                <div class="fr"><%= Html.DatePicker("PhysicianSignatureDate", Model.PhysicianSignatureDate <= DateTime.MinValue ? DateTime.Now : Model.PhysicianSignatureDate, true)%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>