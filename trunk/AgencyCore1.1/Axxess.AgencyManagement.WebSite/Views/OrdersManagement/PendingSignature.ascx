﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OrderGridViewData>" %>
<span class="wintitle"> Orders Pending Signature | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  string pagename = "OrdersPendingSignature"; %>
    <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%> 
    <ul class="fr buttons ac">
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
	    <li><a area="<%= area %>" service="<%= (int)Model.ExportPermissions %>" url="/Export/OrdersPendingSignature" class="export servicepermission">Excel Export</a></li><br />
    <%  } %>
		<li><a area="<%= area %>" url="/OrdersManagement/PendingSignatureContent" class="grid-refresh">Refresh</a></li>
	</ul>
    <fieldset class="grid-controls ac">
        <div class="filter ignore-grid-binding"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pagename, new { @class = "service ", @id = pagename + "_ServiceId" })%></div>
        <div class="filter">
            <label for="<%= pagename %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.AvailableService, new { @id = pagename + "_BranchId" }) %>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_StartDate">Date Range</label>
            <%= Html.DatePicker("StartDate", DateTime.Now.AddDays(-59), true, new { id = pagename + "_StartDate", @class = "short" })%>
            <label for="<%= pagename %>_EndDate">&#8211;</label>
            <%= Html.DatePicker("EndDate", DateTime.Now, true, new { id = pagename + "_EndDate", @class = "short" })%>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
        <%= Html.Telerik().Grid<Order>().Name(pagename + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
                columns.Bound(o => o.Number).Title("Order").Width(70).ReadOnly();
                columns.Bound(o => o.PatientName).Title("Patient").Width(200).ReadOnly().Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
                columns.Bound(o => o.Text).Title("Type").ReadOnly();
                columns.Bound(o => o.PhysicianName).Title("Physician").ReadOnly();
                columns.Bound(o => o.CreatedDate).ClientTemplate("<#= U.FormatGridDate(CreatedDate) #>").Title("Order Date").Width(80).ReadOnly();
                columns.Bound(o => o.SendDate).ClientTemplate("<#= U.FormatGridDate(SendDate) #>").Title("Sent Date").Width(80).ReadOnly();
			    columns.Bound(o => o.Id).Width(100).Sortable(false).ClientTemplate("<# if (CanUserEdit) #><a class=\"link\" onclick=\"Agency.OrdersManagement.ReceiveOrder('<#= Id #>','<#= PatientId #>','<#= Type #>','<#= Service #>')\">Receive Order</a>").Title("Action").Visible(!Current.IsAgencyFrozen);
            }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("PendingSignatureContent", "OrdersManagement", new { BranchId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().ClientEvents(events => events
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("Agency.OrdersManagement.OrdersOnDataBound")
                .OnError("U.OnTGridError")
            ).NoRecordsTemplate("No Orders found.").Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
</div>
<%  } %>