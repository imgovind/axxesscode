﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimSnapShotViewData>" %>
<fieldset class="grid-controls">
	<legend>Secondary Claim</legend>
	<div class="column">
		<div class="row no-input">
		    <label class="fl">Medicare Number</label>
		    <div class="fr"><%= Model.IsuranceIdNumber %></div>
		</div>
<%  if (Model.Visible) { %>
		<div class="row no-input">
		    <label class="fl">Insurance/Payer</label>
		    <div class="fr"><%= Model.PayorName %></div>
		</div>
		<div class="row no-input">
		    <label class="fl">Health Plan Id</label>
		    <div class="fr"><%= Model.HealthPlainId %></div>
		</div>
		<div class="row no-input">
		    <label class="fl">Authorization Number</label>
		    <div class="fr"><%= Model.AuthorizationNumber %></div>
		</div>
		<div class="row no-input">
		    <label class="fl">Billed Date</label>
		    <div class="fr"><%= Model.ClaimDateFormatted %></div>
		</div>
		<div class="row no-input">
		    <label class="fl">Payment Date</label>
		    <div class="fr"><%= Model.PaymentDateFormatted %></div>
		</div>
		<div class="row no-input">
		    <label class="fl">Claim</label>
		    <div class="fr">$<%= Model.ClaimAmount %></div>
		</div>
		<div class="row no-input">
		    <label class="fl">Payment</label>
		    <div class="fr">$<%= Model.PaymentAmount %></div>
		</div>
<%  } %>
    </div>
</fieldset>