﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimViewData>"%>
<span class="wintitle">Secondary Claims | <%= Model.PrimaryClaimData.PatientName + "(" + Model.PrimaryClaimData.PatientIdNumber + ")" %></span>
<div class="wrapper main blue">
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "SecondaryClaimHistory_PatientId", @class = "activity-input" })%>
    <%= Html.Hidden("PrimaryClaimId", Model.PrimaryClaimId, new { @id = "SecondaryClaimHistory_PrimaryClaimId", @class = "activity-input" })%>
	<div class="inline-fieldset two-wide ac">
	    <div>
	        <fieldset class="grid-controls">
	            <legend>Final</legend>
	            <%  Html.RenderPartial("Secondary/Center/PrimaryClaimInfo", Model.PrimaryClaimData); %>
	        </fieldset>
	    </div>
	</div>
    <div class="content"><% Html.RenderPartial("Secondary/Center/Claims", Model); %></div>   
</div>


