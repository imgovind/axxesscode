﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
 <div class="column">
<%  if (Model.Visible) { %>
    <div class="row no-input">
        <label class="fl">Medicare Number</label>
        <div class="fr"><%= Model.MedicareNumber %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Insurance/Payer</label>
        <div class="fr"><%= Model.PayorName %></div>
    </div>
    <div class="row no-input">
        <label class="fl">HIPPS</label>
        <div class="fr"><%= Model.HIPPS %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Claim Key</label>
        <div class="fr"><%= Model.ClaimKey %></div>
    </div>
    <div class="row no-input">
        <label class="fl">HHRG (Grouper)</label>
        <div class="fr"><%= Model.ProspectivePayment.Hhrg %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Labor Portion</label>
        <div class="fr"><%= Model.ProspectivePayment.LaborAmount %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Non-Labor</label>
        <div class="fr"><%= Model.ProspectivePayment.NonLaborAmount %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Supply Reimbursement</label>
        <div class="fr"><%= Model.ProspectivePayment.NonRoutineSuppliesAmount %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Episode Prospective Pay</label>
        <div class="fr"><%= Model.ProspectivePayment.TotalProspectiveAmount %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Claim Prospective Pay</label>
        <div class="fr"><%= Model.ProspectivePayment.ClaimAmount %></div>
    </div>
<%  } else { %>
    <div class="row no-input">
        <label class="fl">Patient Name</label>
        <div class="fr"><%= Model.PatientName %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Patient MRN</label>
        <div class="fr"><%= Model.PatientIdNumber %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Medicare Number</label>
        <div class="fr"><%= Model.MedicareNumber %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Insurance/Payer</label>
        <div class="fr"><%= Model.PayorName %></div>
    </div>
    <%  if (Model.Visible) { %>
    <div class="row no-input">
        <label class="fl">HIPPS</label>
        <div class="fr"><%= Model.HIPPS %></div>
    </div>
    <div class="row no-input">
        <label class="fl">Claim Key</label>
        <div class="fr"><%= Model.ClaimKey %></div>
    </div>
    <%  } %>
<%  } %>
</div>