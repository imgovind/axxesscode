﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimViewData>" %>
<%  var downLoad = " | <a onclick=\"Billing.Secondary.Claim.GeneratedSingle('<#=Id#>')\" class=\"img icon16 paperclip\"></a>"; %>
<%  var printUrl = "<# if(IsVisitVerified && IsInfoVerified && IsSupplyVerified && IsRemittanceVerified && IsInsuranceVerified){#> <a onclick=\"U.GetAttachment('/Billing/SecondaryClaimPdf', { patientId: '<#=PatientId#>', Id: '<#=Id#>' })\" class='img icon16 print'></a> " + downLoad + "  <#} else {#>   <#}#>"; %>
<%  var action = "<a onclick=\"Billing.Secondary.Claim.Edit('<#=Id#>')\" class='link'>Edit</a>&#160;|&#160;<a onclick=\"Billing.Secondary.Claim.Delete('<#=PatientId#>','<#=Id#>')\" class='link'>Delete</a>"; %>
<%= Html.Telerik().Grid<SecondaryClaimLean>().Name("SecondaryClaims_Grid").HtmlAttributes(new { @class = "activity-container scrollable" }).DataKeys(keys => {
        keys.Add(r => r.Id).RouteKey("Id");
        keys.Add(r => r.PatientId).RouteKey("PatientId");
    }).Columns(columns => {
        columns.Bound(p => p.Id).Hidden(true).HtmlAttributes(new { @class = "cid" });
        columns.Bound(p => p.PatientId).Hidden(true).HtmlAttributes(new { @class = "pid" });
        columns.Bound(o => o.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Secondary.Claim.Open('<#=Id#>')\">Open Claim</a>").Title("").Width(90);
        columns.Bound(o => o.ClaimRange).Title("Claim Range").Width(150).ReadOnly();
        columns.Bound(o => o.StatusName).Title("Status").Width(70);
        columns.Bound(p => p.Balance).Title("Balance").Format("{0:$#0.00;-$#0.00}").Width(90);
        columns.Bound(p => p.IsInfoVerified).Title("Details").ClientTemplate("<span class=\"img icon16 <#= IsInfoVerified ? 'check' : 'ecks' #>\">").Width(50);
        columns.Bound(p => p.IsInsuranceVerified).Title("Insurance").ClientTemplate("<span class=\"img icon16 <#= IsInsuranceVerified ? 'check' : 'ecks' #>\">").Width(75);
        columns.Bound(p => p.IsVisitVerified).Title("Visits").ClientTemplate("<span class=\"img icon16 <#= IsVisitVerified ? 'check' : 'ecks' #>\">").Width(50);
        columns.Bound(p => p.IsRemittanceVerified).Title("Remittance").ClientTemplate("<span class=\"img icon16 <#= IsRemittanceVerified ? 'check' : 'ecks' #>\">").Width(75);
        columns.Bound(p => p.IsSupplyVerified).Title("Supply").ClientTemplate("<span class=\"img icon16 <#= IsSupplyVerified ? 'check' : 'ecks' #>\">").Width(50);
        columns.Bound(p => p.Id).ClientTemplate(printUrl).Title("").Width(120).Encoded(false);
        columns.Bound(p => p.Id).ClientTemplate(action).Title("Action").Width(100).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("SecondaryClaims", "Billing", new { PatientId = Model.PatientId, PrimaryClaimId = Model.PrimaryClaimId })).ClientEvents(events => events
        .OnRowSelect("Billing.Secondary.Center.OnClaimRowSelected")
        .OnDataBound("Billing.Secondary.Center.OnCliamDataBound")
    ).Selectable().Scrollable().Footer(false) %>