﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaimViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("CreateSecondaryClaim", "Billing", FormMethod.Post, new { @id = "createSecondaryClaimForm" })){ %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("PrimaryClaimId", Model.PrimaryClaimId)%>
    <fieldset>
        <legend>Claim Information</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Episode</label>
                <div class="fr"> <%= Model.EpisodeRange %></div>
            </div>
            <div class="row">
                <label for="StartDate" class="fl">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartDate" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="StartDate" /></div>
            </div>
            <div class="row">
                <label for="EndDate" class="fl">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" value="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="EndDate" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add Secondary Claim</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>