﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateSecondaryClaimStatus", "Billing", FormMethod.Post, new { @id = "updateSecondaryClaimForm" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <fieldset>
        <legend>Update Claim Information</legend>
        <div class="column">
            <div class="row">
                <label for="Status" class="fl">Claim Status</label>
                <div class="fr">
                    <%  var status = new SelectList(new[] {
                            new SelectListItem { Value = "3000", Text = "Claim Created" },
                            new SelectListItem { Value = "3005", Text = "Claim Submitted" },
                            new SelectListItem { Value = "3010", Text = "Claim Rejected" },
                            new SelectListItem { Value = "3015", Text = "Payment Pending" },
                            new SelectListItem { Value = "3020", Text = "Claim Accepted/Processing" },
                            new SelectListItem { Value = "3025", Text = "Claim With Errors" },
                            new SelectListItem { Value = "3030", Text = "Paid Claim" },
                            new SelectListItem { Value = "3035", Text = "Cancelled Claim" }
                        }, "Value", "Text", Model.Status); %>
                    <%= Html.DropDownList("Status", status, new { @class = "status" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl">Billed Date</label>
                <div class="fr"><input type="text" name="ClaimDate" class="date-picker claimdate" value="<%= Model.ClaimDate <= DateTime.MinValue ? "" : Model.ClaimDate.ToShortDateString() %>" id="ClaimDate" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Payment" class="fl">Payment Amount</label>
                <div class="fr">$<%= Html.TextBox("Payment", Model.Payment > 0 ? Model.Payment.ToString() : string.Empty, new { })%></div>
            </div>
            <div class="row">
                <label for="PaymentDate" class="fl">Payment Date</label>
                <div class="fr"><input type="text" class="date-picker" name="PaymentDate" value="<%= Model.PaymentDate.Date <= DateTime.MinValue.Date ? "" : Model.PaymentDate.ToShortDateString() %>" /></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="Comment">Comments</label>
                <div class="ac"><%= Html.TextArea("Comment", Model.Comment)%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>

