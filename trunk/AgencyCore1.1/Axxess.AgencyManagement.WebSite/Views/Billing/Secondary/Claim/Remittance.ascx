﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimRemittanceViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SecondaryClaimRemittanceVerify", "Billing", FormMethod.Post, new { @id = "secondaryBillingRemittanceForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "SecondaryVerification_Id", @class = "input" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "SecondaryVerification_PatientId", @class = "input" })%>
    <fieldset>
        <legend>Remittance Information</legend>
        <div class="column-wide">
            <div class="row">
                <label>Remittance Date</label>
                <div class="fr"><%= Html.DatePicker("RemitDate", Model.RemitDate, true, new { id = "SecondaryClaim_RemitDate" }) %></div>
            </div>
            <div class="row">
                <label>Remittance Id</label>
                <div class="fr"><%= Html.TextBox("RemitId", Model.RemitId, new { @class = "required" }) %></div>
            </div>
            <div class="row">
                <label>Total Adjustment Amount</label>
                <div class="fr"><span class="totaladjustmentamount" id="Secondary_TotalAdjustmentAmount"><%= string.Format("{0:C}", Model.TotalAdjustmentAmount) %></span></div>
            </div>
        </div>
    </fieldset>
	<div class="acore-grid">
	<%  if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) { %>
	    <%  var billCategoryVisits = Model.BillVisitDatas[BillVisitCategory.Billable]; %>
	    <%	if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
	    <ul>
		    <li class="ac"><h3>Remittance Adjustments</h3></li>
			<li>
				<span class="grid-twentieth"></span>
				<span class="grid-fifth">Visit Type</span>
				<span class="grid-fifth">Scheduled Date</span>
				<span class="grid-fifth">Visit Date</span>
				<span class="grid-quarter"></span>
			</li>
		</ul>
            <%  int visitIndex = 0; %>
			<%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
			    <%  var visits = billCategoryVisits[disciplineVisitKey]; %>
				<%  if (visits != null && visits.Count > 0) { %>
		<ol>
			<li class="group-header no-hover">
				<label class="strong"><%= disciplineVisitKey.GetDescription() %></label>
			</li>
					<%  int i = 1; %>
					<%  foreach (var visit in visits) { %> 
			<li class="main-line-item">
				<span class="grid-twentieth"><%= i++ %>.</span>
				<span class="grid-fifth"><%= visit.PereferredName.IsNotNullOrEmpty() ? visit.PereferredName : visit.DisciplineTaskName %></span>
				<span class="grid-fifth"><%= visit.EventDate != DateTime.MinValue ? visit.EventDate.ToZeroFilled() : "" %></span>
				<span class="grid-fifth"><%= visit.VisitDate != DateTime.MinValue ? visit.VisitDate.ToZeroFilled() : (visit.EventDate != DateTime.MinValue ? visit.EventDate.ToZeroFilled() : "")%></span>
				<span class="grid-quarter"></span>
				<span class="adjustment-add-button fr t-icon t-add" val="<%= visitIndex %>"></span>
				        <%  if (visit.Adjustments != null && visit.Adjustments.Count > 0) { %>
			    <div class="acore-grid">
					<ul>
					    <li><h3>Visit Adjustments</h3></li>
					</ul>
					<ol>
					    <%= Html.Hidden("[" + (visitIndex) + "].Key", visit.EventId) %>
						    <%  for (int adjustmentCount = 0; adjustmentCount < visit.Adjustments.Count; adjustmentCount++) { %>
						<li class="adjustment-item <%= adjustmentCount + 1 == visit.Adjustments.Count ? "last" : string.Empty %>">
						    <span class="grid-quarter">
                                <label for="[<%= visitIndex %>].Value[<%= adjustmentCount %>].AdjGroup" class="strong">Group Code</label>
                                <%= Html.TextBox("[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjGroup", visit.Adjustments[adjustmentCount].AdjGroup, new { @class = "shorter required", id = "[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjGroup" })%>
                            </span>
							<span class="grid-quarter">
							    <label for="[<%= visitIndex %>].Value[<%= adjustmentCount %>].AdjData.AdjReason" class="strong">Reason Code</label>
								<%= Html.TextBox("[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjData.AdjReason", visit.Adjustments[adjustmentCount].AdjData.AdjReason, new { @class = "shorter required", id = "[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjData.AdjReason" })%>
							</span>
							<span class="grid-quarter">
							    <label for="[<%= visitIndex %>].Value[<%= adjustmentCount %>].AdjData.AdjAmount" class="strong">Amount </label>
								$<%= Html.TextBox("[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjData.AdjAmount", visit.Adjustments[adjustmentCount].AdjData.AdjAmount, new { @class = "shorter floatnum adjustment-amount required", id = "[" + (visitIndex) + "].Value[" + adjustmentCount + "].AdjData.AdjAmount" })%>
							</span>
							<span class="delete fr">
							    <div onclick="Billing.Secondary.Claim.DeleteAdjustment($(this).parent().parent())" class="t-icon t-delete"/>
							</span>
						</li>
						    <%  } %>
					</ol>
				</div>
					    <%  } else { %>
				<div class="acore-grid" style="display:none;">
					<ul>
					    <li><h3>Visit Adjustments</h3></li>
					</ul>
					<ol><%= Html.Hidden("[" + (visitIndex) + "].Key", visit.EventId)%></ol>
				</div>
					    <%  } %>
			</li>
			            <%  visitIndex++; %>
					<%  } %>
		</ol>
				<%  } %>
			<%  } %>
		<%  } %>
	<%  } %>
    </div>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
    <%  if (Model.IsInfoVerified && Model.IsInsuranceVerified && Model.IsVisitVerified) { %>
		<li><a class="save">Verify and Next</a></li>
    <%  } %>
    <%  if (Model.IsRemittanceVerified) { %>
		<li><a class="next">Next</a></li>
	<%  } %>
    </ul>
<%  } %>
</div>