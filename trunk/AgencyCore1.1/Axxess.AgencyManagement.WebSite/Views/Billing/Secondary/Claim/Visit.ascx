﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SecondaryClaimVisitVerify", "Billing", FormMethod.Post, new { @id = "secondaryBillingVisitForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "SecondaryVerification_Id", @class = "input"})%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "SecondaryVerification_PatientId", @class = "input" })%>
    <div class="acore-grid selectable">
        <h3 class="ac">Date Range: <%= Model.StartDate.ToZeroFilled()%> &#8211; <%= Model.EndDate.ToZeroFilled()%></h3>
    <%  if (Model != null && Model.BillVisitDatas.IsNotNullOrEmpty()) { %>
        <%  foreach (var billCategoryKey in Model.BillVisitDatas.Keys) { %>
            <%  var billCategoryVisits = Model.BillVisitDatas[billCategoryKey]; %>
            <%  var isBillable = billCategoryKey == BillVisitCategory.Billable; %>
            <%  if (billCategoryVisits.IsNotNullOrEmpty() ) { %>
		<ul>
			<li class="ac">
				<h3><%= billCategoryKey.GetDescription()%></h3>
			</li>
			<li>
				<span class="grid-twentieth"></span>
				<span class="grid-sixth">Visit Type</span>
				<span class="grid-seventh">Scheduled Date</span>
				<span class="grid-tenth">Visit Date</span>
				<span class="grid-eleventh">HCPCS</span>
				<span class="grid-tenth">Modifiers</span>
				<span class="grid-sixth">Status</span>
				<span class="grid-eleventh">Units</span>
				<span class="grid-eleventh">Charge</span>
			</li>
		</ul>
                <%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
                    <%  var visits = billCategoryVisits[disciplineVisitKey]; %>
                    <%  if (visits != null && visits.Count > 0) { %>
		<ol>
			<li class="group-header no-hover">
				<label class="strong"><%= disciplineVisitKey.GetDescription() %></label>
			</li>
						<%  int i = 1; %>
						<%  foreach (var visit in visits) { %> 
			<li class="main-line-item <%= !visit.EventId.IsEmpty() && !visit.Adjustments.IsNotNullOrEmpty() ? "" : "" %>">
				<span class="grid-twentieth">
							<%  if (!visit.IsExtraTime) { %>
					<input class="<%= visit.UnderlyingVisits.IsNotNullOrEmpty() ? "day-per-line" : "" %>" name="Visit" type="checkbox" id="<%= visit.ViewId  + "_" + billCategoryKey.GetCustomShortDescription()%>" value="<%= visit.EventId.ToString() %>" <%= isBillable.ToChecked() %> />
					<%= i++ %>.
							<%  } %>
			    </span>
			    <span class="grid-sixth"><%= visit.PereferredName.IsNotNullOrEmpty() ? visit.PereferredName : visit.DisciplineTaskName %></span>
			    <span class="grid-seventh"><%= visit.EventDate.IsValid() ? visit.EventDate.ToString("MM/dd/yyy") : "" %></span>
			    <span class="grid-tenth"><%= visit.VisitDate.IsValid() ? visit.VisitDate.ToString("MM/dd/yyy") : (visit.EventDate.IsValid() ? visit.EventDate.ToString("MM/dd/yyy") : "") %></span>
			    <span class="grid-eleventh"><%= visit.HCPCSCode %></span>
			    <span class="grid-tenth"><%= visit.Modifier %>&nbsp;<%= visit.Modifier2 %>&nbsp;<%= visit.Modifier3 %>&nbsp;<%= visit.Modifier4 %></span>
			    <span class="grid-sixth"><%= visit.StatusName %></span>
			    <span class="grid-eleventh"><%= visit.Unit %></span>
			    <span class="grid-eleventh"><%= string.Format("{0:$#0.00;-$#0.00}", visit.Charge) %></span>
							<%  if (visit.UnderlyingVisits != null && visit.UnderlyingVisits.Count > 1) { %>
			<ul>
			    <h3>Underlying Visits</h3>
			</ul>
			<ol>
										<% int i2 = 1; %> 
								<%  foreach (var visit2 in visit.UnderlyingVisits) { %> 
				<li class="notready">
					<span class="grid-twentieth">
									<%  if (!visit2.IsExtraTime) { %>
						<input name="Visit" type="checkbox" id="<%= visit2.ViewId %>" value="<%= isBillable ? visit2.EventId.ToString() + "\" checked=\"checked"  : visit2.EventId.ToString() %>" />
						<%= i2++ %>. 
									<%  } %>
					</span>
					<span class="grid-sixth"><%= visit2.PereferredName.IsNotNullOrEmpty() ? visit2.PereferredName : visit2.DisciplineTaskName%></span>
					<span class="grid-seventh"><%= visit2.EventDate.IsValid() ? visit2.EventDate.ToString("MM/dd/yyy") : ""%></span>
					<span class="grid-tenth"><%= visit2.VisitDate.IsValid() ? visit2.VisitDate.ToString("MM/dd/yyy") : (visit2.EventDate.IsValid() ? visit2.EventDate.ToString("MM/dd/yyy") : "")%></span>
					<span class="grid-eleventh"><%= visit2.HCPCSCode%></span>
					<span class="grid-tenth"><%= visit2.Modifier%>&nbsp;<%= visit2.Modifier2%>&nbsp;<%= visit2.Modifier3%>&nbsp;<%= visit2.Modifier4%></span>
					<span class="grid-sixth"><%= visit2.StatusName%></span>
					<span class="grid-eleventh"><%= visit2.Unit%></span>
					<span class="grid-eleventh"><%= string.Format("{0:$#0.00;-$#0.00}", visit2.Charge)%></span>
				</li>
								<%  } %>
			</ol>
							<%  } %>
						<%  if (visit.Adjustments.IsNotNullOrEmpty()) { %>
			<div class="acore-grid">
				<ul>
					<li><h3>Adjustments</h3></li>
				</ul>
				<ol>
							<%  foreach (var adjustment in visit.Adjustments) { %> 
					<li>
						<span class="grid-third">
							<label class="strong">Group Code</label>
						    <label><%= adjustment.AdjGroup%></label>
						</span>
						<span class="grid-third">
							<label class="strong">Reason Code</label>
							<label><%= adjustment.AdjData.AdjReason %></label>
						</span>
						<span class="grid-third">
							<label class="strong">Amount</label>
							<label><%= String.Format("{0:C}", adjustment.AdjData.AdjAmount.ToDouble()) %></label>
						</span>
                    </li>
							<%  } %>
				</ol>
			</div>
						<%  } %>
		</li>
					<%  } %>
    </ol>
                    <%  } %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </div>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
    <%  if(Model.IsInfoVerified && Model.IsInsuranceVerified){ %>
		<li><a class="save">Verify and Next</a></li>
    <%  } %>
    <%  if(Model.IsVisitVerified) { %>
		<li><a class="next">Next</a></li>
    <%  } %>
    </ul>
<%  } %>
</div>
