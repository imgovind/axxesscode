﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SecondaryClaimInsuranceVerify", "Billing", FormMethod.Post, new { @id = "secondaryBillingInsuranceForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "SecondaryVerification_Id", @class = "input" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "SecondaryVerification_PatientId", @class = "input" })%>
    <%= Html.Hidden("ServiceId", (int)AgencyServices.HomeHealth, new { @id = "SecondaryVerification_ServiceId", @class = "input" })%>
    <%= Html.Hidden("TypeId", (int)ClaimTypeSubCategory.Secondary, new { @id = "SecondaryVerification_TypeId", @class = "input" })%>
    <%  if (Model.SecondaryInsuranceId >= 1000) { %>
    <fieldset>
	    <legend>Insurance Details</legend>
		<div id="Secondary_InsuranceContent" class="primaryinsurancecontent"><% Html.RenderPartial("InsuranceInfoContent", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Relationship = Model.Relationship, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "Secondary" }); %></div>  
	</fieldset>
	<%  } %>
    <div class="clr"></div>
	<%  if (Model.InvoiceType == (int)InvoiceType.UB) { %>
	    <%  Html.RenderPartial("Locator/UB04AllLocator", Model, new ViewDataDictionary { { "IdPrefix", "Secondary" } }); %>
	<%  } else if (Model.InvoiceType == (int)InvoiceType.HCFA) { %>
		<%  Html.RenderPartial("Locator/HCFALocators", Model.LocatorHCFA ?? new List<Locator>(), new ViewDataDictionary { { "IdPrefix", "Secondary" } }); %>
	<%  } %>
	<fieldset>
	    <legend>Visit Rates</legend>
		<div class="wide-column">
			<div class="row">
				<div class="button fl"><a class="reloadrates">Reload Visit Rates</a></div>
				<div class="ancillary-button fr"><a class="newrate">Add Visit Information</a></div>
			</div>
			<div class="row">
	<%  var billData = Model.VisitRates ?? new List<ChargeRate>(); %>
			    <%= Html.Telerik().Grid<ChargeRate>(billData).HtmlAttributes(new { @class = string.Format("position-relative {0}-{1} claim-rates", Model.Service.ToString().ToLowerCase(), "secondary") }).Name("SecondaryBillRate_Grid").Columns(columns => {
					    columns.Bound(e => e.DisciplineTaskName).Title("Task");
						columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
						columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(55);
						columns.Bound(e => e.Code).Title("HCPCS").Width(50);
						columns.Bound(e => e.Charge).Format("{0:$#0.00;-$#0.00}").Title("Rate").Width(45);
						columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
						if (Model.SecondaryInsuranceId >= 1000) columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
						columns.Bound(e => e.TimeLimit).Template(t => t.IsTimeLimit && t.ChargeType != "1" ? t.TimeLimit.ToHourMinTime() : string.Empty).ClientTemplate("<#= IsTimeLimit && TimeLimitHour != 0 && TimeLimitMin != 0 && ChargeType != \"1\" ? $.telerik.formatString('{0:H:mm}', TimeLimit) : '' #>").Title("Time Limit").Width(65);
						columns.Bound(e => e.Id).Template(t => "<a class=\"edit link\">Edit</a> | <a class=\"delete link\">Delete</a>").ClientTemplate("<a class=\"edit link\">Edit</a> | <a class=\"delete link\">Delete</a>").HtmlAttributes(new { @class = "action" }).Title("Action").Width(95).Sortable(false);
						columns.Bound(e => e.Id).Hidden(true).HtmlAttributes(new { @class = "rid" });
					}).DataBinding(dataBinding => dataBinding.Ajax().Select("SecondaryClaimInsuranceRates", "Billing", new { Id = Model.Id }).OperationMode(GridOperationMode.Client)).ClientEvents(events => events
                        .OnDataBound("Billing.Rate.OnDataBound")
			        ).Sortable().Footer(false) %>
			</div>
		</div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
    <%  if (Model.IsInfoVerified) { %>
		<li><a class="save">Verify and Next</a></li>
    <%  } %>
    <%  if (Model.IsInsuranceVerified) { %>
		<li><a class="next">Next</a></li>
    <%  } %>
    </ul>
<%  } %>
</div>
<%  var disabledTabs = new string[] { Model.IsInsuranceVerified ? string.Empty : "2", Model.IsVisitVerified ? string.Empty : "3", Model.IsRemittanceVerified ? string.Empty : "4", Model.IsSupplyVerified ? string.Empty : "5" }; %>
<script type="text/javascript">
	$("#SecondaryVerification_TabStrip").tabs("option", "disabled", [ <%= disabledTabs.Join(", ") %> ])
</script>