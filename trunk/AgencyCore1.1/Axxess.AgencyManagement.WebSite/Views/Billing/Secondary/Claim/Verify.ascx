﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SecondaryClaim>" %>
<span class="wintitle">Secondary Claims | <%= Model.DisplayName %></span>
<div id="SecondaryVerification_TabStrip" class="wrapper blue horizontal-tabs main maintab">
    <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
    <%= Html.Hidden("ServiceId",(int) AgencyServices.HomeHealth, new { @class = "tabinput" })%>
    <%= Html.Hidden("Type", ClaimTypeSubCategory.Secondary, new { @class = "tabinput" })%>
    <ul class="horizontal-tab-list">
	    <li class="ac">
	        <a href="/Billing/SecondaryClaimInfo" name="Info">
	            <strong>Step 1 of 6</strong>
	            <br />
	            <span>Demographics</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsInfoVerified ? "" : "disabled" %>">
	        <a href="/Billing/SecondaryClaimInsurance" name="Insurance">
	            <strong>Step 2 of 6</strong>
	            <br />
	            <span>Verify Insurance</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsInsuranceVerified || Model.IsVisitVerified ? "" : "disabled" %>">
	        <a href="/Billing/SecondaryClaimVisit" name="Visit">
	            <strong>Step 3 of 6</strong>
	            <br />
	            <span>Verify Visits</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsVisitVerified || Model.IsRemittanceVerified ? "" : "disabled" %>">
	        <a href="/Billing/SecondaryClaimRemittance" name="Remittance">
	            <strong>Step 4 of 6</strong>
	            <br />
	            <span>Verify Remittance</span>
	        </a>
	    </li>
	    
	    <li class="ac <%= Model.IsRemittanceVerified || Model.IsSupplyVerified ? "" : "disabled" %>">
	        <a href="/Billing/SecondaryClaimSupply" name="Supply">
	            <strong>Step 5 of 6</strong>
	            <br />
	            <span>Verify Supplies</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsSupplyVerified ? "" : "disabled" %>">
	        <a href="/Billing/SecondaryClaimSummary" name="Summary">
	            <strong>Step 6 of 6</strong>
	            <br />
	            <span>Summary</span>
	        </a>
	    </li>
    </ul>
   <%-- <div id="SecondaryVerification_Info" class="tab-content"><%  Html.RenderPartial("Secondary/Claim/Info", Model); %></div>
    <div id="SecondaryVerification_Insurance" class="tab-content"></div>
    <div id="SecondaryVerification_Visit" class="tab-content"></div>
    <div id="SecondaryVerification_Remittance" class="tab-content"></div>
    <div id="SecondaryVerification_Supply" class="tab-content"></div>
    <div id="SecondaryVerification_Summary" class="tab-content"></div>--%>
</div>