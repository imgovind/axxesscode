﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InsuranceAuthorizationViewData>" %>
 <div class="column">
    <div class="row"><label for="HealthPlanId" class="fl">Health Plan Id</label><div class="fr"><%= Html.TextBox("HealthPlanId", Model.HealthPlanId, new { })%></div></div>
    <div class="row"><label for="GroupName" class="fl">Group Name</label><div class="fr"><%= Html.TextBox("GroupName", Model.GroupName, new { })%></div></div>
    <div class="row"><label for="GroupId" class="fl">Group Id</label><div class="fr"><%= Html.TextBox("GroupId", Model.GroupId, new { })%></div></div>
    <div class="row" ><label for="Relationship" class="fl">Relationship to Patient</label><div class="fr"><%= Html.InsuranceRelationships("Relationship", Model.Relationship, new { })%></div></div>
</div>
<div class="column">
	<div class="row">
		<label for="Authorization" class="fl">Authorizations</label>
		<div class="fr"> <%= Html.DropDownList("Authorization", Model.Authorizations != null && Model.Authorizations.Count > 0 ? Model.Authorizations : new List<SelectListItem>(), new { @id = Model.ClaimTypeIdentifier + "_Authorization", @class = "authorization" })%></div>
	</div>
	<div id="<%=Model.ClaimTypeIdentifier %>_AuthorizationContent" class="authorizationcontent">
		<% Html.RenderPartial("AuthorizationContent", Model.Authorization ?? new Authorization()); %>
	</div>
</div>
