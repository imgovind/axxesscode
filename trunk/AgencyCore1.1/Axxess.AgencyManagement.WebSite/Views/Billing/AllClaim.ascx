﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Bill>>" %>
<span class="wintitle">All Insurances/Payors <%= Current.AgencyName %></span>
<%  var pageName = "Billing_AllClaimCenter";  %>
<div class="wrapper main">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" }) %>
    <%  var claimType = ViewData.GetEnum<ClaimTypeSubCategory>("ClaimType"); %>
    <%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
    <%  var insuranceId = ViewData.ContainsKey("InsuranceId") ? ViewData["InsuranceId"].ToString() : string.Empty; %>
    <%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
    <%  var area=service.ToArea(); %>
    <ul class="buttons fr top ac">
        <li><a class="print" url="<%=area %>/Billing/ClaimsPdf">Print</a></li><br />
        <li><a class="export" url="<%=area %>/Billing/ClaimsXls">Excel Export</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh" url="<%=area %>/Billing/AllClaimGrid">Refresh</a></div>
        <%= Html.Hidden("Service", (int)service, new { @class = "service", @id = pageName + "_Service" }) %> 
        <div class="filter">
            <label for="<%= pageName %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pageName + "_BranchId", @class = "location long" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_ClaimType">Bill Type</label>
            <%= Html.ClaimTypeSubCategories("ClaimType", claimType.ToString(), false, "", new { @id = pageName + "_ClaimType", @class = "claimtype" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_InsuranceId">Insurance</label>
            <%= Html.Insurances("InsuranceId", insuranceId,  (int)service,true, true, "All", new { @id = pageName + "_InsuranceId", @class = "insurance hybridinsurance not-zero long zeroprefix" })%>
        </div>
    </fieldset>
    <div id="<%= pageName %>GridContainer" style="min-height:200px"><% Html.RenderPartial("AllClaimContent", Model); %></div>
</div>
