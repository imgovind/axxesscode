﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Locator>>" %>
<% var locators = Model ?? new List<Locator>(); %>
<%var IdPrefix = ViewData["IdPrefix"];%>
<fieldset>
	<legend>Locator Information</legend>
	<div class="column">
		<div class="row">
			<label class="fl">HCFA 1500 Locator 33</label>
		</div>
		<div class="sub row">
			<label for="HCFALocators" class="fl">b.</label>
			<% var locator1 = locators.FirstOrDefault(l => l.LocatorId == "33Locatorb") ?? new Locator(); %>
			<%= Html.Hidden("LocatorHCFA[0].LocatorId", "33Locatorb")%>
			<div class="fr">
				<%= Html.TextBox("LocatorHCFA[0].Code1", locator1.Code1, new { @maxlength = "12" })%>
			</div>
		</div>
	</div>
</fieldset>
