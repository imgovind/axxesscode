﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Locator>>" %>
<div class="column">
	<div class="row">
		<label>UB04 Locator 81cc</label>
	</div>
	<% var IdPrefix = ViewData["IdPrefix"];%>
	<% for (int i = 0; i < 4; i++){%>
		<div class="sub row">
			<% var locator = Model.FirstOrDefault(l => l.LocatorId == "Locator" + (i + 1)) ?? new Locator(); %>
			<label for="Ub04Locator81ccb" class="fl"><%= (char)(i + 97)%>.</label>
			<%= Html.Hidden("Locator81cca[" + i + "].LocatorId", "Locator" + (i + 1))%>
			<div class="fr">
				<%= Html.TextBox("Locator81cca[" + i + "].Code1", locator.Code1, new { @id = IdPrefix + "_Locator" + (i + 1) + "_Code1", @class = "shorter less", @maxlength = "2" })%>
				<%= Html.TextBox("Locator81cca[" + i + "].Code2", locator.Code2, new { @id = IdPrefix + "_Locator" + (i + 1) + "_Code2", @class = "short less", @maxlength = "10" })%>
				<%= Html.TextBox("Locator81cca[" + i + "].Code3", locator.Code3, new { @id = IdPrefix + "_Locator" + (i + 1) + "_Code3", @class = "short", @maxlength = "12" })%>
			</div>
		</div>
	<% } %>
</div>
