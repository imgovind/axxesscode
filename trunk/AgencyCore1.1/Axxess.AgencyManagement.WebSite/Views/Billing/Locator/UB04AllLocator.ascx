﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BaseClaim>" %>
<%var IdPrefix = ViewData["IdPrefix"];%>
<fieldset>
	<legend>Locator Information</legend>
	<% Html.RenderPartial("Locator/Ub04Locator", Model.Locator31 ?? new List<Locator>(), new ViewDataDictionary { { "LocatorTypeId", 31 }, { "IdPrefix", IdPrefix } }); %>
	<% Html.RenderPartial("Locator/Ub04Locator", Model.Locator32 ?? new List<Locator>(), new ViewDataDictionary { { "LocatorTypeId", 32 }, { "IdPrefix", IdPrefix } }); %>
	<% Html.RenderPartial("Locator/Ub04Locator", Model.Locator33 ?? new List<Locator>(), new ViewDataDictionary { { "LocatorTypeId", 33 }, { "IdPrefix", IdPrefix } }); %>
	<% Html.RenderPartial("Locator/Ub04Locator", Model.Locator34 ?? new List<Locator>(), new ViewDataDictionary { { "LocatorTypeId", 34 }, { "IdPrefix", IdPrefix } }); %>
	<% Html.RenderPartial("Locator/Ub04Locator39", Model.Locator39 ?? new List<Locator>(), new ViewDataDictionary { { "IdPrefix", IdPrefix } }); %>
	<% Html.RenderPartial("Locator/Ub04Locator81ccb", Model.Locator81cca ?? new List<Locator>(), new ViewDataDictionary { { "IdPrefix", IdPrefix } }); %>
</fieldset>
