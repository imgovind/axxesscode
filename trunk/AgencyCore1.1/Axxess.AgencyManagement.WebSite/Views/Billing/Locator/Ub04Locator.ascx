﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Locator>>" %>
<%var locatorTypeId = ViewData["LocatorTypeId"];%>
<%var IdPrefix = ViewData["IdPrefix"];%>
<% var locators = Model ?? new List<Locator>(); %>
<div class="column">
	<div class="row">
		<label class="fl">Ub04 Locator <%=locatorTypeId %></label>
	</div>
	<div class="sub row narrow">
		<label for="Ub04Locator<%=locatorTypeId %>" class="fl">a.</label>
		<% var locator1 = locators.FirstOrDefault(l => l.LocatorId == locatorTypeId + "Locator1") ?? new Locator(); %>
		<%= Html.Hidden("Locator" + locatorTypeId + "[0].LocatorId", locatorTypeId + "Locator1", new { @class = "Locator" + locatorTypeId + "_0_LocatorId" })%>
		<div class="fr">
			<%= Html.TextBox("Locator" + locatorTypeId + "[0].Code1", locator1.Code1, new { @id = IdPrefix + "_" + locatorTypeId + "Locator1_Code1", @class = "short", @maxlength = "3" })%>
			<%= Html.TextBox("Locator" + locatorTypeId + "[0].Code2", locator1.Code2, new { @id = IdPrefix + "_" + locatorTypeId + "Locator1_Code2", @class = "short date-picker", @maxlength = "10" })%>
		</div>
	</div>
	<div class="sub row narrow">
		<label for="Ub04Locator<%=locatorTypeId %>" class="fl">b.</label>
		<% var locator2 = locators.FirstOrDefault(l => l.LocatorId == locatorTypeId + "Locator2") ?? new Locator(); %>
		<%= Html.Hidden("Locator" + locatorTypeId + "[1].LocatorId", locatorTypeId + "Locator2", new { @class = "Locator" + locatorTypeId + "_1_LocatorId" })%>
		<div class="fr">
			<%= Html.TextBox("Locator" + locatorTypeId + "[1].Code1", locator2.Code1, new { @id = IdPrefix + "_" + locatorTypeId + "Locator2_Code1", @class = "short", @maxlength = "3" })%>
			<%= Html.TextBox("Locator" + locatorTypeId + "[1].Code2", locator2.Code2, new { @id = IdPrefix + "_" + locatorTypeId + "Locator2_Code2", @class = "short date-picker", @maxlength = "10" })%>
		</div>
	</div>
</div>
