﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Locator>>" %>

<div class="column">
	<div class="row">
		<label>UB04 Locator 39</label>
	</div>
	<% var IdPrefix = ViewData["IdPrefix"];%>
	<% for (int i = 0; i < 4; i++){%>
		<div class="sub row">
			<% var locator = Model.FirstOrDefault(l => l.LocatorId == "39Locator" + (i + 1)) ?? new Locator(); %>
			<label for="Ub04Locator39" class="fl"><%= (char)(i + 97)%>.</label>
			<%= Html.Hidden("Locator39[" + i + "].LocatorId", "39Locator" + (i + 1))%>
			<div class="fr">
				<%= Html.TextBox("Locator39[" + i + "].Code1", locator.Code1, new { @id = IdPrefix + "_39Locator" + (i + 1) + "_Code1", @class = "shorter", @maxlength = "2" })%>
				<%= Html.TextBox("Locator39[" + i + "].Code2", locator.Code2, new { @id = IdPrefix + "_39Locator" + (i + 1) + "_Code2", @class = "short", @maxlength = "10" })%>
			</div>
		</div>
	<% } %>
</div>