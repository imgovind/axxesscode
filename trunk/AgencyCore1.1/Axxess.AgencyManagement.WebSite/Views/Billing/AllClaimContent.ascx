﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Bill>>" %>
<% foreach( var data in Model){ %>
	<% Html.RenderPartial(data.ClaimType != ClaimTypeSubCategory.ManagedCare ? "Medicare/" + data.ClaimType.ToString().ToTitleCase() + "Grid" : "Managed/ManagedGrid", data); %>
<% } %>