﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle"><%= Model.ClaimType.GetDescription() %> Claim Summary | <%= Current.AgencyName %></span>
<%  var IsManagedCare = Model.ClaimType == ClaimTypeSubCategory.ManagedCare; %>
<%  var area = Model.Service.ToArea(); %>
<%  var gridWidth = IsManagedCare ? "grid-third" : "grid-quarter"; %>
<%  var gridWidthMRN = IsManagedCare ? "grid-quarter" : "grid-eighth"; %>
<div class="wrapper main content">
	<%= Html.Hidden("StatusType", "Submit", new { }) %>
    <%= Html.Hidden("BranchId", Model.BranchId, new { }) %>
    <%= Html.Hidden("InsuranceId", Model.Insurance, new { }) %>
    <%= Html.Hidden("ClaimType", Model.ClaimType.ToString(), new { }) %>
    <div class="acore-grid">
	    <ul class="sortable">
		    <li class="ac">
				<h3><%= Model.BranchName + " | " + Model.InsuranceName %></h3>
		    </li>
		    <li>
                <span class="grid-twentieth"></span>
			    <span class="<%= gridWidth %>">Patient Name</span>
                <span class="<%= gridWidthMRN %>">MRN</span>
<%  if (!IsManagedCare) { %>
                <span class="grid-sixth">Medicare No</span>
<%  } %>
			    <span class="<%= gridWidth %>">Episode Range</span>
<%  if (!IsManagedCare) { %>
                <span class="grid-eighth">Claim Amount</span>
<%  } %>
		    </li>
	    </ul>
	    <ol>
<%  int j = 1; %>
<%  foreach (var claim in Model.Claims) { %>
			<li>
			    <span class="grid-twentieth">
			        <%= Html.Hidden("ClaimSelected", claim.Id, new { id = "ClaimSelected" + claim.Id }) %>
			        <%= j %>.
			    </span>
			    <span class="<%= gridWidth %>"><%= claim.DisplayName %></span>
                <span class="<%= gridWidthMRN %>"><%= claim.PatientIdNumber %></span>
    <%  if (!IsManagedCare) { %>
				<span class="grid-sixth"><%= claim.MedicareNumber %></span>
	<%  } %>
	            <span class="<%= gridWidth %>"><%= claim.DateRange%></span>
	<%  if (!IsManagedCare) { %>
				<span class="grid-eighth"><%= claim.ProspectivePay %></span>
    <%  } %>
            </li>
    <%  j++; %>
<%  } %>
	    </ol>
    </div>
    <div class="clr"></div>
    <ul class="buttons ac">
<%  if (Model.IsElectronicSubmssion) { %>
        <li class="al"><a class="submit" url="<%= area%>/Billing/SubmitClaimDirectly">Submit Electronically</a></li>
<%  } else { %>
        <li class="al"><a class="download" url="<%= area%>/Billing/CreateANSI">Download Claim(s)</a></li>
<%  } %>
<%  if (!Model.IsElectronicSubmssion) { %>
        <li><a class="updatestatus" url="<%= area%>/Billing/MarkSubmitted">Mark Claim(s) As Submitted</a></li>
<%  } %>
    </ul>
</div>