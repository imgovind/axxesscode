﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Authorization>" %>
<div class="sub row">
	<label for="AuthorizationNumber" class="fl">1</label>
	<div class="fr"><%= Html.TextBox("AuthorizationNumber", Model.Number1, new { @maxlength = "30" })%></div>
</div>
<div class="sub row">
	<label for="AuthorizationNumber2" class="fl">2</label>
	<div class="fr"><%= Html.TextBox("AuthorizationNumber2", Model.Number2, new { @maxlength = "30" })%></div>
</div>
<div class="sub row">
	<label for="AuthorizationNumber3" class="fl">3</label>
	<div class="fr"><%= Html.TextBox("AuthorizationNumber3", Model.Number3, new { @maxlength = "30" })%></div>
</div>
