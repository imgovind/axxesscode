﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<EditBillDataViewData>" %>
<div class="wrapper main">
<%  var data = Model.ChargeRate ?? new ChargeRate(); %>
<%  using (Html.BeginForm((Model.TypeOfClaim == ClaimTypeSubCategory.ManagedCare ? "Managed" : Model.TypeOfClaim.ToString()) + "ClaimUpdateBillData", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "editInsuranceBillData", @class = "mainform" })) { %>
    <%= Html.Hidden("ClaimId", Model.ClaimId, new { @id = "Edit_BillData_ClaimId" })%>
    <%= Html.Hidden("PayorType", Model.PayorType, new { @id = "Edit_InsuranceBillData_PayorType" })%>
    <%= Html.Hidden("InsuranceId", data.InsuranceId, new { @id = "Edit_InsuranceBillData_InsuranceId" })%>
    <input type="hidden" id="Edit_BillData_Id" name="Id" class="task" value="<%= data.Id %>" />
    <%  Html.RenderPartial("~/Views/Rate/EditContent.ascx", data , new ViewDataDictionary { { "IdPrefix", "Edit_BillData" } }); %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
