﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewBillDataViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm((Model.TypeOfClaim == ClaimTypeSubCategory.ManagedCare ? "Managed" : Model.TypeOfClaim.ToString()) + "ClaimAddBillData", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newInsuranceBillData", @class = "mainform" })) { %>
    <%= Html.Hidden("ClaimId", Model.ClaimId, new { @id = "New_InsuranceBillData_ClaimId" })%>
    <%= Html.Hidden("InsuranceId", Model.InsuranceId, new { @id = "New_InsuranceBillData_InsuranceId" })%>
    <%= Html.Hidden("PayorType", Model.PayorType, new { @id = "New_InsuranceBillData_PayorType" })%>
    <%  Html.RenderPartial("~/Views/Rate/NewContent.ascx", new ChargeRate { InsuranceId = Model.InsuranceId, IsMedicareHMO = Model.IsMedicareHMO, IsTraditionalMedicare = Model.IsTraditionalMedicare, ExistingNotes = Model.ExistingNotes }, new ViewDataDictionary { { "IdPrefix", "New_BillData" } }); %>
    <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
