﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimEditSupplyViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SupplyBillableUpdate", "Billing", FormMethod.Post, new {@class=Model.Supply.IsBillable? "billable":"unbillable", @id = "edit" + Model.Type + "ClaimSupplyForm" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id)%>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <%= Html.Hidden("UniqueIdentifier", Model.Supply.UniqueIdentifier)%>
    <%= Html.Hidden("BillingId", Model.Supply.BillingId)%>
    <%= Html.Hidden("SupplyListIdentifier", string.Format("{0}{1}{2}", Model.Type.ToLowerCase(), Model.Service.ToString().ToLowerCase(), Model.Supply.IsBillable ? "billable" : "unbillable"))%>
    <fieldset>
        <legend>Edit Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", Model.Supply.Description, new { @class = "required description", @style = "width:320px;" })%></div>
            </div>
            <div class="row">
                <label class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", Model.Supply.RevenueCode, new { @class = "required revenuecode" })%></div>
            </div>
            <div class="row">
                <label class="fl">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Supply.Code, new { @class = "required code" })%></div>
            </div>
            <div class="row">
                <label class="fl">Date</label>
                <div class="fr"><%= Html.TextBox("Date", Model.Supply.Date, new { @class = "date-picker required" })%></div>
            </div>
            <div class="row">
                <label class="fl">Unit</label>
                <div class="fr"><%= Html.TextBox("Quantity", Model.Supply.Quantity, new { @class = "required numeric quantity" })%></div>
            </div>
            <div class="row">
                <label class="fl">Unit Cost</label>
                <div class="fr">$<%= Html.TextBox("UnitCost", Model.Supply.UnitCost, new { @class = "required floatnum unitcost" })%></div>
            </div>
            <div class="row">
                <label class="fl">Total Cost</label>
                <div class="fr">$<%= Html.TextBox("TotalCost", Model.Supply.TotalCost, new { @class = "totalcost", @id = "EditSupply_TotalCost", disabled = "disabled" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>