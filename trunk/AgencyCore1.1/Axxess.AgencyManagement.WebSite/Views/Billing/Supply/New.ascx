﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimNewSupplyViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SupplyBillableAdd", "Billing", FormMethod.Post, new { @id = "new" + Model.Type + "ClaimSupplyForm" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id)%>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <%= Html.Hidden("UniqueIdentifier", Guid.Empty)%>
    <%= Html.Hidden("SupplyListIdentifier", string.Format("{0}{1}{2}", Model.Type.ToLowerCase(), Model.Service.ToString().ToLowerCase(), "billable" ))%>
    <fieldset>
        <legend>New Supply</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Description</label>
                <div class="fr"><%= Html.TextBox("Description", string.Empty, new { @class = "required long" })%></div>
            </div>
            <div class="row">
                <label class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", string.Empty, new { @class = "required" }) %></div>
            </div>
            <div class="row">
                <label class="fl">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", string.Empty, new { @class = "required" })%></div>
            </div>
            <div class="row">
                <label class="fl">Date</label>
                <div class="fr"><%= Html.TextBox("Date", string.Empty, new { @class = "date-picker required" })%></div>
            </div>
            <div class="row">
                <label class="fl">Unit</label>
                <div class="fr"><%= Html.TextBox("Quantity", string.Empty, new { @class = "required numeric" })%></div>
            </div>
            <div class="row">
                <label class="fl">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", string.Empty, new { @class = "required currency" })%></div>
            </div>
            <div class="row">
                <label class="fl">Total Cost</label>
                <div class="fr"><%= Html.TextBox("TotalCost", string.Empty, new { @class = "currency", @id = "NewSupply_TotalCost", disabled = "disabled" }) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add Supply</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>