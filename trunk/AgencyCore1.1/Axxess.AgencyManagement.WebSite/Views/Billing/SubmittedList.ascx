﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Claim Submission History | <%= Current.AgencyName %></span>
<%  var pageName = "SubmittedClaims";  %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
	<div class="button fr"><a url="Export/SubmittedBatchClaims" class="export">Excel Export</a></div>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh trebind">Refresh</a></div>
        <div class="filter">
			<label for="<%= pageName %>_ClaimType">Claim Type</label>
			<%= Html.ClaimTypes("ClaimType", new { @id = pageName + "_ClaimType" })%>
        </div>
        <div class="filter">
			<label for="<%= pageName %>_StartDate">Date Range</label>
			<input type="text" class="date-picker short" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" />
			<label for="<%= pageName %>_EndDate">&ndash;</label>
			<input type="text" class="date-picker short" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
        </div>
    </fieldset>
    <div id="<%= pageName %>GridContainer">
        <%= Html.Telerik().Grid<ClaimDataLean>().HtmlAttributes(new { @class = "grid-container" }).Name(pageName + "_Grid").Columns(columns => {
                columns.Bound(o => o.Id).Title("Batch Id").Width(80);
                columns.Bound(o => o.Created).Format("{0:MM/dd/yyyy}").Title("Submission Date").Width(120);
                columns.Bound(o => o.Count).Title("# of claims").Sortable(false);
                columns.Bound(o => o.RAPCount).Title("# of RAPs").Sortable(false);
                columns.Bound(o => o.FinalCount).Title("# of Finals").Sortable(false);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimSubmittedList", "Billing", new { ClaimType="ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }).OperationMode(GridOperationMode.Client)).ClientEvents(events => events
			    .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("U.OnTGridDataBound")
                .OnError("U.OnTGridError")
                .OnDetailViewCollapse("U.OnGridDetailViewCollapse")
                .OnDetailViewExpand("U.OnGridDetailViewExpand")
            ).NoRecordsTemplate("No Batches Found").Sortable().Scrollable().Footer(false).DetailView(details => details.ClientTemplate(
			    Html.Telerik().Grid<ClaimInfoDetail>().Name("SubmittedClaimDetails_<#= Id #>").HtmlAttributes(new { @class = "position-relative" }).ToolBar(commands => commands.Template("<h3>Claims</h3><div class='buttons fr'><ul><li><a href='Export/SubmittedClaimDetails/<#= Id #>'>Excel Export</a></li></ul></div>")).ClientEvents(events => events
				    .OnDataBound("U.OnTGridDataBound")
				    .OnDataBinding("U.OnTGridDataBinding")
                ).Columns(columns => {
				    columns.Bound(o => o.PatientIdNumber).Title("MRN").Width(2);
				    columns.Bound(o => o.DisplayName).Title("Patient").Width(3);
				    columns.Bound(o => o.Range).Title("Episode").Width(3);
				    columns.Bound(o => o.BillType).Width(2);
			    }).DataBinding(dataBinding => dataBinding.Ajax().Select("SubmittedClaimDetail", "Billing", new { Id = "<#= Id #>" })
            ).Footer(false).ToHtmlString())) %>
     </div>
</div>
