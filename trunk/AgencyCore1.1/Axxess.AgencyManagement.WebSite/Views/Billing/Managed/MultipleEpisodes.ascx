﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MultipleEpisodeViewData>" %>
<div class="wrapper">
<%  using (Html.BeginForm("ManagedClaimAssessmentData", "Billing", FormMethod.Post)) { %>
	<%= Html.Hidden("PatientId", Model.PatientId) %>
	<fieldset>
		<legend>Select Associated Episode</legend>
		<div class="wide-column">
			<span>To create this claim, you will need assessment, diagnosis and authorization information from an associated episode. Please select the episode from the list below to auto populate/pull the information.</span>
			<div class="row">
				<label for="Managed_MultipleEpisodes_EpisodeId" class="fl">Select Episode</label>
				<div class="fr"><%= Html.DropDownList("Episode", Model.EpisodeItems.AsEnumerable(), new { @id = "Managed_MultipleEpisodes_EpisodeId" }) %></div>
			</div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Select</a></li>
		<li><a class="close">Cancel</a></li>
	</ul>
<%  } %>
</div>