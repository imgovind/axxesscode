﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<string, string>>" %>
<% var service = Model.Get("Service").ToEnum(AgencyServices.None); %>
<span class="wintitle"><%= service.Has(AgencyServices.PrivateDuty) ? "Claim" : "Managed Claim"%> | <%= Model.Get("DisplayName") %></span>
<% var area = service.ToArea(); %>
<div id="<%=area %>ManagedVerification_TabStrip" class="wrapper blue horizontal-tabs main maintab">
    <%= Html.Hidden("Id", Model.Get("Id"), new { @class = "tabinput" })%>
    <%= Html.Hidden("PatientId", Model.Get("PatientId"), new { @class = "tabinput" })%>
    <%= Html.Hidden("ServiceId", (int)service, new { @class = "tabinput" })%>
    <%= Html.Hidden("Type", ClaimTypeSubCategory.ManagedCare, new { @class = "tabinput" })%>
    <ul class="horizontal-tab-list">
	    <li class="ac">
	        <a href="<%= area %>/Billing/ManagedClaimInfo" name="Info">
	            <strong>Step 1 of 5</strong>
	            <br />
	            <span>Demographics</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.Get("IsInfoVerified").ToBoolean() ? string.Empty : "disabled" %>">
	        <a href="<%= area %>/Billing/ManagedClaimInsurance" name="Insurance">
	            <strong>Step 2 of 5</strong>
	            <br />
	            <span>Verify Insurance</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.Get("IsInsuranceVerified").ToBoolean() ? string.Empty : "disabled" %>">
	        <a href="<%= area %>/Billing/ManagedClaimVisit" name="Visit">
	            <strong>Step 3 of 5</strong>
	            <br />
	            <span>Verify Visits</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.Get("IsVisitVerified").ToBoolean() ? string.Empty : "disabled" %>">
	        <a href="<%= area %>/Billing/ManagedClaimSupply" name="Supply">
	            <strong>Step 4 of 5</strong>
	            <br />
	            <span>Verify Supplies</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.Get("IsSupplyVerified").ToBoolean() ? string.Empty : "disabled" %>">
	        <a href="<%= area %>/Billing/ManagedClaimSummary" name="Summary">
	            <strong>Step 5 of 5</strong>
	            <br />
	            <span>Summary</span>
	        </a>
	    </li>
    </ul>
</div>