﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateManagedClaimStatus", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @class = "mainform", @id = "updateManagedClaimForm" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <fieldset>
        <legend>Update Claim Information</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Claim Status</label>
                <div class="fr">
                    <%  var status = new SelectList(new[] {
                            new SelectListItem { Value = "3000", Text = "Claim Created" },
                            new SelectListItem { Value = "3005", Text = "Claim Submitted" },
                            new SelectListItem { Value = "3010", Text = "Claim Rejected" },
                            new SelectListItem { Value = "3015", Text = "Payment Pending" },
                            new SelectListItem { Value = "3020", Text = "Claim Accepted/Processing" },
                            new SelectListItem { Value = "3025", Text = "Claim With Errors" },
                            new SelectListItem { Value = "3030", Text = "Paid Claim" },
                            new SelectListItem { Value = "3035", Text = "Cancelled Claim" }
                        }, "Value", "Text", Model.Status); %>
                    <%= Html.DropDownList("Status", status, new { @class = "status" }) %>
                </div>
            </div>
            <div class="row">
                <label class="fl">Billed Date</label>
                <div class="fr"><input type="text" name="ClaimDateValue" class="date-picker claimdate" value="<%= Model.ClaimDate <= DateTime.MinValue ? "" : Model.ClaimDate.ToShortDateString() %>" id="ClaimDateValue" /></div>
            </div>
            <div class="row">
	            <label class="fl">Comments</label>
                <div class="ac"><%= Html.TextArea("Comment", Model.Comment) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>