﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var billData = Model.VisitRates ?? new List<ChargeRate>(); %>
<fieldset>
	<legend>Visit Rates</legend>
	<div class="wide-column">
		<div class="row">
			<div class="button fl"><a class="reloadrates">Reload Visit Rates</a></div>
			<div class="ancillary-button fr"><a class="newrate">Add Visit Information</a></div>
		</div>
		<div class="row">
            <%= Html.Telerik().Grid<ChargeRate>(billData).HtmlAttributes(new { @class = string.Format("position-relative {0}-{1} claim-rates", Model.Service.ToString().ToLowerCase(), "managed" )}).Name(Model.Service.ToArea()+ "ManagedCareBillRate_Grid").DataKeys(keys => keys.Add(r => r.Id).RouteKey("Id")).Columns(columns => {
					columns.Bound(e => e.DisciplineTaskName).Title("Task");
					columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
					columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
					columns.Bound(e => e.Code).Title("HCPCS").Width(55);
					columns.Bound(e => e.Charge).Format("{0:$#0.00;-$#0.00}").Title("Rate").Width(45);
					columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
					columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
					columns.Bound(e => e.TimeLimit).Template(t => t.IsTimeLimit && t.ChargeType != "1" ? t.TimeLimit.ToHourMinTime() : string.Empty).ClientTemplate("<#= IsTimeLimit && TimeLimitHour != 0 && TimeLimitMin != 0 && ChargeType != \"1\" ? $.telerik.formatString('{0:H:mm}', TimeLimit) : '' #>").Title("Time Limit").Width(65);
					columns.Bound(e => e.Id).Template(t => "<a class=\"edit link\">Edit</a> <a class=\"delete link\">Delete</a>").ClientTemplate("<a class=\"edit link\">Edit</a> <a class=\"delete link\">Delete</a>").HtmlAttributes(new { @class = "action" }).Title("Action").Width(90).Sortable(false);
					columns.Bound(e => e.Id).Hidden(true).HtmlAttributes(new { @class = "rid" });
				}).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimInsuranceRates", "Billing", new { area = Model.Service.ToArea(), Id = Model.Id }))
                .ClientEvents(events=> events
                    .OnDataBound("Billing.Rate.OnDataBound")
                    .OnDataBinding("U.OnDataBindingStopAction")
                ).Sortable().Footer(false) %>
		</div>
	</div>
</fieldset>