﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var prefix = Model.Service.ToArea(); %>
<fieldset>
	<legend>Insurance Details</legend>
	<div id="<%=prefix %>ManagedCare_InsuranceContent" class="primaryinsurancecontent"><% Html.RenderPartial("InsuranceInfoContent", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Relationship = Model.Relationship, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "ManagedCare" }); %></div>
</fieldset>
<div class="clr"></div>
<%  if (Model.InvoiceType == (int)InvoiceType.UB) { %>
	<%  Html.RenderPartial("Locator/UB04AllLocator", Model, new ViewDataDictionary { { "IdPrefix", prefix + "ManagedClaim" } }); %>
<%  } else if (Model.InvoiceType == (int)InvoiceType.HCFA) { %>
	<%  Html.RenderPartial("Locator/HCFALocators", Model.LocatorHCFA ?? new List<Locator>(), new ViewDataDictionary { { "IdPrefix", prefix + "ManagedClaim" } }); %>
<%  } %>
<%  Html.RenderPartial("Managed/Claim/Insurance/List", Model); %>
