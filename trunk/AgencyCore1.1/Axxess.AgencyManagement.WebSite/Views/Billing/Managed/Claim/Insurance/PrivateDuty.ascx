﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<% var prefix = Model.Service.ToArea(); %>
<%if (Model.PayorType == 10) { %>
    <%if (Model.IsBillingAddressDifferent){ %>
		<fieldset>
			<legend>Billing Address</legend>
			<%= Html.Hidden("PrivatePayor.Id", Model.PrivatePayor.Id, new { @id = prefix + "ManagedClaim_Id" })%>
			<div class="column">
				<div class="row">
					<label for="PrivatePayor.FirstName" class="fl">First Name</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.FirstName", Model.PrivatePayor.FirstName, new { @id = prefix + "ManagedClaim_FirstName", @class = "required", @maxlength = "75" })%></div>
				</div>
				<div class="row">
					<label for="PrivatePayor.LastName" class="fl">Last Name</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.LastName", Model.PrivatePayor.LastName, new { @id = prefix + "ManagedClaim_LastName", @class = "required", @maxlength = "75" })%></div>
				</div>
			</div>
			<div class="column">
			   <div class="row">
					<label for="PrivatePayor.MiddleName" class="fl">Middle Initial</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.MI", Model.PrivatePayor.MI, new { @id = prefix + "ManagedClaim_MiddleName", @class = "shortest", @maxlength = "1" })%></div>
				</div>
			   <div class="row">
				   <label for="PrivatePayor.Relationship" class="fl">Relation to Patient</label>
				   <div class="fr">
					   <%= Html.Relationships("PrivatePayor.Relationship", Model.PrivatePayor.Relationship, true, "-- Select Relationship --", new { @id = prefix + "ManagedClaim_Relationship", @class = "required not-zero relationship" })%>
					   <br />
					   <%= Html.TextBox("PrivatePayor.OtherRelationship", Model.PrivatePayor.OtherRelationship, new { @id = prefix + "ManagedClaim_OtherRelationship", @maxlength = "75", @class = "otherrelationship " })%>
				   </div>
			   </div>
			</div>
			<div class="column">
				<div class="row">
					<label for="PrivatePayor.AddressLine1" class="fl">Address Line 1</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.AddressLine1", Model.PrivatePayor.AddressLine1, new { @id = prefix + "ManagedClaim_AddressLine1", @maxlength = "75", @class = "required" })%></div>
				</div>
				<div class="row">
					<label for="PrivatePayor.AddressLine2" class="fl">Address Line 2</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.AddressLine2", Model.PrivatePayor.AddressLine2, new { @id = prefix + "ManagedClaim_AddressLine2", @maxlength = "75" })%></div>
				</div>
				<div class="row">
					<label for="PrivatePayor.AddressCity" class="fl">City</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.AddressCity", Model.PrivatePayor.AddressCity, new { @id = prefix + "ManagedClaim_AddressCity", @maxlength = "75", @class = "address-city required" })%></div>
				</div>
				<div class="row">
					<label for="PrivatePayor.AddressStateCode" class="fl">State, Zip</label>
					<div class="fr">
						<%= Html.States("PrivatePayor.AddressStateCode", Model.PrivatePayor.AddressStateCode, new { @id = prefix + "ManagedClaim_AddressStateCode", @class = "address-state required nonzero short" })%>
						<%= Html.TextBox("PrivatePayor.AddressZipCode", Model.PrivatePayor.AddressZipCode, new { @id = "New_User_AddressZipCode", @class = "shorter numeric zip required", @maxlength = "9" })%>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="row">
					<label for="<%= prefix %>ManagedClaim_HomePhoneArray1" class="fl">Home Phone</label>
					<div class="fr">
						<%= Html.PhoneOrFax("PrivatePayor.HomePhoneArray", Model.PrivatePayor.PhoneHome, prefix + "ManagedClaim_HomePhoneArray", "autotext")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= prefix %>ManagedClaim_MobilePhoneArray1" class="fl">Mobile Phone</label>
					<div class="fr">
						<%= Html.PhoneOrFax("PrivatePayor.MobilePhoneArray", Model.PrivatePayor.PhoneMobile, prefix + "ManagedClaim_MobilePhoneArray", "autotext")%>
					</div>
				</div>
				<div class="row">
					<label for="<%= prefix %>ManagedClaim_FaxPhoneArray1" class="fl">Fax Phone</label>
					<div class="fr">
						<%= Html.PhoneOrFax("PrivatePayor.FaxPhoneArray", Model.PrivatePayor.FaxNumber, prefix + "ManagedClaim_FaxPhoneArray", "autotext")%>
					</div>
				</div>
				<div class="row">
					<label for="PrivatePayor.EmailAddress" class="fl">E-mail Address</label>
					<div class="fr"><%= Html.TextBox("PrivatePayor.EmailAddress", Model.PrivatePayor.EmailAddress, new { @id = prefix + "ManagedClaim_EmailAddress", @class = "email", @maxlength = "100" })%></div>
				</div>
			</div>
    </fieldset>
    <%} %>
<%} else { %>
 <fieldset>
	 <legend>Insurance Details</legend>
	 <div class="primaryinsurancecontent">
		 <% Html.RenderPartial("InsuranceInfoContent", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Relationship = Model.Relationship, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "ManagedCare" }); %>
	 </div>
 </fieldset>
<% } %>
<% Html.RenderPartial("Managed/Claim/Insurance/List", Model); %>
