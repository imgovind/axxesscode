﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var prefix = Model.Service.ToArea(); %>
<%  var isVisitReady = Model.IsInfoVerified && Model.IsInsuranceVerified; %>
<div class="wrapper main">
<%  using (Html.BeginForm("ManagedVisitVerify", "Billing", FormMethod.Post, new { area = prefix, @id = "managedBillingVisitForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id =prefix+ "ManagedVerification_Id", @class = "input" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = prefix + "ManagedVerification_PatientId", @class = "input" })%>
    <div class="acore-grid selectable">
		<h3 class="ac">Date Range: <%= Model.EpisodeStartDate.ToZeroFilled() %> &#8211; <%= Model.EpisodeEndDate.ToZeroFilled() %></h3>
	<%  if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) { %>
		<%  foreach (var billCategoryKey in Model.BillVisitDatas.Keys) { %>
			<%  var billCategoryVisits = Model.BillVisitDatas[billCategoryKey]; %>
			<%  var isBillable = billCategoryKey == BillVisitCategory.Billable; %>
			<%  if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
		<ul>
		    <li class="ac">
		        <h3><%= billCategoryKey.GetDescription()%></h3>
		    </li>
		    <li>
				<span class="grid-twentieth"></span>
				<span class="grid-sixth">Visit Type</span>
				<span class="grid-seventh">Scheduled Date</span>
				<span class="grid-tenth">Visit Date</span>
				<span class="grid-eleventh">HCPCS</span>
				<span class="grid-tenth">Modifiers</span>
				<span class="grid-sixth">Status</span>
				<span class="grid-eleventh">Units</span>
				<span class="grid-eleventh">Charge</span>
			</li>
		</ul>
		        <%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
					<%  var visits = billCategoryVisits[disciplineVisitKey]; %>
					<%  if (visits != null && visits.Count > 0) { %>
		<ol>
		    <li class="group-header no-hover">
				<label class="strong"><%= disciplineVisitKey.GetDescription() %></label>
			</li>
						<%  int i = 1; %>
						<%  foreach (var visit in visits) { %> 
			<li>
				<span class="grid-twentieth">
							<%  if (!visit.IsExtraTime) { %>
								<%  if (isVisitReady) { %>
					<input name="Visit" type="checkbox" id="<%="Visit" + visit.EventId.ToString() %>" value="<%= visit.EventId.ToString() %>" <%= isBillable.ToChecked() %> />
				                <%  } %>
				    &nbsp;<%= i%>.
				            <%  } %>
				</span>
				<span class="grid-sixth"><%= visit.PereferredName.IsNotNullOrEmpty() ? visit.PereferredName : visit.DisciplineTaskName %></span>
				<span class="grid-seventh"><%= visit.EventDate != DateTime.MinValue ? visit.EventDate.ToZeroFilled() : "" %></span>
	            <span class="grid-tenth"><%= visit.VisitDate != DateTime.MinValue ? visit.VisitDate.ToZeroFilled() : (visit.EventDate != DateTime.MinValue ? visit.EventDate.ToZeroFilled() : "")%></span>
				<span class="grid-eleventh"><%= visit.HCPCSCode %></span>
				<span class="grid-tenth"><%= visit.Modifier %>&nbsp;<%= visit.Modifier2 %>&nbsp;<%= visit.Modifier3 %>&nbsp;<%= visit.Modifier4 %></span>
				<span class="grid-sixth"><%= visit.StatusName %></span>
				<span class="grid-eleventh"><%= visit.Unit %></span>
				<span class="grid-eleventh"><%= string.Format("{0:$#0.00;-$#0.00}", visit.Charge) %></span>
							<%  if (visit.UnderlyingVisits.IsNotNullOrEmpty()) { %>
				<div class="acore-grid">
				    <ul>
				        <li class="ac">
				            <h3>Underlying Visits</h3>
				        </li>
				    </ul>
				    <ol>
				                <%  int i2 = 1; %> 
								<%  foreach (var visit2 in visit.UnderlyingVisits) { %> 
						<li>
							<span class="grid-twentieth">
									<%  if (!visit2.IsExtraTime) { %>
								<input name="Visit" type="checkbox" id="<%= visit2.ViewId %>" value="<%= isBillable ? visit2.EventId.ToString() + "\" checked=\"checked"  : visit2.EventId.ToString() %>" />
								&nbsp;<%= i2 %>. 
									<%  } %>
							</span>
							<span class="grid-sixth"><%= visit2.PereferredName.IsNotNullOrEmpty() ? visit2.PereferredName : visit2.DisciplineTaskName%></span>
							<span class="grid-seventh"><%= visit2.EventDate.IsValid() ? visit2.EventDate.ToZeroFilled() : "" %></span>
							<span class="grid-tenth"><%= visit2.VisitDate.IsValid() ? visit2.VisitDate.ToZeroFilled() : (visit2.EventDate != DateTime.MinValue ? visit2.EventDate.ToZeroFilled() : "")%></span>
							<span class="grid-eleventh"><%= visit2.HCPCSCode%></span>
							<span class="grid-tenth"><%= visit2.Modifier%>&nbsp;<%= visit2.Modifier2%>&nbsp;<%= visit2.Modifier3%>&nbsp;<%= visit2.Modifier4%></span>
							<span class="grid-sixth"><%= visit2.StatusName%></span>
							<span class="grid-eleventh"><%= visit2.Unit%></span>
						    <span class="grid-eleventh"><%= string.Format("{0:$#0.00;-$#0.00}", visit2.Charge)%></span>
						</li>
									<%  if (!visit2.IsExtraTime) i2++; %>
								<%  } %>
					</ol>
				</div>
							<%  } %>
			</li>
							<%  if (!visit.IsExtraTime) i++; %>
						<%  } %>
		</ol>
                    <%  } %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </div>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
    <%  if (isVisitReady) { %>
        <li><a class="save">Verify and Next</a></li>
    <%  } %>
    <%  if (Model.IsVisitVerified) { %>
		<li><a class="next">Next</a></li>
	<%  } %>
    </ul>
<%  } %>
</div>