﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewManagedClaimViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("CreateManagedClaim", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @class = "mainform", @id = "createManagedClaimForm" })) { %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <fieldset>
        <legend>Claim Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="InsuranceId" class="fl">Insurances</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("InsuranceId", Model.SelecetdInsurance, (int)Model.Service, true, true, "** Select Insurance **", new { @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="StartDate" class="fl">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="StartDate" /></div>
            </div>
            <div class="row">
                <label for="EndDate" class="fl">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="EndDate" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add Claim</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>