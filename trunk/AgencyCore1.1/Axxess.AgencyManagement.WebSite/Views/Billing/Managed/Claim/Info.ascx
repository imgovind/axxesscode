﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var prefix = Model.Service.ToArea(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("ManagedInfoVerify", "Billing", FormMethod.Post, new { area = prefix, @id = "managedBillingInfo" })) { %>
	<%= Html.Hidden("Id", Model.Id, new { @id = prefix + "ManagedVerification_Id", @class="input" }) %>
	<%= Html.Hidden("PatientId", Model.PatientId, new { @id = prefix + "ManagedVerification_PatientId", @class = "input" })%>
	<%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO) %>
	<%= Html.Hidden("ClaimType", ClaimTypeSubCategory.ManagedCare.ToString()) %>
	<%= Html.Hidden("HasMultipleEpisodes", Model.HasMultipleEpisodes) %>
	<fieldset>
		<legend>Patient Information</legend>
		<div class="column">
			<div class="row">
				<label for="FirstName" class="fl">Patient First Name</label>
				<div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "required", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<label for="LastName" class="fl">Patient Last Name</label>
				<div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @class = "required", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<label for="PatientIdNumber" class="fl">Patient Record #</label>
				<div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "required", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<label for="DOB" class="fl">Date of Birth</label>
				<div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" /></div>
			</div>
			<div class="row">
				<label for="Gender" class="fl">Gender</label>
				<div class="fr"><%= Html.Gender("Gender", Model.Gender, new { @class = "required not-zero"}) %></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="AddressLine1" class="fl">Address Line 1</label>
				<div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @class = "required" }) %></div>
			</div>
			<div class="row">
				<label for="AddressLine2" class="fl">Address Line 2</label>
				<div class="fr"><%= Html.TextBox("AddressLine2",Model.AddressLine2) %></div>
			</div>
			<div class="row">
				<label for="AddressCity" class="fl">City</label>
				<div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @class = "address-city required" })%></div>
			</div>
			<div class="row">
				<label for="AddressStateCode" class="fl">State, Zip Code</label>
				<div class="fr">
				    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @class = "address-state short" })%>
				    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required numeric shorter zip", @maxlength = "9" }) %>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Physician Information</legend>
		<div class="column">
			<div class="row">
				<label for="PhysicianLastName" class="fl">Physician Last Name</label>
				<div class="fr"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "required", @maxlength = "20" }) %></div>
			</div>
			<div class="row">
				<label for="PhysicianFirstName" class="fl">Physician First Name</label>
				<div class="fr"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "required", @maxlength = "20" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="PhysicianNPI" class="fl">Physician NPI #</label>
				<div class="fr"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "required", @maxlength = "10" }) %></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Insurance Information</legend>
	<%  if (Model.Service == AgencyServices.PrivateDuty) { %>
		<div class="wide-column">
			<div class="row">
				<label>Check the Insurance that applies to this claim</label>
				<ul class="checkgroup one-wide">
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton("PayorType", 10, Model != null && Model.PayorType == 10, new { @id = "PayorType_10", @class = "required" })%>
							<label for="PayorType_10">Patient Itself</label>
						</div>
						<div class="more">
							<ul class="checkgroup one-wide">
								<li class="option">
									<div class="wrapper">
										<%= Html.CheckBox("IsBillingAddressDifferent", Model.IsBillingAddressDifferent, new { @id = prefix + "ManagedClaim_IsBillingAddressDifferent", @class = "fl" })%>
										<label for="<%= prefix %>ManagedClaim_IsBillingAddressDifferent">Check if the billing address different from the patient address.</label>
									</div>
									<div class="more">
										<label for="PrivatePayorId" class="fl">Payor</label>
										<div class="fr"><%= Html.PatientPrivatePayorList((int)Model.Service, "PrivatePayorId", Model.PatientId, Model.PrivatePayorId, new { @class = "required nonzero"})%></div>
										<div class="clr"></div>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li class="option">
						<div class="wrapper">
							<%= Html.RadioButton("PayorType", 11, Model != null && Model.PayorType == 11, new { @id = "PayorType_11", @class = "required" })%>
							<label for="PayorType_11">Other Insurance</label>
						</div>
						<div class="more">
							<label for="PrimaryInsuranceId" class="fl">Insurance</label>
							<div class="fr"><%= Html.InsurancesNoneMedicareTraditional("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), (int)Model.Service, false, true, "-- Select Insurnace --", new { @id = prefix + "ManagedCare_PrimaryInsuranceId", @class = "required not-zero" })%></div>
							<div class="clr"></div>
							<label for="InsuranceIdNumber" class="fl">Patient Insurance Id #</label>
							<div class="fr"><%= Html.TextBox("InsuranceIdNumber", Model.InsuranceIdNumber, new { @class = "required", @maxlength = "20" })%></div>
							<div class="clr"></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
    <%  } else { %>
		<%= Html.Hidden("PayorType", 11)%>
		<div class="column">
		    <div class="row">
		        <label for="PrimaryInsuranceId" class="fl">Insurance</label>
		        <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), (int)Model.Service, false, true, "-- Select Insurnace --", new { @id = prefix + "ManagedCare_PrimaryInsuranceId", @class = "required not-zero claiminsurance" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="InsuranceIdNumber" class="fl">Patient Insurance Id #</label>
				<div class="fr"><%= Html.TextBox("InsuranceIdNumber", Model.InsuranceIdNumber, new { @class = "required", @maxlength = "20" })%></div>
			</div>
		</div>
	<%  } %>
	</fieldset>
	<fieldset>
		<div class="column">
			<div class="row">
				<label for="Type" class="fl">Bill Type</label>
				<div class="fr"><%= Html.BillType("Type", Model.Type, new { @class = "required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="EpisodeStartDate" class="fl">Start Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="<%=prefix %>ManagedCare_EpisodeStartDate" /></div>
			</div>
			<div class="row">
				<label for="EpisodeEndDate" class="fl">End Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="EpisodeEndDate" value="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="<%=prefix %>ManagedCare_EpisodeEndDate" /></div>
			</div>
			<div class="row">
				<label for="StartOfCareDate" class="fl">Admission Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="<%=prefix %>ManagedCare_StartOfCareDate" /></div>
			</div>
	<%  if (Model.Service == AgencyServices.PrivateDuty) { %>
		</div>
		<div class="column">
	<%  } %>
			<div class="row">
				<label for="AdmissionSource" class="fl">Admission Source</label>
				<div class="fr"><%= Html.AdmissionSources("AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : string.Empty, new { @class = "admission-source required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="UB4PatientStatus" class="fl">Patient Status</label>
				<div class="fr"><%= Html.UB4PatientStatus("UB4PatientStatus", Model.UB4PatientStatus, new { @id =prefix+ "ManagedClaim_UB4PatientStatus", @class = "required not-zero" })%></div>
			</div>
			<div class="row ub4patientstatuscontent" id="ManagedClaimPatientStatusRow">
				<label for="DischargeDate" class="fl">Discharge Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="DischargeDate" value="<%= Model.DischargeDate.ToZeroFilled() %>" id="<%=prefix %>ManagedClaim_DischargeDate" /></div>
			</div>
	<%  if (Model.Service == AgencyServices.HomeHealth) { %>
		</div>
		<div class="column">
	<%  } %>
			<div class="row">
				<label for="FirstBillableVisitDate" class="fl">First Billable Visit Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="FirstBillableVisitDate" value="<%= (Model.FirstBillableVisitDate.IsValid() ? Model.FirstBillableVisitDate : Model.EpisodeStartDate).ToShortDateString() %>" id="<%=prefix %>ManagedClaim_FirstBillableVisitDate" /></div>
			</div>
			<div class="row">
				<label class="fl">Please verify the first billable visit date from the schedule.</label>
				<div class="ancillary-button fr"><a onclick="UserInterface.ShowScheduleCenter('<%= Model.PatientId %>','<%= Model.PatientStatus == (int)PatientStatus.Discharged %>')">View Schedule</a></div>
			</div>
	<%  if (Model.Service != AgencyServices.PrivateDuty) { %>
			<div class="row">
				<%= Html.Hidden("AssessmentType", Model.AssessmentType, new { @id=prefix+"ManagedClaim_AssessmentType" })%>
				<label for="HippsCode" class="fl">HIPPS Code</label>
				<div class="fr"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @id = prefix + "ManagedClaim_HippsCode", @maxlength = "5" }) %></div>
			</div>
			<div class="row">
				<label for="ClaimKey" class="fl">OASIS Matching Key</label>
				<div class="fr"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @id = prefix + "ManagedClaim_ClaimKey", @maxlength = "18" }) %></div>
			</div>
			<div class="row">
				<label for="ProspectivePay" class="fl">HIPPS Code Payment</label>
				<div class="fr"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @id = prefix + "ManagedClaim_ProspectivePay", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<ul class="checkgroup one-wide">
				    <%= Html.CheckgroupOption("IsHomeHealthServiceIncluded", "true", false, "Uncheck if the home health service line is not included.") %>
				</ul>
			</div>
	<%  } else { %>
		    <%= Html.Hidden("IsHomeHealthServiceIncluded", false) %>
	<%  } %>
		</div>
	</fieldset>
	<fieldset>
		<legend>Diagnosis Codes</legend>
		<div class="column">
			<div class="row">
				<label class="fl">Primary</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Primary" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Primary : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Secondary</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Second" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Second : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Third</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Third" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Third : string.Empty %>" /></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label class="fl">Fourth</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Fourth" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Fourth : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Fifth</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Fifth" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Fifth : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Sixth</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Sixth" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Sixth : string.Empty %>" /></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Condition Codes</legend>
		<div class="column">
			<div class="row">
				<label class="fl">18.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode18" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode18 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">19.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode19" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode19 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">20.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode20" value="<%=Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode20 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">21.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode21" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode21 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">22.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode22" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode22 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">23.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode23" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode23 : string.Empty %>" /></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label class="fl">24.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode24" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode24 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">25.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode25" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode25 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">26.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode26" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode26 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">27.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode27" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode27 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">28.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode28" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode28 : string.Empty %>" /></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Remark</legend>
		<div class="wide-column">
			<div class="row ac"><%= Html.TextArea("Remark", Model.Remark, new { id = "Managed_Remark" })%></div>
		</div>
	</fieldset>
	<ul class="buttons">
		<li><a class="save">Verify &#38; Next</a></li>
	<%  if (Model.IsInfoVerified) { %>
		<li><a class="next">Next</a></li>
	<%  } %>
	</ul>
<%  } %>
</div>