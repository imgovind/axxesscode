﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var prefix = Model.Service.ToArea(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("ManagedInsuranceVerify", "Billing", FormMethod.Post, new { area = prefix, @id = "managedBillingInsuranceForm" })) { %>
	<%= Html.Hidden("Id", Model.Id, new { @id = prefix + "ManagedVerification_Id" }) %>
	<%= Html.Hidden("PatientId", Model.PatientId, new { @id = prefix + "ManagedVerification_PatientId" }) %>
	<%= Html.Hidden("ServiceId", (int)Model.Service, new { @id = prefix + "ManagedVerification_ServiceId" }) %>
	<%= Html.Hidden("TypeId", (int)ClaimTypeSubCategory.ManagedCare, new { @id = prefix + "ManagedVerification_TypeId" }) %>
	<%= Html.Hidden("PayorType", Model.PayorType, new { @id = prefix + "ManagedVerification_PayorType" }) %>
	<%= Html.Hidden("PrimaryInsuranceId", Model.PrimaryInsuranceId, new { @id = "ManagedVerification_PrimaryInsuranceId" }) %>
	<%  Html.RenderPartial("Managed/Claim/Insurance/" + Model.Service.ToString(), Model); %>
	<ul class="buttons ac">
	    <li><a class="back">Back</a></li>
	    <li><a class="save">Verify and Next</a></li>
	<%  if (Model.IsInsuranceVerified) { %>
		<li><a class="next">Next</a></li>
	<%  } %>
    </ul>
<%  } %>
</div>
<%  var disabledTabs = new string[] { Model.IsInsuranceVerified ? string.Empty : "2", Model.IsVisitVerified ? string.Empty : "3", Model.IsSupplyVerified ? string.Empty : "4" }; %>
<script type="text/javascript">
	$("#<%= prefix %>ManagedVerification_TabStrip").tabs("option", "disabled", [ <%= disabledTabs.Join(", ") %> ])
</script>
