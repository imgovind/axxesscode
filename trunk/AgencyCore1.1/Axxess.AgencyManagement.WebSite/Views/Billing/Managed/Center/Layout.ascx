﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("Managed/Center/Filters", Model); %></div>
        <div class="bottom"><% Html.RenderPartial("Managed/Center/Patients", Model); %></div>
    </div>
    <div id="ManagedBillingMainResult" class="ui-layout-center"><% Html.RenderPartial("Managed/Center/Content", Model); %></div>
</div>