﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%  var prefix = Model.Service.ToString(); %>
<%= Html.Hidden("PatientId", Guid.Empty, new { @id = prefix + "ManagedBillingHistory_PatientId", @class = "activity-input" }) %>
<%  Html.Telerik().Grid<PatientSelection>().Name(prefix + "ManagedBillingSelection_Grid").HtmlAttributes(new { @class = "patientlist-container" }).Columns(columns => {
        columns.Bound(p => p.LastName).HtmlAttributes(new { @class = "searchL" }); ;
        columns.Bound(p => p.ShortName).Title("First Name/MI").HtmlAttributes(new { @class = "searchF" }); ;
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "pid" }).Width(0);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("AllInsurancePatient", "PatientProfile", new { area = Model.Service.ToArea(), BranchId = Model.Id, StatusId = Model.Status, Name = string.Empty, InsuranceId = Model.SubId })).Sortable().Selectable().Scrollable().Footer(false).NoRecordsTemplate(" ").ClientEvents(events => events
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("Billing.Managed.Center.PatientListDataBound")
        .OnRowSelect("Billing.Managed.Center.OnPatientRowSelected"))
    .Render(); %>