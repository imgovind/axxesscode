﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%  var prefix = Model.Service.ToString(); %>
<div class="center-filter">
    <label for="<%= prefix %>ManagedBillingHistory_BranchId" class="fl">Branch</label>
    <div class="filter-fill text"><%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.Service, true, new { @class = "filterInput location", @id = prefix + "ManagedBillingHistory_BranchId" }) %></div>
</div>
<div class="center-filter">
    <label for="<%= prefix %>ManagedBillingHistory_StatusId" class="fl">View</label>
    <div class="filter-fill"><%= Html.PatientStatusList("StatusId", Model.Status.ToString(), true, false, string.Empty, new { @class = "filterInput", @id = prefix + "ManagedBillingHistory_StatusId" }) %></div>
</div>
<div class="center-filter">
    <label for="<%= prefix %>ManagedBillingHistory_InsuranceId" class="fl">Filter</label>
    <div class="filter-fill"><%= Html.Insurances("InsuranceId", Model.SubId.ToString(), (int)Model.Service, true, true, "All", new { @class = "filterInput insurance", @id = prefix + "ManagedBillingHistory_InsuranceId" }) %></div>
</div>
<div class="center-filter">
    <label for="<%= prefix %>ManagedBillingHistory_Name" class="fl">Find</label>
    <div class="filter-fill text"><input class="searchtext text filterInput" name="Name" value="" type="text" id="<%= prefix %>ManagedBillingHistory_Name" /></div>
</div>