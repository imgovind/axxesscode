﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimSnapShotViewData>" %>
<% if (!Current.IsAgencyFrozen && Model.IsUserCanAdd) { %>
<ul class="window-menu">
    <li><a onclick="Billing.Managed.Claim.New('<%=Model.PatientId %>','<%= (int)Model.Service %>')">New Claim</a></li>
</ul>
<% } %>
 <div class="wrapper main claim-information">
    <div class="boundary">
	    <% if(Model.IsUserCanViewList || Model.IsUserCanViewLog) { %>
			<div class="reports fr">
				<h5 class="reports-head">Quick Reports</h5>
				<ul>
					<% if (Model.IsUserCanViewList){ %>
					<li><%= string.Format("<a onclick=\"Billing.Managed.Payment.OpenList('{0}','{1}','{2}')\">View Payments</a>", Model.Id, Model.PatientId, (int)Model.Service) %></li>
					<li><%= string.Format("<a onclick=\"Billing.Managed.Adjustment.OpenList('{0}','{1}','{2}')\">View Adjustments</a>", Model.Id, Model.PatientId, (int)Model.Service) %></li>
					<% } %>
					<% if ( Model.IsUserCanViewLog){ %>
					<li><%= string.Format("<a onclick=\"Log.LoadClaimLog('ManagedClaim','{0}','{1}')\">Activity Logs</a>", Model.Id, Model.PatientId) %></li>
					<% } %>
				</ul>
			</div>
        <% } %>
        <div class="billing-info">
            <h2 class="billing ac">Managed Care Claim</h2>
            <%= Model.PatientName.IsNotNullOrEmpty() ? "<div>Patient Name<div class='fr'>" + Model.PatientName + "</div></div>" : string.Empty %>
            <%= Model.PatientIdNumber.IsNotNullOrEmpty() ? "<div>Patient MRN<div class='fr'>" + Model.PatientIdNumber + "</div></div>" : string.Empty%>
            <%= Model.IsuranceIdNumber.IsNotNullOrEmpty() ? "<div>Insurance Id<div class='fr'>" + Model.IsuranceIdNumber + "</div></div>" : string.Empty%>
            <%= Model.PaymentDate.IsValid() ? "<div>Payment Date<div class='fr'>" + Model.PaymentDate.ToZeroFilled() + "</div></div>" : string.Empty%>
<%  if (Model.Visible) { %>
            <%= Model.PayorName.IsNotNullOrEmpty() ? "<div>Insurance/Payor<div class='fr'>" + Model.PayorName + "</div></div>" : string.Empty%>
            <%= Model.HealthPlainId.IsNotNullOrEmpty() ? "<div>Health Plan Id<div class='fr'>" + Model.HealthPlainId + "</div></div>" : string.Empty%>
            <%= Model.AuthorizationNumber.IsNotNullOrEmpty() ? "<div>Authorization Number<div class='fr'>" + Model.AuthorizationNumber + "</div></div>" : string.Empty%>
            <%= Model.ClaimDate.IsValid() ? "<div>Billed Date<div class='fr'>" + Model.ClaimDate.ToZeroFilled() + "</div></div>" : string.Empty%>
<%  } %>
            <ul class="buttons ac">
	            <% if(!Current.IsAgencyFrozen && Model.IsUserCanEdit) { %>
                <li class="add-payment-button"><a onclick="Billing.Managed.Payment.New('<%= Model.Id %>','<%= (int)Model.Service %>')">Post Payment</a></li>
                <li class="add-adjustment-button"><a onclick="Billing.Managed.Adjustment.New('<%= Model.Id %>','<%= (int)Model.Service %>')">Post Adjustment</a></li>
                <% } %>
                <%--//TODO: Fix Invoice for Patient button--%>
                <%--<li><a onclick="U.GetAttachment('<%= Model.Service.ToArea() %>/Billing/InvoicePdf', { patientId: '<%= Model.PatientId %>', Id: '<%= Model.Id %>', isForPatient: 'true' })">Create Invoice</a></li>--%>
            </ul>
        </div>
        <div class="clr"></div>
    </div>
</div>