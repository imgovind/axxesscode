﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<% var area = Model.Service.ToArea(); %>
<div class="abs above ac">
    <div class="fr"><a class="grid-resize img grid-expand"></a></div>
    <label>Insurance</label>
    <select name="InsuranceId" id="<%= Model.ToString() %>ManagedBillingHistory_PrimaryInsuranceId" class="insurance activity-input long"></select>
</div>
<%  Html.Telerik().Grid<ManagedClaimLean>().Name(Model.Service.ToString() + "ManagedBillingActivity_Grid").HtmlAttributes(new { @class = "activity-container scrollable" }).Columns(columns => {
        columns.Bound(p => p.Id).ClientTemplate("").Title("").HtmlAttributes(new { @class = "edit" }).HeaderHtmlAttributes(new { @class = "edit" }).Width(110).Sortable(false);
        columns.Bound(p => p.ClaimRange).Title("Claim Date Range").Width(215).Sortable(false);
        columns.Bound(p => p.StatusName).Title("Status").Width(70);
		columns.Bound(p => p.ClaimAmount).Title("Claim").Format("{0:$#0.00;-$#0.00}").Width(100);
		columns.Bound(p => p.PaymentAmount).Title("Total Payments").Format("{0:$#0.00;-$#0.00}").Width(120);
		columns.Bound(p => p.AdjustmentAmount).Title("Total Adjustments").Format("{0:$#0.00;-$#0.00}").Width(132);
		columns.Bound(p => p.Balance).Title("Balance").Format("{0:$#0.00;-$#0.00}").Width(80);
        columns.Bound(p => p.IsInfoVerified).Title("Details").ClientTemplate("<span class=\"img icon16 <#= IsInfoVerified ? 'check' : 'ecks' #>\">").Width(55).Sortable(false);
		columns.Bound(p => p.IsVisitVerified).Title("Visits").ClientTemplate("<span class=\"img icon16 <#= IsVisitVerified ? 'check' : 'ecks' #>\">").Width(50).Sortable(false);
		columns.Bound(p => p.IsSupplyVerified).Title("Supply").ClientTemplate("<span class=\"img icon16 <#= IsSupplyVerified ? 'check' : 'ecks' #>\">").Width(58).Sortable(false);
        columns.Bound(p => p.Id).ClientTemplate("").HtmlAttributes(new { @class = "print" }).HeaderHtmlAttributes(new { @class = "print" }).Encoded(false).Title("").Width(65).Sortable(false);
        columns.Bound(p => p.Id).ClientTemplate("").Title("Action").HtmlAttributes(new { @class = "action" }).HeaderHtmlAttributes(new { @class = "action" }).Width(95).Visible(!Current.IsAgencyFrozen).Sortable(false);
        columns.Bound(p => p.Id).Hidden(true).HtmlAttributes(new {  @class = "cid" });
        columns.Bound(p => p.PatientId).Hidden(true).HtmlAttributes(new {  @class = "pid" });
        columns.Bound(p => p.Id).ClientTemplate(((int)ClaimTypeSubCategory.ManagedCare).ToString()).Hidden(true).HtmlAttributes(new { @class = "ctype" });
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimsActivity", "Billing", new { area = area, PatientId = Guid.Empty, InsuranceId = 0 })).ClientEvents(events => events
        .OnDataBinding("Billing.Managed.Center." + Model.Service.ToString() + ".OnClaimDataBinding")
        .OnDataBound("Billing.Managed.Center." + Model.Service.ToString() + ".OnClaimDataBound")
        .OnRowDataBound("Billing.Managed.Center." + Model.Service.ToString() + ".OnClaimRowDataBound")
		.OnRowSelect("Billing.Managed.Center." + Model.Service.ToString() + ".OnClaimRowSelected")
    ).Sortable().NoRecordsTemplate(" ").Selectable().Scrollable().Footer(false).Render(); %>