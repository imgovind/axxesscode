﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimPayment>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateManagedClaimPaymentDetails", "Billing", FormMethod.Post, new { @id = "updateManagedClaimPaymentForm" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("ClaimId", Model.ClaimId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Update Information</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Payment Amount</label>
                <div class="fr">$<%= Html.TextBox("PaymentAmount", Model.Payment, new {@class = "required" })%></div>
            </div>
            <div class="row">
                <label class="fl">Payment Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="PaymentDateValue" value="<%= Model.PaymentDate.ToZeroFilled() %>" id="PaymentDateValue" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>