﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<%  var isDataExist = (Model != null && Model.Claims != null && Model.Claims.Count > 0); %>
<%  var area = Model.Service.ToArea(); %>
<%  var isPrivateDuty = (Model.Service == AgencyServices.PrivateDuty); %>
<div class="subcontent">
    <div class="acore-grid selectable">
        <input type="hidden" class="subinput" name="BranchId" value="<%=Model.BranchId %>" />
        <input type="hidden" class="subinput" name="InsuranceId" value="<%=Model.Insurance %>" />
        <input type="hidden" class="subinput" name="ClaimType" value="<%= (int)ClaimTypeSubCategory.ManagedCare %>" />
        <input type="hidden" class="subinput" name="ParentSortType" value="branch" />
        <input type="hidden" name="CurrentSort" value="name" />
        <input type="hidden" name="IsSortInvert" value="true" />
        <ul>
            <li class="ac">
               <h3><%= Model.BranchName + "|" + Model.InsuranceName %></h3>
<%  if (isDataExist) { %> 
    <%  if (Model.IsUserCanExport) { %>
                <span>[ <a class="subexport" url="<%=area %>/Billing/ClaimsXls">Excel Export</a> ]</span>
    <%  } %>
    <%  if (Model.IsUserCanExport) { %>
                <span>[ <a class="subprint" url="<%=area %>/Billing/ClaimsPdf">Print</a> ]</span>
    <%  } %>
<%  } %>
            </li>
            <li>
				<span class="grid-twentieth"></span>
                <span class="grid-quarter pointer sortable" sortname="name">Patient Name</span>
                <span class="grid-eighth pointer sortable" sortname="id">MRN</span>
                <span class="grid-fifth pointer sortable" sortname="episode">Episode Date</span>
                <span class="grid-sixteenth ac">Detail</span>
                <span class="grid-sixteenth ac">Visit</span>
                <span class="grid-sixteenth ac">Supply</span>
                <span class="grid-sixteenth ac">Verified</span>
            </li>
        </ul>
<%  if (isDataExist) { %>
        <ol>
    <%  var claims = Model.Claims.Where(c =>c.EpisodeEndDate.Date <= DateTime.Now.Date); %>
    <%  int i = 1; %>
    <%  foreach (ManagedBill claim in claims) { %>
		<%  var nameText = claim.LastName + ", " + claim.FirstName; %>
			<li>
        <%  if (!Current.IsAgencyFrozen && Model.IsUserCanGenerate) { %>
                <span class="grid-twentieth"><%= i %>. <%= !isPrivateDuty && (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? "<input name='ClaimSelected' class='radio' type='checkbox' value='" + claim.Id + "' id='ManagedClaimSelected" + claim.Id + "' />" : string.Empty %></span>
        <%  } else { %>
                <span class="grid-twentieth"><%= i %></span>
        <%  } %>
                <span class="grid-quarter">
		<%  if (!Current.IsAgencyFrozen && Model.IsUserCanEdit) { %>
					<a onclick="Billing.Managed.Claim.Open('<%= claim.Id %>','<%= claim.PatientId %>','<%= (int)Model.Service %>')" title="Claim Ready"><%= nameText %></a>
		<%  } else { %>
		            <%= nameText %>
		<%  } %> 
				</span>
                <span class="grid-eighth"><%= claim.PatientIdNumber%></span>
				<span class="grid-fifth">
					<span class="hidden"><%= claim.EpisodeStartDate.IsValid() ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
					<%= claim.EpisodeStartDate.ToZeroFilled() %>
					<%= claim.EpisodeEndDate.IsValid() ? "&#8211;" + claim.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %>
				</span>
                <span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsInfoVerified? "check" : "ecks" %>'></span></span>
                <span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsVisitVerified? "check" : "ecks" %>'></span></span>
                <span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsSupplyVerified ? "check" : "ecks" %>'></span></span>
                <span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified ? "check" : "ecks" %>'></span></span>
		<%  if (Model.IsUserCanPrint) { %>
		        <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? string.Format("<a class='fr img icon16 print' onclick=\"U.GetAttachment('{2}/Billing/ManagedClaimPdf', {{ 'Id': '{0}' ,'patientId': '{1}' }});\" />", claim.Id, claim.PatientId, area) : string.Empty %>
		<%  } %>
			</li>
        <%  i++; %>
    <%  } %>
        </ol>
<%  } else { %>
		<ol>
			<li class="no-hover">
				<h4>No claims found for <%= Model.InsuranceName %>.</h4>
			</li>
		</ol>
<%  } %>
    </div>
<%  if (!isPrivateDuty && isDataExist && Model.Insurance > 0 && Model.IsUserCanGenerate && !Current.IsAgencyFrozen) { %>
	<ul class="buttons ac">
		<li><a class="submitclaim" claimtype="<%= Model.ClaimType.ToString().ToLower()%>">Generate Selected</a></li>
		<li><a class="submitallclaim" claimtype="<%= Model.ClaimType.ToString().ToLower()%>">Generate All Completed</a></li>
	</ul>
<%  } %>
</div>