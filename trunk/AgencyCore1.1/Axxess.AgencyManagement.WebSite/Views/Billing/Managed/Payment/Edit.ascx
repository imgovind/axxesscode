﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimPayment>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateManagedClaimPaymentDetails", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "updateManagedClaimPaymentForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("ClaimId", Model.ClaimId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Update Payment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Payment Amount</label>
                <div class="fr"><%= Html.TextBox("Payment", String.Format("{0:0.00}", Model.Payment), new { @class = "currency required" })%></div>
            </div>
            <div class="row">
                <label class="fl">Payment Date</label>
                <div class="fr"><%= Html.DatePicker("PaymentDate", Model.PaymentDate, true, new { id = "PaymentDate" })%></div>
            </div>
            <div class="row">
                <label class="fl">Payor</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("Payor", Model.Payor.ToString(),(int) Model.Service,true, true, "-- Select Payor --", new { @class = "required not-zero" })%></div>
            </div>
             <div class="row">
                <label class="fl">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", Model.Comments) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>