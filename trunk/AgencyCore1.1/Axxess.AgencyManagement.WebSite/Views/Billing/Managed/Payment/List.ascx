﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%  Html.Telerik().Grid<ManagedClaimPayment>().Name("ManagedBillingPaymentsGrid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
        columns.Bound(p => p.PayorName).Title("Payor");
        columns.Bound(p => p.Payment).Format("{0:$#0.00;-$#0.00}").Width(200);
        columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width(200);
        columns.Bound(p => p.Comments).Title("").Width(30).ClientTemplate("<a class=\"img icon16 yellow note\" tooltip=\"<#=Comments#>\"></a>").Sortable(false);
        columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Managed.Payment.Edit('<#=Id#>')\">Update</a> | <a class=\"link\" onclick=\"Billing.Managed.Payment.Delete('<#=PatientId#>','<#=ClaimId#>','<#=Id#>')\">Delete</a>").Title("Actions").Width(200).Sortable(false).Visible(!Current.IsAgencyFrozen && Model.EditPermissions != AgencyServices.None);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimPaymentsGrid", "Billing", new { claimId = Model.Id }).OperationMode(GridOperationMode.Client)).ClientEvents(evnts => evnts
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("U.OnTGridDataBound")
        .OnError("U.OnTGridError")
        .OnRowDataBound("Billing.Managed.OnRowWithCommentsDataBound")
    ).NoRecordsTemplate("No Payments found.").Sortable().Scrollable().Render(); %>