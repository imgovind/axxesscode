﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddManagedClaimPayment", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newManagedClaimPaymentForm", @class = "mainform" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Post Payment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Payment Amount</label>
                <div class="fr"><%= Html.TextBox("PaymentAmount", string.Empty, new { @class = "required currency" })%></div>
            </div>
            <div class="row">
                <label class="fl">Payment Date</label>
                <div class="fr"><input type="text" class="date-picker" name="PaymentDate" id="PaymentDateValue" /></div>
            </div>
            <div class="row">
                <label class="fl">Payor</label>
                <div class="fr"><%= Html.InsurancesNoneMedicareTraditional("Payor", string.Empty, (int)Model.Service, true, true, "-- Select Payor --", new { @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label class="fl">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", string.Empty) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Post Payment</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>