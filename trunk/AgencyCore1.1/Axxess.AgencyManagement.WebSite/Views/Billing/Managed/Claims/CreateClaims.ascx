﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle"> <% =Model.Service.GetDescription() %> | Create Claims | <%= Current.AgencyName%></span>
<%  var pageName = Model.Service.ToString() + Model.ClaimType + "Center";  %>
<%  var area = Model.Service.ToArea(); %>
<div id="<%= pageName %>MainWrapper" class="main wrapper">
<%  if (Model.Service != AgencyServices.None && Model.IsUserCanView && Model.Insurance != 0) { %>
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <%  if (Model.IsUserCanExport) { %>
    <ul class="buttons fr top ac">
        <li><a url="<%=area %>/Billing/ClaimsPdf" class="printclaims">Print</a></li><br />
        <li><a url="<%=area %>/Billing/ClaimsXls" class="export">Excel Export</a></li>
    </ul>
    <%  } %>
    <fieldset class="grid-controls ac">
	    <div class="button fr"><a url="<%=area %>/Billing/ManagedGrid" class="grid-refresh">Refresh</a></div>
        <%= Html.Hidden("Service", (int)Model.Service, new { @class = "input service", @id = pageName + "_Service" }) %>
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.BranchList("BranchId", Model.BranchId.ToString(), (int)Model.Service, new { @id = pageName + "_BranchId", @class = "input location long" }) %>
        </div>
        <div class="filter">
            <label for="<%=pageName%>_InsuranceId" class="strong">Insurance</label>
            <%= Html.InsurancesNoneMedicareTraditional("InsuranceId", Model.Insurance.ToString(), (int)Model.Service, true, false, string.Empty, new { @id = pageName + "_InsuranceId", @class = "input managedcare not-zero long insurance" }) %>
        </div>
        <div class="filter">
            <%= Html.Hidden("ClaimType", (int)Model.ClaimType, new { @id = pageName + "_ClaimType", @class = "input claimtype", @billtype = "managedcare" })%>
            <%= Html.Hidden("ParentSortType", "branch", new { @id = pageName + "_ParentSortType", @class = "input" })%>
            <%= Html.Hidden("ColumnSortType", "", new { @id = pageName + "_ColumnSortType", @class = "input" })%>
        </div>
    </fieldset>
    <div id="<%= pageName %>GridContainer" class="main-container" style="min-height:200px;"><% Html.RenderPartial("Managed/ManagedGrid", Model); %></div>
<%  } else if (Model.IsUserCanView && Model.Insurance == 0) { %>
	<script>
		var button = <% if (Model.IsUserCanAdd)
		                { %> [{ Text: "New Insurance", Click: Insurance.New }]; <% } else { %> null; <% } %>
		$("#<%= pageName %>MainWrapper").html(U.MessageWarn("No Insurances", "No Insurances were found for this agency. Please create one.", button));
	</script>
<% } %>
</div>
