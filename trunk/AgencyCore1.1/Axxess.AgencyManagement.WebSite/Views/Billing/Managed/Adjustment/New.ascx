﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddManagedClaimAdjustment", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newManagedClaimAdjustmentForm", @class = "mainform" })) { %>
    <%= Html.Hidden("ClaimId", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Post Adjustment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Adjustment Amount</label>
                <div class="fr"><%= Html.TextBox("Adjustment", "", new {@class = "required currency" }) %></div>
            </div>
            <div class="row">
                <label class="fl">Type</label>
                <div class="fr"><%= Html.AdjustmentCodes("TypeId", "", "-- Select Type --", new { @class = "required not-zero" }) %></div>
            </div>
             <div class="row">
                <label class="fl">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", "") %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Post Adjustment</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>