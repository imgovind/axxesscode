﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%  Html.Telerik().Grid<ManagedClaimAdjustment>().Name("ManagedBillingAdjustmentsGrid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
		columns.Bound(p => p.Type);
		columns.Bound(p => p.Adjustment).Format("{0:$#0.00;-$#0.00}").Width(200);
		columns.Bound(p => p.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"img icon16 yellow note\" tooltip=\"<#=Comments#>\"> </a>").Sortable(false);
		columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Managed.Adjustment.Edit('<#=Id#>')\">Update</a><a class=\"link\" onclick=\"Billing.Managed.Adjustment.Delete('<#=PatientId#>','<#=ClaimId#>','<#=Id#>')\">Delete</a>").Title("Actions").Width(200).Sortable(false).Visible(!Current.IsAgencyFrozen && Model.EditPermissions != AgencyServices.None);
	}).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimAdjustmentsGrid", "Billing", new { claimId = Model.Id }).OperationMode(GridOperationMode.Client)).ClientEvents(evnts => evnts
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("U.OnTGridDataBound")
        .OnError("U.OnTGridError")
        .OnRowDataBound("Billing.Managed.OnRowWithCommentsDataBound")
    ).NoRecordsTemplate("No Adjustments found.").Sortable().Scrollable().Render(); %>