﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimAdjustment>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateManagedClaimAdjustmentDetails", "Billing", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "updateManagedClaimAdjustmentForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("ClaimId", Model.ClaimId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Update Adjustment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="fl">Adjustment Amount</label>
                <div class="fr"><%= Html.TextBox("Adjustment", String.Format("{0:0.00}", Model.Adjustment), new {@class = "required currency" }) %></div>
            </div>
            <div class="row">
                <label class="fl">Type</label>
                <div class="fr"><%= Html.AdjustmentCodes("TypeId", Model.TypeId.ToString(), "-- Select Code --", new { @class = "required not-zero" }) %></div>
            </div>
             <div class="row">
                <label class="fl">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", Model.Comments) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update Adjustment</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>