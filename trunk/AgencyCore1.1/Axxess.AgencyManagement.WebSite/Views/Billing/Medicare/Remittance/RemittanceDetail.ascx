﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<span class="wintitle">Remittance Detail | <%= Current.AgencyName %></span>
<div class="wrapper main note">
    <input type="hidden" name="Id" class="input" value="<%= Model.Id %>'" />
    <ul class="buttons ac">
<%  if (Model.IsUserCanPost && !Current.IsAgencyFrozen) { %>
        <li><a onclick="Billing.Medicare.Remittance.Details.Post($('#RemittanceDetailContainer .acore-grid .episodes'))">Post Selected</a></li>
<%  } %>
<%  if (Model.IsUserCanPrint) { %>
        <li><a onclick="Billing.Medicare.Remittance.Details.Print()">Print</a></li>
<%  } %>
    </ul>
<%  if (Model != null) { %>
    <%  var data = Model.RemittanceData ?? new RemittanceData(); %>
	<fieldset>
		<legend>Details</legend>
		<%= Html.Hidden("Id", Model.Id, new { id = "RemittanceDetail_Id" }) %>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Check Number</label>
                <label class="fr"><%= data.CheckNo %></label>
            </div>
            <div class="row no-input">
                <label class="fl">Payment Total</label>
                <label class="fr"><%= Model.PaymentAmount.ToCurrencyFormat() %></label>
            </div>
            <div class="row no-input">
                <label class="fl">Total Claims</label>
                <label class="fr"><%= Model.TotalClaims %></label>
            </div>
        </div>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Remittance Date</label>
                <label class="fr"><%= Model.RemittanceDate > DateTime.MinValue ? Model.RemittanceDate.ToShortDateString() : string.Empty %></label>
            </div>
            <div class="row no-input">
                <label class="fl">Payment Date</label>
                <label class="fr"><%= Model.PaymentDate > DateTime.MinValue ? Model.PaymentDate.ToShortDateString() : string.Empty %></label>
            </div>
        </div>
	</fieldset>
    <div class="inline-fieldset two-wide">
	    <div>
		    <fieldset>
			    <legend>Payer</legend>
			    <div class="column">
				    <div class="row no-input">
					    <label class="fl">Name</label>
						<div class="fr"><%= data.Payer != null ? data.Payer.Name : string.Empty %></div>
				    </div>
    <%  if (data.Payer != null && data.Payer.RefType.IsNotNullOrEmpty()) { %>
						<div class="row no-input">
							<label class="fl"><%= data.Payer.RefType %></label>
							<div class="fr"><%= data.Payer.RefNum %></div>
						</div>
    <%  } %>
					<div class="row no-input">
						<label class="fl">Address 1</label>
						<div class="fr"><%= data.Payer != null ? data.Payer.Add1 : string.Empty %></div>
					</div>
					<div class="row no-input">
						<label class="fl">Address 2</label>
						<div class="fr"><%= data.Payer != null ? data.Payer.Add2 : string.Empty %></div>
					</div>
					<div class="row no-input">
						<label class="fl">City</label>
						<div class="fr"><%= data.Payer != null ? data.Payer.City : string.Empty %></div>
					</div>
					<div class="row no-input">
						<label class="fl">State</label>
						<div class="fr"><%= data.Payer != null ? data.Payer.State : string.Empty %></div>
					</div>
					<div class="row no-input">
						<label class="fl">Zip</label>
						<div class="fr"><%= data.Payer != null ? data.Payer.Zip : string.Empty %></div>
					</div>
                </div>
		    </fieldset>
	    </div>
	    <div>
		    <fieldset>
			    <legend>Payee</legend>
			    <div class="column">
				    <div class="row no-input">
					     <label class="fl">Name</label>
					     <div class="fr"><%= data.Payee != null ? data.Payee.Name : string.Empty %></div>
				    </div>
    <%  if (data.Payee != null && data.Payee.RefType.IsNotNullOrEmpty()) { %>
					<div class="row no-input">
						<label class="fl"><%= data.Payee.RefType %></label>
						<div class="fr"><%= data.Payee.RefNum %></div>
					</div>
    <%  } %>
    <%  if (data.Payee != null && data.Payee.IdType.IsNotNullOrEmpty()) { %>
					<div class="row no-input">
						<label class="fl"><%= data.Payee.IdType %></label>
						<div class="fr"><%= data.Payee.Id %></div>
					</div>
    <%  } %>
                    <div class="row no-input">
                        <label class="fl">Address 1</label>
                        <div class="fr"><%= data.Payee != null ? data.Payee.Add1 : string.Empty %></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Address 2</label>
                        <div class="fr"><%= data.Payee != null ? data.Payee.Add2 : string.Empty %></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">City</label>
                        <div class="fr"><%= data.Payee != null ? data.Payee.City : string.Empty %></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">State</label>
                        <div class="fr"><%= data.Payee != null ? data.Payee.State : string.Empty %></div>
                    </div>
                    <div class="row no-input">
                        <label class="fl">Zip</label>
                        <div class="fr"><%= data.Payee != null ? data.Payee.Zip : string.Empty %></div>
                    </div>
			    </div>
		    </fieldset>
	    </div>
    </div>
    <div id="RemittanceDetailContainer"><% Html.RenderPartial("Medicare/Remittance/RemittanceDetailContent", Model); %></div>
<%  } %>
</div>
