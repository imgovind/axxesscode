﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("RemittanceUpload", "Billing", FormMethod.Post, new { @id = "externalRemittanceUploadForm" })) { %>
	<fieldset>
	    <legend>Remittance Upload</legend>
		<div class="wide-column">
		    <div class="row">
			    <label class="fl">Select a Remittance File</label>
				<div class="fr"><input id="Billing_ExternalRemittanceUpload" type="file" name="Upload" /></div>
			</div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Upload</a></li>
		<li><a class="save clear">Upload and Upload Another</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>