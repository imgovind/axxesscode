﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<div class="acore-grid">
<%  if (Model != null && Model.RemittanceData != null && Model.RemittanceData.Claim != null && Model.RemittanceData.Claim.Count > 0) { %>
    <%  int zebra = 0, count = 0; %>
	<ul>
	    <li class="ac"><h3>Claim(s)</h3></li>
	</ul>
	<ol>
	<%  foreach (var claim in Model.RemittanceData.Claim) { %>
		<%  if (claim.ProviderLevelAdjustment != null) { %>
        <li class="no-hover">
			<div class="wide-column">
				<div class="row ac">
					<h3>Provider Level Adjustment</h3>
				</div>
				<div class="inline-row four-wide">
					<div>
						<div class="row no-input">
						    <label class="fl">Provider Identifier</label>
						    <div class="fr"><%=claim.ProviderLevelAdjustment.ProviderIdentifier%></div>
						</div>
					</div>
					<div>
					    <div class="row no-input">
					        <label class="fl">Fiscal Period Date</label>
					        <div class="fr"><%=claim.ProviderLevelAdjustment.FiscalPeriodDate.IsValidPHPDate() ? claim.ProviderLevelAdjustment.FiscalPeriodDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></div>
					    </div>
					</div>
                    <div>
						<div class="row no-input">
						    <label class="fl">Adjustment Reason</label>
						    <div class="fr"><%= claim.ProviderLevelAdjustment.AdjustmentReasonDesc%></div>
						</div>
					</div>
					<div>
						<div class="row no-input">
						    <label class="fl">Adjustment Amount</label>
						    <div class="fr"><%= claim.ProviderLevelAdjustment.ProviderAdjustmentAmount.ToCurrencyFormat() %></div>
						</div>
					</div>
				</div>
			</div>
		</li>
			<%  zebra++; %>
		<%  } %>
		<%  if (claim.ClaimPaymentInformation != null && claim.ClaimPaymentInformation.Count > 0) { %>
		    <%  var i = 0; %>
			<%  foreach (var claimPaymentInfo in claim.ClaimPaymentInformation) { %>
		<li class="no-hover">
			<div class="wide-column">
				<div class="inline-row four-wide">
					<div>
						<div class="row no-input">
						    <label class="fl">Patient Name</label>
						    <div class="fr"><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty %></div>
						</div>
				<%  if (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty()) { %>
						<div class="row no-input">
				            <label class="fl"><%= claimPaymentInfo.Patient.IdQualifierName%></label>
				            <label class="fr"><%= claimPaymentInfo.Patient.Id%></label>
				        </div>
				<%  } %>
						<div class="row no-input">
						    <label class="fl">Patient Control Number</label>
						    <div class="fr"><%= claimPaymentInfo.PatientControlNumber%></div>
						</div>
					</div>
					<div>
						<div class="row no-input">
						    <label class="fl">ICN Number</label>
						    <div class="fr"><%= claimPaymentInfo.PayerClaimControlNumber%></div>
						</div>
						<div class="row no-input">
						    <label class="fl">Start Date</label>
						    <div class="fr"><%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></div>
						</div>
						<div class="row no-input">
						    <label class="fl">End Date</label>
						    <div class="fr"><%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></div>
						</div>
					</div>
					<div>
						<div class="row no-input">
						    <label class="fl">Type Of Bill</label>
						    <div class="fr"><%= claimPaymentInfo.TypeOfBill%></div>
						</div>
						<div class="row no-input">
						    <label class="fl">Claim Status</label>
						    <div class="fr"><%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode)%></div>
						</div>
						<div class="row no-input">
						    <label class="fl">Claim Number</label>
						    <div class="fr"><%= count + 1 %></div>
						</div>
					</div>
					<div>
						<div class="row no-input">
						    <label class="fl">Reported Charge</label>
						    <div class="fr"><%= claimPaymentInfo.TotalClaimChargeAmount.ToCurrencyFormat() %></div>
						</div>
						<div class="row no-input">
						    <label class="fl">Remittance</label>
						    <div class="fr"><%= claimPaymentInfo.TotalClaimChargeAmount.ToCurrencyFormat()%></div>
						</div>
						<div class="row no-input">
						    <label class="fl">Line Adjustment Amount</label>
						    <div class="fr"><%= claimPaymentInfo.ServiceAdjustmentTotal.ToCurrencyFormat() %></div>
						</div>
						<div class="row no-input">
						    <label class="fl">Paid Amount</label>
						    <div class="fr"><%= claimPaymentInfo.ClaimPaymentAmount.ToCurrencyFormat() %></div>
						</div>
					</div>
				</div>
			</div>
                <%  if (!claimPaymentInfo.IsPosted && claimPaymentInfo.AssociatedClaims != null && claimPaymentInfo.AssociatedClaims.Count > 0){ %>
			<div class="episodes">
				<div class="acore-grid selectable">  
					<ul>
						<li>
					<%  if (Model.IsUserCanPost) { %>
					        <span class="grid-check"></span>
					<%  } %>
							<span class="grid-seventh">PatientId</span>
							<span class="grid-quarter">Patient Name</span>
							<span class="grid-fifth">Episode</span>
							<span class="grid-sixth">Claim Date</span>
							<span class="grid-seventh">Batch Id</span>
						</li>
					</ul>
					<ol>
					<%  foreach (var cl in claimPaymentInfo.AssociatedClaims) { %>
						<li>
						<%  if (Model.IsUserCanPost) { %>
						    <span class="grid-check"><%= string.Format("<input id=\"Episodes_{3}\" type=\"radio\" value=\"{0}|{1}|{2}\" name=\"Episodes{2}\" />", cl.EpisodeId, cl.BatchId, claimPaymentInfo.PayerClaimControlNumber, i) %></span>
						<%  } %>
							<span class="grid-seventh"><%= cl.PatientIdNumber%></span>
							<span class="grid-quarter"><%= cl.DisplayName%></span>
							<span class="grid-fifth"><%= cl.EpisodeRange%></span>
							<span class="grid-sixth"><%= cl.ClaimDateFormatted%></span>
							<span class="grid-seventh"><%= cl.BatchId%></span>
						</li>
					<%  } %>
					</ol>
				</div>
                    <%  if (Model.IsUserCanPost) { %>
				<ul class="buttons ac">
				    <li><a class="post">Post</a></li>
				</ul>
				    <%  } %>
			</div>
			    <%  } %>			
		</li>
                <%  count++; %>
				<%  zebra++; %>
			<% } %>
		<% } %>
	<% } %>
    </ol>
<%  } %>
</div>
