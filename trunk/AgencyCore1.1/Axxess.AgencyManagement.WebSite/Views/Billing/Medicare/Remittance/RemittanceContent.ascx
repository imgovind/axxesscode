﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RemittanceViewData>" %>
<%  if (Model.Service != AgencyServices.None && Model.List.Count > 0){ %>
<ul>
    <li>
        <span class="grid-twentieth"></span>
        <span class="grid-sixth">Remittance Id</span>
        <span class="grid-sixth">Remittance Date</span>
        <span class="grid-sixth">Payment Date</span>
        <span class="grid-seventh">Provider Payment</span>
        <span class="grid-seventh">Claim Count</span>
		<% if (Model.ViewDetailPermissions.Has(Model.Service) || ( Model.DeletePermissions.Has(Model.Service) && !Current.IsAgencyFrozen)){ %>
        <span class="grid-sixth">Action</span>
        <% } %>
    </li>
</ul>
<ol>
    <% int count = 1; %>
    <% foreach (var remittance in  Model.List) { %>
		<li>
			<span class="grid-twentieth"><%= count++ %>.</span>
			<span class="grid-sixth"><%= remittance.RemitId %></span>
			<span class="grid-sixth"><%= remittance.RemittanceDate.ToZeroFilled() %></span>
			<span class="grid-sixth"><%= remittance.PaymentDate.ToZeroFilled() %></span>
			<span class="grid-seventh"><%= remittance.PaymentAmount.ToCurrencyFormat() %></span>
			<span class="grid-seventh"><%= remittance.TotalClaims %></span>
			<span class="grid-sixth">
                <%  if (Model.ViewDetailPermissions.Has(Model.Service)){%> 
				<a href="javaScript:void(0);" onclick="Billing.Medicare.Remittance.Details.Open('<%= remittance.Id %>');">View Details</a>
				<%}%>
				<% if(Model.ViewDetailPermissions.Has(Model.Service) && Model.DeletePermissions.Has(Model.Service) && !Current.IsAgencyFrozen){ %>|<% } %>
				<% if (Model.DeletePermissions.Has(Model.Service) && !Current.IsAgencyFrozen) { %>
				<a href="javaScript:void(0);" onclick="Billing.Medicare.Remittance.Delete('<%= remittance.Id %>');">Delete</a>
				<% } %>
			</span>
		</li>
    <% } %>
</ol>
<% } %>