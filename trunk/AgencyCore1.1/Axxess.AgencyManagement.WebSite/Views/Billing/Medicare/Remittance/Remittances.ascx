﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RemittanceViewData>" %>
<span class="wintitle">Remittance Advices | <%= Current.AgencyName %></span>
<%  if (Model.Service != AgencyServices.None && Model.ViewListPermissions.Has(Model.Service)) { %>
<%  var pagename = "BillingRemittance"; %>
<div class="wrapper main">
	<%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" }) %>
	<ul class="fr buttons top ac">
    <%  if (Model.UploadPermissions.Has(Model.Service) && !Current.IsAgencyFrozen) { %>
	    <li><a class="upload">Upload</a></li><br />
	<%  } %>
	<%  if (Model.ExportPermissions.Has(Model.Service)) { %>
        <li><a url="Billing/RemittancesPdf" class="print">Print</a></li>
    <%  } %>
	</ul>
	<fieldset class="grid-controls ac">
		<div class="button fr"><a url="Billing/RemittanceContent" class="grid-refresh">Refresh</a></div>
		<div class="filter">
			<label for="<%= pagename %>_StartDate">Date Range</label>
			<input type="text" class="date-picker short" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
			<label for="<%= pagename %>_EndDate">&ndash;</label>
			<input type="text" class="date-picker short" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
		</div>
	</fieldset>
	<input type="hidden" disabled="disabled" name="ErrorMessageHead" value="No Remittances Found"/>
	<input type="hidden" disabled="disabled" name="ErrorMessageBody" value="No remittances could be found for this date range."/>
	<div id="<%= pagename %>GridContainer" class="content acore-grid">
		<%  Html.RenderPartial("Medicare/Remittance/RemittanceContent", Model); %>
	</div>
</div>
<%  } %>