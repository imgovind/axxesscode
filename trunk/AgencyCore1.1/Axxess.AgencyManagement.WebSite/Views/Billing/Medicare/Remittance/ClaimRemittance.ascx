﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PaymentInformation>>" %>
<div id="ClaimRemittance" class="acore-grid">
	<% if (Model != null && Model.Count > 0) { %>
		<ul><li class="ac"><h3></h3></li></ul>
		<ol>
			<% var count = 1; %>
			<% foreach (var claimPaymentInfo in Model){ %>
					<li class="no-hover">
						<div class="wide-column">
							<div class="inline-row four-wide">
								<div>
									<div class="row no-input"><label class="fl">Patient Name</label><div class="fr"><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty%></div></div>
									<div class="row no-input"><%  if (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty()) { %><label class="fl"><%= claimPaymentInfo.Patient.IdQualifierName%></label><label class="fr"><%= claimPaymentInfo.Patient.Id%></label><%  } %></div>
									<div class="row no-input"><label class="fl">Patient Control Number</label><div class="fr"><%= claimPaymentInfo.PatientControlNumber%></div></div>
								</div>
								<div>
									<div class="row no-input"><label class="fl">ICN Number</label><div class="fr"><%= claimPaymentInfo.PayerClaimControlNumber%></div></div>
									<div class="row no-input"><label class="fl">Start Date</label><div class="fr"><%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></div></div>
									<div class="row no-input"><label class="fl">End Date</label><div class="fr"><%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></div></div>
								</div>
								<div>
									<div class="row no-input"><label class="fl">Type Of Bill</label><div class="fr"><%= claimPaymentInfo.TypeOfBill%></div></div>
									<div class="row no-input"><label class="fl">Claim Status</label><div class="fr"><%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode)%></div></div>
									<div class="row no-input"><label class="fl">Claim Number</label><div class="fr"><%= count ++ %></div></div>
								</div>
								<div>
									<div class="row no-input"><label class="fl">Reported Charge</label><div class="fr"><%= claimPaymentInfo.TotalClaimChargeAmount.ToCurrencyFormat() %></div></div>
									<div class="row no-input"><label class="fl">Remittance</label><div class="fr"><%= claimPaymentInfo.TotalClaimChargeAmount.ToCurrencyFormat()%></div></div>
									<div class="row no-input"><label class="fl">Line Adjustment Amount</label><div class="fr"><%= claimPaymentInfo.ServiceAdjustmentTotal.ToCurrencyFormat() %></div></div>
									<div class="row no-input"><label class="fl">Paid Amount</label><div class="fr"><%= claimPaymentInfo.ClaimPaymentAmount.ToCurrencyFormat() %></div></div>
								</div>
							</div>
						</div>
					</li>
			<% } %>
		</ol>
	<% } else { %>
		<script type="text/javascript">$("#ClaimRemittance").replaceWith(U.MessageInfo("No Remittance Found", "There is no remittance information currenty related to this claim."));</script>
	<% } %>
</div>
