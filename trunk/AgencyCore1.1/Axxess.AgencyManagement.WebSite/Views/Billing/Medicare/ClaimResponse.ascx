﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Claim Response | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <fieldset>
        <pre><%= Model.Replace('\u0009'.ToString(), "    ") %></pre>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
</div>