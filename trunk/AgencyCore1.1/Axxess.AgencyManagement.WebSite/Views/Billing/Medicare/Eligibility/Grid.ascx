﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% Html.Telerik().Grid<MedicareEligibility>()
	   .Name("MedicareEligibility_Grid")
	   .Columns(columns =>
	   {
		   columns.Bound(c => c.TaskName).Title("Task").Width(25);
		   columns.Bound(c => c.EpisodeRange).Title("Episode").Width(25).Sortable(false);
		   columns.Bound(c => c.AssignedTo).Title("Assigned").Width(15);
		   columns.Bound(c => c.StatusName).Title("Status").Width(15);
		   columns.Bound(c => c.Created).Format("{0:MM/dd/yyyy}").Title("Date").Width(8);
		   columns.Bound(c => c.PrintUrl).Title("").Width(2).Sortable(false).Encoded(false).HtmlAttributes("centered-unpadded-cell");
	   }).DataBinding(d => d.Ajax().Select("EligibilityList", "Billing"))
	   .ClientEvents(events => events
		   .OnDataBinding("U.OnTGridDataBinding")
			.OnDataBound("U.OnTGridDataBound")
			.OnError("U.OnTGridError"))
       .Sortable().Scrollable().NoRecordsTemplate("No Eligibilities found.").Footer(false)
	   .Render(); %>
