﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Medicare Eligibility Report | <%= Current.AgencyName %></span>
<div id="MedicareEligiblity_Report" class="main wrapper blue">
    <fieldset class="grid-controls ac">
	    <div class="filter">
			<label for="PatientId">Patient</label>
			<input type="text" name="PatientId" id="MedicareEligiblity_Report_Patients" class="patient-picker required" service="1"/>
		</div>
		<div class="button fr"><a class="grid-refresh">Refresh</a></div>
	</fieldset>
</div>
<div id="MedicareEligiblity_ReportContent">
	<% Html.RenderPartial("Medicare/Eligibility/Grid", null); %>
</div>


