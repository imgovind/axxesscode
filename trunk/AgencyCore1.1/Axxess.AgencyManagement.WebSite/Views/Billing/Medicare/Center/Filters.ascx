﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<div class="center-filter">
    <label class="fl">Branch</label>
    <div class="filter-fill"><%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.Service, true, new { @class = "filterInput", @id = "BillingHistory_BranchId" })%></div>
</div>
<div class="center-filter">
    <label class="fl">View</label>
    <div class="filter-fill">
        <select name="StatusId" class="filterInput">
            <option value="1">Active Patients</option>
            <option value="2">Discharged Patients</option>
        </select>
    </div>
</div>
<div class="center-filter">
    <label class="fl">Filter</label>
    <div class="filter-fill"><%= Html.Insurances("InsuranceId", Model.SubId.ToString(), (int)Model.Service, false, false, "", new { @class = "filterInput activity-input insurance", @id = "BillingHistory_InsuranceId" })%></div>
</div>
<div class="center-filter">
    <label class="fl">Find</label>
    <div class="filter-fill text"><input class="searchtext text filterInput" name="Name" value="" type="text" /></div>
</div>
