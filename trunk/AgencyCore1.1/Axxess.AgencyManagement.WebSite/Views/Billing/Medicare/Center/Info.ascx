﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
<% if (!Current.IsAgencyFrozen && Model.IsUserCanAdd){  %>
<ul class="window-menu">
	<li><a onclick="Billing.Medicare.Claim.New('<%=Model.PatientId %>','<%=(int)ClaimTypeSubCategory.RAP %>')">New RAP</a></li>
	<li><a onclick="Billing.Medicare.Claim.New('<%=Model.PatientId %>','<%=(int)ClaimTypeSubCategory.Final %>')">New Final</a></li>
</ul>
<% } %>
<div id="billingHistoryClaimData" class="wrapper main claim-information">
    <div class="boundary">
        <div class="reports fr">
            <h5 class="reports-head">Quick Reports</h5>
            <ul>
                <%if (Model.IsUserCanViewRemittance){%>
					<li><%= string.Format("<a onclick=\"Billing.Medicare.Claim.Remittance('{0}','{1}')\">Remittance</a>", Model.Id, Model.Type)%></li>
                <% } %>
                <% if (Model.IsUserCanViewList && Model.Type.Equals("Final")){ %>
					<li><%= string.Format("<a onclick=\"Billing.Secondary.Center.Open('{0}','{1}')\">Secondary Claims</a>", Model.Id, Model.PatientId) %></li>
                <% } %>
                <%if (Model.IsUserCanViewLog) {%>
					<li><%= string.Format("<a onclick=\"Log.LoadClaimLog('{0}','{1}','{2}')\">Activity Logs</a>", Model.Type, Model.Id, Model.PatientId)%></li>
                <% } %>
            </ul>
        </div>
        <div class="billing-info">
            <h2 class="billing ac"><%= Model.Type %></h2>
            <%= Model.PatientName.IsNotNullOrEmpty() ? "<div>Patient Name<div class='fr'>" + Model.PatientName + "</div></div>" : string.Empty%>
            <%= Model.PatientIdNumber.IsNotNullOrEmpty() ? "<div>MRN<div class='fr'>" + Model.PatientIdNumber + "</div></div>" : string.Empty%>
            <%= Model.MedicareNumber.IsNotNullOrEmpty() ? "<div>Medicare Number<div class='fr'>" + Model.MedicareNumber + "</div></div>" : string.Empty%>
            <%= Model.PayorName.IsNotNullOrEmpty() ? "<div>Insurance/Payor<div class='fr'>" + Model.PayorName + "</div></div>" : string.Empty%>
<% if (Model.Visible) { %>
            <%= Model.HIPPS.IsNotNullOrEmpty() ? "<div>HIPPS<div class='fr'>" + Model.HIPPS + "</div></div>" : string.Empty %>
            <%= Model.ClaimKey.IsNotNullOrEmpty() ? "<div>Claim Key<div class='fr'>" + Model.ClaimKey + "</div></div>" : string.Empty%>
            <%= Model.ProspectivePayment.Hhrg.IsNotNullOrEmpty() ? "<div>HHRG (Grouper)<div class='fr'>" + Model.ProspectivePayment.Hhrg + "</div></div>" : string.Empty%>
            <%= Model.ProspectivePayment.LaborAmount.IsNotNullOrEmpty() ? "<div>Labor Portion<div class='fr'>" + Model.ProspectivePayment.LaborAmount + "</div></div>" : string.Empty%>
            <%= Model.ProspectivePayment.NonLaborAmount.IsNotNullOrEmpty() ? "<div>Non-Labor<div class='fr'>" + Model.ProspectivePayment.NonLaborAmount + "</div></div>" : string.Empty%>
            <%= Model.ProspectivePayment.NonRoutineSuppliesAmount.IsNotNullOrEmpty() ? "<div>Supply Reimbursement<div class='fr'>" + Model.ProspectivePayment.NonRoutineSuppliesAmount + "</div></div>" : string.Empty%>
            <%= Model.ProspectivePayment.TotalProspectiveAmount.IsNotNullOrEmpty() ? "<div>Episode Prospective Pay<div class='fr'>" + Model.ProspectivePayment.TotalProspectiveAmount + "</div></div>" : string.Empty%>
            <%= Model.ProspectivePayment.ClaimAmount.IsNotNullOrEmpty() ? "<div>Claim Prospective Pay<div class='fr'>" + Model.ProspectivePayment.ClaimAmount + "</div></div>" : string.Empty%>
<% } %>
		<% if (Model.IsCreateSecondaryClaimButtonVisible && Model.IsUserCanAdd) { %>
            <ul class="buttons ac">
                <li><a onclick="Billing.Secondary.Claim.New('<%= Model.EpisodeId %>','<%= Model.PatientId %>','<%= Model.Id %>')">Create Secondary Claim</a></li>
            </ul>
		<% } %>
        </div>
        <div class="clr"></div>
    </div>
</div>