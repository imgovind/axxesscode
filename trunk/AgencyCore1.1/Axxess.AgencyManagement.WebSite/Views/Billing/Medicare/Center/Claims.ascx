﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="abs above ac">
    <div class="fr">
        <a class="grid-resize img grid-expand"></a>
    </div>
</div>
<%  var val = !Model.IsEmpty() ? Model : Guid.Empty; %>
<%  Html.Telerik().Grid<ClaimHistoryLean>().Name("BillingHistoryActivity_Grid").HtmlAttributes(new { @class = "activity-container scrollable" }).Columns(columns => {
		columns.Bound(p => p.TypeName).Title("Type").Width(50);
		columns.Bound(p => p.EpisodeStartDate).ClientTemplate("<#= U.FormatGridDate(EpisodeStartDate) #>&#8211;<#=U.FormatGridDate(EpisodeEndDate)#>").Title("Episode Range").Width(210);
		columns.Bound(p => p.StatusName).Title("Status").Width(150);
		columns.Bound(p => p.ClaimAmount).Title("Claim Amount").Format("${0:#0.00}").Width(100);
		columns.Bound(p => p.PaymentAmount).Title("Payment Amount").Format("${0:#0.00}").Width(120);
		columns.Bound(p => p.PaymentDate).ClientTemplate("<#= U.FormatGridDate(PaymentDate) #>").Title("Payment Date").Width(100);
        columns.Bound(p => p.IsUserCanPrint).Title("").ClientTemplate("").HtmlAttributes(new { @class = "ubprint" }).HeaderHtmlAttributes(new { @class = "ubprint" }).Width(75);
        columns.Bound(p => p.IsUserCanPrint).Title("").ClientTemplate("").HtmlAttributes(new { @class = "print" }).HeaderHtmlAttributes(new { @class = "print" }).Width(35);
        columns.Bound(p => p.Id).ClientTemplate("").Title("Action").HtmlAttributes(new { @class = "action" }).HeaderHtmlAttributes(new { @class = "action" }).Width(95).Visible(!Current.IsAgencyFrozen);
		columns.Bound(p => p.PatientId).Hidden(true).HtmlAttributes(new {@class = "pid" });
        columns.Bound(p => p.Id).Hidden(true).HtmlAttributes(new { @class = "cid" });
        columns.Bound(p => p.Type).Hidden(true).HtmlAttributes(new { @class = "ctype" });
	}).DetailView(details => details.ClientTemplate(
		Html.Telerik().Grid<ClaimSnapShotViewData>().HtmlAttributes(new { @class = "position-relative" }).Name("ClaimSnapShot_<#= Id #><#= TypeName #>").DataBinding(dataBinding => dataBinding.Ajax().Select("SnapShotClaims", "Billing", new {
		    Id = "<#=Id#>",
		    Type = "<#=TypeName#>"
		}).Update("UpdateSnapShotClaim", "Billing")).DataKeys(keys => {
			keys.Add(r => r.Id).RouteKey("Id");
            keys.Add(r => r.BatchId).RouteKey("BatchId");
			keys.Add(r => r.Type).RouteKey("TypeName");
		}).ClientEvents(events => events
            .OnDataBound("Billing.Medicare.Center.OnClaimSnapShotDataBound")
			.OnDataBinding("U.OnTGridDataBinding")
        ).Columns(columns => {
			columns.Bound(o => o.BatchId).Title("Batch Id").Width(80).ReadOnly();
			columns.Bound(o => o.EpisodeRange).Width(150);
			columns.Bound(o => o.ClaimDate).Width(140).Title("Claim Date").ReadOnly();
			columns.Bound(o => o.PaymentDate).ClientTemplate("<#= U.FormatGridDate(PaymentDate) #>").Width(100).Title("Payment Date");
			columns.Bound(o => o.PaymentAmount).Format("${0:#0.00}").Title("Payment Amount").Width(130);
			columns.Bound(o => o.Status).ClientTemplate("<label><#= StatusName #></label>").Width(80);
			columns.Bound(o => o.IsUserCanEdit).ClientTemplate("<#if(IsUserCanEdit)#><a class=\"link\" onclick=\"Billing.Medicare.Claim.BatchEdit('<#=Id#>','<#=BatchId#>','<#=TypeName#>')\">Update</a>").Title("Action").Width(80).Visible(!Current.IsAgencyFrozen);
		}).Footer(false).ToHtmlString())
    ).DataBinding(dataBinding => dataBinding.Ajax().Select("HistoryActivity", "Billing", new { PatientId = val, InsuranceId = 0 })).ClientEvents(events => events
	    .OnDataBound("Billing.Medicare.Center.OnClaimDataBound")
        .OnRowDataBound("Billing.Medicare.Center.OnClaimRowDataBound")
	    .OnRowSelect("Billing.Medicare.Center.OnClaimRowSelected")
	    .OnDataBinding("Billing.Medicare.Center.OnClaimDataBinding")
	    .OnDetailViewCollapse("U.OnGridDetailViewCollapse")
	    .OnDetailViewExpand("U.OnGridDetailViewExpand")
    ).Sortable().NoRecordsTemplate(" ").Selectable().Scrollable().Footer(false).Render(); %>