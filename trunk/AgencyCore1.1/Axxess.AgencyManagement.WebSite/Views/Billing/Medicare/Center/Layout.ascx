﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Billing History | <%= Current.AgencyName %></span>
<%if(Model.Service!=AgencyServices.None && Model.AvailableService.Has(Model.Service)){ %>

<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("Medicare/Center/Filters", Model); %></div>
        <div class="bottom"><% Html.RenderPartial("Medicare/Center/Patients", Model); %></div>
    </div>
    <div id="MedicareBillingMainResult" class="ui-layout-center"><% Html.RenderPartial("Medicare/Center/Content", Guid.Empty); %></div>
</div>
<%} %>