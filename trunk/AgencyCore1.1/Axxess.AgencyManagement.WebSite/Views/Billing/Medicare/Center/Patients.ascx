﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%= Html.Hidden("PatientId", Guid.Empty, new { @id = "BillingHistory_PatientId", @class = "activity-input" }) %>
<%  Html.Telerik().Grid<PatientSelection>().Name("BillingSelectionGrid").HtmlAttributes(new { @class = "patientlist-container" }).Columns(columns => {
        columns.Bound(p => p.LastName).HtmlAttributes(new { @class = "searchL" }); 
        columns.Bound(p => p.ShortName).Title("First Name/MI").HtmlAttributes(new { @class = "searchF" });
        columns.Bound(p => p.Id).HtmlAttributes(new { @class = "pid" }).Hidden(true);
    }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("AllMedicare", "PatientProfile", new { BranchId = Model.Id, StatusId = Model.Status, InsuranceId = Model.SubId, Name = string.Empty })).Sortable().Selectable().Scrollable().Footer(false).NoRecordsTemplate(" ").ClientEvents(events => events
        .OnDataBound("Billing.Medicare.Center.PatientListDataBound")
        .OnDataBinding("U.OnTGridDataBinding")
        .OnRowSelect("Billing.Medicare.Center.OnPatientRowSelected")
    ).Render(); %>