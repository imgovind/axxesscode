﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Pending Claims | <%= Current.AgencyName %></span>
<%  if (Model.Service != AgencyServices.None && Model.ViewListPermissions.Has(Model.Service)) { %>
    <%  string pagename = "PendingClaim"; %>
<div class="wrapper main blue grid-bg">    
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="button fr transistion"><a class="refresh">Refresh</a></div>
        <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" }) %> 
        <div class="filter">
            <label for="<%= pagename %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(), (int)Model.Service, new { @id = pagename + "_BranchId", @class = "input location rebind" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_InsuranceId">Insurance</label>
             <%= Html.InsurancesMedicare("InsuranceId", Model.SubId.ToString(), false, "", new { @id = pagename + "_InsuranceId", @class = "input insurance medicare not-zero zeroprefix "+ AgencyServices.HomeHealth.ToString().ToLowerCase() })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_Type">Claim Type</label>
			<%  var types = new List<SelectListItem>();  %>
			<%  types.Add(new SelectListItem() { Text = "Rap", Selected = true, Value = "0" }); %>
			<%  types.Add(new SelectListItem() { Text = "Final", Selected = false, Value = "1" }); %>
            <%= Html.DropDownList("Type", types, new { @id = pagename + "_Type", @class = "claimtype" }) %>
        </div>
    </fieldset>
</div>
<div id="<%= pagename %>_rapContent" class="rapcontent"><% Html.RenderPartial("Medicare/Pending/ClaimRap",Model); %></div>
<div id="<%= pagename %>_finalContent" class="finalcontent"><% Html.RenderPartial("Medicare/Pending/ClaimFinal", Model); %></div>
<%  } %>