﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%  var isVisible = !Current.IsAgencyFrozen && Model.EditPermissions.Has(Model.Service);%>
<%= Html.Telerik().Grid<PendingClaimLean>().HtmlAttributes(new { @class = "grid-container", name="RAP" }).ToolBar(t => t.Template("<h3>Rap(s)</h3>")).Name("Raps").Columns(columns => {
        columns.Bound(e => e.PaymentDate).ClientTemplate("<#= U.FormatGridDate(PaymentDate) #>").Title("Payment Date").Width(90);
        columns.Bound(e => e.DisplayName).Title("Patient").ReadOnly();
        columns.Bound(e => e.MedicareNumber).Title("Medicare #").Width(80).ReadOnly();
        columns.Bound(e => e.EpisodeRange).Width(180).ReadOnly();
        columns.Bound(e => e.ClaimAmount).Format("${0:#0.00}").Width(130).ReadOnly();
        columns.Bound(e => e.Status).ClientTemplate("<label><#= StatusName #></label>");
        columns.Bound(e => e.PaymentAmount).Format("${0:#0.00}").Width(130);
        columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Medicare.Claim.Edit('<#= PatientId #>','<#= Id #>','" + (int)ClaimTypeSubCategory.RAP + "')\">Update</a>").Title("Action").Width(70).Visible(isVisible);
    }).DetailView(details => details.ClientTemplate(
        Html.Telerik().Grid<RapSnapShot>().HtmlAttributes(new { @class = "position-relative" }).Name("RapSnapShot_<#= Id #>").DataKeys(keys => { keys.Add(r => r.Id).RouteKey("Id"); keys.Add(r => r.BatchId).RouteKey("BatchId"); }).Columns(columns => {
            columns.Bound(o => o.BatchId).Title("Batch Id").Width(80).ReadOnly();
            columns.Bound(o => o.EpisodeRange).Width(180).ReadOnly();
            columns.Bound(o => o.ClaimDate).Format("{0:MM/dd/yyyy hh:mm tt}").Width(150);
            columns.Bound(o => o.PaymentDate).ClientTemplate("<#= U.FormatGridDate(PaymentDate) #>").Title("Payment Date").Width(110);
            columns.Bound(o => o.Payment).Title("Payment Amount").Format("${0:#0.00}").Width(110);
            columns.Bound(o => o.Status).ClientTemplate("<label><#= StatusName #></label>");
            columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Medicare.Claim.BatchEdit('<#= Id #>','<#= BatchId #>','" + (int)ClaimTypeSubCategory.RAP + "')\">Update</a>").Title("Action").Width(70).Visible(isVisible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RapSnapShots", "Billing", new { Id = "<#= Id #>" })).Footer(false).ToHtmlString())
    ).DataBinding(dataBinding => dataBinding.Ajax().Select("PendingClaimRaps", "Billing", new { branchId = Model.Id, insuranceId = Model.SubId })).ClientEvents(events => events
        .OnDetailViewCollapse("U.OnGridDetailViewCollapse")
        .OnDetailViewExpand("U.OnGridDetailViewExpand")
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("U.OnTGridDataBound")
        .OnRowDataBound("U.OnRowDataBoundHideEmptyHierarchy")
        .OnError("U.OnTGridError")
    ).NoRecordsTemplate("No RAPs found.").Sortable().Scrollable().Footer(false) %>