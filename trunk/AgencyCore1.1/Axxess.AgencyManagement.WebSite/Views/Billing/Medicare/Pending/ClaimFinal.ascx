﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<%  var isVisible = !Current.IsAgencyFrozen && Model.EditPermissions.Has(Model.Service);%>
<%= Html.Telerik().Grid<PendingClaimLean>().HtmlAttributes(new { @style = "display:none;", @class = "grid-container", name = "Final" }).ToolBar(t => t.Template("<h3>Final(s)</h3>")).Name("Finals").Columns(columns => {
        columns.Bound(e => e.PaymentDate).ClientTemplate("<#= U.FormatGridDate(PaymentDate) #>").Title("Payment Date").Width(90);
        columns.Bound(e => e.DisplayName).Title("Patient").ReadOnly();
        columns.Bound(e => e.MedicareNumber).Title("Medicare #").Width(80).ReadOnly();
        columns.Bound(e => e.EpisodeRange).Width(180).ReadOnly();
        columns.Bound(e => e.ClaimAmount).Format("{0:$#0.00;-$#0.00}").Width(130).ReadOnly();
        columns.Bound(e => e.Status).ClientTemplate("<label><#= StatusName #></label>");
        columns.Bound(e => e.PaymentAmount).Format("{0:$#0.00;-$#0.00}").Width(130);
        columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Medicare.Claim.Edit('<#=PatientId#>','<#=Id#>','" + (int)ClaimTypeSubCategory.Final + "')\">Update</a>").Title("Action").Width(70).Visible(isVisible);
    }).DetailView(details => details.ClientTemplate(
        Html.Telerik().Grid<FinalSnapShot>().HtmlAttributes(new { @style = "position:relative;" }).Name("FinalSnapShot_<#= Id #>").Columns(columns => {
            columns.Bound(o => o.BatchId).Title("Batch Id").Width(80).ReadOnly();
            columns.Bound(o => o.EpisodeRange).Width(180).ReadOnly();
            columns.Bound(o => o.ClaimDate).Format("{0:MM/dd/yyyy hh:mm tt}").Width(150).Title("Claim Date").ReadOnly();
            columns.Bound(o => o.PaymentDate).ClientTemplate("<#= U.FormatGridDate(PaymentDate) #>").Width(90).Title("Payment Date");
            columns.Bound(o => o.Payment).Title("Payment Amount").Format("{0:$#0.00;-$#0.00}").Width(130);
            columns.Bound(o => o.Status).ClientTemplate("<label><#= StatusName #></label>");
            columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Medicare.Claim.BatchEdit('<#=Id#>','<#=BatchId#>','" + (int)ClaimTypeSubCategory.Final + "')\">Update</a>").Title("Action").Width(70).Visible(isVisible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("FinalSnapShots", "Billing", new { Id = "<#= Id #>" })).Footer(false).ToHtmlString())
    ).DataBinding(dataBinding => dataBinding.Ajax().Select("PendingClaimFinals", "Billing", new { branchId = Model.Id, insuranceId = Model.SubId })).ClientEvents(events => events
        .OnDetailViewCollapse("U.OnGridDetailViewCollapse")
        .OnRowDataBound("U.OnRowDataBoundHideEmptyHierarchy")
        .OnDetailViewExpand("U.OnGridDetailViewExpand")
        .OnDataBinding("U.OnTGridDataBinding")
        .OnDataBound("U.OnTGridDataBound")
        .OnError("U.OnTGridError")
    ).NoRecordsTemplate("No Finals found.").Sortable().Scrollable().Footer(false) %>