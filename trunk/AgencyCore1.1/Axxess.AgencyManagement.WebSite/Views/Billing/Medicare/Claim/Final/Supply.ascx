﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimSupplyViewData>" %>
<div id="supplyTab" class="wrapper main">
<%  using (Html.BeginForm("SupplyVerify", "Billing", FormMethod.Post, new { @id = "finalBillingSupplyForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "FinalVerification_Id", @class = "input" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "FinalVerification_PatientId", @class = "input" })%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "FinalVerification_EpisodeId", @class = "input" })%>
    <%= Html.Hidden("Type", Model.Type, new { @id = "FinalVerification_Type", @class = "input" })%>
	<h3 class="ac">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy") %> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy") %></h3>
    <fieldset>
	    <div class="wide-column">
			<div class="row">
				<ul class="checkgroup one-wide">
				    <%= Html.CheckgroupOption("IsSupplyNotBillable", "true", false, "Check this box if you do not want to bill for supplies.<em>(This removes all supplies from the claim)</em>") %>
				</ul>
	        </div>
			<div class="row">
				<div id="FinalBillingSupplyContent">
					<ul class="buttons fl ac">
						<li><a class="add-supply-button">Add New Supply</a></li>
						<li><a class="unbill-supply-button">Mark As Non-Billable</a></li>
						<li><a class="delete-supply-button">Delete</a></li>
					</ul>
                    <label class="fr">Note: <em>Click on the checkbox(es) to make the appropriate selection.</em></label>
					<div class="clr"></div>
					<%= Html.Telerik().Grid<Supply>(Model.BilledSupplies).Name("FinalBillingSupplyGrid").DataKeys(keys => keys.Add(c => c.BillingId).RouteKey("BillingId")).ToolBar(t => t.Template("<h3>Billable Supplies</h3>")).HtmlAttributes(new { @class = string.Format("{0}{1}{2}supply position-relative", Model.Type.ToString().ToLowerCase(), Model.Service.ToString().ToLowerCase(), "billable") }).Columns(columns => {
							columns.Bound(s => s.BillingId).ClientTemplate("<input name='BillingId' type='checkbox'  value='<#= BillingId #>' />").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Template(s => s.IdCheckbox).Title("").Width(30).Sortable(false);
							columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(110);
							columns.Bound(s => s.Description).Title("Description");
							columns.Bound(s => s.Code).Title("HCPCS").Width(70);
							columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
							columns.Bound(s => s.Quantity).Title("Unit").Width(50);
							columns.Bound(s => s.UnitCost).Format("{0:$#0.00;-$#0.00}").Title("Unit Cost").Width(80);
							columns.Bound(s => s.TotalCost).Format("{0:$#0.00;-$#0.00}").Title("Total Cost").Width(85);
							columns.Bound(s => s.BillingId).ClientTemplate("<a onclick=\"Billing.Medicare.Claim.Final.Supply.Edit('" + Model.Id + "','" + Model.PatientId + "','<#= BillingId #>',true)\">Edit</a>").Sortable(false).Template(s => "<a onclick=\"Billing.Medicare.Claim.Final.Supply.Edit('" + Model.Id + "','" + Model.PatientId + "','" + s.BillingId + "',true)\">Edit</a>").Width(70).Title("Action");
						}).DataBinding(dataBinding => dataBinding.Ajax().Select("SupplyBillable", "Billing", new { Id = this.Model.Id, patientId = this.Model.PatientId, Type = "Final" })).NoRecordsTemplate("<h3>No Billable Supplies found.</h3>").ClientEvents(events => events
							.OnDataBinding("U.OnTGridDataBinding")
                            .OnDataBound("U.OnTGridDataBound")
							.OnError("U.OnTGridError")					
							.OnRowSelect("Billing.Supply.RowSelected")
					    ).Sortable().Selectable().Scrollable().Footer(false) %>
				</div>
			</div>
			<div class="row">
				<div id="FinalUnBillingSupplyContent">
					<ul class="buttons fl ac">
						<li><a class="bill-supply-button">Mark As Billable</a></li>
						<li><a class="delete-supply-button">Delete</a></li>
					</ul>
					<label class="fr">Note: <em>Click on the checkbox(es) to make the appropriate selection.</em></label>
					<div class="clr"></div>
					<%= Html.Telerik().Grid<Supply>(Model.UnbilledSupplies).Name("FinalUnBillingSupplyGrid").DataKeys(keys => keys.Add(c => c.BillingId).RouteKey("BillingId")).ToolBar(t => t.Template("<h3>Non-Billable Supplies</h3>")).HtmlAttributes(new { @class = string.Format("{0}{1}{2}supply position-relative", Model.Type.ToString().ToLowerCase(), Model.Service.ToString().ToLowerCase(), "unbillable") }).Columns(columns => {
							columns.Bound(s => s.BillingId).ClientTemplate("<input name='BillingId' type='checkbox'  value='<#= BillingId #>' />").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Template(s => s.IdCheckbox).ReadOnly().Title("").Width(30).Sortable(false);
							columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(110);
							columns.Bound(s => s.Description).Title("Description");
							columns.Bound(s => s.Code).Title("HCPCS").Width(70);
							columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(115);
							columns.Bound(s => s.Quantity).Title("Unit").Width(50);
							columns.Bound(s => s.UnitCost).Format("{0:$#0.00;-$#0.00}").Title("Unit Cost").Width(80);
							columns.Bound(s => s.TotalCost).Format("{0:$#0.00;-$#0.00}").Title("Total Cost").Width(85);
							columns.Bound(s => s.BillingId).ClientTemplate("<a onclick=\"Billing.Medicare.Claim.Final.Supply.Edit('" + Model.Id + "','" + Model.PatientId + "','<#= BillingId #>',false)\">Edit</a>").Sortable(false).Template(s => "<a onclick=\"Billing.Medicare.Claim.Final.Supply.Edit('" + Model.Id + "','" + Model.PatientId + "','" + s.BillingId + "',false)\">Edit</a>").Width(70).Title("Action");
						}).DataBinding(dataBinding => dataBinding.Ajax().Select("SupplyUnBillable", "Billing", new { Id = this.Model.Id, patientId = this.Model.PatientId, Type = "Final" })).NoRecordsTemplate("<h3>No Non-Billable Supplies found.</h3>").ClientEvents(events => events
							.OnDataBinding("U.OnTGridDataBinding")
							.OnDataBound("U.OnTGridDataBound")
							.OnError("U.OnTGridError")
							.OnRowSelect("Billing.Supply.RowSelected")
					    ).Sortable().Selectable().Scrollable().Footer(false) %>
				</div>
			</div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="save">Verify and Next</a></li>
    </ul>
<%  } %>
</div>
