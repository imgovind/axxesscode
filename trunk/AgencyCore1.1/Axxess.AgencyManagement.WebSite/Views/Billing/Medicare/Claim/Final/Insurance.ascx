﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("InsuranceVerify", "Billing", FormMethod.Post, new { @id = "finalBillingInsuranceForm" })) { %>
	<%= Html.Hidden("Id", Model.Id, new { @id = "Final_Id", @class = "input" })%>
	<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Final_PatientId", @class = "input" })%>
	<%= Html.Hidden("ServiceId", (int)AgencyServices.HomeHealth, new { @id = "Final_ServiceId", @class = "input" })%>
	<%= Html.Hidden("TypeId", (int)ClaimTypeSubCategory.Final, new { @id = "Final_TypeId", @class = "input" })%>
	<%= Html.Hidden("PayorType", Model.PayorType, new { @id = "Final_PayorType", @class = "input" })%>
	<%= Html.Hidden("PrimaryInsuranceId", Model.PrimaryInsuranceId, new { @id = "Final_PrimaryInsuranceId", @class = "input" })%>
	<%  if (Model.PrimaryInsuranceId >= 1000) { %>
	<fieldset>
		<div id="Final_InsuranceContent" class="primaryinsurancecontent">
			<% Html.RenderPartial("InsuranceInfoContent", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Relationship = Model.Relationship, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "Final" }); %>
		</div>
	</fieldset>
	<%  } %>
	<%  if (Model.InvoiceType == (int)InvoiceType.UB) { %>
	<%  Html.RenderPartial("Locator/UB04AllLocator", Model, new ViewDataDictionary { { "IdPrefix", "Final" } }); %>
	<%  } %>
	<%  var billData = Model.VisitRates ?? new List<ChargeRate>(); %>
	<fieldset>
		<legend>Visit Rates</legend>
		<div class="wide-column">
			<div class="row">
				<div class="ancillary-button fr"><a class="newrate">Add Visit Information</a></div>
			</div>
			<div class="row">
			    <%= Html.Telerik().Grid<ChargeRate>(billData).HtmlAttributes(new { @class = string.Format("position-relative {0}-{1}  claim-rates",AgencyServices.HomeHealth.ToString().ToLowerCase(),"final" )}).Name("FinalBillRate_Grid").DataKeys(keys => keys.Add(r => r.Id).RouteKey("Id")).Columns(columns => {
						columns.Bound(e => e.DisciplineTaskName).Title("Task");
						columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
						columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
						columns.Bound(e => e.Code).Title("HCPCS").Width(55);
						columns.Bound(e => e.Charge).Format("{0:$#0.00;-$#0.00}").Title("Rate").Width(45);
						columns.Bound(e => e.Modifiers).Title("Modifiers").Width(100);
						if (Model.PrimaryInsuranceId >= 1000)
						{
							columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
							columns.Bound(e => e.TimeLimit).Template(t => t.IsTimeLimit && t.ChargeType != "1" ? t.TimeLimit.ToHourMinTime() : string.Empty).ClientTemplate("<#= IsTimeLimit && TimeLimitHour != 0 && TimeLimitMin != 0 && ChargeType != \"1\" ? $.telerik.formatString('{0:H:mm}', TimeLimit) : '' #>").Title("Time Limit").Width(65);
						}
						columns.Bound(e => e.Id).Template(t => "<a class=\"edit link\">Edit</a> | <a class=\"delete link\">Delete</a> ").ClientTemplate("<a class=\"edit link\">Edit</a> | <a class=\"delete link\">Delete</a>").HtmlAttributes(new { @class = "action" }).Title("Action").Width(90).Sortable(false);
						columns.Bound(e => e.Id).Hidden(true).HtmlAttributes(new { @class = "rid" });
					}).DataBinding(dataBinding => dataBinding.Ajax().Select("FinalClaimInsuranceRates", "Billing", new {  Id = Model.Id })).ClientEvents(events => events
                        .OnDataBound("Billing.Rate.OnDataBound")
			        ).Sortable().Footer(false) %>
			</div>
        </div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="back">Back</a></li>
	<%  if (Model.IsFinalInfoVerified) { %>
		<li><a class="save">Verify and Next</a></li>
	<%  } %>
	</ul>
<%  } %>
</div>
