﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("VisitVerify", "Billing", FormMethod.Post, new { @id = "billingVisitForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "FinalVerification_Id", @class = "input" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "FinalVerification_PatientId", @class = "input" })%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "FinalVerification_EpisodeId", @class = "input" })%>
    <div class="acore-grid selectable">
		<h3 class="ac">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy")%> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy")%></h3>
    <%  if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) { %>
        <%	foreach (var billCategoryKey in Model.BillVisitDatas.Keys) { %>
            <%	var billCategoryVisits = Model.BillVisitDatas[billCategoryKey]; %>
            <%	var isBillable = billCategoryKey == BillVisitCategory.Billable; %>
            <%	if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
		<ul>
		    <li class="ac"><h3><%= billCategoryKey.GetDescription() %></h3></li>
		    <li>
				<span class="grid-twentieth"></span>
				<span class="grid-fifth">Visit Type</span>
				<span class="grid-seventh">Scheduled Date</span>
				<span class="grid-tenth">Visit Date</span>
				<span class="grid-eleventh">HCPCS</span>
				<span class="grid-fifth">Status</span>
				<span class="grid-eleventh">Units</span>
				<span class="grid-eleventh">Charge</span>
			</li>
		</ul>
                <%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
                    <%	var visits = billCategoryVisits[disciplineVisitKey]; %>
                    <%	if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="group-header no-hover">
                <input class="discpilinegeneral" name="Visit" type="checkbox" id="disciplineVisitKey_<%= disciplineVisitKey %>" />
				<label class="strong" for="disciplineVisitKey_<%= disciplineVisitKey %>"><%= disciplineVisitKey.GetDescription() %></label>
				<div class="clr"></div>
			</li>
			            <%  int i = 1; %>
						<%  foreach (var visit in visits) { %> 
			<li>
				<span class="grid-twentieth"><input name="Visit" type="checkbox" id="<%= "Visit" + visit.EventId.ToString() %>" value="<%= visit.EventId.ToString() %>" <%= isBillable.ToChecked() %> /><%= i%>.</span>
				<span class="grid-fifth"><%= visit.DisciplineTaskName%></span>
				<span class="grid-seventh"><%= visit.EventDate.IsValid() ? visit.EventDate.ToString("MM/dd/yyy") : string.Empty%></span>
				<span class="grid-tenth"><%= visit.VisitDate.IsValid() ? visit.VisitDate.ToString("MM/dd/yyy") : string.Empty%></span>
				<span class="grid-eleventh"><%= visit.HCPCSCode %></span>
				<span class="grid-fifth"><%= visit.StatusName %></span>
				<span class="grid-eleventh"><%= visit.Unit %></span>
				<span class="grid-eleventh"><%= string.Format("{0:$#0.00;-$#0.00}", visit.Charge)%></span>
			</li>
							<%  i++; %>
						<%  } %>
		</ol>
                    <%  } %>
                <%	} %>
            <%	} %>
        <%	} %>
    <%	} %>
	</div>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="save">Verify and Next</a></li>
    </ul>
<%  } %>
</div>