﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
    <ul class="buttons ac">
        <%= string.Format("<li><a onclick=\"U.GetAttachment('Billing/FinalPdf',{{episodeId:'{0}',patientId:'{1}'}})\">Print</a></li>", Model.EpisodeId, Model.PatientId) %>
    </ul>
	<fieldset>
		<div class="column">
			<div class="row no-input">
				<label class="fl">Patient First Name</label>
				<div class="fr light"><%= Model.FirstName%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Patient Last Name</label>
				<div class="fr light"><%= Model.LastName%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Medicare #</label>
				<div class="fr light"><%= Model.MedicareNumber%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Patient Record #</label>
				<div class="fr light"><%= Model.PatientIdNumber%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Gender</label>
				<div class="fr light"><%= Model.Gender%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Date of Birth</label>
				<div class="fr light"><%= Model.DOB.ToZeroFilled() %></div>
			</div>
		</div>
		<div class="column">
			<div class="row no-input">
				<label class="fl">Episode Start Date</label>
				<div class="fr light"><%= Model.EpisodeStartDate.ToZeroFilled() %></div>
			</div>
			<div class="row no-input">
				<label class="fl">Admission Date</label>
				<div class="fr light"><%= Model.StartofCareDate.ToZeroFilled() %></div>
			</div>
			<div class="row no-input">
				<label class="fl">Address Line 1</label>
				<div class="fr light"><%= Model.AddressLine1%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Address Line 2</label>
				<div class="fr light"><%= Model.AddressLine2%></div>
			</div>
			<div class="row no-input">
				<label class="fl">City</label>
				<div class="fr light"><%= Model.AddressCity%></div>
			</div>
			<div class="row no-input">
				<label class="fl">State, Zip Code</label>
				<div class="fr light"><%= Model.AddressStateCode + ", " + Model.AddressZipCode%></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="column">
			<div class="row no-input">
				<label class="fl">HIPPS Code</label>
				<div class="fr light"><%= Model.HippsCode%></div>
			</div>
			<div class="row no-input">
				<label class="fl">OASIS Matching Key</label>
				<div class="fr light"><%= Model.ClaimKey%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Date Of First Billable Visit</label>
				<div class="fr light"><%= Model.FirstBillableVisitDate.ToZeroFilled()%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Physician Last Name, F.I.</label>
				<div class="fr light"><%= (Model.PhysicianLastName) + " " + (Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.Substring(0, 1) + "." : "")%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Physician NPI #</label>
				<div class="fr light"><%= Model.PhysicianNPI%></div>
			</div>
			<div class="row no-input">
				<label class="fl">Remark</label>
				<br/>
				<p><%= Model.Remark%></p>
			</div>
		</div>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Diagonsis Codes</label>
            </div>
            <div class="sub row no-input">
                <label class="fl">Primary</label>
                <div class="fr light"><%= Model.DiagnosisCodesObject != null && Model.DiagnosisCodesObject.Primary.IsNotNullOrEmpty() ? Model.DiagnosisCodesObject.Primary.Replace(".", "") : string.Empty%></div>
			</div>    
            <div class="sub row no-input">
                <label class="fl">Second</label>
                <div class="fr light"><%= Model.DiagnosisCodesObject != null && Model.DiagnosisCodesObject.Second.IsNotNullOrEmpty() ? Model.DiagnosisCodesObject.Second.Replace(".", "") : string.Empty%></div>
            </div>
            <div class="sub row no-input">
                <label class="fl">Third</label>
                <div class="fr light"><%= Model.DiagnosisCodesObject != null && Model.DiagnosisCodesObject.Third.IsNotNullOrEmpty() ? Model.DiagnosisCodesObject.Third.Replace(".", "") : string.Empty%></div>
            </div>
            <div class="sub row no-input">
                <label class="fl">Fourth</label>
                <div class="fr light"><%= Model.DiagnosisCodesObject != null && Model.DiagnosisCodesObject.Fourth.IsNotNullOrEmpty() ? Model.DiagnosisCodesObject.Fourth.Replace(".", "") : string.Empty%></div>
            </div>
            <div class="sub row no-input">
                <label class="fl">Fifth</label>
                <div class="fr light"><%= Model.DiagnosisCodesObject != null && Model.DiagnosisCodesObject.Fifth.IsNotNullOrEmpty() ? Model.DiagnosisCodesObject.Fifth.Replace(".", "") : string.Empty%></div>
            </div>
            <div class="sub row no-input">
                <label class="fl">Sixth</label>
                <div class="fr light"><%= Model.DiagnosisCodesObject != null && Model.DiagnosisCodesObject.Sixth.IsNotNullOrEmpty() ? Model.DiagnosisCodesObject.Sixth.Replace(".", "") : string.Empty%></div>
            </div>
        </div>
    </fieldset>
    <div class="acore-grid">
<%  var total = 0.0; %>
        <ul>
            <li>
                <span class="grid-eleventh"></span>
                <span class="grid-fifth">Description</span>
                <span class="grid-fifth">HCPCS/HIPPS Code</span>
                <span class="grid-sixth">Service Date</span>
                <span class="grid-seventh">Service Unit</span>
                <span class="grid-sixth">Total Charges</span>
            </li>
        </ul>
        <ol>
            <li>
                <span class="grid-eleventh">0023</span>
                <span class="grid-fifth">Home Health Services</span>
                <span class="grid-fifth"><%= Model.HippsCode%></span>
                <span class="grid-sixth"><%= Model.FirstBillableVisitDate.ToZeroFilled() %></span>
                <span class="grid-seventh"></span>
                <span class="grid-sixth"></span>
            </li>
<%  if (!Model.IsSupplyNotBillable) { %>
	<%  var supplies = Model.Supply.IsNotNullOrEmpty() ? Model.Supply.ToObject<List<Supply>>() : new List<Supply>(); %>
	<%  supplies = supplies.Where(s => s.IsBillable && s.IsDeprecated == false).ToList(); %>
	<%  var serviceSupplies = supplies.Where(s => s.RevenueCode == "0270").ToList(); %>
	<%  var woundSupplies = supplies.Where(s => s.RevenueCode == "0623").ToList(); %>
	<%  var woundTotal = woundSupplies.Sum(s => s.TotalCost); %>
	<%  var medicalSupplyTotal = serviceSupplies.Sum(s => s.TotalCost); %>
	<%  total += medicalSupplyTotal > 0 ? medicalSupplyTotal : Model.SupplyTotal - woundTotal; %>
	<%  bool hasServiceSupplies = supplies.Except(woundSupplies).Any(); %>
	<%  if (hasServiceSupplies || !supplies.IsNotNullOrEmpty()) { %>
			<li>
				<span class="grid-eleventh"><%= medicalSupplyTotal > 0 ? "0270" : "0272"%></span>
				<span class="grid-fifth">Service Supplies</span>
				<span class="grid-fifth"></span>
				<span class="grid-sixth"><%= Model.FirstBillableVisitDate.ToZeroFilled() %></span>
				<span class="grid-seventh"></span>
				<span class="grid-sixth"><%= string.Format("{0:$#0.00;-$#0.00}", medicalSupplyTotal > 0 ? medicalSupplyTotal : Model.SupplyTotal - woundTotal)%></span>
			</li>
	<%  } %>
	<%  if (woundTotal > 0) { %>
		<%  total += woundTotal; %>
			<li>
				<span class="grid-eleventh">0623</span>
				<span class="grid-fifth">Wound Supplies</span>
				<span class="grid-fifth"></span>
				<span class="grid-sixth"><%= Model.FirstBillableVisitDate.ToZeroFilled() %></span>
				<span class="grid-seventh"></span>
				<span class="grid-sixth"><%= string.Format("{0:$#0.00;-$#0.00}", woundTotal)%></span>
			</li>
    <%  } %>
<%  } %>
<%  if (Model != null && Model.BillVisitSummaryDatas != null && Model.BillVisitSummaryDatas.Count > 0) { %>
    <%  foreach (var visit in Model.BillVisitSummaryDatas) { %>
			<li>
				<span class="grid-eleventh"><%= visit.RevenueCode%></span>
				<span class="grid-fifth">Visit</span>
				<span class="grid-fifth"><% =visit.HCPCSCode%></span>
				<span class="grid-sixth"><%= visit.VisitDate.IsValid() ? visit.VisitDate.ToZeroFilled() : (visit.EventDate.IsValid() ? visit.EventDate.ToZeroFilled() : string.Empty)%></span>
				<span class="grid-seventh"><% = visit.Unit%> </span>
				<span class="grid-sixth"><% = string.Format("{0:$#0.00;-$#0.00}", visit.Charge)%></span>
			</li>
		<%  total += visit.Charge; %>
	<%  } %>
<%  } %>
        </ol>
        <ul class="total">
	        <li class="ar">
		        <label>Total <%= string.Format("{0:$#0.00;-$#0.00}", total)%></label> 
	        </li>
        </ul>
    </div>
<%  using (Html.BeginForm("FinalComplete", "Billing", FormMethod.Post, new { @id = "finalCompleteForm" })) { %>
    <ul class="buttons ac">
        <%= Html.Hidden("Id", Model.Id, new { @id = "FinalVerification_Id", @class = "input" })%>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "FinalVerification_PatientId", @class = "input" })%>
        <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "FinalVerification_EpisodeId", @class = "input" })%>
        <li><a class="back">Back</a></li>
        <li><a onclick="U.GetAttachment('Billing/FinalPdf',{episodeId:'<%= Model.EpisodeId %>',patientId:'<%= Model.PatientId %>'})">Print</a></li>
        <li><a class="save close">Complete</a></li>
    </ul>
<%  } %>
</div>