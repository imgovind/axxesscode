﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<span class="wintitle">Final (EOE) Claims  | <%= Model.DisplayName %></span>
<div id="FinalVerification_TabStrip" class="wrapper blue horizontal-tabs main maintab">
    <%= Html.Hidden("Id", Model.Id, new { @class = "tabinput" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @class = "tabinput" })%>
    <%= Html.Hidden("ServiceId",(int) AgencyServices.HomeHealth, new { @class = "tabinput" })%>
    <%= Html.Hidden("Type", ClaimTypeSubCategory.Final, new { @class = "tabinput" })%>
    <ul class="horizontal-tab-list">
	    <li class="ac">
	        <a href="/Billing/Info" name="Info">
	            <strong>Step 1 of 4</strong>
	            <br />
	            <span>Demographics</span>
	        </a>
	    </li>
	     <li class="ac" >
	        <a href="/Billing/Insurance" name="Insurance">
	            <strong>Step 2 of 5</strong>
	            <br />
	            <span>Verify Insurance</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsFinalInfoVerified ? "" : "disabled" %>">
	        <a href="/Billing/Visit" name="Visit">
	            <strong>Step 3 of 4</strong>
	            <br />
	            <span>Verify Visits</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsVisitVerified ? "" : "disabled" %>">
	        <a href="/Billing/Supply" name="Supply">
	            <strong>Step 4 of 4</strong>
	            <br />
	            <span>Verify Supplies</span>
	        </a>
	    </li>
	    <li class="ac <%= Model.IsSupplyVerified ? "" : "disabled" %>">
	        <a href="/Billing/Summary" name="Summary">
	            <strong>Step 5 of 5</strong>
	            <br />
	            <span>Summary</span>
	        </a>
	    </li>
    </ul>
  <%--  <div id="FinalVerification_Info" class="tab-content"><%  Html.RenderPartial("Medicare/Claim/Final/Info", Model); %></div>
    <div id="FinalVerification_Visit" class="tab-content"></div>
    <div id="FinalVerification_Supply" class="tab-content"></div>
    <div id="FinalVerification_Summary" class="tab-content"></div>--%>
</div>