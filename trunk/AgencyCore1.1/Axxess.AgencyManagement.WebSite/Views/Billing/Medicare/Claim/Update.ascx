﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("SaveUpdate", "Billing", FormMethod.Post, new { @id = "updateClaimForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%= Html.Hidden("PatientId", Model.PatientId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>Update Claim Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Status" class="fl">Claim Status</label>
                <div class="fr"><%= Html.BillStatus("Status", Model.Status.ToString(), false, new { })%></div>
            </div>
            <div class="row">
                <label for="PaymentAmount" class="fl">Payment Amount</label>
                <div class="fr"><%= Html.TextBox("PaymentAmount", Model.PaymentAmount > 0 ? Model.PaymentAmount.ToString() : string.Empty, new { @class = "currency" })%></div>
            </div>
            <div class="row">
                <label for="PaymentDateValue" class="fl">Payment Date</label>
                <div class="fr"><input type="text" class="date-picker" name="PaymentDateValue" value="<%= Model.PaymentDate.ToZeroFilled() %>" /></div>
            </div>
            <div class="row">
                <label for="Comment">Comments</label>
                <div class="ac"><%= Html.TextArea("Comment", Model.Comment)%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>