﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl <NewClaimViewData>" %>
<div class="wrapper main">
<%  if (Enum.IsDefined(typeof(ClaimTypeSubCategory), Model.Type)) { %>
    <%  if (Model != null && Model.EpisodeData != null && Model.EpisodeData.Count > 0) { %>
        <%  using (Html.BeginForm("Create" + ((ClaimTypeSubCategory)Model.Type).ToString() + "Claim", "Billing", FormMethod.Post, new { @id = "createClaimForm" , @class = "mainform" })) { %>
            <%  var listOfEpisode = (Model != null && Model.EpisodeData != null ? Model.EpisodeData : new List<ClaimEpisode>()).Select(i => new SelectListItem { Value = i.Id.ToString(), Text = string.Format("{0}-{1}", i.StartDate.ToString("MM/dd/yyyy"), i.EndDate.ToString("MM/dd/yyyy")) }); %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <%= Html.Hidden("Type",Model.Type) %>
    <%= Html.Hidden("InsuranceId", "", new { id = "NewClaim_InsuranceId" })%>
    <fieldset>
        <legend>Episode Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewPatient_LocationId" class="fl">Episodes</label>
                <div class="fr"><%= Html.DropDownList("EpisodeId", listOfEpisode) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add <%=Model.TypeName%></a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <script type="text/javascript">
        $("#NewClaim_InsuranceId").val($("#BillingHistory_InsuranceId").val())
    </script>
        <%  } %>
    <%  } else { %>
    <div class="no-claimless-episode-alert"></div>
    <%  } %>
<%  } else { %>
    <div class="no-claim-type-alert"></div>
<%  } %>
</div>