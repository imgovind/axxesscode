﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimSnapShotViewData>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("UpdateSnapShotClaim", "Billing", FormMethod.Post, new { @id = "updateBatchClaimForm" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%= Html.Hidden("BatchId", Model.BatchId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>Batch Claim - Update</legend>
        <div class="wide-column">
            <div class="row">
                <label for="PaymentAmountValue" class="fl">Payment Amount</label>
                <div class="fr"><%= Html.TextBox("PaymentAmountValue", String.Format("{0:0.00}", Model.PaymentAmount), new { @class = "currency" })%></div>
            </div>
            <div class="row">
                <label for="PaymentDate" class="fl">Payment Date</label>
                <div class="fr"><input type="text" class="date-picker" name="PaymentDate" value="<%= Model.PaymentDate.ToZeroFilled() %>" /></div>
            </div>
            <div class="row">
                <label for="Status" class="fl">Status</label>
                <div class="fr"><%= Html.BillStatus("Status", Model.Status.ToString(), false, new {}) %> </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>