﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rap>" %>
<span class="wintitle">RAP | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("RapVerify", "Billing", FormMethod.Post, new { @id = "rapVerification", @class = "mainform" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Rap_PatientId" })%>
    <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO)%>
    <%= Html.Hidden("ClaimType", ClaimTypeSubCategory.RAP.ToString())%>
	<%  var diagnosis = Model.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(Model.DiagnosisCode) : null; %>
	<ul class="buttons ac">
        <%= String.Format("<li><a onclick=\"U.GetAttachment('Billing/RapPdf',{{episodeId:'{0}',patientId:'{1}'}})\">Print</a></li>", Model.EpisodeId, Model.PatientId) %>
	</ul>
	<fieldset>
		<legend>Patient Information</legend>
		<div class="column">
			<div class="row">
				<label for="FirstName" class="fl">Patient First Name</label>
				<div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "required", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<label for="LastName" class="fl">Patient Last Name</label>
				<div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @class = "required", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<label for="PatientIdNumber" class="fl">Patient ID/MR Number</label>
				<div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "required", @maxlength = "30" }) %></div>
			</div>
			<div class="row">
				<label for="Gender" class="fl">Gender</label>
				<div class="fr"><%= Html.Gender("Gender", Model.Gender, new { @class = "required"}) %></div>
			</div>
			<div class="row">
				<label for="DOB" class="fl">Date of Birth</label>
				<div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Rap_DOB" /></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="AddressLine1" class="fl">Address Line 1</label>
				<div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @class = "required" }) %></div>
			</div>
			<div class="row">
				<label for="AddressLine2" class="fl">Address Line 2</label>
				<div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2) %></div>
			</div>
			<div class="row">
				<label for="AddressCity" class="fl">City</label>
				<div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @class = "address-city required" })%></div>
			</div>
			<div class="row">
				<label for="AddressStateCode" class="fl">State, Zip Code</label>
				<div class="fr">
					<%= Html.States("AddressStateCode", Model.AddressStateCode, new { @class = "short address-state" })%>
					<%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits zip shorter", @maxlength = "9" }) %>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Physician Information</legend>
		<div class="column">
			<div class="row">
				<label for="PhysicianLastName" class="fl">Physician Last Name</label>
				<div class="fr"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "required", @maxlength = "20" }) %></div>
			</div>
			<div class="row">
				<label for="PhysicianFirstName" class="fl">Physician First Name</label>
				<div class="fr"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "required", @maxlength = "20" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="PhysicianNPI" class="fl">Physician NPI #</label>
				<div class="fr"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "required", @maxlength = "10" }) %></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Insurance Information</legend>
		<div class="column">
			<div class="row">
				<label for="PrimaryInsuranceId" class="fl">Insurance</label>
				<div class="fr"><%= Html.InsurancesMedicareByBranch("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), Model.AgencyLocationId, true, true, "-- Select Insurance --", new { @class = "required not-zero claiminsurance", @id = "Rap_PrimaryInsuranceId" })%></div>
			</div>
			<div class="row">
				<label for="MedicareNumber" class="fl">Medicare #</label>
				<div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @class = "required", @maxlength = "12" }) %></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="Type" class="fl">Bill Type</label>
				<div class="fr">
				    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Initial Rap", Value = "0" }
                        }, "Value", "Text", Model.Type); %>
					<%= Html.DropDownList("Type", billType) %>
				</div>
			</div>
		</div>
		<div class="clr"></div>
		<div id="Rap_InsuranceContent" class="primaryinsurancecontent">
            <%  if (Model.PrimaryInsuranceId >= 1000) Html.RenderPartial("~/Views/Billing/InsuranceInfoContent.ascx", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "Rap" }); %>
        </div>
	</fieldset>
	<fieldset>
		<legend>Episode Information</legend>
		<%= Html.Hidden("AssessmentType", Model.AssessmentType)%>
		<div class="column">
			<div class="row">
				<label for="AdmissionSource" class="fl">Admission Source</label>
				<div class="fr"><%= Html.AdmissionSources("AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : "", new { @class = "admission-source required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="StartOfCareDate" class="fl">Admission Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Rap_StartOfCareDate" /></div>
			</div>
			<div class="row">
				<label for="PatientStatus" class="fl">Patient Status</label>
				<div class="fr"><%= Html.UB4PatientStatus("UB4PatientStatus", ((int)UB4PatientStatus.StillPatient).ToString(), new { @id = "Rap_PatientStatus", @class = "required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="EpisodeStartDate" class="fl">Episode Start Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="Rap_EpisodeStartDate" /></div>
				<%= Html.Hidden("EpisodeEndDate", Model.EpisodeEndDate, new { @id = "Rap_EpisodeEndDate" })%>
			</div>
		</div>
		<div class="column">
			<div class="row no-input">
				<span class="fr">Recommended / Previously Entered First Billable Date: <%= Model.FirstBillableVisitDateFormat %></span>
			</div>
			<div class="row">
				<label for="FirstBillableVisitDateFormatInput" class="fl">Date Of First Billable Visit</label>
				<div class="fr"><input type="text" class="date-picker required" name="FirstBillableVisitDateFormatInput" value="<%= Model.FirstBillableVisitDateFormat %>" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="Rap_FirstBillableVisitDateFormat" /></div>
			</div>
			<div class="row">
				<em>Please Verifiy the first billable visit date from the schedule.</em>
				<div class="button fr"><a onclick="UserInterface.ShowScheduleCenter('<%= Model.PatientId %>','<%= Model.PatientStatus %>')">View Schedule</a></div>
			</div>
			<div class="row">
				<label for="HippsCode" class="fl">HIPPS Code</label>
				<div class="fr"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @class = "required", @maxlength = "20" }) %></div>
			</div>
			<div class="row">
				<label for="ClaimKey" class="fl">OASIS Matching Key</label>
				<div class="fr"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @class = "required", @maxlength = "18" }) %></div>
			</div>
			<div class="row">
				<label for="ProspectivePay" class="fl">HIPPS Code Payment</label>
				<div class="fr"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @maxlength = "20" })%></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Diagnosis Codes</legend>
		<div class="column">
			<div class="row">
				<label class="fl">Primary</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Primary" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Primary : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Secondary</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Second" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Second : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Third</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Third" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Third : string.Empty %>" /></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label class="fl">Fourth</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Fourth" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Fourth : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Fifth</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Fifth" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Fifth : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">Sixth</label>
				<div class="fr"><input type="text" class="short" name="DiagnosisCodesObject.Sixth" value="<%= Model.DiagnosisCodesObject != null ? Model.DiagnosisCodesObject.Sixth : string.Empty %>" /></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Locators</legend>
		<%  Html.RenderPartial("Locator/Ub04Locator81ccb", Model.Locator81cca ?? new List<Locator>(), new ViewDataDictionary { { "IdPrefix", "Rap" } }); %>
		<%  Html.RenderPartial("Locator/Ub04Locator39", Model.Locator39 ?? new List<Locator>(), new ViewDataDictionary { { "IdPrefix", "Rap" } }); %>
	</fieldset>
	<fieldset>
		<legend>Condition Codes</legend>
		<div class="column">
			<div class="row">
				<label class="fl">18.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode18" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode18 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">19.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode19" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode19 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">20.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode20" value="<%=Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode20 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">21.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode21" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode21 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">22.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode22" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode22 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">23.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode23" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode23 : string.Empty %>" /></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label class="fl">24.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode24" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode24 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">25.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode25" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode25 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">26.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode26" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode26 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">27.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode27" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode27 : string.Empty %>" /></div>
			</div>
			<div class="row">
				<label class="fl">28.</label>
				<div class="fr"><input type="text" class="short" maxlength="2" name="ConditionCodesObject.ConditionCode28" value="<%= Model.ConditionCodesObject != null ? Model.ConditionCodesObject.ConditionCode28 : string.Empty %>" /></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Remark</legend>
		<div class="wide-column">
			<div class="row ac"><%= Html.TextArea("Remark", Model.Remark) %></div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Verify</a></li>
		<%= String.Format("<li><a onclick=\"U.GetAttachment('Billing/RapPdf',{{episodeId:'{0}',patientId:'{1}'}})\">Print</a></li>", Model.EpisodeId, Model.PatientId) %>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>
