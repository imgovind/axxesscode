﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% var isDataExist = (Model != null && Model.Claims != null && Model.Claims.Count > 0); %>
<div class="subcontent">
	<div class="acore-grid selectable">
	    <input type="hidden" class="subinput"  name="BranchId" value="<%= Model.BranchId %>" />
		<input type="hidden" class="subinput"  name="InsuranceId" value="<%= Model.Insurance %>" />
		<input type="hidden" class="subinput"  name="ClaimType" value="Final" />
		<input type="hidden" class="subinput" name="ParentSortType" value="branch" />
		<input type="hidden" name="CurrentSort" value="name" />
		<input type="hidden" name="IsSortInvert" value="true" />
		<ul>
			<li class="ac">
				<h3><%= Model.BranchName + " | " + Model.InsuranceName %></h3>
<% if (isDataExist) { %> 
    <% if (Model.IsUserCanExport) { %>
                <span>[ <a class="subexport" url="Billing/ClaimsXls">Export to Excel</a> ]</span>
    <% } %>
    <% if (Model.IsUserCanExport) { %>
                <span>[ <a class="subprint" url="Billing/ClaimsPdf">Print</a> ]</span>
    <% } %>
<% } %>
            </li>
			<li>
			    <span class="grid-twentieth"></span>
				<span class="grid-quarter pointer sortable" sortname="name">Patient Name</span>
				<span class="grid-eighth pointer sortable" sortname="id">MRN</span>
				<span class="grid-fifth pointer sortable" sortname="episode">Episode Period</span>
				<span class="grid-sixteenth ac">RAP</span>
				<span class="grid-sixteenth ac">Visit</span>
				<span class="grid-sixteenth ac">Order</span>
				<span class="grid-sixteenth ac">Verified</span>
			</li>
		</ul>
<% if (isDataExist) { %>
		<ol>
	<% var finals = Model.Claims.Where(c =>c.EpisodeEndDate < DateTime.Now); %>
	<% int i = 1; %>
	<% foreach (FinalBill claim in finals) { %>
			<li class="<%= claim.IsRapGenerated ? "ready" : "notready" %>">
		<% var nameText = claim.LastName + ", " + claim.FirstName; %>
		<% if (!Current.IsAgencyFrozen && Model.IsUserCanGenerate) { %>
		        <span class="grid-twentieth"><%= i %>. <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified && claim.IsInsuranceVerified && claim.AreOrdersComplete && claim.IsRapGenerated) ? "<input name='ClaimSelected' type='checkbox' value='" + claim.Id + "' id='FinalSelected" + claim.Id + "' />" : string.Empty %></span>
		<% } else { %>
		        <span class="grid-twentieth"><%= i%> </span>
		<% } %>
		        <span class="grid-quarter">
		<% if (!Current.IsAgencyFrozen && Model.IsUserCanEdit) { %>
				    <a class="<%= claim.IsRapGenerated ? "strong" : "disabled" %>" onclick="<%= (claim.IsRapGenerated) ? "Billing.Medicare.Claim.Final.Open('" + claim.EpisodeId + "','" + claim.PatientId + "',arguments[0])\" title=\"Final Ready" : "U.Growl('Rap Not Generated','error')\" title=\"Not Complete" %>"><%= nameText %></a>
		<% } else { %>
		            <%= nameText %>
		<% } %>
		        </span>
		        <span class="grid-eighth"><%= claim.PatientIdNumber%></span>
				<span class="grid-fifth">
				    <span class="hidden"><%= claim.EpisodeStartDate.IsValid() ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
				    <%= claim.EpisodeStartDate.IsValid() ? claim.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %>
				    <%= claim.EpisodeEndDate.IsValid() ? "&#8211;" + claim.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %>
				</span>
				<span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsRapGenerated ? "check" : "ecks" %>'></span></span>
				<span class="grid-sixteenth ac"><span class='img icon16 <%= claim.AreVisitsComplete? "check" : "ecks" %>'></span></span>
				<span class="grid-sixteenth ac"><a class="img icon16 <%= claim.AreOrdersComplete ? "check" : "ecks" %>" onclick="Patient.Orders.Episode.Open('<%=claim.EpisodeId %>','<%=claim.PatientId %>',arguments[0])"/></span>
				<span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified && claim.IsInsuranceVerified ? "check" : "ecks" %>'></span></span>
		<% if (Model.IsUserCanPrint){ %>
		        <span class="fr"><a class="img icon16 print" onclick="Billing.Medicare.Claims.PrintClaim(arguments[0],'<%= claim.EpisodeId %>','<%= claim.PatientId %>','Final')"/></span>
		<% } %>
			</li>
		<% i++; %>
	<% } %>
		</ol>
<% } else { %>
	<ol>
		<li class="no-hover"><h4>No Finals found.</h4></li>
	</ol>
<% } %>
    </div>
<% if (isDataExist && Model.Insurance > 0 && Model.IsUserCanGenerate && !Current.IsAgencyFrozen) { %>
	<ul class="buttons ac">
		<li><a class="submitclaim" claimtype="<%= Model.ClaimType.ToString().ToLower()%>">Generate Selected</a></li>
		<li><a class="submitallclaim" claimtype="<%= Model.ClaimType.ToString().ToLower()%>">Generate All Completed</a></li>
	</ul>
<% } %>
</div>