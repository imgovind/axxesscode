﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% var isDataExist = (Model != null && Model.Claims != null && Model.Claims.Count > 0); %>
<div class="subcontent">
    <div class="acore-grid selectable">
	    <input type="hidden" class="subinput" name="BranchId" value="<%=Model.BranchId %>" />
		<input type="hidden" class="subinput" name="InsuranceId" value="<%=Model.Insurance %>" />
		<input type="hidden" class="subinput" name="ClaimType" value="RAP" />
		<input type="hidden" class="subinput" name="ParentSortType" value="branch" />
		<input type="hidden"  name="CurrentSort" value="name" />
		<input type="hidden" name="IsSortInvert" value="true" />
		<ul class="sortable">
			<li class="ac">
				<h3><%= Model.BranchName + " | " + Model.InsuranceName %></h3>
<% if (isDataExist && Model.IsUserCanExport) { %>  
				<span>[ <a class="subexport" url="Billing/ClaimsXls">Export to Excel</a> ]</span>
				<span>[ <a class="subprint" url="Billing/ClaimsPdf">Print</a> ]</span>
<% } %>
			</li>
			<li>
			    <span class="grid-twentieth"></span>
				<span class="grid-quarter pointer sort" sortname="name">Patient Name</span>
				<span class="grid-fifth pointer sort" sortname="id">MRN</span>
				<span class="grid-fifth pointer sort" sortname="episode">Episode Period</span>
				<span class="grid-sixteenth ac">OASIS</span>
				<span class="grid-tenth ac">Billable Visit</span>
				<span class="grid-sixteenth ac">Verified</span>
			</li>
		</ul>
<% if (isDataExist) { %>
		<ol>
    <% int i = 1; %>
    <% foreach (RapBill claim in Model.Claims) { %>
			<li class="<%= claim.IsOasisComplete && claim.IsFirstBillableVisit ? "ready" : "notready" %>">
		<% var nameText = claim.LastName + ", " + claim.FirstName; %>
		<% if (!Current.IsAgencyFrozen && Model.IsUserCanGenerate) { %>
			    <span class="grid-twentieth"><%= i %>. <%=  claim.IsOasisComplete &&  claim.IsFirstBillableVisit && claim.IsVerified ? "<input name='ClaimSelected' class='radio' type='checkbox' value='" + claim.Id + "' id='RapSelected" + claim.Id + "' />" : "" %></span>
		<% } else { %>
		        <span class="grid-twentieth"><%= i %></span>
		<% } %>
				<span class="grid-quarter">
		<% if (!Current.IsAgencyFrozen && Model.IsUserCanEdit) { %>
		            <a class="<%= claim.IsOasisComplete && claim.IsFirstBillableVisit ? "strong" : "disabled" %>" onclick="<%= claim.IsOasisComplete && claim.IsFirstBillableVisit ? "Billing.Medicare.Claim.Rap.Open('" + claim.EpisodeId + "','" + claim.PatientId + "',arguments[0])" : "U.Growl('An OASIS and a billable visit must be completed before billing this RAP.','error')" %>"><%= nameText %></a>
		<% } else { %>
					<%= nameText %>
		<% } %> 
				</span>
				<span class="grid-fifth"><%= claim.PatientIdNumber%></span>
				<span class="grid-fifth">
					<span class="hidden"><%= claim.EpisodeStartDate.IsValid() ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
					<%= claim.EpisodeStartDate.IsValid() ? claim.EpisodeStartDate.ToString("MM/dd/yyyy") : string.Empty %>
					<%= claim.EpisodeEndDate.IsValid() ? "&#8211;" + claim.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty %>
				</span>
				<span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsOasisComplete ? "check" : "ecks" %>'></span></span>
				<span class="grid-tenth ac"><span class='img icon16 <%= claim.IsFirstBillableVisit ? "check" : "ecks" %>'></span></span>
				<span class="grid-sixteenth ac"><span class='img icon16 <%= claim.IsOasisComplete && claim.IsFirstBillableVisit && claim.IsVerified ? "check" : "ecks" %>'></span></span>
				<span class="fr"><a class="img icon16 print" onclick="Billing.Medicare.Claims.PrintClaim(arguments[0],'<%= claim.EpisodeId %>','<%= claim.PatientId %>', 'Rap')"/></span>
			</li>
		<% i++; %>
	<% } %>
        </ol>
<% } else { %>
	<ol>
		<li class="no-hover"><h4>No RAPs found.</h4></li>
	</ol>
<% } %>
    </div>
<% if (isDataExist && Model.Insurance > 0 && Model.IsUserCanGenerate && !Current.IsAgencyFrozen) { %>
    <ul class="buttons ac">
        <li><a class="submitclaim" claimtype="<%= Model.ClaimType.ToString().ToLower() %>">Generate Selected</a></li>
        <li><a class="submitallclaim"  claimtype="<%= Model.ClaimType.ToString().ToLower() %>">Generate All Completed</a></li>
    </ul>
<% } %>
</div>