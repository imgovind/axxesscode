﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle"><%= Model.ClaimType.GetDescription() %>s | <%= Current.AgencyName %></span>
<%  if (Model.IsUserCanView) { %>
    <%  var titleCaseType = Model.ClaimType.ToString().ToTitleCase(); %>
    <%  var pageName = "Billing_" + titleCaseType + "Center"; %>
    <%  var area = Model.Service.ToArea(); %>
<div id="Billing_<%= titleCaseType %>CenterContent" class="main wrapper">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="buttons fr top ac">
    <%  if (Model.IsUserCanExport) { %>
        <li><a class="printclaims" url="<%= area %>/Billing/ClaimsPdf">Print</a></li><br />
    <%  } %>
    <%  if (Model.IsUserCanExport) { %>
        <li><a class="export" url="<%=area %>/Billing/ClaimsXls">Excel Export</a></li>
    <%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a class="grid-refresh" url="<%=area %>/Billing/<%=titleCaseType%>Grid">Refresh</a></div>
        <%= Html.Hidden("Service", (int)Model.Service, new { @class = "input service", @id = pageName + "_Service" }) %> 
        <div class="filter">
            <label for="<%=pageName%>_BranchId" class="strong">Branch</label>
            <%= Html.BranchList("BranchId", Model.BranchId.ToString(), (int)Model.Service, new { @id = pageName + "_BranchId", @class = "input location rebind" })%>
        </div>
        <div class="filter">
            <label for="<%=pageName%>_InsuranceId" class="strong">Insurance</label>
            <%= Html.InsurancesMedicare("InsuranceId", Model.Insurance.ToString(), true, "Unassigned Insurance", new { @id = pageName + "_InsuranceId", @class = "input insurance medicare not-zero long zeroprefix" })%>
        </div>
        <div class="filter">
            <%= Html.Hidden("ClaimType", ((int)Model.ClaimType), new { @id = pageName + "_ClaimType", @class = "input claimtype", @billtype = "medicare" })%>
            <%= Html.Hidden("ParentSortType", "branch", new { @id = pageName + "_ParentSortType", @class = "input" })%>
            <%= Html.Hidden("ColumnSortType", "", new { @id = pageName + "_ColumnSortType", @class = "input" })%>
        </div>
    </fieldset>
</div>
<div id="<%= pageName %>GridContainer" style="min-height:200px;" class="main-container">
    <% Html.RenderPartial("Medicare/" + titleCaseType + "Grid",  Model); %>
</div>
<%  } %>