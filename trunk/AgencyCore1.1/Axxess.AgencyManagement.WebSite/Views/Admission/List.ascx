﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdmissionListViewData>" %>
<span class="wintitle">Patient Admission Periods | <%= Model.DisplayName %></span>
<%  var pageName = "PatientAdmissionPeriod"; %>
<%  var markAsCurrentLink = !Current.IsAgencyFrozen ? " (<a onclick=\"Patient.Admission.MarkAsCurrent(\\'" + Model.PatientId + "" + "\\', \\'\' + Id  + \'\\');\">Set Current</a>)" : string.Empty; %>
<%  var editLink = "<a onclick=\"Patient.Admission.Edit(\\'" + Model.PatientId + "" + "\\', \\'\' + Id  + \'\\', \\'Edit\\');\">Edit</a>"; %>
<%  var deleteLink = " | <a onclick=\"Patient.Admission.Delete(\\'" + Model.PatientId + "" + "\\', \\'\' + Id  + \'\\');\">Delete</a>"; %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" }) %>
    <ul class="fr buttons">
<%  if (!Current.IsAgencyFrozen) { %>
        <li><a class="add-button">Add New Period</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">	  
        <div class="button fr"><a class="grid-refresh">Refresh</a></div>
		<div class="filter ignore-grid-binding">
<%  if (!(Model.PatientsServices & Current.AcessibleServices).IsAlone()) { %>
            <label for="PatientAdmissionPeriod_Service">Service</label>
            <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.PatientsServices, false, new { @id = "PatientAdmissionPeriod_Service" })%>
<%  } else { %>
            <%= Html.Hidden("ServiceId", (Model.Service & Current.AcessibleServices & Model.PatientsServices).ToString(), new { @Id = "PatientAdmissionPeriod_Service" })%>
<%  } %>
        </div>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "PatientAdmissionPeriod_PatientId" })%>
        <div class="filter grid-search"></div>
    </fieldset>
<%  Html.Telerik().Grid<AdmissionEpisode>().Name("PatientManagedDates_Grid").Columns(columns => {
        columns.Bound(c => c.StartOfCareDate).ClientTemplate("<#= U.FormatGridDate(StartOfCareDate) #>").Title("Admission Date");
		columns.Bound(c => c.DischargedDate).ClientTemplate("<#= "+ Model.IsPatientActive.ToString().ToLower() + " && Id == '" + Model.AdmissionId.ToString() + "' ? '' : U.FormatGridDate(DischargedDate)#>").Title("Discharge Date");
		columns.Bound(c => c.IsCurrent).ClientTemplate("<#= IsCurrent ? 'Yes' : ('No' + '" + markAsCurrentLink + "')#>").Title("Current Admission Period").Sortable(false);
		columns.Bound(c => c.HasDetails).ClientTemplate("<#= HasDetails ? 'Yes' : 'No' #>").Title("Associated Episodes").Sortable(false);
		columns.Bound(c => c.PatientId).ClientTemplate("<#= (IsCurrent ? '' : \'" + editLink + "\') + (IsCurrent || HasDetails ? '' : \'" + deleteLink + "\')#>").Title("Current Admission Period").Sortable(false);
    }).DetailView(detailView => detailView.ClientTemplate(
        Html.Telerik().Grid<EpisodeLean>().Name("DetailPatientAdmission<#=Id#>").HtmlAttributes(new { @class = "position-relative" }).Columns(columns => {
		    columns.Bound(c => c.StartDate).ClientTemplate("<#= U.FormatGridDate(StartDate) #>").Title("Start Date").HeaderHtmlAttributes(new { name = "StartDate"});
            columns.Bound(c => c.EndDate).ClientTemplate("<#= U.FormatGridDate(EndDate) #>").Title("End Date").HeaderHtmlAttributes(new { name = "EndDate" });
			columns.Bound(c => c.Id).Width(130).ClientTemplate("<a onclick=\"Episode[$('#PatientAdmissionPeriod_Service').val()].Edit('<#= Id #>','<#= PatientId #>',function(){Patient.Admission.Refresh()})\">Edit</a>").Title("Action").Visible(!Current.IsAgencyFrozen).Sortable(false);
		}).Sortable().DataBinding(dataBinding => dataBinding.Ajax().Select("ListContentDetails", "Admission", new { patientId = "<#= PatientId #>", admissionId = "<#=Id#>" }).OperationMode(GridOperationMode.Client)).ClientEvents(events => events
            .OnDataBound("U.OnTGridDataBound")
            .OnDataBinding("U.OnTGridDataBinding")
            .OnRowDataBound("Patient.Admission.OnDetailRowDataBound")
            .OnLoad("Patient.Admission.OnDetailGridLoad")
            .OnError("U.OnTGridError")
        ).Footer(false).ToHtmlString()
    )).DataBinding(dataBinding => dataBinding.Ajax().Select("ListContent", "Admission", new { patientId = Model.PatientId }).OperationMode(GridOperationMode.Server)).ClientEvents(events => events
        .OnRowDataBound("U.OnRowDataBoundHideEmptyHierarchy")
        .OnDataBound("U.OnTGridDataBound")
        .OnDataBinding("U.OnTGridDataBinding")
        .OnError("U.OnTGridError")
    ).Scrollable(scrolling => scrolling.Enabled(true)).Sortable().Footer(false).Render(); %>
</div>