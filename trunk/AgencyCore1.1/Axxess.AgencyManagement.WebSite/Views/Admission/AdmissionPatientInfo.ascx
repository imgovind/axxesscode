﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%@ Import Namespace="System.Globalization" %>
<span class="wintitle">Patient Information | <%= Model.DisplayName %></span>
<%  var type = ViewData.Get("Type").ToString().ToTitleCase(); %>
<%  var isDischarge = ViewData.Get("IsDischarge").ToString().IsEqual("true"); %>
<div class="wrapper main">
<%  using (Html.BeginForm(type, "Admission", FormMethod.Post, new { area = ViewContext.RouteData.DataTokens["area"], @id = type.ToLowerCase() + "PatientAdmissionForm" })) { %>
    <%= string.Format("<input type=\"hidden\" value=\"{0}\" name=\"Id\" id=\"{1}_Admission_Id\">", Model.Id, type)%>  
    <%= Html.Hidden("profile.AdmissionId", Model.Profile.AdmissionId, new { @id = type + "_Admission_AdmissionId" })%>     
	<fieldset>
		<legend>Patient Demographics</legend>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_FirstName" class="fl">
				    <span class="green">(M0040)</span>
				    First Name
				</label>
				<div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = type + "_Admission_FirstName", @class = "required", @maxlength = "100" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_MiddleInitial" class="fl">MI</label>
				<div class="fr"><%= Html.TextBox("MiddleInitial", Model.MiddleInitial, new { @id = type + "_Admission_MiddleInitial", @class = "shortest", @maxlength = "1" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_LastName" class="fl">Last Name</label>
				<div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = type + "_Admission_LastName", @class = "required", @maxlength = "100" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_Gender" class="fl">
				    <span class="green">(M0069)</span>
				    Gender
				</label>
				<div class="fr"><%= Html.Gender("Gender", Model.Gender, new { @id = type + "_Admission_Gender", @class = "required" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_DOB" class="fl">
				    <span class="green">(M0066)</span>
				    Date of Birth
				</label>
				<div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="<%= type %>_Admission_DOB" /></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_MaritalStatus" class="fl">Marital Status</label>
				<div class="fr"><%= Html.MaritalStatus("MaritalStatus", Model != null ? Model.MaritalStatus : string.Empty, true, "-- Select Marital Status --", "0", new { @id = type + "_Admission_MaritalStatus" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_Height" class="fl">Height</label>
				<div class="fr">
					<%= Html.TextBox("Height", Model.Height, new { @id = type + "_Admission_Height", @class = "numeric short", @maxlength = "3" })%>
					<%= Html.HeightMetric("HeightMetric", Model.HeightMetric, new { @class = "shorter" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_Weight" class="fl">Weight</label>
				<div class="fr">
					<%= Html.TextBox("Weight", Model.Weight, new { @id = type + "_Admission_Weight", @class = "numeric short", @maxlength = "3" })%>
					<%= Html.WeightMetric("WeightMetric", Model.WeightMetric, new { @class = "shorter" })%>
				</div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_LocationId" class="fl">Agency Branch</label>
				<div class="fr"><%= Html.BranchOnlyList("profile.AgencyLocationId", Model.Profile.AgencyLocationId.ToString(), Model.Profile.ServiceType, new { @id = type + "_Admission_LocationId", @service = Model.Profile.ServiceType, @class = "branch-location" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_PatientID" class="fl">
				    <span class="green">(M0020)</span>
				    Patient ID
				</label>
				<div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = type + "_Admission_PatientID", @class = "required", @maxlength = "15" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_MedicareNumber" class="fl">
				    <span class="green">(M0063)</span>
				    Medicare Number
				</label>
				<div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = type + "_Admission_MedicareNumber", @maxlength = "12" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_MedicaidNumber" class="fl">
				    <span class="green">(M0065)</span>
				    Medicaid Number
				</label>
				<div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = type + "_Admission_MedicaidNumber", @maxlength = "20" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_SSN" class="fl">
				    <span class="green">(M0064)</span>
				    SSN
				</label>
				<div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = type + "_Admission_SSN", @class = "ssn numeric", @maxlength = "9" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_StartOfCareDate" class="fl">
				    <span class="green">(M0030)</span>
				    Start of Care Date
				</label>
				<div class="fr"><input type="text" class="date-picker required" name="profile.StartOfCareDate" value="<%= !Model.Profile.StartofCareDate.IsValid() ? Model.Profile.StartofCareDate.ToShortDateString() : string.Empty %>" id="<%= type %>_Admission_StartOfCareDate" /></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_DischargeDate" class="fl">Discharge Date</label>
				<div class="fr"><input type="text" class="date-picker<%= isDischarge ? " required" : string.Empty %>" name="profile.DischargeDate" value="<%= !Model.Profile.DischargeDate.IsValid() ? Model.Profile.DischargeDate.ToShortDateString() : string.Empty %>" id="<%= type %>_Admission_DischargeDate" /></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_CaseManager" class="fl">Case Manager</label>
				<div class="fr"><%= Html.CaseManagers("profile.CaseManagerId", Model.Profile.CaseManagerId.ToString(), new { @id = type + "_Admission_CaseManager", @class = "user-selector required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_Assign" class="fl">Clinician</label>
				<div class="fr"><%= Html.Clinicians("profile.UserId", Model.Profile.UserId.ToString(), new { @id = type + "_Admission_Assign", @class = "user-selector required not-zero" })%></div>
			</div>
		</div>
	</fieldset>
	<%= Html.Partial("Patient/Ethnicities", new ArrayIdViewData(Model.Ethnicities, type + "_Admission"))%>
	<%= Html.Partial("Patient/PaymentSources", new AdvancedArrayIdViewData(Model.Profile.PaymentSource, Model.Profile.OtherPaymentSource, type + "_Admission_", "profile.", true))%>
	<fieldset>
		<legend>Patient Address</legend>
		<div class="marginBreak">
			<div class="column">
				<div class="row">
					<label for="<%= type %>_Admission_AddressLine1" class="fl">Address Line 1</label>
					<div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = type + "_Admission_AddressLine1", @class = "required ", @maxlength = "50" })%></div>
				</div>
				<div class="row">
					<label for="<%= type %>_Admission_AddressLine2" class="fl">Address Line 2</label>
					<div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = type + "_Admission_AddressLine2", @maxlength = "50" })%></div>
				</div>
				<div class="row">
					<label for="<%= type %>_Admission_AddressCity" class="fl">City</label>
					<div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = type + "_Admission_AddressCity", @class = "address-city required", @maxlength = "50" })%></div>
				</div>
				<div class="row">
					<label for="<%= type %>_Admission_AddressStateCode" class="fl">
					    <span class="green">(M0050)</span>
					    State,
					    <span class="green">(M0060)</span>
					    Zip
					</label>
					<div class="fr">
					    <%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = type + "_Admission_AddressStateCode", @class = "address-state required not-zero" })%>
					    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = type + "_Admission_AddressZipCode", @class = "required numeric zip", @maxlength = "9" })%>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="row">
					<label class="fl">Home Phone</label>
					<div class="fr"><%= Html.PhoneOrFax("PhoneHomeArray", Model.PhoneHome, type + "_Admission_HomePhone", "required")%></div>
				</div>
				<div class="row">
					<label class="fl">Mobile Phone</label>
					<div class="fr"><%= Html.PhoneOrFax("PhoneMobileArray", Model.PhoneMobile, type + "_Admission_MobilePhone", "required")%></div>
				</div>
				<div class="row">
					<label for="<%= type %>_Admission_Email" class="fl">Email</label>
					<div class="fr"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = type + "_Admission_Email", @class = "email", @maxlength = "50" })%></div>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Insurance / Payor Information</legend>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_PrimaryInsurance" class="fl">Primary</label>
				<div class="fr"><%= Html.InsurancesByBranch("profile.PrimaryInsurance", Model.Profile.PrimaryInsurance.ToString(CultureInfo.InvariantCulture), Model.Profile.AgencyLocationId, Model.Profile.ServiceType, false, new { @id = type + "_Admission_PrimaryInsurance", @class = "insurance required not-zero", @status = "Primary Insurance (Required)" })%></div>
			</div>
			<div id="<%= type %>_Admission_PrimaryInsuranceContent" class="<%= Model.Profile.PrimaryInsurance >= 1000 ? "" : "hidden" %>"><%= Html.Partial("InsuranceInfoContent", new PatientInsuranceInfoViewData() { InsuranceType = "Primary", GroupId = Model.Profile.PrimaryGroupId, GroupName = Model.Profile.PrimaryGroupName, HealthPlanId = Model.Profile.PrimaryHealthPlanId, Relationship = Model.Profile.PrimaryRelationship, ActionType = type, IdPrefix = "Admission_", NamePrefix = "profile." }) %></div>
		</div>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_SecondaryInsurance" class="fl">Secondary</label>
				<div class="fr"><%= Html.InsurancesByBranch("profile.SecondaryInsurance", Model.Profile.SecondaryInsurance.ToString(), Model.Profile.AgencyLocationId, Model.Profile.ServiceType, false, new { @id = type + "_Admission_SecondaryInsurance", @class = "insurance", @status = "Secondary Insurance" })%></div>
			</div>
			<div id="<%= type %>_Admission_SecondaryInsuranceContent" class="<%= Model.Profile.SecondaryInsurance >= 1000 ? "" : "hidden" %>"><%= Html.Partial("InsuranceInfoContent", new PatientInsuranceInfoViewData() { InsuranceType = "Secondary", GroupId = Model.Profile.SecondaryGroupId, GroupName = Model.Profile.SecondaryGroupName, HealthPlanId = Model.Profile.SecondaryHealthPlanId, Relationship = Model.Profile.SecondaryRelationship, ActionType = type, IdPrefix = "Admission_", NamePrefix = "profile." })%></div>
		</div>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_TertiaryInsurance" class="fl">Tertiary</label>
				<div class="fr"><%= Html.InsurancesByBranch("profile.TertiaryInsurance", Model.Profile.TertiaryInsurance.ToString(), Model.Profile.AgencyLocationId, Model.Profile.ServiceType, false, new { @id = type + "_Admission_TertiaryInsurance", @class = "insurance", @status = "Tertiary Insurance" })%></div>
			</div>
			<div id="<%= type %>_Admission_TertiaryInsuranceContent" class="<%= Model.Profile.TertiaryInsurance >= 1000 ? "" : "hidden" %>"><%= Html.Partial("InsuranceInfoContent", new PatientInsuranceInfoViewData() { InsuranceType = "Tertiary", GroupId = Model.Profile.TertiaryGroupId, GroupName = Model.Profile.TertiaryGroupName, HealthPlanId = Model.Profile.TertiaryHealthPlanId, Relationship = Model.Profile.TertiaryRelationship, ActionType = type, IdPrefix = "Admission_", NamePrefix = "profile." })%></div>
		</div>
	</fieldset>
	<div class="inline-fieldset two-wide">
		<div>
			<fieldset>
				<legend>Evacuation Zone</legend>
				<div class="column">
					<div class="row">
						<label class="fl">Evacuation Zone</label>
						<div class="fr">
							<%  var evacuationZone = new SelectList(new[] {
								new SelectListItem { Text = "", Value = "" },
								new SelectListItem { Text = "Zone A", Value = "A" },
								new SelectListItem { Text = "Zone B", Value = "B" },
								new SelectListItem { Text = "Zone C", Value = "C"},
								new SelectListItem { Text = "Zone D", Value = "D" },
								new SelectListItem { Text = "Zone E", Value = "E" },
								new SelectListItem { Text = "Zone NE", Value = "NE"},
								new SelectListItem { Text = "Zone SN", Value = "SN" }
								}, "Value", "Text", Model.EvacuationZone); %>
							<%= Html.DropDownList("EvacuationZone", evacuationZone, new { @id = type + "_Admission_EvacuationZone" })%>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Pharmacy</legend>
				<div class="column">
				    <div class="row">
				        <label for="<%= type %>_Admission_PharmacyName" class="fl">Name</label>
				        <div class="fr"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = type + "_Admission_PharmacyName", @maxlength = "100" })%></div>
				    </div>
				</div>
				<div class="column">
				    <div class="row">
				        <label for="<%= type %>_Admission_PharmacyPhone1" class="fl">Phone</label>
				        <div class="fr"><%= Html.PhoneOrFax("PharmacyPhoneArray", Model.PharmacyPhone, type + "_Admission_PharmacyPhone", string.Empty) %></div>
				    </div>
				</div>
			</fieldset>
			<%= Html.Partial("Patient/ServiceRequired", new AdvancedArrayIdViewData(Model.ServicesRequired, type + "_Admission", string.Empty)) %>
		</div>
		<div>
			<%= Html.Partial("Patient/Triage", new ValueIdViewData<int>(Model.Triage, "_Admission"))%>
		</div>
	</div>
	<%= Html.Partial("Patient/DurableMedicalEquipment", new ArrayIdViewData(Model.DME, Model.OtherDME, type + "_Admission"))%>
	<fieldset>
		<legend>Referral Source</legend>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_ReferralPhysician" class="fl">Physician</label>
				<div class="fr"><%= Html.TextBox("ReferrerPhysician", Model.ReferrerPhysician.ToString(), new { @id = type + "_Admission_ReferrerPhysician", @class = "physician-picker" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_AdmissionSource" class="fl">Admission Source:</label>
				<div class="fr"><%= Html.AdmissionSources("AdmissionSource", Model.Profile.AdmissionSource, new { @id = type + "_Admission_AdmissionSource", @class = "admission-source" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="<%= type %>_Admission_OtherReferralSource" class="fl">Other Referral Source:</label>
				<div class="fr"><%= Html.TextBox("OtherReferralSource", Model.Profile.OtherReferralSource, new { @id = type + "_Admission_OtherReferralSource", @maxlength = "30" })%></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_PatientReferralDate" class="fl"><span class="green">(M0104)</span> Referral Date:</label>
				<div class="fr"><input type="text" class="date-picker" name="ReferralDate" value="<%= Model.Profile.ReferralDate.IsValid() ? Model.Profile.ReferralDate.ToZeroFilled() : string.Empty %>" id="<%= type %>_Admission_PatientReferralDate" /></div>
			</div>
			<div class="row">
				<label for="<%= type %>_Admission_InternalReferral" class="fl">Internal Referral:</label>
				<div class="fr"><%= Html.Users("InternalReferral", Model.Profile.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, 0, new { @id = type + "_Admission_InternalReferral", @class = "user-selector" })%></div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Discharged Reason</legend>
		<div class="wide-column">
			<div class="row">
			    <div class="template-text">
				    <%= Html.ToggleTemplates(type + "_Admission_DischargeReasonTemplates")%>
				    <textarea id="<%= type %>_Admission_DischargeReason" name="DischargeReason"><%= Model.Profile.DischargeReason%></textarea>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Comments</legend>
		<div class="wide-column">
			<div class="row">
			    <div class="template-text">
    				<%= Html.ToggleTemplates(type + "_Admission_CommentsTemplates")%>
	    			<textarea id="<%= type %>_Admission_Comments" name="Comments"><%= Model.Comments %></textarea>
	    		</div>
			</div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="save close">Save</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>
<script type="text/javascript">
    $("#<%= type %>_Admission_LocationId").on("change", function() {
        Agency.LoadAgencyInsurances($(this).val(), "<%= type %>_Admission_PrimaryInsurance", $(this).attr("service"));
        Agency.LoadAgencyInsurances($(this).val(), "<%= type %>_Admission_SecondaryInsurance", $(this).attr("service"));
        Agency.LoadAgencyInsurances($(this).val(), "<%= type %>_Admission_TertiaryInsurance", $(this).attr("service"));
    })
</script>