﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "User", FormMethod.Post, new { @id = "newUserForm" , @class = "mainform"})) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_User_EmailAddress" class="fl">E-mail Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "NewUser_EmailAddress", @class = "email required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_User_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewUser_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_User_MiddleName" class="fl">Middle Initial</label>
                <div class="fr"><%= Html.TextBox("MiddleName", string.Empty, new { @id = "NewUser_MiddleName", @class = "shortest", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="New_User_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewUser_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
             <div class="row">
                <label for="NewUser_Suffix" class="fl">Suffix</label>
                <div class="fr"><%= Html.TextBox("Suffix", string.Empty, new { @id = "NewUser_Suffix", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label class="fl">Gender</label>
                <div class="fr"><%= Html.Gender("Profile.Gender","",new { @id = "NewUser_Gender", @class = "required nonzero" }) %></div>
            </div>
            <div class="row">
                <label for="NewUser_DOB" class="fl">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker" name="DOB" id="NewUser_DOB" /></div>
            </div>
            <div class="row">
                <label for="New_User_Credentials" class="fl">Credentials</label>
                <div class="fr"><%= Html.CredentialTypes("Credentials", string.Empty, new { @id = "NewUser_Credentials", @class = "required" })%></div>
                <div class="fr" id="NewUser_OtherCredentials">
                    <label for="NewUser_OtherCredentials"><em>(Specify)</em></label>
                    <%= Html.TextBox("CredentialsOther", string.Empty, new { @id = "NewUser_OtherCredentials", @maxlength = "20", @class = "required" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_TitleType" class="fl">Title</label>
                <div class="fr"><%= Html.TitleTypes("TitleType", string.Empty, new { @id = "NewUser_TitleType", @class = "required not-zero" })%></div>
                <div class="fr" id="NewUser_OtherTitleType">
					<label for="NewUser_OtherTitleType" class="fl"><em>(Specify)</em></label>
					<%= Html.TextBox("TitleTypeOther", string.Empty, new { @id = "NewUser_OtherTitleType", @maxlength = "20", @class = "required" })%>
				</div>
            </div>
			<div class="row">
				<label class="fl">Employment Type</label>
				<div class="fr">
					<ul class="checkgroup one-wide">
						<%= Html.CheckgroupOption("EmploymentType", "Employee", false, "Employee")%>
						<%= Html.CheckgroupOption("EmploymentType", "Contractor", false, "Contractor")%>
					</ul>
				</div>
			</div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewUser_SSN" class="fl">Social Security Number</label>
                <div class="fr"><%= Html.TextBox("SSN", string.Empty, new { @id = "NewUser_SSN", @class = "ssn numeric", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_CustomId" class="fl">Agency Employee Id</label>
                <div class="fr"><%= Html.TextBox("CustomId", string.Empty, new { @id = "NewUser_CustomId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_LocationId" class="fl">Agency Branch</label>
                <div class="fr"><%= Html.BranchOnlyList("AgencyLocationId", string.Empty, 0, new { @id = "NewUser_LocationId", @class = "branch-location" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", string.Empty, new { @id = "NewUser_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", string.Empty, new { @id = "NewUser_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", string.Empty, new { @id = "NewUser_AddressCity", @maxlength = "75", @class = "address-city" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("Profile.AddressStateCode", string.Empty, new { @id = "NewUser_AddressStateCode", @class = "address-state short" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", string.Empty, new { @id = "NewUser_AddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_HomePhoneArray1" class="fl">Home Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("HomePhoneArray", string.Empty, "NewUser_HomePhoneArray", "") %></div>
            </div>
            <div class="row">
                <label for="NewUser_MobilePhoneArray1" class="fl">Mobile Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("MobilePhoneArray", string.Empty, "NewUser_MobilePhoneArray", "") %></div>
            </div>
            <div class="row">
                <label for="NewUser_FaxPhoneArray1" class="fl">Fax Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxPhoneArray", string.Empty, "NewUser_FaxPhoneArray", "") %></div>
            </div>
            
            <div class="row">
                <label for="NewUser_HireDate" class="fl">Hire Date</label>
                <div class="fr"><input type="text" class="date-picker" name="HireDate"  id="NewUser_HireDate" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <div class="wide-column">
            <div class="row">
    <%  string[] roleNames = new string[] { "Administrator", "Director of Nursing", "Case Manager", "Clerk (non-clinical)",
		    "Physical Therapist", "Occupational Therapist", "Speech Therapist", "Medical Social Worker", "Home Health Aide",
			"Scheduler", "Biller", "Quality Assurance", "Physician", "Office Manager", "Community Liason Officer/Marketer",
			"External Referral Source", "Driver/Transportation", "Office Staff", "State Surveyor/Auditor" }; %>
			    <%= Html.CheckgroupOptions("AgencyRoleList", "NewUser_Role", roleNames, new string[] {}, new { @class = "required"}) %>
            </div>
        </div>
    </fieldset>
    <%  if (Current.Services.IsAlone()) { %>
	<%= Html.Hidden("ServiceArray", (int)Current.Services, new { @id = "EditUser_ServiceArray" }) %>
    <%  } else { %>
    <fieldset>
		<legend>Service</legend>
        <div class="wide-column">
		    <div class="row"><%= Html.ServiceCheckgroupOptions("ServiceArray", "NewUser_ServiceArray", Current.Services, Current.Services, false, new { @class = "required" }) %></div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="NewUser_Comments" name="Comments" maxcharacters="500" class="taller"></textarea>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save next">Save &#38; Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>