﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserLicense>" %>
<div class="wrapper main">
    <%= Html.Hidden("Id", Model.UserId, new { })%>
    <ul class="buttons">
        <li class="fl"><a class="new-license" guid="<%= Model.UserId %>">New License</a></li>
        <li class="fr"><a class="grid-refresh">Refresh</a></li>
    </ul>
    <div class="clr"></div>
    <div id="EditUser_LicenseGrid" class="acore-grid user-license-listcontainer" guid="<%= Model.UserId %>">
    <% Html.RenderPartial("~/Views/License/List.ascx", Model.Licenses??new List<License>()); %>
    </div>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
</div>