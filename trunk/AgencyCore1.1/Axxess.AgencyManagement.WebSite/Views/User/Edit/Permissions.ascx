﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GeneralPermission>" %>
<div class="wrapper main userpermissions">
<% var userServicesList = Model.AvailableServices.ToList();%>
<%  using (Html.BeginForm("UpdatePermissions", "User", FormMethod.Post, new { @id = "EditUserPermissions_Form", @class = "mainform" })) { %>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditUserPermissions_UserId" })%>
    <fieldset>
     <legend>Location Access</legend>
     <%= Html.Locations2(Model.ServiceLocations, Model.AvailableServices, "required")%>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="one-wide checkgroup">
                    <li class="option">
	                    <div class="wrapper">
							<input id="EditUserPermissions_AllPermissions" type="checkbox" value="" />
							<label for="EditUserPermissions_AllPermissions">Select all Permissions</label>
						</div>
                    </li>
                </ul>
            </div>
        </div>
        <% foreach(AgencyServices service in userServicesList){ %>
			<%= Html.Hidden("Services", (int)service)%>
		<% } %>
		<div id="EditUserPermissions_Accordion">
			<% foreach (var categoryPermission in Model.Permissions){ %>		   
				<h3><input type="checkbox" class="select-all" target="div" <%= categoryPermission.Key.IsAllSelected.ToChecked() %>/><%= categoryPermission.Key.Name %></h3>
				<div class="<%= categoryPermission.Key.IsSubCategory ? "accordion" : string.Empty %>">
					<% if(!categoryPermission.Key.IsSubCategory) { %>
						<% foreach (var groupPermission in categoryPermission){ %>
							<div class="acore-grid">
								<ul>
									<li class="ac"><h3>Permissions</h3></li>
									<li>
										<span class="grid-third">Description</span>
										<% foreach(int service in groupPermission.Key.AvailableForServices){ %>
											<span class="grid-fifth fr al"><label><input type="checkbox" class="select-all" target="ol" service="<%= service %>" <%= ((groupPermission.Key.AllSelected & service) == service).ToChecked() %>/><% if(Enum.IsDefined(typeof(AgencyServices), service) && !Model.AvailableServices.IsAlone()) { %><%= ((AgencyServices)service).GetDescription() %></label><% } %></span>
										<% } %>
									</li>
								</ul>
								<ol>
									<% foreach (var permission in groupPermission) { %>
										<li tooltip="<%= permission.ToolTip %>">
											<%= Html.Hidden("Permissions[" + groupPermission.Key.Id + "].Value.Index", permission.ChildId)%>
											<%= Html.Hidden("Permissions[" + groupPermission.Key.Id + "].Value[" + permission.ChildId + "].Key", permission.ChildId)%>
											<span class="grid-third"><%= permission.Description %></span>
											<% foreach(int service in groupPermission.Key.AvailableForServices){ %>
												<span class="grid-fifth al fr"><input type="checkbox" name="<%= "Permissions[" + groupPermission.Key.Id + "].Value[" + permission.ChildId + "].Value" %>" value="<%= service %>" <%= ((permission.Checked & service) == service).ToChecked() %>/></span>
											<% } %>
											<div class="clr"/>
										</li>
									<% } %>
									<%= Html.Hidden("Permissions.Index", groupPermission.Key.Id)%>
									<%= Html.Hidden("Permissions[" + groupPermission.Key.Id + "].Key", groupPermission.Key.Id)%>
								</ol>
							</div>
						<% } %>
					<% } else { %>
						<% foreach (var groupPermission in categoryPermission){ %>
							<h3><input type="checkbox" class="select-all" target="div" <%= groupPermission.Key.IsAllSelected.ToChecked() %>/><%= groupPermission.Key.Name %></h3>
							<div>
								<%= Html.Hidden("Permissions.Index", groupPermission.Key.Id)%>
								<%= Html.Hidden("Permissions[" + groupPermission.Key.Id + "].Key", groupPermission.Key.Id)%>
								<%--<% var services = groupPermission.Key.HasServiceScope ? (Model.AvailableServices & groupPermission.Key.AvailableForServices).ToList() : AgencyServices.None.ToList(); %>--%>
								<div class="acore-grid">
									<ul>
										<li class="ac"><h3>Permissions</h3></li>
										<li>
											<span class="grid-third">Description</span>
											<% foreach(int service in groupPermission.Key.AvailableForServices){ %>
												<span class="grid-fifth al fr"><label><input type="checkbox" class="select-all" target="ol" <%= ((groupPermission.Key.AllSelected & service) == service).ToChecked() %> service="<%= service %>" /><% if(Enum.IsDefined(typeof(AgencyServices), service) && !Model.AvailableServices.IsAlone()) { %> <%= ((AgencyServices)service).GetDescription() %><% } %></label></span>
											<% } %>
										</li>
									</ul>
									<ol>
										<% foreach (var permission in groupPermission) { %>
											<li tooltip="<%= permission.ToolTip %>">
												<%= Html.Hidden("Permissions[" + groupPermission.Key.Id + "].Value.Index", permission.ChildId)%>
												<%= Html.Hidden("Permissions[" + groupPermission.Key.Id + "].Value[" + permission.ChildId + "].Key", permission.ChildId)%>
												<span class="grid-third"><%= permission.Description %></span>
												<% foreach(int service in groupPermission.Key.AvailableForServices){ %>
													<span class="grid-fifth al fr"><input type="checkbox" name="<%= "Permissions[" + groupPermission.Key.Id + "].Value[" + permission.ChildId + "].Value" %>" value="<%= groupPermission.Key.HasServiceScope ? (int)service : (int)AgencyServicesBoundary.All %>" <%= ((permission.Checked & (int)service) == (int)service).ToChecked() %>/></span>
												<% } %>
												<div class="clr"/>
											</li>
										<% } %>
									</ol>
								</div>
							</div>
						<% } %>
					<% } %>
				</div>
			<% } %>
			<h3><input type="checkbox" class="select-all" target="div" <%= Model.IsAllReportsSelected.ToChecked() %>/>Reports</h3>
				<div class="accordion">
				<% foreach (var groupPermission in Model.ReportPermissions){ %>
					<h3><input type="checkbox" class="select-all" target="div" <%= groupPermission.Key.IsAllSelected.ToChecked() %>/><%= groupPermission.Key.Name %></h3>
					<div>
						<%--<% var services = groupPermission.Key.HasServiceScope && groupPermission.Key.AvailableForServices == AgencyServices.None ? AgencyServices.None.ToList() : (Model.AvailableServices & groupPermission.Key.AvailableForServices).ToList(); %>--%>
						<div class="acore-grid">
							<ul>
								<li class="ac"><h3>Permissions</h3></li>
								<li>
									<span class="grid-third">Description</span>
									<% foreach(AgencyServices service in groupPermission.Key.ServicesInCategory.ToList()){ %>
										<span class="grid-quarter ac">
											<label><input type="checkbox" class="select-all" target="all" service="<%= (int)service %>" <%= ((groupPermission.Key.ServicesSelected & service) == service).ToChecked() %>/><%= service != AgencyServices.None && !Model.AvailableServices.IsAlone() ? service.GetDescription() : "All" %></label><br/>
											<label><input type="checkbox" class="select-all" target="ol" permission="<%= (int)PermissionActions.ViewList %>" service="<%= (int)service %>" <%= ((groupPermission.Key.AllViewSelected & service) == service).ToChecked() %>/>View</label>
											|
											<label><input type="checkbox" class="select-all" target="ol" permission="<%= (int)PermissionActions.Export %>" service="<%= (int)service %>" <%= ((groupPermission.Key.AllExportSelected & service) == service).ToChecked() %>/>Export</label>
										</span>
									<% } %>
								</li>
							</ul>
							<ol>
								<% foreach (var permission in groupPermission) { %>
									<li tooltip="<%= permission.ToolTip %>">
										<%= Html.Hidden("ReportPermissions.Index", permission.Id)%>
										<%= Html.Hidden("ReportPermissions[" + permission.Id + "].Key", permission.Id)%>
										
										<%= Html.Hidden("ReportPermissions[" + permission.Id + "].Value.Index", (int)PermissionActions.ViewList)%>
										<%= Html.Hidden("ReportPermissions[" + permission.Id + "].Value[" + (int)PermissionActions.ViewList + "].Key", (int)PermissionActions.ViewList)%>
										
										<%= Html.Hidden("ReportPermissions[" + permission.Id + "].Value.Index", (int)PermissionActions.Export)%>
										<%= Html.Hidden("ReportPermissions[" + permission.Id + "].Value[" + (int)PermissionActions.Export + "].Key", (int)PermissionActions.Export)%>
										<span class="grid-third"><%= permission.Description %></span>
										<% foreach(AgencyServices service in permission.AvailableForServices.ToList()){ %>
											<span class="grid-quarter ac">
												<input type="checkbox" name="<%= "ReportPermissions[" + permission.Id + "].Value[" + (int)PermissionActions.ViewList + "].Value" %>" permission="<%= (int)PermissionActions.ViewList %>" value="<%= (int)service %>" <%= permission.ViewChecked.Has(service).ToChecked() %>/>
												|
												<input type="checkbox" name="<%= "ReportPermissions[" + permission.Id + "].Value[" + (int)PermissionActions.Export + "].Value" %>" permission="<%= (int)PermissionActions.Export %>" value="<%= (int)service %>" <%= permission.ExportChecked.Has(service).ToChecked() %>/>
											</span>
										<% } %>
									</li>
								<% } %>
							</ol>
						</div>
					</div>
				<% } %>
			</div>
		</div>
    </fieldset>
    <%--<%= Html.PermissionList4("EditUser", Model.UserPermissions, Model.AvailableServices) %>--%>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="save">Save</a></li>
        <li><a class="save next">Save &#38; Next</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>
<script>
	
</script>