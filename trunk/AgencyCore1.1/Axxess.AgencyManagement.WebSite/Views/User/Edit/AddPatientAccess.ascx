﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddPatientAccess", "User", FormMethod.Post, new { @id = "AddPatientAccess_Form" })) { %>
    <%= Html.Hidden("UserId",Model.Id) %>
    <fieldset class="medication">
        <div class="wide-column">
            <div class="row">
                <%= Html.Telerik().Grid<SelectedPatient>().Name("AddUserPatientAccess_List").HtmlAttributes(new { @style = "position-relative" }).ToolBar(commnds => commnds.Custom()).Columns(columns => {
                        columns.Bound(p => p.PatientId).Hidden(true);
                        columns.Bound(p => p.Selected).Title("").ClientTemplate("<input name=\"selectedPatient\" type=\"checkbox\" value=\"<#= PatientId #>\"/>").Width(40);
                        columns.Bound(p => p.DisplayName).Title("Name");
                    }).DataBinding(databinding => databinding.Ajax().Select("GetAddPatientAccess", "User", new { userId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false) %>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save">Add Selected</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>
<script type="text/javascript">
    $("#AddUserPatientAccess_List .t-grid-toolbar").html("").append($("<div/>").GridSearch());
    $("#AddUserPatientAccess_List .t-grid-content").css("top", 60);
</script>