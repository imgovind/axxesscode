﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
<%  string[] roles = Model.Roles.IsNotNullOrEmpty() ? Model.Roles.Split(';') : new string[] {};  %>
<%  if (roles.Contains("1") || roles.Contains("2") || roles.Contains("5") || roles.Contains("11") || roles.Contains("12") || roles.Contains("15")) { %>
    <fieldset>
        <legend>Notice</legend>
        <div class="wide-column">
            <div class="row">
                <p>
                    This user already has access to all patients because of his/her role. &#160;If you wish to alter
                    patient access for this user please change his/her role to a clinician role under the the
                    &#8220;Information&#8221; tab.
                </p>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
<%  } else { %>
    <fieldset>
        <legend>Notice</legend>
        <div class="wide-column">
            <div class="row">
                <p>
                    Use this tool to grant access to this user to view all visits/documents including those not
                    assigned to them for those patients listed under &#8220;Granted Access.&#8221; &#160;This user
                    will always have access to all visits/documents assigned to them even on patients listed under
                    &#8220;Denied Access.&#8221;
                </p>
            </div>
        </div>
    </fieldset>
    <div class="patient-access" guid="<%= Model.Id %>"></div>
<%  } %>
</div>