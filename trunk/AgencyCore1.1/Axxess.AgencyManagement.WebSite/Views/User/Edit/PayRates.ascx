﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserPayRatesViewData>" %>
<div class="wrapper main">
    <%= Html.Hidden("Id", Model.UserId, new { })%>
    <ul class="buttons fr">
        <li><a class="new-rate" guid="<%= Model.UserId %>">New Pay Rate</a></li>
        <li><a class="load-from-other-user">Load From other User</a></li>
        <li><a class="grid-refresh">Refresh</a></li>
    </ul>
    <div class="clr"></div>
    <div id="EditUser_PayRateGrid" class="acore-grid user-rates-listcontainer" guid="<%= Model.UserId %>">
	    <% Html.RenderPartial("Rate/List", Model.PayorRates ?? new List<UserPayorRates>()); %>
    </div>
    <ul class="buttons ac">
        <li><a class="back">Back</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
</div>