﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "User", FormMethod.Post, new { @id = "EditUser_Form", @class = "mainform" })){ %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUser_Id" }) %>
    <fieldset>
        <legend>User Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditUser_FirstName" class="fl">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditUser_FirstName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_MiddleName" class="fl">Middle Initial</label>
                <div class="fr"><%= Html.TextBox("MiddleName", Model.MiddleName, new { @id = "EditUser_MiddleName", @class = "shortest", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_LastName" class="fl">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditUser_LastName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_Suffix" class="fl">Suffix</label>
                <div class="fr"><%= Html.TextBox("Suffix", Model.Suffix, new { @id = "EditUser_Suffix", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label class="fl">Gender</label>
                <div class="fr"><%= Html.Gender("Profile.Gender", Model.Profile.Gender, new { @id = "EditUser_Gender", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_DOB" class="fl">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker" name="DOB" value="<%= !Model.DOB.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.DOB.ToString("MM/dd/yyyy") : string.Empty %>" id="EditUser_DOB" /></div>
            </div>
            <div class="row">
                <label for="EditUser_Credentials" class="fl">Credentials</label>
                <div class="fr"><%= Html.CredentialTypes("Credentials", Model.Credentials, new { @id = "EditUser_Credentials", @class = "required" })%></div>
                <div class="fr" id="EditUser_CredentialsMore">
                    <label for="EditUser_OtherCredentials"><em>(Specify)</em></label>
                    <%= Html.TextBox("CredentialsOther", Model.CredentialsOther, new { @id = "EditUser_OtherCredentials", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUser_TitleType" class="fl">Title</label>
                <div class="fr"><%= Html.TitleTypes("TitleType", Model.TitleType, new { @id = "EditUser_TitleType", @class = "required" })%></div>
                <div class="fr" id="EditUser_TitleTypeMore">
                    <label for="EditUser_OtherTitleType"><em>(Specify)</em></label>
                    <%= Html.TextBox("TitleTypeOther", Model.TitleTypeOther, new { @id = "EditUser_OtherTitleType", @maxlength = "20" })%>
                </div>
            </div>
			<div class="row">
				<label class="fl">Employment Type</label>
				<div class="fr">
					<ul class="checkgroup one-wide">
						<%= Html.CheckgroupOption("EmploymentType", "Employee", "Employee" == Model.EmploymentType, "Employee")%>
						<%= Html.CheckgroupOption("EmploymentType", "Contractor", "Contractor" == Model.EmploymentType, "Contractor")%>
					</ul>
				</div>
			</div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditUser_SSN" class="fl">Social Security Number</label>
                <div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = "EditUser_SSN", @class = "ssn" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_CustomId" class="fl">Agency Employee Id</label>
                <div class="fr"> <%= Html.TextBox("CustomId", Model.CustomId, new { @id = "EditUser_CustomId", @maxlength = "20" })%></div>
            </div>
            
            <div class="row">
                <label for="EditUser_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "EditUser_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "EditUser_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "EditUser_AddressCity", @maxlength = "75", @class = "address-city" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "EditUser_AddressStateCode", @class = "address-state short" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "EditUser_AddressZipCode", @class = "numeric zip shorter", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUser_HomePhoneArray" class="fl">Home Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("HomePhoneArray", Model.Profile.PhoneHome, "EditUser_HomePhoneArray", "") %></div>
            </div>
            <div class="row">
                <label for="EditUser_MobilePhoneArray" class="fl">Mobile Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("MobilePhoneArray", Model.Profile.PhoneMobile, "EditUser_MobilePhoneArray", "")%></div>
            </div>
            <div class="row">
                <label for="EditUser_FaxPhoneArray" class="fl">Fax</label>
                <div class="fr"><%= Html.PhoneOrFax("FaxPhoneArray", Model.Profile.PhoneFax, "EditUser_FaxPhoneArray", "") %></div>
            </div>
        </div>
    </fieldset>
    <%if (Current.Services.IsAlone()){%>
         <%=Html.Hidden("ServiceArray", (int)Current.Services, new { @id= "EditUser_ServiceArray"})%>
         <%=Html.Hidden("PreferredService", (int)Current.Services, new { @id = "EditUser_PreferredService" })%>
    <%} else { %>
      <fieldset>
         <legend>Service</legend>
           <div class="wide-column">
            <div class="row">
	            <label>Accessible Services</label>
               <%= Html.ServiceCheckgroupOptions("ServiceArray", "EditUser_ServiceArray", Current.Services, Model.AccessibleServices, false, new { @class = "required" })%>
             </div>
           
        </div>
        <div class="wide-column">
           <div class="row" id="EditUser_PreferredServices">
	            <label>Preferred Service</label>
               <%= Html.ServiceCheckgroupOptions("PreferredService", "EditUser_PreferredService", Current.Services, Model.PreferredService, true, new { @class = "required" })%>
           </div>
        </div>
      </fieldset>
    <%} %>
    <fieldset>
        <legend>Roles</legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] roles = Model.Roles.IsNotNullOrEmpty() ? Model.Roles.Split(';') : new string[] {};
					string[] roleNames = new string[]{"Administrator","Director of Nursing","Case Manager","Clerk (non-clinical)",
						"Physical Therapist","Occupational Therapist","Speech Therapist","Medical Social Worker","Home Health Aide",
						"Scheduler","Biller","Quality Assurance","Physician","Office Manager","Community Liason Officer/Marketer",
						"External Referral Source","Driver/Transportation","Office Staff","State Surveyor/Auditor" };%>
				<%= Html.CheckgroupOptions("AgencyRoleList", "EditUser_Role", roleNames, roles, new { @class = "required"}) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Access &#38; Restrictions</legend>
        <div class="wide-column">
            <div class="row narrower">
                <ul class="checkgroup one-wide"><%= Html.CheckgroupOption("AllowWeekendAccess", "EditUser_AllowWeekendAccess", "true", Model.AllowWeekendAccess, "Allow Weekend Access", null)%></ul>
            </div>
        </div>
        <div  class="column">   
            <div class="row">
                <label for="EditUser_EarliestLoginTime" class="fl">Earliest Login Time</label>
                <div class="fr"><input value="<%= Model.EarliestLoginTime %>" type="text" size="10" id="EditUser_EarliestLoginTime" name="EarliestLoginTime" class="time-picker" /></div>
            </div>
        </div>
        <div  class="column">
            <div class="row">
                <label for="EditUser_AutomaticLogoutTime" class="fl">Automatic Logout Time</label>
                <div class="fr"><input value="<%= Model.AutomaticLogoutTime %>" type="text" size="10" id="EditUser_AutomaticLogoutTime" name="AutomaticLogoutTime" class="time-picker" /></div>
            </div>
        </div>
    </fieldset>
    <%  if (Current.IsTestingAgency) { %>
        <div>
            <fieldset>
                <legend>Trial Account</legend>
                <div class="column">
                    <div class="row">
                        <label for="EditUser_AccountExpireDate" class="fl">Trial Expiration Date</label>
                        <div class="fr"><input type="text" class="date-picker" name="AccountExpireDate" id="EditUser_AccountExpireDate" value="<%= Model.AccountExpireDate.IsValid() ? Model.AccountExpireDate.ToShortDateString() : string.Empty %>" /></div>
                    </div>
                </div>
            </fieldset>
        </div>
    <%  } %>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="EditUser_Comments" name="Comments" maxcharacters="500" class="taller"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog) { %>
    	<a class="fr img icon32 log" onclick="Log.LoadUserLog('<%= Model.Id %>')"/>
	<% } %>
    <ul class="buttons ac">
        <li><a class="save">Save</a></li>
        <li><a class="save next">Save &#38; Next</a></li>
        <li><a class="next">Next</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>