﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<div class="wrapper main">
    <fieldset class="half center">
        <legend>GoToMeeting</legend>
        <div class="column">
            <div class="row">
                <label for="MeetingId" class="fl">Meeting ID</label>
                <div class="fr"><input type="text" name="MeetingId" id="MeetingId" class="shorter numeric" maxlength="9" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a id="JoinMeeting">Join</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
</div>
<script type="text/javascript">
    $(".numeric").numeric();
    $("#JoinMeeting").on("click", function() {
        if ($("#MeetingId").val().length != 9) U.Growl("Please enter your nine digit meeting ID.", "error");
        else {
            var meetUrl = "https://www.gotomeeting.com/join/" + $("#MeetingId").val();
            $("#window_GoToMeeting_content").empty().append($("<iframe/>", { "src": meetUrl, "class": "fit-window" }))
        }
    })
</script>