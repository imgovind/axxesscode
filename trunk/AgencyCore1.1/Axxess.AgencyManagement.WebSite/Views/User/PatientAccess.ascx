﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%= Html.Hidden("UserId", Model.Id, new { @id = "PatientAccessUserId"}) %>
<div class="main">
<div class="clear"></div>
<%= Html
      .Telerik()
      .Grid<SelectedPatient>()
      .Name("List_UserPatientAccess")
      .HtmlAttributes(new {@style = "height:auto; position: relative;margin-bottom: 200px;"})
      .ToolBar(commnds => commnds.Custom())
      .Columns(columns =>
      {
          columns.Bound(p => p.PatientId).Hidden(true);
          columns.Bound(p => p.Selected).Title("").ClientTemplate("<input type=\"checkbox\" onclick=\"Patient.PatientAccess.UserClick(this)\" value=\"<#=PatientUserId#>\"/>").Width(40);
          columns.Bound(p => p.DisplayName).Title("Name");
      })
      .DataBinding(databinding => databinding.Ajax().Select("GetPatientAccess", "User", new { userId = Model.Id }))
      .Sortable()
      .Scrollable(scrolling => scrolling.Enabled(true))
      .Footer(false)
      .ClientEvents(events => events.OnRowDataBound("Patient.UserAccess.OnRowBound")) %>
</div>
<script type="text/javascript">
    $("#List_UserPatientAccess .t-grid-toolbar").html("")
    .append($("<div/>").GridSearch())
    .append("<div class='fl'>Note: Changes are saved automatically.</div>");
    $("#List_UserPatientAccess .t-grid-content").css({ top: 59 });
</script>