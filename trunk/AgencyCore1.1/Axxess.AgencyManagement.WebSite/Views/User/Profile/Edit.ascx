﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<span class="wintitle">Edit Profile | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Profile", "User", FormMethod.Post, new { @id = "EditProfile_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Profile_Id" }) %>
    <div class="inline-fieldset two-wide">
        <div>
            <fieldset>
                <legend>Login Password</legend>
                <div class="wide-column">
                    <div class="row">
                        <label for="EditProfile_CurrentPassword">Current Password</label>
                        <div class="fr"><%= Html.Password("PasswordChanger.CurrentPassword", string.Empty, new { @id = "EditProfile_CurrentPassword", @maxlength = "15" }) %></div>
                    </div>
                    <div class="row">
                        <label for="EditProfile_NewPassword">New Password</label>
                        <div class="fr"><%= Html.Password("PasswordChanger.NewPassword", string.Empty, new { @id = "EditProfile_NewPassword", @maxlength = "15" }) %></div>
                    </div>
                    <div class="row">
                        <label for="EditProfile_NewPasswordConfirm">Confirm New Password</label>
                        <div class="fr"><%= Html.Password("PasswordChanger.NewPasswordConfirm", string.Empty, new { @id = "EditProfile_NewPasswordConfirm", @maxlength = "15" }) %></div>
                        <div class="ac clr">
                            <div class="password-check"></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Electronic Signature</legend>
                <div class="wide-column">
                    <div class="row">
                        <label for="EditProfile_CurrentSignature">Current Signature</label>
                        <div class="fr"><%= Html.Password("SignatureChanger.CurrentSignature", string.Empty, new { @id = "EditProfile_CurrentSignature", @maxlength = "15" }) %></div>
                    </div>
                    <div class="row">
                        <label for="EditProfile_NewSignature">New Signature</label>
                        <div class="fr"><%= Html.Password("SignatureChanger.NewSignature", string.Empty, new { @id = "EditProfile_NewSignature", @maxlength = "15" }) %></div>
                    </div>
                    <div class="row">
                        <label for="EditProfile_NewSignatureConfirm">Confirm New Signature</label>
                        <div class="fr"><%= Html.Password("SignatureChanger.NewSignatureConfirm", string.Empty, new { @id = "EditProfile_NewSignatureConfirm", @maxlength = "15" }) %></div>
                        <div class="ac clr">
                            <div class="signature-check"></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditProfile_AddressLine1" class="fl">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "EditProfile_AddressLine1", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditProfile_AddressLine2" class="fl">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "EditProfile_AddressLine2", @maxlength = "75" }) %></div>
            </div>
            <div class="row">
                <label for="EditProfile_AddressCity" class="fl">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "EditProfile_AddressCity", @maxlength = "75", @class = "address-city" })%></div>
            </div>
            <div class="row">
                <label for="EditProfile_AddressStateCode" class="fl">State, Zip</label>
                <div class="fr">
                    <%= Html.States("Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "EditProfile_AddressStateCode", @class = "address-state short" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "EditProfile_AddressZipCode", @class = "numeric zip shorter", @maxlength = "9" }) %>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditProfile_PhonePrimary1">Home Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("HomePhoneArray", Model.Profile.PhoneHome, "EditProfile_PhonePrimary", string.Empty)%></div>
            </div>
            <div class="row">
                <label for="EditProfile_PhoneAlternate1">Mobile Phone</label>
                <div class="fr"><%= Html.PhoneOrFax("MobilePhoneArray", Model.Profile.PhoneMobile, "EditProfile_PhoneAlternate", string.Empty) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>