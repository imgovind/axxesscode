﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="EditUser_Tabs" class="tabs vertical-tabs maintab">
    <%= Html.Hidden("UserId", Guid.Empty, new { @class = "tabinput" })%>
    <ul class="vertical-tab-list strong">
        <li><a href="#NewUser_Information" name="Information" url="User/Information">Information</a></li>
        <li><a href="User/Permissions" name="Permissions">Permissions</a></li>
        <li><a href="User/Licenses" name="Licenses">Licenses</a></li>
        <li><a href="User/Rates" name="Rates">Pay Rates</a></li>

    </ul>
     <div id="NewUser_Information" class="tab-content"><%  Html.RenderPartial("New/Information", new User()); %></div>
</div>
