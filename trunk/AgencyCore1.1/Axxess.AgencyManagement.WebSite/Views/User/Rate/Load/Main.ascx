﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main"> 
<%  using (Html.BeginForm("UpdateLoadUserRate", "User", FormMethod.Post, new { @id = "NewUserPayRate_Form", @class = "mainform" })) { %>
    <%= Html.Hidden("ToUserId", Model, new { }) %>
    <fieldset>
        <legend>Import Pay Rates</legend>
        <div class="wide-column">
            <div class="row">
                <label for="EditUserRate_UserId" class="fl">Duplicate Pay Rates from</label>
                <div class="fl"><%= Html.RatedUser("FromUserId", Guid.Empty, Model, true, "-- Select User --", new { @id = "LoadRate_FromUserId" }) %></div>
            </div>
        </div>
    </fieldset>
    <div id="LoadRate_PayRateGrid" class="acore-grid load-rates-listcontainer selectable"><% Html.RenderPartial("Rate/Load/Content", new List<UserPayorRates>()); %></div>
    <ul class="buttons ac">
        <li><a class="save close">Apply</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>