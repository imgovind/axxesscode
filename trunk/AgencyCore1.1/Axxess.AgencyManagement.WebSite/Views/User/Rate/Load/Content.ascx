﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserPayorRates>>" %>
<ul>
    <li class="ac"><h3>Pay Rates</h3></li>
    <li>
	    <span class="grid-check"/>
        <span class="grid-quarter">Task</span>
        <span class="grid-fifth">Rate Type</span>
        <span class="grid-fifth">User Rate</span>
        <span class="grid-fifth">Mileage Rate</span>
    </li>
</ul>
<% var i = 1; %>
<% if (Model != null && Model.Count > 0){ %>
  <% foreach (var rates in Model){ %>
   <% if (rates.PayorRates.IsNotNullOrEmpty()){  %>
<ol>
    <li class="group-header no-hover">
	    <input type="hidden" name="Payors.Index" value="<%= i %>" />
		<input type="hidden" name="Payors[<%= i %>].Id" value="<%=rates.PayorId %>" />
		<span class="grid-check"><input type="checkbox" value="true" name="Payors[<%= i %>].IsAll" class="selectall" /></span> 
		<label class="strong"><%= rates.InsuranceName%></label>
    </li>
    <% foreach (var rate in rates.PayorRates){ %>
    <li>
        <span class="grid-check"><input type="checkbox" value="<%=rate.Id %>" name="Payors[<%= i %>].Tasks" class="input" /></span>
        <span class="grid-quarter"><%= rate.DisciplineTaskName %></span>
        <span class="grid-fifth"><%= rate.RateTypeName %></span>
        <span class="grid-fifth"><%= rate.Rate.ToCurrencyFormat() %></span>
        <span class="grid-fifth"><%= rate.MileageRate.ToCurrencyFormat() %></span>
    </li>
    <% } %>
</ol>
<% i++;
     } %>
<%} %>
<% } else{ %>
    <ol><li><h4 class="blue">No Pay Rates Found</h4></li></ol>
<% } %> 
