﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Rate/Add", "User", FormMethod.Post, new { @id = "NewUserPayRate_Form", @class = "mainform" })) { %>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "NewUserPayRate_UserId" })%>
    <fieldset>
        <legend>New Pay Rate</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewUserPayRate_Insurance" class="fl">Insurance</label>
                <div class="fr"><%= Html.Insurances("Insurance", Model.Insurance.ToString(), (int)AgencyServices.HomeHealth, false, true, "** Select Insurance **", new { @id = "NewUserPayRate_Insurance", @class = "insurance required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_Task" class="fl">Task</label>
                <div class="fr"><%= Html.UserDisciplineTask("Id", 0, Model.UserId, Model.Insurance, true, new { @id = "NewUserPayRate_Task", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_RateType" class="fl">Rate Type</label>
                <div class="fr"><%= Html.UserRateTypes("RateType", "0", new { @id = "NewUserPayRate_RateType", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_VisitRate" class="fl">User Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", string.Empty, new { @id = "NewUserPayRate_VisitRate", @class = "currency required", @maxlength = "8" })%></div>
            </div>
            <div class="row">
                <label for="NewUserPayRate_MileageRate" class="fl">Mileage Rate</label>
                <div class="fr"><%= Html.TextBox("MileageRate", string.Empty, new { @id = "NewUserPayRate_MileageRate", @class = "currency", @maxlength = "8" })%></div>
            </div>
        </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Add</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
<%  } %>
</div>