﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Rate/Update", "User", FormMethod.Post, new { @id = "EditUserPayRate_Form" , @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUserPayRate_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditUserPayRate_UserId" })%>
    <fieldset>
        <legend>Edit Pay Rate</legend>
        <div class="wide-column">
    <%  if (Model.Insurance <= 0 && Model.RateType == 0) { %>
            <div class="row">
                <label for="EditUserPayRate_Insurance" class="fl">Insurance</label>
                <div class="fr"><%= Html.Insurances("Insurance", Model.Insurance.ToString(), (int)AgencyServices.HomeHealth, false, true, "-- Select Insurance --", new { @id = "EditUserPayRate_Insurance", @class = "insurance required not-zero" })%></div>
            </div>
    <%  } else { %>
            <div class="row no-input">
                <label for="EditUserPayRate_Insurance" class="fl">Insurance</label>
                <div class="fr"><%= Model.InsuranceName %></div>
                <%= Html.Hidden("Insurance", Model.Insurance, new { @id = "EditUserPayRate_Insurance" }) %>
            </div>
    <%  } %>
            <div class="row no-input">
                <label class="fl">Task</label>
                <div class="fr"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_RateType" class="fl">Rate Type</label>
                <div class="fr"><%=Html.UserRateTypes("RateType", "0", new { @id = "EditUserPayRate_RateType", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_VisitRate" class="fl">User Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", String.Format("{0:0.00}",Model.Rate), new { @id = "EditUserPayRate_VisitRate", @class = "required currency", @maxlength = "8" })%></div>
            </div>
            <div class="row">
                <label for="EditUserPayRate_MileageRate" class="fl">Mileage Rate</label>
                <div class="fr"><%= Html.TextBox("MileageRate", String.Format("{0:0.00}",Model.MileageRate), new { @id = "EditUserPayRate_MileageRate", @class = "currency", @maxlength = "8" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Update</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>