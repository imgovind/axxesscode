﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserPayorRates>>" %>
<ul>
    <li class="ac"><h3>Pay Rates</h3></li>
    <li>
        <span class="grid-quarter">Task</span>
        <span class="grid-fifth">Rate Type</span>
        <span class="grid-fifth">User Rate</span>
        <span class="grid-fifth">Mileage Rate</span>
        <span>Action</span>
    </li>
</ul>
<%  if (Model != null && Model.Count() > 0) { %>
    <%  foreach (var rates in Model) { %>
        <%  if (rates.PayorRates.IsNotNullOrEmpty()) { %>
<ol>
    <li class="group-header no-hover">
        <label class="strong"><%= rates.InsuranceName %></label>
        [ <a payorId="<%= rates.PayorId%>" class="new-payor-rate">New Rate</a> ]
    </li>
            <%  foreach (var rate in rates.PayorRates) { %>
    <li>
        <span class="grid-quarter"><%= rate.DisciplineTaskName%></span>
        <span class="grid-fifth"><%= rate.RateTypeName%></span>
        <span class="grid-fifth"><%= rate.Rate.ToCurrencyFormat() %></span>
        <span class="grid-fifth"><%= rate.MileageRate.ToCurrencyFormat() %></span>
        <span><a class="link edit-rate" guid="<%= rate.Id %>" insurance="<%= rate.Insurance %>">Edit</a><a class="link delete-rate" guid="<%= rate.Id %>" insurance="<%= rate.Insurance %>">Delete</a></span>
    </li>
            <%  } %>
</ol>
        <%  } %>
    <%  } %>
<%  } else { %>
<ol>
    <li class="no-hover"><h4>No Pay Rates Found</h4></li>
</ol>
<%  } %>