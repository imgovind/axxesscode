﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Reset <%= Current.DisplayName %>&#8217;s Signature | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <fieldset>
        <legend>Reset Signature</legend>
        <div class="wide-column">
            <div class="row">
                <p>
                    Click on the button below to reset your signature. An e-mail with
                    instructions on how to reset your signature will be sent to <%= Model %>.
                </p>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a id="lnkRequestSignatureReset">Reset Signature</a></li>
    </ul>
</div>