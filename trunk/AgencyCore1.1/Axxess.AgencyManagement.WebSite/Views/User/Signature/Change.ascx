﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Reset", "Account", FormMethod.Post, new { @id = "ResetSignature_Form" })) { %>
    <fieldset>
        <legend>Electronic Signature Reset</legend>
        <div class="wide-column">
            <div class="row">
                <p>
                    Please check your e-mail and enter the temporary signature along with your
                    new signature below to complete the signature reset request.
                </p>
            </div>
            <div class="row">
                <label for="ResetSignature_CurrentSignature">Temporary Signature</label>
                <div class="fr"><%= Html.Password("CurrentSignature", string.Empty, new { @id = "ResetSignature_CurrentSignature", @class = "required", @maxlength = "15" }) %></div>
            </div>
            <div class="row">
                <label for="ResetSignature_NewSignature">New Signature</label>
                <div class="fr"><%= Html.Password("NewSignature", string.Empty, new { @id = "ResetSignature_NewSignature", @class = "required", @maxlength = "15" }) %></div>
            </div>
            <div class="row">
                <label for="ResetSignature_NewSignatureConfirm">Confirm New Signature</label>
                <div class="fr"><%= Html.Password("NewSignatureConfirm", string.Empty, new { @id = "ResetSignature_NewSignatureConfirm", @class = "required", @maxlength = "15" }) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>