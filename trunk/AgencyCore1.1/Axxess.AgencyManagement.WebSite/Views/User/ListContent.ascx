﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserListViewData<User>>" %>
<%  var pageName = "UserList";%>
<%= Html.Filter("SortParams", string.Format("{0}-{1}", Model.SortColumn, Model.SortDirection), new { @class = "sort-parameters" })%>
<% float count = Model.IsUserCanEdit.ToInteger() + Model.IsUserCanDeactivate.ToInteger() + Model.IsUserCanActivate.ToInteger() + Model.IsUserCanDelete.ToInteger(); %>
<%  Func<User, object> actionFunc = delegate(User u) { %>
    <%  var action = ""; %>
    <%  if (Model.IsUserCanEdit) action += string.Concat("<a onclick=\"User.Edit('", u.Id ,"')\" class=\"link\">Edit</a>"); %>
    <%  if (Model.IsUserCanDeactivate || Model.IsUserCanActivate) action += string.Concat("<a onclick=\"User.Toggle('", u.Id, "','", u.DisplayName, "',", Model.IsUserCanActivate.ToString().ToLower(),")\" class=\"link\">", Model.IsUserCanDeactivate ? "Deactivate" : "Activate", "</a>"); %>
    <%  if (Model.IsUserCanDelete) action += string.Concat("<a onclick=\"User.Delete('", u.Id, "','", u.DisplayName, "')\" class=\"link\">Delete</a>"); %>
    <%  return action; %>
<%  };%>
<%= Html.Telerik().Grid(Model.Users).Name(pageName + "_Grid").HtmlAttributes(new { @class = "aggregated" }).Columns(columns => {
		columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Width(20);
		columns.Bound(u => u.DisplayTitle).Title("Title").Sortable(true).Width(15);
		columns.Bound(u => u.EmailAddress).Template(u => string.Format("<a href='mailto:{0}'>{0}</a>", u.EmailAddress)).ClientTemplate("<a href='mailto:<#= EmailAddress #>'><#= EmailAddress #></a>").Title("Email").Width(20).Sortable(true);
		columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(8).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
		columns.Bound(u => u.MobilePhone).Title("Mobile").Sortable(false).Width(8);
		columns.Bound(u => u.EmploymentType).Sortable(true).Width(14);
		columns.Bound(u => u.Profile.Gender).Title("Gender").Width(6).Sortable(true);
		columns.Bound(u => u.Comments).Title("").Sortable(false).Width(3);
		columns.Bound(u => u.Id).Width((int)(6.4 * count)).Sortable(false).Template(actionFunc).Visible(count > 0 ).Title("Action");
	})
   .Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order =>
   {
       var sortName = Model.SortColumn;
       var sortDirection = Model.SortDirection;
       if (sortName == "DisplayName")
       {
           if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
           else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
       }
       else if (sortName == "DisplayTitle")
       {
           if (sortDirection == "ASC") order.Add(o => o.DisplayTitle).Ascending();
           else if (sortDirection == "DESC") order.Add(o => o.DisplayTitle).Descending();
       }
       else if (sortName == "EmailAddress")
       {
           if (sortDirection == "ASC") order.Add(o => o.EmailAddress).Ascending();
           else if (sortDirection == "DESC") order.Add(o => o.EmailAddress).Descending();
       }
       else if (sortName == "EmploymentType")
       {
           if (sortDirection == "ASC") order.Add(o => o.EmploymentType).Ascending();
           else if (sortDirection == "DESC") order.Add(o => o.EmploymentType).Descending();
       }
       else if (sortName == "Gender")
       {
           if (sortDirection == "ASC") order.Add(o => o.Profile.Gender).Ascending();
           else if (sortDirection == "DESC") order.Add(o => o.Profile.Gender).Descending();
       }
   }))
	.Sortable()
	.Scrollable(scrolling => scrolling.Enabled(true)) %>