﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserListViewData<User>>" %>
<span class="wintitle">List Users | <%= Current.AgencyName %></span>
<%  var pageName = "UserList";  %>
<div class ="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
	<ul class="fr buttons ac">
<%  if (!Current.IsAgencyFrozen && Model.IsUserCanAdd) { %>
	    <li><a class="new-user">New User</a></li>
	    <div class="clr"></div>
<%  } %>
<%  if (Model.IsUserCanExport) { %>
		<li><a url="Export/Users" class="export">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a url="User/ListContent" class="grid-refresh">Refresh</a></div>
        <div class="filter">
			<label for="<%= pageName %>_Status">Status</label>
			<select id="<%= pageName %>_Status" name="Status">
                <option value="1" selected="selected">Active</option>
                <option value="2">Inactive</option>
            </select>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer"><% Html.RenderPartial("ListContent", Model, ViewData); %></div>
</div>