﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<span class="wintitle">My Monthly Calendar | <%= Current.AgencyName%></span>
<%  if (Model != null) { %>
    <%  if (Model.UserEvents != null) { %>
        <%  var userEvents = Model.UserEvents; %>
        <%  var startdate = Model.FromDate; %>
        <%  var enddate = Model.ToDate; %>
        <%  var currentdate = startdate.AddDays(-(int)startdate.DayOfWeek); %>
<div class="wrapper main">
    <span class="button top left">
        <a class="navigate" status="<%= string.Format("Navigate to {0:MMMM} {0:yyyy} Calendar", startdate.AddMonths(-1)) %>" month="<%= Model.FromDate.AddDays(-1).Month%>" year="<%=Model.FromDate.AddDays(-1).Year %>">
            <span class="large-font">&#8617;</span>
            <%= string.Format("{0:MMMM} {0:yyyy}", startdate.AddMonths(-1))%>
        </a>
    </span>
    <span class="button top right">
        <a class="navigate" status="<%= string.Format("Navigate to {0:MMMM} {0:yyyy} Calendar", startdate.AddMonths(1)) %>" month="<%= Model.ToDate.AddDays(1).Month%>" year="<%=Model.ToDate.AddDays(1).Year %>">
            <%= string.Format("{0:MMMM} {0:yyyy}", startdate.AddMonths(1))%>
            <span class="large-font">&#8618;</span>
        </a>
    </span>
    <div class="ac">
        <h1><%= Current.UserFullName %></h1>
        <ul class="buttons">
            <li><a class="navigate" status="Refresh Calendar Data" month="<%= Model.FromDate.Month %>" year="<%= Model.FromDate.Year %>">Refresh</a></li>
            <li><a class="print" status="Print Calendar" month="<%= Model.FromDate.Month %>" year="<%= Model.FromDate.Year %>">Print</a></li>
        </ul>
        <div class="cal big">
            <table>
                <thead>
                    <tr>
                        <th colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate)%></th>
                    </tr>
                    <tr>
                        <th>Sunday</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                    </tr>
                </thead>
                <tbody>
        <%  var weekNumber = DateUtilities.Weeks(startdate.Month, startdate.Year) - ((int)startdate.DayOfWeek == 0 ? 1 : 0); %>
        <%  for (int i = 0; i <= weekNumber; i++) { %>
                    <tr>
            <%  string tooltip = ""; %>
            <%  int addedDate = (i) * 7; %>
            <%  for (int j = 0; j <= 6; j++) { %>
                <%  var specificDate = currentdate.AddDays(j + addedDate); %>
                <%  if (specificDate.Date < startdate.Date || specificDate.Date > enddate.Date) { %>
                        <td class="inactive"><%= string.Format("<div class=\"datelabel\">{0}</div>", specificDate.Day) %></td>
                <%  } else { %>
                    <%  var events = userEvents.FindAll(e => e.ScheduleDate.IsValid() && e.ScheduleDate.Date == specificDate.Date); %>
                    <%  var count = events.Count; %>
                    <%  if (count > 1) { %>
                        <%  var allevents = "<br />"; %>
                        <%  events.ForEach(e => { allevents += string.Format("{0}- <em>{1}</em><br />", e.TaskName, e.PatientName.Clean()); }); %>
                        <%  tooltip = specificDate.ToShortDateString() + allevents; %>
                        <td class="multi in-month" date="<%= specificDate.ToShortDateString() %>" tooltip ="<%=tooltip %>" >
                        
                    <%  } else if (count == 1) { %>
                        <%  var evnt = events.First(); %>
                        <%  var missed = (evnt.IsMissedVisit) ? "missed" : string.Empty; %>
                        <%  var status = evnt.Status != null ? evnt.Status : 1000; %>
                        <%  tooltip = specificDate.ToShortDateString() + string.Format("<br />{0}- <em>{1}</em>", evnt.TaskName, evnt.PatientName.Clean()); %>
                        <td class="status<%= status %> scheduled <%= missed %> in-month" date="<%= specificDate.ToShortDateString() %>"  tooltip ="<%=tooltip %>">
                        
                    <%  } else { %>
                        <td class="in-month" date="<%= specificDate.ToShortDateString() %>">
                        <%  tooltip = ""; %>
                    <%  } %>
                            <%= string.Format("<div class=\"datelabel\">{0}</div></td>", specificDate.Day)%>
                <%  } %>
            <%  } %>
                    </tr>
        <%  } %>
                </tbody>
            </table>
        </div>
    </div>
    <div class="clear"></div>
    <div class="ac">
        <fieldset class="legend">
            <ul>
                <li>
                    <div class="scheduled">&#160;</div>
                    Scheduled
                </li>
                <li>
                    <div class="completed">&#160;</div>
                    Completed
                </li>
                <li>
                    <div class="multi">&#160;</div>
                    Multiple
                </li>
            </ul>
        </fieldset>
    </div>
    <div class="clear"></div>
    <div id="UserCalendar_GridContainer"><% Html.RenderPartial("~/Views/User/CalendarGrid.ascx", Model); %></div>
    <%  } %>
<%  } %>
</div>