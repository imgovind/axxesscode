﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<span class="wintitle">Edit User | <%= Model.DisplayName %></span>
<div id="EditUser_Tabs" class="tabs vertical-tabs maintab">
    <%= Html.Hidden("UserId", Model.Id, new { @class = "tabinput" })%>
    <ul class="vertical-tab-list strong">
        <li><a href="#EditUser_Information" name="Information" url="User/Information">Information</a></li>
        <li><a href="User/Permissions" name="Permissions">Permissions</a></li>
        <li><a href="User/Licenses" name="Licenses">Licenses</a></li>
        <li><a href="User/Rates" name="Rates">Pay Rates</a></li>
<%--<%  if (Current.HasRight(Permissions.ManagePatientAccess)) { %>
        <li><a href="#EditUser_PatientAccess">Patient Access</a></li>
<%  } %>--%>
    </ul>
     <div id="EditUser_Information" class="tab-content"><%  Html.RenderPartial("Edit/Information", Model); %></div>
</div>
