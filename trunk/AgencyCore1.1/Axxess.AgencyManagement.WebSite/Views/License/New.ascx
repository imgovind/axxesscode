<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "License", FormMethod.Post, new { @id = "NewLicense_Form" })) { %>
    <fieldset class="newmed">
        <legend>New License<%= Model.IsEmpty() ? " for Non Software User" : string.Empty %></legend>
        <div class="wide-column">
    <%  if (Model.IsEmpty()) { %>
	        <%= Html.Hidden("IsNonUser", true)%>
            <div class="row">
                <label for="NewLicense_UserId" class="fl">Non-Software User</label>
                <div class="fr"><%= Html.NonSoftwareUsers("UserId", "", new { @id = "NewLicense_UserId", @class = "required not-zero" })%></div>
            </div>
    <%  } else { %>
	        <%= Html.Hidden("UserId", Model) %>
    <%  } %>
            <div class="row">
                <label for="NewLicense_LicenseNumber" class="fl">License Number</label>
                <div class="fr"><input type="text" name="LicenseNumber" id="NewLicense_LicenseNumber" class = "" /></div>
            </div>
            <div class="row">
                <label for="NewLicense_LicenseType" class="fl">License Type:</label>
                <div class="fr"><%= Html.LicenseTypes("LicenseType", "", new { @id = "NewLicense_Type", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewLicense_TypeMore">
	                <label for="NewLicense_TypeOther"><em>(Specify)</em></label>
					<%= Html.TextBox("OtherLicenseType", "", new { @id = "NewLicense_TypeOther", @maxlength = "25" }) %>
                </div>
            </div>
            <div class="row">
                <label for="NewLicense_InitDate" class="fl">Issue Date</label>
                <div class="fr"><input type="text" class="date-picker" name="IssueDate" id="NewLicense_IssueDate" /></div>
            </div>
            <div class="row">
                <label for="NewLicense_ExpDate" class="fl">Expire Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ExpireDate" id="NewLicense_ExpDate" /></div>
            </div>
            <div class="row">
                <label for="NewLicense_Attachment" class="fl">File Attachment</label>
                <div class="fr"><input type="file" name="Attachment" id="NewLicense_Attachment" /></div>
            </div>
        </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>