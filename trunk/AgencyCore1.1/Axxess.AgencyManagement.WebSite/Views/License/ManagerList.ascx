﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<License>>" %>
<ul>
    <li>
        <span class="sortable grid-fifth">Employee</span>
        <span class="sortable grid-fifth">License Type</span>
        <span class="sortable grid-fifth">License Number</span>
        <span class="sortable grid-eleventh">Issue Date</span>
        <span class="sortable grid-tenth">Expire Date</span>
        <span class="sortable grid-tenth">Software User?</span>
<%  if (!Current.IsAgencyFrozen) { %>
        <span>Action</span>
<%  } %>
    </li>
</ul>
<%  if (Model != null && Model.Count > 0) { %>
<ol>
    <%  foreach (var license in Model) { %>
    <li>
        <span class="grid-fifth"><%= license.DisplayName %></span>
        <span class="grid-fifth"><%= license.AssetUrl %></span>
        <span class="grid-fifth"><%= license.LicenseNumber %></span>
        <span class="grid-eleventh"><%= license.IssueDate.ToZeroFilled() %></span>
        <span class="grid-tenth"><%= license.ExpireDate.ToZeroFilled()%></span>
        <span class="grid-tenth"><%= license.IsUser %></span>
        <%  if (!Current.IsAgencyFrozen) { %>
        <span>
            <a class="link" onclick="LicenseManager.Edit('<%= license.Id %>', '<%= license.UserId %>')">Edit</a>
            <a class="link" onclick="LicenseManager.Delete('<%= license.Id %>', '<%= license.UserId %>')">Delete</a>
        </span>
        <%  } %>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<h4>No Licenses Found</h4>
<%  } %>