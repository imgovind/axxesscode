<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<License>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "License", FormMethod.Post, new { @id = "EditLicense_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditLicense_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditLicense_UserId" })%>
    <%= Html.Hidden("IsNonUser", Model.IsNonUser, new { @id = "EditLicense_IsNonUser" })%>
    <fieldset>
        <legend>Edit License</legend>
        <div class="wide-column">
            <div class="row">
                <label for="EditLicense_Type" class="fl">License Type</label>
                <div class="fr"><%= Html.LicenseTypes("LicenseType", Model.LicenseType, new { @id = "EditLicense_Type", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="EditLicense_TypeMore">
                    <label for="EditLicense_TypeOther"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", Enum.IsDefined(typeof(LicenseTypes), Model.LicenseType) ? string.Empty : Model.LicenseType, new { @id = "EditLicense_TypeOther", @maxlength = "25" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditLicense_LicenseNumber" class="fl">License Number</label>
                <div class="fr"><input type="text" name="LicenseNumber" id="EditLicense_LicenseNumber" value="<%= Model.LicenseNumber %>" /></div>
            </div>
            <div class="row">
                <label for="EditLicense_InitDate" class="fl">Issue Date</label>
                <div class="fr"><input type="text" class="date-picker" name="IssueDate" id="EditLicense_IssueDate" value="<%= Model.IssueDate.ToZeroFilled() %>" /></div>
            </div>
            <div class="row">
                <label for="EditLicense_ExpDate" class="fl">Expire Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ExpireDate" id="EditLicense_ExpireDate" value="<%= Model.ExpireDate.ToZeroFilled() %>" /></div>
            </div>
        </div>   
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>