﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<License>>" %>
<div class="wrapper main blue">
    <ul class="buttons">
<%  if (!Current.IsAgencyFrozen) { %>
        <li class="fl"><a onclick="LicenseManager.New()">Add Non-Software User License</a></li>
<%  } %>
        <li class="fr"><a onclick="LicenseManager.Refresh()">Refresh</a></li>
    </ul>
    <div class="clr"></div>
    <div id="LicenseManager_List" class="standard-chart acore-grid"><%  Html.RenderPartial("ManagerList", Model); %></div>
</div>