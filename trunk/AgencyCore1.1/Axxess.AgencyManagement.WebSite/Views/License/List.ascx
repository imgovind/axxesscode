﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<ul>
    <li class="ac"><h3>Current Licenses</h3></li>
    <li>
        <span class="grid-quarter">License Type</span>
        <span class="grid-fifth">License Number</span>
        <span class="grid-fifth">Issue Date</span>
        <span class="grid-fifth">Expire Date</span>
        <span>Action</span>
    </li>
</ul>
<%  if (Model.IsNotNullOrEmpty()) { %>
<ol>
    <%  foreach(var license in Model) { %>
    <li>
        <span class="grid-quarter"><%=  license.AssetUrl%></span>
        <span class="grid-fifth"><%= license.LicenseNumber %></span>
        <span class="grid-fifth"><%= license.IssueDate.ToZeroFilled() %></span>
        <span class="grid-fifth"><%= license.ExpireDate.ToZeroFilled() %></span>
        <span><a class="link edit-license" guid="<%= license.Id %>">Edit</a><a class="link delete-license" guid="<%= license.Id %>">Delete</a></span>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<ol>
	<li class="no-hover"><h4>No Licenses Found</h4></li>
</ol>
<%  } %>