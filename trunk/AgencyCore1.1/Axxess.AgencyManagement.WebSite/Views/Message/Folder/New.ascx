﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("FolderCreate", "Message", FormMethod.Post, new { @id = "MessageFolder_Form" })) { %>
    <fieldset>
        <legend>New Folder</legend>
        <div class="wide-column">
            <div class="row">
                <label for="MessageFolder_Name" class="fl">Name</label>
                <div class="fr"><%= Html.TextBox("newFolderName", string.Empty, new { @id = "New_MessageFolder_Name", @class = "required", @maxlength = "200" }) %></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
<%  } %>
</div>

