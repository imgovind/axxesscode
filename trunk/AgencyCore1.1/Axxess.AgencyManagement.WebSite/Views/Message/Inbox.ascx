﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Clean() %>&#8217;s Messages | <%= Current.AgencyName.Clean() %></span>
<div id="Div1" class="wrapper layout">
    <div class="ui-layout-west"><% Html.RenderPartial("List"); %></div>
    <div id="message-view" class="ui-layout-center loading"></div>
</div>