﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Message | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout messaging-container">
<%  using (Html.BeginForm("Create", "Message", FormMethod.Post, new { @id = "NewMessage_Form" })) { %>
    <div class="ui-layout-west"><%= Html.Recipients() %></div>
    <div class="compose message-content ui-layout-center">
        <div class="compose-header">
            <div class="button fr"><a class="close">Cancel</a></div>
            <span>New Message</span>
        </div>
        <div class="compose message-header-container">
            <div class="button send abs">
                <a class="save close">
                    <span class="img send"></span>
                    <br />Send
                </a>
            </div>
            <div class="message-header-row">
                <label for="NewMessage_Recipents" class="strong">To</label>
                <div class="compose-input to">
                    <input type="text" id="NewMessage_Recipents" name="newMessageRecipents" />
                </div>
            </div>
            <div class="message-header-row">
                <label for="NewMessage_Subject" class="strong">Subject</label>
                <div class="compose-input">
                    <input type="text" id="NewMessage_Subject" name="Subject" maxlength="75" class="required" />
                </div>
            </div>
    <%  if (!Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) && !Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer)) { %>
            <div class="message-header-row">
                <label for="NewMessage_PatientId" class="strong">Regarding</label>
                <div class="compose-input">
                    <%= Html.PatientsFilteredByService((int)Current.AcessibleServices, "PatientId", "", new { @id = "NewMessage_PatientId" })%>
                </div>
            </div>
    <%  } %>
            <div class="message-header-row">
                <label for="NewMessage_Attachment" class="strong">Attachments</label>
                <div class="compose-input">
                    <input type="file" id="NewMessage_Attachment" name="Attachment1" />
                </div>
            </div>
        </div>
        <div class="new-message-row" id="NewMessage_BodyWrapper">
            <textarea id="NewMessage_Body" name="Body"></textarea>
        </div>
    </div>
<%  } %>
</div>