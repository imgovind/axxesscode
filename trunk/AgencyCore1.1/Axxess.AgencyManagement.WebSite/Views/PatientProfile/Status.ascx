﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateStatus", "PatientProfile", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "changePatientStatusForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%  if (Model.IsDischarged) { %>
    <fieldset>
        <legend>Change Status</legend>
        <div class="wide-column">
            <div class="row">
                <label for="ChangeStatus_Status" class="fl">Status</label>
                <div class="fr"><%= Html.StatusPatients("Status", PatientStatus.Discharged, new { @id = "ChangeStatus_Status", @class = "required nonzero" })%></div>
            </div>
            <div class="row ac">
                <p>
                    This patient will be added to your list of active patients. If the patient to be readmitted you have to exit and readmit with a new start of care but reactivating will take the last start of care date.<br /><br />
                    <strong>Are you sure you want to activate this patient?</strong>
                </p>
            </div>
        </div>
    </fieldset>
    <%  } else { %>
    <fieldset>
        <legend>Change Status</legend>
        <div class="wide-column">
            <div class="row">
                <label for="ChangeStatus_Status" class="fl">Status</label>
                <div class="fr"><%= Html.StatusPatients("Status", PatientStatus.Active, new { @id = "ChangeStatus_Status", @class = "required nonzero" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset id="dischargeContainer" class="discharge-patient-container">
        <legend>Discharge Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="ChangeStatus_DischargeDate" class="fl">Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="DateOfDischarge" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="ChangeStatus_DischargeDate" /></div>
            </div>
            <div class="row">
                <label for="ChangeStatus_DischargeReason" class="fl">Discharge Reason</label>
                <div class="fr"><%= Html.DischargeReasons("ReasonId", new { @id = "ChangeStatus_Reason", @class = "required nonzero" })%></div>
            </div>
            <div class="row">
                <label for="Comment">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", "") %></div>
            </div>
            <div class="row ac">
                <p>
                    This patient will be added to your list of discharged patients. To provide services to this patient in the future, you will have to re-admit the patient.<br /><br />
                    <strong>Are you sure you want to discharge this patient?</strong>
                </p>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  Html.RenderPartial("NonAdmission"); %>
    <ul class="buttons ac">
       <li><a id="ChangeStatus_SaveButton" class="save close">Yes</a></li>
       <li><a id="ChangeStatus_CancelButton" class="close">No</a></li>
    </ul>
<%  } %>
</div>