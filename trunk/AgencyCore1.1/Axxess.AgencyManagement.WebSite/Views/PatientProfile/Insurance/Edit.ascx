﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientInsurance>" %>
<% var IsPayorExist = Model.Id > 0;%>
<% var action = (IsPayorExist ? "Edit" : "") + "PatientInsurance_";%>
<div class="wrapper main">
<%  using (Html.BeginForm((IsPayorExist ? "Update" : "Add") + "Insurance", "PatientProfile", FormMethod.Post, new { area = Model.Service.ToArea(), @id = action + "Form", @class = "mainform" }))
    { %>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = action + "PatientId" })%>
<%= Html.Hidden("AdmissionId", Model.AdmissionId, new { @id = action + "AdmissionId" })%>
<%= Html.Hidden("Id", Model.Id, new { @id = action + "Id" })%>
<fieldset>
<legend>Insurance</legend>
     <div class="column">
        <div class="sub row">
            <label for="<%=action %>InsuranceId" class="fl">Insurance</label>
            <div class="fr">
            <%if (IsPayorExist)
              { %>
              <%= Html.Hidden("InsuranceId", Model.InsuranceId, new { @id = action + "InsuranceId" })%><%=Model.Name%>
            <%}
              else
              { %>
            <%= Html.InsurancesByBranch("InsuranceId", Model.InsuranceId.ToString(), Guid.Empty, (int)Model.Service, true, new { @id = action + "InsuranceId", @class = "insurance required not-zero" })%>
            <%} %>
            </div>
        </div>
        <div class="sub row">
            <label for="<%=action %>Type" class="fl">Type</label>
            <div class="fr"><%= Html.InsuranceType("Type", Model.Type.ToString(), new { @id = action + "Type", @class = "required not-zero" })%></div>
        </div>
        <div class="sub row">
            <label for="<%=action %>InsuranceId" class="fl">Parent Insurance</label>
            <div class="fr"><select name="ParentInsuranceId" id="<%=action %>ParentInsuranceId"></select></div>
        </div>
        <div class="sub row">
            <label for="<%= action %>HealthPlanId" class="fl">Health Plan Id</label>
            <div class="fr"><%= Html.TextBox("HealthPlanId", Model.HealthPlanId, new { @id = action + "HealthPlanId", @class = "healthplanid required short more" })%></div>
        </div>
        <div class="sub row">
            <label for="<%= action %>GroupName" class="fl">Group Name</label>
            <div class="fr"><%= Html.TextBox("GroupName", Model.GroupName, new { @id = action + "GroupName", @class = "short more" })%></div>
        </div>
        <div class="sub row">
            <label for="<%= action %>GroupId" class="fl">Group Id</label>
            <div class="fr"><%= Html.TextBox("GroupId", Model.GroupId, new { @id = action + "GroupId", @class = "short more" })%></div>
        </div>
     </div>
     <div class="column">
        <div class="sub row">
            <label for="<%= action %>RelationId" class="fl">Relationship to Patient</label>
            <div class="fr"><%= Html.InsuranceRelationships("RelationId", Model.RelationId.ToString(), new { @id = action + "RelationId", @class = "short" })%></div>
        </div>
       <%--  <div class="sub row">
            <label for="<%=action %>IsApplicableRange" class="fl">Check if it only apply for specific date range </label>
            <div class="fl"><%= Html.CheckBox("IsApplicableRange", Model.ActualStartDate.IsValid(), new { @id = action + "IsApplicableRange" })%></div>
        </div>
         <div class="sub row">
           <label for="<%=action %>ActualStartDate" class="fl">Start Date </label>
            <div class="fr">
                <%= Html.TextBox("ActualStartDate", Model.ActualStartDate.IsValid() ? Model.ActualStartDate.ToString("MM/dd/yyyy") : string.Empty, new { @id = action + "ActualStartDate", @class = "date-picker required" })%>
           </div>
           <div class="clr" /><em class="fl">Note: The start date is relative to  care period where the payor assigned.</em>
        </div>
         <div class="sub row">
            <label for="<%=action %>IsEndDateSet" class="fl">Check there is specific end date</label>
            <div class="fl"><%= Html.CheckBox("IsEndDateSet", Model.ActualStartDate.IsValid()&& Model.EndDate.IsValid(), new { @id = action + "IsEndDateSet" })%></div>
        </div>
         <div class="sub row">
            <label for="<%=action %>EndDate" class="fl">End Date</label>
            <div class="fr"><%= Html.TextBox("ActualEndDate",Model.ActualEndDate.IsValid()? Model.ActualEndDate.ToString("MM/dd/yyyy"):string.Empty, new { @id = action + "ActualEndDate", @class = "date-picker" })%></div>
        </div>--%>
       </div>
</fieldset>
<fieldset>
<legend>Authorization</legend>
 <div class="sub row">
            <label for="<%=action %>IsApplicableRange" class="fl">Check if this payor only apply for specific date range </label>
            <div class="fl"><%= Html.CheckBox("IsApplicableRange", Model.ActualStartDate.IsValid(), new { @id = action + "IsApplicableRange" })%></div>
        </div>
         <div class="sub row">
           <label for="<%=action %>ActualStartDate" class="fl">Start Date </label>
            <div class="fr">
                <%= Html.TextBox("ActualStartDate", Model.ActualStartDate.IsValid() ? Model.ActualStartDate.ToString("MM/dd/yyyy") : string.Empty, new { @id = action + "ActualStartDate", @class = "date-picker required" })%>
           </div>
           <div class="clr" /><em class="fl">Note: The start date is relative to  care period where the payor assigned.</em>
        </div>
         <div class="sub row">
            <label for="<%=action %>IsEndDateSet" class="fl">Check there is specific end date</label>
            <div class="fl"><%= Html.CheckBox("IsEndDateSet", Model.ActualStartDate.IsValid()&& Model.EndDate.IsValid(), new { @id = action + "IsEndDateSet" })%></div>
        </div>
         <div class="sub row">
            <label for="<%=action %>EndDate" class="fl">End Date</label>
            <div class="fr"><%= Html.TextBox("ActualEndDate",Model.ActualEndDate.IsValid()? Model.ActualEndDate.ToString("MM/dd/yyyy"):string.Empty, new { @id = action + "ActualEndDate", @class = "date-picker" })%></div>
        </div>
</fieldset>
 <ul class="buttons ac">
        <li><a class="save close">Save &#38; Exit</a></li>
        <li><a class="close">Exit</a></li>
  </ul>
  <%} %>
  </div>