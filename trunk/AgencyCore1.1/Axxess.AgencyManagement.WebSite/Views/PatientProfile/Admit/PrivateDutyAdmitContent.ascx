﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<div class="wrapper main">
<%  if (PatientStatusFactory.PossibleAdmit().Contains(Model.Status)) { %>
    <%  using (Html.BeginForm("Admit", "PatientProfile", FormMethod.Post, new { @id = "newPdPatientProfileForm", @class = "tabform" })) { %>
    <fieldset>
	    <legend>Private Duty</legend>
		<input type="hidden" name="profile.Index" value="2" />
		<input type="hidden" name="profile[2].ServiceType" value="2" />
		<%= Html.Hidden("profile[2].Id", Model.Id, new { @id = "Admit_Patient_PdProfileId" }) %>
		<div class="column">
			<div class="row">
				<label for="Admit_Patient_PdStartOfCareDate" class="fl">
				    <span class="green">(M0030)</span>
				    Start of Care Date
				</label>
				<div class="fr"><input type="text" class="date-picker required" name="profile[2].StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Admit_Patient_PdStartOfCareDate" /></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdEpisodeStartDate" class="fl">Care Period Start Date</label>
				<div class="fr"><input type="text" class="date-picker required care-period-start" name="profile[2].EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Patient_PdEpisodeStartDate" /></div>
			</div>
            <div class="row">
                <label for="EditPrivateDutyCarePeriod_PeriodLength" class="fl">Care Period Length</label>
                <div class="fr">
                    <select id="EditPrivateDutyCarePeriod_PeriodLength" class="care-period-length">
                        <option value="14">14 Days</option>
                        <option value="30">30 Days</option>
                        <option value="60">60 Days</option>
                        <option value="90">90 Days</option>
                        <option value="120">120 Days</option>
                        <option value="specify" selected="selected">Specify End Date</option>
                    </select>
                </div>
            </div>
			<div class="row">
				<label for="Admit_Patient_PdEpisodeEndDate" class="fl">Care Period End Date</label>
				<div class="fr"><input type="text" class="date-picker required care-period-end" name="profile[2].EpisodeEndDate" id="Admit_Patient_PdEpisodeEndDate" /></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdCaseManager" class="fl">Case Manager</label>
				<div class="fr"><%= Html.CaseManagers("profile[2].CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "Admit_Patient_PdCaseManager", @class = "user-selector required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdAssign" class="fl">Assign to Clinician</label>
				<div class="fr"><%= Html.Clinicians("profile[2].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "Admit_Patient_PdAssign", @class = "required not-zero user-selector" })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="Admit_Patient_PdLocationId" class="fl">Agency Branch</label>
				<div class="fr"><%= Html.BranchOnlyList("profile[2].AgencyLocationId", !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty,(int)AgencyServices.PrivateDuty, new { @id = "Admit_Patient_PdLocationId", @class = "branch-location required not-zero", @service = (int)AgencyServices.PrivateDuty })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdPatientReferralDate" class="fl">Referral Date</label>
				<div class="fr"><input type="text" class="date-picker" name="profile[2].ReferralDate" value="<%= (Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : "" %>" id="Admit_Patient_PdPatientReferralDate" /></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdAdmissionSource" class="fl">Admission Source</label>
				<div class="fr"><%= Html.AdmissionSources("profile[2].AdmissionSource", Model.AdmissionSource, new { @id = "Admit_Patient_PdAdmissionSource", @class = "admission-source required not-zero required" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdOtherReferralSource" class="fl">Other Referral Source</label>
				<div class="fr"><%= Html.TextBox("profile[2].OtherReferralSource", Model.OtherReferralSource, new { @id = "Admit_Patient_PdOtherReferralSource", @maxlength = "30" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_PdInternalReferral" class="fl">Internal Referral Source</label>
				<div class="fr"><%= Html.Users("profile[2].InternalReferral", Model.InternalReferral.ToString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.PrivateDuty, new { @id = "Admit_PdPatient_InternalReferral", @class = "user-selector" })%></div>
			</div>
		</div>
		<div class="wide-column">
			<div class="row ac">
				<label>Insurance/Payor</label>
			</div>
			<div class="inline-row three-wide">
				<div>
					<div class="row">
						<label for="Admit_Patient_PdPrimaryInsurance" class="fl">Primary</label>
						<div class="fr"><%= Html.InsurancesByBranch("profile[2].PrimaryInsurance", Model.PrimaryInsurance.ToSafeString(), Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, true, new { @id = "Admit_Patient_PdPrimaryInsurance", @class = "insurance required not-zero" })%></div>
					</div>
					<div id="Admit_Patient_PdPrimaryInsuranceContent" class="<%= Model.PrimaryInsurance >= 1000 ? "" : "hidden" %>">
						<% Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Patient", InsuranceType = "Primary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = Model.PrimaryHealthPlanId, GroupName = Model.PrimaryGroupName, GroupId = Model.PrimaryGroupId, Relationship = Model.PrimaryRelationship }); %>
					</div>
				</div>
				<div>
					<div class="row">
						<label for="Admit_Patient_PdSecondaryInsurance" class="fl">Secondary</label>
						<div class="fr"><%= Html.InsurancesByBranch("profile[2].SecondaryInsurance", Model.SecondaryInsurance.ToSafeString(), Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, true, new { @id = "Admit_Patient_PdSecondaryInsurance", @class = "insurance" })%></div>
					</div>
					<div id="Admit_Patient_PdSecondaryInsuranceContent" class="<%= Model.SecondaryInsurance >= 1000 ? "" : "hidden" %>">
						<% Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Patient", InsuranceType = "Secondary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = Model.SecondaryHealthPlanId, GroupName = Model.SecondaryGroupName, GroupId = Model.SecondaryGroupId, Relationship = Model.SecondaryRelationship }); %>
					</div>
				</div>
				<div>
					<div class="row">
						<label for="Admit_Patient_PdTertiaryInsurance" class="fl">Tertiary</label>
						<div class="fr"><%= Html.InsurancesByBranch("profile[2].TertiaryInsurance", Model.TertiaryInsurance.ToSafeString(), Model.AgencyLocationId, (int)AgencyServices.PrivateDuty, true, new { @id = "Admit_Patient_PdTertiaryInsurance", @class = "insurance" })%></div>
					</div>
					<div id="Admit_Patient_PdTertiaryInsuranceContent" class="<%= Model.TertiaryInsurance >= 1000 ? "" : "hidden" %>">
						<% Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Patient", InsuranceType = "Tertiary", IdPrefix = "Pd", NamePrefix = "profile[2].", HealthPlanId = Model.TertiaryHealthPlanId, GroupName = Model.TertiaryGroupName, GroupId = Model.TertiaryGroupId, Relationship = Model.TertiaryRelationship }); %>
					</div>
				</div>
			</div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="back">Back</a></li>
		<li><a class="save close">Admit &#38; Close</a></li>
		<li><a class="close">Close</a></li>
	</ul>
	<%  } %>
<%  } else { %>
    <%= Html.ErrorMessage("Unable to Admit", "The patient could not be admittied for this service, the patient is " + (Model.IsDeprecated ? "deleted" : ((PatientStatus)Model.Status).ToString()) + ".")%>
	<ul class="buttons ac">
		<li><a class="back">Back</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>