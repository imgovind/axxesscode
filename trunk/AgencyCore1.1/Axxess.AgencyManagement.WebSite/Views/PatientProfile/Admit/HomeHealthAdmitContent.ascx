﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Profile>" %>
<div class="wrapper main">
<%  if (PatientStatusFactory.PossibleAdmit().Contains(Model.Status)) { %>
    <%  using (Html.BeginForm("Admit", "PatientProfile", FormMethod.Post, new { @id = "newHHPatientProfileForm", @class = "tabform" })) { %>
    <fieldset>
        <legend>Home Health</legend>
		<input type="hidden" name="profile.Index" value="1" />
		<input type="hidden" name="profile[1].ServiceType" value="1" />
		<%= Html.Hidden("profile[1].Id", Model.Id, new { @id = "Admit_Patient_HHProfileId" })%>
		<div class="column">
		    <div class="row">
		        <label for="Admit_Patient_StartOfCareDate" class="fl">
		            <span class="green">(M0030)</span>
		            Start of Care
		        </label>
				<div class="fr"><input type="text" class="date-picker required" name="profile[1].StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Admit_Patient_StartOfCareDate" /></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_EpisodeStartDate" class="fl">Episode Start Date</label>
				<div class="fr"><input type="text" class="date-picker required" name="profile[1].EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Patient_EpisodeStartDate" /></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_CaseManager" class="fl">Case Manager</label>
				<div class="fr"><%= Html.CaseManagers("profile[1].CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "Admit_Patient_CaseManager", @class = "user-selector required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_Assign" class="fl">Assign to Clinician</label>
				<div class="fr"><%= Html.Clinicians("profile[1].UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : string.Empty, new { @id = "Admit_Patient_Assign", @class = "required not-zero user-selector" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_LocationId" class="fl">Agency Branch</label>
				<div class="fr"><%= Html.BranchOnlyList("profile[1].AgencyLocationId", Model != null && !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty, (int)AgencyServices.HomeHealth, new { @id = "Admit_Patient_LocationId", @class = "branch-location required not-zero", @service = (int)AgencyServices.HomeHealth })%></div>
			</div>
		</div>
		<div class="column">
			<div class="row">
				<label for="Admit_Patient_PatientReferralDate" class="fl">Referral Date</label>
				<div class="fr"><input type="text" class="date-picker" name="profile[1].ReferralDate" value="<%= (Model != null && Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : string.Empty %>" id="Admit_Patient_PatientReferralDate" /></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_AdmissionSource" class="fl">Admission Source</label>
				<div class="fr"><%= Html.AdmissionSources("profile[1].AdmissionSource", Model.AdmissionSource, new { @id = "Admit_Patient_AdmissionSource", @class = "admission-source required not-zero" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_OtherReferralSource" class="fl">Other Referral Source</label>
				<div class="fr"><%= Html.TextBox("profile[1].OtherReferralSource", Model.OtherReferralSource, new { @id = "Admit_Patient_OtherReferralSource", @maxlength = "30" })%></div>
			</div>
			<div class="row">
				<label for="Admit_Patient_InternalReferral" class="fl">Internal Referral Source</label>
				<div class="fr"><%= Html.Users("profile[1].InternalReferral", Model.InternalReferral.ToSafeString(), "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "Admit_Patient_InternalReferral", @class = "user-selector" })%></div>
			</div>
			<div class="row">
			    <ul class="checkgroup one-wide">
			        <%= Html.CheckgroupOption("profile[1].IsFaceToFaceEncounterCreated", "true", false, "Create a face to face encounter<br /><em>This is applicable for SOC date after 1/1/2011</em>") %>
			    </ul>
			</div>
		</div>
		<div class="column">
		    <div class="row">
		        <label for="Admit_Patient_PrimaryInsurance" class="fl">Primary Insurance</label>
		        <div class="fr"><%= Html.InsurancesByBranch("profile[1].PrimaryInsurance", Model.PrimaryInsurance.ToSafeString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, true, new { @id = "Admit_Patient_PrimaryInsurance", @class = "insurance required not-zero" })%></div>
		    </div>
		    <div id="Admit_Patient_PrimaryInsuranceContent" class="<%= Model.PrimaryInsurance >= 1000 ? "" : "hidden" %> row">
		        <% Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Patient", InsuranceType = "Primary", NamePrefix = "profile[1].", HealthPlanId = Model.PrimaryHealthPlanId, GroupName = Model.PrimaryGroupName, GroupId = Model.PrimaryGroupId, Relationship = Model.PrimaryRelationship }); %>
		    </div>
		</div>
		<div class="column">
		    <div class="row">
		        <label for="Admit_Patient_SecondaryInsurance" class="fl">Secondary Insurance</label>
		        <div class="fr"><%= Html.InsurancesByBranch("profile[1].SecondaryInsurance", Model.SecondaryInsurance.ToSafeString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, true, new { @id = "Admit_Patient_SecondaryInsurance", @class = "insurance" })%></div>
		    </div>
		    <div id="Admit_Patient_SecondaryInsuranceContent" class="<%= Model.SecondaryInsurance >= 1000 ? "" : "hidden" %> row">
		        <% Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Patient", InsuranceType = "Secondary", NamePrefix = "profile[1].", HealthPlanId = Model.SecondaryHealthPlanId, GroupName = Model.SecondaryGroupName, GroupId = Model.SecondaryGroupId, Relationship = Model.SecondaryRelationship }); %>
		    </div>
		</div>
		<div class="column">
		    <div class="row">
		        <label for="Admit_Patient_TertiaryInsurance" class="fl">Tertiary Insurance</label>
		        <div class="fr"><%= Html.InsurancesByBranch("profile[1].TertiaryInsurance", Model.TertiaryInsurance.ToSafeString(), Model.AgencyLocationId, (int)AgencyServices.HomeHealth, true, new { @id = "Admit_Patient_TertiaryInsurance", @class = "insurance" })%></div>
		    </div>
		    <div id="Admit_Patient_TertiaryInsuranceContent" class="<%= Model.TertiaryInsurance >= 1000 ? "" : "hidden" %> row">
		        <% Html.RenderPartial("Patient/InsuranceInfoContent", new PatientInsuranceInfoViewData { ActionType = "Admit_Patient", InsuranceType = "Tertiary", NamePrefix = "profile[1].", HealthPlanId = Model.TertiaryHealthPlanId, GroupName = Model.TertiaryGroupName, GroupId = Model.TertiaryGroupId, Relationship = Model.TertiaryRelationship }); %>
		    </div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="back">Back</a></li>
		<li><a class="save next">Admit &#38; Next</a></li>
		<li><a class="save close">Admit &#38; Close</a></li>
		<li><a class="close">Close</a></li>
	</ul>
    <%  } %>
<%  } else { %>
    <fieldset>
		<div class="wide-column">
		    <div class="row">
		        <p>
		            The patient could not be admittied for this service, the patient is
		            <%= string.Format("{0}.", Model.IsDeprecated ? "deleted" : ((PatientStatus)Model.Status).ToString()) %>
		        </p>
		    </div>
		</div>
	</fieldset>
	<ul class="buttons ac">
		<li><a class="back">Back</a></li>
		<li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>