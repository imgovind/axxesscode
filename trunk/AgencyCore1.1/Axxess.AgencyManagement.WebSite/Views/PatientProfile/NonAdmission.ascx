﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<fieldset id="nonAdmissionContainer" class="hidden nonadmission-patient-container">
    <legend>Non-Admit Information</legend>
    <div class="wide-column">
        <div class="row">
            <label for="ChangeStatus_NonAdmitDate" class="fl">Non-Admit Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="NonAdmitDate" value="<%= DateTime.Today.ToShortDateString() %>" id="ChangeStatus_NonAdmitDate" /></div>
        </div>
        <div class="row">
            <label class="fl">Reason</label>
            <div class="fr">
                <select id="ChangeStatus_Reason" name="Reason" class="fr required">
                    <option value="">- Select Reason -</option>
                    <option value="Inappropriate For Home Care">Inappropriate for Home Care</option>
                    <option value="Referral Refused Service">Referral Refused Service</option>
                    <option value="Out of Service Area">Out of Service Area</option>
                    <option value="On Service with another agency">On Service with Another Agency</option>
                    <option value="Not a Provider">Not a Provider</option>
                    <option value="Not Homebound">Not Homebound</option>
                    <option value="Redirected to alternate care facility">Redirected to Alternate Care Facility</option>
                    <option value="Other">Other (Specify in Comments)</option>
                </select>
            </div>
        </div>
        <div class="row">
            <label for="Comments">Comments</label>
            <div class="ac"><%= Html.TextArea("Comments", "") %></div>
        </div>
        <div class="row ac">
            <strong>Are you sure you want to set this patient to Non-Admit?</strong>
        </div>
    </div>
</fieldset>