﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientDataViewData>" %>
<span class="wintitle">List Patients | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  var pageName = "PatientList"; %>
    <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="fr buttons ac">
    <%  if (!Current.IsAgencyFrozen && Model.NewPermissions != AgencyServices.None) { %>
        <li><a onclick="Patient.Demographic.New()" service="<%= (int)Model.NewPermissions %>" class="servicepermission">New Patient</a></li>
        <div class="clr"></div>
    <%  } %>
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a area="<%=area %>" url="/Export/Patients" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li>
        <div class="clr"></div>
    <%  } %>
        <li><a area="<%=area %>" url="/PatientProfile/ListContent" class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter ignore-grid-binding"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @id = pageName + "_ServiceId" })%></div> 
        <div class="filter optional">
            <label for="<%= pageName%>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.AvailableService, new { @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_Status">Status</label>
            <select id="<%= pageName %>_Status" name="Status" >
                <option value="0">All</option>
                <option value="1" selected>Active</option>
                <option value="2">Discharged</option>
                <option value="3">Pending</option>
                <option value="4">Non-Admit</option>
            </select>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div class="clr"></div>
    <div id="<%= pageName %>GridContainer"><% Html.RenderPartial("ListContent", Model); %></div>
</div>
<%  } %>