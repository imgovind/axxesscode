﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientScheduleEventViewData>" %>
<%  if (Model != null && Model.Patient != null) { %>
<div class="top" id="patientTop"><% Html.RenderPartial("~/Views/PatientProfile/Charts/Info.ascx", Model.Patient); %></div>
<div class="bottom"><% Html.RenderPartial("~/Views/PatientProfile/Charts/Activities.ascx", Model); %></div>
<%  } else { %>
<script type="text/javascript">
    U.MessageError("No Patient Data Found", "Unable to obtain data on this patient.  Please try again later.");
</script>
<%  } %>