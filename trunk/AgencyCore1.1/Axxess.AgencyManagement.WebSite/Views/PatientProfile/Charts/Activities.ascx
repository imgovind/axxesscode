﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientScheduleEventViewData>" %>
<%  var area = Model.Service.ToArea(); %>
<%  var val = Model != null && Model.Patient != null ? Model.Patient.Id : Guid.Empty; %>
<%  var isActionVisible = Model.DeleteDocuments > 0 || Model.IsUserCanEditDetail || Model.IsUserCanReassign||Model.IsUserCanReopen||Model.IsUserCanRestore; %>
<%  var url = new Func<GridTask, string>(t =>
    {
        var actions = new List<string>();
        if (t.IsUserCanEditDetail) actions.Add("<li><a class=\"link edit-details\"><span class='img icon22 edit-schedule'/>Details</a></li>");
        if (t.IsUserCanReopen) actions.Add("<li><a class=\"link reopen-task\"><span class='img icon22 reopen-schedule'/>Reopen</a></li>");
        if (t.IsUserCanReassign) actions.Add("<li><a class=\"link reassign-task\"><span class='img icon22 reassign-schedule'/>Reassign</a></li>");
        if (t.IsUserCanRestore) actions.Add("<li><a class=\"link restore-task\"><span class='img icon22 reopen-schedule'/>Restore</a></li>");
        if (t.IsUserCanDelete) actions.Add("<li><a class=\"link delete-task\"><span class='img icon22 delete'/>Delete</a></li>");
        if (actions.IsNotNullOrEmpty())
        {
            return string.Format("<div class=\"grid-action\"><span class='img icon22 pointer right'/><ul class=\"action-menu\">{0}</ul></div>", actions.ToArray().Join(" "));
        }
        else
        {
            return string.Empty;
        }
    }); %>
<div class="abs above ac">
    <div class="fr"><a class="grid-resize img grid-expand"></a></div>
    <input type="hidden" class="activityFilterInput" name="PatientId" value="<%= Model.Patient.Id %>" />
    <label for="PatientActivity_Discipline">Show</label>
    <%= Html.PatientSchedulesDisciplines("Discipline", Model.DisciplineFilterType, new { @id = "PatientActivity_Discipline", @class = "activityFilterInput" })%>
    <label for="PatientActivity_Date">Date</label>
    <%= Html.PatientSchedulesDateRangeFilter((int)Model.Service, "DateRangeId", Model.DateFilterType, new { @id = "PatientActivity_Date", @class = "activityFilterInput" })%>
    <span class="activity-date-range">
        <span id="date-range-text"><%= Model.Range %></span>
        <span class="custom-date-range <%= Model.DateFilterType.IsEqual("DateRange") ? string.Empty: "hidden" %>">
            <label for="PatientActivity_StartDate">From</label>
            <input type="text" class="date-picker" name="RangeStartDate" value="<%= DateTime.Now.ToShortDateString() %>" id="PatientActivity_StartDate" />
            <label for="PatientActivity_EndDate">&#8211;</label>
            <input type="text" class="date-picker" name="RangeEndDate" value="<%= DateTime.Now.AddDays(60).ToShortDateString() %>" id="PatientActivity_EndDate" />
            <div class="button"><a class="activity-search">Search</a></div>
        </span>
    </span>
</div>
<fieldset class="note-legend" style="right:<%= isActionVisible ? 6.9 : 0 %>em">
    <ul>
        <% if ((Model.Service == AgencyServices.HomeHealth) && Model.IsOASISProfileExist){ %> <li><span class="img icon16 money"></span> OASIS Profile</li><%} %>
        <%if (Model.IsStickyNote){ %> 
            <li><span class="img icon16 note yellow"></span> Visit Comments</li>
            <li><span class="img icon16 note blue"></span> Episode Comments</li>
            <li><span class="img icon16 note red"></span> Missed/Returned</li>
         <%} %>
         <%if(Model.IsUserCanPrint){ %>
        <li><span class="img icon16 print"></span> Print Document</li>
        <%} %>
        <li><span class="img icon16 paperclip"></span> Attachments</li>
    </ul>
</fieldset>
<%  Html.Telerik().Grid(Model.ScheduleEvents).Name("PatientActivityGrid").HtmlAttributes(new { @class = "patient-activity-grid" }).Columns(columns => {
        columns.Bound(s => s.IsUserCanEdit).Template(s => s.IsUserCanEdit ? string.Format("<a class=\"{0} {1}\">{2}</a>", s.IsMissedVisit ? "missedvisit" : "edit", s.IsComplete ? "complete" : "", s.TaskName) : s.TaskName).ClientTemplate("<# if (IsUserCanEdit) { #><a class=\"<# if (IsMissedVisit) { #>missedvisit<# } else { #>edit<# } #> <# if (IsComplete) { #>complete<# } #>\"><#= TaskName #></a><# } else { #><#=TaskName#><# } #> ").Title("Task").HtmlAttributes(new { @class = "edit-task" });
        columns.Bound(s => s.DateIn).Format("{0:MM/dd/yyyy}").HtmlAttributes(new { @class = "schedule-date" }).Title("Scheduled Date").Width(110);
        columns.Bound(s => s.Range).Title("Time In - Out").Width(130).Visible(Model.Service == AgencyServices.PrivateDuty);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(140);
        columns.Bound(s => s.StatusName).Title("Status");
        columns.Bound(s => s.IsOASISProfileExist).Template(s => s.IsOASISProfileExist ? "<a class=\"OASISprofile img icon16 money\"></a>" : string.Empty)
            .ClientTemplate("<# if (IsOASISProfileExist) { #><a class=\"OASISprofile img icon16 money\"></a><# } #>")
            .Title(" ").HeaderHtmlAttributes(new { @class = "oasisprofileprint icon-header-overlayed" }).HtmlAttributes(new { @class = "oasisprofileprint icon-overlayed" }).Width(30).Sortable(false).Visible((Model.Service == AgencyServices.HomeHealth) && Model.IsOASISProfileExist);
        columns.Bound(s => s.StatusComment).Template(s => s.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 red note\" tooltip=\"{0}\"></a>", s.StatusComment) : string.Empty)
            .ClientTemplate("<# if (StatusComment) { #><a class=\"img icon16 red note\" tooltip=\"<#= StatusComment #>\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).Visible(Model.IsStickyNote).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.Comments).Template(s => s.Comments.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\" tooltip=\"{0}\"></a>", s.Comments) : string.Empty)
            .ClientTemplate("<# if (Comments) { #><a class=\"img icon16 yellow note\" tooltip=\"<#= Comments #>\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).Visible(Model.IsStickyNote).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.EpisodeNotes).Template(s => s.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 blue note\" tooltip=\"{0}\"></a>", s.EpisodeNotes) : string.Empty)
            .ClientTemplate("<# if (EpisodeNotes) { #><a class=\"img icon16 blue note\" tooltip=\"<#= EpisodeNotes #>\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).Visible(Model.IsStickyNote).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.IsUserCanPrint).Template(s => s.IsUserCanPrint ? "<a class=\"" + s.Group + " img icon16 print\"></a>" : string.Empty)
            .ClientTemplate("<# if (IsUserCanPrint) { #><a class=\"<#=Group#> img icon16 print\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.IsAttachmentExist).Template(s => s.IsAttachmentExist ? "<a class=\"img icon16 paperclip attachment\"></a>" : string.Empty)
            .ClientTemplate("<# if(IsAttachmentExist){#> <a class=\"img icon16 paperclip attachment\"></a><#}#>")
            .HeaderHtmlAttributes(new { @class = "attachmentload icon-header-overlayed" }).HtmlAttributes(new { @class = "attachmentload icon-overlayed" }).Title(" ").Width(30).Sortable(false);
        columns.Bound(s => s.Id).Template(s => url(s)).ClientTemplate("<# if(IsUserCanEditDetail || IsUserCanReopen || IsUserCanReassign || IsUserCanRestore || IsUserCanDelete) #><div class='grid-action'><span class='img icon22 pointer right'/></div>").HeaderHtmlAttributes(new { @class = "action" }).HtmlAttributes(new { @class = "action menu-based" }).Title("Action").Width(65).Sortable(false).Visible(isActionVisible);
		columns.Bound(s => s.EpisodeId).HtmlAttributes(new { @class = "eid" }).Hidden(true);
		columns.Bound(s => s.PatientId).HtmlAttributes(new { @class = "pid" }).Hidden(true);
		columns.Bound(s => s.Id).HtmlAttributes(new { @class = "id" }).Hidden(true);
		columns.Bound(s => s.Service).HtmlAttributes(new { @class = "service" }).Hidden(true);
		columns.Bound(s => s.Type).HtmlAttributes(new {  @class = "type" }).Hidden(true);
        columns.Bound(s => s.IsComplete).Visible(false);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ActivitySort", "PatientProfile", new { area = area, PatientId = val, Discipline = Model.DisciplineFilterType, DateRangeId = Model.DateFilterType, RangeStartDate = Model.StartDate, RangeEndDate = Model.EndDate })).ClientEvents(c => c
	   .OnDataBinding("Patient." + Model.Service.ToString() + ".Activities.OnDataBinding")
	   .OnDataBound("Patient." + Model.Service.ToString() + ".Activities.OnDataBound")
	   .OnRowDataBound("Patient." + Model.Service.ToString() + ".Activities.OnRowDataBound")
    ).RowAction(row => {
       if (row.DataItem.IsComplete) {
           if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("completed")) row.HtmlAttributes["class"] += " completed";
           else row.HtmlAttributes.Add("class", "completed");
       }
       if (row.DataItem.IsOrphaned) {
           if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("orphaned")) row.HtmlAttributes["class"] += " orphaned";
           else row.HtmlAttributes.Add("class", "orphaned");
       }
    }).Sortable(sr => sr.OrderBy(so => so.Add(s => s.DateIn).Descending())).Scrollable().Footer(false).Render(); %>
<div class="icon-overlay" style="right:<%= isActionVisible ? 6.9 : 0 %>em"></div>
