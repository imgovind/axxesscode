﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%  var permission = Current.Permissions; %>
<ul class="window-menu">
<%  if (!Current.IsAgencyFrozen) { %>
    <%  var newList = new List<string>(); %>
    <%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Orders, PermissionActions.Add)) { %>
        <%  newList.Add("<li><a class=\"new-order\">New Order</a></li>"); %>
    <%  } %>
    <%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.CommunicationNote, PermissionActions.Add)) { %>
        <%  newList.Add("<li><a class=\"new-comnote\">New Communication Note</a></li>"); %>
    <%  } %>
    <%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Insurance, PermissionActions.Add)) { %>
        <%  newList.Add("<li><a class=\"new-auth\">New Authorization</a></li>"); %>
    <%  } %>
    <%  if (newList.IsNotNullOrEmpty()) { %>
    <li>
        <a class="menu-trigger">Create</a>
        <ul class="menu"><%= newList.ToArray().Join(" ") %></ul>
    </li>
    <%  } %>
<%  } %>
<%  var quickReportsList = new List<string>(); %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Patient, PermissionActions.ViewProfile)) { %>
    <%  quickReportsList.Add("<li><a class=\"patient-profile\">Patient Profile</a></li>"); %>
<%  } %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.MedicationProfile, PermissionActions.ViewList)) { %>
    <%  quickReportsList.Add("<li><a class=\"med-profile\">Medication Profile(s)</a></li>"); %>
<%  } %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.AllergyProfile, PermissionActions.ViewList)) { %>
    <%  quickReportsList.Add("<li><a class=\"allergy-profile\">Allergy Profile</a></li>"); %>
<%  } %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Insurance, PermissionActions.ViewList)) { %>
    <%  quickReportsList.Add("<li><a class=\"auth-list\">Authorizations Listing</a></li>"); %>
<%  } %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.CommunicationNote, PermissionActions.ViewList)) { %>
    <%  quickReportsList.Add("<li><a class=\"comnote-list\">Communication Notes</a></li>"); %>
<%  } %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Orders, PermissionActions.ViewList)) { %>
    <%  quickReportsList.Add(" <li><a class=\"order-list\">Orders and Care Plans</a></li>"); %>
<%  } %>
<%  if (Model.RequestedService==AgencyServices.HomeHealth && Model.HomeHealthStatus == 1) { %>
    <%  quickReportsList.Add("<li><a class=\"summary-list\">60 Day Summaries</a></li>"); %>
<%  } %>
<%  quickReportsList.Add("<li><a class=\"vital-signs\">Vital Signs Charts</a></li>"); %>
<%  quickReportsList.Add("<li><a class=\"triage-class\">Triage Classification</a></li>"); %>
<%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Schedule, PermissionActions.ViewList)) { %>
    <%  quickReportsList.Add("<li><a class=\"deleted-list\">Deleted Tasks/Documents</a></li>"); %>
<%  } %>
<%  if (quickReportsList.IsNotNullOrEmpty()) { %>
    <li>
        <a class="menu-trigger">View</a>
        <ul class="menu"><%= quickReportsList.ToArray().Join(" ") %></ul>
    </li>
<%  } %>
<%  if (!Current.IsAgencyFrozen) { %>
    <%  var scheduleList = new List<string>(); %>
    <%  scheduleList.Add(string.Format("<li><a class=\"schedule\" discharged=\"{0}\" displayname=\"{1}\">Schedule Activity</a></li>", Model.Profile.IsDischarged, Model.DisplayName)); %>
    <%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Schedule, PermissionActions.Reassign)) { %>
        <%  scheduleList.Add("<li><a class=\"reassign\">Reassign Schedules</a></li>"); %>
    <%  } %>
    <%  if (scheduleList.IsNotNullOrEmpty()) { %>
    <li>
        <a class="menu-trigger">Schedule</a>
        <ul class="menu"><%= scheduleList.ToArray().Join(" ") %></ul>
    </li>
    <%  } %>
<%  } %>
    <li>
        <a class="menu-trigger">Upload Document</a>
        <ul class="menu">
            <li><a class="new-upload">New Document</a></li>
            <li><a class="upload-list">View Documents</a></li>
        </ul>
    </li>
<%--<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
    <li><a class="user-access">User Access</a></li>
<%  } %>--%>
</ul>
<%  if (quickReportsList.IsNotNullOrEmpty()) { %>
<div class="wrapper main">
    <div class="boundary">
        <div class="reports fr">
            <h5 class="reports-head">Quick Reports</h5>
            <ul><%= quickReportsList.ToArray().Join(" ") %></ul>
        </div>
        <div class="patient-info">
            <h2 class="patient ac"><%= Model.DisplayNameWithMi %></h2>
            <div class="fl ac">
    <%  if (!Model.PhotoId.IsEmpty()) { %>
                <img src="/Asset/<%= Model.PhotoId %>" alt="User Photo" />
    <%  } else { %>
                <img src="/Images/blank_user.jpeg" alt="User Photo" />
    <%  } %>
                <div class="clear"></div>
    <%  if (!Current.IsAgencyFrozen && permission.IsInPermission(Model.RequestedService, ParentPermission.Patient, PermissionActions.Edit)) { %>
                [ <a class="link edit-photo">Change Photo</a> ]
    <%  } %>
            </div>
            <div>MRN<div class="fr"><%= Model.PatientIdNumber %></div></div>
            <div>DOB<div class="fr"><%= Model.DOB.IsValid() ? Model.DOB.ToShortDateString() : string.Empty %></div></div>
            <div>SOC<div class="fr"><%= Model.Profile.StartOfCareDateFormatted %></div></div>    
    <%  if (Model.PhoneHome.IsNotNullOrEmpty()) { %>
            <div>Phone<div class="fr"><%= Model.PhoneHome.ToPhone()%></div></div>
    <%  } %>
    <%  if (Model.Physician != null) { %>
            <div>Physician<div class="fr"><%= Model.Physician.DisplayName.Trim() %></div></div>
    <%  } %>
            <div>Insurance<div class="fr"><%= Model.Profile.PrimaryInsuranceName %> / <%= Model.Profile.PrimaryHealthPlanId.IsNotNullOrEmpty() ? Model.Profile.PrimaryHealthPlanId : Model.MedicareNumber %></div></div>
            <div class="ac">
                <span>
    <%  if (!Current.IsAgencyFrozen && permission.IsInPermission(Model.RequestedService, ParentPermission.Patient, PermissionActions.Edit)) { %>
                    [ <a class="link edit-patient">Edit</a> ]
    <%  } %>
                    [ <a class="link patient-moreinfo">More</a> ]
                    [ <a class="link patient-map">Directions</a> ]
    <%  if (permission.IsInPermission(Model.RequestedService, ParentPermission.Patient, PermissionActions.Edit))
        { %>
                    [ <a class="link admit-list">Admissions</a> ]
    <%  } %>
                </span>
            </div>
            <ul class="buttons ac">
                <li><a class="refresh">Refresh</a></li>
    <%  if (permission.IsInPermission(Model.RequestedService,ParentPermission.Schedule,PermissionActions.ViewList) && !Current.IsAgencyFrozen)
        { %>
                <li><a class="schedule" discharged="<%= Model.Profile.IsDischarged %>" displayname="<%= Model.DisplayName %>">Schedule Activity</a></li>
    <%  } %>
    <%  if (!Current.IsAgencyFrozen && permission.IsInPermission(Model.RequestedService, ParentPermission.Patient, PermissionActions.ChangeStatus)) { %>
                <li><a class="change-status">Change Status</a></li>
    <%  } %>
    <%  if (Model.Profile.IsDischarged && !Current.IsAgencyFrozen && permission.IsInPermission(Model.RequestedService, ParentPermission.Patient, PermissionActions.Admit)) { %>
                <li><a class="readmit">Re-Admit</a></li>
    <%  } %>
            </ul>
        </div>
        <div class="clr"></div>
    </div>
</div>
<%  } %>