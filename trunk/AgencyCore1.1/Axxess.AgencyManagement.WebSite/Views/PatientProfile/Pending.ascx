﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Pending Patient Admissions | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  var pageName = "PatientPending"; %>
    <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="fr buttons ac">
    <%  if (!Current.IsAgencyFrozen && Model.NewPermissions!= AgencyServices.None) { %>
        <li><a service="<%= (int)Model.NewPermissions %>" class="new servicepermission">New Patient</a></li><br />
    <%  } %>
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a area="<%=area %>" url="/Export/PendingAdmissionsXls" service="<%= (int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li>
    <%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a area="<%=area %>" url="/PatientProfile/PendingList" class="grid-refresh">Refresh</a></div>
        <div class="filter"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" })%></div>
        <div class="filter optional">
            <label for="<%= pageName%>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.Id.ToString(),(int)Model.AvailableService, new { @class = "input", @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer">
    <%  var action = "<# if (IsUserCanEdit) { #><a onclick=\"Patient.Demographic.Edit.Open('<#= Id #>','<#= ServicePrefix #>')\" class=\"link\">Edit</a><# } #><# if (IsUserCanAdmit) { #><a onclick=\"Patient.Demographic.Admit.Open('<#=Id#>')\" class=\"link\">Admit</a><# } #><# if (IsUserCanNonAdmit) { #><a onclick=\"Patient.Demographic.NonAdmit.Open('<#=Id#>')\" class=\"link\">Non-Admit</a><# } #>";%>
        <%= Html.Telerik().Grid<PendingPatient>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
                columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(120);
                columns.Bound(p => p.DisplayName).Title("Patient");
                columns.Bound(p => p.PrimaryInsuranceName).Title("Primary Insurance").Sortable(false);
			    columns.Bound(p => p.Branch).Title("Branch").Sortable(false).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
                columns.Bound(p => p.Created).Format("{0:MM/dd/yyyy}").Title("Date Added").Width(100);
                columns.Bound(p => p.Id).Sortable(false).ClientTemplate(action).Title("Action").Width(155).Visible(!Current.IsAgencyFrozen);
            }).NoRecordsTemplate("No Pending Patients found.").DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("PendingList", "PatientProfile", new { @area=area, BranchId = Model.Id})).Sortable().ClientEvents(events => events
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("Patient.Demographic.Pending.OnDataBound")
                .OnError("U.OnTGridError")
            ).Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
</div>
<%  } %>