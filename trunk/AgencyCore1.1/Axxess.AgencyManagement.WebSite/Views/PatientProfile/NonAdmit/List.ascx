﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">Non-Admitted Patients | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
<%  var pageName = "PatientNonAdmit"; %>
<%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="fr buttons">
<%  if (!Current.IsAgencyFrozen && Model.NewPermissions != AgencyServices.None) { %>
        <li><a service="<%=(int)Model.NewPermissions %>" class="new servicepermission">New Patient</a></li>
        <div class="clr"></div>
<%  } %>
<%  if (Model.ExportPermissions != AgencyServices.None) { %>
		<li><a area="<%=area %>" url="/Export/NonAdmitExport" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li>
<%  } %>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a area="<%=area %>" url="/PatientProfile/NonAdmitList" class="grid-refresh">Refresh</a></div>
        <div class="filter"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" })%></div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer">
        <%  var action = "<# if (IsUserCanAdmit) { #><a onclick=\"<#=NonAdmitTypeName#>.Demographic.Admit.Open('<#=Id#>')\">Admit</a><# } #>"; %>
        <%= Html.Telerik().Grid<NonAdmit>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
				columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(15).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
				columns.Bound(p => p.DisplayName).Title("Patient").Width(20);
				columns.Bound(p => p.InsuranceName).Title("Insurance").Width(20).Sortable(false);
				columns.Bound(p => p.PolicyNumber).Title("Policy #").Width(15).Sortable(false);
				columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("DOB").Width(12);
				columns.Bound(p => p.Phone).Title("Phone").Width(15).Sortable(false);
				columns.Bound(p => p.Gender).Width(8);
				columns.Bound(p => p.NonAdmissionReason).Width(25).Title("Non-Admit Reason");
				columns.Bound(p => p.NonAdmitDate).ClientTemplate("<#= U.FormatGridDate(NonAdmitDate) #>").Title("Non-Admit Date").Width(16);
				columns.Bound(p => p.Comments).Width(5).Sortable(false).Title(" ").HtmlAttributes(new { @class = "centered-unpadded-cell" }).ClientTemplate("<a tooltip=\"<#=CommentsCleaned#>\" class=\"note img icon16\" />");
                columns.Bound(p => p.IsUserCanAdmit).Width(8).Sortable(false).ClientTemplate(action).Title("Action").Visible(!Current.IsAgencyFrozen);
			}).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("NonAdmitList", "PatientProfile")).ClientEvents(evnts => evnts
                .OnRowDataBound("U.DataItemToolTip")
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("Patient.Demographic.NonAdmit.OnDataBound")
                .OnError("U.OnTGridError")
            ).NoRecordsTemplate("No Non-Admitted Patients found.").Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
	</div>
</div>
<%  } %>