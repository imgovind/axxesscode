﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientScheduleEventViewData>" %>
<div class="wrapper main">
    <fieldset class="no-input">
        <div class="wide-column">
            <div class="row">
                <label class="fl">MRN</label>
                <div class="fr"><%= Model.Patient.PatientIdNumber %></div>
            </div>
            <div class="row">
                <label class="fl">Name</label>
                <div class="fr"><%= Model.Patient.DisplayName%></div>
            </div>
            <div class="row">
                <label class="fl">Gender</label>
                <div class="fr"><%= Model.Patient.Gender%></div>
            </div>
            <div class="row">
                <label class="fl">Address</label>
                <div class="fr"><%= Model.Patient.AddressFirstRow.ToTitleCase()%></div>
            </div>
            <div class="row">
                <label class="fl">City, State, Zip</label>
                <div class="fr"><%= Model.Patient.AddressSecondRow.IsNotNullOrEmpty() ? Model.Patient.AddressSecondRow.ToTitleCase() : string.Empty%></div>
            </div>
            <div class="row">
                <label class="fl">Home Phone</label>
                <div class="fr"><%= Model.Patient.PhoneHome.ToPhone()%></div>
            </div>
            <div class="row">
                <label class="fl">Mobile Phone</label>
                <div class="fr"><%= Model.Patient.PhoneMobile.ToPhone()%></div>
            </div>
            <div class="row">
                <label class="fl">Start of Care Date</label>
                <div class="fr"><%= Model.Patient.Profile.StartOfCareDateFormatted%></div>
            </div>
            <div class="row">
                <label class="fl">Date of Birth</label>
                <div class="fr"><%= Model.Patient.DOBFormatted%></div>
            </div>
            <div class="row">
                <label class="fl">Medicare #</label>
                <div class="fr"><%= Model.Patient.MedicareNumber%></div>
            </div>
            <div class="row">
                <label class="fl">Medicaid #</label>
                <div class="fr"><%= Model.Patient.MedicaidNumber %></div>
            </div>
<%  if (Model.Patient.Physician != null) { %>
            <div class="row">
                <label class="fl">Physician Name</label>
                <div class="fr"><%= Model.Patient.Physician.DisplayName%></div>
            </div>
            <div class="row">
                <label class="fl">Physician Phone</label>
                <div class="fr"><%= Model.Patient.Physician.PhoneWork.ToPhone()%></div>
            </div>
            <div class="row">
                <label class="fl">Physician Fax</label>
                <div class="fr"><%= Model.Patient.Physician.FaxNumber.ToPhone()%></div>
            </div>
<%  } %>
<%  if (Model.Patient.EmergencyContact != null) { %>
            <div class="row">
                <label class="fl">Emergency Name</label>
                <div class="fr"><%= Model.Patient.EmergencyContact.DisplayName%></div>
            </div>
            <div class="row">
                <label class="fl">Emergency Phone</label>
                <div class="fr"><%= Model.Patient.EmergencyContact.PrimaryPhone.ToPhone()%></div>
            </div>
<%  } %>
            <div class="row">
                <label class="fl">DNR</label>
                <div class="fr"><%= Model.Patient.IsDNR ? "Yes" : "No"%></div>
            </div>
        </div>
    </fieldset>
</div>