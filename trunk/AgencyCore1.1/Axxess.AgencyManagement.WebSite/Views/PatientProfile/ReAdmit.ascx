﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientNameViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("PatientReadmit", "PatientProfile", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "readmitForm", @class = "mainform" })) { %>
    <%= Html.Hidden("PatientId", Model.PatientId)%>
    <fieldset>
        <legend>Re-Admission Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="ChangeStatus_ReadmissionDate" class="fl">Re-admission Date (Start of Care)</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReadmissionDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="ChangeStatus_ReadmissionDate" /></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Re-admit</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>