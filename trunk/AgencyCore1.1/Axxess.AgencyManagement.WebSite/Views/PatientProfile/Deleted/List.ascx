﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientDataViewData>" %>
<span class="wintitle">Deleted Patients | <%= Current.AgencyName %></span>
<%  if (Model.AvailableService != AgencyServices.None && Model.Service != AgencyServices.None) { %>
    <%  var pageName = "DeletedPatients"; %>
    <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%>
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
    <div class="fr button"><a area="<%= area %>" url="/Export/DeletedPatients" service="<%= (int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></div>
    <%  } %>
    <fieldset class="grid-controls ac">
        <div class="button fr"><a area="<%= area %>" url="/PatientProfile/DeletedPatientContent" class="grid-refresh">Refresh</a></div>
        <div class="filter ignore-grid-binding"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" })%></div>
        <div class="filter">
            <label for="<%= pageName%>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.AvailableService,  new { @class = "location", @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <div id="<%= pageName %>GridContainer"><% Html.RenderPartial("Deleted/ListContent", Model); %> </div>
</div>
<%  } %>