﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientDataViewData>" %>
<%  if (Model.AvailableService.Has(Model.Service)) { %>
    <%  var pageName = "DeletedPatients"; %>
<%= Html.Filter("SortParams", string.Format("{0}-{1}", Model.SortColumn, Model.SortDirection), new { @id = pageName + "_SortParams", @class = "sort-parameters" })%>  
<%= Html.Telerik().Grid(Model.Patients).Name(pageName+"_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }) .Columns(columns => {
        columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90);
        columns.Bound(p => p.DisplayName).Title("Patient").Width(180);
        columns.Bound(p => p.Address).Title("Address").Sortable(false);
        columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(100);
        columns.Bound(p => p.Gender).Width(80).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
        columns.Bound(p => p.Phone).Title("Phone").Width(120).Sortable(false);
        columns.Bound(p => p.Status).Title("Last Status").Width(95);
        if (Model.RestorePermissions.Has(Model.Service)) columns.Bound(p => p.Id).Width(90).Sortable(false).Template(s => string.Format("<a onclick=\"Patient.Demographic.Restore('{0}')\">Restore</a>", s.Id)).Title("Action").Visible(!Current.IsAgencyFrozen);
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "PatientIdNumber") {
            if (sortDirection == "ASC") order.Add(o => o.PatientIdNumber).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientIdNumber).Descending();
        } else if (sortName == "DisplayName") {
            if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
        } else if (sortName == "DateOfBirth") {
            if (sortDirection == "ASC") order.Add(o => o.DateOfBirth).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DateOfBirth).Descending();
        } else if (sortName == "Gender") {
            if (sortDirection == "ASC") order.Add(o => o.Gender).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Gender).Descending();
        } else if (sortName == "Status") {
            if (sortDirection == "ASC") order.Add(o => o.Status).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Status).Descending();
        }
    })).NoRecordsTemplate("No Deleted Patients found.").Scrollable(scrolling => scrolling.Enabled(true)) %>
<%  } %>