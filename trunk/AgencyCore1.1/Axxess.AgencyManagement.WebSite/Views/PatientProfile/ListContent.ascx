﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientDataViewData>" %>
<%  if (Model.AvailableService.Has(Model.Service)) { %>
    <%  var pageName = "PatientList"; %>
    <%  var sortName = Model.SortColumn; %>
    <%  var sortDirection = Model.SortDirection; %>
<%= Html.Filter("SortParams", string.Format("{0}-{1}", sortName, sortDirection), new { @id = pageName + "_SortParams", @class = "sort-parameters" })%>  
<%= Html.Telerik().Grid(Model.Patients).Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
        columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90);
        columns.Bound(p => p.DisplayName).Title("Patient").Width(180);
        columns.Bound(p => p.InsuranceName).Title("Insurance").Width(105).Sortable(false);
        columns.Bound(p => p.PolicyNumber).Title("Policy #").Width(105).Sortable(false);
        columns.Bound(p => p.Address).Title("Address").Sortable(false).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result => result != null && result.Count != null ? "Total: " + result.Count.Format("{0:N0}") : "Total: 0");
        columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(85).Sortable(true);
        columns.Bound(p => p.Gender).Width(60).Sortable(true);
        columns.Bound(p => p.Phone).Title("Phone").Width(105).Sortable(false);
        columns.Bound(p => p.Status).Title("Status").Width(80).Sortable(false);
        if (!Current.IsAgencyFrozen) {
            var action = new List<string>();
            if (Model.EditPermissions.Has(Model.Service)) action.Add("<a onclick=\"Patient.Demographic.Edit.Open('{0}','{1}')\" class=\"link\">Edit</a>");
            if (Model.DeletePermissions.Has(Model.Service)) action.Add("<a onclick=\"Patient.Demographic.Delete('{0}')\" class=\"deletePatient link\">Delete</a>");
            if (action.IsNotNullOrEmpty()) columns.Bound(p => p.Id).Width(95).Sortable(false).Template(s => string.Format(action.ToArray().Join(""), s.Id, s.ServicePrefix)).Title("Action");
        }
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            if (sortName == "PatientIdNumber") {
                if (sortDirection == "ASC") order.Add(o => o.PatientIdNumber).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.PatientIdNumber).Descending();
            } else if (sortName == "DisplayName") {
                if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
            } else if (sortName == "DateOfBirth") {
                if (sortDirection == "ASC") order.Add(o => o.DateOfBirth).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DateOfBirth).Descending();
            } else if (sortName == "Gender") {
                if (sortDirection == "ASC") order.Add(o => o.Gender).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.Gender).Descending();
            }
    })).NoRecordsTemplate("No Patients found.").Scrollable(scrolling => scrolling.Enabled(true)).Footer(true) %>
<%  } %>