﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<TaskLean>>" %>
<% string pagename = "ScheduleVisitsByType"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
                 .Columns(columns =>
                 {
                 columns.Bound(m => m.PatientIdNumber).Title("MRN").Width(150);
                 columns.Bound(m => m.PatientName).Title("Patient");
                 columns.Bound(m => m.StatusName).Title("Status");
                 columns.Bound(m => m.EventDate).Title("Schedule Date").Width(110);
                 columns.Bound(p => p.UserName).Title("Clinician");
               })
                           .Sortable(sorting =>
              sorting.SortMode(GridSortMode.SingleColumn)
                  .OrderBy(order =>
                  {
                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                      if (sortName == "PatientIdNumber")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientIdNumber).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientIdNumber).Descending();
                          }
                      }
                      else if (sortName == "PatientName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientName).Descending();
                          }

                      }
                      else if (sortName == "UserName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.UserName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.UserName).Descending();
                          }
                      }
                      else if (sortName == "StatusName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.StatusName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.StatusName).Descending();
                          }
                      }
                      else if (sortName == "EventDate")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.EventDate).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.EventDate).Descending();
                          }
                      }

                  })
                  )
               .Scrollable().Footer(false)  %>
   

