﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "ScheduleEmployeeWeekly"; %>
 <%=Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Title("Task").Sortable(false);
         columns.Bound(p => p.StatusName).Title("Status").Sortable(false);
         columns.Bound(p => p.ScheduleDate).Title("Schedule Date").Width(120);
         columns.Bound(p => p.VisitDate).Title("Visit Date").Width(110);
         })
             .Sortable(sorting =>
                  sorting.SortMode(GridSortMode.SingleColumn)
                      .OrderBy(order =>
                      {
                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                          if (sortName == "PatientName")
                          {
                              if (sortDirection == "ASC")
                              {
                                  order.Add(o => o.PatientName).Ascending();
                              }
                              else if (sortDirection == "DESC")
                              {
                                  order.Add(o => o.PatientName).Descending();
                              }
                          }
                          else if (sortName == "ScheduleDate")
                          {
                              if (sortDirection == "ASC")
                              {
                                  order.Add(o => o.ScheduleDate).Ascending();
                              }
                              else if (sortDirection == "DESC")
                              {
                                  order.Add(o => o.ScheduleDate).Descending();
                              }
                          }
                          else if (sortName == "VisitDate")
                          {
                              if (sortDirection == "ASC")
                              {
                                  order.Add(o => o.VisitDate).Ascending();
                              }
                              else if (sortDirection == "DESC")
                              {
                                  order.Add(o => o.VisitDate).Descending();
                              }
                          }
                      })
                  )
                .Scrollable()
                .Footer(false)%>
   
