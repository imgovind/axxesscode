﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<TaskLean>>" %>
<% string pagename = "PatientMonthlySchedule"; %>
 <% =Html.Telerik()
     .Grid(Model)
      .Name(pagename + "Grid")        
         .Columns(columns =>
                     {
                     columns.Bound(s => s.DisciplineTaskName).Title("Task");
                     columns.Bound(p => p.StatusName).Title("Status").Sortable(false);
                     columns.Bound(p =>p.EventDate).Title("Schedule Date").Width(100);
                     columns.Bound(p =>p.VisitDate).Title("Visit Date").Width(80);
                     columns.Bound(s => s.UserName).Title("Employee");
                     }).Sortable(sorting =>
                      sorting.SortMode(GridSortMode.SingleColumn)
                          .OrderBy(order =>
                          {
                              var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                              var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                              if (sortName == "DisciplineTaskName")
                              {
                                  if (sortDirection == "ASC")
                                  {
                                      order.Add(o => o.DisciplineTaskName).Ascending();
                                  }
                                  else if (sortDirection == "DESC")
                                  {
                                      order.Add(o => o.DisciplineTaskName).Descending();
                                  }
                              }
                              else if (sortName == "UserName")
                              {
                                  if (sortDirection == "ASC")
                                  {
                                      order.Add(o => o.UserName).Ascending();
                                  }
                                  else if (sortDirection == "DESC")
                                  {
                                      order.Add(o => o.UserName).Descending();
                                  }
                              }
                              else if (sortName == "EventDate")
                              {
                                  if (sortDirection == "ASC")
                                  {
                                      order.Add(o => o.EventDate).Ascending();
                                  }
                                  else if (sortDirection == "DESC")
                                  {
                                      order.Add(o => o.EventDate).Descending();
                                  }
                              }
                              else if (sortName == "VisitDate")
                              {
                                  if (sortDirection == "ASC")
                                  {
                                      order.Add(o => o.VisitDate).Ascending();
                                  }
                                  else if (sortDirection == "DESC")
                                  {
                                      order.Add(o => o.VisitDate).Descending();
                                  }
                              }})).Scrollable().Footer(false)%>
  
