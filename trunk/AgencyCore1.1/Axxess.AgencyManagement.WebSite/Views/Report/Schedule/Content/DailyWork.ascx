﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<TaskLean>>" %>
<% string pagename = "ScheduleDailyWork"; %>
  <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
             columns.Bound(s => s.PatientIdNumber).Title("MRN").Width(70);
             columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.DisciplineTaskName).Title("Task").Sortable(false);
         columns.Bound(s => s.StatusName).Title("Status").Sortable(false);
         columns.Bound(s => s.EventDate).Title("Schedule Date").Width(100);
         columns.Bound(s => s.UserName).Title("Employee");
       })
              // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, Date = DateTime.Now }))
               .Sortable(sorting =>
                              sorting.SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                  {
                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                      if (sortName == "PatientIdNumber")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.PatientIdNumber).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.PatientIdNumber).Descending();
                                          }

                                      }
                                      else if (sortName == "PatientName")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.PatientName).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.PatientName).Descending();
                                          }

                                      }
                                      else if (sortName == "UserName")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.UserName).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.UserName).Descending();
                                          }
                                      }
                                      else if (sortName == "EventDate")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.EventDate).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.EventDate).Descending();
                                          }
                                      }

                                  })).Scrollable().Footer(false)%>
  
