﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<TaskLean>>" %>
<span class="wintitle">Visits By Status | Report Center</span>
<%  string pagename = "ScheduleVisitsByStatus"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="VisitsByStatusContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportVisitsByStatus">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StartDate" class="fl">Date Range</label>
            <div class="fr">
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                &#8211;
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_Status" class="fl">Status</label>
            <div class="fr"><%= Html.VisitStatus("Status", "", 1, true, "-- Select Status --", "0", new { @id = pagename + "_Status", @class = "required not-zero" })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_Patient" class="fl">Patient</label>
            <div class="fr"><%= Html.Patients((int)service, "PatientId",branchId.ToGuid(), "", 0, "All", new { @id = pagename + "_Patient", @class = " patient all" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_Clinician" class="fl">Assigned To</label>
            <div class="fr"><%= Html.Users("ClinicianId", "", "All",Guid.Empty, (int)UserStatus.Active, (int)service, new { @id = pagename + "_Clinician", @class = "user" })%></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Schedule/Content/VisitsByStatus", Model); %></div>