﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<License>>" %>
<% string pagename = "AllEmployeeLicense"; %>

<% Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
	{
		columns.Bound(l => l.DisplayName).Sortable(true).Hidden(true).Title("Employee").GroupHeaderTemplate(result =>
			{%>
				<% var split = result.Key.ToString().Split('_'); %>
				<%= result.Title %>: <%= split[0] %><%= split.Length > 1 ? ", Custom Id: " + split[1] : string.Empty %><%
			});
		columns.Bound(l => l.LicenseType).Title("License Type");
		columns.Bound(l => l.LicenseNumber);
		columns.Bound(l => l.IssueDate);
		columns.Bound(l => l.ExpireDate).Title("Expiration Date");
	})
    .Groupable(settings => settings.Groups(groups => groups.Add(s => s.DisplayName)).Visible(false))
           .Sortable(sorting =>
            sorting.SortMode(GridSortMode.SingleColumn)
                .OrderBy(order =>
                {
                    var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                    var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                    if (sortName == "LicenseType")
                    {
                        if (sortDirection == "ASC")
                        {
                            order.Add(o => o.LicenseType).Ascending();
                        }
                        else if (sortDirection == "DESC")
                        {
                            order.Add(o => o.LicenseType).Descending();
                        }
                    }
					else if (sortName == "IssueDate")
                    {
                        if (sortDirection == "ASC")
                        {
							order.Add(o => o.IssueDate).Ascending();
                        }
                        else if (sortDirection == "DESC")
                        {
                            order.Add(o => o.IssueDate).Descending();
                        }
                    }
					else if (sortName == "ExpireDate")
                    {
                        if (sortDirection == "ASC")
                        {
							order.Add(o => o.ExpireDate).Ascending();
                        }
                        else if (sortDirection == "DESC")
                        {
                            order.Add(o => o.ExpireDate).Descending();
                        }
                    }
                })
        )
    .Scrollable()
    .Footer(false)
	.Render();%>
<script>
	U.OnTGridDataBoundCollapseGroups($("#<%= pagename %>Grid"));
</script>