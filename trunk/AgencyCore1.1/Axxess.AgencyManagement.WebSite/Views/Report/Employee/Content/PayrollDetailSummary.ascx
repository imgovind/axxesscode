﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PayrollDetailSummaryItem>>" %>
<% string pagename = "PayrollDetailSummaryContent"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").HtmlAttributes(new { @class = "bottom-bar"})
	   .Columns(columns =>
	   {
		   columns.Bound(s => s.TaskName).Title("Task").ReadOnly().Width(450);
		   columns.Bound(s => s.TaskCount).Title("Visits").Width(100).ReadOnly();
		   columns.Bound(s => s.TotalTaskTimeInGrid).Template(t => t.TotalTaskTimeFormatted).Title("Time").Width(180).ReadOnly()
               .Aggregate(aggregate => aggregate.Sum()).FooterTemplate(ft => ft.Sum != null ? "<p class='al'>Total Time: " + ((int)ft.Sum.Value / 60) + " Hrs, " + ((int)ft.Sum.Value % 60)  + " Min.</p>" : string.Empty);
		   columns.Bound(s => s.TotalMileage).Template(t => t.TotalMileageFormatted).Title("Mileage").Width(105).ReadOnly()
               .Aggregate(aggregate => aggregate.Sum()).FooterTemplate(ft => ft.Sum != null ? "<p class='al'>Total Mileage: " + ft.Sum + "</p>" : string.Empty);
		   columns.Bound(s => s.TotalSurcharges).Template(t => t.TotalSurchargesFormatted).Title("Surcharge").Width(150).ReadOnly()
			   .Aggregate(aggregate => aggregate.Sum()).FooterTemplate(ft => ft.Sum != null ? "<p class='al'>Total Surcharge: " + string.Format("{0:C}", ft.Sum.Value) + "</p>" : string.Empty);
	   }).Scrollable()
 %>
 <script>
	 $("#PayrollDetailSummaryContentGrid .t-grid-bottom").remove();
 </script>
