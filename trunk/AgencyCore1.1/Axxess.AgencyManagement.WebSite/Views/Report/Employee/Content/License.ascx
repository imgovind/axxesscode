﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<License>>" %>
<% string pagename = "EmployeeLicenseListing"; %>
<% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(l => l.LicenseType).Title("License Name");
         columns.Bound(l => l.LicenseNumber).Title("License Number");
         columns.Bound(l => l.IssueDate).Title("Initiation Date");
         columns.Bound(l => l.ExpireDate).Title("Expiration Date");
}).Sortable(sorting =>
                         sorting.SortMode(GridSortMode.SingleColumn)
                             .OrderBy(order =>
                             {
                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                 if (sortName == "LicenseType")
                                 {
                                     if (sortDirection == "ASC")
                                     {
                                         order.Add(o => o.LicenseType).Ascending();
                                     }
                                     else if (sortDirection == "DESC")
                                     {
                                         order.Add(o => o.LicenseType).Descending();
                                     }
                                 }
								 else if (sortName == "IssueDate")
                                 {
                                     if (sortDirection == "ASC")
                                     {
										 order.Add(o => o.IssueDate).Ascending();
                                     }
                                     else if (sortDirection == "DESC")
                                     {
										 order.Add(o => o.IssueDate).Descending();
                                     }
                                 }
								 else if (sortName == "ExpireDate")
                                 {
                                     if (sortDirection == "ASC")
                                     {
										 order.Add(o => o.ExpireDate).Ascending();
                                     }
                                     else if (sortDirection == "DESC")
                                     {
										 order.Add(o => o.ExpireDate).Descending();
                                     }
                                 }

                             })
                     )
           .Scrollable()
                   .Footer(false)%>
  


