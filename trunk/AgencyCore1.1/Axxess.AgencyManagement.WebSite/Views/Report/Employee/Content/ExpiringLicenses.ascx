﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<% string pagename = "EmployeeExpiringLicense"; %>
<% Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
 {
     columns.Bound(l => l.CustomDisplayName).Sortable(true).Hidden(true).GroupHeaderTemplate(result =>
			{%>
				<% var split = result.Key.ToString().Split('_'); %>
			    Employee: <%= split[0] %><%= split.Length > 1 ? ", Custom Id: " + split[1] : string.Empty %><%
			});
     columns.Bound(l => l.LicenseType).Title("License Name").Sortable(false);
     columns.Bound(l => l.LicenseNumber).Title("License Number");
     columns.Bound(l => l.IssueDate).Title("Initiation Date");
     columns.Bound(l => l.ExpireDate).Title("Expiration Date");
 })
 .Groupable(settings => settings.Groups(groups => groups.Add(s => s.CustomDisplayName)))
 .Sortable(sorting =>
	 sorting.SortMode(GridSortMode.SingleColumn)
		 .OrderBy(order =>
		 {
			 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
			 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
			 if (sortName == "DisplayName")
			 {
				 if (sortDirection == "ASC")
				 {
					 order.Add(o => o.DisplayName).Ascending();
				 }
				 else if (sortDirection == "DESC")
				 {
					 order.Add(o => o.DisplayName).Descending();
				 }
			 }
			 else if (sortName == "CustomId")
			 {
				 if (sortDirection == "ASC")
				 {
					 order.Add(o => o.CustomId).Ascending();
				 }
				 else if (sortDirection == "DESC")
				 {
					 order.Add(o => o.CustomId).Descending();
				 }
			 }
			 else if (sortName == "LicenseNumber")
			 {
				 if (sortDirection == "ASC")
				 {
					 order.Add(o => o.LicenseNumber).Ascending();
				 }
				 else if (sortDirection == "DESC")
				 {
					 order.Add(o => o.LicenseNumber).Descending();
				 }
			 }
			 else if (sortName == "IssueDate")
			 {
				 if (sortDirection == "ASC")
				 {
					 order.Add(o => o.IssueDate).Ascending();
				 }
				 else if (sortDirection == "DESC")
				 {
					 order.Add(o => o.IssueDate).Descending();
				 }
			 }
			 else if (sortName == "ExpireDate")
			 {
				 if (sortDirection == "ASC")
				 {
					 order.Add(o => o.ExpireDate).Ascending();
				 }
				 else if (sortDirection == "DESC")
				 {
					 order.Add(o => o.ExpireDate).Descending();
				 }
			 }
		 })
	)
 .Scrollable()
 .Footer(false)
 .Render();%>
<script>
	U.OnTGridDataBoundCollapseGroups($("#<%= pagename %>Grid"));
</script>