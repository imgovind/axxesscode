﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserPermissionViewData>>" %>
<% string pagename = "EmployeePermissions"; %>

<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
{
   //columns.Bound(s => s.UserDisplayName).Sortable(true).Visible(false);
    columns.Bound(p => p.Permission).Title("Permission");
})
        .Groupable(settings => settings.Groups(groups =>
        {
            groups.Add(s => s.Employee);
        }))
               .Sortable(sorting =>
                sorting.SortMode(GridSortMode.SingleColumn)
                    .OrderBy(order =>
                    {
                        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                    })
            )
        .Scrollable()
        .Footer(false)%>
<script>
     U.OnTGridDataBoundCollapseGroups($("#<%= pagename %>Grid"));
</script>