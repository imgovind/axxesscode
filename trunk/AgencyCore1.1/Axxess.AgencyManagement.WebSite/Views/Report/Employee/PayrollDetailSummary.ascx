﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PayrollDetailSummaryItem>>" %>
<span class="wintitle">Payroll Detail Summary | Report Center</span>
<%  string pagename = "PayrollDetailSummary"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="PayrollDetailSummaryContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportPayrollDetailSummary">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StartDate" class="fl">Date Range</label>
            <div class="fr">
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-15).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                &#8211;
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(15).ToShortDateString() %>" id="<%= pagename %>_EndDate" />
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_UserId" class="fl">Employee Name</label>
            <div class="fr"><%= Html.Users("UserId", "", "-- Select User --", branchId.ToGuid(), 0, (int)service, new { @id = pagename + "_UserId", @class = "user required not-zero" })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_PayrollStatus" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_PayrollStatus" name="PayrollStatus">
                    <option value="true">Paid</option>
                    <option value="false" selected="selected">Unpaid</option>
                </select>
            </div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Employee/Content/PayrollDetailSummary", Model); %></div>