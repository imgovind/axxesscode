﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<span class="wintitle">Employee License Listing | Report Center</span>
<%  string pagename = "EmployeeLicenseListing"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="EmployeeLicenseContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportEmployeeLicense">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Inactive</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_UserId" class="fl">Employee</label>
            <div class="fr"><%= Html.GroupedUsersAndNonUsers("UserId", Guid.Empty, "-- Select Employee --", new { @id = pagename + "_UserId", @updateGroup = "0", @class = "user grouped required not-zero" })%></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Employee/Content/License", Model); %></div>