﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Birthday>>" %>
<span class="wintitle">Employee Birthday Listing | Report Center</span>
<%  string pagename = "EmployeeBirthdayList"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="EmployeeBirthdaysContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportEmployeeBirthdays">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Inactive</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_Month" class="fl">Month</label>
            <div class="fr"><%= Html.CalendarMonths("Month",DateTime.Now.Month,true,new { @id = pagename + "_Month" }) %></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Employee/Content/BirthdayList", Model); %></div>