﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<Revenue>>" %>
<% string pagename = "UnbilledRevenue"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient");
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode").Width(150);
               columns.Bound(p => p.AssessmentTypeName).Sortable(false).Title("Assessment");
               columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
               columns.Bound(p => p.RapAmount).Sortable(false).Format("${0:0.00}").Title("RAP Amount");
               columns.Bound(p => p.ProspectivePayment).Sortable(false).Format("{0:0.00}").Title("Episode Payment");
               columns.Bound(p => p.BillableVisitCount).Sortable(false).Title("Total Visits");
               columns.Bound(p => p.CompletedVisitCount).Sortable(false).Title("Completed Visits");
               columns.Bound(p => p.UnbilledVisitCount).Sortable(false).Title("Unbilled Visits");
               columns.Bound(p => p.UnitAmount).Sortable(false).Format("${0:0.00}").Title("Unit Amount");
               columns.Bound(p => p.UnbilledRevenueAmount).Sortable(false).Format("${0:0.00}").Title("Unbilled Amount");
           })
            .Sortable(sorting =>
             sorting.SortMode(GridSortMode.SingleColumn)
                 .OrderBy(order =>
                 {
                     var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                     var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                     if (sortName == "PatientIdNumber")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.PatientIdNumber).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.PatientIdNumber).Descending();
                         }
                     }
                     else if (sortName == "DisplayName")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.DisplayName).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.DisplayName).Descending();
                         }
                     }
                 })).Scrollable().Footer(false)%>

