﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "ManagedClaimsByStatus"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient");
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode").Width(160);
               columns.Bound(p => p.ClaimAmount).Sortable(false).Format("${0:0.00}").Title("Claim Amount").Width(100);
               columns.Bound(p => p.ClaimDate).Title("Claim Date").Width(80);
               columns.Bound(p => p.PaymentAmount).Sortable(false).Format("${0:0.00}").Title("Payment Amount").Width(110);
               columns.Bound(p => p.PaymentDate).Title("Payment Date").Width(90);
           })
                                 .Sortable(sorting =>
                                                      sorting.SortMode(GridSortMode.SingleColumn)
                                                          .OrderBy(order =>
                                                          {
                                                              var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                              var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                              if (sortName == "PatientIdNumber")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.PatientIdNumber).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.PatientIdNumber).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "DisplayName")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.DisplayName).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.DisplayName).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "PaymentDate")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.PaymentDate).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.PaymentDate).Descending();
                                                                  }
                                                              }
                                                              else if (sortName == "ClaimDate")
                                                              {
                                                                  if (sortDirection == "ASC")
                                                                  {
                                                                      order.Add(o => o.ClaimDate).Ascending();
                                                                  }
                                                                  else if (sortDirection == "DESC")
                                                                  {
                                                                      order.Add(o => o.ClaimDate).Descending();
                                                                  }
                                                              }

                                                          })
                                                  )
                           .Scrollable()
                                   .Footer(false)
        %>

