﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Billing Batch | Report Center</span>
<%  string pagename = "BillingBatch"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="BillingBatchContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportBillingBatch">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_ClaimType" class="fl">Claim Type</label>
            <div class="fr"><%= Html.ClaimTypes("ClaimType", new { @id = pagename + "_ClaimType"})%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_BatchDate">Date</label>
            <div class="fr"><input type="text" class="date-picker" name="BatchDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_BatchDate" /></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Billing/Content/BillingBatch", Model); %></div>