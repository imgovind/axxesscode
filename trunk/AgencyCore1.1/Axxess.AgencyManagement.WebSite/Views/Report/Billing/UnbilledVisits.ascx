﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Unbilled Visits for Managed Claims Report | Report Center</span>
<% string pagename = "UnbilledVisitsForManagedClaims"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="request" request="UnbilledVisitsForManagedClaimsReport">Request Report</a></li><br />
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr">
                <%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_InsuranceId" class="fl">Insurance</label>
            <div class="fr">
                <%= Html.InsurancesNoneMedicareTraditional("InsuranceId", "", (int)service, false, true, "** Select Insurance **", new { @id = pagename + "_InsuranceId", @class = "insurance nonzero " + service.ToString().ToLowerCase() })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StartDate" class="fl">Date Range</label>
            <div class="fr">
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Today.AddDays(-59).ToZeroFilled() %>"
                    id="<%= pagename %>_StartDate" />
                &#8211;
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Today.ToZeroFilled() %>"
                    id="<%= pagename %>_EndDate" />
            </div>
        </div>
    </div>
</fieldset>
