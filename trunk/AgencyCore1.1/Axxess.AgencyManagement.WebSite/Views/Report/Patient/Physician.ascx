﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Patient By Physician Listing | Report Center</span>
<%  string pagename = "PatientByPhysicians"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="PhysicianPatientsContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
    <li><a class="export" report="ExportPhysicianPatients">Excel Export</a></li>
    <div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_PhysicianId" class="fl">Physician</label>
            <div class="fr"><%= Html.TextBox("PhysicianId", "", new { @id = pagename + "_PhysicianId", @class = "physician-picker required" })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Patient Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId" class="report_input">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active Patients</option>
                    <option value="2">Discharged Patients</option>
                </select>
            </div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Patient/Content/Physician", Model); %></div>