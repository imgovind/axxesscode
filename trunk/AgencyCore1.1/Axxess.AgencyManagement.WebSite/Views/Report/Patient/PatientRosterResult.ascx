﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<h3>Patient Roster</h3>
<ul>
    <li class="PatientResult">
        <span class="PatientPatientID"> Patient ID</span>
        <span class="PatientFirstName">First Name</span>
        <span class="PatientLastName">Last Name</span>
        <span class="PatientGender">Gender</span>
        <span class="PatientMedicareNumber">Policy#</span>
        <span class="PatientDOB">Date of Birth</span>
        <span class="PatientPhone">Phone</span>
        <span class="PatientAddressLine1">Address Line 1</span>
        <span class="PatientAddressLine2">Address Line</span>
        <span class="PatientAddressCity">City</span>
        <span class="PatientAddressStateCode">State</span>
        <span class="PatientAddressZipCode">Zip Code</span>
        <span class="PatientSoC">SoC Date</span>
        <span class="PatientPrimaryDiagnosis">Primary Diagnosis</span>
        <span class="PatientSecondaryDiagnosis">Secondary Diagnosis</span>
        <span class="PatientInsurance">Patient Insurance</span>
        <span class="PhysicianName">Physician Name</span>
        <span class="PhysicianPhone">Physician Phone</span>
        <span class="PhysicianFacsimile">Physician Fax</span>
        <span class="PhysicianPhoneHome">Physician Home Phone</span>
        <span class="PhysicianEmailAddress">Physician Email</span>
    </li>
<%  foreach (var p in Model) { %>
    <li class="PatientResult">
        <span class="PatientPatientID"><%= p.PatientId%></span>
        <span class="PatientFirstName"><%= p.PatientFirstName%></span>
        <span class="PatientLastName"><%= p.PatientLastName%></span>
        <span class="PatientGender"><%= p.PatientGender%></span>
        <span class="PatientMedicareNumber"><%= p.PatientMedicareNumber.IsNotNullOrEmpty() ? p.PatientMedicareNumber : p.PatientInsurance %></span>
        <span class="PatientDOB"><%= p.PatientDOB%></span>
        <span class="PatientPhone"><%= p.PatientPhone%></span>
        <span class="PatientAddressLine1"><%= p.PatientAddressLine1%></span>
        <span class="PatientAddressLine2"><%= p.PatientAddressLine2%></span>
        <span class="PatientAddressCity"><%= p.PatientAddressCity%></span>
        <span class="PatientAddressStateCode"><%= p.PatientAddressStateCode%></span>
        <span class="PatientAddressZipCode"><%= p.PatientAddressZipCode%></span>
        <span class="PatientSoC"><%= p.PatientSoC%></span>
        <span class="PatientPrimaryDiagnosis"><%= p.PatientPrimaryDiagnosis%></span>
        <span class="PatientSecondaryDiagnosis"><%= p.PatientSecondaryDiagnosis%></span>
        <span class="PatientInsurance"><%= p.PatientInsurance%></span>
        <span class="PhysicianName"><%= p.PhysicianName%></span>
        <span class="PhysicianPhone"><%= p.PhysicianPhone%></span>
        <span class="PhysicianFacsimile"><%= p.PhysicianFacsimile%></span>
        <span class="PhysicianPhoneHome"><%= p.PhysicianPhoneHome%></span>
        <span class="PhysicianEmailAddress"><%= p.PhysicianEmailAddress%></span>
    </li>
<%  }%>
</ul>