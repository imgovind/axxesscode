﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<span class="wintitle">Patient By Responsible Case Manager Listing | Report Center</span>
<%  string pagename = "PatientByResponsibleCaseManager"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString().ToGuid() : Guid.Empty; %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="CaseManagerPatientsContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportCaseManagerPatients">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId.ToString(), service, new { @id = pagename + "_BranchId" })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Employee Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId" > 
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Inactive</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_UserId" class="fl">Employee</label>
            <div class="fr"><%= Html.Users("UserId", Guid.Empty.ToString(), "-- Select User --", branchId, (int)UserStatus.Active, service, new { @id = pagename + "_UserId", @class = "user required not-zero" })%></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Patient/Content/PatientByResponsibleCaseManagerContent", Model); %></div>