﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<SurveyCensus>>" %>
<span class="wintitle">Survey Census | Report Center</span>
<%  string pagename = "PatientSurveyCensus"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="SurveyCensusContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportSurveyCensus">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Discharged</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_InsuranceId" class="fl">Insurance</label>
            <div class="fr"><%= Html.Insurances("InsuranceId", "0", (int)service, true, true, "All", new { @id = pagename + "_InsuranceId", @class = "insurance " + service.ToString().ToLowerCase() }) %></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid clear"><% Html.RenderPartial("Patient/Content/SurveyCensusContent", Model); %></div>