﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">CAHPS Report | <%= Current.AgencyName %></span>
<%  string pagename = "Cahps"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service, new { @class = "report_input service", @id = pagename + "_Service" }) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), service, new { @id = pagename + "_BranchId " })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_SampleMonth" class="fl">Sample Month</label>
            <div class="fr"><%= Html.CalendarMonths("sampleMonth", DateTime.Now.Month, false, new { @id = pagename + "_SampleMonth" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_SampleYear" class="fl">Sample Year</label>
            <div class="fr"><%= Html.SampleYear("sampleYear", new { @id = pagename + "_SampleYear" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_PaymentSource" class="fl">Payment Source</label>
            <div class="fr">
                <select id="<%= pagename %>_PaymentSource" name="paymentSources" multiple="multiple" class="multiple-payment required">
					<option value="0">None; No Charge</option>
					<option value="1" selected="selected">Medicare (Traditional)</option>
					<option value="2">Medicare (HMO/Managed Care)</option>
					<option value="3">Medicaid (Traditional)</option>
					<option value="4">Medicaid (HMO/Managed Care)</option>
					<option value="5">Workers' Compensation</option>
					<option value="6">Title Programs (Title III, V, XX)</option>
					<option value="7">Other Government (CHAMPUS, VA, etc)</option>
					<option value="8">Private Insurance</option>
					<option value="9">Private HMO/Managed Care</option>
					<option value="10">Self-pay</option>
					<option value="11">Unknown</option>
					<option value="12">Other</option>
                </select>
            </div>
        </div>
    </div>
</fieldset>
<ul class="buttons ac">
    <li><a class="export" report="ExportCahps">Excel Export</a></li>
    <li><a class="report-home">Reports Home</a></li>
</ul>
