﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientRoster"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
        {
            columns.Bound(r => r.PatientId).Title("MRN").Width(100);
            columns.Bound(r => r.PatientDisplayName).Title("Patient");
            columns.Bound(r => r.PatientInsuranceName).Title("Insurance").Sortable(false);
            columns.Bound(r => r.PatientPolicyNumber).Title("Policy #").Sortable(false).Width(85);
            columns.Bound(r => r.PatientSoC).Title("SOC").Width(85);
            columns.Bound(r => r.PatientDischargeDate).Title("D/C Date").Width(75);
            columns.Bound(r => r.AddressFull).Title("Address").Sortable(false);
            columns.Bound(r => r.PatientDOB).Title("DOB").Width(75);
            columns.Bound(r => r.PatientPhone).Title("Home Phone").Sortable(false).Width(110);
            columns.Bound(r => r.PatientGender).Title("Gender").Sortable(false).Width(60);
            columns.Bound(r => r.Triage).Title("Triage").Width(60);
        })
       // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, StatusId = 1, InsuranceId = 0 }))
                 .Sortable(sorting =>
                sorting.SortMode(GridSortMode.SingleColumn)
                    .OrderBy(order =>
                    {
                        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                        if (sortName == "PatientId")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientId).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientId).Descending();
                            }
                        }
                        else if (sortName == "PatientDisplayName")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientDisplayName).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientDisplayName).Descending();
                            }
                        }
                        else if (sortName == "PatientSoC")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientSoC).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientSoC).Descending();
                            }
                        }
                        else if (sortName == "PatientDischargeDate")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientDischargeDate).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientDischargeDate).Descending();
                            }
                        }
                        else if (sortName == "PatientDOB")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientDOB).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientDOB).Descending();
                            }
                        }
                        else if (sortName == "Triage")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.Triage).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.Triage).Descending();
                            }
                        }
                    })
            )
            .Scrollable()
            .Footer(false)
%>
