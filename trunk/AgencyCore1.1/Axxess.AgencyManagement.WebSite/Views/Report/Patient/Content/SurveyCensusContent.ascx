﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<SurveyCensus>>" %>
<% string pagename = "PatientSurveyCensus"; %>
 <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
 {
 columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(2);
 columns.Bound(p => p.PatientDisplayName).Title("Patient").Width(5);
 columns.Bound(p => p.InsuranceName).Title("Insurance").Width(4).Sortable(false);
 columns.Bound(p => p.PolicyNumber).Title("Policy #").Width(3).Sortable(false);
 columns.Bound(p => p.DOB).Title("DOB").Width(3);
 columns.Bound(p => p.SOC).Title("SOC Date").Width(3);
 columns.Bound(p => p.CertPeriod).Title("Cert Period(Recent)").Width(4).Sortable(false);
 columns.Bound(p => p.PrimaryDiagnosis).Title("Primary Diagnosis").Sortable(false).Width(3);
 columns.Bound(p => p.SecondaryDiagnosis).Title("Secondary Diagnosis").Sortable(false).Width(3);
 columns.Bound(p => p.Triage).Title("Triage").Sortable(false).Width(2);
 columns.Bound(p => p.EvacuationZone).Title("Evacuation Zone").Sortable(false).Width(3);
 columns.Bound(p => p.Discipline).Title("Disciplines").Width(3).Sortable(false);
 })
 // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, StatusId = 1 }))
         .Sortable(sorting =>
                     sorting.SortMode(GridSortMode.SingleColumn)
                         .OrderBy(order =>
                         {
                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                             if (sortName == "PatientIdNumber")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.PatientIdNumber).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.PatientIdNumber).Descending();
                                 }
                             }
                             else if (sortName == "PatientDisplayName")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.PatientDisplayName).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.PatientDisplayName).Descending();
                                 }
                             }
                             else if (sortName == "DOB")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.DOB).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.DOB).Descending();
                                 }
                             }
                             else if (sortName == "SOC")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.SOC).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.SOC).Descending();
                                 }
                             }
                         })
                 )
     .Scrollable()
      .Footer(false)
%>

