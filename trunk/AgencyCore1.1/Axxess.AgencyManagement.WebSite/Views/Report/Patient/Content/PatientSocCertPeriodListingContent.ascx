﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSocCertPeriod>>" %>
<% string pagename = "PatientSocCertPeriodListing"; %>
<% =Html.Telerik().Grid(Model)
         .Name(pagename + "Grid")        
         .Columns(columns =>
     {
     columns.Bound(p => p.PatientPatientID).Title("MRN").Width(90);
     columns.Bound(p => p.PatientLastName).Title("Last Name");
     columns.Bound(p => p.PatientFirstName).Title("First Name");
     columns.Bound(p => p.PatientMiddleInitial).Title("Middle Initial").Width(100);
     columns.Bound(p => p.PatientSoC).Title("SOC Date").Width(80);
     columns.Bound(p => p.SocCertPeriod).Title("SOC Cert Period").Width(155);
     columns.Bound(p => p.PhysicianName);
     columns.Bound(p => p.respEmp).Title("Employee");
     })
        .Sortable(sorting =>
             sorting.SortMode(GridSortMode.SingleColumn)
                 .OrderBy(order =>
                 {
                     var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                     var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                     if (sortName == "PatientPatientID")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.PatientPatientID).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.PatientPatientID).Descending();
                         }
                     }
                     else if (sortName == "PatientLastName")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.PatientLastName).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.PatientLastName).Descending();
                         }
                     }
                     else if (sortName == "PatientFirstName")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.PatientFirstName).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.PatientFirstName).Descending();
                         }
                     }
                     else if (sortName == "PatientSoC")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.PatientSoC).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.PatientSoC).Descending();
                         }
                     }
                     else if (sortName == "PhysicianName")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.PhysicianName).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.PhysicianName).Descending();
                         }
                     }
                     else if (sortName == "respEmp")
                     {
                         if (sortDirection == "ASC")
                         {
                             order.Add(o => o.respEmp).Ascending();
                         }
                         else if (sortDirection == "DESC")
                         {
                             order.Add(o => o.respEmp).Descending();
                         }
                     }
                 })
            )
   .Scrollable()
   .Footer(false)%>
   