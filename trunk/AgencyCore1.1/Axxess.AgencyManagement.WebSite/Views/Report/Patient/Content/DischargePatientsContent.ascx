﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<DischargePatient>>" %>
<% string pagename = "DischargePatients"; %>
<% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90);
         columns.Bound(p => p.LastName).Title("Last Name");
         columns.Bound(p => p.FirstName).Title("First Name");
         columns.Bound(p => p.MiddleInitial).Title("Middle Initial").Width(100);
         columns.Bound(p => p.StartofCareDate).Format("{0:MM/dd/yyyy}").Title("SOC Date").Width(100);
         columns.Bound(p => p.DischargeDate).Format("{0:MM/dd/yyyy}").Title("Discharge Date").Width(100);
         columns.Bound(p => p.DischargeReason).Sortable(false).Title("Reason");
         })
                                    .Sortable(sorting =>
                                                                         sorting.SortMode(GridSortMode.SingleColumn)
                                                                             .OrderBy(order =>
                                                                             {
                                                                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                                 if (sortName == "PatientIdNumber")
                                                                                 {
                                                                                     if (sortDirection == "ASC")
                                                                                     {
                                                                                         order.Add(o => o.PatientIdNumber).Ascending();
                                                                                     }
                                                                                     else if (sortDirection == "DESC")
                                                                                     {
                                                                                         order.Add(o => o.PatientIdNumber).Descending();
                                                                                     }
                                                                                 }
                                                                                 else if (sortName == "LastName")
                                                                                 {
                                                                                     if (sortDirection == "ASC")
                                                                                     {
                                                                                         order.Add(o => o.LastName).Ascending();
                                                                                     }
                                                                                     else if (sortDirection == "DESC")
                                                                                     {
                                                                                         order.Add(o => o.LastName).Descending();
                                                                                     }
                                                                                 }
                                                                                 else if (sortName == "FirstName")
                                                                                 {
                                                                                     if (sortDirection == "ASC")
                                                                                     {
                                                                                         order.Add(o => o.FirstName).Ascending();
                                                                                     }
                                                                                     else if (sortDirection == "DESC")
                                                                                     {
                                                                                         order.Add(o => o.FirstName).Descending();
                                                                                     }
                                                                                 }
                                                                                 else if (sortName == "StartofCareDate")
                                                                                 {
                                                                                     if (sortDirection == "ASC")
                                                                                     {
                                                                                         order.Add(o => o.StartofCareDate).Ascending();
                                                                                     }
                                                                                     else if (sortDirection == "DESC")
                                                                                     {
                                                                                         order.Add(o => o.StartofCareDate).Descending();
                                                                                     }
                                                                                 }
                                                                                 else if (sortName == "DischargeDate")
                                                                                 {
                                                                                     if (sortDirection == "ASC")
                                                                                     {
                                                                                         order.Add(o => o.DischargeDate).Ascending();
                                                                                     }
                                                                                     else if (sortDirection == "DESC")
                                                                                     {
                                                                                         order.Add(o => o.DischargeDate).Descending();
                                                                                     }
                                                                                 }

                                                                             })).Scrollable().Footer(false)%>
   
