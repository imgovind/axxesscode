﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EmergencyContactInfo>>" %>
<% string pagename = "PatientEmergencyList"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
   {
       columns.Bound(p => p.PatientName).Title("Patient");
       columns.Bound(p => p.Triage).Title("Triage").Width(50);
       columns.Bound(p => p.ContactName).Title("Contact");
       columns.Bound(p => p.ContactRelation).Title("Relationship");
       columns.Bound(p => p.ContactPhoneHome).Title("Contact Phone").Width(110);
       columns.Bound(p => p.ContactEmailAddress).Title("Contact E-mail");

   })
    //.DataBinding(dataBinding => dataBinding.Ajax().Select("PatientEmergencyList", "Report", new { StatusId = 1, BranchId = Guid.Empty }))
           .Sortable(sorting =>
                          sorting.SortMode(GridSortMode.SingleColumn)
                              .OrderBy(order =>
                              {
                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                  if (sortName == "PatientName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientName).Descending();
                                      }
                                  }
                                  else if (sortName == "ContactName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.ContactName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.ContactName).Descending();
                                      }
                                  }
                              })
                      )
     .Scrollable()
     .Footer(false)
%>
