﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<Birthday>>" %>
<span class="wintitle">Patient Birthdays | Report Center</span>
<%  string pagename = "PatientBirthdayList"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString();%>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="BirthdaysContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportBirthdays">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, service, new { @id = pagename + "_BranchId" })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_Month" class="fl">Month</label>
            <div class="fr"><%= Html.CalendarMonths("Month", DateTime.Now.Month, true, new { @id = pagename + "_Month", @class = "required not-zero" })%></div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Patient/Content/BirthdayListContent", Model); %></div>