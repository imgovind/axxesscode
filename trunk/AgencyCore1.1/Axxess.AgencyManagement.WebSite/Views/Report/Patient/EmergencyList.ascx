﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EmergencyContactInfo>>" %>
<span class="wintitle">Patient Emergency Contact | Report Center</span>
<%  string pagename = "PatientEmergencyList"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString().ToGuid() : Guid.Empty; %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="EmergenciesContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportEmergencies">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service, new { @id = pagename + "_Service" })%>
    <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" })%>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]), new { @id = pagename + "_SortParams" })%>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId.ToString(),service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId" class="report_input">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Discharged</option>
                </select>
            </div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Patient/Content/EmergencyListContent", Model); %></div>