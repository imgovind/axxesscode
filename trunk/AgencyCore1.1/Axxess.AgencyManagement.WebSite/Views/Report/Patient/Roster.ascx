﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<span class="wintitle">Patient Roster | Report Center</span>
<%  string pagename = "PatientRoster"; %>
<%  var service = ViewData.GetEnum<AgencyServices>("Service"); %>
<%  var branchId = ViewData.GetOrDefault("BranchId", Guid.Empty).ToString();%>
<ul class="buttons fr ac">
	<li><a class="grid-refresh" report="PatientRosterContent">Refresh</a></li><br/>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportPatientRoster">Excel Export</a></li><br/>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", (int)service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId", branchId, (int)service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Discharged</option>
                    <option value="3">Pending</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_InsuranceId" class="fl">Insurance</label>
            <div class="fr"><%= Html.Insurances("InsuranceId", "0", (int)service, true, true, "All", new { @id = pagename + "_InsuranceId", @class = "insurance " + service.ToString().ToLowerCase() }) %></div>
        </div>
        <div class="narrower row">
            <ul class="checkgroup one-wide">
                <li class="option">
                    <div class="wrapper">
                        <input class="report_input" value="true" type="checkbox" name="IsRangeApplicable" id="<%= pagename %>_ActiveRangeCheckBox" />
                        <label for="<%= pagename %>_ActiveRangeCheckBox">Active Range</label>
                    </div>
                    <div class="more ac">
                        <input type="text" class="date-picker report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                        &#8211;
                        <input type="text" class="date-picker report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
                    </div>
                </li>
            </ul>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Patient/Content/RosterContent", Model); %></div>
