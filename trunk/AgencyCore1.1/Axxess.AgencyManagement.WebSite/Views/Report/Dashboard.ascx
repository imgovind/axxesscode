﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<int, List<ReportDescription>>>" %>
<div class="fl">
<%  var patientReports = Model.ContainsKey((int)ReportCategory.Patient) ? Model[(int)ReportCategory.Patient] : new List<ReportDescription>(); %>
<%  if (patientReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Patient.GetDescription()%></h5>
        </div>
        <ul>
    <%  foreach (var report in patientReports) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>"> 
			    <a class="report-link"  href= "<%= report.Link %>" description="<%= report.Description %>">
				    <span class="tooltip">
					    <p><strong>Description:</strong> <%= report.Description %></p><br/>
						<p><strong>Parameters:</strong> <%= report.Parameters %></p><br/>
						<p><strong>Report Includes:</strong> <%= report.InformationInclude %></p><br/>
		<%  if (report.Comments.IsNotNullOrEmpty()) { %>
		                <p><strong>Comments:</strong> <%= report.Comments %></p><br/>
		<%  } %>
					</span>
					<%= report.Title %>
                </a>
            </li>
        <%  } %>
        </ul>
    </div>  
<%  } %>
<%  var clinicalReports = Model.ContainsKey((int)ReportCategory.Clinical) ? Model[(int)ReportCategory.Clinical] : new List<ReportDescription>(); %>
<%  if (clinicalReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Clinical.GetDescription()%></h5>
        </div>
        <ul> 
    <%  foreach (var report in clinicalReports) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>"> 
			    <a class="<%= report.IsOnclick ? "onclick-link" : "report-link" %>" <%= !report.IsOnclick ? "href=\"" + report.Link + "\"" : string.Empty %> description="<%= report.Description %>" onclick="<%= report.IsOnclick ? report.Link : "return false;" %>">
				    <span class="tooltip">
					    <p><strong>Description:</strong> <%= report.Description %></p><br/>
						<p><strong>Parameters:</strong> <%= report.Parameters %></p><br/>
						<p><strong>Report Includes:</strong> <%= report.InformationInclude %></p><br/>
		<%  if (report.Comments.IsNotNullOrEmpty()) { %>
		                <p><strong>Comments:</strong> <%= report.Comments %></p><br/>
		<%  } %>
					</span>
					<%= report.Title %>
				</a>
			</li> 
    <%  } %>
        </ul>
    </div>
<%  } %>
<%  var payrollDetailSummaryReports = Model.ContainsKey((int)ReportCategory.PayrollDetailSummary) ? Model[(int)ReportCategory.PayrollDetailSummary] : new List<ReportDescription>(); %>
<%  if (payrollDetailSummaryReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%= ReportCategory.PayrollDetailSummary.GetDescription() %></h5>
        </div>
        <ul> 
    <%  foreach (var report in payrollDetailSummaryReports) { %>
            <li>
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description:</strong> <%= report.Description %></p><br/>
                        <p><strong>Parameters:</strong> <%= report.Parameters %></p><br/>
                        <p><strong>Report Includes:</strong> <%= report.InformationInclude %></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments:</strong> <%= report.Comments %></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title %>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %> 
</div> 
<div class="fl">
<%  var financialReports = Model.ContainsKey((int)ReportCategory.BillingFinancial) ? Model[(int)ReportCategory.BillingFinancial] : new List<ReportDescription>(); %>
<%  if (financialReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.BillingFinancial.GetDescription()%></h5>
        </div>
        <ul> 
    <%  foreach (var report in financialReports) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>">
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description:</strong> <%= report.Description %></p><br/>
                        <p><strong>Parameters:</strong> <%= report.Parameters %></p><br/>
                        <p><strong>Report Includes:</strong> <%= report.InformationInclude %></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments:</strong> <%= report.Comments %></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title %>
                </a>
            </li>
    <%  } %>
        </ul>
    </div>
<%  } %>   
<%  var scheduleReports = Model.ContainsKey((int)ReportCategory.Schedule) ? Model[(int)ReportCategory.Schedule] : new List<ReportDescription>(); %>
<%  if (scheduleReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Schedule.GetDescription()%></h5>
        </div>
        <ul>
    <%  foreach (var report in scheduleReports) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>">
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description: </strong> <%= report.Description %></p><br/>
                        <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                        <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments: </strong> <%= report.Comments %></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title %>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %>   
</div> 
<div class="fl">
<%  var statisticalReports = Model.ContainsKey((int)ReportCategory.Statistical) ? Model[(int)ReportCategory.Statistical] : new List<ReportDescription>(); %>
<%  if (statisticalReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Statistical.GetDescription()%></h5>
        </div>
        <ul>
    <%  foreach (var report in statisticalReports) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>">
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description: </strong> <%= report.Description %></p><br/>
                        <p><strong>Parameters: </strong> <%= report.Parameters %></p><br/>
                        <p><strong>Report Includes: </strong> <%= report.InformationInclude %></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments: </strong> <%= report.Comments %></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title %>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %>
<%  var surveyReportMissouri = Model.ContainsKey((int)ReportCategory.AnnualSurveyReportMissouri) ? Model[(int)ReportCategory.AnnualSurveyReportMissouri] : new List<ReportDescription>(); %>
<%  if (surveyReportMissouri.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.AnnualSurveyReportMissouri.GetDescription()%></h5>
        </div>
        <ul> 
    <%  foreach (var report in surveyReportMissouri) { %>
            <li export="<%= report.ExportService %>" view="<%= report.ViewService %>">
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description:</strong> <%= report.Description%></p><br/>
                        <p><strong>Parameters:</strong> <%= report.Parameters%></p><br/>
                        <p><strong>Report Includes:</strong> <%= report.InformationInclude%></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments:</strong> <%= report.Comments%></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title %>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %>
<%  var annualUtilizationReport = Model.ContainsKey((int)ReportCategory.AnnualUtilizationReportCalifornia) ? Model[(int)ReportCategory.AnnualUtilizationReportCalifornia] : new List<ReportDescription>(); %>
<%  if (annualUtilizationReport.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.AnnualUtilizationReportCalifornia.GetDescription()%></h5>
        </div>
        <ul> 
    <%  foreach (var report in annualUtilizationReport) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>">
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description:</strong> <%= report.Description%></p><br/>
                        <p><strong>Parameters:</strong> <%= report.Parameters%></p><br/>
                        <p><strong>Report Includes:</strong> <%= report.InformationInclude%></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments:</strong> <%= report.Comments%></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title%>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %>
<%  var accessEmployeeReports = Model.ContainsKey((int)ReportCategory.Employee) ? Model[(int)ReportCategory.Employee] : new List<ReportDescription>(); %>
<%  if (accessEmployeeReports.IsNotNullOrEmpty()) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Employee.GetDescription()%></h5>
        </div>
        <ul>
    <%  foreach (var report in accessEmployeeReports) { %>
            <li export="<%=report.ExportService %>" view="<%=report.ViewService %>">
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description:</strong> <%= report.Description %></p><br/>
                        <p><strong>Parameters:</strong> <%= report.Parameters %></p><br/>
                        <p><strong>Report Includes:</strong> <%= report.InformationInclude %></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments:</strong> <%= report.Comments %></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title %>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %> 
<%--
<%  var patientsReport2 = Model.ContainsKey((int)ReportCategory.Testing) ? Model[(int)ReportCategory.Testing] : new List<ReportDescription>(); %>
<%  if (patientsReport2.Count > 0) { %>
    <div class="reports">
        <div class="reports-head">
            <h5><%=ReportCategory.Testing.GetDescription()%></h5>
        </div>
        <ul> 
    <%  foreach (var report in patientsReport2) { %>
            <li>
                <a class="report-link" href="<%= report.Link %>" description="<%= report.Description %>">
                    <span class="tooltip">
                        <p><strong>Description:</strong> <%= report.Description %></p><br/>
                        <p><strong>Parameters:</strong> <%= report.Parameters %></p><br/>
                        <p><strong>Report Includes:</strong> <%= report.InformationInclude %></p><br/>
        <%  if (report.Comments.IsNotNullOrEmpty()) { %>
                        <p><strong>Comments:</strong> <%= report.Comments%></p><br/>
        <%  } %>
                    </span>
                    <%= report.Title%>
                </a>
            </li> 
    <%  } %>
        </ul>
    </div>
<%  } %> 
--%>
</div>