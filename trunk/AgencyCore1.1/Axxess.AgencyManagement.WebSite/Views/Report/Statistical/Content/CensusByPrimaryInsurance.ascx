﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<%  string pagename = "CensusByPrimaryInsurance"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns => {
        columns.Bound(r => r.PatientDisplayName).Title("Patient");
        columns.Bound(r => r.PatientAddressLine1).Sortable(false).Title("Address");
        columns.Bound(r => r.PatientAddressCity).Sortable(false).Title("City");
        columns.Bound(r => r.PatientAddressStateCode).Sortable(false).Title("State").Width(50);
        columns.Bound(r => r.PatientAddressZipCode).Sortable(false).Title("Zip Code").Width(60);
        columns.Bound(r => r.PatientPhone).Sortable(false).Title("Home Phone").Width(110);
        columns.Bound(r => r.PatientGender).Sortable(false).Title("Gender").Width(60);
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "PatientDisplayName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientDisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientDisplayName).Descending();
        }
    })).Scrollable().Footer(false) %>