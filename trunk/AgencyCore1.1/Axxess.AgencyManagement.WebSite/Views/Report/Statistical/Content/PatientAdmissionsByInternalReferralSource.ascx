﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientAdmission>>" %>
<%  string pagename = "StatisticalPatientAdmissionsByInternalReferralSource"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns => {
        columns.Bound(s => s.DisplayName).Title("Patient");
        columns.Bound(s => s.PatientId).Title("MRN");
        columns.Bound(s => s.PhoneHome).Title("Phone").Width(150).Sortable(false);
        columns.Bound(s => s.Admit).Title("Admit").Width(50);
        columns.Bound(s => s.StartOfCareDateFormatted).Title("SOC").Width(100);
        columns.Bound(s => s.DischargedDate).Template(t => (t.Status == (int)PatientStatus.Pending || t.Status == (int)PatientStatus.NonAdmission) ? string.Empty : t.DischargedDate.ToZeroFilled()).Title("D/C").Width(100);
        columns.Bound(s => s.InsuranceName).Title("Insurance").Sortable(false);
        columns.Bound(s => s.PhysicianName).Title("Primary Physician").Sortable(false);
    }).Groupable(groups => groups.Groups(group => group.Add(g => g.InternalReferralName)).Enabled(false)).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "DisplayName") {
            if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
        } else if (sortName == "PatientId") {
            if (sortDirection == "ASC") order.Add(o => o.PatientId).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientId).Descending();
        } else if (sortName == "Admit") {
            if (sortDirection == "ASC") order.Add(o => o.Admit).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.Admit).Descending();
        } else if (sortName == "StartOfCareDateFormatted") {
            if (sortDirection == "ASC") order.Add(o => o.StartOfCareDateFormatted).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.StartOfCareDateFormatted).Descending();
        } else if (sortName == "DischargedDate") {
            if (sortDirection == "ASC") order.Add(o => o.DischargedDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.DischargedDate).Descending();
        }
    })).Scrollable().Footer(false) %>