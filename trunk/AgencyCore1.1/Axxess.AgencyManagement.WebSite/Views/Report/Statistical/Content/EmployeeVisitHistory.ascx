﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "StatisticalEmployeeVisitHistory"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.TaskName).Title("Task").Sortable(false);
               columns.Bound(s => s.StatusName).Title("Status").Sortable(false);
               columns.Bound(s => s.ScheduleDate).Title("Schedule Date");
               columns.Bound(s => s.VisitDate).Title("Visit Date");
               columns.Bound(s => s.PatientName).Title("Patient");
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { UserId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                                       .Sortable(sorting =>
                                                 sorting.SortMode(GridSortMode.SingleColumn)
                                                     .OrderBy(order =>
                                                     {
                                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                         if (sortName == "ScheduleDate")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.ScheduleDate).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.ScheduleDate).Descending();
                                                             }
                                                         }
                                                         else if (sortName == "VisitDate")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.VisitDate).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.VisitDate).Descending();
                                                             }
                                                         }
                                                         else if (sortName == "PatientName")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.PatientName).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.PatientName).Descending();
                                                             }
                                                         }
                                                     })
                                             )
                                   .Scrollable()
                                           .Footer(false)
        %>
   

