﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeTherapyException>>" %>
<span class="wintitle">13th And 19th Visit Exception | Report Center</span>
<%  string pagename = "ClinicalThirteenAndNineteenVisitException"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="ThirteenAndNineteenVisitExceptionContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportThirteenAndNineteenVisitException">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId",  branchId, service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId" class="report_input">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Discharged</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_PatientId" class="fl">Patient</label>
            <div class="fr"><%= Html.Patients(service, "PatientId", branchId.ToGuid(), Guid.Empty.ToString(), 1, "All", new { @id = pagename + "_PatientId", @class = "patient all" })%></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StartDate" class="fl">Date Range</label>
            <div class="fr">
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                &#8211;
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
            </div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Clinical/Content/ThirteenAndNineteenVisitException", Model); %></div>