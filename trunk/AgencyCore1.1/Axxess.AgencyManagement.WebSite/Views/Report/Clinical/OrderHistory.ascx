﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<PhysicianOrder>>" %>
<span class="wintitle">Order History | Report Center</span>
<%  string pagename = "ClinicalPhysicianOrderHistory"; %>
<%  var service = ViewData.ContainsKey("Service") && ViewData["Service"].ToString().IsInteger() ? ViewData["Service"].ToString().ToInteger() : 0; %>
<%  var branchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<ul class="buttons fr ac">
    <li><a class="grid-refresh" report="PhysicianOrderHistoryContent">Refresh</a></li>
    <div class="clr"></div>
<%  if ("1" == ViewData.GetOrDefault("IsUserCanExport").ToString()) { %>
	<li><a class="export" report="ExportPhysicianOrderHistory">Excel Export</a></li>
	<div class="clr"></div>
<%  } %>
    <li><a class="report-home">Reports Home</a></li>
</ul>
<fieldset class="report-controls">
    <%= Html.Hidden("Service", service) %>
    <%= Html.Hidden("PageName", pagename) %>
    <%= Html.Hidden("SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"])) %>
    <div class="wide-column">
        <div class="narrower row">
            <label for="<%= pagename %>_BranchId" class="fl">Branch</label>
            <div class="fr"><%= Html.BranchList("BranchId",  branchId, service, new { @id = pagename + "_BranchId" }) %></div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StatusId" class="fl">Status</label>
            <div class="fr">
                <select id="<%= pagename %>_StatusId" name="StatusId">
                    <option value="000" selected="selected">All</option>
                    <option value="100">Not Yet Started</option>
                    <option value="105">Not Yet Due</option>
                    <option value="110">Saved</option>
                    <option value="115">Submitted (Pending QA Review)</option>
                    <option value="120">Returned For Review</option>
                    <option value="125">To Be Sent To Physician</option>
                    <option value="130">Sent To Physician (Manually)</option>
                    <option value="135">Returned W/ Physician Signature</option>
                    <option value="140">Reopened</option>
                    <option value="145">Sent To Physician (Electronically)</option>
                    <option value="150">Saved By Physician</option>
                </select>
            </div>
        </div>
        <div class="narrower row">
            <label for="<%= pagename %>_StartDate" class="fl">Date Range</label>
            <div class="fr">
                <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
                &#8211;
                <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
            </div>
        </div>
    </div>
</fieldset>
<div id="<%= pagename %>GridContainer" class="report-grid"><% Html.RenderPartial("Clinical/Content/OrderHistory", Model); %></div>