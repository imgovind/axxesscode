﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<Order>>" %>
<% string pagename = "ClinicalPlanOfCareHistory"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(o => o.Number).Title("Order").Width(70);
               columns.Bound(o => o.PatientName).Title("Patient");
               columns.Bound(o => o.PhysicianName).Title("Physician");
               columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100);
			   columns.Bound(o => o.SendDate).ClientTemplate("<#= U.FormatGridDate(SendDate) #>").Title("Sent Date").Width(100);
			   columns.Bound(o => o.ReceivedDate).ClientTemplate("<#= U.FormatGridDate(ReceivedDate) #>").Title("Received Date").Width(120);
           })
                  // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 000, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                                      .Sortable(sorting =>
                                                  sorting.SortMode(GridSortMode.SingleColumn)
                                                      .OrderBy(order =>
                                                      {
                                                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                          if (sortName == "Number")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.Number).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.Number).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "PatientName")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.PatientName).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.PatientName).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "PhysicianName")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.PhysicianName).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.PhysicianName).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "CreatedDate")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.CreatedDate).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.CreatedDate).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "SendDate")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.SendDate).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.SendDate).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "ReceivedDate")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.ReceivedDate).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.ReceivedDate).Descending();
                                                              }

                                                          }

                                                      })
                                              )
                                   .Scrollable()
                                           .Footer(false)
        %>


