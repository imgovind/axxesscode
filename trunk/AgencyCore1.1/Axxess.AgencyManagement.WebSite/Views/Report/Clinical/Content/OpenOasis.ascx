﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IList<OpenOasis>>" %>
<% string pagename = "ClinicalOpenOasis"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(e => e.PatientIdNumber).Title("MRN").Width(90);
               columns.Bound(e => e.PatientName).Title("Patient");
               columns.Bound(e => e.AssessmentName).Sortable(false).Title("Assessment");
               columns.Bound(e => e.Date).Width(80);
               columns.Bound(e => e.Status).Sortable(false);
               columns.Bound(e => e.CurrentlyAssigned).Title("Employee");
           })
.Sortable(sorting =>
                          sorting.SortMode(GridSortMode.SingleColumn)
                              .OrderBy(order =>
                              {
                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                  if (sortName == "PatientIdNumber")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientIdNumber).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientIdNumber).Descending();
                                      }

                                  }
                                  else if (sortName == "PatientName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientName).Descending();
                                      }

                                  }
                                  else if (sortName == "Date")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.Date).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.Date).Descending();
                                      }

                                  }
                                  else if (sortName == "CurrentlyAssigned")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.CurrentlyAssigned).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.CurrentlyAssigned).Descending();
                                      }

                                  }
                              })
                      )
                   .Scrollable()
                           .Footer(false)
        %>
 