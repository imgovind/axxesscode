﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReportViewData>" %>
<div class="wrapper main">
    <div class="report-output"></div>
    <div class="report-home">
<%  if (!Model.AvailableService.IsAlone()) { %>
        <fieldset class="grid-controls ac">
            <div class="filter"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, "Report", new { @class = "service input", @id = "Report_Service" })%></div>
        </fieldset>
<%  } else { %>
        <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, "Report", new { @class = "service input", @id = "Report_Service" })%>
<%  } %>
        <% Html.RenderPartial("Dashboard", Model.Reports); %>
    </div>
</div>