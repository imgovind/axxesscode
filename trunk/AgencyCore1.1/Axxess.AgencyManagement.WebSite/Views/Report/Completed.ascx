﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Reports | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
	<fieldset class="grid-controls ac">
		<div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<ReportLite>().Name("List_CompletedReports").Columns(columns => {
            columns.Bound(r => r.Name).Encoded(false).Title("Name");
            columns.Bound(r => r.Format).Title("Format").Width(80);
            columns.Bound(r => r.Status).Title("Status").Width(90);
            columns.Bound(r => r.UserName).Title("Requested By");
			columns.Bound(r => r.Created).Title("Started").Width(175);
            columns.Bound(r => r.Completed).Title("Completed").Width(175);
            columns.Bound(r => r.Id).ClientTemplate("<a class=\"link\" onclick=\"Report.Delete('<#=Id#>')\">Delete</a>").Title("Action").Width(65);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("CompletedList", "Report")).EnableCustomBinding(true).Scrollable(scrolling => scrolling.Enabled(true)).Pageable(pager => pager.PageSize(15)) %>
</div>