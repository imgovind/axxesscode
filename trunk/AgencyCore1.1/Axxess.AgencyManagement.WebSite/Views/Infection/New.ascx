﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceAndGuidViewData>" %>
<span class="wintitle">New Infection Log | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  if (Model.IsUserCanAdd) { %>
    <%  using (Html.BeginForm("Add", "Infection", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newInfectionReportForm", @class = "mainform" })){ %>
    <%= Html.Hidden("ServiceId", (int)Model.Service, new { @id = "New_Infection_ServiceId", @disabled = "disabled" })%>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Infection_PatientId" class="fl">Patient</label>
                <div class="fr"><%= Html.PatientsFilteredByService((int)Model.Service, "PatientId", Model.Id.ToString(), new { @id = "New_Infection_PatientId", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <%= Html.CarePeriodLabel((int)Model.Service, new { @for = "New_Infection_EpisodeList", @class = "fl" })%>
                <div class="fr"><%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.Id, Guid.Empty.ToString(), new { @id = "New_Infection_EpisodeList", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="New_Infection_InfectionDate" class="fl">Date of Infection</label>
                <div class="fr"><input type="text" class="date-picker required" name="InfectionDate" id="New_Infection_InfectionDate" /></div>
            </div>
            <div class="row">
                <label for="New_Infection_PhysicianId" class="fl">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("PhysicianId", "", new { @id = "New_Infection_PhysicianId", @class = "physician-picker" })%><br />
					<% if(Model.IsUserCanAddPhysicain) { %>
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                    <% } %>
                </div>
            </div>                
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Infection_TreatmentPrescribed" class="fl">Treatment Prescribed?</label>
                <div class="fr">
                    <%= Html.DropDownList("TreatmentPrescribed", new List<SelectListItem> {
                            new SelectListItem() { Text = "Yes", Value = "Yes" },
                            new SelectListItem() { Text = "No", Value = "No" },
                            new SelectListItem() { Text = "N/A", Value = "NA" }
                        }, new { @id = "New_Infection_TreatmentPrescribed" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_Infection_MDNotified" class="fl">M.D. Notified?</label>
                <div class="fr">
                    <%= Html.DropDownList("MDNotified", new List<SelectListItem>{
                            new SelectListItem (){ Text = "Yes", Value = "Yes" },
                            new SelectListItem () { Text = "No", Value = "No" },
                            new SelectListItem (){ Text = "N/A", Value = "NA" }
                        }, new { @id = "New_Infection_MDNotified" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_Infection_NewOrdersCreated" class="fl">New Orders?</label>
                <div class="fr">
                    <%= Html.DropDownList("NewOrdersCreated", new List<SelectListItem>{
                            new SelectListItem (){ Text = "Yes", Value = "Yes" },
                            new SelectListItem (){ Text = "No", Value = "No" },
                            new SelectListItem (){ Text = "N/A", Value = "NA" }
                        }, new { @id = "New_Infection_NewOrdersCreated" })%><br />
                    <div class="ancillary-button"><a class="new-order-button">New Order</a></div>
                </div>
            </div>
            <div class="diagnosises hidden">
	            <%= Html.PrimaryDiagnosisRow((int)Model.Service, string.Empty, string.Empty, new { @class = "primary-diagnosis" })%>
				<%= Html.SecondaryDiagnosisRow((int)Model.Service, string.Empty, string.Empty, new { @class = "secondary-diagnosis" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Type of Infection</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup six-wide">
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Gastrointestinal", false, "Gastrointestinal")%>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Respiratory", false, "Respiratory")%>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Skin", false, "Skin")%>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Wound", false, "Wound")%>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Urinary", false, "Urinary")%>
                    <li class="option">
                        <div class="wrapper">
                            <input id="New_Infection_InfectionType6" type="checkbox" value="Other" name="InfectionTypeArray" />
                            <label for="New_Infection_InfectionType6">Other</label>
                        </div>
                        <div class="more">
                            <label for="New_Infection_InfectionTypeOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox("InfectionTypeOther", "", new { @id = "New_Infection_InfectionTypeOther", @maxlength = "100" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Treatment</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Treatment">Treatment/Antibiotic</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("TreatmentTemplates") %>
                    <%= Html.TextArea("Treatment", new { @id="New_Infection_Treatment", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="Orders">Narrative</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("NarrativeTemplates") %>
                    <%= Html.TextArea("Orders", new { @id = "New_Infection_Narrative", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="FollowUp">Follow Up</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("NarrativeFollowUp")%>
	                <%= Html.TextArea("FollowUp", new { @id = "New_Infection_FollowUp", @class = "taller" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Signature</legend>
        <div class="column">
            <div class="row">
                <label for="New_Infection_ClinicianSignature" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @class = "complete-required", @id = "New_Infection_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Infection_SignatureDate" class="fl">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="New_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Infection_Status" })%>
    <%= Html.Hidden("SaveStatus", 515, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 520, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("create a new infection log")%>
<%  } %>
</div>