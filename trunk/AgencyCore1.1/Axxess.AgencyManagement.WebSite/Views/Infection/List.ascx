﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">List Infections | <%= Current.AgencyName %></span>
<%  if (Model.Service != AgencyServices.None && Model.ViewListPermissions != AgencyServices.None) { %>
    <%  var pageName = "InfectionList"; %>
    <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%> 
    <ul class="fr buttons">
    <%  if (Model.NewPermissions != AgencyServices.None) { %>
        <li><a class="new-infection-log servicepermission" service="<%=(int)Model.NewPermissions %>">New Infection</a></li><br />
	<%  } %>
    <%  if (Model.ExportPermissions != AgencyServices.None) { %>
        <li><a area="<%=area %>" url="/Export/Infections" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li><br />
	<%  } %>
		<li><a area="<%=area %>" url="/Infection/ListContent" class="grid-refresh">Refresh</a></li>
	</ul>
    <fieldset class="grid-controls ac">
        <div class="filter optional"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" })%></div>
        <div class="filter grid-search"></div>
    </fieldset>
	<div class="clr"></div>
    <%= Html.Telerik().Grid<Infection>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
			columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
			columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
			columns.Bound(i => i.InfectionType).Title("Type of Infection").Sortable(true);
			columns.Bound(i => i.InfectionDateFormatted).Title("Infection Date").Sortable(true).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
			columns.Bound(i => i.StatusName).Title("Status").Sortable(true);
            columns.Bound(i => i.IsUserCanPrint).Title(" ").ClientTemplate("").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Width(35).Sortable(false);
			columns.Bound(i => i.Id).ClientTemplate("").Title("Action").Width(85).Visible(!Current.IsAgencyFrozen);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ListContent", "Infection", new { area }).OperationMode(GridOperationMode.Client)).NoRecordsTemplate("No Infections found.").ClientEvents(events => events
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("Infection.OnDataBound")
            .OnRowDataBound("Infection.OnRowDataBound")
            .OnError("U.OnTGridError")
        ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } else { %>
<div class="wrapper main"><%= Html.NotAuthorized("view the list of infections logs") %></div>
<%  } %>