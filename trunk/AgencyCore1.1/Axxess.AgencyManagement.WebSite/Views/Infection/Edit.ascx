﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Infection>" %>
<span class="wintitle">Edit Infection Log | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  if (Model.IsUserCanEdit) { %>
    <%  using (Html.BeginForm("Update", "Infection", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "editInfectionReportForm", @class = "mainform" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Infection_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Infection_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Infection_UserId" })%>
        <%  string[] infectionTypes = Model.InfectionType.IsNotNullOrEmpty() ? Model.InfectionType.Split(';') : null; %>
        <%  if (Model != null) Model.SignatureDate = DateTime.Today; %>
        <%  if (Model.IsUserCanSeeSticky) { %>
	<%= Html.Partial("ReturnCommentFieldSet", new NoteTopViewData(Model.Id, Model.PatientId, Model.EpisodeId, Model.StatusComment, Model.Service))%>
	    <%  } %>
	<fieldset>
	    <legend>Information</legend>
	    <div class="column">
            <div class="row no-input">
                <label for="Edit_Infection_PatientId" class="fl">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
        <%  var service = (int)Model.Service; %>
        <%  if (Model.EpisodeId.IsEmpty()) { %>
            <div class="row">
                <%= Html.CarePeriodLabel(service, new { @for = "Edit_Infection_EpisodeId", @class = "fl" })%>
                <div class="fr"><%= Html.PatientCarePeriods(service, "EpisodeId", Model.PatientId, Guid.Empty.ToString(), new { @id = "Edit_Infection_EpisodeId", @class = "required not-zero" })%></div>
            </div>
        <%  } else if (Model.EpisodeStartDate.IsNotNullOrEmpty()) { %>
            <div class="row no-input">
                <%= Html.CarePeriodLabel(service, new { @class = "fl" })%>
                <div class="fr"><%= string.Format("{0}-{1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></div>
            </div>
            <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Infection_EpisodeId" })%>
        <%  } %>
        <%  if (Model.Diagnosis != null) { %>
            <%= Html.PrimaryDiagnosisRow(service, Model.Diagnosis.Get("PrimaryDiagnosis"), Model.Diagnosis.Get("ICD9M"), new { @class = "primary-diagnosis" })%>
            <%= Html.SecondaryDiagnosisRow(service, Model.Diagnosis.Get("SecondaryDiagnosis"), Model.Diagnosis.Get("ICD9M2"), new { @class = "secondary-diagnosis" })%>
        <% } %>
            <div class="row">
                <label for="Edit_Infection_PhysicianId" class="fl">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("PhysicianId", Model.PhysicianId != Guid.Empty ? Model.PhysicianId.ToString() : string.Empty, new { @id = "Edit_Infection_PhysicianId", @class = "physician-picker" })%><br />
					<% if(Model.IsUserCanAddPhysicain) { %>
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                    <% } %>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_InfectionDate" class="fl">Date of Infection</label>
                <div class="fr"><input type="text" class="date-picker required" name="InfectionDate" value="<%= Model.InfectionDate.IsValid() ? Model.InfectionDate.ToShortDateString() : string.Empty %>" id="Edit_Infection_InfectionDate" /></div>
            </div>
            <div class="row">
                <label for="Edit_Infection_TreatmentPrescribed" class="fl">Treatment Prescribed?</label>
                <div class="fr">
                    <%= Html.DropDownList("TreatmentPrescribed", new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" },
                            new SelectListItem { Text = "N/A", Value = "NA" }
                        }, "Value", "Text", Model.TreatmentPrescribed).AsEnumerable(), new { @id = "Edit_Infection_TreatmentPrescribed" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_MDNotified" class="fl">M.D. Notified?</label>
                <div class="fr">
                    <%= Html.DropDownList("MDNotified", new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" },
                            new SelectListItem { Text = "N/A", Value = "NA" }
                        }, "Value", "Text", Model.MDNotified).AsEnumerable(), new { @id = "Edit_Infection_MDNotified" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_NewOrdersCreated" class="fl">New Orders?</label>
                <div class="fr">
                    <%= Html.DropDownList("NewOrdersCreated", new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" },
                            new SelectListItem { Text = "N/A", Value = "NA" }
                        }, "Value", "Text", Model.NewOrdersCreated).AsEnumerable(), new { @id = "Edit_Infection_NewOrdersCreated" })%>
                </div>
            </div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Type of Infection</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup six-wide">
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Gastrointestinal", false, "Gastrointestinal") %>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Respiratory", false, "Respiratory") %>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Skin", false, "Skin") %>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Wound", false, "Wound") %>
                    <%= Html.CheckgroupOption("InfectionTypeArray", "Urinary", false, "Urinary") %>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='Edit_Infection_InfectionType6' type='checkbox' value='Other' name='InfectionTypeArray' class='required' {0} />", infectionTypes != null && infectionTypes.Contains("Other") ? "checked='checked'" : "")%>
                            <label for="Edit_Infection_InfectionType6">Other</label>
                        </div>
                        <div class="more">
                            <label for="Edit_Infection_InfectionTypeOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox("InfectionTypeOther", Model.InfectionTypeOther, new { @id = "Edit_Infection_InfectionTypeOther", @maxlength = "100" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Treatment</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Infection_Treatment">Treatment/Antibiotic</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("TreatmentTemplates") %>
                    <%= Html.TextArea("Treatment", Model.Treatment, new { @id = "Edit_Infection_Treatment", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_Orders">Narrative</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("OrdersTemplates")%>
                    <%= Html.TextArea("Orders", Model.Orders, new { @id = "Edit_Infection_Orders", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_FollowUp">Follow Up</label>
                <div class="ac"><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "Edit_Infection_FollowUp", @class = "taller" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Signature</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_ClinicianSignature" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @class = "complete-required", @id = "Edit_Infection_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_SignatureDate" class="fl">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="Edit_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "Edit_Infection_Status" })%>
    <%= Html.Hidden("SaveStatus", 515, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 520, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("edit a infection log")%>
<%  } %>
</div>