﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  var scheduleEvents = Model != null && Model.ScheduleEvents != null ? Model.ScheduleEvents.Where(e => e.DateIn.IsValid() && e.DateIn >= Model.StartDate && e.DateIn <= Model.EndDate).OrderBy(o => o.DateIn).ToList() : new List<GridTask>(); %>
<%  var startWeekDay = (int)Model.StartDate.DayOfWeek; %>
<%  var startDate = Model.StartDate; %>
<%  var endDate = Model.EndDate; %>
<%  var currentDate = Model.StartDate.AddDays(-startWeekDay); %>
<div class="wrapper main">
<%= Html.Hidden("PatientId", Model.PatientId) %>
<%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
<%  if (Model.HasPrevious) { %>
    <div class="button left top">
        <a class="navigate previous-episode" guid="<%= Model.PreviousEpisode %>"><span class="largefont">&#8617;</span> Previous Episode</a>
    </div>
<%  } %>
<%  if (Model.HasNext) { %>
    <div class="button right top">
        <a class="navigate next-episode" guid="<%= Model.NextEpisode %>">Next Episode <span class="largefont">&#8618;</span></a>
    </div>
<%  } %>
    <div class="ac">
    <%  if (Model.IsUserCanPrint) { %>
        <div class="button"><a class="print">Print</a></div>
    <%  } %>
	    <fieldset>
	        <div class="column">
	            <div class="row no-input">
	                <label class="fl">Patient</label>
	                <label class="fr"><%= Model.DisplayName %></label>
	            </div>
	            <div class="row no-input">
	                <label class="fl">Episode</label>
	                <label class="fr"><%= string.Format("{0}-{1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) %></label>
	            </div>
	            <div class="row no-input">
	                <label class="fl">Frequencies</label>
	                <label class="fr"><%= Model.FrequencyList %></label>
	            </div>
	        </div>
	        <div class="column">
	            <div class="row">
		            <ul class="checkgroup one-wide">
		                <li class="option">
		                    <div class="wrapper al">
		                        <input type="checkbox" id="MasterCalendar_ShowMissedVisits" name="ShowMissedVisits" />
		                        <label for="MasterCalendar_ShowMissedVisits">Show Missed Visits</label>
		                    </div>
		                </li>
		            </ul>
	            </div>
	        </div>
	    </fieldset>
        <div class="cal big">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Sun</th>
                        <th>Mon</th>
                        <th>Tue</th>
                        <th>Wed</th>
                        <th>Thu</th>
                        <th>Fri</th>
                        <th>Sat</th>
                    </tr>
                </thead>
                <tbody>
<%  for (int i = 1; i <= 10; i++) { %>
    <%  int addedDate = (i - 1) * 7; %>
                    <tr>
                        <th>Week <%= i %></th>
    <%  for (int j = 0; j <= 6; j++) { %>
        <%  var specificDate = currentDate.AddDays(j + addedDate); %>
        <%  if (specificDate < startDate || specificDate > endDate) { %>
                        <td class="inactive"></td>
        <%  } else { %>
            <%  var currentSchedules = scheduleEvents.FindAll(e => e.DateIn == specificDate); %>
            <%  string tooltip = ""; %>
            <%  string label = ""; %>
            <%  if (currentSchedules.Count > 0) { %>
                <%  foreach (var evnt in currentSchedules) { %>
                    <%  if (!evnt.IsMissedVisit && evnt.Discipline != Disciplines.Orders.ToString()) { %>
                        <%  tooltip += "<strong>" + evnt.TaskName + "</strong><br/>Employee: " + evnt.UserName + "<br/>Status: " + evnt.StatusName + "<br/>"; %>                    
                        <%  label += evnt.TaskName + "<br/>"; %>                    
                    <%  } %>
                <%  } %>
						<td tooltip="<%= tooltip %>">
							<strong><%= string.Format("{0:MM/dd}", specificDate) %></strong>
							<br />
							<%= label %>
						</td>
            <%  } else { %>
						<td>
							<strong><%= string.Format("{0:MM/dd}", specificDate) %></strong>
						</td>
            <%  } %>
        <%  } %>
    <%  } %>
					</tr>
<%  } %>
				</tbody>
			</table>
		</div>
	</div>
</div>