﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceTaskViewData>" %>
<%  string pagename = "AgencyScheduleDeviation"; %>
<%= Html.Filter("SortParams", string.Format("{0}-{1}", Model.SortColumn, Model.SortDirection), new { @id = pagename + "_SortName", @class = "sort-parameters" })%>
<%= Html.Telerik().Grid(Model.Tasks).Name(pagename + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
        columns.Bound(m => m.PatientIdNumber).Title("MRN").Width(70);
        columns.Bound(m => m.PatientName).Title("Patient");
        columns.Bound(m => m.DisciplineTaskName).Sortable(false).Title("Task");
        columns.Bound(m => m.StatusName).Sortable(false).Title("Status");
        columns.Bound(m => m.UserName).Title("Employee").Width(155);
		columns.Bound(m => m.EventDate).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(105);
		columns.Bound(m => m.VisitDate).Title("Visit Date").Format("{0:MM/dd/yyyy}").Width(80);
        columns.Bound(m => m.Id).Template(t => (Model.EditDetailPermissions.Has(Model.Service) && !Current.IsAgencyFrozen) ? string.Format("<a class=\"link\" onclick=\"Schedule.Task.HomeHealth.Edit('{0}','{1}')\">Details</a>", t.PatientId, t.Id) : string.Empty).Sortable(false).Title("Action").Width(80).Aggregate(aggregates => aggregates.Count()).FooterTemplate(result =>string.Format("Total : {0}",result.Count));
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = Model.SortColumn;
        var sortDirection =Model.SortDirection;
        if (sortName == "PatientIdNumber") {
            if (sortDirection == "ASC") order.Add(o => o.PatientIdNumber).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientIdNumber).Descending();
        } else if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "VisitDate") {
            if (sortDirection == "ASC") order.Add(o => o.VisitDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.VisitDate).Descending();
        }
    })).Scrollable().NoRecordsTemplate("No deviating schedules found.").Footer(true) %>