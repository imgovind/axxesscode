﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AttachmentViewData>" %>
<div class="wrapper main">
    <span class="bigtext ac"><%= Model.DisciplineTaskName %> | <%= Model.PatientName %></span>
    <fieldset>
        <legend>Attachments</legend>
        <div class="wide-column">
            <div class="row no-input">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                <span>There are <span id="scheduleEvent_Assest_Count"><%= Model.Assets.Count.ToString() %></span> attachment(s).</span>
            </div>
<%  foreach (Guid assetId in Model.Assets) { %>
            <div class="row no-input"><%= Html.Asset(assetId) %></div>
<%  } %>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
</div>