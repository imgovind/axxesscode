﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Reassign" + Model.Type + "Schedules", "Schedule", FormMethod.Post, new { @id = "reassign" + Model.Type + "Form", @class = "mainform" })) { %>
    <%  if (!Model.Type.IsEqual("All")) { %>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" })%>
    <%  } %>
	<%  DateTime startDate = Model != null && Model.StartDate != DateTime.MinValue ? Model.StartDate : DateTime.Now.AddDays(-59); %>
	<%  DateTime endDate = Model != null && Model.EndDate != DateTime.MinValue ? Model.EndDate : DateTime.Now; %>
	<fieldset>
	    <legend>Reassign Tasks</legend>
	    <div class="wide-column">
	<%  if (Model != null && !Model.Type.IsEqual("All")) { %>
            <div class="row">
                <label class="fl">Patient Name</label>
				<div class="fr"><label><%= Model.PatientDisplayName %></label></div> 
			</div>
	<%  } %>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_EmployeeOldId">Employee From</label>
				<div class="fr"><%= Html.Users("EmployeeOldId", "", "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = Model.Type + "_EmployeeOldId", @class = "required not-zero" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_EmployeeId">Employee To</label>
				<div class="fr"><%= Html.Users("EmployeeId", "", "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = Model.Type + "_EmployeeId", @class = "required not-zero" })%></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_StartDate">Date From</label>
				<div class="fr"><input type="text" class="date-picker required" name="StartDate" value="<%= startDate.ToShortDateString() %>" id="<%= Model.Type %>_StartDate" /></div>
			</div>
			<div class="row">
				<label class="fl" for="<%= Model.Type %>_EndDate">Date To</label>
				<div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= endDate.ToShortDateString() %>" id="<%= Model.Type %>_EndDate" /></div>
			</div>
			<div class="row ac"><em><strong>Note: </strong> Only tasks that are not started and not yet due will be reassigned.</em></div>
		</div>
	</fieldset>
	<ul class="buttons ac">
	    <li><a class="save close">Reassign</a></li>
	    <li><a class="close">Close</a></li>
	</ul>
<%  } %>
</div>