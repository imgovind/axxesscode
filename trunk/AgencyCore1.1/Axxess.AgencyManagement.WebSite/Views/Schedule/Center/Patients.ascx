﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientSelectionViewData>" %>
<%  Html.Telerik().Grid(Model.Patients).Name("ScheduleSelectionGrid").HtmlAttributes(new { @class = "patient-list" }).Columns(columns => {
        columns.Bound(p => p.LastName).HtmlAttributes(new { @class = "searchL" });
        columns.Bound(p => p.ShortName).Title("First Name").HtmlAttributes(new { @class = "searchF" });
		columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "pid" }).Width(0).Hidden();
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("AllSort", "Schedule", new { BranchId = Guid.Empty, StatusId = 1, PaymentSourceId = 0 }).OperationMode(GridOperationMode.Client)).ClientEvents(events => events
        .OnDataBinding("Schedule.Center.HomeHealth.PatientSelector.OnDataBinding")
        .OnDataBound("Schedule.Center.HomeHealth.PatientSelector.OnDataBound")
        .OnRowSelect("Schedule.Center.HomeHealth.PatientSelector.OnRowSelected")
    ).Sortable().Selectable().Scrollable().Footer(false).Sortable(sr => sr.OrderBy(so => so.Add(s => s.LastName).Ascending())).Render(); %>z