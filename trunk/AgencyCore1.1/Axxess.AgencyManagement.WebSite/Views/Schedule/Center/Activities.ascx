﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  var widthIndex = 0; %>
<%  var isActionVisible = false; %>
<%  if (Model.IsUserCanDelete) { %>
    <%  widthIndex++; %>
    <%  isActionVisible = true; %>
<%  } %>
<%  if (Model.IsUserCanEditDetail) { %>
    <%  widthIndex++; %>
    <%  isActionVisible = true; %>
<%  } %>
<%  if (Model.IsUserCanReassign) { %>
    <%  widthIndex++; %>
    <%  isActionVisible = true; %>
<%  } %>
<%  if (Model.IsUserCanReopen) { %>
    <%  widthIndex++; %>
    <%  isActionVisible = true; %>
<%  } %>
<%  if (Model.IsUserCanRestore) { %>
    <%  widthIndex++; %>
    <%  isActionVisible = true; %>
<%  } %>
<%  var url = new Func<GridTask, string>(t =>
    {
        var actions = new List<string>();
        if (t.IsUserCanEditDetail) actions.Add("<li><a class=\"link edit-details\"><span class='img icon22 edit-schedule'/>Details</a></li>");
        if (t.IsUserCanReopen) actions.Add("<li><a class=\"link reopen-task\"><span class='img icon22 reopen-schedule'/>Reopen</a></li>");
        if (t.IsUserCanReassign) actions.Add("<li><a class=\"link reassign-task\"><span class='img icon22 reassign-schedule'/>Reassign</a></li>");
        if (t.IsUserCanRestore) actions.Add("<li><a class=\"link restore-task\"><span class='img icon22 reopen-schedule'/>Restore</a></li>");
        if (t.IsUserCanDelete) actions.Add("<li><a class=\"link delete-task\"><span class='img icon22 delete'/>Delete</a></li>");
        if (actions.IsNotNullOrEmpty())
        {
            return string.Format("<div class=\"grid-action\"><span class='img icon22 pointer right'/><ul class=\"action-menu\">{0}</ul></div>", actions.ToArray().Join(" "));
        }
        else
        {
            return string.Empty;
        }
    }); %>
<div class="abs above ac scheduler-toggle">
    <div class="fr">
        <a class="grid-resize img grid-expand"></a>
    </div>
    <label for="ScheduleActivity_Discipline">Show</label>
    <select id="ScheduleActivity_Discipline" class="schedule-discipline">
        <option value="All" selected="selected">All</option>
        <option value="Nursing">Nursing</option>
        <option value="HHA">HHA</option>
        <option value="MSW">MSW</option>
        <option value="Therapy">Therapy</option>
        <option value="Dietician">Dietician</option>
    </select>
</div>
<fieldset class="note-legend" style="right:7.3em">
    <ul>
        <li><span class="img icon16 money"></span> OASIS Profile</li>
        <li><span class="img icon16 note yellow"></span> Visit Comments</li>
        <li><span class="img icon16 note blue"></span> Episode Comments</li>
        <li><span class="img icon16 note red"></span> Missed/Returned</li>
        <li><span class="img icon16 print"></span> Print Document</li>
        <li><span class="img icon16 paperclip"></span> Attachments</li>
    </ul>
</fieldset>
<%  Html.Telerik().Grid(Model.ScheduleEvents).Name("ScheduleActivityGrid").HtmlAttributes(new { @class = "schedule-activity-grid" }).Columns(columns => {
        columns.Bound(s => s.TaskName)
            .Template(s => s.IsUserCanEdit ? string.Format("<a class=\"{0} {1}\">{2}</a>", s.IsMissedVisit ? "missedvisit" : "edit", s.IsComplete ? "complete" : string.Empty, s.TaskName) : s.TaskName)
            .ClientTemplate("<# if (IsUserCanEdit) { #><a class=\"<# if(IsMissedVisit) { #>missedvisit<# } else { #>edit<# } #><# if (IsComplete) { #> complete<# } #>\"><#= TaskName #></a><# } else { #><#= TaskName #><# } #>")
            .Title("Task")
            .HtmlAttributes(new { @class = "edit-task" });
        columns.Bound(s => s.DateIn).Format("{0:MM/dd/yyyy}").Title("Scheduled Date").Width(105);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(180);
        columns.Bound(s => s.StatusName).Title("Status");
        columns.Bound(s => s.IsOASISProfileExist).Template(s => s.IsOASISProfileExist ? "<a class=\"OASISprofile img icon16 money\"></a>" : string.Empty)
            .ClientTemplate("<# if (IsOASISProfileExist) { #><a class=\"OASISprofile img icon16 money\"></a><# } #>")
            .Title(" ").HeaderHtmlAttributes(new { @class = "oasisprofileprint icon-header-overlayed" }).HtmlAttributes(new { @class = "oasisprofileprint icon-overlayed" }).Width(30).Sortable(false).Visible(Model.IsOASISProfileExist);
        columns.Bound(s => s.StatusComment).Template(s => s.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 red note\" tooltip=\"{0}\"></a>", s.StatusComment) : string.Empty)
            .ClientTemplate("<# if (StatusComment) { #><a class=\"img icon16 red note\" tooltip=\"<#=StatusComment#>\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).Visible(Model.IsStickyNote).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.Comments).Template(s => s.Comments.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\" tooltip=\"{0}\"></a>", s.Comments) : string.Empty)
            .ClientTemplate("<# if (Comments) { #><a class=\"img icon16 yellow note\" tooltip=\"<#=Comments#>\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).Visible(Model.IsStickyNote).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.EpisodeNotes).Template(s => s.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 blue note\" tooltip=\"{0}\"></a>", s.EpisodeNotes) : string.Empty)
            .ClientTemplate("<# if (EpisodeNotes) { #><a class=\"img icon16 blue note\" tooltip=\"<#=EpisodeNotes#>\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).Visible(Model.IsStickyNote).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.IsUserCanPrint).Template(s => s.IsUserCanPrint ? "<a class=\"" + s.Group + " img icon16 print\"></a>" : string.Empty)
            .ClientTemplate("<# if (IsUserCanPrint) { #><a class=\"<#=Group#> img icon16 print\"></a><# } #>")
            .Title(" ").Width(30).Sortable(false).HeaderHtmlAttributes(new { @class = "icon-header-overlayed" }).HtmlAttributes(new { @class = "icon-overlayed" });
        columns.Bound(s => s.IsAttachmentExist).Template(s => s.IsAttachmentExist ? "<a class=\"img icon16 paperclip attachment\"></a>" : string.Empty)
            .ClientTemplate("<# if(IsAttachmentExist){#> <a class=\"img icon16 paperclip attachment\"></a><#}#>")
            .HeaderHtmlAttributes(new { @class = "attachmentload icon-header-overlayed" }).HtmlAttributes(new { @class = "attachmentload icon-overlayed" }).Title(" ").Width(30).Sortable(false);
        columns.Bound(s => s.Id)
            .Template(s => url(s))
            .ClientTemplate("<# if(IsUserCanEditDetail || IsUserCanReopen || IsUserCanReassign || IsUserCanRestore || IsUserCanDelete) #><div class='grid-action'><span class='img big pointer right'/></div>")
            .HeaderHtmlAttributes(new { @class = "action menu-based" })
            .HtmlAttributes(new { @class = "action menu-based" })
            .Title("Action")
            .Width(70)
            .Sortable(false)
            .Visible(isActionVisible);
        columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "eid" }).Width(0).Hidden();
		columns.Bound(s => s.PatientId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "pid" }).Width(0).Hidden();
		columns.Bound(s => s.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "id" }).Width(0).Hidden();
		columns.Bound(s => s.Service).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "service" }).Width(0).Hidden();
		columns.Bound(s => s.Type).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "type" }).Width(0).Hidden();
        columns.Bound(s => s.IsComplete).Visible(false);
    }).ClientEvents(c => c
        .OnDataBinding("Schedule.Center.HomeHealth.Activities.OnDataBinding")
        .OnDataBound("Schedule.Center.HomeHealth.Activities.OnDataBound")
        .OnRowDataBound("Schedule.Center.HomeHealth.Activities.OnRowDataBound")
    ).DataBinding(dataBinding => dataBinding.Ajax().Select("ActivitySort", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline })).RowAction(row => {
        if (row.DataItem.IsComplete) {
            if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("completed")) row.HtmlAttributes["class"] += " completed";
            else row.HtmlAttributes.Add("class", "completed");
        }
        if (row.DataItem.IsOrphaned) {
            if (row.HtmlAttributes.ContainsKey("class") && !row.HtmlAttributes["class"].ToString().Contains("orphaned")) row.HtmlAttributes["class"] += " orphaned";
            else row.HtmlAttributes.Add("class", "orphaned");
        }
    }).Sortable(sr => sr.OrderBy(so => so.Add(s => s.DateIn).Descending())).Scrollable().Footer(false).Render(); %>
<div class="icon-overlay" style="right:7.3em"></div>