﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<span class="wintitle">Schedule Center | <%= Current.AgencyName %></span>
<% string[] stabs = new string[] { "Nursing", "HHA","Therapy"}; %>
<div class="wrapper layout">
	<div class="ui-layout-west">
        <div class="top ac"><% Html.RenderPartial("Center/Filters", Model); %></div>
        <div class="bottom">
		    <%= Html.Hidden("CurrentPatientId", Model != null && Model.SelectionViewData != null ? Model.SelectionViewData.CurrentPatientId : Guid.Empty, new { @id = "ScheduleCenter_CurrentPatientId" })%>
		    <%  Html.RenderPartial("Center/Patients", Model.SelectionViewData); %>
       </div>
    </div>
    <div id="ScheduleMainResult" class="ui-layout-center">
<%  if (Model != null && Model.SelectionViewData != null && Model.SelectionViewData.Count > 0) { %>
        <%  Html.RenderPartial("Center/Content", Model.CalendarData); %>
<%  } else { %>
        <script type="text/javascript">
            $("#ScheduleMainResult").empty().html(U.MessageWarn("No Patients", "No Patients found that fit your search criteria."));
        </script>
<%  } %>
    </div>
</div>
