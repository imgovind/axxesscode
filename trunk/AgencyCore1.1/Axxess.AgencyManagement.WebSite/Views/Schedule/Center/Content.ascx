﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  if (Model != null) { %>
<div class="top" id="scheduleTop">
    <ul class="window-menu scheduler-toggle">
    <%  if (Model.IsUserCanAdd) { %>
        <li><a class="new-schedules" status="Multiple Employee">Schedule Employee</a></li>
    <%  } %>
        <li>
            <a class="menu-trigger">Episode Manager</a>
            <ul class="menu">
    <%  if (Model.IsUserCanAddEpisode) { %>
                <li><a class="new-episode" status="Add New Episode">New Episode</a></li>
    <%  } %>
    <%  if (Model.IsUserCanViewEpisodeList) { %>
                <li><a class="inactive-episodes">Inactive Episodes</a></li>
    <%  } %>
                <li><a class="episode-frequencies">Episode Frequencies</a></li>
            </ul>
        </li>
        <li><a class="master-calendar">Master Calendar</a></li>
    <%  if (Model.IsUserCanReassign) { %>
        <li><a class="reassign-schedules" status="Reassign Schedules">Reassign Schedules</a></li>
    <%  } %>
    <%  if (Model.IsUserCanDelete) { %>
        <li><a class="delete-schedules" status="Delete Schedules">Delete Multiple Tasks</a></li>
    <%  } %>
    </ul>
    <div class="wrapper main rel"><%  Html.RenderPartial("Center/Calendar", Model); %></div>
    <%  if (Model.IsUserCanAdd) { %>
    <div id="ScheduleCenter_Collapsed"><a class="show-scheduler no-border">Show Scheduler</a></div>
    <div id="ScheduleCenter_TabStrip" class="scheduler horizontal-tabs"><% Html.RenderPartial("Center/Scheduler", Model); %></div>
    <%  } %>
</div>
<div id="scheduleBottomPanel" class="bottom"><% Html.RenderPartial("Center/Activities", Model); %></div>
<%  } else { %>
<script type="text/javascript">
    U.MessageWarn("No Episodes", "No episodes found for this patient.");
</script>
<%  } %>
