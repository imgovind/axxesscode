﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  if (Model != null && Model.IsEpisodeExist) { %>
    <%  var scheduleEvents = Model.ScheduleEvents != null && Model.ScheduleEvents.Count > 0 ? Model.ScheduleEvents.Where(s => s.DateIn.Date >= Model.StartDate.Date && s.DateIn.Date <= Model.EndDate.Date).OrderBy(o => o.DateIn.Date).ToList() : new List<GridTask>();%>
    <%  DateTime[] startdate = new DateTime[3], enddate = new DateTime[3], currentdate = new DateTime[3]; %>
    <%  startdate[0] = DateUtilities.GetStartOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
    <%  enddate[0] = DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
    <%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
    <%  startdate[1] = enddate[0].AddDays(1); %>
    <%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
    <%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
    <%  startdate[2] = enddate[1].AddDays(1); %>
    <%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
    <%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
    <input type="hidden" name="PatientId" value="<%= Model.PatientId %>" id="ScheduleCenter_PatientId" />
    <input type="hidden" name="EpisodeId" value="<%= Model.EpisodeId %>" id="ScheduleCenter_EpisodeId" />
    <input type="hidden" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" id="ScheduleCenter_StartDate" />
    <input type="hidden" name="EndDate" value="<%= Model.EndDate.ToShortDateString() %>" id="ScheduleCenter_EndDate" />
    <input type="hidden" name="Discpline" value="<%= Model.Discpline %>" id="ScheduleCenter_Discpline" />
    <%  if (Model.HasPrevious) { %>
<span class="button left top scheduler-toggle">
    <a class="navigate previous-episode" guid="<%= Model.PreviousEpisode %>"><span class="largefont">&#8617;</span> Previous Episode</a>
</span>
    <%  } %>
    <%  if (Model.HasNext) { %>
<span class="button right top scheduler-toggle">
    <a class="navigate next-episode" guid="<%= Model.NextEpisode %>">Next Episode <span class="largefont">&#8618;</span></a>
</span>
    <%  } %>
<div class="ac">
    <div class="patient-info schedule ac">
        <div class="al ma">
			<div class="fr scheduler-toggle"><%= Html.PatientEpisodes((int)AgencyServices.HomeHealth, "EpisodeList", Model.EpisodeId.ToString(), Model.PatientId, new { @id = "calendar-episode-list", @class = "calendar-episode-list" })%></div>
            <h2 class="patient ac"><%= Model.DisplayName %></h2>
        </div>
        <div class="clr"></div>
        <div class="ma scheduler-toggle">
            <ul class="buttons ac">
    <%  if (Model.IsUserCanEditEpisode) { %>
                <li><a class="edit-episode">Manage Episode</a></li>
    <%  } %>
                <li><a class="refresh">Refresh</a></li>
                <li><a class="patient-charts">Patient Charts</a></li>
            </ul>
        </div>
        <div class="calendar">
   
    <%  for (int c = 0; c < 3; c++) { %>
        <%  if (startdate[c] <= Model.EndDate) { %>
            <div class="cal">
                <table>
                    <thead>
                        <tr>
                            <th colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></th>
                        </tr>
                        <tr>
                            <th>Su</th>
                            <th>Mo</th>
                            <th>Tu</th>
                            <th>We</th>
                            <th>Th</th>
                            <th>Fr</th>
                            <th>Sa</th>
                        </tr>
                    </thead>
                    <tbody>
            <%  var maxWeek = DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); %>
            <%  for (int i = 0; i <= maxWeek; i++) { %>
                        <tr>
                <%  string tooltip = ""; %>
                <%  int addedDate = (i) * 7; %>
                <%  if (currentdate[c].AddDays(7 + addedDate).Date >= Model.StartDate && currentdate[c].AddDays(addedDate) <= enddate[c] && currentdate[c].Date.AddDays(addedDate) <= Model.EndDate) for (int j = 0; j <= 6; j++) { %>
                    <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
                    <%  if (specificDate < Model.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate.Date > Model.EndDate) { %>
                            <td class="inactive"></td>
                    <%  } else { %>
                        <%  var events = scheduleEvents.FindAll(e => e.DateIn.Date == specificDate.Date); %>
                        <%  var count = events.Count; %>
                        <%  if (count > 1) { %>
                            <%  var allevents = "<br />"; %>
                            <%  events.ForEach(e => { allevents += string.Format("{0} - <em>{1}</em><br />", e.TaskName, e.UserName); }); %>
                            <%  tooltip = specificDate.ToShortDateString() + allevents; %>
                            <td class="multi">
                                <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                            </td>
                        <%  } else if (count == 1) { %>
                            <%  var evnt = events.First(); %>
                            <%  var missed = (evnt.IsMissedVisit) ? "missed" : ""; %>
                            <%  var status = !evnt.IsMissedVisit && evnt.IsComplete ? "completed" : ""; %>
                            <%  tooltip = specificDate.ToShortDateString() + string.Format("<br />{0} - <em>{1}</em>", evnt.TaskName, evnt.UserName); %>
                            <td class="status <%= status %> scheduled <%= missed %>">
                                <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                            </td>
                        <%  } else { %>
                            <%  tooltip = ""; %>
                            <td>
                                <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                            </td>
                        <%  } %>
                    <%  } %>
                <%  } %>
                        </tr>
            <%  } %>
                    </tbody>
                </table>
            </div>
        <% } %>
    <% } %>
        </div>
	    <fieldset class="legend scheduler-toggle">
		    <ul>
			    <li><div class="scheduled">&#160;</div> Scheduled</li>
			    <li><div class="completed">&#160;</div> Completed</li>
			    <li><div class="missed">&#160;</div> Missed</li>
			    <li><div class="multi">&#160;</div> Multiple</li>
		    </ul>
	    </fieldset>
	</div>
</div>
<%  } %>