﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<ul class="horizontal-tab-list">
	<li><a href="#ScheduleCenter_NursingTab" discipline="Nursing" class="no-border">Nursing</a></li>
	<li><a href="#ScheduleCenter_HHATab" discipline="HHA" class="no-border">HHA</a></li>
	<li><a href="#ScheduleCenter_MSWTab" discipline="MSW" class="no-border">MSW/Other</a></li>
	<li><a href="#ScheduleCenter_TherapyTab" discipline="Therapy" class="no-border">Therapy</a></li>
	<li><a href="#ScheduleCenter_DieticianTab" discipline="Dietician" class="no-border">Dietician</a></li>
	<li><a href="#ScheduleCenter_OrdersTab" discipline="Orders" class="no-border">Orders/Care Plans</a></li>
	<li><a href="#ScheduleCenter_OutlierTab" discipline="Multiple" class="no-border">Daily/Outlier</a></li>
</ul>
<%  string[] stabs = new string[] { "Nursing", "HHA", "MSW", "Therapy", "Dietician" }; %>
<%  for (int sindex = 0; sindex < stabs.Length; sindex++) { %>
    <%  string stitle = stabs[sindex]; %>
    <%  string tabname = stitle == "MSW" ? "MSW / Other" : stitle; %>
<div id="ScheduleCenter_<%= stitle %>Tab" class="scheduler tab-content">
    <ul class="buttons fr ac">
        <li><a class="schedule-save">Save</a></li><br />
        <li><a class="schedule-cancel">Cancel</a></li>
    </ul>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="grid-third">Task</span>
                <span class="grid-third">User</span>
                <span>Date</span>
            </li>
        </ul>
        <ol></ol>
    </div>
</div>
<%  } %>
<div id="ScheduleCenter_OrdersTab" class="scheduler tab-content">
    <ul class="buttons fr ac">
        <li><a class="schedule-save">Save</a></li><br />
        <li><a class="schedule-cancel">Cancel</a></li>
    </ul>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="grid-third">Task</span>
                <span class="grid-third">User</span>
                <span>Date</span>
            </li>
        </ul>
        <ol></ol>
    </div>
</div>
<div id="ScheduleCenter_OutlierTab" class="scheduler tab-content">
    <ul class="buttons fr ac">
        <li><a class="schedule-save multiple">Save</a></li><br />
        <li><a class="schedule-cancel">Cancel</a></li>
    </ul>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="grid-third">Task</span>
                <span class="grid-third">User</span>
                <span>Date Range</span>
            </li>
        </ul>
        <ol>
            <li class="permanent no-hover">
                <span class="grid-third"><%= Html.MultipleDisciplineTasks("DisciplineTask", "0", new { })%></span>
                <span class="grid-third"><%= Html.Users("UserId", "", "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { })%></span>
                <span>
                    <input type="text" name="StartDate" class="schedule-date-range" />
                    &#8211;
                    <input type="text" name="EndDate" class="schedule-date-range" />
                </span>
            </li>
        </ol>
    </div>
</div>