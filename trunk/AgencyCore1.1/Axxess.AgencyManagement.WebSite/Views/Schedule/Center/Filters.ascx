﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<div class="center-filter">
    <label for="ScheduleCenter_BranchId" class="fl">Branch</label>
    <div class="filter-fill"><%= Html.BranchList("BranchId","", 1, true, "-- All Branches --", new { @class = "PatientBranchId filterInput", @id = "ScheduleCenter_BranchId" })%></div>
</div>
<div class="center-filter">
    <label for="ScheduleCenter_StatusId" class="fl">View</label>
    <div class="filter-fill"><%= Html.PatientStatusList("StatusId", Model.SelectionViewData.PatientListStatus.ToString(), true, false, "", new { @id = "ScheduleCenter_StatusId", @class = "PatientStatusDropDown filterInput" })%></div>
</div>
<div class="center-filter">
    <label for="ScheduleCenter_PaymentSourceId" class="fl">Filter</label>
    <div class="filter-fill">
        <select name="PaymentSourceId" id="ScheduleCenter_PaymentSourceId" class="PatientPaymentDropDown filterInput">
            <option value="0">All</option>
            <option value="1">Medicare (traditional)</option>
            <option value="2">Medicare (HMO/managed care)</option>
            <option value="3">Medicaid (traditional)</option>
            <option value="4">Medicaid (HMO/managed care)</option>
            <option value="5">Workers' compensation</option>
            <option value="6">Title programs</option>
            <option value="7">Other government</option>
            <option value="8">Private</option>
            <option value="9">Private HMO/managed care</option>
            <option value="10">Self Pay</option>
            <option value="11">Unknown</option>
        </select>
    </div>
</div>
<div class="center-filter">
    <label for="txtSearch_Patient_Selection" class="fl">Find</label>
    <div class="filter-fill text"><input id="txtSearch_Patient_Selection" class="text filterInput" name="SearchText" value="" type="text" /></div>
</div>