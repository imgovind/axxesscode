﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ITask>" %>
<span class="wintitle">Task Details | <%= Model.DisciplineTaskName %> | <%= Model.PatientName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdateDetails", "Schedule", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "ScheduleTaskDetailForm", @class = "mainform" }))
    { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("LocationId", Model.LocationId)%>
    <%= Html.Hidden("Discipline", Model.Discipline)%>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row no-input">
                <label class="fl">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
            <div class="row no-input">
                <label class="fl">MRN</label>
                <div class="fr"><%= Model.PatientIdNumber %></div>
            </div>
            <%  if (Model.IsTaskChangeable)
                { %>
        <div class="row">
	        <label class="fl">Task</label>
            <div class="fr">
                <span class="button"><a class="load-disciplinetask">Load Discipline Task</a></span>
                <%= Html.Hidden("DisciplineTask", Model.DisciplineTask, new { @id = "ScheduleTaskDetail_DisciplineTask", @class = "required not-zero" })%>
            </div>
        </div>
<%  }
                else
                { %>
        <%= Html.Hidden("DisciplineTask", Model.DisciplineTask, new { @id = "ScheduleTaskDetail_DisciplineTask" })%>
<%  }%>
            <% Html.RenderPartial(string.Format("Detail/{0}", Model.Service.ToString()), Model); %>
        </div>
        <div class="column">
            <div class="row no-input">
	            <%= Html.CarePeriodLabel((int)Model.Service, null) %>
                <div class="fr"><%= Model.StartDate.ToShortDateString().ToZeroFilled() %> &#8211; <%= Model.EndDate.ToShortDateString().ToZeroFilled() %></div>
            </div>
            <div class="row">
                <label for="ScheduleTaskDetail_Status" class="fl">Status</label>
                <div class="fr"><%= Html.Status("Status", Model.Status, Model.DisciplineTask, Model.EventDate, new { @id = "ScheduleTaskDetail_Status" }) %></div>
            </div>
            <div class="row <%= Model.IsComplete ? "no-input" : string.Empty %>">
                <label for="ScheduleTaskDetail_AssignedTo" class="fl">Assigned To</label>
                <div class="fr">
                    <%  if (!Model.IsComplete){ %>
						<%= Html.ToggleUsers("UserId", Model.UserId.ToString(), Model.UserName, "ScheduleTaskDetail_AssignedTo", "user-selector required not-zero")%>
					<%  } else { %>
						<%= Html.Hidden("UserId", Model.UserId)%>
						<%= Model.UserName %>
                    <%  } %>
                </div>
            </div>
            <%  if (Model.Discipline.IsEqual(Disciplines.Orders.ToString())) { %>
            <div class="row">
                <label for="ScheduleTaskDetail_PhysicianId" class="fl">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "ScheduleTaskDetail_PhysicianId", @class = "physician-picker" })%></div>
            </div>
            <%  } %>
            <div class="row">
                <label for="ScheduleTaskDetail_Surcharge" class="fl">Surcharge</label>
                <div class="fr"><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "ScheduleTaskDetail_Surcharge", @class = "currency", @maxlength = "7" })%></div>
            </div>
            <div class="row">
                <label for="ScheduleTaskDetail_AssociatedMileage" class="fl">Associated Mileage</label>
                <div class="fr"><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "ScheduleTaskDetail_AssociatedMileage", @class = "numeric", @maxlength = "7" })%></div>
            </div>
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "ScheduleTaskDetail_Billable" })%>
                            <label for="ScheduleTaskDetail_Billable">Billable</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <%  if (Model.CanAddSupplies) { %>
    <fieldset>
        <legend>Supply</legend>
        <div class="wide-column">
            <div class="row ac">
                <div class="button"><a onclick="Supply.<%= Model.Service.ToString() %>.LoadWorkSheet('<%= Model.PatientId %>','<%= Model.Id %>')">Supply Worksheet</a></div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.Service == AgencyServices.HomeHealth) { %>
    <fieldset>
        <legend>Reassign Episode</legend>
        <div class="column">
            <div class="row">
                <ul class="checkgroup one-wide">
                    <li class="option">
                        <div class="wrapper">
                            <%= Html.CheckBox("IsEpisodeReassiged", false, new { @id = "ScheduleTaskDetail_IsEpisodeReassiged" })%>
                            <label for="ScheduleTaskDetail_IsEpisodeReassiged">Reassign this Event to Another Episode</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="column">
            <div class="row episodereassigedcontainer" id="ScheduleTaskDetail_EpisodeReassigedContainer">
                <label for="ScheduleTaskDetail_NewEpisodeId">Select Episode</label>
                <%= Html.PatientEpisodesExceptCurrent((int)Model.Service, "NewEpisodeId", Model.EpisodeId.ToString(), Model.PatientId, new { @id = "ScheduleTaskDetail_NewEpisodeId" })%>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Comments <span class="img icon16 note yellow"></span> <em>(Yellow Sticky Note)</em></legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="ScheduleTaskDetail_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
   <%-- //TODO:Need permission work Current.HasRight(Permissions.AccessCaseManagement)--%>
    <%  if (true) { %>
    <fieldset>
        <legend>Return Reason <span class="img icon16 red note"></span> <em>(Red Sticky Note)</em></legend>
        <div class="wide-column">
            <div class="row ac">
                <div class="button"><%= Html.ReturnCommentsLink(Model.Service, Model.Id, Model.EpisodeId, Model.PatientId, false)%></div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Attachments</legend>
        <div class="column">
            <div class="row">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
	            <span>There are <span  id="scheduleEvent_Assest_Count" class="attachment-count"><%= Model.Assets.Count.ToString() %></span> attachment(s).</span>
    <%  if (Model.Assets.Count > 0) { %>
                <ul>
        <%  foreach (Guid assetId in Model.Assets) { %>
                    <li><%= Html.Asset(assetId) + string.Format("<a class=\"link\" onclick=\"Schedule.Task.{3}.DeleteAsset($(this),'{0}','{1}','{2}');\">Delete</a>", Model.PatientId, Model.Id, assetId, Model.Service.ToString())%></li>
        <%  } %>
                </ul>
    <%  } %>
            </div>
        </div>
        <div class="column">
            <div class="row ac">
                <div>Use the upload fields below to upload files associated with this scheduled task.</div>
                <div><input id="ScheduleTaskDetail_File1" type="file" name="Attachment1" /></div>
                <div><input id="ScheduleTaskDetail_File2" type="file" name="Attachment2" /></div>
                <div><input id="ScheduleTaskDetail_File3" type="file" name="Attachment3" /></div>
            </div>
        </div>
    </fieldset>
    <a class="fr" onclick="Schedule.LoadLog('<%= Model.PatientId %>','<%= Model.Id %>','<%= Model.DisciplineTask %>')" ><span class="img icon32 log"></span></a>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>
