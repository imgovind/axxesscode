﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PrivateDutyScheduleTask>" %>
<div class="row">
    <label class="fl" for="EditPrivateDutyTask_EventStartDate">Scheduled Start Time</label>
    <div class="fr">
        <%= Html.DatePicker("EventStartTime.Date", Model.EventStartTime, true, new { @class = "short", @id = "EditPrivateDutyTask_EventStartDate" })%>
        <span><%= Html.TimePicker("EventStartTime.Time", Model.EventStartTime, true, new { @class = "short", @id = "EditPrivateDutyTask_EventStartTime" })%></span>
    </div>
</div>
<div class="row">
    <label class="fl" for="EditPrivateDutyTask_EventEndDate">Scheduled End Time</label>
    <div class="fr">
        <%= Html.DatePicker("EventEndTime.Date", Model.EventEndTime, true, new { @class = "short", @id = "EditPrivateDutyTask_EventEndDate" })%>
        <span><%= Html.TimePicker("EventEndTime.Time", Model.EventEndTime, true, new { @class = "short", @id = "EditPrivateDutyTask_EventEndTime" })%></span>
    </div>
</div>
<div class="row">
    <label class="fl" for="EditPrivateDutyTask_StartDate">Actual Visit Date/Time In</label>
    <div class="fr">
        <%= Html.DatePicker("VisitStartTime.Date", Model.VisitStartTime, true, new { @class = "short", @id = "EditPrivateDutyTask_VisitStartDate" })%>
        <span><%= Html.TimePicker("VisitStartTime.Time", Model.VisitStartTime, true, new { @class = "short", @id = "EditPrivateDutyTask_VisitStartTime" })%></span>
    </div>
</div>
<div class="row">
    <label class="fl" for="EditPrivateDutyTask_EndDate">Actual Visit Date/Time Out</label>
    <div class="fr">
        <%= Html.DatePicker("VisitEndTime.Date", Model.VisitEndTime, true, new { @class = "short", @id = "EditPrivateDutyTask_VisitEndDate" })%>
        <span><%= Html.TimePicker("VisitEndTime.Time", Model.VisitEndTime, true, new { @class = "short", @id = "EditPrivateDutyTask_VisitEndTime" })%></span>
    </div>
</div>
<div class="row">
    <ul class="checkgroup one-wide">
        <li class="option">
            <div class="wrapper">
                <%= string.Format("<input type=\"checkbox\" name=\"IsAllDay\" id=\"EditPrivateDutyTask_IsAllDay\" value=\"true\" {0} />", Model.IsAllDay.ToChecked())%>
                <label for="EditPrivateDutyTask_IsAllDay">All Day Event</label>
            </div>
        </li>
    </ul>
</div>