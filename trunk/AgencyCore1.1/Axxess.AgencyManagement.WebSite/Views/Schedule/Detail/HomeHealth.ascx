﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<div class="row">
    <label for="Schedule_Detail_EventDate" class="fl">Scheduled Date</label>
    <div class="fr"><input type="text" class="date-picker required" name="EventDate" value="<%= Model.EventDate.ToString("MM/dd/yyyy") %>" id="Schedule_Detail_EventDate" /></div>
</div>
<div class="row">
    <label for="Schedule_Detail_VisitDate" class="fl">Actual Visit Date</label>
    <div class="fr"><input type="text" class="date-picker required" name="VisitDate" value="<%= Model.VisitDate.ToString("MM/dd/yyyy") %>" id="Schedule_Detail_VisitDate" /></div>
</div>
<div class="row">
    <label for="Schedule_Detail_TimeIn" class="fl"><%= Model.DisciplineTask == (int)DisciplineTasks.PASTravel ? "Travel Start Time" : "Time In" %></label>
    <div class="fr"><input type="text" id="Schedule_Detail_TimeIn" name="TimeIn" class="time-picker" value="<%= Model.TimeIn %>" /></div>
</div>
<div class="row">
    <label for="Schedule_Detail_TimeOut" class="fl"><%= Model.DisciplineTask == (int)DisciplineTasks.PASTravel ? "Travel End Time" : "Time Out" %></label>
    <div class="fr"><input type="text" id="Schedule_Detail_TimeOut" name="TimeOut" class="time-picker" value="<%= Model.TimeOut %>" /></div>
</div>