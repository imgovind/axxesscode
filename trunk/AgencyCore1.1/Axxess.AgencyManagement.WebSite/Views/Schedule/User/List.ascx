﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<%  var pagename = "UserSchedule"; %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Schedule and Tasks | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
	<%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" }) %> 
    <div class="fr ac">
    <%  if (Model.IsUserCanExport) { %>
        <div class="button"><a url="Export/ScheduleList" class="export">Excel Export</a></div>
        <div class="clr"></div>
    <%  } %>
        <div class="button"><a class="grid-refresh" url="Schedule/UserScheduleListContent">Refresh</a></div>
    </div>
    <fieldset class="grid-controls ac">
	     <ul class="buttons ac">
            <li><a class="group-button" grouping="PatientName">Group By Patient</a></li>
            <li><a class="group-button" grouping="EventDate">Group By Date</a></li>
            <li><a class="group-button" grouping="TaskName">Group By Task</a></li>
        </ul>
        <p>
            Note: This list shows you items/tasks dated 3 months into the past and 2 weeks into
            the future. To find older items, look in the Patient&#8217;s Chart or Schedule Center.
        </p>
    </fieldset>
    <div id="<%= pagename %>GridContainer" init="User.InitUserScheduleContent"><% Html.RenderPartial("User/ListContent",Model); %></div>
</div>