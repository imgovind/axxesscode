﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<%  var data = Model.GroupName; %>
<%  var sortParams = string.Format("{0}-{1}",Model.SortColumn,Model.SortDirection); %>
<%= Html.Filter("GroupName", data, new { @class = "grouping" })%>
<%= Html.Filter("SortParams", sortParams, new { @class = "sort-parameters" })%>
<%= Html.Telerik().Grid(Model.UserEvents).Name("UserSchedule_Grid").HtmlAttributes(new { @class = "user-myschedule-activity" }).Columns(columns => {
        columns.Bound(v => v.PatientName);
        columns.Bound(v => v.TaskName).Template(v => v.IsEditable ? "<a class=\"edit link\">" + v.TaskName + "</a>" : v.TaskName).HtmlAttributes(new { @class = "edit-task" }).Title("Task").Width(250);
        columns.Bound(v => v.ScheduleDate).Format("{0:MM/dd/yyyy}").Title("Date").Width(100);
        columns.Bound(v => v.StatusName).Width(200).Title("Status");
        columns.Bound(v => v.StatusComment).Title(" ").Sortable(false).Width(30).Template(v => v.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 red note\">{0}</a>", v.StatusComment) : string.Empty).Visible(Model.IsStickyNote);
        columns.Bound(v => v.VisitNotes).Title(" ").Sortable(false).Width(30).Template(v => v.VisitNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 yellow note\">{0}</a>", v.VisitNotes) : string.Empty).Visible(Model.IsStickyNote);
        columns.Bound(v => v.EpisodeNotes).Title(" ").Sortable(false).Width(30).Template(v => v.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"img icon16 blue note\">{0}</a>", v.EpisodeNotes) : string.Empty).Visible(Model.IsStickyNote);
        columns.Bound(v => v.Id).Template(v => v.IsMissedVisitReady ? "<a class=\"mark-missedvisit link\">Missed Visit Form</a>" : string.Empty).Title(" ").Width(150).Sortable(false).Visible(!Current.IsAgencyFrozen);
        columns.Bound(s => s.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "id" }).Width(0).Sortable(false).Hidden(true);
		columns.Bound(s => s.PatientId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "pid" }).Width(0).Sortable(false).Hidden(true);
		columns.Bound(s => s.Id).Template(v => Model.Service.ToString()).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "service" }).Width(0).Sortable(false).Hidden(true);
		columns.Bound(s => s.Type).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;", @class = "type" }).Width(0).Sortable(false).Hidden(true);
    }).Groupable(settings => settings.Groups(groups => {
        if (data == "PatientName") groups.Add(s => s.PatientName);
        else if (data == "ScheduleDate") groups.Add(s => s.VisitDate);
        else if (data == "TaskName") groups.Add(s => s.TaskName);
        else groups.Add(s => s.ScheduleDate);
    })).Scrollable().Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = Model.SortColumn;
        var sortDirection = Model.SortDirection;
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "ScheduleDate") {
            if (sortDirection == "ASC") order.Add(o => o.ScheduleDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.ScheduleDate).Descending();
        } else if (sortName == "TaskName") {
            if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        } else if (sortName == "StatusName") {
            if (sortDirection == "ASC") order.Add(o => o.StatusName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.StatusName).Descending();
        }
    })).Footer(false) %>