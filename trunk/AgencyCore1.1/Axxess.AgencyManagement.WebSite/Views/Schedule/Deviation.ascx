﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceTaskViewData>" %>
<%  string pagename = "AgencyScheduleDeviation"; %>
<span class="wintitle">Schedule Deviation | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pagename, new { @class = "pagename", @id = pagename + "_PageName" }) %>
    <div class="fr ac">
<%  if (Model.ExportPermissions.Has(Model.Service)) { %>
        <div class="button"><a class="export" url="ExportScheduleDeviations">Excel Export</a></div>
    	<div class="clr"></div>
<%  } %>
        <div class="button"><a url="Schedule/DeviationContent" class="grid-refresh">Refresh</a></div>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter">
            <label for="<%= pagename %>_BranchId">Branch</label>
            <%= Html.BranchList("BranchId", Model.LocationId.ToString(), (int)Model.Service, new { @id = pagename + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_StartDate">Date Range</label>
            <input type="text" class="date-picker short" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
            <label for="<%= pagename %>_EndDate">&minus;</label>
            <input type="text" class="date-picker short" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer"><% Html.RenderPartial("DeviationContent", Model); %></div>
</div>