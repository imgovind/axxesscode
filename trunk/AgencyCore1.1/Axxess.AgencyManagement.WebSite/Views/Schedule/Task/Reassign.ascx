﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Reassign", "Schedule", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "ReassignTask_Form" , @class = "mainform"})) { %>
	<%= Html.Hidden("Id", Model.Id) %>
	<%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
		<legend>Reassign Task</legend>
        <div class="wide-column">
			<div class="row">
                <label class="fl" for="ReassignTask_Task">Task</label>
                <div class="fr"><%= Model.Type %></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignTask_Patient">Patient</label>
                <div class="fr"><%= Model.PatientDisplayName %></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignTask_ScheduledDate">Scheduled Date/Time</label>
                <div class="fr"><%= Model.EventDate %></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignTask_OldUserId">Existing User/Employee</label>
                <div class="fr"><%= Model.OldUserName %></div>
            </div>
            <div class="row">
                <label class="fl" for="ReassignTask_NewUserId">New User/Employee</label>
                <div class="fr"><%= Html.UsersFilteredByService((int)Model.Service, "UserId", string.Empty, new { @id = "ReassignTask_NewUserId", @class = "required not-zero" })%></div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Reassign</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>