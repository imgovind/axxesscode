﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientNameViewData>" %>
<span class="wintitle">Patient Deleted Tasks/Documents History | <%= Model.DisplayName %></span>
<%  var pageName = "DeletedTasks"; %>
<%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" })%>  
    <div class="button fr">
		<a class="grid-refresh" area="<%=area %>" url="/Schedule/DeletedTaskHistoryList">Refresh</a>
    </div>
    <fieldset class="grid-controls ac">
    <%  if (!Model.AvailableService.IsAlone()) { %>
        <div class="filter optional">
            <label for="<%= pageName%>_Service">Service</label>
            <%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, new { @class = "service", @id = pageName + "_Service" }) %>
        </div>
    <%  } else { %>
        <div class="filter"><%= Html.Hidden("ServiceId", (int)Model.Service, new { @class = "service", @id = pageName + "_Service" })%></div>
    <%  } %>
        <div class="filter grid-search no-left-margin rel"></div>
        <div class="filter"><%= Html.Hidden("PatientId", Model.PatientId, new { @id = pageName+"_PatientId" }) %></div>
    </fieldset>
	<%= Html.Telerik().Grid<DeletedTaskViewData>().Name(pageName + "_Grid").Columns(columns => {
            columns.Bound(e => e.TaskName).Title("Task/Document");
            columns.Bound(e => e.DateFormatted).Title("Scheduled Time").Width(150);
            columns.Bound(e => e.Status).Title("Status");
            columns.Bound(e => e.User).Title("User");
            columns.Bound(e => e.Id).Sortable(false).ClientTemplate("<a onclick=\"Schedule.Task." + Model.Service.ToString() + ".Restore('<#=PatientId#>','<#=Id#>')\" class=\"link\">Restore</a>").Title("Action").Width(100).Visible(!Current.IsAgencyFrozen);
        }).DataBinding(dataBinding => dataBinding.Ajax().OperationMode(GridOperationMode.Client).Select("DeletedTaskHistoryList", "Schedule", new { area = Model.Service.ToArea(), patientId = Model.PatientId })).ClientEvents(evnts => evnts
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("U.OnTGridDataBound")
            .OnError("U.OnTGridError")
	    ).NoRecordsTemplate("No deleted Tasks/Documents found.").Footer(false).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>