﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<span class="wintitle">Master Calendar | <%= Model.DisplayName %></span>
<div id="masterCalendarResult"><% Html.RenderPartial("MasterCalendar", Model); %></div>



