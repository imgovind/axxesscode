﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddMultiDaySchedule", "Schedule", FormMethod.Post, new { @id = "multiDayScheduleForm", @class = "mainform" })) { %>
    <%  DateTime[] startdate = new DateTime[3]; %>
    <%  DateTime[] enddate = new DateTime[3]; %>
    <%  DateTime[] currentdate = new DateTime[3]; %>
    <%  startdate[0] = DateUtilities.GetStartOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
    <%  enddate[0] = DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
    <%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
    <%  startdate[1] = enddate[0].AddDays(1); %>
    <%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
    <%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
    <%  startdate[2] = enddate[1].AddDays(1); %>
    <%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
    <%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "multiDayScheduleEpisodeId" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "multiDaySchedulePatientId" })%>
    <%= Html.Hidden("EventDates", "", new { @id = "multiDayScheduleVisitDates" })%>
    <fieldset>
        <legend>Schedule Employee</legend>
        <div class="column">
            <div class="row">
                <label class="fl">Patient</label>
                <label class="fr"><%= Model.DisplayName %></label>
            </div>
            <div class="row">
                <label class="fl">Episode</label>
                <label class="fr"><%= Model.StartDate.ToShortDateString() %> &#8211; <%= Model.EndDate.ToShortDateString() %></label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="multiDayScheduleUserID" class="fl">User/Employee</label>
                <div class="fr"><%= Html.Users("UserId", "", "-- Select User --", Guid.Empty, (int)UserStatus.Active, (int)AgencyServices.HomeHealth, new { @id = "multiDayScheduleUserID", @class = "user-selector required not-zero" }) %></div>
            </div>
            <div class="row">
                <label for="multiDayScheduleDisciplineTask" class="fl">Task</label>
                <div class="fr"><%= Html.MultipleDisciplineTasks("DisciplineTaskId", "", new { @id = "multiDayScheduleDisciplineTask", @class = "MultipleDisciplineTask required" }) %></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label class="fl">To schedule visits for the selected user, click on the desired dates in the calendar below.</label>
                <div class="ac">
    <%  for (int c = 0; c < 3; c++) { %>
                    <div class="cal">
                        <table>
                            <thead class="ac">
                                <tr>
                                    <th colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></th>
                                </tr>
                                <tr>
                                    <th>Su</th>
				                    <th>Mo</th>
				                    <th>Tu</th>
				                    <th>We</th>
				                    <th>Th</th>
				                    <th>Fr</th>
				                    <th>Sa</th>
			                    </tr>
		                    </thead>
					        <tbody>
        <%  var weekNumber = DateUtilities.Weeks(startdate[c].Month, startdate[c].Year) - ((int)startdate[c].DayOfWeek == 0 ? 1 : 0); %>
        <%  for (int i = 0; i <= weekNumber; i++) { %>
						        <tr>
			<%  int addedDate = (i) * 7; %>
			<%  for (int j = 0; j <= 6; j++) { %>
			    <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
			    <%  if (specificDate < Model.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate > Model.EndDate.AddDays(1)) { %>
									<td class="inactive"></td>
				<%  } else { %>
									<td date="<%= specificDate.ToShortDateString() %>">
										<div class="datelabel"><%= specificDate.Day %></div>
									</td>
				<%  } %>
			<%  } %>
						        </tr>
		<%  } %>
					        </tbody>
	                    </table>
	                </div>
    <%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="close">Close</a></li>
    </ul>
<%  } %>
</div>