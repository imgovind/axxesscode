﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<%  string pagename = "DeleteMultiple"; %>
<div class="wrapper main blue">
	<div id="<%= pagename %>GridContainer" class="grid-container">
		<% Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "_Grid").HtmlAttributes(new { @class = "button-bottom-bar" }).Columns(columns => {
                columns.Bound(s => s.Id).ClientTemplate("<input name='EventId' type='checkbox' value='<#= Id #>' />").Title("").Sortable(false).Width(20).HeaderTemplate(ht => "<input id='" + pagename + "_SelectAll' class='radio selectall' type='checkbox' style='margin:0' />");
                columns.Bound(s => s.DisciplineTaskName).Title("Task").Width(140);
                columns.Bound(s => s.EventDate).Format("{0:MM/dd/yyyy}").Title("Scheduled Date").Width(80);
                columns.Bound(s => s.UserName).Title("Assigned To").Width(110);
                columns.Bound(s => s.StatusName).Title("Status").Width(100);
                columns.Bound(s => s.IsComplete).Visible(false);
            })
			.ClientEvents(c => c
                .OnDataBinding("U.OnTGridDataBinding")
                .OnDataBound("U.OnTGridDataBound")
                .OnError("U.OnTGridError")
				.OnRowSelect("Schedule.Delete.ActivityRowSelected")
				.OnRowDataBound("Schedule.Delete.OnRowDataBound"))
			.DataBinding(dataBinding => dataBinding.Ajax()
				.Select("DeleteList", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId }))
			.Sortable().Selectable().Scrollable().Footer(false).Render(); %>
	</div>
	<div id="<%=pagename %>_BottomPanel" class="buttons t-grid-footer">
		<ul class="buttons ac">
<%  if (!Current.IsAgencyFrozen) { %>
			<li><a class="delete">Delete</a></li>
<%  } %>
			<li><a class="close">Close</a></li>
		</ul>
	</div>
</div>