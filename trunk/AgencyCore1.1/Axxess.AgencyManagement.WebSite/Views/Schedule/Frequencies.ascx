﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FrequenciesViewData>" %>
<div id="Episode_Frequencies" class="wrapper main">
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Episode Frequencies</h3></li>
            <li>
                <span class="grid-third">Visit Type</span>
                <span class="grid-third">Frequency</span>
                <span>Visit Count</span>
            </li>
        </ul>
	<%  if (Model.Visits.Count > 0) { %>
        <ol>
			<% int i = 1; %>
			<% foreach (var pair in Model.Visits) { %>
				<li>
					<span class="grid-third"><%= pair.Key %></span>
					<span class="grid-third"><%= pair.Value.Frequency %></span>
					<span><%= pair.Value.Count %></span>
				</li>
			<%  i++; %>
			<%  } %>
        </ol>
	<% } else { %> 
	    <h4>No Frequencies Set</h4>
	<% } %>
    </div>
    <ul class="buttons ac">
        <li><a class="close">Close</a></li>
    </ul>
</div>