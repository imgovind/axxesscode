﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Incident>" %>
<span class="wintitle"><%= Model.Service.GetDescription() %> |Edit Incident/Accident Log | <%= Current.AgencyName%></span>
<div class="wrapper main">
<%  if (Model.IsUserCanEdit) { %>
    <%  using (Html.BeginForm("Update", "Incident", FormMethod.Post, new { @area = Model.Service.ToArea(), @class = "mainform", @id = "editIncidentReportForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditIncident_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditIncident_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditIncident_UserId" })%>
        <%  if (Model != null) Model.SignatureDate = DateTime.Today; %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label class="fl">Patient Name</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
        <%  if (Model.EpisodeId.IsEmpty()) { %> 
            <div class="row">
                <%= Html.CarePeriodLabel((int)Model.Service, new { @for = "EditIncident_EpisodeId", @class = "fl" })%>
                <div class="fr"><%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.PatientId, Guid.Empty.ToString(), new { @id = "EditIncident_EpisodeId", @class = "required not-zero" })%></div> 
            </div>
        <%  } else if (Model.EpisodeStartDate.IsNotNullOrEmpty()) { %> 
            <div class="row">
                <%= Html.CarePeriodLabel((int)Model.Service, new { @class = "fl" })%>
                <div class="fr"><%= string.Format("{0}-{1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></div>
                <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "EditIncident_EpisodeId" })%>
            </div>
        <%  } %>
            <div class="row">
                <label for="EditIncident_PhysicianId" class="fl">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "EditIncident_PhysicianId", @class = "physician-picker" })%>
					<% if(Model.IsUserCanAddPhysicain) { %>
					<br/>
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                    <% } %>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditIncident_IncidentDate" class="fl">Date of Incident</label>
                <div class="fr"><input type="text" class="date-picker required" name="IncidentDate" id="EditIncident_IncidentDate" value="<%= Model.IncidentDate.IsValid() ? Model.IncidentDate.ToShortDateString() : string.Empty %>" /></div>
            </div>
            <div class="row">
                <label for="EditIncident_IncidentType" class="fl">Type of Incident</label>
                <div class="fr"><%= Html.TextBox("IncidentType", Model.IncidentType, new { @id = "EditIncident_IncidentType", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditIncident_MDNotified" class="fl">Doctor Notified</label>
                <div class="fr">
                    <%= Html.DropDownList("MDNotified", new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" },
                            new SelectListItem { Text = "N/A", Value = "NA" }
                        }, "Value", "Text", Model.MDNotified).AsEnumerable(), new { @id = "Edit_Infection_MDNotified" }) %>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_FamilyNotified" class="fl">Family/CG Notified</label>
                <div class="fr">
                     <%= Html.DropDownList("FamilyNotified", new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" },
                            new SelectListItem { Text = "N/A", Value = "NA" }
                        }, "Value", "Text", Model.FamilyNotified).AsEnumerable(), new { @id = "Edit_Infection_FamilyNotified" }) %>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_NewOrdersCreated" class="fl">New Orders</label>
                <div class="fr">
                     <%= Html.DropDownList("NewOrdersCreated", new SelectList(new[] {
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No" },
                            new SelectListItem { Text = "N/A", Value = "NA" }
                        }, "Value", "Text", Model.NewOrdersCreated).AsEnumerable(), new { @id = "Edit_Infection_NewOrdersCreated" }) %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) Involved</legend>
        <%  string[] individualsInvolved = Model.IndividualInvolved.IsNotNullOrEmpty() ? Model.IndividualInvolved.Split(';') : null;  %>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupOption("IndividualInvolvedArray", "Patient", individualsInvolved != null && individualsInvolved.Contains("Patient"), "Patient")%>
                    <%= Html.CheckgroupOption("IndividualInvolvedArray", "Caregiver", individualsInvolved != null && individualsInvolved.Contains("Caregiver"), "Caregiver")%>
                    <%= Html.CheckgroupOption("IndividualInvolvedArray", "Employee/Contractor", individualsInvolved != null && individualsInvolved.Contains("Employee/Contractor"), "Employee/Contractor")%>
                    <li class="option">
                        <div class="wrapper">
                            <%= string.Format("<input id='EditIncident_IndividualInvolved4' type='checkbox' value='Other' name='IndividualInvolvedArray' class='required' {0} />", individualsInvolved != null && individualsInvolved.Contains("Other") ? "checked='checked'" : "")%>
                            <label for="EditIncident_IndividualInvolved4">Other</label>
                        </div>
                        <div class="more">
                            <label for="EditIncident_IndividualInvolvedOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox("IndividualInvolvedOther", Model.IndividualInvolvedOther, new { @id = "EditIncident_IndividualInvolvedOther", @maxlength = "100" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <div class="wide-column">
            <div class="row">
                <label for="EditIncident_Description">Describe Incident/Accident</label>
                <div class="ac"><%= Html.TextArea("Description", Model.Description, new { @id = "EditIncident_Description", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="EditIncident_ActionTaken">Action Taken/Interventions Performed</label>
                <div class="ac"><%= Html.TextArea("ActionTaken", Model.ActionTaken, new { @id = "EditIncident_ActionTaken", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="EditIncident_Orders">Orders</label>
                <div class="ac"><%= Html.TextArea("Orders", Model.Orders, new { @id = "EditIncident_Orders", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="EditIncident_FollowUp">Follow Up</label>
                <div class="ac"><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "EditIncident_FollowUp", @class = "taller" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Signature</legend>
        <div class="column">
            <div class="row">
                <label for="EditIncident_ClinicianSignature" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @class = "complete-required", @id = "EditIncident_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditIncident_SignatureDate" class="fl">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="EditIncident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "EditIncident_Status" })%>
    <%= Html.Hidden("SaveStatus", 515, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 520, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Exit</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("edit a incident log")%>
<%  } %>
</div>