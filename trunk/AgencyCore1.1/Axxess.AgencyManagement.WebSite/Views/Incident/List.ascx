﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GridViewData>" %>
<span class="wintitle">List Incidents | <%= Current.AgencyName %></span>
<%  if (Model.Service != AgencyServices.None && Model.ViewListPermissions != AgencyServices.None) { %>
    <%  var pageName = "IncidentList"; %>
    <%  var area = Model.Service.ToArea(); %>
<div class="wrapper main blue">
	<%= Html.Hidden("PageName", pageName, new { @class = "pagename", @id = pageName + "_PageName" }) %> 
    <ul class="fr buttons">
        <%  if (Model.NewPermissions!=AgencyServices.None) { %>
	    <li><a class="new-incident-log servicepermission" service="<%=(int)Model.NewPermissions %>">New Incident</a></li><div class="clr"></div>
		<%  } %>
        <%  if (Model.ExportPermissions!=AgencyServices.None) { %>
		<li><a area="<%=area %>" url="/Export/Incidents" service="<%=(int)Model.ExportPermissions %>" class="export servicepermission">Excel Export</a></li><div class="clr"></div>
		<%  } %>
		<li><a area="<%=area %>" url="/Incident/ListContent" class="grid-refresh">Refresh</a></li>
    </ul>
    <fieldset class="grid-controls ac">
        <div class="filter optional"><%= Html.LimitedAgencyServicesByCurrentUser("ServiceId", Model.Service, Model.AvailableService, true, pageName, new { @class = "service input", @id = pageName + "_ServiceId" }) %></div>
        <div class="filter grid-search"></div>
    </fieldset>
	<div class="clr"></div>
    <%= Html.Telerik().Grid<Incident>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "grid-container aggregated" }).Columns(columns => {
			columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
			columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
			columns.Bound(i => i.IncidentType).Title("Type of Incident").Sortable(true);
			columns.Bound(i => i.IncidentDateFormatted).Title("Incident Date").Sortable(true).Aggregate(aggregates => aggregates.Count()).ClientFooterTemplate("Total: <#= $.telerik.formatString('{0:n,0}', Count) #>");
			columns.Bound(i => i.StatusName).Title("Status").Sortable(true);
            columns.Bound(i => i.IsUserCanPrint).ClientTemplate("").HtmlAttributes(new { @class = "centered-unpadded-cell" }).Title(" ").Width(35).Sortable(false);
			columns.Bound(i => i.Id).ClientTemplate("").Sortable(false).Title("Action").Width(100).Visible(!Current.IsAgencyFrozen);
		}).DataBinding(dataBinding => dataBinding.Ajax().Select("ListContent", "Incident", new { area =area }).OperationMode(GridOperationMode.Client)).NoRecordsTemplate("No Incidents found.").ClientEvents(events => events
            .OnDataBinding("U.OnTGridDataBinding")
            .OnDataBound("Incident.OnDataBound")
            .OnRowDataBound("Incident.OnRowDataBound")
            .OnError("U.OnTGridError")
        ).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<%  } else { %>
<div class="wrapper main"><%= Html.NotAuthorized("view the list of incident logs") %></div>
<%  } %>