﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ServiceAndGuidViewData>" %>
<span class="wintitle"><%= Model.Service.GetDescription() %> | New Incident/Accident Log | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  if (Model.IsUserCanAdd) { %>
    <%  using (Html.BeginForm("Add", "Incident", FormMethod.Post, new { area = Model.Service.ToArea(), @id = "newIncidentReportForm", @class = "mainform"})) { %>
    <%= Html.Hidden("ServiceId", (int)Model.Service, new { @id = "NewIncident_ServiceId", @disabled = "disabled" })%>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewIncident_PatientId" class="fl">Patient</label>
                <div class="fr"><%= Html.PatientsFilteredByService((int)Model.Service, "PatientId", Model.Id.ToString(), new { @id = "NewIncident_PatientId", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <%= Html.CarePeriodLabel((int)Model.Service, new { @for = "NewIncident_EpisodeList", @class = "fl" })%>
                <div class="fr"><%= Html.PatientCarePeriods((int)Model.Service, "EpisodeId", Model.Id, Guid.Empty.ToString(), new { @id = "NewIncident_EpisodeList", @class = "required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="NewIncident_IncidentType" class="fl">Type of Incident</label>
                <div class="fr"><%= Html.TextBox("IncidentType", "", new { @id = "NewIncident_IncidentType", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewIncident_IncidentDate" class="fl">Date of Incident</label>
                <div class="fr"><input type="text" class="date-picker required" name="IncidentDate" id="NewIncident_IncidentDate" /></div>
            </div>
            <div class="row">
               
                <label for="NewIncident_PhysicianId" class="fl">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("PhysicianId", "", new { @id = "NewIncident_PhysicianId", @class = "physician-picker" })%><br />
                     <% if(Model.IsUserCanAddPhysicain) { %>
                    <div class="ancillary-button"><a onclick="Physician.New(true)">New Physician</a></div>
                      <% } %>
                </div>
            </div>
         </div>
         <div class="column">
            <div class="row">
                <label for="NewIncident_MDNotified" class="fl">M.D. Notified?</label>
                <div class="fr">
                    <%= Html.DropDownList("MDNotified",  new List<SelectListItem>{
                            new SelectListItem() { Text = "Yes", Value = "Yes" },
                            new SelectListItem() { Text = "No", Value = "No" },
                            new SelectListItem() { Text = "N/A", Value = "NA" }
                        }, new { @id = "New_Infection_MDNotified" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_FamilyNotified" class="fl">Family/CG Notified?</label>
                <div class="fr">
                    <%= Html.DropDownList("FamilyNotified",  new List<SelectListItem>{
                            new SelectListItem() { Text = "Yes", Value = "Yes" },
                            new SelectListItem() { Text = "No", Value = "No" },
                            new SelectListItem() { Text = "N/A", Value = "NA" }
                        }, new { @id = "New_Infection_FamilyNotified" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_NewOrdersCreated" class="fl">New Orders?</label>
                <div class="fr">
                    <%= Html.DropDownList("NewOrdersCreated",  new List<SelectListItem>{
                            new SelectListItem() { Text = "Yes", Value = "Yes" },
                            new SelectListItem() { Text = "No", Value = "No" },
                            new SelectListItem() { Text = "N/A", Value = "NA" }
                        }, new { @id = "New_Infection_NewOrdersCreated" })%>
                </div>
            </div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) Involved</legend>
        <div class="wide-column">
            <div class="row">
                <ul class="checkgroup four-wide">
                    <%= Html.CheckgroupOption("IndividualInvolvedArray", "Patient", false, "Patient")%>
                    <%= Html.CheckgroupOption("IndividualInvolvedArray", "Caregiver", false, "Caregiver")%>
                    <%= Html.CheckgroupOption("IndividualInvolvedArray", "Employee/Contractor", false, "Employee/Contractor")%>
                    <li class="option">
                        <div class="wrapper">
                            <input id="NewIncident_IndividualInvolved4" type="checkbox" value="Other" name="IndividualInvolvedArray" />
                            <label for="NewIncident_IndividualInvolved4">Other</label>
                        </div>
                        <div class="more">
                            <label for="NewIncident_IndividualInvolvedOther" class="fl">Specify</label>
                            <div class="fr"><%= Html.TextBox("IndividualInvolvedOther", "", new { @id = "NewIncident_IndividualInvolvedOther", @maxlength = "100" })%></div>
                            <div class="clr"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewIncident_Description">Describe Incident/Accident</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("DescriptionTemplates")%>
                    <%= Html.TextArea("Description", new { @id = "NewIncident_Description", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_ActionTaken">Action Taken/Interventions Performed</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("ActionTakenTemplates")%>
                    <%= Html.TextArea("ActionTaken", new { @id = "NewIncident_ActionTaken", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_Narrative">Orders</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("NarrativeTemplates") %>
                    <%= Html.TextArea("Orders", new { @id = "NewIncident_Narrative", @class = "taller" })%>
                </div>
            </div>
            <div class="row">
                <label for="FollowUp">Follow Up</label>
                <div class="template-text">
                    <%= Html.ToggleTemplates("FollowUpTemplates")%>
	                <%= Html.TextArea("FollowUp", new { @id = "NewIncident_FollowUp", @class = "taller" })%>
                </div>
            </div>
        </div>  
    </fieldset>
    <fieldset>
        <legend>Signature</legend>
        <div class="column">
            <div class="row">
                <label for="NewIncident_ClinicianSignature" class="fl">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @class = "complete-required", @id = "NewIncident_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewIncident_SignatureDate" class="fl">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="NewIncident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "NewIncident_Status" })%>
    <%= Html.Hidden("SaveStatus", 515, new { @disabled = "true" })%>
    <%= Html.Hidden("CompleteStatus", 520, new { @disabled = "true" })%>
    <ul class="buttons ac">
        <li><a class="save close">Save</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul>
    <%  } %>
<%  } else { %>
    <%= Html.NotAuthorized("create a new incident log")%>
<%  } %>
</div>