﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<%var IdPrefix = ViewData["IdPrefix"];%>
<fieldset class="newmed">
        <legend>New Visit Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="<%=IdPrefix %>_Task" class="fl">Task</label>
                <div class="fr"><%=Html.InsuranceDisciplineTask("Id", 0, Model.ExistingNotes, true, new { @id = IdPrefix + "_Task", @class = "task required not-zero" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_PreferredDescription" class="fl">Preferred Description</label>
                <div class="fr"><%= Html.TextBox("PreferredDescription", "", new { @id = IdPrefix + "_PreferredDescription", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_RevenueCode" class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", "", new { @id = IdPrefix + "_RevenueCode", @class = "Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_HCPCS" class="fl">HCPCS Code</label>
                <div class="fr"><%= Html.TextBox("Code", "", new { @id = IdPrefix + "_HCPCS", @maxlength = "100" })%></div>
            </div>
             <%if (Model.IsExpectedRateNeeded)
               {%>
            <div class="row" id="<%=IdPrefix %>_ExpectedRate_Div">
                <label for="<%=IdPrefix %>_ExpectedRate" class="fl">Expected Rate</label>
                <div class="fr"><%= Html.TextBox("ExpectedRate", "", new { @id = IdPrefix + "_ExpectedRate", @class = "currency", @maxlength = "100" })%></div>
            </div>
            <%} %>
            <div class="row">
                <label for="<%=IdPrefix %>_ChargeRate" id="<%=IdPrefix %>_ChargeRate_Label" class="fl">Rate</label>
                <div class="fr"><%= Html.TextBox("Charge", "", new { @id = IdPrefix + "_ChargeRate", @class="currency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_Modifier" class="fl">Modifier</label>
                <div class="fr">
                    <%= Html.TextBox("Modifier", "", new { @id = IdPrefix + "_Modifier", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", "", new { @id = IdPrefix + "_Modifier2", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", "", new { @id = IdPrefix + "_Modifier3", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", "", new { @id = IdPrefix + "_Modifier4", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_ChargeType" class="fl">Service Unit Type</label>
                <div class="fr"><%=Html.UnitType("ChargeType", "0", new { @id = IdPrefix + "_ChargeType", @class = "required not-zero" })%></div>
            </div>
            <% if (Model.IsMedicareHMO){ %>
             <div class="row hidden medicarehmoratecontent" id="<%=IdPrefix %>_MedicareHMORateContent">
                <label for="<%=IdPrefix %>_MedicareHMORate" class="fl">Medicare HMO Rate</label>
                <div class="fr"><%= Html.TextBox("MedicareHMORate", "", new { @id = IdPrefix + "_MedicareHMORate", @class = "currency", @maxlength = "100" })%></div>
                 <div class="clear"></div>
                <em>This rate applies for the Medicare final claim per visit.</em>
            </div>
            <%} %>
              <div class="row pervisitunitcontent" id="<%=IdPrefix %>_PerVisitUnitContent">
                <label for="<%=IdPrefix %>_Unit" class="fl">Service Units per Visit</label>
                <div class="fr"><%= Html.TextBox("Unit", "1", new { @id = IdPrefix + "_Unit", @class = "numeric", @maxlength = "1" })%></div>
                <div class="clear"></div>
                <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit.</em>
            </div>
            <div class="row">
                <%= Html.CheckBox("IsUnitsPerDayOnSingleLineItem", false, new { @id = IdPrefix + "_IsUnitsPerDayOnSingleLineItem", @class = "fl" })%>
                <label for="<%=IdPrefix %>_IsUnitsPerDayOnSingleLineItem">Check if the units of this visit type are totaled per day on a single line.</label>
            </div>
            <div id="<%=IdPrefix %>_IsTimeLimitContent" class="hidden chargetypetimelimitcontent">
                <div class="row">
                    <%= Html.CheckBox("IsTimeLimit", false, new { @id = IdPrefix + "_IsTimeLimit", @class = "fl" })%>
                    <label for="<%=IdPrefix %>_IsTimeLimit">Check if a time limit applies per unit.</label>
                </div>
                <div class="margin hidden timelimitcontent" id="<%=IdPrefix %>_TimeLimitContent">
                    <div class="row">
                        <label for="<%=IdPrefix %>_TimeLimit" class="fl">Time Limit</label>
                        <div class="fr">
                            Hours <%= Html.TextBox("TimeLimitHour", "", new { @id = IdPrefix + "_TimeLimitHour", @class = "shortest", @maxlength = "2" })%>
                            Minutes <%= Html.TextBox("TimeLimitMin", "", new { @id = IdPrefix + "_TimeLimitMin", @class = "shortest", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondDescription" class="fl">Description</label>
                        <div class="fr"><%= Html.TextBox("SecondDescription", "", new { @id = IdPrefix + "_SecondDescription", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondRevenueCode" class="fl">Revenue Code</label>
                        <div class="fr"><%= Html.TextBox("SecondRevenueCode", "", new { @id = IdPrefix + "_SecondRevenueCode", @class = "Frequency", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondCode" class="fl">HCPCS Code</label>
                        <div class="fr"><%= Html.TextBox("SecondCode", "", new { @id = IdPrefix + "_SecondCode", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsSecondChargeDifferent", false, new { @id = IdPrefix + "_IsSecondChargeDifferent", @class = "fl" })%>
                         <label for="<%=IdPrefix %>_IsSecondChargeDifferent">Check if a rate is different.</label>
                    </div>
                    <div id="<%=IdPrefix %>_SecondRateContent" class="hidden secondratecontent">
                     <%if (Model.IsExpectedRateNeeded)
                       {%>
                        <div class='row' id="<%=IdPrefix %>_SecondExpectedRateContent">
                             <label for="<%=IdPrefix %>_SecondExpectedRate" class="fl">Expected Rate</label>
                             <div class="fr"><%= Html.TextBox("SecondExpectedRate", "", new { @id = IdPrefix + "_SecondExpectedRate", @class = "currency", @maxlength = "100" })%></div>
                        </div>
                        <%} %>
                        <div class="row" id="<%=IdPrefix %>_SecondChargeContent">
                             <label for="<%=IdPrefix %>_SecondCharge" class="fl" id="<%=IdPrefix %>_SecondChargeLabel">Rate</label>
                             <div class="fr"><%= Html.TextBox("SecondCharge", "", new { @id = IdPrefix + "_SecondCharge", @class = "currency", @maxlength = "100" })%></div>
                        </div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondModifier" class="fl">Modifier</label>
                        <div class="fr">
                            <%= Html.TextBox("SecondModifier", "", new { @id = IdPrefix + "_SecondModifier", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", "", new { @id = IdPrefix + "_SecondModifier2", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", "", new { @id = IdPrefix + "_SecondModifier3", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", "", new { @id = IdPrefix + "_SecondModifier4", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondChargeType" class="fl">Unit Type</label>
                        <div class="fr"><%=Html.UnitType("SecondChargeType", "0", new { @id = IdPrefix + "_SecondChargeType" })%></div>
                   </div>
                    <div class="row secondpervisitunitcontent" id="<%=IdPrefix %>_SecondPerVisitUnitContent">
                        <label for="<%=IdPrefix %>_SecondUnit" class="fl">Service Units per Visit</label>
                        <div class="fr"><%= Html.TextBox("SecondUnit", "1", new { @id = IdPrefix + "_SecondUnit", @class = "numeric", @maxlength = "1" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsUnitPerALineItem", false, new { @id = IdPrefix + "_IsUnitPerALineItem", @class = "fl" })%>
                         <label for="<%=IdPrefix %>_IsUnitPerALineItem">Check if a unit apply per a line item.</label>
                    </div>
                </div>
            </div>
        </div>   
    </fieldset>
