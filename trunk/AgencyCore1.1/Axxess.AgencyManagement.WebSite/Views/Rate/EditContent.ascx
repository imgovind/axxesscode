﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<%var IdPrefix = ViewData["IdPrefix"];%>
<fieldset class="newmed">
        <legend>Edit Visit Information</legend>
        <div class="wide_column">
            <div class="row">
                <label for="<%=IdPrefix %>_Task" class="fl">Task</label>
                <div class="fr"><%= Model.DisciplineTaskName  %></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_PreferredDescription" class="fl">Preferred Description</label>
                <div class="fr"><%= Html.TextBox("PreferredDescription", Model.PreferredDescription, new { @id = IdPrefix + "_PreferredDescription", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_RevenueCode" class="fl">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = IdPrefix + "_RevenueCode", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_Code" class="fl">HCPCS Code</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = IdPrefix + "_Code", @maxlength = "100" })%></div>
            </div>
            <%if (Model.IsExpectedRateNeeded) {%>
            <div class="row" id="<%=IdPrefix %>_ExpectedRate_Div">
                <label for="<%=IdPrefix %>_ExpectedRate" class="fl">Expected Rate</label>
                <div class="fr"><%= Html.TextBox("ExpectedRate", Model.ExpectedRate, new { @id = IdPrefix + "_ExpectedRate", @maxlength = "100" })%></div>
            </div>
            <%} %>
            <div class="row">
                <label for="<%=IdPrefix %>_ChargeRate" id="<%=IdPrefix %>_ChargeRate_Label" class="fl">Rate</label>
                <div class="fr"><%= Html.TextBox("Charge", Model.Charge, new { @id = IdPrefix + "_ChargeRate", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_Modifier" class="fl">Modifier</label>
                <div class="fr">
                    <%= Html.TextBox("Modifier", Model.Modifier, new { @id = IdPrefix + "_Modifier", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", Model.Modifier2, new { @id = IdPrefix + "_Modifier2", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", Model.Modifier3, new { @id = IdPrefix + "_Modifier3", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", Model.Modifier4, new { @id = IdPrefix + "_Modifier4", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%=IdPrefix %>_ChargeType" class="fl">Service Unit Type</label>
                <div class="fr"><%=Html.UnitType("ChargeType", Model.ChargeType, new { @id = IdPrefix + "_ChargeType", @class = "required not-zero" })%></div>
            </div>
            <div class='row pervisitunitcontent <%= Model.ChargeType=="1" ? "" : "hidden" %>' id="<%=IdPrefix %>_PerVisitUnitContent">
                <label for="<%=IdPrefix %>_Unit" class="fl">Service Units per Visit</label>
                <div class="fr"><%= Html.TextBox("Unit", Model.Unit, new { @id = IdPrefix + "_Unit", @class = string.Format("text  sn {0}", (Model.ChargeType == "1" ? "required" : "")), @maxlength = "1" })%></div>
                <div class="clear"></div>
                <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit.</em>
            </div>
            <% if (Model.IsMedicareHMO)
               { %>
            <% var isRateHidden = (this.Model.ChargeType != "2" && this.Model.ChargeType != "3");%>
            <div class='row medicarehmoratecontent <%= isRateHidden ? "hidden" : "" %>' id="<%=IdPrefix %>_MedicareHMORateContent">
                <label for="<%=IdPrefix %>_MedicareHMORate" class="fl">Medicare HMO Rate</label>
                <div class="fr"><%= Html.TextBox("MedicareHMORate", Model.MedicareHMORate, new { @id = IdPrefix + "_MedicareHMORate", @class = string.Format("text {0}", !isRateHidden ? "required" : ""), @maxlength = "100" })%></div>
                <div class="clear"></div>
                <em>This rate applies for the Medicare final claim per visit if the service unit type is not per a visit.</em>
            </div>
            <%} %>
            <div class="row">
                <%= Html.CheckBox("IsUnitsPerDayOnSingleLineItem", Model.IsUnitsPerDayOnSingleLineItem, new { @id = IdPrefix + "_IsUnitsPerDayOnSingleLineItem" })%>
                <label for="<%=IdPrefix %>_IsUnitsPerDayOnSingleLineItem">Check if the units of this visit type are totaled per day on a single line.</label>
            </div>
            <div id="<%=IdPrefix %>_IsTimeLimitContent" class='chargetypetimelimitcontent <%= (Model.ChargeType == "3" || Model.ChargeType == "2") ? "" : "hidden" %>'>
                <div class="row">
                    <%= Html.CheckBox("IsTimeLimit", Model.IsTimeLimit, new { @id = IdPrefix + "_IsTimeLimit" })%>
                    <label for="<%=IdPrefix %>_IsTimeLimit">Check if a time limit applies per unit.</label>
                </div>
                <div class='margin timelimitcontent  <%= Model.IsTimeLimit ? "" : "hidden" %>' id="<%=IdPrefix %>_TimeLimitContent">
                    <div class="row">
                        <label for="<%=IdPrefix %>_TimeLimit" class="fl">Time Limit</label>
                        <div class="fr">
                            Hours <%= Html.TextBox("TimeLimitHour", Model.TimeLimitHour, new { @id = IdPrefix + "_TimeLimitHour", @class = "shortest", @maxlength = "2" })%>
                            Minutes <%= Html.TextBox("TimeLimitMin", Model.TimeLimitMin, new { @id = IdPrefix + "_TimeLimitMin", @class = "shortest", @maxlength = "2" })%>
                        </div>
                        <div class="clear"></div>
                        <em>This applies for visit times which exceed the insurance provider limit</em>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondDescription" class="fl">Description</label>
                        <div class="fr"><%= Html.TextBox("SecondDescription", Model.SecondDescription, new { @id = IdPrefix + "_SecondDescription", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondRevenueCode" class="fl">Revenue Code</label>
                        <div class="fr"><%= Html.TextBox("SecondRevenueCode", Model.SecondRevenueCode, new { @id = IdPrefix + "_SecondRevenueCode", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondCode" class="fl">HCPCS Code</label>
                        <div class="fr"><%= Html.TextBox("SecondCode", Model.SecondCode, new { @id = IdPrefix + "_SecondCode", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsSecondChargeDifferent", Model.IsSecondChargeDifferent, new { @id = IdPrefix + "_IsSecondChargeDifferent" })%>
                         <label for="<%=IdPrefix %>_IsSecondChargeDifferent">Check if a rate is different.</label>
                    </div>
                    <div id="<%=IdPrefix %>_SecondRateContent" class="secondratecontent  <%= Model.IsSecondChargeDifferent ? "" : "hidden" %>">
                         <%if (Model.IsExpectedRateNeeded)
                           {%>
                        <div class='row'  id="<%=IdPrefix %>_SecondExpectedRateContent">
                             <label for="<%=IdPrefix %>_SecondExpectedRate" class="fl">Expected Rate</label>
                             <div class="fr"><%= Html.TextBox("SecondExpectedRate", Model.SecondExpectedRate, new { @id = IdPrefix + "_SecondExpectedRate", @maxlength = "100" })%></div>
                        </div>
                        <%} %>
                        <div class='row secondchargecontent'  id="<%=IdPrefix %>_SecondChargeContent">
                             <label for="<%=IdPrefix %>_SecondCharge" class="fl" id="<%=IdPrefix %>_SecondChargeLabel">Rate</label>
                             <div class="fr"><%= Html.TextBox("SecondCharge", Model.SecondCharge, new { @id = IdPrefix + "_SecondCharge", @maxlength = "100" })%></div>
                        </div>
                     </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondModifier" class="fl">Modifier</label>
                        <div class="fr">
                            <%= Html.TextBox("SecondModifier", Model.SecondModifier, new { @id = IdPrefix + "_SecondModifier", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", Model.SecondModifier2, new { @id = IdPrefix + "_SecondModifier2", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", Model.SecondModifier3, new { @id = IdPrefix + "_SecondModifier3", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", Model.SecondModifier4, new { @id = IdPrefix + "_SecondModifier4", @class = "insurance-modifier shortest", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="<%=IdPrefix %>_SecondChargeType" class="fl">Unit Type</label>
                        <div class="fr"><%=Html.UnitType("SecondChargeType", Model.SecondChargeType, new { @id = IdPrefix + "_SecondChargeType" })%></div>
                   </div>
                    <div class="row secondpervisitunitcontent <%= Model.SecondChargeType=="1" ? "" : "hidden" %>" id="<%=IdPrefix %>_SecondPerVisitUnitContent">
                        <label for="<%=IdPrefix %>_SecondUnit" class="fl">Service Units per Visit</label>
                        <div class="fr"><%= Html.TextBox("SecondUnit", Model.SecondUnit, new { @id = IdPrefix + "_SecondUnit", @class = string.Format("text sn {0}", (Model.SecondChargeType == "1" ? "required" : "")), @maxlength = "1" })%></div>
                    </div>
                    <div class="row">
                         <%= Html.CheckBox("IsUnitPerALineItem", Model.IsUnitPerALineItem, new { @id = IdPrefix + "_IsUnitPerALineItem" })%>
                         <label for="<%=IdPrefix %>_IsUnitPerALineItem">Check if a unit apply per a line item.</label>
                    </div>
                </div>
            </div>
         </div>  
    </fieldset>
