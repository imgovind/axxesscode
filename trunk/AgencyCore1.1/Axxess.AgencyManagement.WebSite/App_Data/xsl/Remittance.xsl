<?xml  version ="1.0"  encoding ="utf-8"?>
<xsl:stylesheet  version ="1.0"  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform" >
   <xsl:output  omit-xml-declaration ="yes"   />
   <xsl:template  match ="/"><xsl:apply-templates  select ="Remittance"/></xsl:template >
   <xsl:template  match ="Remittance" >
     <table class="remittance" style="width:100%;">
       <tr>
         <td>
           <label class="fl">Check Number: </label>
           <xsl:value-of select="CheckNo"/>
         </td>
         <td>
           <label class="fl">Payment Total</label>
           <xsl:value-of select="PaymentTotal"/>
         </td>
         <td>
           <label class="fl"> Total Claims</label><xsl:value-of select="CheckNo"/>
         </td>
         <td>
           
         </td>
       </tr>
       <tr><th colspan="2">Payer</th><th colspan="2">Payee</th></tr>
       <tr>
         <td colspan="2"><xsl:apply-templates  select ="Payer"/></td>
         <td colspan="2"><xsl:apply-templates  select ="Payee"/></td>
       </tr>
       <xsl:for-each select="Claim">
         <xsl:apply-templates  select ="ClaimPaymentInformation"/>
       </xsl:for-each>
     </table>
   </xsl:template >
   <xsl:template  match ="Payer" >
      <ul>
          <li><label class="fl"> Payer Name</label> <xsl:value-of select="Name" /></li>
          <li><label class="fl"><xsl:value-of select="RefType" />: </label><xsl:value-of select="RefNum" /></li>
          <li><label class="fl">Payer Address 1</label> <xsl:value-of select="Add1" /></li>
          <li><label class="fl">Payer Address 2</label> <xsl:value-of select="Add2" /></li>
          <li><label class="fl">Payer City</label> <xsl:value-of select="City" /></li>
          <li><label class="fl">Payer State</label> <xsl:value-of select="State" /></li>
          <li><label class="fl">Payer Zip</label> <xsl:value-of select="Zip" /></li>
      </ul>
   </xsl:template >
   <xsl:template  match ="Payee" >
       <ul>
            <li><label class="fl"> Payee Name: </label><xsl:value-of select="Name" /></li>
            <li><label class="fl"><xsl:value-of select="RefType" />: </label><xsl:value-of select="RefNum" /></li>
            <li><label class="fl">Payee Address 1: </label><xsl:value-of select="Add1" /></li>
            <li><label class="fl">Payee Address 2: </label><xsl:value-of select="Add2" /></li>
            <li><label class="fl">Payee City: </label><xsl:value-of select="City" /></li>
            <li><label class="fl">Payee State</label> <xsl:value-of select="State" /></li>
            <li><label class="fl">Payee Zip: </label><xsl:value-of select="Zip" /></li>
      </ul>
    </xsl:template>
   <xsl:template  match ="ClaimPaymentInformation" >
        <tr><td colspan="4" style="height:5px; border-bottom:solid 1px black;"></td></tr>
        <tr>
            <td><label class="fl">Patient Name</label>
              <div class="row"> <xsl:value-of select="Patient/FirstName" /> <xsl:value-of select="Patient/LastName" />
              </div>
            </td>
            <td><label class="fl">HIC  Number</label><xsl:value-of select="HICNumber" /></td>
            <td><label class="fl">Patient Control Number</label><xsl:value-of select="PatientControlNumber" /></td>
            <td><label class="fl">ICN Number</label><xsl:value-of select="PayerClaimControlNumber" /></td>
        </tr>
        <tr>
          <td><label class="fl">Start Date</label> <xsl:value-of select="ClaimStatementPeriodStartDate" /></td>
          <td><label class="fl">End Date</label> <xsl:value-of select="ClaimStatementPeriodEndDate" /></td>
          <td><label class="fl">Type Of Bill</label><xsl:value-of select="TypeOfBill" /></td>
          <td><label class="fl"> Claim Status</label><xsl:value-of select="ClaimStatusCode" /></td>
        </tr>
        <tr>
          <td><label class="fl">Patient Control Number</label><xsl:value-of select="PatientControlNumber" /></td>
          <td><label class="fl"> Claim Status</label><xsl:value-of select="ClaimStatusCode" /></td>
          <td><label class="fl">Claim  Amount</label><xsl:value-of select="TotalClaimChargeAmount" /></td>
          <td><label class="fl">Payment Amount</label><xsl:value-of select="ClaimPaymentAmount" /></td>
        </tr>
  </xsl:template >
   <xsl:template  match ="Patient" ></xsl:template>
</xsl:stylesheet > 