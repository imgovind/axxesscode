﻿if (typeof U == "undefined") var U = new Object();
$.extend(U, {
    EnableIfChecked: function(Checkbox, Field, Callback) {
        this.IfChecked(true, Checkbox, Field, true, Callback);
    },
    EnableIfRadioEquals: function(Group, Value, Field, Callback) {
        this.IfRadioEquals(true, Group, Value, Field, true, Callback);
    },
    HideIfChecked: function(Checkbox, Field, Callback) {
        this.IfChecked(false, Checkbox, Field, Callback);
    },
    HideIfRadioEquals: function(Group, Value, Field, Callback) {
        this.IfRadioEquals(false, Group, Value, Field, Callback)
    },
    HideIfSelectEquals: function(Select, Value, Field, Callback) {
        this.IfSelectEquals(false, Select, Value, Field, false, Callback);
    },
    IfChecked: function(Show, Checkbox, Field, Enable, Callback) {
        if (Enable == undefined) Enable = false;
        Checkbox.on("change", function(e) {
            var checked = false;
            Checkbox.each(function() {
                if ($(this).prop("checked")) {
                    if (Show) U.ToggleOn(Field, Enable);
                    else U.ToggleOff(Field, Enable);
                    checked = true;
                    return false;
                }
            });
            if (!checked) {
                if (Show) U.ToggleOff(Field, Enable);
                else U.ToggleOn(Field, Enable);
            }
        });
        Checkbox.trigger("change");
        if (typeof Callback == "function") Callback();
    },
    IfRadioEquals: function(Show, Group, Value, Field, Enable, Callback) {
        Value = Value.split("|");
        if (($.inArray($(":radio[name=" + Group + "]:checked").val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable);
        else U.ToggleOff(Field, Enable);
        $(":radio[name=" + Group + "]").on("click", function() {
            if (($.inArray($(":radio[name=" + Group + "]:checked").val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable, true);
            else U.ToggleOff(Field, Enable, true);
        });
        if (typeof Callback == "function") Callback();
    },
    IfSelectEquals: function(Show, Select, Value, Field, Enable, Callback) {
        Value = Value.split("|");
        if (($.inArray(Select.val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable);
        else U.ToggleOff(Field, Enable);
        Select.on("change", function() {
            if (($.inArray(Select.val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable, true);
            else U.ToggleOff(Field, Enable, true);
        });
        if (typeof Callback == "function") Callback();
    },
    LoadSelect: function(Url, Arguments, Select, Title, ZeroIndex, Action) {
        U.PostUrl(Url, Arguments, function(Data) {
            Select.empty();
            if (ZeroIndex) Select.append(new Option(Title, "0"));
            $.each(Data, function(Key, Value) { Select.append(new Option(Value.Name, Value.Id)) });
            if (typeof Action == "function") Action();
        })
    },
    NoneOfTheAbove: function(Checkbox, Group, Callback) {
        if (Checkbox.prop("checked")) Group.each(function() {
            if ($(this).prop("id") != Checkbox.prop("id")) $(this).prop("checked", false).change();
        });
        Group.on("change", function() {
            if ($(this).prop("id") != Checkbox.prop("id") && $(this).prop("checked")) Checkbox.prop("checked", false).change();
        });
        Checkbox.on("change", function() {
            if ($(this).prop("checked")) Group.each(function() {
                if ($(this).prop("id") != Checkbox.prop("id")) $(this).prop("checked", false).change();
            })
        });
        if (typeof Callback == "function") Callback();
    },
    SelectAll: function(Checkbox, Group, Callback) {
        var indeterminateFunction = function() {
            var groupCheckedCount = Group.filter(":checked").length,
                check = groupCheckedCount == Group.length;
            //Checkbox.prop("indeterminate", groupCheckedCount > 0 && !check);
            return check;
        };
        //indeterminateFunction();
        Checkbox.on("change", function() {
            var groupChanged = Group;
            Group.each(function() {
                if ($(this).prop("checked") != Checkbox.prop("checked")) $(this).prop("checked", Checkbox.prop("checked"));
                else groupChanged = groupChanged.not($(this));
            });
            if (groupChanged && groupChanged.length) groupChanged.change();
            U.SelectAllLabelRename(this);
        });
        //        Group.on("change", function() {
        //            Checkbox.prop("checked", indeterminateFunction());
        //        });
        if (typeof Callback == "function") Callback();
    },
    SelectAllLabelRename: function(checkbox) {
        checkbox = $(checkbox);
        var label = checkbox.closest(".window-content").find("label[for=" + checkbox.attr("id") + "]");
        if (label != undefined && label.text().indexOf(" ")) {
            var labelSplit = label.text().split(/ /);
            if (labelSplit[0] == "Select" || "Deselect") label.text((checkbox.prop("checked") ? "Deselect" : "Select") + label.text().substring(label.text().indexOf(" "), label.text().length));
        }
    },
    ShowIfChecked: function(Checkbox, Field, Callback) {
        this.IfChecked(true, Checkbox, Field, Callback);
    },
    ShowIfRadioEquals: function(Group, Value, Field, Callback) {
        this.IfRadioEquals(true, Group, Value, Field, Callback);
    },
    ShowIfSelectEquals: function(Select, Value, Field, Callback) {
        this.IfSelectEquals(true, Select, Value, Field, false, Callback);
    },
    ToggleOn: function(Field, Enable, Animate) {
        if (Enable) Field.removeAttr("disabled").removeClass("form-omitted");
        else if (Animate) Field.Animate("slideDown", function() {
            if (Field.closest(".grid-controls,.report-controls")) Field.closest(".window-content").find(".t-grid").css("top", Field.closest(".wrapper").prop("offsetHeight"));
        }).removeClass("form-omitted");
        else Field.show().removeClass("form-omitted");
        if (Field.hasClass("required-disabled")) Field.removeClass("required-disabled").addClass("required");
        if ($(".required-disabled", Field).length) $(".required-disabled", Field).removeClass("required-disabled").addClass("required");
    },
    ToggleOff: function(Field, Enable, Animate) {
        if (Enable) Field.attr("disabled", true).addClass("form-omitted");
        else if (Animate) Field.Animate("slideUp", function() {
            if (Field.closest(".grid-controls,.report-controls")) Field.closest(".window-content").find(".t-grid").css("top", Field.closest(".wrapper").prop("offsetHeight"));
        }).addClass("form-omitted");
        else Field.hide().addClass("form-omitted");
        if (Field.hasClass("required")) Field.removeClass("required").addClass("required-disabled");
        if ($(".required", Field).length) $(".required", Field).removeClass("required").addClass("required-disabled");
    },
    ToggleRequired: function(Checkbox, Checked, Field) {
        if (Checkbox.prop("checked") == Checked) Field.addClass("required");
        else Field.removeClass("required");
        Checkbox.on("change", function() {
            if (Checkbox.prop("checked") == Checked) Field.addClass("required");
            else Field.removeClass("required");
        })
    },
    IncludeCommandInForm: function(Form, Button, Event) {
        if ($(Button).closest("ul").hasClass("send-commands")) {
            Form.on("ProcessingStart", function() {
                var input = $("<input>").attr("type", "hidden").attr("name", "button").val($(Event.target).attr("command"));
                Form.append(input);
            });
            Form.on("ProcessingComplete", function() {
                $("input[name='button']", Form).remove();
            });
        }
    },
    SetStatusField: function(statusFieldName, Form) {
        var statusField = $("[name='Status']", Form);
        if (statusField.length) {
            var status = $("[name='" + statusFieldName + "']", Form);
            if (status.length) {
                statusField.val(status.val());
            }
        }
    },
    CheckTimeInOut: function(timeIn, timeOut, showMessage) {
        var timeOutNextDay = new Date(timeOut.getTime());
        timeOutNextDay.setDate(timeOutNextDay.getDate() + 1);
        if (timeIn && timeOut) {
            var timeDiff = (timeOut - timeIn) / 60 / 60 / 1000;
            var timeDiffNextDay = (timeOutNextDay - timeIn) / 60 / 60 / 1000;
            var over3Hours = (timeDiff > 3 || timeDiff < 0) && (timeDiffNextDay > 3 || timeDiffNextDay < 0);
            if (over3Hours == true) {
                if (showMessage == true) {
                    var growl = $(".jGrowl-notification").text();
                    if (growl == "") {
                        U.Growl("WARNING: The Time In and Out are over 3 hours apart.", "warning");
                    }
                }
                else {
                    var defer = $.Deferred();
                    Acore.Confirm({
                        Name: "Time In and Out Warning",
                        Message: "The time out is 3 hours greater than the time in. Are you sure you want to continue?",
                        Yes: function() { defer.resolve("true"); },
                        No: function() { defer.resolve("false"); }
                    });
                    return defer.promise();
                }
            } else {
                return true;
            }
        }
    }
});