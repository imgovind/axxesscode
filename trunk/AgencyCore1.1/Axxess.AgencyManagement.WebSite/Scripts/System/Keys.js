﻿if (typeof U == "undefined") var U = new Object();
// These methods test the argument event.which from the keypress event and not a keydown event
$.extend(U, {
    IsDollarSign: function(key) {
        if (key == 36) return true;                 // Dollar Sign
        return false;
    },
    IsCommaOrPeriod: function(key) {
        if (key == 44) return true;                 // Comma
        if (key == 46) return true;                 // Period
        return false;
    },
    IsIcdAlpha: function(key) {
        if (key == 101 || key == 69) return true;   // Letter e (Case Insensitive)
        if (key == 118 || key == 86) return true;   // Letter v (Case Insensitive)
        return false;
    },
    IsNumber: function(key) {
        if (key >= 48 && key <= 57) return true;    // Numbers
        return false;
    },
    IsSeparator: function(key) {
        if (key == 44) return true;                 // Comma
        if (key == 45) return true;                 // Hyphen
        if (key == 46) return true;                 // Period
        if (key == 47) return true;                 // Forward slash
        if (key == 92) return true;                 // Back slash
        return false;
    },
    IsSystemKey: function(key) { // Takes event.keyCode argument instead of event.which
        if (key == 8) return true;                  // Backspace
        if (key == 45) return true;                 // Insert
        if (key == 46) return true;                 // Delete
        if (key >= 33 && key <= 40) return true;    // Cursor Placement (Home, End, PgUp, PgDown, Arrow Keys)
        return false;
    }
});