if (typeof U == "undefined") var U = new Object();
$.extend(U, {
    Input: function(e) { return $("fieldset .input", e).serializeArray(); },
    MultiSelectInput: function(control) { return $(control).multiselect("getChecked").map(function() { return this.value; }).get(); },
    ParameterByName: function(url, name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if (results == null) return "";
        else return decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    GetGridFilterParameters: function(e, includeSortParams, includeGrouping) {
        var data = {};
        var selector = ".filter";
        var notSelector = ".ajax-autocomplete";
        if (!includeSortParams) notSelector += ",.sort-parameters";
        if (!includeGrouping) notSelector += ",.grouping";
        $(selector, e).not(".grid-search,.ignore-grid-binding").find(":input").not(notSelector).each(function () {
            var value = $(this).val() || "",
            name = $(this).attr("name");
            if ($(this).hasClass("required") && !$.validator.methods["not-zero"](value)) {
                $(this).addClass("error");
                data = false;
                return false;
            } else $(this).removeClass("error");
            if (typeof value == "string") data[name] = value;
            else data[name] = value.join();
        });
        return data;
    },
    TGridAjax: function(url, input, grid, callback) {
        U.PostUrl(url, input, function(result) {
            if (typeof callback == "function") {
                try {
                    callback(result);
                } catch (e) {
                    $.error(e);
                }
            } else if (result.isSuccessful) {
                U.RebindTGrid(grid);
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    FilterResults: function(type) {
        $('#' + type + 'MainResult').empty().addClass("loading");
        U.RebindTGrid($('#' + type + 'SelectionGrid'), { branchId: $("select." + type + "BranchId").val(), statusId: $("select." + type + "StatusDropDown").val(), paymentSourceId: $("select." + type + "PaymentDropDown").val() });
    },
    RebindTGrid: function(g, a) { if (g.data('tGrid') != null) { if (a) { g.data('tGrid').rebind(a); } else { g.data('tGrid').rebind(); } } },
    RebindTGridByAjaxPost: function(gridControl, url, input, callback) {
        if (gridControl) {
            var grid = gridControl.data("tGrid");
            if (grid) {
                if (url) grid.ajax.selectUrl = url;
                if (input) grid.rebind(input);
                else grid.rebind();
            }
        }
    },
    RebindDataGridContent: function(pageName, url, jsonData, callback) { U.RebindGridContent($("#" + pageName + "GridContainer"), url, jsonData, callback); },
    RebindContent: function(selector, url, data, callback) {
        //if (!Message) Message = U.DefaultErrorMessage;
        $(selector).Load(url, data, callback, false, true);
    },
    RebindGridContent: function(selector, url, data, callback) {
        //if (!Message) Message = U.DefaultErrorMessage;
        $(selector).Load(url, data, callback, true, true);
    },
    // Rebind multiple grids
    RebindGrids: function(grid) { grid.each(function() { U.RebindGrid($(this)); }); },
    // Rebind all current grids in DOM
    RebindAllGrids: function() {
        if ($(".t-grid").not(".args").length) $(".t-grid").not(".args").each(function() { if ($(this).data("tGrid")) $(this).data("tGrid").rebind(); });
        if ($(".grid-refresh").length) $(".grid-refresh").each(function() { $(this).click(); });
        // if (Acore.Windows[Patient.Charts.AcoreId].IsOpen) Patient.Charts.Refresh();
        // if (Acore.Windows[Schedule.Center.AcoreId].IsOpen) Schedule.Center.Refresh();
        //if (Acore.Windows[Schedule.Center.PrivateDuty.AcoreId].IsOpen) Schedule.Center.PrivateDuty.Refresh();
    },
    //Bind to a Telerik Grid's OnRowDataBound event or used in that grids own function for the event to have the Sticky Notes initialized
    DataItemToolTip: function(e) { U.ToolTip(e.row); },
    ServerBindScheduleActivityInit: function(grid) {
        if (grid.length > 0) {
            grid.removeClass("loading");
            $(".t-grid-content tbody tr", grid).each(function(index, element) {
                var isComplete = $(this).hasClass("completed");
                var isOrphaned = $(this).hasClass("orphaned");
                if (isComplete) $(this).addClass("darkgreen");
                if (isOrphaned) {
                    $(this).addClass("black").Tooltip({
                        top: 5,
                        left: 5,
                        extraClass: "calday error",
                        bodyHandler: function() {
                            return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly.";
                        }
                    });
                    $("td:first", this).next().addClass("darkred");
                }
                U.ToolTip(element);
                $(element).on("contextmenu", function(event) {
                    var menu = $("<ul/>");
                    if (isComplete) menu.append(
                        $("<li/>").append(
                            $("<a/>").text("Reopen Task").on("click", function() {
                                $(e.row).find("a:contains('Reopen Task')").click();
                            })));
                    else if (!isOrphaned) menu.append(
                        $("<li/>").append(
                            $("<a/>").text("Edit Note").on("click", function() {
                                $(e.row).find("a:first").click();
                            })));
                    menu.append(
                        $("<li/>").append(
                            $("<a/>").text("Details").on("click", function() {
                                $(e.row).find("a:contains('Details')").click();
                            }))).append(
                        $("<li/>").append(
                            $("<a/>").text("Delete").on("click", function() {
                                $(e.row).find("a:contains('Delete')").click();
                            }))).append(
                        $("<li/>").append(
                            $("<a/>").text("Print").on("click", function() {
                                $(e.row).find(".print").parent().click();
                            })));
                    menu.ContextMenu(event);
                })
            })
        }
    },
    ClientBindScheduleActivityInit: function(grid) {
        if (grid.length) {
            var gridControl = grid.data('tGrid');
            if (gridControl) {
                grid.removeClass("loading");
                var datas = gridControl.data;
                if (datas) {
                    $('.t-grid-content tbody tr', grid).each(function(index, element) {
                        var dataItem = datas[index];
                        if (dataItem != null) {
                            if (dataItem.IsComplete) { $(this).addClass('darkgreen'); }
                            if (dataItem.IsOrphaned) {
                                $(this).addClass('black').Tooltip({
                                    top: 5,
                                    left: 5,
                                    extraClass: "calday error",
                                    bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
                                });
                                $('td:first', element).next().addClass('darkred');
                            }
                            U.ToolTip(element);
                            $(element).on("contextmenu", function(event) {
                                var menu = $("<ul/>");
                                if (dataItem.IsComplete) menu.append(
                                    $("<li/>").append(
                                        $("<a/>", { text: "Reopen Task" }).on("click", function() {
                                            $(e.row).find("a:contains('Reopen Task')").click();
                                        })
                                    )
                                );
                                else if (!dataItem.IsOrphaned) menu.append(
                                    $("<li/>").append(
                                        $("<a/>", { text: "Edit Note" }).on("click", function() {
                                            $(e.row).find("a:first").click();
                                        })
                                    )
                                );
                                menu.append(
                                    $("<li/>").append(
                                        $("<a/>", { text: "Details" }).on("click", function() {
                                            $(e.row).find("a:contains('Details')").click();
                                        }))).append(
                                    $("<li/>").append(
                                        $("<a/>", { text: "Delete" }).on("click", function() {
                                            $(e.row).find("a:contains('Delete')").click();
                                        }))).append(
                                    $("<li/>").append(
                                        $("<a/>", { text: "Print" }).on("click", function() {
                                            $(e.row).find(".print").parent().click();
                                        })
                                    )
                                );
                                menu.ContextMenu(event);
                            })
                        }
                    })
                }
            }
        }
    },
    ActivityGridContextMenuInit: function(activityGrid) {
        var contextMenuHoverTimer;
        var hoverDisplayFunc = function(pointer, action) {
            pointer.toggleClass("down right");
            action.toggleClass("hover");
        };
        $(".t-grid-content", activityGrid).on("mouseenter mouseleave", ".grid-action", function(event) {
            var pointer = $(this).find(".pointer"),
                    $this = $(this);
            if (event.type == "mouseenter") {
                contextMenuHoverTimer = setTimeout(function() {
                    var menu,
                        tableRow = $this.closest("tr"),
                        serviceName = "",
                        patientId = "",
                        eventId = "",
                        grid = activityGrid.data("tGrid");
                    if (grid.data && grid.data.length > 0) {
                        var index = $("tr", activityGrid).index(tableRow);
                        var rowData = grid.data[index - 1];
                        if (rowData) {
                            patientId = rowData.PatientId;
                            eventId = rowData.Id;
                            var menuItems = [];
                            if (rowData.IsUserCanEditDetail) {
                                menuItems.push("<li><a class=\"edit-details\"><span class='img icon22 edit-schedule'/>Details</a></li>");
                            }
                            if (rowData.IsUserCanEdit) {
                                var date = U.ConvertJsonDateToJavascriptDate(rowData.DateIn);
                                if (!rowData.IsMissedVisit && date <= new Date()) {
                                    menuItems.push("<li><a class=\"mark-missedvisit\"><span class='img icon22 delete-schedule'/>Missed Visit Form</a></li>");
                                }
                            }
                            if (rowData.IsUserCanReopen) menuItems.push("<li><a class=\"reopen-task\"><span class='img icon22 reopen-schedule'/>Reopen</a></li>");
                            if (rowData.IsUserCanReassign) menuItems.push("<li><a class=\"reassign-task\"><span class='img icon22 reassign-schedule'/>Reassign</a></li>");
                            if (rowData.IsUserCanRestore) menuItems.push("<li><a class=\"restore-task\"><span class='img icon22 reopen-schedule'/>Restore</a></li>");
                            if (rowData.IsUserCanDelete) menuItems.push("<li><a class=\"delete-task\"><span class='img icon22 delete'/>Delete</a></li>");
                            if (menuItems.length > 0) {
                                serviceName = Acore.AppService(rowData.Service);
                                menu = $("<ul/>", { "class": "action-menu" }).append($.map(menuItems, function(val, index) { return val; }).join(""));
                            }
                        }
                    } else {
                        var tempMenu = $this.data("menu");
                        if (!tempMenu) {
                            tempMenu = $this.find(".action-menu").detach();
                            $this.data("menu", tempMenu);
                        }
                        patientId = $(".pid", tableRow).text();
                        eventId = $(".id", tableRow).text();
                        serviceName = $(".service", tableRow).text();
                        menu = tempMenu;
                    }
                    if(menu && menu.length){
                        menu.on("click", "li", function() {
                            var link = $(this).find("a");
                            if (link.hasClass("edit-details")) Schedule.Task[serviceName].Edit(patientId, eventId);
                            if (link.hasClass("reopen-task")) Schedule.Task[serviceName].Reopen(patientId, eventId);
                            if (link.hasClass("reassign-task")) Schedule.Task[serviceName].Reassign(patientId, eventId);
                            if (link.hasClass("delete-task")) Schedule.Task[serviceName].Delete(patientId, eventId);
                            if (link.hasClass("restore-task")) MissedVisit[serviceName].Restore(patientId, eventId);
                            if (link.hasClass("mark-missedvisit")) MissedVisit[serviceName].New(patientId, eventId);
                            $(this).closest("ul").remove();
                            hoverDisplayFunc(pointer, $this);
                        }).on("mouseleave", function() {
                            $(this).remove();
                            hoverDisplayFunc(pointer, $this);
                        });
                        $(document.body).append(menu);
                        U.SetContextMenuPosition(menu, $this.parent(), $this);
                    }
                }, 300);
            } else {
                var relatedTarget = $(event.relatedTarget);
                if (relatedTarget.length) {
                    if (!relatedTarget.is("ul")) relatedTarget = relatedTarget.closest("ul");
                    if (relatedTarget.hasClass("action-menu")) return;
                }
                if (contextMenuHoverTimer) clearTimeout(contextMenuHoverTimer);
                $(document.body).find("ul.action-menu").remove();
            }
            hoverDisplayFunc(pointer, $this);
        });
    },
    SetContextMenuPosition: function(contextMenu, tableCell, openLink) {
        var cellHeight = openLink.outerHeight(true) - 5;
        var x = tableCell.offset().left,
            y = openLink.offset().top + cellHeight;
        var winWidth = $(window).width();
        var winHeight = $(window).height();
        var ulWidth = contextMenu.outerWidth();
        var ulHeight = contextMenu.outerHeight(true);
        if (ulWidth + x > winWidth) {
            x -= ulWidth / 2;
        }
        if (ulHeight + y > winHeight) {
            y -= ulHeight + cellHeight;
        }
        contextMenu.css({ top: y, left: x });
    },

    RowColorCode: function (element, dataItem) {
        if (dataItem != null) {
            if (dataItem.IsComplete) { $(element).addClass('darkgreen'); }
            if (dataItem.IsOrphaned) {
                $(element).addClass('black').Tooltip({
                    top: 5,
                    left: 5,
                    extraClass: "calday error",
                    bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
                });
                $('td.schedule-date', element).addClass('darkred');
            }
        }
    },
    GridContext: function(element, dataItem) {
        if (dataItem != null) {
            $(element).bind("contextmenu", function(event) {
                var actionTd = $("td.action", element);
                if (actionTd.length > 0) {
                    var menu = $("<ul/>");
                    var editDetail = $(actionTd).find("a.edit-details");
                    if (editDetail.length > 0) menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Details" }).on("click", function() {
                                editDetail.click();
                            })
                        )
                    );
                    if (dataItem.IsComplete) {
                        var reopen = $(actionTd).find("a.reopen-task");
                        if (reopen.length > 0) menu.append(
                            $("<li/>").append(
                                $("<a/>", { text: "Reopen Task" }).on("click", function() {
                                    reopen.click();
                                })
                            )
                        );
                    } else if (!dataItem.IsOrphaned) {
                        var editNoteDetail = $(actionTd).find("a.edit-details");
                        if (editNoteDetail.length > 0) menu.append(
                            $("<li/>").append(
                                $("<a/>", { text: "Edit Note" }).on("click", function() {
                                    editNoteDetail.click();
                                })
                            )
                        );
                    }
                    var reassign = $(actionTd).find("a.reassign");
                    if (reassign.length > 0) menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Reassign" }).on("click", function() {
                                reassign.click();
                            })
                        )
                    );
                    var deleteTask = $(actionTd).find("a.delete-task");
                    if (deleteTask.length > 0) menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Delete" }).on("click", function() {
                                deleteTask.click();
                            })
                        )
                    );
                    menu.ContextMenu(event);
                }
            })
        }
    },
    //Used to stop grids that have been server bound with an empty list from sending a client request for data.
    OnDataBindingStopAction: function(e, params, url) {
        if (e) {
            var gridControl = $(e.currentTarget);
            if (gridControl) {
                U.OnTGridDataBinding(e);
                var check = gridControl.data('tGrid');
                if (check) {
                    if (!check.HasServerBindingBeenDone) {
                        check.HasServerBindingBeenDone = true;
                        U.PreventGridDefaultAndStopLoading(e);
                    } else if (params && url) check.ajax.selectUrl = url + "?" + $.param(params);
                } else U.PreventGridDefaultAndStopLoading(e);
            } else e.preventDefault();
        }
    },
    //Used in conjunction with OnDataBindingStopAction to stop a telerik grid, that has both server and client binding, from rebinding if the server model is empty
    StopTGridFirstAction: function(gridControl) {
        var data;
        if (gridControl.length) data = gridControl.data('tGrid');
        else data = gridControl;
        if (data) data.HasServerBindingBeenDone = true;
    },
    PreventGridDefaultAndStopLoading: function(e) {
        e.preventDefault();
        U.OnTGridDataBound(e);
    },
    FormatGridDate: function (date) {
        if (date != null && date instanceof Date) {
            if (date.getTime() == U.GetMinDate().getTime()) return '';
            else return $.datepicker.formatDate('mm/dd/yy', date);
        } else return '';
    },
    ServiceBranchFilter: function(e, selectedService) {
        var branch = $("[name=BranchId]", e);
        if (branch) {
            branch.children().hide().filter(function() {
                return (((selectedService & $(this).attr("service")) == selectedService) || (U.GuidEmpty == $(this).val()));
            }).show();
            var visible = branch.children(':visible');
            if (visible) {
                var selected = visible.filter(":selected");
                if (selected.length > 0) {
                } else {
                    visible.prop('selected', 'selected');
                }
            }
        }
    },
    OnGridDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    OnGridDetailViewExpand: function(e) { $("tr.t-detail-row", $(e.target)).not(e.detailRow).remove(); $("tr.t-master-row", $(e.target)).not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    ListInit: function(r, t, x, e) {
        U.SetGridServiceBranch(e);
        U.SetPrintOrExport(e, "export");
        var pageName = $(".pagename", e).val();
        var refresh = $(".grid-refresh", e);
        var url = U.GetUrlForService(refresh);
        U.InitListContent($("#" + pageName + "GridContainer", e), url, pageName);
        U.SetRefresh(e, pageName);
        U.SetGrouping(e, pageName);
        U.FilterChangeBind(e, refresh);
    },
    ListInitWithArea: function(r, t, x, e) {
        U.SetGridServiceBranch(e, true);
        U.SetPrintOrExport(e, "export", true);
        var pageName = $(".pagename", e).val();
        var refresh = $(".grid-refresh", e);
        var url = U.GetUrlForService(refresh, true);
        U.InitListContent($("#" + pageName + "GridContainer", e), url, pageName);
        U.SetRefresh(e, pageName, true);
        U.SetGrouping(e, pageName, true);
        U.FilterChangeBind(e, refresh);
    },
    //Reloads the content for a server bound telerik grid
    LoadListContent: function(e, url, pageName, input) { U.RebindGridContent($("#" + pageName + "GridContainer", e), url, input, function() { U.InitListContent($("#" + pageName + "GridContainer", e), url, pageName); }); },
    //Inits the content for a server bound telerik grid
    InitListContent: function(e, url, pageName) {
        var grid = $(".t-grid", e);
        U.InitServerSideTGridSorting(e.closest(".window"), url, pageName, function(element) { return U.GetGridFilterParameters(element, true, true); },
            function(e2, url2, pageName2, input) { U.LoadListContent(e2, url2, pageName2, input); });
        var init = e.attr("init");
        if (init != undefined) {
            try {
                var callBackFunctionChain = init.split(".");
                var functionHolder = window;
                if (callBackFunctionChain.length > 0) {
                    for (var funIndex in callBackFunctionChain) {
                        functionHolder = functionHolder[callBackFunctionChain[funIndex]];
                    }
                    if (typeof functionHolder === "function") {
                        functionHolder(grid);
                    }
                }
            } catch (e) {
                $.error(e)
            }
        }
        var toolTipElements = $("a.note", grid);
        if (toolTipElements) {
            U.ToolTipsByElements(toolTipElements);
        }
        U.FormatTGridTotalFooter(grid);
        $(".t-grid-bottom", grid).hide();
    },
    InitServerSideTGridSorting: function(e, url, pageName, getInput, refreshFunction) {
        $(".t-grid", e).on("click", ".t-grid-header .t-link, .t-grid-bottom .t-refresh", function(evnt) {
            if ($(this).attr("href")) {
                var link = $(this).attr("href");
                var input = getInput(e);
                input.SortParams = U.ParameterByName(link, pageName + "_Grid-orderBy");
                refreshFunction(e, url, pageName, input);
                evnt.stopPropagation();
                return false;
            }
        });
    },
    //T stands for Teleik
    InitForNonT: function(r, t, x, e) {
        U.SetPrintOrExport(e, "print");
        U.SetPrintOrExport(e, "export");
        var pageName = $(".pagename", e).val();
        var gridContainer = $("#" + pageName + "GridContainer", e);
        if (!gridContainer.html().trim()) gridContainer.html(U.MessageInfo($("[name=ErrorMessageHead]", e).val(), $("[name=ErrorMessageBody]", e).val()));
        var refresh = $(".grid-refresh", e).on("click", function() {
            U.RebindDataGridContent(pageName, $(this).attr("url"), U.GetGridFilterParameters(e), function(r2, t2, x2, e2) {
                if (!r2) e2.html(U.MessageInfo($("[name=ErrorMessageHead]", e).val(), $("[name=ErrorMessageBody]", e).val()));
            });
            return false;
        });
        U.FilterChangeBind(e, refresh);
    },
    //Client side binding for the telerik grid that does not have a service filter
    ListInitClientRebind: function(r, t, x, e) {
        U.SetGridServiceBranch(e);
        U.SetPrintOrExport(e, "export");
        U.InitLocationDependent(e);
        var grid = $(".t-grid", e);
        var refresh = $(".grid-refresh", e).on("click", function() { U.RebindTGrid(grid, U.GetGridFilterParameters(e)); return false; });
        if (grid.hasClass("aggregated")) $(".t-grid-bottom", grid).hide();
        U.FilterChangeBind(e, refresh);
    },
    //Client side binding for the telerik grid that does have a service filter
    InitTGridServiceAndBranchFilter: function(r, t, x, e) {
        U.SetPrintOrExport(e, "export", true);
        U.SetGridServiceBranch(e, true);
        var grid = $(".t-grid", e);
        var refresh = $(".grid-refresh", e).on("click", function() { U.RebindTGridByAjaxPost(grid, $(this).attr("area") + $(this).attr("url"), U.GetGridFilterParameters(e)); return false; });
        if (grid.hasClass("aggregated")) $(".t-grid-bottom", grid).hide();
        U.FilterChangeBind(e, refresh);
    },
    FilterChangeBind: function(e, refresh) {
        $(".grid-controls select", e).on("change", function() { refresh.click(); });
        $(".ajax-autocomplete", e).next().on("change", function() { refresh.click(); });
        $(".grid-controls .date-picker", e).on("change", function() {
            if (!$(this).hasClass("error")) refresh.click();
        });
    },
    OnTGridError: function(e) {
        console.log(e);
        if (e.textStatus == "abort") {
            e.preventDefault();
            return false;
        }
        var gridContent = $(this).find(".t-grid-content").first();
        if (e.type == "error") gridContent.html(U.MessageErrorAjax());
        else if (e.type == "timeout") gridContent.html(U.MessageWarn("Timed Out", "The server took too long to respond, please try again. If problem persists, contact Axxess Healthcare Consult."));
        else if (e.type == "parseerror") console.log("The grid did not recieve a JSON object.");
        return false;
    },
    OnTGridDataBinding: function(e) {
        var grid = $(e.target || e),
            body;
        if (grid.parent().hasClass("t-detail-cell")) body = grid;
        else {
            body = $(".t-grid-header+div.t-grid-content", grid);
            if (body.length == 0) body = $(".t-grid-header+tbody", grid);
        }
        body.addClass("loading-visibility");
        //$(".t-grid-header+div,.t-grid-header+tbody", $(e.target)).addClass("loading");
    },
    OnTGridDataBound: function(e) {
        var grid = $(e.target || e);
        if (grid.parent().hasClass("t-detail-cell")) grid.removeClass("loading-visibility");
        else $(".t-grid-content,.t-grid-header+tbody", grid).removeClass("loading-visibility");
        U.FormatTGridTotalFooter(grid);
    },
    OnTGridDataBoundCollapseGroups: function(e) {
        var grid;
        if (e.target) grid = $(e.target);
        else grid = e;
        if (grid.find(".t-grid-content tr.t-grouping-row").length) {
            grid.find(".t-grid-content tr").each(function() {
                if ($(this).hasClass("t-grouping-row")) $(this).find("a").removeClass("t-collapse").addClass("t-expand");
                else $(this).css("display", "none");
            });
        }
    },
    FormatTGridTotalFooter: function(grid) {
        if (grid.hasClass("aggregated")) {
            if ($(".t-grid-footer", grid).length) {
                var footerRowCount = $(".t-footer-template td", grid).length;
                if (footerRowCount > 1) $(".t-footer-template td", grid).each(function() {
                    if (!$(this).html() || $(this).html() == "&nbsp;") $(this).hide();
                    else $(this).attr("colspan", footerRowCount).addClass("ac t-aggregate-cell");
                });
            }
        }
    },

    SetArea: function(e, serviceValue) {
        if (+serviceValue > 0) {
            serviceValue = Acore.AppService(serviceValue);
        }
        var area = window[serviceValue].Area;
        $(".grid-refresh", e).attr("area", area);
        $(".export", e).attr("area", area);
        $(".print", e).attr("area", area);
    },
    InitLocationDependent: function(e, moreOnChangeAction) {
        var location = $(".location", e);
        var insurance = $("select.insurance", e);
        if (location.length && insurance.length) {
            if (insurance.hasClass("homehealth")) U.MedicareOnlyInsuranceRebindHelper(insurance, location);
            location.on("change", function() {
                if (insurance.length && insurance.hasClass("homehealth")) U.MedicareOnlyInsuranceRebindHelper(insurance, $(this));
                if (typeof moreOnChangeAction == "function") moreOnChangeAction();
            });
        }
    },
    MedicareOnlyInsuranceRebindHelper: function(insurance, location) {
        var locationPayor = location.is('select') ? $("option:selected", location).attr("payor") : location.attr("payor");
        insurance.find("option").show();
        insurance.find("option[itype=medicaretraditional][value !=" + (locationPayor != undefined ? locationPayor : 0) + " ]").hide();
    },
    Filter: function(text, gridContent) {
        if (gridContent != undefined) {
            if (text != undefined && text.length > 0) {
                text = $.trim(text).toUpperCase();
                var search = text.split(" ");
                if (search != undefined && search.length > 0) {
                    var gridTr = $("tbody  tr", gridContent);
                    if (gridTr != undefined && gridTr.length > 0) {
                        gridTr.removeClass("match").hide();
                        var matchedTrs = gridTr.filter(function(index) {
                            var tdL = $(this).find("td.searchL").text();
                            var tdF = $(this).find("td.searchF").text();
                            var result = $.grep(search, function(s, i) {
                                return (tdL.indexOf(s) > -1 || tdF.indexOf(s) > -1);
                            });
                            return result.length > 0;
                        });
                        matchedTrs.removeClass("t-alt").addClass("match").show().filter(":even").addClass("t-alt");
                    }
                }
            }
        }
    },
    SetPrintOrExport: function(e, className, isAreaFromAnchor) {
        $("." + className, e).off("click");
        $("." + className, e).on("click", function() {
            var url = "";
            if (isAreaFromAnchor) {
                url = $(this).attr("area");
            }
            url += $(this).attr("url");
            var input = U.GetGridFilterParameters(e);
            if (input) U.GetAttachment(url, input);
            return false;
        });
    },
    SetGridServiceBranch: function(e, isAreaFromAnchor) {
        var service = $("[name=ServiceId]", e);
        if (service) {
            var permissions = $("a.servicepermission", e);
            if (permissions) {
                permissions.each(function() {
                    if (($(this).attr("service") & service.val()) == service.val()) {
                        $(this).parent().show();
                    }
                    else {
                        $(this).parent().hide();
                    }
                });
            }
            U.ServiceBranchFilter(e, service.val());
            service.on("change", function() {
                var val = $(this).val();
                U.ServiceBranchFilter(e, val);
                if (isAreaFromAnchor) {
                    U.SetArea(e, val);
                }
                if (permissions) {
                    permissions.each(function() {
                        if (($(this).attr("service") & val) == val) {
                            $(this).parent().show();
                        }
                        else {
                            $(this).parent().hide();
                        }
                    });
                }
            });
        }
    },
    //Initiates grouping for a grid
    SetGrouping: function(e, pageName, isAreaFromAnchor) {
        $(".group-button", e).each(function() {
            var groupValue = $(this).attr("grouping");
            $(this).on("click", function() {
                var url = "";
                if (isAreaFromAnchor) {
                    url = $(".grid-refresh", e).attr("area");
                }
                url += $(".grid-refresh", e).attr("url");
                var input = U.GetGridFilterParameters(e, true);
                input.GroupName = groupValue;
                U.LoadListContent(e, url, pageName, input);
                return false;
            });
        });
    },
    GetUrlForService: function(refreshControl, isAreaFromAnchor) {
        var url = "";
        if (refreshControl) {
            if (isAreaFromAnchor) {
                url += refreshControl.attr("area");
            }
            url += refreshControl.attr("url");
        }
        return url;
    },
    SetRefresh: function(e, pageName, isAreaFromAnchor) {
        $(".grid-refresh", e).on("click", function() {
            var url = "",
                input = U.GetGridFilterParameters(e, true, true);
            if (isAreaFromAnchor) {
                url = $(this).attr("area");
            }
            url += $(this).attr("url");
            if(input) U.LoadListContent(e, url, pageName, input);
            return false;
        });
    },
    // rebinds grid which has already open window
    RefreshOpenGrid: function(e, windowId) {
        if (!e) {
            e = $("#window_" + windowId + "_content");
        }
        var refresh = $(".grid-refresh", e);
        if (refresh) { refresh.click(); }
    },
    // rebinds grid which may or may not open window
    RefreshGrid: function(e, windowId) {
        if (Acore.Windows[windowId].IsOpen) {
            U.RefreshOpenGrid(e, windowId);
        }
    },
    // removed the telerik grid hierarchy on a row by row case if 
    OnRowDataBoundHideEmptyHierarchy: function(e) {
        if (!e.dataItem.HasDetails) {
            $(e.row).find(".t-hierarchy-cell .t-icon").hide();
        }
    },
    FilterGridByDateRange: function (grid, startDate, endDate) {
        if (grid) {
            $("tr", grid.$tbody).each(function (index, element) {
                var row = $(element);
                var taskStartDate = $("td.start-date", row).text().split(" ")[0];
                var taskEndDate = $("td.end-date", row).text().split(" ")[0];
                if (taskStartDate) {
                    if (taskStartDate) {
                        if ((taskStartDate >= startDate && taskStartDate < endDate) || (taskEndDate < endDate && taskEndDate >= startDate) || (startDate >= taskStartDate && startDate < taskEndDate)) {
                            row.show();
                        }
                        else {
                            row.hide();
                        }
                    } else {
                        row.hide();
                    }
                }
                else {
                    row.hide();
                }
            });
        }
    }

});
$.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }