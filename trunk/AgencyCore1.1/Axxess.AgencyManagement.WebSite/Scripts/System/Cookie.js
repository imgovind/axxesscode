﻿if (typeof U == "undefined") var U = new Object();
$.extend(U, {
    Cookie: {
        Delete: function(id) { $.cookie(id + "-" + Acore.AgencyId + "-" + Acore.UserId, null) },
        Get: function(id) { return JSON.parse($.cookie(id + "-" + Acore.AgencyId + "-" + Acore.UserId)) },
        Set: function(id, val) { $.cookie(id + "-" + Acore.AgencyId + "-" + Acore.UserId, JSON.stringify(val), { expires: Acore.CookieLife }) }
    }
});