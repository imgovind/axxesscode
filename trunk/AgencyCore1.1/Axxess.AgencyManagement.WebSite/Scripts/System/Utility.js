if (typeof U == "undefined") var U = new Object();
$.extend(U, {
    DangerousRequestMessage: "[HttpRequestValidationException (0x80004005): A potentially dangerous Request.Form value was detected from the client",
    DefaultErrorMessage: "This page can't be loaded. Please close this window and try again.",
    GuidEmpty: "00000000-0000-0000-0000-000000000000",
    MinDate: null,
    GetMinDate: function () {
        if (U.MinDate == null) {
            U.MinDate = new Date();
            U.MinDate.setFullYear(1, 0, 1);
            U.MinDate.setHours(0, 0, 0, 0);
        }
        return U.MinDate;
    },
    DisplayDate: function() {
        var currentDate = new Date(),
            days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            dateSuffix = "th";
        if (currentDate.getDate() % 10 == 1 && currentDate.getDate() != 11) dateSuffix = "st";
        else if (currentDate.getDate() % 10 == 2 && currentDate.getDate() != 12) dateSuffix = "nd";
        else if (currentDate.getDate() % 10 == 3 && currentDate.getDate() != 13) dateSuffix = "rd";
        return days[currentDate.getDay()] + ", " + months[currentDate.getMonth()] + " " + currentDate.getDate() + dateSuffix + ", " + currentDate.getFullYear()
    },
    FormatMoney: function (num) {
        if (isNaN(Number(num))) return "";
        var result = String(Math.round(Math.abs(num) * 100));
        result = result.substr(0, result.length - 2) + "." + result.substr(result.length - 2);
        for (var i = 0; i * 4 + 6 < result.length; i++)
            result = result.substr(0, result.length - (i * 4 + 6)) + "," + result.substr(result.length - (i * 4 + 6));
        result = "$" + result;
        if (num < 0) result = "-" + result;
        return result;
    },
    GetInt: function (field, defaultValue) {
        if (field.length) {
            var value = field.val();
            if ($.isNumeric(value)) {
                return +value;
            }
            else {
                return defaultValue;
            }
        }
        else {
            return defaultValue;
        }
    },
    IsTime: function (Time) {
        return /^(0[1-9]|1[012]):[0-5][0-9] [AP]M$/.test(Time)
    },
    IsGuid: function (Guid) {
        return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(Guid) && !(/^[0-]*$/.test(Guid))
    },
    IsEmail: function (Email) {
        return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(Email)
    },
    IsDate: function (date) {
        if (date && date.match(/^(0?[1-9]|1[012])\/(0?[1-9]|[12]\d|3[01])\/(19|20)?\d\d$/)) {
            date = date.split("/");
            if (date[0].match(/^(0?[469]|11)$/) && parseInt(date[1]) > 30) return false; // More than 30 days in April, June, September, or November
            if (date[0].match(/^(0?2)$/)) {
                if (parseInt(date[2]) % 4 && parseInt(date[1]) > 28) return false; // More than 28 days in February on non-Leap Year
                if (parseInt(date[2]) % 4 == 0 && parseInt(date[1]) > 29) return false; // More than 29 days in February on a Leap Year
            }
            return true;
        }
        return false;
    },
    Y2KConvert: function (ShortYear) {
        var CurrentYear = String(new Date().getFullYear()),
            CurrentPrefix = parseInt(CurrentYear.substr(0, 2)),
            CurrentShortYear = parseInt(CurrentYear.substr(2, 2));
        if (parseInt(ShortYear) > CurrentShortYear) return String(--CurrentPrefix) + String(ShortYear);
        else return String(CurrentPrefix) + String(ShortYear);
    },
    EnableDatePicker: function (e) { $(".date", e.row).DatePicker(); },
    ValidationError: function (control) {
        if (control.closest(".window-content").find(".error:first").closest("fieldset").length) var scroll = control.closest(".window-content").find(".error:first").closest("fieldset").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("td").length) var scroll = control.closest(".window-content").find(".error:first").closest("td").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("div").length) var scroll = control.closest(".window-content").find(".error:first").closest("div").position().top;
        else var scroll = 0;
        control.closest(".window-content").scrollTop(scroll);
        U.Growl("Error: There was a problem validating your form, please review your information and try again.", "error");
    },
    GetAttachment: function (Url, Data) {
        $.fileDownload(Url, {
            data: Data,
            httpMethod: "POST",
            successCallback: function (url) { },
            failCallback: function (responseHtml, url) {
                U.Growl(JSON.parse(responseHtml.replace("<pre>", "").replace("</pre>", "")).errorMessage, 'error');
            }
        });
    },
    HideOptions: function () {
        $(".option").each(function () {
            if ($(this).closest("li").find(".extra").length) {
                if ($(this).find("input").is(":not(:checked)")) {
                    $(this).closest("li").find(".extra").addClass("form-omitted").hide();

                }
                $(this).find("input").on("change", function () {
                    $(this).closest("li").find(".extra").toggle().find(".checkgroup .extra").each(function () {
                        $(this).addClass("form-omitted").hide();
                        if ($(this).closest("li").find(".option input").is(":checked")) {
                            $(this).removeClass("form-omitted").show();
                        }
                    });
                });
            }
        });
    },
    ShowTxtWhenChooseOther: function () {
        $(".otherOption").each(function () {
            if ($(this).closest("div").find(".otherTxt").length) {
                if ($(this).val().toLowerCase() != "other") {
                    $(this).closest("div").find(".otherTxt").hide();
                }
                $(this).on("change", function () {
                    if ($(this).val().toLowerCase() == "other") {
                        $(this).closest("div").find(".otherTxt").show();
                    } else {
                        $(this).closest("div").find(".otherTxt").hide();
                    }
                });
            }
        });
    },
    DeselectableRadio: function (control) {
        if (control == undefined) control = $("#desktop");
        $("input[type=radio].deselectable", control).mouseup(function () {
            if ($(this).prop("checked")) $(this).addClass("deselect");
        });
        $("input[type=radio].deselectable", control).on("click", function () {
            if ($(this).hasClass("deselect")) {
                $(this).removeClass("deselect").prop("checked", false).change();
            } else return true;
        });
    },
    AjaxError: function () {
        return U.MessageError("There was an error loading this window.", "Please try again later, if problem persists, contact Axxess for assistance.");
    },
    Message: function (Title, Text, Class, ButtonItems) {
        return $("<div/>", { "class": "error-box" }).addClass(Class).append(
                    $("<div/>", { "class": "logo" }).append(
                        $("<span/>", { "class": "img icon48 fl" }).addClass(Class)).append(
                        $("<h1/>", { "text": Title })).append(
                        $("<p/>", { "text": Text })).append(
                        $("<div/>").Buttons(ButtonItems)));
    },
    MessageError: function (Title, Text, Buttons, Html) {
        var ReturnValue = U.Message(Title, Text, "error", Buttons);
        return Html ? ReturnValue.html() : ReturnValue;
    },
    MessageErrorAjax: function () {
        return U.MessageError("Request Error", "An error has been detected while procuring data. Please check your connection and try again, if problem persists, contact Axxess for assistance.");
    },
    MessageErrorJS: function (Growl) {
        var Title = "Browser Error",
            Text = "An error has been detected in your browser. Please refresh the page and try again, if problem persists, contact Axxess for assistance.";
        if (Growl) return Title + "<br />" + Text;
        else return U.MessageError(Title, Text);
    },
    MessageInfo: function (Title, Text, Buttons, Html) {
        var ReturnValue = U.Message(Title, Text, "info", Buttons);
        return Html ? ReturnValue.html() : ReturnValue;
    },
    MessageSuccess: function (Title, Text, Buttons, Html) {
        var ReturnValue = U.Message(Title, Text, "success", Buttons);
        return Html ? ReturnValue.html() : ReturnValue;
    },
    MessageWarn: function (Title, Text, Buttons, Html) {
        var ReturnValue = U.Message(Title, Text, "warning", Buttons);
        return Html ? ReturnValue.html() : ReturnValue;
    },
    Growl: function (message, theme, life) {
        if (typeof message == "object") message = message.toSource();
        else if (typeof message != "string") message = String(message);
        $.jGrowl($.trim(message), { theme: theme, life: (life > 0 ? life : 5000) })
    },
    GrowlResult: function(result) {
        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
    },
    PhoneAutoTab: function(name) {
        $('#' + name + '1').autotab({ target: name + '2', format: 'numeric' });
        $('#' + name + '2').autotab({ target: name + '3', format: 'numeric', previous: name + '1' });
        $('#' + name + '3').autotab({ format: 'numeric', previous: name + '2' });
    },
    Delete: function (name, url, data, callback, bypassAlerts, excludeThis) {
        var action = function () {
            U.PostUrl(url, data, function (result) {
                if (result.isSuccessful) {
                    if (callback != undefined && typeof (callback) == 'function') callback(result);
                    if (!bypassAlerts) U.Growl(name + " has been successfully deleted.", "success");
                } else U.Growl(result.errorMessage, "error");
            });
        };
        if (bypassAlerts) action();
        else Acore.Confirm({ Message: "Are you sure you want to delete " + (excludeThis ? name : "this " + name.toLowerCase()) + "?", Yes: action });
    },
    DeleteTemplateWithInput: function (type, input, callback, name) {
        if (name == undefined) name = type;
        U.Delete(name, type + "/Delete", input,
            function () {
                if (callback != undefined && typeof (callback) == 'function') callback();
            });
    },
    //If isCallBackRefresh is true then it contains the refresh for the related list
    DeleteTemplate: function (type, id, callback, name, isCallBackRefresh) {
        if (name == undefined) name = type;
        U.Delete(name, type + "/Delete", { Id: id },
             function () {
                 if (!isCallBackRefresh) eval(type + ".RebindList()");
                 if (callback != undefined && typeof (callback) == 'function') callback();
             }
        );
    },
    InitValidation: function (form, callback, before) {
        form.Validate({ Success: callback, BeforeSubmit: before });
    },
    InitForm: function (form, callback, before) {
        $(".names", form).alpha({ nocaps: false });
        U.InitValidation(form, callback, before);
    },
    Submit: function (Form, Button) {
        if (Form.valid()) {
            Button.trigger("ProcessingStart");
            Form.submit();
        }
    },
    InitTemplate: function (formobj, callback, message, before) {
        $(".names").alpha({ nocaps: false });
        formobj.Validate({ Message: message, Before: before, Success: function (result) { if (callback != undefined && typeof (callback) == 'function') callback(result); } });
        $(formobj.selector + " .zip").ZipInput(formobj);
    },
    InitEditTemplate: function (type, callback, literalType) {
        if (literalType == undefined) literalType = U.TypeToWords(type);
        U.InitTemplate($("#edit" + type + "Form"), function () {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
        }, literalType + " has been successfully saved.");
    },
    InitNewTemplate: function (type, callback, literalType) {
        if (literalType == undefined) literalType = U.TypeToWords(type);
        U.InitTemplate($("#new" + type + "Form"), function () {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
        }, literalType + " has been successfully created.");
    },
    ShowDialog: function (popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function () {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            }
        });
    },
    CloseDialog: function () { $.unblockUI(); },
    GetUrl: function (url, input, onSuccess) {
        $.ajax({
            url: url,
            data: input,
            dataType: 'json',
            success: function (data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            }
        });
    },
    PostUrl: function (url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function (data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    PostTrackedUrl: function(url, input, onSuccess, onFailure, id) {
        var request = U.PostHelper(url, input, onSuccess, onFailure);
        if (id) RequestTracker.GridTrack(id, request, url);
        else RequestTracker.Track(request, url);
    },
    LoadTrackedUrl: function(url, input, callback) { RequestTracker.Track(U.PostHelper(url, input, callback, callback, 'html'), url); },
    PostHelper: function(url, input, onSuccess, onFailure, dataType) {
        return $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: dataType ? dataType : 'json',
            success: function (data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function (data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    Block: function () {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: "<img src='/Images/loading.gif' style='padding:4em;'>",
            css: {
                "background-color": "#fff",
                "border": "0 none",
                "top": "50%",
                "left": "50%",
                "margin": "-10em",
                "text-align": "center",
                "width": "20em",
                "height": "20em",
                "border-radius": "20em",
                "-moz-border-radius": "20em",
                "-webkit-border-radius": "20em",
                "-o-border-radius": "20em",
                "-khtml-border-radius": "20em",
                "box-shadow": "-.3em -.5em 3em #000 inset",
                "-moz-box-shadow": "-.3em -.5em 3em #000 inset",
                "-webkit-box-shadow": "-.3em -.5em 3em #000 inset",
                "-o-box-shadow": "-.3em -.5em 3em #000 inset",
                "-khtml-box-shadow": "-.3em -.5em 3em #000 inset"
            }
        });
    },
    UnBlock: function (selector) {
        $.unblockUI();
    },
    ToTitleCase: function (text) {
        var txt = '';
        var txtArray = text.toLowerCase().split(' ');
        if (txtArray.length > 1) {
            var i = 0;
            for (i = 0; i < txtArray.length; i++) {
                txt += txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
            }
        }
        else {
            txt = text.toLowerCase().substr(0, 1).toUpperCase() + text.toLowerCase().substr(1);
        }
        return txt;
    },
    TypeToWords: function (input) {
        var result = input;
        var txtArray = input.match(/([A-Z]?[^A-Z]*)/g).slice(0, -1);
        if (txtArray.length > 1) {
            for (var i = 0; i < txtArray.length; i++) {
                if (i == 0) result = txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
                else result += txtArray[i].toLowerCase() + ' ';
            }
        }
        return result.trim();
    },
    ClearRadio: function (selector) {
        $("input[name=" + selector + "]").each(function () { $(this).removeAttr('checked'); });
    },
    ChangeToRadio: function (selector) {
        var $checkbox = $("input[name=" + selector + "]");
        $checkbox.on("click", function () {
            if ($(this).attr('checked')) {
                $checkbox.removeAttr('checked');
                $(this).attr('checked', true);
            }
        });
    },
    TimePicker: function (selector) {
        $(selector).TimePicker();
    },
    BlockClick: function () {
        return false;
    },
    PullMultiComboBoxValues: function (comboBx) {
        if (comboBx.val() != "") {
            return comboBx.val();
        }
        var checkedOptions = comboBx.next().children(".checked");
        var inputs = checkedOptions.find("input");
        return inputs.map(function () { return this.value; }).get().join(',');
    },
    HtmlEncode: function (value) {
        if (value) {
            return jQuery('<div />').text(value).html();
        } else {
            return '';
        }
    },
    HtmlDecode: function (value) {
        if (value) {
            return $('<div />').html(value).text();
        } else {
            return '';
        }
    },
    DoIfChecked: function (show, checkbox, doFunction, undoFunction) {
        if (checkbox.prop("checked") == show) {
            doFunction();
        } else {
            undoFunction();
        }
        checkbox.on("change", function () {
            if (checkbox.prop("checked") == show) {
                doFunction();
            } else {
                undoFunction();
            }
        });
    },
    LoadContent: function (contentUrl, contentElement) {
        if (contentUrl)
            $(contentElement).html("&#160;").addClass("loading").load(contentUrl, null, function () {
                $(contentElement).removeClass("loading");
            });
    },
    BasicTabSetup: function (Element) {
        Element.InitForms();
        $("a.back", Element).on("click", function () {
            var tabStrip = $("a.back").closest(".ui-tabs");
            tabStrip.find("li.ui-tabs-active").prev("li").find("a").click();
        });
    },
    Navigate: function (form, callback) {
        U.InitValidation(form, function (result) {
            var tabStrip = form.closest(".ui-tabs"); //jquery object of the tab strip
            var index = tabStrip.tabs("option", "active"); //.data("uiTabs").options.selected;
            tabStrip.tabs("enable", index + 1);
            tabStrip.data("uiTabs").active.next().find("a").click();
            if (typeof callback == "function") callback(result);
        });
    },
    NavigateOrExit: function (anchorControl) {
        if (anchorControl.hasClass("exit")) anchorControl.closest('.window').Close();
        else if (anchorControl.hasClass("next")) {
            var tabStrip = anchorControl.closest(".ui-tabs"); //jquery object of the tab strip
            var index = tabStrip.tabs("option", "active"); //tabStrip.data("uiTabs").options.selected;
            tabStrip.tabs("enable", index + 1);
            tabStrip.data("uiTabs").active.next().find("a").click();
        }
    },
    DisableTabs: function (tabSelector) {
        var indexes = [];
        $(tabSelector).find("li").each(function (index) {
            if ($(this).hasClass("disabled")) indexes.push(index);
        });
        $(tabSelector).tabs('option', 'disabled', indexes);
    },
    UnBlockTabAndEnable: function (tabStrip) {
        var element = $("ul li.ui-tabs-active", tabStrip).next();
        element.removeClass("t-state-disabled");
        element.find(".t-link").off('click', U.BlockClick);
    },
    DataItemToolTip: function (e) { U.ToolTip(e.row); },
    ToolTip: function (element) {
        $("a.note", element).each(function () { U.ToolTipHelper($(this)); });
    },
    ToolTipsByElements: function (elements) {
        elements.each(function () { U.ToolTipHelper($(this)); });
    },
    ToolTipHelper: function (element) {
        element.Tooltip({
            extraClass: "sticky-note",
            isNote: true
        });
    },
    FormatDate: function (date) {
        var dateToFormat = new Date(date);
        var month = dateToFormat.getMonth() + 1;
        var day = dateToFormat.getDate().toString();
        if (day.length == 1) day = "0" + day;
        var year = dateToFormat.getFullYear();
        return month + "/" + day + "/" + year;
    },
    FormatTime: function (date) {
        var dateToFormat = new Date(date);
        var hours = dateToFormat.getHours();
        if (hours >= 12) {
            hours -= 12;
            var meridian = "PM";
        } else var meridian = "AM";
        if (hours == 0) hours = 12;
        var minutes = dateToFormat.getMinutes().toString();
        if (minutes.length == 1) minutes = "0" + minutes;
        return hours + ":" + minutes + " " + meridian;
    },
    ConvertJsonDateToJavascriptDate: function (jsonDate) {
        if (Object.prototype.toString.call(jsonDate) === '[object Date]') return jsonDate;
        else if (typeof jsonDate == "string" && jsonDate.indexOf("Date") != -1 && jsonDate.length > 6) return new Date(parseInt(jsonDate.substr(6)));
        return new Date();
    },
    ConvertToTime: function (time) {
        if (time) {
            var hours = +time.substring(0, 2);
            var minutes = +time.substring(3, 5);
            var pm = time.indexOf("P") != -1;
            if (pm && hours !== 12) {
                hours += 12;
            }
            if (!pm && hours == 12) {
                hours -= 12;
            }
            var date = new Date();
            date.setHours(hours);
            date.setMinutes(minutes);
            return date;
        }
        else
            return new Date();
    },
    NumberOfDaysInBetween: function (date1, date2) {
        var intMilDay = 24 * 60 * 60 * 1000;
        var intMilDif = date2 - date1;
        var intDays = Math.floor(intMilDif / intMilDay);
        return intDays;
    },
    ToggleCollapsableContent: function (e, callback) {
        $("h3 > .img", e).toggleClass("more").toggleClass("less");
        if (Acore.Animated) e.find("fieldset").toggle(Acore.AnimationSpeed, typeof callback == "function" ? callback : function () { });
        else {
            e.find("fieldset").toggle();
            if (typeof callback == "function") callback();
        }
    },
    SetPrefix: function (element, prefix) {
        if (prefix) {
            $('[id]', element).each(function () {
                $(this).attr("id", prefix + "-" + $(this).attr("id"));
                if ($(this).hasClass("date")) {
                    $(this).DatePicker("ChangeId", $(this).attr("id"));
                }
            });
            $('[for]', element).each(function () {
                $(this).attr("for", prefix + "-" + $(this).attr("for"));
            });
            $('[template]', element).each(function () {
                var temp = $(this).attr("template");
                $(this).attr("template", temp.slice(0, 1) + prefix + "-" + temp.slice(1));
            });
        }
    },
    GenerateTitleFromPrefix: function (prefix) {
        var title;
        if (!prefix) {
            title = "Home Health ";
        } else {
            title = prefix.match(/([A-Z]?[^A-Z]*)/g).slice(0, -1).join(" ") + " ";
        }
        return title;
    },
    AppendAreaToForm: function (e, prefix) {
        if (prefix) {
            var form = $("form", e);
            if (form.length) form.attr("action", prefix + form.attr("action"));
        }
    },
    InitShared: function (r, t, x, e) {
        $("form", e).Validate();
    },
    FormatInternationalDate: function (date) {
        var yyyy = date.getFullYear().toString(),
            mm = (date.getMonth() + 1).toString(),
            dd = date.getDate().toString();
        return yyyy + "-" + (mm[1] ? mm : "0" + mm) + "-" + (dd[1] ? dd : "0" + dd);
    },
    //Serializes a JQuery array and passes it to JsonInput to be converted to Json 
    SerializeForm: function (form) {
        var o = {};
        if (form.length) {
            var a = form.serializeArray();
            o = U.JsonInput(a);
        }
        return o;
    },
    //Converts a JQuery array to Json
    JsonInput: function (arrayInputs) {
        var o = {};
        if (arrayInputs) {
            $.each(arrayInputs, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
        }
        return o;
    },
    ServicesContainerInit: function (containerPrefix, e) {
        var services = $("input[name=ServiceProvided]", e);
        if (services) {
            services.each(function () {
                U.ServicesContainerInitHelper($(this), $("#" + containerPrefix + $(this).val(), e));
            });
            services.on("click", function () {
                U.ServicesContainerInitHelper($(this), $("#" + containerPrefix + $(this).val(), e));
            });
        }
    },
    ServicesContainerInitHelper: function (serviceControl, container) {
        if ($(serviceControl).is(':checked')) {
            container.show();
            $("input,select,textarea", container).each(function () { $(this).removeClass("ignorevalidation"); });
        } else {
            $("input,select,textarea", container).each(function () { $(this).addClass("ignorevalidation"); });
            container.hide();
        }
    },
    GetWidth: function(elements) {
        var totalWidth = 0;
        elements.each(function() {
            totalWidth += parseInt($(this).outerWidth());
        });
        return totalWidth
    }
});
$.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }