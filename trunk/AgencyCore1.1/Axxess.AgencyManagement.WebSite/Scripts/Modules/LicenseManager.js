﻿var LicenseManager = {
    New: function(userId) {
        Acore.Modal({
            Name: "Add New License",
            Url: "License/New",
            Input: { userId: userId },
            OnLoad: LicenseManager.InitNew,
            Width: "650px",
            WindowFrame: false
        });
    },
    Delete: function(licenseId, userId) {
        var editUserId = $("#EditUser_LicenseGrid").attr("guid");
        U.DeleteTemplateWithInput("License", { Id: licenseId, userId: userId }, function() {
            LicenseManager.Refresh();
            if (userId == editUserId) User.License.Refresh(userId);
        });
    },
    Edit: function(licenseId, userId) {
        Acore.Modal({
            Name: "Edit License",
            Url: "License/Edit",
            Input: { licenseId: licenseId, userId: userId },
            OnLoad: LicenseManager.InitEdit,
            Width: "650px",
            WindowFrame: false
        });
    },
    InitNew: function(r, t, x, e) {
        var type = "NewLicense", prefix = "#" + type + "_";
        U.ShowIfSelectEquals($(prefix + "Type", e), "Other", $(prefix + "TypeMore", e));
        $("#NewLicense_Form", e).Validate({ Success: function(result) { LicenseManager.Refresh(); User.License.Refresh(result.Id); } });
        $("[name=ExpireDate]", e).rules("add", { greaterThan: ["[name=IssueDate]", "Issue Date"] });
    },
    InitEdit: function(r, t, x, e) {
        var type = "EditLicense", prefix = "#" + type + "_";
        U.ShowIfSelectEquals($(prefix + "Type", e), "Other", $(prefix + "TypeMore", e));
        $("#EditLicense_Form", e).Validate({ Success: function(result) { LicenseManager.Refresh(); User.License.Refresh(result.Id); } });
        $("[name=ExpireDate]", e).rules("add", { greaterThan: ["[name=IssueDate]", "Issue Date"] });
    },
    Refresh: function() {
        $("#LicenseManager_List ol").addClass("loading");
        $("#LicenseManager_List").load("License/ManagerList", {},
            function(responseText, textStatus) {
                Acore.OnLoad($(this));
                $('#LicenseManager_List ol').removeClass("loading");
                if (textStatus == 'error') $('#LicenseManager_List').html(U.AjaxError);
            }
        );
    }
}

//$("#LicenseManager_List").Load("cotroller-url")