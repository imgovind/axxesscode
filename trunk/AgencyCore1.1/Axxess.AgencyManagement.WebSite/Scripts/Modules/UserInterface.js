var UserInterface = {

    ShowPrint: function(Options) {
        var Arguments = {
            UrlPrefix: Options.Area,
            Url: Options.Url,
            PdfUrl: Options.PdfUrl,
            PdfData: Options.Data
        };
        if (Options.QA) $.extend(Arguments, {
            ReturnClick: function() {
                if (typeof Options.Return === "function") {
                    try {
                        Options.Return();
                    } catch (e) {
                        U.Growl("Unable to return the note.  Please contact Axxess for assistance.", "error");
                        console.log("Error in return function on load for " + Options.Type + ": " + e.toString());
                        $.error(e);
                    }
                }

            },
            Buttons: [
                {
                    Text: "Edit",
                    Click: function() {
                        if (typeof Options.Load === "function") {
                            try {
                                Options.Load();
                            } catch (e) {
                                U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                console.log("Error in edit function on load for " + Type + ": " + e.toString());
                                $.error(e);
                            }
                        }
                    }
                }, {
                    Text: "Approve",
                    Click: function() {
                        if (typeof Options.Approve === "function") {
                            try {
                                Options.Approve();
                            } catch (e) {
                                U.Growl("Unable to approve the note.  Please contact Axxess for assistance.", "error");
                                console.log("Error in approve function on load for " + Type + ": " + e.toString());
                                $.error(e);
                            }
                        }
                    }
                }
            ]
        });
        Acore.OpenPrintView(Arguments);
    },
    ShowNoteModal: function(Text, Color) {
        if (Color != "red" && Color != "blue") Color = "yellow";
        $("body").append(
            $("<div/>", { id: "shade" })).append(
            $("<div/>").addClass("note-modal bottom " + Color)).append(
            $("<div/>").addClass("note-modal middle " + Color)).append(
            $("<div/>").addClass("note-modal top " + Color).append(
                $("<div/>").addClass("note-close").text("X")).append(
                $("<div>").addClass("note-content").html(Text)));
        $(".note-close").on("click", function() {
            $("#shade,.note-modal").remove();
        });
    },
    ShowPatientChart: function(Id, service) {
        if (Id != undefined && U.IsGuid(Id)) {
            service = service || 1;
            if (Acore.Windows.PatientCharts.IsOpen) {
                var selectedTd = $('td:contains(' + Id + ')', $('#PatientSelectionGrid'));
                if (selectedTd.length) { selectedTd.closest('tr').click(); }
                else {
                    U.PostUrl("/Patient/Get", { id: Id }, function(result) {
                        var status = result[Acore.AppService(service) + "Status"];
                        if (status == 1 || status == 2) {
                            Patient.Charts.PatientId = Id;
                            $("#PatientCharts_BranchId").val("00000000-0000-0000-0000-000000000000");
                            $("#PatientCharts_StatusId").val(status);
                            $("#PatientCharts_ServicesId").val(service);
                            $("#PatientCharts_PaymentSourceId").val(0);
                            Patient.Charts.PatientSelector.Rebind();
                        }
                    });
                }
                $("#window_PatientCharts").WinFocus();
            } else { Acore.Open("PatientCharts", "Patient/Charts", { patientId: Id, status: 1, Service: service }); }
        }
    },
    ShowScheduleCenter: function(Id, patientStatus) {
        if (Id != undefined && U.IsGuid(Id)) {
            if (Acore.Windows.ScheduleCenter.IsOpen) {
                var selectedTd = $('td:contains(' + Id + ')', $('#ScheduleSelectionGrid'));
                if (selectedTd != undefined && selectedTd.length > 0) { selectedTd.closest('tr').click(); }
                else {
                    U.PostUrl("/Patient/Get", { id: Id }, function(result) {
                        if (result != null && (result.Status == 1 || result.Status == 2)) {
                            var input = { BranchId: "00000000-0000-0000-0000-000000000000", StatusId: result.Status, PaymentSourceId: 0 };
                            Schedule.Center.HomeHealth.PatientId = Id;
                            Schedule.Center.HomeHealth.RebindPatientsSelection(input);
                            $("#ScheduleCenter_BranchId").val("00000000-0000-0000-0000-000000000000");
                            $("#ScheduleCenter_StatusId").val(result.Status);
                            $("#ScheduleCenter_PaymentSourceId").val(0);
                        }
                    });
                }
                $("#window_ScheduleCenter").WinFocus();
            } else { Acore.Open("ScheduleCenter", "Schedule/Center/1/" + Id, Schedule.Center.HomeHealth.Init); }
        }
    },
    ShowMessages: function(messageId) { Acore.Open("Inbox", 'Message/Inbox', function() { Message.Init(); Message._InboxType = "inbox"; Message._MessageId = messageId; }); },
    ShowNewMessage: function() { Acore.Open("newmessage", 'Message/New', function() { Message.InitNew();}); },
    ShowModalNewMessageFolder: function() {
        Acore.Modal
        ({
            Name: "Add Custom Folder",
            Url: "MessageFolder/New",
            OnLoad: MessageFolder.InitNew,
            Width: 400,
            VersitileHeight: true
        })
    },
    ShowUserTasks: function() {
        Acore.Open("MySchedule", 'Schedule/Schedule', function() { });
    },
    ShowPastDueRecerts: function() {
        Acore.Open("listpastduerecerts", 'Schedule/RecertsPastDueGrid', U.ListInitClientRebind);
    },
    ShowUpcomingRecerts: function() {
        Acore.Open("listupcomingrecerts", 'Schedule/RecertsUpcomingGrid', U.ListInitClientRebind);
    },
    ShowPatientCharts: function() {
        Acore.Open("PatientCharts", 'Patient/Charts');
    },
    ShowNewContact: function() {
        Acore.Open("newcontact", 'Contact/New', function() { Contact.InitNew(); });
    },
    ShowEditContact: function(Id) {
        Acore.Open("editcontact", "Contact/Edit", function() { Contact.InitEdit(); }, { Id: Id });
    },
    ShowNewLocation: function() {
        Acore.Open("newlocation", 'Location/New', function() { Location.InitNew(); });
    },
    ShowEditLocation: function(Id) {
        Acore.Open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { Id: Id });
    },
    ShowBillingCenterRAP: function() {
        Acore.Open("billingcenterrap", "Billing/RAPCenter", function() { });
    },
    ShowBillingCenterFinal: function() {
        Acore.Open("billingcenterfinal", "Billing/FinalCenter", function() { });
    },
    ShowBillingCenterAllClaim: function() {
        Acore.Open("allbillingclaims", "Billing/AllClaims", function() { });
    },
    
    ShowPatientVitalSigns: function(patientId) {
        Acore.Open("patientvitalsigns", 'Patient/VitalSigns', function() { }, { PatientId: patientId });
    },
    ShowMedicareEligibilityReports: function(patientId) {
        Acore.Open("medicareeligibilitylist", 'Patient/MedicareEligibilityList', function() { }, { patientId: patientId });
    },
    ShowPatientHospitalizationLogs: function(Id) {
        Acore.Open("patienthospitalizationlogs", { patientId: Id });
    },
    ShowPatientEligibility: function(medicareNumber, lastName, firstName, dob, gender) {
        if (medicareNumber == "" || lastName == "" || firstName == "" || dob == "" || gender == undefined) {
            var error = "Unable to process Medicare eligibility request:";
            if (medicareNumber == "") error += "<br /> &#8226; Medicare Number is Required";
            if (lastName == "") error += "<br /> &#8226; Last Name is Required";
            if (firstName == "") error += "<br /> &#8226; First Name is Required";
            if (dob == "") error += "<br /> &#8226; Date of Birth is Required";
            if (gender == undefined) error += "<br /> &#8226; Gender is Required";
            U.Growl(error, "error");
        } else {
            Acore.Modal({
                Name: "Medicare Eligibility",
                Url: "PatientProfile/Verify",
                Input: {
                    medicareNumber: medicareNumber,
                    lastName: lastName,
                    firstName: firstName,
                    dob: dob,
                    gender: gender
                },
                Width: "800px",
                Height: "400px",
                WindowFrame: false
            })
        }
    },
    ShowMedicareEligibility: function(medEligibilityId, patientId) {
        Acore.Modal({
            Name: "Medicare Eligibility",
            Url: "PatientProfile/MedicareEligibility",
            Input: {
                medicareEligibilityId: medEligibilityId,
                patientId: patientId
            },
            Width: "800px",
            Height: "400px",
            WindowFrame: false
        })
    },
    ShowMedicationModal: function(patientId, isnew) {
        Acore.Modal({
            Name: "Medications",
            Url: "PlanOfCare/Medication",
            Input: { patientId: patientId },
            OnLoad: function() {
                $("#PlanofCareMedicationIsNew").val(isnew);
                $("#window_ModalWindow_content").css("overflow", "hidden");
                $('#MedicationGrid485 .t-grid-content').css("top", "60px")
            },
            Width: "900px",
            Height: "400px",
            WindowFrame: false
        })
    },
    CloseWindow: function(window) {
        if (Acore.Windows[window] && Acore.Windows[window].IsOpen) $("#window_" + window).Close();
    },
    ShowNewLocationBillData: function(locationId) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/NewLocationBillData",
            Input: { LocationId: locationId },
            OnLoad: Agency.InitLocationBillDate,
            Width: "550px",
            Height: "345px",
            WindowFrame: false
        })
    },
    ShowEditLocationBillData: function(locationId, Id) {
        Acore.Modal({
            Name: "Visit Bill Info.",
            Url: "Agency/EditLocationBillData",
            Input: { LocationId: locationId, Id: Id },
            OnLoad: Agency.InitLocationEditBillDate,
            Width: "550px",
            Height: "345px",
            WindowFrame: false
        })
    },
    ShowFrequencies: function(Id, patientId) {
        Acore.Dialog({
            Name: "Visit Frequencies and Count",
            Url: "Oasis/Frequencies",
            Input: { episodeId: Id, patientId: patientId },
            Width: 500
        })
    },
    ShowNewAdjustmentCode: function() {
        Acore.Open("newadjustmentcode", 'AdjustmentCode/New', function() { AdjustmentCode.InitNew(); });
    },
    ShowEditAdjustmentCode: function(id) {
        Acore.Open("editadjustmentcode", "AdjustmentCode/Edit", function() { AdjustmentCode.InitEdit(); }, { Id: id });
    },
    ShowNewUploadType: function() {
        Acore.Open("newuploadtype", "UploadType/New", function() { UploadType.InitNew(); });
    },
    ShowEditUploadType: function(id) {
        Acore.Open("edituploadtype", "UploadType/Edit", function() { UploadType.InitEdit(); }, { Id: id });
    },
    EditReturnComments: function(id, existing, element, urlPrefix) {
        Acore.Modal({
            Name: "Edit Return Comment",
            Content: $("<div/>").addClass("main wrapper").append(
                $("<fieldset/>").append(
                    $("<legend/>").text("Edit Return Comment")).append(
                    $("<div/>").addClass("wide-column").append(
                        $("<div/>").addClass("row").append(
                            $("<textarea>").addClass("tall").val(existing))))).append(
                $("<div/>").Buttons([
                    {
                        Text: "Update",
                        Click: function() {
                            U.PostUrl(urlPrefix + "/Schedule/EditReturnReason", { id: id, comment: $(this).closest(".main").find("textarea").val() }, function(Result) {
                                U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                if (Result.isSuccessful) {
                                    $(".return-comments-container").each(function() { $(this).ReturnComments({ Action: "Refresh", UrlPrefix: urlPrefix }) });
                                    Acore.CloseDialog()
                                }
                            })
                        }
                    }, {
                        Text: "Cancel",
                        Click: function() { Acore.CloseDialog() }
                    }
                ])
            ),
            Width: 400,
            WindowFrame: false
        })
    }

}
