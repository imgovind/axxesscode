﻿var CommunicationNote = {
    ListWindowId: "communicationnoteslist",
    Edit: function (patientId, id, prefix) { Acore.Open("CommunicationNote", { patientId: patientId, Id: id }, (prefix || "") + "/" + Acore.Windows["CommunicationNote"].Url); },
    InitNew: function (r, t, x, e) {
        var service = $("input[name=ServiceId]", e).val();
        var serviceName = Acore.AppService(service);
        Episode.InitPatientChangeDropDown(e, serviceName);
        CommunicationNote.Init(r, t, x, e);
    },
    Init: function (r, t, x, e) {
        var form = $("form.mainform", e);
        $("input.send-as-message", e).on("click", function () { $(".togglable-recipients", e).toggle(); });
        U.InitForm(form, function (data) {
            Refresh.All(data);
        });
    },
    InitList: function (r, t, x, e) {
        $(".new-communication-note", e).on("click", function () {
            var temp = $("[name=ServiceId]", e).val();
            CommunicationNote[Acore.AppService(temp)].New($("[name=PatientId]", e).val());
            return false;
        });
        var episodeDropDown = $("select[name=EpisodeId]", e);
        if (episodeDropDown.length) {
            var episodeSetDateFunc = function () {
                if (episodeDropDown.val() != U.GuidEmpty) {
                    var range = episodeDropDown.find("option:selected").text();
                    $("input[name=StartDate]", e).val(range.split("-")[0].trim());
                    $("input[name=EndDate]", e).val(range.split("-")[1].trim());
                }
            };
            episodeSetDateFunc();
            episodeDropDown.change(episodeSetDateFunc);
            $("[name=ServiceId]").change(function () {
                var service = Acore.AppService($(this).val());
                Episode[service].LoadDropDown(episodeDropDown, $("input[name=PatientId]", e));
                episodeDropDown.prev("label").text(Episode[service].Label);
            });
        }
        U.ListInitWithArea(r, t, x, e);
    },
    Delete: function (patientId, id, url, callback) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this task?",
            Yes: function () {
                U.PostUrl(url, { Id: id, patientId: patientId }, function (result) {
                    if (result.isSuccessful && typeof callback === "function") callback();
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                })
            }
        })
    },
    Process: function (button, patientId, eventId, prefix, reason) {
        var input = { patientId: patientId, eventId: eventId },
            isReturning = button == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/CommunicationNote/" + button, input, function (result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.All(result);
            } else if (isReturning) Acore.ReturnReasonFailed();
        });
    },
    Refresh: function (e) {
        if (Acore.Windows[CommunicationNote.ListWindowId].IsOpen) {
            if (!e) {
                e = $("#window_" + CommunicationNote.ListWindowId);
            }
            $(".grid-refresh", e).click();
        }
    },
    GetPatientIdFromList: function (e) {
        if (!e) {
            e = $("#window_" + CommunicationNote.ListWindowId);
        }
        return e.find("[name=PatientId]").val();
    }
}
