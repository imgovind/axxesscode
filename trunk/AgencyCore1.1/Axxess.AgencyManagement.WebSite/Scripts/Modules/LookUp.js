﻿var Lookup = {
    _Users: "",
    _Discipline: new Array(),
    Load: function(u, p, v, n, a) {
        U.PostUrl(u, v, function(data) {
            $(p).each(function() {
                var s = this;
                $(this).children('option').remove();
                if (n != undefined) s[0] = new Option("-- Select " + n + " --", "", false, false);
                if (a) s[1] = new Option("** Add New " + n + " **", "new", false, false);
                $.each(data, function(index, itemData) {
                    s[s.options.length] = new Option(
                            (n == "Admission Source" ? "(" + itemData.Code + ") - " + itemData.Description : "") +
                            (n == "Task" ? itemData.Task : "") +
                            (n == "Physician" || n == "User" ? itemData.DisplayName : "") +
                            (n == "Race" || n == "Referral Source" ? itemData.Description : "") +
                            (n == "Insurance" || n == "State" ? itemData.Name : ""),
                            (n == "State" || n == "Admission Source" ? itemData.Code : itemData.Id), false, false);
                    if (n == "Task") $("option:last", s).attr("data", itemData.Discipline).attr("IsBillable", itemData.IsBillable);
                });
            });
        });
    },
    LoadAdmissionSources: function() { Lookup.Load("LookUp/AdmissionSources", ".admission-source", null, "Admission Source"); },
    LoadDiscipline: function(control, input) { Lookup.Load("/LookUp/DisciplineTasks", control, "Discipline=" + input, "Task"); },
    LoadInsurance: function() { Lookup.Load("/Agency/GetInsurances", ".insurance", null, "Insurance", true); },
    LoadMultipleDisciplines: function(control) { Lookup.Load("/LookUp/MultipleDisciplineTasks", control + " select.MultipleDisciplineTask", null, "Task"); },
    LoadRaces: function() { Lookup.Load("/LookUp/Races", "select.EthnicRaces", null, "Race"); },
    LoadReferralSources: function() { Lookup.Load("/LookUp/ReferralSources", "select.ReferralSources", null, "Referral Source"); },
    LoadStates: function() { Lookup.Load("/LookUp/States", "select.States", null, "State"); },
    LoadUsersDropDown: function(control, input) {
        U.PostUrl("User/BranchList", input, function(data) {
            var s = $(control);
            if (s.hasClass("grouped") && s.attr("updateGroup")) {
                var optGroup = s.find("optgroup[value='" + s.attr("updateGroup") + "']");
                optGroup.children('option').remove();
                $.each(data, function(index, itemData) { optGroup.append(new Option(itemData.Name, itemData.Id, false, false)) });
            } else {
                s.children('option').remove();
                s.get(0).options[0] = new Option("-- Select User --", "00000000-0000-0000-0000-000000000000", false, false);
                $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false) });
            }
        });
    },
    LoadEmployeeDropDown: function(control, input) {
        U.PostUrl("User/BranchList", input, function(data) {
            var s = $(control);
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select User --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false);
            });
        });
    },
    LoadReportPatientDropDown: function(control, input) {
        U.PostUrl("PatientProfile/BranchList", {
            branchId: $("#" + input + "_BranchId").val(),
            statusId: $("#" + input + "_StatusId").val()
        }, function(data) {
            var s = typeof input == "string" ? $("select#" + control) : control;
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Patient --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false);
            });
        });
    },
    SetDiscipline: function(name, discipline) { Lookup._Discipline[name] = discipline; },
    Language: function(selector) { $(selector).Autocomplete({ source: ["Spanish", "Chinese", "French", "German", "Filipino", "Vietnamese", "Italian", "English"] }) },
    LoadPatientsDropDown: function(control, input, prefix, callback) {
        U.PostUrl(prefix + "/PatientProfile/BranchList", input, function(data) {
            var s = $(control)
            s.children('option').remove();
            $(control).empty();
            s.get(0).options[0] = new Option(s.hasClass("all") ? "All" : "-- Select Patient --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false);
            });
            if (typeof callback == "function") callback();
        });
    }
}