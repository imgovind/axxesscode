﻿var Contact = {
    Delete: function(Id) { U.DeleteTemplate("Contact", Id); },
    Edit: function(id) {
        Acore.Open("editcontact", "Contact/Edit", Contact.InitEdit, { Id: id });
    },
    InitEdit: function(r, t, x, e) {
        U.ShowIfSelectEquals($("#EditContact_Type", e), "Other", $("#EditContact_ContactTypeOther", e));
        U.InitTemplate($("form.mainform", e), function() { Contact.RebindList(); }, "Contact has been successfully updated.");
    },
    InitNew: function(r, t, x, e) {
        U.ShowIfSelectEquals($("#NewContact_Type", e), "Other", $("#NewContact_ContactTypeOther", e));
        U.InitTemplate($("form.mainform", e), function() { Contact.RebindList(); }, "Contact has been successfully added.");
    },
    InitList: function(r, t, x, e) {
        $("a.new", e).on("click", function() { Acore.Open("newcontact"); });
        U.ListInitClientRebind(r, t, x, e);
    },
    RebindList: function() { U.RebindTGrid($('#List_Contact')); },
    ListRowDataBound: function(e) {
        U.DataItemToolTip(e);
    }
}