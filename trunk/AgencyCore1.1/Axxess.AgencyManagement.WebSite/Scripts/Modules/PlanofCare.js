﻿var PlanOfCare = {
    Init: function(r, t, x, e) {
        var form = $("form", e);
        if (form.length) {
            form.Validate({ Success: Refresh.All });
            var action = form.attr("action").replace("/PlanOfCare/Save", "");
            $("#Edit_485_RefreshData", e).on("click", function() {
                PlanOfCare.Refresh($("#Edit_485_EpisodeId", e).val(), $("#Edit_485_PatientId", e).val(), $("#Edit_485_Id", e).val(), action);
            });
            PlanOfCare.InitContent(r, t, x, e);
        }
        else {
            $(e).html(U.MessageWarn("No care plan", "There is no care plan found for this event."));
        }
    },
    InitContent: function(r, t, x, e) {
        $("#Edit_485LatestMedications", e).on("click", function() {
            Medication.GetLatest($("#Edit_485_PatientId", e).val(), $("#Edit_485_Medications", e), $(this));
        });
        $("#Edit_485LatestAllergies", e).on("click", function() {
            Allergy.GetCurrent($("#Edit_485_PatientId", e).val(), $('#Edit_485AllergiesDescription', e), $(this));
        });
    },
    Refresh: function(episodeId, patientId, eventId, actionPrefix) {
        $("#planofCareContentId").Load(actionPrefix + '/PlanofCare/Content', { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(r, t, x, e) {
            if (t == 'error') U.Growl('This page could not be refreshed. Please close this window and try again.', 'error');
            PlanOfCare.InitContent(r, t, x, e);
        });
    }
}