﻿var ChangePassword = {
    Init: function() {
        if ($(window).width() < 767) {
            viewport = document.querySelector("meta[name=viewport]");
            viewport.setAttribute('content', 'width=700, initial-scale=0.5, maximum-scale=1.0, user-scalable=0');
        }
        $("#changePasswordForm").validate({
            growlOnError: false,
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        $('.form-wrapper').hide();
                        Logon.LoadingAnimationStart();
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $(form).hide();
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="img icon32 success"></span><span>' + result.errorMessage + '</span>');
                        }
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="img icon32 error"></span><span>' + result.errorMessage + '</span>');
                        }
                        $('.form-wrapper').show();
                        Logon.LoadingAnimationStop();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Logon = {
    Init: function() {
        this.Validate();
        $("#Login_UserName").focus();
        // Mobile Viewport
        if ($(window).width() < 767) {
            viewport = document.querySelector("meta[name=viewport]");
            viewport.setAttribute('content', 'width=700, initial-scale=0.5, maximum-scale=1.0, user-scalable=0');
        }
        // Upcoming Events
        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: 'https://axxessweb.com/seminar/upcomingEvents',
            jsonpCallback: 'mycallback',
            cache: true,
            success: function(data) {
                var $container = $('.upcoming-events').html('<h1>Training Opportunities</h1>');
                var $innerContainer = $("<div/>");
                $.each(data, function (i, object) {
                    var series = '';
                    $.each(object, function (property, value) {
                        series += '<li><a href="http://axxessweb.com/seminar/' + value.series + '/' + value.slug + '" target="_blank">'
		                    	+ value.location + '<span class="date">' + value.date + '</span></a></li>';
                    });
                    $innerContainer.append('<div class="series">'
		                	+ '<h4><a href="http://axxessweb.com/seminar/' + object[0].series + '" target="_blank">' + object[0].title + ' &raquo;</a></h4>'
		                	+ '<ul id="' + object[0].series + '">' + series + '</ul>'
		                	+ '</div>');
                });
                $container.append($innerContainer).append('<div class="btn-wrapper"><a href="http://axxessweb.com/seminar" class="btn-custom">SEE MORE</a></div>');
            }
        });

        // Did You Know?
        var dyk = $('.didyouknow p');
        dyk.eq(Math.floor(Math.random() * (dyk.length))).show();
        window.setInterval(function() {
            var current = $('.didyouknow p:visible');
            // if (current.is(':hover')) return;
            current.animate({ top: '2em', opacity: 0 }, 800, function() { $(this).hide() });
            var next = current.next();
            if (next.length === 0) next = current.siblings('p').first();
            next.css({ top: '-2em', opacity: 0 }).show().animate({ top: 0, opacity: 1 }, 800, function() { $(this).show() });
        }, 7000);
    },
    Validate: function() {
        $("#login-form")
            .on('submit', function(event) { event.preventDefault(); })
            .validate({
                growlOnError: false,
                messages: { UserName: "Please enter your email address.", Password: "Please enter your password." },
                submitHandler: function(form) {
                    var options = {
                        dataType: "json",
                        beforeSubmit: function(values, form, options) {
                            $('.form-wrapper').hide();
                            Logon.LoadingAnimationStart();
                        },
                        success: function(result) {

                            if (result.isSuccessful) {

                                if (result.hasMultiple) {

                                    // Multiple Agencies
                                    $('.referral').hide();
                                    $('.login-agency-selection').load(
                                        "/Account/Agencies",
                                        { loginId: result.id },
                                        function(responseText, textStatus, XMLHttpRequest) {
                                            Logon.LoadingAnimationStop();
                                            $(this).show();
                                            $('.didyouknow').hide();
                                            $('.morechoose').hide();
                                            if (textStatus == "success") return;
                                        }
                                    );

                                } else {
                                    // Complete, Successful Login
                                    Logon.LoadingAnimationStop();
                                    Logon.Agreement(result);
                                }
                            } else {

                                Logon.LoadingAnimationStop();
                            
                                if (result.isAccountInUse) {
                                    // Already logged in elsewhere
                                    Logon.AlreadyLoggedIn(result);
                                } else {
                                    // Failed login
                                    if (result.isLocked) {
                                        Logon.Lock(result);
                                    } else {
                                        $('.form-wrapper').show();
                                        U.Growl(result.errorMessage, "error");

                                    }
                                }
                            }
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            })
    },
    Select: function(agencyId, userId) {
    
        $('.login-agency-selection').hide();
        Logon.LoadingAnimationStart();

        U.PostUrl("/Account/Agency", "agencyId=" + agencyId + "&userId=" + userId, function(result) {

            Logon.LoadingAnimationStop();
        
            if (result.isSuccessful) {
                Logon.Agreement(result);
            } else {
                // U.UnBlock();
                if (result.isAccountInUse) {
                    // Already Logged In Elsewhere
                    Logon.AlreadyLoggedIn(result);
                } else {
                    // Failed login
                    if (result.isLocked) {
                        Logon.Lock(result);
                    } else {
                        $('.login-agency-selection').show();
                        U.Growl(result.errorMessage, "error");
                    }
                }
            }
        });
    },
    Lock: function(result) {
        // Show Error
        Logon.Message(result.errorMessage + '<div class="countdown"></div>');
        // Countdown
        $('.login-message .countdown').data('seconds', 5 * 60).text('5:00');
        Logon.MessageCountdown = window.setInterval(function() {
            var c = $('.login-message .countdown');
            var seconds = c.data('seconds') - 1;
            if (seconds === 0) {
                window.clearInterval(Logon.MessageCountdown);
                $('.login-message').hide();
                $('.form-wrapper').show();
            } else {
                c.data('seconds', seconds);
                var minutes = Math.floor(seconds / 60);
                var seconds = seconds - (minutes * 60);
                if (seconds < 10) { seconds = "0" + seconds; }
                c.text(minutes + ':' + seconds);
            }
        }, 1000);
    },
    AlreadyLoggedIn: function(result) {
        Logon.Message(
            '<h3>User Already Logged In</h3>' +
            '<p>This user is already logged in on another computer. If you choose to proceed, the user will automatically be logged off the other computer and their work will not be saved. Are you sure you want to continue?</p>',
            function() {
                $('.login-message').hide();
                $('.form-wrapper').show();
            },
            function() {
                U.PostUrl("/Account/Kick", "loginId=" + result.id + "&emailAddress=" + result.email, function(result) {
                    if (result.isSuccessful) {
                        $('.login-message').hide();
                        Logon.LoadingAnimationStart();
                        window.location.href = result.redirectUrl;
                    }
                    else { U.Growl(result.errorMessage, "error"); }
                });
            }
        );
    },
    Agreement: function(result) {
        Logon.Message(
            '<h3>Login Successful</h3>' +
            '<p>This system and all its components and contents (collectively, the "System") are intended for authorized business use only. All data within is considered confidential and proprietary. Unauthorized access, use, modification, destruction, disclosure or copy of this system is prohibited and will result in prosecution. Click OK to continue.</p>',
            function() { window.location.href = '/logout'; },
            function() {
                $('.login-message').hide()
                Logon.LoadingAnimationStart();
                window.location.href = result.redirectUrl;
            }
        );
    },
    // Login Message
    MessageCountdown: null,
    Message: function(message, cancel, ok) {
        var loginMessage = $('.login-message');
        loginMessage.find('.message').html(message);
        if (cancel === undefined)
            loginMessage.find('.btn-cancel').hide();
        else
            loginMessage.find('.btn-cancel').off('click').click(cancel).show();
        if (ok === undefined)
            loginMessage.find('.btn-ok').hide();
        else
            loginMessage.find('.btn-ok').off('click').click(ok).show();
        loginMessage.show();
    },
    // Loading Animation
    LoadingStep: 0,
    LoadingInterval: null,
    LoadingAnimationStart: function() {
        $('.login-loading').fadeIn();
        Logon.LoadingStep = 0;
        Logon.LoadingInterval = window.setInterval(Logon.LoadingAnimationStep, 200);
    },
    LoadingAnimationStop: function() {
        window.clearInterval(Logon.LoadingInterval);
        $('.login-loading').stop(true, true).hide();
    },
    LoadingAnimationStep: function() {
        var dots = $('.login-loading i');
        dots.eq(Logon.LoadingStep).animate({ 'font-size': '2em' }, 300);
        dots.eq((Logon.LoadingStep - 1) % 10).animate({ 'font-size': '1em' }, 300);
        Logon.LoadingStep = (Logon.LoadingStep + 1) % 10;
    }
}
var ResetPassword = {
    Init: function() {
        if ($(window).width() < 767) {
            viewport = document.querySelector("meta[name=viewport]");
            viewport.setAttribute('content', 'width=700, initial-scale=0.5, maximum-scale=1.0, user-scalable=0');
        }
        $("#forgotPasswordForm").validate({
            growlOnError: false,
            messages: { 
                EmailAddress: {
                    required: "* Required", email: "The Email Address is invalid."
                }
            },
            rules: {
                EmailAddress: { required: true, email: true }
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        $('.form-wrapper').hide();
                        Logon.LoadingAnimationStart();
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $(form).hide();
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="img icon32 success"></span><span>' + result.errorMessage + '</span>');
                        } else $("#messages").empty().removeClass().addClass("notification error").append('<span class="img icon32 error"></span><span>' + result.errorMessage + '</span>');
                        $('.form-wrapper').show();
                        Logon.LoadingAnimationStop();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Activate = {
    Init: function() {
        if ($(window).width() < 767) {
            viewport = document.querySelector("meta[name=viewport]");
            viewport.setAttribute('content', 'width=700, initial-scale=0.5, maximum-scale=1.0, user-scalable=0');
        }
        $("#activateAccountForm").validate({
            growlOnError: false,
            rules: {
                Password: { required: true, minlength: 8 }
            },
            messages: {
                Password: { required: "* Required", minlength: "8 characters minimum" }
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        $('.form-wrapper').hide();
                        Logon.LoadingAnimationStart();
                    },
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $('.form-wrapper').show();
                            Logon.LoadingAnimationStop();
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="img icon32 error"></span><span>' + result.errorMessage + '</span>');
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}