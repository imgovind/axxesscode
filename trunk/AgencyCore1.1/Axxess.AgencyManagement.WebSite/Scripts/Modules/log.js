﻿var Log = {
    LoadEpisodeLog: function(episodeId, patientId) { Acore.Open("episodelogs", 'Schedule/EpisodeLogs', function() { }, { episodeId: episodeId, patientId: patientId }); },
    LoadPatientLog: function(patientId) { Acore.Open("patientlogs", { patientId: patientId }); },
    LoadMedicationLog: function(patientId) { Acore.Open("medicationlogs", { patientId: patientId }); },
    LoadPhysicianLog: function(physicianId) { Acore.Open("physicianlogs", { physicianId: physicianId }); },
    LoadUserLog: function(userId) { Acore.Open("userlogs", { userId: userId }); },
    LoadReferralLog: function(referralId) { Acore.Open("referrallogs", { referralId: referralId }); },
    LoadContactLog: function(contactId) { Acore.Open("contactlogs", { contactId: contactId }); },
    LoadInsuranceLog: function(insuranceId) { Acore.Open("insurancelogs", { insuranceId: insuranceId }); },
    LoadLocationLog: function(locationId) { Acore.Open("locationlogs", { locationId: locationId }); },
    LoadHospitalLog: function(hospitalId) { Acore.Open("hospitallogs", { hospitalId: hospitalId }); },
    LoadTemplateLog: function(templateId) { Acore.Open("templatelogs", { templateId: templateId }); },
    LoadSupplyLog: function(supplyId) { Acore.Open("supplylogs", { supplyId: supplyId }); },
    LoadClaimLog: function(type, claimId, patientId) { Acore.Open("claimlogs", { type: type, claimId: claimId, patientId: patientId }); },
    LoadAdjustmentCodeLog: function(adjustmentCodeId) { Acore.Open("adjustmentcodelogs", { adjustmentCodeId: adjustmentCodeId }); },
    LoadUploadTypeLog: function(uploadTypeId) { Acore.Open("uploadtypelogs", { uploadtypeId: uploadTypeId }); }
}