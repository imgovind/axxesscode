var Medication = {
    assessmentType: "",
    Activate: function(medProfileId, medicationId, assessmentType) {
        Acore.Confirm({
            Message: "Are you sure you want to activate this medication?",
            Yes: function() {
                U.PostUrl('Patient/UpdateMedicationStatus', { medProfileId: medProfileId, medicationId: medicationId, medicationCategory: "Active", dischargeDate: "01/01/0001" }, function(result) {
                    if (result.isSuccessful) {
                        U.Growl(result.errorMessage, "success");
                        Medication.Refresh(medProfileId, assessmentType);
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        });
    },
    Add: function(medProfileId, assessmentType) {
        Acore.Modal({
            "Name": "Add New Medication",
            "Url": "Patient/NewMedication",
            "Input": { medProfileId: medProfileId },
            "OnLoad": function(r, t, x, e) { Medication.InitNew(assessmentType, e) },
            "Width": "650px",
            "Height": "390px",
            "WindowFrame": false
        })
    },
    Delete: function(medProfileId, medicationId, assessmentType) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this medication?",
            Yes: function() {
                U.PostUrl("Patient/DeletePatientMedication", { medProfileId: medProfileId, medicationId: medicationId }, function(result) {
                    if (result.isSuccessful) {
                        U.Growl(result.errorMessage, "success");
                        Medication.Refresh(medProfileId);
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        });
    },
    Discontinue: function(medProfileId, medicationId, assessmentType) {
        Acore.Modal({
            "Name": "Discharge Medication",
            "Url": "Patient/DischargeMedication",
            "Input": { medProfileId: medProfileId, medicationId: medicationId },
            "OnLoad": function(r, t, x, e) { Medication.InitDischarge(assessmentType, e) },
            "Width": "650px",
            "WindowFrame": false
        })
    },
    Edit: function(medProfileId, medicationId, assessmentType) {
        Acore.Modal({
            "Name": "Edit Medication",
            "Url": "Patient/EditMedication",
            "Input": { medProfileId: medProfileId, medicationId: medicationId },
            "OnLoad": function(r, t, x, e) { Medication.InitEdit(assessmentType, e) },
            "Width": "650px",
            "Height": "390px",
            "WindowFrame": false
        });
    },
    InitAutocomplete: function(e) {
        $("input[name=MedicationDosage]", e).AjaxAutocomplete({
            minLength: 2,
            SourceUrl: "LookUp/Drugs",
            Format: function(json) {
                return json.Name
            },
            MagnifyingGlass: true,
            Select: function(json, input) {
                input.val(json.Name).next().val(json.Name).closest("form");
                $('#New_Medication_DrugId', e).val(json.LexiDrugId);
                U.PostTrackedUrl("LookUp/DrugClassifications", { genericDrugId: json.LexiDrugId }, function(result) {
                    if (result != undefined) {
                        if (result.length == 1) {
                            $("#New_Medication_Classification", e).val(result[0]);
                        } else if (result.length > 1) {
                            $("#New_Medication_Classification", e).Autocomplete({
                                minLength: 0,
                                source: result
                            }).autocomplete("search", "");
                        }
                    }
                });
            }
        });
        $("input[name=Route]", e).AjaxAutocomplete({
            minLength: 1,
            SourceUrl: "LookUp/MedicationRoute",
            MagnifyingGlass: true,
            Format: function(json) {
                return json.Id + " &#8211; " + json.ShortName + " " + json.LongName
            },
            Select: function(json, input) {
                input.val(json.LongName + " (" + json.ShortName + ")").next().val(json.LongName + " (" + json.ShortName + ")")
            }
        });
        $("input[name=Frequency]", e).Autocomplete({
            source: [
                "Before Meals",
                "After meals",
                "Daily",
                "Every Day",
                "Twice/day",
                "Three times/day",
                "Four times /day",
                "Every other day",
                "PRN",
                "Bedtime"
            ]
        });
    },
    InitDischarge: function(assessmentType, e) {
        $("#dischargeMedicationProfileForm", e).Validate({ Success: function() { Medication.Refresh($("input[name='medProfileId']", e).val()); } });
        if (assessmentType != undefined) {
            this.assessmentType = assessmentType;
            $("#Discharge_Medication_AssessmentType", e).val(assessmentType);
        }
    },
    InitEdit: function(assessmentType, e) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        U.HideIfChecked($("#Edit_Medication_IsLongStanding", e), $("#Edit_Medication_StartDateRow", e));
        this.InitAutocomplete(e);
        $("#editMedicationForm", e).Validate({ Success: function() { Medication.Refresh($("#Edit_Medication_ProfileId", e).val(), assessmentType); } });
    },
    InitNew: function(assessmentType, e) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        U.HideIfChecked($("#New_Medication_IsLongStanding", e), $("#New_Medication_StartDateRow", e));
        this.InitAutocomplete(e);
        $("#newMedicationForm", e).Validate({ Success: function() { Medication.Refresh($("#New_Medication_ProfileId", e).val(), assessmentType); } });
    },
    Refresh: function(medicationProfileId, prefix) {
        if (prefix == undefined && this.assessmentType.length) prefix = this.assessmentType;
        else if (prefix == undefined) prefix = "MedProfile";
        $("#" + prefix + "_activeMeds ol").addClass("loading");
        $("#" + prefix + "_dischargeMeds ol").addClass("loading");
        $("#" + prefix + "_medications").load(
            (prefix == "MedProfile" ? "Patient/Medications" : "Patient/MedicationsForOasis"),
            (prefix == "MedProfile" ? { "medicationProfileId": medicationProfileId} : { "medicationProfileId": medicationProfileId, "assessmentType": prefix }),
            function(responseText, textStatus, xmlHttpRequest) {
                Acore.OnLoad($(this));
                $('#' + prefix + '_activeMeds ol').removeClass("loading");
                $('#' + prefix + '_dischargeMeds ol').removeClass("loading");
                if (textStatus == 'error') $('#' + prefix + '_medications').html(U.AjaxError);
            }
        );
    },
    InitDrugDrugInteractions: function(e, prefix) {
        var form = $("#drugDrugInteractionForm", e),
            action = form.attr("action");
        if (prefix && action.indexOf("HomeHealth") > -1) action.replace("HomeHealth", prefix);
        form.attr("action", action);
        form.ValidateForFile({
            BeforeDownload: function() {
                if ($("input[name=drugsSelected]:checked", e).length > 1) return true;
                else { U.Growl("Select at least two medications to screen.", "error"); return false; }
            }
        });
    },
    DrugDrugInteractions: function(medProfileId, prefix) {
        Acore.Modal({
            "Name": "Drug-Drug Interactions",
            "Url": "Patient/DrugDrugInteractions",
            "Input": { medicationProfileId: medProfileId },
            "OnLoad": function(r, t, x, e) { Medication.InitDrugDrugInteractions(e, prefix); },
            "Width": "750px",
            "Height": "350px",
            "WindowFrame": false
        });
    },
    OnComplete: function(result) {
        if (result.name == "update" || result.name == "delete") {
            U.Growl("The Signed Medication Profile has been successfully " + result.name + "d.", "success");
        }
    },
    Profile: function(patientId, actionPrefix) {
        Acore.Open("medicationprofile", (actionPrefix != "HomeHealth" ? actionPrefix : '') + '/MedicationProfile', function() { }, { patientId: patientId });
    },
    Snapshot: function(patientId, assessmentType, actionPrefix) {
        Acore.Open("medicationprofilesnapshot", (actionPrefix ? actionPrefix : '') + '/PatientProfile/MedicationProfileSnapShot', function() { Medication.InitSnapshot(assessmentType); }, { patientId: patientId });
    },
    InitSnapshot: function(assessmentType) {
        U.PhoneAutoTab("MedProfile_PharmacyPhone");
        $("#newMedicationProfileSnapShotForm").Validate({ BeforeSubmit:
            function(values, form, options) {
                if (assessmentType != undefined) {
                    var assessmentId = $(assessmentType + "_Id").val();
                    if (assessmentId != undefined && assessmentId != "") { values.push({ name: "AssociatedAssessment", value: assessmentId }); }
                }
            }
        });
    },
    GetLatest: function(patientId, $content, $button) {
        $button.trigger("ProcessingStart");
        U.PostTrackedUrl("Patient/LastestMedications", { patientId: patientId }, function(result) {
            $content.val(result).text(result);
            $button.trigger("ProcessingComplete");
        }, function() {
            $button.trigger("ProcessingComplete");
            U.Growl("A problem occured while attempting to get the latest medications.", "error");
        });
    },
    SnapshotHistory: function(patientId) {
        Acore.Open("medicationprofilesnapshothistory", '/Patient/MedicationProfileSnapShotHistory', function() { }, { patientId: patientId });
    }
}
