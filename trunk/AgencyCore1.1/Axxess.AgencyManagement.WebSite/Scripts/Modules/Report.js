﻿var Report = {
    AcoreId: "ReportCenter",
    Report: "",
    Service: "",
    Delete: function (completedReportId) {
        Acore.Confirm({
            Name: "Delete Report",
            Message: "Are you sure you want to delete this Report?",
            Yes: function () {
                U.PostUrl("Request/DeleteReport", { completedReportId: completedReportId }, function (result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) Report.RebindCompletedList();
                })
            }
        })
    },
    Export: function (report, service, e) {
        if (Report.Valid(e)) {
            U.GetAttachment(service + "/Report/" + report, Report.GetInput(e));
        }
    },
    Request: function (request, service, button, e) {
        if (Report.Valid(e)) {
            button.trigger("ProcessingStart");
            var input = {};
            if (window[service].ApplicationService == HomeHealth.ApplicationService) service = "";
            $(".report-controls", e).find(":input:not(.form-submitted)").each(function () {
                input[$(this).attr("name")] = $(this).val();
            });
            U.PostUrl(window[service].Area + "/Request/" + request, input, function () {
                button.trigger("ProcessingComplete");
                U.Growl("The request has been sent to the server. It can be found in the Completed Reports, under the Reports menu, when it has completed.", "success");
            }, function () {
                button.trigger("ProcessingComplete");
                U.Growl("The request failed to be sent to the server. Please try again.", "error");
            })
        }
    },
    Init: function (r, t, x, e) {
        $("a.report-link", e).on("click", function () {
            var serviceValue = $("[name=ServiceId]", e).val()
            Report.Show($(this).attr("href"), Acore.AppService(serviceValue), e, this);
            return false;
        });
        var serviceValue = $("[name=ServiceId]", e).val();
        Report.InitContent(serviceValue, e);
        $("[name=ServiceId]", e).on("change", function () {
            Report.InitContent($(this).val(), e);
        });
        if (Report.Report.length && Report.Service.length) {
            Report.Show(Report.Report, Report.Service, e);
            Report.Report = Report.Service = "";
        }
    },
    InitContent: function (serviceValue, e) {
        $(".reports ul li", e).each(function () {
            var value = $(this).attr("view");
            if (+value > 0) {
                if ((value & serviceValue) == serviceValue) $(this).show();
                else $(this).hide();
            } else $(this).hide();
            $("a.report-link, a.onclick-link", this).each(function () {
                Report.ToolTip($(this));
            })
        });
        $(".reports", e).show().each(function () {
            if ($("ul li:visible", this).length > 0) $(this).show();
            else $(this).hide();
        })
    },
    InitEmployeeByStatus: function (e) {
        $("[name=StatusId]", e).on("change", function () {
            Lookup.LoadEmployeeDropDown($(this).closest("fieldset").find("[name=UserId]"), {
                branchId: $("[name=BranchId]", e).val(),
                statusId: $(this).val(),
                service: $("[name=Service]", e).val()
            });
        })
    },
    InitPatientByStatus: function (e) {
        var report = $("[name=PageName]", e).val();
        $("[name=StatusId]", e).on("change", function() {
            Lookup.LoadReportPatientDropDown(report + "_PatientId", report);
        })
    },
    InitReport: {
        EmployeePatients: function (service, e) {
            Report.InitEmployeeByStatus(e);
        },
        CaseManagerPatients: function (service, e) {
            Report.InitEmployeeByStatus(e);
        },
        EmployeeWeekly: function (service, e) {
            Report.InitEmployeeByStatus(e);
        },
        EmployeeMonthly: function (service, e) {
            Report.InitEmployeeByStatus(e);
        },
        EmployeeVisits: function (service, e) {
            Report.InitEmployeeByStatus(e);
        },
        EmployeeLicense: function (service, e) {
            Report.InitEmployeeByStatus(e);
        },
        VitalSigns: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        SixtyDaySummaries: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        ThirteenAndNineteenVisitException: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        ThirteenTherapyReevaluationException: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        NineteenTherapyReevaluationException: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        PatientWeekly: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        PatientMonthly: function (service, e) {
            Report.InitPatientByStatus(e);
        },
        PatientVisits: function (service, e) {
            Report.InitPatientByStatus(e);
        }
    },
    Home: function (e) {
        e.addClass("transistion").find(".wrapper.main").Animate({ left: "100%" }, function () {
            $(".wrapper.main.blue", e).removeClass("blue");
            $(".report-output").empty();
            $(".report-home").show();
            e.Rename("Report Center").Status("");
            $(".wrapper.main", e).css("left", "-100%").Animate({ left: 0 }, function () {
                e.removeClass("transistion");
            })
        })
    },
    ToolTip: function (Element) {
        $(Element).Tooltip({
            top: 10,
            left: 10,
            extraClass: "report-description",
            bodyHandler: function () {
                return $(".tooltip", Element).html();
            }
        })
    },
    Open: function (report, service) {
        report = report == undefined ? "" : report;
        service = service == undefined ? "" : service;
        if (Acore.Windows[Report.AcoreId].IsOpen) {
            var content = $("#window_" + Report.AcoreId + "_content");
            var reportElement = content.find(".reports a.report-link[href='" + report + "']");
            if (reportElement) Report.Show(report, service, content, reportElement);
        } else {
            Report.Report = report;
            Report.Service = service;
            Acore.Open(Report.AcoreId);
        }
    },
    RebindCompletedList: function () {
        U.RebindTGrid($('#List_CompletedReports'));
    },
    GetInput: function (e) {
        return U.JsonInput($(".report-controls", e).find(":input").not(".form-submitted"));
    },
    Refresh: function (report, service, e) {
        if (Report.Valid(e)) {
            $(".report-grid", e).Load(service + "/Report/" + report, Report.GetInput(e), null, true, true);
        }
    },
    InitSort: function (service, e) {
        var gridContainer = $(".report-grid", e),
            gridId = $(".t-grid", gridContainer).attr("id");
        gridContainer.on("click", ".t-grid-header .t-link", function(evnt) {
            if ($(this).attr("href")) {
                var link = $(this).attr("href");
                var sortOrder = U.ParameterByName(link, gridId + "-orderBy");
                console.log(sortOrder);
                $("[name=SortParams]", e).val(sortOrder);
                $(".grid-refresh", e).click();
                evnt.stopPropagation();
                return false;
            }
        });
    },
    Show: function (report, service, e, reportElement) {
        e.addClass("transistion").find(".wrapper.main").Animate({ left: "-100%" }, function () {
            $(".report-home", e).hide();
            $(".report-output", e).Load(window[service].Area + "/Report/" + report, function (r, t, x, e) {
                if ($(".t-grid", e).length) e.closest(".wrapper.main").addClass("blue");
                $(".report-home", e).on("click", function () { Report.Home(e.closest(".window-content")) });
                $(".grid-refresh", e).on("click", function () { Report.Refresh($(this).attr("report"), service, e) });
                $(".export", e).on("click", function () { Report.Export($(this).attr("report"), service, e) });
                $(".request", e).on("click", function () { Report.Request($(this).attr("request"), service, $(this), e) });
                Report.InitSort(service, e);
                if (typeof Report.InitReport[report] == "function") Report.InitReport[report](service, e);
                var toolTip = $(".report-controls div.report-help", e);
                if (toolTip.length > 0) Report.ToolTip(toolTip);
                else if (reportElement) {
                    var toolTipElement = $("<div/>").addClass("button report-help").append(
                        $("<a/>").text("?")).append(
                        $(reportElement).find("span.tooltip").clone(true));
                    $(".report-controls ", e).prepend($(toolTipElement));
                    Report.ToolTip($(".report-help", $(".report-controls", e)));
                }
            });
            $(".wrapper.main", e).css("left", "100%").Animate({ left: 0 }, function () {
                e.removeClass("transistion");
            })
        })
    },
    Valid: function (e) {
        var valid = true;
        $(".report-controls .required", e).each(function () {
            var isValid;
            if ($(this).hasClass("not-zero")) isValid = $.validator.methods["not-zero"]($(this).val());
            else isValid = $(this).val() != "";
            if (isValid) $(this).removeClass("error");
            else {
                valid = isValid;
                $(this).addClass("error");
            }
        });
        if (!valid) U.Growl("Please fill out all required fields.", "error");
        return valid;
    }
}