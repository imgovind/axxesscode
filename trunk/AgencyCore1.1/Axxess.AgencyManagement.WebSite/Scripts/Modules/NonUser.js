﻿var NonUser = {
    New: function() {
        Acore.Open("NewNonUser", NonUser.InitNew);
    },
    Edit: function(id) {
        Acore.Open("EditNonUser", { id: id });
    },
    InitNew: function() {
        var form = $("#newNonUserForm");
        if (form.length) {
            $(".names", form).alpha({ nocaps: false });
            U.InitValidation(form, NonUser.RebindList);
        }
    },
    InitEdit: function(r, t, x, e) {
        var form = $("#editNonUserForm", e);
        if (form.length) {
            $(".names", form).alpha({ nocaps: false });
            U.InitValidation(form, function() { NonUser.RebindList(); LicenseManager.Refresh(); });
        }
    },
    Delete: function(id, userName) {
        userName = userName ? userName : "this non-software user";
        U.Delete(userName, "/NonUser/Delete", { id: id }, function() { NonUser.RebindList(); LicenseManager.Refresh(); }, false, true);
    },
    RebindList: function() {
        $("#window_listnonusers .grid-refresh").click();
    }
}
