﻿var Allergy = {
    assessmentType: "",
    New: function(allergyProfileId, assessmentType) {
        Acore.Modal({
            Name: "Add New Allergy",
            Url: "AllergyProfile/NewAllergy",
            Input: { allergyProfileId: allergyProfileId },
            OnLoad: function() { Allergy.InitNew(assessmentType); },
            Width: "650px"
        });
    },
    Delete: function(allergyProfileId, allergyId, assessmentType) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this allergy?",
            Yes: function() {
                U.PostUrl("AllergyProfile/UpdateAllergyStatus", { allergyProfileId: allergyProfileId, allergyId: allergyId, isDeprecated: true }, function(result) {
                    if (result.isSuccessful) Allergy.Refresh(allergyProfileId, assessmentType);
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                })
            }
        })
    },
    Edit: function(allergyProfileId, allergyId, assessmentType) {
        Acore.Modal({
            Name: "Add New Allergy",
            Url: "AllergyProfile/EditAllergy",
            Input: { allergyProfileId: allergyProfileId, allergyId: allergyId },
            OnLoad: function() { Allergy.InitEdit(assessmentType); },
            Width: "650px"
        });
    },
    InitAutocomplete: function() {
        $("input[name=Type]").Autocomplete({
            source: [
                "Medication",
                "Food",
                "Animals",
                "Plants",
                "Latex",
                "Environmental"
            ]
        });
    },
    InitNew: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        this.InitAutocomplete();
        $("#newAllergyForm").Validate({
            Success: function() {
                var allergyProfileId = $("#New_Allergy_ProfileId").val();
                Allergy.Refresh(allergyProfileId, assessmentType);
            }
        });
    },
    InitEdit: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        this.InitAutocomplete();
        $("#editAllergyForm").Validate({
            Success: function() {
                var allergyProfileId = $("#Edit_Allergy_ProfileId").val();
                Allergy.Refresh(allergyProfileId, assessmentType);
            }
        });
    },
    Refresh: function(allergyProfileId, prefix) {
        if (prefix == undefined) prefix = "AllergyProfile";
        var activeAllergyCount = $("#" + prefix + "_AllergyProfileList .allergy-profile-active .allergy").length;
        var inactiveAllergyCount = $("#" + prefix + "_AllergyProfileList .allergy-profile-inactive .allergy").length;
        $("#" + prefix + "_active ol").addClass("loading");
        $("#" + prefix + "_inactive ol").addClass("loading");
        $("#" + prefix + "_AllergyProfileList").load("AllergyProfile/List", { "allergyProfileId": allergyProfileId, prefix: prefix },
            function(r, t) {
                Acore.OnLoad($(this));
                $('#' + prefix + '_active ol').removeClass("loading");
                $('#' + prefix + '_inactive ol').removeClass("loading");
                if (t == 'error') $('#' + prefix + '_list').html(U.AjaxError);
                else {

                    var refreshedActiveAllergyCount = $("#" + prefix + "_AllergyProfileList .allergy-profile-active .allergy").length;
                    var refreshedInactiveAllergyCount = $("#" + prefix + "_AllergyProfileList .allergy-profile-inactive .allergy").length;
                    if (activeAllergyCount !== refreshedActiveAllergyCount || inactiveAllergyCount !== refreshedInactiveAllergyCount) {
                        var patientId = $("#" + prefix + "_PatientId").val();
                        if (!patientId) patientId = $("#" + prefix + "_PatientGuid").val();
                        var textFields = $(".allergy-text");
                        if (textFields.length) {
                            U.PostUrl("/AllergyProfile/LastestAllergies", { patientId: patientId }, function(data) {
                                textFields.each(function() {
                                    if ($(this).closest(".window-content").find("#PatientId").val() == patientId) {
                                        if ($(this).is(":input")) $(this).val(data);
                                        else $(this).text(data);
                                    }
                                });
                            });
                        }
                    }
                }
            }
        );
    },
    Restore: function(allergyProfileId, allergyId, assessmentType) {
        Acore.Confirm({
            Message: "Are you sure you want to restore this allergy?",
            Yes: function() {
                U.PostUrl("AllergyProfile/UpdateAllergyStatus", { allergyProfileId: allergyProfileId, allergyId: allergyId, isDeprecated: false }, function(result) {
                    if (result.isSuccessful) Allergy.Refresh(allergyProfileId, assessmentType);
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                })
            }
        })
    },
    List: function(patientId, serviceName) {
        var area = "";
        if (serviceName) {
            area = window[serviceName].Area;
        }
        Acore.Open("allergyprofile", area + '/AllergyProfile', function() { }, { patientId: patientId });
    },
    GetCurrent: function(patientId, locator, button) {
        button = button ? $(button) : $(locator);
        button.trigger("ProcessingStart");
        U.PostTrackedUrl("/AllergyProfile/LastestAllergies", { patientId: patientId }, function(data) {
            $(locator).val(data).text(data);
            button.trigger("ProcessingComplete");
        }, function() {
            button.trigger("ProcessingComplete");
            U.Growl("A problem occured while attempting to get the latest medications.", "error");
        });
    }
}