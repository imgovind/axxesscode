﻿var Physician = {
    New: function(isModal) {
        if (typeof isModal == "object") isModal = false;
        if (isModal) Acore.Modal({
            Name: "New Physician",
            Url: "Physician/New",
            OnLoad: function(r, t, x, e) { Physician.InitNew(r, t, x, e, isModal) },
            Width: 900
        });
        else Acore.Open("newphysician");
    },
    Edit: function(Id) {
        Acore.Open("editphysician", 'Physician/Edit', Physician.InitEdit, { Id: Id });
    },
    Delete: function(Id) { U.DeleteTemplate("Physician", Id, Physician.List.Refresh, undefined, true); },
    InitEdit: function(r, t, x, e) {
        $('input[name=PhysicianAccess]', e).on("change", function() {
            if ($(this).is(':checked')) $(".email", e).addClass("required");
            else $(".email", e).removeClass("required");
        });
        e.find("form").Validate({ Success: function() { Physician.List.Refresh(); } });
    },
    InitNew: function(r, t, x, e, isModal) {
        var prefix = "NewPhysician_" + (isModal ? "Modal_" : "");
        if (isModal) {
            $("#window_Dialog_content label").each(function() {
                var forValue = $(this).attr("for");
                forValue = forValue ? forValue.replace("NewPhysician_", prefix) : "";
                $(this).attr("for", forValue);
            });
            $("#window_Dialog_content :input").each(function() {
                var idValue = $(this).attr("id");
                idValue = idValue ? idValue.replace("NewPhysician_", prefix) : "";
                $(this).attr("id", idValue);
            });
            $("#NewPhysician_PecosCheck").attr("id", prefix + "PecosCheck");
        }
        $("#" + prefix + "NpiNumber").blur(function() {
            var npiNumber = $("#" + prefix + "NpiNumber").val();
            if (npiNumber.length > 0) {
                U.PostUrl("LookUp/CheckPecos", "npi=" + npiNumber, function(data) {
                    if (data.isSuccessful) $("#" + prefix + "PecosCheck").html("<span class=\"img icon16 check\"></span> Pecos Verified")
                    else $("#" + prefix + "PecosCheck").html("<span class=\"img icon16 ecks\"></span> Not Pecos Verified")
                })
            }
            else $(prefix + "PecosCheck").hide();
        });
        $('input[name=PhysicianAccess]', e).on("change", function() {
            if ($(this).is(':checked')) $(".email", e).addClass("required");
            else $(".email", e).removeClass("required");
        });
        $("#" + prefix + "NpiSearch").AjaxAutocomplete({
            minLength: 1,
            SourceUrl: "LookUp/Npis",
            Format: function(jsonResult) {
                return jsonResult.Id + " &#8211 " + jsonResult.ProviderFirstName + "  " + jsonResult.ProviderLastName
            },
            Select: function(physicianJson, input) {
                input.val(physicianJson.Id);
                if (id = physicianJson.Id) $("#" + prefix + "NpiNumber").val(id).blur();
                if (fname = physicianJson.ProviderFirstName) $("#" + prefix + "FirstName").val(U.ToTitleCase(fname));
                if (mname = physicianJson.ProviderMiddleName) $("#" + prefix + "MiddleIntial").val(mname.substring(0, 1).toUpperCase());
                if (lname = physicianJson.ProviderLastName) $("#" + prefix + "LastName").val(U.ToTitleCase(lname));
                if (cred = physicianJson.ProviderCredentialText) $("#" + prefix + "Credentials").val(cred);
                if (add1 = physicianJson.ProviderFirstLineBusinessPracticeLocationAddress) $("#" + prefix + "AddressLine1").val(U.ToTitleCase(add1));
                if (add2 = physicianJson.ProviderSecondLineBusinessPracticeLocationAddress) $("#" + prefix + "AddressLine2").val(U.ToTitleCase(add2));
                if (city = physicianJson.ProviderBusinessPracticeLocationAddressCityName) $("#" + prefix + "AddressCity").val(U.ToTitleCase(city));
                if (state = physicianJson.ProviderBusinessPracticeLocationAddressStateName) $("#" + prefix + "AddressStateCode").val(state);
                if (zip = physicianJson.ProviderBusinessPracticeLocationAddressPostalCode) $("#" + prefix + "AddressZipCode").val(zip.substring(0, 5));
                if (phone = physicianJson.ProviderBusinessPracticeLocationAddressTelephoneNumber) {
                    $("#" + prefix + "Phone1").val(phone.substring(0, 3));
                    $("#" + prefix + "Phone2").val(phone.substring(3, 6));
                    $("#" + prefix + "Phone3").val(phone.substring(6, 10))
                }
                if (fax = physicianJson.ProviderBusinessPracticeLocationAddressFaxNumber) {
                    $("#" + prefix + "Fax1").val(fax.substring(0, 3));
                    $("#" + prefix + "Fax2").val(fax.substring(3, 6));
                    $("#" + prefix + "Fax3").val(fax.substring(6, 10))
                }
            }
        });
        $("form", e).Validate({
            Success: function() {
                Physician.List.Refresh()
            }
        })
    },
    License: {
        New: function(physicianId) {
            Acore.Modal({
                "Name": "Add License",
                "Url": "Physician/NewLicense",
                "Input": { physicianId: physicianId },
                "OnLoad": Physician.License.Init,
                "Width": "590px",
                "WindowFrame": false
            });
        },
        Edit: function(licenseId, physicianId) {
            Acore.Modal({
                "Name": "Edit License",
                "Url": "Physician/EditLicense",
                "Input": { licenseId: licenseId, physicianId: physicianId },
                "OnLoad": Physician.License.Init,
                "Width": "650px",
                "WindowFrame": false
            });
        },

        Init: function(r, t, x, e) {
//             BeforeSubmit: function(arr, $form) {
//                    var issueDate = new Date($("[name=InitiationDate]", $form).val()).getTime();
//                    var expireDate = new Date($("[name=ExpirationDate]", $form).val()).getTime();
//                    if (issueDate && expireDate && expireDate < issueDate) {
//                        
//                    }
//                    return (issueDate && expireDate) && (expireDate > issueDate);
//                },
            $("form", e).Validate({ Success: Physician.License.RefreshList });
            $("[name=ExpirationDate]", e).rules("add", { greaterThan: ["[name=InitiationDate]", "Issue Date"] });
        },
        Delete: function(licenseId, physicianId) {
            U.Delete("Physician License", "/Physician/DeleteLicense", { Id: licenseId, physicianId: physicianId }, Physician.License.RefreshList);
        },
        RefreshList: function() { U.RebindTGrid($("#List_Physician_Licenses")); }
    },
    List: {
        WindowId: "listphysicians",
        Init: function(r, t, x, e) {
            $(".new", e).click(Physician.New);
            U.ListInit(r, t, x, e);
            //Physician.List.InitContent(r, t, x, e);
        },
        Refresh: function() {
            if (Acore.Windows[Physician.List.WindowId].IsOpen) {
                $("#window_" + Physician.List.WindowId).find("a.grid-refresh").click();
            }
        }
    }
};