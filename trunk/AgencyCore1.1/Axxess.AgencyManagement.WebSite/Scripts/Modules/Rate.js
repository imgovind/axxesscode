﻿var Rate = {
    InitCommon: function(r, t, x, e) {
        var chargeType = $("select[name=ChargeType]", e);
        if (chargeType) {
            chargeType.on("change", function() {
                var chargeTypevalue = $(this).val();
                if (chargeTypevalue == 3 || chargeTypevalue == 2)
                { $(".chargetypetimelimitcontent", e).show(); }
                else { $(".chargetypetimelimitcontent", e).hide(); }
                if (chargeTypevalue == 1) {
                    $(".pervisitunitcontent", e).show();
                    $("input[name=Unit]", e).addClass("required");
                    var hmoContent = $(".medicarehmoratecontent", e);
                    if (hmoContent) { hmoContent.hide().find("input[name=MedicareHMORate]").removeClass("required"); }
                } else {
                    $(".pervisitunitcontent", e).hide();
                    $("input[name=Unit]", e).removeClass("required");
                    if (chargeTypevalue != 0) {
                        var hmoContent = $(".medicarehmoratecontent", e);
                        if (hmoContent) { hmoContent.show().find("input[name=MedicareHMORate]").addClass("required"); }
                    }
                }
            });
        }
        var timeLimit = $("input[name=IsTimeLimit]", e);
        if (timeLimit) {
            timeLimit.on("click", function() {
                if ($(this).is(":checked")) {
                    $(".timelimitcontent", e).addClass("required").show();
                } else {
                    $(".timelimitcontent", e).removeClass("required").hide();
                }
            });
        }
        var isSecondCharge = $("input[name=IsSecondChargeDifferent]", e);
        if (isSecondCharge) {
            isSecondCharge.on("click", function() {
                if ($(this).is(":checked")) {
                    $(".secondratecontent", e).show()
                    $("input[name=SecondCharge]", e).addClass("required");
                } else {
                    $(".secondratecontent", e).hide();
                    $("input[name=SecondCharge]", e).removeClass("required");
                }
            });
        }
        $(".task", e).on("change", function() {
            if ($(this).val() > 0) {
                var value = $(this).find("option:selected").text();
                if (value) {
                    $("input[name=PreferredDescription]", e).val(value);
                }
            }
        });
        $("select[name=SecondChargeType]", e).on("change", function() {
            var secondChargeTypevalue = $(this).val();
            if (secondChargeTypevalue == 1) {
                $(".secondpervisitunitcontent", e).show();
                $("input[name=SecondPerVisitUnit]", e).addClass("required");
            } else {
                $(".secondpervisitunitcontent", e).hide();
                $("input[name=SecondPerVisitUnit]", e).removeClass("required");
            }
        });
        $(".insurance-modifier", e).each(function() {
            $(this).autotab({ target: $(this).prev(), previous: $(this).next() });
        });
    }
}