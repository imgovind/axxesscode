﻿var CommandFactory = {
    //Used to open document window from a given id, patientid, and document type
    Open: function(dataItem, area) {
        var windowId = "";
        if (dataItem.Type.indexOf("OASISC") >= 0) {
            windowId = dataItem.Type.replace("OASISC", "").replace("OT", "").replace("PT", "").replace("ST", "");
        } else if (dataItem.Type.indexOf("HCFA485") >= 0) {
            windowId = "HCFA485"
        } else {
            windowId = dataItem.Type;
        }
        if (windowId.length > 0) {
            var Options = Acore.Windows[windowId];
            if (Options) {
                var url = Options.Url;
                if (url && url.charAt(0) != "/") {
                    url = area + "/" + url;
                } else {
                    url = area + url;
                }
                Acore.Open(windowId, { PatientId: dataItem.PatientId, Id: dataItem.Id }, url);
            } else U.Growl("Not able to find window: " + windowId + ".  Please contact Axxess for assistance in troubleshooting.", "error");
        }
    },
    //Used to create a print argument based on the document type
    Print: function(element, dataItem, serviceName, isQa) {
        var result = [],
            area = window[serviceName].Area,
            urlAction = isQa ? "QAPreview" : "PrintPreview",
            buttons = [];
        result.UrlPrefix = area;
        result.PdfData = { patientId: dataItem.PatientId, eventId: dataItem.Id };
        if ($(element).hasClass("notes")) {
            result.PdfUrl = area + "/Note/Pdf";
            result.Url = area + "/Note/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { Visit.ProcessNote("Return", dataItem.PatientId, dataItem.Id, area, reason); };
                if ($(element).hasClass("poc")) {
                    buttons.push({
                        Text: "View Plan of Care",
                        Click: function() {
                            $(this).trigger("ProcessingStart");
                            Visit.GetCarePlan(dataItem.PatientId, dataItem.Id, $(this), area);
                        }
                    });
                }
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() {
                            CommandFactory.Open(dataItem, area);
                            Acore.ClosePrintView();
                        }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() {
                            Visit.ProcessNote("Approve", dataItem.PatientId, dataItem.Id, area);
                        }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("oasis")) {
            result.Url = area + "/Oasis/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            result.PdfUrl = area + "/Oasis/Pdf";
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { Oasis.StatusAction('Return', dataItem.PatientId, dataItem.Id, area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() {
                            CommandFactory.Open(dataItem, area);
                            Acore.ClosePrintView();
                        }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { Oasis.StatusAction('Approve', dataItem.PatientId, dataItem.Id, area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("planofcare")) {
            result.Url = area + "/PlanOfCare/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            result.PdfUrl = area + "/PlanOfCare/Pdf";
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { Agency.OrdersManagement.Process('Return', dataItem.PatientId, dataItem.Id, 'PlanofCare', area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() {
                            PlanOfCare[serviceName].Edit(dataItem.PatientId, dataItem.Id);
                            Acore.ClosePrintView();
                        }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { Agency.OrdersManagement.Process('Approve', dataItem.PatientId, dataItem.Id, 'PlanofCare', area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("physicianorder")) {
            result.PdfUrl = area + "/Order/Pdf";
            result.Url = area + "/Order/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { Agency.OrdersManagement.Process('Return', dataItem.PatientId, dataItem.Id, 'PhysicianOrder', area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() {
                            PhysicianOrder[serviceName].Edit(dataItem.Id, dataItem.PatientId);
                            Acore.ClosePrintView();
                        }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { Agency.OrdersManagement.Process('Approve', dataItem.PatientId, dataItem.Id, 'PhysicianOrder', area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("communicationnote")) {
            result.PdfUrl = area + "/CommunicationNote/Pdf";
            result.Url = area + "/CommunicationNote/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { CommunicationNote.Process("Return", dataItem.PatientId, dataItem.Id, area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() {
                            CommunicationNote.Edit(dataItem.PatientId, dataItem.Id, area);
                            Acore.ClosePrintView();
                        }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { CommunicationNote.Process("Approve", dataItem.PatientId, dataItem.Id, area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("facetofaceencounter")) {
            result.Url = area + "/FaceToFaceEncounter/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            result.PdfUrl = area + "/FaceToFaceEncounter/Pdf";
            if (isQa && !$(element).hasClass("print")) result.PdfUrl = "";
        } else if ($(element).hasClass("infection")) {
            result.Url = area + "/Infection/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            result.PdfUrl = area + "/Infection/Pdf";
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { Infection.Process("Return", dataItem.PatientId, dataItem.Id, area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() { Infection[serviceName].Edit(dataItem.PatientId, dataItem.Id); Acore.ClosePrintView(); }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { Infection.Process("Approve", dataItem.PatientId, dataItem.Id, area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("incidentaccident")) {
            result.PdfUrl = area + "/Incident/Pdf";
            result.Url = area + "/Incident/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { Incident.Process("Return", dataItem.PatientId, dataItem.Id, area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() { Incident[serviceName].Edit(dataItem.PatientId, dataItem.Id); Acore.ClosePrintView(); }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { Incident.Process("Approve", dataItem.PatientId, dataItem.Id, area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else if ($(element).hasClass("medicareeligibility")) {
            result.Url = area + "/PatientProfile/MedicareEligibilityReportPrint/" + dataItem.PatientId + "/" + dataItem.Id;
            result.PdfUrl = area + "/PatientProfile/MedicareEligibilityReportPdf";
            if (isQa && !$(element).hasClass("print")) result.PdfUrl = "";
        } else if ($(element).hasClass("missedvisit")) {
            result.Url = area + "/MissedVisit/" + urlAction + "/" + dataItem.PatientId + "/" + dataItem.Id;
            result.PdfUrl = area + "/MissedVisit/Pdf";
            if (isQa) {
                if (!$(element).hasClass("print")) result.PdfUrl = "";
                if ($(element).hasClass("return")) result.ReturnClick = function(reason) { MissedVisit.Process("Return", dataItem.PatientId, dataItem.Id, area, reason); };
                if ($(element).hasClass("edit")) {
                    buttons.push({
                        Text: "Edit",
                        Click: function() {
                            MissedVisit[serviceName].Edit(dataItem.Id);
                            Acore.ClosePrintView();
                        }
                    });
                }
                if ($(element).hasClass("approve")) {
                    buttons.push({
                        Text: "Approve",
                        Click: function() { MissedVisit.Process("Approve", dataItem.PatientId, dataItem.Id, area); }
                    });
                }
                result.Buttons = buttons;
            }
        } else {
            return undefined;
        }
        return result;
    },
    Delete: function(element, dataItem, serviceName) {
        var result = "";
        if (element) {
            var type = $(element).attr("docType");
            if (type == "notes") result = "Schedule.Task." + serviceName + ".Delete('" + dataItem.Id + "','" + dataItem.PatientId + "')";
            else if (type == "physicianorder") result = "Patient.Orders." + serviceName + ".DeleteOrder('" + dataItem.Id + "','" + dataItem.PatientId + "')";
            else if (type == "communicationnote") result = "CommunicationNote." + serviceName + ".Delete('" + dataItem.Id + "','" + dataItem.PatientId + "')";
            else if (type == "infection") result = "Incident." + serviceName + ".Delete('" + dataItem.Id + "','" + dataItem.PatientId + "');";
            else if (type == "incidentaccident") result = "Infection." + serviceName + ".Delete('" + dataItem.Id + "','" + dataItem.PatientId + "');";
        }
        return result;
    },
    GridInit: function(element, dataItem, serviceName) {
        if (element) {
            $("td.edit-task a", element).on("click", function() {
                if ($(this).hasClass("edit")) {
                    var area = window[serviceName].Area;
                    CommandFactory.Open(dataItem, area);
                } else if ($(this).hasClass("missedvisit")) {
                    if ($(this).hasClass("complete")) {
                        MissedVisit[serviceName].Open(dataItem.Id);
                    } else {
                        MissedVisit[serviceName].Edit(dataItem.Id);
                    }
                }
            });
            $("td a.print", element).on("click", function() {
                var result = CommandFactory.Print(this, dataItem, serviceName);
                if (result != undefined) {
                    Acore.OpenPrintView(result);
                }
            });
            $("td.oasisprofileprint a.OASISprofile", element).on("click", function() { Acore.OpenPrintView({ Url: "Oasis/Profile/" + dataItem.PatientId + "/" + dataItem.Id, PdfUrl: 'Oasis/ProfilePdf', PdfData: { 'patientId': dataItem.PatientId, 'Id': dataItem.Id} }); });
            $("td.attachmentload a.attachment", element).on("click", function() { Schedule.Task[serviceName].GetAttachments(dataItem.PatientId, dataItem.Id); });
        }
    }
}