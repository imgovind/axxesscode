﻿var Infection = {
    ListWindowId: "listinfections",
    InitNew: function(r, t, x, e) {
        var service = $("input[name=ServiceId]", e).val();
        var serviceName = Acore.AppService(service);
        Episode.InitPatientChangeDropDown(e, serviceName);
        PhysicianOrder.InitNewOrderButton(e, serviceName);
        U.ShowIfChecked($("#New_Infection_IndividualInvolved4", e), $("#New_Infection_IndividualInvolvedOther", e));
        Infection.Init(r, t, x, e);
    },
    Init: function(r, t, x, e) {
        var form = $("form.mainform", e);
        U.InitValidation(form, function(result) { Refresh.CenterQAMySchedule(result); Infection.Refresh(); });
    },
    InitList: function(r, t, x, e) {
        Infection.BindNewButton(e);
        var service = $("[name=ServiceId]", e).val();
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
    },
    BindNewButton: function(e) {
        $(".new-infection-log", e).off("click");
        $(".new-infection-log", e).on("click", function() {
            var service = $("[name=ServiceId]", e).val();
            Infection[Acore.AppService(service)].New();
            return false;
        });
    },
    Delete: function(patientId, id, serviceId) {
        var service = Acore.AppService(serviceId);
        if (service) {
            var area = window[service].Area;
            Acore.Confirm({
                Message: "Are you sure you want to delete this Infection Log?",
                Yes: function() {
                    U.PostUrl(area + "/Infection/Delete", { Id: id, patientId: patientId }, function(result) {
                        if (result.isSuccessful) Infection.Refresh();
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    });
                }
            });
        }
    },
    OnDataBound: function(e) {
        U.OnTGridDataBound(e);
        var grid = $(e.target).data('tGrid');
        if (grid) {
            var datas = grid.data;
            if (datas.length > 0) {
                var data = datas[0];
                if (data != null) {
                    if (data.IsUserCanEdit || data.IsUserCanDelete) {
                        var width = 0;
                        if (data.IsUserCanEdit) width += 50;
                        if (data.IsUserCanDelete) width += 50;
                        grid.showColumn(6);
                        $("td:eq(6),th:eq(6),col:eq(6)", $(e.target).find("tbody tr,colgroup")).css({ width: width });
                    }
                    else { grid.hideColumn(6); }
                    if (data.IsUserCanPrint) { grid.showColumn(5); } else { grid.hideColumn(5); }
                }
            }
            else {
                grid.hideColumn(5);
                grid.hideColumn(6);
            }
        }
    },
    OnRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.IsUserCanEdit || dataItem.IsUserCanDelete) {
            var action = "";
            if (dataItem.IsUserCanEdit) {
                action = "<a class=\"link\" onclick=\"Infection.Edit('" + dataItem.Id + "','" + dataItem.Service + "');\">Edit</a>";
            }
            if (dataItem.IsUserCanDelete) {
                action += "<a class=\"link\" onclick=\"Infection.Delete('" + dataItem.PatientId + "','" + dataItem.Id + "','" + dataItem.Service + "');\" >Delete</a>";
            }
            e.row.cells[6].innerHTML = action;
        }
        if (dataItem.IsUserCanPrint) {
            e.row.cells[5].innerHTML = "<a onclick=\"Infection.Print('" + dataItem.PatientId + "','" + dataItem.Id + "','" + dataItem.Service + "');\"><span class='img icon16 print'></span></a>";
        }
    },
    Edit: function(id, serviceId) {
        var service = Acore.AppService(serviceId);
        Infection[service].Edit(id)
    },
    Print: function(patientId, id, serviceId) {
        var service = Acore.AppService(serviceId);
        var area = window[service].Area;
        Acore.OpenPrintView({ Url: area + "/Infection/PrintPreview/" + patientId + "/" + id, PdfUrl: area + "/Infection/Pdf", PdfData: { patientId: patientId, EventId: id} });
    },
    Refresh: function() { U.RebindTGrid($('#InfectionList_Grid')) },
    Process: function(button, patientId, eventId, prefix, reason) {
        var input = { patientId: patientId, eventId: eventId },
            isReturning = button == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/Infection/" + button, input, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.All(result);
                if (result.IsInfectionRefresh) Infection.Refresh();
            }else if (isReturning) Acore.ReturnReasonFailed();
        });
    }
}