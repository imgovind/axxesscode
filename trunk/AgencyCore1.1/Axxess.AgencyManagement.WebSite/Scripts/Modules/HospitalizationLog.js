﻿var HospitalizationLog = {
    assessmentType: "",
    Add: function(patientId) {
        Acore.Open("newhospitalizationlog", 'Patient/NewHospitalizationLog', function() { HospitalizationLog.InitNew(); }, { patientId: patientId });
    },
    InitNew: function() {
        var form = $("#newHospitalizationLogForm");
        form.validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $("#New_HospitalizationLog_PatientName").val();
                            UserInterface.CloseWindow('newhospitalizationlog');
                            HospitalizationLog.Refresh(patientId);
                            HospitalizationLog.RebindList();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(patientId, hospitalizationLogId) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this Hospitalization Log?",
            Yes: function() {
                U.PostUrl("Patient/UpdateHospitalizationLogStatus", { patientId: patientId, hospitalizationLogId: hospitalizationLogId, isDeprecated: true }, function(result) {
                    if (result.isSuccessful) HospitalizationLog.Refresh(patientId);
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                })
            }
        })
    },
    Edit: function(patientId, hospitalizationLogId) {
        Acore.Open("edithospitalizationlog", 'Patient/EditHospitalizationLog', function() { HospitalizationLog.InitEdit(patientId); }, { patientId: patientId, hospitalizationLogId: hospitalizationLogId });
    },
    InitEdit: function(patientId) {
        var form = $("#editHospitalizationLogForm");
        form.validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            HospitalizationLog.Refresh(patientId);
                            HospitalizationLog.RebindList();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Refresh: function(patientId) {
        $("#Hospitalization_list ol").addClass("loading");
        $("#Hospitalization_list").load("Patient/Hospitalizations", { "patientId": patientId },
            function(responseText, textStatus, XMLHttpRequest) {
                $('#Hospitalization_list ol').removeClass("loading");
                if (textStatus == 'error') {
                    $('#Hospitalization_list').removeClass("standard-chart");
                    $('#Hospitalization_list').html(U.AjaxError);

                }
            }
        );
    },
    RebindList: function() { U.RebindTGrid($('#List_Patient_Hospitalized_Grid')); }
}