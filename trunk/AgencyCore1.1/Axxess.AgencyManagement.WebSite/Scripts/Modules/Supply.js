﻿var Supply = {
    Edit: function(id) {
        Acore.Open("editsupply", "Supply/Edit", Supply.InitEdit, { Id: id });
    },
    InitEdit: function(r, t, x, e) {
        U.InitTemplate($("form.mainform", e), function() { Supply.RebindList(); }, "Supply has been successfully updated.");
    },
    InitNew: function(r, t, x, e) {
        U.InitTemplate($("form.mainform", e), function() { Supply.RebindList(); }, "Supply has been successfully added.");
    },
    InitList: function(r, t, x, e) {
        $("a.new", e).on("click", function() { Acore.Open("newsupply"); });
        U.ListInitClientRebind(r, t, x, e);
    },
    RebindList: function() {  U.RebindTGrid($('#List_Supply')); },
    Search: function(Element) {
        Element.find("input[name=Description]").AjaxAutocomplete({
            ExtraParameters: { 'limit': 50 },
            minLength: 1,
            SourceUrl: "Agency/SuppliesSearch",
            Format: function(json) { return json.Description },
            Select: function(json, input) {
                input.val(json.Description);
                Element.find("input[name=Code]").val(json.Code);
                Element.find("input[name=UnitCost]").val(json.UnitCost);
                Element.find("input[name=UniqueIdentifier]").val(json.Id);
                Element.find("input[name=RevenueCode]").val(json.RevenueCode);
            }
        });
    },
    OnSupplyEdit: function(e) {
        if (e != null && e != undefined && e.mode == 'edit') {
            $(e.form).find("input[name=RevenueCode]").val(e.dataItem.RevenueCode);
            $(e.form).find("input[name=Code]").val(e.dataItem.Code);
        }
        Supply.Search($(e.form));
    },
    Delete: function(id) {
        U.Delete("Supply", "/Supply/Delete", { Id: id },
            function() {
                Supply.RebindList();
            }, false);
    }
}