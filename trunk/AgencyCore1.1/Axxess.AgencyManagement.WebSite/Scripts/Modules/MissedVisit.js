﻿var MissedVisit = {
    ListWindowId: "listmissedvisits",
    Open: function(missedVisitId, prefix) {
        if (!prefix) prefix = "";
        Acore.Dialog({
            Name: "Missed Visit",
            Url: prefix + "/MissedVisit/Popup",
            Input: { id: missedVisitId },
            Width: "500px"
        });
    },
    New: function(patientId, eventId, prefix) {
        if (!prefix) prefix = "";
        Acore.Modal({
            Name: "Missed Visit",
            Url: prefix + "/MissedVisit/New",
            Input: { patientId: patientId, eventId: eventId },
            OnLoad: MissedVisit.InitNew,
            Width: 800
        });
    },
    Edit: function(eventId, prefix) {
        if (!prefix) prefix = "";
        Acore.Open("editmissedvisit", prefix + "/MissedVisit/Edit", { Id: eventId });
    },
    InitNew: function(r, t, x, e) {
        var form = $("form.mainform", e);
        U.InitValidation(form, function(result) {
            MissedVisit.Refresh();
            Refresh.All(result);
        });
    },
    InitEdit: function(r, t, x, e) {
        var form = $("form.mainform", e);
        U.InitValidation(form, function(result) {
            UserInterface.CloseWindow('editmissedvisit');
            Acore.CloseDialog();
            MissedVisit.Refresh();
            Refresh.All(result);
        });
    },
    InitList: function(r, t, x, e) {
        U.ListInitWithArea(r, t, x, e);
    },
    InitListContent: function(gridContent) {
        var grid = gridContent.data('tGrid');
        if (grid) {
            var trs = $("tr:not(.t-grouping-row,.t-no-data)", grid.$tbody);
            var serviceName = "";
            if (trs.length > 0) {
                trs.each(function(index) {
                    if (index == 0) { serviceName = $(this).find("td.service").text(); }
                    var dataItem = { Id: $(this).find("td.id").text(), PatientId: $(this).find("td.pid").text() };
                    $("td a.link", this).not(".delete-task,.restore").on("click", function() {
                        if ($(this).hasClass("complete")) {
                            MissedVisit[serviceName].Open(dataItem.Id);
                        }
                        else {
                            MissedVisit[serviceName].Edit(dataItem.Id);
                        }
                    });

                    var action = $("td.action", this);
                    if (action) {
                        $("a.delete-task", action).on("click", function() { Schedule.Task[serviceName].Delete(dataItem.PatientId, dataItem.Id); });
                        $("a.restore", action).on("click", function() { MissedVisit[serviceName].Restore(dataItem.PatientId, dataItem.Id); });
                    }

                });
            }
        }
    },
    Restore: function(Url, Data, Callback) {
        Acore.Confirm({
            Message: "Are you sure you want to restore this missed visit?",
            Yes: function() {
                U.PostUrl(Url, Data, function(data) {
                    U.Growl(data.errorMessage, data.isSuccessful ? "success" : "error");
                    if (data.isSuccessful && typeof Callback === "function") Callback(data);
                })
            }
        })
    },
    Refresh: function() {
        if (Acore.Windows[MissedVisit.ListWindowId].IsOpen) {
            var window = $("#window_" + MissedVisit.ListWindowId);
            $(".grid-refresh", window).click();
        }
    },
    Process: function(button, patientId, eventId, prefix, reason) {
        var input = { patientId: patientId, eventId: eventId },
            isReturning = button == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/MissedVisit/" + button, input, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.All(result);
                MissedVisit.Refresh();
            }else if (isReturning) Acore.ReturnReasonFailed();
        });
    }
}
    