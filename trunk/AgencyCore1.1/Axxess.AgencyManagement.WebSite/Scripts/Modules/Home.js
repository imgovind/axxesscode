﻿var Home = {
    Init: function() {
        var area = window[$("#window_Dashboard").find("input[name=PerferredService]").val()].Area;
        if (Acore.GetRemoteContent) {
            U.PostUrl("/Message/CustomWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(message) {
                if (message != undefined && message.Text != null && message.Text.length > 0) {
                    $('#custom-message-widget').html(message.Text);
                    Message.InitWidget();
                }
                else {
                    $('#custom-message-widget').html("<div class='ac'><span class='bigtext'>Welcome to</span><br /><span class='img acorelogo'></span></div><div>AgencyCore&#8482; is a secure Enterprise Agency Management software designed from the ground up to provide powerful online capabilities that enables real-time collaboration for you and your team.</div>");
                }
            });
        }

        U.PostUrl("/Report/PatientBirthdayWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.data && data.data.length > 0) {
                for (var i = 0; i < data.data.length && i < 5; i++) $('#birthdayWidgetContent').append("<tr><td>" + data.data[i].BirthDay + "</td><td>" + data.data[i].Age + "</td><td><a onclick=\"UserInterface.ShowPatientChart('" + data.data[i].Id + "', '" + data.data[i].IsDischarged + "');\">" + data.data[i].Name + "</a></td><td>" + data.data[i].PhoneHomeFormatted + "</td></tr>");
            } else {
                $('#birthdayWidgetContent').append("<tr><td colspan='5' class='ac'><h1 class='blue'>No Birthdays Found</h1></td></tr>");
            }
        });
        U.PostUrl(area + "/Schedule/ScheduleWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++)
                    $('#scheduleWidgetContent').append($("<tr/>")
                    .append($("<td/>").append($("<a/>", { text: data[i].PatientName }).on("click", data[i], function(event) {
                        UserInterface.ShowPatientChart(event.data.PatientId, event.data.IsDischarged);
                    })))
                    .append($("<td/>").append(
                     $(data[i].IsUserCanEdit ? "<a/>" : "<span />", { text: data[i].TaskName, "class": data[i].Type }).on("click", data[i], function(event) {
                         if (event.data.IsUserCanEdit) {
                             CommandFactory.Open({ PatientId: event.data.PatientId, Id: event.data.EventId, Type: event.data.Type }, window[Acore.AppService(event.data.Service)].Area);
                         }
                         else {
                             return false;
                         }
                     })
                    )).append($("<td/>", { text: data[i].EventDate })));
                var functionToCall;
                if (!area) functionToCall = User.Calendar.Open;
                else {
                    if (PrivateDuty && area == PrivateDuty.Area) functionToCall = PrivateDuty.UserSchedule.Open;
                }
                if (functionToCall) $('#userScheduleWidgetMore').click(functionToCall);
            }
            else {
                $('#scheduleWidgetContent').append("<tr><td colspan='5' class='ac'><h1 class='blue'>No Scheduled Tasks Found</h1></td></tr>");
                $('#userScheduleWidgetMore').hide();
            }
        });

        U.PostUrl("/Schedule/RecertsPastDueWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#recertPastDueWidgetContent').append("<tr><td><a onclick=\"UserInterface.ShowPatientChart('" + data[i].Id + "', 'false');\">" + data[i].PatientName + "</a></td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDateFormatted + "</td></tr>");
            } else {
                $('#recertPastDueWidgetContent').append("<tr><td colspan='3' class='ac'><h1 class='blue'>No Past Due Recertifications Found</h1></td></tr>");
                $('#pastDueRecertsMore').hide();
            }
        });

        U.PostUrl("/Schedule/RecertsUpcomingWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#recertUpcomingWidgetContent').append("<tr><td><a onclick=\"UserInterface.ShowPatientChart('" + data[i].Id + "', 'false');\">" + data[i].PatientName + "</a></td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDateFormatted + "</td></tr>");
            } else {
                $('#recertUpcomingWidgetContent').append("<tr><td colspan='3' class='ac'><h1 class='blue'>No Upcoming Recertifications Found</h1></td></tr>");
                $('#upcomingRecertsMore').hide();
            }
        });

        U.PostUrl("/Billing/Unprocessed", "", function(data) {
            if (data && data.length > 0) for (var i = 0; i < data.length && i < 5; i++) $('#claimsWidgetContent').append("<tr><td><a onclick=\"UserInterface.ShowPatientChart('" + data[i].Id + "', '" + data[i].IsDischarged + "');\">" + data[i].PatientDisplayName + "</a></td><td>" + data[i].Type + "</td><td>" + data[i].EpisodeRange + "</td></tr>");
            else $('#claimsWidgetContent').append("<tr><td colspan='3' class='ac'><h1 class='blue'>No Bills Found</h1></td></tr>").closest('widget').find('#claimsWidgetMore').hide();
        });
    }
}