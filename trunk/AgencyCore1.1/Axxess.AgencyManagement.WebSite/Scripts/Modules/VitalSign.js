﻿var VitalSign = {
    New: function(patientId, entityId) {
        VitalSign.Modal("VitalSign/New", { PatientId: patientId, EntityId: entityId });
    },
    Edit: function(patientId, entityId, id) {
        VitalSign.Modal("VitalSign/Edit", { PatientId: patientId, EntityId: entityId, Id: id });
    },
    Modal: function(url, input) {
        Acore.Modal({
            Name: "Vital Signs",
            Url: url,
            Input: input,
            Width: 858,
            VersitalHeight: true,
            OnLoad: function(r, t, x, e) { VitalSign.Init(e); }
        });
    },
    Init: function(e, isEmbededInNote) {
        VitalSign.SetupAddDeleteButton($("#VitalSign_Pulses", e), "");
        VitalSign.SetupAddDeleteButton($("#VitalSign_BloodPressures", e), "0");
        if (isEmbededInNote) {
            var changedInput = $("#VitalSign_VitalSignsChanged", e);
            $(":input", e).each(function() {
                $(this).attr("name", "vitalSignLog." + $(this).attr("name"));
            });
            $("select, input:not(#VitalSign_VitalSignsChanged)", e).on("change", function() {
                changedInput.val("true");
            });
            e.closest("form").on("AjaxSubmitSuccess", function() {
                e.find("[name=IsNew]").val(false);
            });
        } else {
            $("form", e).Validate();
        }
    },
    Delete: function(patientId, entityId, id) {
        U.Delete("Vital Sign", "VitalSign/Delete", { patientId: patientId, entityId: entityId, id: id }, function() {
            var window = $("[name='EventId'][value='" + entityId + "']").closest(".window");
            U.RebindTGrid($("#List_EntityVitalSigns", window));
        });
    },
    OpenEntityList: function(patientId, entityId, disciplineTaskId) {
        Acore.Open("listentityvitalsigns", { patientId: patientId, entityId: entityId, disciplineTaskId: disciplineTaskId });
    },
    SetupAddDeleteButton: function($content, defaultSelectValue) {
        var firstItemSpan = $("div:first-child", $content).find("span"),
            hiddenRows = $(".row.removable.hidden", $content),
            addButton = $("a.add-button", $content);
        addButton.on("click", function() {
            $(hiddenRows.splice(0, 1)).removeClass("hidden");
            if (hiddenRows.length == 0) $(this).parent().hide();
            firstItemSpan.show();
        });
        $(".remove-button", $content).on("click", function() {
            var row = $(this).closest(".row");
            if (row.length) {
                $("input", row).val("");
                $("select", row).val(defaultSelectValue);
                hiddenRows.push(row.addClass("hidden")[0]);
            }
            if (hiddenRows.length == 5) firstItemSpan.hide();
            addButton.parent().show();
        });
    }
};