﻿var Agency = {
    _patientId: "",
    _dateFilter: "",
    _showFilter: "",
    _isRebind: false,
    _fromDate: "",
    _toDate: "",
    Edit: function(Id) { Acore.Open("editagency", 'Agency/Edit', function() { Agency.InitEdit(); }, { id: Id }); },
    InitEdit: function() { U.InitEditTemplate("Agency"); },
    InitEditCost: function(r, t, x, e) { U.InitTemplate($("#editVisitCostForm", e), function() { UserInterface.CloseWindow('visitrates'); }, "Visit rate successfully updated"); },
    InitSignature: function(r, t, x, e) {
        $("form", e).Validate({
            PreprocessForm: function(form) {
                if (!$("#checkSignature", form).val().length) {
                    U.Growl("Please provide your signature before proceeding", 'error');
                    return false;
                }
                return true;
            },
            Success: function() { Acore.Open("agencyinfo"); }
        });
    },
    InitInfo: function(r, t, x, e) {
        U.InitTemplate($("#editAgencyForm", e), function() { }, "Agency successfully updated.");
        $("#Edit_AgencyInfo_LocationId", e).on("change", function() { Agency.LoadBranchContent($(this).val()); });
    },
    InitNew: function() {
        Lookup.LoadStates();
        $(".names").alpha({ nocaps: false });
        $("input[name=Edit_Agency_SameAsAdmin]").on("click", function() {
            if ($(this).is(':checked')) {
                $("#Edit_Agency_ContactPersonEmail").val($("#Edit_Agency_AdminUsername").val());
                $("#Edit_Agency_ContactPersonFirstName").val($("#Edit_Agency_AdminFirstName").val());
                $("#Edit_Agency_ContactPersonLastName").val($("#Edit_Agency_AdminLastName").val());
            } else $("#Edit_Agency_ContactPersonEmail").add("#Edit_Agency_ContactPersonFirstName").add("#Edit_Agency_ContactPersonLastName").val('');
        });
        $("input[name=Edit_Agency_AxxessBiller]").on("click", function() {
            if ($(this).is(':checked')) {
                $("#Edit_Agency_SubmitterId").val("SW23071");
                $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
                $("#Edit_Agency_SubmitterPhone1").val("214");
                $("#Edit_Agency_SubmitterPhone2").val("575");
                $("#Edit_Agency_SubmitterPhone3").val("7711");
                $("#Edit_Agency_SubmitterFax1").val("214");
                $("#Edit_Agency_SubmitterFax2").val("575");
                $("#Edit_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#Edit_Agency_SubmitterId").add("#Edit_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });
        $("input[name=IsAgreementSigned]").on("change", function() {
            if ($(this).val() == "true") $("#Edit_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#Edit_Agency_TrialPeriod").removeAttr("disabled");
        });
        U.InitNewTemplate("Agency");
    },
    InitLocationBillDate: function() { U.InitTemplate($("#newLocationBillData"), function() { Agency.RebindLocationBillDataList(); }, "Insurance rate successfully saved"); },
    InitLocationEditBillDate: function() { U.InitTemplate($("#editLocationBillData"), function() { Agency.RebindLocationBillDataList(); }, "Insurance rate successfully updated"); },
    DeleteLocationBillData: function(locationId, Id) {
        U.Delete("Visit rate", "/Agency/DeleteLocationBillData", { LocationId: locationId, Id: Id }, Agency.RebindLocationBillDataList);
    },
    ToolTip: function(Id) {
        $("a.note", $(Id)).Tooltip({
            top: 5,
            left: -15,
            isNote: true,
            extraClass: "sticky-note"
        });
    },
    LoadVisitRate: function() { Acore.Open("visitrates"); },
    LoadVisitRateContent: function(branchId) {
        $("#Edit_VisitRate_Container").empty().addClass("loading").load('Agency/VisitRateContent', { branchId: branchId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('Visit rates could not be loaded based on the selected branch. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#Edit_VisitRate_Container").removeClass("loading");
        });
    },
    LoadBranchContent: function(branchId) {
        $("#Edit_AgencyInfo_Container").Load('Agency/InfoContent', { branchId: branchId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('Branch Information could not be loaded based on the selected branch. Please close this window and try again.', 'error');
        });
    },
    OpenReferralPatientInfo: function(id) {
        Acore.Open("referralpatient", "Therapy/Agency/ReferralInfo", Agency.RebindReferrals, { id: id });
    },
    OpenMsgInfo: function(id) { },
    OpenContractInfo: function(id) { },
    RebindList: function() { U.RebindTGrid($('#List_Agencies')); },
    RebindAgencyCommunicationNotes: function() {
        var grid = $('#AgencyCommunicationNotes_Grid').data('tGrid');
        if (grid != null && grid != undefined) {
            { grid.rebind({ BranchId: $("#AgencyCommunicationNotes_BranchId").val(), Status: $("#AgencyCommunicationNotes_Status").val(), StartDate: $("#AgencyCommunicationNotes_StartDate-input").val(), EndDate: $("#AgencyCommunicationNotes_EndDate-input").val() }); }
            var $exportLink = $('#AgencyCommunicationNotes_ExportLink');
            if ($exportLink != undefined) {
                var href = $exportLink.attr('href');
                if (href != undefined) {
                    href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyCommunicationNotes_BranchId").val());
                    href = href.replace(/Status=([^&]*)/, 'Status=' + $("#AgencyCommunicationNotes_Status").val());
                    href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyCommunicationNotes_StartDate-input").val());
                    href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgencyCommunicationNotes_EndDate-input").val());
                    $exportLink.attr('href', href);
                }
            }
        }
    },

    RebindLocationBillDataList: function() { var grid = $("#Edit_Location_BillDatas").data('tGrid'); if (grid != null) { grid.rebind({ InsuranceId: $("#Edit_Insurance_Id").val() }); } },
    LoadAgencyInsurances: function(branchId, insuranceControlId, service) {
        U.PostUrl("Agency/PatientInsurances", {
            branchId: branchId,
            service: service != undefined ? service : 1
        }, function(data) {
            var s = $("select#" + insuranceControlId);
            s.children('option').remove();
            s.append(data);
        })
    },
    LoadInsurances: function(branchId, mainDOM, className, service) {
        U.PostUrl("Agency/PatientInsurances", {
            branchId: branchId,
            service: service != undefined ? service : 1
        }, function(data) {
            var list = $("select." + className, mainDOM);
            if (list != null && list != undefined) {
                list.each(function() { $(this).children('option').remove().append(data); });
            }
        })
    },
    RebindMedicareEligibilitySummaryGridContent: function(reportId, action, jsonData, sortParams) {
        var input = jsonData;
        if (sortParams != null && sortParams != undefined) {
            $.extend(input, { SortParams: sortParams });
        }
        $("#" + reportId + "GridContainer").empty().addClass("loading").load('Agency/' + action, input, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("This report can't be loaded. Please close this window and try again.", 'error');
            }
            else if (textStatus == "success") {
                $(this).removeClass("loading");
                var $exportLink = $('#' + reportId + '_ExportLink');
                if ($exportLink != null && $exportLink != undefined) {
                    var href = $exportLink.attr('href');
                    if (href != null && href != undefined) {
                        $.each(jsonData, function(key, value) {
                            var filter = new RegExp(key + "=([^&]*)");
                            href = href.replace(filter, key + '=' + value);
                        });
                        $exportLink.attr('href', href);
                    }
                }
            }
        });
    },
    InitQaCenter: function() {
        $('#window_caseManagementcenter_content .t-grid-content').css({ height: 'auto' });
        $('#window_caseManagementcenter .layout').Layout({
            west: {
                paneSelector: ".ui-layout-west",
                size: 200,
                minSize: 160,
                maxSize: 400,
                livePaneResizing: true,
                spacing_open: 3
            }
        })
    },
    QaListDataBound: function() {
        Patient.Filter($("#txtSearch_Qa_Selection").val());
        if ($("#CaseManagerSelectionGrid .t-grid-content tr").length) {
            if (Agency._patientId == "") $('#CaseManagerSelectionGrid .t-grid-content tr' + ($("#txtSearch_Qa_Selection").val().length ? ".match" : "")).eq(0).click();
            else $('td:contains(' + Agency._patientId + ')', $('#CaseManagerSelectionGrid')).closest('tr').click();
        } else $("#QaMainResult").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
        if ($("#CaseManagerSelectionGrid .t-state-selected").length) $("#CaseManagerSelectionGrid .t-grid-content").scrollTop($("#CaseManagerSelectionGrid .t-state-selected").position().top - 50);
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            if (!Agency._isRebind) {
                Agency._dateFilter = "";
                Agency._showFilter = "";
                Agency._fromDate = "";
                Agency._toDate = "";
            }
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Agency.LoadInfoAndActivity(patientId);
        }
    },
    LoadInfoAndActivity: function(patientId) {
        $("#qacenterpatient-info").remove();
        $('#QaMainResult').empty().addClass('loading').load('Agency/CaseManagementData', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#QaMainResult').removeClass('loading');
            if (textStatus == 'error') $('#QaMainResult').html(U.AjaxError);
            $('#window_caseManagementcenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    RebindAgenciesSelection: function(state, status) {
        var grid = $('#AgencySelectionGrid');
        if (grid != undefined) {
            var agencySelectionGridData = grid.data('tGrid');
            if (agencySelectionGridData != undefined) {
                $.ajax({
                    url: 'Therapy/Agency/AgencyEmployors',
                    type: "POST",
                    data: { state: state, status: status },
                    dataType: 'json',
                    success: function(dataResult) {
                        agencySelectionGridData.dataBind(dataResult);
                        var girdContent = grid.find("div.t-grid-content");
                        $('tr:first', girdContent).click();
                        if (!$(dataResult).length) {
                            $("#AgencyMainResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Agency Found to Meet Your Search Requirements%3C/h1%3E" +
                    "%3C/div%3E"));
                        }
                    }
                });
            }
        }
    },
    AgencyListFirstDataBound: function(Id, gridId) {
        var selectedTr = $('td:contains(' + Id + ')', $('#' + gridId)).closest('tr');
        if (selectedTr != undefined && selectedTr != null) {
            selectedTr.addClass("t-state-selected");
            var gridContent = selectedTr.closest(".t-grid-content");
            if (gridContent != undefined && gridContent != null) {
                var position = selectedTr.position();
                if (position != undefined) {
                    var scroll = position.top + gridContent.scrollTop() - 24;
                    if (scroll != undefined && scroll > 24) {
                        gridContent.animate({ scrollTop: scroll }, 'slow');
                    }
                }
            }
        }
    },
    Filter: function(text, gridContent) {
        if (gridContent != undefined) {
            if (text != undefined && text.length > 0) {
                text = $.trim(text).toUpperCase();
                var search = text.split(" ");
                if (search != undefined && search.length > 0) {
                    var gridTr = $("tbody  tr", gridContent);
                    if (gridTr != undefined && gridTr.length > 0) {
                        gridTr.removeClass("match").hide();
                        var matchedTrs = gridTr.filter(function(index) {
                            var tdN = $(this).find("td.searchN").text();
                            var result = $.grep(search, function(s, i) {
                                return (tdN.indexOf(s) > -1);
                            })
                            return result.length > 0;
                        });
                        matchedTrs.removeClass("t-alt").addClass("match").show().filter(":even").addClass("t-alt");
                    }
                }
            }
        }
    },
    ShowReferralInfo: function(id) {

    },
    OnAgencyRowSelected: function(e) {
        if (e.row.cells[1] != undefined) {
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var agencyId = e.row.cells[1].innerHTML;
            if (agencyId != undefined && agencyId != null && agencyId != "" && U.IsGuid(agencyId)) {
                Agency.LoadInfoAndReferrals(agencyId);
            }
        }
    },
    LoadInfoAndReferrals: function(agencyId) {
        $('#AgencyMainResult').empty().addClass('loading').load('Therapy/Agency/AgencyData', { agencyId: agencyId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#AgencyMainResult').removeClass('loading');
            if (textStatus == 'error') $('#AgencyMainResult').html(U.AjaxError);
            $('#window_agencymanagementcenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    SignReferral: function(id, status) {
        U.PostUrl("Therapy/Agency/AcceptPatient", { id: id, status: status }, function(result) {
            if (result.isSuccessful) {
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    ViewNotice: function(id) {
        Acore.Open("noticeinfo", 'Therapy/Agency/ReadNotice', Agency.RebindReferrals, { id: id });
    },
    RebindReferrals: function() {
        var agencyId = $("input[name=Referrals_AgencyId]").val();
        var contentGrid = $('#ReferralPatientsGrid');
        U.RebindTGrid(contentGrid, { agencyId: agencyId, status: 0 });
    },
    MessageTypeChange: function(agencyId, type, status) {
        var contentGrid = $('#ReferralPatientsGrid');
        if (contentGrid != undefined) {
            U.RebindTGrid(contentGrid, { agencyId: agencyId, status: status, type: type, area: "Therapy" });
        }
    },
    ActivityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).Tooltip({
            top: 5,
            left: -15,
            isNote: true,
            extraClass: "sticky-note"
        });
        if (dataItem.IsComplete) {
            $(e.row).addClass('darkgreen');
            if (dataItem.StatusName == "Missed Visit(complete)") {
                $(e.row).removeClass('darkgreen');
                $(e.row).addClass('darkred');
            }
        }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').Tooltip({
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).on("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append(
                $("<li/>").append(
                    $("<a/>", { text: "Reopen Task" }).on("click", function() {
                        $(e.row).find("a:contains('Reopen Task')").click();
                    })));
            else if (!dataItem.IsOrphaned) Menu.append(
                $("<li/>").append(
                    $("<a/>", { text: "Edit Note" }).on("click", function() {
                        $(e.row).find("a:first").click();
                    })));
            Menu.append(
                $("<li/>").append(
                    $("<a/>", { "text": "Details" }).on("click", function() {
                        $(e.row).find("a:contains('Details')").click();
                    }))).append(
                $("<li/>").append(
                    $("<a/>", { text: "Delete" }).on("click", function() {
                        $(e.row).find("a:contains('Delete')").click();
                    }))).append(
                $("<li/>").append(
                    $("<a/>", { text: "Print" }).on("click", function() {
                        $(e.row).find(".print").parent().click();
                    })));
            Menu.ContextMenu(Event);
        });
    }
}