﻿var Hospital = {
    New: function() {
        Acore.Open("newhospital", "Hospital/New", function() { Hospital.InitNew(); });
    },
    Edit: function(Id) {
        Acore.Open("edithospital", "Hospital/Edit", function() { Hospital.InitEdit(); }, { Id: Id });
    },
    Delete: function(Id) { U.DeleteTemplate("Hospital", Id); },
    InitEdit: function() {
        U.InitEditTemplate("Hospital");
        U.PhoneAutoTab("Edit_Hospital_PhoneArray");
        U.PhoneAutoTab("Edit_Hospital_FaxNumberArray");
    },
    InitNew: function() {
        U.InitNewTemplate("Hospital");
        U.PhoneAutoTab("New_Hospital_PhoneArray");
        U.PhoneAutoTab("New_Hospital_FaxNumberArray");
    },
    RebindList: function() { U.RebindTGrid($('#List_AgencyHospital')); }
}