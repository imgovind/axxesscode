﻿var Incident = {
    ListWindowId: "listincidents",
    InitNew: function(r, t, x, e) {
        var service = $("input[name=ServiceId]", e).val();
        var serviceName = Acore.AppService(service);
        Episode.InitPatientChangeDropDown(e, serviceName);
        PhysicianOrder.InitNewOrderButton(e, serviceName);
        Incident.Init(r, t, x, e);
    },
    Init: function(r, t, x, e) {
        var form = $("form.mainform", e);
        U.InitValidation(form, function(result) {
            Refresh.CenterQAMySchedule(result);
            Incident.Refresh();
        });
    },
    InitList: function(r, t, x, e) {
        Incident.BindNewButton(e);
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
    },
    BindNewButton: function(e) {
        $(".new-incident-log", e).off("click");
        $(".new-incident-log", e).on("click", function() {
            var service = $("[name=ServiceId]", e).val();
            Incident[Acore.AppService(service)].New();
            return false;
        });
    },
    Delete: function(patientId, id, serviceId) {
        var service = Acore.AppService(serviceId);
        if (service) {
            var area = window[service].Area;
            Acore.Confirm({
                Message: "Are you sure you want to delete this Incident Log?",
                Yes: function() {
                    U.PostUrl(area + "/Incident/Delete", { Id: id, patientId: patientId }, function(result) {
                        if (result.isSuccessful) Incident.Refresh();
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    });
                }
            });
        }
    },
    OnDataBound: function(e) {
        U.OnTGridDataBound(e);
        var grid = $(e.target).data('tGrid');
        if (grid) {
            var datas = grid.data;
            if (datas.length > 0) {
                var data = datas[0];
                if (data != null) {

                    if (data.IsUserCanEdit || data.IsUserCanDelete) {
                        var width = 0;
                        if (data.IsUserCanEdit) width += 50;
                        if (data.IsUserCanDelete) width += 50;
                        grid.showColumn(6);
                        $("td:eq(6),th:eq(6),col:eq(6)", $(e.target).find("tbody tr,colgroup")).css({ width: width });
                    }
                    else { grid.hideColumn(6); }

                    if (data.IsUserCanPrint) { grid.showColumn(5); } else { grid.hideColumn(5); }
                }
            }
            else {
                grid.hideColumn(5);
                grid.hideColumn(6);
            }
        }
    },
    OnRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.IsUserCanEdit || dataItem.IsUserCanDelete) {
            var action = "";
            var isEdit = false;
            if (dataItem.IsUserCanEdit) {
                action = "<a class=\"link\" onclick=\"Incident.Edit('" + dataItem.Id + "','" + dataItem.Service + "');\">Edit</a>";
                isEdit = true;
            }
            if (dataItem.IsUserCanDelete) {
                action += "<a class=\"link\" onclick=\"Incident.Delete('" + dataItem.PatientId + "','" + dataItem.Id + "','" + dataItem.Service + "');\" >Delete</a>";
            }
            e.row.cells[6].innerHTML = action;
        }
        if (dataItem.IsUserCanPrint) {
            e.row.cells[5].innerHTML = "<a onclick=\"Incident.Print('" + dataItem.PatientId + "','" + dataItem.Id + "','" + dataItem.Service + "');\"><span class='img icon16 print'></span></a>";
        }
    },
    Edit: function(id, serviceId) {
        var service = Acore.AppService(serviceId);
        Incident[service].Edit(id)
    },
    Print: function(patientId, id, serviceId) {
        var service = Acore.AppService(serviceId);
        var area = window[service].Area;
        Acore.OpenPrintView({ Url: area + "/Incident/PrintPreview/" + patientId + "/" + id, PdfUrl: area + "/Incident/Pdf", PdfData: { patientId: patientId, EventId: id} });
    },
    Refresh: function() { U.RebindTGrid($('#IncidentList_Grid')); },
    Process: function(button, patientId, eventId, prefix, reason) {
        var input = { patientId: patientId, eventId: eventId },
            isReturning = button == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/Incident/" + button, input, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.All(result);
                if (result.IsIncidentRefresh) Incident.Refresh();
            } else if (isReturning) Acore.ReturnReasonFailed();
        });
    }
}