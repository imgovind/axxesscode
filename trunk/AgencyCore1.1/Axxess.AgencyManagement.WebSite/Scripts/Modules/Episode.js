﻿var Episode = {
    InitEdit: function (r, t, x, e) {
        //$("[name=StartDate]", e).on("blur", function () { setTimeout(function () { Episode.StartDateChange('Edit') }, 200) });
        Episode.InitShared(e);
    },
    InitNew: function (r, t, x, e) {
        // $("[name=StartDate]", e).on("blur", function () { setTimeout(function () { Episode.StartDateChange('New') }, 200) });
        Episode.InitShared(e);
    },
    InitTopMenuNew: function (prefix, e) {
        $('[name=PatientId]', e).on("change", function () {
            var patientId = $(this).val();
            $('#topMenuNewEpisodeContent', e).Load(prefix + '/NewPatientEpisodeContent', { patientId: patientId }, function (responseText, textStatus, XMLHttpRequest) {
                if (textStatus == 'error') {
                    $('#topMenuNewEpisodeContent', e).html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere w" +
                    "as an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fur" +
                    "ther assistance.%3C/div%3E"));
                }
                else {
                    var element = $(e);
                    $("[name=StartDate]", element).on("ValueSet", function (event) { Episode.WarningMessage(element); });
                    $("[name=StartDate]", element).on("change", function () { Episode.StartDateChange(element); })
                }
            });
        });
        Episode.InitShared(e);

    },
    InitShared: function (e) {
        $("[name=StartDate]", e).on("change", function () { Episode.StartDateChange(e); })
        $("[name=StartDate]", e).on("ValueSet", function (event) { Episode.WarningMessage(e); });
        var form = $(".mainform", e);
        form.Validate({ Success: function (result) {
            Refresh.CenterActivity(result);
            if (result.IsDataCentersRefresh) {
                var service = Acore.AppService(result.ServiceId);
                var area = window[service].Area;
                if (area) { Episode.ReLoadInactives(area, result.PatientId); }
            }
        }
        });
        $("a.save").on("click", function () { Episode.WarnEpisodeGap($(e), form, $(this)); })
    },
    New: function (prefix, Id) {
        Acore.Modal({
            Name: "New Episode",
            Url: prefix + "/New",
            Input: { patientId: Id },
            OnLoad: Episode.InitNew,
            Width: 900
        })
    },
    Edit: function (prefix, episodeId, patientId, action) {
        Acore.Modal({
            Name: "Edit Episode",
            Url: prefix + "/Edit",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: Episode.InitEdit,
            Width: 900
        })
    },
    Activate: function (prefix, episodeId, patientId) {
        U.PostUrl(prefix + '/Activate', { 'episodeId': episodeId, 'patientId': patientId }, function (data) {
            if (data.isSuccessful) {
                Refresh.CenterQAMySchedule(patientId);
                Episode.ReLoadInactives(prefix, patientId);
            } else U.Growl(data.errorMessage, "error");
        });
    },
    LoadInactives: function (prefix, patientId) { Acore.Open("inactiveepisode", prefix + '/Inactive', function () { }, { patientId: patientId }); },
    ReLoadInactives: function (prefix, patientId) {
        $("#InactiveEpisodesContent ol").addClass('loading');
        $("#InactiveEpisodesContent").load(prefix + '/InactiveGrid', { patientId: patientId }, function (responseText, textStatus) {
            $("#InactiveEpisodesContent ol").removeClass("loading");
            if (textStatus == 'error') $('#InactiveEpisodesContent').html(U.AjaxError);
        });
    },
    LoadLog: function (prefix, episodeId, patientId) { Acore.Open("episodelogs", prefix + '/EpisodeLogs', function () { }, { episodeId: episodeId, patientId: patientId }); },
    StartDateChange: function (e) {
        var startDate = $("[name=StartDate]", e), endDate = $("[name=EndDate]", e);
        if (startDate.val()) {
            var newStartDate = new Date(startDate.val());
            var newEndDate = new Date(startDate.val());
            newEndDate.setDate(newStartDate.getDate() + 59);
            var month = newEndDate.getMonth() + 1, day = newEndDate.getDate(), year = newEndDate.getFullYear();
            endDate.val(month + "/" + day + "/" + year);
        }
    },
    InitPatientChangeDropDown: function (e, serviceName) {
        var patient = $("select[name=PatientId]", e);
        //if (patient.val() != U.GuidEmpty) Episode[prefix].LoadDropDown($('.episode-dropdown', e), patient);
        patient.on("change", function () {
            Episode[serviceName].LoadDropDown($('select[name=EpisodeId]', e), $(this));
            if ($(".primary-diagnosis", e).length) $(".primary-diagnosis", e).text('');
            if ($(".secondary-diagnosis", e).length) $(".secondary-diagnosis", e).text('');
            if ($("#ICD9M", e).length) $("#ICD9M", e).remove();
            if ($("#ICD9M1", e).length) $("#ICD9M1", e).remove();
        });
    },
    LoadDropDown: function (urlPrefix, dropDown, control, title) {
        dropDown.trigger("ProcessingStart");
        U.PostUrl(urlPrefix + "/RangeList", { patientId: $(control).val() }, function (data) {
            var s = typeof dropDown == "string" ? $("select#" + dropDown) : dropDown;
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select " + title + " --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function (index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Range, itemData.Id, false, false); });
            dropDown.trigger("ProcessingComplete");
        });
    },

    WarnEpisodeGap: function (e, Form, Button) {
        if (Form.valid()) {
            var result = Episode.CheckEpisodeStartDate(e);
            if (result.IsValid) {
                if (result.NumberOfDays > 1) {
                    return Acore.Confirm({
                        Message: "There is a gap between the start date and the last episode&#8217;s end date. Are you sure you want to save?",
                        Yes: function () { Button.trigger("ProcessingStart"); Form.submit(); },
                        No: function () { return false; }
                    });
                }
                else {
                    Button.trigger("ProcessingStart"); 
                    return Form.submit();
                }
            }
            else {
                if (result.NumberOfDays <= 0) {
                    U.Growl("Error: This episode overlaps with other episode.", "error");
                    return false
                }
                else {
                    Button.trigger("ProcessingStart");
                    return Form.submit();
                }
            }
        }
    },
    WarningMessage: function (e) {
        var result = Episode.CheckEpisodeStartDate(e);
        if (result.IsValid) {
            if (result.NumberOfDays > 1) {
                U.Growl("WARNING: There is a gap between the start date and the last episode&#8217;s end date.", "warning");
            }
        }
        else {
            if (result.NumberOfDays <= 0) {
                U.Growl("Error: This episode overlaps with other episode.", "error");
            }
        }
    },
    CheckEpisodeStartDate: function (e) {
        var result = [];
        result.IsValid = true;
        result.NumberOfDays = 0;
        result.Message = "";
        var d1 = $("[name=StartDate]", e).val() || "";
        var d2 = $("[name=PreviousEndDate]", e).val() || "";
        if (d1 != "" && d2 != "") {
            var startDate = $.datepicker.parseDate("mm/dd/yy", d1);
            var previousEndDate = $.datepicker.parseDate("mm/dd/yy", d2);
            var days = U.NumberOfDaysInBetween(previousEndDate, startDate);
            if (days <= 0) {
                result.Message = "Error: This episode overlaps with other episode.";
                result.IsValid = false;
                result.NumberOfDays = days;
            }
            else if (days > 1) {
                result.Message = "WARNING: There is a gap between the start date and the last episode&#8217;s end date.";
                result.IsValid = true;
                result.NumberOfDays = days;
            }
        }
        return result;
    }

}
