﻿var PhysicianOrder = {
    InitNew: function(e, isAlone, title, prefix) {
        Episode.InitPatientChangeDropDown(e, prefix);
        PhysicianOrder.Init(e, isAlone, title, prefix);
    },
    Init: function(e, isAlone, title, prefix) {
        var form = $("form", e);
        U.InitForm(form, Refresh.All);
        if (!isAlone) {
            e.PrependToTitle(title);
            U.SetPrefix($(".window-content", e).parent(), prefix);
        }
    },
    InitNewOrderButton: function(e, prefix) {
        $(".new-order-button", e).on("click", function() {
            PhysicianOrder[prefix].New($("[name=PatientId]", e).val(), $("[name=EpisodeId]", e).val());
        });
    }
}
