﻿var WoundCare = {
    Init: function(r, t, x, e) {
        e.find("form").Validate();
        WoundCare.InputInit(e);
    },
    InputInit: function (e) {
        $(".WoundType", e).Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Statis Ulcer", "Arterial Ulcer"] });
        $(".DeviceType", e).Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
        $(".IVSiteDressingUnit", e).Autocomplete({ source: ["Normal Saline", "Heparin"] });
    },
    Load: function(patientId, eventId, area) { CommandFactory.Open({ Type: "woundcare", PatientId: patientId, Id: eventId }, area);}
}