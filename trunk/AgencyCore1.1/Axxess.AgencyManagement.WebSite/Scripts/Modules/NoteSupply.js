﻿var NoteSupply =
{
    Delete: function(id, eventId, patientId, episodeId) {
        U.Delete("Supply", "Note/DeleteNoteSupply", { EventId: eventId, PatientId: patientId, EpisodeId: episodeId, Id: id },
            function() {
                NoteSupply.RebindList();
            }, false);
    },
    InitEdit: function() {
        $("#editNoteSupplyForm").Validate({ Success: NoteSupply.RebindList });
        NoteSupply.OnSupplyEdit({ form: $("#editNoteSupplyForm") });
    },
    InitNew: function() {
        $("#newNoteSupplyForm").Validate({ Success: NoteSupply.RebindList });
        NoteSupply.OnSupplyEdit({ form: $("#newNoteSupplyForm") });
    },
    RebindList: function() {
        U.RebindTGrid($("#window_notessupplyworksheet .t-widget.t-grid"));
    },
    OnSupplyEdit: function(e) {
        $(e.form).find("input[name=Description]").AjaxAutocomplete({
            ExtraParameters: { 'limit': 50 },
            minLength: 1,
            SourceUrl: "Agency/SuppliesSearch",
            Format: function(json) { return json.Description },
            Select: function(json, input) {
                input.val(json.Description);
                $(e.form).find("input[name=Code]").val(json.Code);
                $(e.form).find("input[name=UnitCost]").val(json.UnitCost);
                $(e.form).find("input[name=UniqueIdentifier]").val(json.Id);
                $(e.form).find("input[name=RevenueCode]").val(json.RevenueCode);
            }
        });
    }
}