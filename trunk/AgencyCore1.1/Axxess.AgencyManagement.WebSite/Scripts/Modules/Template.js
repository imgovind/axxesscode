﻿var Template = {
    Delete: function(id) { U.DeleteTemplate("Template", id); },
    Edit: function(id) {
        Acore.Open("edittemplate", "Template/Edit", Template.InitEdit, { Id: id });
    },
    InitList: function(r, t, x, e) {
        $("a.new", e).on("click", function() { Acore.Open("newtemplate") });
        U.ListInitClientRebind(r, t, x, e);
    },
    InitEdit: function(r, t, x, e) {
        U.InitTemplate($("form.mainform", e), function() { Template.RebindList() }, "Template has been successfully updated.");
    },
    InitNew: function(r, t, x, e) {
        U.InitTemplate($("form.mainform", e), function() { Template.RebindList() }, "Template has been successfully added.");
    },
    RebindList: function() {
        U.RebindTGrid($("#List_Template"));
    },
    OnChangeInit: function() {
        $("select").on("change", function() {
            var selectList = this;
            var textarea = $(this).attr("template");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/Template/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == "") {
                                $(textarea).val(template.Text);
                                $(textarea).trigger("keyup");
                            } else {
                                $(textarea).val(existingText + "\n" + template.Text);
                                $(textarea).trigger("keyup");
                            }
                        }
                    })
                }
            }
        });
    },
    DropDownList: function(currentItem, form, callback) {
        U.PostUrl("/Agency/TemplateSelect", null, function(data) {
            var options = [];
            $.each(data, function(index, itemData) {
                options[index] = new Option(itemData.Text, itemData.Value, false, false);
            });
            if (typeof callback == "function") callback(currentItem, form, options);
        }, function() {
            U.Error("Unable to load templates, please try again later");
            $(".template-text select").removeClass("processing").hide();
            $(".template-text .button").show();
        })
    },
    LoadFormTemplates: function(currentClickControl, contaningForm) {
        var listElement = $(currentClickControl).closest(".template-text").find("select");
        $(".template-text .button").hide();
        $(".template-text select").show().addClass("processing");
        var callBack = function(currentSelect, form, options) {
            currentSelect.empty();
            currentSelect.append(options);
            var clonedOptions = $("option", currentSelect).clone();
            $(".template-text select", form).not(currentSelect).each(function(index, element) {
                var s = $(element);
                s.empty();
                s.append(clonedOptions);
                s.show();
                clonedOptions = $("option", s).clone();
                s.closest(".template-text").find("span").hide();
            });
            $(".template-text select").removeClass("processing");
        };
        Template.DropDownList(listElement, contaningForm, callBack);
    }
}