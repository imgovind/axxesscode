﻿if (typeof Insurance == "undefined") var Insurance = new Object();
$.extend(Insurance, {
    Rate: {
        New: function(insuranceId) {
            Acore.Modal({
                Name: "Visit Bill Info.",
                Url: "Agency/NewBillData",
                Input: { InsuranceId: insuranceId },
                OnLoad: Insurance.Rate.InitNew,
                Width: "620px",
                WindowFrame: false
            })
        },
        Edit: function(insuranceId, Id) {
            Acore.Modal({
                Name: "Visit Bill Info.",
                Url: "Agency/EditBillData",
                Input: { InsuranceId: insuranceId, Id: Id },
                OnLoad: Insurance.Rate.InitEdit,
                Width: "620px",
                WindowFrame: false
            })
        },
        InitNew: function(r, t, x, e) {
            Rate.InitCommon(r, t, x, e);
            $("form.mainform", e).Validate({ Success: Insurance.Rate.RebindList });
        },
        InitEdit: function(r, t, x, e) {
            Rate.InitCommon(r, t, x, e);
            $("form.mainform", e).Validate({ Success: Insurance.Rate.RebindList });
        },
        RebindList: function() {
            if (Acore.Windows[Insurance.AcoreEditId].IsOpen) {
                var e = $("#window_" + Insurance.AcoreEditId + "_content");
                if (e) {
                    var grid = $(".insurance-bill-rates", e).data('tGrid');
                    if (grid != null) { grid.rebind(); }
                }
            }
        },
        Delete: function(insuranceId, Id) {
            U.Delete("Visit rate", "/Agency/DeleteBillData", { InsuranceId: insuranceId, Id: Id }, Insurance.Rate.RebindList);
        }
    }

});