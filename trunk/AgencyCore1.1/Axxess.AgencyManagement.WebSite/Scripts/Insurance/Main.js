﻿var Insurance = {
    AcoreEditId: "editinsurance",
    AcoreListId: "listinsurances",
    Delete: function(Id) { U.DeleteTemplate("Insurance", Id); },
    New: function() {
        Acore.Open("newinsurance", "Insurance/New", Insurance.InitNew);
    },
    Edit: function(Id) {
        Acore.Open("editinsurance", "Insurance/Edit", Insurance.InitEdit, { Id: Id });
    },
    InitShared: function(r, t, x, e) {
        U.ShowIfSelectEquals($("[name=PayorType]", e), "10", $("div.usingpatientratesrow", e));
        U.HideIfChecked($("input[name=IsAxxessTheBiller]", e), $("div.clearinghouseediInformation", e));
        $("[name=InvoiceType]", e).on("change", function() {
            var selectedValue = $(this).val();
            if (selectedValue == 1) {
                $("div.ub04locators", e).removeClass("hidden");
                $("div.hcfalocators", e).addClass("hidden");
            } else if (selectedValue == 2) {
                $("div.ub04locators", e).addClass("hidden");
                $("div.hcfalocators", e).removeClass("hidden");
            } else {
                $("div.ub04locators", e).addClass("hidden");
                $("div.hcfalocators", e).addClass("hidden");
            }
        });
        $("form.mainform", e).Validate({ Success: Insurance.RebindList });
    },
    InitList: function(r, t, x, e) {
        U.ListInitClientRebind(r, t, x, e);

    },
    OnRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem) {

            $("td a.edit", e.row).on("click", function() {
                if ($(this).hasClass("tradtionalmedicare")) {
                    Agency.LoadVisitRate();
                }
                else {
                    Insurance.Edit(dataItem.Id);
                }
            });


            $("td a.delete", e.row).on("click", function() { Insurance.Delete(dataItem.Id);});
        }
    },
    InitEdit: function(r, t, x, e) {
        U.DoIfChecked(true, $("input[name=HasContractWithAgency]", e),
            function() {
                $(".insurance-bill-rates", e).data('tGrid').showColumn("ExpectedRate");
                $(".insurance-bill-rates th.rate-header a", e).text("Billed Rate");
            },
            function() {
                $(".insurance-bill-rates", e).data('tGrid').hideColumn("ExpectedRate");
                $(".insurance-bill-rates th.rate-header a", e).text("Rate");
            }
        );
        Insurance.InitShared(r, t, x, e);
    },
    InitNew: function(r, t, x, e) {
        Insurance.InitShared(r, t, x, e);
    },
    RebindList: function() {
        if (Acore.Windows[Insurance.AcoreListId].IsOpen) {
            var e = $("#window_" + Insurance.AcoreListId + "_content");
            if (e) {
                U.RebindTGrid($('#List_AgencyInsurance', e));
            }
        }
    },
    DropDownHelper: function(control, title, isZeroIndexPrefilled, action, data) {
        var s = $(control);
        s.children('option').remove();
        if (isZeroIndexPrefilled)
        { s.get(0).options[0] = new Option(title, "0", false, false); }
        $.each(data, function(index, itemData)
        { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, itemData.Selected) });
        if (action != null && action != undefined && typeof (action) == "function")
        { action(); }
    },
    VisitInfoReplace: function(Id, replacedId) {
        if (replacedId <= 0) {
            U.Growl('Select the insurance.', "error");
            return;
        }
        else Acore.Confirm({
            Message: "Are you sure you want to replace/overwrite visit information?",
            Yes: function() {
                U.PostUrl("Agency/ReplaceInsuranceVisit", { Id: Id, replacedId: replacedId }, function(result) {
                    if (result.isSuccessful) {
                        Insurance.Rate.RebindList();
                    }
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                })
            }
        })
    }
}