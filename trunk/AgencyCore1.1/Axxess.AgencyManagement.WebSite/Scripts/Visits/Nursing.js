﻿$.extend(Visit, {
    PediatricAssessment: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.PediatricAssessment.InitContent);
        },
        InitContent: function(type, e) {
            var prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "ProsthesisLevel", e), "Other", $(prefix + "ProsthesisOther", e));
            U.ShowIfSelectEquals($(prefix + "GrabbarsLevel", e), "Other", $(prefix + "GrabbarsOther", e));
            U.ShowIfSelectEquals($(prefix + "HospitalBedLevel", e), "Other", $(prefix + "HospitalBedOther", e));

        }
    },
    DischargeSummary: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "NotificationDate", e), "3", $(prefix + "NotificationDateOther", e));
            U.ShowIfSelectEquals($(prefix + "ReasonForDC", e), "9", $(prefix + "ReasonForDCOther", e));
            Visit.Init(e);
        }
    },
    SixtyDaySummary: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "NotificationDate", e), "3", $(prefix + "NotificationDateOther", e));
            U.ShowIfChecked($(prefix + "HomeboundStatusOtherCheck", e), $(prefix + "HomeboundStatusOther", e));
            U.ShowIfChecked($(prefix + "ServiceProvidedOther", e), $(prefix + "ServiceProvidedOtherValue", e));
            U.ShowIfChecked($(prefix + "RecommendedServiceOther", e), $(prefix + "RecommendedServiceOtherValue", e));
            Visit.Init(e);
        },
        List: function(patientId) {
            Acore.Open("patientsixtydaysummary", { patientId: patientId });
        },
        OnRowDataBound: function(e) {
            var printLink = $(e.row).find(".print");
            if (printLink.length) {
                printLink.click(function() {
                    var result = CommandFactory.Print(printLink, { PatientId: e.dataItem.PatientId, Id: e.dataItem.EventId }, "HomeHealth");
                    if (result) Acore.OpenPrintView(result);
                });
            }
        }
    }
});