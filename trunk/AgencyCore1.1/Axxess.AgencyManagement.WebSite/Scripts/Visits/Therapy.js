﻿$.extend(Visit, {
    PTReassessment: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.PTReassessment.InitContent);
        },
        InitContent: function(type, e) {
            var prefix = "#" + type + "_";
            U.HideIfChecked($(prefix + "GenericIsWCMobilityApplied1", e), $(prefix + "WCMobilityContainer", e));
        }
    },
    PTVisit: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.PTVisit.InitContent);
        },
        InitContent: function(type, e) {
            var prefix = "#" + type + "_";
            var version = $("[name=Version]", e.closest(".window"));
            if (version.length && version.val() > 1) {
                U.HideIfChecked($(prefix + "IsObjectiveApply", e), $(prefix + "ObjectiveContainer", e));
                U.HideIfChecked($(prefix + "IsBedApply", e), $(prefix + "BedContainer", e));
                U.HideIfChecked($(prefix + "IsTransferApply", e), $(prefix + "TransferContainer", e));
                U.HideIfChecked($(prefix + "IsGaitApply", e), $(prefix + "GaitContainer", e));
                U.HideIfChecked($(prefix + "IsTeachingApply", e), $(prefix + "TeachingContainer", e));
                U.HideIfChecked($(prefix + "IsPainApply", e), $(prefix + "PainContainer", e));
                U.HideIfChecked($(prefix + "IsPlanApply", e), $(prefix + "PlanContainer", e));
                U.HideIfChecked($(prefix + "IsAssessmentApply", e), $(prefix + "AssessmentContainer", e));
                U.HideIfChecked($(prefix + "IsNarrativeApply", e), $(prefix + "NarrativeContainer", e));
                U.HideIfChecked($(prefix + "IsProgressApply", e), $(prefix + "ProgressContainer", e));
                U.HideIfChecked($(prefix + "IsTreatmentApply", e), $(prefix + "TreatmentContainer", e));
            }
        }
    },
    OTVisit: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.OTVisit.InitContent);
        },
        InitContent: function(type, e) {
        }
    },
    STEvaluation: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.STEvaluation.InitContent);

        },
        InitContent: function(type, e) {
            var prefix = "#" + type + "_";
            //U.ShowIfChecked($(prefix + "GenericHomeboundReason9", e), $(prefix + "GenericHomeboundReasonOther", e));
            //U.ShowIfRadioEquals(prefix + "_GenericOrdersForEvaluationOnly", "0", $(prefix + "GenericIfNoOrdersAreSpan", e));
            //U.ShowIfRadioEquals(prefix + "_GenericIsSSE", "1", $(prefix + "GenericGenericSSESpecifySpan", e));
            //U.ShowIfRadioEquals(prefix + "_GenericIsVideoFluoroscopy", "1", $(prefix + "GenericVideoFluoroscopySpecify", e));

            //            U.ShowIfChecked($(prefix + "GenericLiquids2", e), $(prefix + "GenericLiquidsThick", e));
            //            U.ShowIfChecked($(prefix + "GenericLiquids3", e), $(prefix + "GenericLiquidsOther", e));
            //            U.ShowIfChecked($(Prefix + "GenericReferralFor4", e), $(prefix + "GenericReferralForOther", e));
            //            U.ShowIfChecked($(prefix + "GenericDischargeDiscussedWith4", e), $(prefix + "GenericDischargeDiscussedWithOther", e));
            //            U.ShowIfChecked($(prefix + "GenericCareCoordination7", e), $(prefix + "GenericCareCoordinationOther", e));
        }
    },
    STReassessment: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.STReassessment.InitContent);
        },
        InitContent: function(type, e) {
            var prefix = "#" + type + "_";
            //U.ShowIfChecked($(prefix + "GenericHomeboundReason9", e), $(prefix + "GenericHomeboundReasonOther", e));
            //U.ShowIfRadioEquals(prefix + "_GenericOrdersForEvaluationOnly", "0", $(prefix + "GenericIfNoOrdersAreSpan", e));
            //U.ShowIfRadioEquals(prefix + "_GenericIsSSE", "1", $(prefix + "GenericGenericSSESpecifySpan", e));
            //U.ShowIfRadioEquals(prefix + "_GenericIsVideoFluoroscopy", "1", $(prefix + "GenericVideoFluoroscopySpecify", e));
            //            U.ShowIfChecked($(prefix + "GenericLiquids2", e), $(prefix + "GenericLiquidsThick", e));
            //            U.ShowIfChecked($(prefix + "GenericLiquids3", e), $(prefix + "GenericLiquidsOther", e));
            //            U.ShowIfChecked($(prefix + "GenericReferralFor4", e), $(prefix + "GenericReferralForOther", e));
            //            U.ShowIfChecked($(prefix + "GenericDischargeDiscussedWith4", e), $(prefix + "GenericDischargeDiscussedWithOther", e));
            //            U.ShowIfChecked($(prefix + "GenericCareCoordination7", e), $(prefix + "GenericCareCoordinationOther", e));
        }
    },
    STVisit: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.STVisit.InitContent);
        },
        InitContent: function() {
            U.HideOptions();
        }
    },
    TherapySupervisoryVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val();
            $("#window_" + type + " form label.error", e).css({ 'position': 'relative', 'float': 'left' });
            Visit.Init(e);
        }
    },
    TherapyDischargeSummary: {
        Init: function(r, t, x, e) {
            Visit.Init(e, Visit.TherapyDischargeSummary.InitContent);
        },
        InitContent: function(type, e) {
            var prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "NotificationDate", e), "3", $(prefix + "NotificationDateOther", e));
            U.ShowIfSelectEquals($(prefix + "ReasonForDC", e), "9", $(prefix + "ReasonForDCOther", e));
            //			U.ShowIfChecked($(prefix + "ServiceProvidedOther", e), $(prefix + "ServiceProvidedOtherValue", e));
            //			U.ShowIfChecked($(prefix + "DischargeInstructionsGivenTo4", e), $(prefix + "DischargeInstructionsGivenToOther", e));
        }
    }
});