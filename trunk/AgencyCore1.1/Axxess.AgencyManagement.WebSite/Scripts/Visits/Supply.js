﻿$.extend(Visit, {
    Supply: {
        AcoreId: "notessupplyworksheet",
        Init: function(r, t, x, e) {
            var form = $("form.mainform", e).Validate({ Success: Visit.Supply.RebindList });
            Supply.Search(form);
        },
        RebindList: function() {
            var e = $("#window_" + Visit.Supply.AcoreId + "_content");
            $('.note-supply-grid', e).data('tGrid').rebind();
        },
        LoadWorkSheet: function(patientId, eventId, prefix) {
            Acore.Open("notessupplyworksheet", prefix + '/Note/SupplyWorksheet', { patientId: patientId, Id: eventId });
        },
        InitList: function(r, t, x, e) {
            $(".add-supply", e).on("click", function() { Visit.Supply.NewNoteSupplyModal($(this).attr("url"), $("input.input", e).serializeArray()); });
//            $(".t-grid", e).css({ 'top': '60px !important'});
        },
        NewNoteSupplyModal: function(url, input) {
            Acore.Modal({
                Name: "New Supply",
                Url: url,
                Input: input,
                OnLoad: Visit.Supply.Init,
                Width: 520,
                WindowFrame: false
            });
        },
        Edit: function(inputs) {
            if (inputs) {
                var area = window[Acore.AppService(inputs["ServiceId"])].Area;
                if (area != undefined) {
                    Acore.Modal({
                        Name: "Edit Supply",
                        Url: area + "/Note/EditNoteSupply",
                        Input: inputs,
                        OnLoad: Visit.Supply.Init,
                        Width: 520,
                        WindowFrame: false
                    });
                }
            }
        },
        Delete: function(inputs) {
            if (inputs) {
                var area = window[Acore.AppService(inputs["ServiceId"])].Area;
                if (area != undefined) U.Delete("Supply", area + "/Note/DeleteNoteSupply", inputs, Visit.Supply.RebindList, false);
            }
        },
        EditDeleteBind: function(wrapper, grid) {
            var inputs = U.JsonInput($("input.input", wrapper).serializeArray());
            $("tbody tr td.action a.edit", grid).on("click", function() {
                var id = $(this).closest("tr").find("td.uid").text();
                inputs["Id"] = id;
                Visit.Supply.Edit(inputs);
            });
            $("tbody tr td.action a.delete", grid).on("click", function() {
                var id = $(this).closest("tr").find("td.uid").text();
                inputs["Id"] = id;
                Visit.Supply.Delete(inputs);
            });
        },
        OnDataBound: function(e) {
            U.OnTGridDataBound(e);
            Oasis.Supply.EditDeleteBind($(e.target).closest(".wrapper"), e.target);
        }
    }
});