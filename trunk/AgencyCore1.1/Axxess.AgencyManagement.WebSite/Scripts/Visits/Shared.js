﻿var Visit = {
    AfterSubmit: function (result) {
        if (result.isSuccessful) Refresh.CenterQAMySchedule(result);
    },
    CheckTimeInOut: function (form, type, showMessage) {
        if ($("#" + type + "_TimeIn", form).length != 0) {
            var tempTimeIn = $("#" + type + "_TimeIn", form).val();
            var tempTimeOut = $("#" + type + "_TimeOut", form).val();
            var timeIn = U.ConvertToTime(tempTimeIn);
            var timeOut = U.ConvertToTime(tempTimeOut);
            return U.CheckTimeInOut(timeIn, timeOut, showMessage || false);
        } else return true;
    },
    PreprocessForm: function (form) {
        var type = $(form).find("[name='Type']").val();
        return Visit.CheckTimeInOut($(form), type);
    },
    SharedInit: function(r, t, x, e) { Visit.Init(e); },
    Init: function (e, initFunction, contentUrl) {
        var form = $(e).find("form");
        var type = $("input[name=Type]", e).val(),
            input = { PatientId: $("input[name=PatientId]", form).val(), EventId: $("input[name=EventId]", form).val() },
            area = Acore.Windows[type].Area;
        Visit.InitContent(type, form, initFunction);
        form.Validate({ Success: Visit.AfterSubmit, PreprocessForm: Visit.PreprocessForm });
        var prefix = "#" + type + "_";
        if (contentUrl == undefined) contentUrl = Acore.Windows[type].Area + "/Note/Content";
        if ($(prefix + "TimeIn").length != 0) Visit.CheckTimeInOut(form, type, true);
        Visit.InitReturn(e, form, input, area);
        $(".load-disciplinetask", form).on("click", function() { Schedule.Task.LoadTasks($(this), { LocationId: $("input[name=LocationId]", form).val(), DisciplineTask: $("[name=DisciplineTask]", form).val() }); });
        Visit.InitPreviousNotes(form, input, initFunction, contentUrl);
    },
    InitReturn: function (e, form, input, area) {
        var buttons = $(".buttons.send-commands", form),
            returnForSignature = $("[name=ReturnForSignature]", form).on("change", function() {
                buttons.find("a.save,a.complete").parent().toggleClass("disabled");
            }),
            acoreId = e.closest(".window").GetAcoreId();
        $("a[command=return]", form).off("click").on("click", function (evnt) {
            if (!Acore.Windows[Acore.ReturnReasonId]) Acore.Windows[acoreId].Close = Acore.CloseReturnReason;
            U.IncludeCommandInForm(form, this, evnt);
            $(".complete-required", form).removeClass("required error");
            if (form.valid()) {
                buttons.hide();
                Acore.ReturnReason({
                    EventId: input.EventId,
                    PatientId: input.PatientId,
                    UrlPrefix: area,
                    Comment: returnForSignature.is(":checked") ? "This document has been returned for the clinicians signature." : "",
                    ReturnFunction: function (reason) {
                        form.append("<input type='hidden' value='" + reason + "' name='ReturnReason'/>");
                        form.off("SubmitSuccess").on("SubmitSuccess", function() {
                            Acore.CloseReturnReason();
                            Refresh.All(result);
                            form.closest(".window").Close();
                        });
                        form.off("SubmitFailure").on("SubmitFailure", function() {
                            $("[name=ReturnReason]", form).remove();
                            Acore.ReturnReasonFailed();
                        });
                        form.submit();
                    },
                    CloseFunction: function() {
                        buttons.show();
                        Acore.Windows[acoreId].Close = undefined;
                    }
                });
            }
        });
    },
    InitLoadDisciplineTasks: function (form) {
        $(".load-disciplinetask", form).on("click", function () {
            var callBack = function (currentSelect, options) {
                if (options.length > 0) {
                    var parentElement = currentSelect.closest("div"),
                        taskElement = parentElement.find("[name=DisciplineTask]"),
                        attributes = { id: taskElement.attr("id"), name: taskElement.attr("name"), "class": "required not-zero" };
                    $("[name=DisciplineTask]", parentElement).remove();
                    parentElement.append($("<select/>", attributes).append(options)).find(".load-disciplinetask").closest("span").hide();
                } else U.Growl("Unable to load the tasks.  Try again.", "error");
            };
            Visit.LoadTasks($(this), { LocationId: $("input[name=LocationId]", form).val(), DisciplineTask: $("[name=DisciplineTask]", form).val() }, callBack);
        });
    },
    InitPreviousNotes: function (form, baseInput, initFunction, contentUrl) {
        $(".load-previousnotes", form).on("click", function () {
            var previousNoteInput = $.extend({}, baseInput, { DisciplineTask: $("[name=DisciplineTask]", form).val(), EventDate: $("input[name=EventDate]", form).val() });
            Visit.PreviousNoteList(area, previousNoteInput, $(this).closest("div").find(".previousnotes-list"),
                function (currentSelect, options) {
                    currentSelect.empty();
                    currentSelect.append(options);
                    currentSelect.show();
                    currentSelect.closest("div").find(".load-previousnotes").closest("span").hide();
                });
        });
        $("[name=PreviousNotes]", form).on("change", function () {
            var $this = $(this);
            var noteId = $this.val();
            if (noteId && noteId != U.GuidEmpty) {
                Acore.Confirm({
                    Message: "Are you sure you want to load from the previous note?<br/><em>Note:If you load the previous note it will over write any data in this document.</em>",
                    Width: 500,
                    Yes: function () {
                        $(prefix + "ContentId").Load(contentUrl, $.extend({}, baseInput, { previousNoteId: noteId, type: type }), function (r, t, x, e) { Visit.InitContent(type, e, initFunction); });
                    },
                    No: function () { $this.val(U.GuidEmpty); }
                });
            }
        });
    },
   
    //initMoreContent is a function for initializing more specific inputs in the visit
    InitContent: function (type, element, initMoreContent) {
        var vitalSignsNewVersion = $("#VitalSign_Content", element);
        if (vitalSignsNewVersion.length) VitalSign.Init(vitalSignsNewVersion, true);
        if (typeof initMoreContent == "function") initMoreContent(type, element);
        $("a.care-plan-print", element).on("click", function () {
            var result = CommandFactory.Print(this, { PatientId: $("input[name=PatientId]", element).val(), Id: $(this).attr("careplanid") }, $("input[name=Service]", element).val());
            if (result) Acore.OpenPrintView(result);
        });
    },
    ProcessNote: function (button, patientId, eventId, prefix, reason) {
        var input = { patientId: patientId, eventId: eventId },
            isReturning = button == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/Note/" + button, input, function (result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.All(result);
            } else if (isReturning) Acore.ReturnReasonFailed();
        });
    },
    PreviousNoteList: function (prefix, input, currentItem, callback) {
        U.PostUrl(prefix + "/Note/Previous", input, function (data) {
            var options = [];
            $.each(data, function(index, itemData) { options[index] = new Option(itemData.Text, itemData.Value, false, false); });
            if (typeof callback == "function") callback(currentItem, options);
        });
    },
    Load: function(Options) { if (Options && Options.Type) { Acore.Open(Options.Type, Options.Data, Options.Url); } },
    PhlebotomyLab: function(selector) { $(selector).Autocomplete({ source: ["CBC", "Chem 7", "BMP", "PT/INR", "PT", "Chem 8", "K+", "Urinalysis", "TSH", "CBC W/ diff", "Hemoglobin A1c", "Lipid Panel", "Comp Metabolic Panel  (CMP 14)", "TSH", "ALT/SGOT", "ALT/SGBOT", "Iron (Fe)"] }) },
    GetCarePlan: function (patientId, eventId, button, area) {
        $.fileDownload(area + "/Note/GetCarePlan", {
            httpMethod: "POST",
            data: { patientId: patientId, eventId: eventId },
            successCallback: function () {
                if (button && button.length) button.trigger("ProcessingComplete");
            },
            failCallback: function (responseHtml) {
                if (button && button.length) button.trigger("ProcessingComplete");
                try {
                    U.Growl(JSON.parse(responseHtml.replace("<pre>", "").replace("</pre>", "")).errorMessage, "error");
                } catch (err) {
                    U.Growl("A problem occured while downloading the care plan.", "error");
                }
            }
        });
    }
};