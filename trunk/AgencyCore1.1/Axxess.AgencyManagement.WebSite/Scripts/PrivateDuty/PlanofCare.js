﻿if (typeof PlanOfCare == "undefined") var PlanOfCare = new Object();
$.extend(PlanOfCare, {
    PrivateDuty: {
        Edit: function(patientId, eventId) {
            Acore.Open("HCFA485", { patientId: patientId, eventId: eventId });
        },
        SaveMedications: function(patientId) {
            PlanOfCare.SaveMedications(patientId, "PrivateDuty");
        }
    }
});