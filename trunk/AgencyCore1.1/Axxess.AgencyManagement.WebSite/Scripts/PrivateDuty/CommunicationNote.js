﻿if (typeof CommunicationNote == "undefined") var CommunicationNote = new Object();
$.extend(CommunicationNote, {
    PrivateDuty: {
        New: function(patientId) {
            if (patientId) {
                Acore.Open("PrivateDutynewcommnote", { patientId: patientId });
            }
            else {
                Acore.Open("PrivateDutynewcommnote");
            }
        },
        Delete: function(patientId, id) { CommunicationNote.Delete(patientId, id, 'PrivateDuty/CommunicationNote/Delete', CommunicationNote.Refresh); },
        LoadList: function(patientId) { Acore.Open("communicationnoteslist", 'PrivateDuty/CommunicationNote/List', { patientId: patientId }); }
    }
});