﻿if (typeof MissedVisit == "undefined") var MissedVisit = new Object();
$.extend(MissedVisit, {
    PrivateDuty: {
        Open: function(missedVisitId) {
            MissedVisit.Open(missedVisitId, PrivateDuty.Area);
        }, 
        New: function(patientId, eventId) {
            MissedVisit.New(patientId, eventId, PrivateDuty.Area);
        },
        Edit: function(eventId) {
            MissedVisit.Edit(eventId, PrivateDuty.Area);
        },
        Restore: function(patientId, eventId) {
            var input = { patientId: patientId, eventId: eventId };
            MissedVisit.Restore("PrivateDuty/MissedVisit/Restore", input, function(data) {
                MissedVisit.Refresh();
                Refresh.All(data);
            });
        }
    }
});