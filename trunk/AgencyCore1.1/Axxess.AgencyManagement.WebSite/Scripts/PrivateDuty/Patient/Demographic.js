﻿if (typeof Patient.Demographic == "undefined") $.extend(Patient, { Demographic: new Object() });
$.extend(Patient.Demographic, {
    PrivateDuty: {
        Prefix: "PrivateDuty",
        AdmitPatient: function(id, type) { Patient.Demographic.AdmitPatient(id, type, Patient.Demographic.PrivateDuty.Prefix); }
    }
});