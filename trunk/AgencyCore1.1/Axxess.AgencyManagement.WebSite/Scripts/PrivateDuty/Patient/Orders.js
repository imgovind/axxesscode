﻿if (typeof Patient.Orders == "undefined") $.extend(Patient, { Orders: new Object() });
$.extend(Patient.Orders, {
    PrivateDuty: {
        DeleteOrder: function(id, patientId, episodeId) {
            Patient.Orders.DeleteOrder(id, patientId, episodeId, "PrivateDuty/Order/Delete", Refresh.Orders);
        }
    }
});