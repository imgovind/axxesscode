﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    PrivateDuty: {
        Activities: {
            OnDataBinding: function(e) { U.OnDataBindingStopAction(e, Patient.Charts.Activities.RebindInput(), "PrivateDuty/PatientProfile/ActivitySort"); },
            OnDataBound: function(e) { Patient.Charts.Activities.OnDataBound(e); },
            OnRowDataBound: function(e) {
                Patient.Charts.Activities.OnRowDataBound(e, "PrivateDuty");
            },
            Rebind: function(e) { Patient.Charts.Rebind(e, "PrivateDuty"); }
        }
    }
});
