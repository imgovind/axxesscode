﻿if (typeof Agency.PrintQueue == "undefined") $.extend(Agency, { PrintQueue: new Object() });
$.extend(Agency.PrintQueue, {
    PrivateDuty: {
        WindowId: "PDPrintQueue",
        Init: function(r, t, x, e) {
            PrivateDuty.RenameWindow(e);
            Agency.PrintQueue.Init(r, t, x, e);
        }
    }
});