﻿if (typeof PrivateDuty == "undefined") var PrivateDuty = new Object();
$.extend(PrivateDuty, {
    UserSchedule: {
        AcoreId: "PrivateDutyUserSchedule", // Acore Window ID for Private Duty User Schedule
        UserId: "",
        UserName: "",
        Open: function() {
            Acore.Open(PrivateDuty.UserSchedule.AcoreId);
        },
        Init: function(r, t, x, e) {
            PrivateDuty.UserSchedule.UserId = $(".user", e).attr("guid"), PrivateDuty.UserSchedule.UserName = $(".user", e).text();
            $(".user", e).remove();
            $(".print", e).on("click", function() {
                PrivateDuty.UserSchedule.Print(e);
                return false;
            });
            $(".export", e).on("click", function() {
                PrivateDuty.UserSchedule.Export(e);
                return false;
            });
            // Toggle List Views
            $(".show-list", e).on("click", function() {
                PrivateDuty.UserSchedule.ShowList(e);
                return false;
            });
            $(".hide-list", e).on("click", function() {
                PrivateDuty.UserSchedule.HideList(e);
                return false;
            });
            U.ActivityGridContextMenuInit($(".t-grid", e));
            PrivateDuty.UserSchedule.Refresh();
        },
        // External Loader
        Load: function() {
            if (Acore.Windows[PrivateDuty.UserSchedule.AcoreId].IsOpen) $("#window_" + PrivateDuty.UserSchedule.AcoreId).WinFocus();
            else Acore.Open(PrivateDuty.UserSchedule.AcoreId);
        },
       
        // Print Calendar
        Print: function(e) {
            U.GetAttachment("PrivateDuty/Schedule/Print", {
                patientId: U.GuidEmpty,
                userId: PrivateDuty.UserSchedule.UserId,
                startDate: $(".pd-calendar", e).PrivateDutyScheduler("getStartDate"),
                endDate: $(".pd-calendar", e).PrivateDutyScheduler("getEndDate")
            });
        },
        // Refresh Calendar Data
        Refresh: function(e) {
            if (!e) {
                e = $("#window_" + PrivateDuty.UserSchedule.AcoreId + "_content");
            }
            $(".pd-calendar", e).PrivateDutyScheduler({
                CurrentId: PrivateDuty.UserSchedule.UserId,
                CurrentName: PrivateDuty.UserSchedule.UserName,
                PatientSelection: false,
                Scheduler: $(".pd-calendar", e).hasClass("scheduler")
            });
        },
        RefreshCurrentView: function() {
            var e = $("#window_" + PrivateDuty.UserSchedule.AcoreId + "_content");
            if (e) { $(".pd-calendar ", e).PrivateDutyScheduler("refreshCurrentView");}
        },
        ShowList: function(e) {
            $(".hide-list,.show-list", e).toggle();
            $(".fc-list-tasks", e).css("visibility", "hidden");
            $(".list-collapsed", e).Animate({ top: "6em" });
            $(".schedule-list", e).css({ bottom: 0, top: "auto", height: "auto" }).Animate({ top: "6.3em", width: "100%" }, function() {
                $(".fc-content", e).hide();
            });
        },
        HideList: function(e) {
            $(".hide-list,.show-list", e).toggle();
            $(".fc-content", e).show();
            $(".fc-button", e).attr("style", "");
            var listToggler = $(".list-collapsed", e),
                scheduleList = $(".schedule-list", e),
                destinationTop = $(e).height();
            $(".pd-calendar ", e).PrivateDutyScheduler("rerenderEventsAsNew");
            scheduleList.Animate({ top: destinationTop }, function() {
                scheduleList.css({ top: "auto" });
            });
            listToggler.Animate({ top: destinationTop - listToggler.outerHeight() }, function() {
                listToggler.css({ top: "auto" });
            });
        },
        Export: function(e) {
            U.GetAttachment("PrivateDuty/Export/ScheduleList", {
                startDate: $(".pd-calendar", e).PrivateDutyScheduler("getStartDate"),
                endDate: $(".pd-calendar", e).PrivateDutyScheduler("getEndDate")
            });
        }
    }
});