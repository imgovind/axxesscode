﻿if (typeof Schedule.Task == "undefined") $.extend(Schedule, { Task: new Object() });
$.extend(Schedule.Task, {
    PrivateDuty: {
        // Delete Private Duty Event
        Delete: function (patientId, id) {
            Schedule.Task.Delete(patientId, id, PrivateDuty.Area);
        },
        // Reopen Private Duty Event
        Reopen: function (patientId, id) {
            Schedule.Task.Reopen(patientId, id, PrivateDuty.Area);
        },
        // Restore Private Duty Event
        Restore: function (patientId, id) {
            Schedule.Task.Restore(patientId, id, PrivateDuty.Area);
        },
        // Open Edit Private Event Window
        Edit: function (patientId, id) {
            Acore.Open("scheduledetails", "PrivateDuty/Task/EditDetails", Schedule.Task.PrivateDuty.InitEdit, { id: id, patientId: patientId });
        },
        // Initialize Edit Private Event Window
        InitEdit: function (r, t, x, e) {
            $(".load-disciplinetask", e).on("click", function () { Schedule.Task.LoadTasks($(this), { LocationId: $("input[name=LocationId]", e).val(), DisciplineTask: $("[name=DisciplineTask]", e).val() }); });
            $(".load-users", e).on("click", function () { User.LoadUsers($(this), { branchId: $("input[name=LocationId]", e).val(), statusId: 1, service: 2 }, $("input[name=UserId]", e).val()); });

            $("div.returncommentsdetail", e).ReturnComments({ UrlPrefix: PrivateDuty.Area, EventId: $("input[name=Id]", e), EpisodeId: $("input[name=EpisodeId]", e), PatientId: $("input[name=PatientId]", e) });
            Schedule.Task.PrivateDuty.InitShared(r, t, x, e);
        },
        // Initialize New Private Event Window
        InitNew: function (r, t, x, e) {
            $("[name=PatientId] , input.startdate,input.enddate", e).on("change", function () {
                var input = { PatientId: $("[name = PatientId]", e).val(), StartDate: $("input.startdate", e).val(), EndDate: $("input.enddate", e).val() };
                if (U.IsGuid(input.PatientId) && input.PatientId != U.GuidEmpty && input.StartDate != "" && input.EndDate != "" && input.EndDate > input.StartDate) {
                    Patient.Insurance.LoadTaskPayors(input, "PrivateDuty", $("[name=PayorId]", e));
                }
                else {
                    $("[name=PayorId]", e).empty();
                }
            });
            Schedule.Task.PrivateDuty.InitShared(r, t, x, e);
        },
        InitShared: function (r, t, x, e) {
            U.HideIfChecked($("[name=IsAllDay]", e), $(".time-picker", e).parent());
            $("form.mainform", e).Validate({ Success: Refresh.All });
        },
        // Open New Private Event Window
        New: function (currentId, patientSelection, date, time) {
            Acore.Modal({
                Name: "Employee Scheduler",
                Url: "PrivateDuty/Task/New",
                Input: {
                    patientId: patientSelection ? currentId : U.GuidEmpty,
                    userId: patientSelection ? U.GuidEmpty : currentId,
                    startDate: date,
                    startTime: time
                },
                OnLoad: Schedule.Task.PrivateDuty.InitNew,
                Width: 500
            })
        },
        NewMultiple: function (e) {
            var data = Schedule.Center.PrivateDuty.GetData(e);
            var currentId = data.CurrentId;
            var patientSelection = data.PatientSelection;
            Acore.Modal({
                Name: "Employee Scheduler",
                Url: "PrivateDuty/Task/NewMultiple",
                Input: {
                    patientId: patientSelection ? currentId : U.GuidEmpty,
                    userId: patientSelection ? U.GuidEmpty : currentId,
                    startDate: $(".pd-calendar", e).PrivateDutyScheduler("getStartDate"),
                    endDate: $(".pd-calendar", e).PrivateDutyScheduler("getEndDate")
                },
                OnLoad: Schedule.Task.InitNewMultiple,
                Width: 500
            })
        },
        Reassign: function (patientId, id) { Schedule.Task.Reassign(patientId, id, PrivateDuty.Area); },
        MultipleReassign: function (patientId) {
            Schedule.Task.MultipleReassign({
                patientId: patientId,
                userId: U.GuidEmpty
            }, PrivateDuty.Area);
        },
        SchedulerMultipleReassign: function (e) {
            var currentId = $(".pd-calendar", e).data("PrivateDutyScheduler").CurrentId;
            var patientSelection = $(".pd-calendar", e).data("PrivateDutyScheduler").PatientSelection;
            Schedule.Task.MultipleReassign({
                patientId: patientSelection ? currentId : U.GuidEmpty,
                userId: patientSelection ? U.GuidEmpty : currentId,
                startDate: $(".pd-calendar", e).PrivateDutyScheduler("getStartDate"),
                endDate: $(".pd-calendar", e).PrivateDutyScheduler("getEndDate")
            }, PrivateDuty.Area);
        },
        DeleteAsset: function (control, patientId, eventId, assetId) { Schedule.Task.DeleteAsset(control, { patientId: patientId, Id: eventId, assetId: assetId }, PrivateDuty.Area) },
        GetAttachments: function (patientId, eventId) { Schedule.Task.GetAttachments(patientId, eventId, PrivateDuty.Area); }
    }
});