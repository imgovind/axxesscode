﻿if (typeof Schedule.Center == "undefined") $.extend(Schedule, { Center: new Object() });
$.extend(Schedule.Center, {
    PrivateDuty: {
        AcoreId: "PrivateDutyScheduleCenter",   // Acore Window ID for Private Duty Schedule Center
        CurrentId: "",                          // Current Active Patient/User's GUID
        CurrentName: "",                        // Current Active Patient/User's Name
        PatientSelection: true,                 // Toggle between patient and user selection
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).Layout({
                west: {
                    paneSelector: ".ui-layout-west",
                    size: 200,
                    minSize: 170,
                    maxSize: 400,
                    livePaneResizing: true,
                    spacing_open: 3,
                    onresize_end: function() { $(".pd-calendar ", e).trigger("resize"); }
                }
            });
            // Set switcher between Patient/User selector
            $(".schedule-by", e).on("click", function() {
                if ($(this).parent().not(".selected")) Schedule.Center.PrivateDuty.Selector.Switch(e, $(this).hasClass("patient"));
                return false;
            });
            // Set patient selector filters
            $(".ui-layout-west .top input:not([readonly])", e).on("keyup", function() { Schedule.Center.PrivateDuty.Selector.Filter(e) });
            $(".ui-layout-west .top select:visible", e).on("change", function() { Schedule.Center.PrivateDuty.Selector.Rebind(e) });
            // Hide inactive filters
            Schedule.Center.PrivateDuty.PatientSelection = true;
            //if (Schedule.Center.PrivateDuty.PatientSelection) {
            $(".user-filters").hide();
            //$(".ui-layout-west .bottom", e).css({ top: "13em" });
            //            } else {
            //                $(".patient-filters").hide();
            //                $(".ui-layout-west .bottom", e).css({ top: "10em" });
            //            }
            // Window menu
            $(".new-task", e).on("click", function() {
                var calendarData = $(".pd-calendar", e).data("PrivateDutyScheduler");
                Schedule.Task.PrivateDuty.New(calendarData.CurrentId, calendarData.PatientSelection);
                return false;
            });
            $(".new-care-period", e).on("click", function() {
                Episode.PrivateDuty.New(Schedule.Center.PrivateDuty.CurrentId);
                return false;
            });
            $(".list-care-periods", e).on("click", function() {
                Episode.PrivateDuty.List(Schedule.Center.PrivateDuty.CurrentId);
                return false;
            });
            $(".reassign-schedule", e).on("click", function() {
                Schedule.Task.PrivateDuty.SchedulerMultipleReassign(e);
                return false;
            });
            $(".schedule-employee", e).on("click", function() {
                Schedule.Task.PrivateDuty.NewMultiple(e);
                return false;
            });
            $(".print", e).on("click", function() {
                Schedule.Center.PrivateDuty.Print(e);
                return false;
            });
            // Toggle List Views
            $(".show-list", e).on("click", function() {
                Schedule.Center.PrivateDuty.ShowList(e);
                return false;
            });
            $(".hide-list", e).on("click", function() {
                Schedule.Center.PrivateDuty.HideList(e);
                return false;
            });
            U.ActivityGridContextMenuInit($("#PrivateDutyTask_Grid", e));
            $(".window-menu", e).Menu();
            Schedule.Center.PrivateDuty.Selector.Init();
        },
        // External Loader
        Load: function(currentId, currentName, patientSelection) {
            patientSelection = patientSelection != false;
            if (Acore.Windows[Schedule.Center.PrivateDuty.AcoreId].IsOpen) {
                Schedule.Center.PrivateDuty.LoadContent(currentId, currentName, patientSelection);
                $("#window_" + Schedule.Center.PrivateDuty.AcoreId).WinFocus();
            } else {
                Schedule.Center.PrivateDuty.CurrentId = currentId, Schedule.Center.PrivateDuty.CurrentName = currentName, Schedule.Center.PrivateDuty.PatientSelection = patientSelection;
                Acore.Open(Schedule.Center.PrivateDuty.AcoreId);
            }
        },
        // Patient Selection Loader
        LoadContent: function(currentId, currentName, patientSelection) {
            var e = $("#window_" + Schedule.Center.PrivateDuty.AcoreId + "_content");
            if ($(".hide-list", e).is(":visible")) {
                Schedule.Center.PrivateDuty.HideList(e);
                // $(".schedule-list", e).empty();
            }
            if (U.IsGuid(currentId)) {
                var row = $(".ui-layout-west td.id:contains(" + currentId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
                row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, 500);
                Schedule.Center.PrivateDuty.CurrentId = currentId, Schedule.Center.PrivateDuty.CurrentName = currentName, Schedule.Center.PrivateDuty.PatientSelection = patientSelection;
                Schedule.Center.PrivateDuty.Refresh();
            }
        },
        // Print Calendar
        Print: function(e) {
            U.GetAttachment("PrivateDuty/Schedule/Print", {
                patientId: Schedule.Center.PrivateDuty.PatientSelection ? Schedule.Center.PrivateDuty.CurrentId : U.GuidEmpty,
                userId: Schedule.Center.PrivateDuty.PatientSelection ? U.GuidEmpty : Schedule.Center.PrivateDuty.CurrentId,
                startDate: $(".pd-calendar", e).PrivateDutyScheduler("getStartDate"),
                endDate: $(".pd-calendar", e).PrivateDutyScheduler("getEndDate")
            })
        },
        // Refresh Calendar Data
        Refresh: function(Data) {
            if (Data == undefined || Data.PatientId == Schedule.Center.PrivateDuty.CurrentId) {
                var e = $("#window_" + Schedule.Center.PrivateDuty.AcoreId + "_content");
                $(".ui-layout-center", e).find(".error-box").remove().end().find("#PDScheduleMainResult").show();
                var calendar = $(".pd-calendar", e);
                if (calendar) {
                    calendar.fullCalendar('destroy');
                    calendar.PrivateDutyScheduler({
                        CurrentId: Schedule.Center.PrivateDuty.CurrentId,
                        CurrentName: Schedule.Center.PrivateDuty.CurrentName,
                        PatientSelection: Schedule.Center.PrivateDuty.PatientSelection,
                        Scheduler: calendar.hasClass("scheduler"),
                        HasSchedulerPermission: $(".new-task", e).length
                    })
                }
            }
        },
        RefreshCurrentView: function() {
            var e = $("#window_" + Schedule.Center.PrivateDuty.AcoreId + "_content");
            if (e) {
                $(".pd-calendar ", e).PrivateDutyScheduler("refreshCurrentView");
            }
        },
        GetData: function(e) {
            var data = e.data("PrivateDutyScheduler");
            if (!data) data = $(".scheduler", e).data("PrivateDutyScheduler");
            return data;
        },
        Selector: {
            // Initialize Patient Selector
            Init: function() {
                var e = $("#window_" + Schedule.Center.PrivateDuty.AcoreId + "_content");
                $(".ui-layout-west .t-grid-content", e).css("height", "auto");
                //Schedule.Center.PrivateDuty.Selector.Filter(e);
                if (U.IsGuid(Schedule.Center.PrivateDuty.CurrentId)) {
                    var row = $(".ui-layout-west .id:contains(" + Schedule.Center.PrivateDuty.CurrentId + ")", e).parent();
                    if (row.length) Schedule.Center.PrivateDuty.LoadContent($("td:eq(3)", row).text(), $("td:eq(2)", row).text(), row.closest(".t-grid").hasClass("patient-selector"));
                }
                if ($(".ui-layout-west .t-state-selected", e).length == 0) {
                    var row = $(".ui-layout-west .t-grid-content tr:first", e).not(".t-no-data");
                    if (row.length) {
                        Schedule.Center.PrivateDuty.LoadContent($("td:eq(3)", row).text(), $("td:eq(2)", row).text(), row.closest(".t-grid").hasClass("patient-selector"));
                    }
                    else {
                        $(".ui-layout-center", e).find("#PDScheduleMainResult").hide().end().append(Schedule.Center.PrivateDuty.Selector.BuildNoDataFoundMessage());
                    }
                }
                var grid = $(".ui-layout-west .t-grid", e);
                U.OnTGridDataBound({ target: grid });
                U.StopTGridFirstAction(grid);
            },
            // Text Filter Functionality
            Filter: function(e) {
                var text = $(".ui-layout-west .top input[name='SearchText']", e).val();
                if (text && text.length > 0) {
                    var search = text.split(" ");
                    $(".ui-layout-west .t-grid-content tr", e).removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) {
                        $(".ui-layout-west .t-grid-content td", e).each(function() {
                            if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                        });
                    }
                    $(".ui-layout-west .t-grid-content tr:not(.match)", e).hide();

                    if ($(".ui-layout-west .t-grid-content .match", e).length) {
                        $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    }
                    else {
                        $(".ui-layout-center", e).find("#PDScheduleMainResult").hide().end().append(Schedule.Center.PrivateDuty.Selector.BuildNoDataFoundMessage());
                        Schedule.Center.PrivateDuty.CurrentId = "";
                    }
                } else {
                    $(".ui-layout-west .t-grid-content tr", e).not(".t-no-data").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
                    if (Schedule.Center.PrivateDuty.CurrentId === "" || Schedule.Center.PrivateDuty.CurrentId === undefined) {
                        $(".ui-layout-west .t-grid-content tr", e).eq(0).click();
                    }
                }
            },
            Rebind: function(e) {
                if (!e.length) e = $("#window_" + Schedule.Center.PrivateDuty.AcoreId + "_content");
                U.RebindTGrid($(".ui-layout-west .t-grid", e), Schedule.Center.PrivateDuty.Selector.GetFilterInput(e));
            },
            // Selecting Patient from Patient Selector
            Select: function(e) {
                Schedule.Center.PrivateDuty.LoadContent($("td:eq(3)", e.row).text(), $("td:eq(2)", e.row).text(), $(e.row).closest(".t-grid").hasClass("patient-selector"));
            },
            Switch: function(e, PatientSelection) {
                var oldSelection = Schedule.Center.PrivateDuty.PatientSelection;
                Schedule.Center.PrivateDuty.PatientSelection = PatientSelection;
                var bottomSection = $(".ui-layout-west .bottom", e);
                if (PatientSelection) {
                    $(".care-period-menu", e).show();
                    bottomSection.addClass("four-filter");
                }
                else {
                    $(".care-period-menu", e).hide();
                    bottomSection.removeClass("four-filter");
                }
                if (oldSelection !== PatientSelection) {
                    $(".patient-filters,.user-filters", e).toggle();
                    $(".schedule-by").parent().toggleClass("selected");
                    $("input[name=SearchText]", e).val('');
                }
                $(".pd-calendar", e).parent().addClass("loading-visibility");
                bottomSection.Load("PrivateDuty/Schedule/" + (PatientSelection ? "Patient" : "User") + "Selector", Schedule.Center.PrivateDuty.Selector.GetFilterInput($(".ui-layout-west .top", e)), Schedule.Center.PrivateDuty.Selector.Init);
            },
            GetFilterInput: function(e) {
                var inputs = { branchId: $("[name=BranchId]", e).val(), statusId: $("[name=" + (Schedule.Center.PrivateDuty.PatientSelection ? "Patient" : "User") + "Status]:visible", e).val() }
                if (Schedule.Center.PrivateDuty.PatientSelection) $.extend(inputs, { paymentSourceId: $("[name=PaymentSource]").val() });
                return inputs;
            },
            OnDataBinding: function(e) {
                U.OnDataBindingStopAction(e);

            },
            BuildNoDataFoundMessage: function() {
                var text = Schedule.Center.PrivateDuty.PatientSelection ? "Patients" : "Employees";
                return U.MessageWarn("No " + text, "No " + text.toLowerCase() + " found for your selection.  Please reduce your filters and try again.");
            }
        },
        ShowList: function(e) {
            $(".hide-list,.show-list", e).toggle();
            $(".fc-list-tasks", e).css("visibility", "hidden");
            var currentPosition = $(".ui-layout-center", e).height(),
                listToggler = $(".list-collapsed", e);
            listToggler.css({top: currentPosition - listToggler.outerHeight()}).Animate({ top: "6em" });
            $(".schedule-list", e).css({ bottom: 0, top: currentPosition, height: "auto" }).Animate({ top: "6.3em", width: "100%" }, function() {
                $(".fc-content", e).hide();
            });
        },
        HideList: function(e) {
            $(".hide-list,.show-list", e).toggle();
            $(".fc-content", e).show();
            $(".fc-button", e).attr("style", "");
            var listToggler = $(".list-collapsed", e),
                scheduleList = $(".schedule-list", e),
                destinationTop = $(".ui-layout-center", e).height();
            $(".pd-calendar ", e).PrivateDutyScheduler("rerenderEventsAsNew");
            scheduleList.Animate({ top: destinationTop }, function() {
                scheduleList.css({ top: "auto" });
            });
            listToggler.Animate({ top: destinationTop - listToggler.outerHeight() }, function() {
                listToggler.css({ top: "auto" });
            });
        },
        OnDataBinding: function(e) {
            if (!e.isSorting) e.preventDefault();
        },
        OnDataBound: function (e) {
            var grid = $(e.currentTarget);
            if (grid.length > 0) {
                var gridEntity = grid.data("tGrid");
                if (gridEntity) {
                    var calendar = grid.closest(".layout").find(".pd-calendar");
                    if (calendar.length > 0) {
                        U.FilterGridByDateRange(gridEntity, calendar.PrivateDutyScheduler("getStartDate"), calendar.PrivateDutyScheduler("getEndDate"));
                    }
                }
            }
        },
        OnRowDataBound: function (e) {
            var span = $("<div />", { text: "More ", "class": "grid-action" }).append($("<span/>").addClass("img icon22 pointer right"));
            $("td.action", e.row).append(span);
            U.ToolTip(e.row);
            U.RowColorCode(e.row, e.dataItem);
            CommandFactory.GridInit(e.row, e.dataItem, "PrivateDuty");
        }
    }
});