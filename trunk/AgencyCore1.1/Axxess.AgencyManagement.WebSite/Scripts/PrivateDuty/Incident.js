﻿if (typeof Incident == "undefined") var Incident = new Object();
$.extend(Incident, {
    PrivateDuty: {
        Prefix: "PrivateDuty",
        New: function(patientId) {
            if (patientId) Acore.Open("PrivateDutynewincidentreport", { patientId: patientId });
            else Acore.Open("PrivateDutynewincidentreport");
        },
        Edit: function(Id) { Acore.Open("PrivateDutyeditincidentreport", { Id: Id }); }
    }
});