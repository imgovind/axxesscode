﻿if (typeof Billing.Managed.Center == "undefined") $.extend(Billing.Managed, { Center: new Object() });
$.extend(Billing.Managed.Center, {
    PrivateDuty: {
        Area: "PrivateDuty",
        AcoreId: "PDmanagedclaims",
        Init: function(r, t, x, e) { Billing.Managed.Center.Init(r, t, x, e) },
        OnClaimDataBinding: function(e) { Billing.Managed.Center.OnClaimDataBinding(e); },
        OnClaimDataBound: function(e) { Billing.Managed.Center.OnClaimDataBound(e, PrivateDuty.ApplicationService); },
        OnClaimRowDataBound: function(e) { Billing.Managed.Center.OnClaimRowDataBound(e, PrivateDuty.ApplicationService); },
        OnClaimRowSelected: function(e) { Billing.Managed.Center.OnClaimRowSelected(e, Billing.Managed.Center.PrivateDuty.Area); },
        ActivityRefresh: function(patientId) { Billing.Managed.Center.ActivityRefresh(Billing.Managed.Center.PrivateDuty.AcoreId, patientId); },
        Refresh: function(patientId) {
            U.RebindTGrid($('#ManagedBillingPaymentsGrid'));
            U.RebindTGrid($('#ManagedBillingAdjustmentsGrid'));
            Billing.Managed.Center.PrivateDuty.Refresh("PrivateDuty", patientId);
        }
    }
});