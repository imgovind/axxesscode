﻿if (typeof Billing.Managed.Claims == "undefined") $.extend(Billing.Managed, { Claims: new Object() });
$.extend(Billing.Managed.Claims, {
    PrivateDuty: {
        ManagedClaimsWindowId: "PDcreatemanagedclaims",
        ReLoadUnProcessedManagedClaim: function(data) { Billing.Managed.Claims.ReLoadUnProcessedManagedClaim(Billing.Managed.Claims.PrivateDuty.ManagedClaimsWindowId, data); }
    }
});