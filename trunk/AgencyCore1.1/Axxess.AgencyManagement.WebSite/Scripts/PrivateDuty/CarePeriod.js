﻿if (typeof Episode == "undefined") var Episode = new Object();
$.extend(Episode, {
    PrivateDuty: {
        AcoreId: "PrivateDutyCarePeriodList",   // Acore Window ID for Private Duty Care Period List
        Label: "Care Period",
        Prefix: "PrivateDuty/CarePeriod",
        InitCarePeriodLength: function(e) {
            $(".care-period-length", e).on("change", function() {
                if (parseInt($(this).val())) {
                    if (U.IsDate($(".care-period-start", e).val())) {
                        var calcEndDate = $(".care-period-start", e).datepicker("getDate").valueOf(),
                            endDateName = $(".care-period-end", e).attr("name");
                        calcEndDate += 86400000 * parseInt($(this).val());
                        $(".care-period-end-hidden", e).remove();
                        $(".care-period-end", e).datepicker("setDate", new Date(calcEndDate)).prop("disabled", true).after(
                            $("<input/>", { type: "hidden", name: endDateName, value: U.FormatDate(calcEndDate) }).addClass("care-period-end-hidden"));
                    } else {
                        U.Growl("Need a Start Date to Calculate End Date", "warning");
                        // $(".care-period-length", e).val("specify");
                        $(".care-period-start", e).focus();
                    }
                } else {
                    $(".care-period-end", e).prop("disabled", false).focus();
                    $(".care-period-end-hidden", e).remove();
                }
            });
            $(".care-period-start", e).on("change", function() {
                if (parseInt($(".care-period-length", e).val()) && U.IsDate($(this).val())) $(".care-period-length", e).trigger("change");
            });
        },
        LoadDropDown: function (dropDown, control) { Episode.LoadDropDown(Episode.PrivateDuty.Prefix, dropDown, control, Episode.PrivateDuty.Label); },
        // Delete Private Duty Care Period
        Activate: function(id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to activate this care period?",
                Yes: function() {
                    U.PostUrl("PrivateDuty/CarePeriod/Activate", {
                        id: id,
                        patientId: patientId
                    }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Refresh.CenterQAMySchedule(result);
                            Episode.PrivateDuty.Refresh();
                        }
                    })
                }
            })
        },
        Deactivate: function(id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to deactivate this care period?",
                Yes: function() {
                    U.PostUrl("PrivateDuty/CarePeriod/Deactivate", {
                        id: id,
                        patientId: patientId
                    }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Episode.PrivateDuty.Refresh();
                            Refresh.CenterQAMySchedule(result);
                        }
                    })
                }
            })
        },
        // Open Edit Private Duty Care Period Window
        Edit: function(id, patientId) {
            Acore.Modal({
                Name: "Care Period Editor",
                Url: "PrivateDuty/CarePeriod/Edit",
                Input: {
                    id: id,
                    patientId: patientId
                },
                OnLoad: Episode.PrivateDuty.InitEdit,
                Width: 880
            })
        },
        // Initialize Edit Private Duty Care Period Window
        InitEdit: function(r, t, x, e) {
            Episode.PrivateDuty.InitShared(r, t, x, e);
        },
        // Initialize Private Duty Care Period List Window
        InitList: function(r, t, x, e) {
            $(".filter", e).change(Episode.PrivateDuty.Refresh);
            Episode.PrivateDuty.InitListContent(r, t, x, e);
        },
        InitListContent: function(r, t, x, e) {
            $(".activate-care-period", e).on("click", function() {
                Episode.PrivateDuty.Activate($(this).closest("li").attr("guid"), $(this).closest("li").attr("patient"));
                return false;
            });
            $(".deactivate-care-period", e).on("click", function() {
                Episode.PrivateDuty.Deactivate($(this).closest("li").attr("guid"), $(this).closest("li").attr("patient"));
                return false;
            });
            $(".edit-care-period", e).on("click", function() {
                Episode.PrivateDuty.Edit($(this).closest("li").attr("guid"), $(this).closest("li").attr("patient"));
                return false;
            });
            e.closest(".window").SetHeight();
        },
        // Initialize New Private Duty Care Period Window
        InitNew: function(r, t, x, e) {
            Episode.PrivateDuty.InitShared(r, t, x, e);
        },
        InitNewFromMenu: function(r, t, x, e) {
            $(".patient-picker", e).on("change", function() {
                //console.log("changed - " + e)
                if (U.IsGuid($("[name='PatientId']", e).val()) && $(this).val() != U.GuidEmpty) {
                    var patientId = $("[name='PatientId']", e).val();
                    $(".content", e).Load("PrivateDuty/CarePeriod/NewContent", { patientId: patientId, isFromMainMenu: true }, Episode.PrivateDuty.InitShared);
                }
            });
            Episode.PrivateDuty.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            Episode.PrivateDuty.InitCarePeriodLength(e);
            $("form", e).Validate({
                Success: function(result) {
                    Refresh.CenterQAMySchedule(result);
                    if (result.IsDataCentersRefresh) Episode.PrivateDuty.Refresh();
                }
            })
        },
        List: function(patientId) {
            Acore.Open(Episode.PrivateDuty.AcoreId, { patientId: patientId });
        },
        // Open New Private Duty Care Period Window
        New: function(patientId) {
            Acore.Modal({
                Name: "New Private Duty Care Period",
                Url: "PrivateDuty/CarePeriod/New",
                Input: { patientId: patientId },
                OnLoad: Episode.PrivateDuty.InitNew,
                Width: 1000
            })
        },
        Refresh: function() {
            var e = $("#window_" + Episode.PrivateDuty.AcoreId + "_content");
            $(".patient-care-periods", e).Load("PrivateDuty/CarePeriod/ListContent", {
                patientId: $("[name=PatientId]", e).val(),
                activeInactive: $("[name=ActiveInactive]", e).val()
            }, Episode.PrivateDuty.InitListContent)
        }
    }
});