﻿if (typeof Infection == "undefined") var Infection = new Object();
$.extend(Infection, {
    PrivateDuty: {
        Prefix: "PrivateDuty",
        New: function(patientId) {
            if (patientId) Acore.Open("PrivateDutynewinfectionreport", { patientId: patientId });
            else Acore.Open("PrivateDutynewinfectionreport");
        },
        Edit: function(Id) { Acore.Open("PrivateDutyeditinfectionreport", { Id: Id }); },
        InitNew: function(r, t, x, e) { Infection.InitNew(r, t, x, e); }
    }
});