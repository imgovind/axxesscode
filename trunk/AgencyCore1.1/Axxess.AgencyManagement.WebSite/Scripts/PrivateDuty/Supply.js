﻿if (typeof Supply == "undefined") var Supply = new Object();
$.extend(Supply, {
    PrivateDuty: {
        Area: "PrivateDuty",
        LoadWorkSheet: function(patientId, eventId) {
            Visit.Supply.LoadWorkSheet(patientId, eventId, Supply.PrivateDuty.Area);
        }
    }
});