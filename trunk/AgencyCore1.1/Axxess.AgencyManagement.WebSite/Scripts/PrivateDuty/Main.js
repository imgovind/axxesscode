﻿if (typeof PrivateDuty == "undefined") var PrivateDuty = new Object();
$.extend(PrivateDuty, {
    ApplicationService: 2,
    IsActive: false,
    Area: "PrivateDuty",
    SetActive: function(service) { this.IsActive = (service & this.ApplicationService) === this.ApplicationService; },
    IsAlone: function() {
        return Acore.ApplicationServices == PrivateDuty.ApplicationService;
    },
    RenameWindow: function(e, addPrefix) {
        if (Acore.ApplicationServices != this.ApplicationService) {
            e.Rename("Private Duty | " + e.parent().find(".title").text());
            if (addPrefix) U.SetPrefix(e, "PrivateDuty");
        }
    }
});