﻿if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    PrivateDuty: {
        Delete: function(cont, id) {
            Oasis.Delete(cont, id, "PrivateDuty/Oasis/Delete");
        },
        HandleRefresh: function(result) {
            try { window.parent.Refresh.All(result); } catch (e) { }
        }
    }
});