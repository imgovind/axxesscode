﻿if (typeof PhysicianOrder == "undefined") var PhysicianOrder = new Object();
$.extend(PhysicianOrder, {
PrivateDuty: {
        New: function(patientId, episodeId) {
            episodeId = episodeId || U.GuidEmpty;
            Acore.Open("PrivateDutyneworder",  { patientId: patientId, episodeId: episodeId });
        },
        InitNew: function(r, t, x, e) { PhysicianOrder.InitNew(e, PrivateDuty.IsAlone(), "Private Duty ", "PrivateDuty"); },
        Edit: function(id, patientId) { Acore.Open("PrivateDutyeditorder", { id: id, patientId: patientId }); },
        InitEdit: function(r, t, x, e) { PhysicianOrder.Init(e, PrivateDuty.IsAlone(), "Private Duty ", "PrivateDuty"); }
    }
});