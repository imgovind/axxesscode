﻿var User = {
    AcoreEditId: "EditUser",
    AcoreListId: "listusers",
    New: function() { Acore.Open("newuser", "User/New", User.InitNew); },
    InitNew: function(r, t, x, e) {
        var tab = $(".maintab", e);
        tab.tabs({
            beforeLoad: function(event, ui) {
                ui.ajaxSettings.type = 'POST';
                ui.ajaxSettings.hasContent = true;
                ui.jqXHR.setRequestHeader("Content-Type", ui.ajaxSettings.contentType);
                var postData = U.JsonInput($("input[name=UserId].tabinput", tab).serializeArray());
                var userId = postData["UserId"];
                if (userId && userId != U.GuidEmpty) {
                    ui.ajaxSettings.data = jQuery.param(postData);
                    ui.panel.addClass("loading tab-content");
                }
                else {
                    U.Growl("User is not created/identified.", "error");
                }
            },
            load: function(event, ui) {
                var category = $("a", ui.tab).attr("name");
                ui.panel.removeClass("loading");
                User.InitSection(category, ui.panel);
            },
            beforeActivate: function(event, ui) {
                if ($("a", ui.newTab).attr("href").indexOf("#") != -1) $("a", ui.newTab).attr("href", $("a", ui.newTab).attr("url"));
            },
            create: function(event, ui) {
                User.Sections.NewInformation(ui.panel);
            },
            disabled: [1, 2, 3]
        });
    },
    Edit: function(id) { Acore.Open("EditUser", { Id: id }, User.InitEdit); },
    InitEdit: function(r, t, x, e) {
        var tab = $(".maintab", e);
        var postData = U.JsonInput($("input[name=UserId].tabinput", tab).serializeArray());
        tab.tabs({
            beforeLoad: function(event, ui) {
                ui.ajaxSettings.type = 'POST';
                ui.ajaxSettings.hasContent = true;
                ui.jqXHR.setRequestHeader("Content-Type", ui.ajaxSettings.contentType);
                ui.ajaxSettings.data = jQuery.param(postData);
                ui.panel.addClass("loading tab-content");
            },
            load: function(event, ui) {
                var category = $("a", ui.tab).attr("name");
                ui.panel.removeClass("loading");
                User.InitSection(category, ui.panel);
            },
            beforeActivate: function(event, ui) {
                if ($("a", ui.newTab).attr("href").indexOf("#") != -1) $("a", ui.newTab).attr("href", $("a", ui.newTab).attr("url"));
            },
            create: function(event, ui) {
                User.Sections.Information(ui.panel);
            }
        });
    },
    InitSection: function(category, panel) {
        Acore.OnLoad(panel);
        User.Sections[category](panel);
    },
    Sections: {
        NewInformation: function(e) {
            U.ShowIfSelectEquals($("[name=TitleType]", e), "Other", $("[name=TitleTypeOther]", e).parent("div"));
            U.ShowIfSelectEquals($("[name=Credentials]", e), "Other", $("[name=CredentialsOther]", e).parent("div"));
            $("form.mainform", e).Validate({ Success:
                function(result) {
                    var tab = e.closest(".maintab");
                    if (result.isSuccessful) {
                        if (result.Id && result.Id != U.GuidEmpty) {
                            tab.find("input[name=UserId].tabinput").val(result.Id);
                            tab.tabs("enable");
                        } else tab.tabs("option", "disabled", [1, 2, 3]);
                    } else tab.tabs("option", "disabled", [1, 2, 3]);
                }
            });
        },
        Information: function(e) {
            var services = $("[name=ServiceArray]", e);
            if (services.is(":checkbox")) {
                var perferredFunc = function() {
                    if (services.filter(":checked").length > 1) $("#EditUser_PreferredServices", e).show();
                    else $("#EditUser_PreferredServices", e).hide();
                };
                perferredFunc();
                services.change(perferredFunc);
            }
            $(".multiple",e).MultiSelect({ DefaultText: '- Select Location(s) -' });
            U.ShowIfSelectEquals($("[name=TitleType]", e), "Other", $("[name=TitleTypeOther]", e).parent("div"));
            U.ShowIfSelectEquals($("[name=Credentials]", e), "Other", $("[name=CredentialsOther]", e).parent("div"));
            $("form.mainform", e).Validate();
        },
        Permissions: function(e) {
            var type = "EditUserPermissions", prefix = "#" + type + "_",
                allPermissionCkbx = $(prefix + "AllPermissions", e),
                mainAccordion = $(prefix + "Accordion", e);
            mainAccordion.Tree();
            U.SelectAll(allPermissionCkbx, $("input:checkbox", e));
            mainAccordion.TogglePanels({ active: [0, 1, 2, 3] });
            $(".accordion", mainAccordion).TogglePanels();
            //Allows the checkboxes within the accordion headers to be checked and open the accordion if it closed only if checking the checkbox
            $("h3 > input.select-all", mainAccordion).click(function(evnt) {
                if ($(this).prop("checked") && !$(this).closest("h3").hasClass("ui-accordion-header-active")) $(this).parent().click();
                evnt.stopPropagation();
            });
            if (!$("input:checkbox", mainAccordion).not(":checked").length) {
                allPermissionCkbx.prop("checked", true);
                U.SelectAllLabelRename(allPermissionCkbx);
            }
            $("ol li", mainAccordion).each(function() {
                if ($(this).attr("tooltip") != undefined) $(this).Tooltip({
                    top: 10,
                    left: 10,
                    extraClass: "calday"
                });
            });
            $("form.mainform", e).Validate();
        },
        Licenses: function(e) {
            var userId = $("input[name=Id]", e).val();
            $(".new-license", e).on("click", function() {
                LicenseManager.New(userId);
            });
            $(".grid-refresh", e).on("click", function() {
                User.License.Refresh(userId, $(".user-license-listcontainer", e));
            });
            User.License.InitList(null, null, null, e);
        },
        PatientAccess: function(e) {
            $(".patient-access", e).MultiGrid({
                DataUri: "User/PatientAccessData",
                MoveUri: "User/PatientAccessMove",
                LeftGridTitle: "User has Access to These Patients",
                RightGridTitle: "User does not have Access to These Patients"
            })
        },
        Rates: function(e) {
            var userId = $("input[name=Id]", e).val();
            $(".loadrates", e).on("click", function() { User.Rate.LoadRate($("[name=FromUserId]", e).val(), userId); });

            $(".new-rate", e).on("click", function() {
                User.Rate.New(userId)
            });

            $(".new-payor-rate", e).on("click", function() {
                User.Rate.New(userId, $(this).attr("payorId"))
            });

            $(".load-from-other-user", e).on("click", function() {
                User.Rate.LoadRate2(userId)
            });

            $(".grid-refresh", e).on("click", function() {
                User.Rate.Refresh(userId, $(".user-rates-listcontainer", e))
            });
            User.Rate.InitList(null, null, null, e)
        }
    },
    InitList: function(r, t, x, e) {
        $(".new-user", e).on("click", function() {
            User.New();
            return false;
        });
        U.ListInit(r, t, x, e);
    },
    RebindList: function() {
        if (Acore.Windows[User.AcoreListId].IsOpen) {
            var refresh = $("#window_" + User.AcoreListId).find(".grid-refresh");
            if (refresh) refresh.click();
        }
    },
    License: {
        InitList: function(r, t, x, e) {
            var userId = e.closest(".window-content").find("input[name=Id]").val();
            $(".edit-license", e).on("click", function() {
                LicenseManager.Edit($(this).attr("guid"), userId);
            });
            $(".delete-license", e).on("click", function() {
                LicenseManager.Delete($(this).attr("guid"), userId);
            });
        },
        Refresh: function(userId, grid) {
            grid = grid || $("#EditUser_LicenseGrid");
            if (grid.length && userId) {
                grid.Load("License/List", { UserId: userId }, User.License.InitList);
            }
        }
    },
    Rate: {
        Delete: function(id, userId, insurance) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this pay rate?",
                Yes: function() {
                    U.PostUrl("User/Rate/Delete", { Id: id, UserId: userId, Insurance: insurance }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful)
                            User.Rate.Refresh(userId);
                    })
                }
            })
        },
        InitEdit: function(r, t, x, e) {
            $("form.mainform", e).Validate({ Success: function() { User.Rate.Refresh($("[name=UserId]", e).val()) } });
        },
        InitList: function(r, t, x, e) {
            var userId = e.closest(".window-content").find("input[name=Id]").val();
            $(".edit-rate", e).on("click", function() {
                User.Rate.Edit($(this).attr("guid"), userId, $(this).attr("insurance"))
            });
            $(".delete-rate", e).on("click", function() {
                User.Rate.Delete($(this).attr("guid"), userId, $(this).attr("insurance"))
            });
        },
        InitNew: function(r, t, x, e) {
            $("form.mainform", e).Validate({ Success: function() { User.Rate.Refresh($("[name=UserId]", e).val()) } });
        },
        New: function(userId, insuranceId) {
            Acore.Modal({
                Name: "New Pay Rate",
                Url: "User/Rate/New",
                Input: { UserId: userId, InsuranceId: insuranceId ? insuranceId : 0 },
                OnLoad: User.Rate.InitNew,
                Width: 500,
                WindowFrame: false
            });
        },
        Edit: function(id, userId, insurance) {
            Acore.Modal({
                Name: "Edit License",
                Url: "User/Rate/Edit",
                Input: { Id: id, UserId: userId, Insurance: insurance },
                OnLoad: User.Rate.InitEdit,
                Width: 500,
                WindowFrame: false
            });
        },
        LoadRate: function(fromId, toId) {
            if (fromId == User.GuidEmpty || fromId == "0") {
                U.Growl("You are required to select the user.", "error");
                return;
            }
            Acore.Confirm({
                Message: "Are you sure you want to apply the same rate as the selected user?",
                Yes: function() {
                    U.PostUrl("/User/LoadUserRate", { fromId: fromId, toId: toId }, function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            User.Rate.Refresh(toId);
                        }
                        else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); }
                    });
                }
            });
        },
        LoadRate2: function(userId) {
            Acore.Modal({
                Name: "Load Rate From Other User",
                Url: "User/Rate/Load",
                Input: { UserId: userId },
                OnLoad: User.Rate.InitLoad,
                Width: 750,
                Height: 400,
                WindowFrame: false
            });
        },
        InitLoad: function(r, t, x, e) {
            $("[name=FromUserId]", e).change(function() {
                $(".load-rates-listcontainer", e).Load("User/Rate/LoadContent", { userId: $(this).val() }, User.Rate.InitLoadContent);
            });
            User.Rate.InitLoadContent(r, t, x, e);
            $("form.mainform", e).Validate({ Success: function() { User.Rate.Refresh($("[name=ToUserId]", e).val()) } });
        },
        InitLoadContent: function(r, t, x, e) {
            var selectAll = $(".selectall", e);
            if (selectAll) {
                selectAll.change(function() {
                    var input = $(this);
                    $("input[type=checkbox].input", $(this).closest("ol")).each(function() {
                        $(this).prop("checked", input.prop("checked"));
                    });
                });
            }
            $("input[type=checkbox].input", e).on("click", function() {
                var parentControl = $(this).closest("ol");
                var count = $("input[type=checkbox].input", parentControl).length;
                User.Rate.SelectHelper(count, parentControl);
            });
        },
        SelectHelper: function(count, parentControl) {
            var checkedCount = $("input[type=checkbox].input", parentControl).filter(':checked').length;
            if (count == checkedCount && count > 0) {
                $(".selectall", parentControl).prop("checked", true);
            }
            else {
                $(".selectall", parentControl).prop("checked", false);
            }
        },
        Refresh: function(userId, grid) {
            if (!grid) {
                grid = $("#window_" + User.AcoreEditId).find(".user-rates-listcontainer");
            }
            if (grid) {
                grid.Load("User/Rate/List", { UserId: userId }, User.Rate.InitList);
            }
        }
    },
    InitProfile: function(r, t, x, e) {
        $("form", e).Validate();
        $(".password-check", e).PasswordCheck({
            type: "Passwords",
            password: $("#EditProfile_NewPassword", e),
            confirm: $("#EditProfile_NewPasswordConfirm", e)
        });
        $(".signature-check", e).PasswordCheck({
            type: "Signatures",
            password: $("#EditProfile_NewSignature", e),
            confirm: $("#EditProfile_NewSignatureConfirm", e)
        });
    },
    InitForgotSignature: function() {
        $("#lnkRequestSignatureReset").on("click", function() {
            U.PostUrl("/Signature/Email", null, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Your request to reset your signature was successful. Please check your e-mail.", "success");
                    UserInterface.CloseWindow('ForgotSignature');
                }
                else {
                    $("#resetSignatureMessage").addClass("error-message").html(result.errorMessage);
                }
            });
        });
    },
    ListUserRowDataBound: function(e) {
        $("a.tooltip", e.row).each(function() {
            $(this).Tooltip({
                top: 5,
                left: -15
            });
        })
    },
    SelectPermissions: function(control, className) {
        if (control.attr("checked")) $(className).each(function() {
            if (!$(this).attr("checked")) $(this).attr("checked", true);
        });
        else $(className).each(function() {
            if ($(this).attr("checked")) $(this).attr("checked", false);
        })
    },
    InitUserSchedule: function(r, t, x, e) { U.ListInit(r, t, x, e); },
    InitUserScheduleContent: function(gridContent) {
        if (gridContent) {
            var grid = gridContent.data('tGrid');
            if (grid) {
                User.InitUserScheduleGrid(grid, "HomeHealth");
            }
        }
    },
    Delete: function(userId, userName) {
        userName = userName ? userName : "this user";
        U.Delete(userName, "/User/Delete", { userId: userId }, function() {
            User.RebindList();

        }, false, true);
    },
    Toggle: function(userId, userName, activate) {
        var statusText = activate ? "activate " : "deactivate ";
        Acore.Confirm({
            Message: "Are you sure you want to " + statusText + userName + "?",
            Yes: function() {
                var input = "userId=" + userId;
                U.PostUrl(activate ? "/User/Activate" : "/User/Deactivate", input, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) {
                        User.RebindList();
                    }
                });
            }
        });
    },
    RebindScheduleList: function() {
        $("#window_MySchedule .grid-refresh").click();
    },
    InitUserScheduleGrid: function(grid, serviceName) {
        var trs = $("tr", grid.$tbody);
        if (trs.length > 0) {
            trs.each(function(index, element) {
                $("td.edit-task a", element).on("click", function() {
                    if ($(this).hasClass("edit")) {
                        var dataItem = { Id: $(element).find("td.id").text(), PatientId: $(element).find("td.pid").text(), Type: $(element).find("td.type").text() };
                        var area = window[serviceName].Area;
                        CommandFactory.Open(dataItem, area);
                    }
                });
                $("td a.mark-missedvisit", element).on("click", function() {
                    var dataItem = { Id: $(element).find("td.id").text(), PatientId: $(element).find("td.pid").text() };
                    MissedVisit[serviceName].New(dataItem.PatientId, dataItem.Id);
                });
                U.ToolTip(element);
            });
        }
    },
    LoadUsers: function(currentItem, input, currentUserId) {

        var callback = function(currentSelect, options) {
            if (options.length > 0) {
                var parentElement = currentSelect.closest("div.user-selector");
                var taskElement = parentElement.find("[name=UserId]");
                var attributes = { id: taskElement.attr("id"), name: taskElement.attr("name"), "class": "required not-zero" }
                $("[name=UserId]", parentElement).closest("span").remove();
                parentElement.append($("<select/>", attributes).append(options)).find(".load-users").closest("span").hide();
            }
            else {
                U.Growl("Unable to load the users.  Try again.", "error");
            }
        }

        U.PostUrl("User/BranchList", input, function(data) {

            var options = [];
            options[0] = new Option("-- Select User --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                options[index + 1] = new Option(itemData.Name, itemData.Id, false, itemData.Id == currentUserId);
            });
            if (typeof callback == "function") callback(currentItem, options);


        });
    }
}
