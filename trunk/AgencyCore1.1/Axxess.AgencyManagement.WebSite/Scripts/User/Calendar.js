﻿if (typeof User == "undefined") var User = new Object();
$.extend(User, {
    Calendar: {
        Open: function() {
            Acore.Open("UserSchedule");
        },
        Init: function(r, t, x, e) {
            $(".navigate", e).on("click", function() {
                User.Calendar.Navigate($(this).attr("month"), $(this).attr("year"), $(this).closest(".window-content"));
            });
            $(".print", e).on("click", function() { U.GetAttachment("Schedule/UserCalendarPdf", { month: $(this).attr("month"), year: $(this).attr("year") }); });
            $("div.cal table tbody tr td[tooltip]", e).each(function() {
                U.ToolTipsByElements($(this));
            });
            var gridContent = $(".user-monthly-activity", e);
            if (gridContent) {
                var grid = gridContent.data('tGrid');
                if (grid) {
                    var headers = grid.$header;
                    if (headers.length) {
                        headers.each(function(index, element) {
                            $("a", element).on("click", function() {
                                grid.DoNotNeedRebindIfDataEmpty = true;
                            });
                        });
                    }
                    var serviceName = "HomeHealth";
                    User.InitUserScheduleGrid(grid, serviceName);
                }
            }
        },
        OnDataBinding: function(e) {
            var grid = $(e.currentTarget).data('tGrid');
            if (grid) {
                var url = grid.ajax.selectUrl;
                U.OnDataBindingStopAction(e, { month: U.ParameterByName(url, "month"), year: U.ParameterByName(url, "year") }, "/Schedule/UserVisits");
            }
        },
        OnDataBound: function(e) {
            var serviceName = "HomeHealth";
            U.OnTGridDataBound(e);
            User.InitUserScheduleGrid($(e.target).data('tGrid'), serviceName);
        },
        Navigate: function(m, y, windowContent) {
            windowContent.empty().Load("Schedule/UserCalendarNavigate", { month: m, year: y }, function(r, t, x, e) {
                if (t == "error") e.html(U.AjaxError);
                else User.Calendar.Init(r, t, x, e);
            });
        }
    }
});