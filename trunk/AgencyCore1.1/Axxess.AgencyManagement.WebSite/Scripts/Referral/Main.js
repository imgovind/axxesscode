﻿var Referral = {
    InitEdit: function(r, t, x, e) {
        $("a.addphysician", e).on("click", function() {
            var physician = $("input[name=Physicians]", e);
            if (physician) {
                var physId = physician.val();
                if (U.IsGuid(physId)) Referral.AddPhysician(physId, $("input[name=Id]", e).val(), $(this));
                else U.Growl("Please Select a Physician", "error");
            }
        });
        var form = $("form.mainform", e);
        U.InitValidation(form, function(data) { Referral.RebindGrids(data); });
    },
    InitNew: function(r, t, x, e) {
        var form = $("form.mainform", e);
        U.InitValidation(form, function(data) { Referral.RebindGrids(data); });
    },
    InitNonAdmit: function(r, t, x, e) {
        if ($("[name=ServiceProvided]", e).length > 1) {
            U.ServicesContainerInit("NonAdmit_Referral_ServiceContainer", e);
        }
        U.InitValidation($("#newNonAdmitReferralForm", e), function(data) { Referral.RebindGrids(data) });
    },
    InitDelete: function(r, t, x, e) { U.InitValidation($("#deleteNonAdmitReferralForm", e), function(data) { Referral.RebindGrids(data); }); },
    InitList: function(r, t, x, e) {
        U.ListInitClientRebind(r, t, x, e);
        var newAction = $(".new", e);
        if (newAction) { newAction.click(function() { Referral.New(); }); }
    },
    New: function() { Acore.Open("newreferral"); },
    Edit: function(Id) { Acore.Open("editreferral", { Id: Id }); },
    NonAdmit: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Referral",
            Url: "Referral/NewNonAdmit",
            Input: { referralId: Id },
            OnLoad: Referral.InitNonAdmit,
            Width: 450
        })
    },
    Delete: function(Id) {
        Acore.Modal({
            Name: "Delete Referral",
            Url: "Referral/DeleteView",
            Input: { Id: Id },
            OnLoad: Referral.InitDelete,
            Width: 600
        });
    },
    RebindGrids: function(data) {
        if (data.IsNonAdmitPatientListRefresh) { U.RebindTGrid($('#PatientNonAdmit_Grid')); }
        if (data.IsReferralListRefresh) { Referral.RebindList(); }
    },
    RebindList: function() {
        U.RebindTGrid($("#ReferralsList_Grid"), U.SerializeForm($("fieldset.grid-controls .input", $("#ReferralsList_Grid").closest("div.wrapper"))));
    },
    OnDataBound: function(e) {
        U.OnTGridDataBound(e);
        var grid = $(e.target).data('tGrid');
        if (grid) {
            var datas = grid.data;
            if (datas.length > 0) {
                var data = datas[0];
                if (data != null) {
                    if (data.IsUserCanPrint) grid.showColumn("7");
                    else grid.hideColumn("7");
                    if (data.IsUserCanEdit || data.IsUserCanDelete || data.IsUserCanAdmit || data.IsUserCanNonAdmit) {
                        var width = 0;
                        if (data.IsUserCanEdit) width += 50;
                        if (data.IsUserCanDelete) width += 50;
                        if (data.IsUserCanAdmit) width += 50;
                        if (data.IsUserCanNonAdmit) width += 60;
                        grid.showColumn("8");
                        $(".t-grid-header-wrap colgroup col:eq(8),.t-grid-header-wrap tbody tr th:eq(8), .t-grid-content colgroup col:eq(8) , .t-grid-content tbody tr td:eq(8)", e.target).css({ width: width });
                    }
                    else grid.hideColumn("8");
                }
            }
            else {
                grid.hideColumn("7");
                grid.hideColumn("8");
            }
        }
    },
    Admit: function(id) { Acore.Open("NewPatient", { referralId: id }); },
    AddPhysician: function(id, referralId) {
        U.TGridAjax("Referral/AddPhysician", { id: id, referralId: referralId }, $("#EditReferral_PhysicianGrid"));
    },
    DeletePhysician: function(id, referralId) {
        Acore.Confirm({
            Message: "Are you sure you want to remove this physician?",
            Yes: function() {
                U.TGridAjax("Referral/DeletePhysician", { id: id, referralId: referralId }, $("#EditReferral_PhysicianGrid"));
            }
        })
    },
    SetPrimaryPhysician: function(id, referralId) {
        Acore.Confirm({
            Message: "Are you sure you want to set this physician as primary?",
            Yes: function() {
                U.TGridAjax("Referral/SetPrimaryPhysician", { id: id, referralId: referralId }, $("#EditReferral_PhysicianGrid"))
            }
        })
    }
}