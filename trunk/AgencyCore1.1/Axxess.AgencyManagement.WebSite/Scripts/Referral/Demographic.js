﻿if (typeof Referral == "undefined") var Referral = new Object();
$.extend(Referral, {
    Demographic: {
        Admit: {
            Init: function(r, t, x, e) {
                Episode.PrivateDuty.InitCarePeriodLength(e);
                $("#Admit_Patient_StartOfCareDate", e).on("change", function() {
                    var val = $(this).datepicker("getDate");
                    $("#Admit_Patient_EpisodeStartDate", e).datepicker("setDate", val);
                    $("#Admit_Patient_EpisodeStartDate", e).datepicker("option", "mindate", val);
                });
                Patient.Demographic.InitInsuranceChange(e);
                Patient.Demographic.OnLocationChange(e);
                var element = $(".tabform", e);
                U.InitValidation(element, function(data) { Refresh.RebindPatientGrids(data); });
            },
            Open: function(id) { Acore.Open("admitreferral", { id: id }); }
        }
    }
});