﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    HomeHealth: {
        Activities: {
            OnDataBinding: function(e) {
                U.OnDataBindingStopAction(e, Patient.Charts.Activities.RebindInput(), "/PatientProfile/ActivitySort");
            },
            OnDataBound: function(e) {
                Patient.Charts.Activities.OnDataBound(e);
            },
            OnRowDataBound: function(e) {
                Patient.Charts.Activities.OnRowDataBound(e, "HomeHealth");
            },
            Rebind: function(e) { Patient.Charts.Rebind(e, ""); }
        }
    }
});
