﻿if (typeof Patient.Orders == "undefined") $.extend(Patient, { Orders: new Object() });
$.extend(Patient.Orders, {
    HomeHealth: {
        DeleteOrder: function(id, patientId, episodeId) {
            Patient.Orders.DeleteOrder(id, patientId, episodeId, "/Order/Delete", function(result) {
                Patient.Orders.RebindHistory(patientId, result.ServiceId);
                Patient.Orders.Episode.Refresh(patientId, episodeId);
                Billing.Medicare.Claims.ReLoadFinal();
            });
        }
    }
});