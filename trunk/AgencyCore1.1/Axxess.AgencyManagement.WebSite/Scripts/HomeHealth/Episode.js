﻿if (typeof Episode == "undefined") var Episode = new Object();
$.extend(Episode, {
    HomeHealth: {
        Label: "Episode",
        Prefix: "Episode",
        InitTopMenuNew: function (r, t, x, e) { Episode.InitTopMenuNew(Episode.HomeHealth.Prefix,e); },
        New: function(Id) { Episode.New(Episode.HomeHealth.Prefix, Id); },
        Edit: function(episodeId, patientId) { Episode.Edit(Episode.HomeHealth.Prefix, episodeId, patientId); },
        LoadInactives: function(patientId) { Episode.LoadInactives(Episode.HomeHealth.Prefix, patientId); },
        Activate: function(episodeId, patientId) { Episode.Activate(Episode.HomeHealth.Prefix, episodeId, patientId); },
        ReLoadInactives: function(patientId) { Episode.ReLoadInactives(Episode.HomeHealth.Prefix, patientId); },
        LoadLog: function(episodeId, patientId) { Episode.LoadLog(Episode.HomeHealth.Prefix, episodeId, patientId); },
        LoadDropDown: function(dropDown, control) { Episode.LoadDropDown(Episode.HomeHealth.Prefix, dropDown, control, "Episode"); }
    }
});
