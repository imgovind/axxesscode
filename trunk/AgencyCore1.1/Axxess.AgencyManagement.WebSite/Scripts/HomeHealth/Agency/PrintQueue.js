﻿if (typeof Agency.PrintQueue == "undefined") $.extend(Agency, { PrintQueue: new Object() });
$.extend(Agency.PrintQueue, {
    HomeHealth: {
        WindowId: "printqueue",
        Init: function(r, t, x, e) {
            HomeHealth.RenameWindow(e);
            U.SetArea();
            Agency.PrintQueue.Init(r, t, x, e);

        }
    }
});