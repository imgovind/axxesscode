﻿if (typeof Incident == "undefined") var Incident = new Object();
$.extend(Incident, {
    HomeHealth: {
        Prefix: "HomeHealth",
        New: function(patientId) {
            if (patientId) Acore.Open("newincidentreport", { patientId: patientId });
            else Acore.Open("newincidentreport", 'Incident/New', Incident.InitNew);
        },
        Edit: function(Id) { Acore.Open("editincidentreport", { Id: Id }); }
    }
});