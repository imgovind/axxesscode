﻿if (typeof Billing.Managed.Claims == "undefined") $.extend(Billing.Managed, { Claims: new Object() });
$.extend(Billing.Managed.Claims, {
    HomeHealth: {
        ManagedClaimsWindowId: "createmanagedclaims",
        ReLoadUnProcessedManagedClaim: function(data) { Billing.Managed.Claims.ReLoadUnProcessedManagedClaim(Billing.Managed.Claims.HomeHealth.ManagedClaimsWindowId, data); }
    }
});