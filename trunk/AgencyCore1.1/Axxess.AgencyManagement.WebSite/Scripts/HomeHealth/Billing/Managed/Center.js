﻿if (typeof Billing.Managed.Center == "undefined") $.extend(Billing.Managed, { Center: new Object() });
$.extend(Billing.Managed.Center, {
    HomeHealth: {
        Area: "",
        AcoreId: "managedclaims",
        Init: function(r, t, x, e) { Billing.Managed.Center.Init(r, t, x, e) },
        OnClaimDataBinding: function(e) { Billing.Managed.Center.OnClaimDataBinding(e); },
        OnClaimDataBound: function(e) { Billing.Managed.Center.OnClaimDataBound(e, 1); },
        OnClaimRowDataBound: function(e) { Billing.Managed.Center.OnClaimRowDataBound(e, 1); },
        OnClaimRowSelected: function(e) { Billing.Managed.Center.OnClaimRowSelected(e, Billing.Managed.Center.HomeHealth.Area); },
        ActivityRefresh: function(patientId) { Billing.Managed.Center.ActivityRefresh(Billing.Managed.Center.HomeHealth.AcoreId, patientId); },
        Refresh: function(patientId) {
            U.RebindTGrid($('#ManagedBillingPaymentsGrid'));
            U.RebindTGrid($('#ManagedBillingAdjustmentsGrid'));
            Billing.Managed.Center.HomeHealth.ActivityRefresh("HomeHealth", patientId);
        }
    }
});