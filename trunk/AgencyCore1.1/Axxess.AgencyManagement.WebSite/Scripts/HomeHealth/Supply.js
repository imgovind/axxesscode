﻿if (typeof Supply == "undefined") var Supply = new Object();
$.extend(Supply, {
    HomeHealth: {
        Area: "",
        LoadWorkSheet: function(patientId, eventId) {
            Visit.Supply.LoadWorkSheet(patientId, eventId, Supply.HomeHealth.Area);
        }
    }
});