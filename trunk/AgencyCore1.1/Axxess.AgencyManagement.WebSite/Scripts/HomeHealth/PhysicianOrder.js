﻿if (typeof PhysicianOrder == "undefined") var PhysicianOrder = new Object();
$.extend(PhysicianOrder, {
    HomeHealth: {
        New: function(patientId, episodeId) {
            episodeId = episodeId || U.GuidEmpty;
            Acore.Open("neworder", { patientId: patientId, episodeId: episodeId });
        },
        InitNew: function(r, t, x, e) { PhysicianOrder.InitNew(e, HomeHealth.IsAlone(), "Home Health ", "HomeHealth"); },
        Edit: function(id, patientId) { Acore.Open("editorder", { id: id, patientId: patientId }); },
        InitEdit: function(r, t, x, e) { PhysicianOrder.Init(e, HomeHealth.IsAlone(), "Home Health ", "HomeHealth"); }
    }
});