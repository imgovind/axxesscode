﻿if (typeof CommunicationNote == "undefined") var CommunicationNote = new Object();
$.extend(CommunicationNote, {
    HomeHealth: {
        New: function(patientId) {
            if (patientId) {
                Acore.Open("newcommnote", { patientId: patientId });
            } else {
                Acore.Open("newcommnote");
            }
        },
        LoadList: function(patientId) { Acore.Open("communicationnoteslist", 'CommunicationNote/List', { patientId: patientId }); },
        Delete: function(patientId, Id) { CommunicationNote.Delete(patientId, Id, 'CommunicationNote/Delete', CommunicationNote.Refresh); }
    }
});