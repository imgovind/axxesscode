﻿if (typeof Schedule.Task == "undefined") $.extend(Schedule, { Task: new Object() });
$.extend(Schedule.Task, {
    HomeHealth: {
        Area: "",
        // Delete Home Health Event
        Delete: function(patientId, id) {
            Schedule.Task.Delete(patientId, id, Schedule.Task.HomeHealth.Area);
        },
        // Reopen Home Health Event
        Reopen: function(patientId, id) {
            Schedule.Task.Reopen(patientId, id, Schedule.Task.HomeHealth.Area);
        },
        // Restore Home Health Event
        Restore: function(patientId, id) {
            Schedule.Task.Restore(patientId, id, Schedule.Task.HomeHealth.Area);
        },
        // Open Edit Home Health Event Window
        Edit: function(patientId, id) {
            Acore.Open("scheduledetails", "Schedule/EditDetails", Schedule.Task.HomeHealth.InitEdit, { id: id, patientId: patientId });
        },
        // Initialize Edit Home Health Event Window
        InitEdit: function(r, t, x, e) {
            var form = $("form", e);
            U.ShowIfChecked($("input[name=IsEpisodeReassiged]", e), $("div.episodereassigedcontainer", e));
            $("div.returncommentsdetail", e).ReturnComments({ EventId: $("input[name=Id]", e).val(), EpisodeId: $("input[name=EpisodeId]", e).val(), PatientId: $("input[name=PatientId]", e).val() });
            form.Validate({ Success: function(result) { Refresh.CenterQAMySchedule(result); } });
        },
        // Initialize New Home Health Event Window
        InitNew: function(r, t, x, e) {
            Schedule.Task.HomeHealth.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            U.HideIfChecked($("[name=IsAllDay]", e), $(".time-picker,.clock", e));
            $("form.mainform", e).Validate({ Success: Refresh.CenterQAMySchedule });
        },
        NewMultiple: function(episodeId, patientId) {
            Acore.Modal({
                Name: "Quick Employee Scheduler",
                Url: "Schedule/MultiDay",
                Input: { episodeId: episodeId, patientId: patientId },
                OnLoad: Schedule.Task.InitNewMultiple,
                Width: 900
            });
        },
        Reassign: function(patientId, id) { Schedule.Task.Reassign(patientId, id, Schedule.Task.HomeHealth.Area); },
        MultipleReassign: function(patientId, episodeId) {
            Schedule.Task.MultipleReassign({ episodeId: episodeId ? episodeId : U.GuidEmpty, patientId: patientId }, Schedule.Task.HomeHealth.Area);
        },
        DeleteAsset: function(control, patientId, eventId, assetId) { Schedule.Task.DeleteAsset(control, { patientId: patientId, Id: eventId, assetId: assetId }, Schedule.Task.HomeHealth.Area) },
        GetAttachments: function(patientId, eventId) { Schedule.Task.GetAttachments(patientId, eventId, Schedule.Task.HomeHealth.Area); }
    }
});