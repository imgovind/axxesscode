﻿if (typeof Schedule.Center == "undefined") $.extend(Schedule, { Center: new Object() });
$.extend(Schedule.Center, {
    HomeHealth: {
        AcoreId: "ScheduleCenter",  // Acore Window ID for Home Health Schedule Center
        CookieId: "ScheduleCenter", // Name for cookie to store previous state
        Discipline: "All",          // Current Active Discipline
        EpisodeId: "",              // Current Active Episode
        PatientId: "",              // Current Active Patient
        SchedulerMaxLines: 5,       // Number of entries that can be added into a scheduler tab at one time
        Activities: {
            // Adjust positioning of activities grid based upon height of top panel
            FixPosition: function(e) {
                var top = $(e).closest(".ui-layout-center").find(".top").height() - 19;
                if ($(".above", e).is(":visible")) top += $(".above", e).height();
                $(e).Animate({ top: top }, function() { $(".icon-overlay", e).show() });
            },
            // Hide Icon Legend on Mouse Leave
            HideLegend: function(e) {
                if ($(".t-no-data", e).length == 0) {
                    $(".note-legend", e).Animate({ top: 0 });
                    $(".icon-overlay", e).removeClass("hover");
                }
            },
            // Initialize Activities Grid
            Init: function(r, t, x, e) {
                var bottom = $(".bottom", e);
                // Toggle Activites Grid to Full PAnel and Standard Display
                $(".grid-resize", e).on("click", function() {
                    Schedule.Center.HomeHealth.Activities.Resize(bottom);
                });
                // Filter by Discipline
                $(".schedule-discipline", e).on("change", function() {
                    Schedule.Center.HomeHealth.ChangeDiscipline($(this).val());
                });
                // Toggle Icon Legend On and Off
                $(".icon-overlay,td.icon-overlayed", e).on("mouseenter", function() {
                    Schedule.Center.HomeHealth.Activities.ShowLegend(e);
                }).on("mouseleave", function() {
                    Schedule.Center.HomeHealth.Activities.HideLegend(e);
                }); 
                // Restore Current Discipline
                $(".schedule-discipline > option[value=" + Schedule.Center.HomeHealth.Discipline + "]", e).prop("selected", true);
                Schedule.Center.HomeHealth.Activities.OnFirstDataBound($(".schedule-activity-grid", bottom));
                U.ActivityGridContextMenuInit($(".t-grid", e));
                $(".icon-overlay", bottom).css("width", U.GetWidth($("th.icon-header-overlayed", bottom)) - 2 + "px");
                // Standard Grid Initialization Functions
                //U.ServerBindScheduleActivityInit($(".patient-activity-list", e));
                //U.StopTGridFirstAction($(".patient-activity-list", e));
            },
            // Standard Grid Initialization Functions
            OnDataBinding: function(e) {
                var container = $(e.currentTarget).closest(".ui-layout-center");
                var parameters = {
                    patientId: $("input[name=PatientId]", container).val(),
                    episodeId: $("input[name=EpisodeId]", container).val(),
                    discipline: $("input[name=Discpline]", container).val()
                };
                var url = "/Schedule/ActivitySort";
                U.OnDataBindingStopAction(e, parameters, url);
            },
            // Standard Grid Initialization Functions
            OnDataBound: function(e) {
                U.ClientBindScheduleActivityInit($(e.currentTarget));
            },
            OnFirstDataBound: function(gridContent) {
                var grid = $(gridContent).data('tGrid')
                if (grid) {
                    grid.$tbody.closest(".t-grid-content").removeClass("loading");
                    var trs = $("tr", grid.$tbody);
                    var serviceName = "";
                    if (trs.length > 0) {
                        trs.each(function(index, element) {
                            var dataItem = { IsOrphaned: $(this).hasClass('orphaned'), IsComplete: $(this).hasClass('completed') },
                                gridAction = $(".grid-action", element),
                                menu = gridAction.find(".action-menu");
                            if (menu) gridAction.data("menu", menu.detach());
                            U.ToolTip(element);
                            U.RowColorCode(element, dataItem);
                            U.GridContext(element, dataItem);
                            if (index == 0) {
                                serviceName = $(this).find("td.service").text();
                            }
                            CommandFactory.GridInit(element, { Id: $(this).find("td.id").text(), PatientId: $(this).find("td.pid").text(), EpisodeId: $(this).find("td.eid").text(), Type: $(this).find("td.type").text() }, serviceName);
                        });
                    }
                }
            },
            OnRowDataBound: function(e) {
                var dataItem = e.dataItem;
                if (dataItem) {
                    var init = false;
                    if (dataItem.IsUserCanEdit) init = true;
                    if (dataItem.IsOASISProfileExist) {
                        $("td.oasisprofileprint", e.row).html("<a class=\"OASISprofile img icon16 money\"></a>");
                        init = true;
                    } else {
                        $("td.oasisprofileprint", e.row).html("");
                    }
                    if (dataItem.IsAttachmentExist) {
                        $("td.attachmentload", e.row).html("<a class=\"img icon16 paperclip attachment\"></a>");
                        init = true;
                    }
                    U.ToolTip(e.row);
                    U.RowColorCode(e.row, dataItem);
                    U.GridContext(e.row, dataItem);
                    if (init) CommandFactory.GridInit(e.row, dataItem, "HomeHealth");
                }
            },
            // Rebind Activities Grid
            Refresh: function() {
                var e = $("#window_" + Schedule.Center.HomeHealth.AcoreId + "_content .ui-layout-center .bottom .t-grid"),
                    data = e.data("tGrid");
                if (e && data) {
                    $(".t-grid-content", e).addClass("loading");
                    U.PostUrl("Schedule/Activity", {
                        patientId: Schedule.Center.HomeHealth.PatientId,
                        episodeId: Schedule.Center.HomeHealth.EpisodeId,
                        discipline: Schedule.Center.HomeHealth.Discipline
                    }, function(result) {
                        data.dataBind(result.ScheduleEvents);
                        $(".icon-overlay", e.parent()).css("width", U.GetWidth($("th.icon-header-overlayed", bottom)) - 2 + "px");
                        $(".t-grid-content", e).removeClass("loading");
                    }, function() {
                        $(".t-grid-content", e).removeClass("loading");
                    });
                }
            },
            // Toggle Activites Grid to Full PAnel and Standard Display
            Resize: function(e) {
                if ($(".grid-resize", e).hasClass("grid-expand")) $(e).Animate({ top: $(".above", e).height() }, function() { $(".icon-overlay", e).hide() });
                else Schedule.Center.HomeHealth.Activities.FixPosition(e);
                $(".grid-resize", e).toggleClass("grid-expand grid-reduce");
            },
            // Hide Icon Legend on Mouse Enter
            ShowLegend: function(e) {
                if ($(".t-no-data", e).length == 0) {
                    $(".note-legend", e).Animate({ top: $(".note-legend", e).height() * -1 });
                    $(".icon-overlay", e).addClass("hover");
                }
            }
        },
        // Change the Selected Discipline Filter and Refresh Calendar and Activites
        ChangeDiscipline: function(discipline) {
            Schedule.Center.HomeHealth.Discipline = discipline;
            Schedule.Center.HomeHealth.SaveState();
            Schedule.Center.HomeHealth.RefreshCalendar();
            Schedule.Center.HomeHealth.Activities.Refresh();
        },
        // Initialize Schedule Center
        Init: function(r, t, x, e) {
            // Set layout plugin for patient selector
            $(".layout", e).Layout();
            // Set patient selector filters
            $(".ui-layout-west .top select", e).on("change", function() { Schedule.Center.HomeHealth.PatientSelector.Refresh(e) });
            $(".ui-layout-west .top input", e).on("keyup", function() { Schedule.Center.HomeHealth.PatientSelector.TextFilter(e) });
            // First Patient Load
            if (Acore.Cookies) {
                var cookie = U.Cookie.Get(Schedule.Center.HomeHealth.CookieId);
                if (cookie) {
                    Schedule.Center.HomeHealth.PatientSelector.Init(cookie.PatientId, e);
                    if ($(".patient-list", e).data("tGrid").data) Schedule.Center.HomeHealth.LoadContent(cookie.PatientId, cookie.EpisodeId, cookie.Discipline);
                } else {
                    Schedule.Center.HomeHealth.PatientSelector.Init($("input[name=CurrentPatientId]", e).val(), e);
                    if ($(".patient-list", e).data("tGrid").data) Schedule.Center.HomeHealth.InitContent(r, t, x, $(".ui-layout-center", e));
                }
            } else {
                Schedule.Center.HomeHealth.PatientSelector.Init($("input[name=CurrentPatientId]", e).val(), e);
                Schedule.Center.HomeHealth.InitContent(r, t, x, $(".ui-layout-center", e));
            }
            U.ActivityGridContextMenuInit($("#ScheduleActivityGrid", e));
        },
        // Initialize Calendar Panel
        InitCalendar: function(r, t, x, e) {
            var scheduler = $("#" + Schedule.Center.HomeHealth.AcoreId + "_TabStrip");
            // Set current episode
            Schedule.Center.HomeHealth.EpisodeId = $("input[name=EpisodeId]", e).val();
            Schedule.Center.HomeHealth.SaveState();
            // Set episode navigation controls
            $(".navigate", e).on("click", function() {
                Schedule.Center.HomeHealth.Navigate($(this).attr("guid"), e, $(this).hasClass("previous-episode"));
            });
            $("[name=EpisodeList]", e).on("change", function() {
                Schedule.Center.HomeHealth.Navigate($(this).val(), e);
            });
            // Set button controls
            $(".edit-episode", e).on("click", function() {
                Episode.HomeHealth.Edit(Schedule.Center.HomeHealth.EpisodeId, Schedule.Center.HomeHealth.PatientId);
            });
            $(".refresh", e).on("click", function() {
                Schedule.Center.HomeHealth.LoadContent();
            });
            $(".patient-charts", e).on("click", function() {
                Patient.Charts.Load(Schedule.Center.HomeHealth.PatientId);
            });
            // Set calendar tooltips
            $("[tooltip]", e).each(function() {
                $(this).parent().Tooltip($(this).attr("tooltip"));
            });
            // If scheduler is present
            if (scheduler.length) {
                // Click to add
                $(".datelabel", e).parent().on("click", function() {
                    Schedule.Center.HomeHealth.Scheduler.RowAdd($("a", this).attr("date"));
                });
                // Hide items if scheduler is open
                if (scheduler.is(":visible")) $(".scheduler-toggle", e).hide();
            }
            // Fix activities position
            Schedule.Center.HomeHealth.Activities.FixPosition(e.closest(".ui-layout-center").find(".bottom"));
        },
        // Initialize Content Panel
        InitContent: function(r, t, x, e) {
            // Set top menu
            $(".new-episode", e).on("click", function() {
                Episode.HomeHealth.New(Schedule.Center.HomeHealth.PatientId);
            });
            $(".new-schedules", e).on("click", function() {
                Schedule.Task.HomeHealth.NewMultiple(Schedule.Center.HomeHealth.EpisodeId, Schedule.Center.HomeHealth.PatientId);
            });
            $(".inactive-episodes", e).on("click", function() {
                Episode.HomeHealth.LoadInactives(Schedule.Center.HomeHealth.PatientId);
            });
            $(".episode-frequencies", e).on("click", function() {
                UserInterface.ShowFrequencies(Schedule.Center.HomeHealth.EpisodeId, Schedule.Center.HomeHealth.PatientId);
            });
            $(".master-calendar", e).on("click", function() {
                Schedule.MasterCalendar.Load(Schedule.Center.HomeHealth.PatientId, Schedule.Center.HomeHealth.EpisodeId);
            });
            $(".reassign-schedules", e).on("click", function() {
                Schedule.Task.HomeHealth.MultipleReassign(Schedule.Center.HomeHealth.PatientId, Schedule.Center.HomeHealth.EpisodeId);
            });
            $(".delete-schedules", e).on("click", function() {
                Schedule.Delete.Open(Schedule.Center.HomeHealth.EpisodeId, Schedule.Center.HomeHealth.PatientId);
            });
            $(".window-menu", e).Menu();
            // Run other inits
            if ($(".scheduler", e).length) Schedule.Center.HomeHealth.Scheduler.Init(r, t, x, e);
            Schedule.Center.HomeHealth.InitCalendar(r, t, x, $(".wrapper", e));
            Schedule.Center.HomeHealth.Activities.Init(r, t, x, e);
        },
        // Load new patient info
        LoadContent: function(patientId, episodeId, discipline) {
            Schedule.Center.HomeHealth.PatientId = U.IsGuid(patientId) ? patientId : Schedule.Center.HomeHealth.PatientId;
            Schedule.Center.HomeHealth.EpisodeId = U.IsGuid(episodeId) ? episodeId : Schedule.Center.HomeHealth.EpisodeId;
            Schedule.Center.HomeHealth.Discipline = discipline ? discipline : Schedule.Center.HomeHealth.Discipline;
            Schedule.Center.HomeHealth.SaveState();
            if (Acore.Windows[Schedule.Center.HomeHealth.AcoreId].IsOpen) $(".ui-layout-center", "#window_" + Schedule.Center.HomeHealth.AcoreId).Load("Schedule/Data", { patientId: Schedule.Center.HomeHealth.PatientId, episodeId: Schedule.Center.HomeHealth.EpisodeId, discipline: Schedule.Center.HomeHealth.Discipline }, Schedule.Center.HomeHealth.InitContent, false, true);
            else Acore.Open(Schedule.Center.HomeHealth.AcoreId);
        },
        // Navigate to another episode
        Navigate: function(episodeId, e, previous) {
            var patientSelector = e.closest(".window-content").find(".ui-layout-west"),
                panel = e.closest(".ui-layout-center"),
                style = panel.attr("style"),
                animation = { left: previous ? "100%" : "-100%" },
                intermitentCss = { left: previous ? "-100%" : "100%" };
            patientSelector.css("z-index", 1);
            panel.Animate(animation, function() {
                panel.css(intermitentCss);
                Schedule.Center.HomeHealth.EpisodeId = episodeId;
                Schedule.Center.HomeHealth.SaveState();
                Schedule.Center.HomeHealth.RefreshCalendar();
                Schedule.Center.HomeHealth.Activities.Refresh();
                Schedule.Center.HomeHealth.Scheduler.Reset(panel);
                panel.Animate({ left: 0 }, function() {
                    patientSelector.css("z-index", 0);
                    panel.attr("style", style);
                })
            })
        },
        PatientSelector: {
            // Initialize Patient Selector
            Init: function(patientId, e) {
                if (!e) e = $("#window_" + Schedule.Center.HomeHealth.AcoreId + "_content");
                var grid = $(".patient-list", e);
                if (grid) Schedule.Center.HomeHealth.PatientSelector.OnFirstDataBound(patientId, grid);
            },
            // Standard Grid Initialization Functions
            OnDataBinding: function(e) {
                U.OnDataBindingStopAction(e, $(e.currentTarget).closest(".ui-layout-west").find(".top select").serializeArray(), "Schedule/AllSort");
            },
            // Standard Grid Initialization Functions
            OnDataBound: function(e) {
                var container = $(".t-grid-content", $(e.currentTarget)).removeClass("loading").closest(".layout");
                if (container.length > 0) {
                    $(".ui-layout-center", container).empty();
                    var filterBoxText = $(".ui-layout-west .top input[name=SearchText]", container).val();
                    var gridContentTrs = $(".ui-layout-west .bottom .patient-list .t-grid-content tr", container).not(".t-no-data");
                    if (gridContentTrs.length > 0) {
                        if (Schedule.Center.HomeHealth.PatientId == "") {
                            if (filterBoxText.length) gridContentTrs.filter(".match").eq(0).click()
                            else gridContentTrs.eq(0).click();
                        } else {
                            var tdWithPatientId = $('td:contains(' + Schedule.Center.HomeHealth.PatientId + ')', gridContentTrs);
                            if (tdWithPatientId.length > 0) tdWithPatientId.eq(0).closest('tr').click();
                            else gridContentTrs.eq(0).click();
                        }
                    } else $(".ui-layout-center", container).removeClass("loading").html(U.MessageWarn("No Patients", "No Patients found that fit your search criteria."));
                }
            },
            // Standard Grid Initialization Functions
            OnFirstDataBound: function(patientId, grid) {
                if (grid) {
                    var selectedTr = $("td:contains(" + patientId + ")", grid).closest("tr");
                    if (selectedTr.length) {
                        selectedTr.addClass("t-state-selected");
                        var gridContent = selectedTr.closest(".t-grid-content");
                        if (gridContent) {
                            var position = selectedTr.position();
                            if (position) {
                                var scroll = position.top + gridContent.scrollTop() - 24;
                                if (scroll > 24) gridContent.Animate({ scrollTop: scroll });
                            }
                        }
                    }
                    U.StopTGridFirstAction(grid);
                    Schedule.Center.HomeHealth.PatientId = patientId;
                }
            },

            // When new patient has been selected
            OnRowSelected: function(e) {
                var gridContent = $(e.currentTarget).find(".t-grid-content");
                if (gridContent) {
                    var patientId = $("td.pid", e.row).text();
                    var position = $(e.row).position();
                    if (position) {
                        var scroll = position.top + gridContent.scrollTop() - 24;
                        gridContent.Animate({ scrollTop: scroll });
                    }
                    if (U.IsGuid(patientId)) {
                        Schedule.Center.HomeHealth.PatientId = patientId;
                        Schedule.Center.HomeHealth.EpisodeId = "";
                        Schedule.Center.HomeHealth.Discipline = "All";
                        Schedule.Center.HomeHealth.LoadContent();
                        gridContent.closest(".ui-layout-west").find("input[name=CurrentPatientId]").val(patientId);
                    }
                }
            },
            // Rebind Patient Selector Grid
            Refresh: function(e, input) {
                if (!e) e = $("#window_" + Schedule.Center.HomeHealth.AcoreId);
                if (!input) input = $(".ui-layout-west .filterInput", e).serializeArray();
                U.PostUrl("Schedule/AllSort", input, function(result) {
                    $(".ui-layout-west .t-grid", e).data("tGrid").dataBind(result);
                });
            },
            // Filter Patient List by Text Entry
            TextFilter: function(e, load) {
                // Set search string
                var text = $(".ui-layout-west .top input[name=SearchText]", e).val();
                if (text && text.length) {
                    // Apply search
                    search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    // If set to autoload patient (default), load first listed patient
                    if (load != false) {
                        if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                        else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters", [{ Text: "Add New Patient", Click: function() { Patient.New() } }]));
                    }
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            }
        },
        // Refresh Calendar Data
        RefreshCalendar: function() {
            var e = $("#window_" + Schedule.Center.HomeHealth.AcoreId + "_content .ui-layout-center");
            $(".calendar td", e).empty().attr("class", "inactive");
            $(".wrapper", e).load("Schedule/CalendarNav", {
                patientId: Schedule.Center.HomeHealth.PatientId,
                episodeId: Schedule.Center.HomeHealth.EpisodeId,
                discipline: Schedule.Center.HomeHealth.Discipline
            }, function(r, t, x) { Schedule.Center.HomeHealth.InitCalendar(r, t, x, $(this))})
        },
        Scheduler: {
            // Function for Adding to Schedule
            Add: function(button) {
                var e = $(button).closest(".scheduler"),
                    multiple = $(button).hasClass("multiple"),
                    uri = multiple ? "Schedule/AddMultiple" : "Schedule/Add",
                    input = {};
                e.parent().addClass("loading");
                if (!multiple) {
                    input = [];
                    input.push({ name: "PatientId", value: Schedule.Center.HomeHealth.PatientId });
                    input.push({ name: "EpisodeId", value: Schedule.Center.HomeHealth.EpisodeId });
                    var i = 0;
                    $("ol li", e).each(function() {
                        if ($("[name=DisciplineTask]", this).val() && $("[name=UserId]", this).val() && $("[name=EventDate]", this).val()) {
                            input.push({ name: "Tasks[" + i + "].DisciplineTask", value: $("[name=DisciplineTask]", this).val() });
                            input.push({ name: "Tasks[" + i + "].UserId", value: $("[name=UserId]", this).val() });
                            input.push({ name: "Tasks[" + i + "].EventDate", value: $("[name=EventDate]", this).val() });
                            input.push({ name: "Tasks[" + i + "].Discipline", value: $("[name=DisciplineTask]", this).attr("data") });
                            input.push({ name: "Tasks[" + i + "].IsBillable", value: $("[name=DisciplineTask]", this).attr("isbillable") });
                            i++;
                        }
                    });
                } else input = {
                    PatientId: Schedule.Center.HomeHealth.PatientId,
                    EpisodeId: Schedule.Center.HomeHealth.EpisodeId,
                    DisciplineTask: $("[name=DisciplineTask]", e).val(),
                    UserId: $("[name=UserId]", e).val(),
                    StartDate: $("[name=StartDate]", e).val(),
                    EndDate: $("[name=EndDate]", e).val()
                };
                U.PostUrl(uri, input, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) Schedule.Center.HomeHealth.LoadContent(result.PatientId, result.EpisodeId, "All");
                    else e.parent().removeClass("loading");
                });
            },
            // For date-picking on the current episode
            CalendarModal: function(input) {
                var e = $("#window_" + Schedule.Center.HomeHealth.AcoreId + "_content");
                Acore.Modal({
                    Content: $("<div/>").addClass("wrapper main ac").append(
                        $(".calendar", e).clone()).append(
                        $("<div/>").Buttons([{ Text: "Cancel", Click: function() { $(this).closest(".window").Close() } }])),
                    Width: "75%"
                });
                $("#window_" + Acore.DialogId).SetHeight().find(".datelabel").parent().on("click", function() {
                    $(input).val($("a", this).attr("date"));
                    $(this).closest(".window").Close();
                });
            },
            // Initalize Scheduler
            Init: function(r, t, x, e) {
                var id = "#" + Schedule.Center.HomeHealth.AcoreId + "_TabStrip";
                // Clicking on scheduler tab
                $(".show-scheduler", e).on("click", function() { Schedule.Center.HomeHealth.Scheduler.ShowHide() });
                // Initialize tabs
                $(id).tabs({
                    activate: function(event, ui) {
                        Schedule.Center.HomeHealth.ChangeDiscipline($("a", ui.newTab).attr("discipline") == "Multiple" ? "All" : $("a", ui.newTab).attr("discipline"));
                        if ($("ol li", ui.tab).length) Schedule.Center.HomeHealth.Scheduler.RowAdd();
                    }
                });
                // Setup Multiple Tab
                $("ol li", "#" + Schedule.Center.HomeHealth.AcoreId + "_OutlierTab [type=text]").on("click", function() {
                    Schedule.Center.HomeHealth.Scheduler.CalendarModal(this);
                });
                // Initialize buttons
                $(".schedule-save", e).on("click", function() {
                    Schedule.Center.HomeHealth.Scheduler.Add(this);
                });
                $(".schedule-cancel", e).on("click", function() {
                    Schedule.Center.HomeHealth.Scheduler.Reset(e);
                });
            },
            // Reset Scheduler
            Reset: function(e) {
                $(".scheduler ol li:not(.permanent)", e).remove();
                $(".scheduler .permanent", e).find(":input").val("");
                if ($(".scheduler", e).is(":visible")) Schedule.Center.HomeHealth.Scheduler.ShowHide();
            },
            // Function for Adding Row from Scheduler
            RowAdd: function(date) {
                var e = $("#" + Schedule.Center.HomeHealth.AcoreId + "_TabStrip"),
                    tabContent = $($(".ui-tabs-active > a", e).attr("href"), e),
                    discipline = $(".ui-tabs-active > a", e).attr("discipline");
                // If scheduler is hidden, display it
                if (e.is(":hidden")) Schedule.Center.HomeHealth.Scheduler.ShowHide(date);
                // If empty slot found, set date
                else if ($("input:blank", tabContent).length) $("input:blank:first", tabContent).val(date);
                // Add new row, if space permits, set date on new row
                else if ($("ol li", tabContent).length < Schedule.Center.HomeHealth.SchedulerMaxLines) {
                    if (discipline != "Multiple") {
                        var scheduleContent = $("ol", tabContent);
                        var firstLi = $("li.schedule-row", scheduleContent).first().clone(true);
                        if (firstLi.length > 0) {
                            $("[name=EventDate]", firstLi).val(date);
                            $(scheduleContent).append(firstLi);
                        }
                        else {
                            $(scheduleContent).append(
                            $("<li/>", { "class": "schedule-row" }).append(
                                $("<a/>").addClass("fr img icon22 delete").on("click", function() {
                                    Schedule.Center.HomeHealth.Scheduler.RowRemove(this);
                                })).append(
                                $("<span/>").addClass("grid-third").append(
                                    $("<select/>", { name: "DisciplineTask" }).append(
                                        $("<option/>").val("0").text("-- Select Task --")))).append(
                                $("<span/>").addClass("grid-third").append(
                                    $("<select/>", { name: "UserId" }).append(
                                        $("<option/>").val("0").text("-- Select User --")))).append(
                                $("<span/>").append(
                                    $("<input/>", { type: "text", name: "EventDate", value: date }).on("click", function() {
                                        Schedule.Center.HomeHealth.Scheduler.CalendarModal(this);
                                    })))).Zebra();
                            Lookup.LoadDiscipline($("[name=DisciplineTask]", tabContent), discipline);
                            Lookup.LoadUsersDropDown($("[name=UserId]", tabContent), { branchId: $("#window_" + Schedule.Center.HomeHealth.AcoreId).find(".PatientBranchId.filterInput").val(), statusId: 1, service: HomeHealth.ApplicationService });
                        }
                    }
                } else U.Growl("Can only add " + Schedule.Center.HomeHealth.SchedulerMaxLines + " items at a time in the scheduler", "warning");
            },
            // Function for Removing Row from Scheduler
            RowRemove: function(e) {
                var list = $(e).closest("ol");
                $(e).closest("li").remove();
                if ($("li", list).length == 0) Schedule.Center.HomeHealth.Scheduler.ShowHide();
                else list.Zebra();
            },
            // Function to Toggle the Scheduler
            ShowHide: function(date) {
                var e = $("#window_" + Schedule.Center.HomeHealth.AcoreId + "_content"),
                    scheduler = $("#" + Schedule.Center.HomeHealth.AcoreId + "_TabStrip"),
                    activityPosition = parseInt($(".ui-layout-center .bottom", e).css("top"));
                // If Scheduler is Open, Close It
                if (scheduler.is(":visible")) {
                    // Toggle Show/Hide Text on Tab
                    $(".show-scheduler", e).text("Show Scheduler");
                    // Reshow All the Elements Hidden when Scheduler is Open
                    $(".scheduler-toggle", e).Animate("slideDown", function() {
                        // Match Discipline Drop-Down to Current Discipline
                        $(".schedule-discipline > option[value=" + Schedule.Center.HomeHealth.Discipline + "]", e).prop("selected", true);
                    });
                    // Scroll Activities Up and Over Scheduler
                    $(".ui-layout-center .bottom", e).Animate({ top: activityPosition - 25 }, function() {
                        // Hide Scheduler
                        scheduler.hide();
                        // Adjust Activites Grid Position for New Top Panel Height
                        Schedule.Center.HomeHealth.Activities.FixPosition($(".ui-layout-center .bottom", e));
                    });
                    // If Scheduler is Closed, Open It
                } else {
                    // Toggle Show/Hide Text on Tab
                    $(".show-scheduler", e).text("Hide Scheduler");
                    // Show Scheduler
                    scheduler.show();
                    // Active Tab Per Current Discipline
                    if (Schedule.Center.HomeHealth.Discipline == "Nursing") scheduler.tabs("option", "active", 0);
                    if (Schedule.Center.HomeHealth.Discipline == "HHA") scheduler.tabs("option", "active", 1);
                    if (Schedule.Center.HomeHealth.Discipline == "MSW") scheduler.tabs("option", "active", 2);
                    if (Schedule.Center.HomeHealth.Discipline == "Therapy") scheduler.tabs("option", "active", 3);
                    if (Schedule.Center.HomeHealth.Discipline == "Dietician") scheduler.tabs("option", "active", 4);
                    if (Schedule.Center.HomeHealth.Discipline == "Orders") scheduler.tabs("option", "active", 5);
                    // If No Rows on Current Tab, Add One
                    if ($($(".ui-tabs-selected > a", e).attr("href")).find("ol > li").length == 0) Schedule.Center.HomeHealth.Scheduler.RowAdd(date);
                    // Hide All the Elements Hidden when Scheduler is Open
                    $(".scheduler-toggle", e).Animate("slideUp");
                    // Scroll Activites to Show Scheduler
                    $(".ui-layout-center .bottom", e).Animate({ top: activityPosition + 25 }, function() {
                        // Adjust Activites Grid Position for New Top Panel Height
                        Schedule.Center.HomeHealth.Activities.FixPosition($(".ui-layout-center .bottom", e));
                    })
                }
            }
        },
        // Set Cookie for Current Schedule Center State Variables
        SaveState: function() {
            if (Acore.Cookies) U.Cookie.Set(Schedule.Center.HomeHealth.CookieId, {
                PatientId: Schedule.Center.HomeHealth.PatientId,
                EpisodeId: Schedule.Center.HomeHealth.EpisodeId,
                Discipline: Schedule.Center.HomeHealth.Discipline
            })
        }
    }
});