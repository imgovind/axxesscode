﻿if (typeof MissedVisit == "undefined") var MissedVisit = new Object();
$.extend(MissedVisit, {
    HomeHealth: {
        Open: function(missedVisitId) {
            MissedVisit.Open(missedVisitId, HomeHealth.Area);
        },
        New: function(patientId, eventId) {
            MissedVisit.New(patientId, eventId, HomeHealth.Area);
        },
        Edit: function(eventId) {
            MissedVisit.Edit(eventId, HomeHealth.Area);
        },
        Restore: function(episodeId, patientId, eventId) {
            var input = { patientId: patientId, eventId: eventId };
            MissedVisit.Restore("MissedVisit/Restore", input, function(data) {
                MissedVisit.Refresh();
                Refresh.All(data);
            });
        }
    }
});