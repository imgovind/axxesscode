﻿if (typeof HomeHealth == "undefined") var HomeHealth = new Object();
$.extend(HomeHealth, {
    ApplicationService: 1,
    IsActive: false,
    Area: "",
    SetActive: function(service) { this.IsActive = (service & this.ApplicationService) === this.ApplicationService; },
    IsAlone: function() {
        return Acore.ApplicationServices == HomeHealth.ApplicationService;
    },
    RenameWindow: function(e, addPrefix) {
        if (Acore.ApplicationServices != HomeHealth.ApplicationService) {
            e.Rename("Home Health | " + e.parent().find(".title").text());
            if (addPrefix) U.SetPrefix(e, "HomeHealth");
        }
    }
});