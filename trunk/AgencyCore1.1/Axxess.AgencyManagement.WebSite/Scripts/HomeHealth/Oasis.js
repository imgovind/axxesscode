﻿if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    HomeHealth: {
        AcoreExportedId: "oasisExported",
        AcoreNotExportedId: "oasisNotExported",
        Delete: function(cont, id) { Oasis.Delete(cont, id, "Oasis/Delete"); },
        GenerateSubmit: function(form) {
            if (form) {
                if ($("input[name=OasisSelected]:checked", form).length > 0) {
                    if ($("[name=BranchId]", form).val() != "00000000-0000-0000-0000-000000000000") {
                        form.submit();
                    }
                }
                else { U.Growl("Select at least one OASIS assessment to generate.", "error"); }
            }
        },
        MarkAsExported: function(inputs, type) {
            if (inputs.length > 0) {
                inputs[inputs.length] = { name: "StatusType", value: type };
                U.PostUrl('Oasis/MarkExported', inputs, function(data) {
                    U.Growl(data.errorMessage, data.isSuccessful ? "success" : "error");
                    if (data.isSuccessful) {
                        Refresh.All(data);
                        Refresh.OASISGrids(data);
                    }
                });
            }
        },
        MarkAsSubmited: function(control, type) {
            var flag = (type === "Exported");
            var inputs = $("input[name=OasisSelected]:checked", $(control)).serializeArray();
            if (inputs.length > 0) {
                Acore.Confirm({
                    Message: flag ? "Are you sure that this OASIS file has been accepted by CMS?<br/><em>Please verify that your OASIS submission was accepted by CMS (Center for Medicare &#38; Medicaid Services) before you complete this step. If this OASIS file was not accepted, do not mark as exported.</em>" : "Are you sure that you want mark this OASIS Completed without Export?",
                    Width: 700,
                    Yes: function() { Oasis.HomeHealth.MarkAsExported(inputs, type) }
                });
            }
            else U.Growl("Select at least one OASIS assessment to mark as " + (flag ? "Exported." : "Completed ( Not Exported )."), "error");
        },
        RebindExported: function(e) { U.RefreshOpenGrid(e, Oasis.HomeHealth.AcoreExportedId); },
        RebindNotExported: function(e) { U.RefreshOpenGrid(e, Oasis.HomeHealth.AcoreNotExportedId); },
        ShowCorrectionNumberModal: function(Id, patientId, correctionNumber) {
            Acore.Modal({
                Name: "Correction Number",
                Url: "Oasis/Correction",
                Input: { Id: Id, PatientId: patientId, CorrectionNumber: correctionNumber },
                OnLoad: Oasis.HomeHealth.InitEditCorrectionNumber,
                Width: "675px",
                WindowFrame: false
            });
        },
        LoadDiagnosisData: function(patientId, episodeId, e) {
            U.PostTrackedUrl("Oasis/GetDiagnosisData", { patientId: patientId, episodeId: episodeId }, function(data) {
                if (data.dataFound) {
                    $(".primary-diagnosis", e).text(data.diag1);
                    $(".secondary-diagnosis", e).text(data.diag2);
                    if (data.code1 === '') $("#ICD9M").remove();
                    else $(".primary-diagnosis", e).append($("<a/>", { id: "ICD9M", "class": "img icon16 book", href: "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code1 + "&informationRecipient.languageCode.c=en", target: "_blank" }));
                    if (data.code2 === '') $("#ICD9M1").remove();
                    else $(".secondary-diagnosis", e).append($("<a/>", { id: "ICD9M1", "class": "img icon16 book", href: "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + data.code2 + "&informationRecipient.languageCode.c=en", target: "_blank" }));
                    $(".diagnoses", e).removeClass("hidden");
                } else $(".diagnoses", e).addClass("hidden");
            })
        },
        InitEditCorrectionNumber: function(r, t, x, e) {
            $("form.mainform", e).validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: "json",
                        beforeSubmit: function() { },
                        success: function(result) {
                            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                            if (result.isSuccessful) {
                                Acore.CloseDialog();
                                Refresh.OASISGrids(result);
                            } 
                        },
                        error: function(result) { }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            })
        },
        HandleRefresh: function(result) {
            try {
                window.parent.Refresh.All(result);
                window.parent.Refresh.OASISGrids(result);
            } catch (e) { }
        },
        Export: {
            AcoreId: "oasisExport",
            Init: function(r, t, x, e) {
                $(".export,.print", e).on("click", function() {
                    var input = U.GetGridFilterParameters(e);
                    U.GetAttachment($(this).attr("url"), input);
                    return false;
                });
                var pageName = $(".pagename", e).val();
                var refresh = $(".grid-refresh", e);
                var url = "";
                if (refresh.length) {
                    url = refresh.attr("url");
                    refresh.on("click", function() {
                        Oasis.HomeHealth.Export.RefreshHelper(e, pageName, url);
                        return false;
                    });
                }
                Oasis.HomeHealth.Export.InitContent(e, url, pageName);
                $("select.location", e).on("change", function() { $(".grid-refresh", e).click(); });
            },
            RefreshHelper: function(e, pageName, url, sortParams) {
                var paymentSource = U.MultiSelectInput($("[name=PaymentSources]")).join()
                if (paymentSource && paymentSource.length > 0) {
                    if (!sortParams) {
                        sortParams = $("input[name=SortParams]", e).val();
                    }
                    U.RebindDataGridContent(pageName, url, { BranchId: $("[name=BranchId]", e).val(), PaymentSources: paymentSource, SortParams: sortParams }, function() { Oasis.HomeHealth.Export.InitContent(e, url, pageName); });
                }
                else {
                    U.Growl("Select at least one payment sources.", "error");
                }
            },
            InitContent: function(e, url, pageName) {
                $("a.generate", e).on("click", function() { Oasis.HomeHealth.GenerateSubmit($(this).closest("form")); });
                $("a.marksubmit", e).on("click", function() { Oasis.HomeHealth.MarkAsSubmited($(this).closest("form"), $(this).attr("value")); });
                var grid = $(".grid-container", e);
                var selectAll = $(".selectall", e);
                if (selectAll.length) { selectAll.on("change", function() { $(".grid-container .t-grid-content input[name='OasisSelected']", e).each(function() { $(this).prop("checked", selectAll.prop("checked")); }) }); }
                $(".t-grid-content", grid).css({ "height": "auto", "position": "absolute", "top": "25px" });
                $("div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link", e).each(function() {
                    var link = $(this).attr("href");
                    $(this).on("click", function() {
                        Oasis.HomeHealth.Export.RefreshHelper(e, pageName, url, U.ParameterByName(link, pageName + "_Grid-orderBy"));
                    });
                });
            },
            Rebind: function(e) { U.RefreshOpenGrid(e, Oasis.HomeHealth.Export.AcoreId); }
        }
    }
});