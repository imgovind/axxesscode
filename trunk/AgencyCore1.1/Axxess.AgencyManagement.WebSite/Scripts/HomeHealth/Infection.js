﻿if (typeof Infection == "undefined") var Infection = new Object();
$.extend(Infection, {
    HomeHealth: {
        Prefix: "HomeHealth",
        New: function(patientId) {
            if (patientId) Acore.Open("newinfectionreport", { patientId: patientId });
            else Acore.Open("newinfectionreport");
        },
        Edit: function(Id) { Acore.Open("editinfectionreport", { Id: Id }); },
        InitNew: function(r, t, x, e) {
            //TODO Loading Diagnoses must be added to Private Duty
            $("[name=EpisodeId]", e).on("change", function() { Oasis.HomeHealth.LoadDiagnosisData($("select[name=PatientId]", e).val(), $(this).val(), e); });
            Infection.InitNew(r, t, x, e);
        }
    }
});