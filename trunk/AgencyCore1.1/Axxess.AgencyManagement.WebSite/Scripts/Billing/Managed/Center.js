﻿if (typeof Billing.Managed == "undefined") $.extend(Billing, { Managed: new Object() });
$.extend(Billing.Managed, {
    Center: {
        Area: "",
        Init: function(r, t, x, e) { Billing.Center.Init(e); },
        PatientListDataBound: function(e) { Billing.Center.PatientListDataBound(e); },
        OnPatientRowSelected: function(e) { Billing.Center.OnPatientRowSelected(e); },
        OnClaimDataBinding: function(e) { Billing.Center.OnClaimDataBinding(e); },
        OnClaimDataBound: function(e, serviceId) { Billing.Center.OnClaimDataBound(e, "Managed", serviceId); },
        OnClaimRowDataBound: function(e, serviceId) { Billing.Center.OnClaimRowDataBound(e, "Managed", serviceId); },
        OnClaimRowSelected: function(e, area) { Billing.Center.OnClaimRowSelected(e, area); },
        ActivityRefresh: function(winId, patientId) { Billing.Center.ActivityRefresh(winId, patientId); }
    }
});