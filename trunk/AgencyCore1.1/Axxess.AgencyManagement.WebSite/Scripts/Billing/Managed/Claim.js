﻿if (typeof Billing.Managed == "undefined") $.extend(Billing, { Managed: new Object() });
$.extend(Billing.Managed, {
    Claim: {
        New: function(patientId, serviceId) { Billing.Claim.New({ patientId: patientId }, 3, serviceId); },
        Edit: function(patientId, Id, serviceId) { Billing.Claim.Edit({ patientId: patientId, id: Id }, 3, serviceId); },
        Delete: function(patientId, id, serviceId) { Billing.Claim.Delete({ patientId: patientId, id: id }, 3, serviceId); },
        Open: function(id, patientId, serviceId) { Billing.Claim.Open({ Id: id, patientId: patientId }, 3, serviceId); },
        GeneratedSingle: function(id, area) { Billing.LoadGeneratedSingle(id, area + "/Billing/CreateSingleManagedANSI"); },
        Supply: {
            Edit: function(claimId, patientId, id, area) { Billing.Supply.Edit(claimId, patientId, id, "ManagedCare", area); }
        }

    }
});