﻿if (typeof Billing.Managed == "undefined") $.extend(Billing, { Managed: new Object() });
$.extend(Billing.Managed, {
    Payment: {
        New: function(Id, serviceId) {
            var area = window[Acore.AppService(serviceId || HomeHealth.ApplicationService)].Area;
            Acore.Modal({
                Name: "Add Claim Payment",
                Url: area + "/Billing/NewManagedClaimPayment",
                Input: { id: Id },
                OnLoad: Billing.Managed.Payment.InitNew,
                Width: "500px",
                WindowFrame: false
            });
        },
        Edit: function(Id, serviceId) {
            var area = window[Acore.AppService(serviceId || HomeHealth.ApplicationService)].Area;
            Acore.Modal({
                Name: "Update Claim Payment",
                Url: area + "/Billing/UpdateManagedClaimPayment",
                Input: { id: Id },
                OnLoad: Billing.Managed.Payment.InitEdit,
                Width: "500px",
                WindowFrame: false
            });
        },
        InitNew: function(r, t, x, e) { U.InitTemplate($("form.mainform", e), function(data) { Billing.Managed.Center[data.Service].Refresh(data.PatientId); }, "Payment was created successfully.") },
        InitEdit: function(r, t, x, e) { U.InitTemplate($("form.mainform", e), function(data) { Billing.Managed.Center[data.Service].Refresh(data.PatientId); }, "Payment was updated successfully.") },
        OpenList: function(Id, patientId, serviceId) {
            var prefix = Billing.PrefixById[serviceId] || "";
            Acore.Open(prefix + "managedclaimpayments", { ClaimId: Id, patientId: patientId });
        },
        Delete: function(patientId, claimId, id, serviceId) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this payment?",
                Yes: function() {
                    var area = window[Acore.AppService(serviceId || HomeHealth.ApplicationService)].Area;
                    if (!area) area = "";
                    U.PostUrl(area + "/Billing/DeleteManagedClaimPayment", { patientId: patientId, claimId: claimId, id: id }, function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Billing.Managed.Center[result.Service].Refresh(patientId);
                        }
                        else {
                            U.Growl(result.errorMessage, "error");
                        }
                    });
                }
            }); 
        }
    }
});