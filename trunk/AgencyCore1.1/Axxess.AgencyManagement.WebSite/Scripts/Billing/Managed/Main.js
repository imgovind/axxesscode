﻿if (typeof Billing.Managed == "undefined") $.extend(Billing, { Managed: new Object() });
$.extend(Billing.Managed, {
    _sort: {},
    //Used for when payments or adjustments are databound
    OnRowWithCommentsDataBound: function(e) { U.ToolTip(e.row);}
});