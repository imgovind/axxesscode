﻿if (typeof Billing.Managed == "undefined") $.extend(Billing, { Managed: new Object() });
$.extend(Billing.Managed, {
    Claims: {
        ReLoadUnProcessedManagedClaim: function(winId, data) {
            if (data.IsManagedCareRefresh) {
                if (Acore.Windows[winId].IsOpen) { $("div.wrapper .grid-controls a.grid-refresh", $("#window_" + winId)).click(); }
            }
        }
    }
});