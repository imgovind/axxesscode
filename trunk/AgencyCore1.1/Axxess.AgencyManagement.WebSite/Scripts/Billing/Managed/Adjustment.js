﻿if (typeof Billing.Managed == "undefined") $.extend(Billing, { Managed: new Object() });
$.extend(Billing.Managed, {
    Adjustment: {
        New: function(Id, serviceId) {
            var area = window[Acore.AppService(serviceId || HomeHealth.ApplicationService)].Area;
            Acore.Modal({
                Name: "Add Claim Adjustment",
                Url: area + "/Billing/NewManagedClaimAdjustment",
                Input: { id: Id },
                OnLoad: Billing.Managed.Adjustment.InitNew,
                Width: "500px",
                WindowFrame: false
            });
        },
        Edit: function(Id, serviceId) {
            var area = window[Acore.AppService(serviceId || HomeHealth.ApplicationService)].Area;
            Acore.Modal({
                Name: "Update Claim Adjustment",
                Url: area + "/Billing/UpdateManagedClaimAdjustment",
                Input: { id: Id },
                OnLoad: Billing.Managed.Adjustment.InitEdit,
                Width: "400px",
                WindowFrame: false
            });
        },
        InitNew: function(r, t, x, e) { U.InitTemplate($("form.mainform", e), function(data) { Billing.Managed.Center[data.Service].Refresh(data.PatientId); }, "Adjustment was created successfully."); },
        InitEdit: function(r, t, x, e) { U.InitTemplate($("form.mainform", e), function(data) { Billing.Managed.Center[data.Service].Refresh(data.PatientId); }, "Adjustment was updated successfully."); },
        OpenList: function(Id, patientId, serviceId) {
            var prefix = Billing.PrefixById[serviceId] || "";
            Acore.Open(prefix + "managedclaimadjustments", { ClaimId: Id, patientId: patientId });
        },
        Delete: function(patientId, claimId, id, serviceId) {
            if (confirm("Are you sure you want to delete this adjustment?")) {
                var area = window[Acore.AppService(serviceId || HomeHealth.ApplicationService)].Area;
                U.PostUrl(area + "/Billing/DeleteManagedClaimAdjustment", { patientId: patientId, claimId: claimId, id: id}, function(result) {
                    if (result.isSuccessful) {
                        U.Growl(result.errorMessage, "success");
                        Billing.Managed.Center[result.Service].Refresh(patientId);
                    }
                    else {
                        U.Growl(result.errorMessage, "error");
                    }
                });
            }
        }
    }
});