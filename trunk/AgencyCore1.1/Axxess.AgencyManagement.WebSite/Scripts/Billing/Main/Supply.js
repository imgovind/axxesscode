﻿if (typeof Billing == "undefined") var Billing = new Object();
$.extend(Billing, {
    Supply: {
        InitNew: function(responseText, textStatus, xmlHttpRequest, element) { Billing.Supply.InitCommon(element, "Supply created successfully."); },
        InitEdit: function(responseText, textStatus, xmlHttpRequest, element) { Billing.Supply.InitCommon(element, "Supply updated successfully."); },
        InitCommon: function(element, message) {
            Supply.Search(element);
            element.find("input[name=Quantity]").bind('input', function(e) {
                var units = $(this).val();
                var cost = element.find("input[name=UnitCost]").val();
                if (!isNaN(units) && !isNaN(cost)) {
                    var total = units * cost;
                    element.find("input[name=TotalCost]").val(U.FormatMoney(total));
                }
            });
            element.find("input[name=UnitCost]").bind('input', function(e) {
                var cost = $(this).val();
                var units = element.find("input[name=Quantity]").val();
                if (!isNaN(units) && !isNaN(cost)) {
                    var total = units * cost;
                    element.find("input[name=TotalCost]").val(U.FormatMoney(total));
                }
            });
            U.InitTemplate($("form", element), function() { Billing.Supply.RebindList($("." + $("input[name=SupplyListIdentifier]", element).val() + "supply")); }, message);
        },
        New: function(input, area) {
            if (!area) area = "";
            Acore.Modal({
                Name: "New Supply",
                Url: area + "/Billing/NewSupplyBillable",
                Input: input,
                OnLoad: Billing.Supply.InitNew,
                Width: "750px",
                Height: "310px",
                WindowFrame: false
            });
        },
        Edit: function(claimId, patientId, id, type, area) {
            if (!area) area = "";
            Acore.Modal({
                Name: "Edit Supply",
                Url: area + "/Billing/EditSupplyBillable",
                Input: { ClaimId: claimId, PatientId: patientId, Id: id, Type: type },
                OnLoad: Billing.Supply.InitEdit,
                Width: "750px",
                Height: "310px",
                WindowFrame: false
            });
        },
        ChangeBillableStatus: function(claimInformationInput, control, isBillable, area, callback) {
            if (!area) area = "";
            var inputs = $(":input", $(control)).serializeArray();
            if (inputs.length < 1) {
                Acore.Alert('Select at least one supply before clicking on the ' + (isBillable ? '"Mark As Billable"' : '"Mark As Non-Billable"') + 'button.');
                return;
            } else {
                if (inputs) {
                    $.merge(inputs, claimInformationInput);
                    inputs[inputs.length] = { name: "IsBillable", value: isBillable };
                    U.PostUrl(area + "/Billing/ChangeSupplyBillStatus", inputs, function(result) {
                        if (typeof callback === "function") callback();
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            var params = U.JsonInput(claimInformationInput);
                            $(control).closest(".tab-content").find(".t-grid").each(function() { U.RebindTGrid($(this), params); });
                        }
                    })
                }
            }
        },
        Delete: function(claimInformationInput, control, area, beforePostCallback, callback) {
            if (!area) area = "";
            var inputs = $(":input", $(control)).serializeArray();
            if (inputs.length < 1) {
                Acore.Alert('Select at least one supply before clicking on the "Delete" button.');
                return;
            } else Acore.Confirm({
                Message: "Are you sure you want to delete this Supply/Supplies?",
                Yes: function() {
                    if (inputs != null) {
                        $.merge(inputs, claimInformationInput);
                        if (typeof beforePostCallback === "function") beforePostCallback();
                        U.PostUrl(area + "/Billing/SuppliesDelete", inputs, function(result) {
                            if (typeof callback === "function") callback();
                            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                            if (result && result.isSuccessful) U.RebindTGrid($(control).closest(".t-grid"), U.JsonInput(claimInformationInput));
                        })
                    }
                }
            })
        },
        RowSelected: function(e) { $("[type=checkbox]", e.row).click(); },
        RebindList: function(locator) {
            U.RebindTGrid($(locator));
        }
    }
});