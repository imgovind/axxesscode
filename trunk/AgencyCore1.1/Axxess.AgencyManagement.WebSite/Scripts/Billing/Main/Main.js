﻿if (typeof Billing == "undefined") var Billing = new Object();
$.extend(Billing, {
    _sort: {},
    AreaByName: { "HomeHealth": "", "PrivateDuty": "PrivateDuty" },
    AreaById: { 1: "", 2: "PrivateDuty" },
    PrefixById: { 1: "", 2: "PD" },
    ClaimCategory: new Array("NotApplicable", "RAP", "Final", "Managed", "Secondary"),
    LoadInsuranceContent: function(contentId, patientId, insuranceId, startDate, endDate, claimTypeIdentifier) {
        $(contentId).load('Billing/InsuranceInfoContent', {
            PatientId: patientId,
            InsuranceId: insuranceId,
            StartDate: startDate,
            EndDate: endDate,
            ClaimTypeIdentifier: claimTypeIdentifier
        }, function(r, t, x, e) { $("input[name=Authorization]", e).on("change", function() { Billing.LoadAuthorizationContent($("div.authorizationcontent", e), $(this).val()) }); });
    },
    LoadAuthorizationContent: function(contentId, authorizationId) {
        if (authorizationId != U.GuidEmpty) {
            $(contentId).load('Billing/AuthorizationContent', { AuthorizationId: authorizationId }, function(responseText, textStatus, XMLHttpRequest) {
            });
        } else {
            $(contentId).find("input").val("");
        }
    },
    ReloadInsuranceData: function(claimId, patientId, type) {
        Acore.Confirm({
            Message: "Any edits you have made to the visit rates or locators will be lost. Are you sure you want to reload the insurance?",
            Yes: function() {
                $("#" + type + "Verification_TabStrip div.ui-tabs-panel:visible").Load(Billing[type].Claim.Area + "/Billing/" + type + "ClaimReloadInsurance", { Id: claimId, PatientId: patientId }, Billing[type].Claim.Sections.Insurance);
            }
        });
    },
    Init: function(r, t, x, e) {
        var location = $(".location", e);
        var claimType = $(".claimtype", e);
        var insurance = $(".insurance", e);
        location.on("change", function() {
            Billing.InsuranceRebindHelper(insurance, claimType, $(this), true);
            $("a.grid-refresh", e).click();
        });

        claimType.on("change", function() {
            Billing.InsuranceRebindHelper(insurance, $(this), location, true);
            $("a.grid-refresh", e).click();
        });
        insurance.on("change", function() { $("a.grid-refresh", e).click(); });
        Billing.InsuranceRebindHelper(insurance, claimType, location, false);
        $(".printclaims", e).off("click");
        $(".printclaims", e).on("click", function() {
            U.GetAttachment($(this).attr("url"), U.GetGridFilterParameters(e));
            return false;
        });
        $(".export", e).off("click");
        $(".export", e).on("click", function() {
            U.GetAttachment($(this).attr("url"), U.GetGridFilterParameters(e));
            return false;
        });
        var pageName = $("[name=PageName]", e).val();
        $(".grid-refresh", e).on("click", function() {
            U.RebindDataGridContent(pageName, $(this).attr("url"), U.GetGridFilterParameters(e), Billing.InitContent);
            return false;
        });
        Billing.InitContent(r, t, x, $("#" + pageName + "GridContainer", e));
    },
    InitContent: function(r, t, x, e) {
        //        No Data found for current filter.
        if (!e.html()) e.html(U.MessageInfo('No Claims Found', 'No claims could be found for current filters. Change the filters and try again.'));
        $(".content ol li:first", e).each(function() { $(this).addClass("first") });
        $(".content ol li:last", e).each(function() { $(this).addClass("last"); });
        $(".subprint", e).off("click");
        $(".subprint", e).on("click", function() {
            U.GetAttachment($(this).attr("url"), U.JsonInput(Billing.ContentInput($(this))));
            return false;
        });
        $(".subexport", e).off("click");
        $(".subexport", e).on("click", function() {
            U.GetAttachment($(this).attr("url"), U.JsonInput(Billing.ContentInput($(this))));
            return false;
        });
        $(".submitclaim", e).on("click", function() {
            Billing.LoadGenerate($(this).closest(".subcontent"), $(this).attr("claimtype") + "claimsummary");
            return false;
        });
        $(".submitallclaim", e).on("click", function() {
            Billing.GenerateAllCompleted($(this).closest(".subcontent"), $(this).attr("claimtype") + "claimsummary");
            return false;
        });
    },
    ContentInput: function(control) { return $(control).closest(".subcontent").find(":input.subinput").serializeArray(); },
    GenerateInput: function(control) { return $(control).closest(".content").find("input.input, input[name=ClaimSelected]").serializeArray(); },
    LoadGenerate: function(control, pageId) {
        var input = $(":input.subinput, input[name=ClaimSelected]:checked", $(control));
        if (!input.not(".subinput").length) U.Growl("At least one claim must be selected.", "error");
        else Acore.Open(pageId, input.serializeArray());
    },
    GenerateAllCompleted: function(control, pageId) {
        $("input[name=ClaimSelected]", $(control)).each(function() { $(this).attr("checked", true) });
        Billing.LoadGenerate(control, pageId);
    },
    GenerateInit: function(r, t, x, e) {
        $(".submit ,.updatestatus ,.download", e).on("click", function() {
            Billing.GenerateAction($(this));
            return false;
        });
    },
    GenerateAction: function(control) {
        var btn = $(control),
            input = Billing.GenerateInput(control);
        btn.trigger("ProcessingStart");
        U.PostUrl(btn.attr("url"), input, function(result) {
            if (result.isSuccessful) {
                if (result.isDownload) jQuery('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                else {
                    U.Growl(result.errorMessage, "success");
                    $(control).closest(".window").Close();
                    Billing.Category[Billing.ClaimCategory[input["ClaimType"]]].Refresh(result);
                }
                btn.trigger("ProcessingComplete");
            } else {
                U.Growl(result.errorMessage, "error");
                btn.trigger("ProcessingComplete");
            }
        }, null);
    },
    LoadGeneratedSingle: function(id, url) {
        U.PostUrl(url, { Id: id }, function(result) {
            if (result != null) {
                if (result.isSuccessful) {
                    $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                } else U.Growl(result.errorMessage, "error");
            }
        });
    },
    //isRebind flag used to identify weather to mark the first option selected or not
    InsuranceRebindHelper: function(insurance, claimType, location, isRebind) {
        var locationPayor = location.is('select') ? $("option:selected", location).attr("payor") : location.attr("payor");
        var claimTypeName = claimType.is('select') ? $("option:selected", claimType).attr("billtype") : claimType.attr("billtype");
        if (claimTypeName == "medicare") {
            insurance.find("option").hide();
            insurance.find("option[itype=medicaretraditional][value =" + (locationPayor != undefined ? locationPayor : 0) + " ],option[itype=medicarehmo],option[itype=default]").show();
            if (isRebind != undefined && isRebind) {
                $("option:first-child", insurance).prop("selected", "selected");
            }
        } else if (claimTypeName == "managedcare") {
            insurance.find("option").hide();
            insurance.find("option:not([itype=medicaretraditional])").show();
            if (isRebind != undefined && isRebind) {
                $("option:first-child", insurance).prop("selected", "selected");
            }
        }
    },
    NoPatientsFound: function(centerLayout) {
        RequestTracker.AbortAllGridRequests($(".bottom .activity-container", centerLayout).attr("id"));
        var bottom = $(".bottom", centerLayout);
        $(".activity-container .t-grid-content tbody", bottom).empty();
        bottom.find(".error-box").remove();
        $(".top .claim-information, .top .window-menu", centerLayout).Animate({ opacity: "0" });
        bottom.Animate({ top: "110%" }, function() { $(".top", centerLayout).removeClass("loading").html(U.MessageInfo("No Results", "No Patients found that fit your search criteria.", "")); });
    },
    Category: {
        RAP: {
            Refresh: function(result) {
                if (result.IsMedicareHistoryRefresh) {
                    Billing.Medicare.Center.ActivityRefresh(result.PatientId);
                }
                if (result.IsRAPRefresh) {
                    Billing.Medicare.Claims.ReLoadUpProcessedClaimOnAction(result);
                }
                if (result.IsPedingRAPRefresh || result.IsPedingFinalRefresh) {
                    Billing.Medicare.Pending.Refresh(result.IsPedingRAPRefresh, result.IsPedingFinalRefresh);
                }
            }
        },
        Final: {
            Refresh: function(result) {
                if (result.IsMedicareHistoryRefresh) {
                    Billing.Medicare.Center.ActivityRefresh(result.PatientId);
                }
                if (result.IsFinalRefresh) {
                    Billing.Medicare.Claims.ReLoadUpProcessedClaimOnAction(result);
                }
                if (result.IsPedingRAPRefresh || result.IsPedingFinalRefresh) {
                    Billing.Medicare.Pending.Refresh(result.IsPedingRAPRefresh, result.IsPedingFinalRefresh);
                }
            }
        },
        Managed: {
            Refresh: function(result) {
                if (result.IsManagedCareRefresh || result.IsManagedCareHistoryRefresh) {
                    if (result.IsManagedCareHistoryRefresh) {
                        Billing.Managed.Center[result.Service].ActivityRefresh(result.PatientId);
                    }
                    if (result.IsManagedCareRefresh) {
                        Billing.Managed.Claims[result.Service].ReLoadUnProcessedManagedClaim(result);
                    }
                }
            }
        },
        Secondary: {
            Refresh: function(result) {
                // if (result.IsSecondaryHistoryRefresh) { }
                if (result.IsSecondaryRefresh) {
                    Billing.Secondary.Center.RebindActivity(result.PatientId);
                }
            }
        }
    }
});
