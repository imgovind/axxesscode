﻿if (typeof Billing == "undefined") var Billing = new Object();
$.extend(Billing, {
    Claim: {
        New: function (input, typeId, serviceId) {
            var type = Billing.ClaimCategory[typeId];
            var area = Billing.AreaById[serviceId];
            if (!area) area = "";
            Acore.Modal({
                Name: "New Claim",
                Url: area + "/Billing/New" + type + "Claim",
                Input: input,
                OnLoad: Billing.Claim.InitNew,
                Width: 525
            });
        },
        Edit: function (input, typeId, serviceId) {
            var type = Billing.ClaimCategory[typeId];
            var area = Billing.AreaById[serviceId];
            if (!area) area = "";
            Acore.Modal({
                Name: "Update Status of Claim",
                Url: area + "/Billing/Update" + type + "Claim",
                Input: input,
                OnLoad: Billing.Claim.InitEdit,
                Width: 525
            });
        },
        Delete: function (input, typeId, serviceId) {
            var type = Billing.ClaimCategory[typeId];
            var area = Billing.AreaById[serviceId];
            U.Delete("Claim", area + "/Billing/Delete" + type + "Claim", input, function (data) { Billing.Category[Billing.ClaimCategory[data.Category]].Refresh(data); });
        },
        InitNew: function (r, t, x, e) {
            if ($(".no-claimless-episode-alert", e).length) {
                Acore.Alert("All episodes for this patient have claims associated with them. You must delete existing claims before creating new one.", "exclamation");
                e.hide();
                e.Close();
            } else if ($(".no-claim-type-alert", e).length) {
                Acore.Alert("Claim type is not identified, please select a claim type and try again.", "exclamation");
                e.hide();
                e.Close();
            } else U.InitTemplate($("form.mainform", e), function (data) { Billing.Category[Billing.ClaimCategory[data.Category]].Refresh(data); }, "Claim is created successfully.");
        },
        InitEdit: function (r, t, x, e) {
            $("select.status", e).change(function () {
                var selectValue = $(this).val();
                if (selectValue == "305" || selectValue == 3005) {
                    var now = new Date();
                    $(".claimdate", e).val(U.FormatDate(now));
                }
            });
            U.InitTemplate($("form.mainform", e), function (data) { Billing.Category[Billing.ClaimCategory[data.Category]].Refresh(data); }, "Claim updated successfully.");
        },
        Open: function (input, typeId, serviceId) {
            var type = Billing.ClaimCategory[typeId];
            var prefix = Billing.PrefixById[serviceId];
            Acore.Open(prefix + type.toLowerCase() + "claimedit", input);
        },
        //Opens the window for selecting an associated episode for a Managed Claim
        LoadEpisodeOptions: function (input, area) {
            Acore.Modal({
                Name: "Found Multiple Episodes in your date range",
                Url: area + "/Billing/ManagedClaimEpisodes",
                Input: input,
                OnLoad: Billing.Claim.InitEpisodeOptions,
                Width: "500px",
                Height: "200px",
                WindowFrame: false
            });
        },
        InitEpisodeOptions: function (r, t, x, e) {
            $("form", e).Validate({
                BeforeSubmit: function () {
                    var result = $("#Managed_MultipleEpisodes_EpisodeId").val() == null;
                    if (result) U.Growl("An episode must be selected. If there are no episodes to select, than the date range specified for this claim does not contain an episode.", "error");
                    return !result;
                },
                AfterSubmit: function (managedClaim) {
                    if (managedClaim.AssessmentType) $("#ManagedClaim_AssessmentType").val(managedClaim.AssessmentType);
                    if (managedClaim.ClaimKey) $("#ManagedClaim_ClaimKey").val(managedClaim.ClaimKey);
                    if (managedClaim.HippsCode) $("#ManagedClaim_HippsCode").val(managedClaim.HippsCode);
                    if (managedClaim.ProspectivePay) $("#ManagedClaim_ProspectivePay").val(managedClaim.prospectivePay);
                    if (managedClaim.DiagnosisCode1) $("#ManagedClaim_DiagnosisCode1").val(managedClaim.DiagnosisCode1);
                    if (managedClaim.DiagnosisCode2) $("#ManagedClaim_DiagnosisCode2").val(managedClaim.DiagnosisCode2);
                    if (managedClaim.DiagnosisCode3) $("#ManagedClaim_DiagnosisCode3").val(managedClaim.DiagnosisCode3);
                    if (managedClaim.DiagnosisCode4) $("#ManagedClaim_DiagnosisCode4").val(managedClaim.DiagnosisCode4);
                    if (managedClaim.DiagnosisCode5) $("#ManagedClaim_DiagnosisCode5").val(managedClaim.DiagnosisCode5);
                    if (managedClaim.DiagnosisCode6) $("#ManagedClaim_DiagnosisCode6").val(managedClaim.DiagnosisCode6);
                }
            });
        },
        //Visit Tab Initialization shared b/w Managed and Secondary Claims
        VisitTabInit: function (e) {
            var perDayLine = $(".day-per-line", e);
            if (perDayLine) {
                perDayLine.each(function (index, Element) {
                    var checkbox = $(Element);
                    checkbox.closest(".main-line-item").find("ol input").each(function (index2, actualVisitCheckbox) {
                        var jActualVisitCheckbox = $(actualVisitCheckbox);
                        jActualVisitCheckbox.change(function () {
                            if (jActualVisitCheckbox.prop("checked") == false) {
                                checkbox.prop('checked', false);
                            }
                            var secondarySection = jActualVisitCheckbox.closest(".main-line-item ol");
                            var totalCount = secondarySection.find("input").length;
                            var checkedCount = secondarySection.find("input:checked").length;
                            if (totalCount == checkedCount) {
                                checkbox.prop('checked', true);
                            }
                        });
                    });
                    checkbox.change(function () {
                        if (checkbox.prop("checked") == true) {
                            checkbox.parent().parent().next().find("input").each(function (index2, secondary) {
                                $(secondary).prop('checked', true);
                            });
                        } else {
                            checkbox.parent().parent().next().find("input").each(function (index2, secondary) {
                                $(secondary).prop('checked', false);
                            });
                        }
                    });
                });
            }
            var discpilineGeneral = $(".discpilinegeneral", e);
            if (discpilineGeneral) {
                discpilineGeneral.change(function () {
                    if ($(this).attr("checked")) {
                        $(this).closest("ol").find(":checkbox").attr("checked", true);
                    } else {
                        $(this).closest("ol").find(":checkbox").attr("checked", false);
                    }
                });
            }
        },
        //Info Tab(first tab) initialization for Finals, Managed, and Secondary claims.
        InfoTabInit: function (e) {
            var status = $("select[name=UB4PatientStatus]", e);
            var field = $(".ub4patientstatuscontent", e);
            U.HideIfSelectEquals(status, "0|30", field);
            var hasEpisodes = $("input[name=HasMultipleEpisodes]");
            if (hasEpisodes && hasEpisodes.val() == "True") {
                var form = $(e).find("form");
                Billing.Claim.LoadEpisodeOptions($("input.input", form).serializeArray(), form.attr("area"));
            }
            $("div.column.hidden").hide();
        },
        //Supply Tab initialization for Finals, Managed, and Secondary claims.
        SupplyTabInit: function (e, input, newSupplyFunction, area) {
            $(".add-supply-button", e).on("click", function () { newSupplyFunction(input, area); });
            $(".delete-supply-button", e).on("click", function () {
                var btn = $(this);
                Billing.Supply.Delete(input, $(this).closest(".row").find(".t-grid"), area, function () {
                    btn.trigger("ProcessingStart");
                }, function () {
                    btn.trigger("ProcessingComplete");
                });
            });
            $(".unbill-supply-button", e).on("click", function () {
                var btn = $(this).trigger("ProcessingStart");
                Billing.Supply.ChangeBillableStatus(input, $(this).closest(".row").find(".t-grid"), false, area, function () {
                    btn.trigger("ProcessingComplete");
                });
            });
            $(".bill-supply-button", e).on("click", function () {
                var btn = $(this).trigger("ProcessingStart");
                Billing.Supply.ChangeBillableStatus(input, $(this).closest(".row").find(".t-grid"), true, area, function () {
                    btn.trigger("ProcessingComplete");
                });
            });
            //$(".t-grid .t-grid-content", e).css({ 'height': 'auto', 'position': 'relative', 'top': '0' });
        },
        Sections: {
            Init: function (r, t, x, e) {
                var tab = $(".maintab", e);
                var postData = U.JsonInput($(".tabinput", tab).serializeArray());
                tab.tabs({
                    beforeLoad: function (event, ui) {
                        ui.ajaxSettings.type = 'POST';
                        ui.ajaxSettings.hasContent = true;
                        ui.jqXHR.setRequestHeader("Content-Type", ui.ajaxSettings.contentType);
                        ui.ajaxSettings.data = jQuery.param(postData);
                        ui.panel.addClass("loading tab-content");

                    },
                    load: function (event, ui) {
                        var category = $("a", ui.tab).attr("name");
                        ui.panel.removeClass("loading");
                        Billing.Claim.Sections[category](ui.panel);
                    }
                });
                // $(tab).tabs("disable");
                U.DisableTabs(tab);
            },
            InitSection: function (e) {
                Acore.OnLoad(e);
                if (!$(e).is("form")) e = $(e).find("form");
                U.Navigate(e, function (result) { Billing.Category[Billing.ClaimCategory[result.Category]].Refresh(result); });
            },
            Info: function (e) {
                Billing.Claim.InfoTabInit(e);
                Billing.Claim.Sections.InitSection(e);
            },
            Insurance: function (e) {
                $("a.newrate", e).on("click", function () { Billing.Rate.New(e); });
                $("a.reload", e).on("click", function () { Billing.Rate.Reload(e); });
                $("a.reloadrates", e).on("click", function () { Billing.Rate.ReloadRates(e); });
                $("select[name=Authorization]", e).change(function () { Billing.LoadAuthorizationContent($("div.authorizationcontent", e), $(this).val()) });
                U.ShowIfSelectEquals($("select.relationship", e), "G8", $("input.otherrelationship", e));
                var grid = $(".claim-rates", e);
                Billing.Rate.EditDeleteBind($("form", e), grid);
                U.StopTGridFirstAction(grid)
                Billing.Claim.Sections.InitSection(e);
            },
            Visit: function (e) {
                Billing.Claim.VisitTabInit(e);
                Billing.Claim.Sections.InitSection(e);
            },
            Remittance: function (e) {
                $("li .required", e).closest("input").after("<span class='required-red'>*</span>");
                $("input.adjustment-amount", e).change(function () {
                    var sum = 0;
                    $(".adjustment-amount", e).each(function () {
                        sum += Number($(this).val());
                    });
                    $(".totaladjustmentamount", e).text(U.FormatMoney(sum));
                });
                $(".floatnum", e).floatnum();
                $(".adjustment-add-button", e).on("click", function () {
                    var index = $(this).attr("val");
                    var list = $(this).parent().find(".acore-grid > ol");
                    var numberOfRows = $(list).find("li").length;
                    if (numberOfRows == 0) {
                        list.closest(".acore-grid").show();
                    }
                    var lastAdjustment = $(list).find("li:last");
                    lastAdjustment.removeClass("last");
                    list.append('<li class="adjustment-item last">' +
                        '<span class="grid-quarter"><label for="[' + index + '].Value[' + numberOfRows + '].AdjGroup" class="strong">Group Code</label> <input id="[' + index + '].Value[' + numberOfRows + '].AdjGroup" type="text" name="[' + index + '].Value[' + numberOfRows + '].AdjGroup" class="shorter required"/><span class="required-red">*</span></span>' +
                        '&nbsp;<span class="grid-quarter"><label for="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjReason" class="strong">Reason Code</label> <input id="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjReason" type="text" name="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjReason" class="shorter required"/><span class="required-red">*</span></span>' +
                        '&nbsp;<span class="grid-quarter"><label for="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjAmount" class="strong">Amount</label> $<input id="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjAmount" type="text" name="[' + index + '].Value[' + numberOfRows + '].AdjData.AdjAmount" class="shorter floatnum adjustment-amount required"/><span class="required-red">*</span></span>' +
                        '<span class="delete fr">&nbsp;<div onclick="Billing.Secondary.Claim.DeleteAdjustment($(this).parent().parent());" class="t-icon t-delete"/></div></span></li>');
                    //Acore.OnLoad(e);
                    //$(".floatnum", $(this).parent().parent().find("ol li.last")).floatnum();
                });
                Billing.Claim.Sections.InitSection(e);
            },
            Supply: function (e) {
                var input = $("input", e).serializeArray();
                Billing.Claim.SupplyTabInit(e, input, Billing.Supply.New, $(e).find("form").attr("area"));
                Billing.Claim.Sections.InitSection(e);
            },
            Summary: function (e) { Billing.Claim.Sections.InitSection(e); }
        }
    }
});