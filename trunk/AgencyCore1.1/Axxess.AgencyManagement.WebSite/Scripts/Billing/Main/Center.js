﻿if (typeof Billing == "undefined") var Billing = new Object();
$.extend(Billing, {
    Center: {
        Claims: {
            // Adjust positioning of activities grid based upon height of top panel
            FixPosition: function(e) {
                var top = $(e).closest(".ui-layout-center").find(".top").height() - 19;
                if ($(".above", e).is(":visible")) top += $(".above", e).height();
                $(e).Animate({ top: top }, function() { $(".icon-overlay", e).show() });
            },
            // Initialize Activities Grid
            Init: function(r, t, x, e) {
                // Toggle Activites Grid to Full PAnel and Standard Display
                $(".grid-resize", e).on("click", function() {
                    Billing.Center.Claims.Resize($(".bottom", e));
                });
            },
            // Rebind Activities Grid
            Refresh: function() {
            },
            // Toggle Activites Grid to Full PAnel and Standard Display
            Resize: function(e) {
                if ($(".grid-resize", e).hasClass("grid-expand")) $(e).Animate({ top: $(".above", e).height() });
                else Billing.Center.Claims.FixPosition(e);
                $(".grid-resize", e).toggleClass("grid-expand grid-reduce");
            }
        },
        Init: function(e) {
            $(".top input.searchtext", e).keyup(function() {
                var girdContent = $(".ui-layout-west .bottom  div.t-grid-content", e);
                if ($(this).val() != undefined && $(this).val().length) {
                    U.Filter($(this).val(), girdContent);
                    if ($("tr.match", girdContent).length) $("tr.match:first", girdContent).click();
                    else Billing.NoPatientsFound($(".ui-layout-center", e));
                } else {
                    $("tr", girdContent).removeClass("match t-alt").show();
                    $("tr:even", girdContent).addClass("t-alt");
                    girdContent.find("tr:first").click();
                }
            });
            $(".ui-layout-west .top select.filterInput", e).on("change", function() {
                Billing.Center.RebindPatientList(e, Billing.Center.RebindInput(e));
            });
            $('.layout', e).Layout();
            var location = $(".ui-layout-west .top .location", e);
            var insurance = $(".ui-layout-west .top .insurance", e);
            var activityinsurance = $(".ui-layout-center .bottom select.activity-input", e);
            location.on("change", function() {
                Billing.Center.InsuranceRebindHelper(insurance, $(this));
                if (activityinsurance) Billing.Center.InsuranceRebindHelper(activityinsurance, $(this));
            });
            Billing.Center.InsuranceRebindHelper(insurance, location);
            if (activityinsurance) {
                $(insurance).children().clone().removeAttr("selected").appendTo(activityinsurance);
                activityinsurance.on("change", function() {
                    Billing.Center.RebindActivity(e, Billing.Center.ActivityInput(e));
                });
            }
            var top = 0;
            $(".ui-layout-west .top div.center-filter", e).each(function() {
                top += Math.max($("div.filter-fill", this).outerHeight(true), $("label.fl", this).outerHeight(true));
            });
            $('.ui-layout-west .bottom', e).css("top", top);
        },
        RebindInput: function(e) { return $(".ui-layout-west .top .filterInput", e).serializeArray(); },
        RebindPatientList: function(e, input) {
            var patientGrid = $(".patientlist-container", e).data('tGrid');
            if (patientGrid != null) { patientGrid.rebind(U.JsonInput(input)); }
        },
        PatientListDataBound: function(e) {
            U.OnTGridDataBound(e);
            var rows = $(".t-grid-content tbody tr:not(.t-no-data)", e.target);
            if (rows != undefined && rows.length > 0) {
                var patientId = $(e.target).closest(".bottom").find("input[name=PatientId].activity-input").val();
                if (patientId == U.GuidEmpty) {
                    rows.eq(0).click();
                }
                else {
                    var row = $('td:contains(' + patientId + ')', e.target).closest('tr');
                    if (row.length > 0) {
                        row.click();
                    }
                    else {
                        rows.eq(0).click();
                    }
                }
            } else Billing.NoPatientsFound($(e.target).closest(".layout").find(".ui-layout-center"));
        },
        OnPatientRowSelected: function(e) {
            var layout = $(e.target).closest(".layout");
            var patientId = $("td.pid", e.row).text();
            $(".ui-layout-west .bottom input[name=PatientId].activity-input", layout).val(patientId);
            $(".ui-layout-center .top .claim-information", layout).remove();
            var gridContent = $(e.target).find(".t-grid-content");
            var scroll = $(e.row).position().top + gridContent.scrollTop() - 24;
            gridContent.Animate({ scrollTop: scroll });
            if (patientId != U.GuidEmpty) {
                Billing.Center.RebindActivity(layout, Billing.Center.ActivityInput(layout));
            }
            else Billing.NoPatientsFound($(".ui-layout-center", layout));
        },
        OnClaimDataBinding: function(e) {
            if (e.target != e.currentTarget) return false;
            var val = U.ParameterByName($(e.currentTarget).data('tGrid').ajax.selectUrl, "PatientId");
            if (val == U.GuidEmpty) e.preventDefault();
            else {
                U.OnTGridDataBinding(e);
                var container = $(e.currentTarget).closest(".ui-layout-center");
                $(".top", container).empty();
            }
        },
        // mainCategory stands for Managed, Medicare, Secondary
        OnClaimDataBound: function(e, mainCategory, serviceId) {
            if (e.target != e.currentTarget) return false;
            U.OnTGridDataBound(e);
            var target = e.target;
            var rows = $(".t-grid-content tbody tr:not(.t-no-data)", target);
            if (rows != undefined && rows.length > 0) {
                var grid = $(target).data('tGrid');
                if (grid) {
                    var headerWrapperRow = $(".t-grid-header-wrap tbody tr", target);
                    var count = $("th.t-hierarchy-cell.t-header", headerWrapperRow).length || 0;
                    var datas = grid.data;
                    if (datas.length > 0) {
                        var data = datas[0];
                        if (data != null) {
                            //Set defaults for data
                            $.extend({ IsUserCanPrint: false, IsUserCanEdit: false, IsUserCanDelete: false}, data);
                            var printIndex = $("th.print", headerWrapperRow).index() - count;
                            var ubPrintIndex = $("th.ubprint", headerWrapperRow).index() - count;
                            var actionIndex = $("th.action", headerWrapperRow).index() - count;
                            var editIndex = $("th.edit", headerWrapperRow).index() - count;
                            if (data.IsUserCanPrint) grid.showColumn(ubPrintIndex);
                            else grid.hideColumn(ubPrintIndex);
                            if (data.IsUserCanPrint) grid.showColumn(printIndex);
                            else grid.hideColumn(printIndex);
                            if (data.IsUserCanEdit || data.IsUserCanDelete) grid.showColumn(actionIndex);
                            else grid.hideColumn(actionIndex);
                            if (data.IsUserCanEdit) grid.showColumn(editIndex);
                            else grid.hideColumn(editIndex);
                        }
                    } else {
                        grid.hideColumn($("th.print", headerWrapperRow).index() - count);
                        grid.hideColumn($("th.ubprint", headerWrapperRow).index() - count);
                        grid.hideColumn($("th.action", headerWrapperRow).index() - count);
                        grid.hideColumn($("th.edit", headerWrapperRow).index() - count);
                    }
                }
                rows.eq(0).click();
 				$(target).closest(".ui-layout-center").find(".grid-resize").show();
            } else {
                var area = Billing.AreaById[serviceId];
                if (mainCategory == "Medicare") {
                    RequestTracker.Abort(area + "/Billing/RAPSnapShotClaimInfo");
                    RequestTracker.Abort(area + "/Billing/FinalSnapShotClaimInfo");
                } else if (mainCategory == "Managed") RequestTracker.Abort(area + "/Billing/ManagedSnapShotClaimInfo");
                var layout = $(e.target).closest(".layout"),
                    layoutCenter = $(".ui-layout-center", layout),
                    layoutWest = $(".ui-layout-west", layout),
                    patientId = $(".bottom input[name=PatientId].activity-input", layoutWest).val(),
                    insurance = $("select[name=InsuranceId].activity-input option", layout).filter(":selected"),
                    insuranceText = insurance.val() == 0 ? " all insurnaces" : insurance.text(),
                    buttons = [];
                $(".top", layoutCenter).removeClass("loading").empty();
                $(".bottom .above", layoutCenter).css({ top: 0 });
                $(".bottom", layoutCenter).Animate({ top: 0 }).find(".activity-container").hide();
                if (mainCategory == "Medicare") buttons.push({
                    Text: "Create RAP",
                    Click: function() { Billing.Medicare.Claim.New(patientId, 1) }
                }).push({
                    Text: "Create Final",
                    Click: function() { Billing.Medicare.Claim.New(patientId, 2) }
                });
                else if (mainCategory == "Managed") buttons.push({ Text: "Create Claim", Click: function() { Billing.Managed.Claim.New(patientId, serviceId) } });
                $(".grid-resize", layoutCenter).hide();
                $(".bottom", layoutCenter).append(U.MessageInfo("No Claims", "No Claims could be found for " + insuranceText + ".", buttons));
            }
        },
        OnClaimRowDataBound: function(e, mainCategory, serviceId) {
            var dataItem = e.dataItem;
            //Set defaults for dataItem
            $.extend({ IsUserCanPrint: false, IsUserCanEdit: false, IsUserCanDelete: false, InvoiceType: 0, IsUserCanGenerate: false, IsVisitVerified: false, IsInfoVerified: false, IsSupplyVerified: false }, dataItem);
            var action = [];
            var url = "";
            if (mainCategory == "Medicare") {
                if (dataItem.IsUserCanPrint) {
                    if (dataItem.Type == 1) $("td.print", e.row).html(
                        $("<a/>").addClass("img icon print").on("click", function() {
                            U.GetAttachment("Billing/RapPdf", { episodeId: dataItem.EpisodeId, patientId: dataItem.PatientId })
                        }));
                    else if (dataItem.Type == 2) $("td.print", e.row).html(
                        $("<a/>").addClass("img icon print").on("click", function() {
                            U.GetAttachment("Billing/FinalPdf", { episodeId: dataItem.EpisodeId, patientId: dataItem.PatientId })
                        }));
                    else $("td.print", e.row).html("");
                } else $("td.print", e.row).html("");
                if (dataItem.IsUserCanPrint) {
                    if (dataItem.InvoiceType == 1) $("td.ubprint", e.row).html(
                        $("<a/>").text("UB-04").on("click", function() {
                            U.GetAttachment("Billing/UB04Pdf", { patientId: dataItem.PatientId, Id: dataItem.Id, type: dataItem.Type })
                        }));
                    else if (dataItem.InvoiceType == 2) $("td.ubprint", e.row).html(
                        $("<a/>").text("HCFA-1500").on("click", function() {
                            U.GetAttachment("Billing/HCFA1500Pdf", { patientId: dataItem.PatientId, Id: dataItem.Id, type: dataItem.Type })
                        }));
                    else $("td.ubprint", e.row).html("");
                } else $("td.ubprint", e.row).html("");
                if (dataItem.IsUserCanEdit || dataItem.IsUserCanDelete) {
                    if (dataItem.IsUserCanEdit) action.push("<a class=\"link\" onclick=\"Billing.Medicare.Claim.Edit('" + dataItem.PatientId + "','" + dataItem.Id + "','" + dataItem.Type + "')\">Edit</a>");
                    if (dataItem.IsUserCanDelete) action.push("<a class=\"link\" onclick=\"Billing.Medicare.Claim.Delete('" + dataItem.PatientId + "','" + dataItem.Id + "','" + dataItem.Type + "')\">Delete</a>");
                    url = $.map(action, function(val) { return val; }).join("");
                    $("td.action", e.row).html(url);
                } else $("td.action", e.row).html("");
            }
            else if (mainCategory == "Managed") {
                if (dataItem.IsUserCanPrint || dataItem.IsUserCanGenerate) {
                    if (dataItem.IsVisitVerified && dataItem.IsInfoVerified && dataItem.IsSupplyVerified) {
                        var area = Billing.AreaById[serviceId];
                        var anchor = [];
                        if (dataItem.IsUserCanPrint) anchor.push("<a class=\"img icon16 print\" onclick=\"U.GetAttachment('" + area + "/Billing/ManagedClaimPdf',{patientId:'" + dataItem.PatientId + "',Id:'" + dataItem.Id + "'})\"></a>");
                        if (dataItem.IsUserCanGenerate) anchor.push("<a class=\"img icon16 paperclip\" onclick=\"Billing.Managed.Claim.GeneratedSingle('" + dataItem.Id + "','" + area + "')\"></a>");
                        url = $.map(anchor, function(val) { return val; }).join("");
                        $("td.print", e.row).html(anchor);
                    } else $("td.print", e.row).html("");
                }
                if (dataItem.IsUserCanEdit || dataItem.IsUserCanDelete) {
                    if (dataItem.IsUserCanEdit) {
                        action.push("<a class=\"link\" onclick=\"Billing.Managed.Claim.Edit('" + dataItem.PatientId + "','" + dataItem.Id + "','" + serviceId + "')\">Edit</a>");
                        var edit = "<a onclick=\"Billing.Managed.Claim.Open('" + dataItem.Id + "','" + dataItem.PatientId + "','" + serviceId + "')\">Open Claim</a>"
                        $("td.edit", e.row).html(edit);
                    } else $("td.edit", e.row).html("");
                    if (dataItem.IsUserCanDelete) action.push("<a class=\"link\" onclick=\"Billing.Managed.Claim.Delete('" + dataItem.PatientId + "','" + dataItem.Id + "','" + serviceId + "')\">Delete</a>");
                    url = $.map(action, function(val) { return val; }).join("");
                    $("td.action", e.row).html(url);
                } else {
                    $("td.action", e.row).html("");
                    $("td.edit", e.row).html("");
                }
            }
        },
        OnClaimRowSelected: function(e, area) {
            var row = e.row,
                claimType = $("td.ctype", row).text(),
                claimId = $("td.cid", row).text(),
                patientId = $("td.pid", row).text();
            Billing.Center.LoadClaimInfo($(e.target).closest(".ui-layout-center").find(".top"), { PatientId: patientId, ClaimId: claimId }, claimType, area);
        },
        LoadClaimInfo: function(control, input, typeId, area) {
            var type = Billing.ClaimCategory[typeId];
            $(control).Load(area + "/Billing/" + type + "SnapShotClaimInfo", input, function() {
                Billing.Center.PositionBottom($(control));
                $(control).closest(".ui-layout-center").find(".grid-resize").on("click", function() {
                    Billing.Center.ResizeGrid($(control).closest(".ui-layout-center").find(".bottom"));
                });
            }, false, true);
        },
        PositionBottom: function(e) {
            var top = $(e).height(),
                bottom = $(e).next(".bottom"),
                above = bottom.find(".above");
            if (above) top += above.height();
            bottom.Animate({ top: top });
        },
        ResizeGrid: function(e) {
            if ($(".grid-resize", e).hasClass("grid-expand")) $(e).Animate({ top: $(".above", e).height() });
            else Billing.Center.PositionBottom(e.closest(".ui-layout-center").find(".top"));
            $(".grid-resize", e).toggleClass("grid-expand grid-reduce");
        },
        ActivityInput: function(e) {
            return U.JsonInput($(".activity-input", e).serializeArray());
        },
        RebindActivity: function(e, input) {
            var bottom = $(".ui-layout-center .bottom", e);
            $(".error-box.info", bottom).remove();
            $(".above", bottom).css("top", "");
            var grid = $(".activity-container", bottom).show().data('tGrid');
            if (grid != null) grid.rebind(input);
        },
        ActivityRefresh: function(winId, patientId) {
            if (Acore.Windows[winId].IsOpen) {
                var container = $("#window_" + winId);
                if (container) {
                    var input = Billing.Center.ActivityInput($(".layout ", container));
                    if (input != undefined && input["PatientId"] == patientId) {
                        Billing.Center.RebindActivity(container, input);
                    }
                }
            }
        },
        InsuranceRebindHelper: function(insurance, location) {
            var locationPayor = location.is("select") ? $("option:selected", location).attr("payor") : location.attr("payor");
            insurance.find("option").show();
            insurance.find("option[itype=medicaretraditional][value!=" + (locationPayor ? locationPayor : 0) + "]").hide();
        }
    }
});