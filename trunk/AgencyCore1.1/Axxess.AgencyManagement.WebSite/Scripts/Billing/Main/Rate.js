﻿if (typeof Billing == "undefined") var Billing = new Object();
$.extend(Billing, {
    Rate: {
        New: function (e) {
            var inputs = U.JsonInput($("input", e).serializeArray());
            if (inputs) {
                var type = Billing.ClaimCategory[inputs["TypeId"]];
                var area = Billing.AreaById[inputs["ServiceId"]];
                Acore.Modal({
                    Name: "Visit Bill Info.",
                    Url: area + "/Billing/" + type + "ClaimNewBillData",
                    Input: inputs,
                    OnLoad: Billing.Rate.InitNew,
                    Width: "650px",
                    WindowFrame: false
                });
            }
        },
        Edit: function (inputs) {
            if (inputs) {
                var type = Billing.ClaimCategory[inputs["TypeId"]];
                var area = Billing.AreaById[inputs["ServiceId"]];
                Acore.Modal({
                    Name: "Visit Bill Info.",
                    Url: area + "/Billing/" + type + "ClaimEditBillData",
                    Input: inputs,
                    OnLoad: Billing.Rate.InitEdit,
                    Width: "650px",
                    WindowFrame: false
                });
            }
        },
        InitCommon: function (r, t, x, e) {
            Rate.InitCommon(r, t, x, e);
        },
        InitNew: function (r, t, x, e) {
            Billing.Rate.InitCommon(r, t, x, e);
            U.InitTemplate($("form.mainform", e), function (result) { if (result.isSuccessful) { Billing.Rate.RebindList(result.Service.toLowerCase() + "-" + Billing.ClaimCategory[result.Category].toLowerCase()); } }, "Visit rate successfully saved");
        },
        InitEdit: function (r, t, x, e) {
            Billing.Rate.InitCommon(r, t, x, e);
            U.InitTemplate($("form.mainform", e), function (result) { if (result.isSuccessful) { Billing.Rate.RebindList(result.Service.toLowerCase() + "-" + Billing.ClaimCategory[result.Category].toLowerCase()); } }, "Visit rate successfully updated");
        },
        RebindList: function (className) { U.RebindTGrid($("." + className + ".claim-rates")) },
        Delete: function (inputs) {
            if (inputs) {
                var type = Billing.ClaimCategory[inputs["TypeId"]];
                var area = Billing.AreaById[inputs["ServiceId"]];
                Acore.Confirm({
                    Message: "Are you sure you want to delete this visit rate?",
                    Yes: function () {
                        U.PostUrl(area + "/Billing/" + type + "ClaimDeleteBillData", inputs, function (result) {
                            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                            if (result.isSuccessful) Billing.Rate.RebindList(result.Service.toLowerCase() + "-" + Billing.ClaimCategory[result.Category].toLowerCase());
                        })
                    }
                })
            }
        },
        Reload: function (e) {
            var inputs = U.SerializeForm($("input", e));
            if (inputs) {
                var type = Billing.ClaimCategory[inputs["TypeId"]];
                var area = Billing.AreaById[inputs["ServiceId"]];
                Acore.Confirm({
                    Message: "Any edits you have made to the visit rates or locators will be lost. Are you sure you want to reload the insurance?",
                    Yes: function () {
                        $(e).empty().Load(area + "/Billing/" + type + "ClaimReloadInsurance", inputs, function () { Billing.Claim.Sections.Insurance(e) });
                    }
                })
            }
        },
        ReloadRates: function (e) {
            var inputs = U.SerializeForm($("input", e));
            if (inputs) {
                var type = Billing.ClaimCategory[inputs["TypeId"]];
                var area = Billing.AreaById[inputs["ServiceId"]];
                Acore.Confirm({
                    Message: "Any edits you have made to the visit rates will be lost. Are you sure you want to reload?",
                    Yes: function () {
                        U.PostUrl(area + "/Billing/" + type + "ClaimReloadRates", inputs, function (data) {
                            $(".claim-rates", e).data("tGrid").dataBind(data);
                        })
                    }
                })
            }
        },
        EditDeleteBind: function (form, grid) {
            var inputs = U.SerializeForm($("input", form));
            $("tbody tr td.action a.edit", grid).on("click", function () {
                var id = $(this).closest("tr").find("td.rid").text();
                inputs["RateId"] = id;
                Billing.Rate.Edit(inputs);
            });
            $("tbody tr td.action a.delete", grid).on("click", function () {
                var id = $(this).closest("tr").find("td.rid").text();
                inputs["RateId"] = id;
                Billing.Rate.Delete(inputs);
            });
        },
        OnDataBound: function (e) {
            U.OnTGridDataBound(e);
            Billing.Rate.EditDeleteBind($(e.target).closest("form"), e.target);
        }
    }
});