﻿if (typeof Billing.Medicare == "undefined") $.extend(Billing, { Medicare: new Object() });
$.extend(Billing.Medicare, {
    Remittance: {
        Init: function(r, t, x, e) {
            U.InitForNonT(r, t, x, e);
            $(".upload", e).on("click", Billing.Medicare.Remittance.OpenUpload);
        },
        OpenUpload: function() {
            Acore.Modal({ Id: "remittanceupload", Url: "Billing/RemittanceUpload", Width: 500, OnLoad: function(r, t, x, e) { $("#externalRemittanceUploadForm", e).Validate(); }, Input: null });
        },
        Delete: function(id) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this remittance?",
                Yes: function() {
                    U.PostUrl("Billing/DeleteRemittance", { Id: id }, function(result) {
                        if (result.isSuccessful) U.RebindDataGridContent('BillingRemittance', 'Billing/RemittanceContent', { StartDate: $('#BillingRemittance_StartDate').val(), EndDate: $('#BillingRemittance_EndDate').val() });
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    })
                }
            })
        },
        Details: {
            RemittanceId: "",
            Init: function(r, t, x, e) {
                Billing.Medicare.Remittance.Details.RemittanceId = $("#RemittanceDetail_Id", e).val();
                $('div#RemittanceDetailContainer div.episodes', e).on("click", "div.buttons ul li a", function(evnt) {
                    Billing.Medicare.Remittance.Details.Post($(evnt.delegateTarget));
                });
            },
            Open: function(id) {
                Acore.Open("remittancedetail", 'Billing/RemittanceDetail', Billing.Medicare.Remittance.Details.Init, { Id: id });
            },
            Print: function() {
                U.GetAttachment('Billing/RemittancePdf', { 'Id': Billing.Medicare.Remittance.Details.RemittanceId });
            },
            Post: function(control) {
                var inputs = $("input[type=radio]:checked", $(control)),
                    postInput = [];
                if (inputs.length) {
                    inputs.each(function(index) { postInput[index] = { name: "Episodes", value: $(this).val() }; });
                    postInput[inputs.length] = { name: "Id", value: Billing.Medicare.Remittance.Details.RemittanceId };
                    U.PostUrl("Billing/PostRemittance", postInput, function(result) {
                        if (result) {
                            if (result.isSuccessful) {
                                U.RebindContent("#RemittanceDetailContainer", 'Billing/RemittanceDetailContent', { Id: Billing.Medicare.Remittance.Details.RemittanceId });
                                U.Growl(result.errorMessage, "success");
                            } else U.Growl(result.errorMessage, "error");
                        }
                    }, function() { U.Growl("A problem occcured while posting. Please try again.", "error"); });
                }
                else {
                    Acore.Alert('Select at least one option.');
                    return;
                }
            }
        }
    }
});