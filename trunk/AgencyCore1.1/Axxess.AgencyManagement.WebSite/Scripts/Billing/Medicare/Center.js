﻿if (typeof Billing.Medicare == "undefined") $.extend(Billing, { Medicare: new Object() });
$.extend(Billing.Medicare, {
    Center: {
        AcoreId: "BillingHistory",
        Init: function(r, t, x, e) { Billing.Center.Init(e); },
        PatientListDataBound: function(e) { Billing.Center.PatientListDataBound(e); },
        OnPatientRowSelected: function(e) { Billing.Center.OnPatientRowSelected(e); },
        OnClaimDataBinding: function(e) { Billing.Center.OnClaimDataBinding(e); },
        OnClaimDataBound: function(e) { Billing.Center.OnClaimDataBound(e, "Medicare", 1); },
        OnClaimRowSelected: function(e) { Billing.Center.OnClaimRowSelected(e, ""); },
        OnClaimRowDataBound: function(e) {
            U.OnRowDataBoundHideEmptyHierarchy(e);
            Billing.Center.OnClaimRowDataBound(e, "Medicare", 1);
        },
        OnClaimSnapShotDataBound: function(e) {
            U.OnTGridDataBound(e);
            var grid = $(e.target).data('tGrid');
            if (grid) {
                var index = grid.columns.length - 1;
                if (grid.data.length > 0) {
                    var data = grid.data[0];
                    if (data != null) {
                        if (data.IsUserCanEdit) grid.showColumn(index);
                        else grid.hideColumn(index);
                    }
                } 
            }
        },
        ActivityRefresh: function(patientId) { Billing.Center.ActivityRefresh(Billing.Medicare.Center.AcoreId, patientId); }
    }
});