﻿if (typeof Billing.Medicare == "undefined") $.extend(Billing, { Medicare: new Object() });
$.extend(Billing.Medicare, {
    Pending: {
        AcoreId: "pendingClaim",
        Init: function(r, t, x, e) {
            var pageName = $(".pagename", e).val(),
                claimType = $(".claimtype", e),
                rapGrid = $("#" + pageName + "_rapContent > #Raps", e),
                finalGrid = $("#" + pageName + "_finalContent > #Finals", e);
            claimType.change(function() {
                e.addClass("transistion");
                if ($(this).val() == "0") {
                    finalGrid.Animate({ left: "105%", right: "-105%" }, function() {
                        finalGrid.hide();
                        U.RebindTGrid(rapGrid);
                    });
                    rapGrid.css({ left: "-105%", display: "block" }).Animate({ left: 0, right: 0 }, function() {
                        e.removeClass("transistion");
                    });
                }
                if ($(this).val() == "1") {
                    rapGrid.Animate({ left: "-105%", right: "105%" }, function() {
                        rapGrid.hide();
                        U.RebindTGrid(finalGrid);
                    });
                    finalGrid.css({ left: "105%", display: "block" }).Animate({ left: 0, right: 0 }, function() {
                        e.removeClass("transistion");
                    });
                }
            });
            U.InitLocationDependent(e, function() { Billing.Medicare.Pending.Refresh(true, true); });
            $("select.insurance", e).on("change", function() { Billing.Medicare.Pending.Refresh(true, true); });
            $(".button .refresh", e).on("click", function() { Billing.Medicare.Pending.Refresh(true, true); });
        },
        ReLoadPendingClaimRap: function(e, input) { U.RebindTGrid($(".rapcontent .grid-container:visible", e), input); },
        ReLoadPendingClaimFinal: function(e, input) { U.RebindTGrid($(".finalcontent .grid-container:visible", e), input); },
        Refresh: function(isRapRefresh, isFinalRefresh) {
            if (isRapRefresh || isFinalRefresh) {
                var winId = Billing.Medicare.Pending.AcoreId;
                if (winId) {
                    var container = $("#window_" + winId + "_content");
                    if (container != undefined) {
                        var input = U.GetGridFilterParameters(container);
                        if (isRapRefresh) { Billing.Medicare.Pending.ReLoadPendingClaimRap(container, input); }
                        if (isFinalRefresh) { Billing.Medicare.Pending.ReLoadPendingClaimFinal(container, input); }
                    }
                }
            }
        }
    }
});