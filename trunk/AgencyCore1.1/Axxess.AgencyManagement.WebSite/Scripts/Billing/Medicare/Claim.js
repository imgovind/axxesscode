﻿if (typeof Billing.Medicare == "undefined") $.extend(Billing, { Medicare: new Object() });
$.extend(Billing.Medicare, {
    Claim: {
        New: function(patientId, type) { Billing.Claim.New({ patientId: patientId }, type, 1); },
        Edit: function(patientId, Id, type) { Billing.Claim.Edit({ patientId: patientId, id: Id }, type, 1);  },
        Delete: function(patientId, id, type) { Billing.Claim.Delete({ patientId: patientId, id: id }, type, 1); },
        Rap: {
            Open: function(Id, patientId, e) { Acore.Open("rapclaimedit", "Billing/RapClaim", Billing.Medicare.Claim.Rap.Init, { Id: Id, patientId: patientId }); if(e) e.stopPropagation(); },
            Init: function(r, t, x, e) {
                //Billing.InfoTabInit(r, t, x, e);
                $("select.claiminsurance", e).on("change", function() {
                    if ($(this).val() >= 1000) {
                        Billing.LoadInsuranceContent($("div.primaryinsurancecontent", e), $("input[name=PatientId]", e).val(), $(this).val(), $("input[name=EpisodeStartDate]", e).val(), $("input[name=EpisodeEndDate]", e).val(), $("input[name=ClaimType]", e).val());
                    }
                    else $("div.primaryinsurancecontent", e).empty();
                });
                U.InitTemplate($("form.mainform", e), function(result) { Billing.Medicare.Claims.ReLoadUpProcessedClaimOnAction(result); }, "Rap is successfully verified.");
            }
        },
        Final: {
            Open: function(Id, patientId, e) { Billing.Claim.Open({ Id: Id, patientId: patientId }, 2, 1); if (e) e.stopPropagation(); },
            Supply: {
                Edit: function(claimId, patientId, Id) { Billing.Supply.Edit(claimId, patientId, Id, "Final"); }
            }
        },
        BatchEdit: function(Id, batchId, type) {
            Acore.Modal({
                Name: "Edit Batch Claim",
                Url: "Billing/EditClaimSnapShot",
                Input: { id: Id, batchId: batchId, type: type },
                OnLoad: function() { Billing.Medicare.Claim.InitBatch(type) },
                Width: "500px",
                WindowFrame: false
            });
        },
        InitBatch: function(type) { U.InitTemplate($("#updateBatchClaimForm"), function(data) { Billing.Medicare.Pending.Refresh(type == 1 ? true : false, type == 2 ? true : false); Billing.Medicare.Center.ActivityRefresh(data.PatientId); Billing.Medicare.Claims.ReLoadUpProcessedClaimOnAction(data); }, "Batch Claim updated successfully."); },
        Response: function(Id) { Acore.Open("billingclaimresponse", 'Billing/ClaimResponse', function() { }, { Id: Id }); },
        Remittance: function(Id, type) { Acore.Open("claimremittances", { Id: Id, Type: type }); }
    }
});