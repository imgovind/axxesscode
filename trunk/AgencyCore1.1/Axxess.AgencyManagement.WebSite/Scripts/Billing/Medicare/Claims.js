﻿if (typeof Billing.Medicare == "undefined") $.extend(Billing, { Medicare: new Object() });
$.extend(Billing.Medicare, {
    //Claims contains all functions related to generating a claim from the create claim pages
    Claims: {
    
        RAPClaimsWindowId: "billingcenterrap",
        FinalClaimsWindowId: "billingcenterfinal",
        ReLoadUpProcessedClaimOnAction: function(data) {
            if (data.IsRAPRefresh) {
                Billing.Medicare.Claims.ReLoadRAP();
            }
            if (data.IsFinalRefresh) {
                Billing.Medicare.Claims.ReLoadFinal();
            }
        },
        ReLoadRAP: function() {
            var winId = Billing.Medicare.Claims.RAPClaimsWindowId;
            if (winId) {
                if (Acore.Windows[winId].IsOpen) {
                    $("div.wrapper .grid-controls a.grid-refresh", $("#window_" + winId)).click();
                }
            }
        },
        ReLoadFinal: function() {
            var winId = Billing.Medicare.Claims.FinalClaimsWindowId;
            if (winId) {
                if (Acore.Windows[winId].IsOpen) {
                    $("div.wrapper .grid-controls a.grid-refresh", $("#window_" + winId)).click();
                }
            }
        },
        PrintClaim: function(e, episodeId, patientId, claimType) {
            U.GetAttachment('Billing/' + claimType + 'Pdf', { 'episodeId': episodeId, 'patientId': patientId });
            e.stopPropagation();
            return false;
        }
    }
});