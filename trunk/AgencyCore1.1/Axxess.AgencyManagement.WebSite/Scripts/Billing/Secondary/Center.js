﻿if (typeof Billing.Secondary == "undefined") $.extend(Billing, { Secondary: new Object() });
$.extend(Billing.Secondary, {
    Center: {
        AcoreId: "SecondaryClaims",
        Open: function(Id, patientId) {
            Acore.Open(Billing.Secondary.Center.AcoreId, { PatientId: patientId, PrimaryClaimId: Id });
        },
        Init: function(r, t, x, e) { },
        RebindActivity: function(patientId) {
            Billing.Center.ActivityRefresh(Billing.Secondary.Center.AcoreId, patientId);
        },
        OnClaimRowSelected: function(e) {
            var patientId = $("td.pid", e.row).text(),
                claimId = $("td.cid", e.row).text(),
                control = $(e.row).closest(".wrapper").find(".inline-fieldset");
            if (patientId != U.GuidEmpty && claimId != U.GuidEmpty) Billing.Secondary.Center.LoadClaimInfo(control, { patientId: patientId, claimId: claimId });
        },
        OnCliamDataBound: function(e) {
            var rows = $(".t-grid-content tbody tr:not(.t-no-data)", e.target);
            if (rows != undefined && rows.length > 0) rows.eq(0).click();
            else $(e.target).closest(".wrapper").find(".inline-fieldset > div:eq(1)").remove();
        },
        LoadClaimInfo: function(control, input) {
            while ($(control).children().length > 1) $(control).children("div:eq(1)").remove();
            $(control).append($("<div/>").hide());
            $(control).children("div:last").Load("Billing/SecondarySnapShotClaimInfo", input, function(r, t, x, e) { e.show() });
        }
    }
});