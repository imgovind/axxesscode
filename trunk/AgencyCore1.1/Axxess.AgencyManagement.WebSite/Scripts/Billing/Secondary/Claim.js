﻿if (typeof Billing.Secondary == "undefined") $.extend(Billing, { Secondary: new Object() });
$.extend(Billing.Secondary, {
    Claim: {
        Area: "",   //This varialbe will never actually be set. It will just allow the use of an area more easily for managed claims for the bill data functions.
        New: function(episodeId, patientId, primaryClaimId) { Billing.Claim.New({ episodeId: episodeId, patientId: patientId, primaryClaimId: primaryClaimId }, 4, 1); },
        Edit: function(Id) { Billing.Claim.Edit({ id: Id }, 4, 1); },
        Delete: function(patientId, id) { Billing.Claim.Delete({ patientId: patientId, id: id }, 4, 1); },
        Open: function(Id) { Billing.Claim.Open({ Id: Id }, 4, 1); },
        DeleteAdjustment: function(row) {
            var listItem = $(row);
            if (listItem.hasClass("last")) {
                var previous = listItem.prev("li");
                if (!previous.length) {
                    listItem.closest(".acore-grid").hide();
                }
                else {
                    previous.addClass("last");
                }
                listItem.remove();
            }
            else {
                listItem.remove();
            }
        },
        Supply: {
            Edit: function(claimId, patientId, Id) {
                Billing.Supply.Edit(claimId, patientId, Id, "Secondary");
            }
        },
        GeneratedSingle: function(id, area) { Billing.LoadGeneratedSingle(id, "/Billing/CreateSingleSecondaryANSI"); }
    }
});