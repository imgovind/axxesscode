﻿if (typeof Schedule == "undefined") var Schedule = new Object();
$.extend(Schedule, {
    Task: {
        // Delete Event
        Delete: function(patientId, id, prefix) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this task?",
                Yes: function() {
                    U.PostUrl(prefix + "/Schedule/Delete", {
                        id: id,
                        patientId: patientId
                    }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Refresh.All(result);
                            MissedVisit.Refresh();
                            U.RebindTGrid($('#DeletedTasks_Grid'));
                        }
                    })
                }
            });
        },
        // Reopen  Event
        Reopen: function(patientId, id, prefix) {
            Acore.Confirm({
                Message: "Are you sure you want to reopen this task?",
                Yes: function() {
                    U.PostUrl(prefix + "/Schedule/Reopen", {
                        id: id,
                        patientId: patientId
                    }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Refresh.All(result);
                        }
                    });
                }
            });
        },
        // Restore  Event
        Restore: function(patientId, id, prefix) {
            Acore.Confirm({
                Message: "Are you sure you want to restore this task?",
                Yes: function() {
                    U.PostUrl(prefix + "/Schedule/Restore", {
                        id: id,
                        patientId: patientId
                    }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Refresh.All(result);
                            MissedVisit.Refresh();
                            U.RebindTGrid($('#DeletedTasks_Grid'));
                        }
                    });
                }
            });
        },
        InitNewMultiple: function(r, t, x, e) {
            U.HideIfChecked($("[name=IsAllDay]", e), $(".time-picker", e).closest(".row"));
            $("td[date]", e).on("click", function() {
                $(this).toggleClass("scheduled");
                var dates = $("[name=EventDates]", e).val().split(",");
                if ($(this).hasClass("scheduled")) dates.push($(this).attr("date"));
                else dates.splice(dates.indexOf($(this).attr("date")), 1);
                $("[name=EventDates]", e).val(dates.join(",").replace(/^,/, ""));
            });
            $("form.mainform", e).Validate({ Success: Refresh.CenterQAMySchedule });
        },
        Reassign: function(PatientId, Id, area) {
            Acore.Modal({
                Name: "Reassign Task",
                Url: area + "/Schedule/ReassignModal",
                Input: { patientId: PatientId, Id: Id },
                OnLoad: Schedule.Task.InitReassign,
                Width: 500
            });
        },
        InitReassign: function(r, t, x, e) { $("form", e).Validate({ Success: Refresh.All }); },
        MultipleReassign: function(input, prefix) {
            Acore.Modal({
                Name: "Reasign Multiple Tasks",
                Url: prefix + "/Schedule/ReassignMultipleModal",
                Input: input,
                OnLoad: Schedule.Task.InitReassign,
                Width: 500,
                WindowFrame: false
            });
        },
        DeleteAsset: function(control, input, prefix) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this asset?",
                Yes: function() {
                    U.PostUrl(prefix + "/Schedule/DeleteAsset", input, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            $(control).closest('span').remove();
                            var assetCountField = $(control).closest("fieldset").find(".attachment-count");
                            if (assetCountField) {
                                var value = $(assetCountField).text();
                                if (Math.floor(value) == value && $.isNumeric(value)) {
                                    $(assetCountField).text(value - 1);
                                }
                            }
                            //Schedule.Center.PrivateDuty.RefreshGrid(result);
                        }
                    })
                }
            });
        },
        DeleteWoundCareAsset: function (control, name, assetId, prefix) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this asset?",
                Yes: function() {
                    var input = { episodeId: $("#WoundCare_EpisodeId").val(), patientId: $("#WoundCare_PatientId").val(), eventId: $("#WoundCare_EventId").val(), name: name, assetId: assetId };
                    U.PostUrl(prefix + "/Note/DeleteWoundCareAsset", input, function(result) {
                        if (result.isSuccessful)
                            $(control).closest('td').empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" size=\"13.75\" class = \"fl uploadWidth\" />");
                    })
                }
            });
        },
        DeletedList: function(patientId, prefix) { Acore.Open("patientdeletedtaskhistory", prefix + "/Schedule/DeletedTaskHistory", { patientId: patientId }); },
        LoadTasks: function(currentItem, input) {
            var callback = function(currentSelect, options) {
                if (options.length > 0) {
                    var parentElement = currentSelect.closest("div");
                    var taskElement = parentElement.find("[name=DisciplineTask]");
                    var attributes = { id: taskElement.attr("id"), name: taskElement.attr("name"), "class": "required not-zero" }
                    $("[name=DisciplineTask]", parentElement).remove();
                    parentElement.append($("<select/>", attributes).append(options)).find(".load-disciplinetask").closest("span").hide();
                }
                else {
                    U.Growl("Unable to load the tasks.  Try again.", "error");
                }
            }
            U.PostUrl("/Agency/DisciplineTypes", input, function(data) {
                var options = [];
                $.each(data, function(index, itemData) { options[index] = new Option(itemData.Text, itemData.Value, false, itemData.Selected); });
                if (typeof callback == "function") callback(currentItem, options);
            });
        },
        GetAttachments: function(patientId, eventId, prefix) {
            Acore.Modal({
                Url: prefix + "/Schedule/Attachments",
                Input: { patientId: patientId, eventId: eventId },
                Width: 500
            });
        }
    }
});