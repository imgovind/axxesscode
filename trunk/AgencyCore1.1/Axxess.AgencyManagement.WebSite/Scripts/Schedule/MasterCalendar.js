﻿if (typeof Schedule == "undefined") var Schedule = new Object();
$.extend(Schedule, {
    MasterCalendar: {
        Init: function(r, t, x, e) {
            $(".navigate", e).each(function() {
                Schedule.MasterCalendar.Navigate($("[name=PatientId]", e).val(), $("[name=EpisodeId]", e).val(), $(this).hasClass("previous-episode"), $(this), e);
            });
            $(".print", e).on("click", function() {
                Schedule.MasterCalender.Print($("[name=PatientId]", e).val(), $("[name=EpisodeId]", e).val(), $("[name=ShowMissedVisits]", e).prop("checked"));
            });
            U.ShowIfChecked($("[name=ShowMissedVisits]", e), $(".missed-visit.eventTitle", e));
            $("[name=ShowMissedVisits]", e).on("change", function() {
                if ($(this).is(":checked")) $(".missed-visit.desc", e).removeClass("hidden");
                else $(".missed-visit.desc", e).addClass("hidden");
            });
            $("[tooltip]", e).each(function() {
                $(this).Tooltip($(this).attr("tooltip"));
            });
        },
        Load: function(patientId, episodeId) {
            Acore.Open("MasterCalendar", { patientId: patientId, episodeId: episodeId });
        },
        Navigate: function(patientId, episodeId, previous, button, e) {
            button.on("click", function() {
                e.Animate({ left: previous ? "100%" : "-100%" }, function() {
                    e.Load("Schedule/MasterCalendar", { patientId: patientId, episodeId: episodeId }, Schedule.MasterCalendar.Init);
                })
            })
        },
        Print: function(patientId, episodeId, showMissedVisits) {
            U.GetAttachment("Schedule/MasterCalendarPdf", { patientId: patientId, episodeId: episodeId, showMissedVisits: showMissedVisits });
        }
    }
});