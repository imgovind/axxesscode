﻿if (typeof Schedule == "undefined") var Schedule = new Object();
$.extend(Schedule, {
    Delete: {
        Init: function(r, t, x, e) {
            U.ListInitClientRebind(r, t, x, e);
            $(".delete", e).on("click", function() {
                Schedule.Delete.Action(e);
                return false;
            });
            var selectAll = $(".selectall", e);
            if (selectAll) { selectAll.on("change", function() { $(".grid-container .t-grid-content tr input", e).each(function() { $(this).prop("checked", selectAll.prop("checked")); }) }); }
        },
        Open: function(episodeId, patientId) {
            Acore.Open("ScheduleDeleteMultiple", { episodeId: episodeId, patientId: patientId });
        },
        Action: function(e) {
            var data = $(".grid-container .t-grid-content tr input:checked", e).serializeArray();
            if (!data.length) { U.Growl("You must select at least one task to delete.", 'error'); }
            else Acore.Confirm({
                Message: "Are you sure you want to delete these event(s)?",
                Yes: function() {
                    $("fieldset .input", e).each(function() {
                        data.push({ 'name': $(this).attr("name"), 'value': $(this).val() });
                    });
                    U.PostUrl("Schedule/DeleteMultiple", data, function(data) {
                        U.Growl(data.errorMessage, data.isSuccessful ? "success" : "error");
                        if (data.isSuccessful) {
                            MissedVisit.Refresh();
                            Refresh.All(data);
                            UserInterface.CloseWindow("ScheduleDeleteMultiple");
                        }
                    })
                }
            })
        },
        ActivityRowSelected: function(e) {
            var checkbox = $(e.row).find("input");
            checkbox.prop("checked", !checkbox.prop("checked"));
            var grid = $(e.currentTarget);
            var count = grid.find("tr input[name=EventId]").length;
            Schedule.Delete.SelectHelper(count, grid);
        },
        OnRowDataBound: function(e) {
            var grid = $(e.currentTarget);
            var count = grid.find("tr input[name=EventId]").length;
            $(e.row).find("input[name=EventId]").on("click", function() {
                Schedule.Delete.SelectHelper(count, grid);
            });
        },
        SelectHelper: function(count, grid) {
            var checkedCount = grid.find("tr input[name=EventId]").filter(':checked').length;
            if (count == checkedCount && count > 0) $(".selectall", grid).prop("checked", true);
            else $(".selectall", grid).prop("checked", false);
        }
    }
});

