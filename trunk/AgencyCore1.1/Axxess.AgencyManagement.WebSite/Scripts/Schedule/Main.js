﻿if (typeof Schedule == "undefined") var Schedule = new Object();
$.extend(Schedule, {
    ToolTip: function(e) {
        U.ToolTip(e.row);
    },
    WoundCareDeleteAsset: function(control, name, assetId) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this asset?",
            Yes: function() {
                var input = "episodeId=" + $("#WoundCare_EpisodeId").val() + "&patientId=" + $("#WoundCare_PatientId").val() + "&eventId=" + $("#WoundCare_EventId").val() + "&name=" + name + "&assetId=" + assetId;
                U.PostUrl("Note/DeleteWoundCareAsset", input, function(result) {
                    if (result.isSuccessful) $(control).closest('td').empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" class=\"fl uploadWidth\" />");
                })
            }
        })
    },
    LoadLog: function(patientId, eventId, task) {
        Acore.Open("schdeuleeventlogs", { patientId: patientId, eventId: eventId, task: task });
    }
});

