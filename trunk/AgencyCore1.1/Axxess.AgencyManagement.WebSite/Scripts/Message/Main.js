﻿if (typeof Message == "undefined") var Message = new Object();
$.extend(Message, {
    Delete: function(id, type, inboxType, bypass, callback) {
        if (bypass) U.TGridAjax("Message/Delete", { Id: id, messageType: type, inboxType: inboxType }, $("#list-messages"), callback);
        else Acore.Confirm({
            Message: "Are you sure you want to delete this message?",
            Yes: function() { U.TGridAjax("Message/Delete", { Id: id, messageType: type, inboxType: inboxType }, $("#list-messages")); }
        });
    },
    Move: function(id, messageType, folderId, folderName) {
        // Ensure the menus are all hidden
        $("ul.menu").hide();
        U.PostUrl("Message/Move", {
            id: id,
            messageType: messageType,
            folderId: folderId,
            folderName: folderName
        }, function(result) {
            U.Growl(result.errorMessage, "success");
            Message.RefreshInbox();
        }, function(result) {
            U.Growl(result.errorMessage, "error");
        });
    },
    New: function() {
        Acore.Open("NewMessage");
    },
    InitWidget: function() {
        $(".widget-more,li:eq(0)", "#messages-widget").on("click", function() { Message.Inbox() });
        $("li:eq(1)", "#messages-widget").on("click", function() {
            Acore.Open("NewMessage");
            return false;
        });
        $("li:eq(2)", "#messages-widget").on("click", function() {
            if ($("input:checked", "#messages-widget .widget-message-list").length) Acore.Confirm({
                Message: "Are you sure you want to delete the selected message(s)?",
                Yes: function() {
                    var inboxGrid = $("#list-messages");    
                    var messageList = $("input:checked", "#messages-widget").serializeArray();
                    if (messageList.length > 0) {
                        U.TGridAjax("Message/DeleteMany", messageList, inboxGrid, function(result) {
                            if (result.isSuccessful) {
                                U.Growl(result.errorMessage, "success");
                                $("#messages-widget .widget-message-list").empty();
                                Message.GetMessagesWidget($(".widget-message-list", "#messages-widget"));
                                U.RebindTGrid(inboxGrid);
                            } else {
                                U.Growl(result.errorMessage, "error");
                            }
                        });
                    }
                }
            });
            else U.Growl("Please select the message(s) you would like to delete", "error");
        });
        Message.GetMessagesWidget($(".widget-message-list", "#messages-widget"));
    },
    GetMessagesWidget: function(e) {
        U.PostUrl("Message/GridWidget", {}, function(data) {
            if (data != undefined && data.Data != undefined && data.Data.length > 0) {
                for (var i = 0; i < data.Data.length; i++)
                    e.append(
                        $("<li/>", { "class": (data.Data[i].MarkAsRead ? "" : "strong") }).append(
                            $("<input/>", { type: "checkbox", messagetype: data.Data[i].Type, value: data.Data[i].Id, name: "deleteMessage" })).append(
                                $("<a/>", { "html": data.Data[i].FromName + " &#8211; " + data.Data[i].Subject }).on("click", function() {
                                    $(this).parent().removeClass("strong");
                                    Message.Inbox($(this).siblings("input").val());
                                })
                            ).on("mouseover", function() {
                                $(this).addClass("hover");
                                $(this).closest(".window").Status("View this Message in the Message Center Window");
                            }).on("mouseout", function() {
                                $(this).removeClass("hover");
                                $(this).closest(".window").Status("");
                            }));
            } else e.html($("<h1/>", { "class": "ac blue", "text": "No messages found." }));
        });
    },
    OnDataBound: function(e) {
        U.OnTGridDataBound(e);
        if (Message.DefaultMessage && $("#" + Message.DefaultMessage).length) $("#" + Message.DefaultMessage).closest("tr").click();
        else {
            var messageRow = $("#list-messages .t-grid-content tr").not(".t-no-data").eq(0).click();
            if (messageRow.length == 0 && !Acore.ReadOnly) $("<div/>", { "class": "floater" }).prependTo(
                $("#message-view").removeClass("loading").html(
                    U.MessageInfo("No messages found.", "There are currently no messages in this mailbox.", [{
                        Text: "Add New Message",
                        Click: function() { Message.New() }
                    }])
                )
            )
        }
        Message.DefaultMessage = "";
    }
});