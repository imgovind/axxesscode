﻿if (typeof Refresh == "undefined") var Refresh = new Object();
$.extend(Refresh, {
    PrivateDuty: {
        ScheduleCenter: function(data) {
            try {
                if (data.IsCenterRefresh) {
                    if (PrivateDuty.IsActive && Acore.Windows[Schedule.Center.PrivateDuty.AcoreId].IsOpen) {
                        Schedule.Center.PrivateDuty.Refresh();
                    }
                }
            }
            catch (err) { }
        },
        ScheduleActivity: function(data) {
            try {
                if (data.IsDataCentersRefresh) {
                    if (PrivateDuty.IsActive && Acore.Windows[Schedule.Center.PrivateDuty.AcoreId].IsOpen) {
                        var isPatientSelection = Schedule.Center.PrivateDuty.PatientSelection,
                            userIdIndex = $.inArray(Schedule.Center.PrivateDuty.CurrentId, data.UserIds);
                        if (!(data.PatientId && data.UserIds) || (isPatientSelection && Schedule.Center.PrivateDuty.CurrentId == data.PatientId) || (!isPatientSelection && userIdIndex > -1)) Schedule.Center.PrivateDuty.RefreshCurrentView();
                    }
                }
            }
            catch (err)
            { }
        },
        MyScheduleTasks: function(data) {
            try {
                if (data.IsMyScheduleTaskRefresh) {
                    if (PrivateDuty.IsActive && Acore.Windows[PrivateDuty.UserSchedule.AcoreId].IsOpen) {
                        PrivateDuty.UserSchedule.RefreshCurrentView();
                    }
                }
            } catch (err) { }
        }
    }
});