﻿if (typeof Refresh == "undefined") var Refresh = new Object();
$.extend(Refresh, {
    HomeHealth: {
        ScheduleCenter: function(data) {
            try {
                if (data.IsCenterRefresh && HomeHealth.IsActive && Acore.Windows[Schedule.Center.HomeHealth.AcoreId].IsOpen) Schedule.Center.HomeHealth.PatientSelector.Refresh();
            } catch (err) { }
        },
        ScheduleActivity: function(data) {
            try {
                if (data.IsDataCentersRefresh && HomeHealth.IsActive && Acore.Windows[Schedule.Center.HomeHealth.AcoreId].IsOpen && Schedule.Center.HomeHealth.PatientId == data.PatientId) {
                    if (U.GuidEmpty != data.EpisodeId && Schedule.Center.HomeHealth.EpisodeId == data.EpisodeId) Schedule.Center.HomeHealth.RefreshCalendar();
                    //else Schedule.Center.HomeHealth.LoadCalendarAndActivities(data.PatientId);
                }
            } catch (err) { }
        },
        MyScheduleTasks: function(data) {
            try {
                if (data.IsMyScheduleTaskRefresh && HomeHealth.IsActive && Acore.Windows["MySchedule"].IsOpen) User.RebindScheduleList();
            } catch (err) { }
        }
    }
});