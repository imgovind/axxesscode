﻿var Refresh = {
    PatientCharts: function(data) {
        if (data.IsCenterRefresh) {
            if (Acore.Windows[Patient.Charts.AcoreId].IsOpen) {
                var e = $("#window_" + Patient.Charts.AcoreId);
                var service = e.find("[name=ServiceId]").val();
                if (service && service == data.ServiceId) {
                    Patient.Charts.PatientSelector.Rebind(e);
                }
            }
        }
    },
    PatientChartActivity: function(data) {
        if (data.IsDataCentersRefresh) {
            if (Acore.Windows[Patient.Charts.AcoreId].IsOpen) {
                var e = $("#window_" + Patient.Charts.AcoreId);
                var service = e.find("[name=ServiceId]").val();
                if (service && service == data.ServiceId) {
                    Patient.Charts.Activities.Rebind($(".ui-layout-center", e));
                }
            }
        }
    },
    QACenter: function(data) {
        try {
            if (data.IsCaseManagementRefresh) {
                if (Acore.Windows[Agency.QA.WindowId].IsOpen) {
                    var e = $("#window_" + Agency.QA.WindowId);
                    var service = e.find("[name=ServiceId]").val();
                    if (service && service == data.ServiceId) {
                        $(".grid-refresh", e).click();
                    }
                }
            }
        } catch (err) { }
    },
    Orders: function(data) {
        if (data.IsOrdersToBeSentRefresh) {
            Agency.OrdersManagement.RebindOrdersToBeSent(data.ServiceId);
        }
        if (data.IsOrdersPendingRefresh) {
            Agency.OrdersManagement.RebindPendingOrders(data.ServiceId);
        }
        if (data.IsOrdersHistoryRefresh) {
            Agency.OrdersManagement.RebindOrdersHistory(data.ServiceId);
        }
        if (data.IsPhysicianOrderPOCRefresh) {
            Patient.Orders.RebindHistory(data.PatientId, data.ServiceId);
        }
    },
    CommunicationNotes: function(data) {
        if (data.IsCommunicationNoteRefresh) {
            if (Acore.Windows[CommunicationNote.ListWindowId].IsOpen) {
                var e = $("#window_" + CommunicationNote.ListWindowId);
                var service = e.find("[name=ServiceId]").val();
                if (service && service == data.ServiceId) {
                    var commNotesPatientId = CommunicationNote.GetPatientIdFromList(e);
                    if (commNotesPatientId != U.GuidEmpty) {
                        if (U.IsGuid(commNotesPatientId) && commNotesPatientId == data.PatientId) {
                            $(".grid-refresh", e).click();
                        }
                    }
                    else {
                        $(".grid-refresh", e).click();
                    }
                }
            }
        }
    },
    //Refreshes the patient list with in the schedule centers, which will cause the activity list to be refreshed as well.
    ScheduleCenter: function(data) {
        Refresh[data.Service].ScheduleCenter(data);
    },
    //schedule center activity and calendar 
    ScheduleActivity: function(data) {
        Refresh[data.Service].ScheduleActivity(data);
    },
    // patient center activity and schedule center activity with calendar
    CenterActivity: function(data) {
        Refresh[data.Service].ScheduleActivity(data);
        Refresh.PatientChartActivity(data);
    },
    // For both schedule and patient center refresh
    PatientSelectors: function(data) {
        Refresh[data.Service].ScheduleCenter(data);
        Refresh.PatientCharts(data);
    },
    MyScheduleTasks: function(data) {
        Refresh[data.Service].MyScheduleTasks(data);
    },
    CaseManagementMyScheduleTask: function(data) {
        Refresh.QACenter(data);
        Refresh.MyScheduleTasks(data);
    },
    All: function(data) {
        Refresh.CenterActivity(data);
        Refresh.CaseManagementMyScheduleTask(data);
        Refresh.Orders(data);
        Refresh.CommunicationNotes(data);
    },
    CenterQAMySchedule: function(data) {
        if (data.IsCenterRefresh) {
            Refresh.PatientSelectors(data);
        }
        else {
            if (data.IsDataCentersRefresh) {
                Refresh.CenterActivity(data);
            }
        }
        Refresh.CaseManagementMyScheduleTask(data);
    },
    OASISGrids: function(data) {
        if (data.IsExportOASISRefresh) { Oasis.HomeHealth.Export.Rebind(); }
        if (data.IsExportedOASISRefresh) { Oasis.HomeHealth.RebindExported(); }
        if (data.IsNotExportedOASISRefresh) { Oasis.HomeHealth.RebindNotExported(); }
    },
     RebindPatientGrids: function(data) {
        if (data.IsCenterRefresh) Refresh.PatientSelectors(data);
        if (data.IsPatientListRefresh) { $("#window_listpatients_content a.grid-refresh").click(); }
        if (data.IsNonAdmitPatientListRefresh) { U.RebindTGrid($('#PatientNonAdmit_Grid')); }
        if (data.IsPendingPatientListRefresh) { U.RebindTGrid($('#PatientPending_Grid')); }
        if (data.IsReferralListRefresh) { Referral.RebindList(); }
        if (data.IsDeletedPatientListRefresh) { $('#window_listdeletedpatients_content a.grid-refresh').click(); }
    }
}