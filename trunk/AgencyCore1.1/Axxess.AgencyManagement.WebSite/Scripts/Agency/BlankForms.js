﻿$.extend(Agency, {
    BlankForms: {
        Init: function(r, t, x, e) {
            $("a.note-blank", e).each(function() {
                var noteType = $(this).attr("notetype");
                $(this).on("click", function() {
                    U.GetAttachment("Note/Blank", { Type: noteType });
                });
                $(this).removeAttr("notetype");
            });
        }
    }
});