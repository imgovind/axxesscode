﻿$.extend(Agency, {
    PrintQueue: {
        Init: function(r, t, x, e) {
            U.ListInitWithArea(r, t, x, e);
            var form = $("form.markasprinted", e);
            if (form) {
                U.InitValidation(form,
                function() { $("a.grid-refresh", e).click() },
                 function(arr, form, options) {
                     options.url = window.location.href + window[Acore.AppService($("[name=ServiceId]", e).val())].Area + form.attr("action");
                     var result = arr.length > 2;
                     if (!result) U.Growl("At least one item must be selected before marking as printed.", "error");
                     return result;
                 });
            }
        },
        InitContent: function(gridContent) {
            var grid = gridContent.data('tGrid')
            if (grid) {
                var trs = $("tr:not(.t-grouping-row,.t-no-data)", grid.$tbody);
                var serviceName = "";
                if (trs.length > 0) {
                    trs.each(function(index, element) {
                        if (index == 0) {
                            serviceName = $(this).find("td.service").text();
                        }
                        Agency.PrintQueue.Print(element, { Id: $(this).find("td.id").text(), PatientId: $(this).find("td.pid").text() }, serviceName);
                    });
                }
            }

        },
        Print: function(element, dataItem, serviceName) {
            $("td a.print", element).on("click", function() {
            var result = CommandFactory.Print(this, dataItem, serviceName);
                if (result != undefined) {
                    U.GetAttachment(result.PdfUrl, result.PdfData);
                }
            });
        }
    }
});