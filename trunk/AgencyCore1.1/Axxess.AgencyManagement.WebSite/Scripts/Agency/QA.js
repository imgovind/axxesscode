﻿if (typeof Agency.QA == "undefined") $.extend(Agency, { QA: new Object() });
$.extend(Agency.QA, {
    WindowId: "caseManagement",
    DefaultReloadFailureMessage: 'Quality Assurance Center could not be refreshed. Please close this window and try again.',
    GroupFailureMessage: 'Quality Assurance Center could not group. Please close this window and try again.',
    SortFailureMessage: 'Quality Assurance Center could not sort. Please close this window and try again.',
    Init: function(r, t, x, e, prefix) {
        $("a.approve-button,a.return-button", e).on("click", function() {
            Agency.QA.BulkUpdate($(this));
            return false;
        });
        Agency.QA.InitGrid(r, t, x, e);
        //window[prefix].RenameWindow(e);
    },
    InitGrid: function(r, t, x, e) {
        U.ListInitWithArea(r, t, x, e);
        //U.ToolTip($("#CaseManagement_Grid", e));
    },
    InitGridContent: function(gridContent) {
        var grid = gridContent.data('tGrid')
        if (grid) {
            var trs = $("tr:not(.t-grouping-row,.t-no-data)", grid.$tbody);
            var serviceName = "";
            if (trs.length > 0) {
                trs.each(function(index, element) {
                    if (index == 0) { serviceName = $(this).find("td.service").text(); }
                    Agency.QA.Open(element, { Id: $(this).find("td.id").text(), PatientId: $(this).find("td.pid").text(), Type: $(this).find("td.type").text() }, serviceName);
                });
            }
        }
    },
    Open: function(element, dataItem, serviceName) {
        $("td a.qalink", element).on("click", function() {
            var result = CommandFactory.Print(this, dataItem, serviceName, true);
            if (result != undefined) {
                Acore.OpenPrintView(result);
            }
        });
    },
    Refresh: function(e) {
        if (!e) {
            var e = $("#window_" + Agency.QA.WindowId);
        }
        $(".grid-refresh", e).click();
    },
    BulkUpdate: function(button) {
        var content = $(button).closest(".window-content");
        if (content) {
            var form = content.find(".qa-activity-grid");
            if (form) {
                var inputs = $("input[name=CustomValue]:checked", form).serializeArray();
                if (inputs.length > 0) {
                    var service = $("[name=ServiceId]", content).val();
                    var url = window[Acore.AppService(service)].Area + "/QA/" + button.attr("commandtype");
                    button.trigger("ProcessingStart");
                    //inputs.push({ name: "CommandType", value: button.attr("commandtype") });
                    U.PostUrl(url, inputs, function(data) {
                        button.trigger("ProcessingComplete");
                        U.Growl(data.errorMessage, data.isSuccessful ? "success" : "error");
                        if (data.isSuccessful) {
                            Refresh.All(data);
                            Refresh.OASISGrids(data);
                        }
                    });
                } else {
                    U.Growl("At least one item must be selected.", "error");
                }
            }
        }
    }
});