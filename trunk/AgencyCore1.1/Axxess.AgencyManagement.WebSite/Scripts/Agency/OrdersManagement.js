﻿if (typeof Agency.OrdersManagement == "undefined") $.extend(Agency, { OrdersManagement: new Object() });
$.extend(Agency.OrdersManagement, {
    OrdersToBeSentWindowId: "orderstobesent",
    OrdersHistoryWindowId: "ordersHistory",
    OrdersPendingSignatureWindowId: "orderspendingsignature",
    OrdersHistoryInit: function(r, t, x, e) {
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
    },
    OrdersPendingSignatureInit: function(r, t, x, e) {
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
    },
    OrdersToBeSentInit: function(r, t, x, e) {
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
        $(".send-electronically", e).on("click", function() {
            var area = window[Acore.AppService($("[name=ServiceId]", e).val())].Area;
            Agency.OrdersManagement.MarkOrdersAsSent(area, e);
            return false;
        });
        $(".SendAutomatically", e).on("change", function() {
            $(".grid-refresh", e).click();
            if ($(this).val() == 'true') $(".send-electronically", e).text("Send Electronically");
            else $(".send-electronically", e).text("Mark as Sent");
        });
    },
    
    //Used by order grids whose last column is an action column and needs to be hidden if there are no link within in
    OrdersOnDataBound: function(e) {
        var grid = $(e.target).data("tGrid");
        if (grid) {
            if (grid.total) {
                //index of the last column in the grid
                var index = grid.columns.length - 1;
                if (!$(e.target).find("a.link").length) grid.hideColumn(index);
                else grid.showColumn(index);
            }
            U.OnTGridDataBound(e);
        }
    },
    OrderToBeSentOnDataBound: function(e) {
        var grid = $(e.target).data("tGrid");
        if (grid) {
            U.OnTGridDataBound(e);
            if (grid.total) {
                var currentWindow = $(e.target).closest(".window-content");
                var service = $("[name=ServiceId]", currentWindow).val();
                if (service > 0) {
                    var sentPermission = $("a.send-electronically", currentWindow).attr("service");
                    if (sentPermission > 0) {
                        var checkBoxIndex = $("th.checkbox", grid.$header).index();
                        if ((sentPermission & service) == service) grid.showColumn(checkBoxIndex);
                        else grid.hideColumn(checkBoxIndex);
                    }
                }
                if (!$(e.target).find("a.print").length) grid.hideColumn(6);
                else grid.showColumn(6);
            }
        }
    },
    OrdersPrintableOnRowDataBound: function(e) {
        var printLink = $(e.row).find(".print");
        if (printLink.length) {
            printLink.click(function() {
                var result = CommandFactory.Print(printLink, { PatientId: e.dataItem.PatientId, Id: e.dataItem.Id }, Acore.AppService(e.dataItem.Service));
                if (result) Acore.OpenPrintView(result);
            });
        }
    },
    MarkOrder: function(prefix, mark, control) {
        fields = $("select.SendAutomatically, input.OrdersToBeSent:checked", $(control)).serializeArray();
        U.PostUrl(prefix + "/OrdersManagement/MarkAs" + mark, fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, 'success');
                Refresh.Orders(data);
                Refresh.PatientSelectors(data);
            } else U.Growl(data.errorMessage, 'error');
        });
    },
    MarkOrdersAsReturned: function(prefix, e) { Agency.OrdersManagement.MarkOrder(prefix, "Returned", e); },
    MarkOrdersAsSent: function(prefix, e) { Agency.OrdersManagement.MarkOrder(prefix, "Sent", e); },
    RebindOrdersToBeSent: function(service) { Agency.OrdersManagement.CommonRefresh(Agency.OrdersManagement.OrdersToBeSentWindowId, service); },
    RebindPendingOrders: function(service) { Agency.OrdersManagement.CommonRefresh(Agency.OrdersManagement.OrdersPendingSignatureWindowId, service); },
    RebindOrdersHistory: function(service) { Agency.OrdersManagement.CommonRefresh(Agency.OrdersManagement.OrdersHistoryWindowId, service); },
    ReceiveOrder: function(id, patientId, type, serviceId) {
        var area = window[Acore.AppService(serviceId)].Area;
        if (area != undefined) {
            Acore.Modal({
                Name: "Receive Order",
                Url: area + "/OrdersManagement/ReceiveOrder",
                Input: { id: id, patientId: patientId, type: type },
                OnLoad: Agency.OrdersManagement.ReceiveOrderInit,
                Width: "550px",
                WindowFrame: false
            });
        }
    },
    ReceiveOrderInit: function(r, t, x, e) {
        U.InitTemplate($("form.mainform", e), function(data) {
            Refresh.Orders(data);
            Acore.CloseDialog();
        });
    },
    EditOrder: function(id, patientId, episodeId, type, serviceId) {
        var area = window[Acore.AppService(serviceId)].Area;
        if (area != undefined) {
            Acore.Modal({
                Name: "Orders History",
                Url: area + "/OrdersManagement/HistoryEdit",
                Input: { id: id, patientId: patientId, episodeId: episodeId, type: type },
                OnLoad: Agency.OrdersManagement.OrderHistoryEditInit,
                Width: "520px",
                WindowFrame: false
            });
        }
    },
    OrderHistoryEditInit: function(r, t, x, e) { U.InitTemplate($("#updateOrderHistry", e), function(data) { Agency.OrdersManagement.RebindOrdersHistory(data.Service); Acore.CloseDialog(); }, "Order successfully updated"); },
    PendingSignatureOrdersOnEdit: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != null) {
            var filterValues = U.GetGridFilterParameters();
            dataItem.StartDate = filterValues.StartDate;
            dataItem.EndDate = filterValues.EndDate;
            dataItem.BranchId = filterValues.BranchId;
            e.dateItem = dataItem;
        }
    },
    RebindCurrentGrid: function() { $(this).data("tGrid").rebind(U.GetGridFilterParameters($(this).parent())) },
    CommonRefresh: function(windowId, service) {
        try {
            if (Acore.Windows[windowId].IsOpen) {
                var e = $("#window_" + windowId);
                var serviceId = e.find("[name=ServiceId]").val();
                if (serviceId && serviceId == service) {
                    $(".grid-refresh", e).click();
                }
            }
        } catch (err) { }
    },
    Process: function(button, patientId, eventId, orderType, prefix, reason) {
        var input = { patientId: patientId, eventId: eventId, orderType: orderType },
            isReturning = button == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/OrdersManagement/" + button, input, function(result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.CenterQAMySchedule(result);
                Refresh.Orders(result);   
            } else if (isReturning) Acore.ReturnReasonFailed();
        });
    }
});