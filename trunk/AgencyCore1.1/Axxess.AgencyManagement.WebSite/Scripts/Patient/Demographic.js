﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    Demographic: {
        AcoreEditId: "EditPatient",
        InitNew: function (r, t, x, e) {
            Episode.PrivateDuty.InitCarePeriodLength(e);
            $(".branch-location.rebind.homehealth", e).on("change", function () {
                var isRebind = $(this).attr("rebind");
                if (isRebind == "True") Patient.Demographic.OnLocationChange(e);
            });
            Patient.Demographic.InitInsuranceChange(e);
            if ($("[name=ServiceProvided]", e).length > 1) {
                U.ServicesContainerInit("NewPatient_ServiceContainer", e);
            }
            var episodeCreateFunc = function (profileIndex) {
                var episodeCheckBox = $("[name='profile[" + profileIndex + "].ShouldCreateEpisode']", e);
                var rows = $("[name='profile[" + profileIndex + "].EpisodeStartDate'],[name='profile[" + profileIndex + "].EpisodeEndDate'],.care-period-length", e).closest(".row");
                var face2FaceOpt = $("[name='profile[" + profileIndex + "].IsFaceToFaceEncounterCreated']", e).closest(".option");
                U.ShowIfChecked(episodeCheckBox, rows.add(face2FaceOpt));
                $("[name='profile[" + profileIndex + "].StartOfCareDate']").on("change", function () {
                    var date = $(this).datepicker("getDate");
                    $("[name='profile[" + profileIndex + "].EpisodeStartDate']").datepicker("setDate", date)
                    $("[name='profile[" + profileIndex + "].EpisodeStartDate']").datepicker("option", "mindate", date);
                });
            }
            episodeCreateFunc(1);
            episodeCreateFunc(2);
            var form = $("form.mainform", e);
            form.Validate({
                Success: function (result) {
                    Refresh.RebindPatientGrids(result);
                    Patient.Demographic.PatientPrompt(result.PatientId);
                }
            });
        },
        PatientPrompt: function (patientId) {
            Acore.Modal({
                Alert: true,
                Name: "Next Step",
                Width: 450,
                Content: $("<div/>").AcoreDialog("Patient saved successfully. What would you like to do next?", "question", [{
                    Text: "Add Another Patient",
                    Click: function () {
                        Acore.CloseAlert();
                        Patient.Demographic.New();
                    }
                }, {
                    Text: "Go to Patient Charts",
                    Click: function () {
                        Acore.CloseAlert();
                        Patient.Charts.Load(patientId);
                    }
                }, {
                    Text: "Close",
                    Click: Acore.CloseAlert
                }])
            });
            $("#window_" + Acore.AlertId).SetHeight();
        },
        New: function () {
            Acore.Open("NewPatient");
        },
        InitNewPhoto: function (r, t, x, e) {
            $(".save", e).parent().hide();
            $("[name=photo]", e).on("change", function () {
                var ext = $("#Change_Patient_Photo").val().split('.').reverse()[0].toLowerCase();
                if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") $(".save", e).parent().show();
                else {
                    $(this).val("");
                    $(".save", e).parent().hide();
                    Acore.Alert("Please use a valid image (JPEG, GIF, or PNG)");
                }
            });
            U.InitValidation($("#changePatientPhotoForm"), function (result) {
                if (result.isSuccessful) {
                    $("#window_" + Acore.DialogId).Close();
                    Patient.Charts.Load(patientId);
                }
            })
        },
        NewPhoto: function (patientId) {
            Acore.Modal({
                Name: "New Photo",
                Url: "Patient/NewPhoto",
                Input: { patientId: patientId },
                OnLoad: Patient.Demographic.InitNewPhoto,
                Width: 450
            });
        },
        RemovePhoto: function (patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to remove this photo?",
                Yes: function () {
                    $("#window_" + Acore.DialogId + "_content").addClass("loading");
                    U.PostUrl("Patient/RemovePhoto", { patientId: patientId }, function (result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            $("#window_" + Acore.DialogId).Close();
                            Patient.Charts.Load(patientId);
                        } else $("#window_" + Acore.DialogId + "_content").removeClass("loading");
                    })
                }
            })
        },
        Maps: function (id) {
            Acore.Open("PatientMap", { id: (id == undefined ? Patient.Charts.PatientId : id) });
        },
        AddPhysRow: function (el) { $(el).closest('.column').find('.row.hidden').first().removeClass('hidden').find("input:first").blur(); },
        RemPhysRow: function (el) { $(el).closest('.row').addClass('hidden').appendTo($(el).closest('.column')).find('select').val('00000000-0000-0000-0000-000000000000'); },
        InitDeleteOrRestore: function (r, t, x, e) { U.InitValidation($("form.formsubmit", e), function (data) { Refresh.RebindPatientGrids(data); }); },
        Delete: function (patientId) {
            Acore.Modal({
                Name: "Delete Patient",
                Url: "Patient/DeleteView",
                Input: { Id: patientId },
                OnLoad: Patient.Demographic.InitDeleteOrRestore,
                Width: 600
            });
        },
        Restore: function (patientId) {
            Acore.Modal({
                Name: "Restore Patient",
                Url: "Patient/RestoreView",
                Input: { Id: patientId },
                OnLoad: Patient.Demographic.InitDeleteOrRestore,
                Width: 600
            });
        },
        DisplayStatusContainer: function displayStatusContainer(e, statusId) {
            if (statusId == "1") {
                $(".active-patient-container", e).show();
                $(".nonadmission-patient-container", e).hide().find(".required").addClass("ignorevalidation");
                $("a.yes", e).text("Yes, Activate");
            } else if (statusId == "2") {
                $(".discharge-patient-container", e).show().find(".required").removeClass("ignorevalidation");
                $(".nonadmission-patient-container ,.active-patient-container", e).hide().find(".required").addClass("ignorevalidation");
                $("a.yes", e).text("Yes, Discharge");
            } else if (statusId == "4") {
                $(".nonadmission-patient-container", e).show().find(".required").removeClass("ignorevalidation");
                $(".discharge-patient-container,.active-patient-container", e).hide().find(".required").addClass("ignorevalidation");
                $("a.yes", e).text("Yes, Non-Admit");
            } else {
                $(".nonadmission-patient-container,.discharge-patient-container,.active-patient-container", e).hide().find(".required").addClass("ignorevalidation");
                $("a.yes", e).text("Yes, Pending");
            }
        },
        InitChangePatientStatus: function (r, t, x, e) {
            $("[name=Status]", e).on("change", function () {
                Patient.Demographic.DisplayStatusContainer(e, $(this).val());
                $("#window_" + Acore.DialogId).SetHeight();
            });
            Patient.Demographic.DisplayStatusContainer(e, $("[name=Status]", e).val());
            U.InitForm($("form.mainform", e), function (data) { Refresh.RebindPatientGrids(data); });
        },
        ChangeStatus: function (Id, prefix) {
            Acore.Modal({
                Name: "Change Status",
                Url: prefix + "/PatientProfile/Status",
                Input: { patientId: Id },
                OnLoad: Patient.Demographic.InitChangePatientStatus,
                Width: 500
            });
        },

        InitReadmitted: function (r, t, x, e) { U.InitForm($("form.mainform", e), function (data) { Refresh.RebindPatientGrids(data); }); },
        ReadmitPatient: function (Id, prefix) {
            Acore.Modal({
                Name: "Patient Re-admit",
                Url: prefix + "/PatientProfile/Readmit",
                Input: { patientId: Id },
                OnLoad: Patient.Demographic.InitReadmitted,
                Width: 600
            });
        },

        LoadEditEmergencyContact: function (patientId, id) {
            Acore.Open("editemergencycontact", 'Patient/EditEmergencyContactContent', Patient.Demographic.InitEmergencyContact, { patientId: patientId, Id: id });
        },
        LoadNewEmergencyContact: function (patientId) {
            Acore.Open("newemergencycontact", 'Patient/NewEmergencyContactContent', Patient.Demographic.InitEmergencyContact, { patientId: patientId });
        },
        InitEmergencyContact: function (r, t, x, e) {
            U.InitValidation($("form.mainform", e), function () { Patient.Demographic.RebindEmergencyContact(); }, function () { });
        },
        DeleteEmergencyContact: function (id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this emergency contact?",
                Yes: function () {
                    U.PostUrl("Patient/DeleteEmergencyContact", { id: id, patientId: patientId }, function (result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Patient.Demographic.RebindEmergencyContact();
                        }
                    })
                }
            })
        },
        RebindEmergencyContact: function () {
            if (Acore.Windows[Patient.Demographic.AcoreEditId].IsOpen) {
                var e = $("#window_" + Patient.Demographic.AcoreEditId + "_content");
                if (e) {
                    var emergencyContact = $(".emergencycontacts", e).data('tGrid');
                    if (emergencyContact) {
                        emergencyContact.rebind();
                    }
                }
            }
        },
        AddPhysician: function (id, patientId, addButton) {
            var grid = $('#EditPatient_PhysicianGrid');
            addButton.trigger("ProcessingStart");
            U.TGridAjax("/Patient/AddPatientPhysician", { "id": id, "patientId": patientId }, grid,
                function (result) {
                    addButton.trigger("ProcessingComplete");
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful) {
                        $("[name=AgencyPhysicians_text]").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
                        U.RebindTGrid(grid);
                    }
                }
            );
        },
        DeletePhysician: function (id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this physician?",
                Yes: function () {
                    U.PostUrl("Patient/DeletePhysicianContact", { "id": id, "patientId": patientId }, function () { Patient.Demographic.RebindPhysician(); });
                }
            })
        },
        SetPrimaryPhysician: function (id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to set this physician as primary?",
                Yes: function () {
                    U.PostUrl("Physician/SetPrimary", { id: id, patientId: patientId }, function (result) {
                        U.Growl(data.errorMessage, data.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            U.RebindTGrid($("#EditPatient_PhysicianGrid"), { PatientId: result.PatientId });
                            Patient.LoadInfoAndActivity(result.PatientId);
                        }
                    })
                }
            })
        },
        RebindPhysician: function () {
            if (Acore.Windows[Patient.Demographic.AcoreEditId].IsOpen) {
                var e = $("#window_" + Patient.Demographic.AcoreEditId + "_content");
                if (e) {
                    var physicians = $(".patientphysicians", e).data('tGrid');
                    if (physicians) {
                        physicians.rebind();
                    }
                }
            }
        },
        OnLocationChange: function (e) {
            $("select.branch-location", e, "Insurances").on("change", function () { Agency.LoadInsurances($(this).val(), e, "Insurances", $(this).attr("service")); })
        },
        InitInsuranceChange: function (e) {
            $(".insurance", e).on("change", function () {
                var selectList = this;
                var value = $(this).val();
                if (value == "new") {
                    Acore.Open('newinsurance');
                    selectList.selectedIndex = 0;
                } else if (value == "spacer") {
                    selectList.selectedIndex = 0;
                }
                var id = $(this).attr("Id");
                if ($(this).find(":selected").attr("ishmo") == "1") {
                    $("#" + id + "Content").removeClass("hidden"); //.find("input,textarea,select").prop("disabled", false);
                } else {
                    $("#" + id + "Content").addClass("hidden"); //.find("input,textarea,select").prop("disabled", true);
                }
            });
        },
        NonAdmit: {
            InitList: function (r, t, x, e) {
                U.InitTGridServiceAndBranchFilter(r, t, x, e);
                var newAction = $(".new", e);
                if (newAction) { newAction.click(function () { Patient.Demographic.New(); }); }
            },
            Init: function (r, t, x, e) {
                if ($("[name=ServiceProvided]", e).length > 1) {
                    U.ServicesContainerInit("NonAdmit_Patient_ServiceContainer", e);
                }
                U.InitValidation($("form.mainform", e), function (data) { Refresh.RebindPatientGrids(data); });
            },
            Open: function (Id) {
                Acore.Modal({
                    Name: "Non-Admit Patient",
                    Url: "Patient/NewNonAdmit",
                    Input: { patientId: Id },
                    OnLoad: Patient.Demographic.NonAdmit.Init,
                    Width: 500
                })
            },
            OnDataBound: function (e) {
                var grid = $(e.target).data('tGrid');
                if (grid) {
                    var datas = grid.data;
                    if (datas.length > 0) {
                        var data = datas[0];
                        if (data != null) {
                            if (data.IsUserCanAdmit) grid.showColumn("IsUserCanAdmit");
                            else grid.hideColumn("IsUserCanAdmit");
                        }
                    }
                    else {
                        grid.hideColumn("IsUserCanAdmit");
                    }
                }
                U.OnTGridDataBound(e);
            }
        },
        Pending: {
            InitList: function (r, t, x, e) {
                U.InitTGridServiceAndBranchFilter(r, t, x, e);
                var newAction = $(".new", e);
                if (newAction) { newAction.click(function () { Patient.Demographic.New(); }); }
            },
            RebindGrid: function () {
                U.RebindTGrid($("#PatientPendingGridContainer .t-grid"));
            },
            OnDataBound: function (e) {
                var grid = $(e.target).data('tGrid');
                if (grid) {
                    var datas = grid.data;
                    if (datas.length > 0) {
                        var data = datas[0];
                        if (data != null) {
                            if (data.IsUserCanEdit || data.IsUserCanAdmit || data.IsUserCanNonAdmit) {
                                var width = 0;
                                if (data.IsUserCanEdit) width += 55;
                                if (data.IsUserCanAdmit) width += 55;
                                if (data.IsUserCanNonAdmit) width += 55;
                                $("td:eq(5),th:eq(5) ,col:eq(5)", $(e.target).find("tbody tr,colgroup").not(".t-footer-template")).show().css({ width: width });
                            }
                            else {
                                $("td:eq(5),th:eq(5), col:eq(5)", $(e.target).find("tbody tr,colgroup").not(".t-footer-template")).hide();
                            }
                        }
                    }
                    else {
                        $("td:eq(5),th:eq(5), col:eq(5)", $(e.target).find("tbody tr,colgroup").not(".t-footer-template")).hide();
                    }
                }
                U.OnTGridDataBound(e);
            }
        },
        Edit: {
            Init: function (r, t, x, e) {
                var type = "EditPatient", prefix = "#" + type + "_";
                $(prefix + "Tabs").tabs().on("tabsbeforeactivate", {
                    Id: $(prefix + "Id").val()
                }, function (event, ui) {
                    var tabLink = $("a", ui.newTab);
                    $(tabLink.attr("href")).Load(tabLink.attr("action"), { PatientId: event.data.Id }, Patient.Demographic.Edit.InitSection);
                });
                Patient.Demographic.Edit.InitSection(r, t, x, e.find(".tab-content:eq(0)"));
            },
            Open: function (id, prefix) {
                Acore.Open("EditPatient", { patientId: id });
            },
            LoadProfile: function (Id, prefix, isPrefixNeeded) {
                // /PatientProfile/EditPatientContent
                $("#Edit_Patient_" + prefix + " a").trigger("ProcessingStart");
                $("#Edit_Patient_" + prefix + "Content").Load((isPrefixNeeded ? prefix : "") + "/PatientProfile/EditPatientProfile", { patientId: Id }, function (responseText, textStatus, XMLHttpRequest) {
                    $("#Edit_Patient_" + prefix + " a").trigger("ProcessingComplete");
                    if (textStatus == 'error') { U.Growl("Could not load this profile information.", "error"); }
                    else if (textStatus == "success") { $(this).removeClass("loading"); $("#Edit_Patient_" + prefix + ">fieldset").hide(); }
                });
            },
            UnloadProfile: function (prefix) {
                $("#Edit_Patient_" + prefix + "Content").html("");
                $("#Edit_Patient_" + prefix + ">fieldset").show();
            },
            InitSection: function (r, t, x, e) {
                if (e && e.attr("id")) var Category = e.attr("id").split("_")[1];
                if (typeof Patient.Demographic.Edit.Sections[Category] == "function") Patient.Demographic.Edit.Sections[Category](r, t, x, e);
            },
            Sections: {
                Information: function (r, t, x, e) {
                    var form = $("form", e);
                    form.Validate();
                },
                HomeHealth: function (r, t, x, e) {
                    $(".branch-location.rebind.homehealth", e).on("change", function () {
                        var isRebind = $(this).attr("rebind");
                        if (isRebind == "True") { Patient.Demographic.OnLocationChange(e); }
                    });
                    Patient.Demographic.InitInsuranceChange(e);
                    $("form", e).Validate();
                },
                PrivateDuty: function (r, t, x, e) {
                    Patient.Demographic.InitInsuranceChange(e);
                    $("form", e).Validate();
                },
                VisitRates: function (r, t, x, e) {
                },
                Physicians: function (r, t, x, e) {
                    $("a.addphysician", e).on("click", function () {
                        var PhysId = $("[name=AgencyPhysicians]", e).val();
                        $("[name=AgencyPhysicians_text]", e).AjaxAutocomplete("reset");
                        if (U.IsGuid(PhysId)) Patient.Demographic.AddPhysician(PhysId, $("[name=Id]", e).val(), $(this));
                        else U.Growl("Please select a Physician.", "error");
                    });
                },
                EmergencyContacts: function (r, t, x, e) {
                }
            }
        },
        LoadNewServiceProfile: function (control) {
            if ($(control).is(':checked')) { $(control).closest("form").find("#NewPatient_ServiceContainer" + $(control).val()).show(); }
            else { $(control).closest("form").find("#NewPatient_ServiceContainer" + $(control).val()).hide(); }
        },
        Admit: {
            Init: function (r, t, x, e) {
                var tab = $(".maintab", e);
                var postData = U.JsonInput($(".tabinput", tab).serializeArray());
                tab.tabs({
                    beforeLoad: function (event, ui) {
                        ui.ajaxSettings.type = 'POST';
                        ui.ajaxSettings.hasContent = true;
                        ui.jqXHR.setRequestHeader("Content-Type", ui.ajaxSettings.contentType);
                        ui.ajaxSettings.data = jQuery.param(postData);
                        ui.panel.addClass("loading tab-content");
                    },
                    load: function (event, ui) {
                        var category = $("a", ui.tab).attr("name");
                        ui.panel.removeClass("loading");

                        Patient.Demographic.Admit.Sections[category](null, null, null, ui.panel)
                        ui.panel.InitForms();
                    },
                    beforeActivate: function (event, ui) {
                        if ($("a", ui.newTab).attr("href").indexOf("#") != -1) $("a", ui.newTab).attr("href", $("a", ui.newTab).attr("url"));
                    },
                    create: function (event, ui) { Patient.Demographic.Admit.Sections.Info(r, t, x, e); }
                });

            },
            Open: function (id, url) {
                Acore.Modal({
                    Name: "Admit Patient",
                    Url: "/Patient/NewAdmit",
                    Input: { id: id },
                    OnLoad: Patient.Demographic.Admit.Init,
                    Width: 1000,
                    Height: 475,
                    Overflow: false
                });
            },
            Sections: {
                Info: function (r, t, x, e) {
                    $("form.tabform", e).Validate();
                },
                HomeHealth: function (r, t, x, e) {
                    Patient.Demographic.InitInsuranceChange(e);
                    Patient.Demographic.OnLocationChange(e);
                    $(".startofcaredate", e).on("change", function () {
                        var val = $(this).datepicker("getDate");
                        $(".episodestartdate", e).datepicker("setDate", val);
                        $(".episodestartdate", e).datepicker("option", "mindate", val);
                    });
                    if (!$("#Admit_Patient_PrivateDuty").length) $(".save.next", e).parent().remove();
                    $("form.tabform", e).Validate({ Success: Patient.Demographic.Pending.RebindGrid });
                },
                PrivateDuty: function (r, t, x, e) {
                    Episode.PrivateDuty.InitCarePeriodLength(e);
                    Patient.Demographic.InitInsuranceChange(e);
                    Patient.Demographic.OnLocationChange(e);
                    $("form.tabform", e).Validate({ Success: Patient.Demographic.Pending.RebindGrid });
                }
            }
        }
    }
});