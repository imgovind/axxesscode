﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    Admission: {
        WindowId: "window_patientmanageddates",
        Service: "",
        Init: function(r, t, x, e) {
            Patient.Demographic.InitInsuranceChange(e);
            var form = $("form", e);
            form.Validate({ Success: function() { Patient.Admission.Refresh(); } });
        },
        Delete: function(patientId, id) {
            U.Delete("Admission period", window[Patient.Admission.Service].Area + "/Admission/Delete", { patientId: patientId, Id: id }, function(result) {
                if (result.isSuccessful) Patient.Admission.Refresh();
            });
        },
        MarkAsCurrent: function(patientId, id) {
            Acore.Confirm({
                Message: "Are you sure you want to mark this admission as active? This will update this admission with the current patient information.",
                Yes: function() {
                    U.PostUrl(window[Patient.Admission.Service].Area + "/Admission/MarkAsCurrent", { patientId: patientId, Id: id }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) Patient.Admission.Refresh();
                    });
                }
            });
        },
        Edit: function(patientId, id, type) {
            Acore.Open(type.toLowerCase() + "patientadmission", window[Patient.Admission.Service].Area + "/Admission/PatientInfo", { patientId: patientId, Id: id, Type: type });
        },
        Load: function(id, service) {
            Patient.Admission.Service = service;
            Acore.Open("patientmanageddates", window[service].Area + "/Admission/List", Patient.Admission.InitList, { patientId: id });
        },
        InitList: function(r, t, x, e) {
            U.InitTGridServiceAndBranchFilter(r, t, x, e);
            $("[name=ServiceId]", e).on("change", function() {
                Patient.Admission.Service = $(this).val();
            });
            $(".add-button", e).on("click", function() {
                Patient.Admission.Edit($("[name=PatientId]", e).val(), U.GuidEmpty, 'New', window[Patient.Admission.Service].Area);
            });
        },
        OnDetailGridLoad: function(e) {
            var label = Episode[$('#PatientAdmissionPeriod_Service').val()].Label,
                startDate = $("th[name=StartDate] a", $(this)),
                endDate = $("th[name=EndDate] a", $(this));
            startDate.text(label + " " + startDate.text());
            endDate.text(label + " " + endDate.text());
        },
        OnDetailRowDataBound: function(e) {
            var action = $(e.row).find("td:last a"),
                label = Episode[$('#PatientAdmissionPeriod_Service').val()].Label;
            action.text(action.text() + " " + label);
        },
        Refresh: function() {
            $("#" + Patient.Admission.WindowId).find(".grid-refresh").click();
        }
    }
});