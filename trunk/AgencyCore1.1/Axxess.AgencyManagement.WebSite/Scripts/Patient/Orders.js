﻿if (typeof Patient.Orders == "undefined") $.extend(Patient, { Orders: new Object() });
$.extend(Patient.Orders, {
    WindowId: "patientordershistory",
    LoadHistory: function(patientId, area) {
        Acore.Open(Patient.Orders.WindowId, area + '/OrdersManagement/PatientHistory', { patientId: patientId });
    },
    HistoryInit: function(r, t, x, e) {
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
    },
    OnRowDataBound: function(e) {
        Agency.OrdersManagement.OrdersPrintableOnRowDataBound(e);
        var deleteLink = $(e.row).find(".delete");
        if (deleteLink.length) {
            deleteLink.click(function() {
                var result = CommandFactory.Delete(deleteLink, { PatientId: e.dataItem.PatientId, Id: e.dataItem.Id }, Acore.AppService(e.dataItem.Service));
                eval(result);
            });
        }
    },
    OnDataBound: function(e) {
        var grid = $(e.target).data("tGrid");
        if (grid) {
            if (grid.total) {
                var index = grid.columns.length - 1;
                if (!$(e.target).find("a.link").length) grid.hideColumn(index);
                else grid.showColumn(index);
                if (!$(e.target).find("a.print").length) grid.hideColumn(index - 1);
                else grid.showColumn(index - 1);
            }
            U.OnTGridDataBound(e);
        }
    },
    RebindHistory: function(id, serviceId) {
        if (Acore.Windows[Patient.Orders.WindowId].IsOpen) {
            var e = $("#window_" + Patient.Orders.WindowId);
            if (e) {
                var service = window.find("[name=ServiceId]").val();
                if (service && service == serviceId) {
                    var patientId = e.find("[name=PatientId]").val();
                    if (patientId) {
                        if (U.IsGuid(patientId) && patientId == id) {
                            $(".grid-refresh", e).click();
                        }
                    }
                    else {
                        $(".grid-refresh", e).click();
                    }
                }
            }
        }
    },
    DeleteOrder: function(id, patientId, episodeId, url, callback) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this task?",
            Yes: function() {
                U.PostUrl(url, { id: id, patientId: patientId, episodeId: episodeId }, function(result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful && typeof callback == "function") callback(result);
                });
            }
        });
    },
    Episode: {
        WindowId: "patientepisodeorders",
        Open: function(episodeId, patientId, e) {
            Acore.Open("patientepisodeorders", { episodeId: episodeId, patientId: patientId });
            if (e) e.stopPropagation();
        },
        Refresh: function(patientId, episodeId) {
            if (Acore.Windows[Patient.Orders.Episode.WindowId].IsOpen) {
                var e = $("#window_" + Patient.Orders.Episode.WindowId);
                if (e) {
                    var currentPatientId = e.find("[name=PatientId]").val();
                    var currentEpisodeId = e.find("[name=EpisodeId]").val();
                    if (currentPatientId && currentEpisodeId) {
                        if (currentPatientId == patientId && currentEpisodeId == episodeId) {
                            var grid = $('#EpisodeOrders_Grid', e).data('tGrid');
                            if (grid != null) { grid.rebind({ patientId: patientId, episodeId: episodeId }); }
                        }
                    }
                }
            }
        }
    },
    InitNewFaceToFaceEncounter: function(r, t, x, e) {
        Episode.InitPatientChangeDropDown(e, "HomeHealth");
        U.InitValidation($("form", e), Refresh.All);
    }
});