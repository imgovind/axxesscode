﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    VisitRate: {
        Prefix: "PrivateDuty/PatientProfile",
        New: function(patientId) {
            Acore.Modal({
                Name: "Visit Rate Info.",
                Url: Patient.VisitRate.Prefix + "/NewVisitRate",
                Input: { PatientId: patientId },
                OnLoad: Patient.VisitRate.InitShared,
                Width: "620px",
                WindowFrame: false
            });
        },
        Edit: function(patientId, Id) {
            Acore.Modal({ Name: "Visit Rate Info.", Url: Patient.VisitRate.Prefix + "/EditVisitRate", Input: { PatientId: patientId, Id: Id }, OnLoad: Patient.VisitRate.InitShared, Width: "620px",  WindowFrame: false });
        },
        InitShared: function(r, t, x, e) {
            Rate.InitCommon(r, t, x, e);
            e.find("form.mainform").Validate({ Success: Patient.VisitRate.RebindGrid });
        },
        Delete: function(patientId, Id) {
            U.Delete("Patient visit rate", Patient.VisitRate.Prefix + "/DeleteVisitRate", { PatientId: patientId, Id: Id }, Patient.VisitRate.RebindGrid);
        },
        RebindGrid: function() {
            U.RebindTGrid($("#EditPatient_VisitRatesGrid"));
        },
        ReplaceAll: function(Id, replacedId) {
            if (replacedId <= 0) {
                U.Growl("Please select an insurance.", "error");
                return;
            }
            else {
                Acore.Confirm({
                    Message: "Are you sure you want to replace/overwrite visit information?",
                    Yes: function() {
                        U.PostUrl(Patient.VisitRate.Prefix + "/ReplaceVisitRates", { PatientId: Id, replacedId: replacedId }, function(result) {
                            if (result.isSuccessful) {
                                Patient.VisitRate.RebindGrid();
                                U.Growl(result.errorMessage, "success");
                            } else {
                                U.Growl(result.errorMessage, "error");
                            }
                        });
                    }
                });
            }
        }
    }
});