﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    VitalSigns: {
        AcoreId: "PatientVitalSigns",
        CookieId: "PatientVitalSigns",
        PatientId: "",
        CurrentView: "vitals-graphs",
        DisplayCustomization: new Array(),
        StartDate: "",
        EndDate: "",
        ChangePatient: function(patientId) {
            var e = $("#window_" + Patient.VitalSigns.AcoreId);
            Patient.VitalSigns.PatientId = patientId;
            $("[name=PatientId]", e).val(patientId);
            Patient.VitalSigns.Refresh();
        },
        Export: function(patientId, startDate, endDate) {
            U.GetAttachment("Report/VitalSigns", {
                PatientId: U.IsGuid(patientId) ? patientId : Patient.VitalSigns.PatientId,
                StartDate: U.IsDate(startDate) ? startDate : Patient.VitalSigns.StartDate,
                EndDate: U.IsDate(endDate) ? endDate : Patient.VitalSigns.EndDate
            })
        },
        Init: function(r, t, x, e) {
            if (!Patient.VitalSigns.PatientId) Patient.VitalSigns.PatientId = $("#Patient_VitalSignsCharts_PatientId", e).val();
            $(e).closest(".window").on("resizestop", function() { $(".acore-graph", e).Graph("resize") });
            $(".vitals-report-show").hide();
            $(".buttons.continuous a").on("click", function() { if (!$(this).parent().hasClass("selected")) Patient.VitalSigns.SwitchView(e, $(this).attr("class")) });
            $(".grid-refresh", e).on("click", function() { Patient.VitalSigns.Refresh() });
            $(".export", e).on("click", function() { Patient.VitalSigns.Export() });
            $(".grid-controls .date-picker", e).on("change", function() { Patient.VitalSigns.UpdateViewData() });
            $(".grid-controls .patient-picker", e).on("change", function() {
                Patient.VitalSigns.UpdateViewData();
                Patient.VitalSigns.Refresh();
            });
            $(".vitals-content", e).sortable({
                stop: function(event, ui) {
                    $(event.toElement).one("click", function(e) { e.stopImmediatePropagation() });
                    Patient.VitalSigns.UpdateViewData();
                }
            });
            if (Acore.Cookies) Patient.VitalSigns.UpdateFromCookie(e);
            else Patient.VitalSigns.InitContent(r, t, x, $(".vitals-content", e));
        },
        InitContent: function(r, t, x, e) {
            $("h3", e).on("click", function() {
                var element = $(this).parent();
                U.ToggleCollapsableContent(element, function() {
                    Patient.VitalSigns.UpdateViewData();
                    $(".acore-graph", element).Graph("resize");
                })
            });
            if (Patient.VitalSigns.CurrentView == "vitals-graphs") for (var i = 0; i < Patient.VitalSigns.DisplayCustomization.length; i++) {
                $(Patient.VitalSigns.DisplayCustomization[i].element).appendTo(e);
                if (Patient.VitalSigns.DisplayCustomization[i].collapsed) U.ToggleCollapsableContent($(Patient.VitalSigns.DisplayCustomization[i].element));
            }
            Patient.VitalSigns.UpdateViewData();
        },
        Load: function(patientId, area) {
            if (Acore.Windows[Patient.VitalSigns.AcoreId].IsOpen) {
                $("#window_" + Patient.VitalSigns.AcoreId).WinFocus();
                if (U.IsGuid(patientId) && patientId != Patient.VitalSigns.PatientId) Acore.Confirm({
                    Message: "Are you sure you want to change patients in the vital signs charts?",
                    Yes: function() { Patient.VitalSigns.ChangePatient(patientId) }
                });
            } else {
                Patient.VitalSigns.PatientId = patientId;
                Acore.Open(Patient.VitalSigns.AcoreId, area + "/Note/VitalSignsCharts", { patientId: patientId });
            }
        },
        Print: function(patientId, startDate, endDate) {
            // To Be Completed Later
        },
        Refresh: function() {
            var e = $("#window_" + Patient.VitalSigns.AcoreId);
            $(".vitals-content", e).Load(Acore.Windows[Patient.VitalSigns.AcoreId].Area + "/Note/VitalSignsChartsContent", {
                PatientId: Patient.VitalSigns.PatientId,
                CurrentView: Patient.VitalSigns.CurrentView,
                StartDate: Patient.VitalSigns.StartDate,
                EndDate: Patient.VitalSigns.EndDate
            }, Patient.VitalSigns.InitContent, true)
        },
        SwitchView: function(e) {
            $(".vitals-report-show,.vitals-graphs-show", e).toggle();
            $(".buttons.continuous li", e).toggleClass("selected");
            Patient.VitalSigns.CurrentView = $(".buttons.continuous .selected a", e).attr("class");
            Patient.VitalSigns.Refresh();
        },
        UpdateFromCookie: function(e) {
            var PreviousState = U.Cookie.Get(Patient.VitalSigns.CookieId);
            if (PreviousState) {
                Patient.VitalSigns.DisplayCustomization = PreviousState.DisplayCustomization;
                if (U.IsDate(PreviousState.StartDate)) $("[name=StartDate]", e).val(Patient.VitalSigns.StartDate = PreviousState.StartDate);
                if (U.IsDate(PreviousState.EndDate)) $("[name=EndDate]", e).val(Patient.VitalSigns.EndDate = PreviousState.EndDate);
                if (PreviousState.CurrentView == "vitals-report") Patient.VitalSigns.SwitchView(e)
                else Patient.VitalSigns.InitContent("", "", "", $(".vitals-content", e));
            }
        },
        UpdateToCookie: function() {
            U.Cookie.Set(Patient.VitalSigns.CookieId, {
                CurrentView: Patient.VitalSigns.CurrentView,
                StartDate: Patient.VitalSigns.StartDate,
                EndDate: Patient.VitalSigns.EndDate,
                DisplayCustomization: Patient.VitalSigns.DisplayCustomization
            });
        },
        UpdateViewData: function() {
            var e = $("#window_" + Patient.VitalSigns.AcoreId);
            if (U.IsGuid($("[name=PatientId]", e).val())) Patient.VitalSigns.PatientId = $("[name=PatientId]", e).val();
            if (Patient.VitalSigns.CurrentView == "vitals-report") {
                Patient.VitalSigns.StartDate = $("[name=StartDate]", e).val();
                Patient.VitalSigns.EndDate = $("[name=EndDate]", e).val();
            } else {
                Patient.VitalSigns.DisplayCustomization = new Array();
                $(".vitals-content", e).children().each(function() {
                    Patient.VitalSigns.DisplayCustomization.push({
                        element: "#" + $(this).attr("id"),
                        collapsed: $("h3 > .img", this).hasClass("more")
                    })
                })
            }
            if (Acore.Cookies) Patient.VitalSigns.UpdateToCookie();
        }
    }
});