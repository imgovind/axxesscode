﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    Charts: {
        // Patient Charts Variables
        AcoreId: "PatientCharts",   // Acore Window ID for Patient Charts
        CookieId: "PatientCharts",  // Name for cookie to store previous state
        Date: "",                   // Current Active Date Range
        Discipline: "",             // Current Active Discipline
        EndDate: "",                // If Custom Date Range, Current End Date
        PatientId: "",              // Current Active Patient
        Service: 1,                 // Current Service
        StartDate: "",              // If Custom Date Range, Current Start Date
        // Initialize Schedule Center
        Init: function(r, t, x, e) {
            // Set layout plugin for patient selector
            $(".layout", e).Layout();
            // Set patient selector filters
            Patient.Charts.PatientSelector.BranchFilter(e);
            $("[name=ServicesId]", e).on("change", function() { Patient.Charts.PatientSelector.BranchFilter(e) });
            $(".ui-layout-west select", e).on("change", function() { Patient.Charts.PatientSelector.Rebind(e) });
            $("[name=SearchText]", e).keyup(function() { Patient.Charts.PatientSelector.TextFilter(e) });
            Patient.Charts.PatientSelector.OnFirstDataBound($("input[name=CurrentPatientId]", e).val(), e);
            var layoutCenter = $(".ui-layout-center", e);
            if (layoutCenter) Patient.Charts.InitContent(r, t, x, layoutCenter);
        },
        InitContent: function(r, t, x, e) {
            $(".window-menu", e).Menu();
            var service = Acore.AppService(Patient.Charts.Service),
                area = window[service].Area;
            $(".new-order", e).on("click", function() {
                PhysicianOrder[service].New(Patient.Charts.PatientId);
            });
            $(".new-comnote", e).on("click", function() {
                CommunicationNote[service].New(Patient.Charts.PatientId);
            });
            $(".new-auth", e).on("click", function() {
                Patient.Authorization.New(Patient.Charts.PatientId, area);
            });
            $(".patient-profile", e).on("click", function() {
                Acore.OpenPrintView({
                    Url: area + "/PatientProfile/Profile/" + Patient.Charts.PatientId,
                    PdfUrl: area + "/PatientProfile/ProfilePdf",
                    PdfData: { id: Patient.Charts.PatientId }
                });
            });
            $(".med-profile", e).on("click", function() {
                Medication.Profile(Patient.Charts.PatientId, service);
            });
            $(".allergy-profile", e).on("click", function() {
                Allergy.List(Patient.Charts.PatientId, service);
            });
            $(".auth-list", e).on("click", function() {
                Patient.Authorization.LoadList(Patient.Charts.PatientId, area);
            });
            $(".comnote-list", e).on("click", function() {
                CommunicationNote[service].LoadList(Patient.Charts.PatientId);
            });
            $(".order-list", e).on("click", function() {
                Patient.Orders.LoadHistory(Patient.Charts.PatientId, area);
            });
            $(".summary-list", e).on("click", function() {
                Visit.SixtyDaySummary.List(Patient.Charts.PatientId);
            });
            $(".vital-signs", e).on("click", function() {
                Patient.VitalSigns.Load(Patient.Charts.PatientId, area);
            });
            $(".triage-class", e).on("click", function() {
                Acore.OpenPrintView({
                    Url: area + "/PatientProfile/Triage/" + Patient.Charts.PatientId,
                    PdfUrl: area + "/PatientProfile/TriagePdf/",
                    PdfData: { id: Patient.Charts.PatientId }
                });
            });
            $(".deleted-list", e).on("click", function() {
                Schedule.Task.DeletedList(Patient.Charts.PatientId, area);
            });
            $(".schedule", e).on("click", function() {
                if (Patient.Charts.Service == 1) UserInterface.ShowScheduleCenter(Patient.Charts.PatientId, $(this).attr("discharged"));
                else if (Patient.Charts.Service == 2) Schedule.Center.PrivateDuty.Load(Patient.Charts.PatientId, $(this).attr("displayname"), true);
            });
            $(".reassign", e).on("click", function() {
                Schedule.Task[Acore.AppService(Patient.Charts.Service)].MultipleReassign(Patient.Charts.PatientId);
            });
            $(".new-upload", e).on("click", function() {
                Patient.Document.New(Patient.Charts.PatientId);
            });
            $(".upload-list", e).on("click", function() {
                Patient.Document.Load(Patient.Charts.PatientId);
            });
            $(".user-access", e).on("click", function() {
                Patient.UserAccess.List(Patient.Charts.PatientId);
            });
            $(".edit-photo", e).on("click", function() {
                Patient.Demographic.NewPhoto(Patient.Charts.PatientId);
            });
            $(".edit-patient", e).on("click", function() {
                Patient.Demographic.Edit.Open(Patient.Charts.PatientId);
            });
            $(".patient-moreinfo", e).on("click", function() {
                Patient.Charts.MoreInfo(Patient.Charts.PatientId);
            });
            $(".patient-map", e).on("click", function() {
                Patient.Demographic.Maps(Patient.Charts.PatientId);
            });
            $(".admit-list", e).on("click", function() {
                Patient.Admission.Load(Patient.Charts.PatientId, service);
            });
            $(".refresh", e).on("click", function() {
                Patient.Charts.Load(Patient.Charts.PatientId);
            });
            $(".change-status", e).on("click", function() {
                Patient.Demographic.ChangeStatus(Patient.Charts.PatientId, service);
            });
            $(".readmit", e).on("click", function() {
                Patient.Demographic.ReadmitPatient(Patient.Charts.PatientId, window[Acore.AppService(Patient.Charts.Service)].Area);
            });
            Patient.Charts.Activities.Init(r, t, x, e);
        },
        Load: function(patientId) {
            var e = $("#window_" + Patient.Charts.AcoreId);
            Patient.Charts.PatientId = patientId;
            if (!Acore.Windows[Patient.Charts.AcoreId].IsOpen) Acore.Open(Patient.Charts.AcoreId, { PatientId: patientId, Service: 0 });
            else {
                var serviceId = $(".ui-layout-west .top [name=ServicesId].filterInput", e).val();
                if (serviceId) {
                    var area = window[Acore.AppService(serviceId)].Area;
                    Patient.Charts.Service = serviceId;
                    e.WinFocus();
                    if (patientId != U.GuidEmpty) {
                        Patient.Charts.PatientSelector.Select(patientId, e);
                        $(".ui-layout-center", e).Load(area + "/PatientProfile/Data", { patientId: patientId }, Patient.Charts.InitContent, false, true);
                    }
                }
            }
        },
        // More Patient Information Popup
        MoreInfo: function(id) {
            if (id == undefined) id = Patient.Charts.PatientId;
            Acore.Dialog({
                Name: "Patient Information",
                Url: "PatientProfile/MoreInfo",
                Input: { id: id },
                VersitileHeight: true,
                Width: 450
            });
        },
        Activities: {
            FixPosition: function(e) {
                $(e).Animate({ top: $(e).closest(".ui-layout-center").find(".top").height() + $(".above", e).height() }, function() { $(".icon-overlay", e).show() });
            },
            HideLegend: function(e) {
                $(".note-legend", e).Animate({ top: 0 });
                $(".icon-overlay", e).removeClass("hover");
            },
            Init: function(r, t, x, e) {
                var bottom = $(".bottom", e);
                $(".grid-resize", bottom).on("click", function() {
                    Patient.Charts.Activities.Resize(bottom);
                });
                $(".above select", bottom).on("change", function() {
                    Patient.Charts.Activities.Rebind(bottom);
                });
                $(".above .activity-search", bottom).on("click", function() {
                    Patient.Charts.Activities.Rebind(bottom);
                });
                Patient.Charts.Activities.OnFirstDataBound($(".patient-activity-grid", bottom));
                $(".icon-overlay,td.icon-overlayed", bottom).on("mouseover", function() {
                    Patient.Charts.Activities.ShowLegend(bottom);
                }).mouseleave(function() {
                    Patient.Charts.Activities.HideLegend(bottom);
                });
                $(".icon-overlay", bottom).css("width", U.GetWidth($("th.icon-header-overlayed", bottom)) - 2 + "px");
                Patient.Charts.Activities.FixPosition(bottom);
                U.ActivityGridContextMenuInit($(".t-grid", e));
            },
            OnDataBound: function(e) { U.OnTGridDataBound(e); },
            OnFirstDataBound: function(gridContent) {
                var grid = gridContent.data('tGrid');
                if (grid) {
                    U.StopTGridFirstAction(grid);
                    grid.$tbody.closest(".t-grid-content").removeClass("loading");
                    var trs = $("tr", grid.$tbody);
                    var serviceName = "";
                    if (trs.length > 0) {
                        trs.each(function(index, element) {
                            var dataItem = { IsOrphaned: $(this).hasClass('orphaned'), IsComplete: $(this).hasClass('completed') },
                                gridAction = $(".grid-action", element),
                                menu = gridAction.find(".action-menu");
                            if (menu) gridAction.data("menu", menu.detach());
                            U.ToolTip(element);
                            U.RowColorCode(element, dataItem);
                            //U.GridContext(element, dataItem);
                            if (index == 0) {
                                serviceName = $(this).find("td.service").text();
                            }
                            CommandFactory.GridInit(element, { Id: $(this).find("td.id").text(), PatientId: $(this).find("td.pid").text(), EpisodeId: $(this).find("td.eid").text(), Type: $(this).find("td.type").text() }, serviceName);
                        });
                    }
                }
            },
            OnRowDataBound: function(e, serviceName) {
                var dataItem = e.dataItem;
                if (dataItem) {
                    U.ToolTip(e.row);
                    U.RowColorCode(e.row, dataItem);
                    //U.GridContext(e.row, dataItem);
                    CommandFactory.GridInit(e.row, dataItem, serviceName);
                }
            },
            Rebind: function(e) {
                if (!e) e = $("#window_" + Patient.Charts.AcoreId + " .ui-layout-center");
                var gridControl = $(".patient-activity-grid", e);
                if (gridControl) {
                    var grid = gridControl.data("tGrid");
                    if (grid) {
                        var url = grid.ajax.selectUrl.split("?")[0].replace('ActivitySort', 'Activity');
                        U.OnTGridDataBinding(e);
                        var filterBar = $(".bottom .above", e),
                            input = $(".activityFilterInput", filterBar).serializeArray();
                        if (filterBar && input[2].name == "DateRangeId") {
                            var dateRangeId = input[2].value;
                            if (dateRangeId == "DateRange") {
                                $(".bottom .above #date-range-text").hide();
                                $(".bottom .above .custom-date-range").show();
                                input.push({ name: "RangeStartDate", value: $("input[name=RangeStartDate]", filterBar).val() });
                                input.push({ name: "RangeEndDate", value: $("input[name=RangeEndDate]", filterBar).val() });
                            }
                            else if (dateRangeId == "All") {
                                $("#date-range-text,.custom-date-range", filterBar).hide();
                            }
                            else {
                                $("#date-range-text", filterBar).show();
                                $(".custom-date-range", filterBar).hide();
                            }
                        }
                        U.PostUrl(url, input, function(result) {
                            grid.dataBind(result.ScheduleEvents);
                            $(".icon-overlay", bottom).css("width", U.GetWidth($("th.icon-header-overlayed", bottom)) - 2 + "px");
                            if (filterBar) { $("#date-range-text", filterBar).empty().append(result.Range); }
                        }, function() { Patient.Charts.Activities.OnDataBound(gridControl) });
                    }
                }
            },
            Resize: function(e) {
                if ($(".grid-resize", e).hasClass("grid-expand")) $(e).Animate({ top: $(".above", e).height() }, function() { $(".icon-overlay", e).hide() });
                else Patient.Charts.Activities.FixPosition(e);
                $(".grid-resize", e).toggleClass("grid-expand grid-reduce");
            },
            RebindInput: function() {
                return $(".ui-layout-center  .bottom .above .activityFilterInput", $("#window_" + Patient.Charts.AcoreId)).serializeArray();
            },
            ShowLegend: function(e) {
                $(".note-legend", e).Animate({ top: $(".note-legend", e).height() * -1 });
                $(".icon-overlay", e).addClass("hover");
            }
        },
        RebindInput: function() { return $("#window_" + Patient.Charts.AcoreId + " .ui-layout-west .top .filterInput").serializeArray(); },
        PatientSelector: {
            BranchFilter: function(e) { Patient.Charts.Service = $("[name=ServicesId]", e).val(); U.ServiceBranchFilter(e, Patient.Charts.Service); },
            OnDataBinding: function(e) {
                var inputs = U.JsonInput($(e.currentTarget).closest(".ui-layout-west").find(".filterInput").serializeArray());
                var area = window[Acore.AppService(inputs["ServicesId"])].Area;
                U.OnDataBindingStopAction(e, inputs, area + "/PatientProfile/AllSort");
            },
            OnFirstDataBound: function(patientId, e) {
                var grid = Patient.Charts.PatientSelector.Select(patientId, e);
                if (grid) {
                    U.StopTGridFirstAction(grid);
                    Patient.Charts.PatientId = patientId;
                }
            },
            Select: function(patientId, e) {
                if (!e) e = $("#window_" + Patient.Charts.AcoreId + "_content");
                var grid = $('.patient-list', e);
                if (grid) {
                    var alreadySelectedTr = $('tr.t-state-selected', grid);
                    var selectedTr = $('td:contains(' + patientId + ')', grid).closest('tr');
                    if (selectedTr.length && (!alreadySelectedTr.length || !alreadySelectedTr.is(selectedTr))) {
                        alreadySelectedTr.removeClass("t-state-selected");
                        selectedTr.addClass("t-state-selected");
                        var gridContent = selectedTr.closest(".t-grid-content");
                        if (gridContent) {
                            var position = selectedTr.position();
                            if (position != undefined) {
                                var scroll = position.top + gridContent.scrollTop() - 24;
                                if (scroll && scroll > 24) gridContent.animate({ scrollTop: scroll }, 'slow');
                            }
                        }
                    }
                }
                return grid;
            },
            OnDataBound: function(e) {
                U.OnTGridDataBound(e);
                var westContent = $(e.currentTarget).closest(".layout");
                Patient.Charts.PatientSelector.TextFilter(westContent);
                var gridContentTrs = $(".ui-layout-west .t-grid-content tr", westContent).not(".t-no-data");
                if (gridContentTrs.length > 0) {
                    var tdWithPatientId = $('td:contains(' + Patient.Charts.PatientId + ')', gridContentTrs);
                    if (tdWithPatientId.length > 0) tdWithPatientId.eq(0).closest('tr').click();
                    else gridContentTrs.eq(0).click();
                } else $(".ui-layout-center", westContent).removeClass("loading").html(U.MessageWarn("No Patients", "No Patients found that fit your search criteria.", [{ Text: "Add New Patient", Click: function() { Patient.New() } }]));
            },
            OnRowSelected: function(e) {
                var gridContent = $(e.currentTarget).find(".t-grid-content");
                if (gridContent) {
                    var patientId = $("td.pid", e.row).text();
                    var scroll = $(e.row).position().top + gridContent.scrollTop() - 24;
                    gridContent.Animate({ scrollTop: scroll });
                    if (U.IsGuid(patientId)) {
                        Patient.Charts.Load(patientId);
                        gridContent.closest(".ui-layout-west").find("input[name=CurrentPatientId]").val(patientId);
                    }
                }
            },
            Rebind: function(e, input) {
                if (!e) e = $("#window_" + Patient.Charts.AcoreId);
                if (!input) input = $(".ui-layout-west .filterInput", e).serializeArray();
                var jsonInput = U.JsonInput(input),
                    area = window[Acore.AppService(jsonInput["ServicesId"])].Area,
                    grid = $(".ui-layout-west .t-grid", e);
                U.OnTGridDataBinding(grid);
                U.PostUrl(area + "/PatientProfile/All", input, function(result) {
                    grid.data("tGrid").dataBind(result);
                });
            },
            TextFilter: function(e, load) {
                // Set search string
                var text = $(".ui-layout-west .top input[name=SearchText]", e).val();
                if (text && text.length) {
                    // Apply search
                    var search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    // If set to autoload patient (default), load first listed patient
                    if (load != false) {
                        if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                        else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters.", [{ Text: "Add New Patient", Click: function() { Patient.New() } }]));
                    }
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            }
        }
    }
});