﻿if (typeof Patient.Authorization == "undefined") $.extend(Patient, { Authorization: new Object() });
$.extend(Patient.Authorization, {
    WindowId: 'window_listauthorizations',
    New: function (patientId, area) {
        if (!area) area = '';
        Acore.Open("newauthorization", area + '/Authorization/New', { patientId: patientId });
    },
    Edit: function (patientId, id, area) {
        Acore.Open("editauthorization", area + '/Authorization/Edit', { patientId: patientId, Id: id });
    },
    LoadList: function (patientId, area) {
        Acore.Open("listauthorizations", area + '/Authorization/List', { patientId: patientId });
    },
    InitNew: function (r, t, x, e) {
        var form = $("form", e),
            serviceElement = $("#LimitedService", form),
            locationElement = $("[name=AgencyLocationId]", form);
        if (serviceElement.is("select")) {
            Patient.Authorization.ShowHideLocationsBasedOnService(locationElement, serviceElement.val());
            serviceElement.on("change", function (event) {
                Patient.Authorization.ServiceDropDownChanged(event, form);
            });
        }
        $("[name=PatientId]", form).on("change", function () {
            var patientId = $(this).val();
            locationElement.trigger("ProcessingStart");
            $("#NewAuthorization_Service", form).empty().load("Patient/AccessibleServices", { patientId: patientId },
				function (r2, t2, x2, e2) {
				    var service = $("#LimitedService", e2);
				    Patient.Authorization.ServiceDropDownChanged(service, form, true);
				    if (service.is("select")) service.on("change", function (event) { Patient.Authorization.ServiceDropDownChanged(event, e); });
				}
			);
        });
        U.InitValidation(form, function (result) { Patient.Authorization.Refresh(result.PatientId, result.ServiceId); });
    },
    InitEdit: function (r, t, x, e) {
        var serviceElement = $("#LimitedService", e);
        if (serviceElement.is("select")) {
            Patient.Authorization.ShowHideLocationsBasedOnService($("[name=AgencyLocationId]", e), serviceElement.val());
            serviceElement.on("change", function (event) {
                Patient.Authorization.ServiceDropDownChanged(event, e);
            });
        }
        $("form", e).Validate({ Success: function (result) { Patient.Authorization.Refresh(result.PatientId, result.ServiceId); } });
    },
    InitList: function (r, t, x, e) {
        U.InitTGridServiceAndBranchFilter(r, t, x, e);
        $(".add-authorization", e).on("click", function () {
            Patient.Authorization.New($("[name=PatientId]", e).val(), window[Acore.AppService($("[name=ServiceId]", e).val())].Area);
        });
    },
    OnRowDataBound: function (e) {
        var dataItem = e.dataItem;
        $("a.print", e.row).on("click", function () {
            var area = "";
            var container = $(this).closest("div.wrapper");
            var serviceId = container.find("[name=ServiceId]").val();
            if (serviceId) {
                var area = window[Acore.AppService(serviceId)].Area;
                U.GetAttachment(area + '/PatientProfile/AuthorizationPdf', { patientId: container.find("[name=PatientId]").val(), id: dataItem.Id });
            }
        });
        $("a.edit", e.row).on("click", function () {
            var area = "";
            var container = $(this).closest("div.wrapper");
            var serviceId = container.find("[name=ServiceId]").val();
            if (serviceId) {
                var area = window[Acore.AppService(serviceId)].Area;
                Patient.Authorization.Edit(container.find("[name=PatientId]").val(), dataItem.Id, area);
            }
        });
        $("a.delete", e.row).on("click", function () {
            var area = "";
            var container = $(this).closest("div.wrapper");
            var serviceId = container.find("[name=ServiceId]").val();
            if (serviceId) {
                Patient.Authorization.Delete(container.find("[name=PatientId]").val(), dataItem.Id, area)
            }
        });
        U.DataItemToolTip(e);
    },
    Delete: function (patientId, id, area) {
        U.Delete("Authorization", area + "/Authorization/Delete", { Id: id, patientId: patientId }, function (result) { Patient.Authorization.Refresh(result.PatientId, result.ServiceId); });
    },
    Refresh: function (patientId, service) {
        var window = $("#" + Patient.Authorization.WindowId);
        var authorizationGrid = $('#List_Authorizations', window).data('tGrid');
        if (authorizationGrid != null) {
            var gridCurrentService = $("[name=ServiceId]", window).val();
            if ($("[name=PatientId]", window).val() == patientId && service == gridCurrentService) {
                authorizationGrid.rebind({ patientId: patientId });
            }
        }
    },
    ServiceDropDownChanged: function (service, sourceWindow, isPatientChange) {
        if (service.target != undefined) service = $(service.target);
        var serviceText = Acore.AppService(service.val());
        var locationElement = $("[name=AgencyLocationId]", sourceWindow);
        if (!isPatientChange) locationElement.trigger("ProcessingStart");
        U.PostTrackedUrl(window[serviceText].Area + "/Authorization/NewData", { patientId: $("[name=PatientId]", sourceWindow).val() },
            function (result) {
                if (isPatientChange && result.PrimaryInsurance >= 1000) $("[name=Insurance]", sourceWindow).val(result.PrimaryInsurance);
                Patient.Authorization.ShowHideLocationsBasedOnService(locationElement, service.val()).trigger("ProcessingComplete").val(result.AgencyLocationId);
            }
        );
    },
    ShowHideLocationsBasedOnService: function (locationControl, service) {
        service = +service;
        locationControl.find("option").each(function () {
            var serviceAttr = +$(this).attr("service");
            if ((service & serviceAttr) === service) $(this).show();
            else $(this).hide();
        });
        return locationControl;
    }
});