﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    Insurance: {
        New: function (input, area) { Acore.Modal({ Name: "Insurance Information", Url: area + "/PatientProfile/NewInsurance", Input: input, OnLoad: Patient.Insurance.Init, Left: "210px", Width: "900px", Height: "400px", WindowFrame: false }); },
        Init: function (r, t, x, e) {
            var parentInsurance = $("[name=ParentInsuranceId]", e);
            var type = $("[name=Type]", e);
            var currentSelectedInsurance = $("[name=InsuranceId]", e)
            Patient.Insurance.InitHelper(parentInsurance, type, currentSelectedInsurance);
            type.on("change", function () { Patient.Insurance.InitHelper(parentInsurance, $(this), currentSelectedInsurance); });
            currentSelectedInsurance.on("change", function () { Patient.Insurance.InitHelper(parentInsurance, type, $(this)); });

            var onDateRange = function (element, elementContent) {
                if (element.is(':checked')) {
                    $("[name=ActualStartDate]", elementContent).addClass("required").closest("div.row").show();
                    $("[name=IsEndDateSet]", elementContent).closest("div.row").show();
                    var isCurrentlyActive = $("[name=IsEndDateSet]", elementContent);
                    if (isCurrentlyActive.is(':checked')) {
                        $("[name=ActualEndDate]", elementContent).addClass("required").closest("div.row").show();
                    }
                    else {
                        $("[name=ActualEndDate]", elementContent).removeClass("required").closest("div.row").hide();
                    }
                }
                else {
                    $("[name=ActualStartDate]", elementContent).addClass("required").closest("div.row").hide();
                    $("[name=IsEndDateSet]", elementContent).closest("div.row").hide();
                    $("[name=ActualEndDate]", elementContent).removeClass("required").closest("div.row").hide();
                }
            }

            var isApplicableRange = $("[name=IsApplicableRange]", e);
            onDateRange(isApplicableRange, e);
            isApplicableRange.on("change", function () { onDateRange($(this), e); });
            var onEndDateApplicable = function (element, elementContent) {
                if ($("[name=IsApplicableRange]", elementContent).is(':checked') && $(element).is(':checked')) {
                    $("[name=ActualEndDate]", e).addClass("required").closest("div.row").show();
                }
                else {
                    $("[name=ActualEndDate]", e).removeClass("required").closest("div.row").hide();
                }
            }
            var isCurrentlyActive = $("[name=IsEndDateSet]", e);
            onEndDateApplicable(isCurrentlyActive, e);
            isCurrentlyActive.on("change", function () { onEndDateApplicable($(this), e); });
            e.find("form.mainform").Validate({ Success: function () { Patient.Insurance.RebindGrid(); } });
        },
        Delete: function (input, area) { U.Delete("Payor Information", area + "/PatientProfile/DeleteInsurance", input, Patient.Insurance.RebindGrid); },
        Edit: function (input, area) { Acore.Modal({ Name: "Insurance Information", Url: area + "/PatientProfile/EditInsurance", Input: input, OnLoad: Patient.Insurance.Init, Left: "210px", Width: "900px", Height: "400px", WindowFrame: false }); },
        RebindGrid: function () {
            //.find(".patientinsurances");
            var grid = $("#EditPatient_PatientInsuranceGrid").data('tGrid');
            if (grid != null) { grid.rebind(); }
        },

        InitHelper: function (parentInsurance, type, currentSelectedInsurance) {
            var typeValue = type.val();
            if (+typeValue != 0 && +typeValue != 1) {
                var options = currentSelectedInsurance.find(" > option , > optgroup").clone();
                var selectValue = currentSelectedInsurance.val();
                if (+selectValue > 0) {
                    parentInsurance.empty().append(options).closest("div.row").show();
                    $(parentInsurance).find("option[value='" + selectValue + "']").remove();
                }
                else {
                    parentInsurance.empty().append(options).closest("div.row").show();
                }
            }
            else {
                parentInsurance.empty().closest("div.row").hide();
            }
        },
        LoadTaskPayors: function (input, area, control) {
            var selectedValue = control.val() | "0";
            control.trigger("ProcessingStart");
            U.PostUrl(area + "/PatientProfile/TaskPayors", input, function (data) {
                control.empty();
                control.append('<option value="0" InsuranceId="0">-- Select Payor --</option>');
                $.each(data, function (index, itemData) {
                    control.append('<option value="' + itemData.Id + '" InsuranceId="' + itemData.InsuranceId + '">' + itemData.Name + '(' + itemData.TypeName + ')</option>');
                });
                control.val(selectedValue).trigger("ProcessingComplete");
            }, function () { control.trigger("ProcessingComplete"); });
        },
        Payor: {
            Prefix: "PrivateDuty/PatientProfile",
            InitShared: function (r, t, x, e) {
                U.ShowIfSelectEquals($("select.relationship", e), "G8", $("input.otherrelationship", e));
                e.find("form").Validate({ Success: Patient.Insurance.Payor.RebindGrid });
            },
            RebindGrid: function () { U.RebindTGrid($("#EditPatient_BillingInformationGrid")); },
            New: function (patientId) { Acore.Modal({ Name: "Payor Information", Url: Patient.Insurance.Payor.Prefix + "/NewPayor", Input: { PatientId: patientId }, OnLoad: Patient.Insurance.Payor.InitShared, Left: "210px", Width: "900px", Height: "400px", WindowFrame: false }); },
            Edit: function (patientId, Id) { Acore.Modal({ Name: "Payor Information", Url: Patient.Insurance.Payor.Prefix + "/EditPayor", Input: { PatientId: patientId, Id: Id }, OnLoad: Patient.Insurance.Payor.InitShared, Left: "210px", Width: "900px", Height: "400px", WindowFrame: false }); },
            Delete: function (patientId, Id) { U.Delete("Payor Information", Patient.Insurance.Payor.Prefix + "/DeletePayor", { PatientId: patientId, Id: Id }, Patient.Insurance.Payor.RebindGrid); }
        }
    }
});