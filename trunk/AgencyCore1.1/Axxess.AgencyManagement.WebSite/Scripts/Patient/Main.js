﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    Document: {
        Init: function(r, t, x, e) { U.InitValidation($("form", e), Patient.Document.RebindGrid); },
        Load: function(patientId) { Acore.Open("patientdocuments", { patientId: patientId }); },
        New: function(patientId) {
            Acore.Modal({
                Name: "New Document",
                Url: "Patient/NewDocument",
                Input: { patientId: patientId },
                OnLoad: Patient.Document.Init,
                Width: 500
            });
        },
        Edit: function(patientId, documentId) {
            Acore.Modal({
                Name: "Edit Document",
                Url: "Patient/EditDocument",
                Input: { patientId: patientId, documentId: documentId },
                OnLoad: Patient.Document.Init,
                Width: 500
            });
        },
        Delete: function(patientId, documentId) { U.Delete("Document", "Patient/Document/Delete", { patientId: patientId, documentId: documentId }, Patient.Document.RebindGrid); },
        RebindGrid: function() { U.RebindTGrid($("#List_PatientDocuments")); }
    },
    UserAccess: {
        OnRowBound: function(e) {
            var row = e.row;
            if ($("input", row.innerHTML).attr("value").indexOf("00000000-0000-0000-0000-000000000000") === -1) {
                $("input", row).attr("checked", "checked");
            }
        },
        AddUserAccess: function(data, e) {
            $.ajax({
                url: "Patient/AddUserAccess",
                type: "POST",
                data: data,
                dataType: "json",
                success: function(dataResult) {
                    var patientUserId = dataResult.PatientId;
                    $(e).attr("value", patientUserId);
                }
            });
        },
        RemoveUserAccess: function(data) {
            $.ajax({
                url: "Patient/RemoveUserAccess",
                type: "POST",
                data: data,
                dataType: "json",
                success: function(dataResult) {
                }
            });
        },
        UserClick: function(e) {
            if ($(e).is(":checked")) {
                var patientId = $("#UserAccessPatientId")[0].defaultValue;
                var userId = $(e).parent().parent().children(":first")[0].innerHTML;
                var data = {
                    patientId: patientId,
                    userId: userId
                };
                Patient.UserAccess.AddUserAccess(data, e);
            } else {
                var patientUserId = $(e).attr("value");
                var data = {
                    patientUserId: patientUserId
                };
                Patient.UserAccess.RemoveUserAccess(data);
            }
        },
        List: function(patientId) { Acore.Open("patientuseraccess", { patientId: patientId }); }
    },
    PatientAccess: {
        UserClick: function(e) {
            if ($(e).is(":checked")) {
                var userId = $("#PatientAccessUserId")[0].defaultValue
                var patientId = $(e).parent().parent().children(":first")[0].innerHTML;
                var data = {
                    patientId: patientId,
                    userId: userId
                };
                Patient.UserAccess.AddUserAccess(data, e);
            } else {
                var patientUserId = $(e).attr("value");
                var data = {
                    patientUserId: patientUserId
                };
                Patient.UserAccess.RemoveUserAccess(data);
            }
        }
    }
});
