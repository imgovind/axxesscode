(function ($) {
    $.extend($.fn, {
        Buttons: function (Buttons) {
            return this.each(function () {
                var $buttons = $(this);
                $buttons.addClass("buttons").append(
                    $("<ul/>")
                );
                if (Buttons) {
                    $.each(Buttons, function () {
                        var input = {};
                        if (this.Input != undefined) input = this.Input;
                        $("ul", $buttons).append(
                            $("<li/>").append(
                                $("<a/>", { text: this.Text, "class": this.Class }).on("click", input, this.Click)
                            )
                        );
                    });
                }
            });
        },
        BloodPressure: function () {
            return this.each(function () {
                $(this).keypress(function (event) {
                    if (event.keyCode != "9" && event.which != "9") {
                        if (!U.IsNumber(event.which) && !U.IsSeparator(event.which) && !U.IsSystemKey(event.keyCode)) event.preventDefault();
                        else if (U.IsSeparator(event.which) && /[\\\/\,\.]/.test($(this).val())) event.preventDefault();
                    }
                }).blur(function () {
                    var bp = $(this).val(), sbp = 0, dbp = 0;
                    if (bp.indexOf("/") == 1) sbp = parseInt(bp.split("/")[0]), dbp = parseInt(bp.split("/")[1]);
                    else {
                        bp = bp.replace(/[^0-9]*/g, "");
                        if (bp.length > 3 && bp.length < 7) sbp = parseInt(bp.substring(0, bp.length > 4 ? 3 : 2)), dbp = parseInt(bp.substring(bp.length > 4 ? 3 : 2));
                    }
                    if (sbp > 50 && sbp < 300 && dbp > 20 && dbp < 200) $(this).val(String(sbp) + "/" + String(dbp)).removeClass("error");
                    else if ($(this).val() == "" && !$(this).hasClass("required")) $(this).removeClass("error");
                    else $(this).addClass("error");
                })
            })
        },
        Checkgroup: function () {
            return this.each(function () {
                $(".option .wrapper > input[type=checkbox],.option .wrapper > input[type=radio]", this).each(function () {
                    if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
                    else $(this).closest(".option").removeClass("selected");
                    $(this).on("change", function (e) {
                        $(this).closest(".row").find(".option").each(function () {
                            if ($(".wrapper > input[type=checkbox],.wrapper > input[type=radio]", this).prop("checked")) $(this).addClass("selected");
                            else {
                                $(this).removeClass("selected");
                                U.ToggleOff($("> .more, .wrapper > .more", this), false, true);
                            }
                        });
                    });
                });
                $(".more", this).each(function () {
                    U.ShowIfChecked($(this).closest(".option").find("[type=checkbox],[type=radio]"), $(this));
                });
            });
        },
        Currency: function (Options) {
            return this.each(function () {
                $(this).Decimal({ ZeroFill: true });
                $(this).before("<span class='currency-symbol'>$</span>");
            })
        },
        Decimal: function (Options) {
            return this.each(function () {
                if (Options == undefined) Options = {};
                if (Options.DecimalLength == undefined) Options.DecimalLength = 2;
                if (Options.ZeroFill == undefined) Options.ZeroFill = false;
                if (Options.Dollar) Options.ZeroFill = true;
                $(this).keypress(function (event) {
                    if (event.keyCode != "9" && event.which != "9") {
                        if (U.IsDollarSign(event.which)) {
                            if (Options.Dollar != true || /\$/.test($(this).val())) event.preventDefault();
                        }
                        if (!U.IsNumber(event.which) && !U.IsCommaOrPeriod(event.which) && !U.IsSystemKey(event.keyCode)) event.preventDefault();
                        else if (U.IsCommaOrPeriod(event.which) && /[\,\.]/.test($(this).val())) event.preventDefault();
                        //else if (Options.Dollar != true || (U.IsDollarSign(event.which) && /\$/.test($(this).val()))) event.preventDefault();
                    }
                }).keyup(function () {
                    if (/\$/g.test($(this).val()) && !/^$/g.test($(this).val())) $(this).val($(this).val().replace(/\$/g, ""));
                }).blur(function () {
                    var Value = $(this).val();
                    Value = Value.replace(/[^0-9\.]*/g, "");
                    var ValSplit = Value.split(/\./);
                    if (ValSplit.length > 1) {
                        Value = ValSplit[0] + ".";
                        for (i = 1; i < ValSplit.length; i++) Value = Value + ValSplit[i];
                        if (Options.ZeroFill) for (i = 0; i < Options.DecimalLength; i++) Value = Value + "0";
                        if (Value.length > Value.indexOf(".") + Options.DecimalLength) Value = Value.substring(0, Value.indexOf(".") + Options.DecimalLength + 1);
                    } else if (Options.ZeroFill && Value.length > 0) Value = Value + ".00";
                    $(this).val((Options.Dollar ? "$" : "") + Value);
                });
            })
        },
        DeselectableRadio: function () {
            return this.each(function () {
                var radio = $(this);
                radio.add("label[for=" + $(this).attr("id") + "]").mouseup(function () {
                    if (radio.prop("checked")) radio.data("deselect", true);
                    else radio.data("deselect", false);
                });
                radio.on("click", function () {
                    if (radio.data("deselect")) radio.prop("checked", false).trigger("change");
                });
            });
        },
        GridSearch: function () {
            return this.each(function () {
                $(this).append(
                    $("<label/>", { text: "Filter by Text" }).on("click", function () {
                        $(this).next().focus()
                    })).append(
                    $("<input/>", { type: "text" }).attr("placeholder", "Start Typing").keyup(function () {
                        var SearchString = $(this).val(),
                            GridContent = $(this).closest(".wrapper").find(".t-grid-content:eq(0)");
                        if (SearchString) $("tr", GridContent).each(function () {
                            $(this).removeClass("match").hide();
                            $("td", this).each(function () {
                                if ($(this).text().toLowerCase().indexOf(SearchString.toLowerCase()) > -1) $(this).closest("tr").addClass("match");
                            })
                        });
                        else $("tr", GridContent).addClass("match");
                        $(".match", GridContent).removeClass("t-alt").show().filter(":even").addClass("t-alt");
                    })
                )
            })
        },
        IcdCode: function (Options) {
            return this.each(function () {
                if (Options == undefined) Options = {};
                if (Options.VECodes == undefined) Options.VECodes = true;
                $(this).keypress(function (event) {
                    if (!U.IsNumber(event.which) && !U.IsCommaOrPeriod(event.which) && !U.IsSystemKey(event.keyCode) && !U.IsIcdAlpha(event.which)) event.preventDefault();
                    else if (U.IsCommaOrPeriod(event.which) && /[\,\.]/.test($(this).val())) event.preventDefault();
                    else if (!Options.VECodes || (U.IsIcdAlpha(event.which) && /[evEV]/.test($(this).val()))) event.preventDefault();
                    else if (!U.IsSystemKey(event.keyCode) && $(this).val().length > 6) event.preventDefault();
                    else if (!U.IsSystemKey(event.keyCode) && /^[vV0-9]/.test($(this).val()) && $(this).val().length > 5) event.preventDefault();
                }).keyup(function () {
                    var code = $(this).val().replace(/\,/g, ".").replace(/[^0-9\.]/g, "").replace(/^\./, ""),
                        vcode = $(this).val().substring(0, 1).toUpperCase() == "V",
                        ecode = $(this).val().substring(0, 1).toUpperCase() == "E";
                    if (code.indexOf(".") > 0) for (var i = 0; i < (vcode ? 2 : 3) - code.split(".")[0].length; i++) code = "0" + code;
                    $(this).val((vcode ? "V" : "") + (ecode ? "E" : "") + code);
                })
            })
        },
        MultiRow: function (Options) {
            return this.each(function () {
                var $row = $(this);
                $row.addClass("row").append(
                    $("<div/>", { "class": "main-label", "text": Options.Label })
                );
                $.each(Options.Subrows, function () {
                    $row.append(
                        $("<div/>").Row($.extend({}, { Prefix: Options.Prefix }, this)).removeClass("row").addClass("subrow")
                    )
                })
            })
        },
        Row: function (Options) {
            return this.each(function () {
                var $row = $(this),
                    Id = Options.Prefix + "_" + Options.Name;
                $row.addClass("row" + (Options.Class ? " " + Options.Class : "")).append(
                    $("<label/>", { "for": Id, "text": Options.Label })).append(
                    $("<div/>", { "class": "answer" })
                );
                if (Options.Type == "select") $(".answer", $row).append(
                    $("<select>").Select({
                        Attr: {
                            "id": Id,
                            "name": Id
                        },
                        Options: Options.Options,
                        Value: Options.Value
                    })
                );
                else if (Options.Type == "file") $(".answer", $row).append(
                $("<input/>").Uploader({
                    Name: Id,
                    Size: Options.Size
                })
                );
                else {
                    $(".answer", $row).append(
                        $("<input/>", {
                            "id": Id,
                            "name": Id,
                            "type": "text",
                            "value": Options.Value
                        })
                    );
                    if (Options.Type == "date") $(".answer input", $row).DatePicker();
                    else if (Options.Type == "centimeters") $(".answer input", $row).addClass("centimeter").after("cm");
                    else if (Options.Type == "percent") $(".answer input", $row).addClass("percent").after("%");
                }
            })
        },
        MultiSelect: function (Options) {
            return this.each(function () {
                if (Options != null && typeof Options.OnSelect == "function") var Callback = Options.OnSelect;
                $(this).multiselect({
                    noneSelectedText: Options != null && Options.DefaultText != undefined ? Options.DefaultText : "",
                    selectedList: 4,
                    minWidth: Options != null && Options.Width != undefined ? Options.Width : "250px",
                    classes: Options != null && Options.Classes != undefined ? Options.Classes : "",
                    click: Callback
                });
                $(".multiSelect").attr("href", "").append(
                    $("<select/>").focus(function () { $(this).parent().focus() })
                )
            })
        },
        PasswordCheck: function (Options) {
            return this.each(function () {
                var e = $(this),
                    validRegExp = new RegExp(".{6}$"),
                    strongRegExp = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", "g"),
                    invalidMessage = Options.type + " need to be at least 6 characters",
                    weakMessage = "Weak, but Valid" + Options.type;
                e.hide();
                $(Options.password).add(Options.confirm).on("change", function () {
                    var containerClass = "",
                        iconClass = "",
                        message = "";
                    if (Options.password.val() && Options.confirm.val()) {
                        if (Options.password.val() == Options.confirm.val() && validRegExp.test(Options.password.val())) {
                            if (strongRegExp.test(Options.password.val())) containerClass = "match", iconClass = "success", message = Options.type + " Match";
                            else containerClass = "weak", iconClass = "exclamation", message = Options.type + " is Weak";
                        } else {
                            iconClass = "error";
                            if (!validRegExp.test(Options.password.val())) message = invalidMessage;
                            else if (Options.password.val() != Options.confirm.val()) message = Options.type + " Do Not Match";
                        }
                        e.empty().show().removeClass("match weak").addClass(containerClass).append(
                            $("<span/>").addClass("img icon32 " + iconClass)).append(
                            $("<span/>").addClass("msg").text(message));
                    } else e.hide();
                })
            })
        },
        PhoneAutoTab: function () {
            return this.each(function () {
                var $npa = $(this),
                    $nxx = $(this).closest(".row").find(".phone-short:eq(1)"),
                    $num = $(this).closest(".row").find(".phone-long");
                $npa.autotab({ target: $nxx, format: "numeric" });
                $nxx.autotab({ target: $num, previous: $npa, format: "numeric" });
                $num.autotab({ previous: $nxx, format: "numeric" });
            })
        },
        Select: function (Options) {
            return this.each(function () {
                var $select = $(this);
                if (Options.Attr) $select.attr(Options.Attr);
                if (Options.Css) $select.css(Options.Css);
                if (Options.BlankEntry == null || Options.BlankEntry) $select.append(
                    $("<option/>")
                );
                $.each(Options.Options, function () {
                    $select.append(
                        $("<option/>").val(this.Value).text(this.Text ? this.Text : this.Value).attr("selected", Options.Value === this.Value)
                    );
                })
            })
        },
        TemplateSelect: function () {
            return this.each(function () {
                var $select = $(this),
                    $textarea = $select.next("textarea");
                if ($textarea.length == 0) $textarea = $($select.attr("template"));
                if ($textarea.length == 0) $textarea = $select.parent().next("textarea");
                $select.on("change", function () {
                    if ($select.val() == "empty") {
                        $textarea.val("");
                        $select.prop("selectedIndex", 0);
                    } else if ($select.val() == "spacer") $select.prop("selectedIndex", 0);
                    else if ($select.val().length > 0) {
                        var currentText = $textarea.val();
                        $textarea.empty().addClass("loading");
                        U.PostUrl("Template/Get", { id: $select.val() }, function (template) {
                            if (template != undefined) {
                                
                                $textarea.val((currentText.trim().length == 0 ? "" : currentText + "\n") + template.Text).focus().removeClass("loading");
                            }
                        })
                    }
                }).prev("label").attr("for", $(this).attr("id"))
            })
        },
        Uploader: function (Options) {
            return this.each(function () {
                $(this).attr({
                    type: "file",
                    name: Options.Name,
                    id: Options.Id ? Options.Id : Options.Name,
                    size: Options.Size ? String(Options.Size) : "15"
                }).addClass(Options.Class).on("change", function () {
                    $(this).next(".dummy-file").find("input").val($(this).val())
                }).after(
                    $("<div/>", { "class": "dummy-file" }).append(
                        $("<input/>")).append(
                        $("<div/>").Buttons([
                            { Text: "Browse", Click: function () { } }
                        ])
                    )
                )
            })
        },
        ContainerToggle: function () {
            return this.each(function () {
                var checkbox = $(this),
                    container = checkbox.closest("fieldset").find(".collapsible-container");
                U.HideIfChecked(checkbox, container);
            });
        }
    })
})(jQuery);