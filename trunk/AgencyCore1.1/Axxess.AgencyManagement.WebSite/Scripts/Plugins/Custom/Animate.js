(function($) {
    $.extend($.fn, {
        Animate: function(styleChanges, callback) {
            return this.each(function() {
                if (typeof styleChanges == "object") {
                    if (Acore.Animated) $(this).stop().animate(styleChanges, Acore.AnimationSpeed, callback);
                    else {
                        $(this).css(styleChanges);
                        if (typeof callback == "function") callback.apply(this);
                    }
                } else if (typeof styleChanges == "string") {
                    if (Acore.Animated && typeof $.fn[styleChanges] == "function") {
                        //$(this).stop(); -- Had to remove for rapid click bug
                        $(this)[styleChanges](Acore.AnimationSpeed, callback);
                    } else {
                        if (styleChanges == "slideUp") $(this).hide();
                        if (styleChanges == "slideDown") $(this).show();
                        if (typeof callback == "function") callback.apply(this);
                    }
                }
            });
        }
    });
})(jQuery);