﻿var RequestTracker = {
    Requests: {},
    GridRequests: {},
    Abort: function(url) {
        if (RequestTracker.Requests[url] != undefined) {
            RequestTracker.Requests[url].abort();
            RequestTracker.Requests[url] = undefined;
        }
    },
    Track: function(request, url) {
        RequestTracker.Abort(url);
        request.complete = RequestTracker.CreateRequestCallbackArray(request.complete, url);
        request.error = RequestTracker.CreateRequestCallbackArray(request.error, url);
        RequestTracker.Requests[url] = request;
    },
    GridAbort: function(id, url) {
        if (RequestTracker.GridRequests[id] != undefined && RequestTracker.GridRequests[id][url] != undefined) {
            RequestTracker.GridRequests[id][url].abort();
            RequestTracker.GridRequests[id][url] = undefined;
        }
    },
    GridTrack: function(id, request, url) {
        RequestTracker.GridAbort(id, url);
        var values = RequestTracker.GridRequests[id];
        request.complete = RequestTracker.CreateRequestCallbackArray(request.complete, url, id);
        request.error = RequestTracker.CreateRequestCallbackArray(request.error, url, id);
        if (!values) values = {};
        values[url] = request;
        RequestTracker.GridRequests[id] = values;
    },
    AbortAllGridRequests: function(id) {
        if (RequestTracker.GridRequests[id]) {
            for (prop in RequestTracker.GridRequests[id]) {
                if (prop) {
                    RequestTracker.GridRequests[id][prop].abort();
                    RequestTracker.GridRequests[id][prop] = undefined;
                }
            }
            RequestTracker.GridRequests[id] = undefined;
        }
    },
    CreateRequestCallbackArray: function(requestCallback, url, id) {
        var callback = function() { RequestTracker.Requests[url] = undefined; };
        if (id) callback = function() { RequestTracker.GridRequests[id][url] = undefined; };
        return [requestCallback, callback];
    }
};