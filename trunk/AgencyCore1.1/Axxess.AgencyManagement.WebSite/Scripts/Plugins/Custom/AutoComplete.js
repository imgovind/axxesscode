(function($) {
    $.extend($.fn, {
        // Plugin for AJAX-based autocomplete inputs
        AjaxAutocomplete: function(Options) {
            return this.each(function() {
                //  Options:
                //      ReverseLookup       URL to POST to for looking up literal value from hidden value
                //      MagnifyingGlass     Bool to switch on and off the Magnifying Glass icon
                //      SourceUrl           URL to obtain source JSON list
                //      ExtraParameters     JSON for additional variables to be sent to the server
                //      Width               Width of drop-down list in pixels, if undefined, will be set to match input field
                //      FilterSearch(Term)  Function used to parse search term prior to the AJAX call
                //      Format(Json)        Function to parse the JSON data and return a string formatted for the drop-down menu
                //      Select(Json, Input) Function called after menu selection has been made

                // If passing Options object and control is not already enabled with this plugin
                if (typeof Options === "object" && !$(this).hasClass("ajax-autocomplete")) {
                    // Create alias for control, remember existing maxlength settings, and create numeric iterator for concurrent uses of this plugin
                    var Input = $(this), MaxLength = $(this).attr("maxlength"),
                        Nth = Acore.AutocompleteId++,
                        Loaded = false;
                    // On focus of input box, set maxlength to seemingly unreachable thresholds, and clear the input of "Start Typing"
                    Input.addClass("ajax-autocomplete").focus(function() {
                        if (!Loaded) {
                            // Enable parent plugin
                            Input.autocomplete(Options);
                            // Set identifier on related jQuery UI autocomplete drop-down menu
                            $(".ui-autocomplete:last").attr("id", "autocomplete" + Nth);
                        }
                        $(this).attr("maxlength", 9999).AjaxAutocomplete("clear");
                    });
                    if (!Input.attr("placeholder")) Input.attr("placeholder", "Start Typing");
                    // If control works off a reverse lookup
                    if (Options.ReverseLookup != null) {
                        // Add hidden saving input after display input
                        Input.after(
					        $("<input/>", { "type": "hidden", "name": Input.attr("name") })
    				    );
                        // Rename display input and set blur function to ensure an option is selected
                        Input.attr("name", Input.attr("name") + "_text").blur(function() {
                            var Input = $(this),
                                Guid = Input.siblings("input[type=hidden]").val();
                            if (Input.val().length > 0 && U.IsGuid(Guid))
                                U.PostTrackedUrl(Options.ReverseLookup, { "id": Guid }, function(data) {
                                    // If reverse lookup is successful, set display input to reverse lookup response
                                    Input.val(data);
                                }, function() {
                                    // If reverse lookup failed, reset the inputs
                                    Input.AjaxAutocomplete("reset").siblings("input[type=hidden]").val("");
                                });
                            // If incomplete selection or field has been cleared, reset the inputs
                            else Input.AjaxAutocomplete("reset").siblings("input[type=hidden]").val("");
                        });
                        // If input has an existing, GUID value, but does not have a prefilled value then run reverse lookup to populate data
                        var prefilledValue = Input.attr("prefilled"),
                            setHiddenValueFunc = function(inputValue) {
                                Input.siblings("input[type=hidden]").val(Input.val()).prev(".ajax-autocomplete").val(inputValue);
                            };
                        if (U.IsGuid(Input.val()) && Input.val() != U.GuidEmpty) {
                            if (prefilledValue) setHiddenValueFunc(prefilledValue);
                            else U.PostTrackedUrl(Options.ReverseLookup, { "id": Input.val() }, function(data) {
                                // If reverse lookup is successful, set hidden input to existing GUID value and set visible input to reverse lookup response
                                setHiddenValueFunc(data);
                            }, function() {
                                // If reverse lookup failed, reset the inputs
                                Input.AjaxAutocomplete("reset");
                            });
                        } else Input.AjaxAutocomplete("reset");
                        // Else if control doesn't use a reverse lookup
                    } else {
                        // Set blur function, if field is cleared, set input to reset settings
                        Input.blur(function() {
                            if (Input.val().length == 0) Input.AjaxAutocomplete("reset");
                        });
                        // If field is currently empty, set input to reset settings
                        if (Input.val().length == 0) Input.AjaxAutocomplete("reset");
                    }
                    // If the magnifying glass is enabled, add in the icon and set the click to empty search
                    if (Options.MagnifyingGlass) {
                        Input.next().after(
                            $("<span/>", { "class": "img icon16 view" }).on("click", function() {
                                $(this).siblings(".ajax-autocomplete").focus().autocomplete("search", "");
                            })
                        );
                        Options.minLength = 0;
                    }
                    // Set parent plugin's source function(event when control is initialized and requests data for menu)
                    Options.source = function(request, response) {
                        // If filter search function exists, filter search term
                        if (typeof Options.FilterSearch == "function") request.term = Options.FilterSearch(request.term);
                        // Merge extra parameters with the current request object
                        if (typeof Options.ExtraParameters == "object") request = $.extend({}, request, Options.ExtraParameters);
                        // Make the request for menu options
                        U.PostTrackedUrl(Options.SourceUrl, request, function(data) {
                            // Build an array of server responses and pass it to the parent plugin's response function
                            var Text = new Array();
                            for (var i = 0; i < data.length; i++) Text[i] = JSON.stringify(data[i]);
                            response(Text);
                            Input.removeClass("ui-autocomplete-loading");
                        }, function() {
                            Input.removeClass("ui-autocomplete-loading");
                        })
                    };
                    // If width is not set, default it to input width
                    if (Options.Width == null) Options.Width = Input.width();
                    // Set parent plugin's open function(event when drop-down is first displayed)
                    Options.open = function(event, ui) {
                        // Set menu width
                        $("#autocomplete" + Nth).css("width", (Options.Width > 0 ? Options.Width : 200) + "px");
                        // Cycle through each menu item and format it per Format function
                        $(".ui-menu-item a", "#autocomplete" + Nth).each(function() {
                            var Json = JSON.parse($(this).text());
                            $(this).html(Options.Format(Json));
                        });
                    };
                    // Set parent plugin's select function(event when an option is selected)
                    Options.select = function(event, ui) {
                        // If validator has marked it as invalid, remove the error
                        if (Input.hasClass("error")) Input.removeClass("error").next("label.error").remove();
                        // Create JSON object and parse it through the Select function
                        var Json = JSON.parse(ui.item.value);
                        if (Json) Options.Select(Json, Input);
                        // If previously had maxlength set, restore that value
                        if (MaxLength > 0) Input.attr("maxlength", MaxLength);
                        // Return false, to not set input value back to JSON
                        return false;
                    };
                    // Set parent plugin's focus function(event when option is highlighted in the drop-down)
                    Options.focus = function(event, ui) {
                        // Set input to menu item's text
                        $(this).val(JSON.parse(ui.item.value).Text);
                        return false;
                    };
                    // If issuing a command to an existing plugin
                } else if (typeof Options == "string" && $(this).hasClass("ajax-autocomplete")) {
                    // If command is reset, then set default settings
                    if (Options == "reset" && $(this).width() > 100) $(this).val("");
                }
            })
        },
        // Plugin for standard (non-AJAX) autocomplete inputs
        Autocomplete: function(Options) {
            return this.each(function() {
                // If width is not set, default to control's width
                if (Options.Width == null) Options.Width = $(this).width();
                // Set parent plugin's open function(event when drop-down is first displayed)
                Options.open = function(event, ui) {
                    // Set menu width
                    $(ui)
                    $(".ui-autocomplete").css("width", Options.Width + "px");
                }
                // Enable parent plugin
                $(this).autocomplete(Options);
            })
        },
        // Plugin for Diagnosis, Procedure, and ICD-9 inputs
        IcdInput: function() {
            return this.each(function() {
                // Set type and url based upon control's class settings
                var type, url;
                if ($(this).hasClass("diagnosis")) type = "DIAGNOSIS", url = "LookUp/DiagnosisCode";
                else if ($(this).hasClass("icd")) type = "ICD", url = "LookUp/DiagnosisCode";
                else if ($(this).hasClass("procedureDiagnosis")) type = "PROCEDURE", url = "LookUp/ProcedureCode";
                else if ($(this).hasClass("procedureICD")) type = "PROCEDUREICD", url = "LookUp/ProcedureCode";
                else if ($(this).hasClass("diagnosisM1024")) type = "DIAGNOSIS", url = "LookUp/DiagnosisCodeNoVE";
                else if ($(this).hasClass("ICDM1024")) type = "icd", url = "LookUp/DiagnosisCodeNoVE";
                // If both variables are set, procede in enabling the plugin
                if (type && url) {
                    if (type == "PROCEDUREICD" || type == "icd") $(this).Decimal();
                    if (type == "ICD") $(this).IcdCode();
                    $(this).AjaxAutocomplete({
                        ExtraParameters: { "type": type },
                        minLength: 2,
                        SourceUrl: url,
                        Width: 400,
                        FilterSearch: function(Term) {
                            if (type == "ICD" || type == "PROCEDUREICD" || type == "icd") return Term.replace(/\./g, "");
                            else return Term;
                        },
                        Format: function(json) {
                            // Format menu to "ICD9 - Description"
                            return json.FormatCode + " &#8211; " + json.ShortDescription;
                        },
                        Select: function(json, input) {
                            if (url == "LookUp/DiagnosisCode") {
                                if (type == "DIAGNOSIS") input.val(json.ShortDescription).closest("li,tr").find(".icd").val(json.FormatCode);
                                else input.val(json.FormatCode).closest("li,tr").find(".diagnosis").val(json.ShortDescription);
                                if (!input.attr("id").match(/^Edit_485.*$/)) input.closest("li,tr").find(".icd").parent().find(".book").remove().end().append(
                                    $("<a/>", {
                                        "class": "img icon16 book",
                                        "href": "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + json.FormatCode + "&informationRecipient.languageCode.c=en",
                                        "target": "_blank"
                                    })
                                )
                            }
                            else if (type == "PROCEDURE") input.val(json.ShortDescription).parent().find(".procedureICD").val(json.FormatCode);
                            else if (type == "PROCEDUREICD") input.val(json.FormatCode).parent().find(".procedureDiagnosis").val(json.ShortDescription);
                            else if (type == "DIAGNOSIS") input.val(json.ShortDescription).parent().find(".ICDM1024").val(json.FormatCode);
                            else if (type == "icd") input.val(json.FormatCode).parent().find(".diagnosisM1024").val(json.ShortDescription);
                        }
                    })
                }
            })
        },
        // Plugin for Patient Selector
        PatientInput: function() {
            return this.each(function() {
                $(this).AjaxAutocomplete({
                    MagnifyingGlass: true,
                    Width: 183,
                    ReverseLookup: "Lookup/PatientName",
                    SourceUrl: "LookUp/Patients",
                    ExtraParameters: { Service: $(this).attr("service") },
                    Format: function(json) {
                        return json.Text;
                    },
                    Select: function(json, input) {
                        input.val(json.Text).siblings("input[type=hidden]").val(json.Value);
                        input.trigger("change");
                    }
                })
            })
        },
        // Plugin for Physician Selector
        PhysicianInput: function() {
            return this.each(function() {
                $(this).AjaxAutocomplete({
                    MagnifyingGlass: true,
                    Width: 183,
                    ReverseLookup: "Lookup/PhysicianName",
                    SourceUrl: "LookUp/Physicians",
                    Format: function(json) {
                        return json.Text;
                    },
                    Select: function(json, input) {
                        input.val(json.Text).siblings("input[type=hidden]").val(json.Value);
                        input.trigger("change");
                    }
                })
            })
        },
        ZipInput: function(formObj) {
            var fieldset = $(this).closest("fieldset");
            $(this).on("change", function(e) {
                var valid = formObj.data("validator").element(".zip");
                if (valid) {
                    var city = $(".address-city", fieldset);
                    var state = $(".address-state", fieldset);
                    var fullZipCode = e.currentTarget.value;
                    var zipCodeFive = fullZipCode.substring(0, 5);
                    var zipCodeFour = fullZipCode.length == 9 ? fullZipCode.substring(5, 9) : "";
                    U.PostTrackedUrl("LookUp/CityStateByZipCode/", { zipCode: zipCodeFive }, function(data) {
                        if (data.isSuccessful) Acore.Confirm({
                            Message: "The zip code found matches the city and state of: " + data.City + ", " + data.State + ". If this is correct would you like to have the values automatically set?",
                            Yes: function() {
                                city.val(data.City);
                                city.keyup();
                                state.val(data.State);
                            }
                        });
                        else U.Growl("The United States Postal Service cannot find the zip code " + fullZipCode + ". It might be an invalid zipcode.", "warning");
                    })
                }
            })
        }
    })
})(jQuery);