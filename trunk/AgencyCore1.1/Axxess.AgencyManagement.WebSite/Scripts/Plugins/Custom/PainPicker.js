(function ($) {
    var painPickerMethods = {
        build: function () {
            var select = $(this), severity = select.val();
            select.on("change", function () {
                $(this).PainPicker("change", $(this).val());
            }).after(
                $("<div/>").addClass("pain-picker").append(
                    $("<div/>").addClass("pain-slider").slider({
                        change: function (event, ui) {
                            $(this).closest(".pain-picker").prev().PainPicker("change", ui.value);
                        },
                        min: 0,
                        max: 10,
                        step: 1,
                        value: severity
                    }).append(
                        $("<span/>").addClass("no-pain").text("None")).append(
                        $("<span/>").addClass("mild-pain").text("Mild")).append(
                        $("<span/>").addClass("moderate-pain").text("Moderate")).append(
                        $("<span/>").addClass("severe-pain").text("Severe"))).append(
                    $("<table/>").append(
                        $("<tbody/>").append(
                            $("<tr/>")))));
            for (var i = 0; i < 11; i++) select.next().find("tr").append(
                $("<td/>").text(i).addClass(severity == i ? "selected" : "").on("click", function () {
                    $(this).closest(".pain-picker").prev().PainPicker("change", $(this).text());
                }));
            return this;
        },
        change: function (severity) {
            var select = $(this);
            select.val(severity);
            select.next().find("td").removeClass("selected").eq(severity).addClass("selected");
            select.next().find(".pain-slider").slider("value", severity);
            return this;
        },
        init: function (options) {
            return this.each(function () {
                var select = $(this);
                select.PainPicker("build");
            })
        }
    };
    $.fn.PainPicker = function (method) {
        if (painPickerMethods[method]) return painPickerMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" || !method) return painPickerMethods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist for the Pain Picker Plugin.");
    };
})(jQuery);