(function($) {
    $.extend($.fn, {
        CurrentDate: function() {
            return this.each(function() {
                var currentDate = new Date();
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                $(this).empty().append(
                    $("<span/>", { "class": "month", "text": months[currentDate.getMonth()] })).append(
                        $("<span/>", { "class": "day", "text": currentDate.getDate() })).append(
                            $("<span/>", { "class": "year", "text": currentDate.getFullYear() })
                        );
                var superText = "th";
                if (currentDate.getDate() % 10 == 1 && currentDate.getDate() != 11) superText = "st";
                else if (currentDate.getDate() % 10 == 2 && currentDate.getDate() != 12) superText = "nd";
                else if (currentDate.getDate() % 10 == 3 && currentDate.getDate() != 13) superText = "rd";
                $(".day", this).append(
                    $("<super/>").text(superText)
                );
            });
        }
    });
})(jQuery);