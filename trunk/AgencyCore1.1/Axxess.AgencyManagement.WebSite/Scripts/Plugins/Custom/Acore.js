﻿var Acore = {
    AlertId: "Alert",                   // Default acore id for alert window
    AgencyId: "",                       // Current Agency ID
    Animated: true,                     // Toggle animations on and off
    AnimationSpeed: 350,                // Set the speed of the animations
    ApplicationServices: 0,             // The AgencyServices value to determine which services are available
    AutocompleteId: 0,                  // Autocomplete iterator
    CascadeGap: 25,                     // Pixel gap for window cascading
    Cookies: true,                      // Toggle cookies on and off
    CssTop: 0,                          // CSS top iterator, used for window cascading
    CssLeft: 0,                         // CSS left iterator, used for window cascading
    DefaultWindow: "Dashboard",         // Window to open automatically on activate
    DefaultWinHeight: "85%",            // Default window height
    DefaultWinWidth: "85%",             // Default window width
    DialogId: "Dialog",                 // Default acore id for dialog window
    GetRemoteContent: true,             // Enable remote content loading
    IdleTime: 0,                        // System idle timer
    MaxWindows: 8,                      // Maximum allowed number of concurrent open windows
    MinWinHeight: 350,                  // Minimum height of any window (may be overloaded per window)
    MinWinWidth: 1000,                  // Minimum width of any window (may be overloaded per window)
    Mobile: false,                      // Mobile site switch
    OpenWindows: 0,                     // Open window counter
    OpenWindowsCookie: "OpenWindows",   // Name for Open Windows cookie
    PingTimer: null,                    // Timer for keep-alive pings
    ReadOnly: false,                    // Read Only Version of Acore
    ReturnReasonId: "ReturnReason",     // Default acore id for return reason
    PrintPreviewId: "PrintPreview",      // Default acore id for print preview
    Timeout: 45,                        // Auto-logout to occur at this cycle number
    TimeoutCount: 5,                    // Number of cycles to display auto-logout warning
    TimeoutInterval: (60 * 1000),       // Auto-logout interval (cycle length in milliseconds)
    TimeoutTimer: null,                 // Timer for idle timeout counting
    UserId: "",                         // Current User ID
    WindowBorder: ".25em",              // Window border
    Windows: {},                        // Registry of available windows
    AddMenu: function (Options) {
        //  Options:
        //      Name        Display name for the menu
        //      Id          DOM identifier for the menu
        //      Parent      DOM identifier for the parent menu
        //      IconClass   CSS Class for Icon
        //      IconX       Pixel position in sprite for menu icon X axis
        //      IconY       Pixel position in sprite for menu icon Y axis
        if (Options.Name != null && (Options.Parent == null || $("#" + Options.Parent).length)) {
            // Create basic menu option
            var menu =
                $("<li/>").append(
                    $("<a/>", { "class": "menu-trigger", text: Options.Name })).append(
                    $("<ul/>", { id: Options.Id == undefined ? Options.Name.replace(/[^0-9a-zA-Z]/, "") : Options.Id, "class": "menu" })
                );
            // If icon x position in sprite defined, add icon (default y is 86)
            if (Options.IconClass || (Options.IconX && Options.IconY)) {
                $("a", menu).prepend($("<span/>", { "class": "img icon22" }));
                if (Options.IconClass) $(".icon22", menu).addClass(Options.IconClass);
                if (Options.IconX && Options.IconY) $(".icon22", menu).css("background-position", "-" + Options.IconX + "px -" + Options.IconY + "px");
            }
            // Add menu into main or submenu depending upon the Parent option
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(menu);
        }
    },
    AddMenuItem: function (Options) {
        //  Options:
        //      Id      Window identifier
        //      Name    Name for menu item
        //      Href    URL for menu link
        //      Parent  DOM identifier for parent menu
        //      Order   The order of the menu item in the parent menu
        // If external link
        if (Options.Name != null && Options.Href != null) $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
            $("<li/>").append(
                $("<a/>", { href: Options.Href, target: "_blank", text: Options.Name })
            )
        );
        // If internal window load
        if (Options.Id != null && this.Windows[Options.Id] != null) $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
            $("<li/>").data("order", Options.Order).on("click", function () {
                //Acore.Open(Options.Id, Acore.Windows[Options.Id].Url, Acore.Windows[Options.Id].OnLoad, Acore.Windows[Options.Id].Input);
                Acore.OpenTest({ Id: Options.Id, Url: Acore.Windows[Options.Id].Url, OnLoad: window[Acore.Windows[Options.Id].OnLoad], Inputs: Acore.Windows[Options.Id].Input, IsPost: Acore.Windows[Options.Id].IsPost });
            }).append(
                $("<a/>", { text: this.Windows[Options.Id].MenuName })
            )
        );
    },
    AddWindow: function (Options) {
        //  Options:
        //      AlwaysOnTop     Bool switch to keep window above all other windows
        //      BackgroundLoad  Keep window hidden until specifically made visible
        //      Center          Bool switch to display window centered as opposed to cascading
        //      Cookies         Whether or not to use cookies to restore window on reload
        //      Content         Alternate to URL, pass DOM object to append to window content
        //      Height          Overloaded window default height
        //      IconClass       CSS Class for Icon
        //      IconX           Pixel position in sprite for Window/Taskbar icon X axis
        //      IconY           Pixel position in sprite for Window/Taskbar icon Y axis
        //      Id              Identifier for window in acore's window registry
        //      IgnoreMinSize   Bool switch to ignore the minimum height and width for window
        //      Input           Variables to post to Url
        //      Maximizable     Bool switch to allow window to be maximized
        //      MaxOnMobile     Bool switch to auto-maximize windows on mobile devices
        //      Menu            DOM identifier for parent menu (leave null if not on menu, pass array if in multiple menus)
        //      MenuName        Name as seen on the top navigation bar menus
        //      MinHeight       Minimum height for the window
        //      MinWidth        Minimum width for the window
        //      Modal           Bool switch to shade out background
        //      Name            Master window name
        //      OnLoad          Window onLoad/Callback event
        //      Status          Default text seen on the status bar of the window
        //      StatusBar       Bool switch to display window with or without the status bar
        //      TaskName        Name as seen on the bottom task bar
        //      Resize          Bool switch to allow or disallow window resizing
        //      Url             AJAX URL for window's content
        //      VersitileHeight Bool switch to adjust height dynamically to content size
        //      Width           Overloaded window default width
        //      WindowClass     Additional CSS class to skin window
        //      WindowFrame     Bool switch to display window with or without the standard frame
        //      WindowName      Name as seen on the top of the window
        //		Overflow		Sets the overflow of the window
        //      TwinId          Id of window that shares views with this window. Used to keep users from having both windows open.
        // Ensure Id and Name are set and that the Id isn't already used
        if (Options.Id != null && Options.Name != null && this.Windows[Options.Id] == null) {
            // If more specific names are not set, set them to the default name
            Options.WindowName = Options.WindowName == null ? Options.Name : Options.WindowName;
            Options.MenuName = Options.MenuName == null ? Options.Name : Options.MenuName;
            Options.TaskName = Options.TaskName == null ? Options.Name : Options.TaskName;
            // Set defaults
            Options = $.extend({
                Area: '',
                AlwaysOnTop: false,
                BackgroundLoad: false,
                Center: false,
                Cookies: true,
                Height: Acore.DefaultWinHeight,
                IconClass: "axxess",
                IgnoreMinSize: false,
                Maximizable: true,
                MaxOnMobile: true,
                MinHeight: Acore.MinWinHeight,
                MinWidth: Acore.MinWinWidth,
                Modal: false,
                StatusBar: true,
                Resize: true,
                VersitileHeight: false,
                Width: Acore.DefaultWinWidth,
                WindowFrame: true,
                Overflow: true,
                IsPost: false,
                MenuOrder: 0
            }, Options);
            // Set options
            this.Windows[Options.Id] = Options;
            // Add the items to the menu accordingly
            if (Options.Menu != null && Options.Menu.constructor == Array) for (i = 0; i < Options.Menu.length; i++) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu[i], Order: Options.MenuOrder.constructor == Array ? Options.MenuOrder[i] : Options.MenuOrder });
            else if (Options.Menu != null) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu, Order: Options.MenuOrder });
        }
    },
    Alert: function (Message, WindowClass) {
        if (Acore.Windows[Acore.AlertId] != undefined) $("#window_" + Acore.AlertId + "_content").html($("<div/>").AcoreAlert(Message)).parent().SetHeight();
        else {
            Acore.Modal({
                Alert: true,
                Name: "Notice",
                Width: 400,
                WindowClass: WindowClass ? WindowClass : "info",
                Content: $("<div/>").AcoreAlert(Message, WindowClass ? WindowClass : "info")
            });
            $("#window_" + Acore.AlertId).SetHeight();
        }
    },
    AppService: function (service) {
        if (service == HomeHealth.ApplicationService) return "HomeHealth";
        else if (service == PrivateDuty.ApplicationService) return "PrivateDuty";
        else return "";
    },
    Close: function (Window) {
        var AcoreId = Window.GetAcoreId();
        // If CKEditor found in window, remove instance
        if ($(".cke", Window).length) $(".cke", Window).each(function () {
            var editor = CKEDITOR.instances[$(this).attr("id").replace(/cke_/, "")];
            if (editor) CKEDITOR.remove(editor);
        });
        // Set IsOpen flag to false
        Acore.Windows[AcoreId].IsOpen = false;
        if (typeof Acore.Windows[AcoreId].Close === "function") Acore.Windows[AcoreId].Close();
        // Decrement number of open windows
        Acore.OpenWindows--;
        // If temporary modal window, remove window from registry
        if (AcoreId == Acore.DialogId) Acore.RemoveWindow(Acore.DialogId);
        else if (AcoreId == Acore.AlertId) Acore.RemoveWindow(Acore.AlertId);
        else if (AcoreId == Acore.ReturnReasonId) Acore.RemoveWindow(Acore.ReturnReasonId);
        else if (AcoreId == Acore.PrintPreviewId) Acore.RemoveWindow(Acore.PrintPreviewId);
        // If not temporary window, and cookies are enabled remove from open windows cookie
        else if (Acore.Cookies && Acore.Windows[AcoreId].Cookies) {
            var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
            if (OpenWindows.length) {
                for (var i = 0; i < OpenWindows.length; i++) if (OpenWindows[i].Id == AcoreId) OpenWindows.splice(i--, 1);
                U.Cookie.Set(Acore.OpenWindowsCookie, OpenWindows);
            } else U.Cookie.Delete(Acore.OpenWindowsCookie);
        }
        // Remove task bar from DOM
        Window.GetTask().Animate({ width: 0 }, function () { Window.GetTask().remove() });
        // Remove window from DOM
        var CenterX = Window.width() / 2 + Window.position().left, CenterY = Window.height() / 2 + Window.position().top;
        Window.find(".window-content").empty();
        if (Window.is(":visible")) {
            Window.Animate({ width: 0, height: 0, left: CenterX, top: CenterY }, function () { Window.remove() });
        } else {
            Window.remove();
        }
        // If background is shaded due to window currently being closed, remove shade
        if ($("#shade").length) {
            var removeShade = true;
            $.each(Acore.Windows, function () {
                if ($(this).attr("IsOpen") && $(this).attr("Modal")) {
                    var window = $("#window_" + $(this).attr("Id"));
                    if (window.is(":hidden")) window.show();
                    removeShade = false;
                    return false;
                }
            })
            if (removeShade) $("#shade").Animate({ opacity: 0 }, function () { $("#shade").remove() });
        }
    },
    CloseAlert: function () {
        if (Acore.Windows[Acore.AlertId] != undefined) $("#window_" + Acore.AlertId).Close();
    },
    CloseDialog: function () {
        if (Acore.Windows[Acore.DialogId] != undefined) $("#window_" + Acore.DialogId).Close();
    },
    ClosePrintPreview: function () {
        if (Acore.Windows[Acore.PrintPreviewId] != undefined) $("#window_" + Acore.PrintPreviewId).Close();
    },
    Confirm: function (Options) {
        //  Options:
        //      Name        The title of the confirm box, default is Question
        //      Width       The width of the confrim box, default is 400
        //      WindowClass The window CSS class, default is question
        //      Message     The message displayed to the user
        //      Yes         The callback that is called if the user clicks yes
        //      No          The callback that is called if the user clicks no.
        if (Acore.Windows[Acore.AlertId] != undefined) $("#window_" + Acore.AlertId + "_content").html($("<div/>").AcoreConfirm(Options)).parent().SetHeight();
        else {
            Acore.Modal({
                Alert: true,
                Name: Options.Name ? Options.Name : "Question",
                Width: Options.Width ? Options.Width : 400,
                WindowClass: Options.WindowClass ? Options.WindowClass : "question",
                Content: $("<div/>").AcoreConfirm(Options)
            });
            $("#window_" + Acore.AlertId).SetHeight();
        }
    },
    Dialog: function (Options) {
        //  Options:
        //      Alert           Bool switch to use alert id
        //      ReturnReason    Bool switch to use return reason id
        // Ensure that Modal option is always set
        if (Options.Modal == undefined) Options.Modal = false;
        // Set defaults based upon style of dialog
        if (Options.Alert || Options.ReturnReason) Options.Id = Options.Alert ? Acore.AlertId : Acore.ReturnReasonId, Options.AlwaysOnTop = true, Options.Alert = null, Options.ReturnReason = null;
        else if (!Options.Id) Options.Id = Acore.DialogId;
        if (!Options.Height) Options.VersitileHeight = true;
        // Set defaults and overwrite where applicable
        Options = $.extend({
            Center: true,
            Cookies: false,
            IgnoreMinSize: true,
            MaxOnMobile: Options.MaxOnMobile ? Options.MaxOnMobile : false,
            Maximizable: false,
            Name: "Axxess Agencycore",
            Resize: false,
            StatusBar: false,
            Width: 400,
            WindowFrame: !Options.Modal
        }, Options);
        // Add window
        Acore.AddWindow(Options);
        // Open modal window
        Acore.Open(Options.Id, Options.Url, Options.OnLoad, Options.Input);

    },
    IncrementIdle: function () {
        // Add to idle time
        Acore.IdleTime++;
        // If idle for long enough to start the logout count down
        if (Acore.IdleTime + Acore.TimeoutCount >= Acore.Timeout) {
            // If there is already a dialog window open, close it first
            if (Acore.Windows[Acore.AlertId] != null) $("#window_" + Acore.AlertId).Close();
            // Open AcoreIdle Modal
            Acore.Modal({
                Alert: true,
                Name: "Idle Logout",
                Content: $("<div/>").AcoreIdle()
            });
            $("#window_" + Acore.AlertId).SetHeight();
        }
    },
    Init: function () {
        window.CKEDITOR_BASEPATH = '/Scripts/Plugins/ckeditor/';
    },
    Modal: function (Options) {
        Acore.Dialog($.extend(Options, { Modal: true }));
    },
    OnLoad: function (Element) {
        // Global OnLoad Function
        if (Element.hasClass("window")) var Window = Element;
        else var Window = Element.closest(".window");
        // Rename Window
        Element.Rename();
        // Initialize special inputs
        Element.InitForms();
        // Add hover functions on mouseover fields
        if ($(".reports li,.acore-grid ol li,.hover-toggle", Element).length) $(".reports li,.acore-grid ol li,.hover-toggle", Element).not(".no-hover").on("mouseover", function () {
            $(this).addClass("hover")
        }).on("mouseout", function () {
            $(this).removeClass("hover")
        });
        // Set Status Messages
        if ($("input[type=text]:not([status]),input[type=password]:not([status]),select:not([status])", Element).length) $("input[type=text]:not([status]),input[type=password]:not([status]),select:not([status])", Element).each(function () {
            $(this).attr("status", $(this).closest(".row").find("label").text() + ($(this).hasClass("required") ? " (Required)" : ""));
        });
        if ($(".buttons a:not([status])", Element).length) $(".buttons a:not([status])", Element).each(function () {
            $(this).attr("status", $(this).text());
        });
        if ($(".winstatus", Element).length) Window.Status($(".winstatus", Element).text());
        if ($("[status]", Element).length) $("[status]", Element).on("mouseover", function () {
            $(this).closest(".window").Status($(this).attr("status"));
        }).on("mouseout", function () {
            $(this).closest(".window").Status($(this).closest(".window").find(".winstatus").length ? $(this).closest(".window").find(".winstatus").text() : "")
        });
        // Init Tooltips
        //if ($("[tooltip]", Element).length) ToolTip.Init(Element);
        // Telerick Grid placement and manipulations
        if ($(".t-group-indicator", Element).length) $(".t-group-indicator", Element).hide();
        if ($(".t-grouping-header", Element).length) $(".t-grouping-header", Element).remove();
        if ($(".grid-search", Element).length) $(".grid-search", Element).GridSearch();
        // Acore Grid Zebra Striping
        if ($(".acore-grid", Element).length || (Element.hasClass("acore-grid") && $("ol", Element).length)) {
            if (Element.hasClass("acore-grid") && $("ol", Element).length) var grid = Element;
            else var grid = $(".acore-grid", Element);
            $("ol", grid).Zebra(null, ".group-header");
            if (grid.hasClass("selectable")) {
                $("ol > li", grid.filter(".selectable")).click(function (e) {
                    if (!$(e.target).is(":input,a")) {
                        var selectBox = $(this).find("input:checkbox, input:radio");
                        if (selectBox.length) selectBox.click();
                    }
                });
            }
            if ($("ul .sortable", grid).length) $("ul .sortable", grid).on("click", function () {
                var numbered = $(this).closest("li").find(".grid-number").length,
                    invert = $(this).hasClass("sorted"),
                    eq = $(this).index(),
                    list = $(this).closest("ul").next("ol");
                list.addClass("loading");
                if ($(this).hasClass("sorted")) $(this).removeClass("sorted").addClass("inverted-sorted");
                else if ($(this).hasClass("inverted-sorted")) $(this).removeClass("inverted-sorted").addClass("sorted");
                else {
                    $(this).closest("li").find(".sorted,.inverted-sorted").removeClass("sorted inverted-sorted");
                    $(this).addClass("sorted");
                }
                $(this).closest("li").find(".ui-icon-circle-triangle-n,.ui-icon-circle-triangle-s").remove();
                if (invert) $(this).append($("<span/>").addClass("img icon ui-icon-circle-triangle-n"));
                else $(this).append($("<span/>").addClass("img icon ui-icon-circle-triangle-s"));
                setTimeout(function () {
                    var items = list.children("li").get();
                    items.sort(function (a, b) {
                        var child = $(a).children("span:eq(" + eq + ") .sort-value").length ? "span:eq(" + eq + ") .sort-value" : "span:eq(" + eq + ")",
                            sortValue1 = $(a).children(child).text().trim().toUpperCase(),
                            sortValue2 = $(b).children(child).text().trim().toUpperCase();
                        if (U.IsDate(sortValue1) && U.IsDate(sortValue2)) return new Date(sortValue1) < new Date(sortValue2);
                        return sortValue1.localeCompare(sortValue2);
                    });
                    if (invert) items.reverse();
                    $.each(items, function (index, item) { list.append(item) });
                    if (numbered) $("li", list).each(function (index, item) { $(".grid-number", this).text(String(index + 1) + ".") });
                    list.Zebra(null, ".group-header").removeClass("loading");
                }, 50);
            });
        }
    },
    //Positions any non relative telerik grid during the loading of a window
    InitGridControls: function (Window) {
        var positionFunc = function() {
            var gridControls = $(".grid-controls,.report-controls", Window),
                grid = $(".t-grid", Window).not(".position-relative");
            if (gridControls.length && grid.length) grid.css({ top: $(".wrapper", Window).prop("offsetHeight") });
        };
        if (!$(".window-content", Window).hasClass("transistion")) positionFunc();
        else {
            var clearTransition = setInterval(function() {
                if (!$(".window-content", Window).hasClass("transistion")) {
                    positionFunc();
                    clearInterval(clearTransition);
                }
            }, 10);
        }
    },
    OpenTest: function (OptionsInput) {
        //        if (typeof Id == "object") var Parameters = Id;
        //        else {
        var Parameters = { Id: OptionsInput.Id, Url: OptionsInput.Url, OnLoad: OptionsInput.OnLoad, Inputs: OptionsInput.Inputs };
        //            if (typeof Url == "function") Parameters.OnLoad = Url, Parameters.Url = undefined;
        //            if (typeof Url == "object") Parameters.Inputs = Url, Parameters.Url = undefined;
        //            if (typeof Inputs == "object") Parameters.Inputs = Inputs, Parameters.Url = Url;
        //            if (typeof OnLoad == "object") Parameters.Inputs = OnLoad, Parameters.OnLoad = undefined;
        //            if (typeof OnLoad == "function") Parameters.OnLoad = OnLoad;
        //            if (typeof OnLoad == "string" && OnLoad.length > 0) Parameters.Url = OnLoad;
        if (!OptionsInput.Inputs) {
            if (OptionsInput.IsPost) {
                Parameters.Inputs = {}
            }
        }
        //        }
        // Ensure the top menus are all collapsed
        $("ul.menu").hide();
        // If window does not exists
        if (this.Windows[Parameters.Id] == null) U.Growl("Not able to find window: " + Parameters.Id + ".  Please contact Axxess for assistance in troubleshooting.", "error");
        // If window exists in registry
        else {
            // If window is already open, focus and growl
            if (this.Windows[Parameters.Id].IsOpen) {
                U.Growl("You already have a open window for " + this.Windows[Parameters.Id].Name + ". Please close down this window before opening another one like it.", "error");
                $("#window_" + Parameters.Id).WinFocus();
                // If opening window will not hit max window threshold
            } else if (this.OpenWindows >= this.MaxWindows) {
                U.Growl("You can only have " + this.MaxWindows + " concurrent windows at once. Please close down some of your windows to proceed", "error");
                // Good to open
            } else {
                // Set window options
                var Options = {};
                $.extend(Options, Acore.Windows[Parameters.Id]);
                Options.Id = Parameters.Id;
                // Set window overrides
                if (typeof Parameters.Url == "string") {
                    Options.Url = Parameters.Url;
                    //Set the area of the window
                    if (Options.Url.indexOf(PrivateDuty.Area) != -1) Acore.Windows[Parameters.Id].Area = PrivateDuty.Area;
                    else Acore.Windows[Parameters.Id].Area = "";
                }
                if (typeof Parameters.OnLoad == "function") Options.OnLoad = Parameters.OnLoad;
                if (typeof Parameters.Inputs == "object") Options.Inputs = Parameters.Inputs;
                /* ## UNCOMMENT TO FORCE ALL WINDOWS TO USE POST INSTEAD OF GET ##
                else Options.Inputs = {}; */
                // Add window onto the desktop
                $("#desktop").append(
                    $("<div/>").AcoreWindow(Options)
                );
                // If not a temporary window and cookies are enabled, add to Open Windows cookie
                if (Parameters.Id != Acore.AlertId && Parameters.Id != Acore.DialogId && Parameters.Id != Acore.ReturnReasonId && Acore.Cookies) {
                    var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
                    if (!OpenWindows) OpenWindows = new Array();
                    var push = true;
                    for (var i = 0; i < OpenWindows.length; i++) if (OpenWindows[i].Id == Parameters.Id) push = false;
                    if (push) OpenWindows.push(Parameters);
                    U.Cookie.Set(Acore.OpenWindowsCookie, OpenWindows);
                }
                if (!Acore.Mobile || !Options.MaxOnMobile) {
                    // If window is smaller than minimum sizes, set to minimum size
                    if (!Options.IgnoreMinSize && $("#window_" + Options.Id).width() < Options.MinWidth) $("#window_" + Options.Id).css("width", Options.MinWidth);
                    if (!Options.IgnoreMinSize && $("#window_" + Options.Id).height() < Options.MinHeight) $("#window_" + Options.Id).css("height", Options.MinHeight);
                    // If window is larger than desktop, auto-maximize window
                    if (($("#window_" + Options.Id).width() > $("#desktop").width()) || ($("#window_" + Options.Id).height() > $("#desktop").height())) $("#window_" + Options.Id).Maximize();
                    // If window is to be cascaded, set top and left accordingly
                    if (!Options.Center) {
                        // Increment left and top counters, and if window can't fit reset to zero
                        var Width = Options.Width, Height = Options.Height;
                        if (typeof Width == "string" && Width.indexOf("%") > 0) Width = parseInt($("#desktop").width() * (Width.replace(/[^0-9]/, "") / 100));
                        if (typeof Height == "string" && Height.indexOf("%") > 0) Height = parseInt($("#desktop").height() * (Height.replace(/[^0-9]/, "") / 100));
                        if (++Acore.CssLeft * Acore.CascadeGap + Width > $("#desktop").width()) Acore.CssLeft = 0;
                        if (++Acore.CssTop * Acore.CascadeGap + Height > $("#desktop").height()) Acore.CssTop = 0;
                        // Set top and left accordingly
                        $("#window_" + Options.Id).css({
                            top: Acore.CssTop * Acore.CascadeGap,
                            left: Acore.CssLeft * Acore.CascadeGap
                        });
                    }
                }
                if (Options.Overflow) $("#window_" + Options.Id + "_content").css("overflow", Options.Overflow);
                // Set IsOpen flag to true
                this.Windows[Parameters.Id].IsOpen = true;
            }
        }
    },
    Open: function (Id, Url, OnLoad, Inputs) {
        if (typeof Id == "object") var Parameters = Id;
        else {
            var Parameters = { Id: Id, Url: Url };
            if (typeof Url == "function") Parameters.OnLoad = Url, Parameters.Url = undefined;
            if (typeof Url == "object") Parameters.Inputs = Url, Parameters.Url = undefined;
            if (typeof Inputs == "object") Parameters.Inputs = Inputs, Parameters.Url = Url;
            if (typeof OnLoad == "object") Parameters.Inputs = OnLoad, Parameters.OnLoad = undefined;
            if (typeof OnLoad == "function") Parameters.OnLoad = OnLoad;
            if (typeof OnLoad == "string" && OnLoad.length > 0) Parameters.Url = OnLoad;
        }
        // Ensure the top menus are all collapsed
        $("ul.menu").hide();
        // If window does not exists
        if (this.Windows[Parameters.Id] == null) U.Growl("Not able to find window: " + Parameters.Id + ".  Please contact Axxess for assistance in troubleshooting.", "error");
        // If window exists in registry
        else {
            // If window is already open, focus and growl
            if (this.Windows[Parameters.Id].IsOpen) {
                U.Growl("You already have a open window for " + this.Windows[Parameters.Id].Name + ". Please close down this window before opening another one like it.", "error");
                $("#window_" + Id).WinFocus();
                // If opening window will not hit max window threshold
            } else if (this.OpenWindows >= this.MaxWindows) {
                U.Growl("You can only have " + this.MaxWindows + " concurrent windows at once. Please close down some of your windows to proceed", "error");
                // Good to open
            } else {
                // Set window options
                var Options = {};
                $.extend(Options, Acore.Windows[Parameters.Id]);
                Options.Id = Parameters.Id;
                // Set window overrides
                if (typeof Parameters.Url == "string") {
                    Options.Url = Parameters.Url;
                    //Set the area of the window
                    if (Options.Url.indexOf(PrivateDuty.Area) != -1) Acore.Windows[Parameters.Id].Area = PrivateDuty.Area;
                    else Acore.Windows[Parameters.Id].Area = "";
                }
                if (typeof Parameters.OnLoad == "function") Options.OnLoad = Parameters.OnLoad;
                if (typeof Parameters.Inputs == "object") Options.Inputs = Parameters.Inputs;
                /* ## UNCOMMENT TO FORCE ALL WINDOWS TO USE POST INSTEAD OF GET ##
                else Options.Inputs = {}; */
                // Add window onto the desktop
                $("#desktop").append(
                    $("<div/>").AcoreWindow(Options)
                );
                var window = $("#window_" + Options.Id);
                // If not a temporary window and cookies are enabled, add to Open Windows cookie
                if (Acore.Windows[Parameters.Id].Cookies && Acore.Cookies) {
                    var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
                    if (!OpenWindows) OpenWindows = new Array();
                    var push = true;
                    for (var i = 0; i < OpenWindows.length; i++) if (OpenWindows[i].Id == Parameters.Id) push = false;
                    if (push) OpenWindows.push(Parameters);
                    U.Cookie.Set(Acore.OpenWindowsCookie, OpenWindows);
                }
                if (!Acore.Mobile || !Options.MaxOnMobile) {
                    // If window is smaller than minimum sizes, set to minimum size
                    if (!Options.IgnoreMinSize && window.width() < Options.MinWidth) window.css("width", Options.MinWidth);
                    if (!Options.IgnoreMinSize && window.height() < Options.MinHeight) window.css("height", Options.MinHeight);
                    // If window is larger than desktop, auto-maximize window
                    if ((window.width() > $("#desktop").width()) || (window.height() > $("#desktop").height())) window.Maximize();
                    // If window is to be cascaded, set top and left accordingly
                    if (!Options.Center) {
                        // Increment left and top counters, and if window can't fit reset to zero
                        var Width = Options.Width, Height = Options.Height;
                        if (typeof Width == "string" && Width.indexOf("%") > 0) Width = parseInt($("#desktop").width() * (Width.replace(/[^0-9]/, "") / 100));
                        if (typeof Height == "string" && Height.indexOf("%") > 0) Height = parseInt($("#desktop").height() * (Height.replace(/[^0-9]/, "") / 100));
                        if (++Acore.CssLeft * Acore.CascadeGap + Width > $("#desktop").width()) Acore.CssLeft = 0;
                        if (++Acore.CssTop * Acore.CascadeGap + Height > $("#desktop").height()) Acore.CssTop = 0;
                        // Set top and left accordingly
                        window.css({
                            top: Acore.CssTop * Acore.CascadeGap,
                            left: Acore.CssLeft * Acore.CascadeGap
                        });
                    }
                }
                if (Options.Overflow) $(".window-content", window).css("overflow", Options.Overflow);
                // Set IsOpen flag to true
                this.Windows[Parameters.Id].IsOpen = true;
                if (Options.Content && (Options.Center || Options.VersitalHeight)) window.SetHeight();
            }
        }
    },
    OpenPrintView: function (Options) {
        //  Options:
        //      Url         URL to print preview
        //      PdfUrl      URL to fetch PDF
        //      PdfData     Data for PDF Print POST
        //      ReturnClick Function for return button
        //      Buttons     Buttons array
        //          Text        Button text
        //          Click       Button click function
        // Open AcorePrintView modal
        var acrobatInfo = getAcrobatInfo();
        if (acrobatInfo.acrobat) {
            Acore.Modal({
                Name: "Print View",
                BackgroundLoad: true,
                Height: "95%",
                Width: 950,
                WindowFrame: false,
                Id: Acore.PrintPreviewId
            });
            $("#window_" + Acore.PrintPreviewId).Center().find(".window-content").AcorePrintView(Options);
        } else {
            Acore.Confirm({
                Message: "The print preview cannot be opened. " + navigator.BrowserVersion[0] + " is missing a vital component, the Adobe Reader Plugin. To download the plugin click Yes.",
                Yes: function () {
                    var win = window.open("http://get.adobe.com/reader/", '_blank');
                    win.focus();
                },
                No: function () {
                    U.Growl("Unable to open the print preview.", "error");
                }
            })
        }
    },
    ClosePrintView: function () {
        if (Acore.Windows[Acore.PrintPreviewId] != undefined) $("#window_" + Acore.PrintPreviewId).Close();
    },
    Ping: function () {
        // Make AJAX POST to ping server
        U.PostUrl("Ping", null, function (data) {
            if (!data.isSuccessful) $(location).attr("href", "SessionExpired");
        })
    },
    RemoveWindow: function (Id) {
        // If window is found, remove from Acore window information registry
        if (this.Windows[Id].Menu == undefined) this.Windows[Id] = null;
        // Else growl to user that window cannot be found
        else U.Growl("Cannot remove a window which is referenced in the main menu.", "error");
    },
    ReturnReason: function (Options) {
        if (Acore.Windows[Acore.ReturnReasonId] != undefined) {
            var content = $("#window_" + Acore.ReturnReasonId + " .return-comments-container");
            if (!Options.EventId || Options.EventId == content.attr("EventId")) content.ReturnComments({ Action: "Refresh", UrlPrefix: Options.UrlPrefix });
            else U.Growl("The Return Reason window is already open for another document. Please close the Return Reason window first before opening it for another document.", "error");
        }
        else {
            if (Options.ReturnFunction && !Options.CloseFunction) Options.CloseFunction = function () {
                if ($("#print-controls").length) $("ul", "#print-controls").show();
            };
            var closeClickFunction = function() {
                if (typeof Options.CloseFunction === "function") Options.CloseFunction();
                $(this).closest(".window").Close();
            };
            Acore.AddWindow({
                AlwaysOnTop: true,
                Center: false,
                Content: $("<div/>", { "class": "wrapper main" }).ReturnComments($.extend({}, Options, { CloseFunction: closeClickFunction })),
                Height: 451,
                Id: Acore.ReturnReasonId,
                IgnoreMinSize: true,
                Name: "Return Reason",
                Minimizable: typeof Options.ReturnFunction != "function",
                Maximizable: false,
                Modal: false,
                Resize: false,
                ReturnReason: true,
                StatusBar: false,
                Width: 410,
                WindowClass: "error",
                Close: Options.CloseFunction
            });
            Acore.Open(Acore.ReturnReasonId);
        }
        Acore.OnLoad($("#window_" + Acore.ReturnReasonId));
    },
    ReturnReasonFailed: function () {
        $("#window_" + Acore.ReturnReasonId).find(".buttons a:contains('Return')").trigger("ProcessingComplete");
    },
    CloseReturnReason: function () {
        if (Acore.Windows[Acore.ReturnReasonId] != undefined) $("#window_" + Acore.ReturnReasonId).Close();
    },
    SetApplicationServices: function (service) {
        this.ApplicationServices = service;
        HomeHealth.SetActive(service);
        PrivateDuty.SetActive(service);
    },
    SucessPDFLoad: function (Options, iframe) {
        var windowContent = $(iframe).closest(".window-content");
        $(windowContent).append(
            $("<div/>", { id: "print-controls", "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>").text("Close").on("click", function () {
                            $(windowContent).closest(".window").Close()
                        })
                    )
                )
            )
        );
        // If PDF Print, set click function
        if (Options.PdfUrl != null && Options.PdfUrl != "") {
            $("#print-controls ul", windowContent).prepend(
                $("<li/>").append(
                    $("<a/>", { id: "printbutton", text: "Print" }).on("click", function () {
                        U.GetAttachment(Options.PdfUrl, Options.PdfData)
                    })
                )
            );
        }
        // If return button is enabled, add return reason field and proper button functionality
        if (Options.ReturnClick != null && typeof (Options.ReturnClick) == "function") {
            $("#print-controls ul", windowContent).prepend(
                $("<li/>").append(
                    $("<a/>", { id: "printreturn", text: "Return" }).on("click", function() {
                        $("ul", "#print-controls").hide();
                        Acore.ReturnReason({EventId: Options.PdfData.eventId, PatientId: Options.PdfData.patientId, ReturnFunction:Options.ReturnClick, UrlPrefix:Options.UrlPrefix });
                    })
                )
            )
        }
        // If custom buttons are defined, add them to the start of the button list
        if (Options.Buttons != null) for (i = Options.Buttons.length; i > 0; i--) {
            if (Options.Buttons[i - 1].Hidden == undefined || Options.Buttons[i - 1].Hidden == false) $("#print-controls ul").prepend(
                $("<li/>").append(
                    $("<a/>").text(Options.Buttons[i - 1].Text).on("click", Options.Buttons[i - 1].Click)
                )
            )
        }
        $(iframe).removeClass("loading");
        document.cookie = "fileDownload" + "=; expires=" + new Date(1000).toUTCString() + "; path=" + "/";
        $(document, parent.window.document).find("#window_" + Acore.PrintPreviewId).Animate("fadeIn");
        if (typeof Options.Init == "function") Options.Init.apply(windowContent);
    },
    FaildPDFLoad: function (iframe) {
        var content = $(iframe).contents();
        setTimeout(function () { $(iframe).remove() }, 0)
        U.Growl(JSON.parse(content[0].body.innerHTML.replace("<pre>", "").replace("</pre>", "")).errorMessage, "error");
        $(iframe).closest(".window").Close();
    },
    MonitorPDFDownload: function (Options, iframe, count, deferred) {
        if (document.cookie.indexOf("fileDownload" + "=" + "true") != -1) {
            deferred.resolve(Options, iframe);
            return;
        } else {
            var iframedocument = iframe.contentWindow || iframe.contentDocument;
            if (iframedocument) {
                var contentType = iframedocument.contentType || iframedocument.document.contentType;
                if (contentType == 'application/json') {
                    deferred.reject(iframe);
                    return;
                }
            }
        }
        if (count < 10) {
            setTimeout(function () { Acore.MonitorPDFDownload(iframe, count) }, 100);
        }
        count++;
    }
};
(function($) {
    $.extend($.fn, {
        AcoreAlert: function(Message, WindowClass) {
            return this.each(function() {
                $(this).AcoreDialog(Message, WindowClass, [{ Text: "OK", Click: function() { $(this).closest(".window").Close() } }]);
            })
        },
        AcoreConfirm: function(Options) {
            return this.each(function() {
                var e = $(this);
                e.AcoreDialog(Options.Message, Options.WindowClass ? Options.WindowClass : "question", [
                    {
                        Text: "Yes",
                        Click: function() {
                            if (typeof Options.Yes == "function") try {
                                Options.Yes();
                            } catch (error) {
                                console.log(error);
                                U.Growl(U.MessageErrorJS(true), "error")
                            }
                            e.closest(".window").Close();
                            return false;
                        }
                    }, {
                        Text: "No",
                        Click: function() {
                            if (typeof Options.No == "function") try {
                                Options.No();
                            } catch (error) {
                                console.log(error);
                                U.Growl(U.MessageErrorJS(true), "error")
                            }
                            e.closest(".window").Close();
                            return false;
                        }
                    }
                ])
            })
        },
        AcoreDialog: function(Message, IconClass, Buttons) {
            return this.each(function() {
                $(this).append(
                    $("<div/>", { id: "dialog-modal", "class": "wrapper main ac strong", html: Message }).prepend(
                        $("<div/>").addClass("fl").append(
                            $("<span/>").addClass("img icon48 " + IconClass)
                        )).append(
                        $("<div>").addClass("clr")).append(
                        $("<div/>").Buttons(Buttons)
                    )
                ).closest(".window").SetHeight();
            })
        },
        AcoreDesktop: function(Options) {
            return this.each(function() {
                // If loaded in frame, reload out of the frame
                if (window.location !== window.top.location) window.top.location = window.location;
                // Build DOM for desktop
                $(this).append(
                    $("<div/>", { id: "desktop" }).on("contextmenu", function(Event) {
                        $("#mainmenu").clone(true).ContextMenu(Event);
                    })).append(
                    $("<div/>", { id: "topbar" }).css({ "z-index": parseInt(Acore.MaxWindows + 1), top: -64 }).append(
                        $("<ul/>", { "class": "fr" }).append(
                            $("<li/>").append(
                                $("<a/>", { "text": "Logout" }).on("click", function() {
                                    Acore.Confirm({
                                        Message: "Are your sure you want to logout?",
                                        Yes: function() {
                                            $(location).attr("href", "Logout");
                                        }
                                    });
                                    return false;
                                }).prepend(
                                    $("<span/>", { "class": "img icon22 logout" }))))).append(
                        $("<ul/>", { id: "mainmenu", "class": "sf-menu" })).append(
                        $("<div/>", { "class": "img tab", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) })).append(
                    )).append(
                    $("<div/>", { id: "bottombar" }).css({ "z-index": parseInt(Acore.MaxWindows + 1), bottom: -32 }).append(
                        $("<ul/>", { "id": "task" })).append(
                        $("<em/>", { "class": "fr", "html": "&#169; 2009 &#8211; " + (new Date).getFullYear() + " Axxess&#8482; Technology Solutions, All Rights Reserved" })
                    )
                );
                $.each(Options, function(index, value) {
                    if (index == "Menu") $.each(value, function() { Acore.AddMenu(this) });
                    else if (index == "MenuLinks") $.each(value, function() { Acore.AddMenuItem(this) });
                    else if (index == "Windows") $.each(value, function() { Acore.AddWindow(this) });
                    else if (index == "ApplicationServices") Acore.SetApplicationServices(value);
                    else Acore[index] = value;
                });
                $("#mainmenu .menu").each(function() {
                    if ($("li > a", this).not(".menu-trigger").length == 0) $(this).parent().remove();
                    else {
                        //Sort the items in the menu
                        var menuItems = $(this).children();
                        menuItems.detach().sort(function(a, b) {
                            // Return $(a).data("order") - $(b).data("order");
                            a = $(a).data("order"),
                            b = $(b).data("order");
                            if (b == 0 && a != 0) return -1;
                            if (b != 0 && a == 0) return 1;
                            if (a == 0 && b == 0) return 0;
                            if (a > b) return 1;
                            if (a < b) return -1;
                            return 0;
                        });
                        $(this).append(menuItems);
                    }
                });
                // Set keyboard shortcuts
                $(document).keypress(function(e) {
                    // ALT+R JavaScript run prompt
                    if (e.which == 114 && e.altKey) eval(prompt("Run:\nRun the following JavaScript command:", ""));
                    // ALT+W Open acore window by id
                    if (e.which == 119 && e.altKey) eval("Acore.Open('" + prompt("Load Window:\nOpen the following AgencyCore window:", "") + "')");
                    // ALT+L Logout of software
                    if (e.which == 108 && e.altKey) Acore.Confirm({
                        Message: "Are you sure you want to logout?",
                        Yes: function() { $(location).attr(href, "/Logout") }
                    });
                    // ALT+X Close current acore window
                    if (e.which == 120 && e.altKey) if ($(".window.active").length) $(".window.active").Close();
                });
                // Bind mouse and keyboard events to trigger resetting idle counter
                $(document).on("mousemove keydown DOMMouseScroll mousewheel mousedown", function() {
                    Acore.IdleTime = 0;
                });
                // Set thread to keep server connection alive
                Acore.PingTimer = setInterval(Acore.Ping, Acore.TimeoutInterval);
                // Set thread to count idle time
                Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                // Set Resize Event to Reposition Grids Under Controls
                $(window).on("resize", function() {
                    $(".grid-controls,.report-controls").closest(".wrapper").each(function() {
                        $(".t-grid", this).not(".position-relative").css("top", $(this).prop("offsetHeight"))
                    })
                });
                // Finalize desktop
                $("#mainmenu").Menu();
                $("#topbar").Animate({ top: 0 });
                $("#bottombar").Animate({ bottom: 0 });
                // If Cookies Enabled, Load Previous State
                if (Acore.Cookies) {
                    var OpenWindows = U.Cookie.Get(Acore.OpenWindowsCookie);
                    if (OpenWindows) {
                        for (var i = 0; i < OpenWindows.length; i++) {
                            if (Acore.Windows[OpenWindows[i].Id] && Acore.Windows[OpenWindows[i].Id].Cookies) Acore.Open(OpenWindows[i]);
                            else OpenWindows.splice(i--, 1);
                        }
                        U.Cookie.Set(Acore.OpenWindowsCookie, OpenWindows);
                    }
                }
                if (Acore.OpenWindows == 0) Acore.Open(Acore.DefaultWindow);
            })
        },
        // Initialize idle modal
        AcoreIdle: function() {
            return this.each(function() {
                // Stop thread to count idle time
                clearInterval(Acore.TimeoutTimer);
                // Build idle countdown modal
                $(this).AcoreDialog("Due to inactivity, you will automatically be logged off in <span id=\"timeoutsec\"></span>", "question", [
                    {
                        Text: "Stay Logged In",
                        Click: function() {
                            // Stop thread for second by second updates
                            clearInterval(IdleCountdown);
                            // Close idle countdown modal
                            $(this).closest(".window").Close();
                            // Reset thread to count idle time
                            Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                            return false;
                        }
                    }, {
                        Text: "Logout",
                        Click: function() {
                            // Logout of software
                            $(location).attr("href", "Logout");
                            return false;
                        }
                    }
                ]);
                // Calculate auto-logout time
                var logout = new Date();
                logout.setTime(logout.getTime() + Acore.TimeoutCount * Acore.TimeoutInterval);
                // Set thread for second by second updates
                IdleCountdown = setInterval(function() {
                    // Calulate time remaining till auto-logout
                    var currentTime = new Date();
                    var remaining = new Date();
                    remaining.setTime(logout.getTime() - currentTime.getTime());
                    // If time is up, logout of software
                    if (parseInt(remaining.getMinutes()) == 59) $(location).attr("href", "SessionExpired");
                    // Else display amount of time remaining
                    else $("#timeoutsec").html(remaining.getMinutes() + ":" + (remaining.getSeconds() < 10 ? "0" + remaining.getSeconds() : remaining.getSeconds()));
                }, 1000);
            })
        },
        AcorePrintView: function(Options) {
            return this.each(function() {
                // Build basic print view
                var e = $(this),
                    iframeAttr = { src: Options.Url, name: "printview", id: "printview", "class": "loading" },
                    loadFunction = function() { };
                if (Options.PdfUrl) loadFunction = function(event) {
                    var deferred = new $.Deferred();
                    deferred.done(Acore.SucessPDFLoad);
                    deferred.fail(Acore.FaildPDFLoad);
                    setTimeout(function() { Acore.MonitorPDFDownload(Options, event.target, 1, deferred) }, 100);
                    return deferred.promise();
                }
                else loadFunction = function(event) {
                    Acore.SucessPDFLoad(Options, this);
                }
                e.css("overflow", "hidden").append(
                    $("<div/>", { id: "print-box" }).append(
                        $("<iframe/>", iframeAttr).load(loadFunction)
                    )
                )
            })
        },
        AcoreTask: function(Options) {
            return this.each(function() {
                // Build task bar item
                $(this).attr({
                    id: "task_" + Options.Id,
                    "class": "task active",
                    style: "width:" + 100 / Acore.MaxWindows + "%"
                }).on("click", function() {
                    if ($(this).closest(".task").GetWindow().hasClass("active")) $(this).closest(".task").GetWindow().Minimize();
                    else $(this).closest(".task").GetWindow().WinFocus();
                }).append(
                    $("<span/>", { "class": "img icon22" })).append(
                    $("<span/>", { "class": "task-name", title: Options.TaskName, text: Options.TaskName })
                ).on("contextmenu", function(Event) {
                    // Build context menu
                    var Menu = $("<ul/>");
                    var AcoreWindow = $(this).GetWindow();
                    // If not active window, add option to bring to front
                    if (!AcoreWindow.hasClass("active")) Menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Bring to Front" }).on("click", function() {
                                AcoreWindow.WinFocus();
                            })
                        )
                    );
                    // If not minimized, add option to minimize
                    if (!AcoreWindow.hasClass("minimized")) Menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Minimize" }).on("click", function() {
                                AcoreWindow.Minimize();
                            })
                        )
                    );
                    // If not maximized, add option to maximize
                    if (!AcoreWindow.hasClass("maximized")) Menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Maximize" }).on("click", function() {
                                AcoreWindow.Maximize();
                            })
                        )
                    );
                    // If maximized, add option to restore
                    else Menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Restore" }).on("click", function() {
                                AcoreWindow.Restore();
                            })
                        )
                    );
                    // Add option to close
                    Menu.append(
                        $("<li/>").append(
                            $("<a/>", { text: "Close" }).on("click", function() {
                                AcoreWindow.Close();
                            })
                        )
                    );
                    // Enable context menu
                    Menu.ContextMenu(Event);
                });
                if (Options.IconClass) $(".icon22", this).addClass(Options.IconClass);
                if (Options.IconX && Options.IconY) $(".icon22", this).css("background-position", "-" + Options.IconX + "px -" + Options.IconY + "px");
            })
        },
        AcoreWindow: function(Options) {
            return this.each(function() {
                // Build basic window
                $(this).attr({
                    id: "window_" + Options.Id,
                    "class": "window"
                }).css("z-index", ++Acore.OpenWindows).on("mousedown", function() {
                    if (!$(this).hasClass("active")) $(this).WinFocus();
                }).append(
                    $("<div/>", { "id": "window_" + Options.Id + "_content", "class": "window-content loading" + (Options.Error ? " error" : "") + (Options.WindowClass ? " " + Options.WindowClass : "") })
                ).hide();
                // If window has frame, add top bar
                if (Options.WindowFrame) {
                    $(".window-content", this).before(
                        $("<div/>", { "class": "window-top" }).append(
                            $("<span/>", { "class": "fl img icon22" }).dblclick(function() {
                                $(this).closest(".window").Close();
                            })
                            ).append(
                            $("<span/>", { "class": "title", "text": Options.WindowName }).dblclick(function() {
                                if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                else $(this).closest(".window").Maximize();
                            })).append(
                            $("<span/>", { "class": "fr" }).append(
                                $("<span/>", { "class": "img win min" }).on("click", function() {
                                    $(this).closest(".window").Minimize();
                                })).append(
                                $("<span/>", { "class": "img win max" }).on("click", function() {
                                    if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                    else $(this).closest(".window").Maximize();
                                })).append(
                                $("<span/>", { "class": "img win pin" }).on("click", function() {
                                    if ($(this).hasClass("selected")) $(this).removeClass("selected").closest(".window").css("z-index", Acore.OpenWindows).removeClass("top-window");
                                    else $(this).addClass("selected").closest(".window").css("z-index", Acore.MaxWindows + ($("#shade").length ? 2 : 1)).addClass("top-window");
                                })).append(
                                $("<span/>", { "class": "img win close" }).on("click", function() {
                                    //if (Options.Id == Acore.ReturnReasonId && $("#print-controls").length) $("ul", "#print-controls").show();
                                    $(this).closest(".window").Close();
                                })
                            )
                        )
                    );
                    $(".img.win", this).on("mouseover", function() {
                        $(this).addClass("hover")
                    }).on("mouseout", function() {
                        $(this).removeClass("hover")
                    });
                    if (Options.IconClass) $(".icon22", this).addClass(Options.IconClass);
                    if (Options.IconX && Options.IconY) $(".icon22", this).css("background-position", "-" + Options.IconX + "px -" + Options.IconY + "px");
                    if (Options.Maximizable == false) $(".win.max", this).remove();
                    // If unframed window, set content top accordingly
                } else $(".window-content", this).css({
                    top: Acore.WindowBorder,
                    right: Acore.WindowBorder,
                    bottom: Acore.WindowBorder,
                    left: Acore.WindowBorder
                });
                // If window is resizable, enable resizing
                if (Options.Resize) $(this).resizable({
                    containment: "parent",
                    handles: "n, ne, e, se, s, sw, w, nw",
                    minWidth: Options.MinWidth > 0 ? Options.MinWidth : Acore.MinWinWidth,
                    minHeight: Options.MinHeight > 0 ? Options.MinHeight : Acore.MinWinHeight
                });
                // If window is not resizable and framed, remove resize button
                else if (Options.WindowFrame) $(".window-resize", this).remove();
                // If framed window has status bar, add in status bar and set content bottom accordingly
                if (Options.WindowFrame && Options.StatusBar) {
                    $(this).append($("<div/>", { "class": "window-bottom", text: Options.Status }));
                    $(".window-content", this).css("bottom", 21);
                }
                // Set window size
                if (parseInt(Options.Width) > 0) $(this).css("width", Options.Width);
                if (parseInt(Options.Height) > 0) $(this).css("height", Options.Height);
                // If modal window, shade out the background and increase the z-index
                if (Options.Modal) {
                    if ($("#shade").length == 0) $("body").append(
                        $("<div/>", { "id": "shade", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) }).on("contextmenu", function(Event) { Event.preventDefault() })
                    );
                    else if($(".window.active").attr("id") != "window_" + Acore.ReturnReasonId) $(".window.active").hide();
                    $(this).css({ "z-index": parseInt(Acore.MaxWindows) + 2 }).removeClass("standard");
                    // If framed non-modal window, enable draggable
                } else if (Options.WindowFrame) $(this).draggable({
                    containment: "parent",
                    handle: ".window-top .title"
                });
                if (Options.MaxOnMobile) $(this).addClass("mobile-max");
                if (Options.Overflow == false) $(".window-content", this).addClass("no-scroll");
                // Fade In Animation
                if (!Options.BackgroundLoad) $(this).Animate("fadeIn");
                // Load window content
                if (Options.Content) $(".window-content", this).removeClass("loading").append(Options.Content)
                else if (Options.Url) $(this).Load(Options.Url, Options.Inputs, Options.OnLoad);
                else $(".window-content", this).removeClass("loading");
                // Build task bar item for window
                $("#task").append($("<li/>").AcoreTask(Options));
                // If always on top, fix z-index and set class
                if (Options.AlwaysOnTop) $(this).css("z-index", parseInt(Acore.MaxWindows + ($("#shade").length ? 2 : 1))).addClass("top-window").find(".img.win.pin").addClass("selected").off("click");
                // Bring window to focus
                $(this).WinFocus();
            })
        },
        Center: function(Animated) {
            return this.each(function() {
                if ($(this).hasClass("window")) {
                    var Window = $(this),
                        Top = ($("#desktop").height() - $(this).height()) / 2,
                        Left = ($("#desktop").width() - $(this).width()) / 2,
                        Width = $(this).css("width"),
                        Height = $(this).css("height");
                    Animated = Animated == false ? false : Acore.Animated;
                    if (Acore.Windows[Window.GetAcoreId()].VersitileHeight) {
                        if (Acore.Windows[Window.GetAcoreId()].IgnoreMinSize) Window.css({ width: 200, height: 200 });
                        else Window.css({ width: Acore.MinWinWidth, height: Acore.MinWinHeight });
                        if (Animated) Window.animate({ top: Top, left: Left, width: Width, height: Height }, Acore.AnimationSpeed, function() { Window.find(".window-content").removeClass("loading") });
                        else Window.css({ top: Top, left: Left, width: Width, height: Height }).find(".window-content").removeClass("loading");
                    } else Window.css({ top: Top, left: Left }).find(".window-content").removeClass("loading");;
                } else U.Growl("Error: Unexpected call to center non-window element. Cannot center element that is not a window.", "error");
            })
        },
        Close: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window && Acore.Windows[Window.GetAcoreId()].HasUnsavedChages) Acore.Confirm({
                    Message: "All unsaved changes will be lost.  Are you sure you want to proceed with closing this window?",
                    Yes: function() { Acore.Close(Window) }
                });
                else if (Window) Acore.Close(Window);
            })
        },
        ContextMenu: function(Event) {
            return this.each(function() {
                // If no other context menus are open and the DOM focus is not upon an input of type text nor textarea
                if ($(".context-menu").length == 0 && $("input.t-input:focus,input[type=text]:focus,textarea:focus").length == 0) {
                    // If no text is highlighted
                    if ((document.getSelection && document.getSelection() == "") || (document.selection && document.selection.createRange().text == "")) {
                        // Prevent the default context menu from popping up
                        Event.preventDefault();
                        // Build context menu into the DOM
                        $("body").append(
                            $("<div/>").css({
                                position: "absolute",
                                top: 0,
                                left: 0,
                                width: "100%",
                                height: "100%",
                                "z-index": 999
                            }).on("click", function() {
                                $(this).remove();
                            }).on("contextmenu", function(Event) {
                                Event.preventDefault();
                                // If menu is too tall to display below the cursor, then display above
                                if (Event.pageY + $(".context-menu").height() > $("#desktop").height()) $(".context-menu").css({ bottom: $("#desktop").height() - Event.pageY, top: "auto" });
                                else $(".context-menu").css({ top: Event.pageY, bottom: "auto" });
                                // If menu is too wide to display to the right of the cursor, then display to the left
                                if (Event.pageX + $(".context-menu").width() > $("#desktop").width()) $(".context-menu").css({ right: $("#desktop").width() - Event.pageX, left: "auto" });
                                else $(".context-menu").css({ left: Event.pageX, right: "auto" });
                            }).append(
                                $(this).addClass("context-menu").css("z-index", "1000")
                            )
                        );
                        // If menu is too tall to display below the cursor, then display above
                        if (Event.pageY + $(".context-menu").height() > $("#desktop").height()) $(".context-menu").css("bottom", $("#desktop").height() - Event.pageY);
                        else $(".context-menu").css("top", Event.pageY);
                        // If menu is too wide to display to the right of the cursor, then display to the left
                        if (Event.pageX + $(".context-menu").width() > $("#desktop").width()) $(".context-menu").css("right", $("#desktop").width() - Event.pageX);
                        else $(".context-menu").css("left", Event.pageX);
                        // If context menu has submenus, enable superfish plugin
                        if ($(".context-menu ul").length) $(".context-menu").Menu();
                        // Apply hover class on mouse over and remove on mouse out
                        $(".context-menu li").each(function() {
                            $(this).on("mouseover", function() {
                                $(this).addClass("hover")
                            })
                        })
                        $(".context-menu li").each(function() {
                            $(this).on("mouseout", function() {
                                $(this).removeClass("hover")
                            })
                        })
                    }
                }
            })
        },
        GetAcoreId: function() {
            // If object is window or task, extrapolate the string literal for the Acore Id and return it
            if ($(this).hasClass("window") || $(this).hasClass("task")) return $(this).attr("id").replace(/^window_/, "");
            else if ($(this).closest(".window").length) return $(this).closest(".window").attr("id").replace(/^window_/, "");
            else return $(".window.active").attr("id").replace(/^window_/, "");
        },
        GetTask: function() {
            // If object is a window, find and return its corresponding task bar item
            if ($(this).hasClass("window") && $(this).attr("id") != null) {
                var task = "#task_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(task).length) return $(task);
            }
            return false;
        },
        GetWindow: function() {
            // If object is a task bar item, find and return its corresponding window
            if (($(this).hasClass("context") || $(this).hasClass("task")) && $(this).attr("id") != null) {
                var window = "#window_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(window).length) return $(window);
            }
            return false;
        },
        InitForms: function() {
            return this.each(function() {
                if (!HomeHealth.IsActive && $("span.green").filter(":contains('(M')").length) $("span.green").filter(":contains('(M0')").remove();
                if ($(".date-picker", this).length) $(".date-picker", this).DatePicker();
                if ($(".time-picker", this).length) $(".time-picker", this).TimePicker();
                if ($(".physician-picker", this).length) $(".physician-picker", this).PhysicianInput();
                if ($(".patient-picker", this).length) $(".patient-picker", this).PatientInput();
                if ($(".zip", this).length && $(".address-state", this).length && $(".address-state", this).length) $(".zip", this).ZipInput($(this).is("form") ? $(this) : $("form", $(this)));
                if ($(".ancillary-button > a", this).length) $(".ancillary-button > a", this).append(
                    $("<span/>").addClass("img icon16 arrow white fr")).append(
                    $("<span/>").addClass("separator fr"));
                // $("input[type=file]", Element).StyledUploader();
                if ($(".multiple-payment", this).length) $(".multiple-payment", this).MultiSelect({ DefaultText: '- Select Payment Source(s) -' });
                if ($(".diagnosis,.icd,.procedureICD,.procedureDiagnosis,.ICDM1024,.diagnosisM1024", this).length) $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis,.ICDM1024,.diagnosisM1024", this).IcdInput();
                if ($(".row", this).find(".phone-short:eq(0)").length) $(".row", this).find(".phone-short:eq(0)").PhoneAutoTab();
                if ($(".floatnum", this).length) $(".floatnum", this).floatnum();
                if ($(".numeric", this).length) $(".numeric", this).numeric();
                if ($(".decimal", this).length) $(".decimal", this).Decimal();
                if ($(".currency", this).length) $(".currency", this).Currency();
                if ($("input, textarea", this).length) $('input, textarea').placeholder();
                if ($(".toggle-container", this).length) $(".toggle-container", this).ContainerToggle();
                if ($(".blood-pressure", this).length) $(".blood-pressure", this).BloodPressure();
                if ($(".template-text", this).length) $(".template-text", this).find("a").on("click", function() { Template.LoadFormTemplates(this, $(this).closest("form")); });
                if ($("select.Templates,.template-text > select", this).length) $("select.Templates,.template-text > select", this).TemplateSelect();
                if ($("textarea[maxcharacters]", this).length) {
                    $("textarea[maxcharacters]", this).after($("<em/>").addClass("characters-remaining"));
                    $("textarea[maxcharacters]", this).limitMaxlength({
                        onEdit: function(remaining) {
                            $(this).siblings(".characters-remaining").text("You have " + remaining + " characters remaining");
                            if (remaining > 0) $(this).css("background-color", "#fff")
                        },
                        onLimit: function() {
                            $(this).css("background-color", "#ecbab3")
                        }
                    })
                }
                if ($("input[type=radio]", this).length) $("input[type=radio]", this).DeselectableRadio();
                // Append red asterisk to required fields
                if ($(".row .required", this).length) {
                    $(".row .required", this).closest(".row").prepend("<span class='required-red abs-right'>*</span>");
                    $("form", this).prepend(
                        $("<div/>", { "class": "abs required-legend", "text": " = Required Field" }).prepend(
                            $("<span/>", { "class": "required-red", "text": "*" })));
                }
                // Set events for select processing
                var disableSelect = function(e) { $(this).blur() };
                $("select", this).on("ProcessingStart", function() {
                    $(this).addClass("processing").prepend(
                        $("<option/>", { text: "Loading...", value: "loading" })
                    ).val("loading").on("focus", disableSelect);
                }).on("ProcessingComplete", function() {
                    $(this).removeClass("processing").off("focus", disableSelect).find("option[value=loading]").remove();
                });
                $("input", this).on("ProcessingStart", function() {
                    $(this).addClass("processing").val("Loading...").prop("disabled", true);
                }).on("ProcessingComplete", function() {
                    $(this).removeClass("processing").prop("disabled", false);
                });
                // Set events for button processing
                $(".buttons a,.button a", this).on("ProcessingStart", function() {
                    $(this).addClass("processing-button").parent().hide().after(
                        $("<li/>", { "class": "processing", text: "Processing..." }).append(
                            $("<span/>").addClass("img processing")));
                }).on("ProcessingComplete", function() {
                    $(this).parent().show().siblings("li.processing").remove();
                });
                // Set checkgoup functionality
                if ($(".checkgroup", this).length) $(".checkgroup", this).Checkgroup();
                // Set standard buttons
                if ($(".buttons .close,.button .close", this).not(".save").length) $(".buttons .close,.button .close", this).not(".save").on("click", function() {
                    $(this).closest(".window").Close();
                    return false;
                });
                if ($(".buttons .next,.button .next", this).not(".save").length) $(".buttons .next,.button .next", this).not(".save").on("click", function() {
                    if ($(this).closest(".window").find(".ui-tabs").length) $(this).closest(".window").find(".ui-tabs-active").next("li").find("a").click();
                });
                if ($(".buttons .back,.button .back", this).not(".save").length) $(".buttons .back,.button .back", this).not(".save").on("click", function() {
                    if ($(this).closest(".window").find(".ui-tabs").length) $(this).closest(".window").find(".ui-tabs-active").prev("li").find("a").click();
                });
                if ($(".buttons .clear,.button .clear", this).not(".save").length) $(".buttons .clear,.button .clear", this).not(".save").on("click", function() {
                    $(this).closest("form").ClearForm();
                    return false;
                });
                if ($(".buttons .download-file, #report-output .buttons .export,.button .download-file", this).length) {
                    $(".buttons .download-file, #report-output .buttons .export,.button .download-file", this).on("click", function(evnt) {
                        var Form = $(this).closest("form"), Button = $(this);
                        Form.off("SubmitSuccess").on("SubmitSuccess", function() {
                            $(".processing-button", this).trigger("ProcessingComplete");
                        });
                        Form.off("SubmitFailure").on("SubmitFailure", function() {
                            $(".processing-button", this).trigger("ProcessingComplete");
                        });
                        Button.trigger("ProcessingStart").closest("form").submit();
                        return false;
                    })
                }
                if ($(".buttons .save,.button .save", this).length) {
                    $(".buttons .save,.button .save", this).on("click", function(evnt) {
                        var Form = $(this).closest("form"), Button = $(this);
                        if (Form.length && (!Button.is(".disabled") && !Button.parent().is(".disabled"))) {
                            U.SetStatusField("SaveStatus", Form);
                            U.IncludeCommandInForm(Form, this, evnt);
                            $(".complete-required", Form).removeClass("required error");
                            if (Button.hasClass("close"))
                                Form.off("SubmitSuccess").on("SubmitSuccess", function() {
                                    $(this).closest(".window").Close();
                                });
                            else if ($(this).hasClass("clear"))
                                Form.off("SubmitSuccess").on("SubmitSuccess", function() {
                                    $(this).ClearForm();
                                    $(".processing-button", this).trigger("ProcessingComplete");
                                });
                            else if ($(this).hasClass("next") && $(this).closest(".ui-tabs").length)
                                Form.off("SubmitSuccess").on("SubmitSuccess", function() {
                                    $(this).closest(".ui-tabs").find(".ui-tabs-active").next("li").find("a").click();
                                });
                            else
                                Form.off("SubmitSuccess").on("SubmitSuccess", function() {
                                    Form.trigger("AjaxSubmitSuccess");
                                    $(".processing-button", this).trigger("ProcessingComplete");
                                });
                            Form.off("SubmitFailure").on("SubmitFailure", function() {
                                $(".processing-button", this).trigger("ProcessingComplete");
                            });

  //Sometimes we need a confirmation for form submittion,we use this to exit from this click submittion and submit based on the confirmation
                        if(Button.hasClass("wait-confirmation")){ return false; }


                            if (Form.valid()) {
                                Button.trigger("ProcessingStart");
                                Form.submit();
                            }
                        }
                        return false;
                    });
                }
                if ($(".buttons .complete,.button .complete", this).length) $(".buttons .complete,.button .complete", this).on("click", function(evnt) {
                    var Button = $(this), Form = Button.closest("form");
                    if (!Button.is(".disabled") && !Button.parent().is(".disabled")) {
                        U.SetStatusField("CompleteStatus", Form);
                        U.IncludeCommandInForm(Form, this, evnt);
                        $(".complete-required", Form).addClass("required");
                        Form.off("SubmitSuccess").on("SubmitSuccess", function() {
                            $(this).closest(".window").Close();
                        }).off("SubmitFailure").on("SubmitFailure", function() {
                            $(".buttons li:hidden a").trigger("ProcessingComplete");
                        });
                        if (Button.closest("form").valid()) Button.trigger("ProcessingStart").closest("form").submit();
                    }
                    return false;
                });
                if ($(".button-with-arrow .new-physician", this).length) $(".button-with-arrow .new-physician", this).on("click", function() {
                    Physician.New(true);
                    return false;
                })
            })
        },
        IsDialog: function() {
            return $(this).GetAcoreId() == Acore.DialogId;
        },
        Load: function(Url, Data, Callback, IsGrid, IsTracked) {
            // If load is on window, set element to content div, detect versitile height
            var Element = $(this).hasClass("window") ? $(".window-content", this) : $(this),
                Window = Element.closest(".window"),
                VersitileHeight = ($(this).hasClass("window") && Acore.Windows[Element.GetAcoreId()].VersitileHeight);
            // Adjust variables is optional variable Data is skipped, but optional variable Callback is specified
            if (typeof Data === "function") Callback = Data, Data = undefined;
            // If object scales to fit, set too small default height/width
            if (VersitileHeight && Acore.Windows[Window.GetAcoreId()].IgnoreMinSize) Window.css({ width: 200, height: 200 });
            else if (VersitileHeight) Window.css({ width: Acore.MinWinWidth, height: Acore.MinWinHeight });
            if (Acore.Windows[Window.GetAcoreId()].Center) Window.css({ top: ($("#desktop").height() - Window.height()) / 2, left: ($("#desktop").width() - Window.width()) / 2 });
            // Empty element, set to loading and make AJAX request for content

            if (IsGrid) {
                var tGrid = Element.is(".t-grid") ? Element : Element.find(".t-grid");
                if (tGrid.length) U.OnTGridDataBinding(tGrid);
                else Element.empty().addClass("loading").addClass("loading-layout-override");
            } else {
                Element.empty().addClass("loading");
            }
            var loadCallback = function(r, t, x) {
                // If there was an error loading page, display error message
                var statusCode = typeof r == "object" ? r.status.toString().match(/[45]0[1034]/g) : r.match(/HTTP ([45]0[1034])/g);
                if (t == "error" || statusCode) {
                    if (statusCode && statusCode[0] == "401") {
                        $(location).attr("href", "SessionExpired");
                    } else {
                        Element.html(U.MessageErrorAjax());
                        if (Acore.Windows[Window.GetAcoreId()].WindowFrame == false && Element.attr("id") != "printview")
                            Element.append(
                                $("<div/>").addClass("abs-bottom").Buttons([{ Text: "Close", Click: function() { Window.Close() } }]));
                        if (VersitileHeight) {
                            Element.find(".error-box").css("top", "4.75em");
                            var Height = Acore.Windows[Element.GetAcoreId()].WindowFrame == false ? 175 : 195;
                            if (Acore.Windows[Element.GetAcoreId()].Center) Window.css({ height: Height, width: 550 }).Center();
                            else Window.Animate({ height: Height, width: 550 }, function() { Element.removeClass("loading").RemoveGridLoadingStyle(IsGrid); });
                        } else Element.removeClass("loading").RemoveGridLoadingStyle(IsGrid);
                    }
                }
                // If No Server Errors
                else {
                    // Run global init script
                    Acore.OnLoad(Element);
                    if (VersitileHeight) Window.SetHeight();
                    else Element.removeClass("loading").RemoveGridLoadingStyle(IsGrid);
                    Acore.InitGridControls(Window);
                    // Run callback function, if specified
                    if (typeof Callback === "function") {
                        try {
                            Callback(r, t, x, Element)
                        } catch (e) {
                            Element.html(U.MessageErrorJS());
                            console.log(e);
                            $.error(e)
                        }
                    }
                    else if (typeof Callback === "string") {
                        try {
                            var callBackFunctionChain = Callback.split(".");
                            var functionHolder = window;
                            if (callBackFunctionChain.length > 0) {
                                for (var funIndex in callBackFunctionChain) {
                                    functionHolder = functionHolder[callBackFunctionChain[funIndex]];
                                }
                                functionHolder(r, t, x, Element);
                            }
                        } catch (e) {
                            Element.html(U.MessageErrorJS());
                            console.log(e);
                            $.error(e)
                        }
                    }
                }
            };
            if (IsTracked) U.LoadTrackedUrl(Url, Data, function(r, t, x) { Element.html(r); loadCallback(r, t, x) });
            else Element.load(Url, Data, loadCallback);
        },
        Maximize: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // Maximize window
                    Window.data({
                        "restore-top": $(this).css("top"),
                        "restore-left": $(this).css("left"),
                        "restore-width": $(this).css("width"),
                        "restore-height": $(this).css("height")
                    }).addClass("maximized").removeClass("minimized");
                    Window.Animate({ top: 0, left: 0, width: "100%", height: "100%" }, function() {
                        Window.trigger("resizestop").find(".img.win.max").removeClass("max").addClass("res");
                        $(window).resize();
                    });
                    // If not the active window, then bring to front
                    if (!Window.hasClass("active")) Window.WinFocus();
                }
            })
        },
        Minimize: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // Minimize window
                if (Window) {
                    Window.data({
                        "restore-top": Window.css("top"),
                        "restore-left": Window.css("left"),
                        "restore-width": Window.css("width"),
                        "restore-height": Window.css("height")
                    });
                    Window.animate({ top: $("#desktop").height(), left: 0, height: 0, width: 0 }, function() {
                        Window.removeClass("active").addClass("minimized").css("z-index", "-1").GetTask().removeClass("active");
                    });
                }
            })
        },
        Rename: function(NewName, TaskRename) {
            return this.each(function() {
                // If pulling name from the DOM
                if (NewName == undefined && $(".wintitle", this).html() != undefined) {
                    NewName = $(".wintitle", this).html();
                    $(".wintitle", this).remove();
                }
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If window has frame, rename at the top of the window
                    if ($(".window-top .title", Window).length) $(".window-top .title", Window).html(NewName);
                    // Rename task bar item
                    if (TaskRename) Window.GetTask().find("a").html(
                            Window.GetTask().find(".img")
                        ).append(NewName).attr("title", Window.GetTask().text());
                }
            })
        },
        Restore: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                // If window is maximized, restore css to old numbers
                if (Window && Window.hasClass("maximized")) {
                    $(this).removeClass("maximized").Animate({
                        top: $(this).data("restore-top"),
                        left: $(this).data("restore-left"),
                        width: $(this).data("restore-width"),
                        height: $(this).data("restore-height")
                    }, function() {
                        Window.trigger("resizestop").find(".img.win.res").removeClass("res").addClass("max");
                        $(window).resize();
                    });
                }
            })
        },
        SetHeight: function(Animated, WidthToBecome) {
            Animated = Animated == false ? false : Acore.Animated;
            return this.each(function() {
                // Set variables
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window"),
                    Content = Window.find(".window-content"),
                    Width = Acore.Windows[Window.GetAcoreId()].Width ? Acore.Windows[Window.GetAcoreId()].Width : Acore.DefaultWinWidth,
                    TempWidth = Window.css("width"),
                    TempHeight = Acore.Windows[Window.GetAcoreId()].IgnoreMinSize ? 200 : Acore.MinWinHeight;
                if (!Window.hasClass("maximized")) {
                    // Shrink window to smaller size
                    if (Acore.Windows[Window.GetAcoreId()].IgnoreMinSize) Window.css({ width: 200, height: 200 });
                    else Window.css({ width: Acore.MinWinWidth, height: Acore.MinWinHeight });
                    if (Acore.Windows[Window.GetAcoreId()].Center) Window.css({ top: ($("#desktop").height() - Window.height()) / 2, left: ($("#desktop").width() - Window.width()) / 2 });
                    // Temporarily adjust window to calculate height, then revert
                    Window.css({ width: Width, height: 50 });
                    Content.removeClass("loading");
                    var Height = Content.prop("scrollHeight");
                    Content.addClass("loading");
                    Window.css({ width: TempWidth, height: TempHeight });
                    // Add to content height for window element heights
                    if (Acore.Windows[Window.GetAcoreId()].WindowFrame == false) Height += 9;
                    else if (Acore.Windows[Window.GetAcoreId()].StatusBar == false) Height += 32;
                    else Height += 48;
                    // If height is larger than desktop, set to default
                    if (Height > $("#desktop").height()) Height = Acore.DefaultWinHeight;
                    // If window is placed out of desktop boundries, move it up some
                    var Top = Window.position().top;
                    if (Top + Height > $("#desktop").height()) Top = $("#desktop").height() - Height - 3;
                    // If centered, send to center function, resize window sizing
                    if (Acore.Windows[Window.GetAcoreId()].Center) Window.css({ width: Width, height: Height }).Center(Animated);
                    else if (Animated) Window.animate({ width: WidthToBecome || Width, height: Height, top: Top }, Acore.AnimationSpeed, function() { Content.removeClass("loading") });
                    else Window.css({ width: WidthToBecome || Width, height: Height, top: Top }).find(".window-content").removeClass("loading");
                } else Window.find(".window-content").removeClass("loading");
            })
        },
        RemoveGridLoadingStyle: function(isGrid) {
            if (isGrid) {
                this.removeClass("loading-layout-override");
            }
        },
        Status: function(Message) {
            return this.each(function() {
                // Find window and status bar
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window"),
                        StatusBar = Window.find(".window-bottom");
                // Set status bar message
                if (StatusBar) StatusBar.text(Message);
            })
        },
        WinFocus: function() {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If mobile, minimize all windows
                    if (Acore.Mobile) $(".window").Minimize();
                    // Clear current active window
                    $(".window.active", "#desktop").removeClass("active");
                    $(".active", "#task").removeClass("active");
                    // Hide menus
                    $(".ui-menu").hide();
                    // Make current window active
                    if (Window.css("z-index") <= Acore.MaxWindows) Window.css("z-index", Acore.OpenWindows);
                    Window.addClass("active");
                    Window.GetTask().addClass("active");
                    if (Window.hasClass("minimized")) {
                        if (Acore.Animated) Window.removeClass("minimized").animate({
                            top: Window.data("restore-top"),
                            left: Window.data("restore-left"),
                            height: Window.data("restore-height"),
                            width: Window.data("restore-width")
                        }, Acore.AnimationSpeed);
                        else Window.css({
                            top: Window.data("restore-top"),
                            left: Window.data("restore-left"),
                            height: Window.data("restore-height"),
                            width: Window.data("restore-width")
                        }).removeClass("minimized");
                    }
                    // Recalculate other windows' z-index
                    $(".window:not(.active):not(.top-window)").each(function() {
                        if (!Acore.Windows[$(this).GetAcoreId()].Modal && $(this).css("z-index") >= $(".window.active").css("z-index")) $(this).css("z-index", parseInt($(this).css("z-index") - 1));
                    });
                }
            })
        },
        PrependToTitle: function(prefix) {
            return this.each(function() {
                // Find window
                var Window = $(this).hasClass("window") ? $(this) : $(this).closest(".window");
                if (Window) {
                    // If window has frame, rename at the top of the window
                    if ($(".window-top .title", Window).length) $(".window-top .title", Window).html(prefix + $(".window-top .title", Window).html());
                }
            })
        },
        Layout: function(Options) {
            if (!Options) Options = {
                closable: false,
                west: {
                    paneSelector: ".ui-layout-west",
                    size: 200,
                    minSize: 100,
                    maxSize: 300
                }
            };
            if ($(this).length) $(this).layout(Options);
        },
        GetTotalHeight: function() {
            var totalHeight = 0;
            this.children().each(function() {
                var height = $(this).GetTotalHeight();
                totalHeight += height != 0 ? height : $(this).outerHeight();
            })
            return totalHeight
        }
    })
})(jQuery);
