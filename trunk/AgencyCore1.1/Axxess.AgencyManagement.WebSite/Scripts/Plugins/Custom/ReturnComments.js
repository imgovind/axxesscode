(function($) {
    $.extend($.fn, {
        //  Arguments:
        //      EventId         Id of the note that the return reason will belong to
        //      PatientId       Related patient's id
        //      EpisodeId       Related episode's id
        //      Comment         Comment to be inserted 
        //      UrlPrefix       Area prefix for any urls used by this plugin
        //      Action          Action to be performed by the plugin, used only by refresh currently
        //      CloseFunction   The function to be called upon closing the return reason
        //      ReturnFunction  The function to be called when returning a document
        ReturnComments: function(Arguments) {
            if (!Arguments.UrlPrefix) Arguments.UrlPrefix = "";
            return this.each(function() {
                if (!$(this).hasClass("return-comments-container")) {
                    var Container = $(this), ButtonArray = new Array();
                    if (typeof Arguments.ReturnFunction == "function") ButtonArray.push({
                        Text: "Return",
                        Class: "save",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            var textAreaValue = $("textarea", Container).val();
                            if (textAreaValue.length) {
                                $(this).trigger("ProcessingStart");
                                Arguments.ReturnFunction(textAreaValue);
                            } else U.Growl("Please give a reason for returning this document.", "error");
                        }
                    });
                    else ButtonArray.push({
                        Text: "Add Comment",
                        Class: "save",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("textarea", Container).val().length) {
                                var button = $(this).trigger("ProcessingStart");
                                U.PostUrl(Arguments.UrlPrefix + "/Schedule/AddReturnReason", { eventId: Container.attr("EventId"), episodeId: Container.attr("EpisodeId"), patientId: Container.attr("PatientId"), comment: $("textarea", Container).val() }, function(Result) {
                                    button.trigger("ProcessingComplete");
                                    U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                    if (Result.isSuccessful) {
                                        Container.ReturnComments({ Action: "Refresh", UrlPrefix: Arguments.UrlPrefix });
                                        $("textarea", Container).val("");
                                        Refresh.CenterQAMySchedule(Result);
                                    }
                                })
                            } else U.Growl("Please enter your comments that you would like to add.", "error");
                        }
                    });
                    if (Container.hasClass("main")) ButtonArray.push({
                        Text: "Close",
                        Click: Arguments.CloseFunction
                    });
                    Container.addClass("return-comments-container").attr({ EventId: Arguments.EventId, EpisodeId: Arguments.EpisodeId, PatientId: Arguments.PatientId }).append(
                        $("<div/>", { "class": "return-comments" }).append("<div/>")).append(
                        $("<textarea/>", { "class": "tall" }).prop("value", Arguments.Comment)).append(
                        $("<div/>").Buttons(ButtonArray)).ReturnComments({ Action: "Refresh", UrlPrefix: Arguments.UrlPrefix });
                    
                } else if (Arguments.Action == "Refresh") {
                    var Container = $(this), commentCotentSection = $(".return-comments", Container);
                    commentCotentSection.addClass("busy");
                    U.PostUrl(Arguments.UrlPrefix + "/Schedule/ReturnReason", { eventId: Container.attr("EventId"), episodeId: Container.attr("EpisodeId"), patientId: Container.attr("PatientId") }, function (Result) {
                        commentCotentSection.removeClass("busy");
                        commentCotentSection.html(Result.errorMessage).scrollTop(commentCotentSection.prop("scrollHeight")).prepend("<div/>");
                        $(".edit-controls", commentCotentSection).each(function () {
                            var CommentId = $(this).text(), Element = $(this).closest(".main");
                            $(this).html(
                                $("<span/>").addClass("img icon22 edit").on("click", function() {
                                    UserInterface.EditReturnComments(CommentId, $(this).parent().next().next().next().text(), Element, Arguments.UrlPrefix)
                                })
                            ).append(
                                $("<span/>").addClass("img icon22 delete").on("click", function() {
                                    U.Delete("Return comment", Arguments.UrlPrefix + "/Schedule/DeleteReturnReason", { id: CommentId }, function() { Container.ReturnComments({ Action: "Refresh", UrlPrefix: Arguments.UrlPrefix }); });
                                })
                            )
                        })
                        }, function (Result) {
                            var title = Result.statusText || "Request Error",
                                message = Result.responseJSON && Result.responseJSON.errorMessage ? Result.responseJSON.errorMessage : "An error has been detected while procuring data. Please check your connection and try again, if problem persists, contact Axxess for assistance.",
                                window = Container.closest(".window-content").html(U.MessageError(title, message, [{ Text: "Close", Click: function(){ window.Close(); } }]).css({ margin: "25px", position: "static" }))
                                    .css("background", "#fff").parent();
                            window.SetHeight(Acore.Animated, 550);
                    })
                }
            })
        }
    })
})(jQuery);