var Widgets = {
    Columns:        ".widgets",             // Widget column class selector
    Handle:         ".widget-head",         // Moveable widget handler class selector
    NumPerCol:      3,                      // Maximum number of widgets per column
    PlaceHolder:    "widget-place-holder",   // Toggle class for dragging placeholder
    Stationary:     ".stationary",          // Stationary widget class selector
    Widget:         ".widget",              // Widget class selector
    Init: function() {
        $("li" + Widgets.Widget + ":not(" + Widgets.Stationary + ")", Widgets.Columns).disableSelection().find(Widgets.Handle)
            .css({
                "cursor":       "move"
            }).on({
                "mousedown":    Widgets.MouseDown,
                "mouseup":      Widgets.MouseUp
            });
        $(Widgets.Columns).sortable({
            items:                  $("li" + Widgets.Widget + ":not(" + Widgets.Stationary + ")", Widgets.Columns),
            connectWith:            $(Widgets.Columns),
            handle:                 Widgets.Handle,
            placeholder:            Widgets.PlaceHolder,
            forcePlaceholderSize:   true,
            revert:                 300,
            delay:                  100,
            opacity:                0.8,
            containment:            "document",
            start:                  Widgets.Start,
            stop:                   Widgets.Stop
        });

        $(Widgets.Widget + " select").on('mousedown.ui-disableSelection selectstart.ui-disableSelection', function(event) {
            event.stopImmediatePropagation();
        })
    },
    MouseDown: function() {
        $("li" + Widgets.Widget + ":not(" + Widgets.Stationary + ")", Widgets.Columns).css({ width: "" });
        $(this).parent().css({ width: $(this).parent().width() + "px" });
    },
    MouseUp: function() {
        $(this).parent().hasClass("dragging") ? $(Widgets.Columns).sortable("disable") : $(this).parent().css({ width: "" });
    },
    Start: function(e, ui) {
        $(ui.helper).addClass("dragging");
    },
    Stop: function(e, ui) {
        $(ui.item).css({ width: "" }).removeClass("dragging");
        $(Widgets.Columns).sortable("enable");
        var col = $(ui.item).closest(Widgets.Columns);
        while (col.find(Widgets.Widget).length > Widgets.NumPerCol) {
            if (col.index() + 1 == $(Widgets.Columns).length) move = $(Widgets.Columns).first();
            else move = col.next();
            if ($(Widgets.Stationary, move).length) $(Widgets.Stationary, move).last().after(col.find(Widgets.Widget).last());
            else move.prepend(col.find(Widgets.Widget).last());
            col = move;
        }
    }
};