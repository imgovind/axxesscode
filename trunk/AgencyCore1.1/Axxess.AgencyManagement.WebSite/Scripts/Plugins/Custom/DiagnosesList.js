(function ($) {
    var diagnosesListMethods = {
        // Add diagnosis item to list
        add: function () {
            var e = $(this), data = e.data("DiagnosesList");
            if ($(".diagnosis-item", e).length < 26) e.append($("<li/>").DiagnosisItem(data));
        },
        // Build list
        build: function () {
            var e = $(this), data = e.data("DiagnosesList");
            // Build Title Row
            e.addClass("diagnoses-list").attr("assessment", data.Assessment).append(
                diagnosesListMethods.buildRow({
                    cssClass: "ac strong",
                    content: data.isOasis ? "Column 1" : "Principal Diagnosis"
                }, {
                    cssClass: "ac strong",
                    content: data.isOasis ? "Column 2" : "ICD-9-C M Code"
                }, {
                    cssClass: "ac strong",
                    content: data.isOasis ? "Column 3" : "O/E"
                }, {
                    cssClass: "ac strong",
                    content: data.isOasis ? "Column 4" : "Date"
                }));
            // If OASIS, Build Instruction Row
            if (data.isOasis) e.append(
                diagnosesListMethods.buildRow({
                    label: "Primary Diagnosis",
                    oasis: "M1020",
                    inputId: data.Assessment + "_M1020PrimaryDiagnosis",
                    content: "Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided."
                }, {
                    label: "ICD-9-C M/ Symptom Control Rating",
                    inputId: data.Assessment + "_M1020ICD9M",
                    content: "ICD-9-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses."
                }, {
                    label: "Description/ ICD-9-C M",
                    oasis: "M1024",
                    inputId: data.Assessment + "_M1024ICD9MA3",
                    note: "(V- or E-codes NOT allowed)",
                    content: "Complete if a V-code is assigned under certain circumstances to Column 2 in place of a case mix diagnosis."
                }, {
                    label: "Description/ ICD-9-C M",
                    oasis: "M1024",
                    inputId: data.Assessment + "_M1024ICD9MA4",
                    note: "(V- or E-codes NOT allowed)",
                    content: "Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis that is a multiple coding situation (e.g., a manifestation code)."
                }));
            // Add primary diagnosis row (M1020)
            e.DiagnosesList("add");
            // Build secondary title row
            if (data.isOasis) e.append(
                diagnosesListMethods.buildRow({
                    label: "Other Diagnoses",
                    oasis: "M1022",
                    inputId: data.Assessment + "_M1022PrimaryDiagnosis1"
                }, {}, {}, {}, "oasis_m1022"));
            else e.append(
                diagnosesListMethods.buildRow({
                    cssClass: "ac strong",
                    content: "Other Diagnosis"
                }, {
                    cssClass: "ac strong",
                    content: "ICD-9-C M Code"
                }, {
                    cssClass: "ac strong",
                    content: "O/E"
                }, {
                    cssClass: "ac strong",
                    content: "Date"
                }, "oasis_m1022"));
            // Cycle through adding proper number of other diagnosis lines (M1022)
            for (var i = 0; i < data.rowCount; i++) e.DiagnosesList("add");
            // Build button to add new diagnosis
            e.append(
                $("<li/>").addClass("oasis-diagnosis diagnosis-title ac").append(
                    $("<div/>").addClass("button").append(
                        $("<a/>").text("Add Another Diagnosis").on("click", function () {
                            $(this).closest(".diagnoses-list").DiagnosesList("add");
                            $(this).closest(".diagnoses-list").DiagnosesList("reorder");
                        }))));
            e.sortable({
                items: ".diagnosis-input",
                handle: ".handle.sortable",
                stop: diagnosesListMethods.reorder
            });
            return e;
        },
        buildColumn: function (options) {
            var e = $("<div/>").append($("<div/>").addClass("row no-input" + (options.cssClass ? " " + options.cssClass : "")));
            if (options.content) $(".row", e).append($("<p/>").text(options.content));
            if (options.label) {
                $(".row", e).append($("<label/>", { "class": "strong", text: (options.oasis ? " " : "") + options.label, "for": options.inputId }));
                if (options.oasis) $("label", e).prepend(
                    $("<a/>", { "class": "green", title: "More Information about " + options.oasis, text: "(" + options.oasis + ")" }).on("click", function () {
                        Oasis.Help("M1020");
                    }));
            }
            if (options.note) $(".row", e).append($("<em/>").text(options.note));
            return e;
        },
        buildRow: function (column1, column2, column3, column4, cssClass) {
            return $("<li/>").append(
                $("<div/>").addClass("wide-column").append(
                    $("<div/>").addClass("inline-row four-wide" + (cssClass ? " " + cssClass : "")).append(
                        diagnosesListMethods.buildColumn(column1)).append(
                        diagnosesListMethods.buildColumn(column2)).append(
                        diagnosesListMethods.buildColumn(column3)).append(
                        diagnosesListMethods.buildColumn(column4))));
        },
        init: function (options) {
            this.each(function () {
                if (typeof options == "undefined" && $(this).attr("data")) options = eval($(this).attr("data"));
                var e = $(this),                                // DOM Pointer to this Diagnoses List
                    rowCount = 1,                               // Number of rows in this List
                    isOasis = options.Assessment != "Edit485";  // Whether it is a Plan of Care or OASIS
                // Calculate row count
                $.each(options, function (index, value) {
                    if (index.match(/^_M1022PrimaryDiagnosis[0-9]*$/)) rowCount = parseInt(index.replace(/^_M1022PrimaryDiagnosis/, ""));
                });
                // Save data in the DOM
                e.data("DiagnosesList", $.extend({ rowCount: ++rowCount, isOasis: isOasis, list: e }, options));
                // Build DOM
                e.DiagnosesList("build").DiagnosesList("reorder");
                return e;
            })
        },
        reorder: function () {
            var e = $(this), data = e.data("DiagnosesList");
            // If last diagnosis has data, add a new row
            if ($(".diagnosis-item:last .diagnosis", e).val().length) e.DiagnosesList("add");
            // Fix all names/ids/labels on diagnosis items
            $(".diagnosis-input", e).DiagnosisItem("setNames");
            // Ensure title rows are in the right order
            $(".diagnosis-input:eq(1)", e).before($(".oasis_m1022", e));
            $(".diagnosis-input:last", e).after($(".diagnosis-title:last", e));
            // Set only last diagnosis to not sort and set change function to add a new row
            $(".diagnosis-item", e).find(".handle").addClass("sortable").find(":input").off("keyup");
            $(".diagnosis-item:last", e).find(".handle").removeClass("sortable").find(":input").on("keyup", function () {
                if ($(this).val()) {
                    e.DiagnosesList("add");
                    e.DiagnosesList("reorder");
                }
            });
            // Set only last diagnosis to not have up/down arrows and delete icon
            $(".arrow.up,.arrow.down,.delete", e).show();
            $(".arrow.up:last,.arrow.down:last,.delete:last,.sortable:last .arrow.down,.arrow.up:first", e).hide();
            return e;
        }
    };
    $.fn.DiagnosesList = function (method) {
        if (diagnosesListMethods[method]) return diagnosesListMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" || !method) return diagnosesListMethods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist for the Diagnoses List Plugin.");
    };
})(jQuery);
(function($) {
    var diagnosisItemMethods = {
        build: function(data) {
            var e = $(this),
                order = data.list.children(".diagnosis-input").length,
                alpha = String.fromCharCode(97 + order),
                severityOptions = { 0: "00", 1: "01", 2: "02", 3: "03", 4: "04" },
                oeOptions = { Exacerbation: "1", Onset: "2" },
                primaryDiagnosis = data[order > 0 ? "_M1022PrimaryDiagnosis" + String(order) : "_M1020PrimaryDiagnosis"],
                primaryIcd = data[order > 0 ? "_M1022ICD9M" + String(order) : "_M1020ICD9M"],
                paymentDiagnosis3 = data["_M1024PaymentDiagnoses" + String.fromCharCode(65 + order) + "3"],
                paymentIcd3 = data["_M1024ICD9M" + String.fromCharCode(65 + order) + "3"],
                paymentDiagnosis4 = data["_M1024PaymentDiagnoses" + String.fromCharCode(65 + order) + "4"],
                paymentIcd4 = data["_M1024ICD9M" + String.fromCharCode(65 + order) + "4"],
                severity = data[order > 0 ? "_M1022OtherDiagnose" + String(order) + "Rating" : "_M1020SymptomControlRating"],
                oe = data["_485ExacerbationOrOnsetPrimaryDiagnosis" + (order > 0 ? String(order) : "")],
                date = data[order > 0 ? "_M1022PrimaryDiagnosis" + String(order) + "Date" : "_M1020PrimaryDiagnosisDate"];
            e.addClass("diagnosis-input diagnosis-item" + (data.isOasis ? " oasis-diagnosis" : "")).append(
                $("<div/>").addClass("wide-column handle").append(
                    $("<span/>").addClass("img icon22 delete")).append(
                    $("<div/>").addClass("inline-row four-wide").append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row"))).append(
                        $("<div/>").append(
                            $("<div/>").addClass("row")))).append(
                    $("<span/>").addClass("img icon22 arrow up")).append(
                    $("<span/>").addClass("img icon22 arrow down")));
            // If OASIS, build layout accordingly
            if (data.isOasis) {
                $(".row:eq(0)", e).append(
                    $("<label/>").addClass("fl").text("Diagnosis").prepend(
                        $("<span/>").addClass("alphali"))).append(
                    $("<div/>").addClass("fr").append(
                        $("<input/>").addClass("diagnosis short").val(primaryDiagnosis).attr({ type: "text", placeholder: "Diagnosis" })));
                $(".row:eq(1)", e).append(
                    $("<label/>").addClass("fl").text("Code")).append(
                    $("<div/>").addClass("fr").append(
                        $("<input/>").addClass("icd shorter").val(primaryIcd).attr({ type: "text", placeholder: "ICD-9" })));
                $(".row:eq(2)", e).append(
                    $("<label/>").addClass("fl").append(
                        $("<span/>").addClass("alphali"))).append(
                    $("<div/>").addClass("fr oasis").append(
                        $("<input/>").addClass("diagnosisM1024 icdtext short").val(paymentDiagnosis3).attr({ type: "text", placeholder: "Diagnosis" })).append(
                        $("<input/>").addClass("ICDM1024 icdwidth shorter").val(paymentIcd3).attr({ type: "text", placeholder: "ICD-9" })));
                $(".row:eq(3)", e).append(
                    $("<label/>").addClass("fl").append(
                        $("<span/>").addClass("alphali"))).append(
                    $("<div/>").addClass("fr oasis").append(
                        $("<input/>").addClass("diagnosisM1024 icdtext short").val(paymentDiagnosis4).attr({ type: "text", placeholder: "Diagnosis" })).append(
                        $("<input/>").addClass("ICDM1024 icdwidth shorter").val(paymentIcd4).attr({ type: "text", placeholder: "ICD-9" })));
                $(".row:eq(4)", e).append(
                    $("<label/>").addClass("fl").text("Severity")).append(
                    $("<div/>").addClass("fr").append(
                        $("<select/>").addClass("severity short").append(
                            $("<option/>", { value: "", text: "" }))));
                $(".row:eq(5)", e).append(
                    $("<label/>").addClass("fl").text("O/E")).append(
                    $("<div/>").addClass("fr").append(
                        $("<select/>", { "class": "oe short" }).append(
                            $("<option/>", { value: "", text: "" }))));
                $(".row:eq(6)", e).append(
                    $("<label/>").addClass("fl").text("Date")).append(
                    $("<div/>").addClass("fr").append(
                        $("<input/>").addClass("date short").val(date).attr("type", "text")));
                $(".row:eq(7)", e).append(
                    $("<div/>").addClass("fr oasis").append(
                        $("<div/>").addClass("button oasis-help").append(
                            $("<a/>", { status: "More Information about M1020", text: "?" }).on("click", function() {
                                Oasis.Help("M1020")
                            }))));
                // Populate severity select
                $.each(severityOptions, function(i, v) {
                    $(".severity", e).append(
                        $("<option/>", { text: i, value: v }).prop("selected", severity === v))
                });
                // Set event to disable severity if v-code is used
                $(".icd", e).keyup(function() {
                    if (/^v/i.test($(this).val())) $(".severity", e).attr("disabled", true);
                    else $(".severity", e).attr("disabled", false);
                });
                // Populate list letter
                $(".alphali", e).text(alpha + ".")
                // If 485 build layout accordingly
            } else {
                $(".row:eq(0)", e).append(
                    $("<input/>").addClass("diagnosis").val(primaryDiagnosis).attr({ type: "text", placeholder: "Diagnosis" }));
                $(".row:eq(1)", e).append(
                    $("<input/>").addClass("icd").val(primaryIcd).attr({ type: "text", placeholder: "ICD-9" }));
                $(".row:eq(2)", e).append(
                    $("<select/>").addClass("oe").append(
                        $("<option/>", { value: "", text: "" })));
                $(".row:eq(3)", e).append(
                    $("<input/>").addClass("date").val(date).attr("type", "text"));
            }
            if (primaryIcd) $(".diagnosis-colwidth:eq(1) input", e).after(
                $("<a/>", { "class": "img icon16 book", href: "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + primaryIcd + "&informationRecipient.languageCode.c=en", target: "_blank" }));
            // Enable diagnosis/ICD inputs for auto-complete functionality
            $(".diagnosis,.icd,.diagnosisM1024,.ICDM1024", e).IcdInput();
            // Enable date input for date picker functionality
            $(".date", e).DatePicker();
            // Populate O/E select
            $.each(oeOptions, function(i, v) {
                $(".oe", e).append(
                    $("<option/>", { text: i, value: v }).prop("selected", oe === v))
            });
            // Set events for up and down arrow icons
            $(".arrow", e).on("click", function() {
                var NewPosition = e.closest(".diagnoses-list").children(".diagnosis-item").index(e) + ($(this).hasClass("up") ? -1 : 2);
                e.closest(".diagnoses-list").find(".diagnosis-item").eq(NewPosition).before(e);
                e.closest(".diagnoses-list").DiagnosesList("reorder");
            });
            // Set event for delete icon click
            $(".delete", e).on("click", function() {
                var Item = $(this).closest(".diagnosis-item"),
                    List = $(this).closest(".diagnoses-list"),
                    Order = List.children(".diagnosis-input").index(Item);
                Acore.Confirm({
                    Message: "Are you sure you want to delete this diagnosis?",
                    Yes: function() {
                        if (Order == 0) {
                            $(".diagnosis-down", Item).click();
                            if ($(".diagnosis-item", List).length == 2) List.DiagnosesList("Add");
                        }
                        Item.remove();
                        if ($(".diagnosis-item", List).length < 26) $(".diagnosis-title:last", List).show();
                        List.DiagnosesList("reorder");
                    }
                })
            })
        },
        // Initialization of plugin
        init: function(data) {
            return this.each(function() {
                $(this).DiagnosisItem("build", data);
            })
        },
        setNames: function() {
            this.each(function() {
                var e = $(this),
                    order = e.closest(".diagnoses-list").children(".diagnosis-item").index(e),
                    list = e.closest(".diagnoses-list"),
                    data = list.data("DiagnosesList"),
                    assessment = data.Assessment,
                    isOasis = data.isOasis,
                    alpha = String.fromCharCode(97 + order),
                    primaryDiagnosis = (isOasis ? assessment + "_" : "") + "M102" + (order > 0 ? "2" : "0") + "PrimaryDiagnosis" + (order > 0 ? String(order) : ""),
                    primaryIcd = (isOasis ? assessment + "_" : "") + "M102" + (order > 0 ? "2" : "0") + "ICD9M" + (order > 0 ? String(order) : ""),
                    paymentDiagnosis3 = (isOasis ? assessment + "_" : "") + "M1024PaymentDiagnoses" + String.fromCharCode(65 + order) + "3",
                    paymentIcd3 = (isOasis ? assessment + "_" : "") + "M1024ICD9M" + String.fromCharCode(65 + order) + "3",
                    paymentDiagnosis4 = (isOasis ? assessment + "_" : "") + "M1024PaymentDiagnoses" + String.fromCharCode(65 + order) + "4",
                    paymentIcd4 = (isOasis ? assessment + "_" : "") + "M1024ICD9M" + String.fromCharCode(65 + order) + "4",
                    severity = (isOasis ? assessment + "_" : "") + "M102" + (order > 0 ? "2OtherDiagnose" + String(order) + "Rating" : "0SymptomControlRating"),
                    oe = (isOasis ? assessment + "_" : "") + "485ExacerbationOrOnsetPrimaryDiagnosis" + (order > 0 ? String(order) : ""),
                    date = (isOasis ? assessment + "_" : "") + "M102" + (order > 0 ? "2" : "0") + "PrimaryDiagnosis" + (order > 0 ? String(order) : "") + "Date";
                $(".diagnosis", e).attr({ id: primaryDiagnosis, name: primaryDiagnosis });
                $(".icd", e).attr({ id: primaryIcd, name: primaryIcd });
                $(".oe", e).attr({ id: oe, name: oe });
                $(".date", e).DatePicker("Rename", date);
                if (isOasis) {
                    $(".alphali", e).text(alpha + ".");
                    $(".diagnosis", e).prev("label").attr("for", primaryDiagnosis);
                    $(".icd", e).prev("label").attr("for", primaryIcd);
                    $(".diagnosisM1024:first", e).attr({ id: paymentDiagnosis3, name: paymentDiagnosis3 });
                    $(".ICDM1024:first", e).attr({ id: paymentIcd3, name: paymentIcd3 });
                    $(".diagnosisM1024:last", e).attr({ id: paymentDiagnosis4, name: paymentDiagnosis4 });
                    $(".ICDM1024:last", e).attr({ id: paymentIcd4, name: paymentIcd4 });
                    $(".severity", e).attr({ id: severity, name: severity }).prev("label").attr("for", severity);
                    $(".oe", e).prev("label").attr("for", oe);
                    $(".date", e).prev("label").attr("for", date);
                }
                return e;
            })
        }
    };
    $.fn.DiagnosisItem = function(method) {
        if (diagnosisItemMethods[method]) return diagnosisItemMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" || !method) return diagnosisItemMethods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist for the Diagnoses Item Plugin.");
    };
})(jQuery);