(function ($) {
    var woundChartMethods = {
        add: function (x, y) {
            var e = $(this), data = e.data("WoundChart"), woundNumber = ++data.woundCount, woundInput = { Number: woundNumber, x: Math.round(x), y: Math.round(y) };
            if (data.woundCount < 100) Acore.Modal({
                    Url: data.controller + "/WoundAdd",
                    Input: $.extend({ Id: data.id, PatientId: data.patientId, Type: data.type }, woundInput),
                    OnLoad: function(r, t, xml, modelElement) {
                        WoundCare.InputInit(modelElement);
                        $("form", modelElement).Validate({
                            Success: function() {
                                $(".patient-map", e).append($("<div/>").WoundMarker(woundInput));
                            }
                        });
                    },
                    Width: 650
                });
            else U.Growl("Unable to add wound because you have too many wounds. There is a limit of 99 wounds. Please remove wounds in order to add more.", "error");
            return e;
        },
        addMode: function () {
            var e = $(this).closest(".wound-chart");
            $(".buttons .selected", e).removeClass("selected");
            $(".add-mode").addClass("selected");
            $("> em", e).text("Click a spot on the body where you would like to chart a new wound.");
            e.removeClass("delete-mode edit-mode move-mode").addClass("add-mode").find(".patient-map").removeClass("busy").off("click").on("click", function (event) {
                var x = parseInt(event.pageX - $(".patient-map", e).offset().left - 10),
                    y = parseInt(event.pageY - $(".patient-map", e).offset().top - 35);
                e.WoundChart("add", x, y);
            });
            return e;
        },
        build: function () {
            var e = $(this),
                data = e.data("WoundChart");
            e.addClass("wound-chart").append(
                $("<div/>").addClass("ac").append(
                    $("<ul/>").addClass("buttons continuous").append(
                        $("<li/>").addClass("edit-mode").append(
                            $("<a/>").text("Edit").on("click", function () {
                                e.WoundChart("editMode");
                            }))).append(
                        $("<li/>").addClass("add-mode").append(
                            $("<a/>").text("Add").on("click", function () {
                                e.WoundChart("addMode");
                            }))).append(
                        $("<li/>").addClass("move-mode").append(
                            $("<a/>").text("Move").on("click", function () {
                                e.WoundChart("moveMode");
                            }))).append(
                        $("<li/>").addClass("delete-mode").append(
                            $("<a/>").text("Delete").on("click", function () {
                                e.WoundChart("deleteMode");
                            }))))).append(
                $("<em/>").addClass("ac")).append(
                $("<div/>").addClass("patient-map").append(
                    $("<em/>").text("Saving...").prepend(
                        $("<span/>").addClass("img icon16 processing"))));
            for (var i = 0; i < data.woundCount; i++) $(".patient-map", e).append($("<div/>").WoundMarker(data.wounds[i]));
            if (data.woundCount) e.WoundChart("editMode");
            else e.WoundChart("addMode");
            return e;
        },
        deleteMode: function () {
            var e = $(this).closest(".wound-chart");
            $(".buttons .selected", e).removeClass("selected");
            $(".delete-mode").addClass("selected");
            $("> em", e).text("Click on the wound you wish to delete.");
            e.removeClass("add-mode edit-mode move-mode").addClass("delete-mode").find(".patient-map").removeClass("busy").off("click").find(".wound-marker").draggable("disable").off("click").on("click", function () {
                var wound = $(this);
                Acore.Confirm({
                    Message: "Are you sure you want to delete this wound?",
                    Yes: function () { wound.WoundMarker("delete") }
                })
            });
            return e;
        },
        editMode: function () {
            var e = $(this);
            $(".buttons .selected", e).removeClass("selected");
            $(".edit-mode").addClass("selected");
            $("> em", e).text("Click on a wound marker to edit that wound or select another action on the buttons below.");
            e.removeClass("add-mode delete-mode move-mode").addClass("edit-mode").find(".patient-map").removeClass("busy").off("click").find(".wound-marker").draggable("disable").off("click").on("click", function () { $(this).WoundMarker("edit") });
            $(".button")
            return e;
        },
        fixNumbers: function () {
            var e = $(this), data = e.data("WoundChart"), woundNumber = 0;
            $(".wound-marker", e).each(function () {
                var wound = $(this), woundData = wound.data("WoundMarker");
                wound.text(++woundNumber).data("WoundMarker", $.extend(woundData, { number: woundNumber }));
            });
            e.data("WoundChart", $.extend(data, { woundCount: woundNumber }));
            return e;
        },
        init: function (options) {
            return this.each(function () {
                var e = $(this), woundCount = options.wounds.length;
                e.data("WoundChart", $.extend({ woundCount: woundCount, chart: e }, options));
                e.WoundChart("build");
            })
        },
        moveMode: function () {
            var e = $(this).closest(".wound-chart");
            $(".buttons .selected", e).removeClass("selected");
            $(".move-mode").addClass("selected");
            $("> em", e).text("Drag and drop a wound to a different location.");
            e.addClass("move-mode").find(".wound-marker").draggable("enable").off("click").on("click", function () { return false });
            return e;
        }
    };
    $.fn.WoundChart = function (method) {
        if (woundChartMethods[method]) return woundChartMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" || !method) return woundChartMethods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist for the Wound Chart Plugin.");
    };
})(jQuery);
(function($) {
    var woundMarkerMethods = {
        build: function(options) {
            return $(this).addClass("wound-marker").css({ left: options.x, top: options.y }).text(options.Number).draggable({
                containment: "parent",
                stop: function (event, ui) {
                    var e = $(this)
                    e.WoundMarker("move", ui.position.left, ui.position.top);
                }
            })
        },
        "delete": function() {
            var e = $(this), chart = e.closest(".wound-chart"), data = chart.data("WoundChart");
            e.closest(".patient-map").addClass("busy");
            U.PostUrl(data.controller + "/WoundDelete", { Id: data.id, PatientId: data.patientId, WoundNumber: $(this).text(), WoundCount: data.woundCount }, function(result) {
                e.closest(".patient-map").removeClass("busy");
                U.GrowlResult(result);
                if (result.isSuccessful) {
                    e.remove();
                    chart.WoundChart("fixNumbers");
                }
            }, function() {
                U.Growl("Unable to delete wound.  Please try again.", "error");
                e.closest(".patient-map").removeClass("busy");
            });
            return e;
        },
        edit: function() {
            var e = $(this), data = e.data("WoundMarker"), chart = e.closest(".wound-chart"), chartData = chart.data("WoundChart");
            Acore.Modal({
                Url: chartData.controller + "/WoundEdit",
                Input: { Id: chartData.id, PatientId: chartData.patientId, WoundNumber: data.Number },
                OnLoad: woundMarkerMethods.initEdit,
                Width: 650
            });
            return e;
        },
        init: function(options) {
            return this.each(function() {
                var e = $(this);
                $(this).data("WoundMarker", options).WoundMarker("build", options);
            })
        },
        initEdit: function(r, t, x, e) {
            WoundCare.InputInit(e);
        },
        move: function(x, y) {
            var e = $(this), data = e.closest(".wound-chart").data("WoundChart");
            e.closest(".patient-map").addClass("busy");
            U.PostUrl(data.controller + "/WoundSetPostion", { Id: data.id, PatientId: data.patientId, Number: $(this).text(), X: Math.round(x), Y: Math.round(y) }, function(result) {
                e.closest(".patient-map").removeClass("busy");
                U.GrowlResult(result);
                if (result.isSuccessful) e.data("WoundMarker", { x: x, y: y });
                else e.css({ left: e.data("WoundMarker").x, top: e.data("WoundMarker").y });
            }, function() {
                U.Growl("Unable to move wound.  Please try again.", "error");
                e.closest(".patient-map").removeClass("busy");
                e.css({ left: e.data("WoundMarker").x, top: e.data("WoundMarker").y });
            });
            return e;
        }
    };
    $.fn.WoundMarker = function(method) {
        if (woundMarkerMethods[method]) return woundMarkerMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" || !method) return woundMarkerMethods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist for the Wound Marker Plugin.");
    };
})(jQuery);