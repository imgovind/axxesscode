(function($) {
    $.extend($.fn, {
        Zebra: function(selector, exclusionarySelector) {
        return this.each(function() {
                if (!selector) selector = "tr,li";
                var rows = $(this).children(selector).not(exclusionarySelector);
                rows.filter(":even").addClass("t-alt even").removeClass("odd");
                rows.filter(":odd").addClass("odd").removeClass("t-alt even");
            });
        }
    });
})(jQuery);