(function ($) {
    var ratioAdjuster = 50,                                        // Fine-tuning to adjust calendar aspect ratio
        dragOpacity = .5,                                           // Opacity when dragging tasks
        colors = {
            pending: { background: "#3e799c", border: "#2a5169" },  // Background and Border colours for Scheduled Tasks
            complete: { background: "#3ea123", border: "#2a6e18" }, // Background and Border colours for Completed Tasks
            missed: { background: "#b20e0e", border: "#800a0a"}     // Background and Border colours for Missed Tasks
        },
        carePeriodColors = [
            { calendar: "#fbf1e2", legend: "#fbdeb5" },
            { calendar: "#fafbe2", legend: "#f9fbb5" },
            { calendar: "#fbe9e9", legend: "#fbbcbc" }
        ],
        privateDutySchedulerMethods = {
            // Function for drag and drop or resize to change scheduled times
            changeTask: function (e, event, revertFunc) {
                Acore.Confirm({
                    Message: "Are you sure you want change the scheduled time of this task?",
                    Yes: function () {
                        U.PostUrl("PrivateDuty/Schedule/Reschedule", {
                            Id: event.id,
                            PatientId: event.patientId,
                            // UserId: data.PatientSelection ? U.GuidEmpty : data.CurrentId,
                            EventStartTime: { Date: U.FormatDate(event.start), Time: U.FormatTime(event.start) },
                            EventEndTime: { Date: U.FormatDate(event.end), Time: U.FormatTime(event.end) }
                        }, function (result) {
                            U.Growl(result.errorMessage, result.isSuccessful ? (result.isWarning ? "warning" : "success") : "error");
                            if (!result.isSuccessful) revertFunc();
                            else {
                                e.PrivateDutyScheduler("rebindGrid")
                            }
                        }, function () {
                            U.Growl("Unable to update scheduled time on this task.", "error");
                            revertFunc();
                        });
                    },
                    No: function () {
                        revertFunc();
                    }
                })
            },
            // Function for rendering care periods on the calendar
            displayCarePeriods: function () {
                var e = $(this),
                    data = e.data("PrivateDutyScheduler");
                $(".episode-legend", e).parent().remove();
                $(".has-care-period", e).css("background-color", "").removeClass("has-care-period");
                if (data.CarePeriods) {
                    $(e).append(
                        $("<div>").addClass("ac").append(
                            $("<fieldset>").addClass("legend").append(
                                $("<ul/>").addClass("episode-legend"))));
                    $.each(data.CarePeriods, function (key, val) {
                        $(".episode-legend", e).append(
                            $("<li/>").html("Care Period " + val.StartDateFormatted + "&#8211;" + val.EndDateFormatted).prepend(
                                $("<div/>").css("background-color", carePeriodColors[key].legend)));
                        var days = 0;
                        $(".fc-day:visible,.fc-agenda-days td:visible").each(function () {
                            var msDay = 1000 * 60 * 60 * 24,
                                currentDate = Math.floor(new Date($(this).attr("data-date")).getTime() / msDay),
                                startDate = Math.floor(U.ConvertJsonDateToJavascriptDate(val.StartDate).getTime() / msDay),
                                endDate = Math.floor(U.ConvertJsonDateToJavascriptDate(val.EndDate).getTime() / msDay);
                            if (currentDate >= startDate && currentDate <= endDate) {
                                if (!$(this).hasClass("fc-other-month")) $(this).addClass("has-care-period").css("background-color", carePeriodColors[key].calendar);
                                days++;
                            }
                        });
                        if (days == 0) $(".episode-legend li:last", e).remove();
                    });
                    if ($(".episode-legend li", e).length == 0) $(".episode-legend", e).parent().remove();
                }
            },
            // Function for pulling data from the server
            fetchTasks: function (e, start, end, callback) {
                var data = e.data("PrivateDutyScheduler");
                var $grid = e.closest(".layout").find(".schedule-list .t-grid");
                if ($grid.length && $grid.is(":visible")) U.OnTGridDataBinding($grid);
                if (U.IsGuid(data.CurrentId)) U.PostTrackedUrl("PrivateDuty/Task/List", {
                    startDate: U.FormatDate(start),
                    endDate: U.FormatDate(end),
                    patientId: data.PatientSelection ? data.CurrentId : U.GuidEmpty,
                    userId: data.PatientSelection ? U.GuidEmpty : data.CurrentId
                }, function (result) {
                    var events = [];
                    if (result.Tasks)
                        $(result.Tasks).each(function (key, val) {
                            if (val.IsMissedVisit) back = colors.missed.background, border = colors.missed.border;
                            else if (val.IsComplete) back = colors.complete.background, border = colors.complete.border;
                            else back = colors.pending.background, border = colors.pending.border;
                            events.push({
                                id: val.Id,
                                patientId: val.PatientId,
                                title: "<strong>" + val.TaskName + "</strong><em>" + (data.PatientSelection ? val.UserName : val.PatientName) + "</em>",
                                allDay: val.IsAllDay,
                                start: U.ConvertJsonDateToJavascriptDate(val.DateIn),
                                end: U.ConvertJsonDateToJavascriptDate(val.DateOut),
                                backgroundColor: back,
                                borderColor: border,
                                group: val.Group,
                                type: val.Type,
                                isMissedVisit: val.IsMissedVisit,
                                qa: val.IsInQA,
                                completed: val.IsComplete,
                                UserCanDelete: val.IsUserCanDelete,
                                UserCanEdit: val.IsUserCanEdit,
                                UserCanPrint: val.IsUserCanPrint,
                                UserCanReopen: val.IsUserCanReopen,
                                UserCanEditDetail: val.IsUserCanEditDetail,
                                UserCanReassign: val.IsUserCanReassign,
                                editable: (val.IsUserCanEdit | val.IsUserCanEdit) ? true : false
                            });
                        });
                    callback(events);
                    data.CarePeriods = result.CarePeriods;
                    e.data("PrivateDutyScheduler", data).PrivateDutyScheduler("displayCarePeriods");
                    var grid = $grid.data("tGrid");
                    if (grid) {
                        if (data.PatientSelection) {
                            grid.hideColumn("PatientName");
                            grid.showColumn("UserName");
                        }
                        else {
                            grid.hideColumn("UserName");
                            grid.showColumn("PatientName");
                        }

                        if (!result.IsStickyNote) {
                            grid.hideColumn("StatusComment");
                            grid.hideColumn("Comments");
                            grid.hideColumn("EpisodeNotes");
                        }
                        if (!result.IsUserCanPrint) {
                            grid.hideColumn("IsUserCanPrint");
                        }
                        var widthIndex = ~ ~result.IsUserCanDelete + ~ ~result.IsUserCanEditDetail + ~ ~result.IsUserCanReassign + ~ ~result.IsUserCanReopen + ~ ~result.IsUserCanRestore;
                        if (widthIndex > 0) {

                        }
                        U.OnTGridDataBound($grid);
                        grid.dataBind(result.Tasks);
                    }
                }, function(err) {
                    if (err.statusText != "abort") U.Growl("There was an error in getting your events.  Please try again.", "error");
                }, $grid.attr("id"))
            },
            rebindGrid: function () {
                var e = $(this);
                var view = e.fullCalendar('getView');
                var data = e.data("PrivateDutyScheduler");
                var grid = e.closest(".layout").find(".schedule-list .t-grid");
                if (grid.length && grid.is(":visible")) U.OnTGridDataBinding(grid);
                if (U.IsGuid(data.CurrentId)) U.PostUrl("PrivateDuty/Task/List", {
                    startDate: U.FormatDate(view.start),
                    endDate: U.FormatDate(view.end),
                    patientId: data.PatientSelection ? data.CurrentId : U.GuidEmpty,
                    userId: data.PatientSelection ? U.GuidEmpty : data.CurrentId
                }, function (result) {
                    if (grid) {
                        grid.data("tGrid").dataBind(result.Tasks);
                        U.OnTGridDataBound(grid);
                    }
                }, function(err) {
                    if (err.statusText != "abort") U.Growl("There was an error in getting your events.  Please try again.", "error")
                })
            },
         
            // Needed to use mark-up in title formatting
            fixTitle: function () {
                var e = $(this),
                    data = e.data("PrivateDutyScheduler");
                $(".fc-display-name", e).text(data.CurrentName);
            },
            getFirstAspectRatio: function () {
                return $(this).parent().width() / $(this).parent().height();
            },
            // Calculate calendar's aspect ratio
            getAspectRatio: function () {
                return $(this).parent().width() / ($(this).parent().height() - ratioAdjuster);
            },
            // Return current view's end date
            getEndDate: function () {
                return U.FormatDate($(this).fullCalendar("getView").end)
            },
            // Return current view's start date
            getStartDate: function () {
                return U.FormatDate($(this).fullCalendar("getView").start)
            },
            // Initialization of plugin
            init: function (options) {
                this.each(function () {
                    // Set variables
                    $(this).data("PrivateDutyScheduler", {
                        CurrentId: U.IsGuid(options.CurrentId) ? options.CurrentId : U.GuidEmpty,
                        CurrentName: options.CurrentName != null ? options.CurrentName : "",
                        PatientSelection: options.PatientSelection != false ? true : false
                    });
                    var e = $(this), data = e.data("PrivateDutyScheduler");
                    $(e).on("mousedown mouseup", "div.fc-event", function(mouseEvent) {
                        var target = $(mouseEvent.target),
                            controls = $(this).find(".task-controls");
                        if (target.is(controls) || target.parent().is(controls)) return;
                        controls.toggle(mouseEvent.type != "mousedown"); 
                        if (mouseEvent.type == "mouseup") controls.css("display", "");
                    });
                    // If already initialized, refresh data
                    if (e.hasClass("fc")) return $(this).PrivateDutyScheduler("refresh");
                    // Otherwise initialize plugin
                    else {
                        e.fullCalendar({
                            aspectRatio: e.PrivateDutyScheduler("getFirstAspectRatio"),
                            buttonText: { today: "Today", month: "Month", week: "Week", day: "Day" },
                            columnFormat: { month: "dddd", week: "ddd, MMM d", day: "dddd, MMMM d, yyyy" },
                            dayClick: function (date, allDay) {
                                var data = $(this).closest(".fc").data("PrivateDutyScheduler");
                                if (options.Scheduler && options.HasSchedulerPermission) {
                                    if (!data.PatientSelection || $(this).hasClass("has-care-period")) Schedule.Task.PrivateDuty.New(e.data("PrivateDutyScheduler").CurrentId, e.data("PrivateDutyScheduler").PatientSelection, U.FormatDate(date), allDay ? "" : U.FormatTime(date));
                                    else Acore.Confirm({
                                        Message: "Cannot schedule a new task for this date because there is no care period covering this day.  Would you like to create a new care period?",
                                        Yes: function () {
                                            Episode.PrivateDuty.New(data.CurrentId);
                                        }
                                    })
                                }
                            },
                            dragOpacity: dragOpacity,
                            editable: options.Scheduler,
                            eventAfterRender: function (event, element, view) {
                                // Process Title Text as HTML
                                $(".fc-event-title", element).html($(".fc-event-title", element).text());
                                // Set local data variables
                                $(element).data({ id: event.id, patientId: event.patientId });
                                // Add task controls
                                $(element).append(
                                    $("<div/>").addClass("task-controls")
                                );
                                var controls = $(".task-controls", element);
                                if (!event.completed && event.UserCanEdit) controls.append(
                                    $("<span/>").addClass("img icon22 " + event.group + " edit").Tooltip("Edit Document").on("click", function() {
                                        if (event.isMissedVisit) {
                                            if (event.completed) {
                                                MissedVisit.PrivateDuty.Open(event.id);
                                            } else {
                                                MissedVisit.PrivateDuty.Edit(event.id);
                                            }
                                        }
                                        else {
                                            CommandFactory.Open({ Id: event.id, PatientId: event.patientId, Type: event.type }, "PrivateDuty");
                                        }
                                    })
                                );
                                if (event.UserCanPrint) controls.append(
                                    $("<span/>").addClass("img icon22 " + event.group + " print").Tooltip("Print").on("click", function() {
                                        var result = CommandFactory.Print(this, { Id: event.id, PatientId: event.patientId }, "PrivateDuty");
                                        if (result) {
                                            Acore.OpenPrintView(result);
                                        }
                                    })
                                );
                                if (!event.isMissedVisit && !event.completed && event.UserCanEdit && event.start <= new Date()) controls.append(
                                    $("<span/>").addClass("img icon22 delete-schedule").Tooltip("Missed Visit Form").on("click", function () {
                                        MissedVisit.PrivateDuty.New(event.patientId, event.id);
                                    })
                                );
                                if (options.Scheduler && event.UserCanEditDetail) controls.append(
                                    $("<span/>").addClass("img icon22 edit-schedule").Tooltip("Edit Scheduled Date/Time").on("click", function () {
                                        Schedule.Task.PrivateDuty.Edit(event.patientId, event.id);
                                    })
                                );
                                if (event.UserCanReopen) controls.append(
                                    $("<span/>").addClass("img icon22 reopen-schedule").Tooltip("Reopen").on("click", function () {
                                        Schedule.Task.PrivateDuty.Reopen(event.patientId, event.id);
                                    })
                                );
                                if (options.Scheduler && event.UserCanDelete) controls.append(
                                    $("<span/>").addClass("img icon22 delete").Tooltip("Delete").on("click", function() {
                                        Schedule.Task.PrivateDuty.Delete(event.patientId, event.id);
                                    })
                                );
                                var icons = controls.find(".img");
                                if (!element.hasClass("fc-event-vert")) controls.css("width", (icons.length * icons.outerWidth(true)) + "px");
                            },
                            eventAfterAllRender: function (view) {
                                $(this).removeClass("loading-visibility");
                            },
                            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
                                privateDutySchedulerMethods.changeTask(e, event, revertFunc);
                            },
                            eventResize: function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
                                privateDutySchedulerMethods.changeTask(e, event, revertFunc);
                            },
                            events: function (start, end, callback) {
                                privateDutySchedulerMethods.fetchTasks(e, start, end, callback);
                            },
                            eventMouseout: function () { },
                            eventMouseover: function (event, jsEvent, view) {
                                var within = view.element;
                                if (within.hasClass("fc-agenda")) {
                                    if ($(this).hasClass("fc-event-hori")) within = $(this).parent().parent();
                                    else within = $(this).parent().parent().parent();
                                }
                                $(this).find(".task-controls").position({ my: "right+25 top-25", at: "right top", of: $(this), collision: "flipfit", within: within });
                            },
                            header: {
                                left: "prev,next today",
                                center: "title",
                                right: "month,agendaWeek,agendaDay"
                            },
                            loading: function (isLoading, view) {
                                if (isLoading) $(this).parent().addClass("loading-visibility");
                                else $(this).parent().removeClass("loading-visibility");
                            },
                            timeFormat: "h:mmTT{-h:mmTT}",
                            titleFormat: {
                                month: "'<span class=\"fc-display-name\"></span> &#8212; 'MMMM yyyy",
                                week: "'<span class=\"fc-display-name\"></span> &#8212; 'MMM d[ yyyy]{'&#8211;'[ MMM]d, yyyy}",
                                day: "'<span class=\"fc-display-name\"></span> &#8212; 'dddd, MMMM d, yyyy"
                            },
                            viewDisplay: function (view) {
                                var start = U.FormatGridDate(view.start);
                                var end = U.FormatGridDate(view.end);
                                if (view.name == "agendaWeek")
                                    for (var i = 0; i < 7; i++)
                                        $(".fc-view-agendaWeek .fc-agenda-days td", this).eq(i).attr("data-date", U.FormatInternationalDate(new Date(view.start.getFullYear(), view.start.getMonth(), view.start.getDate() + i)));
                                if (view.name == "agendaDay")
                                    $(".fc-view-agendaDay .fc-agenda-days .fc-col0", this).attr("data-date", U.FormatInternationalDate(view.start));
                                e.PrivateDutyScheduler("fixTitle");
                                e.PrivateDutyScheduler("displayCarePeriods");
                                U.FilterGridByDateRange(e.closest(".layout").find(".schedule-list .t-grid").data("tGrid"), start, end);
                            },
                            weekMode: "liquid"
                        });
                        // Remove their button reflection
                        $(".fc-button-effect", e).remove();
                        // Add List button
                        if (options.ListButton != false) $(".fc-header-right", e).append(
                            $("<span/>").addClass("fc-button fc-state-default fc-corner-right fc-corner-left fc-list-tasks").append(
                                $("<span/>").addClass("fc-button-inner").append(
                                    $("<span/>").addClass("fc-button-content").text("List Tasks").on("click", function () {
                                        e.parent().parent().find(".show-list").click()
                                    })
                                )
                            )
                        );
                        // Continue DOM Chain
                        return e;
                    }
                })
            },
            refresh: function () {
                return this.each(function () {
                    //Sets the current month to today's month if the calendar's month does not match
                    if ($(this).fullCalendar("getView").name != "month") $(this).fullCalendar("changeView", "month");
                    else {
                        $(this).fullCalendar("refetchEvents");
                        $(this).PrivateDutyScheduler("fixTitle");
                    }
                })
            },
            refreshCurrentView: function () {
                return this.each(function () {
                    $(this).fullCalendar("refetchEvents");
                })
            },
            rerenderEvents: function () {
                return this.each(function () {
                    $(this).PrivateDutyScheduler("setAspectRatio").fullCalendar("rerenderEvents");
                })
            },
            rerenderEventsAsNew: function () {
                return this.each(function () {
                    $(this).PrivateDutyScheduler("setFirstAspectRatio").fullCalendar("rerenderEvents");
                })
            },
            resize: function () {
                return this.each(function () {
                    $(this).fullCalendar("render");
                })
            },
            setAspectRatio: function () {
                return this.each(function () {
                    $(this).fullCalendar("option", "aspectRatio", $(this).PrivateDutyScheduler("getAspectRatio"));
                })
            },
            setFirstAspectRatio: function () {
                return this.each(function () {
                    $(this).fullCalendar("option", "aspectRatio", $(this).PrivateDutyScheduler("getFirstAspectRatio"));
                })
            }
        };
    $.fn.PrivateDutyScheduler = function (method) {
        if (privateDutySchedulerMethods[method]) return privateDutySchedulerMethods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" || !method) return privateDutySchedulerMethods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist for the Private Duty Scheduler.");
    };
})(jQuery);