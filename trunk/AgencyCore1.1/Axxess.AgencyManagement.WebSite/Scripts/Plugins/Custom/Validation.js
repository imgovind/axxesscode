$.validator.messages.required = "* Required";
$.validator.messages.email = $.validator.messages.invalid = "* Invalid";
$.validator.addMethod("not-zero", function(value, element) { return value.trim().length && !/^0$/.test(value) && !/^0{8}-0{4}-0{4}-0{4}-0{12}$/.test(value) }, $.validator.messages.required);
$.validator.addMethod("multiple", function(value, element) { return value != null; }, $.validator.messages.required);
$.validator.addMethod("physician-picker", function(value, element) { return !value.length || /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test($(element).siblings("input[type=hidden]").val().trim()) }, $.validator.messages.invalid);
$.validator.addMethod("phone-short", function(value, element) { return !value.length || /^\d{3}$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("phone-long", function(value, element) { return !value.length || /^\d{4}$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("zip", function(value, element) { return !value.length || /^\d{5}(\d{4})?$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("ssn", function(value, element) { return !value.length || /^\d{9}$/.test(value.trim()) }, $.validator.messages.invalid);
$.validator.addMethod("email", function(value, element) { return !value.length || /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(value) }, $.validator.messages.invalid);
$.validator.addMethod("greaterThan", function(value, element, params) { if (!/Invalid|NaN/.test(new Date(value))) { return new Date(value) > new Date($(params[0]).val()); } return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val()));}, 'Must be greater than {1}.');
(function($) {
    $.extend($.fn, {
        ValidateForFile: function(Options) {
            return this.each(function() {
                var form = $(this);
                form.submit(function(event) {
                    event.preventDefault();
                    if (Options.BeforeDownload)
                        if (!Options.BeforeDownload(form)) {
                        form.trigger("SubmitFailure");
                        return false;
                    }
                    $.fileDownload(form.attr('action'), {
                        httpMethod: "POST",
                        data: form.serializeArray(),
                        successCallback: function(url) {
                            form.trigger("SubmitSuccess");
                        },
                        failCallback: function(responseHtml, url) {
                            form.trigger("SubmitFailure");
                            try {
                                U.Growl(JSON.parse(responseHtml.replace("<pre>", "").replace("</pre>", "")), "error");
                            } catch (err) {
                                U.Growl("An problem occured when downloading the file.", "error");
                            }
                        }
                    })
                })
            })
        },
        Validate: function(Options) {
            return this.each(function() {
                if (Options == undefined) Options = new Object();
                var form = this,
                    AfterSubmit = function(Result) {
                        if (Result.responseText && Result.status == 200) Result = JSON.parse(Result.responseText.replace(/<[^>]*>/g, ""));
                        else if (Result.responseJSON && Result.status != 200) Result = Result.responseJSON;
                        var Message = Result.errorMessage;
                        if (Result.isSuccessful) {
                            if (Options.Message) Message = Options.Message;
                            if (!Message) Message = "Your information was successfully saved.";
                            if (Options.Success != undefined) try {
                                if (typeof Options.Success == "function") Options.Success(Result, form);
                                else if (typeof Options.Success == "string") eval(Options.Success);
                            } catch (e) {
                                U.Growl(U.MessageErrorJS(true), "error");
                                console.log("Error in #" + $(form).attr("id") + " success function: " + e.toString());
                            }
                            U.Growl(Message, "success");
                            $(form).trigger("SubmitSuccess");
                        } else {
                            $(form).trigger("SubmitFailure");
                            if (!Message) Message = "There was an problem while trying to save your information.";
                            if (Options.Success != undefined) try {
                                if (typeof Options.Fail == "function") Options.Fail(Result, form);
                                else if (typeof Options.Fail == "string") eval(Options.Fail);
                            } catch (e) {
                                U.Growl(U.MessageErrorJS(true), "error");
                                console.log("Error in #" + $(form).attr("id") + " fail function: " + e.toString());
                            }
                            U.Growl(Message, "error");
                        }
                    };
                $(form).on("SubmitSuccess", function() { }).on("SubmitFailure", function() { }).validate(
                    $.extend({
                        invalidHandler: function() {
                            U.Growl("There was a problem validating your information.", "error")
                            $(".phone-short.required", form).each(function() { if ($(this).val() == "") $(this).parent().find("input").addClass("error") });
                            $(form).closest(".window-content").scrollTop(0);
                        },
                        ignore: ".ignorevalidation,:hidden",
                        submitHandler: function() {
                            var submitVariables = {
                                beforeSubmit: function(arr, $form, options) {
                                    if (typeof Options.BeforeSubmit == "function")
                                        if (Options.BeforeSubmit(arr, $form, options) === false) {
                                        $form.trigger("SubmitFailure");
                                        return false;
                                    }
                                    if (navigator.appName != "Microsoft Internet Explorer" && $("input[type=file]", $form).length != 0 && this.files) {
                                        var success = true;
                                        var maxSize = parseInt($("input[name=MAX_FILE_SIZE]", $form).val());
                                        $("input[type=file]", $form).each(function(index, element) {
                                            if (typeof this.files[0] !== "undefined") {
                                                size = this.files[0].size;
                                                if (success) success = maxSize > size;
                                            }
                                        });
                                        U.Growl("An attachment is larger than the maximum allowed file size. The maximum allowed file size is 10 MBs (Megabytes).", "error");
                                        return success;
                                    }
                                    return true;
                                },
                                dataType: "json",
                                success: AfterSubmit,
                                error: AfterSubmit
                            };

                            // 
//                            if( Options.IsOnlyValidate)
//                             return false;

                            if (typeof Options.PreprocessForm == "function") {
                                try {
                                    var preProcessResult = Options.PreprocessForm(form);
                                    if (preProcessResult == undefined || preProcessResult) {
                                        if (preProcessResult && typeof preProcessResult == "object")
                                            preProcessResult.then(function(answer) {
                                                answer = answer == 'true';
                                                if (answer) $(form).ajaxSubmit(submitVariables);
                                                else $(form).trigger("SubmitFailure");
                                            });
                                        else $(form).ajaxSubmit(submitVariables);
                                    }
                                    else $(form).trigger("SubmitFailure");
                                } catch (e) {
                                    U.Growl(U.MessageErrorJS(true), "error");
                                    console.log("Error in #" + $(form).attr("id") + " fail function: " + e.toString());
                                }
                            } else $(form).ajaxSubmit(submitVariables);
                            return false;
                        }
                    }, Options)
                )
            })
        }
    })
})(jQuery);