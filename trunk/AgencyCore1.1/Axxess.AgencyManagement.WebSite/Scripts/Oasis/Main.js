if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    Question: "",
    BradenScale: function (type) {
        var e = $("#" + type + "_BradenScale", this);
        $("[tooltip]", e).Tooltip();
        $("[type=radio]", e).on("change", function () {
            var score = 0;
            $(":checked", e).each(function () { score += parseInt($(this).val()) });
            $(".score-list > li", e).removeClass("strong");
            if (score > 18) $(".score-list > li:eq(0)", e).addClass("strong");
            else if (score > 14) $(".score-list > li:eq(1)", e).addClass("strong");
            else if (score > 12) $(".score-list > li:eq(2)", e).addClass("strong");
            else if (score > 9) $(".score-list > li:eq(3)", e).addClass("strong");
            else if (score > 0) $(".score-list > li:eq(4)", e).addClass("strong");
            $("[type=text]", e).val(score);
        });
        $("[type=radio]:first", e).trigger("change");
    },
    Delete: function (cont, id, url) {
        U.Delete("Assessment", url, { Id: id }, function () {
            cont.parents("tr:first").remove()
        })
    },
    DeleteAsset: function (control, assessmentType, name, assetId, area) {
        U.Delete("Asset", (area || "") + "/Oasis/DeleteWoundCareAsset", {
            episodeId: $("#" + assessmentType + "_EpisodeId").val(),
            patientId: $("#" + assessmentType + "_PatientGuid").val(),
            eventId: $("#" + assessmentType + "_Id").val(),
            assessmentType: assessmentType,
            name: name,
            assetId: assetId
        }, function () {
            $(control).closest('td,div').empty().append(
                $("<input/>", {
                    type: "file",
                    name: assessmentType + "_" + name,
                    value: "Upload",
                    size: "1"
                })
            )
        })
    },
    FallAssessment: function (AssessmentType) {
        $("#" + AssessmentType + "_485FallAssessmentScore").val(String($("#" + AssessmentType + "_FallAssessment .option input[type='checkbox']:checked").length));
        $("#" + AssessmentType + "_FallAssessment .option input").on("change", function () {
            $("#" + AssessmentType + "_485FallAssessmentScore").val(String($("#" + AssessmentType + "_FallAssessment .option input[type='checkbox']:checked").length));
        })
    },
    GotoQuestion: function (question, assessmentType, version) {
        var tab = 1, q = parseInt(question.replace(/^[M0]*/, ""));
        if (assessmentType == "StartOfCare" || assessmentType == "ResumptionOfCare") {
            if (q == 1) tab = 17;
            if (q >= 1000 && q <= 1030) tab = 2;
            if (q >= 1032 && q <= 1036) tab = 3;
            if (q == 1100) tab = 5;
            if (q >= 1200 && q <= 1230) tab = 6;
            if (q >= 1240 && q <= 1242) tab = 7;
            if (q >= 1300 && q <= 1350) tab = 8;
            if (q >= 1400 && q <= 1410) tab = 9;
            if (q >= 1600 && q <= 1630) tab = 12;
            if (q >= 1700 && q <= 1750) tab = 14;
            if (q >= 1800 && q <= 1910) tab = 15;
            if (q >= 2000 && q <= 2040) tab = 17;
            if (q >= 2100 && q <= 2110) tab = 18;
            if (q >= 2200 && q <= 2250) tab = 19;
        } else if (assessmentType == "Recertification") {
            if (q == 1) tab = 16;
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 5;
            if (q == 1242) tab = 6;
            if (q >= 1306 && q <= 1350) tab = 7;
            if (q >= 1400 && q <= 1410) tab = 8;
            if (q >= 1610 && q <= 1630) tab = 11;
            if (q >= 1810 && q <= 1910) tab = 14;
            if (q == 2030) tab = 16;
            if (q >= 2200) tab = 17;
        } else if (assessmentType == "FollowUp") {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 3;
            if (q == 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q == 1400) tab = 6;
            if (q >= 1610 && q <= 1630) tab = 7;
            if (q >= 1810 && q <= 1860) tab = 8;
            if (q == 2030) tab = 9;
            if (q == 2200) tab = 10;
        } else if (assessmentType == "Transfer" || assessmentType == "TransferDischarge") {
            if (q >= 1040 && q <= 1055) tab = 2;
            if (q >= 1500 && q <= 1510) tab = 3;
            if (q >= 2004 && q <= 2015) tab = 4;
            if (q >= 2300 && q <= 2310) tab = 5;
            if (q == 903 || q == 906 || (q >= 2400 && q <= 2440)) tab = 6;
        } else if (assessmentType == "Death") {
            if (q == 903 || q == 906) tab = 2;
        } else if (assessmentType == "Discharge") {
            if (version <= 1) {
                if (q >= 1040 && q <= 1050) tab = 2;
                if (q == 1230) tab = 3;
                if (q >= 1242) tab = 4;
                if (q >= 1306 && q <= 1350) tab = 5;
                if (q >= 1400 && q <= 1410) tab = 6;
                if (q >= 1500 && q <= 1510) tab = 7;
                if (q >= 1600 && q <= 1620) tab = 8;
                if (q >= 1700 && q <= 1750) tab = 9;
                if (q >= 1800 && q <= 1890) tab = 10;
                if (q >= 2004 && q <= 2030) tab = 11;
                if (q >= 2100 && q <= 2110) tab = 12;
                if (q >= 2300 && q <= 2310) tab = 13;
                if (q == 903 || q == 906 || (q >= 2400 && q <= 2420)) tab = 14;
            } else if (version == 2) {
                if (q >= 1040 && q <= 1050) tab = 3;
                if (q == 1230) tab = 5;
                if (q >= 1242) tab = 6;
                if (q >= 1306 && q <= 1350) tab = 7;
                if (q >= 1400 && q <= 1410) tab = 8;
                if (q >= 1500 && q <= 1510) tab = 9;
                if (q >= 1600 && q <= 1620) tab = 11;
                if (q >= 1700 && q <= 1750) tab = 13;
                if (q >= 1800 && q <= 1890) tab = 14;
                if (q >= 2004 && q <= 2030) tab = 15;
                if (q >= 2100 && q <= 2110) tab = 16;
                if (q >= 2300 && q <= 2310) tab = 17;
                if (q == 903 || q == 906 || (q >= 2400 && q <= 2420)) tab = 18;
            }
        }
        if (Acore.Windows[assessmentType].IsOpen) {
            $("#window_" + assessmentType).WinFocus();
            if ($(".vertical-tab-list li:nth-child(" + tab + ")", "#window_" + assessmentType).hasClass("ui-state-active")) {
                if ($("#" + assessmentType + "_" + question).length) {
                    var scroll = $("#" + assessmentType + "_" + question).closest("fieldset").position().top;
                    $(".tab-content:visible", "#window_" + assessmentType).Animate({ scrollTop: scroll });
                }
            } else {
                Oasis.Question = question;
                $(".vertical-tab-list li:nth-child(" + tab + ") a", "#window_" + assessmentType).click();
            }
        }
    },
    Help: function (mooCode) {
        U.PostUrl("LookUp/Guide", { mooCode: mooCode }, function (data) {
            Acore.Dialog({
                AlwaysOnTop: true,
                Content: $("<div/>").addClass("wrapper main note").append(
                    $("<fieldset/>").text(data.ItemIntent).append(
                        $("<legend/>").text("Item Intent"))).append(
                    $("<fieldset/>").text(data.Response).append(
                        $("<legend/>").text("Response"))).append(
                    $("<fieldset/>").text(data.DataSources).append(
                        $("<legend/>").text("Data Sources"))).append(
                    $("<div/>").Buttons([{ Text: "Close", Click: function () { $(this).closest(".window").Close() } }])),
                Name: "OASIS Help",
                Width: 600
            });
            $("#window_" + Acore.DialogId).SetHeight();
        })
    },
    HandlerHelperTemplate: function (assessmentType, $control, actionType, action) {
        var $form = $control.closest("form");
        var allRefresh = function (result) {
            Refresh.All(result);
            UserInterface.CloseWindow(assessmentType);
        };
        if ($form != null) {
            $('#' + assessmentType + '_Button', $form).val(actionType);
            $form.validate();
            var options = {
                dataType: "json",
                beforeSubmit: function () { $control.trigger("ProcessingStart"); },
                success: function (result) {
                    $control.trigger("ProcessingComplete");
                    if (result != undefined) {
                        if (result.isSuccessful) {
                            if (actionType == "CheckError" || actionType == "SaveComplete") {
                                if (typeof action == "function") action();
                                Refresh.CenterQAMySchedule(result);
                            } else if (actionType.indexOf("Save") > -1) {
                                U.Growl("Your assessment has been saved.", "success");
                                Refresh.CenterQAMySchedule(result);
                                if (actionType == "SaveExit") $form.Close();
                                if (actionType == "SaveContinue") Oasis.NextTab("#" + assessmentType + "_Tabs");
                            } else if (actionType == "Approve") {
                                allRefresh(result);
                                U.Growl("Your assessment has been approved.", "success");
                            } else if (actionType == "Return") {
                                allRefresh(result);
                                U.Growl("Your assessment has been returned.", "success");
                            } else if (actionType == "SaveCheckError") {
                                U.Growl("Your assessment has been saved.", "success");
                                if (typeof action == "function") action();
                                Refresh.CenterQAMySchedule(result);
                            }
                        } else U.Growl(result.errorMessage, "error");
                    } else U.Growl("Error sending the data", "error");
                },
                error: function (result) {
                    $control.trigger("ProcessingComplete");
                    U.Growl(result, "error");
                }
            };
            $form.find(".form-omitted :input,.form-omitted:input").val("");
            $form.ajaxSubmit(options);
            return false;
        }
        return false;
    },
    Init: function (r, t, x, e) {
        var tab = $(".oasis-container", e);
        var Data = U.JsonInput($(".tabinput", tab).serializeArray());
        if (Data.AssessmentType) {
            tab.addClass("ui-tabs-vertical ui-helper-clearfix").tabs({
                active: 0,
                beforeLoad: function (event, ui) {
                    ui.jqXHR.setRequestHeader("Content-Type", ui.ajaxSettings.contentType);
                    Data.Category = $("a", ui.tab).attr("name");
                    Data.Version = $("a", ui.tab).attr("version");
                    $.extend(ui.ajaxSettings, {
                        type: "POST",
                        hasContent: true,
                        error: function () {
                            ui.panel.removeClass("loading");
                            ui.panel.html(U.MessageErrorAjax);
                        },
                        data: jQuery.param(Data)
                    })
                },
                beforeActivate: function (event, ui) {
                    ui.newPanel.addClass("loading tab-content");
                    var tabLink = $("a", ui.newTab);
                    if ($("a", ui.newTab).attr("href").indexOf("#") != -1) $("a", ui.newTab).attr("href", $("a", ui.newTab).attr("area") + "/Oasis/Category");
                },
                load: function (event, ui) {
                    Acore.OnLoad(ui.panel);
                    Oasis.InitContent(Data.AssessmentType, $("a", ui.tab).attr("name"), $(ui.panel));
                    ui.panel.removeClass("loading");
                    if (Oasis.Question) {
                        if ($("#" + Data.AssessmentType + "_" + Oasis.Question).length) {
                            var scroll = $("#" + Data.AssessmentType + "_" + Oasis.Question).closest("fieldset").position().top;
                            $(ui.panel).Animate({ scrollTop: scroll });
                        }
                        Oasis.Question = "";
                    }
                }
            });
            U.ToolTip($(".vertical-tab-list a", tab), "calday");
            Oasis.InitContent(Data.AssessmentType, $(".ui-tabs-active > a", tab).attr("name"), $(".tab-content", tab));
        }
    },
    InitContent: function (type, tab, e) {
        if (type.substr(0, 8) == "NonOASIS") {
            $("fieldset.oasis", e).removeClass("oasis");
            $("a.green,.oasis-help", e).remove();
        } else $("fieldset.oasis.loc485", e).removeClass("loc485");
        Oasis.InitInterventionsAndGoals($(".interventions,.goals"), e);
        if (typeof Oasis.Tabs[tab] == "function") Oasis.Tabs[tab].apply(e, [type]);
    },
    InitInterventionsAndGoals: function (fieldset) {
        fieldset.find("input[type=checkbox]").each(function () {
            if ($(this).siblings("span").length) {
                if (!$(this).prop("checked")) $(this).siblings("span").find(":input").attr("disabled", true);
                $(this).on("change", function () {
                    if ($(this).prop("checked")) $(this).siblings("span").find(":input").attr("disabled", false);
                    else $(this).siblings("span").find(":input").attr("disabled", true);
                })
            }
        })
    },
    InitWoundCare: function (r, t, x, e) {
        $(".WoundType", e).Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Stasis Ulcer", "Arterial Ulcer"] });
        $(".DeviceType", e).Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
        $(".IVSiteDressingUnit", e).Autocomplete({ source: ["Normal Saline", "Heparin"] });
    },
    LoadCancel: function (Id, serviceId) {
        var service = Acore.AppService(serviceId);
        if (service) {
            var area = window[service].Area;
            U.PostUrl(area + "/Oasis/Inactivate", { Id: Id }, function (result) {
                if (result.isValid) $("<form action=\"/Oasis/GenerateForCancel\" method=\"post\"><input type=\"hidden\" name=\"Id\" value=\"" + Id + "\"></input></form>").appendTo("body").submit().remove();
                else U.Growl(result.Message, "error");
            })
        }
    },
    LoadPrevious: function (type) {
        var previousAssessmentArray = $("#" + type + "_PreviousAssessments", this).val().split('_');
        if (previousAssessmentArray.length > 1) Acore.Confirm({
            Message: "Are you sure you want to load this assessment?",
            Yes: function () {
                var episodeId = $("#" + type + "_EpisodeId").val(),
                    patientId = $("#" + type + "_PatientGuid").val(),
                    assessmentId = $("#<" + type + "_Id").val(),
                    assessmentType = $("#" + type + "_AssessmentType").val();
                U.PostUrl("Oasis/LoadPrevious", {
                    episodeId: episodeId,
                    patientId: patientId,
                    assessmentId: assessmentId,
                    assessmentType: assessmentType,
                    previousAssessmentId: previousAssessmentArray[0],
                    previousAssessmentType: previousAssessmentArray[1]
                }, function (result) {
                    U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                    if (result.isSuccessful && result.IsDataCentersRefresh) Refresh.CenterQAMySchedule(result);
                })
            }
        });
        else U.Growl("Please select a previous assessment first", "error");
    },
    MasterCalendar: function (type) {
        var e = $(this);
        $(".oasis-master-calendar", e).on("click", function () {
            var button = $(this);
            if (button.text().split(" ")[0] == "Show") {
                button.hide();
                $("#" + type + "_BlankMasterCalendar", e).Load("Oasis/BlankMasterCalendar", {
                    episodeId: $("#" + type + "_EpisodeId", e).val(),
                    patientId: $("#" + type + "_PatientGuid", e).val(),
                    assessmentType: type
                }, function (r, t, x, e) {
                    if (t == "success") button.show().text("Hide Calendar");
                    else {
                        $("#" + type + "_BlankMasterCalendar").html('<p>There was an error making the AJAX request</p>');
                        button.show();
                    }
                })
            } else {
                $("#" + type + "_BlankMasterCalendar", e).empty();
                button.text("Show Calendar");
            }
        })
    },
    NextTab: function (id) {
        $(id).data("uiTabs").active.next().find("a").click();
    },
    NonOasisSignature: function (id, patientId, prefix) {
        Acore.Modal({
            Name: "Non-OASIS Signature",
            Url: prefix + "/Oasis/NonOasisSignature",
            Input: { Id: id, PatientId: patientId },
            OnLoad: Oasis.NonOasisSignatureInit,
            Width: 820
        })
    },
    NonOasisSignatureInit: function (r, t, x, e) {
        var $form = $(".mainform", e),
            window = $("input[name=oasisPageName]", $form).val();
        $form.Validate({
            Success: function (result) {
                Refresh.CenterQAMySchedule(result);
                $("#window_" + window).Close();
            }
        })
    },
    NutritionScore: function (type) {
        var e = $("#" + type + "_NutritionHealthScreen", this);
        $(".checkgroup:first [type=checkbox]", e).on("change", function () {
            var score = 0;
            $(".checkgroup:first :checked", e).each(function () { score += parseInt($(this).closest(".option").find(".fr").text()) });
            $(".score-list > li", e).removeClass("strong");
            if (score > 55) $(".score-list > li:eq(0)", e).addClass("strong");
            else if (score > 25) $(".score-list > li:eq(1)", e).addClass("strong");
            else if (score > 0) $(".score-list > li:eq(2)", e).addClass("strong");
            $("#" + type + "_GenericGoodNutritionScore", e).val(score);
        });
        $(".checkgroup:first [type=checkbox]:first", e).trigger("change");
    },
    Refresh: function (id) {
        $(id).tabs().tabs("active", 0).find(id + " .tab-content").each(function () {
            $(this).scrollTop(0)
        })
    },
    Reopen: function (id, patientId, episodeId, actionType, serviceId) {
        var service = Acore.AppService(serviceId);
        if (service) {
            var area = window[service].Area;
            Acore.Confirm({
                Message: "Are you sure you want to reopen the assessment?<br/>" +
                    "<em>Note: If this OASIS assessment was already accepted by CMS (Center for Medicare &#38; Medicaid Services), you will have to re-submit this OASIS assessment to CMS if changes are made to OASIS items.</em><br/>" +
                    "<label for='OasisReopen_Reason' class='fl'>Reason</label><textarea id='OasisReopen_Reason'></textarea>",
                Width: 700,
                Yes: function () {
                    var reason = $(this).closest(".window").find("textarea").val();
                    U.PostUrl(area + "/Oasis/ReOpen", {
                        Id: id,
                        patientId: patientId,
                        episodeId: episodeId,
                        actionType: actionType,
                        reason: reason
                    }, function (data) {
                        if (data.isSuccessful) {
                            U.Growl(data.errorMessage, "success");
                            Refresh.CenterQAMySchedule(data);
                            Refresh.OASISGrids(data);
                            Acore.CloseDialog();
                        } else U.Growl(data.errorMessage, "error");
                    })
                }
            })
        }
    },
    StatusAction: function (actionType, patientId, eventId, prefix, reason) {
        var input = { patientId: patientId, Id: eventId },
            isReturning = actionType == "Return";
        if (isReturning) $.extend(input, { reason: reason });
        U.PostUrl((prefix || "") + "/Oasis/" + actionType, input, function (result) {
            U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
            if (result.isSuccessful) {
                Acore.ClosePrintView();
                if (isReturning) Acore.CloseReturnReason();
                Refresh.CenterQAMySchedule(result);
                Refresh.OASISGrids(result);
            } else if (isReturning) Acore.ReturnReasonFailed();
        });
    },
    TinettiAssessment: function (type) {
        var e = $("#" + type + "_Tinetti", this);
        $("[type=radio]", e).on("change", function () {
            var balance = 0, gait = 0, total = 0;
            $("#" + type + "_Tinetti :checked", e).each(function () {
                if ($(this).hasClass("tinetti-balance")) balance += parseInt($(this).val());
                if ($(this).hasClass("tinetti-gait")) gait += parseInt($(this).val());
                total += parseInt($(this).val());
            });
            $("[name=" + type + "_TinettiBalanceTotal]", e).val(String(balance));
            $("[name=" + type + "_TinettiGaitTotal]", e).val(String(gait));
            $("[name=" + type + "_TinettiTotal]", e).val(String(total));
            $(".score-list > li", e).removeClass("strong");
            if (total > 23) $(".score-list > li:eq(0)", e).addClass("strong");
            else if (total > 18) $(".score-list > li:eq(1)", e).addClass("strong");
            else if (total > 0) $(".score-list > li:eq(2)", e).addClass("strong");
        });
        $("#" + type + "_Tinetti input:first", e).trigger("change");
    },
    Ulcers: function (type) {
        var e = $(this);
        $("#" + type + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent,#" + type + "_M1308NumberNonEpithelializedStageFourUlcerCurrent,#" + type + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", e).keyup(function () {
            var stage3 = $("#" + type + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent", e).val(),
	            stage4 = $("#" + type + "_M1308NumberNonEpithelializedStageFourUlcerCurrent", e).val(),
	            cvrg = $("#" + type + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", e).val();
            if (parseInt(stage3) > 0 || parseInt(stage4) > 0 || parseInt(cvrg) > 0) $("#" + type + "_M1310,#" + type + "_M1312,#" + type + "_M1314", e).show();
            else $("#" + type + "_M1310,#" + type + "_M1312,#" + type + "_M1314", e).each(function () {
                $("input", this).val("")
            }).hide();
        });
        $("#" + type + "_M1306UnhealedPressureUlcers", e).on("change", function () {
            $("#" + type + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent", e).keyup()
        });
        $("#" + type + "_M1306UnhealedPressureUlcers", e).trigger("change");
    }
});