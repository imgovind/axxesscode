if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    Assessment: {
        BlankPrint: function (AssessmentType, Discipline) {
            U.GetAttachment("Oasis/BlankPdf", { AssessmentType: AssessmentType, Discipline: Discipline })
        },
        FormSubmit: function (AssessmentType, control, actionType, action) {
            Oasis.HandlerHelperTemplate(AssessmentType, control, actionType, action)
         },
        Return: function(id, patientId, episodeId, $control, area) {
            var window = $control.closest(".window"), windowAcoreId = window.GetAcoreId();
            if (!Acore.Windows[Acore.ReturnReasonId]) Acore.Windows[windowAcoreId].Close = Acore.CloseReturnReason;
            Acore.ReturnReason({
                EventId: id,
                EpisodeId: episodeId,
                PatientId: patientId,
                UrlPrefix: area,
                ReturnFunction: function(reason) {
                    U.PostUrl(area + "/Oasis/Return", { id: id, patientId: patientId, reason: reason }, function(result) {
                        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
                        if (result.isSuccessful) {
                            Acore.CloseReturnReason();
                            Refresh.All(result);
                            window.Close();
                        } else Acore.ReturnReasonFailed();
                    }, function(result) {
                        U.Growl(result.errorMessage, "error");
                        Acore.ReturnReasonFailed();
                    });
                },
                CloseFunction: function() {
                    Acore.Windows[windowAcoreId].Close = undefined;
                }
            });
        }
    }
});