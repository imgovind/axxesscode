if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    Tabs: {
        AdlIadl: function (type) {
            U.ShowIfChecked($("#" + type + "_GenericAssistiveDevice5", this), $("#" + type + "_GenericAssistiveDevice5More", this));
            Oasis.FallAssessment.apply(this, arguments);
            Oasis.TinettiAssessment.apply(this, arguments);
        },
        Cardiac: function (type) {
            U.ShowIfChecked($("#" + type + "_GenericPittingEdemaType1", this), $("#" + type + "_GenericPittingEdemaType1More", this));
            U.ShowIfChecked($("#" + type + "_GenericPittingEdemaType2", this), $("#" + type + "_GenericPittingEdemaType2More", this));
            U.HideIfRadioEquals(type + "_M1500HeartFailureSymptons", "00|NA", $("#" + type + "_M1510", this));
        },
        Demographics: function (type) {
            var M0100 = $("[name=" + type + "_M0100AssessmentType]", this).val();
            $("[name=" + type + "_M0100AssessmentType]", this).on("click", function (event) {
                $("[name=" + type + "_M0100AssessmentType][value=" + M1000 + "]", this).prop("checked", true);
            });
            U.HideIfChecked($("#" + type + "_M0064PatientSSNUnknown1", this), $("#" + type + "_M0064PatientSSN", this));
            U.HideIfChecked($("#" + type + "_M0063PatientMedicareNumberUnknown1", this), $("#" + type + "_M0063PatientMedicareNumber", this));
            U.HideIfChecked($("#" + type + "_M0065PatientMedicaidNumberUnknown1", this), $("#" + type + "_M0065PatientMedicaidNumber", this));
            U.HideIfChecked($("#" + type + "_M0032ROCDateNotApplicable1", this), $("#" + type + "_M0032ROCDate", this).parent());
            U.HideIfChecked($("#" + type + "_M0018NationalProviderIdUnknown1", this), $("#" + type + "_M0018NationalProviderId", this));
            U.HideIfChecked($("#" + type + "_M0102PhysicianOrderedDateNotApplicable1", this), $("#" + type + "_M0102PhysicianOrderedDate", this).parent());
            U.NoneOfTheAbove($("#" + type + "_M0150PaymentSourceUnknown1", this), $("#" + type + "_M0150 .M0150", this));
            $("#" + type + "_PreviousAssessmentButton", this).on("click", function () {
                Oasis.LoadPrevious.apply(this, arguments);
            });
        },
        Elimination: function (type) {
            U.ShowIfChecked($("#" + type + "_GenericGUUrine5", this), $("#" + type + "_GenericGUUrine5More", this));
            U.ShowIfSelectEquals($("#" + type + "_GenericPatientOnDialysis", this), "1", $("#" + type + "_Dialysis", this));
            U.ShowIfChecked($("#" + type + "_GenericDialysisHemodialysis1", this), $("#" + type + "_GenericDialysisHemodialysis1More", this));
            U.ShowIfChecked($("#" + type + "_GenericDialysisHemodialysis2", this), $("#" + type + "_GenericDialysisHemodialysis2More", this));
            U.ShowIfRadioEquals(type + "_M1610UrinaryIncontinence", "01", $("#" + type + "_M1615", this));
        },
        EmergentCare: function (type) {
            U.HideIfRadioEquals(type + "_M2300EmergentCare", "00|UK", $("#" + type + "_M2310", this));
            U.NoneOfTheAbove($("#" + type + "_M2310ReasonForEmergentCareUK1", this), $("#" + type + "_M2310 .M2310", this));
        },
        Endocrine: function (type) {
            U.ShowIfSelectEquals($("#" + type + "_GenericPatientDiabetic", this), "1", $("#" + type + "_GenericPatientDiabeticMore", this));
            U.ShowIfSelectEquals($("#" + type + "_GenericInsulinDependent", this), "1", $("#" + type + "_GenericInsulinDependentMore", this));
            $("#" + type + "_GenericBloodSugarSiteText", this).Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
        },
        Integumentary: function (type) {
            var e = $(this);
            Oasis.BradenScale.apply(e, arguments);
            Oasis.Ulcers.apply(e, arguments);
            U.ShowIfSelectEquals($("#" + type + "_GenericPressureRelievingDevice", e), "1", $("#" + type + "_GenericPressureRelievingDeviceMore", e));
            U.ShowIfSelectEquals($("#" + type + "_GenericPressureRelievingDeviceType", e), "4", $("#" + type + "_GenericPressureRelievingDeviceTypeMore", e));
            U.ShowIfSelectEquals($("#" + type + "_GenericIVAccess", e), "1", $("#" + type + "_IVAccess", e));
            U.ShowIfSelectEquals($("#" + type + "_GenericFlush", e), "1", $("#" + type + "_GenericFlushMore", e));
            U.HideIfRadioEquals(type + "_M1300PressureUlcerAssessment", "00", $("#" + type + "_M1302", e));
            U.HideIfRadioEquals(type + "_M1306UnhealedPressureUlcers", "0", $("." + type + "_M1306_00", e));
            U.ShowIfRadioEquals(type + "_M1307NonEpithelializedStageTwoUlcer", "02", $("#" + type + "_M1307NonEpithelializedStageTwoUlcer02More", e));
            U.HideIfRadioEquals(type + "_M1330StasisUlcer", "00|03", $("#" + type + "_M1332,#" + type + "_M1334", e));
            U.HideIfRadioEquals(type + "_M1340SurgicalWound", "00|02", $("#" + type + "_M1342", e));
            var wounds = eval($(".wound-care", e).attr("wounds"));
            $(".wound-care", e).WoundChart({
                controller: window[$("[name=" + type + "_Service]", e).val()].Area + "/Oasis",
                id: $("[name=" + type + "_Id]", e).val(),
                patientId: $("[name=" + type + "_PatientGuid]", e).val(),
                type: type,
                wounds: wounds
            });
        },
        Medications: function (type) {
            U.HideIfRadioEquals(type + "_M2000DrugRegimenReview", "00|01|NA", $("#" + type + "_M2002", this));
            U.HideIfRadioEquals(type + "_M2000DrugRegimenReview", "NA", $("#" + type + "_M2010,#" + type + "_M2020,#" + type + "_M2030", this));
        },
        NeuroBehavioral: function (type) {
            U.ShowIfRadioEquals(type + "_M1730DepressionScreening", "01", $("#" + type + "_Phq2", this));
            U.NoneOfTheAbove($("#" + type + "_M1740CognitiveBehavioralPsychiatricSymptomsNone1", this), $("#" + type + "_M1740 .M1740", this));
            U.ShowIfSelectEquals($("#" + type + "_485MSWProviderServicesVisits", this), "0", $("#" + type + "_485MSWProviderServicesVisitsAmount", this));
            U.ShowIfSelectEquals($("#" + type + "_485MSWLongTermPlanningVisits", this), "0", $("#" + type + "_485MSWLongTermPlanningVisitsAmount", this));
            U.ShowIfSelectEquals($("#" + type + "_485MSWCommunityAssistanceVisits", this), "0", $("#" + type + "_485MSWCommunityAssistanceVisitAmount", this));
        },
        Nutrition: function (type) {
            Oasis.NutritionScore.apply(this, arguments);
            U.ShowIfChecked($("#" + type + "_485NutritionalReqsEnteralPer4", this), $("#" + type + "_485NutritionalReqsEnteralPer4More", this));
        },
        OrdersDisciplineTreatment: function (type) {
            Oasis.MasterCalendar.apply(this, arguments);
            $("#" + type + "_485SIVerbalizedUnderstandingPercent", this).Autocomplete({ source: ["GOOD", "NO", "FAIR", "POOR", "0%", "100%", "50%", "FULL", "PARTIAL"] })
        },
        Pain: function (type) {
            $(".pain-picker", this).PainPicker();
        },
        PatientHistory: function (type) {
            VitalSign.Init($("#newOasis" + type + "PatientHistoryForm .vital-signs", this), true);
            U.NoneOfTheAbove($("#" + type + "_M1000InpatientFacilitiesNone1", this), $("#" + type + "_M1000 .M1000", this));
            U.HideIfChecked($("#" + type + "_M1000InpatientFacilitiesNone1", this), $("#" + type + "_M1005,#" + type + "_M1010,#" + type + "_M1012", this).closest("fieldset"));
            U.HideIfChecked($("#" + type + "_M1005InpatientDischargeDateUnknown1", this), $("#" + type + "_M1005InpatientDischargeDate", this).parent());
            U.NoneOfTheAbove($("#" + type + "_M1012InpatientFacilityProcedureCodeNotApplicable1", this), $("#" + type + "_M1012InpatientFacilityProcedureCodeUnknown1", this));
            U.NoneOfTheAbove($("#" + type + "_M1012InpatientFacilityProcedureCodeUnknown1", this), $("#" + type + "_M1012InpatientFacilityProcedureCodeNotApplicable1", this));
            U.HideIfChecked($("#" + type + "_M1012InpatientFacilityProcedureCodeNotApplicable1,#" + type + "_M1012InpatientFacilityProcedureCodeUnknown1", this), $("#" + type + "_M1012 ul", this));
            U.HideIfChecked($("#" + type + "_M1016MedicalRegimenDiagnosisNotApplicable1", this), $("#" + type + "_M1016 ul", this));
            U.NoneOfTheAbove($("#" + type + "_M1018ConditionsPriorToMedicalRegimenNone1", this), $("#" + type + "_M1018 .M1018", this));
            U.NoneOfTheAbove($("#" + type + "_M1018ConditionsPriorToMedicalRegimenNA1", this), $("#" + type + "_M1018 .M1018", this));
            U.NoneOfTheAbove($("#" + type + "_M1018ConditionsPriorToMedicalRegimenUK1", this), $("#" + type + "_M1018 .M1018", this));
            U.NoneOfTheAbove($("#" + type + "_M1030HomeTherapiesNone1", this), $("#" + type + "_M1030 .M1030", this));
            $("#" + type + "_OasisDiagnoses", this).DiagnosesList()
        },
        Respiratory: function (type) {
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds1", this), $("#" + type + "_GenericLungSoundsCTAText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds2", this), $("#" + type + "_GenericLungSoundsRalesText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds3", this), $("#" + type + "_GenericLungSoundsRhonchiText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds4", this), $("#" + type + "_GenericLungSoundsWheezesText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds5", this), $("#" + type + "_GenericLungSoundsCracklesText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds6", this), $("#" + type + "_GenericLungSoundsDiminishedText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds7", this), $("#" + type + "_GenericLungSoundsAbsentText", this));
            U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds8", this), $("#" + type + "_GenericLungSoundsStridorText", this));
            U.NoneOfTheAbove($("#" + type + "_M1410HomeRespiratoryTreatmentsNone1", this), $("#" + type + "_M1410 .M1410", this));
        },
        RiskAssessment: function (type) {
            U.NoneOfTheAbove($("#" + type + "_M1032HospitalizationRiskNone1", this), $("#" + type + "_M1032 .M1032", this));
            U.NoneOfTheAbove($("#" + type + "_M1036RiskFactorsNone1", this), $("#" + type + "_M1036 .M1036", this));
            U.NoneOfTheAbove($("#" + type + "_M1036RiskFactorsUnknown1", this), $("#" + type + "_M1036 .M1036", this));
            U.HideIfRadioEquals(type + "_M1040InfluenzaVaccine", "01|NA", $("#" + type + "_M1045", this));
        },
        SupportiveAssistance: function (type) {
            U.ShowIfSelectEquals($("#" + type + "_GenericLivingSituation", this), "a", $("#" + type + "_M1100LivingSituation.a", this));
            U.ShowIfSelectEquals($("#" + type + "_GenericLivingSituation", this), "b", $("#" + type + "_M1100LivingSituation.b", this));
            U.ShowIfSelectEquals($("#" + type + "_GenericLivingSituation", this), "c", $("#" + type + "_M1100LivingSituation.c", this));
            U.HideIfSelectEquals($("#" + type + "_GenericLivingSituation", this), "", $("#" + type + "_GenericLivingSituationMore", this));
            U.ShowIfSelectEquals($("#" + type + "_GenericCulturalPractices", this), "1", $("#" + type + "_GenericCulturalPracticesYesMore", this));
            U.ShowIfSelectEquals($("#" + type + "_GenericIsHomeBound", this), "Yes", $("#" + type + "_GenericIsHomeBoundMore", this));
            Lookup.Language("#" + type + "_GenericPrimaryLanguage", this);
        },
        TherapyNeed: function (type) {
            U.HideIfChecked($("#" + type + "_M2200TherapyNeedNA1", this), $("#" + type + "_M2200Right", this));
        },
        TransferDischargeDeath: function (type) {
            U.HideIfRadioEquals(type + "_M2410TypeOfInpatientFacility", "02|03|04|NA", $("#" + type + "_M2430", this));
            U.HideIfRadioEquals(type + "_M2410TypeOfInpatientFacility", "01|02|04|NA", $("#" + type + "_M2440", this));
        },
        SuppliesDme: function (type) {
            Oasis.Supply.InitList(type, this);
        }
    }
});