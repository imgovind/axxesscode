if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    Export: {
        OnDataBound: function (e) {
            U.OnTGridDataBound(e);
            var grid = $(e.target).data('tGrid');
            if (grid) {
                var datas = grid.data;
                if (datas.length > 0) {
                    var data = datas[0];
                    if (data != null) {
                        if (data.IsUserCanGenerate) grid.showColumn("IsUserCanGenerate");
                        else grid.hideColumn("IsUserCanGenerate");
                        if (data.IsUserCanReopen) grid.showColumn("IsUserCanReopen");
                        else grid.hideColumn("IsUserCanReopen");
                    }
                } else {
                    grid.hideColumn("IsUserCanGenerate");
                    grid.hideColumn("IsUserCanReopen");
                }
            }
        },
        OnRowDataBound: function (e) {
            var dataItem = e.dataItem;
            if (dataItem.IsUserCanGenerate) $("td.cancel", e.row).html("<a onclick=\"Oasis.LoadCancel('" + dataItem.AssessmentId + "','" + dataItem.Service + "');\" >Generate Cancel</a>");
            if (dataItem.IsUserCanReopen) $("td.action", e.row).html("<a onclick=\"Oasis.Reopen('" + dataItem.AssessmentId + "','" + dataItem.PatientId + "','" + dataItem.EpisodeId + "','+ReOpen','" + dataItem.Service + "');\" >Reopen</a>");
        }
    }
});