if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    Validation: {
        Init: function (r, t, x, e) {
            var prefix = window[$("[name=Service]", e).val()].Area;
            $("form", e).Validate({
                PreprocessForm: function (arr, $form) {
                    var type = $("[name=OasisValidationType]", $form).val();
                    var tempTimeIn = $("#" + prefix + "_TimeIn", $form).val();
                    var tempTimeOut = $("#" + prefix + "_TimeOut", $form).val();
                    var timeIn = U.ConvertToTime(tempTimeIn);
                    var timeOut = U.ConvertToTime(tempTimeOut);
                    return U.CheckTimeInOut(timeIn, timeOut, false);
                },
                Success: function (result) {
                    if (result.isSuccessful) {
                        $("#window_" + $("[name=OasisValidationType]", e).val()).Close();
                        Oasis[$("[name=Service]", e).val()].HandleRefresh(result);
                        Acore.CloseDialog();
                    }
                }
            });
            $(".error-anchor", e).on("click", function () {
                Oasis.GotoQuestion($(this).attr("question"), $("[name=OasisValidationType]", e).val(), $(this).attr("version"));
                Acore.CloseDialog();
            });
            $(".home-health-gold", e).on("click", function () {
                U.GetAttachment(prefix + "/Oasis/AuditPdf", { id: $("[name=Id]", e).val(), patientId: $("[name=PatientId]", e).val(), episodeId: $("[name=EpisodeId]", e).val(), assessmentType: $("[name=OasisValidationType]", e).val() });
            }).tooltip({
                track: true,
                showURL: false,
                top: 25,
                left: 25,
                bodyHandler: function () {
                    return "<div class='strong'>Powered by</div><img src='/Images/hhg-logo.png'/>";
                }
            });
            $(".oasis-export-file", e).on("click", function () {
                U.GetAttachment(prefix + "/Oasis/GenerateExportFile", { assessmentId: $("[name=Id]", e).val() });
            });
            $(".pps-plus-export", e).on("click", function () {
                U.PostUrl("Oasis/PpsExport", { assessmentId: $("[name=Id]", e).val(), assessmentType: $("[name=OasisValidationType]", e).val() }, function (result) {
                    if (result.isSuccessful) U.Growl(result.errorMessage, "success");
                    else U.Growl(result.errorMessage, "error");
                })
                if (prefix != "PrivateDuty") $("#print-controls li:first", this).addClass("red").Tooltip({
                    top: 25,
                    left: 25,
                    bodyHandler: function () {
                        return "<div class='strong'>Powered by</div><img src='/Images/hhg-logo.png'/>";
                    }
                })
            });
        },
        Load: function (id, patientId, episodeId, type, pps, prefix) {
            Acore.Modal({
                Name: "OASIS Validation",
                Url: prefix + "/Oasis/Validate",
                Inputs: { id: id, patientId: patientId, episodeId: episodeId, type: type, pps: pps },
                OnLoad: Oasis.Validation.Init,
                Width: 1000
            })
        }
    }
});