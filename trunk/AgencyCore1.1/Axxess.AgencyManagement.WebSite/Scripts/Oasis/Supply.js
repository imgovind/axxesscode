if (typeof Oasis == "undefined") var Oasis = new Object();
$.extend(Oasis, {
    Supply: {
        New: function (input, prefix, grid) {
            Acore.Modal({
                Name: "OASIS New Supply",
                Url: prefix + "/Oasis/NewSupply",
                Inputs: input,
                Width: 520,
                OnLoad: function (r, t, x, e) { Oasis.Supply.Init(r, t, x, e, grid) }
            })
        },
        Init: function (r, t, x, e, grid) {
            Supply.Search($('form', e).Validate({ Success: function () { Oasis.Supply.RebindList(grid) } }));
        },
        InitList: function (type, e) {
            var input = { patientId: $("#" + type + "_PatientGuid", e).val(), eventId: $("#" + type + "_Id", e).val() },
                area = Acore.Windows[type].Area,
                grid = $("#" + type + "_SupplyGrid", e);
            $(".add-supply-button", e).on("click", function () {
                Oasis.Supply.New(input, area, grid);
            });
            $(grid).on("click", "td.action a.edit", function () {
                input["supplyId"] = $(this).closest("tr").find("td.uid").text();
                Oasis.Supply.Edit(input, area, grid);
            });
            $(grid).on("click", "td.action a.delete", function () {
                input["supplyId"] = $(this).closest("tr").find("td.uid").text();
                Oasis.Supply.Delete(input, area, grid);
            });
            U.StopTGridFirstAction(grid);
        },
        RebindList: function (e) {
            var grid = e.is(".t-grid") ? e : $('.t-grid', e)
            grid.data('tGrid').rebind();
        },
        Edit: function (inputs, area, grid) {
            Acore.Modal({
                Name: "OASIS Edit Supply",
                Url: area + "/Oasis/EditSupply",
                Inputs: inputs,
                Width: 520,
                OnLoad: function (r, t, x, e) { Oasis.Supply.Init(r, t, x, e, grid) }
            })
        },
        Delete: function (inputs, area, grid) {
            if (inputs) U.Delete("Supply", (area || "") + "/Oasis/DeleteSupply", inputs, function () { Oasis.Supply.RebindList(grid) }, false);
        }
    }
});