﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Active Users Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
<% if (!Current.IsIpAddressRestricted) { %>
    <% var activeUsers = Current.ActiveUsers; %>
    <% var inActiveUsers = Current.InActiveUsers; %>
    <% if (activeUsers != null && inActiveUsers != null) { %>
        <%= string.Format("AXDALCEWEB1001 (10.0.5.41) with {0} active and {1} inactive users", activeUsers.Where(x => x.ServerName == "AXDALCEWEB1001").Count(), inActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1001").Count())%><br />
        <%= string.Format("AXDALCEWEB1002 (10.0.5.42) with {0} active and {1} inactive users", activeUsers.Where(x => x.ServerName == "AXDALCEWEB1002").Count(), inActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1002").Count())%><br />
        <%= string.Format("AXDALCEWEB1003 (10.0.5.43) with {0} active and {1} inactive users", activeUsers.Where(x => x.ServerName == "AXDALCEWEB1003").Count(), inActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1003").Count())%><br />
        <%= string.Format("AXDALCEWEB1004 (10.0.5.44) with {0} active and {1} inactive users", activeUsers.Where(x => x.ServerName == "AXDALCEWEB1004").Count(), inActiveUsers.Where(x => x.ServerName == "AXDALCEWEB1004").Count())%><br /><br />
        <%= string.Format("Total Active {0} and Total Inactive {1}", activeUsers.Count, inActiveUsers.Count) %>
    <% } %>
    <br /><br />
    <table id="newspaper-a">
            <thead>
                <tr>
                    <th style="width: 20%; text-align: left;">Email</th>
                    <th style="width: 15%; text-align: left;">Name</th>
                    <th style="width: 20%; text-align: left;">Agency</th>
                    <th style="width: 10%; text-align: left;">IP Address</th>
                    <th style="width: 10%; text-align: left;">Server</th>
                    <th style="width: 10%; text-align: left;">Session Id</th>
                    <th style="width: 15%; text-align: left;">Last Activity</th>
                </tr>
            </thead>
            <tbody>
            <% if (activeUsers != null && activeUsers.Count > 0) {
                activeUsers.ForEach(user => { %>
                    <tr><td><%= user.EmailAddress %></td><td><%= user.FullName%></td><td><%= user.AgencyName %></td><td><%= user.IpAddress %></td><td><%= user.ServerName %></td><td><%= user.SessionId %></td><td><%= user.LastSecureActivity.ToString("MM/dd/yyyy @ hh:mm:ss:tt") %></td></tr>
            <% });
            } else { %>
                <tr><td colspan="6" align="center" style="color: Red;">There are no active users in the system.</td></tr>
            <% } %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="6"><em>Total: <%= activeUsers != null ? activeUsers.Count : 0 %></em></td></tr>
            </tfoot>
        </table>
    <table id="newspaper-b">
            <thead>
                <tr>
                    <th style="width: 20%; text-align: left;">Email</th>
                    <th style="width: 15%; text-align: left;">Name</th>
                    <th style="width: 20%; text-align: left;">Agency</th>
                    <th style="width: 10%; text-align: left;">IP Address</th>
                    <th style="width: 10%; text-align: left;">Server</th>
                    <th style="width: 10%; text-align: left;">Session Id</th>
                    <th style="width: 15%; text-align: left;">Last Activity</th>
                </tr>
            </thead>
            <tbody>
            <% if (inActiveUsers != null && inActiveUsers.Count > 0) {
                inActiveUsers.ForEach(user => { %>
                    <tr><td><%= user.EmailAddress %></td><td><%= user.FullName%></td><td><%= user.AgencyName %></td><td><%= user.IpAddress %></td><td><%= user.ServerName %></td><td><%= user.SessionId %></td><td><%= user.LastSecureActivity.ToString("MM/dd/yyyy @ hh:mm:ss:tt") %></td></tr>
            <% });
            } else { %>
                <tr><td colspan="6" align="center" style="color: Red;">There are no inactive users in the system.</td></tr>
            <% } %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="6"><em>Total: <%= inActiveUsers != null ? inActiveUsers.Count : 0 %></em></td></tr>
            </tfoot>
        </table>
<% } %>
</asp:Content>