﻿namespace Axxess.Api.Oasis
{
    using System;
    using System.Diagnostics;
    using System.ServiceProcess;
    using System.Configuration;

    using Axxess.Api.Contracts;
    using HomeHealthGold.Audits;

    static class Program
    {
        static void Main()
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
		        { 
			        new ValidationWindowsService()
		        };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
