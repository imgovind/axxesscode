﻿namespace Axxess.Support.Application.Exports
{
    using System;
    using System.IO;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    public abstract class BaseExporter : IDisposable
    {
        #region Constructor

        protected HSSFWorkbook workBook;

        public BaseExporter()
        {
            workBook = new HSSFWorkbook();
        }

        #endregion

        #region Methods

        public MemoryStream Process()
        {
            this.Initialize();
            this.Write();
            return this.Stream();
        }

        protected virtual void Initialize()
        {
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;
        }

        protected abstract void Write();

        protected virtual MemoryStream Stream()
        {
            MemoryStream memoryStream = new MemoryStream();
            workBook.Write(memoryStream);
            return memoryStream;
        }

        #endregion

        #region IDisposable

        private bool isDisposed;
        protected bool IsDisposed
        {
            get { return isDisposed; }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.IsDisposed)
                return;

            if (disposing)
            {
                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
