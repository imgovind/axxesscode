﻿namespace Axxess.Support.Application
{
    using System;
    using System.Web;
    using System.Reflection;
    using System.Security.Principal;
    using System.Collections.Generic;

    using Enums;
    using Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;

    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    public static class Current
    {
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        public static Func<DateTime> Time = () => DateTime.UtcNow;

        public static AxxessSupportIdentity User
        {
            get
            {
                AxxessSupportIdentity identity = null;
                if (HttpContext.Current.User is WindowsPrincipal)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessSupportPrincipal)
                {
                    AxxessSupportPrincipal principal = (AxxessSupportPrincipal)HttpContext.Current.User;
                    identity = (AxxessSupportIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DisplayName;
                }

                return string.Empty;
            }
        }

        public static string IpAddress
        {
            get
            {
                var ipAddress = string.Empty;

                if (HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR").IsNotNullOrEmpty())
                {
                    ipAddress = HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
                }
                else
                {
                    ipAddress = HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR");
                }
                return ipAddress;
            }
        }

        public static bool IsAxxessAdmin
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.IsAxxessAdmin;
                }
                return false;
            }
        }

        public static bool IsAxxessSupport
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.IsAxxessSupport;
                }
                return false;
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            }
        }

        public static bool IsIpAddressRestricted
        {
            get
            {
                var result = true;

                if (Current.IpAddress.StartsWith("10.0.1")
                    || Current.IpAddress.StartsWith("97.79.164.66")
                    || Current.IpAddress.StartsWith("10.0.5")
                    || Current.IpAddress.IsEqual("127.0.0.1"))
                {
                    result = false;
                }
                return result;
            }
        }

        public static List<SupportUser> ActiveUsers
        {
            get
            {
                return authenticationAgent.GetSupportUsers(true);
            }
        }

        public static List<SupportUser> InActiveUsers
        {
            get
            {
                return authenticationAgent.GetSupportUsers(false);
            }
        }
    }
}
