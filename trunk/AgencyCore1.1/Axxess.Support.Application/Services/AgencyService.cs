﻿namespace Axxess.Support.Application.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Support.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class AgencyService : IAgencyService
    {
        #region Private Members /Constructor

        private readonly ILoginRepository loginRepository;
        private readonly ISupportAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public AgencyService(ISupportAgencyRepository agencyRepository, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyRepository, "agencyRepository");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.agencyRepository = agencyRepository;
        }

        #endregion

        #region IAgencyService Members

        public bool CreateAgency(Agency agency)
        {
            try
            {
                if (agency != null && agency.DBServerIp.IsNotNullOrEmpty() && agency.DBServerIp.IsValidDate())
                {
                    var submitterInfo = agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger() ? lookupRepository.SubmitterInfo(agency.Payor.ToInteger()) : new AxxessSubmitterInfo();
                    agency.IsSuspended = false;
                    if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                    {
                        agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                    }

                    if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                    {
                        agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                    {
                        agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                    if (agency.IsAxxessTheBiller)
                    {
                        agency.SubmitterId = submitterInfo != null && submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : agency.SubmitterId;
                        agency.SubmitterName = submitterInfo != null && submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : agency.SubmitterId;
                        agency.SubmitterPhone = submitterInfo != null && submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : agency.SubmitterPhone;
                        agency.SubmitterFax = submitterInfo != null && submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : agency.SubmitterFax;
                    }

                    if (agencyRepository.Add(agency.DBServerIp, agency))
                    {
                        agencyRepository.InsertTemplates(agency.DBServerIp, agency.Id);
                        agencyRepository.InsertSupplies(agency.DBServerIp, agency.Id);

                        var location = new AgencyLocation();
                        location.Id = Guid.NewGuid();
                        location.Name = agency.LocationName;
                        location.AddressLine1 = agency.AddressLine1;
                        location.AddressLine2 = agency.AddressLine2;
                        location.AddressCity = agency.AddressCity;
                        location.AddressStateCode = agency.AddressStateCode;
                        location.AddressZipCode = agency.AddressZipCode;
                        location.AddressZipCodeFour = agency.AddressZipCodeFour;
                        location.AgencyId = agency.Id;
                        location.IsMainOffice = true;
                        location.IsDeprecated = false;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                        location.SubmitterId = agency.SubmitterId;
                        location.SubmitterName = agency.SubmitterName;
                        location.Payor = agency.Payor;
                        location.SubmitterPhone = agency.SubmitterPhone;
                        location.SubmitterFax = agency.SubmitterFax;
                        location.IsSubmitterInfoTheSame = true;
                        location.IsAxxessTheBiller = agency.IsAxxessTheBiller;

                        if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                        {
                            location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                        }
                        if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                        {
                            location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                        }

                        string subject = string.Format("{0} needs to be trained.", agency.Name);
                        string bodyText = MessageBuilder.PrepareTextFrom("TrainingRequest",
                            "displayname", loginRepository.GetLoginDisplayName(agency.Trainer),
                            "agencyname", agency.Name,
                            "agencyphone", location.PhoneWorkFormatted,
                            "salesperson", loginRepository.GetLoginDisplayName(agency.SalesPerson));

                        Notify.User(CoreSettings.NoReplyEmail, loginRepository.GetLoginEmailAddress(agency.Trainer), string.Format("{0};{1}", loginRepository.GetLoginEmailAddress(agency.BackupTrainer), loginRepository.GetLoginEmailAddress(agency.SalesPerson)), subject, bodyText);

                        var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
                        if (defaultDisciplineTasks != null)
                        {
                            var costRates = new List<ChargeRate>();
                            defaultDisciplineTasks.ForEach(r =>
                            {
                                if (r.Discipline.IsEqual("Nursing") || r.Discipline.IsEqual("PT") || r.Discipline.IsEqual("OT") || r.Discipline.IsEqual("ST") || r.Discipline.IsEqual("HHA") || r.Discipline.IsEqual("MSW"))
                                {
                                    costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(), Charge = r.Rate });
                                }
                            });
                            location.BillData = costRates.ToXml();
                        }

                        string cbsa = lookupRepository.CbsaCodeByZip(agency.AddressZipCode);
                        location.CBSA = cbsa != null ? cbsa : string.Empty;

                        if (agencyRepository.AddLocation(agency.DBServerIp, location))
                        {
                            var user = new User
                            {
                                AgencyId = agency.Id,
                                AgencyName = agency.Name,
                                AllowWeekendAccess = true,
                                EmploymentType = "Employee",
                                AgencyLocationId = location.Id,
                                Status = (int)UserStatus.Active,
                                LastName = agency.AgencyAdminLastName,
                                FirstName = agency.AgencyAdminFirstName,
                                PermissionsArray = GeneratePermissions(),
                                EmailAddress = agency.AgencyAdminUsername,
                                Credentials = CredentialTypes.None.GetDescription(),
                                TitleType = TitleTypes.Administrator.GetDescription(),
                                Profile = new UserProfile(),
                                AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                            };

                            IUserService userService = Container.Resolve<IUserService>();
                            return userService.CreateUser(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        public bool UpdateAgency(Agency agency)
        {
            bool result = false;
            var agencySnapShot = loginRepository.GetAgencySnapShot(agency.Id);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
            {
                if (agency != null)
                {
                    var existingAgency = agencyRepository.GetAgencyOnly(agencySnapShot.DBServerIp,agency.Id);
                    if (existingAgency != null)
                    {
                        existingAgency.Name = agency.Name;
                        existingAgency.TaxId = agency.TaxId;
                        existingAgency.IsAgreementSigned = agency.IsAgreementSigned;
                        existingAgency.TrialPeriod = agency.TrialPeriod;
                        existingAgency.Package = agency.Package;
                        existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                        existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                        existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                        existingAgency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                        existingAgency.TaxIdType = agency.TaxIdType;
                        existingAgency.Payor = agency.Payor;
                        existingAgency.SubmitterId = agency.SubmitterId;
                        existingAgency.SubmitterName = agency.SubmitterName;
                        existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                        existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                        existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                        existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                        existingAgency.CahpsVendor = agency.CahpsVendor;
                        existingAgency.IsAxxessTheBiller = agency.IsAxxessTheBiller;

                        existingAgency.SalesPerson = agency.SalesPerson;
                        existingAgency.Trainer = agency.Trainer;

                        if (!agency.IsAxxessTheBiller)
                        {
                            existingAgency.SubmitterId = agency.SubmitterId;
                            existingAgency.SubmitterName = agency.SubmitterName;
                            existingAgency.Payor = agency.Payor;
                            if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count > 0)
                            {
                                existingAgency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                            }
                            if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count > 0)
                            {
                                existingAgency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                            }
                        }
                        if (agencyRepository.Update(agencySnapShot.DBServerIp,existingAgency))
                        {
                            var mainLocation = agencyRepository.GetMainLocation(agencySnapShot.DBServerIp,agency.Id);
                            if (mainLocation != null)
                            {
                                mainLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                if (agencyRepository.UpdateLocation(agencySnapShot.DBServerIp,mainLocation))
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            try
            {
                 var agencySnapShot = loginRepository.GetAgencySnapShot(location.AgencyId);
                 if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                 {
                     location.Id = Guid.NewGuid();

                     if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                     {
                         location.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                     }

                     if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                     {
                         location.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                     }
                     if (location.IsLocationStandAlone)
                     {
                         if (location.IsAxxessTheBiller)
                         {
                             var submitterInfo = location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger() ? lookupRepository.SubmitterInfo(location.Payor.ToInteger()) : new AxxessSubmitterInfo();
                             // var agency = agencyRepository.Get(location.AgencyId);
                             if (submitterInfo != null)
                             {
                                 //location.SubmitterId = agency.SubmitterId;
                                 //location.SubmitterName = agency.SubmitterName;
                                 //location.Payor = agency.Payor;
                                 //location.SubmitterPhone = agency.SubmitterPhone;
                                 //location.SubmitterFax = agency.SubmitterFax;

                                 location.SubmitterId = submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : location.SubmitterId;
                                 location.SubmitterName = submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : location.SubmitterId;
                                 location.SubmitterPhone = submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : location.SubmitterPhone;
                                 location.SubmitterFax = submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : location.SubmitterFax;
                             }
                             else
                             {
                                 return false;
                             }
                         }

                     }
                     string cbsa = lookupRepository.CbsaCodeByZip(location.AddressZipCode);
                     location.CBSA = cbsa != null ? cbsa : string.Empty;
                     if (agencyRepository.AddLocation(agencySnapShot.DBServerIp,location))
                     {
                         result = true;
                     }
                 }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return result;
        }

        public bool UpdateLocation(AgencyLocation location)
        {
            var result = false;
             var agencySnapShot = loginRepository.GetAgencySnapShot(location.AgencyId);
             if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
             {
                 if (location != null)
                 {
                     var existingLocation = agencyRepository.FindLocation(agencySnapShot.DBServerIp,location.AgencyId, location.Id);// database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
                     if (existingLocation != null)
                     {
                         if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                         {
                             existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                         }
                         if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                         {
                             existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                         }
                         //existingLocation.IsSubmitterInfoTheSame = location.IsSubmitterInfoTheSame;
                         existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                         if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                         {
                             existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                         }

                         if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                         {
                             existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                         }
                         if (location.ContactPhoneArray != null && location.ContactPhoneArray.Count == 3)
                         {
                             existingLocation.ContactPersonPhone = location.ContactPhoneArray.ToArray().PhoneEncode();
                         }
                         existingLocation.IsAxxessTheBiller = location.IsAxxessTheBiller;
                         if (existingLocation.IsLocationStandAlone)
                         {
                             if (existingLocation.IsAxxessTheBiller)
                             {
                                 var submitterInfo = location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger() ? lookupRepository.SubmitterInfo(location.Payor.ToInteger()) : new AxxessSubmitterInfo();
                                 // var agency = agencyRepository.Get(location.AgencyId);
                                 if (submitterInfo != null)
                                 {
                                     //location.SubmitterId = agency.SubmitterId;
                                     //location.SubmitterName = agency.SubmitterName;
                                     //location.Payor = agency.Payor;
                                     //location.SubmitterPhone = agency.SubmitterPhone;
                                     //location.SubmitterFax = agency.SubmitterFax;
                                     existingLocation.Payor = location.Payor;
                                     existingLocation.SubmitterId = submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : location.SubmitterId;
                                     existingLocation.SubmitterName = submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : location.SubmitterName;
                                     existingLocation.SubmitterPhone = submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : location.SubmitterPhone;
                                     existingLocation.SubmitterFax = submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : location.SubmitterFax;
                                 }
                                 else
                                 {
                                     return false;
                                 }
                             }
                             else
                             {
                                 existingLocation.Payor = location.Payor;
                                 existingLocation.SubmitterId = location.SubmitterId;
                                 existingLocation.SubmitterName = location.SubmitterName;
                                 // existingLocation.SubmitterPhone = location.SubmitterPhone;
                                 // existingLocation.SubmitterFax = location.SubmitterFax;
                             }

                             existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                             existingLocation.TaxId = location.TaxId;
                             existingLocation.TaxIdType = location.TaxIdType;
                             existingLocation.NationalProviderNumber = location.NationalProviderNumber;
                             existingLocation.MedicaidProviderNumber = location.MedicaidProviderNumber;
                             existingLocation.HomeHealthAgencyId = location.HomeHealthAgencyId;
                             existingLocation.ContactPersonFirstName = location.ContactPersonFirstName;
                             existingLocation.ContactPersonLastName = location.ContactPersonLastName;
                             existingLocation.ContactPersonEmail = location.ContactPersonEmail;
                             //existingLocation.ContactPersonPhone = location.ContactPersonPhone;
                             existingLocation.CahpsVendor = location.CahpsVendor;
                             existingLocation.CahpsVendorId = location.CahpsVendorId;
                             existingLocation.CahpsSurveyDesignator = location.CahpsSurveyDesignator;
                             existingLocation.IsAxxessTheBiller = location.IsAxxessTheBiller;
                             existingLocation.OasisAuditVendor = location.OasisAuditVendor;
                         }
                         existingLocation.BranchId = location.BranchId;
                         existingLocation.BranchIdOther = location.BranchIdOther;
                         existingLocation.Name = location.Name;
                         existingLocation.CustomId = location.CustomId;
                         //existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                         existingLocation.AddressLine1 = location.AddressLine1;
                         existingLocation.AddressLine2 = location.AddressLine2;
                         existingLocation.AddressCity = location.AddressCity;
                         existingLocation.AddressStateCode = location.AddressStateCode;
                         existingLocation.AddressZipCode = location.AddressZipCode;
                         existingLocation.AddressZipCodeFour = location.AddressZipCodeFour;
                         existingLocation.Comments = location.Comments;
                         result = agencyRepository.EditLocationModal(agencySnapShot.DBServerIp,existingLocation);
                     }
                 }
             }
            return result;
        }

        //public Agency GetAgency(Guid Id)
        //{
        //    Agency agencyViewData = agencyRepository.Get(Id);
        //    if (agencyViewData != null)
        //    {
        //        return agencyViewData;
        //    }
        //    return null;
        //}

        #endregion

        #region Move data

        public bool LoadFactory(bool IsAll, int actionType, string fromServerIp, string toServerIp, List<Guid> agencyIds, List<int> tablesToMove, List<int> tables)
        {
            if (IsAll)
            {
                var agencies = loginRepository.GetAgencySnapShots();
                if (agencies != null && agencies.Count > 0)
                {
                    if (actionType == 1)
                    {
                        this.LoadNewTableSchema(fromServerIp, toServerIp, agencyIds, tablesToMove);
                    }
                    else if (actionType == 2)
                    {
                        this.Load(fromServerIp, toServerIp, agencies.Select(a=>a.Id).Distinct().ToList(), tables);
                    }
                }
            }
            else {
                if (agencyIds != null && agencyIds.Count > 0)
                {
                    if (actionType == 1)
                    {
                        this.LoadNewTableSchema(fromServerIp, toServerIp, agencyIds, tablesToMove);
                    }
                    else if (actionType == 2)
                    {
                        this.Load(fromServerIp, toServerIp, agencyIds, tables);
                    }
                }
            }
            return false;
        }

        public void LoadNewTableSchema(string fromDataBaseIp, string toDataBaseIp, List<Guid> agencyIds, List<int> tablesToMove)
        {
            if (tablesToMove != null && tablesToMove.Count > 0)
            {
                var typeNames = tablesToMove.Where(t => Enum.IsDefined(typeof(TableToMove), t)).Select(t => ((TableToMove)t));
                if (typeNames != null && typeNames.Count() > 0)
                {
                    if (agencyIds != null && agencyIds.Count > 0)
                    {
                        agencyIds.ForEach(agencyId =>
                      {
                          
                          typeNames.ForEach(tp =>
                         {
                             NewTableSchemaFactory(tp, fromDataBaseIp, toDataBaseIp,  new List<Guid>{ agencyId});
                         });
                      });
                    }
                }
            }
        }


        public void Load(string fromDataBaseIp, string toDataBaseIp, List<Guid> agencyIds, List<int> tables)
        {
            if (tables != null && tables.Count > 0)
            {
                var typeNames = tables.Where(t => Enum.IsDefined(typeof(Tables), t)).Select(t => ((Tables)t));
                if (typeNames != null && typeNames.Count() > 0)
                {
                    if (typeNames.Contains(Tables.Agencies))
                    {
                        var agencies = agencyRepository.All(fromDataBaseIp).ToList();
                        if (agencies != null && agencies.Count > 0)
                        {
                            agencyRepository.AddMultiAgency(toDataBaseIp, agencies);
                        }
                        typeNames = typeNames.Where(t => t != Tables.Agencies);
                    }
                    if (typeNames != null && typeNames.Count() > 0)
                    {
                        agencyIds.ForEach(agencyId =>
                        {

                            typeNames.ForEach(tp =>
                            {
                                AddEntityFactory(tp, fromDataBaseIp, toDataBaseIp, agencyId);
                            });
                        });
                    }
                    
                }
            }
        }

        public void Load(string fromDataBaseIp, string toDatabaseIp,Guid agencyId, List<int> tables)
        {
            if (tables != null && tables.Count > 0)
            {
                var typeNames = tables.Where(t => Enum.IsDefined(typeof(Tables), t)).Select(t => ((Tables)t));
                if (typeNames != null && typeNames.Count() > 0)
                {
                    
                        typeNames.ForEach(tp =>
                            {
                                AddEntityFactory(tp, fromDataBaseIp, toDatabaseIp, agencyId);
                            });
                }
            }
        }

        public bool MoveAgencyEpisodeScheduleToFlat(string fromDataBaseIp, string toDatabaseIp, Guid agencyId)
        {
            var result = false;
            if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
            {
                var episodes = agencyRepository.GetAgencyPatientEpisodes(fromDataBaseIp, agencyId);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(ep =>
                    {
                        var schedules = ep.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedules != null && schedules.Count > 0)
                        {
                            result = agencyRepository.AddSchedules(toDatabaseIp, schedules);
                        }
                    });

                }
            }
            return result;
        }

        public bool MoveAgencyEpisodeScheduleToFlat(string fromDataBaseIp, string toDatabaseIp,List<Guid> agencyIds,List<Guid> Ids)
        {
            var result = false;
            if (agencyIds != null && agencyIds.Count > 0)
            {
                if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
                {
                    Ids=Ids??new List<Guid>();
                    agencyIds.ForEach(Id =>
                    {
                            var episodes = agencyRepository.GetAgencyPatientEpisodes(fromDataBaseIp, Id);
                            if (episodes != null && episodes.Count > 0)
                            {
                                episodes.ForEach(ep =>
                                {
                                    var schedules = ep.Schedule.ToObject<List<ScheduleEvent>>().Where(s=>!Ids.Contains(s.Id)).ToList();
                                    if (schedules != null && schedules.Count > 0)
                                    {
                                        result = agencyRepository.AddSchedules(toDatabaseIp, schedules);
                                    }
                                });

                            }
                    }
                     );
                }
            }
            return result;
        }

        public bool MoveAgencyEpisodeScheduleToFlat(string fromDataBaseIp, string toDatabaseIp)
        {
            var result = false;
            if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
            {
                var agencySnapShots = loginRepository.GetAgencySnapShots();
                if (agencySnapShots != null && agencySnapShots.Count > 0)
                {
                    agencySnapShots.ForEach(agencySnapShot =>
                    {
                        var episodes = agencyRepository.GetAgencyPatientEpisodes(fromDataBaseIp, agencySnapShot.Id);
                        if (episodes != null && episodes.Count > 0)
                        {
                            episodes.ForEach(ep =>
                            {
                                var schedules = ep.Schedule.ToObject<List<ScheduleEvent>>();
                                if (schedules != null && schedules.Count > 0)
                                {
                                    result = agencyRepository.AddSchedules(toDatabaseIp, schedules);
                                }
                            });

                        }
                    }
                     );
                }
            }
            return result;
        }


        public bool MoveAgencyOASISAssessmentsToOneAssessment(string fromDataBaseIp, string toDatabaseIp)
        {
            var result = false;

            if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
            {
                var assessments = agencyRepository.GetAllAssessments(fromDataBaseIp);
                if (assessments != null && assessments.Count > 0)
                {
                    result = agencyRepository.AddAssessments(toDatabaseIp, assessments);
                }
            }
            return result;
        }

        public bool MoveAgencyOASISAssessmentsToOneAssessment(string fromDataBaseIp, string toDatabaseIp, Guid agencyId)
        {
            var result = false;

            if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
            {
              
                var assessments = agencyRepository.GetAllAssessments(fromDataBaseIp, new List<Guid> { agencyId });
                if (assessments != null && assessments.Count > 0)
                {
                    result = agencyRepository.AddAssessments(toDatabaseIp, assessments);
                }
            }
            return result;
        }

        public bool MoveAgencyOASISAssessmentsToOneAssessment(string fromDataBaseIp, string toDatabaseIp, List<Guid> agencyIds, List<Guid> Ids, string tableName)
        {
            var result = false;
            if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
            {
                var assessments = agencyRepository.GetAllAssessments(fromDataBaseIp, agencyIds, Ids, tableName);
                if (assessments != null && assessments.Count > 0)
                {
                    result = agencyRepository.AddAssessments(toDatabaseIp, assessments);
                }
            }
            return result;
        }

        //public bool MoveEntity<T>(string fromDataBaseIp, string toDatabaseIp, Guid agencyId)where T : AgencyBase, new()
        //{
        //    var result = false;

        //    if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
        //    {
        //        var entityList = agencyRepository.GetEntity<T>(fromDataBaseIp,agencyId);
        //        if (entityList != null && entityList.Count > 0)
        //        {
        //            result = agencyRepository.LoadEntity<T>(toDatabaseIp, entityList);
        //        }
        //    }
        //    return result;
        //}


        #endregion


        #region Private Methods

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        //private List<AgencyBase> GetEntityFactory(Tables type,string fromDataBaseIp, Guid agencyId)
        //{
            
        //    switch (type)
        //    {
        //        //case Tables.Agency:
        //        //    baseObject = new Agency();
        //        case Tables.AgencyLocation:
        //            return agencyRepository.GetEntity<AgencyLocation>(fromDataBaseIp, agencyId);
        //            break;
        //        case Tables.AgencyAdjustmentCode:
        //            baseObject = new AgencyAdjustmentCode();
        //            break;
        //        case Tables.AgencyContact:
        //            baseObject = new AgencyContact();
        //            break;
        //        case Tables.AgencyHospital:
        //            baseObject = new AgencyHospital();
        //            break;
        //        case Tables.AgencyInsurance: 
        //            baseObject = new AgencyInsurance();
        //            break;
        //        case Tables.AgencyMedicareInsurance:
        //            baseObject = new AgencyMedicareInsurance();
        //            break;
        //        case Tables.AgencyPhysician: 
        //            baseObject = new AgencyPhysician();
        //            break;
        //        case Tables.AgencySupply: 
        //            baseObject = new AgencySupply();
        //            break;
        //        case Tables.AgencyTemplate:
        //            baseObject = new AgencyTemplate();
        //            break;
        //        //case Tables.AgencySystemMessage: baseObject = new AgencySystemMessage();
        //        //    break;
        //        case Tables.ShpDataLogin:
        //            baseObject = new ShpDataLogin();
        //            break;
        //        case Tables.UploadType:
        //            baseObject = new UploadType();
        //            break;
        //        case Tables.LicenseItem:
        //            baseObject = new LicenseItem();
        //            break;
        //        case Tables.Remittance:
        //            baseObject = new Remittance();
        //            break;

        //        case Tables.Asset: 
        //            baseObject = new Asset();
        //            break;
        //        case Tables.ClaimData:
        //            baseObject = new ClaimData();
        //            break;
        //        case Tables.MedicareEligibilitySummary: 
        //            baseObject = new MedicareEligibilitySummary();
        //            break;
        //        case Tables.User:
        //            baseObject = new User();
        //            break;
        //        case Tables.Patient:
        //            baseObject = new Patient();
        //            break;
        //        case Tables.PatientAdmissionDate:
        //            baseObject = new PatientAdmissionDate();
        //            break;
        //        case Tables.AllergyProfile:
        //            baseObject = new AllergyProfile();
        //            break;
        //        case Tables.MedicationProfile:
        //            baseObject = new MedicationProfile();
        //            break;
        //        case Tables.PatientEmergencyContact:
        //            baseObject = new PatientEmergencyContact();
        //            break;
        //        case Tables.CarePlanOversight: 
        //            baseObject = new CarePlanOversight();
        //            break;
        //        case Tables.PatientDocument:
        //            baseObject = new PatientDocument();
        //            break;


        //        case Tables.Authorization:
        //            baseObject = new Authorization();
        //            break;
        //        case Tables.MessageFolder: 
        //            baseObject = new MessageFolder();
        //            break;
        //        case Tables.Message: 
        //            baseObject = new Message();
        //            break;
        //        //case Tables.PatientUser: baseObject = new PatientUser();
        //        //    break;
        //        case Tables.PatientEpisode:
        //            baseObject = new PatientEpisode();
        //            break;
        //        case Tables.MissedVisit: 
        //            baseObject = new MissedVisit();
        //            break;
        //        case Tables.Assessment: 
        //            baseObject = new Assessment();
        //            break;
        //        case Tables.Final: 
        //            baseObject = new Final();
        //            break;
        //        case Tables.FinalSnapShot:
        //            baseObject = new FinalSnapShot();
        //            break;
        //        case Tables.ManagedClaim: 
        //            baseObject = new ManagedClaim();
        //            break;
        //        case Tables.MedicareEligibility:
        //            baseObject = new MedicareEligibility();
        //            break;
        //        case Tables.Rap: 
        //            baseObject = new Rap();
        //            break;
        //        case Tables.RapSnapShot:
        //            baseObject = new RapSnapShot();
        //            break;
        //        case Tables.ReturnComment: 
        //            baseObject = new ReturnComment();
        //            break;
        //        case Tables.ScheduleEvent: 
        //            baseObject = new ScheduleEvent();
        //            break;
        //        //case Tables.PatientPhysician: baseObject = new PatientPhysician();
        //        //    break;
        //        case Tables.PatientVisitNote:
        //            baseObject = new PatientVisitNote();
        //            break;
        //        case Tables.PhysicianOrder:
        //            baseObject = new PhysicianOrder();
        //            break;
        //        case Tables.PlanofCare: 
        //            baseObject = new PlanofCare();
        //            break;
        //        case Tables.PlanofCareStandAlone: 
        //            baseObject = new PlanofCareStandAlone();
        //            break;
        //        case Tables.CommunicationNote:
        //            baseObject = new CommunicationNote();
        //            break;
        //        case Tables.FaceToFaceEncounter:
        //            baseObject = new FaceToFaceEncounter();
        //            break;
        //        case Tables.HospitalizationLog:
        //            baseObject = new HospitalizationLog();
        //            break;
        //        case Tables.Incident: 
        //            baseObject = new Incident();
        //            break;
        //        case Tables.Infection: 
        //            baseObject = new Infection();
        //            break;
        //        case Tables.Referral:
        //            baseObject = new Referral();
        //            break;
        //        case Tables.ReferralEmergencyContact: 
        //            baseObject = new ReferralEmergencyContact();
        //            break;
        //        case Tables.SecondaryClaim: 
        //            baseObject = new SecondaryClaim();
        //            break;
        //        case Tables.ShpDataBatch: 
        //            baseObject = new ShpDataBatch();
        //            break;
        //        case Tables.ManagedClaimAdjustment:
        //            baseObject = new ManagedClaimAdjustment();
        //            break;
        //        case Tables.ManagedClaimPayment:
        //            baseObject = new ManagedClaimPayment();
        //            break;
        //        case Tables.MedicationProfileHistory:
        //            baseObject = new MedicationProfileHistory();
        //            break;
        //        case Tables.Report: baseObject = new Report();
        //            break;
        //    }
        //    return baseObject;
        //}

        private bool NewTableSchemaFactory(TableToMove type, string fromDataBaseIp, string toDataBaseIp, List<Guid> agencyIds)
        {
            var result = false;
            switch (type)
            {
                   
                case TableToMove.ScheduleEvent:
                    {
                        var exisitngScheduleIds = agencyRepository.GetExistingSchedulesIds(toDataBaseIp, agencyIds);
                        result = this.MoveAgencyEpisodeScheduleToFlat(fromDataBaseIp, toDataBaseIp, agencyIds, exisitngScheduleIds);
                    }
                    break;
                case TableToMove.StartOfCareAssessment:
                case TableToMove.ResumptionOfCareAssessment:
                case TableToMove.RecertificationAssessment:
                case TableToMove.FollowUpAssessment:
                case TableToMove.TransferDischargeAssessment:
                case TableToMove.TransferAssessment:
                case TableToMove.DeathAssessment:
                case TableToMove.DischargeAssessment:
                case TableToMove.NonOASISStartOfCareAssessment:
                case TableToMove.NonOASISRecertificationAssessment:
                case TableToMove.NonOASISDischargeAssessment:
                    var exisitngAssesmentIds = agencyRepository.GetExistingAssessmentsIds(toDataBaseIp, agencyIds);
                    result = this.MoveAgencyOASISAssessmentsToOneAssessment(fromDataBaseIp, toDataBaseIp, agencyIds, exisitngAssesmentIds, type.GetCustomShortDescription());
                    break;
            }
            return result;
        }


        private bool AddEntityFactory(Tables type, string fromDataBaseIp, string toDataBaseIp, Guid agencyId)
        {
            var result = false;
            switch (type)
            {
                case Tables.Agencies:
                    {
                        var agency= agencyRepository.GetAgencyOnly(fromDataBaseIp,agencyId);
                        if (agency != null )
                        {
                            result = agencyRepository.Update(toDataBaseIp, agency);
                        }
                    }
                    break;
                case Tables.Agencylocations:
                    result = MoveEntity<AgencyLocation>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyAdjustmentCodes:
                    result = MoveEntity<AgencyAdjustmentCode>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyContacts:
                    result = MoveEntity<AgencyContact>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyHospitals:
                    result = MoveEntity<AgencyHospital>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyInsurances:
                    result = MoveEntity<AgencyInsurance>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyMedicareInsurances:
                    result = MoveEntity<AgencyMedicareInsurance>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyPhysicians:
                    result = MoveEntity<AgencyPhysician>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencySupplies:
                    result = MoveEntity<AgencySupply>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AgencyTemplates:
                    result = MoveEntity<AgencyTemplate>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                //case Tables.AgencySystemMessage: baseObject = new AgencySystemMessage();
                //    break;
                case Tables.ShpDataLogins:
                    result = MoveEntity<ShpDataLogin>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.UploadTypes:
                    result = MoveEntity<UploadType>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.LicenseItems:
                    result = MoveEntity<License>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Remittances:
                    result = MoveEntity<Remittance>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;

                case Tables.Assets:
                    result = MoveEntity<Asset>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ClaimDatas:
                    result = MoveEntity<ClaimData>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.MedicareEligibilitySummaries:
                    result = MoveEntity<MedicareEligibilitySummary>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Users:
                    result = MoveEntity<User>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Patients:
                    result = MoveEntity<Patient>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.PatientAdmissionDates:
                    result = MoveEntity<PatientAdmissionDate>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.AllergyProfiles:
                    result = MoveEntity<AllergyProfile>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.MedicationProfile:
                    result = MoveEntity<MedicationProfile>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.PatientEmergencyContacts:
                    result = MoveEntity<PatientEmergencyContact>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.CarePlanOverSights:
                    result = MoveEntity<CarePlanOversight>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.PatientDocuments:
                    result = MoveEntity<PatientDocument>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;


                case Tables.Authorizations:
                    result = MoveEntity<Authorization>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.MessageFolders:
                    result = MoveEntity<MessageFolder>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Messages:
                    result = MoveEntity<Message>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.PatientUsers:
                    {
                        var patientUsers = agencyRepository.PatientUsers(fromDataBaseIp, agencyId);
                        if (patientUsers != null && patientUsers.Count > 0)
                        {
                            result = agencyRepository.LoadEntity<PatientUser>(toDataBaseIp, patientUsers);
                        }
                    }
                    break;
                case Tables.PatientEpisodes:
                    result = MoveEntity<PatientEpisode>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.MissedVisits:
                    result = MoveEntity<MissedVisit>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Assessments:
                    result = MoveEntity<AgencyLocation>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Finals:
                    result = MoveEntity<Final>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.FinalSnapShots:
                    result = MoveEntity<FinalSnapShot>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ManagedClaims:
                    result = MoveEntity<ManagedClaim>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.MedicareEligibilities:
                    result = MoveEntity<MedicareEligibility>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Raps:
                    result = MoveEntity<Rap>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.RapSnapShots:
                    result = MoveEntity<RapSnapShot>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ReturnComments:
                    result = MoveEntity<ReturnComment>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                //case Tables.ScheduleEvent:
                //    result = MoveEntity<ScheduleEvent>(fromDataBaseIp, toDataBaseIp, agencyId);
                //    break;
                case Tables.PatientPhysicians:
                    {
                        var patientPhysicians = agencyRepository.PatientPhysicians(fromDataBaseIp, agencyId);
                        if (patientPhysicians != null && patientPhysicians.Count>0)
                        {
                            result = agencyRepository.LoadEntity <PatientPhysician>(toDataBaseIp, patientPhysicians);
                        }
                    }
                    break;
                case Tables.PatientVisitNotes:
                    result = MoveEntity<PatientVisitNote>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.PhysicianOrders:
                    result = MoveEntity<PhysicianOrder>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.PlanOfCares:
                    result = MoveEntity<PlanofCare>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                //case Tables.PlanOfCareAtandAlones:
                //    result = MoveEntity<PlanofCareStandAlone>(fromDataBaseIp, toDataBaseIp, agencyId);
                //    break;
                case Tables.CommunicationNotes:
                    result = MoveEntity<CommunicationNote>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.FaceToFaceEncounters:
                    result = MoveEntity<FaceToFaceEncounter>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.HospitalizationLogs:
                    result = MoveEntity<HospitalizationLog>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Incidents:
                    result = MoveEntity<Incident>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Infections:
                    result = MoveEntity<Infection>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Referrals:
                    result = MoveEntity<Referral>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ReferralEmergencyContacts:
                    result = MoveEntity<ReferralEmergencyContact>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.SecondaryClaims:
                    result = MoveEntity<SecondaryClaim>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ShpDataBatches:
                    result = MoveEntity<ShpDataBatch>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ManagedClaimAdjustments:
                    result = MoveEntity<ManagedClaimAdjustment>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.ManagedClaimPayments:
                    result = MoveEntity<ManagedClaimPayment>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.MedicationProfileHistories:
                    result = MoveEntity<MedicationProfileHistory>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
                case Tables.Reports:
                    result = MoveEntity<Report>(fromDataBaseIp, toDataBaseIp, agencyId);
                    break;
            }
            return result;
        }

        private bool MoveEntity<T>(string fromDataBaseIp, string toDatabaseIp, Guid agencyId) where T : AgencyBase, new()
        {
            var result = false;

            if (toDatabaseIp.IsNotNullOrEmpty() && toDatabaseIp.IsValidIp() && fromDataBaseIp.IsNotNullOrEmpty() && fromDataBaseIp.IsValidIp())
            {
                var entityList = agencyRepository.GetEntity<T>(fromDataBaseIp, agencyId);
                if (entityList != null && entityList.Count > 0)
                {
                    result = agencyRepository.LoadEntity<T>(toDatabaseIp, entityList);
                }
            }
            return result;
        }

        #endregion
    }
}
