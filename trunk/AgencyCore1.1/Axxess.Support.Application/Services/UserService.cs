﻿namespace Axxess.Support.Application.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Support.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    public class UserService : IUserService
    {
        #region Private Members /Constructor

        private readonly ISupportAgencyRepository agencyRepository;
        private readonly ILoginRepository loginRepository;

        public UserService(ISupportAgencyRepository agencyRepository, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyRepository, "agencyRepository");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.agencyRepository = agencyRepository;
        }
        
        #endregion

        #region IUserService Members

        public bool CreateUser(User user)
        {
            try
            {
                var agencySnapShot = loginRepository.GetAgencySnapShot(user.AgencyId);
                if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty()&& agencySnapShot.DBServerIp.IsValidIp())
                {
                    var isNewLogin = false;

                    var login = loginRepository.Find(user.EmailAddress);
                    if (login == null)
                    {
                        login = new Login();
                        login.DisplayName = user.FirstName;
                        login.EmailAddress = user.EmailAddress;
                        login.Role = Roles.ApplicationUser.ToString();
                        login.IsActive = true;
                        login.IsLocked = false;
                        login.IsAxxessAdmin = false;
                        login.IsAxxessSupport = false;
                        login.LastLoginDate = DateTime.Now;
                        if (loginRepository.Add(login))
                        {
                            isNewLogin = true;
                        }
                    }

                    user.LoginId = login.Id;
                    user.Profile = new UserProfile();
                    
                    if (agencyRepository.AddFirstUser(agencySnapShot.DBServerIp,user))
                    {
                        string bodyText = string.Empty;
                        string subject = string.Format("{0} - Invitation to use Axxess Home Health Software", user.AgencyName);

                        if (isNewLogin)
                        {
                            var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
                            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                            bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName,
                                "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
                        }
                        else
                        {
                            bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName,
                                "agencyname", user.AgencyName);
                        }

                        Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return false;
        }

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
                var agencyUsers = loginRepository.GetAllLoginAgencyUsers(login.Id);
                if (agencyUsers.Count > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion
    }
}
