﻿namespace Axxess.Support.Application.Services
{
    using System;

    using Axxess.AgencyManagement.Entities;
    using System.Collections.Generic;
    
    public interface IAgencyService
    {
        //Agency GetAgency(Guid Id);
        bool CreateAgency(Agency agency);
        bool UpdateAgency(Agency agency);
        bool CreateLocation(AgencyLocation location);
        bool UpdateLocation(AgencyLocation location);

        bool LoadFactory(bool IsAll, int actionType, string fromServerIp, string toServerIp, List<Guid> agencyIds, List<int> tablesToMove, List<int> tables);
        void Load(string fromDataBaseIp, string toDatabaseIp, Guid agencyId, List<int> tables);
        bool MoveAgencyEpisodeScheduleToFlat(string fromDataBaseIp, string toDatabaseIp,Guid agencyId);
        //bool MoveAgencyEpisodeScheduleToFlat(string fromDataBaseIp, string toDatabaseIp, List<Guid> agencyIds);
        bool MoveAgencyEpisodeScheduleToFlat(string fromDataBaseIp, string toDatabaseIp);

        bool MoveAgencyOASISAssessmentsToOneAssessment(string fromDataBaseIp, string toDatabaseIp);
        bool MoveAgencyOASISAssessmentsToOneAssessment(string fromDataBaseIp, string toDatabaseIp, Guid agencyId);
        //bool MoveAgencyOASISAssessmentsToOneAssessment(string fromDataBaseIp, string toDatabaseIp, List<Guid> agencyIds);

    }
}
