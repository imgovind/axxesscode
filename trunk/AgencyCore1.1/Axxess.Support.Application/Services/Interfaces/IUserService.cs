﻿namespace Axxess.Support.Application.Services
{
    using System;

    using Axxess.AgencyManagement.Entities;

    public interface IUserService
    {
        bool CreateUser(User user);
        bool IsEmailAddressInUse(string emailAddress);
    }
}
