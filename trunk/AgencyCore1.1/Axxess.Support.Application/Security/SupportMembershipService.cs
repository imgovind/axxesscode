﻿namespace Axxess.Support.Application.Security
{
    using System;
    using System.Web;
    using System.Text;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Api;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.Support.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    public class SupportMembershipService : ISupportMembershipService
    {
        #region Private Members

        private readonly ILoginRepository loginRepository;
        private readonly ISupportRepository supportRepository;
        private readonly ISupportAgencyRepository supportAgencyRepository;
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        #endregion

        #region Constructor

        public SupportMembershipService(IMembershipDataProvider membershipDataProvider, ISupportAgencyRepository supportAgencyRepository)//, IAgencyManagementDataProvider agencyManagementProvider
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
           // Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            //this.userRepository = agencyManagementProvider.UserRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            //this.agencyRepository = agencyManagementProvider.AgencyRepository;
            this.supportAgencyRepository = supportAgencyRepository;
        }

        #endregion

        #region IMembershipService Members

        public bool Activate(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string passwordSalt = string.Empty;
                string passwordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                login.PasswordSalt = passwordSalt;
                login.SignatureSalt = passwordSalt;
                login.PasswordHash = passwordHash;
                login.SignatureHash = passwordHash;
                login.LastLoginDate = DateTime.Now;
                if (loginRepository.Update(login))
                {
                    AxxessSupportPrincipal principal = Get(login.EmailAddress);
                    if (principal != null)
                    {
                        account.UserName = login.EmailAddress;
                        return true;
                    }
                }
            }

            return false;
        }

        public AxxessSupportPrincipal Get(string userName)
        {
            var principal = Cacher.Get<AxxessSupportPrincipal>(userName);
            if (principal == null)
            {
                var login = loginRepository.Find(userName);
                if (login != null && login.IsActive)
                {
                    if (login.IsAxxessAdmin || login.IsAxxessSupport)
                    {
                        var identity = new AxxessSupportIdentity(login.Id, login.EmailAddress);
                        identity.Session =
                            new AdminSession
                            {
                                LoginId = login.Id,
                                DisplayName = login.DisplayName,
                                IsAxxessAdmin = login.IsAxxessAdmin,
                                IsAxxessSupport = login.IsAxxessSupport
                            };

                        principal = new AxxessSupportPrincipal(identity);
                        Cacher.Set<AxxessSupportPrincipal>(userName, principal);
                    }
                }
            }

            return principal;
        }

        public LoginAttemptType Validate(string userName, string password)
        {
            var loginAttempt = LoginAttemptType.Failed;
            
            var login = loginRepository.Find(userName);
            if (login != null && login.IsActive)
            {
                var principal = Get(login.EmailAddress);
                if (authenticationAgent.VerifySupportuser(principal.ToSupportUser()))
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password.Trim(), login.PasswordHash, login.PasswordSalt))
                    {
                        if (login.IsAxxessAdmin || login.IsAxxessSupport)
                        {
                            var identity = new AxxessSupportIdentity(login.Id, login.EmailAddress);
                            identity.Session =
                                new AdminSession
                                {
                                    LoginId = login.Id,
                                    DisplayName = login.DisplayName,
                                    IsAxxessAdmin = login.IsAxxessAdmin,
                                    IsAxxessSupport = login.IsAxxessSupport
                                };

                            login.LastLoginDate = DateTime.Now;
                            if (loginRepository.Update(login))
                            {
                                loginAttempt = LoginAttemptType.Success;
                            }

                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            Cacher.Set<AxxessSupportPrincipal>(userName, principal);
                        }
                    }
                }
            }

            if (loginAttempt == LoginAttemptType.Failed)
            {
                LoginMonitor.Instance.Track(userName, Current.IpAddress);
                if (!LoginMonitor.Instance.HasAttempts(userName, Current.IpAddress))
                {
                    loginAttempt = LoginAttemptType.TooManyAttempts;
                }
            }

            return loginAttempt;
        }

        public string GetImpersonationUrl(Guid loginId)
        {
            var url = new StringBuilder();

            if (!loginId.IsEmpty())
            {
                var link = new ImpersonationLink
                {
                    LoginId = loginId,
                    UserId = Guid.Empty,
                    AgencyId = Guid.Empty,
                    RepName = Current.DisplayName,
                    RepLoginId = Current.LoginId
                };

                if (supportRepository.AddImpersonationLink(link))
                {
                    url.Append(SupportAppSettings.ImpersonatePhysicianUrl);
                    url.Append(Crypto.Encrypt(string.Format("linkid={0}", link.Id.ToString())));
                }
            }

            return url.ToString();
        }

        public string GetImpersonationUrl(Guid agencyId, Guid userId)
        {
            var url = new StringBuilder();
            var agencyUser = loginRepository.GetLoginAgencyUser(agencyId, userId);
            if (agencyUser != null)
            {

                var link = new ImpersonationLink
                {
                    UserId = userId,
                    AgencyId = agencyId,
                    LoginId = agencyUser.LoginId,
                    RepName = Current.DisplayName,
                    RepLoginId = Current.LoginId
                };

                if (supportRepository.AddImpersonationLink(link))
                {
                    url.Append(SupportAppSettings.ImpersonateUrl);
                    url.Append(Crypto.Encrypt(string.Format("linkid={0}", link.Id.ToString())));
                }
            }

            return url.ToString();
        }

        public void LogOff(string emailAddress)
        {
            SessionStore.Abandon();
            Cacher.Remove(emailAddress);
            authenticationAgent.LogoutSupportUser(emailAddress);
        }

        public bool ResendActivationLink(Guid userId, Guid agencyId)
        {
            var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
             {
                 var user = supportAgencyRepository.GetUserOnly(agencySnapShot.DBServerIp, agencyId,userId);
                 if (user != null)
                 {
                     var login = loginRepository.Find(user.LoginId);
                     if (login != null)
                     {
                         string bodyText = string.Empty;
                         string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);

                         //var accounts = userRepository.GetUsersByLoginId(login.Id);
                         var agencyName = agencySnapShot.Name;//AgencyEngine.Get(agencyId).Name;
                         //if (accounts != null && accounts.Count > 1)
                         //{
                             bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", agencyName);
                         //}
                         //else
                         //{
                         //    var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
                         //    var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                         //    bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", agencyName, "encryptedQueryString", encryptedParameters);
                         //}

                         Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, subject, bodyText);
                         return true;
                     }
                 }
             }
            return false;
        }

        #endregion
    }
}
