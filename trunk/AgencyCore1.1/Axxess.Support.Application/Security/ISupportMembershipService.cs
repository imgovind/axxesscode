﻿namespace Axxess.Support.Application.Security
{
    using System;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    
    using Security;

    public interface ISupportMembershipService
    {
        void LogOff(string userName);
        bool Activate(Account account);
        bool ResendActivationLink(Guid userId, Guid agencyId);
        AxxessSupportPrincipal Get(string userName);
        LoginAttemptType Validate(string userName, string password);
        string GetImpersonationUrl(Guid loginId);
        string GetImpersonationUrl(Guid agencyId, Guid userId);
    }
}
