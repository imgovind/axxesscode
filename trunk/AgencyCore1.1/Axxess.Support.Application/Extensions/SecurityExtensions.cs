﻿namespace Axxess.Support.Application
{
    using System;
    using Security;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class SecurityExtensions
    {
        public static SupportUser ToSupportUser(this AxxessSupportPrincipal principal)
        {
            SupportUser supportUser = null;
            if (principal != null)
            {
                AxxessSupportIdentity identity = (AxxessSupportIdentity)principal.Identity;
                if (identity != null)
                {
                    supportUser = new SupportUser()
                    {
                        LoginId = identity.Id,
                        EmailAddress = identity.Name,
                        IpAddress = Current.IpAddress,
                        ServerName = Environment.MachineName,
                        SessionId = SessionStore.SessionId,
                        DisplayName = identity.Session.DisplayName,
                        IsAuthenticated = identity.IsAuthenticated,
                        LastSecureActivity = identity.LastSecureActivity
                    };
                }
            }
            return supportUser;
        }
    }
}
