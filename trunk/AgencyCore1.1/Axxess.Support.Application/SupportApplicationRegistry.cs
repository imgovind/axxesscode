﻿namespace Axxess.Support.Application
{
    using System;
    using System.Web.Mvc;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Membership.Logging;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Support.Repositories;

    using Axxess.Membership.Repositories;

    using Security;

    using Axxess.LookUp.Repositories;

    public class SupportApplicationRegistry : Registry
    {
        public SupportApplicationRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.AddAllTypesOf<IController>();
                x.WithDefaultConventions();
            });

            For<ILog>().Use<DatabaseLog>();
            For<IMembershipDataProvider>().Use<MembershipDataProvider>();
            For<ILookUpDataProvider>().Use<LookUpDataProvider>();
            For<ISupportAgencyRepository>().Use<SupportAgencyRepository>();
            For<ISupportMembershipService>().Use<SupportMembershipService>();
            For<ISystemRepository>().Use<SystemRepository>();
            For<IFormsAuthenticationService>().Use<FormsAuthenticationService>();
        }
    }
}
