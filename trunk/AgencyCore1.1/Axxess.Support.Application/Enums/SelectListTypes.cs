﻿namespace Axxess.Support.Application.Enums
{
    public enum SelectListTypes
    {
        States,
        TitleTypes,
        CredentialTypes,
        LicenseTypes,
        CahpsVendors,
        Users
    }
}
