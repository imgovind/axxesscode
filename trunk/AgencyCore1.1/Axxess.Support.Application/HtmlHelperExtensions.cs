﻿namespace Axxess.Support.Application
{
    using System;
    using System.Web;
    using System.Text;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Collections.Generic;

    using Axxess.LookUp;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Support.Repositories;
    

    public static class HtmlHelperExtensions
    {
        private static ILoginRepository loginRepository
        {
            get
            {
                IMembershipDataProvider dataProvider = Container.Resolve<IMembershipDataProvider>();
                return dataProvider.LoginRepository;
            }
        }

        private static ISupportAgencyRepository agencyRepository
        {
            get
            {
                return Container.Resolve<SupportAgencyRepository>();
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        private static IHostRepository hostRepository
        {
            get
            {
                IMembershipDataProvider dataProvider = Container.Resolve<IMembershipDataProvider>();
                return dataProvider.HostRepository;
            }
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var agencies = loginRepository.GetAgencySnapShots();
            tempItems = from agency in agencies
                        select new SelectListItem
                        {
                            Text = agency.Name,
                            Value = agency.Id.ToString(),
                            Selected = (agency.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, Guid agencyId, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
             var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
             if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
             {
                 var branches = agencyRepository.GetBranches(agencySnapShot.DBServerIp, agencyId);
                 tempItems = from branch in branches
                             select new SelectListItem
                             {
                                 Text = branch.Name,
                                 Value = branch.Id.ToString(),
                                 Selected = (branch.Id.ToString().IsEqual(value))
                             };
                 items = tempItems.ToList();
             }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString CahpsVendors(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var cahpsVendors = new Dictionary<string, int>();
            Array cahpsVendorValues = Enum.GetValues(typeof(CahpsVendors));
            foreach (CahpsVendors cahpsVendor in cahpsVendorValues)
            {
                cahpsVendors.Add(cahpsVendor.GetDescription(), (int)cahpsVendor);
            }

            tempItems = from type in cahpsVendors
                        select new SelectListItem
                        {
                            Text = type.Key,
                            Value = type.Value.ToString(),
                            Selected = (type.Value.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LicenseTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var licenseTypes = new List<string>();
            Array licenseTypeValues = Enum.GetValues(typeof(LicenseTypes));
            foreach (LicenseTypes licenseType in licenseTypeValues)
            {
                licenseTypes.Add(licenseType.GetDescription());
            }

            tempItems = from type in licenseTypes
                        select new SelectListItem
                        {
                            Text = type,
                            Value = type,
                            Selected = (type.IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select License Type --",
                Value = "",
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString CredentialTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var credentialTypes = new List<string>();
            Array credentialValues = Enum.GetValues(typeof(CredentialTypes));
            foreach (CredentialTypes credentialType in credentialValues)
            {
                credentialTypes.Add(credentialType.GetDescription());
            }

            tempItems = from type in credentialTypes
                        select new SelectListItem
                        {
                            Text = type,
                            Value = type,
                            Selected = (type.IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Unknown --",
                Value = ""
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString TitleTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var titleTypes = new List<string>();
            Array titleValues = Enum.GetValues(typeof(TitleTypes));
            foreach (TitleTypes titleType in titleValues)
            {
                titleTypes.Add(titleType.GetDescription());
            }

            tempItems = from type in titleTypes
                        select new SelectListItem
                        {
                            Text = type,
                            Value = type,
                            Selected = (type.IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Title Type --",
                Value = "",
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString States(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var states = lookupRepository.States();
            tempItems = from state in states
                        select new SelectListItem
                        {
                            Text = state.Name,
                            Value = state.Code,
                            Selected = (state.Code.IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select State --",
                Value = " "
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Users(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var users = loginRepository.GetAllByRole(Roles.AxxessAdmin);
            users = users.OrderBy(u => u.DisplayName).ToList();
            if (users.IsNotNullOrEmpty())
            {
               var tempItems = from user in users
                            select new SelectListItem
                            {
                                Text = user.DisplayName,
                                Value = user.Id.ToString(),
                                Selected = (user.Id.ToString().IsEqual(value))
                            };

                items.AddRange(tempItems);
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select User --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DbServers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items =  new List<SelectListItem>();
            var clusters = hostRepository.GetActiveClusters();
            if (clusters != null && clusters.Count > 0)
            {
                var tempItems = from cluster in clusters
                                select new SelectListItem
                                {
                                    Text = cluster.DBServerIp,
                                    Value = cluster.DBServerIp,
                                    Selected = (cluster.DBServerIp.ToString().IsEqual(value))
                                };
                if (tempItems != null && tempItems.Count() > 0)
                {
                    items.AddRange(tempItems.ToList());
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select DB Server --",
                Value = ""
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Tables(this HtmlHelper html, string name, bool isSelectTitle, string title, string defaultValue, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array tables = Enum.GetValues(typeof(Tables));
            foreach (Tables table in tables)
            {
                items.Add(new SelectListItem
                {
                    Text = table.GetDescription(),
                    Value = ((int)table).ToString(),
                    Selected = ((int)table).ToString().IsEqual(defaultValue)
                });
            }
            if (isSelectTitle)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = defaultValue
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString TablesToMove(this HtmlHelper html, string name, bool isSelectTitle, string title, string defaultValue, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array tables = Enum.GetValues(typeof(TableToMove));
            foreach (TableToMove table in tables)
            {
                items.Add(new SelectListItem
                {
                    Text = table.GetDescription(),
                    Value = ((int)table).ToString(),
                    Selected = ((int)table).ToString().IsEqual(defaultValue)
                });
            }
            if (isSelectTitle)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = defaultValue
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

    }
}
