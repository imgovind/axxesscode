﻿namespace Axxess.Support.Application.Controllers
{
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Repositories;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : BaseController
    {
        #region Private Members/Constructor

        private readonly ILoginRepository loginRepository;
        private readonly IErrorRepository errorRepository;

        public HomeController(IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.errorRepository = membershipDataProvider.ErrorRepository;
        }

        #endregion

        #region HomeController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Error()
        {
            return View("~/Views/Shared/Error.aspx");
        }

        #endregion
    }
}
