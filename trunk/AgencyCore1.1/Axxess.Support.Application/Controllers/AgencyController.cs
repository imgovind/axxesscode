﻿namespace Axxess.Support.Application.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Telerik.Web.Mvc;

    using Services;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Support.Repositories;

    using Axxess.Membership.Repositories;
    using Axxess.Membership.Logging;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly ISupportAgencyRepository agencyRepository;
        private readonly ILoginRepository loginRepository;
        private readonly ISystemRepository systemRepository;
        private readonly IHostRepository hostRepository;
        private ISupportMembershipService membershipService = Container.Resolve<ISupportMembershipService>();

        public AgencyController(ISystemRepository systemRepository, ISupportAgencyRepository agencyRepository, IMembershipDataProvider membershipDataProvider, IAgencyService agencyService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyRepository, "agencyManagementDataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.hostRepository = membershipDataProvider.HostRepository;
            this.agencyRepository = agencyRepository;
            this.systemRepository = systemRepository;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserGrid(Guid agencyId)
        {
            return PartialView("Users/List", loginRepository.GetAgencySnapShot(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserList(Guid agencyId)
        {
            IList<UserSelection> userList = new List<UserSelection>();
            var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
            {
                var users = agencyRepository.GetAllUsers(agencySnapShot.DBServerIp, agencyId);
                if (users != null && users.Count > 0)
                {
                    var loginIds = users.Where(u => !u.LoginId.IsEmpty()).Select(u => u.LoginId).ToList();
                    var logins = loginRepository.GetLogins(loginIds);
                    users.ForEach(u =>
                    {
                        var login = logins.FirstOrDefault(l => l.Id == u.LoginId);
                        if (login != null)
                        {
                            userList.Add(new UserSelection
                            {
                                Id = u.Id,
                                LoginId = login.Id,
                                LastName = u.LastName,
                                FirstName = u.FirstName,
                                Credential = u.DisplayTitle,
                                DisplayName = u.IsDeprecated ? u.DisplayName + " [Deprecated]" : u.DisplayName,
                                EmailAddress = login.EmailAddress,
                                LoginCreated = login.CreatedFormatted,
                                IsLoginActive = login.IsActive
                            });
                        }
                    });
                }
            }

            return View(new GridModel(userList.OrderBy(u => u.DisplayName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUser(Guid loginId)
        {
            return PartialView("Users/Edit", loginRepository.Find(loginId));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult UpdateUserEmail(Guid LoginId, string EmailAddress, string DisplayName)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Information could not be saved" };

        //    if (EmailAddress.IsNotNullOrEmpty() && EmailAddress.IsEmail())
        //    {
        //        if (!userService.IsEmailAddressInUse(EmailAddress.Trim()))
        //        {
        //            var login = loginRepository.Find(LoginId);
        //            if (login != null)
        //            {
        //                login.DisplayName = DisplayName;
        //                login.EmailAddress = EmailAddress;
        //                if (!loginRepository.Update(login))
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = "Error in saving the User information.";
        //                }
        //                else
        //                {
        //                    var users = userRepository.GetUsersByLoginId(login.Id);
        //                    if (users != null && users.Count > 0)
        //                    {
        //                        users.ForEach(user =>
        //                        {
        //                            user.Profile = user.ProfileData.IsNotNullOrEmpty() ? user.ProfileData.ToObject<UserProfile>() : null;
        //                            if (user.Profile != null)
        //                            {
        //                                user.Profile.EmailWork = EmailAddress.Trim();
        //                                user.ProfileData = user.Profile.ToXml();
        //                                userRepository.UpdateModel(user, true);
        //                            }
        //                        });
        //                    }
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "User information was saved successfully";
        //                }
        //            }
        //            else
        //            {
        //                viewData.isSuccessful = false;
        //                viewData.errorMessage = "User Information was not found.";
        //            }
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = "This e-mail address is already in use.";
        //        }
        //    }
        //    else
        //    {
        //        viewData.isSuccessful = false;
        //        viewData.errorMessage = "No valid E-mail Address provided.";
        //    }

        //    return Json(viewData);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateUserPassword(Guid LoginId, string Password, string Signature)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Password/Signature could not be saved" };

            var login = loginRepository.Find(LoginId);
            if (login != null)
            {
                var passSalt = string.Empty;
                var passHash = string.Empty;

                var sigSalt = string.Empty;
                var sigHash = string.Empty;

                var saltedHash = new SaltedHash();
                if (Password.IsNotNullOrEmpty())
                {
                    saltedHash.GetHashAndSalt(Password, out passHash, out passSalt);
                    login.PasswordHash = passHash;
                    login.PasswordSalt = passSalt;
                }

                if (Signature.IsNotNullOrEmpty())
                {
                    saltedHash.GetHashAndSalt(Signature, out sigHash, out sigSalt);
                    login.SignatureHash = sigHash;
                    login.SignatureSalt = sigSalt;
                }

                if (!loginRepository.Update(login))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the password or signature.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Password/Signature was saved successfully. ";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User Login was not found.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianGrid(Guid agencyId)
        {
            return PartialView("Physicians", loginRepository.GetAgencySnapShot(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianList(Guid agencyId)
        {
            var physicianList = new List<PhysicianSelection>();
            var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
            {
                var physicians = agencyRepository.GetAgencyPhysicians(agencySnapShot.DBServerIp,agencyId);
                if (physicians != null && physicians.Count > 0)
                {
                    var loginIds = physicians.Select(l => l.LoginId).Distinct().ToList();
                   var logins = loginRepository.GetLogins(loginIds);
                    physicians.ForEach(p =>
                    {
                        if (!p.LoginId.IsEmpty() && p.PhysicianAccess)
                        {
                            var login = logins.FirstOrDefault(l=>l.Id==p.LoginId);
                            if (login != null)
                            {
                                physicianList.Add(new PhysicianSelection
                                {
                                    Id = p.Id,
                                    LoginId = login.Id,
                                    LastName = p.LastName,
                                    FirstName = p.FirstName,
                                    Credentials = p.Credentials,
                                    DisplayName = p.IsDeprecated ? p.DisplayName + " [Deprecated]" : p.DisplayName,
                                    EmailAddress = login.EmailAddress,
                                    IsLoginActive = login.IsActive
                                });
                            }
                        }
                    });
                }
            }
            return View(new GridModel(physicianList.OrderBy(u => u.DisplayName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid())
            {
                if (!agencyService.CreateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            Agency agency = null;
            var agencySnapShot = loginRepository.GetAgencySnapShot(id);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
            {
                agency = agencyRepository.GetAgencyOnly(agencySnapShot.DBServerIp, id);
            }
            return PartialView(agency);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Agreement()
        {
            return PartialView("Agreement", loginRepository.Find(Current.User.Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HumanResource()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid())
            {
                if (!agencyService.UpdateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid()
        {
            var agencies = new List<AgencyLite>();
            var clusters = hostRepository.GetActiveClusters();
            if (clusters != null && clusters.Count > 0)
            {
                var locations = new List<AgencyLocation>();
                var agencyList = new List<Agency>();
                clusters.ForEach(c =>
                {
                    if (c.DBServerIp.IsNotNullOrEmpty() && c.DBServerIp.IsValidIp())
                    {
                        var eachClusterAgencies = agencyRepository.All(c.DBServerIp);
                        if (eachClusterAgencies != null && eachClusterAgencies.Count() > 0)
                        {
                            agencyList.AddRange(eachClusterAgencies);
                            var agencyIds = eachClusterAgencies.Select(s => s.Id).ToList();
                            var mainLocations = agencyRepository.AgencyMainLocations(c.DBServerIp, agencyIds);
                            if (mainLocations != null && mainLocations.Count > 0)
                            {
                                locations.AddRange(mainLocations);
                            }
                        }
                    }
                });
               
                if (agencyList != null && agencyList.Count > 0)
                {
                    var loginIds = new List<Guid>();

                    var salesPersonLoginIds = agencyList.Select(l => l.SalesPerson);
                    if (salesPersonLoginIds != null && salesPersonLoginIds.Count() > 0)
                    {
                        loginIds.AddRange(salesPersonLoginIds.Distinct().ToList());
                    }
                    var trainerLoginIds = agencyList.Select(l => l.Trainer);
                    if (trainerLoginIds != null && trainerLoginIds.Count() > 0)
                    {
                        loginIds.AddRange(trainerLoginIds.Distinct().ToList());
                    }
                    var logins = loginRepository.GetLogins(loginIds);


                    agencyList.ForEach(a =>
                    {
                        var salesPersonName = string.Empty;
                        var salesPersonLogin = logins.FirstOrDefault(l => l.Id == a.SalesPerson);
                        if (salesPersonLogin != null)
                        {
                            salesPersonName = salesPersonLogin.DisplayName;
                        }

                        var trainerName = string.Empty;
                        var trainerNameLogin = logins.FirstOrDefault(l => l.Id == a.Trainer);
                        if (trainerNameLogin != null)
                        {
                            trainerName = trainerNameLogin.DisplayName;
                        }
                        var location = locations.FirstOrDefault(l => l.AgencyId == a.Id);

                        agencies.Add(new AgencyLite
                        {
                            Id = a.Id,
                            Name = a.Name,
                            SalesPerson = salesPersonName,
                            Trainer = trainerName,

                            ContactPersonEmail = a.ContactPersonEmail,
                            ContactPersonDisplayName = a.ContactPersonDisplayName,
                            ContactPersonPhoneFormatted = a.ContactPersonPhoneFormatted,
                            City = location != null ? location.AddressCity : string.Empty,
                            State = location != null ? location.AddressStateCode : string.Empty,

                            IsSuspended = a.IsSuspended,
                            IsDeprecated = a.IsDeprecated,
                            Date = a.Created.ToString("MM/dd/yyyy"),
                            ActionText = a.IsDeprecated ? "Restore" : "Suspend"
                        });
                    });
                }
            }
            return View(new GridModel(agencies));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Impersonate(Guid agencyId, Guid userId)
        {
            var url = membershipService.GetImpersonationUrl(agencyId, userId);
            if (url.IsNotNullOrEmpty())
            {
                return Redirect(url);
            }
            return RedirectToAction("Index", "Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ImpersonatePhysician(Guid loginId)
        {
            var url = membershipService.GetImpersonationUrl(loginId);
            if (url.IsNotNullOrEmpty())
            {
                return Redirect(url);
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLocation(Guid agencyId)
        {
            return PartialView("Location/New", loginRepository.GetAgencySnapShot(agencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid())
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationGrid(Guid agencyId)
        {
            return PartialView("Location/List", loginRepository.GetAgencySnapShot(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationList(Guid agencyId)
        {
            IList<AgencyLocation> locations = new List<AgencyLocation>();
            var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
            {
                locations = agencyRepository.GetBranches(agencySnapShot.DBServerIp,agencyId);
            }
            return View(new GridModel(locations));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid agencyId, Guid locationId)
        {
            AgencyLocation location = new AgencyLocation();
            var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
            if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
            {
                location = agencyRepository.FindLocation(agencySnapShot.DBServerIp,agencyId, locationId);
            }
            return PartialView("Location/Edit", location);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid())
            {
                if (!agencyService.UpdateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was edited successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not complete this action for this agency." };
             var agencySnapShot = loginRepository.GetAgencySnapShot(id);
             if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
             {
                 if (agencyRepository.ToggleDelete(agencySnapShot.DBServerIp,id))
                 {
                     viewData.isSuccessful = true;
                     viewData.errorMessage = "Templates have been uploaded for this agency.";
                 }
             }
            return Json(viewData);
        }

        #endregion

        #region Templates, Supplies

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Templates(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not load templates for this agency." };
            try
            {
                var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
                if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                {
                    agencyRepository.InsertTemplates(agencySnapShot.DBServerIp, agencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Templates have been added for this agency.";
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Supplies(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not load supplies for this agency." };
            try
            {
                var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
                if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                 {
                     agencyRepository.InsertSupplies(agencySnapShot.DBServerIp, agencyId);
                     viewData.isSuccessful = true;
                     viewData.errorMessage = "Supplies have been added for this agency.";
                 }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StandAloneContent(Guid agencyId, Guid locationId)
        {
            var agencyLocation = new AgencyLocation();
            if (!locationId.IsEmpty() && !agencyId.IsEmpty())
            {
                var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
                if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                {
                    agencyLocation = agencyRepository.FindLocation(agencySnapShot.DBServerIp, agencyId, locationId) ?? new AgencyLocation();
                }
            }
            return PartialView("Location/StandAloneInfo", agencyLocation);
        }

        #endregion

        #region Notes

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNote(Guid agencyId)
        {
            return PartialView("Note/New", loginRepository.GetAgencySnapShot(agencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddNote([Bind] CustomerNote note)
        {
            Check.Argument.IsNotNull(note, "note");

            var viewData = new JsonViewData();

            if (note.IsValid())
            {
                if (!systemRepository.AddCustomerNote(note))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Note.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Note was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = note.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteGrid(Guid agencyId)
        {
            return PartialView("Note/List", loginRepository.GetAgencySnapShot(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteList(Guid agencyId)
        {
            var notes = systemRepository.GetCustomerNotes(agencyId);
            notes.ForEach(n =>
            {
                n.RepName = loginRepository.GetLoginDisplayName(n.LoginId);
            });
            return View(new GridModel(notes));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditNote(Guid agencyId, Guid noteId)
        {
            var note = systemRepository.GetCustomerNote(agencyId, noteId);
            if (note != null)
            {
                note.RepName = loginRepository.GetLoginDisplayName(note.LoginId);
                return PartialView("Note/Edit", note);
            }
            return PartialView("Note/Edit", new CustomerNote {} );
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewNote(Guid agencyId, Guid noteId)
        {
            var note = systemRepository.GetCustomerNote(agencyId, noteId);
            if (note != null)
            {
                note.RepName = loginRepository.GetLoginDisplayName(note.LoginId);
                return PartialView("Note/View", note);
            }
            return PartialView("Note/View", new CustomerNote { });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateNote([Bind] CustomerNote note)
        {
            Check.Argument.IsNotNull(note, "note");

            var viewData = new JsonViewData();

            if (note.IsValid())
            {
                var existingNote = systemRepository.GetCustomerNote(note.AgencyId, note.Id);
                if (existingNote != null)
                {
                    existingNote.Comments = note.Comments;
                    existingNote.Modified = note.Modified;
                    if (!systemRepository.UpdateCustomerNote(existingNote))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in updating the note.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Note was updated successfully";
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = note.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteNote(Guid agencyId, Guid noteId)
        {
            Check.Argument.IsNotEmpty(noteId, "noteId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not delete this note." };

            var existingNote = systemRepository.GetCustomerNote(agencyId, noteId);
            if (existingNote != null)
            {
                existingNote.IsDeprecated = true;
                if (systemRepository.UpdateCustomerNote(existingNote))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Note has been deleted.";
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Move Data

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoadingList()
        {
            return PartialView("LoadingList", loginRepository.GetAgencySnapShots().ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Load(bool IsAll, int ActionType, string FromServerIp, string ToServerIp, List<Guid> AgencyIds, List<int> TablesToMove, List<int> Tables)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could  this note." };
            var result = agencyService.LoadFactory(IsAll, ActionType, FromServerIp, ToServerIp, AgencyIds, TablesToMove, Tables);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Note has been deleted.";
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult FlatAgencySchedules(string fromDatabaseIp, string toDatabaseIp, List<Guid> agencyId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could  this note." };
        //    var result = agencyService.MoveAgencyEpisodeScheduleToFlat(fromDatabaseIp,toDatabaseIp, agencyId);
        //    if (result)
        //    {
        //        viewData.isSuccessful = true;
        //        viewData.errorMessage = "The Note has been deleted.";
        //    }
        //    return Json(viewData);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult FlatAgencyOASIS(string fromDatabaseIp, string toDatabaseIp, List<Guid> agencyId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could  this note." };
        //    var result = agencyService.MoveAgencyOASISAssessmentsToOneAssessment(fromDatabaseIp, toDatabaseIp, agencyId);
        //    if (result)
        //    {
        //        viewData.isSuccessful = true;
        //        viewData.errorMessage = "The Note has been deleted.";
        //    }
        //    return Json(viewData);
        //}

        #endregion

    }
}
