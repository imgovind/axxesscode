﻿namespace Axxess.Support.Application.Controllers
{
    using System.Linq;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Exports;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Support.Repositories;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Entities;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly ISupportAgencyRepository agencyRepository;
        private readonly IHostRepository hostRepository;

        public ExportController(ISupportAgencyRepository agencyRepository, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(agencyRepository, "agencyRepository");

            this.agencyRepository = agencyRepository;
            this.hostRepository = membershipDataProvider.HostRepository;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Agencies()
        {
            var agencyList = new List<Agency>();
            var clusters = hostRepository.GetActiveClusters();
            if (clusters != null && clusters.Count > 0)
            {
                clusters.ForEach(c =>
               {
                   if (c.DBServerIp.IsNotNullOrEmpty() && c.DBServerIp.IsValidIp())
                   {
                       var eachClusterAgencies = agencyRepository.AllAgencies(c.DBServerIp);
                       if (eachClusterAgencies != null && eachClusterAgencies.Count > 0)
                       {
                           agencyList.AddRange(eachClusterAgencies);
                       }
                   }
               });
            }

            var export = new AgencyExporter(agencyList);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Agencies.xls");
        }

        #endregion
    }
}
