﻿namespace Axxess.Support.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;

    public interface ISystemRepository
    {

        bool AddSystemMessage(SystemMessage message);
        bool AddDashboardMessage(DashboardMessage message);


        bool AddCustomerNote(CustomerNote note);
        CustomerNote GetCustomerNote(Guid agencyId, Guid noteId);
        bool UpdateCustomerNote(CustomerNote note);
        IList<CustomerNote> GetCustomerNotes(Guid agencyId);

    }
}
