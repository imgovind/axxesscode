﻿namespace Axxess.Support.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    public interface ISupportAgencyRepository
    {
        bool ToggleDelete(string databaseIp, Guid id);
        bool Add(string databaseIp, Agency agency);
        bool Update(string databaseIp, Agency agency);
        bool AddModal(string databaseIp, Agency agency);
        Agency GetAgencyOnly(string databaseIp, Guid id);
        IEnumerable<Agency> All(string databaseIp);
        IList<Agency> AllAgencies(string databaseIp);
        bool AddMultiAgency(string databaseIp, List<Agency> agencies);

        bool AddLocation(string databaseIp, AgencyLocation agencyLocation);
        bool UpdateLocation(string databaseIp, AgencyLocation location);
        bool EditLocationModal(string databaseIp, AgencyLocation location);
        AgencyLocation GetMainLocation(string databaseIp, Guid agencyId);
        AgencyLocation FindLocation(string databaseIp, Guid agencyId, Guid Id);
        IList<AgencyLocation> GetBranches(string databaseIp, Guid agencyId);
        List<AgencyLocation> AgencyMainLocations(string databaseIp, List<Guid> agencyIds);

        User GetUserOnly(string databaseIp, Guid agencyId, Guid Id);
        bool AddFirstUser(string databaseIp, User user);
        List<User> GetAllUsers(string databaseIp, Guid agencyId);

        IList<AgencyPhysician> GetAgencyPhysicians(string databaseIp, Guid agencyId);

        void InsertTemplates(string databaseIp, Guid agencyId);
        void InsertSupplies(string databaseIp, Guid agencyId);

        List<PatientEpisode> GetAgencyPatientEpisodes(string databaseIp, Guid agencyId);
        List<Guid> GetExistingSchedulesIds(string databaseIp, List<Guid> agencyIds);
        bool AddSchedules(string databaseIp, List<ScheduleEvent> scheduleEvents);

        List<Assessment> GetAllAssessments(string databaseIp, List<Guid> agencyIds, List<Guid> assesmentIds, string tableName);
        List<Assessment> GetAllAssessments(string databaseIp, List<Guid> agencyIds);
        List<Assessment> GetAllAssessments(string databaseIp);

        List<Guid> GetExistingAssessmentsIds(string databaseIp, List<Guid> agencyIds);

        bool AddAssessments(string databaseIp, List<Assessment> assessments);
        List<T> GetEntity<T>(string databaseIp, Guid agencyId) where T : AgencyBase, new();
        bool LoadEntity<T>(string databaseIp, List<T> listOfData) where T : class, new();
        List<PatientPhysician> PatientPhysicians(string databaseIp, Guid agencyId);
        List<PatientUser> PatientUsers(string databaseIp, Guid agencyId);

    }
}
