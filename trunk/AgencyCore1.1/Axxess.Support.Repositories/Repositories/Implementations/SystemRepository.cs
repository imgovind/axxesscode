﻿namespace Axxess.Support.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.Repository;

    using Axxess.Core;

    using Axxess.AgencyManagement.Entities;

    public class SystemRepository : ISystemRepository
    {
        private readonly SimpleRepository database;

        public SystemRepository()
        {
            this.database = new SimpleRepository("AgencySystemDataConnectionString", SimpleRepositoryOptions.None);
        }

        public bool AddSystemMessage(SystemMessage message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<SystemMessage>(message);
                result = true;
            }

            return result;
        }

        public bool AddDashboardMessage(DashboardMessage message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<DashboardMessage>(message);
                result = true;
            }

            return result;
        }

        #region CustomerNotes

        public bool AddCustomerNote(CustomerNote note)
        {
            bool result = false;
            if (note != null)
            {
                note.Id = Guid.NewGuid();
                note.Created = DateTime.Now;
                note.Modified = DateTime.Now;
                note.IsDeprecated = false;
                database.Add<CustomerNote>(note);
                result = true;
            }

            return result;
        }

        public CustomerNote GetCustomerNote(Guid agencyId, Guid noteId)
        {
            return database.Single<CustomerNote>(r => r.Id == noteId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateCustomerNote(CustomerNote note)
        {
            var result = false;
            if (note != null)
            {
                database.Update<CustomerNote>(note);
                result = true;
            }
            return result;
        }

        public IList<CustomerNote> GetCustomerNotes(Guid agencyId)
        {
            return database.Find<CustomerNote>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderByDescending(c => c.Created).ToList();
        }

        #endregion

    }
}
