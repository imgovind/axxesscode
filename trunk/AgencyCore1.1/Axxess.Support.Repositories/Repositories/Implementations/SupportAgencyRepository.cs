﻿namespace Axxess.Support.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class SupportAgencyRepository : ISupportAgencyRepository
    {
        #region Constructor

       // private readonly SimpleRepository database;

        private Func<string,SimpleRepository> databaseFactory = databaseIp => new SimpleRepository(string.Format(CoreSettings.AgencyManagementConnectionString, databaseIp), CoreSettings.AgencyManagementProviderName, SimpleRepositoryOptions.None);

        public SupportAgencyRepository()//SimpleRepository database
        {
            //Check.Argument.IsNotNull(database, "database");

            //this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool ToggleDelete(string databaseIp, Guid id)
        {
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    var agency = database.Single<Agency>(a => a.Id == id);
                    if (agency != null)
                    {
                        agency.IsDeprecated = !agency.IsDeprecated;
                        agency.IsSuspended = !agency.IsSuspended;
                        agency.Modified = DateTime.Now;
                        database.Update<Agency>(agency);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool Update(string databaseIp, Agency agency)
        {
            bool result = false;
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    if (agency != null)
                    {
                        return database.Update<Agency>(agency)>0;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool AddModal(string databaseIp, Agency agency)
        {
            bool result = false;
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    if (agency != null)
                    {
                        var x = database.Add<Agency>(agency);
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool Add(string databaseIp, Agency agency)
        {
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    if (agency != null)
                    {
                        agency.Id = Guid.NewGuid();
                        agency.OasisAuditVendor = (int)OasisAuditVendors.HHG;
                        agency.Created = DateTime.Now;
                        agency.Modified = DateTime.Now;
                        database.Add<Agency>(agency);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public Agency GetAgencyOnly(string databaseIp, Guid id)
        {
            Agency agency = null;
             var database = databaseFactory(databaseIp);
             if (database != null)
             {
                 agency = database.Single<Agency>(a => a.Id == id);
             }
            return agency;
        }

        public IEnumerable<Agency> All(string databaseIp)
        {
            IEnumerable<Agency> agencies = null;
             var database = databaseFactory(databaseIp);
             if (database != null)
             {
                 return database.All<Agency>().AsEnumerable<Agency>();
             }
             return agencies;
        }


        public IList<Agency> AllAgencies(string databaseIp)
        {
            IList<Agency> agencies = null;
             var database = databaseFactory(databaseIp);
             if (database != null)
             {
                  agencies = database.All<Agency>().ToList();
                 if (agencies != null && agencies.Count > 0)
                 {
                     agencies.ForEach(a =>
                     {
                         a.MainLocation = GetMainLocation(databaseIp,a.Id);
                     });
                     return agencies.OrderBy(a => a.Name).ToList();
                 }
                
             }
             return agencies;
        }


        public bool AddMultiAgency(string databaseIp, List<Agency> agencies)
        {
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    if (agencies != null && agencies.Count>0)
                    {
                        database.AddMany<Agency>(agencies);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }


        #endregion

        #region Location Methods

        public bool AddLocation(string databaseIp, AgencyLocation agencyLocation)
        {
            var result = false;
            try
            {
                if (agencyLocation != null)
                {
                    if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                    {
                        agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                    {
                        agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                    }

                    agencyLocation.Created = DateTime.Now;
                    agencyLocation.Modified = DateTime.Now;

                    var database = databaseFactory(databaseIp);
                    if (database != null)
                    {
                        database.Add<AgencyLocation>(agencyLocation);
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateLocation(string databaseIp, AgencyLocation agencyLocation)
        {
            var result = false;
            try
            {
                if (agencyLocation != null)
                {
                    var database = databaseFactory(databaseIp);
                    if (database != null)
                    {
                        database.Update<AgencyLocation>(agencyLocation);
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool EditLocationModal(string databaseIp, AgencyLocation location)
        {
            var result = false;
            try
            {
                if (location != null)
                {
                    try
                    {
                         var database = databaseFactory(databaseIp);
                         if (database != null)
                         {
                             location.Modified = DateTime.Now;
                             database.Update<AgencyLocation>(location);
                             result = true;
                         }
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public AgencyLocation GetMainLocation(string databaseIp, Guid agencyId)
        {
            AgencyLocation location = null;
             var database = databaseFactory(databaseIp);
             if (database != null)
             {
                location= database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
             }
            return location;
        }

        public AgencyLocation FindLocation(string databaseIp, Guid agencyId, Guid Id)
        {
            AgencyLocation location = null;
              var database = databaseFactory(databaseIp);
              if (database != null)
              {
                  location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id);
              }
            return location;
        }

        public IList<AgencyLocation> GetBranches(string databaseIp, Guid agencyId)
        {
            IList<AgencyLocation> locations = null;
            var database = databaseFactory(databaseIp);
            if (database != null)
            {
                locations = database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false, "Created", 0, 20).OrderBy(a => a.Name).ToList();
            }
            return locations;
        }

        public List<AgencyLocation> AgencyMainLocations(string databaseIp, List<Guid> agencyIds)
        {
            var list = new List<AgencyLocation>();
            if (agencyIds != null && agencyIds.Count > 0)
            {
                var ids = agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                        AgencyId as AgencyId,
                        AddressCity as AddressCity,
                        AddressStateCode as AddressStateCode
                            FROM
                                agencylocations 
                                    WHERE 
                                         IsMainOffice = 1 AND 
                                         IsDeprecated = 0 AND
                                         AgencyId IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<AgencyLocation>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                     .SetMap(reader => new AgencyLocation
                     {
                         AgencyId = reader.GetGuid("AgencyId"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode")

                     }).AsList();
                }
            }
            return list;
        }


        #endregion


        #region Agency User

        public User GetUserOnly(string databaseIp, Guid agencyId, Guid Id)
        {
            User user = null;
             var database = databaseFactory(databaseIp);
             if (database != null)
             {
                 user= database.Single<User>(u => u.AgencyId == agencyId && u.Id == Id);
             }
             return user;
        }

        public bool UpdateModel(string databaseIp, User user)
        {
            bool result = false;
            var database = databaseFactory(databaseIp);
            if (database != null)
            {
                if (user != null)
                {
                    user.Modified = DateTime.Now;
                    if (database.Update<User>(user) > 0)
                    {
                        result = true;

                    }
                }
            }
            return result;
        }

        public bool AddFirstUser(string databaseIp, User user)
        {
            if (user != null)
            {
                try
                {
                    var database = databaseFactory(databaseIp);
                    if (database != null)
                    {
                        user.Id = Guid.NewGuid();
                        if (user.AgencyRoleList.Count > 0) user.Roles = user.AgencyRoleList.ToArray().AddColons();
                        user.Status = (int)UserStatus.Active;
                        var userProfile = user.Profile;
                        //user.ProfileData = user.Profile.ToXml();
                        user.Messages = new List<MessageState>().ToXml();
                        if (user.PermissionsArray.Count > 0) user.Permissions = user.PermissionsArray.ToXml();
                        user.Created = DateTime.Now;
                        user.Modified = DateTime.Now;
                        database.Add<User>(user);
                        database.Add<UserProfile>(userProfile);
                        //UserEngine.Refresh(user.AgencyId);
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public List<User> GetAllUsers(string databaseIp, Guid agencyId)
        {
            var list = new List<User>();
           
            var script = string.Format(@"SELECT 
                                users.Id as Id ,
                                users.LoginId as LoginId ,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.TitleType as TitleType ,
                                users.TitleTypeOther as  TitleTypeOther ,
                                users.IsDeprecated  as IsDeprecated ,
                                users.Status as  Status,
                                users.Created as Created ,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther 
                                    FROM 
                                        users 
                                            WHERE 
                                                users.AgencyId = @agencyid AND users.IsDeprecated = 0 AND users.Status = {0} ", (int)PatientStatus.Active);
            using (var cmd = new FluentCommand<User>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new User
                {
                    Id = reader.GetGuid("Id"),
                    LoginId = reader.GetGuid("LoginId"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    Suffix = reader.GetStringNullable("Suffix"),
                    TitleType = reader.GetStringNullable("TitleType"),
                    TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Status = reader.GetInt("Status"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    Created = reader.GetDateTime("Created")
                }).AsList();
            }
            return list;
        }

        public bool UpdateUserProfile(string databaseIp, Guid agencyId, Guid userId, string profile)
        {
            bool result = false;
            if (profile.IsNotNullOrEmpty())
            {
                try
                {
                    var script = string.Format(@"UPDATE users set Profile = @profile WHERE AgencyId = @agencyid AND Id = @userId ");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userId", userId)
                        .AddString("profile", profile)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }


        #endregion

        //public bool AddSystemMessage(SystemMessage message)
        //{
        //    bool result = false;
        //    if (message != null)
        //    {
        //        message.Id = Guid.NewGuid();
        //        message.Created = DateTime.Now;
        //        database.Add<SystemMessage>(message);
        //        result = true;
        //    }

        //    return result;
        //}

        //public bool AddDashboardMessage(DashboardMessage message)
        //{
        //    bool result = false;
        //    if (message != null)
        //    {
        //        message.Id = Guid.NewGuid();
        //        message.Created = DateTime.Now;
        //        database.Add<DashboardMessage>(message);
        //        result = true;
        //    }

        //    return result;
        //}


        public IList<AgencyPhysician> GetAgencyPhysicians(string databaseIp, Guid agencyId)
        {
            IList<AgencyPhysician> physicians = null;
            var database = databaseFactory(databaseIp);
            if (database != null)
            {
                physicians=database.Find<AgencyPhysician>(p => p.AgencyId == agencyId && p.IsDeprecated == false).OrderBy(p => p.FirstName).ToList();
            }
            return physicians;
        }

        public IList<AgencyPhysician> GetAgencyPhysiciansWithPecosVerification(string databaseIp, Guid agencyId)
        {
            List<AgencyPhysician> list = new List<AgencyPhysician>();

            var script = @"SELECT 
                    ap.`Id`,
                    ap.`NPI`, 
                    ap.`Credentials`,
                    ap.`FirstName`,
                    ap.`LastName`,
                    ap.`MiddleName`,
                    ap.`Gender`,
                    ap.`AddressLine1`,
                    ap.`AddressLine2`, 
                    ap.`AddressCity`, 
                    ap.`AddressStateCode`,
                    ap.`AddressZipCode`,
                    ap.`PhoneWork`,
                    ap.`PhoneAlternate`,
                    ap.`FaxNumber`, 
                    ap.`EmailAddress`, 
                    ap.`PhysicianAccess`,
                    pp.Id IS NOT NULL AS IsPecosVerified 
                        FROM 
                            agencymanagement.agencyphysicians ap LEFT JOIN axxesslookup.pecosphysicians pp ON ap.NPI = pp.Id 
                                WHERE
                                    ap.AgencyId = @agencyid AND
                                    ap.IsDeprecated = 0";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    NPI = reader.GetStringNullable("NPI"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Credentials = reader.GetStringNullable("Credentials"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    PhysicianAccess = reader.GetBoolean("PhysicianAccess"),
                    IsPecosVerified = reader.GetBoolean("IsPecosVerified")
                }).AsList();
            }
            return list;
        }



        #region Template Methods

        public void InsertTemplates(string databaseIp , Guid agencyId)
        {
            var templateSQL = MessageBuilder.ReadTextFrom("Templates");

            if (templateSQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(templateSQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Supply Methods

        public void InsertSupplies(string databaseIp, Guid agencyId)
        {
            var supplySQL = MessageBuilder.ReadTextFrom("Supplies");

            if (supplySQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(supplySQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                    .AsNonQuery();
                }
            }
        }

        #endregion

        //#region CustomerNotes

        //public bool AddCustomerNote(CustomerNote note)
        //{
        //    bool result = false;
        //    if (note != null)
        //    {
        //        note.Id = Guid.NewGuid();
        //        note.Created = DateTime.Now;
        //        note.Modified = DateTime.Now;
        //        note.IsDeprecated = false;
        //        database.Add<CustomerNote>(note);
        //        result = true;
        //    }

        //    return result;
        //}

        //public CustomerNote GetCustomerNote(Guid agencyId, Guid noteId)
        //{
        //    return database.Single<CustomerNote>(r => r.Id == noteId && r.AgencyId == agencyId && r.IsDeprecated == false);
        //}

        //public bool UpdateCustomerNote(CustomerNote note)
        //{
        //    var result = false;
        //    if (note != null)
        //    {
        //        database.Update<CustomerNote>(note);
        //        result = true;
        //    }
        //    return result;
        //}

        //public IList<CustomerNote> GetCustomerNotes(Guid agencyId)
        //{
        //    return database.Find<CustomerNote>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderByDescending(c => c.Created).ToList();
        //}

        //#endregion

        #region Move Data

        public List<PatientEpisode> GetAgencyPatientEpisodes(string databaseIp, Guid agencyId)
        {

           var list = new List<PatientEpisode>();

            var script = @"SELECT 
                    `Id`,
                    `PatientId`, 
                    `AgencyId`,
                    `Schedule`
                        FROM 
                            patientepisodes
                                WHERE
                                    AgencyId = @agencyid ";

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule")
                }).AsList();
            }
            return list;
        }

        public List<Guid> GetExistingSchedulesIds(string databaseIp, List<Guid> agencyIds)
        {

            var ids = new List<Guid>();
            if (agencyIds != null && agencyIds.Count > 0)
            {
                var agencyFilter = string.Empty;
                if (agencyIds.Count == 1)
                {
                    agencyFilter = string.Format(" AgencyId = '{0}' ", agencyIds.FirstOrDefault());
                }
                else
                {
                    agencyFilter = string.Format(" AgencyId IN ({0}) ", agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }

                var script = string.Format(@"SELECT 
                        `Id`
                            FROM 
                                `scheduleevents`
                                    WHERE  
                                        {0} ", agencyFilter);

                using (var cmd = new FluentCommand<Guid>(script))
                {
                    ids = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                       .SetMap(reader =>

                           reader.GetGuid("Id")

                       ).AsList();
                }
            }

            return ids;

        }

        public bool AddSchedules(string databaseIp, List<ScheduleEvent> scheduleEvents)
        {
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                    {
                        database.AddMany<ScheduleEvent>(scheduleEvents);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public List<Assessment> GetAllAssessments(string databaseIp, List<Guid> agencyIds, List<Guid> assesmentIds, string tableName)
        {

            var assessments = new List<Assessment>();
            if (agencyIds != null && agencyIds.Count > 0)
            {
                var agencyFilter = string.Empty;
                if (agencyIds.Count == 1)
                {
                    agencyFilter = string.Format(" AgencyId = '{0}' ", agencyIds.FirstOrDefault());
                }
                else
                {
                    agencyFilter = string.Format(" AgencyId IN ({0}) ", agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }

                var idFilter = string.Empty;
                if (assesmentIds != null && assesmentIds.Count > 0)
                {
                    if (assesmentIds.Count == 1)
                    {
                        idFilter = string.Format(" Id != '{0}' ", assesmentIds.FirstOrDefault());
                    }
                    else
                    {
                        idFilter = string.Format(" Id NOT IN ({0}) ", assesmentIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    }
                }
                var oasisDatabase = new[]{
                "startofcareassessments"
                ,"recertificationassessments"
                ,"resumptionofcareassessments"
                ,"transferdischargeassessments"
                ,"transfernotdischargedassessments"
                ,"followupassessments"
                ,"dischargefromagencyassessments"
                ,"deathathomeassessments"
                ,"nonoasisdischargeassessments"
                ,"nonoasisrecertificationassessments"
                ,"nonoasisstartofcareassessments"
            };
                if (oasisDatabase.Contains(tableName))
                {
                    var script = string.Format(@"SELECT 
                        `Id`, 
                        `AgencyId`,
                        `PatientId`,
                        `EpisodeId`,
                        `UserId`, 
                        `OasisData`,
                        `HippsVersion`,
                        `HippsCode`,
                        `ClaimKey`, 
                        `SubmissionFormat`, 
                        `CancellationFormat`, 
                        `Status`,
                        `VersionNumber`,
                        `AssessmentDate`,
                        `ExportedDate`, 
                        `Created`, 
                        `Modified`, 
                        `Supply`,
                        `IsDeprecated`,
                        `SignatureText`,
                        `SignatureDate`,
                        `TimeIn`,
                        `TimeOut`,
                        `Type`, 
                        `IsValidated` 
                            FROM 
                                `{0}`
                                    WHERE  
                                        {1} {2} ", tableName, agencyFilter, idFilter.IsNotNullOrEmpty() ? " AND " + idFilter : string.Empty);
                   
                    using (var cmd = new FluentCommand<Assessment>(script))
                    {
                        assessments = cmd.SetConnection("OasisCConnectionString", databaseIp)
                           .SetMap(reader => new Assessment
                           {
                               Id = reader.GetGuid("Id"),
                               AgencyId = reader.GetGuid("AgencyId"),
                               PatientId = reader.GetGuid("PatientId"),
                               EpisodeId = reader.GetGuid("EpisodeId"),
                               UserId = reader.GetGuidIncludeEmpty("UserId"),
                               OasisData = reader.GetStringNullable("OasisData"),
                               HippsVersion = reader.GetStringNullable("HippsVersion"),
                               HippsCode = reader.GetStringNullable("HippsCode"),
                               ClaimKey = reader.GetStringNullable("ClaimKey"),
                               SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                               CancellationFormat = reader.GetStringNullable("CancellationFormat"),
                               Status = reader.GetInt("Status"),
                               VersionNumber = reader.GetInt("VersionNumber"),
                               AssessmentDate = reader.GetDateTime("AssessmentDate"),
                               ExportedDate = reader.GetDateTime("ExportedDate"),
                               Created = reader.GetDateTime("Created"),
                               Modified = reader.GetDateTime("Modified"),
                               Supply = reader.GetStringNullable("Supply"),
                               IsDeprecated = reader.GetBoolean("IsDeprecated"),
                               SignatureText = reader.GetStringNullable("SignatureText"),
                               SignatureDate = reader.GetDateTime("SignatureDate", DateTime.MinValue),
                               TimeIn = reader.GetStringNullable("TimeIn"),
                               TimeOut = reader.GetStringNullable("TimeOut"),
                               Type = reader.GetStringNullable("Type"),
                               IsValidated = reader.GetBoolean("IsValidated")
                           }).AsList();
                    }
                }

            }
            return assessments;

        }

        public List<Assessment> GetAllAssessments(string databaseIp, List<Guid> agencyIds)
        {
            
            var assessments = new List<Assessment>();
            if (agencyIds != null && agencyIds.Count > 0)
            {
                var agencyFilter = string.Empty;
                if (agencyIds.Count == 1)
                {
                    agencyFilter = string.Format(" AgencyId = '{0}' ",agencyIds.FirstOrDefault());
                }
                else
                {
                    agencyFilter = string.Format(" AgencyId IN ({0}) ", agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }
                var oasisDatabase = new[]{
                "startofcareassessments"
                ,"recertificationassessments"
                ,"resumptionofcareassessments"
                ,"transferdischargeassessments"
                ,"transfernotdischargedassessments"
                ,"followupassessments"
                ,"dischargefromagencyassessments"
                ,"deathathomeassessments"
                ,"nonoasisdischargeassessments"
                ,"nonoasisrecertificationassessments"
                ,"nonoasisstartofcareassessments"
            };
                var script = @"SELECT 
                        `Id`, 
                        `AgencyId`,
                        `PatientId`,
                        `EpisodeId`,
                        `UserId`, 
                        `OasisData`,
                        `HippsVersion`,
                        `HippsCode`,
                        `ClaimKey`, 
                        `SubmissionFormat`, 
                        `CancellationFormat`, 
                        `Status`,
                        `VersionNumber`,
                        `AssessmentDate`,
                        `ExportedDate`, 
                        `Created`, 
                        `Modified`, 
                        `Supply`,
                        `IsDeprecated`,
                        `SignatureText`,
                        `SignatureDate`,
                        `TimeIn`,
                        `TimeOut`,
                        `Type`, 
                        `IsValidated` 
                            FROM 
                                `{0}`
                                    WHERE  
                                        {1} ";
                var unionScriptList = new List<string>();
                oasisDatabase.ForEach(d =>
                {
                    unionScriptList.Add(string.Format(script, d, agencyFilter));
                });
                var unionScript = unionScriptList.ToArray().Join(" UNION ");
                using (var cmd = new FluentCommand<Assessment>(unionScript))
                {
                    assessments = cmd.SetConnection("OasisCConnectionString",databaseIp)
                       .SetMap(reader => new Assessment
                       {
                           Id = reader.GetGuid("Id"),
                           AgencyId = reader.GetGuid("AgencyId"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           UserId = reader.GetGuidIncludeEmpty("UserId"),
                           OasisData = reader.GetStringNullable("OasisData"),
                           HippsVersion = reader.GetStringNullable("HippsVersion"),
                           HippsCode = reader.GetStringNullable("HippsCode"),
                           ClaimKey = reader.GetStringNullable("ClaimKey"),
                           SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                           CancellationFormat = reader.GetStringNullable("CancellationFormat"),
                           Status = reader.GetInt("Status"),
                           VersionNumber = reader.GetInt("VersionNumber"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           ExportedDate = reader.GetDateTime("ExportedDate"),
                           Created = reader.GetDateTime("Created"),
                           Modified = reader.GetDateTime("Modified"),
                           Supply = reader.GetStringNullable("Supply"),
                           IsDeprecated = reader.GetBoolean("IsDeprecated"),
                           SignatureText = reader.GetStringNullable("SignatureText"),
                           SignatureDate = reader.GetDateTime("SignatureDate",DateTime.MinValue),
                           TimeIn = reader.GetStringNullable("TimeIn"),
                           TimeOut = reader.GetStringNullable("TimeOut"),
                           Type = reader.GetStringNullable("Type"),
                           IsValidated = reader.GetBoolean("IsValidated")
                       }).AsList();
                }
            }

            return assessments;

        }

        public List<Assessment> GetAllAssessments(string databaseIp)
        {

              var assessments = new List<Assessment>();
                
                var oasisDatabase = new[]{
                "startofcareassessments"
                ,"recertificationassessments"
                ,"resumptionofcareassessments"
                ,"transferdischargeassessments"
                ,"transfernotdischargedassessments"
                ,"followupassessments"
                ,"dischargefromagencyassessments"
                ,"deathathomeassessments"
                ,"nonoasisdischargeassessments"
                ,"nonoasisrecertificationassessments"
                ,"nonoasisstartofcareassessments"
            };
                var script = @"SELECT 
                        `Id`, 
                        `AgencyId`,
                        `PatientId`,
                        `EpisodeId`,
                        `UserId`, 
                        `OasisData`,
                        `HippsVersion`,
                        `HippsCode`,
                        `ClaimKey`, 
                        `SubmissionFormat`, 
                        `CancellationFormat`, 
                        `Status`,
                        `VersionNumber`,
                        `AssessmentDate`,
                        `ExportedDate`, 
                        `Created`, 
                        `Modified`, 
                        `Supply`,
                        `IsDeprecated`,
                        `SignatureText`,
                        `SignatureDate`,
                        `TimeIn`,
                        `TimeOut`,
                        `Type`, 
                        `IsValidated` 
                            FROM 
                                `{0}` ";
                var unionScriptList = new List<string>();
                oasisDatabase.ForEach(d =>
                {
                    unionScriptList.Add(string.Format(script, d));
                });
                var unionScript = unionScriptList.ToArray().Join(" UNION ");

                using (var cmd = new FluentCommand<Assessment>(unionScript))
                {
                    assessments = cmd.SetConnection("OasisCConnectionString",databaseIp)
                       .SetMap(reader => new Assessment
                       {
                           Id = reader.GetGuid("Id"),
                           AgencyId = reader.GetGuid("AgencyId"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           UserId = reader.GetGuid("UserId"),
                           OasisData = reader.GetStringNullable("OasisData"),
                           HippsVersion = reader.GetStringNullable("HippsVersion"),
                           HippsCode = reader.GetStringNullable("HippsCode"),
                           ClaimKey = reader.GetStringNullable("ClaimKey"),
                           SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                           CancellationFormat = reader.GetStringNullable("CancellationFormat"),
                           Status = reader.GetInt("Status"),
                           VersionNumber = reader.GetInt("VersionNumber"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           ExportedDate = reader.GetDateTime("ExportedDate"),
                           Created = reader.GetDateTime("Created"),
                           Modified = reader.GetDateTime("Modified"),
                           Supply = reader.GetStringNullable("Supply"),
                           IsDeprecated = reader.GetBoolean("IsDeprecated"),
                           SignatureText = reader.GetStringNullable("SignatureText"),
                           SignatureDate = reader.GetDateTime("SignatureDate"),
                           TimeIn = reader.GetStringNullable("TimeIn"),
                           TimeOut = reader.GetStringNullable("TimeOut"),
                           Type = reader.GetStringNullable("Type"),
                           IsValidated = reader.GetBoolean("IsValidated")
                       }).AsList();
                }

            return assessments;

        }


        public List<Guid> GetExistingAssessmentsIds(string databaseIp, List<Guid> agencyIds)
        {

            var ids = new List<Guid>();
            if (agencyIds != null && agencyIds.Count > 0)
            {
                var agencyFilter = string.Empty;
                if (agencyIds.Count == 1)
                {
                    agencyFilter = string.Format(" AgencyId = '{0}' ", agencyIds.FirstOrDefault());
                }
                else
                {
                    agencyFilter = string.Format(" AgencyId IN ({0}) ", agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }
               
                var script =string.Format(@"SELECT 
                        `Id`
                            FROM 
                                `assessments`
                                    WHERE  
                                        {0} ",agencyFilter);
              
                using (var cmd = new FluentCommand<Guid>(script))
                {
                    ids = cmd.SetConnection("AgencyManagementConnectionString", databaseIp)
                       .SetMap(reader => 
                       
                           reader.GetGuid("Id")
                           
                       ).AsList();
                }
            }

            return ids;

        }


        public bool AddAssessments(string databaseIp, List<Assessment> assessments)
        {
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                    if (assessments != null && assessments.Count > 0)
                    {
                        database.AddMany<Assessment>(assessments);
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public List<T> GetEntity<T>(string databaseIp, Guid agencyId) where T : AgencyBase, new()
        {
            try
            {
                var database = databaseFactory(databaseIp);
                if (database != null)
                {
                      return database.Find<T>( l => l.AgencyId == agencyId).ToList(); 
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        public bool LoadEntity<T>(string databaseIp, List<T> listOfData) where T : class, new()
        {
            try
            {
                if (listOfData != null && listOfData.Count > 0)
                {
                    var database = databaseFactory(databaseIp);
                    if (database != null)
                    {
                        if (listOfData != null && listOfData.Count > 0)
                        {
                            database.AddMany<T>(listOfData);
                            return true;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }


        public List<PatientPhysician> PatientPhysicians(string databaseIp, Guid agencyId)
        {

            var patientPhysicians = new List<PatientPhysician>();

            var script = @"SELECT 
                        `Id`, 
                        `PatientId`,
                        `PhysicianId`,
                        `IsPrimary` 
                            FROM 
                                `patientphysicians` ";


            using (var cmd = new FluentCommand<PatientPhysician>(script))
            {
                patientPhysicians = cmd.SetConnection("AgencyManagementConnectionString")
                   .SetMap(reader => new PatientPhysician
                   {
                       Id = reader.GetInt("Id"),
                       PatientId = reader.GetGuid("PatientId"),
                       PhysicianId = reader.GetGuid("PhysicianId"),
                       IsPrimary = reader.GetBoolean("IsPrimary")
                   }).AsList();
            }

            return patientPhysicians;

        }


        public List<PatientUser> PatientUsers(string databaseIp, Guid agencyId)
        {

            var patientUsers = new List<PatientUser>();

            var script = @"SELECT 
                        `Id`, 
                        `PatientId`,
                        `UserId`,
                            FROM 
                                `patientusers` ";


            using (var cmd = new FluentCommand<PatientUser>(script))
            {
                patientUsers = cmd.SetConnection("AgencyManagementConnectionString",databaseIp)
                   .SetMap(reader => new PatientUser
                   {
                       Id = reader.GetGuid("Id"),
                       PatientId = reader.GetGuid("PatientId"),
                       UserId = reader.GetGuid("UserId"),
                   }).AsList();
            }

            return patientUsers;

        }


        #endregion

    }
}
