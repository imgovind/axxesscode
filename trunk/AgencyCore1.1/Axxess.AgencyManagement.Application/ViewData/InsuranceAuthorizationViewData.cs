﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Entities;

   public class InsuranceAuthorizationViewData
    {
       public string HealthPlanId { get; set; }
       public string GroupName { get; set; }
       public string GroupId { get; set; }
       public string Relationship { get; set; }
       public Authorization Authorization { get; set; }
       public List<SelectListItem> Authorizations { get; set; }

       public string ClaimTypeIdentifier { get; set; }
    }
}
