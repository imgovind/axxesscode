﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class PlanofCareViewData
    {
        public PlanofCareViewData()
        {
            this.DisplayData = new Dictionary<string, string>();
            this.PatientData = new Dictionary<string, string>();
        }
        public AgencyServices Service { get; set; }
        public bool IsUserCanAddPhysicain { get; set; }
        public bool IsUserCanEditPatient { get; set; }
        public bool IsUserCanLoadPrevious { get; set; }
        public bool IsUserCanSeeSticky { get; set; }
        public PlanofCare PlanOfCare { get; set; }
        public IDictionary<string, string> PatientData { get; set; }
        public IDictionary<string, string> DisplayData { get; set; }
    }
}