﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class MedicationProfileViewData
    {
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public string DisplayName { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public MedicationProfile MedicationProfile { get; set; }
        public string PrimaryDiagnosis { get; set; }
        public string SecondaryDiagnosis { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianDisplayName { get; set; }
        public AssessmentType AssessmentType { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public AllergyProfile AllergyProfile { get; set; }
        public Agency Agency { get; set; }
        public string SignatureName { get; set; }
        public DateTime SignatureDate { get; set; }

        public bool IsUserCanAdd { get; set; }
        public bool IsUserCanSign { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanViewLog { get; set; }
    }
}