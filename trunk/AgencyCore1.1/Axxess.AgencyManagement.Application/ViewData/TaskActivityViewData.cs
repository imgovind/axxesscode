﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Enums;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
   public abstract class TaskActivityViewData 
    {
        public bool IsOASISProfileExist { get; set; }
        public bool IsStickyNote { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanReassign { get; set; }
        public bool IsUserCanReopen { get; set; }
        public bool IsUserCanEditDetail { get; set; }
        public bool IsUserCanRestore { get; set; }
        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanAdd { get; set; }
        public bool IsUserCanExport { get; set; }
        public List<GridTask> ScheduleEvents { get; set; }
        public AgencyServices Service { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }

        public int PrintDocuments { get; set; }
        public int EditDocuments { get; set; }
        public int DeleteDocuments { get; set; }
        public int RestoreDocuments { get; set; }
        public int ViewDocuments { get; set; }

        public bool IsUserCanEditEpisode { get; set; }
        public bool IsUserCanAddEpisode { get; set; }
        public bool IsUserCanViewEpisodeList { get; set; }


    }
}
