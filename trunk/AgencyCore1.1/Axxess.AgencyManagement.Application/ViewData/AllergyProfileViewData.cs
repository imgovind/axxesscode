﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public class AllergyProfileViewData
    {
        public Guid EpisodeId { get; set; }
        public PatientProfileLean PatientProfile { get; set; }
        public AllergyProfile AllergyProfile { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianDisplayName { get; set; }
        public LocationPrintProfile LocationProfile { get; set; }
        public bool IsCanUserAdd { get; set; }
        public bool IsCanUserPrint { get; set; }
    }
}