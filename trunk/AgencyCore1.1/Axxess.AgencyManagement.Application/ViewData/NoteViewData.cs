﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;

    using Axxess.Core.Infrastructure;

    public class NoteViewData : JsonViewData
    {
       public string Note { get; set; }
    }
}
