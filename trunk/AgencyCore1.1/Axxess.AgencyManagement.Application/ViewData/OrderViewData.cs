﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using Axxess.Core.Enums;

    public class OrderViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public string OrderDate { get; set; }
        public string OrderSummary { get; set; }
        public string OrderText { get; set; }
        public int OrderProcessType { get; set; }
        public AgencyServices Service { get; set; }

        public OrderViewData()
        {

        }

        public OrderViewData(AgencyServices service)
        {
            this.Service = service;
        }

        public bool IsUserCanAddPhysicain { get; set; }
        public bool IsUserCanAdd { get; set; }
    }
}
