﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using System.Web.Mvc;

    public class MultipleEpisodeViewData
    {
        public Guid PatientId { get; set; }
        public List<SelectListItem> EpisodeItems { get; set; }
    }
}
