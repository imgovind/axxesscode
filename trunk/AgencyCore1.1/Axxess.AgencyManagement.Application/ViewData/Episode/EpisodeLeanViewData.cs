﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;

    public class EpisodeLeanViewData
    {
        public List<EpisodeLean> List { get; set; }
        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanReactivate { get; set; }
    }
}
