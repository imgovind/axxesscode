﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class EpisodeAndTaskViewData<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        public List<T> Tasks { get; set; }
        public List<E> CarePeriods { get; set; }

        public EpisodeAndTaskViewData(List<T> tasks, List<E> careperiods)
        {
            this.Tasks = tasks;
            this.CarePeriods = careperiods;
        }
    }
}
