﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Infrastructure;

   public class NoteJsonViewData : JsonViewData
   {
       public string Command { get; set; }  
   }
}
