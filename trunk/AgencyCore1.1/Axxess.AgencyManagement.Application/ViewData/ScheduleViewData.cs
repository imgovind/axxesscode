﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    public class ScheduleViewData
    {
        public ScheduleViewData()
        {
            this.CalendarData = new CalendarViewData();
            this.SelectionViewData = new PatientSelectionViewData();
        }
        public PatientSelectionViewData SelectionViewData { get; set; }
        public CalendarViewData CalendarData { get; set; }
       
    }
}
