﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class ServiceAndGuidViewData
    {
        public Guid Id { get; set; }
        /// <summary>
        /// reperesent the current request/response service
        /// </summary>
        public AgencyServices Service { get; set; }

       
       
  
        public ServiceAndGuidViewData(AgencyServices service)
        {
            this.Service = service;
        }

        public ServiceAndGuidViewData(AgencyServices service, Guid id)
        {
            this.Id = id;
            this.Service = service;
        }

        public bool IsUserCanAddPhysicain { get; set; }
        /// <summary>
        /// this is the permission on the context of the document 
        /// </summary>
        public bool IsUserCanAdd { get; set; }
    }
}
