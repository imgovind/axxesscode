﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class CommunicationNoteViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid LocationId { get; set; }
        public List<CommunicationNote> Notes { get; set; }
        public string DisplayName { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public bool IsSinglePatient { get; set; }
        public AgencyServices Service { get; set; }

        /// <summary>
        /// Reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }
        /// <summary>
        /// Holds the permission for New
        /// </summary>

        public AgencyServices NewPermissions { get; set; }
        /// <summary>
        /// Export Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }

        /// <summary>
        /// View List Permissions
        /// </summary>
        public AgencyServices ViewListPermissions { get; set; }


        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanPrint { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
    }
}
