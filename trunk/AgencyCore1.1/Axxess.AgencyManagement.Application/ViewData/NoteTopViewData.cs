﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class NoteTopViewData
    {
        public NoteTopViewData(string title, Guid id, Guid patientId, Guid episodeId, string statusComment, AgencyServices service, string colWidth)
        {
            this.Title = title;
            this.Id = id;
            this.PatientId = patientId;
            this.EpisodeId = episodeId;
            this.StatusComment = statusComment;
            this.Service = service;
            this.ColWidth = colWidth;
        }

        public NoteTopViewData(Guid id, Guid patientId, Guid episodeId, string statusComment, AgencyServices service)
        {
            this.Id = id;
            this.PatientId = patientId;
            this.EpisodeId = episodeId;
            this.StatusComment = statusComment;
            this.Service = service;
        }

        public string Title { get; set; }

        public AgencyServices Service { get; set; }

        public Guid Id { get; set; }

        public Guid PatientId { get; set; }

        public Guid EpisodeId { get; set; }

        public string StatusComment { get; set; }

        public string ColWidth { get; set; }
    }
}
