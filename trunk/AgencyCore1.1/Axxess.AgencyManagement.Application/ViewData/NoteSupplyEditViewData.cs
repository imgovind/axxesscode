﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.LookUp.Domain;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class NoteSupplyEditViewData
    {
        public Guid EventId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Supply Supply { get; set; }
        public AgencyServices Service { get; set; }

        public NoteSupplyEditViewData()
        {

        }

        public NoteSupplyEditViewData(Guid eventId, Guid patientId, Guid episodeId, Supply supply)
        {
            EventId = eventId;
            PatientId = patientId;
            EpisodeId = episodeId;
            Supply = supply;
        }
    }
}
