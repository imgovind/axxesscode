﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class NoteSupplyNewViewData
    {
        public Guid EventId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public AgencyServices Service { get; set; }

        public NoteSupplyNewViewData()
        {

        }

        public NoteSupplyNewViewData(Guid eventId, Guid patientId, Guid episodeId)
        {
            EventId = eventId;
            PatientId = patientId;
            EpisodeId = episodeId;
        }
    }
}
