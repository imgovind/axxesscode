﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using System.Web.UI.WebControls;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;

    public class EntityVitalSignViewData
    {
        public Guid PatientId { get; set; }
        public Guid EntityId { get; set; }
        public string PatientName { get; set; }
        public string EntityName { get; set; }

        public EntityVitalSignViewData()
        {
            
        }

        public EntityVitalSignViewData(VisitNoteViewData viewData)
        {
            PatientId = viewData.PatientId;
            EntityId = viewData.EventId;
            PatientName = viewData.PatientProfile.DisplayName;
            EntityName = viewData.DisciplineTask.ToEnum(DisciplineTasks.NoDiscipline).GetDescription();
        }
    }
}
