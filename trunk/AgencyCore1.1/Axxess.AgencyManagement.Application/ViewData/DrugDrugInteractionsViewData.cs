﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    using LexiData;

    public class DrugDrugInteractionsViewData
    {
        public PatientProfileLean PatientProfile { get; set; }
        public LocationPrintProfile LocationProfile { get; set; }
        public List<DrugDrugInteractionResult> DrugDrugInteractions { get; set; }
    }
}
