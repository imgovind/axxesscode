﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using System.ComponentModel.DataAnnotations;

    using Axxess.LookUp.Domain;

    public class VisitNoteViewData
    {
        public VisitNoteViewData()
        {
            //this.PreviousNotes = new List<ScheduleEvent>();
            //this.PreviousPrivateDutyScheduleTasks = new List<PrivateDutyScheduleTask>();
        }
        public LocationPrintProfile LocationProfile { get; set; }
        public PatientProfileLean PatientProfile { get; set; }
        public VitalSignLog VitalSignLog { get; set; }

        public List<PreviousNote> PreviousNotes { get; set; }
        public IDictionary<string, NotesQuestion> Questions { get; set; }
        public IDictionary<string, NotesQuestion> WoundCare { get; set; }
        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EventStartDate { get; set; }

        public DateTime VisitStartDate { get; set; }
        public DateTime VisitEndDate { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string Surcharge { get; set; }
        public string AssociatedMileage { get; set; }

        public Guid SelectedEpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid EventId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
        public string PhysicianDisplayName { get; set; }
      
        public bool IsWoundCareExist { get; set; }
        public bool IsSupplyExist { get; set; }


        public bool IsCarePlanVisible{ get; set; }
        public Guid CarePlanOrEvalId { get; set; }
        public string CarePlanOrEvalGroup { get; set; }

        public string SignatureText { get; set; }
        public string SignatureDate { get; set; }

        public string StatusComment { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public string UserDisplayName { get; set; }
        public string Allergies { get; set; }
        public string PrintViewJson { get; set; }
        public int DisciplineTask { get; set; }
        public string TaskName { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime PhysicianSignatureDate { get; set; }

        //public NoteArguments Arguments { get; set; }
        public string ContentPath { get; set; }
        public string EpisodeRange { get { return (this.StartDate.IsValid() && this.EndDate.IsValid()) ? string.Format("{0} - {1}", this.StartDate.ToString("MM/dd/yyyy"), this.EndDate.ToString("MM/dd/yyyy")) : string.Empty; } }
        

        public int Version { get; set; }
        public bool HasReturnReasons { get; set; }
        public bool CanChangeDisciplineType { get; set; }

       
        public int VitalSignNewVersion { get; set; }

        public FooterSettings FooterSettings { get; set; }

        public AgencyServices Service { get; set; }

      
        /// <summary>
        /// this represent the time in , time out milage, Surcharge charge
        /// </summary>
        public bool IsVisitParameterVisible { get; set; }
        public bool IsTravelParameterVisible { get; set; }
        public bool IsTimeIntervalVisible { get; set; }
        public bool IsPhysicianVisible { get; set; }
        public bool IsPhysicianDateVisible { get; set; }
        public bool IsSelectedEpisodeVisible { get; set; }
        public bool IsDiagnosisVisible { get; set; }
        public bool IsAllergiesVisible { get; set; }
        public bool IsAllergiesSavable { get; set; }
        public bool IsDNRVisible { get; set; }
        public bool IsFrequencyVisible { get; set; }
      
        public bool IsUserCanMarkAsOrder { get; set; }

        public bool IsUserCanLoadPreviousNotes { get; set; }
        public bool IsUserCanScheduleVisits { get; set; }
        public bool IsUserCanCreateInfection { get; set; }
        public bool IsUserCanCreateIncident { get; set; }
        public bool IsUserCanCreateOrder { get; set; }
        public bool IsUserCanReturn { get; set; }
        public bool IsUserCanApprove { get; set; }

        [UIHint("HiddenMinDate")]
        public DateTime SortableSignatureDate
        {
            get
            {
                return this.SignatureDate.IsValidDate() ? this.SignatureDate.ToDateTime() : DateTime.MinValue;
            }
        }
        [UIHint("HiddenMinDate")]
        public DateTime SortableVisitDate
        {
            get
            {
                return this.VisitStartDate.IsValid() ? this.VisitStartDate : DateTime.MinValue;
            }
        }

        public string WoundCareJson { get; set; }
       
    }
}