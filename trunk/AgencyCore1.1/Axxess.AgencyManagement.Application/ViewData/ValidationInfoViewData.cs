﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;

    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Enums;


    public class ValidationInfoViewData
    {
      public Guid AssessmentId { get; set; }
      public Guid PatientId { get; set; }
      public Guid EpisodeId { get; set; }
      public string AssessmentType { get; set; }
      public List<ValidationError> ValidationErrors { get; set; }
      public int Count { get; set; }
      public string Message { get; set; }
      public string HIPPSCODE { get; set; }
      public string HIPPSKEY { get; set; }
      public double StandardPaymentRate { get; set; }
      public string HHRG { get; set; }
      public DateTime EpisodeStartDate { get; set; }
      public DateTime EpisodeEndDate { get; set; }
      public string TimeIn { get; set; }
      public string TimeOut { get; set; }
      public bool isValid { set; get; }
      public string Status { set; get; }
      public bool IsErrorFree { set; get; }
      public int Version { get; set; }
      public AgencyServices Service { get; set; }
    }
}
