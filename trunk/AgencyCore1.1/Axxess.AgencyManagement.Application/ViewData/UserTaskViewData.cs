﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class UserTaskViewData : TaskActivityViewData
    {
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public string GroupName { get; set; }
       
    }
}
