﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    public class SingleServiceGridViewData
    {
        /// <summary>
        /// Represent entity id, usually location id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Represent field Id,such as isurance, payor
        /// </summary>
        public int SubId { get; set; }

        /// <summary>
        /// Represent status Id,such as patient, claim
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Represent field diaplay name, usually patient, user, agency 
        /// </summary>
        public string DisplayName { get; set; }


        /// <summary>
        /// reperesent the current request/response service
        /// </summary>
        public AgencyServices Service { get; set; }
        /// <summary>
        /// reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }
        /// <summary>
        /// Holds the permission of new
        /// </summary>

        public bool IsUserCanAdd { get; set; }
        /// <summary>
        /// Export Permissions
        /// </summary>
        public bool IsUserCanExport { get; set; }

        /// <summary>
        /// View List Permissions
        /// </summary>
        public bool IsUserCanViewList { get; set; }

        /// <summary>
        /// Edit Permissions
        /// </summary>
        public bool IsUserCanEdit { get; set; }

        /// <summary>
        /// Delete Permissions
        /// </summary>
        public bool IsUserCanDelete { get; set; }
        /// <summary>
        /// Delete Permissions
        /// </summary>
        public bool IsUserCanPrint { get; set; }

        public string ServiceName
        {
            get
            {
                if (this.Service > 0 && Enum.IsDefined(typeof(AgencyServices), this.Service))
                {
                    return this.Service.GetDescription();
                }
                return string.Empty;

            }
        }

    }
}
