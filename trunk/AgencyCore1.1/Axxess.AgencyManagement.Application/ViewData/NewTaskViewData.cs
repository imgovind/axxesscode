﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class NewTaskViewData
    {
        public string PatientName { get; set; }
        public Guid PatientId { get; set; }
        public string UserName { get; set; }
        public Guid UserId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StartDate { get; set; }

        public NewTaskViewData(Guid patientId, Guid userId)
        {
            this.PatientId = patientId;
            this.UserId = userId;
        }
    }
}
