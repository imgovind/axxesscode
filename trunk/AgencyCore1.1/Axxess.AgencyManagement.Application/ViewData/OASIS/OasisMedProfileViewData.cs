﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class OasisMedicationProfileViewData
    {
        public Guid Id { get; set; }
        public string AssessmentType { get; set; }
        public AgencyServices Service { get; set; }
        public MedicationProfile Profile { get; set; }
    }
}