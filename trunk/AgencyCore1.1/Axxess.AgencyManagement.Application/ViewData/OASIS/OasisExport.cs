﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    public class OasisExport
    {
        
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Insurance { get; set; }
        public string Identifier { get; set; }
        public string PatientName { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentName { get; set; }
        public string AssessmentDate { get; set; }
        public int CorrectionNumber { get; set; }
        public string CorrectionNumberFormat { get; set; }
    }
}
