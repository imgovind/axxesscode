﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;

    public class OasisAudit
    {
        public Guid Id { get; set; }
        public PatientProfileLean PatientProfile { get; set; }
        public Agency Agency { get; set; }
        public AgencyLocation Location { get; set; }
        public DateTime AssessmentDate { get; set; }
        public string TypeDescription { get; set; }
        public List<Axxess.Api.Contracts.LogicalError> LogicalErrors { get; set; }
    }
}
