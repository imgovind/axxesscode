﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;


    public class AssessmentExportViewData
    {
        public Guid LocationId { get; set; }
        public List<AssessmentExport> Assessments { get; set; }
        public AgencyServices Service { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }

        /// <summary>
        /// reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }

        /// <summary>
        /// View List Permissions
        /// </summary>
        public AgencyServices ViewListPermissions { get; set; }

        /// <summary>
        /// Export Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }

        /// <summary>
        /// Generate OASIS Permissions
        /// </summary>
        public AgencyServices GeneratePermissions { get; set; }
    }
}
