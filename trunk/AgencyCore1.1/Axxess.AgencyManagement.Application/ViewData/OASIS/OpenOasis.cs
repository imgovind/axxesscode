﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class OpenOasis
    {
        [UIHint("HiddenMinDate")]
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public string AssessmentName { get; set; }
        public string CurrentlyAssigned { get; set; }
    }
}
