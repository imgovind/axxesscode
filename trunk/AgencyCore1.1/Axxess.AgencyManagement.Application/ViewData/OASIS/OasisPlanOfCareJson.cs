﻿namespace Axxess.AgencyManagement.Application.ViewData {
    using Axxess.Core.Infrastructure;
    using System;
    public class OasisPlanOfCareJson : JsonViewData {
        public Guid episodeId { get; set; }
        public Guid patientId { get; set; }
        public Guid eventId { get; set; }
    }
}