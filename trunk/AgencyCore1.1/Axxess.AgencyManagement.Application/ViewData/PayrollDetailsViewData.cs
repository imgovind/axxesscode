﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.Core.Enums;

   public class PayrollDetailsViewData
    {
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public List<PayrollDetail> Details { get; set; }
       public string PayrollStatus { get; set; }
       public AgencyServices Service { get; set; }
       public bool IsUserCanExport { get; set; }

    }
}
