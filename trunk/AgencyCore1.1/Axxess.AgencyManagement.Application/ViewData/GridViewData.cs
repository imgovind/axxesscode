﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class GridViewData
    {
        /// <summary>
        /// Represent entity id, usually location id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Represent field Id,such as isurance, payor
        /// </summary>
        public int SubId { get; set; }

        /// <summary>
        /// Represent status Id,such as patient, claim
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Represent field diaplay name, usually patient, user, agency 
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// reperesent the current request/response service
        /// </summary>
        public AgencyServices Service { get; set; }
        /// <summary>
        /// reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }
        /// <summary>
        /// Holds the permission of new
        /// </summary>

        public AgencyServices NewPermissions { get; set; }
        /// <summary>
        /// Export Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }

        /// <summary>
        /// View List Permissions
        /// </summary>
        public AgencyServices ViewListPermissions { get; set; }

        /// <summary>
        /// Edit Permissions
        /// </summary>
        public AgencyServices EditPermissions { get; set; }

        /// <summary>
        /// Delete Permissions
        /// </summary>
        public AgencyServices DeletePermissions { get; set; }

       
    }
}
