﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class MissedVisitTaskViewData
    {
        public List<PatientEpisodeEvent> Activities { get; set; }
        public Guid LocationId { get; set; }
        public AgencyServices Service { get; set; }
        public string GroupName { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public bool ShowTime { get; set; }
        //public bool IsApproveOrReturnPermission { get; set; }


        /// <summary>
        /// Reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }

        /// <summary>
        /// Holds the permission for Restore
        /// </summary>
        public bool IsUserCanRestore { get; set; }

        /// <summary>
        /// Holds the permission for Delete
        /// </summary>
        public bool IsUserCanDelete { get; set; }

        /// <summary>
        /// Export Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }

        /// <summary>
        /// View List Permissions
        /// </summary>
        public AgencyServices ViewListPermissions { get; set; }

        public bool IsUserCanSeeStickyNote { get; set; }

        //public string MRN { get; set; }
        //public string Patient { get; set; }
        //public string TaskName { get; set; }
        //public string Url { get; set; }
        //public string Employee { get; set; }
        //public string Status { get; set; }
        //public DateTime ScheduledDate { get; set; }
        //public string ScheduledTime { get; set; }
        //public string StatusComment { get; set; }
        //public string Comments { get; set; }
        //public string EpisodeNotes { get; set; }
        //public string ActionUrl { get; set; }

        //public MissedVisitTaskViewData(ITask task)
        //{
        //    this.MRN = task.PatientIdNumber;
        //    this.Patient = task.PatientName;
        //    this.TaskName = task.DisciplineTaskName;
        //    this.Url = task.Url;
        //    this.Employee = task.UserName;
        //    this.Status = task.StatusName;
        //    this.Comments = task.Comments;
        //    this.StatusComment = task.StatusComment;
        //    this.EpisodeNotes = task.EpisodeNotes;
        //    this.ActionUrl = task.ActionUrl;
        //}

        //public MissedVisitTaskViewData(PrivateDutyScheduleTask task)
        //    : this(task as ITask)
        //{
        //    this.ScheduledDate = task.EventStartTime;
        //    this.ScheduledTime = task.EventDateTimeRange;
        //}

        //public MissedVisitTaskViewData(ScheduleEvent task)
        //    : this(task as ITask)
        //{
        //    this.ScheduledDate = task.EventDate;
        //}
    }
}
