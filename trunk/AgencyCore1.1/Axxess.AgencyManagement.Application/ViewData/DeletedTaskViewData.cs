﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using System.ComponentModel.DataAnnotations;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;


    public  class DeletedTaskViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string TaskName { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string User { get; set; }
        public AgencyServices Service { get; set; }

        public string DateFormatted
        {
            get
            {
                var result = string.Empty;
                if (this.Date.IsValid())
                {
                    switch (this.Service)
                    {
                        case AgencyServices.HomeHealth:
                            result = string.Format("{0:MM/dd/yyyy}",this.Date);
                            break;
                        case AgencyServices.PrivateDuty:
                            result = string.Format("{0:MM/dd/yyyy hh:mm tt}", this.Date);
                            break;
                    }
                }
                return result;
            }
        }
    }

   
}
