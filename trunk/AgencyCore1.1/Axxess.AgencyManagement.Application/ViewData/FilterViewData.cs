﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
   public class FilterViewData
    {
       public int SelecetdInsurance { get; set; }
       public Guid SelecetdBranch { get; set; }
       public AgencyServices Service { get; set; }
       public int PatientStatus { get; set; }
    }
}
