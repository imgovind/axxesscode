﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class UserCalendarViewData
    {
        public List<UserVisit> UserEvents { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set;}
        public AgencyServices Service { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public string GroupName { get; set; }

        public bool IsStickyNote { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanExport { get; set; }
        public bool IsUserCanEdit { get; set; }
    }
}
