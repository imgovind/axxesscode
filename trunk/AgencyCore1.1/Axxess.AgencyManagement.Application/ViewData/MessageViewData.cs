﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Entities;

    public class MessageViewData
    {
        public int CurrentPage { get; set; }
        public int TotalMessage { get; set; }
        public int MessagePerPage { get; set; }
        public int CurrentMessageindex { get; set; }
        public int NumberOfMessagePerPage { get; set; }
        public ICollection<Message> Messages { get; set; }
    }
}