﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using Axxess.AgencyManagement.Entities;

    public class VisitNoteSectionViewData
    {
        public IDictionary<string, NotesQuestion> Questions { get; set; }
        /// <summary>
        /// Whether or not the section contains an NA checkbox to hide all content
        /// </summary>
        public bool IsNA { get; set; }
        public string Type { get; set; }

        public VisitNoteSectionViewData(IDictionary<string, NotesQuestion> questions, bool isNA, string type)
        {
            Questions = questions;
            IsNA = isNA;
            Type = type;
        }
    }
}
