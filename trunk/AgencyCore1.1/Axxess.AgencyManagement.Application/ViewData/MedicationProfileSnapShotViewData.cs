﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using Entities;

    public class MedicationProfileSnapshotViewData
    {
        public LocationPrintProfile LocationProfile { get; set; }
        public PatientProfileLean PatientProfile { get; set; }
        public MedicationProfile MedicationProfile { get; set; }

        public Guid EpisodeId { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public string Allergies { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public string PhysicianName { get; set; }
        public string SignatureText { get; set; }
        public string PrimaryDiagnosis { get; set; }
        public string SecondaryDiagnosis { get; set; }
        public DateTime SignatureDate { get; set; }
    }
}