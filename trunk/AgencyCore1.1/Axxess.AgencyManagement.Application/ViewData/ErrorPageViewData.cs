﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;

    public class ErrorPageViewData
    {
        public string message { get; set; }

        public ErrorPageViewData()
        {
                
        }

        public ErrorPageViewData(string message)
        {
            this.message = message;
        }
    }
}
