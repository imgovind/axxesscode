﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class PatientNameViewData
    {
        public Guid EntityId { get; set; }

        public Guid PatientId { get; set; }

        public string DisplayName { get; set; }

        public AgencyServices Service{ get; set; }

        public AgencyServices AvailableService { get; set; }
    }
}
