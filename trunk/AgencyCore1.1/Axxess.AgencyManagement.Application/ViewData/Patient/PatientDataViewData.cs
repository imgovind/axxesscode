﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities;

    public class PatientDataViewData
    {
        public Guid LocationId { get; set; }
        public AgencyServices Service { get; set; }
        public List<PatientData> Patients { get; set; }

        public string SortDirection { get; set; }
        public string SortColumn { get; set; }

        /// <summary>
        /// Reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }
        /// <summary>
        /// Holds the permission of new
        /// </summary>

        public AgencyServices NewPermissions { get; set; }
        /// <summary>
        /// Export Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }

        /// <summary>
        /// Holds the permission of delete
        /// </summary>

        public AgencyServices DeletePermissions { get; set; }
        /// <summary>
        /// Edit Permissions
        /// </summary>
        public AgencyServices EditPermissions { get; set; }

        /// <summary>
        /// Restore Permissions
        /// </summary>
        public AgencyServices RestorePermissions { get; set; }

        /// <summary>
        /// View List Permissions
        /// </summary>
        public AgencyServices ViewListPermissions { get; set; }
    }
}
