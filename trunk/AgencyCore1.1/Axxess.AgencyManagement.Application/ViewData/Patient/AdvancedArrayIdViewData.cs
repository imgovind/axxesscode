﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using Axxess.Core.Extension;

    public class AdvancedArrayIdViewData
    {
        public string[] ArrayValues { get; set; }
        public string OtherValue { get; set; }
        public string IdPrefix { get; set; }
        public string NamePrefix { get; set; }
        public string CheckGroupWidth { get; set; }
        public bool HasOwnFieldSet { get; set; }

        public AdvancedArrayIdViewData(string arrayValues, string idPrefix, string checkGroupWidth)
            : this(arrayValues, string.Empty, idPrefix, string.Empty, false, checkGroupWidth)
        {
        }

        public AdvancedArrayIdViewData(string arrayValues, string otherValue, string idPrefix, string namePrefix, bool hasOwnFieldSet)
            : this(arrayValues, otherValue, idPrefix, namePrefix, hasOwnFieldSet, string.Empty)
        {
        }

        public AdvancedArrayIdViewData(string arrayValues, string otherValue, string idPrefix, string namePrefix, bool hasOwnFieldSet, string checkGroupWidth)
        {
            this.ArrayValues = !string.IsNullOrEmpty(arrayValues) ? arrayValues.Split(';') : null;
            this.OtherValue = otherValue;
            IdPrefix = idPrefix;
            HasOwnFieldSet = hasOwnFieldSet;
            NamePrefix = namePrefix;
            CheckGroupWidth = checkGroupWidth;
        }

        public bool HasValue(string value)
        {
            return this.ArrayValues.IsNotNullOrEmpty() && this.ArrayValues.Contains(value);
        }
    }
}
