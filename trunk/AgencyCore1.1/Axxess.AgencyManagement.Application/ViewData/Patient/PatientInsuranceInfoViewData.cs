﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PatientInsuranceInfoViewData
    {
        public string ActionType { get; set; }
        public string InsuranceType { get; set; }
        public string HealthPlanId { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public string Relationship { get; set; }

        /// <summary>
        /// Used to identify services
        /// </summary>
        public string IdPrefix { get; set; }
        public string NamePrefix { get; set; }

    }
}
