﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class PatientIdAndEpisodesViewData
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public PatientIdAndEpisodesViewData() {}

        /// <summary>
        /// Constructor for PatientIdAndEpisodesViewData
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="episodeName">Will be used to create the title for the drop down and the label</param>
        public PatientIdAndEpisodesViewData(Guid patientId, string episodeName)
        {
            this.Id = patientId;
            Label = "Associated " + episodeName;
        }
    }
}
