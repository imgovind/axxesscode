﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using Axxess.AgencyManagement.Entities;
    using System.Collections.Generic;


    public class PatientSelectionViewData
    {
        public PatientSelectionViewData()
        {
        }
        public List<PatientSelection> Patients { get; set; }
        public int PatientListStatus { get; set; }
        public int Count { get; set; }
        public Guid CurrentPatientId { get; set; }
        public bool IsDischarged { get; set; }
        public string DisplayName { get; set; }
    }
}
