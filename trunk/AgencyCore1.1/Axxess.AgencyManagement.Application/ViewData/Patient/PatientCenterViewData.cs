﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class PatientChartsViewData
    {
        public PatientChartsViewData()
        {
            SelectionViewData = new PatientSelectionViewData();
            PatientScheduleEvent = new PatientScheduleEventViewData();
        }
        public PatientSelectionViewData SelectionViewData { get; set; }
        public PatientScheduleEventViewData PatientScheduleEvent { get; set; }
        public string ApplicationName { get; set; }
        public AgencyServices Service { get; set; }
    }
}
