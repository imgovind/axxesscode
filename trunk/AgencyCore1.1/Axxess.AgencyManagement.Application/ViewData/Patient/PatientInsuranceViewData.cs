﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class PatientInsuranceViewData
    {
        public Guid PatientId { get; set; }
        public int InsuranceId { get; set; }
        public string Name { get; set; }

        public List<PatientInsurance> Insurances{ get; set; }
    }
}
