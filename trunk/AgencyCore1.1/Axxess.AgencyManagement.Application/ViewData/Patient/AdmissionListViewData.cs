﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    public class AdmissionListViewData
    {
        public Guid PatientId { get; set; }
        public Guid AdmissionId { get; set; }
        public string DisplayName { get; set; }
        public AgencyServices Service { get; set; }
        public AgencyServices PatientsServices { get; set; }
        public string Area
        {
            get
            {
                return Service.ToArea();
            }
        }
        public bool IsPatientActive { get; set; }

        public AdmissionListViewData()
        {
            PatientId = Guid.Empty;
            AdmissionId = Guid.Empty;
            DisplayName = string.Empty;
            Service = AgencyServices.None;
            IsPatientActive = false;
        }
    }
}
