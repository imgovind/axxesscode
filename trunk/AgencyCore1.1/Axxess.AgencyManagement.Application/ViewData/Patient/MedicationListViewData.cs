﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    public class MedicationListViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public List<Medication> List { get; set; }
        public bool IsUserCanAdd { get; set; }
        public bool IsUserCanEdit { get; set; }
        public bool IsUserCanSign { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanReactivate { get; set; }
        public bool IsUserCanDiscontinue { get; set; }
        public bool IsUserCanViewList { get; set; }
    }
}
