﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Application
{
    using Axxess.Core.Enums;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WoundViewData<T>
    {
        public Guid EventId { get; set; }
        public Guid PatientId { get; set; }
        public WoundPosition Position { get; set; }
        public string Type { get; set; }
        public Dictionary<string, T> Data { get; set; }

    }
}
