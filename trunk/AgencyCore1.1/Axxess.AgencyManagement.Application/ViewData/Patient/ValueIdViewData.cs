﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    public class ValueIdViewData<T>
    {
        public T Value { get; set; }
        public string IdPrefix { get; set; }

        public ValueIdViewData(T value, string idPrefix)
        {
            Value = value;
            IdPrefix = idPrefix;
        }
    }
}