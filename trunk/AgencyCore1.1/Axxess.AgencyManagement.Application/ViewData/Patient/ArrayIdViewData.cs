﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using Axxess.Core.Extension;

    public class ArrayIdViewData
    {
        public string[] Values { get; set; }
        public string OtherValue { get; set; }
        public string IdPrefix { get; set; }

        public ArrayIdViewData(string values, string idPrefix) 
            : this(values, null, idPrefix)
        {
            
        }

        public ArrayIdViewData(string values, string otherValue, string idPrefix)
        {
            this.Values = !string.IsNullOrEmpty(values) ? values.Split(';') : null;
            OtherValue = otherValue;
            IdPrefix = idPrefix;
        }

        public bool HasValue(string value)
        {
            return this.Values.IsNotNullOrEmpty() && this.Values.Contains(value);
        }
    }
}