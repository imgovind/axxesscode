﻿
namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class RemittanceViewData
    {
        public Guid Id { get; set; }
        public AgencyServices Service { get; set; }
        public IList<RemittanceLean> List { get; set; }

        /// <summary>
        /// Reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }

        /// <summary>
        /// Print Permissions
        /// </summary>
        public AgencyServices PrintPermissions { get; set; }

        /// <summary>
        /// Export  Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }

        /// <summary>
        /// Upload  Permissions
        /// </summary>
        public AgencyServices UploadPermissions { get; set; }

        /// <summary>
        /// Upload  Permissions
        /// </summary>
        public AgencyServices DeletePermissions { get; set; }

        /// <summary>
        /// View List  Permissions
        /// </summary>
        public AgencyServices ViewListPermissions { get; set; }

        /// <summary>
        /// View Detail  Permissions
        /// </summary>
        public AgencyServices ViewDetailPermissions { get; set; }

    }
}
