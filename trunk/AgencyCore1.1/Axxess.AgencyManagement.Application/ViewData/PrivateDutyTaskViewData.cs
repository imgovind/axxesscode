﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Axxess.AgencyManagement.Entities;
//using Axxess.Core.Extension;

//namespace Axxess.AgencyManagement.Application.ViewData
//{
//    using Axxess.AgencyManagement.Entities.Enums;

//    public class TaskViewData
//    {
//        public Guid Id { get; set; }
//        public Guid PatientId { get; set; }
//        public Guid UserId { get; set; }
//        public string PatientName { get; set; }
//        public string UserName { get; set; }
//        public string DisciplineTaskName { get; set; }
//        public int Status { get; set; }
//        public DateTime EventStartTime { get; set; }
//        public DateTime EventEndTime { get; set; }
//        //public string PrintUrl { get; set; }
//        //public string Url { get; set; }
//        //public bool IsMissedVisit { get; set; }
//        public string DisciplineTask { get; set; }
//        public string DocumentType { get; set; }
//        public bool IsAllDay { get; set; }
//        public bool IsUserCanDelete { get; set; }
//        public bool IsUserCanPrint { get; set; }
//        public bool IsUserCanEdit { get; set; }
//        public bool IsUserCanReopen { get; set; }
//        public bool IsUserCanEditDetail { get; set; }
       

//        public TaskViewData(PrivateDutyScheduleTask task)
//        {
//            this.Id = task.Id;
//            this.PatientName = task.PatientName;
//            this.PatientId = task.PatientId;
//            this.UserName = task.UserName;
//            this.DisciplineTaskName = task.DisciplineTaskName;
//            this.Status = task.Status;
//            this.EventStartTime = task.EventStartTime;
//            this.EventEndTime = task.EventEndTime;
//            //this.IsMissedVisit = ScheduleStatusFactory.MissedVisitStatus().Contains(task.Status);
//            //this.IsMissedVisitReady = this.EventStartTime.IsValid() && this.EventStartTime.Date <= DateTime.Now.Date;
//            this.IsAllDay = task.IsAllDay;
//            this.IsUserCanDelete = task.IsUserCanDelete;
//            this.IsUserCanEdit = task.IsUserCanEdit;
//            this.IsUserCanPrint = task.IsUserCanPrint;
//            this.IsUserCanReopen = task.IsUserCanReopen;
//            this.IsUserCanEditDetail = task.IsUserCanEditDetail;
//            this.DisciplineTask = task.DisciplineTask.ToEnum(DisciplineTasks.NoDiscipline).ToString();
//            this.DocumentType = UrlFactory<PrivateDutyScheduleTask>.GetType(task);
//        }
//    }
//}
