﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class AttachmentViewData
    {
        public string PatientName { get; set; }
        public string DisciplineTaskName { get; set; }
        public List<Guid> Assets { get; set; }
    }
}
