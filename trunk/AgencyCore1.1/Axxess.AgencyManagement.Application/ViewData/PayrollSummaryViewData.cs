﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Application.Domain;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
   public class PayrollSummaryViewData
    {
       public List<VisitSummary> VisitSummary { get; set; }
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public string PayrollStatus { get; set; }
       public AgencyServices Service { get; set; }
       public bool IsUserCanExport { get; set; }
    }
}
