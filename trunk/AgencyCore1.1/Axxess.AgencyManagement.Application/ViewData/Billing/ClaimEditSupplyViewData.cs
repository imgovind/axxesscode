﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.LookUp.Domain;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application
{
    public class ClaimEditSupplyViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Supply Supply { get; set; }
        public string Type { get; set; }
        public AgencyServices Service { get; set; }

        public ClaimEditSupplyViewData()
        {

        }

        public ClaimEditSupplyViewData(Guid id, Guid patientId, string type, Supply supply,AgencyServices service)
        {
            Id = id;
            PatientId = patientId;
            Type = type;
            Supply = supply;
            Service = service;
        }
    }
}
