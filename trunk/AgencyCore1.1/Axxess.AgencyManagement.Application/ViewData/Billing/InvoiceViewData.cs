﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class InvoiceViewData : BaseInvoiceFormViewData
    {
        public int ClaimNumber { get; set; }
        public bool IsForPatient { get; set; }

      
    }
}
