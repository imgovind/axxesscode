﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application
{
    public class ClaimNewSupplyViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string Type { get; set; }
        public AgencyServices Service { get; set; }

        public ClaimNewSupplyViewData()
        {

        }

        public ClaimNewSupplyViewData(Guid id, Guid patientId, string type, AgencyServices service)
        {
            Id = id;
            PatientId = patientId;
            Type = type;
            Service = service;
        }
    }
}
