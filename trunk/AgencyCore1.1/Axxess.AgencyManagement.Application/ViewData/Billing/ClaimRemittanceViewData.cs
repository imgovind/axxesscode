﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Application
{
    public class ClaimRemittanceViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public DateTime RemitDate { get; set; }
        public string RemitId { get; set; }
        public double TotalAdjustmentAmount { get; set; }

        public bool IsInfoVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsRemittanceVerified { get; set; }
        public bool IsSupplyVerified { get; set; }

        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillVisitDatas { get; set; }
    }
}
