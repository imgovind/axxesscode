﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Entities;

    public class BillingHistoryViewData
    {
        public int Count { get; set; }
        public ClaimInfoSnapShotViewData ClaimInfo { get; set; }
    }
}
