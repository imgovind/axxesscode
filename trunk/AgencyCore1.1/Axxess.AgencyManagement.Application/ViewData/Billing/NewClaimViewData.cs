﻿
namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

   public class NewClaimViewData
    {
       public List<ClaimEpisode> EpisodeData { get; set; }
       public int Type { get; set; }
       public Guid PatientId { get; set; }

       public string TypeName
       {
           get
           {
               if (Enum.IsDefined(typeof(ClaimTypeSubCategory), this.Type)) { return ((ClaimTypeSubCategory) this.Type).GetDescription(); } else { return string.Empty; };
           }
       }
    }
}
