﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class EditBillDataViewData
    {
        public Guid ClaimId { get; set; }
        public ChargeRate ChargeRate { get; set; }
        public ClaimTypeSubCategory TypeOfClaim { get; set; }
        public AgencyServices Service { get; set; }
        public int PayorType { get; set; }
        public bool IsTraditionalMedicare { get; set; }
    }
}
