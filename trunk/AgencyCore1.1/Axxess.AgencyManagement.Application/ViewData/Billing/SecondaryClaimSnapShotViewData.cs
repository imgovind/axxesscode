﻿namespace Axxess.AgencyManagement.Application
{
    using Axxess.Core.Extension;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Application.ViewData;

    public class SecondaryClaimSnapShotViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string PayorName { get; set; }
        public string HHRG { get; set; }
        public string HIPPS { get; set; }
        public string ClaimKey { get; set; }

        public string HealthPlainId { get; set; }
        public string AuthorizationNumber { get; set; }


        public double StandardEpisodeRate { get; set; }
        public double SupplyReimbursement { get; set; }
        public double ProspectivePay { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }

        public double ClaimAmount { get; set; }
        public double PaymentAmount { get; set; }


        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToString("MM/dd/yyyy") : string.Empty;
            }
        }
        public DateTime ClaimDate { get; set; }
        public string ClaimDateFormatted
        {
            get
            {
                return ClaimDate != DateTime.MinValue ? ClaimDate.ToString("MM/dd/yyyy") : string.Empty;
            }
        }
    }
}
