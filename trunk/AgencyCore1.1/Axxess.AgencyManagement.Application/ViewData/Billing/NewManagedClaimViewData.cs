﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using Axxess.Core.Enums;

    public class NewManagedClaimViewData
    {
       // public List<SelectListItem> Insurances { get; set; }
        public string SelecetdInsurance { get; set; }
        public Guid PatientId { get; set; }
        public AgencyServices Service { get; set; }
    }
}
