﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class BaseInvoiceFormViewData
    {
        public Agency Agency { get; set; }
        public AgencyLocation AgencyLocation { get; set; }
        public ClaimViewData Claim { get; set; }
        public List<BillSchedule> BillSchedules { get; set; }
        public string PayorName { get; set; }
        public string PayorAddressLine1 { get; set; }
        public string PayorAddressLine2 { get; set; }

        public InvoiceType InvoiceType { get; set; }
        public ClaimTypeSubCategory ClaimTypeSubCategory { get; set; }
    }
}
