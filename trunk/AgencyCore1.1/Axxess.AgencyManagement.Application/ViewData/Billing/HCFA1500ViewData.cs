﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class HCFA1500ViewData : BaseInvoiceFormViewData
    {
        public string PatientMaritalStatus { get; set; }
        public string PatientTelephoneNum { get; set; }

       
    }
}
