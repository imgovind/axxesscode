﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;

    public class BillingJsonViewData : BaseJsonViewData
    {
        public bool IsMedicareHistoryRefresh { get; set; }
        public bool IsRAPRefresh { get; set; }
        public bool IsPedingRAPRefresh { get; set; }
        public bool IsFinalRefresh { get; set; }
        public bool IsPedingFinalRefresh { get; set; }
        public bool IsManagedCareHistoryRefresh { get; set; }
        public bool IsManagedCareRefresh { get; set; }
        public bool IsSecondaryHistoryRefresh { get; set; }
        public bool IsSecondaryRefresh { get; set; }
        public ClaimTypeSubCategory Category { get; set; }
    }
}
