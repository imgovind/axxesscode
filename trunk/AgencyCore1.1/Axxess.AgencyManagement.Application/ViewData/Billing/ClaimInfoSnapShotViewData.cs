﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using Axxess.LookUp.Domain;

    public class ClaimInfoSnapShotViewData
    {
        public Guid Id { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string PayorName { get; set; }
        public string HIPPS { get; set; }
        public string ClaimKey { get; set; }
        public ProspectivePayment ProspectivePayment { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }
        public int Status { get; set; }
        public bool IsCreateSecondaryClaimButtonVisible { get; set; }

        public bool IsUserCanAdd { get; set; }
        public bool IsUserCanViewRemittance { get; set; }
        public bool IsUserCanViewList { get; set; }
        public bool IsUserCanViewLog { get; set; }
    }
}
