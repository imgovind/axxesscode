﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.LookUp.Domain;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    public class ClaimSupplyViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsSupplyNotBillable { get; set; }
        public List<Supply> BilledSupplies { get; set; }
        public List<Supply> UnbilledSupplies { get; set; }
        public AgencyServices Service { get; set; }
        public ClaimTypeSubCategory Type { get; set; }

        public bool IsInfoVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsRemittanceVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
    }
}
