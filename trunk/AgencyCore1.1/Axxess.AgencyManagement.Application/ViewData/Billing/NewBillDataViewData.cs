﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    public class NewBillDataViewData
    {
        public Guid ClaimId { get; set; }
        public Guid PatientId { get; set; }
        public AgencyServices Service { get; set; }
        public bool IsMedicareHMO { get; set; }
        public bool IsTraditionalMedicare { get; set; }
        public ClaimTypeSubCategory TypeOfClaim { get; set; }
        /// <summary>
        /// Used to identify self pay from other type of payment
        /// </summary>
        public int PayorType { get; set; }
        public int InsuranceId { get; set; }
        /// <summary>
        /// Used to capture the payor's already existing task
        /// </summary>
        public List<int> ExistingNotes { get; set; }

    }
}
