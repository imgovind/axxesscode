﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class UserListViewData<T>
    {
        public IList<T> Users { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public int Status { get; set; }

        /// Holds the permission for Exports
        public AgencyServices ExportPermissions { get; set; }


        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanEdit { get; set; }

        /// <summary>
        /// Holds the permission for Activate, and if the user has the proper status to be activated.
        /// </summary>
        public bool IsUserCanActivate { get; set; }

        /// <summary>
        /// Holds the permission for Deactivate, and if the user has the proper status to be deactivated.
        /// </summary>
        public bool IsUserCanDeactivate { get; set; }

        /// <summary>
        /// Holds the permission for Exports
        /// </summary>
        public bool IsUserCanExport { get; set; }

        /// <summary>
        /// Holds the permission for Add
        /// </summary>
        public bool IsUserCanAdd { get; set; }


        public bool IsUserCanSeeStickyNote { get; set; }
    }
}
