﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.ViewData
{
    using Axxess.AgencyManagement.Entities;

    public class UserPayRatesViewData
    {
        public Guid UserId { get; set; }
        public List<UserPayorRates> PayorRates { get; set; }
    }
}
