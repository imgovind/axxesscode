﻿namespace Axxess.AgencyManagement.Application.ViewData
{
   public class InsuranceViewData
    {
       public int Id { get; set; }
       public string Name { get; set; }
    }
}
