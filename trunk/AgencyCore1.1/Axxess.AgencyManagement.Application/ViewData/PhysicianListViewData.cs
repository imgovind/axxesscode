﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;    
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;

    public class PhysicianListViewData
    {
        public IList<AgencyPhysicianGridRow> List { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public bool IsUserCanExport { get; set; }
        public bool IsUserCanAdd { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanEdit { get; set; }
    }
}
