﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

    public class PatientScheduleEventViewData : TaskActivityViewData
    {
        public PatientScheduleEventViewData()
        {
            //this.ScheduleEvents = new List<ScheduleEvent>();
        }
        public Patient Patient { get; set; }
        public string DateFilterType { get; set; }
        public string DisciplineFilterType { get; set; }
        public string Range
        {
            get
            {
                if (EndDate.Date > DateTime.MinValue && StartDate.Date > DateTime.MinValue)
                {
                    return string.Format("{0}-{1}",StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"));
                }
                return "No Episodes Found";
            }
        }

    }
}
