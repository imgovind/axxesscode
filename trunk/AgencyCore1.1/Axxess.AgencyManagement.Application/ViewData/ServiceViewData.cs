﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class ServiceViewData
    {
        public AgencyServices SelectedService { get; set; }
        public AgencyServices LimiterService { get; set; }

        public ServiceViewData(AgencyServices limiterService)
        {
            this.SelectedService = Current.PreferredService;
            this.LimiterService = limiterService;
        }

        public ServiceViewData(AgencyServices selectedService, AgencyServices limiterService)
        {
            this.SelectedService = selectedService;
            this.LimiterService = limiterService;
        }
    }
}
