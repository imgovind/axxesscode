﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Extension;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class PrivateDutySchedulerViewData
    {
        public List<GridTask> Tasks;
        public List<PrivateDutyCarePeriod> CarePeriods;
        public bool IsStickyNote { get; set; }
        public bool IsUserCanPrint { get; set; }
        public bool IsUserCanDelete { get; set; }
        public bool IsUserCanReassign { get; set; }
        public bool IsUserCanReopen { get; set; }
        public bool IsUserCanEditDetail { get; set; }
        public bool IsUserCanRestore { get; set; }
        public bool IsUserCanEdit { get; set; }

        public PrivateDutySchedulerViewData(List<GridTask> tasks, List<PrivateDutyCarePeriod> careperiods)
        {
            this.Tasks = tasks;
            this.CarePeriods = careperiods;
        }
    }
}
