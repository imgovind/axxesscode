﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public class OrderGridViewData : GridViewData
    {
        /// <summary>
        ///  Physicain  Permissions
        /// </summary>
        public AgencyServices SentPermissions { get; set; }
        public AgencyServices ReceivePermissions { get; set; }
    }
}
