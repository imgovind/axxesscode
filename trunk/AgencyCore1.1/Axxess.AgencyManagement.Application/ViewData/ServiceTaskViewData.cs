﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;
    public class ServiceTaskViewData
    {
        public Guid LocationId { get; set; }
        public AgencyServices Service { get; set; }
        public string GroupName { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
        public List<TaskLean> Tasks { get; set; }

        /// <summary>
        /// Reperesent the available services 
        /// </summary>
        public AgencyServices AvailableService { get; set; }

        /// <summary>
        /// Print Permissions
        /// </summary>
        public AgencyServices PrintPermissions { get; set; }

        /// <summary>
        /// Export  Permissions
        /// </summary>
        public AgencyServices ExportPermissions { get; set; }
        /// <summary>
        /// Edit Detail  Permissions
        /// </summary>
        public AgencyServices EditDetailPermissions { get; set; }

    }
}
