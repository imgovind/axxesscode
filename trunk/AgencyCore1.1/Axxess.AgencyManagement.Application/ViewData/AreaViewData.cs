﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.ViewData
{
    public class AreaViewData
    {
        public string Prefix { get; set; }
        public string Area { get; set; }
        public Guid BranchId { get; set; }
        public AgencyServices Service { get; set; }

        public AreaViewData()
        {
            this.Prefix = "";
            this.Area = "";
        }

        public AreaViewData(string prefix)
        {
            this.Prefix = prefix;
            this.Area = "";
        }

        public AreaViewData(string prefix, string area)
        {
            this.Prefix = prefix;
            this.Area = area;
        }
    }
}
