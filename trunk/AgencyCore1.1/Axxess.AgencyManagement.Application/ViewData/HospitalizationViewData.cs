﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
using System;

    public class  HospitalizationViewData
    {
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public List<HospitalizationLog> Logs { get; set; }

        public bool IsUserCanAdd { get; set; }
        public bool IsUserCanExport { get; set; }
    }
}