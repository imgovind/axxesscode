﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using Axxess.LookUp.Domain;
using Axxess.Core.Enums;


    public class ReportViewData
    {
        public AgencyServices AvailableService { get; set; }
        public AgencyServices Service { get; set; }
        public Dictionary<int, List<ReportDescription>> Reports { get; set; }
    }
}
