﻿namespace Axxess.AgencyManagement.Application.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;

   public class QAViewData
    {
       public List<PatientEpisodeEvent> Activities { get; set; }
       public Guid LocationId{ get; set; }
       public AgencyServices Service { get; set; }
       public string GroupName{ get; set; }
       public string SortColumn{ get; set; }
       public string SortDirection{ get; set; }
       //public bool IsApproveOrReturnPermission { get; set; }


       /// <summary>
       /// Reperesent the available services 
       /// </summary>
       public AgencyServices AvailableService { get; set; }
       /// <summary>
       /// Holds the permission for Approve
       /// </summary>

       public AgencyServices ApprovePermissions { get; set; }
       /// <summary>
       /// Export Permissions
       /// </summary>
       public AgencyServices ExportPermissions { get; set; }

       /// <summary>
       /// Holds the permission for Return
       /// </summary>

       public AgencyServices ReturnPermissions { get; set; }
       

       /// <summary>
       /// View List Permissions
       /// </summary>
       public AgencyServices ViewListPermissions { get; set; }

       public bool IsUserCanSeeStickyNote { get; set; }
       public bool IsUserCanPrint { get; set; }
       public bool IsUserCanEditApproved { get; set; }
    }
}
