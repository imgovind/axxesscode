﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ReportModule : Module
    {
        public override string Name
        {
            get { return "Report"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Patient
            //routes.MapRoute(
            //    "ReportPatientRoster",
            //    "Report/Patient/Roster",
            //    new { controller = this.Name, action = "PatientRoster", id = UrlParameter.Optional });

            //routes.MapRoute(
            //      "ReportCAHPSReport",
            //      "Report/Patient/Cahps",
            //      new { controller = this.Name, action = "Cahps", id = UrlParameter.Optional });

            //routes.MapRoute(
            //   "ReportPatientEmergencyList",
            //   "Report/Patient/EmergencyList",
            //   new { controller = this.Name, action = "PatientEmergencyList", id = UrlParameter.Optional });

            //routes.MapRoute(
            //   "ReportPatientBirthdayList",
            //   "Report/Patient/Birthdays",
            //   new { controller = this.Name, action = "PatientBirthdayList", id = UrlParameter.Optional });

            //routes.MapRoute(
            //   "ReportPatientAddressList",
            //   "Report/Patient/AddressList",
            //   new { controller = this.Name, action = "PatientAddressList", id = UrlParameter.Optional });

            //routes.MapRoute(
            //   "ReportPatientPhysicians",
            //   "Report/Patient/Physician",
            //   new { controller = this.Name, action = "PatientByPhysicians", id = UrlParameter.Optional });

            //routes.MapRoute(
            // "ReportPatientSocCertPeriod",
            // "Report/Patient/SocCertPeriod",
            // new { controller = this.Name, action = "PatientSocCertPeriodListing", id = UrlParameter.Optional });

            //routes.MapRoute(
            // "PatientByResponsibleEmployee",
            // "Report/Patient/ResponsibleEmployee",
            // new { controller = this.Name, action = "PatientByResponsibleEmployeeListing", id = UrlParameter.Optional });

            //routes.MapRoute(
            //"PatientByResponsibleCaseManager",
            //"Report/Patient/ResponsibleCaseManager",
            //new { controller = this.Name, action = "PatientByResponsibleCaseManagerListing", id = UrlParameter.Optional });


            //routes.MapRoute(
            //"ReportStatisticalCensusByPrimaryInsurance",
            //"Report/Statistical/CensusByPrimaryInsurance",
            //new { controller = this.Name, action = "StatisticalCensusByPrimaryInsurance", id = UrlParameter.Optional });

            //routes.MapRoute(
            //   "ReportPatientExpiringAuthorizations",
            //   "Report/Patient/ExpiringAuthorizations",
            //   new { controller = this.Name, action = "PatientExpiringAuthorizations", id = UrlParameter.Optional });

            //routes.MapRoute(
            //  "ReportPatientSurveyCensus",
            //  "Report/Patient/SurveyCensus",
            //  new { controller = this.Name, action = "PatientSurveyCensus", id = UrlParameter.Optional });
          

            //routes.MapRoute(
            //"ReportPatientSixtyDaySummary",
            //"Report/Patient/SixtyDaySummary",
            //new { controller = this.Name, action = "PatientSixtyDaySummary", id = UrlParameter.Optional });

            //routes.MapRoute(
            //"ReportPatientDischargePatients",
            //"Report/Patient/DischargePatients",
            //new { controller = this.Name, action = "DischargePatients", id = UrlParameter.Optional });

            routes.MapRoute(
          "ReportPatientVitalSigns",
          "Report/Patient/VitalSigns",
          new { controller = this.Name, action = "PatientVitalSigns", id = UrlParameter.Optional });

            //routes.MapRoute(
            //"VitalSigns",
            //"Report/VitalSigns/{PatientId}/{StartDate}/{EndDate}",
            //new { controller = this.Name, action = "VitalSigns", PatientId = new Guid(), StartDate = new DateTime(), EndDate = new DateTime() });
            
            #endregion

            routes.MapRoute(
                "HomeHealthReportDefault",
                "HomeHealth/Report/{action}",
                new { controller = this.Name, action = "Error" },
             new string[] { "Axxess.AgencyManagement.Application.Controllers"});


        }
    }
}
