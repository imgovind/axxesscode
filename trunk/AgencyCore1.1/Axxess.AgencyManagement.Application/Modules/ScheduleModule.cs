﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;

    public class ScheduleModule : Module
    {
        public override string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapRoute(
            //    "MissedVisit",
            //    "Visit/Miss",
            //    new { controller = this.Name, action = "MissedVisit", id = UrlParameter.Optional },
            //    null,
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //    "MissedVisitPrint",
            //    "MissedVisit/View/{patientId}/{eventId}",
            //    new { controller = this.Name, action = "MissedVisitPrint", patientId = new IsGuid(), eventId = new IsGuid() },
            //    null,
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //   "MissedVisitsList",
            //   "MissedVisits/List",
            //   new { controller = this.Name, action = "MissedVisitsList", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //   "MissedVisitsContent",
            //   "MissedVisits/Content",
            //   new { controller = this.Name, action = "MissedVisitsContent", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });


            routes.MapRoute("ScheduleCenter", "Schedule/Center", new { controller = this.Name, action = "Center", status = (int)PatientStatus.Active, patientId = Guid.Empty }, null, new string[] { "Axxess.AgencyManagement.Application.Controllers" });
        }
    }
}
