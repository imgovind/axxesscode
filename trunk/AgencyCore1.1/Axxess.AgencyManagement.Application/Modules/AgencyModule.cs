﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AgencyModule : Module
    {
        public override string Name
        {
            get { return "Agency"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "NewContact",
               "Contact/New",
               new { controller = this.Name, action = "NewContact", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddContact",
               "Contact/Add",
               new { controller = this.Name, action = "AddContact", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "EditContact",
              "Contact/Edit",
              new { controller = this.Name, action = "EditContact", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "UpdateContact",
              "Contact/Update",
              new { controller = this.Name, action = "UpdateContact", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteContact",
              "Contact/Delete",
              new { controller = this.Name, action = "DeleteContact", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "ContactGrid",
               "Contact/Grid",
               new { controller = this.Name, action = "Contacts", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "ContactList",
               "Contact/List",
               new { controller = this.Name, action = "ContactList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "NewHospital",
               "Hospital/New",
               new { controller = this.Name, action = "NewHospital", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddHospital",
               "Hospital/Add",
               new { controller = this.Name, action = "AddHospital", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "EditHospital",
              "Hospital/Edit",
              new { controller = this.Name, action = "EditHospital", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "UpdateHospital",
              "Hospital/Update",
              new { controller = this.Name, action = "UpdateHospital", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteHospital",
              "Hospital/Delete",
              new { controller = this.Name, action = "DeleteHospital", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "HospitalGrid",
               "Hospital/Grid",
               new { controller = this.Name, action = "Hospitals", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "HospitalList",
               "Hospital/List",
               new { controller = this.Name, action = "HospitalList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "NewLocation",
               "Location/New",
               new { controller = this.Name, action = "NewLocation", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddLocation",
               "Location/Add",
               new { controller = this.Name, action = "AddLocation", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "EditLocation",
              "Location/Edit",
              new { controller = this.Name, action = "EditLocation", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "UpdateLocation",
              "Location/Update",
              new { controller = this.Name, action = "UpdateLocation", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteLocation",
              "Location/Delete",
              new { controller = this.Name, action = "DeleteLocation", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "LocationGrid",
               "Location/Grid",
               new { controller = this.Name, action = "Locations", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "LocationList",
               "Location/List",
               new { controller = this.Name, action = "LocationList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "NewInsurance",
               "Insurance/New",
               new { controller = this.Name, action = "NewInsurance", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddInsurance",
               "Insurance/Add",
               new { controller = this.Name, action = "AddInsurance", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
             "EditInsurance",
             "Insurance/Edit",
             new { controller = this.Name, action = "EditInsurance", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
             "UpdateInsurance",
             "Insurance/Update",
             new { controller = this.Name, action = "UpdateInsurance", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteInsurance",
              "Insurance/Delete",
              new { controller = this.Name, action = "DeleteInsurance", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "InsuranceGrid",
               "Insurance/Grid",
               new { controller = this.Name, action = "Insurances", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "InsuranceList",
               "Insurance/List",
               new { controller = this.Name, action = "InsuranceList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //   "NewPhysician",
            //   "Physician/New",
            //   new { controller = this.Name, action = "NewPhysician", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //   "AddPhysician",
            //   "Physician/Add",
            //   new { controller = this.Name, action = "AddPhysician", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            // "EditPhysician",
            // "Physician/Edit",
            // new { controller = this.Name, action = "EditPhysician", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            // "UpdatePhysician",
            // "Physician/Update",
            // new { controller = this.Name, action = "UpdatePhysician", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //  "DeletePhysician",
            //  "Physician/Delete",
            //  new { controller = this.Name, action = "DeletePhysician", id = new IsGuid() },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //   "PhysicianGrid",
            //   "Physician/Grid",
            //   new { controller = this.Name, action = "Physicians", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //  "PhysicianList",
            //  "Physician/List",
            //  new { controller = this.Name, action = "PhysicianList", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "NewTemplate",
               "Template/New",
               new { controller = this.Name, action = "NewTemplate", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddTemplate",
               "Template/Add",
               new { controller = this.Name, action = "AddTemplate", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "EditTemplate",
              "Template/Edit",
              new { controller = this.Name, action = "EditTemplate", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "UpdateTemplate",
              "Template/Update",
              new { controller = this.Name, action = "UpdateTemplate", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteTemplate",
              "Template/Delete",
              new { controller = this.Name, action = "DeleteTemplate", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
             "GetTemplate",
             "Template/Get",
             new { controller = this.Name, action = "GetTemplate", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "TemplateGrid",
               "Template/Grid",
               new { controller = this.Name, action = "Templates", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "TemplateList",
               "Template/List",
               new { controller = this.Name, action = "TemplateList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "NewAgencySupply",
               "Supply/New",
               new { controller = this.Name, action = "NewSupply", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddAgencySupply",
               "Supply/Add",
               new { controller = this.Name, action = "AddSupply", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "EditAgencySupply",
              "Supply/Edit",
              new { controller = this.Name, action = "EditSupply", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "UpdateAgencySupply",
              "Supply/Update",
              new { controller = this.Name, action = "UpdateSupply", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteAgencySupply",
              "Supply/Delete",
              new { controller = this.Name, action = "DeleteSupply", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
             "GetAgencySupply",
             "Supply/Get",
             new { controller = this.Name, action = "GetSupply", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "SupplyGrid",
               "Supply/Grid",
               new { controller = this.Name, action = "Supplies", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "SupplyList",
               "Supply/List",
               new { controller = this.Name, action = "SupplyList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            //routes.MapRoute(
            //   "MedicareEligibilityContent",
            //   "MedicareEligibility/Content",
            //   new { controller = this.Name, action = "MedicareEligibilityContent", id = UrlParameter.Optional },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            // routes.MapRoute(
            //   "MedicareEligibilityPrint",
            //   "MedicareEligibility/Print/{Id},",
            //   new { controller = this.Name, action = "MedicareEligibilityPrint", id = new IsGuid() },
            //new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "NewAdjustmentCode",
               "AdjustmentCode/New",
               new { controller = this.Name, action = "NewAdjustmentCode", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AddAdjustmentCode",
               "AdjustmentCode/Add",
               new { controller = this.Name, action = "AddAdjustmentCode", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "EditAdjustmentCode",
              "AdjustmentCode/Edit",
              new { controller = this.Name, action = "EditAdjustmentCode", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "UpdateAdjustmentCode",
              "AdjustmentCode/Update",
              new { controller = this.Name, action = "UpdateAdjustmentCode", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
              "DeleteAdjustmentCode",
              "AdjustmentCode/Delete",
              new { controller = this.Name, action = "DeleteAdjustmentCode", id = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AdjustmentCodeGrid",
               "AdjustmentCode/Grid",
               new { controller = this.Name, action = "AdjustmentCodes", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
               "AdjustmentCodeList",
               "AdjustmentCode/List",
               new { controller = this.Name, action = "AdjustmentCodeList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "NewUploadType",
                "UploadType/New",
                new { controller = this.Name, action = "NewUploadType", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "AddUploadType",
                "UploadType/Add",
                new { controller = this.Name, action = "AddUploadType", id = UrlParameter.Optional },
             new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "EditUploadType",
                "UploadType/Edit",
                new { controller = this.Name, action = "EditUploadType", id = UrlParameter.Optional },
             new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "UpdatedUploadType",
                "UploadType/Update",
                new { controller = this.Name, action = "UpdateUploadType", id = UrlParameter.Optional },
             new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "DeleteUploadType",
                "UploadType/Delete",
                new { controller = this.Name, action = "DeleteUploadType", id = UrlParameter.Optional },
             new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "UploadTypeList",
                "UploadType/List",
                new { controller = this.Name, action = "UploadTypes", id = UrlParameter.Optional },
             new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "UploadTypeGrid",
                "UploadType/Grid",
                new { controller = this.Name, action = "UploadTypeGrid", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Controllers" });
        }
    }
}
