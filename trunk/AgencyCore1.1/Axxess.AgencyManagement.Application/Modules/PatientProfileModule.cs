﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Infrastructure;
using System.Web.Mvc;
using System.Web.Routing;

namespace Axxess.AgencyManagement.Application.Modules
{
   public class PatientProfileModule : Module
    {

        public override string Name
        {
            get { return "PatientProfile"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapRoute(
            //    "NewAuthorization",
            //    "Authorization/New",
            //    new { controller = this.Name, action = "NewAuthorization", id = UrlParameter.Optional },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            routes.MapRoute(
                "MedicationProfilePrint",
                "MedicationProfile/Print/{patientId}",
                new { controller = this.Name, action = "MedicationProfilePrint", patientId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute(
                "MedicationProfile",
                "MedicationProfile/{patientId}",
                new { controller = this.Name, action = "MedicationProfile", patientId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute("HomeHealthProfileDefault",
                         "HomeHealth/PatientProfile/{action}",
                         new { controller = this.Name, action = "" },
                      new string[] { "Axxess.AgencyManagement.Application.Controllers" });

            routes.MapRoute(
                "MedicareEligibilityReportPrint",
                "PatientProfile/MedicareEligibilityReportPrint/{patientId}/{eventId}",
                new { controller = this.Name, action = "MedicareEligibilityReportPrint", patientId = new IsGuid(), eventId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
        }
    }
}
