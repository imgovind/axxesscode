﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class NoteModule : Module
    {
        public override string Name
        {
            get { return "Note"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("NoteDefault", "Note/{action}/{patientId}/{eventId}", new { controller = this.Name, action = "Index", patientId = new IsGuid(), eventId = new IsGuid() }, null, new string[] { "Axxess.AgencyManagement.Application.Controllers" });
        }
    }
}
