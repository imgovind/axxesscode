﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;

    public class PatientModule : Module
    {
        public override string Name
        {
            get { return "Patient"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "NewPatient",
                "Patient/New/{referralId}",
                new { controller = this.Name, action = "New", referralId = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute(
               "PatientList",
               "Patient/Grid/{ServiceId}",
               new { controller = this.Name, action = "Grid", ServiceId = 0 },
               new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute(
                "PatientTriageClassification",
                "Patient/TriageClassification/{id}",
                new { controller = this.Name, action = "TriageClassification", id = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute(
                "PatientPhoto",
                "Patient/NewPhoto/{patientId}",
                new { controller = this.Name, action = "NewPhoto", patientId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute(
                "NewPatientDocument",
                "Patient/NewDocument/{patientId}",
                new { controller = this.Name, action = "NewDocument", patientId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            routes.MapRoute(
                "DeletePatientDocument",
                "Patient/Document/Delete/{patientId}/{documentId}",
                new { controller = this.Name, action = "DeleteDocument", patientId = new IsGuid(), documentId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
            //routes.MapRoute(
            //    "EditAuthorization",
            //    "Authorization/Edit",
            //    new { controller = this.Name, action = "EditAuthorization", id = UrlParameter.Optional },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            //routes.MapRoute(
            //    "UpdateAuthorization",
            //    "Authorization/Update",
            //    new { controller = this.Name, action = "UpdateAuthorization", id = UrlParameter.Optional },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            //routes.MapRoute(
            //    "AddAuthorization",
            //    "Authorization/Add",
            //    new { controller = this.Name, action = "AddAuthorization", id = UrlParameter.Optional },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            //routes.MapRoute(
            //    "AuthorizationList",
            //    "Authorization/List",
            //    new { controller = this.Name, action = "AuthorizationList", patientId = UrlParameter.Optional },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            //routes.MapRoute(
            //    "AuthorizationGrid",
            //    "Authorization/Grid",
            //    new { controller = this.Name, action = "AuthorizationGrid", patientId = new IsGuid() },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            routes.MapRoute(
                "PatientCharts",
                "Patient/Charts",
                new { controller = this.Name, action = "Charts", status = (int)PatientStatus.Active, patientId = Guid.Empty, Service = 0 },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
        }
    }
}
