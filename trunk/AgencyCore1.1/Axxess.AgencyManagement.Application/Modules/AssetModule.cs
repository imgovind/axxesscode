﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AssetModule : Module
    {
        public override string Name
        {
            get { return "Asset"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "ServeAssets",
                "Asset/{assetId}",
                new { controller = this.Name, action = "Serve", assetId = new IsGuid() });

            routes.MapRoute(
                "DeleteAsset",
                "DeleteAsset/{assetId}",
                new { controller = this.Name, action = "Delete", assetId = new IsGuid() });
        }
    }
}
