﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AccountModule : Module
    {
        public override string Name
        {
            get { return "Account"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
              "AppLogin",
              "AppLogin",
              new { controller = this.Name, action = "AppLogOn", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "Login",
              "Login",
              new { controller = this.Name, action = "LogOn", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Logout",
                "Logout",
                new { controller = this.Name, action = "LogOff", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "SessionExpired",
                "SessionExpired",
                new { controller = this.Name, action = "LogOff", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ResetAccount",
               "Reset",
               new { controller = this.Name, action = "Reset", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Forgot",
               "Forgot",
               new { controller = this.Name, action = "ForgotPassword", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Activate",
               "Activate",
               new { controller = this.Name, action = "Activate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Link",
               "Link",
               new { controller = this.Name, action = "Link", id = UrlParameter.Optional }
            );
        }
    }
}
