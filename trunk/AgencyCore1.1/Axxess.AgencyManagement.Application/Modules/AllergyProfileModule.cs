﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AllergyProfileModule : Module
    {
        public override string Name
        {
            get { return "AllergyProfile"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "AllergyProfilePrint",
                "AllergyProfile/Print/{id}",
                new { controller = this.Name, action = "Print", id = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
        }
    }
}
