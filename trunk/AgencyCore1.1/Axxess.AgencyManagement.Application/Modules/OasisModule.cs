﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class OasisModule : Module
    {
        public override string Name
        {
            get { return "Oasis"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapRoute(
            //    "485Medication",
            //    "485/Medication/{episodeId}/{patientId}/{eventId}",
            //    new { controller = this.Name, action = "PlanOfCareMedication", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "Profile",
                "Oasis/Profile/{patientId}/{Id}",
                new { controller = this.Name, action = "ProfilePrint", patientId = new IsGuid(), Id = new IsGuid(), type = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" });

        }
    }
}
