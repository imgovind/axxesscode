﻿namespace Axxess.AgencyManagement.Application.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class OrdersManagementModule : Module
    {
        public override string Name
        {
            get { return "OrdersManagement"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapRoute(
            //    "OrdersManagementHistoryDefault",
            //    "OrdersManagement/History/{patientId}",
            //    new { controller = this.Name, action = "PatientHistory", patientId = new IsGuid() },
            //    new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            //);
            routes.MapRoute(
                "OrdersManagementDefault",
                "HomeHealth/OrdersManagement/{action}",
                new { controller = this.Name, action = "Error" },
                new string[] { "Axxess.AgencyManagement.Application.Controllers" }
            );
        }

    }
}
