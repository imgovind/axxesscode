﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using iTextSharp.text;
    class AxxessDoc : Document
    {
        public static Rectangle Landscape = new Rectangle(792, 612);
        public AxxessDoc() : base(iTextSharp.text.PageSize.LETTER, 25, 25, 25, 25) { }
        public AxxessDoc(float[] margins) : base(iTextSharp.text.PageSize.LETTER, margins[3], margins[1], margins[0], margins[2]) { }
        public AxxessDoc(Rectangle PageSize) : base(PageSize, 25, 25, 25, 25) { }
        public AxxessDoc(Rectangle PageSize, float[] margins) : base(PageSize, margins[3], margins[1], margins[0], margins[2]) { }
        public void PopulateContent(IElement[] content)
        {
            foreach (IElement elm in content) this.Add(elm);
        }
    }
}