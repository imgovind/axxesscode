﻿namespace Axxess.AgencyManagement.Application.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities;

    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;

    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.Domain;

   
   
   class OasisProfileXml : BaseXml {
        private IDictionary<String, Question> Data = null;
        public OasisProfileXml(AssessmentPrint data)
            : base(PdfDocs.Get(PDFDOCSList.OasisProfile))
        {
            this.Data = data.Data;
            this.Init();
            this.FilterEmptyDiagnoses();
        }
        public override String GetData(string Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
        private void FilterEmptyDiagnoses() {
            for (int sectionI = 0; sectionI < this.Layout.Count(); sectionI++)
                for (int questionI = 0; questionI < this.Layout[sectionI].Question.Count(); questionI++)
                    for (int subQuestionI = 0; subQuestionI < this.Layout[sectionI].Question[questionI].Subquestion.Count(); subQuestionI++)
                        if (this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Data.Trim().Equals("()")) this.Layout[sectionI].Question[questionI].Subquestion.RemoveRange(--subQuestionI, 2);
                        else if (this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Type.Equals("text") && this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Data.IsNullOrEmpty()) this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Data = " ";
        }
    }
}