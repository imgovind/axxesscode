﻿namespace Axxess.AgencyManagement.Application.iTextExtension.XmlParsing
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Extensions;

    class HospitalizationLogXml : BaseXml
    {
        #region Private Members
        private IDictionary<string, Question> Data = null;
        #endregion

        #region Constructor and Methods

        public HospitalizationLogXml(HospitalizationLog data) : base(PdfDocs.Get(PDFDOCSList.HospitalizationLog))
        {
            this.Data = data.ToDictionary();

            this.Data.Add("M0903LastHomeVisitDate", new Question() { Answer = data.LastHomeVisitDate.ToZeroFilled() });
            this.Data.Add("M0906DischargeDate", new Question() { Answer = data.HospitalizationDate.ToZeroFilled() });

            this.Init();
        }

        public override string GetData(string index)
        {
            return this.Data.AnswerOrEmptyString(index);
        }

        #endregion
    }
}