﻿namespace Axxess.AgencyManagement.Application.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    class ReferralXml : BaseXml {
        private Dictionary<String, NotesQuestion> Data = new Dictionary<String, NotesQuestion>();
        public ReferralXml(Referral data) : base(PdfDocs.Get(PDFDOCSList.Referral)) {
            this.Data.Add("ReferrerPhysician", new NotesQuestion());
            this.Data["ReferrerPhysician"].Answer = data.ReferrerPhysician.ToString();
            this.Data.Add("AdmissionSource", new NotesQuestion());
            this.Data["AdmissionSource"].Answer = data.AdmissionSource.ToString();
            this.Data.Add("OtherReferralSource", new NotesQuestion());
            this.Data["OtherReferralSource"].Answer = data.OtherReferralSource;
            this.Data.Add("ReferralDate", new NotesQuestion());
            this.Data["ReferralDate"].Answer = data.ReferralDate.ToShortDateString();
            this.Data.Add("InternalReferral", new NotesQuestion());
            this.Data["InternalReferral"].Answer = data.InternalReferral.ToString();
            this.Data.Add("FirstName", new NotesQuestion());
            this.Data["FirstName"].Answer = data.FirstName;
            this.Data.Add("MiddleInitial", new NotesQuestion());
            this.Data["MiddleInitial"].Answer = data.MiddleInitial;
            this.Data.Add("LastName", new NotesQuestion());
            this.Data["LastName"].Answer = data.LastName;
            this.Data.Add("Gender", new NotesQuestion());
            this.Data["Gender"].Answer = data.Gender;
            this.Data.Add("DOB", new NotesQuestion());
            this.Data["DOB"].Answer = data.DOB.ToShortDateString();
            this.Data.Add("MaritalStatus", new NotesQuestion());
            this.Data["MaritalStatus"].Answer = data.MaritalStatus;
            this.Data.Add("Height", new NotesQuestion());
            this.Data["Height"].Answer = data.Height.ToString();
            this.Data.Add("HeightMetric", new NotesQuestion());
            this.Data["HeightMetric"].Answer = data.HeightMetric.ToString();
            this.Data.Add("Weight", new NotesQuestion());
            this.Data["Weight"].Answer = data.Weight.ToString();
            this.Data.Add("WeightMetric", new NotesQuestion());
            this.Data["WeightMetric"].Answer = data.WeightMetric.ToString();
            this.Data.Add("UserId", new NotesQuestion());
            this.Data["UserId"].Answer = data.UserId.ToString();
            this.Data.Add("MedicareNumber", new NotesQuestion());
            this.Data["MedicareNumber"].Answer = data.MedicareNumber;
            this.Data.Add("MedicaidNumber", new NotesQuestion());
            this.Data["MedicaidNumber"].Answer = data.MedicaidNumber;
            this.Data.Add("SSN", new NotesQuestion());
            this.Data["SSN"].Answer = data.SSN;
            this.Data.Add("AddressLine1", new NotesQuestion());
            this.Data["AddressLine1"].Answer = data.AddressLine1;
            this.Data.Add("AddressLine2", new NotesQuestion());
            this.Data["AddressLine2"].Answer = data.AddressLine2;
            this.Data.Add("AddressCity", new NotesQuestion());
            this.Data["AddressCity"].Answer = data.AddressCity;
            this.Data.Add("AddressStateCode", new NotesQuestion());
            this.Data["AddressStateCode"].Answer = data.AddressStateCode;
            this.Data.Add("AddressZipCode", new NotesQuestion());
            this.Data["AddressZipCode"].Answer = data.AddressZipCode;
            this.Data.Add("PhoneHome", new NotesQuestion());
            this.Data["PhoneHome"].Answer = data.PhoneHome.ToPhone();
            this.Data.Add("EmailAddress", new NotesQuestion());
            this.Data["EmailAddress"].Answer = data.EmailAddress;
            this.Data.Add("ServicesRequired", new NotesQuestion());
            this.Data["ServicesRequired"].Answer = data.ServicesRequired;
            this.Data.Add("DME", new NotesQuestion());
            this.Data["DME"].Answer = data.DME;

            if (data.EmergencyContact != null)
            {
                this.Data.Add("EmergencyContact.FirstName", new NotesQuestion());
                this.Data["EmergencyContact.FirstName"].Answer = data.EmergencyContact.FirstName;
                this.Data.Add("EmergencyContact.LastName", new NotesQuestion());
                this.Data["EmergencyContact.LastName"].Answer = data.EmergencyContact.LastName;
                this.Data.Add("EmergencyContact.Relationship", new NotesQuestion());
                this.Data["EmergencyContact.Relationship"].Answer = data.EmergencyContact.Relationship;
                this.Data.Add("EmergencyContact.PrimaryPhone", new NotesQuestion());
                this.Data["EmergencyContact.PrimaryPhone"].Answer = data.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() ? data.EmergencyContact.PrimaryPhone.ToPhone() : "";
                this.Data.Add("EmergencyContact.AlternatePhone", new NotesQuestion());
                this.Data["EmergencyContact.AlternatePhone"].Answer = data.EmergencyContact.AlternatePhone.IsNotNullOrEmpty() ? data.EmergencyContact.AlternatePhone.ToPhone() : "";
                this.Data.Add("EmergencyContact.Email", new NotesQuestion());
                this.Data["EmergencyContact.Email"].Answer = data.EmergencyContact.EmailAddress;
            }

            if (data.PrimaryPhysician != null)
            {
                this.Data.Add("Physician.Name", new NotesQuestion());
                this.Data["Physician.Name"].Answer = data.PrimaryPhysician.DisplayName;
                this.Data.Add("Physician.Address", new NotesQuestion());
                this.Data["Physician.Address"].Answer = data.PrimaryPhysician.AddressFull;
                this.Data.Add("Physician.Phone", new NotesQuestion());
                this.Data["Physician.Phone"].Answer = data.PrimaryPhysician.PhoneWork.IsNotNullOrEmpty() ? data.PrimaryPhysician.PhoneWork.ToPhone() : "";
                this.Data.Add("Physician.Fax", new NotesQuestion());
                this.Data["Physician.Fax"].Answer = data.PrimaryPhysician.FaxNumber.IsNotNullOrEmpty() ? data.PrimaryPhysician.FaxNumber.ToPhone() : "";
                this.Data.Add("Physician.NPI", new NotesQuestion());
                this.Data["Physician.NPI"].Answer = data.PrimaryPhysician.NPI;
            }

            this.Data.Add("IsDNR", new NotesQuestion());
            this.Data["IsDNR"].Answer = data.IsDNR.ToString();
            this.Data.Add("Comments", new NotesQuestion());
            this.Data["Comments"].Answer = data.Comments;
            this.Init();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override string GetData(string Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}