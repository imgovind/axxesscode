﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.iTextExtension.XmlParsing
{
    using System.Xml.Linq;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;

    class VitalSignSection : XmlElement
    {
        public List<XmlPrintQuestion> questions; 
        public VitalSignSection(BaseXml Xml, XElement Node)
            : base(Xml, Node)
        {
            questions = new List<XmlPrintQuestion>();
            XElement respQuestion = new XElement("question",
                new XAttribute("type", "multiple"),
                new XAttribute("label", "Respiration"),
                new XAttribute("display", "inline"),
                new XElement("question",
                    new XAttribute("type", "text"),
                    new XAttribute("data", "Resp")),
                new XElement("question",
                    new XAttribute("type", "select"),
                    new XAttribute("data", "RespStatus"),
                    new XElement("option",
                        new XAttribute("label", RegularIrregular.Regular.ToString()),
                        new XAttribute("value", (int)RegularIrregular.Regular)),
                    new XElement("option",
                        new XAttribute("label", RegularIrregular.Irregular.ToString()),
                        new XAttribute("value", (int)RegularIrregular.Irregular))));

            questions.Add(new XmlPrintQuestion(Xml, respQuestion));

            XElement weightQuestion = new XElement("question",
                new XAttribute("type", "multiple"),
                new XAttribute("label", "Weight"),
                new XAttribute("display", "inline"),
                new XElement("question",
                    new XAttribute("type", "text"),
                    new XAttribute("data", "VitalSignWeight")),
                new XElement("question",
                    new XAttribute("type", "select"),
                    new XAttribute("data", "VitalSignWeightUnit"),
                    new XElement("option",
                        new XAttribute("label", WeightUnit.lbs.ToString() ),
                        new XAttribute("value", (int)WeightUnit.lbs)),
                    new XElement("option",
                        new XAttribute("label", WeightUnit.kgs.ToString()),
                        new XAttribute("value", (int)WeightUnit.kgs))));

            questions.Add(new XmlPrintQuestion(Xml, weightQuestion));

            XElement tempQuestion = new XElement("question",
                new XAttribute("type", "multiple"),
                new XAttribute("label", "Temperature"),
                new XAttribute("display", "inline"),
                new XElement("question",
                    new XAttribute("type", "text"),
                    new XAttribute("data", "VitalSignTemp")),
                new XElement("question",
                    new XAttribute("type", "select"),
                    new XAttribute("data", "VitalSignTempUnit"),
                    new XElement("option",
                        new XAttribute("label", TemperatureUnit.F.GetDescription()),
                        new XAttribute("value", (int)TemperatureUnit.F)),
                    new XElement("option",
                        new XAttribute("label", TemperatureUnit.C.GetDescription()),
                        new XAttribute("value", (int)TemperatureUnit.C))),
                new XElement("question",
                    new XAttribute("type", "select"),
                    new XAttribute("data", "VitalSignTempLocation"),
                    new XElement("option",
                        new XAttribute("label", TemperatureLocation.None.ToString()),
                        new XAttribute("value", (int)TemperatureLocation.None)),
                    new XElement("option",
                        new XAttribute("label", TemperatureLocation.Axillary.ToString()),
                        new XAttribute("value", (int)TemperatureLocation.Axillary)),
                    new XElement("option",
                        new XAttribute("label", TemperatureLocation.Oral.ToString()),
                        new XAttribute("value", (int)TemperatureLocation.Oral)),
                    new XElement("option",
                        new XAttribute("label", TemperatureLocation.Tympanic.ToString()),
                        new XAttribute("value", (int)TemperatureLocation.Tympanic)),
                    new XElement("option",
                        new XAttribute("label", TemperatureLocation.Temporal.ToString()),
                        new XAttribute("value", (int)TemperatureLocation.Temporal))));

            questions.Add(new XmlPrintQuestion(Xml, tempQuestion));

            for (int i = 1; i <= 2; i++)
            {
                string num = i > 1 ?  i.ToString() : string.Empty;
                XElement pulseQuestion = new XElement("question",
                    new XAttribute("removable", i > 1),
                    new XAttribute("type", "multiple"),
                    new XAttribute("label", "Pulse"),
                    new XAttribute("display", "inline"),
                    new XElement("question", new XAttribute("type", "text"), new XAttribute("data", "VitalSignPulse" + num)),
                    new XElement("question", new XAttribute("type", "select"), new XAttribute("data", "VitalSignPulseLocation" + num), 
                        new XElement("option", 
                            new XAttribute("label", PulseLocation.Radial.ToString()),
                            new XAttribute("value", (int)PulseLocation.Radial)), 
                        new XElement("option",
                            new XAttribute("label", PulseLocation.Apical.ToString()),
                            new XAttribute("value", (int)PulseLocation.Apical))),
                    new XElement("question", new XAttribute("type", "select"), new XAttribute("data", "VitalSignPulseStatus" + num),
                        new XElement("option",
                            new XAttribute("label", RegularIrregular.Regular.ToString()),
                            new XAttribute("value", (int)RegularIrregular.Regular)),
                        new XElement("option",
                            new XAttribute("label", RegularIrregular.Irregular.ToString()),
                            new XAttribute("value", (int)RegularIrregular.Irregular))));

                questions.Add(new XmlPrintQuestion(Xml, pulseQuestion));
            }

            XElement bloodGlucoseQuestion = new XElement("question",
                 new XAttribute("type", "multiple"),
                 new XAttribute("label", "Blood Glucose"),
                 new XAttribute("display", "inline"),
                 new XElement("question",
                     new XAttribute("type", "text"),
                     new XAttribute("data", "VitalSignBloodGlucose")),
                 new XElement("question",
                     new XAttribute("type", "select"),
                     new XAttribute("data", "VitalSignBloodGlucoseType"),
                     new XElement("option",
                         new XAttribute("label", BloodGlucoseType.Fasting.ToString()),
                         new XAttribute("value", (int)BloodGlucoseType.Fasting)),
                     new XElement("option",
                         new XAttribute("label", BloodGlucoseType.Random.ToString()),
                         new XAttribute("value", (int)BloodGlucoseType.Random))));

            questions.Add(new XmlPrintQuestion(Xml, bloodGlucoseQuestion));

            XElement oxygenSaturationQuestion = new XElement("question",
                 new XAttribute("type", "multiple"),
                 new XAttribute("label", "Oxygen Saturation"),
                 new XAttribute("display", "inline"),
                 new XElement("question",
                     new XAttribute("type", "text"),
                     new XAttribute("data", "VitalSignOxygenSaturation")),
                 new XElement("question",
                     new XAttribute("type", "select"),
                     new XAttribute("data", "VitalSignOxygenSaturationType"),
                     new XElement("option",
                         new XAttribute("label", OxygenSaturationType.None.GetDescription()),
                         new XAttribute("value", (int)OxygenSaturationType.None)),
                     new XElement("option",
                         new XAttribute("label", "On O2"),
                         new XAttribute("value", (int)OxygenSaturationType.Oxygen)),
                     new XElement("option",
                         new XAttribute("label", OxygenSaturationType.RoomAir.GetDescription()),
                         new XAttribute("value", (int)OxygenSaturationType.RoomAir))));

            questions.Add(new XmlPrintQuestion(Xml, oxygenSaturationQuestion));

            for (int i = 1; i <= 6; i++)
            {
                string num = i > 1 ? i.ToString() : string.Empty;
                XElement bloodPressureQuestion = new XElement("question",
                    new XAttribute("removable", i > 1),
                    new XAttribute("type", "multiple"),
                    new XAttribute("label", "Blood Pressure"),
                    new XAttribute("display", "inline"),
                    new XElement("question", new XAttribute("type", "text"), new XAttribute("data", "VitalSignBloodPressure" + num)),
                    new XElement("question", new XAttribute("type", "select"), new XAttribute("data", "VitalSignBloodPressureSide" + num),
                        new XElement("option", 
                            new XAttribute("label", BloodPressureSide.None.GetDescription()),
                            new XAttribute("value", (int)BloodPressureSide.None)),
                        new XElement("option",
                            new XAttribute("label", BloodPressureSide.Left.ToString()), 
                            new XAttribute("value", (int)BloodPressureSide.Left)), 
                        new XElement("option",
                            new XAttribute("label", BloodPressureSide.Right.ToString()),
                            new XAttribute("value", (int)BloodPressureSide.Right))),
                    new XElement("question", new XAttribute("type", "select"), new XAttribute("data", "VitalSignBloodPressurePosition" + num), 
                        new XElement("option",
                            new XAttribute("label", BloodPressurePosition.None.GetDescription()), 
                            new XAttribute("value", (int)BloodPressurePosition.None)),
                        new XElement("option",
                            new XAttribute("label", BloodPressurePosition.Lying.ToString()),
                            new XAttribute("value", (int)BloodPressurePosition.Lying)),
                        new XElement("option",
                            new XAttribute("label", BloodPressurePosition.Sitting.ToString()),
                            new XAttribute("value", (int)BloodPressurePosition.Sitting)), 
                        new XElement("option",
                            new XAttribute("label", BloodPressurePosition.Standing.ToString()),
                            new XAttribute("value", (int)BloodPressurePosition.Standing))));

                questions.Add(new XmlPrintQuestion(Xml, bloodPressureQuestion));
            }

            XElement commentsQuestion = new XElement("question",
                new XAttribute("type", "textarea"),
                new XAttribute("label", "Comments"),
                new XAttribute("data", "VitalSignComments"));

            questions.Add(new XmlPrintQuestion(Xml, commentsQuestion));

        }
    }
}
