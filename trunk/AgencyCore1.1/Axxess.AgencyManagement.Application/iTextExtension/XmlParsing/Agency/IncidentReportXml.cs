﻿namespace Axxess.AgencyManagement.Application.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using System.ComponentModel;
    using System.Reflection;
    class IncidentReportXml : BaseXml {
        private Incident Data = new Incident();
        public IncidentReportXml(Incident data) : base(PdfDocs.Get(PDFDOCSList.IncidentReport)) {
            this.Data = data;
            this.Init();
            this.NotaFilter();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override string GetData(string Index) {
            foreach (PropertyDescriptor p in TypeDescriptor.GetProperties(this.Data)) if (p.Name == Index) return p.GetValue(this.Data) != null ? p.GetValue(this.Data).ToString() : string.Empty;
            return string.Empty;
        }
    }
}