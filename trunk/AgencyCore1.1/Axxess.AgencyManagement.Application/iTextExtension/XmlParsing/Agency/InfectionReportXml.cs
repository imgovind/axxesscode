﻿namespace Axxess.AgencyManagement.Application.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using System.ComponentModel;
    using System.Reflection;
    class InfectionReportXml : BaseXml {
        private Infection Data = new Infection();
        public InfectionReportXml(Infection data) : base(PdfDocs.Get(PDFDOCSList.InfectionReport)) {
            this.Data = data;
            this.Init();
            this.NotaFilter();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override String GetData(String Index) {
            foreach (PropertyDescriptor p in TypeDescriptor.GetProperties(this.Data)) if (p.Name == Index) return p.GetValue(this.Data) != null ? p.GetValue(this.Data).ToString() : string.Empty;
            return string.Empty;
        }
    }
}