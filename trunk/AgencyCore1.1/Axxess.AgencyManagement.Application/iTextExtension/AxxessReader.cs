﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System.IO;
    using iTextSharp.text.pdf;
    class AxxessReader : PdfReader {
        public AxxessReader(MemoryStream mem) : base(mem.ToArray()) { }
        public AxxessReader(PdfDoc doc) : base(doc.GetPdfFile()) { }
    }
}