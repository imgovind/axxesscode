﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using iTextSharp.text;
    class AxxessParagraph : Paragraph {
        public AxxessParagraph(String content, Font font, float fontSize) : base(content, font) {
            font.Size = fontSize;
        }
    }
}