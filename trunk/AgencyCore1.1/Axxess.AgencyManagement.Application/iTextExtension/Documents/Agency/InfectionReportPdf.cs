﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Helpers;
    class InfectionReportPdf : AxxessPdf {
        private InfectionReportXml xml;
        public InfectionReportPdf(Infection data) {
            this.xml = new InfectionReportXml(data);
            this.SetType(PdfDocs.Get(PDFDOCSList.InfectionReport));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans); 
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 200, 28.8F, 70, 28.8F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });

            var location = data.LocationProfile;//.GetBranch(data.PatientProfile != null ? data.PatientProfile.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(location));

            fieldmap[0].Add("infectiondate", data != null && data.InfectionDate.IsValid() ? data.InfectionDate.ToShortDateString() : String.Empty);
            fieldmap[0].Add("episode", (data != null && data.EpisodeStartDate.IsValidDate() ? data.EpisodeStartDate + "—" : String.Empty) + (data != null && data.EpisodeEndDate.IsValidDate() ? data.EpisodeEndDate : String.Empty));
            fieldmap[0].Add("physician", data != null && data.PhysicianName.IsNotNullOrEmpty() ? data.PhysicianName : String.Empty);
            fieldmap[0].Add("treatment", data != null && data.TreatmentPrescribed.IsNotNullOrEmpty() ? data.TreatmentPrescribed : String.Empty);
            fieldmap[0].Add("mdnotice", data != null && data.MDNotified.IsNotNullOrEmpty() ? data.MDNotified : String.Empty);
            fieldmap[0].Add("neworders", data != null && data.NewOrdersCreated.IsNotNullOrEmpty() ? data.NewOrdersCreated : String.Empty);
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : String.Empty);
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : String.Empty);
            fieldmap[0].Add("diag1", data != null && data.Diagnosis != null && data.Diagnosis.ContainsKey("PrimaryDiagnosis") ? data.Diagnosis["PrimaryDiagnosis"] : String.Empty);
            fieldmap[0].Add("diag2", data != null && data.Diagnosis != null && data.Diagnosis.ContainsKey("SecondaryDiagnosis") ? data.Diagnosis["SecondaryDiagnosis"] : String.Empty);
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("mrn", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            
            this.SetFields(fieldmap);
        }
    }
}