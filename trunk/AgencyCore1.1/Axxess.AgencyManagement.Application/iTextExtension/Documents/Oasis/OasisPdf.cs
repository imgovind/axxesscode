﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using XmlParsing;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Entities.Enums;
    class OasisPdf : AxxessPdf
    {
        private OasisXml xml;
        //private static String[] DocumentType = new String[] { "START OF CARE", string.Empty, "RESUMPTION OF CARE", "RECERTIFICATION", "FOLLOW-UP", "TRANSFER NOT DISCHARGED", "TRANSFER DISCHARGE", "DEATH AT HOME", "DISCHARGE FROM AGENCY" };
        public OasisPdf(AssessmentPrint data)
        {
            this.CreatePdf(data, true);
        }
        public OasisPdf(AssessmentPrint data, bool PrintAll)
        {
            this.CreatePdf(data, PrintAll);
        }
        public OasisPdf(AssessmentPrint data, string Discipline)
        {
            data.Discipline = Discipline;
            this.CreatePdf(data, true);
        }
        private void CreatePdf(AssessmentPrint data, bool PrintAll)
        {
            var patientName = string.Empty;
            if (data != null && data.Data != null && data.Data.Count > 0)
            {
                if (data.Data.ContainsKey("M0040LastName"))
                {
                    patientName = data.Data["M0040LastName"].Answer;
                    if (patientName.IsNotNullOrEmpty() && data.Data.ContainsKey("M0040FirstName"))
                    {
                        var firstName = data.Data["M0040FirstName"].Answer;
                        if (firstName.IsNotNullOrEmpty()) patientName += " , " + firstName;
                        else patientName = string.Empty;
                    }
                    else patientName = string.Empty;
                }
            }
            this.xml = new OasisXml(data, PrintAll);
            this.IsOasis = this.xml.GetOasisGeneration() == "0" ? false : true;
            if (data.Type == AssessmentType.Discharge)
            {
                if (data.Version == 2) this.SetType(PdfDocs.Get(PDFDOCSList.Discharge));
                else this.SetType(PdfDocs.Get(PDFDOCSList.Oasis));
            }
            else this.SetType(PdfDocs.Get(PDFDOCSList.Oasis));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection tab in this.xml.GetLayout())
            {
                content[count] = new AxxessTable(1);
                AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { .5F, 0, 0, 0 }), tabTitleCell = new AxxessCell(new float[] { 0, 0, 4, 0 }, new float[] { .5F, 0, 0, 0 });
                tabTitleCell.AddElement(new AxxessTitle(tab.Label, AxxessPdf.sansbold));
                foreach (XmlPrintSection section in tab.Subsection)
                {
                    AxxessContentSection contentSection = new AxxessContentSection(section, fonts, true, 8.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                }
                if (tab.Label.IsNotNullOrEmpty()) content[count].AddCell(tabTitleCell);
                content[count].AddCell(sectionCell);
                content[count].HeaderRows = 1;
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 100F, 28.5F, 25, 28.5F };
            this.SetMargins(margins);
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Location;// agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
            //if (location == null) location = agency.GetMainOffice();
            fieldmap[0].Add("agency",
                    (Current.AgencyName.ToTitleCase() + "\n") +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                );
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[1].Add("patientname", patientName);
            fieldmap[1].Add("AssessmentType", data.TaskName);// (this.IsOasis ? "OASIS-" + this.xml.GetOasisGeneration() : "NON-OASIS") + this.xml.GetDiscipline() + "\n" + OasisPdf.DocumentType[this.xml.GetAssessmentType()]
            this.SetFields(fieldmap);
        }
    }
}