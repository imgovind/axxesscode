﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.ViewData;
    class PrivateDutyCalendarPdf : AxxessPdf {
        public PrivateDutyCalendarPdf(List<PrivateDutyScheduleTask> tasks, DateTime startDate, DateTime endDate, string agencyName, string name, bool patientSelection)
        {
            this.SetPageSize(AxxessDoc.Landscape);
            this.SetType(PdfDocs.Get(PDFDOCSList.PrivateDutyCalendar));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 9F;
            this.SetFonts(fonts);
            AxxessTable[] calendar = new AxxessTable[] { new AxxessTable(7) };
            for (DateTime currentDate = startDate.AddDays(-(int)startDate.DayOfWeek); currentDate < ((int)endDate.DayOfWeek == 0 ? endDate : endDate.AddDays(7 - (int)endDate.DayOfWeek)); currentDate = currentDate.AddDays(1))
            {
                AxxessCell day = new AxxessCell(new float[] { -3, 2, 5, 2 }, new float[] { .2F, .2F, .2F, .2F });
                if (currentDate >= startDate && currentDate < endDate)
                {
                    AxxessTable dayTable = new AxxessTable(1, false);
                    AxxessCell dateNumber = new AxxessCell();
                    dateNumber.AddElement(new Chunk(currentDate.Day.ToString()));
                    dayTable.AddCell(dateNumber);
                    var events = tasks.FindAll(e => currentDate >= e.EventStartTime.ToZeroFilled().ToDateTime() && currentDate <= e.EventEndTime.ToZeroFilled().ToDateTime());
                    var count = events.Count;
                    if (count > 0)
                    {
                        dayTable.HeaderRows = 1;
                        events.ForEach(e =>
                        {
                            AxxessCell eventCell = new AxxessCell();
                            string eventText = e.IsAllDay ? string.Empty : string.Format("{0}-{1}\n", e.EventStartTime.ToShortTimeString(), e.EventEndTime.ToShortTimeString());
                            string DisplayName = e.DisciplineTaskName;
                            if (e.IsMissedVisit) { DisplayName = "Missed Visit"; }
                            eventText += string.Format("{0} - {1}", DisplayName, patientSelection ? e.UserName : e.PatientName);
                            eventCell.AddElement(new Paragraph(eventText, fonts[0]));
                            dayTable.AddCell(eventCell);
                        });
                    }
                    else
                    {
                        AxxessCell eventCell = new AxxessCell();
                        eventCell.AddElement(new Paragraph("\n\n\n", fonts[0]));
                        dayTable.AddCell(eventCell);
                    }
                    day.AddElement(dayTable);
                    calendar[0].AddCell(day);
                }
                else 
                {
                    day.AddElement(new Chunk(""));
                    calendar[0].AddCell(day);
                }
            }
            this.SetContent(calendar);
            this.SetMargins(new float[] { 90, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("Agency", agencyName);
            fieldmap[0].Add("DateRange", String.Format("{0} - {1}-{2}", name, startDate.ToShortDateString(), endDate.ToShortDateString()));
            this.SetFields(fieldmap);
        }
    }
}