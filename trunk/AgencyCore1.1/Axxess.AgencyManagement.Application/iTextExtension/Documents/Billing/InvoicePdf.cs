﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    class InvoicePdf : AxxessPdf
    {

        public InvoicePdf(BaseInvoiceFormViewData data)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.Invoice));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data));
            float[] margins = new float[] { 260, 12, 142, 52.5F };
            this.SetMargins(margins);
            this.SetFields(this.BuildFieldMap(data));
        }

        protected virtual List<Dictionary<String, String>> BuildFieldMap(BaseInvoiceFormViewData data)
        {
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<string, string>() { });
            string agencyName = data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() : string.Empty;
            string agency = agencyName;
            agency += data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? "\n" + data.AgencyLocation.AddressFirstRow.ToUpper() : string.Empty;
            agency += data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? "\n" + data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty;
            agency += data.AgencyLocation != null && data.AgencyLocation.PhoneWork.IsNotNullOrEmpty() ? "\nPhone: " + data.AgencyLocation.PhoneWorkFormatted : string.Empty;
            agency += data.AgencyLocation != null && data.AgencyLocation.FaxNumber.IsNotNullOrEmpty() ? "\tFax: " + data.AgencyLocation.FaxNumberFormatted : string.Empty;
            fieldmap[0].Add("agency", agency);

            string payer = string.Empty;
            if (data.Claim.IsBillingAddressDifferent)
            {
                payer = (data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty) + ("\n" + data.PayorAddressLine1 + "\n" + data.PayorAddressLine2);
                fieldmap[0].Add("payer", payer);
                var patient = (data.Claim != null && data.Claim.DisplayName.IsNotNullOrEmpty() ? data.Claim.DisplayName : string.Empty) + ("\n" + data.Claim.AddressFirstRow + "\n" + data.Claim.AddressSecondRow);
                fieldmap[0].Add("patient", patient);
            }
            else
            {
                payer = (data.Claim != null && data.Claim.DisplayName.IsNotNullOrEmpty() ? data.Claim.DisplayName : string.Empty) + ("\n" + data.Claim.AddressFirstRow + "\n" + data.Claim.AddressSecondRow);
                fieldmap[0].Add("payer", payer);
                fieldmap[0].Add("patient", payer);
            }
            //if (data.IsForPatient)
            //{
            //    payer = (data.Claim.DisplayName != null && data.Claim.DisplayName.IsNotNullOrEmpty() ? data.Claim.DisplayName : string.Empty) + ("\n" + data.Claim.AddressFirstRow + "\n" + data.Claim.AddressSecondRow);
            //}
            //else
           
            double total = CalculateTotal();
            double dueAmount = total - data.Claim.PaymentAmount - data.Claim.AdjustmentAmount;
            fieldmap[0].Add("number", data.Claim.ClaimNumber.ToString());
            fieldmap[0].Add("date", DateTime.Today.ToZeroFilled());

            
            fieldmap[0].Add("top-total", total.ToString("C"));
            fieldmap[0].Add("due-date", "");

            fieldmap[0].Add("bottom-total", total.ToString("C"));
            fieldmap[0].Add("payments", data.Claim.PaymentAmount.ToString("C"));
            fieldmap[0].Add("amount-due", dueAmount.ToString("C"));
            
            string contactPerson = "";
            if(data.Agency != null)
            {
                contactPerson = data.Agency.ContactPersonDisplayName;
            }

            string phoneWork = data.Agency != null && data.Agency.ContactPersonPhoneFormatted.IsNotNullOrEmpty() ? data.Agency.ContactPersonPhoneFormatted : string.Empty;
            string message = string.Format("Please make payment to {0} as soon as possible", agencyName);
            message += contactPerson.IsNotNullOrEmpty() ?  string.Format(" and if you have any questions please call {0} at {1}." , contactPerson, phoneWork) : ".";
            fieldmap[0].Add("message", message);

            return fieldmap;
        }

        protected virtual IElement[] BuildContent(BaseInvoiceFormViewData data)
        {
            var content = new AxxessTable[] { new AxxessTable(new float[] { 1.9F, 9.5F, 5.3F, 4.3F, 3.5F, 2.9F, 4.9F }, false) };
            var font = AxxessPdf.sans;
            font.Size = 8;
            float[] padding = new float[] { .5F, 1, .5F, 1 }, borders = new float[] { 0, 0, 0, 0 }, moneypad = new float[] { .5F, 13, .5F, 1 }, moneypad2 = new float[] { 0, 37, .05F, 1 };

            if (data != null && data.Claim != null && data.Claim.IsHomeHealthServiceIncluded)
            {
                content[0].AddCell("0023", font, padding, borders);
                content[0].AddCell("HOME HEALTH SERVICES", font, padding, borders);
                content[0].AddCell(data.Claim != null && data.Claim.HippsCode.IsNotNullOrEmpty() ? data.Claim.HippsCode : string.Empty, font, padding, borders);
                content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MM/dd/yyyy") : string.Empty, font, padding, borders);
                content[0].AddCell("0.00", font, "Right", moneypad, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, "Right", moneypad2, borders);
            }

            var supplies = data.Claim != null && data.Claim.Supply.IsNotNullOrEmpty() ? data.Claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
            if (supplies != null && supplies.Count > 0)
            {
                var supplyTotal = 0.0;
                foreach (var supply in supplies)
                {
                    content[0].AddCell(supply.RevenueCode, font, padding, borders);
                    content[0].AddCell(supply.Description, font, padding, borders);
                    content[0].AddCell(supply.Code, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit.ToString("MM/dd/yyyy"), font, padding, borders);
                    content[0].AddCell(string.Format("{0:0.00}", supply.UnitCost), font, "Right", moneypad, borders);
                    content[0].AddCell(supply.Quantity > 0 ? supply.Quantity.ToString() : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.TotalCost.ToString("C"), font, "Right", moneypad2, borders);
                    supplyTotal += supply.TotalCost;
                }
                this.SupplyValue = supplyTotal;
            }

            //var schedules = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<ScheduleEvent>();
            if (data != null )
            {
                var visits = data.BillSchedules;// billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, ClaimType.MAN, data.Claim.ChargeRates, true);
                if (visits != null && visits.Count > 0)
                {
                    var total = 0.0;
                    foreach (var visit in visits)
                    {
                        content[0].AddCell(visit.RevenueCode, font, padding, borders);
                        content[0].AddCell(visit.PereferredName != null ? visit.PereferredName : visit.DisciplineTaskName, font, padding, borders);
                        content[0].AddCell(string.Format("{0} {1} {2} {3} {4}", visit.HCPCSCode, visit.Modifier, visit.Modifier2, visit.Modifier3, visit.Modifier4), font, padding, borders);
                        content[0].AddCell(visit.VisitDate.IsValid() ? visit.VisitDate.ToString("MM/dd/yyyy") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.Charge > 0 ? string.Format("{0:#0.00}", visit.Charge) : string.Empty, font, "Right", moneypad, borders);
                        content[0].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : "0", font, padding, borders);
                        double visitTotal = visit.Charge;
                        content[0].AddCell(visitTotal.ToString("C"), font, "Right", moneypad2, borders);
                        total += visit.Charge;
                    }
                    this.VisitValue = total;
                }
            }
            return content;
        }

        protected virtual double CalculateTotal()
        {
            return this.SupplyValue + this.VisitValue;
        }

        protected double SupplyValue { get; set; }
        protected double VisitValue { get; set; }
    }
}