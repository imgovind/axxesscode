﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using iTextSharp.text;
    class BillingRapPdf : AxxessPdf {
        public BillingRapPdf(Rap data) {
            this.SetType(PdfDocs.Get(PDFDOCSList.BillingRap));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 11F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Remark.IsNotNullOrEmpty() ? data.Remark : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 515, 35, 35, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            if (data.LocationProfile != null)
            {
                var location = data.LocationProfile;
               
                    fieldmap[0].Add("agency", (
                            (location.Name.ToTitleCase() + "\n" ) +
                            (location != null ?
                                (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") +
                                (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                                (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") +
                                (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") +
                                (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : "") +
                                (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                                (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                            : "")
                        ));
            }
            fieldmap[0].Add("patient", (
                data != null ?
                    (data.AddressLine1.IsNotNullOrEmpty() ? data.AddressLine1.ToTitleCase() + "\n" : "") +
                    (data.AddressLine2.IsNotNullOrEmpty() ? data.AddressLine2.ToTitleCase() + "\n" : "") +
                    (data.AddressCity.IsNotNullOrEmpty() ? data.AddressCity.ToTitleCase() + ", " : "") +
                    (data.AddressStateCode.IsNotNullOrEmpty() ? data.AddressStateCode.ToTitleCase() + "  " : "") +
                    (data.AddressZipCode.IsNotNullOrEmpty() ? data.AddressZipCode + "\n" : "") : ""));
            fieldmap[0].Add("mcare", data != null && data.MedicareNumber.IsNotNullOrEmpty() ? data.MedicareNumber : "");
            fieldmap[0].Add("record", data != null && data.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientIdNumber : "");
            fieldmap[0].Add("gender", data != null && data.Gender.IsNotNullOrEmpty() ? data.Gender : "");
            fieldmap[0].Add("dob", data != null && data.DOB.IsValid() ? data.DOB.ToShortDateString() : "");
            fieldmap[0].Add("type", data != null ?
                (data.Type == 0 ? "Initial RAP" : "") +
                (data.Type == 1 ? "RAP Cancellation" : "") : "");
            fieldmap[0].Add("admitdate", data != null && data.StartofCareDate.IsValid() ? data.StartofCareDate.ToShortDateString() : "");
            fieldmap[0].Add("admitsource", data != null && data.AdmissionSourceDisplay.IsNotNullOrEmpty() ? data.AdmissionSourceDisplay : "");
            fieldmap[0].Add("status", data != null ?
                (data.PatientStatus == 1 ? "Active" : "") +
                (data.PatientStatus == 2 ? "Discharged" : "") : "");
            fieldmap[0].Add("cond18", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode18 : string.Empty);
            fieldmap[0].Add("cond19", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode19 : string.Empty);
            fieldmap[0].Add("cond20", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode20 : string.Empty);
            fieldmap[0].Add("cond21", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode21 : string.Empty);
            fieldmap[0].Add("cond22", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode22 : string.Empty);
            fieldmap[0].Add("cond23", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode23 : string.Empty);
            fieldmap[0].Add("cond24", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode24 : string.Empty);
            fieldmap[0].Add("cond25", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode25 : string.Empty);
            fieldmap[0].Add("cond26", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode26 : string.Empty);
            fieldmap[0].Add("cond27", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode27 : string.Empty);
            fieldmap[0].Add("cond28", data != null && data.ConditionCodesObject != null ? data.ConditionCodesObject.ConditionCode28 : string.Empty);
            fieldmap[0].Add("hipps", data != null && data.HippsCode.IsNotNullOrEmpty() ? data.HippsCode : "");
            fieldmap[0].Add("matchkey", data != null && data.ClaimKey.IsNotNullOrEmpty() ? data.ClaimKey : "");
            fieldmap[0].Add("startdate", data != null && data.EpisodeStartDate.IsValid() ? data.EpisodeStartDate.ToShortDateString() : "");
            fieldmap[0].Add("billvisit", data != null && data.FirstBillableVisitDate.IsValid() ? data.FirstBillableVisitDate.ToShortDateString() : "");
            fieldmap[0].Add("phys", data != null ? 
                (data.PhysicianLastName.IsNotNullOrEmpty() ? data.PhysicianLastName.ToTitleCase() + ", " : "") +
                (data.PhysicianFirstName.IsNotNullOrEmpty() ? data.PhysicianFirstName.ToTitleCase() : "") : "");
            fieldmap[0].Add("physnpi", data != null && data.PhysicianNPI.IsNotNullOrEmpty() ? data.PhysicianNPI : "");
            fieldmap[0].Add("hippspay", data != null && data.ProspectivePay > 0 ? data.ProspectivePay.ToString() : "");
            fieldmap[0].Add("diag1", data != null && data.DiagnosisCodesObject != null ? data.DiagnosisCodesObject.Primary : string.Empty);
            fieldmap[0].Add("diag2", data != null && data.DiagnosisCodesObject != null ? data.DiagnosisCodesObject.Second : string.Empty);
            fieldmap[0].Add("diag3", data != null && data.DiagnosisCodesObject != null ? data.DiagnosisCodesObject.Third : string.Empty);
            fieldmap[0].Add("diag4", data != null && data.DiagnosisCodesObject != null ? data.DiagnosisCodesObject.Fourth : string.Empty);
            fieldmap[0].Add("diag5", data != null && data.DiagnosisCodesObject != null ? data.DiagnosisCodesObject.Fifth : string.Empty);
            fieldmap[0].Add("diag6", data != null && data.DiagnosisCodesObject != null ? data.DiagnosisCodesObject.Sixth : string.Empty);
            fieldmap[1].Add("patientname", data != null ?
                (data.LastName.IsNotNullOrEmpty() ? data.LastName.ToLower().ToTitleCase() + ", " : "") +
                (data.FirstName.IsNotNullOrEmpty() ? data.FirstName.ToLower().ToTitleCase() : "") : "");
            this.SetFields(fieldmap);
        }
    }
}