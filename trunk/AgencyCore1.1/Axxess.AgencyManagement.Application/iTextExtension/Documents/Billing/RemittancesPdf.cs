﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using iTextSharp.text;
 public   class RemittancesPdf : AxxessPdf {
        public RemittancesPdf(List<RemittanceLean> data, AgencyLocation location) {
            this.SetType(PdfDocs.Get(PDFDOCSList.Remittances));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            if (data.Count == 0) {
                Paragraph[] content = new Paragraph[] { new Paragraph(" ") };
                this.SetContent(content);
            } else {
                AxxessTable[] content = new AxxessTable[1];
                float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { 0, 0, .2F, 0 };
                content[0] = new AxxessTable(new float[] { 10, 61, 44, 40, 43, 32 }, false);
                int count = 0;
                foreach (var remittance in data) {
                    content[0].AddCell((++count).ToString(), fonts[0], padding, borders);
                    content[0].AddCell(remittance.RemitId, fonts[0], padding, borders);
                    content[0].AddCell(remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToString("MM/dd/yyyy") : string.Empty, fonts[0], padding, borders);
                    content[0].AddCell(remittance.PaymentDate > DateTime.MinValue ? remittance.PaymentDate.ToString("MM/dd/yyyy") : string.Empty, fonts[0], padding, borders);
                    content[0].AddCell(string.Format("${0:#,0.00}", remittance.PaymentAmount), fonts[0], padding, borders);
                    content[0].AddCell(remittance.TotalClaims.ToString(), fonts[0], padding, borders);
                }
                this.SetContent(content);
            }
            this.SetMargins(new float[] { 100, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency",
                    (Current.AgencyName.ToTitleCase() + "\n") +
                    location != null ?
                        (location.AddressLine1.ToTitleCase() +
                        (location.AddressLine2.ToTitleCase() + "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") +
                        (location.AddressStateCode.ToUpper() + "  ") +
                        (location.AddressZipCode) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                    ) : "");
            this.SetFields(fieldmap);
        }
    }
}