﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    using iTextSharp.text;
    
    class BillingClaimsPdf : AxxessPdf
    {
        public BillingClaimsPdf(List<Bill> bills, LocationPrintProfile location)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.BillingClaims));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 9F;
            this.SetFonts(fonts);
           var contents = new List<AxxessTable>();
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            float[] smallPadding = new float[] { 2, 2, 2, 2 };
            if (bills != null && bills.Count > 0)
            {
                foreach (var bill in bills)
                {
                    var  content = new AxxessTable(1);
                    content.AddCell(new AxxessTitle(string.Format("{0}(S)", bill.ClaimType.GetDescription()), fonts[1]), padding, borders);
                    content.AddCell(new AxxessTitle(string.Format("{0} | {1}", bill.BranchName,bill.InsuranceName), fonts[1]), padding, borders);
                    content.HeaderRows = 1;
                    var table = this.GetTableContent(bill.ClaimType, bill.Claims, fonts, padding, smallPadding, borders);
                    content.AddCell(table, none, none);
                    contents.Add(content);
                }
                this.SetContent(contents.ToArray());
            }

            var margins = new float[] { 82, 28, 28, 28 };
            this.SetMargins(margins);
            var fieldmap = new List<Dictionary<string, string>>();
            if (location != null)
            {
                fieldmap.Add(new Dictionary<String, String>() { });
                fieldmap[0].Add("agency", ((location.Name + "\n" ) +
                            (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") +
                            (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                            (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") +
                            (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") +
                            (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : "") +
                            (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                            (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                    ));
            }
            this.SetFields(fieldmap);
        }
        private AxxessTable GetTableContent(ClaimTypeSubCategory claimType, IList<Claim> claims, List<Font> fonts, float[] padding, float[] smallPadding, float[] borders)
        {
            var table = new AxxessTable(new float[] { 3, 2, 3, 1, 1, 1, 1.1F }, false);
            float[] none = new float[] { 0, 0, 0, 0 };
            if (claimType == ClaimTypeSubCategory.RAP)
            {
                table = new AxxessTable(new float[] { 2, 3, 2, 2, 3, 1, 1, 1 }, false);
                table.AddCell("MRN", fonts[1], padding, borders);
                table.AddCell("Patient Name", fonts[1], padding, borders);
                table.AddCell("Case Manager", fonts[1], padding, borders);
                table.AddCell("Clinician", fonts[1], padding, borders);
                table.AddCell("Episode Date", fonts[1], padding, borders);
                table.AddCell("Oasis", fonts[1], padding, borders);
                table.AddCell("Billable Visit", fonts[1], padding, borders);
                table.AddCell("Verified", fonts[1], padding, borders);
                table.HeaderRows = 1;
                if (claims != null && claims.Count > 0)
                {
                    foreach (var claim in claims)
                    {
                        var rap = (RapBill)claim;
                        table.AddCell(rap.PatientIdNumber, fonts[0], padding, borders);
                        table.AddCell(rap.LastName + ", " + rap.FirstName, fonts[0], padding, borders);
                        table.AddCell(rap.CaseManager, fonts[0], padding, borders);
                        table.AddCell(rap.Clinician, fonts[0], padding, borders);
                        table.AddCell(rap.DateRange, fonts[0], padding, borders);
                        table.AddCell(new AxxessCheckbox(rap.IsOasisComplete, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(rap.IsFirstBillableVisit, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(rap.IsVerified, 14), none, borders);
                    }
                }
               
            }
            else if (claimType == ClaimTypeSubCategory.Final)
            {
                table = new AxxessTable(new float[] { 2, 3, 3, 2, 2, 1, 1, 1, 1.1F }, false);
                table.AddCell("MRN", fonts[1], padding, borders);
                table.AddCell("Patient Name", fonts[1], padding, borders);
                table.AddCell("Case Manager", fonts[1], padding, borders);
                table.AddCell("Clinician", fonts[1], padding, borders);
                table.AddCell("Episode Date", fonts[1], padding, borders);
                table.AddCell("Rap", fonts[1], smallPadding, borders);
                table.AddCell("Visit", fonts[1], smallPadding, borders);
                table.AddCell("Order", fonts[1], smallPadding, borders);
                table.AddCell("Verified", fonts[1], smallPadding, borders);
                table.HeaderRows = 1;
                if (claims != null && claims.Count > 0)
                {
                    foreach (var claim in claims)
                    {
                        var final = (FinalBill)claim;
                        table.AddCell(final.PatientIdNumber, fonts[0], padding, borders);
                        table.AddCell(final.LastName + ", " + final.FirstName, fonts[0], padding, borders);
                        table.AddCell(claim.CaseManager, fonts[0], padding, borders);
                        table.AddCell(claim.Clinician, fonts[0], padding, borders);
                        table.AddCell(final.DateRange, fonts[0], padding, borders);
                        table.AddCell(new AxxessCheckbox(final.IsRapGenerated, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(final.AreVisitsComplete, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(final.AreOrdersComplete, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(final.IsVisitVerified && final.IsSupplyVerified && final.IsInfoVerified && final.IsInsuranceVerified, 14), none, borders);
                    }
                }
            }
            else if (claimType == ClaimTypeSubCategory.ManagedCare)
            {
                table = new AxxessTable(new float[] { 2, 3, 3, 2, 2, 1, 1, 1, 1.1F }, false);
                table.AddCell("MRN", fonts[1], padding, borders);
                table.AddCell("Patient Name", fonts[1], padding, borders);
                table.AddCell("Case Manager", fonts[1], padding, borders);
                table.AddCell("Clinician", fonts[1], padding, borders);
                table.AddCell("Episode Date", fonts[1], padding, borders);
                table.AddCell("Detail", fonts[1], padding, borders);
                table.AddCell("Visit", fonts[1], padding, borders);
                table.AddCell("Supply", fonts[1], padding, borders);
                table.AddCell("Verified", fonts[1], padding, borders);
                table.HeaderRows = 1;
                if (claims != null && claims.Count > 0)
                {
                    foreach (var claim in claims)
                    {
                        var managedClaim = (ManagedBill)claim;
                        table.AddCell(managedClaim.PatientIdNumber, fonts[0], padding, borders);
                        table.AddCell(managedClaim.LastName + ", " + managedClaim.FirstName, fonts[0], padding, borders);
                        table.AddCell(managedClaim.CaseManager, fonts[0], padding, borders);
                        table.AddCell(managedClaim.Clinician, fonts[0], padding, borders);
                        table.AddCell(managedClaim.DateRange, fonts[0], padding, borders);
                        table.AddCell(new AxxessCheckbox(managedClaim.IsInfoVerified, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(managedClaim.IsVisitVerified, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(managedClaim.IsSupplyVerified, 14), none, borders);
                        table.AddCell(new AxxessCheckbox(managedClaim.IsVisitVerified && managedClaim.IsSupplyVerified && managedClaim.IsInfoVerified, 14), none, borders);
                    }
                }
            }
            return table;
        }
    }
}