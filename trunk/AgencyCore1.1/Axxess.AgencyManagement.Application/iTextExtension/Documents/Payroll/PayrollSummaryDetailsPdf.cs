﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;

    using Axxess.Core.Extension;
    using iTextSharp.text;

    public class PayrollSummaryDetailsPdf : AxxessPdf
    {
        private List<PayrollDetail> data;
        private string agencyName;
        private DateTime startDate;
        private DateTime endDate;
        public PayrollSummaryDetailsPdf(PayrollDetail data, string agencyName, DateTime startDate, DateTime endDate)
        {
            this.SetPageSize(AxxessDoc.Landscape);
            this.data = new List<PayrollDetail>();
            this.data.Add(data);
            this.agencyName = agencyName;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        public PayrollSummaryDetailsPdf(List<PayrollDetail> data, string agencyName, DateTime startDate, DateTime endDate)
        {
            this.SetPageSize(AxxessDoc.Landscape);
            this.data = data;
            this.agencyName = agencyName;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        private void BuildLayout()
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.PayrollDetail));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            fonts[2].Size = 10F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[this.data.Count];
            int counter = 0;
            foreach (var table in data)
            {
                AxxessTable usertable = new AxxessTable(new float[] { 2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, false);
                AxxessCell user = new AxxessCell(padding, borders);
                user.Colspan = 12;
                user.AddElement(new Paragraph("Employee: " + table.Name, fonts[2]));
                usertable.AddCell(user);
                usertable.AddCell("Patient Name", fonts[1], padding, borders);
                usertable.AddCell("Task", fonts[1], padding, borders);
                usertable.AddCell("Visit Date", fonts[1], padding, borders);
                usertable.AddCell("Time In", fonts[1], padding, borders);
                usertable.AddCell("Time Out", fonts[1], padding, borders);
                usertable.AddCell("Hours", fonts[1], padding, borders);
                usertable.AddCell("Pay Rate", fonts[1], padding, borders);
                usertable.AddCell("Miles", fonts[1], padding, borders);
                usertable.AddCell("Mileage Rate", fonts[1], padding, borders);
                usertable.AddCell("Visit Pay", fonts[1], padding, borders);
                usertable.AddCell("Surcharge", fonts[1], padding, borders);
                usertable.AddCell("Total Pay", fonts[1], padding, borders);
                usertable.HeaderRows = 2;
                var totalTime = 0;
                var totalPay = 0.0;
                foreach (UserVisit row in table.Visits)
                {
                    usertable.AddCell(row.PatientName, fonts[0], padding, borders);
                    usertable.AddCell(row.TaskName, fonts[0], padding, borders);
                    usertable.AddCell(row.VisitDate.ToString("MM/dd/yyyy"), fonts[0], padding, borders);
                    usertable.AddCell(row.TimeIn, fonts[0], padding, borders);
                    usertable.AddCell(row.TimeOut, fonts[0], padding, borders);
                    usertable.AddCell(string.Format("{0:#0.00}", (double)row.MinSpent / 60), fonts[0], padding, borders);
                    usertable.AddCell(row.VisitRate, fonts[0], padding, borders);
                    usertable.AddCell(row.AssociatedMileage, fonts[0], padding, borders);
                    usertable.AddCell(row.MileageRate, fonts[0], padding, borders);
                    usertable.AddCell(row.VisitPayment, fonts[0], padding, borders);
                    usertable.AddCell(row.Surcharge, fonts[0], padding, borders);
                    usertable.AddCell(row.TotalPayment, fonts[0], padding, borders);
                    totalTime += row.MinSpent;
                    totalPay += row.Total;
                }
                content[counter++] = usertable;
                AxxessCell userFooter = new AxxessCell(padding, borders);
                userFooter.Colspan = 12;
                userFooter.AddElement(new AxxessTitle(string.Format("Total Visits: {0}, Total Time: {1}, Total Payment: {2}", table.Visits.Count, string.Format(" {0} min = {1:#0.00} hour(s) ", totalTime, (double)totalTime / 60), string.Format("${0:#0.00}", totalPay)), fonts[1]));
                usertable.AddCell(userFooter);
            }
            this.SetContent(content);
            float[] margins = new float[] { 57, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", agencyName);
            fieldmap[0].Add("date", this.startDate.ToShortDateString() + " - " + this.endDate.ToShortDateString());
            this.SetFields(fieldmap);
        }
    }
}