﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class PayrollSummaryPdf : AxxessPdf {
        public PayrollSummaryPdf(List<VisitSummary> data, LocationPrintProfile location, DateTime startDate, DateTime endDate)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.PayrollSummary));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 2, 1 }, false) };
            content[0].AddCell("User", fonts[1], padding, borders);
            content[0].AddCell("Count", fonts[1], padding, borders);
            content[0].HeaderRows = 1;
            foreach (VisitSummary row in data) {
                content[0].AddCell(row.UserName, fonts[0], padding, borders);
                content[0].AddCell(row.VisitCount.ToString(), fonts[0], padding, borders);
            }
            this.SetContent(content);
            float[] margins = new float[] { 100, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", (
                location != null ?
                    (location.Name + "\n" ) +
                    (
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : "") +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                    )
                : ""));
            fieldmap[0].Add("startdate", startDate.ToShortDateString());
            fieldmap[0].Add("enddate", endDate.ToShortDateString());
            this.SetFields(fieldmap);
        }
    }
}