﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    class MSWPdf : VisitNotePdf {
        public MSWPdf(VisitNoteViewData data) : base(data, 0) { }
        protected override float[] Margins(VisitNoteViewData data)
        {
            float bottomMargin = data.Type == "MSWAssessment" || data.Type == "MSWDischarge" || data.Type == "MSWEvaluationAssessment" ? 93 : 60;
            return new float[] { 140, 28.3F, bottomMargin, 28.3F };
        }

        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("visitdate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : "");
            fieldmap[0].Add("timein", data != null && data.TimeIn != null ? data.TimeIn : "");
            fieldmap[0].Add("timeout", data != null && data.TimeOut != null ? data.TimeOut : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName.IsNotNullOrEmpty() ? data.PhysicianDisplayName : "");
            //TODO: Add physician signature
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            switch (data.Type) {
                case "MSWAssessment": fieldmap[1].Add("doctype", "MSW ASSESSMENT"); break;
                case "MSWDischarge": fieldmap[1].Add("doctype", "MSW DISCHARGE"); break;
                case "MSWEvaluationAssessment": fieldmap[1].Add("doctype", "MSW EVALUATION"); break;
                case "MSWProgressNote": fieldmap[1].Add("doctype", "MSW PROGRESS NOTE"); break;
                case "MSWVisit": fieldmap[1].Add("doctype", "MSW VISIT"); break;
            }
            fieldmap[0].Add("patientname", data != null && data.PatientProfile != null ? (data.PatientProfile.LastName.IsNotNullOrEmpty() ? data.PatientProfile.LastName.ToLower().ToTitleCase() + ", " : "") + (data.PatientProfile.FirstName.IsNotNullOrEmpty() ? data.PatientProfile.FirstName.ToLower().ToTitleCase() + " " : "") + (data.PatientProfile.MiddleInitial.IsNotNullOrEmpty() ? data.PatientProfile.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            return fieldmap;
        }
    }
}