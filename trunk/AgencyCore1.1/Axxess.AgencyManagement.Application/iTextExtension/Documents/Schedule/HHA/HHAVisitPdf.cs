﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Helpers;
    class HHAVisitPdf : VisitNotePdf
    {
        public HHAVisitPdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.HHAVisit), 0) { }
        protected override IElement[] Content(VisitNoteXml xml)
        {
            AxxessTable[] content = new AxxessTable[xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in xml.GetLayout())
            {
                content[count] = new AxxessTable(section.Cols > 0 ? section.Cols : 1);
                foreach (XmlPrintSection subsection in section.Subsection)
                {
                    AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessContentSection contentSection = new AxxessContentSection(subsection, this.GetFonts(), true, 7.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                    content[count].AddCell(sectionCell);
                }
                count++;
            }
            return content;
        }
        protected override float[] Margins(VisitNoteViewData data)
        {
            return new float[] { 160, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            fieldmap[0].Add("visitdate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("timein", data != null && data.TimeIn != null ? data.TimeIn : "");
            fieldmap[0].Add("timeout", data != null && data.TimeOut != null ? data.TimeOut : "");
            fieldmap[0].Add("episode", data != null && data.StartDate.IsValid() && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("hhafreq", data != null && data.Questions != null && data.Questions.ContainsKey("HHAFrequency") && data.Questions["HHAFrequency"].Answer.IsNotNullOrEmpty() ? data.Questions["HHAFrequency"].Answer : "");
            fieldmap[0].Add("dnr", data != null && data.Questions != null && data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["DNR"].Answer.Equals("1") ? "Yes" : "") +
                (data.Questions["DNR"].Answer.Equals("0") ? "No" : "") : "");
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("diagnosis2", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("bmdate", data != null && data.Questions != null && data.Questions.ContainsKey("GenericDigestiveLastBMDate") && data.Questions["GenericDigestiveLastBMDate"].Answer.IsNotNullOrEmpty() ? data.Questions["GenericDigestiveLastBMDate"].Answer : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            return fieldmap;
        }
    }
}