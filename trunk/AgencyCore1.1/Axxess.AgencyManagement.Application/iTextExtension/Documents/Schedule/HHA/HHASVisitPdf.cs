﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    class HHASVisitPdf : VisitNotePdf
    {
        public HHASVisitPdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.HHASVisit), 0) { }
        protected override float[] Margins(VisitNoteViewData data)
        {
            return new float[] { 120, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string, string>> FieldMap(VisitNoteViewData data)
        {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("visitdate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            fieldmap[0].Add("aide", data != null && data.Questions != null && data.Questions.ContainsKey("HealthAide") && data.Questions["HealthAide"].Answer.IsNotNullOrEmpty() && data.Questions["HealthAide"].Answer.IsGuid() ? UserEngine.GetName(data.Questions["HealthAide"].Answer.ToGuid(), Current.AgencyId) : "");
            fieldmap[0].Add("aidepresent", data != null && data.Questions != null && data.Questions.ContainsKey("AidePresent") && data.Questions["AidePresent"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["AidePresent"].Answer == "1" ? "Yes" : "") +
                (data.Questions["AidePresent"].Answer == "0" ? "No" : "")
            : "");
            fieldmap[0].Add("milage", data != null && data.AssociatedMileage !=null ? data.AssociatedMileage : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname",PrintHelper.PatientName(data.PatientProfile));
            return fieldmap;
        }
    }
}