﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Enums;
    abstract class VisitNotePdf : AxxessPdf {
        private VisitNoteXml xml;

        public VisitNotePdf(VisitNoteViewData data, int rev)
            : base()
        {
            PDFDOCSList type =default(PDFDOCSList) ;
            bool isInit = true;
            switch (data.Type)
            {
                case "MSWAssessment":
                case "MSWDischarge":
                case "MSWEvaluationAssessment":
                    type = PDFDOCSList.MSWEval;
                    break;
                case "MSWProgressNote":
                    type = PDFDOCSList.MSWProgress;
                    break;
                case "MSWVisit":
                    type = PDFDOCSList.MSWVisit;
                    break;
                case "PTDischarge":
                    type = PDFDOCSList.PTDischarge;
                    break;
                case "PTReEvaluation":
                case "PTMaintenance":
                case "PTEvaluation":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.PTEval2;
                        rev = 0;
                    }
                    else
                        type = PDFDOCSList.PTEval;
                    break;
                case "PTReassessment":
                    type = PDFDOCSList.PTReassessment;
                    break;
                case "PTAVisit":
                case "PTVisit":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.PTVisit2;
                        rev = 0;
                    }
                    else
                    {
                        type = PDFDOCSList.PTVisit;
                    }
                    break;
                case "OTReEvaluation":
                case "OTEvaluation":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.OTEval2;
                        rev = 0;
                    }
                    else
                        type = PDFDOCSList.OTEval;
                    break;
                case "OTDischarge":
                case "OTMaintenance":
                    type = PDFDOCSList.OTEval;
                    break;
                case "OTReassessment":
                    type = PDFDOCSList.OTReassessment;
                    break;
                case "OTVisit":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.OTVisit2;
                        rev = 0;
                    }
                    else
                        type = PDFDOCSList.OTVisit;
                    break;
                case "COTAVisit":
                    type = PDFDOCSList.OTVisit;
                    break;
                case "STEvaluation":
                case "STReEvaluation":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.STEval2;
                        rev = 0;
                    }
                    else
                        type = PDFDOCSList.STEval;
                    break;
                case "STMaintenance":
                    type = PDFDOCSList.STEval;
                    break;
                case "STDischarge":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.STDischarge;
                    }
                    else
                    {
                        type = PDFDOCSList.STEval;
                    }
                    break;
                case "STReassessment":
                    type = PDFDOCSList.STReassessment;
                    break;
                case "STVisit":
                    if (data.Version == 2)
                    {
                        type = PDFDOCSList.STVisit2;
                        rev = 0;
                    }
                    else
                    {
                        type = PDFDOCSList.STVisit;
                    }

                    break;
                case "DriverOrTransportationNote":
                    type = PDFDOCSList.TransportationLog;
                    break;
                default:
                    isInit = false;
                    break;
            }
            if (isInit)
            {
                this.Init(data, PdfDocs.Get(type), rev);
            }
        }
        public VisitNotePdf(VisitNoteViewData data, PdfDoc type, int rev) : base() {
            this.Init(data, type, rev);
        }
     
        private void Init(VisitNoteViewData data, PdfDoc type, int rev) {
            if (rev > 0) this.xml = new VisitNoteXml(data, type, rev);
            else this.xml = new VisitNoteXml(data, type);
            this.SetType(type);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.Content(this.xml));
            this.SetMargins(this.Margins(data));
            var fieldMap = this.FieldMap(data);
            fieldMap[0].Add("mileage", data.AssociatedMileage.IsNotNullOrEmpty() ? data.AssociatedMileage : string.Empty);
            fieldMap[0].Add("surcharge", data.Surcharge.IsNotNullOrEmpty() ? data.Surcharge : string.Empty);

            this.SetFields(fieldMap);
        }
       
        protected virtual IElement[] Content(VisitNoteXml xml) {
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            return content;
        }
       
        protected virtual List<Dictionary<String, String>> FieldMap(VisitNoteViewData data) {
            return new List<Dictionary<string, string>>();
        }
       
        protected virtual float[] Margins(VisitNoteViewData data) {
            return new float[] { 0, 0, 0, 0 };
        }
    }
}