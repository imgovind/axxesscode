﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Helpers;
    class UAPInsulinPdf : VisitNotePdf
    {
        public UAPInsulinPdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.UAPInsulin), 0) { }
        protected override float[] Margins(VisitNoteViewData data)
        {
            return new float[] { 140, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string, string>> FieldMap(VisitNoteViewData data)
        {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[0].Add("mr", data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("episode", data.StartDate.IsValid() && data.EndDate.IsValid() ? string.Format(" {0} – {1}", data.StartDate.ToShortDateString(), data.EndDate.ToShortDateString()) : string.Empty);
            fieldmap[0].Add("visitdate", data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : string.Empty);
            fieldmap[0].Add("timein", data.TimeIn.IsNotNullOrEmpty() ? data.TimeIn : string.Empty);
            fieldmap[0].Add("timeout", data.TimeIn.IsNotNullOrEmpty() ? data.TimeOut : string.Empty);
            fieldmap[0].Add("bmdate", data.Questions != null ? data.Questions.AnswerOrEmptyString("GenericDigestiveLastBMDate") : string.Empty);
            fieldmap[0].Add("footcheck", data.Questions != null && data.Questions.AnswerOrEmptyString("FootCheck").IsNotNullOrEmpty() ? "x" : string.Empty);
            fieldmap[0].Add("sign", data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("signdate", data.SignatureDate.IsNotNullOrEmpty() && data.SignatureDate.ToDateTime() != DateTime.MinValue ? data.SignatureDate : string.Empty);

            return fieldmap;
        }
    }
}