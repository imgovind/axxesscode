﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    class PASCarePlanPdf : VisitNotePdf {
        public PASCarePlanPdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.PASCarePlan), 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 160, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("visitdate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : string.Empty);
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : string.Empty);
            fieldmap[0].Add("diagnosis2", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : string.Empty);
            fieldmap[0].Add("allergies", data != null && data.Questions != null && data.Questions.ContainsKey("AllergiesDescription") && data.Questions["AllergiesDescription"].Answer.IsNotNullOrEmpty() ? data.Questions["AllergiesDescription"].Answer : string.Empty);
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("pasfreq", data != null && data.Questions != null && data.Questions.ContainsKey("PASFrequency") && data.Questions["PASFrequency"].Answer.IsNotNullOrEmpty() ? data.Questions["PASFrequency"].Answer : string.Empty);
            fieldmap[0].Add("dnr", data != null && data.Questions != null && data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["DNR"].Answer.Equals("1") ? "Yes" : string.Empty) +
                (data.Questions["DNR"].Answer.Equals("0") ? "No" : string.Empty) : string.Empty);
            fieldmap[0].Add("diet", data != null && data.Questions != null && data.Questions.ContainsKey("Diet") && data.Questions["Diet"].Answer.IsNotNullOrEmpty() ? data.Questions["Diet"].Answer : string.Empty);
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : string.Empty);
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            return fieldmap;
        }
    }
}