﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Helpers;

    class TherapySupervisoryPdf : VisitNotePdf {
        public TherapySupervisoryPdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.SupervisoryVisit), 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 120, 28.3F, 60, 28.3F };
        }

        protected override List<Dictionary<string, string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency",PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("visitdate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            fieldmap[0].Add("aide", data != null && data.Questions != null && data.Questions.ContainsKey("Therapist") && data.Questions["Therapist"].Answer.IsNotNullOrEmpty() && data.Questions["Therapist"].Answer.IsGuid()? UserEngine.GetName(data.Questions["Therapist"].Answer.ToGuid(), Current.AgencyId) : "");
            fieldmap[0].Add("aidepresent", data != null && data.Questions != null && data.Questions.ContainsKey("TherapistPresent") && data.Questions["TherapistPresent"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["TherapistPresent"].Answer == "1" ? "Yes" : "") +
                (data.Questions["TherapistPresent"].Answer == "0" ? "No" : "")
            : "");
            fieldmap[0].Add("milage", data != null && data.AssociatedMileage !=null ? data.AssociatedMileage : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname",PrintHelper.PatientName(data.PatientProfile));
            switch (data.Type)
            {
                case "PTSupervisoryVisit": fieldmap[1].Add("doctype", "PTA SUPERVISORY VISIT"); break;
                case "OTSupervisoryVisit": fieldmap[1].Add("doctype", "COTA SUPERVISORY VISIT"); break;
            }
            return fieldmap;
        }
    }
}