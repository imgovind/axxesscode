﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    class DischargeSummaryPdf : VisitNotePdf {
        public DischargeSummaryPdf(VisitNoteViewData data, PdfDoc doc) : base(data, doc, 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 155, 28.3F, 80, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;
            fieldmap[0].Add("agency",PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("visitdate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("dcdate", data != null && data.Questions != null && data.Questions.ContainsKey("DischargeDate") && data.Questions["DischargeDate"].Answer.IsNotNullOrEmpty() ? data.Questions["DischargeDate"].Answer : "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            fieldmap[0].Add("dob", data != null && data.PatientProfile != null && data.PatientProfile.DOBFormatted.IsNotNullOrEmpty() ? data.PatientProfile.DOBFormatted : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            string notificationDate = "";
            if (data != null && data.Questions != null && data.Questions.ContainsKey("NotificationDateOther") && data.Questions["NotificationDateOther"].Answer.IsNotNullOrEmpty())
                notificationDate = data.Questions["NotificationDateOther"].Answer;
            fieldmap[0].Add("dcnotice", data != null && data.Questions != null && data.Questions.ContainsKey("IsNotificationDC") && data.Questions["IsNotificationDC"].Answer.Equals("1") ? (data.Questions.ContainsKey("NotificationDate") && data.Questions["NotificationDate"].Answer.IsNotNullOrEmpty() ? (data.Questions["NotificationDate"].Answer.Equals("0") ? "Yes" : "") + (data.Questions["NotificationDate"].Answer.Equals("1") ? "Yes, 5 days" : "") + (data.Questions["NotificationDate"].Answer.Equals("2") ? "Yes, 2 days" : "") + (data.Questions["NotificationDate"].Answer.Equals("3") ? notificationDate : "") : "Yes") : "No");
            fieldmap[0].Add("dcreason", data != null && data.Questions != null && data.Questions.ContainsKey("ReasonForDC") && data.Questions["ReasonForDC"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["ReasonForDC"].Answer.Equals("1") ? "Goals Met" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("2") ? "To Nursing Home" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("3") ? "Deceased" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("4") ? "Noncompliant" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("5") ? "To Hospital" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("6") ? "Moved from Service Area" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("7") ? "Refused Care" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("8") ? "No Longer Homebound" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("9") ? "Other" : "") : "");
            fieldmap[0].Add("diagnosis", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : string.Empty);
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : string.Empty);
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[1].Add("patientname",PrintHelper.PatientName(data.PatientProfile));
            switch (data.Type)
            {
                case "PTDischargeSummary":
                    fieldmap[1].Add("doctype", "PT DISCHARGE SUMMARY");
                    break;
                case "STDischargeSummary":
                    fieldmap[1].Add("doctype", "ST DISCHARGE SUMMARY");
                    break;
            }
            return fieldmap;
        }
    }
}