﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    class SixtyDaySummaryPdf : VisitNotePdf {
        public SixtyDaySummaryPdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.SixtyDaySummary), 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
 	        return new float[] { 120, 28.3F, 125, 28.3F };
        }
        protected override List<Dictionary<string, string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            fieldmap[0].Add("dob", data != null && data.PatientProfile != null && data.PatientProfile.DOB.IsValid()? data.PatientProfile.DOB.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            //string notificationDate="";
            //if (data != null &&data.Questions!=null&& data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer == "1")
            //{
            //    if (data.Questions.ContainsKey("NotificationDate") && data.Questions["NotificationDate"].Answer.IsNotNullOrEmpty())
            //    {
            //        if (data.Questions["NotificationDate"].Answer == "1")
            //            notificationDate = "5 days";
            //        else if (data.Questions["NotificationDate"].Answer == "2")
            //            notificationDate = "2 days";
            //        else if (data.Questions["NotificationDate"].Answer == "3")
            //        {
            //            if (data.Questions.ContainsKey("NotificationDateOther") && data.Questions["NotificationDateOther"].Answer.IsNotNullOrEmpty())
            //                notificationDate = data.Questions["NotificationDateOther"].Answer;
            //        }
            //    }
            //}
            fieldmap[0].Add("dnr", data != null && data.Questions != null ? (data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer == "1" ? "Yes" : "No")  :  "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? string.Format(" {0} – {1}", data.StartDate.ToShortDateString(), data.EndDate.ToShortDateString()) : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            return fieldmap;
        }
    }
}