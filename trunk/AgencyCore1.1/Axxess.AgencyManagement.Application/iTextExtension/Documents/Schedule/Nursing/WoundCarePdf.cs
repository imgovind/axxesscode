﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Helpers;
    class WoundCarePdf : VisitNotePdf {
        public WoundCarePdf(VisitNoteViewData data) : base(data, PdfDocs.Get(PDFDOCSList.WoundCare), 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 83, 28, 25, 28 };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(location));
            return fieldmap;
        }
    }
}