﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Helpers;
    class PsychPdf : VisitNotePdf
    {
        public PsychPdf(VisitNoteViewData data, int rev) : base(data, PdfDocs.Get(PDFDOCSList.Psych), rev) { }
        protected override IElement[] Content(VisitNoteXml xml) {
            AxxessTable[] content = new AxxessTable[xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in xml.GetLayout()) {
                content[count] = new AxxessTable(section.Cols > 0 ? section.Cols : 1);
                foreach (XmlPrintSection subsection in section.Subsection) {
                    AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessContentSection contentSection = new AxxessContentSection(subsection, this.GetFonts(), true, 7.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                    content[count].AddCell(sectionCell);
                }
                count++;
            }
            return content;
        }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 147, 28.3F, 43, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("PatientName", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[0].Add("MR", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("DOB", data != null && data.PatientProfile != null && data.PatientProfile.DOBFormatted.IsNotNullOrEmpty() ? data.PatientProfile.DOBFormatted : string.Empty);
            fieldmap[0].Add("VisitDate", data != null && data.VisitStartDate.IsValid() ? data.VisitStartDate.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("TimeIn", data != null && data.TimeIn != null ? data.TimeIn : string.Empty);
            fieldmap[0].Add("TimeOut", data != null && data.TimeOut != null ? data.TimeOut : string.Empty);
            fieldmap[0].Add("EpisodeRange", data != null && data.StartDate.IsValid() && data.EndDate.IsValid() ? data.StartDate.ToShortDateString().ToZeroFilled() + "-" + data.EndDate.ToShortDateString().ToZeroFilled() : string.Empty);
            fieldmap[0].Add("PrimaryDX", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : string.Empty);
            fieldmap[0].Add("SecondaryDX", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : string.Empty);
            fieldmap[0].Add("Clinician", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("SignatureDate", data != null && data.SignatureDate.IsNotNullOrEmpty() && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : string.Empty);
            fieldmap[0].Add("physiciandate", data != null && data.Questions != null && data.Questions.ContainsKey("LastVisitDate") && data.Questions["LastVisitDate"].Answer.IsNotNullOrEmpty() ? data.Questions["LastVisitDate"].Answer : string.Empty);
            return fieldmap;
        }
    }
}