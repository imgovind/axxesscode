﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.Core.Extension;

    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.Helpers;

    public class MissedVisitPdf : AxxessPdf
    {
        public MissedVisitPdf(MissedVisit data)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.MissedVisit));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Comments.IsNotNullOrEmpty() ? data.Comments : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 335, 33, 105, 33 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;
            fieldmap[0].Add("agency",PrintHelper.AgencyAddress(location));
            fieldmap[0].Add("visittype", data != null && data.DisciplineTaskName.IsNotNullOrEmpty() ? data.DisciplineTaskName : string.Empty);
            fieldmap[0].Add("visitdate", data != null && data.EventDate.IsNotNullOrEmpty() ? data.EventDate : string.Empty);
            fieldmap[0].Add("ordergen", (data != null ? (data.IsOrderGenerated ? "Yes" : "No") : string.Empty));
            fieldmap[0].Add("physnotice", (data != null ? (data.IsPhysicianOfficeNotified ? "Yes" : "No") : string.Empty));
            fieldmap[0].Add("reason", data != null && data.Reason.IsNotNullOrEmpty() ? data.Reason : string.Empty);
            fieldmap[0].Add("clinsign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("clinsigndate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : "");
            this.SetFields(fieldmap);
        }
    }
}