﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.Helpers;

    class DrugDrugInteractionsPdf : AxxessPdf
    {
        public DrugDrugInteractionsPdf(DrugDrugInteractionsViewData data)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.DrugDrugInteraction));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 11F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (data == null || data.DrugDrugInteractions == null || data.DrugDrugInteractions.Count == 0)
            {
                AxxessCell contentcell = new AxxessCell(padding, borders);
                contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], "No interactions found during this screening.", fonts[0], 12));
                content[0].AddCell(contentcell);
            }
            else
            {
                data.DrugDrugInteractions.ForEach(d =>
                {
                    AxxessCell contentcell = new AxxessCell(padding, borders);
                    contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], d.InteractionDescription, fonts[0], 12));
                    contentcell.AddElement(new AxxessContentSection("SeverityDescription", fonts[1], d.SeverityDescription, fonts[0], 12));
                    if (d.ConsumerText.Length > 0)
                    {
                        d.ConsumerText.ForEach(ct =>
                        {
                            contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], ct, fonts[0], 10));
                        });
                    }
                    content[0].AddCell(contentcell);
                });
            }

            this.SetContent(content);
            float[] margins = new float[] { 118, 28.3F, 28.3F, 28.3F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;//.GetBranch(data.PatientProfile != null ? data.PatientProfile.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(location));
            fieldmap[0].Add("auditvendor", "Drug Screening provided by LexiComp Lexi-Data");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[0].Add("mrn", data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);

            this.SetFields(fieldmap);
        }
    }
}