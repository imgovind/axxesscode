﻿namespace Axxess.AgencyManagement.Application.iTextExtension 
{
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;

    using iTextSharp.text;

    public class MedProfilePdf : AxxessPdf
    {
        public MedProfilePdf(MedicationProfileSnapshotViewData data)
            : this(data, PdfDocs.Get(PDFDOCSList.MedProfile))
        {
        }

        public MedProfilePdf(MedicationProfileSnapshotViewData data, PdfDoc doc)
        {
            this.SetType(doc);
            var fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data != null && data.MedicationProfile != null && data.MedicationProfile.Medication.IsNotNullOrEmpty() ? data.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>(), fonts));
            this.SetMargins(new float[] { 155, 28, 120, 28 });
            var fieldmap = new List<Dictionary<string, string>> { new Dictionary<string, string>() { } };
            //var location = data.LocationProfile.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[0].Add("mr", data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("physician", data.PhysicianName.IsNotNullOrEmpty() ? data.PhysicianName : string.Empty);
            fieldmap[0].Add("episode", (data.StartDate.IsValid() ? data.StartDate.ToShortDateString() : string.Empty) + " - " + (data != null && data.EndDate.IsValid() ? data.EndDate.ToShortDateString() : string.Empty));
            fieldmap[0].Add("pharmacy", data.PharmacyName.IsNotNullOrEmpty() ? data.PharmacyName : string.Empty);
            fieldmap[0].Add("pharmphone", data.PharmacyPhone.IsNotNullOrEmpty() ? data.PharmacyPhone.ToPhone() : string.Empty);
            fieldmap[0].Add("pridiagnosis", data.PrimaryDiagnosis.IsNotNullOrEmpty() ? data.PrimaryDiagnosis : string.Empty);
            fieldmap[0].Add("secdiagnosis", data.SecondaryDiagnosis.IsNotNullOrEmpty() ? data.SecondaryDiagnosis : string.Empty);
            fieldmap[0].Add("allergies", data.Allergies.IsNotNullOrEmpty() ? data.Allergies : string.Empty);
            fieldmap[0].Add("sign", data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("signdate", data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : string.Empty);
            this.SetFields(fieldmap);
        }
      
        private AxxessTable[] BuildContent(List<Medication> medications, List<Font> fonts)
        {
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (medications.Count > 0)
            {
                AxxessTable active = new AxxessTable(new float[] { 2.85F, 11.67F, 31.91F, 12.97F, 16.86F, 2.59F, 21.15F }, true);
                AxxessTable discont = new AxxessTable(new float[] { 2.85F, 11.67F, 27.91F, 12.97F, 16.86F, 2.59F, 20.15F, 11.67F }, true);
                AxxessCell lsTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell dateTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell medTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell freqTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell routeTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell typeTitle = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell clasTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell dcTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                lsTitle.AddElement(new Chunk("LS", fonts[1]));
                dateTitle.AddElement(new Chunk("Start Date", fonts[1]));
                medTitle.AddElement(new Chunk("Medication & Dosage", fonts[1]));
                freqTitle.AddElement(new Chunk("Frequency", fonts[1]));
                routeTitle.AddElement(new Chunk("Route", fonts[1]));
                
                typeTitle.AddElement(new Chunk(string.Empty, fonts[1]));
                clasTitle.AddElement(new Chunk("Classification", fonts[1]));
                dcTitle.AddElement(new Chunk("D/C Date", fonts[1]));
                active.AddCell(lsTitle);
                active.AddCell(dateTitle);
                active.AddCell(medTitle);
                active.AddCell(freqTitle);
                active.AddCell(routeTitle);
                active.AddCell(typeTitle);
                active.AddCell(clasTitle);
                active.HeaderRows = 1;
                discont.AddCell(lsTitle);
                discont.AddCell(dateTitle);
                discont.AddCell(medTitle);
                discont.AddCell(freqTitle);
                discont.AddCell(routeTitle);
                discont.AddCell(typeTitle);
                discont.AddCell(clasTitle);
                discont.AddCell(dcTitle);
                discont.HeaderRows = 1;
                foreach (var medication in medications)
                {
                    if (medication.MedicationCategory == "Active")
                    {
                        AxxessCell ls = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell date = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell med = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell route = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell freq = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell clas = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        ls.AddElement(new AxxessCheckbox(string.Empty, medication.IsLongStanding));
                        date.AddElement(new Chunk(medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        med.AddElement(new Chunk(medication.MedicationDosage, fonts[0]));
                        freq.AddElement(new Chunk(medication.Frequency, fonts[0]));
                        route.AddElement(new Chunk(medication.Route, fonts[0]));
                        type.AddElement(new Chunk(medication.MedicationType.Value, fonts[0]));
                        clas.AddElement(new Chunk(medication.Classification, fonts[0]));
                        active.AddCell(ls);
                        active.AddCell(date);
                        active.AddCell(med);
                        active.AddCell(freq);
                        active.AddCell(route);
                        active.AddCell(type);
                        active.AddCell(clas);
                    }
                    if (medication.MedicationCategory == "DC")
                    {
                        AxxessCell ls = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell date = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell med = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell freq = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell route = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell clas = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell dcdate = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        ls.AddElement(new AxxessCheckbox(string.Empty, medication.IsLongStanding));
                        date.AddElement(new Chunk(medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        med.AddElement(new Chunk(medication.MedicationDosage, fonts[0]));
                        route.AddElement(new Chunk(medication.Route, fonts[0]));
                        freq.AddElement(new Chunk(medication.Frequency, fonts[0]));
                        type.AddElement(new Chunk(medication.MedicationType.Value, fonts[0]));
                        clas.AddElement(new Chunk(medication.Classification, fonts[0]));
                        dcdate.AddElement(new Chunk(medication.DCDate.IsValid() ? medication.DCDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        discont.AddCell(ls);
                        discont.AddCell(date);
                        discont.AddCell(med);
                        discont.AddCell(freq);
                        discont.AddCell(route);
                        discont.AddCell(type);
                        discont.AddCell(clas);
                        discont.AddCell(dcdate);
                    }
                }
                if (active.Rows.Count > 1)
                {
                    AxxessCell title = new AxxessCell(new float[] { 0, 0, 8, 0 }, new float[] { 0, 0, .5F, 0 }), chart = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
                    title.AddElement(new AxxessTitle("Active Medications", fonts[1]));
                    chart.AddElement(active);
                    content[0].AddCell(title);
                    content[0].AddCell(chart);
                }
                if (discont.Rows.Count > 1)
                {
                    AxxessCell title = new AxxessCell(new float[] { 0, 0, 8, 0 }, new float[] { 0, 0, .5F, 0 }), chart = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
                    title.AddElement(new AxxessTitle("Discontinued Medications", fonts[1]));
                    chart.AddElement(discont);
                    content[0].AddCell(title);
                    content[0].AddCell(chart);
                }
            }
            else
            {
                AxxessCell title = new AxxessCell();
                title.AddElement(new AxxessTitle("No Medications Found", fonts[1]));
                content[0].AddCell(title);
            }
            return content;
        }
    }
}