﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.Helpers;
    class TriageClassificationPdf : AxxessPdf
    {
        public TriageClassificationPdf(PatientProfile data)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.TriageClassification));
            var isDataExist = data != null && data.Patient != null;
            var patient = data != null ? data.Patient : new Patient();
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(" ") };
            this.SetContent(content);
            this.SetMargins(new float[] { 0, 0, 0, 0 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(location));
            fieldmap[0].Add("patient", isDataExist ? PrintHelper.PatientAddress(patient) : string.Empty);
            fieldmap[0].Add("patientname", isDataExist ? (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToUpper() + "\n" : "\n") : string.Empty);
            fieldmap[0].Add("mrn", isDataExist && patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("dob", isDataExist && patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("ecname", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].DisplayName.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].PrimaryPhone.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("tri1", isDataExist && patient.Triage == 1 ? "Yes" : string.Empty);
            fieldmap[0].Add("tri2", isDataExist && patient.Triage == 2 ? "Yes" : string.Empty);
            fieldmap[0].Add("tri3", isDataExist && patient.Triage == 3 ? "Yes" : string.Empty);
            fieldmap[0].Add("tri4", isDataExist && patient.Triage == 4 ? "Yes" : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}