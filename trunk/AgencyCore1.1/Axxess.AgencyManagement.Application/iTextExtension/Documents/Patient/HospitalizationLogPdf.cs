﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    using iTextSharp.text;

    using XmlParsing;
    using ViewData;

    public class HospitalizationLogPdf : AxxessPdf
    {
        private HospitalizationLogXml xml;
        public HospitalizationLogPdf(HospitalizationLog data)
        {
            this.xml = new HospitalizationLogXml(data);
            this.SetType(PdfDocs.Get(PDFDOCSList.HospitalizationLog));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout())
            {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 102, 28.8F, 70, 28.8F };
            this.SetMargins(margins);
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<string, string>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.Location));
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : string.Empty);
            fieldmap[0].Add("patientname", data != null ? data.PatientName : string.Empty);
            fieldmap[0].Add("user", data.UserName);
            this.SetFields(fieldmap);
        }
    }
}
