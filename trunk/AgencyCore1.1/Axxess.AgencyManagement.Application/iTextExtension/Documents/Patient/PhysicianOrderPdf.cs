﻿namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.Helpers;

    class PhysicianOrderPdf : AxxessPdf
    {
        public PhysicianOrderPdf(PhysicianOrder data)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.PhysicianOrder));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Text.IsNotNullOrEmpty() ? data.Text : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 225, 35, 125, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("patient", PrintHelper.PatientAddress(data.PatientProfile));
            fieldmap[0].Add("physician", (data != null && data.Physician != null ? (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1.ToTitleCase() : "") + (data.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + data.Physician.AddressLine2.ToTitleCase() + "\n" : "\n") + (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity.ToTitleCase() + ", " : "") + (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode + "\n" : "") + (data.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + data.Physician.PhoneWork.ToPhone() : "") + (data.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + data.Physician.FaxNumber.ToPhone() + "\n" : "\n") + (data.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + data.Physician.NPI : "") : ""));
            fieldmap[0].Add("orderdate", data != null && data.OrderDate.IsValid() ? data.OrderDate.ToShortDateString() : "");
            fieldmap[0].Add("ordernum", data != null && data.OrderNumber.ToString().IsNotNullOrEmpty() && data.OrderNumber.ToString().ToInteger() != 0 ? data.OrderNumber.ToString() : "");
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : string.Empty);
            fieldmap[0].Add("dob", data != null && data.PatientProfile != null && data.PatientProfile.DOB.IsValid() ? data.PatientProfile.DOB.ToShortDateString() : string.Empty);
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("allergies", data != null && data.Allergies.IsNotNullOrEmpty() ? data.Allergies : "");
            fieldmap[0].Add("summary", data != null && data.Summary.IsNotNullOrEmpty() ? data.Summary : "");
            fieldmap[0].Add("IsOrderReadAndVerified", data != null && data.IsOrderReadAndVerified ? "X" : "");
            fieldmap[0].Add("clinsign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("clinsigndate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[0].Add("physsign", data != null && data.PhysicianSignatureText.IsNotNullOrEmpty() ? data.PhysicianSignatureText : "");
            fieldmap[0].Add("physsigndate", data != null && data.PhysicianSignatureDate.IsValid() ? data.PhysicianSignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("physicianname", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}