﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    class PhysFaceToFacePdf : AxxessPdf {
        private PhysFaceToFaceXml xml;
        public PhysFaceToFacePdf(FaceToFaceEncounter data) {
            this.xml = new PhysFaceToFaceXml(data);
            this.SetType(PdfDocs.Get(PDFDOCSList.PhysFaceToFace));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans); 
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(new IElement[1] { new AxxessContentSection(this.xml.GetLayout()[0], fonts, true, 10, this.IsOasis) });
            float[] margins = new float[] { 205, 35, 70, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Location;// Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            //if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", ((Current.AgencyName.ToTitleCase() + "\n" ) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : string.Empty)
               ));
            fieldmap[0].Add("patient", PatientAddress(data));
            fieldmap[0].Add("physician", PhysicianAddress(data));
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("order", data != null && data.OrderNumber > 0 ? data.OrderNumber.ToString() : string.Empty);
            fieldmap[0].Add("dob", data != null && data.Patient != null && data.Patient.DOB.IsValid()? data.Patient.DOB.ToString("MM/dd/yyyy") : string.Empty);
            fieldmap[0].Add("socdate", data != null && data.Patient != null && data.Patient.StartofCareDate.IsValid()  ? data.Patient.StartofCareDate.ToString("MM/dd/yyyy"): string.Empty);
            fieldmap[0].Add("episode", (data != null && data.EpisodeStartDate.IsValidDate() ? data.EpisodeStartDate + "—" : string.Empty) + (data != null && data.EpisodeEndDate.IsValidDate() ? data.EpisodeEndDate : string.Empty));
            fieldmap[0].Add("physsign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("physsigndate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : string.Empty);
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[1].Add("physicianname", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }

        private static string PhysicianAddress(FaceToFaceEncounter data)
        {
            return (data != null && data.Physician != null ? (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1.ToTitleCase() : "") + (data.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + data.Physician.AddressLine2.ToTitleCase() + "\n" : "\n") + (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity.ToTitleCase() + ", " : "") + (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode + "\n" : "") + (data.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + data.Physician.PhoneWork.ToPhone() : "") + (data.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + data.Physician.FaxNumber.ToPhone() + "\n" : "\n") + (data.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + data.Physician.NPI : "") : "");
        }

        private static string PatientAddress(FaceToFaceEncounter data)
        {
            return (data != null && data.Patient != null ? (data.Patient.Address.Line1.IsNotNullOrEmpty() ? data.Patient.Address.Line1.ToTitleCase() + "\n" : "") + (data.Patient.Address.Line2.IsNotNullOrEmpty() ? data.Patient.Address.Line2.ToTitleCase() + "\n" : "") + (data.Patient.Address.City.IsNotNullOrEmpty() ? data.Patient.Address.City.ToTitleCase() + ", " : "") + (data.Patient.Address.StateCode.IsNotNullOrEmpty() ? data.Patient.Address.StateCode.ToTitleCase() + "  " : "") + (data.Patient.Address.ZipCode.IsNotNullOrEmpty() ? data.Patient.Address.ZipCode + "\n" : "") + (data.Patient.Phone.IsNotNullOrEmpty() ? data.Patient.Phone + "\n" : "") + (data.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + data.Patient.MedicareNumber : "") : "");
        }
    }
}