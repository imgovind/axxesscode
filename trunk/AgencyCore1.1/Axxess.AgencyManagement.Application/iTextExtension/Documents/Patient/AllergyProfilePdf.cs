﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.Helpers;
    class AllergyProfilePdf : AxxessPdf {
        public AllergyProfilePdf(AllergyProfileViewData data) {
            this.SetType(PdfDocs.Get(PDFDOCSList.AllergyProfile));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 3, 1 },true) };
            var allergies = data != null && data.AllergyProfile != null && data.AllergyProfile.Allergies.IsNotNullOrEmpty() ? data.AllergyProfile.Allergies.ToObject<List<Allergy>>().ToList() : new List<Allergy>();
            if (allergies.Count > 0) {
                foreach (var allergy in allergies) {
                    if (allergy != null && !allergy.IsDeprecated) {
                        AxxessCell name = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        name.AddElement(new Chunk(allergy.Name.IsNotNullOrEmpty() ? allergy.Name : string.Empty, fonts[0]));
                        type.AddElement(new Chunk(allergy.Type.IsNotNullOrEmpty() ? allergy.Type : string.Empty, fonts[0]));
                        content[0].AddCell(name);
                        content[0].AddCell(type);
                    }
                }
                this.SetContent(content);
            } else this.SetContent(new IElement[] { new Chunk("") });
            this.SetMargins(new float[] { 130, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.LocationProfile;//.GetBranch(data.PatientProfile != null ? data.PatientProfile.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency",PrintHelper.AgencyAddress(location));
            fieldmap[1].Add("patientname",PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}