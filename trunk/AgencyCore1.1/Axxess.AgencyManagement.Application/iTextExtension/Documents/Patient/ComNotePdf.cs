﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Application.Helpers;

    public class ComNotePdf : AxxessPdf
    {
        public ComNotePdf(CommunicationNote data)
        {
            this.SetType(PdfDocs.Get(PDFDOCSList.ComNote));
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Text.IsNotNullOrEmpty() ? data.Text : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 185, 35, 75, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });

            if (data.LocationProfile != null)
            {
                var location = data.LocationProfile;

                fieldmap[0].Add("agency", PrintHelper.AgencyAddress(location));
            }
            fieldmap[0].Add("patient",PrintHelper.PatientAddress(data.PatientProfile));
            fieldmap[0].Add("physician", PrintHelper.PhysicianAddressWithNPI(data.Physician));
            fieldmap[0].Add("dob", data != null && data.PatientProfile != null && data.PatientProfile.DOB.IsValid() ? data.PatientProfile.DOB.ToShortDateString() : string.Empty);
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("date", data != null && data.Created.IsValid() ? data.Created.ToShortDateString() : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("physicianname", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}