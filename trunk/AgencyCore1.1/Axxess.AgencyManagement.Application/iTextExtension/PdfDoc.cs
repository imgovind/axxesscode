﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.IO;
using Axxess.Core.Infrastructure;
    public class PdfDoc {
        private static String PdfBasePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DirectorySettings.PdfBasePath);
        private static String XmlBasePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DirectorySettings.XmlBasePath);
        private String PdfPath;
        private String XmlPath;
        //public PdfDoc(String Pdf) {
        //    this.PdfPath = Path.Combine(PdfDoc.PdfBasePath, Pdf);
        //}
        public 
            PdfDoc(PDFElement element) {
                this.PdfPath = Path.Combine(PdfDoc.PdfBasePath, element.PDF);
                if (element.IsXmlExist)
                {
                    this.XmlPath = Path.Combine(PdfDoc.XmlBasePath, element.XML);
                }
                this.DocName = element.Name;
        }
        public String GetPdfFile() {
            return this.PdfPath;
        }
        public String GetXmlFile() {
            return this.XmlPath;
        }
        public String GetXmlFile(int rev) {
            return this.XmlPath + "\\Rev" + rev.ToString() + ".xml";
        }

        public string DocName
        {
            set;
            get;
        }
    }
}
