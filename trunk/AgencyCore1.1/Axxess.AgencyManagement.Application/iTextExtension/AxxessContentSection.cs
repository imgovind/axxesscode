﻿namespace Axxess.AgencyManagement.Application.iTextExtension {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    class AxxessContentSection : AxxessTable
    {
        public AxxessContentSection()
            : base(1)
        {
            this.AddCell(new Chunk(""), new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
        }
        public AxxessContentSection(String label, Font labelFont, String answer, Font answerFont, float fontSize)
            : base(1)
        {
            this.AddCell(buildTextField(label, labelFont, answer, answerFont, fontSize, false), new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
        }
        public AxxessContentSection(String title, IElement content, Font font, float fontSize)
            : base(1)
        {
            font.Size = fontSize;
            PdfPCell header = new PdfPCell(), body = new PdfPCell();
            header.BorderWidth = body.BorderWidth = 0;
            header.BorderWidthTop = .5F;
            header.AddElement(new Paragraph(title, font));
            body.AddElement(content);
            if (title.Trim().Length > 0) this.AddCell(header);
            this.AddCell(body);
            if (title.Trim().Length > 0) this.HeaderRows = 1;
        }
        public AxxessContentSection(XmlPrintSection section, List<Font> fonts, bool Split, float fontSize, bool IsOasis)
            : base(1)
        {
            fonts[2].Size = fontSize;
            AxxessCell header = new AxxessCell(new float[] { 0, 0, 4, 0 }, new float[] { .5F, 0, 0, 0 }), body = new AxxessCell(new float[] { 0, 0, 4, 0 }, new float[] { .5F, 0, 0, 0 });
            IElement[,] content = new IElement[section.Question.Count / section.Cols + (section.Question.Count % section.Cols == 0 ? 0 : 1), section.Cols];
            int i = 0, j = 0;
            foreach (XmlPrintQuestion question in section.Question)
            {
                content[i, j] = buildQuestion(question, fonts[1], fonts[0], fontSize, IsOasis);
                if (++j == section.Cols)
                {
                    i++;
                    j = 0;
                }
            }
            body.AddElement(new AxxessContentTable(content, Split));
            if (section.Label.IsNotNullOrEmpty())
            {
                header.AddElement(new AxxessTitle(section.Label, fonts[2]));
                this.AddCell(header);
                this.HeaderRows = 1;
            }
            this.AddCell(body);
        }
        private static IElement buildQuestion(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            dataFont.Size = labelFont.Size = fontSize;
            switch (question.Type)
            {
                case "text":
                case "date":
                case "state":
                case "phone":
                    return buildTextField((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : string.Empty) + (question.Label.IsNotNullOrEmpty() ? question.Label : string.Empty), labelFont, question.Data, dataFont, fontSize, IsOasis);
                case "phrase":
                    return buildPhraseTextField(question.Label, labelFont, question.Data, dataFont, fontSize, question.Length);
                case "select":
                    return buildSelectField(question, labelFont, dataFont, fontSize, IsOasis);
                case "textarea":
                    return buildTextArea(question, labelFont, dataFont, fontSize, IsOasis);
                case "radio":
                case "checkgroup":
                    return buildCheckGroup(question, labelFont, dataFont, fontSize, IsOasis);
                case "multiple":
                    return buildMultiple(question, labelFont, dataFont, fontSize, IsOasis);
                case "chart":
                    return buildChart(question, labelFont, dataFont, fontSize, IsOasis);
                case "label":
                    return buildLabel(question, labelFont, dataFont, false, fontSize, IsOasis);
                case "title":
                    return buildLabel(question, labelFont, dataFont, true, fontSize, IsOasis);
                case "notacheck":
                case "checkbox":
                case "radioopt":
                    return new AxxessCheckbox(question.Label, question.Data.Equals(question.Value), dataFont);
                default: return new Chunk();
            }
        }

        private static IElement buildTextField(String label, Font labelFont, String data, Font dataFont, float fontSize, bool IsOasis)
        {
            AxxessTable table = new AxxessTable(label.IsNotNullOrEmpty() ? 2 : 1);
            AxxessCell labelCell = new AxxessCell(new float[] { 0, 1, 0, 1 }), dataCell = new AxxessCell(new float[] { 0, 1, 2, 1 }, new float[] { 0, 0, (data == null || data.Length == 0) ? .25F : 0, 0 });
            if (data == null || data.Length == 0) data = " ";
            labelCell.AddElement(new Chunk(label, labelFont));
            dataCell.AddElement(new Chunk(data, dataFont));
            if (label.IsNotNullOrEmpty()) table.AddCell(labelCell);
            table.AddCell(dataCell);
            return table;
        }

        private static IElement buildPhraseTextField(String label, Font labelFont, String data, Font dataFont, float fontSize, int length)
        {
            AxxessTable table = new AxxessTable(1);
            AxxessCell cell = new AxxessCell(new float[] { 0, 1, 0, -1 });
            bool isEmpty = data.IsNullOrEmpty();
            if (isEmpty) data = String.Concat(Enumerable.Repeat("\u00a0", length).ToArray());
            var dataChunk = new Chunk(data, dataFont);
            if (isEmpty) dataChunk = dataChunk.SetUnderline(.25f, -1.5f);
            var phrase = new Phrase(label + " ", labelFont);
            phrase.Add(dataChunk);
            cell.AddElement(phrase);
            table.AddCell(cell);
            return table;
        }

        private static IElement buildSelectField(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            foreach (XmlPrintOption option in question.Option)
            {
                if (question.Data.Equals(option.Value))
                {
                    if (option.Subquestion != null)
                    {
                        AxxessTable optionTable = new AxxessTable(option.Display == "inline" ? option.Subquestion.Count() + 1 : 1);
                        AxxessCell optionCell = new AxxessCell(new float[] { 0, 1, 0, 1 });
                        optionCell.AddElement(buildTextField((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont, option.Label, dataFont, fontSize, IsOasis));
                        optionTable.AddCell(optionCell);
                        foreach (XmlPrintQuestion subquestion in option.Subquestion)
                        {
                            AxxessCell subquestionCell = new AxxessCell();
                            subquestionCell.AddElement(buildQuestion(subquestion, labelFont, dataFont, fontSize, IsOasis));
                            optionTable.AddCell(subquestionCell);
                        }
                        return optionTable;
                    }
                    else return buildTextField((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont, option.Label, dataFont, fontSize, IsOasis);
                }
            }
            return buildTextField((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont, " ", dataFont, fontSize, IsOasis);
        }
        private static IElement buildTextArea(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            if (question.Label.IsNotNullOrEmpty())
            {
                AxxessTable table = new AxxessTable(1, true);
                AxxessCell label = new AxxessCell(new float[] { 0, 1, 0, 1 }), text = new AxxessCell(new float[] { 0, 1, 0, 1 });
                label.AddElement(new Chunk((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont));
                table.AddCell(label);
                text.AddElement(buildTextArea(question.Data, dataFont, question.Length, fontSize, IsOasis));
                table.AddCell(text);
                return table;
            }
            else return buildTextArea(question.Data, dataFont, question.Length, fontSize, IsOasis);
        }
        private static IElement buildTextArea(String data, Font font, int Length, float fontSize, bool IsOasis)
        {
            AxxessTable table = new AxxessTable(1, true);
            AxxessCell dataCell = new AxxessCell(new float[] { 0, 1, 2, 1 }, new float[] { 0, 0, data.Trim().Length == 0 ? .25F : 0, 0 });
            dataCell.AddElement(new Chunk(data.Length == 0 ? " " : data, font));
            table.AddCell(dataCell);
            if (data.Trim().Length == 0 && Length > 1) for (int i = 1; i < Length; i++) table.AddCell(dataCell);
            return table;
        }
        private static IElement buildCheckGroup(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            if (question.Label.IsNotNullOrEmpty())
            {
                AxxessTable table = new AxxessTable(question.Display == "block" ? 1 : 2);
                AxxessCell label = new AxxessCell(new float[] { 0, 1, 0, 1 }), options = new AxxessCell(new float[] { 0, 1, 0, 1 });
                label.AddElement(new Chunk((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont));
                table.AddCell(label);
                options.AddElement(buildOptionsField(question, labelFont, dataFont, fontSize, IsOasis));
                table.AddCell(options);
                return table;
            }
            else return buildOptionsField(question, labelFont, dataFont, fontSize, IsOasis);
        }
        private static IElement buildOptionsField(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            AxxessTable table = new AxxessTable(question.Display == "block" && question.Cols > 0 ? question.Cols : (question.Display == "inline" ? question.Option.Count : 1));
            foreach (XmlPrintOption option in question.Option)
            {
                AxxessCell optionCell = new AxxessCell(new float[] { 0, 1, 0, 1 });
                AxxessCheckbox check = new AxxessCheckbox(option.Label, question.Data.Equals(option.Value) || option.Data.Equals(option.Value) || question.Data.Split(',').Contains(option.Value) || question.Data.Split(';').Contains(option.Value), dataFont);
                optionCell.AddElement(check);
                if (option.Subquestion != null)
                {
                    AxxessTable optionTable = new AxxessTable(option.Display == "inline" ? option.Subquestion.Count() + 1 : 1);
                    optionTable.AddCell(optionCell);
                    foreach (XmlPrintQuestion subquestion in option.Subquestion)
                    {
                        AxxessCell subquestionCell = new AxxessCell();
                        subquestionCell.AddElement(buildQuestion(subquestion, labelFont, dataFont, fontSize, IsOasis));
                        optionTable.AddCell(subquestionCell);
                    }
                    AxxessCell holder = new AxxessCell(optionTable);
                    table.AddCell(holder);
                }
                else table.AddCell(optionCell);
            }
            table.CompleteRow(new AxxessCell());
            return table;
        }
        private static IElement buildMultiple(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            AxxessTable table = new AxxessTable(question.Display == "onecol" ? 1 : (question.Label.IsNotNullOrEmpty() ? 2 : 1));
            AxxessCell label = new AxxessCell(new float[] { 0, 1, 0, 1 }), fields = new AxxessCell(new float[] { 0, 1, 0, 1 });
            label.AddElement(new Chunk((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont));
            if (question.Label.IsNotNullOrEmpty()) table.AddCell(label);
            AxxessTable subquestionTable = new AxxessTable(question.Display == "inline" ? question.Subquestion.Count : 1);
            foreach (XmlPrintQuestion subquestion in question.Subquestion)
            {
                AxxessCell subitemCell = new AxxessCell(new float[] { 0, 1, 0, 1 });
                subitemCell.AddElement(buildQuestion(subquestion, labelFont, dataFont, fontSize, IsOasis));
                subquestionTable.AddCell(subitemCell);
            }
            fields.AddElement(subquestionTable);
            table.AddCell(fields);
            return table;
        }
        private static IElement buildChart(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            if (question.Label.IsNotNullOrEmpty())
            {
                AxxessTable table = new AxxessTable(question.Display == "block" ? 1 : 2);
                AxxessCell label = new AxxessCell(new float[] { 0, 1, 0, 1 }), cells = new AxxessCell(new float[] { 0, 1, 0, 1 });
                label.AddElement(new Chunk((IsOasis && question.Oasis.IsNotNullOrEmpty() ? "(" + question.Oasis + ") " : "") + question.Label, labelFont));
                table.AddCell(label);
                cells.AddElement(buildCells(question, labelFont, dataFont, fontSize, IsOasis));
                table.AddCell(cells);
                return table;
            }
            else return buildCells(question, labelFont, dataFont, fontSize, IsOasis);
        }
        private static IElement buildCells(XmlPrintQuestion question, Font labelFont, Font dataFont, float fontSize, bool IsOasis)
        {
            AxxessTable table = new AxxessTable(question.Cols);
            if (question.ColWidths.Count() == question.Cols) table.SetWidths(question.ColWidths);
            foreach (XmlPrintQuestion subquestion in question.Subquestion)
            {
                AxxessCell subitemCell = new AxxessCell(new float[] { 0, 1, 0, 1 });
                subitemCell.AddElement(buildQuestion(subquestion, labelFont, dataFont, fontSize, IsOasis));
                table.AddCell(subitemCell);
            }
            return table;
        }
        private static IElement buildLabel(XmlPrintQuestion question, Font labelFont, Font dataFont, bool center, float fontSize, bool IsOasis)
        {
            if (question.Instructions.IsNotNullOrEmpty())
            {
                AxxessTable table = new AxxessTable(1);
                AxxessCell cell = new AxxessCell();
                dataFont.Size = fontSize;
                Paragraph label = new Paragraph(question.Label, labelFont), instructions = new Paragraph(question.Instructions, dataFont);
                if (center)
                {
                    label.SetAlignment("center");
                    instructions.SetAlignment("center");
                }
                else if (question.Align.IsNotNullOrEmpty())
                {
                    label.SetAlignment(question.Align);
                    instructions.SetAlignment(question.Align);
                }
                cell.AddElement(label);
                cell.AddElement(instructions);
                table.AddCell(cell);
                return table;
            }
            else
            {
                Paragraph label = new Paragraph(question.Label, labelFont), instructions = new Paragraph(question.Instructions, dataFont);
                if (center) label.SetAlignment("center");
                else if (question.Align.IsNotNullOrEmpty()) label.SetAlignment(question.Align);
                return label;
            }
        }
    }
}