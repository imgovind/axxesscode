﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.iTextExtension
{
    using System.Web;

    using iTextSharp.text;
    using iTextSharp.text.html.simpleparser;

    public class CustomImageProvider : IImageProvider
    {
        public Image GetImage(string src, Dictionary<string, string> imageProperties, ChainedProperties cprops, IDocListener doc)
        {
            string defaultImage = HttpContext.Current.Server.MapPath("../Images/broken-image.png");
            string imageLocation = imageProperties["src"];
            string siteUrl = HttpContext.Current.Server.MapPath(""); //HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "");
	            if (siteUrl.EndsWith("/"))
	                siteUrl = siteUrl.Substring(0, siteUrl.LastIndexOf("/"));
	            iTextSharp.text.Image image = null;
            if (!imageLocation.StartsWith("http:") && !imageLocation.StartsWith("file:")
	                 && !imageLocation.StartsWith("https:") && !imageLocation.StartsWith("ftp:"))
	                imageLocation = siteUrl + (imageLocation.StartsWith("/") ? "" : "/") + imageLocation;
            try
            {
                image = iTextSharp.text.Image.GetInstance(imageLocation);
            }
            catch (Exception e)
            {
                image = iTextSharp.text.Image.GetInstance(defaultImage);
            }
            return image;
        }
    }
}
