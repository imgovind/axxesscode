﻿namespace Axxess.AgencyManagement.Application.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;

    public static class ScheduleEventExtensions
    {
        //public static IDictionary<string, Question> ToOASISDictionary(this ScheduleEvent scheduleEvent)
        //{
        //    IDictionary<string, Question> questions = new Dictionary<string, Question>();
        //    if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
        //    {
        //        var key = string.Empty;
        //        var oasisQuestions = scheduleEvent.Note.ToObject<List<Question>>();
        //        if (oasisQuestions != null && oasisQuestions.Count > 0)
        //        {
        //            oasisQuestions.ForEach(question =>
        //            {
        //                if (question.Type == QuestionType.Moo)
        //                {
        //                    key = string.Format("{0}{1}", question.Code, question.Name);
        //                    if (!questions.ContainsKey(key))
        //                    {
        //                        questions.Add(key, question);
        //                    }
        //                }
        //                else if (question.Type == QuestionType.PlanofCare)
        //                {
        //                    key = string.Format("485{0}", question.Name);
        //                    if (!questions.ContainsKey(key))
        //                    {
        //                        questions.Add(key, question);
        //                    }
        //                }
        //                else if (question.Type == QuestionType.Generic)
        //                {
        //                    key = string.Format("Generic{0}", question.Name);
        //                    if (!questions.ContainsKey(key))
        //                    {
        //                        questions.Add(key, question);
        //                    }
        //                }
        //                else
        //                {
        //                    key = string.Format("{0}", question.Name);
        //                    if (!questions.ContainsKey(key))
        //                    {
        //                        questions.Add(key, question);
        //                    }
        //                }
        //            });
        //        }
        //    }
        //    return questions;
        //}

        //public static IDictionary<string, Question> Diagnosis(this ScheduleEvent scheduleEvent)
        //{
        //    var diagnosis = new Dictionary<string, Question>();
        //    if (scheduleEvent != null)
        //    {
        //        var questions = scheduleEvent.ToOASISDictionary();
        //        diagnosis.AddIfNotEmpty(questions, "M1020PrimaryDiagnosis");
        //        diagnosis.AddIfNotEmpty(questions, "M1020ICD9M");
        //        diagnosis.AddIfNotEmpty(questions, "M1022PrimaryDiagnosis1");
        //        diagnosis.AddIfNotEmpty(questions, "M1022ICD9M1");
        //        diagnosis.AddIfNotEmpty(questions, "M1022PrimaryDiagnosis2");
        //        diagnosis.AddIfNotEmpty(questions, "M1022ICD9M2");
        //        diagnosis.AddIfNotEmpty(questions, "M1022PrimaryDiagnosis3");
        //        diagnosis.AddIfNotEmpty(questions, "M1022ICD9M3");
        //        diagnosis.AddIfNotEmpty(questions, "M1022PrimaryDiagnosis4");
        //        diagnosis.AddIfNotEmpty(questions, "M1022ICD9M4");
        //        diagnosis.AddIfNotEmpty(questions, "M1022PrimaryDiagnosis5");
        //        diagnosis.AddIfNotEmpty(questions, "M1022ICD9M5");
        //    }
        //    return diagnosis;
        //}
    }
}