﻿namespace Axxess.AgencyManagement.Application.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    

    public static class SignableNoteExtensions
    {
        public static ValidationResults SignValidation(this SignableNoteBase note, bool isSignatureCorrect)
        {
            return note.SignValidation(isSignatureCorrect, false);
        }

        public static ValidationResults SignValidation(this SignableNoteBase note, bool isSignatureCorrect, bool ignoreStatus)
        {
            ValidationResults results = new ValidationResults() { IsSuccessful = true };
            if (ScheduleStatusFactory.NoteSignedStatus().Contains(note.Status) || ignoreStatus)
            {
                var rules = new List<Validation>();
                rules.Add(new Validation(() => string.IsNullOrEmpty(note.SignatureText), "User Signature can't be empty."));
                rules.Add(new Validation(() => note.SignatureText.IsNotNullOrEmpty() ? !isSignatureCorrect : false, "User Signature is not correct."));
                rules.Add(new Validation(() => !(note.SignatureDate > DateTime.MinValue), "Signature date is not valid."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    results.IsSigned = true;
                }
                else
                {
                    results.ValidationMessage = entityValidator.Message;
                    results.IsSuccessful = false;
                }
            }
            return results;
        }
    }
}
