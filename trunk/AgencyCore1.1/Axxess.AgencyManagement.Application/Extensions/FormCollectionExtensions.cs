﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Entities;
using System.Web.Mvc;

namespace Axxess.AgencyManagement.Application.Extensions
{
    using Axxess.AgencyManagement.Entities.Common;

    public static class FormCollectionExtensions
    {
        public static List<NotesQuestion> ProcessNoteQuestions(this FormCollection formCollection)
        {
            formCollection.Remove("Type");
            var questions = new Dictionary<string, NotesQuestion>();
            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray != null && nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(",") });
                    }
                }
            }
            return questions.Values.ToList();
        }

        public static List<Question> ProcessPOCQuestions(this FormCollection formCollection, IDictionary<string, Question> questionDictionary, bool isStandAlone)
        {
            formCollection.Remove("Id");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("PatientId");
            formCollection.Remove("Status");
            formCollection.Remove("SignatureText");
            formCollection.Remove("SignatureDate");
            formCollection.Remove("PhysicianId_text");
            formCollection.Remove("PhysicianId");


            //planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
            //IDictionary<string, Question> questions = planofCare.ToDictionary();
            //questions = RemovePrimaryDiagnosis(questionDictionary);

            foreach (var key in formCollection.AllKeys)
            {
                string keyName = key;
                string[] nameArray = key.Split('_');
                if (nameArray != null && nameArray.Length > 2)
                {
                    nameArray.Reverse();
                    keyName = nameArray[0];
                }

                string nonGenericKey = isStandAlone ? keyName.Replace("Generic", "") : keyName;
                string answer = formCollection.GetValues(key).Join(",");
                if (questionDictionary.ContainsKey(keyName))
                {
                    questionDictionary[key].Answer = answer;
                }
                else if (isStandAlone && questionDictionary.ContainsKey(nonGenericKey))
                {
                    questionDictionary[nonGenericKey].Answer = answer;
                }
                else
                {
                    questionDictionary.Add(keyName, QuestionFactory<Question>.Create(keyName, answer));
                }
            }
            return questionDictionary.Values.ToList();
        }

        public static string ToUb04Locator81ccaXml(this FormCollection formCollection, string[] keys)
        {
            var ub04Locator81ccaXml = string.Empty;
            if (formCollection != null && keys != null && keys.Length > 0)
            {
                if (keys.Contains("Ub04Locator81cca"))
                {
                    var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                    var locators = new List<Locator>();
                    if (locatorList != null && locatorList.Length > 0)
                    {
                        locatorList.ForEach(l =>
                        {
                            if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                            {
                                locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                            }
                        });
                    }
                    ub04Locator81ccaXml = locators.ToXml();
                }
            }
            return ub04Locator81ccaXml;
        }

        public static string ToUb04Locator81Xml(this FormCollection formCollection, string[] keys, string nameOfField)
        {
            var ub04Locator81ccaXml = string.Empty;
            if (formCollection != null && keys != null && keys.Length > 0)
            {
                if (keys.Contains(nameOfField))
                {
                    var locatorList = formCollection[nameOfField].ToArray();
                    var locators = new List<Locator>();
                    if (locatorList != null && locatorList.Length > 0)
                    {
                        locatorList.ForEach(l =>
                        {
                            var isValueExist = false;
                            var locator = new Locator();
                            if (keys.Contains(l + "_Code1"))
                            {
                                locator.Code1 = formCollection[l + "_Code1"];
                                isValueExist = true;
                            }
                            if (keys.Contains(l + "_Code2"))
                            {
                                locator.Code2 = formCollection[l + "_Code2"];
                                isValueExist = true;
                            }
                            if (keys.Contains(l + "_Code3"))
                            {
                                locator.Code3 = formCollection[l + "_Code3"];
                                isValueExist = true;
                            }
                            if (isValueExist)
                            {
                                locators.Add(locator);
                            }
                        });
                    }
                    ub04Locator81ccaXml = locators.ToXml();
                }
            }
            return ub04Locator81ccaXml;
        }
    }
}
