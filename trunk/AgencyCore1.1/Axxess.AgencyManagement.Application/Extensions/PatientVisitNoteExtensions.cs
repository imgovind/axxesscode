﻿namespace Axxess.AgencyManagement.Application.Extensions
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    public static class PatientVisitNoteExtensions
    {
        public static IDictionary<string, NotesQuestion> ToDictionary(this IVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.Questions.IsNotNullOrEmpty())
            {
                patientVisitNote.Questions.ForEach(n =>
                {
                    if (n.Name.IsNotNullOrEmpty() && !questions.ContainsKey(n.Name))
                    {
                        questions.Add(n.Name, n);
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToWoundCareDictionary(this IVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            try
            {
                if (patientVisitNote != null && patientVisitNote.WoundQuestions.IsNotNullOrEmpty())
                {
                    patientVisitNote.WoundQuestions.ForEach(n =>
                    {
                        if (n.Name.IsNotNullOrEmpty())
                        {
                            questions[n.Name] = n;
                        }
                    });
                }
            }
            catch (Exception)
            {
                return questions;
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToHHADefaults(this IVisitNote patientVisitNote)
        {
            var vitalSigns = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null)
            {
                var questions = patientVisitNote.ToDictionary();

                var names = new string[] {
                      
                      "PrimaryDiagnosis"
                    , "ICD9M"
                    , "PrimaryDiagnosis1"
                    , "ICD9M1"
                    //, "DNR"
                    , "IsVitalSignParameter"
                    , "SystolicBPGreaterThan"
                    , "DiastolicBPGreaterThan"
                    , "PulseGreaterThan"
                    , "RespirationGreaterThan"
                    , "TempGreaterThan"
                    , "WeightGreaterThan"
                    , "SystolicBPLessThan"
                    , "DiastolicBPLessThan"
                    , "PulseLessThan"
                    , "RespirationLessThan"
                    , "TempLessThan"
                    , "WeightLessThan" 
                    
                    , "IsDiet"
                    , "Diet"
                    , "Allergies"
                    , "AllergiesDescription"
                    , "HHAFrequency"};

                names.ForEach(name =>
                {
                    if (questions.ContainsKey(name) && questions[name] != null)
                    {
                        vitalSigns.Add(name, questions[name]);
                    }
                });
            }
            return vitalSigns;
        }

        public static IDictionary<string, NotesQuestion> ToPASDefaults(this IVisitNote patientVisitNote)
        {
            var vitalSigns = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null)
            {
                var questions = patientVisitNote.ToDictionary();

                var names = new string[] { 
                      "PrimaryDiagnosis"
                    , "ICD9M"
                    , "PrimaryDiagnosis1"
                    , "ICD9M1"
                   // , "DNR"
                    , "IsVitalSignParameter"
                    , "SystolicBPGreaterThan"
                    , "DiastolicBPGreaterThan"
                    , "PulseGreaterThan"
                    , "RespirationGreaterThan"
                    , "TempGreaterThan"
                    , "WeightGreaterThan"
                    , "SystolicBPLessThan"
                    , "DiastolicBPLessThan"
                    , "PulseLessThan"
                    , "RespirationLessThan"
                    , "TempLessThan"
                    , "WeightLessThan"
                    , "PASFrequency"};

                names.ForEach(name =>
                {
                    if (questions.ContainsKey(name) && questions[name] != null)
                    {
                        vitalSigns.Add(name, questions[name]);
                    }
                });
            }
            return vitalSigns;
        }
    }
}
