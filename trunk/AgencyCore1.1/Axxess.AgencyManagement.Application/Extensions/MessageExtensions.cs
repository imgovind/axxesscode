﻿namespace Axxess.AgencyManagement.Application.Extensions
{
    using System;
    using Security;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class MessageExtensions
    {
        public static string ReplaceTokens(this string body)
        {
            var text = body;
            if (body.IsNotNullOrEmpty())
            {
                text = body.Replace("[customer]", Current.DisplayName);
            }
            return text;
        }
    }
}

