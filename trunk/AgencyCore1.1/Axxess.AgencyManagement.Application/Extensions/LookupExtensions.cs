﻿namespace Axxess.AgencyManagement.Application.Extensions
{
    using System;

    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public static class LookupExtensions
    {
        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        public static string ToSourceName(this int admissionSourceId)
        {
            if (admissionSourceId > 0)
            {
                var source = lookupRepository.GetAdmissionSource(admissionSourceId);
                if (source != null)
                {
                    return source.Description;
                }
            }
            return string.Empty;
        }
    }
}
