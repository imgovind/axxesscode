﻿

namespace Axxess.AgencyManagement.WebSite.Areas.Therapy
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class TherapyAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Therapy";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AgencyManagementCenter",
                "Therapy/Agency/Center/{therapyId}",
                new { controller = "Agency", action = "Center", therapyId = new IsGuid()},
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
            );

            context.MapRoute(
                "AgencyData",
                "Therapy/Agency/{action}/{agencyId}",
                new { controller = "Agency", action = "AgencyData", agencyId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
            );

            context.MapRoute(
                "AcceptPatient",
                "Therapy/Agency/AcceptPatient/{id}/{result}",
                new { controller = "Agency", action = "AcceptPatient", id = new IsGuid(), result=0 },
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
           );

            context.MapRoute(
                "AgencyReferralsGrid",
                "Therapy/Agency/AgencyReferralsGrid/{id}/{status}",
                new { controller = "Agency", action = "AgencyReferralsGrid", id = new IsGuid(), status = 0 },
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
           );

            context.MapRoute(
                "AgencyEmployors",
                "Therapy/Agency/AgencyEmployors/{state}/{status}",
                new { controller = "Agency", action = "AgencyEmployors", state = "", status = 0 },
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
           );

            context.MapRoute(
                "ReadNotice",
                "Therapy/Agency/ReadNotice/{id}",
                new { controller = "Agency", action = "ReadNotice",  id = 0 },
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
           );

            context.MapRoute(
                "ReferralInfo",
                "Therapy/Agency/ReferralInfo/{id}",
                new { controller = "Agency", action = "ReferralInfo", id = 0 },
                new string[] { "Axxess.AgencyManagement.Application.Areas.Therapy.Controllers" }
           );
        }
    }
}
