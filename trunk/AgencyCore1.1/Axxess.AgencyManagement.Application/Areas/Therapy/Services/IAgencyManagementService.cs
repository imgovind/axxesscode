﻿
namespace Axxess.AgencyManagement.Application.Areas.Therapy.Service
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;

    using Enums;
    using Domain;
    using ViewData;
    using Axxess.Core.Infrastructure;
    using Axxess.TherapyManagement.Domain;

    public interface IAgencyManagementService
    {
        bool AcceptOrRejectPatient(Guid id, int result);
        AgencyManagementCenterViewData AgencyManagementCenter(Guid therapyId);
        AgencyManagementCenterViewData GetAgencyInfoAndMessages(Guid therapyId, Guid agencyId);
        Notice ReadNotice(int id);
    }
}
