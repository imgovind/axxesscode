﻿

//namespace Axxess.AgencyManagement.Application.Areas.Therapy.Service.Implementation
//{
//    using System;
//    using System.Web.Mvc;
//    using System.Linq;
//    using System.Collections.Generic;

//    using Axxess.AgencyManagement.Entities;

//    using Enums;
//    using Domain;
//    using ViewData;
//    using Axxess.Core.Infrastructure;
//    using Axxess.Core.Extension;
//    using Axxess.TherapyManagement.Repositories;
//    using Axxess.TherapyManagement.Domain;
//    using Axxess.Membership.Repositories;
//    using Axxess.AgencyManagement.Entities.Repositories;
//    using Axxess.TherapyManagement.Enums;

//    public class AgencyManagementService : IAgencyManagementService
//    {
//        #region Private Members
//        private readonly IAgencyReferralRepository agencyReferralRepository;
//        private readonly IApplicationRepository applicationRepository;
//        private readonly IAgencyRepository agencyRepository;
//        private readonly IAgencyCenterRepository agencyCenterRepository;
//        #endregion
        
//        #region Constructor
//        public AgencyManagementService(ITherapyManagementDataProvider therapyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider)
//        {
//            this.agencyReferralRepository = therapyManagementDataProvider.AgencyReferralRepository;
//            this.applicationRepository = membershipDataProvider.ApplicationRepository;
//            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
//            this.agencyCenterRepository = therapyManagementDataProvider.AgencyCenterRepository;
//        }
//        #endregion

//        public bool AcceptOrRejectPatient(Guid id, int result)
//        {
//            AgencyReferralPatient patient = agencyReferralRepository.GetReferralPatientById(id);
//            patient.Status = result;
//            if (agencyReferralRepository.UpdateReferralPatient(patient))
//            {
//                return true;
//            }
//            return false;
//        }

//        public AgencyManagementCenterViewData AgencyManagementCenter( Guid therapyId)
//        {
//            var viewData = new AgencyManagementCenterViewData();
//            viewData.TherapyEmployers = applicationRepository.GetTherapyEmployorsByTherapyId(therapyId);
//            if(viewData.TherapyEmployers!=null){
//                viewData.SelectedAgencyId = viewData.TherapyEmployers.FirstOrDefault().AgencyId;
//                viewData.Agency = agencyRepository.GetAgencyWithMainLocation(viewData.SelectedAgencyId);
//                var ebsMessages = agencyCenterRepository.GetMessagesByStatus(therapyId, viewData.SelectedAgencyId, (int)EbsMessageStatus.unopened);
//                if (ebsMessages != null)
//                {
//                    foreach (EbsMessages ebs in ebsMessages)
//                    {
//                        SetUrl(ebs);
//                    }
//                }
//                viewData.EbsMessages = ebsMessages;
//            }
            
//            return viewData;
//        }

//        public AgencyManagementCenterViewData GetAgencyInfoAndMessages(Guid therapyId, Guid agencyId)
//        {
//            var viewData = new AgencyManagementCenterViewData();
//            viewData.Agency = agencyRepository.GetAgencyWithMainLocation(agencyId);
//            var ebsMessages = agencyCenterRepository.GetMessagesByStatus(therapyId, agencyId, (int)EbsMessageStatus.unopened);
//            if (ebsMessages != null)
//            {
//                foreach (EbsMessages ebs in ebsMessages)
//                {
//                    SetUrl(ebs);
//                }
//            }
//            viewData.EbsMessages = ebsMessages;
//            return viewData;
//        }

//        public Notice ReadNotice(int id)
//        {
//            EbsMessages ebs = agencyCenterRepository.GetMessagesById(id);
//            Notice notice = ebs.Message.ToObject<Notice>();
//            agencyCenterRepository.AddNotice(notice);
//            ebs.Status = (int)EbsMessageStatus.opened;
//            agencyCenterRepository.UpdateMessage(ebs);
//            return notice;
//        }

//        private void SetUrl(EbsMessages ebs)
//        {
           
//                switch (ebs.Type)
//                {
//                    case (int)EbsMessageType.Referral:
//                        ebs.Url = string.Format("Agency.OpenReferralPatientInfo('{0}')", ebs.Id);
//                        break;
//                    case (int)EbsMessageType.Message:
//                        ebs.Url = string.Format("Agency.OpenMsgInfo('{0}')", ebs.Id);
//                        break;
//                    case (int)EbsMessageType.Contract:
//                        ebs.Url = string.Format("Agency.OpenContractInfo('{0}')", ebs.Id);
//                        break;
//                }
            
//        }
//    }
//}
