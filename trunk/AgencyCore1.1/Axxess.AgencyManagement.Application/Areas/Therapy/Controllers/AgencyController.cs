﻿
//namespace Axxess.AgencyManagement.Application.Areas.Therapy.Controllers
//{
//    using System;
//    using System.Text;
//    using System.Linq;
//    using System.Collections.Generic;
//    using System.Web.Mvc;
//    using Axxess.Membership.Repositories;
//    using System.Web.Script.Serialization;
//    using Axxess.Core;
//    using Axxess.Core.Infrastructure;
//    using Axxess.Core.Extension;
//    using Axxess.Membership.Domain;
//    using Axxess.AgencyManagement.Entities;
//    using Axxess.AgencyManagement.Entities.Repositories;
//    using Axxess.AgencyManagement.Application.ViewData;
//    using Axxess.TherapyManagement.Repositories;
//    using Axxess.AgencyManagement.Application.Areas.Therapy.Service;
//    using Telerik.Web.Mvc;
//    using Axxess.TherapyManagement.Domain;

//    [Compress]
//    [AxxessAuthorize]
//    [HandleError]
//    [SslRedirect]
//    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
//    public class AgencyController : BaseController
//    {
//        #region Constructor

//        private readonly IApplicationRepository applicationRepository;
//        private readonly IAgencyRepository agencyRepository;
//        private readonly IAgencyReferralRepository agencyReferralRepository;
//        private readonly IAgencyManagementService agencyManagementService;
//        private readonly IAgencyCenterRepository agencyCenterRepository;

//        public AgencyController(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, ITherapyManagementDataProvider therapyManagementDataProvider, IAgencyManagementService agencyManagementService)
//        {
//            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
//            this.applicationRepository = membershipDataProvider.ApplicationRepository;
//            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
//            this.agencyReferralRepository = therapyManagementDataProvider.AgencyReferralRepository;
//            this.agencyManagementService = agencyManagementService;
//            this.agencyCenterRepository = therapyManagementDataProvider.AgencyCenterRepository;
//        }
//        #endregion

//        [AcceptVerbs(HttpVerbs.Get)]
//        public ActionResult Center(Guid therapyId)
//        {
//            if (therapyId == Guid.Empty)
//            {
//                therapyId = Current.AgencyId;
//            }
//            var viewData = agencyManagementService.AgencyManagementCenter(therapyId);
//            return PartialView("~/Areas/Therapy/Views/Management/Center.ascx",viewData);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult AgencyEmployors(string state, bool status)
//        {
//            var agencyList = new List<TherapyEmployors>();
//            agencyList = applicationRepository.GetTherapyEmployorsByStateAndStatus(state, status,Current.AgencyId);
//            return Json(agencyList.OrderBy(t => t.AgencyName));
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult AgencyData(Guid agencyId)
//        {
//            Guid therapyId=Current.AgencyId;
//            var viewData = agencyManagementService.GetAgencyInfoAndMessages(therapyId, agencyId);
//            return PartialView("~/Areas/Therapy/Views/Management/Data.ascx", viewData);
//        }


//        [AcceptVerbs(HttpVerbs.Post)]
//        public JsonResult AcceptPatient(Guid id, int status)
//        {
//            Check.Argument.IsNotEmpty(id, "id");
//            Check.Argument.IsNotNull(status, "status");
//            var viewData = new JsonViewData { isSuccessful = false };
//            //if (agencyManagementService.AcceptOrRejectPatient(id, status))
//            //{
//            //    viewData.isSuccessful = true;
//            //    viewData.errorMessage = "Update Successfully!";
//            //}
//            return Json(viewData);
//        }

//        [GridAction]
//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult AgencyReferralsGrid(Guid agencyId, int type, int status)
//        {
//            var result = agencyCenterRepository.GetMessagesByTypeAndStatus(Current.AgencyId, agencyId, type, status);
//            return View(new GridModel(result));
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult ReadNotice(int id)
//        {
//            return PartialView("~/Areas/Therapy/Views/Management/Notice/NoticeInfo.ascx", agencyManagementService.ReadNotice(id));
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult ReferralInfo(int id)
//        {
//            var referral = new AgencyReferralPatient();
//            return PartialView("~/Areas/Therapy/Views/Management/ReferralPatient.ascx", referral);
//        }
//    }
//}
