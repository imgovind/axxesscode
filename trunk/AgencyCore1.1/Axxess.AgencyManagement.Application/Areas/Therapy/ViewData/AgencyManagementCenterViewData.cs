﻿

namespace Axxess.AgencyManagement.Application
{

    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;
    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.TherapyManagement.Domain;

    public class AgencyManagementCenterViewData : JsonViewData
    {
        public List<TherapyEmployors> TherapyEmployers { get; set; }
        public int AgencyStatus { get; set; }
        public Guid SelectedAgencyId { get; set; }
        public Agency Agency { get; set; }
        public List<EbsMessages> EbsMessages { get; set; }
    }
}
