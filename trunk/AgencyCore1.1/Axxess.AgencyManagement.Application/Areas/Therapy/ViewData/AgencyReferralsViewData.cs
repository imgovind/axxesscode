﻿
namespace Axxess.AgencyManagement.Application.ViewData
{

    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;
    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Entities;

    public class AgencyReferralsViewData : JsonViewData
    {
        public AgencyLocation AgencyLocation { get; set; }
    }
}
