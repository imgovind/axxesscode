﻿

namespace Axxess.AgencyManagement.Application.Areas.Therapy.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;
    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Entities;

    public class PatientReferralViewData
    {
        public Guid PatientId { get; set; }
    }
}
