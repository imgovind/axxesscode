﻿using System.Web.Mvc;
using Axxess.Core.Infrastructure;

namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty
{
    public class PrivateDutyAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PrivateDuty";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PDAllergyProfilePrint",
                "PrivateDuty/AllergyProfile/Print/{id}",
                new { area = this.AreaName, controller = "AllergyProfile", action = "Print", id = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PDMedicationProfilePrint",
                "PrivateDuty/MedicationProfile/Print/{patientId}",
                new { area = this.AreaName, controller = "PatientProfile", action = "MedicationProfilePrint", patientId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PDMedicationProfile",
                "PrivateDuty/MedicationProfile/{patientId}",
                new { area = this.AreaName, controller = "PatientProfile", action = "MedicationProfile", patientId = new IsGuid() },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PrivateDutyNewAuthorization",
                "PrivateDuty/Authorization/New",
                new { area = this.AreaName, controller = "PatientProfile", action = "NewAuthorization", id = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PrivateDutyScheduleCarePeriodNewContent",
                "PrivateDuty/CarePeriod/New/Content/{patientId}",
                new { area = this.AreaName, controller = "CarePeriod", action = "NewCarePeriodContent", patientId = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PrivateDutyScheduleTask",
                "PrivateDuty/Task/{action}/{id}",
                new { area = this.AreaName, controller = "Schedule", action = "New", id = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );

            context.MapRoute("PrivateDutyDefaultPrintPreview", "PrivateDuty/{controller}/PrintPreview/{patientId}/{eventId}", new { area = this.AreaName, controller = "Note", action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() }, null, new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });
            context.MapRoute("PrivateDutyDefaultQAPrintPreview", "PrivateDuty/{controller}/QAPreview/{patientId}/{eventId}", new { area = this.AreaName, controller = "Note", action = "QAPreview", patientId = new IsGuid(), eventId = new IsGuid() }, null, new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });
            context.MapRoute("PrivateDutyDefaultPrint", "PrivateDuty/{controller}/Pdf/{patientId}/{eventId}", new { area = this.AreaName, controller = "Note", action = "Pdf", patientId = new IsGuid(), eventId = new IsGuid() }, null, new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });
            
            context.MapRoute(
                "PrivateDutyOasisDefault",
                "PrivateDuty/Oasis/{action}/{Id}/{patientId}/{episodeId}/{assessmentType}",
                new { area = this.AreaName, controller = "Oasis", action = "Validate", Id = new IsGuid(), patientId = new IsGuid(), episodeId = new IsGuid(), assessmentType = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PrivateDutyScheduleDefault",
                "PrivateDuty/Schedule/{action}/{id}",
                new { area = this.AreaName, controller = "Schedule", action = "Center", id = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PrivateDutyDefault",
                "PrivateDuty/{controller}/{action}",
                new { area = this.AreaName, controller = "Home", action = "Index" },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            context.MapRoute(
                "PrivateDutyProfileDefault",
                "PrivateDuty/{controller}/{action}/{id}",
                new { area = this.AreaName, controller = "PatientProfile", action = "Profile", id = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
           );

            context.MapRoute(
             "PrivateDutyReportDefault",
             "PrivateDuty/Report/{action}",
             new { area = this.AreaName, controller = "Report", action = "Error" },
             new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
         );
        }
    }
}
