﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.WebSite.Areas.PrivateDuty;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;

    public class ScheduleModule : PrivateDutyAreaRegistration
    {
        public string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterArea(System.Web.Mvc.AreaRegistrationContext context)
        {
            #region Infection Log

            context.MapRoute(
               AreaName + "NewInfection",
               AreaName + "/Infection/New",
               new { controller = this.Name, action = "NewInfection", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
               AreaName + "AddInfection",
               AreaName + "/Infection/Add",
               new { controller = this.Name, action = "AddInfection", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "EditInfection",
                AreaName + "/Infection/Edit",
                new { controller = this.Name, action = "EditInfection", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "UpdateInfection",
                AreaName + "/Infection/Update",
                new { controller = this.Name, action = "UpdateInfection", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "InfectionReportPrint",
                AreaName + "/Infection/View/{patientId}/{eventId}",
                new { controller = this.Name, action = "InfectionReportPrint", patientId = new IsGuid(), eventId = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "ProcessInfection",
                AreaName + "/Infection/Process",
                new { controller = this.Name, action = "ProcessInfection", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "InfectionDelete",
                AreaName + "/Infection/Delete",
                new { controller = this.Name, action = "DeleteInfection", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
               AreaName + "InfectionGrid",
               AreaName + "/Infection/Grid",
               new { controller = this.Name, action = "InfectionGrid", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
              AreaName + "InfectionList",
              AreaName + "/Infection/List",
              new { controller = this.Name, action = "InfectionList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            #endregion

            #region Incident Log

            context.MapRoute(
               AreaName + "NewIncident",
               AreaName + "/Incident/New",
               new { controller = this.Name, action = "NewIncident", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
               AreaName + "AddIncident",
               AreaName + "/Incident/Add",
               new { controller = this.Name, action = "AddIncident", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "EditIncident",
                AreaName + "/Incident/Edit",
                new { controller = this.Name, action = "EditIncident", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "UpdateIncident",
                AreaName + "/Incident/Update",
                new { controller = this.Name, action = "UpdateIncident", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "DeleteIncident",
                AreaName + "/Incident/Delete",
                new { controller = this.Name, action = "DeleteIncident", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "ProcessIncident",
                AreaName + "/Incident/Process",
                new { controller = this.Name, action = "ProcessIncident", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
                AreaName + "IncidentReportPrint",
                AreaName + "/Incident/View/{patientId}/{eventId}",
                new { controller = this.Name, action = "IncidentReportPrint", patientId = new IsGuid(), eventId = new IsGuid() },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
               AreaName + "IncidentGrid",
               AreaName + "/Incident/Grid",
               new { controller = this.Name, action = "IncidentGrid", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            context.MapRoute(
              AreaName + "IncidentList",
              AreaName + "/Incident/List",
              new { controller = "Schedule", action = "IncidentList", id = UrlParameter.Optional },
            new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" });

            #endregion

            context.MapRoute(
              "PrivateDutyScheduleTaskPreview",
              "PrivateDuty/Task/Preview/{episodeId}/{patientId}/{eventId}",
              new { controller = "Schedule", action = "NotePrintPreview", episodeId = UrlParameter.Optional, patientId = UrlParameter.Optional, eventId = UrlParameter.Optional },
              new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
           );

            context.MapRoute(
              "PrivateDutyScheduleTaskPrint",
              "PrivateDuty/Task/Print/{id}",
              new { controller = "Schedule", action = "NotePdf", id = UrlParameter.Optional },
              new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
           );

            context.MapRoute(
               "PrivateDutyScheduleTask",
               "PrivateDuty/Task/{action}/{id}",
               new { controller = "Schedule", action = "New", id = UrlParameter.Optional },
               new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );

            context.MapRoute(
                "PrivateDutyScheduleDefault",
                "PrivateDuty/Schedule/{action}/{id}",
                new { controller = "Schedule", action = "Center", id = UrlParameter.Optional },
                new string[] { "Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers" }
            );
            //base.RegisterArea(context);
        }
    }
}
