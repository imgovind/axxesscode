﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AllergyProfileController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly PrivateDutyPatientProfileService profileService;

        public AllergyProfileController(IPatientService patientService, PrivateDutyPatientProfileService profileService)
        {
            this.profileService = profileService;
            this.patientService = patientService;
        }

        #endregion

        #region Actions

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.AllergyProfile, PermissionActions.ViewList)]
        public ActionResult Index(Guid PatientId)
        {
            return PartialView(patientService.GetPatientAllergyProfileViewData(PatientId));
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.AllergyProfile, PermissionActions.Print)]
        public ActionResult PrintPreview(Guid id)
        {
            return FileGenerator.PreviewPdf(new AllergyProfilePdf(profileService.GetAllergyProfilePrint(id)), "AllergyProfile", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.AllergyProfile, PermissionActions.Print)]
        public FileResult Pdf(Guid id)
        {
            return FileGenerator.Pdf(new AllergyProfilePdf(profileService.GetAllergyProfilePrint(id)), "AllergyProfile");
        }

        //stuff

        #endregion
    }
}