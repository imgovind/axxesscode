﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Application.Filter;

    // ReSharper disable InconsistentNaming
    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReportController : BaseController
    {
        #region Constructor / Member

        private readonly IAgencyService agencyService;
        private readonly IUserService userService;
        private readonly IPatientService patientService;
        private readonly IPayrollService payrollService;
        private readonly PrivateDutyReportService reportService;
        private readonly PrivateDutyTaskService scheduleService;
        private readonly PrivateDutyBillingService billingService;

        private readonly PDReportExcelGenerator reportExcelGenerator;

        public ReportController(IAgencyService agencyService, IUserService userService, IPatientService patientService, IPayrollService payrollService, PrivateDutyTaskService scheduleService, PrivateDutyReportService reportService, PrivateDutyBillingService billingService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.patientService = patientService;
            this.billingService = billingService;
            this.userService = userService;
            this.scheduleService = scheduleService;
            this.payrollService = payrollService;
            this.reportExcelGenerator = Container.Resolve<PDReportExcelGenerator>();
        }

        #endregion

        #region ReportController Actions

        #region General Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var dictionary = reportService.GetAllReport();
            return PartialView("Center", dictionary);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Patient()
        //{
        //    return PartialView("Patient/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Clinical()
        //{
        //    return PartialView("Clinical/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Schedule()
        //{
        //    return PartialView("Schedule/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Billing()
        //{
        //    return PartialView("Billing/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Employee()
        //{
        //    return PartialView("Employee/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Statistical()
        //{
        //    return PartialView("Statistical/Home");
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Completed()
        {
            return PartialView("Completed");
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult CompletedList(GridCommand command)
        {
            return View(new GridModel(reportService.GetRequestedReports(Current.AgencyId, command.PageSize, command.Page)));
        }

        #endregion

        #region Patient Reports

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientRoster()
        {
            SetExportPermission(ReportPermissions.PatientRoster);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Patient/Roster", reportService.GetPatientRoster(branchId, (int)PatientStatus.Active, 0, false));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientRoster, PermissionActions.ViewList)]
        public ActionResult PatientRosterContent(Guid BranchId, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate, string SortParams, bool? IsRangeApplicable)
        {
            SetSortParams(SortParams);
            if (IsRangeApplicable.HasValue && IsRangeApplicable.Value && StartDate.HasValue && EndDate.HasValue)
            {
                AreDatesValid(StartDate.Value, EndDate.Value);
                return PartialView("Patient/Content/RosterContent", reportService.GetPatientRosterByDateRange(BranchId, StatusId, InsuranceId, StartDate.Value, EndDate.Value, false));
            }
            return PartialView("Patient/Content/RosterContent", reportService.GetPatientRoster(BranchId, StatusId, InsuranceId, false));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientRoster, PermissionActions.Export)]
        public ActionResult ExportPatientRoster(Guid BranchId, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate, bool? IsRangeApplicable)
        {
            List<PatientRoster> patientRosters;
            if (IsRangeApplicable.HasValue && IsRangeApplicable.Value && StartDate.HasValue && EndDate.HasValue)
            {
                AreDatesValid(StartDate.Value, EndDate.Value);
                patientRosters = reportService.GetPatientRosterByDateRange(BranchId, StatusId, InsuranceId, StartDate.Value, EndDate.Value, true);
            }
            else
            {
                patientRosters = reportService.GetPatientRoster(BranchId, StatusId, InsuranceId, true);
            }

            return reportExcelGenerator.ExportPatientRoster(patientRosters, BranchId, StatusId, InsuranceId, StartDate, EndDate);
        }

        //[ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CAHPSReport, PermissionActions.ViewList)]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Cahps()
        //{
        //    SetBranchWithService();
        //    return PartialView("Patient/Cahps");
        //}

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmergencyContactListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Emergencies()
        {
            SetExportPermission(ReportPermissions.EmergencyContactListing);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Patient/EmergencyList", reportService.GetPatientEmergencyContacts(branchId, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmergencyContactListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergenciesContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/EmergencyListContent", reportService.GetPatientEmergencyContacts(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmergencyContactListing, PermissionActions.Export)]
        public ActionResult ExportEmergencies(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientEmergencyList(reportService.GetPatientEmergencyContacts(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Birthdays()
        {
            SetExportPermission(ReportPermissions.PatientBirthdayListing);
            var branchId = SetSortParamsWithServiceAndBranch("Name", "ASC", true);
            return PartialView("Patient/BirthdayList", reportService.GetPatientBirthdays(branchId, DateTime.Now.Month));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BirthdaysContent(Guid BranchId, int Month, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/BirthdayListContent", reportService.GetPatientBirthdays(BranchId, Month));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientBirthdayListing, PermissionActions.Export)]
        public ActionResult ExportBirthdays(Guid BranchId, int Month)
        {
            return reportExcelGenerator.ExportPatientBirthdayList(reportService.GetPatientBirthdays(BranchId, Month), Month);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientAddressListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Addresses()
        {
            SetExportPermission(ReportPermissions.PatientAddressListing);
            var branchId = SetSortParamsWithServiceAndBranch("Name", "ASC", true);
            return PartialView("Patient/AddressList", reportService.GetPatientAddressListing(branchId, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientAddressListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddressesContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/AddressListContent", reportService.GetPatientAddressListing(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientAddressListing, PermissionActions.Export)]
        public ActionResult ExportAddresses(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientAddressList(reportService.GetPatientAddressListing(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByPhysicianListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianPatients()
        {
            SetExportPermission(ReportPermissions.PatientByPhysicianListing);
            SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Patient/Physician", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByPhysicianListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianPatientsContent(Guid PhysicianId, string SortParams, int StatusId)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/Physician", PhysicianId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByPhysician(PhysicianId, StatusId, AgencyServices.PrivateDuty));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByPhysicianListing, PermissionActions.Export)]
        public ActionResult ExportPhysicianPatients(Guid PhysicianId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientByPhysicians(PhysicianId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByPhysician(PhysicianId, StatusId, AgencyServices.PrivateDuty), PhysicianId);
        }

        [GridAction]
        public JsonResult PatientBirthdayWidget()
        {
            return Json(new GridModel(reportService.GetCurrentBirthdays()));
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //public ActionResult PatientSocCertPeriodListing()
        //{
        //    SetSortParams("PatientLastName", "ASC");
        //    return PartialView("Patient/PatientSocCertPeriodListing", reportService.GetPatientSocCertPeriod(Guid.Empty, 1, DateTime.Now.AddDays(-59), DateTime.Now));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult PatientSocCertPeriodListingContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        //{
        //    SetSortParams(SortParams);
        //    return PartialView("Patient/Content/PatientSocCertPeriodListingContent", reportService.GetPatientSocCertPeriod(BranchId, StatusId, StartDate, EndDate));
        //}

        //public ActionResult ExportPatientSocCertPeriodListing(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        //{
        //    return reportExcelGenerator.ExportPatientSocCertPeriodListing(reportService.GetPatientSocCertPeriod(BranchId, StatusId, StartDate, EndDate));
        //}

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByResponsibleEmployeeListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeePatients()
        {
            SetExportPermission(ReportPermissions.PatientByResponsibleEmployeeListing);
            SetSortParamsWithServiceAndBranch("PatientLastName", "ASC", true);
            return PartialView("Patient/PatientByResponsibleEmployeeListing", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByResponsibleEmployeeListing, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult EmployeePatientsContent(Guid BranchId, Guid UserId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/PatientByResponsibleEmployeeListingContent", UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableEmployee(BranchId, UserId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByResponsibleEmployeeListing, PermissionActions.Export)]
        public ActionResult ExportEmployeePatients(Guid BranchId, Guid UserId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientByResponsibleEmployeeListing(UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableEmployee(BranchId, UserId, StatusId), UserId);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByResponsibleCaseManagerListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagerPatients()
        {
            SetExportPermission(ReportPermissions.PatientByResponsibleCaseManagerListing);
            SetSortParamsWithServiceAndBranch("PatientLastName", "ASC", true);
            return PartialView("Patient/PatientByResponsibleCaseManager", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByResponsibleCaseManagerListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagerPatientsContent(Guid BranchId, Guid UserId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/PatientByResponsibleCaseManagerContent", UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableByCaseManager(BranchId, UserId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientByResponsibleCaseManagerListing, PermissionActions.Export)]
        public ActionResult ExportCaseManagerPatients(Guid BranchId, Guid UserId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientByResponsibleCaseManager(UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableByCaseManager(BranchId, UserId), UserId);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ExpiringAuthorizations, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExpiringAuthorizations()
        {
            SetExportPermission(ReportPermissions.ExpiringAuthorizations);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Patient/ExpiringAuthorizations", reportService.GetExpiringAuthorizaton(branchId, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ExpiringAuthorizations, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringAuthorizationsContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/ExpiringAuthorizationsContent", reportService.GetExpiringAuthorizaton(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ExpiringAuthorizations, PermissionActions.Export)]
        public ActionResult ExportExpiringAuthorizations(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportExpiringAuthorizations(reportService.GetExpiringAuthorizaton(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.SurveyCensus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SurveyCensus()
        {
            SetExportPermission(ReportPermissions.SurveyCensus);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Patient/SurveyCensus", reportService.GetPatientSurveyCensus(branchId, (int)PatientStatus.Active, 0, false));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.SurveyCensus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SurveyCensusContent(Guid BranchId, int StatusId, int InsuranceId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/SurveyCensusContent", reportService.GetPatientSurveyCensus(BranchId, StatusId, InsuranceId, false));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.SurveyCensus, PermissionActions.Export)]
        public ActionResult ExportSurveyCensus(Guid BranchId, int StatusId, int InsuranceId)
        {
            return reportExcelGenerator.ExportPatientSurveyCensus(reportService.GetPatientSurveyCensus(BranchId, StatusId, InsuranceId, true));
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //public ActionResult PatientVitalSigns()
        //{
        //    SetSortParams("VisitDate", "ASC");
        //    return PartialView("Patient/VitalSigns", new List<VitalSign>());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult VitalSignsContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        //{
        //    SetSortParams(SortParams);
        //    return PartialView("Patient/Content/VitalSignsContent", PatientId.IsEmpty() ? new List<VitalSign>() : noteService.GetPatientVitalSigns(PatientId, StartDate, EndDate));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult ExportVitalSigns(Guid PatientId, DateTime StartDate, DateTime EndDate)
        //{
        //    var vitalSigns = noteService.GetPatientVitalSigns(PatientId, StartDate, EndDate);
        //    var patient = patientService.GetPatientOnly(PatientId);
        //    var export = new VitalSignExporter(vitalSigns.ToList(), StartDate, EndDate, patient != null ? patient.DisplayNameWithMi : string.Empty);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PatientVitalSigns_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //public ActionResult PatientSixtyDaySummary()
        //{
        //    SetSortParams("UserDisplayName", "ASC");
        //    return PartialView("Patient/SixtyDaySummary", new List<VisitNoteViewData>());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult SixtyDaySummaryContent(Guid PatientId, string SortParams)
        //{
        //    SetSortParams(SortParams);
        //    return PartialView("Patient/Content/SixtyDaySummaryContent", PatientId.IsEmpty() ? new List<VisitNoteViewData>() : noteService.GetSixtyDaySummary(PatientId));
        //}

        //public ActionResult ExportPatientSixtyDaySummary(Guid PatientId)
        //{
        //    return reportExcelGenerator.ExportPatientSixtyDaySummary(PatientId.IsEmpty() ? new List<VisitNoteViewData>() : noteService.GetSixtyDaySummary(PatientId), PatientId, patientService.GetPatientNameById(PatientId));
        //}

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.DischargePatients, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargePatients()
        {
            SetExportPermission(ReportPermissions.DischargePatients);
            var branchId = SetSortParamsWithServiceAndBranch("LastName", "ASC", true);
            return PartialView("Patient/DischargePatients", reportService.GetDischargePatients(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.DischargePatients, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargePatientsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Patient/Content/DischargePatientsContent", reportService.GetDischargePatients(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.DischargePatients, PermissionActions.Export)]
        public ActionResult ExportDischargePatients(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportDischargePatients(reportService.GetDischargePatients(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        #endregion

        #region Clinical Reports

       //[ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.TherapyManagement, PermissionActions.ViewList)]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ClinicalTherapyManagement()
        //{
        //    SetBranchWithService();
        //    return PartialView("Clinical/TherapyManagement");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ClinicalOpenOasis()
        //{
        //    SetSortParams("PatientName", "ASC");
        //    return PartialView("Clinical/OpenOasis", reportService.GetAllOpenOasis(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult OpenOasisContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        //{
        //    SetSortParams(SortParams);
        //    return PartialView("Clinical/Content/OpenOasis", reportService.GetAllOpenOasis(BranchId, StartDate, EndDate));
        //}

        //public ActionResult ExportClinicalOpenOasis(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    return reportExcelGenerator.ExportClinicalOpenOasis(reportService.GetAllOpenOasis(BranchId, StartDate, EndDate), StartDate, EndDate);
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ClinicalOrders()
        //{
        //    return PartialView("Clinical/Orders");
        //}

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult ClinicalOrders([Bind] ReportParameters parameters)
        //{
        //    return View(new GridModel(reportService.GetOrders(parameters.StatusId)));
        //}

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.MissedVisitReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisits()
        {
            SetExportPermission(ReportPermissions.MissedVisitReport);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Clinical/MissedVisit", reportService.GetAllMissedVisit(branchId, DateTime.Now.AddDays(-60), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.MissedVisitReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/MissedVisit", reportService.GetAllMissedVisit(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.MissedVisitReport, PermissionActions.Export)]
        public ActionResult ExportMissedVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalMissedVisit(reportService.GetAllMissedVisit(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PhysicianOrderHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderHistory()
        {
            SetExportPermission(ReportPermissions.PhysicianOrderHistory);
            var branchId = SetSortParamsWithServiceAndBranch("OrderNumber", "ASC", true);
            return PartialView("Clinical/OrderHistory", reportService.GetPhysicianOrderHistory(branchId, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PhysicianOrderHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianOrderHistoryContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/OrderHistory", reportService.GetPhysicianOrderHistory(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PhysicianOrderHistory, PermissionActions.Export)]
        public ActionResult ExportPhysicianOrderHistory(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalPhysicianOrderHistory(reportService.GetPhysicianOrderHistory(BranchId, StatusId, StartDate, EndDate), StartDate, EndDate);
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ClinicalPlanOfCareHistory()
        //{
        //    SetSortParams("Number", "ASC");
        //    return PartialView("Clinical/PlanOfCareHistory", reportService.GetPlanOfCareHistory(Guid.Empty, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult PlanOfCareHistoryContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        //{
        //    SetSortParams(SortParams);
        //    return PartialView("Clinical/Content/PlanOfCareHistory", reportService.GetPlanOfCareHistory(BranchId, StatusId, StartDate, EndDate));
        //}

        //public ActionResult ExportClinicalPlanOfCareHistory(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        //{
        //    return reportExcelGenerator.ExportClinicalPlanOfCareHistory(reportService.GetPlanOfCareHistory(BranchId, StatusId, StartDate, EndDate), StartDate, EndDate);
        //}

        #endregion

        #region Schedule Reports

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientWeekly()
        {
            SetExportPermission(ReportPermissions.PatientWeeklySchedule);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Schedule/PatientWeeklySchedule", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientWeeklyContent(Guid PatientId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PatientWeeklySchedule", PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7)));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientWeeklySchedule, PermissionActions.Export)]
        public ActionResult ExportPatientWeekly(Guid PatientId)
        {
            return reportExcelGenerator.ExportSchedulePatientWeeklySchedule(PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7)), PatientId, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientMonthlySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientMonthly()
        {
            SetExportPermission(ReportPermissions.PatientMonthlySchedule);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Schedule/PatientMonthlySchedule", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientMonthlySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientMonthlyContent(Guid PatientId, int Month, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            if (PatientId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/PatientMonthlySchedule", new List<TaskLean>());
            }

            return PartialView("Schedule/Content/PatientMonthlySchedule", reportService.GetPatientScheduleEventsByDateRange(PatientId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientMonthlySchedule, PermissionActions.Export)]
        public ActionResult ExportPatientMonthly(Guid PatientId, int Month, int Year)
        {
            return reportExcelGenerator.ExportPatientMonthlySchedule(PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)), PatientId, Month, Year, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeDailyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DailyWork()
        {
            SetExportPermission(ReportPermissions.EmployeeDailyWorkSchedule);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/DailyWork", reportService.GetScheduleEventsByDateRange(branchId, DateTime.Now, DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeDailyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DailyWorkContent(Guid BranchId, DateTime Date, string SortParams)
        {
            IsDateValid(Date);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/DailyWork", reportService.GetScheduleEventsByDateRange(BranchId, Date, Date));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeDailyWorkSchedule, PermissionActions.Export)]
        public ActionResult ExportDailyWork(DateTime Date, Guid BranchId)
        {
            IsDateValid(Date);
            return reportExcelGenerator.ExportScheduleDailyWork(reportService.GetScheduleEventsByDateRange(BranchId, Date, Date));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeWeekly()
        {
            SetExportPermission(ReportPermissions.EmployeeWeeklySchedule);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/EmployeeWeekly", new List<UserVisit>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeWeeklyContent(Guid BranchId, Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/EmployeeWeekly", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeWeeklySchedule, PermissionActions.Export)]
        public ActionResult ExportEmployeeWeekly(Guid BranchId, Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleEmployeeWeekly(UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, StartDate, EndDate), BranchId, UserId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeMonthlyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeMonthly()
        {
            SetExportPermission(ReportPermissions.EmployeeMonthlyWorkSchedule);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/MonthlyWork", new List<UserVisit>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeMonthlyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeMonthlyContent(Guid BranchId, Guid UserId, int Month, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            if (UserId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/MonthlyWork", new List<UserVisit>());
            }

            return PartialView("Schedule/Content/MonthlyWork", reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeMonthlyWorkSchedule, PermissionActions.Export)]
        public ActionResult ExportEmployeeMonthly(Guid BranchId, Guid UserId, int Month, int Year)
        {
            return reportExcelGenerator.ExportScheduleMonthlyWork((UserId.IsEmpty() || Month <= 0) ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)), BranchId, UserId, Month, Year);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PastDueVisits, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueVisits()
        {
            SetExportPermission(ReportPermissions.PastDueVisits);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/PastDueVisits", reportService.GetPastDueScheduleEvents(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PastDueVisits, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PastDueVisits", reportService.GetPastDueScheduleEvents(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PastDueVisits, PermissionActions.Export)]
        public ActionResult ExportPastDueVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportSchedulePastDueVisits(reportService.GetPastDueScheduleEvents(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PastDueVisitsByDiscipline, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueVisitsPerDiscipline()
        {
            SetExportPermission(ReportPermissions.PastDueVisitsByDiscipline);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(branchId, "Nursing", DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PastDueVisitsByDiscipline, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsPerDisciplineContent(Guid BranchId, string Discipline, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(BranchId, Discipline, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PastDueVisitsByDiscipline, PermissionActions.Export)]
        public ActionResult ExportPastDueVisitsPerDiscipline(Guid BranchId, string Discipline, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportSchedulePastDueVisitsByDiscipline(reportService.GetPastDueScheduleEventsByDiscipline(BranchId, Discipline, StartDate, EndDate), Discipline, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CaseManagerTaskList, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagerTasks()
        {
            SetExportPermission(ReportPermissions.CaseManagerTaskList);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CaseManagerTaskList, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagerTasksContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CaseManagerTaskList, PermissionActions.Export)]
        public ActionResult ExportCaseManagerTasks(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleCaseManagerTask(reportService.GetCaseManagerScheduleByBranch(BranchId, StartDate, EndDate));
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ScheduleDeviation()
        //{
        //    SetSortParams("PatientName", "ASC");
        //    return PartialView("Schedule/Deviation", reportService.GetScheduleDeviation(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult DeviationContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        //{
        //    SetSortParams(SortParams);
        //    return PartialView("Schedule/Content/Deviation", reportService.GetScheduleDeviation(BranchId, StartDate, EndDate));
        //}

        //public ActionResult ExportScheduleDeviation(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var scheduleEvents = reportService.GetScheduleDeviation(BranchId, StartDate, EndDate);
        //    var workbook = new HSSFWorkbook();
        //    var si = PropertySetFactory.CreateSummaryInformation();
        //    si.Subject = "Axxess Data Export - Schedule Deviation";
        //    workbook.SummaryInformation = si;
        //    var sheet = workbook.CreateSheet("ScheduleDeviation");
        //    var titleRow = sheet.CreateRow(0);
        //    titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
        //    titleRow.CreateCell(1).SetCellValue("Schedule Deviation");
        //    titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
        //    var headerRow = sheet.CreateRow(1);
        //    headerRow.CreateCell(0).SetCellValue("Task");
        //    headerRow.CreateCell(1).SetCellValue("Patient Name");
        //    headerRow.CreateCell(2).SetCellValue("MR #");
        //    headerRow.CreateCell(3).SetCellValue("Status");
        //    headerRow.CreateCell(4).SetCellValue("Employee");
        //    headerRow.CreateCell(5).SetCellValue("Schedule Date");
        //    headerRow.CreateCell(6).SetCellValue("Visit Date");
        //    sheet.CreateFreezePane(0, 2, 0, 2);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        int rowNumber = 2;
        //        foreach (var evnt in scheduleEvents)
        //        {
        //            var row = sheet.CreateRow(rowNumber);
        //            row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
        //            row.CreateCell(1).SetCellValue(evnt.PatientName);
        //            row.CreateCell(2).SetCellValue(evnt.PatientIdNumber);
        //            row.CreateCell(3).SetCellValue(evnt.StatusName);
        //            row.CreateCell(4).SetCellValue(evnt.UserName);
        //            row.CreateCell(5).SetCellValue(evnt.EventDate);
        //            row.CreateCell(6).SetCellValue(evnt.VisitDate);
        //            rowNumber++;
        //        }
        //        var totalRow = sheet.CreateRow(rowNumber + 2);
        //        totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviation: {0}", scheduleEvents.Count));
        //    }
        //    sheet.AutoSizeColumn(0);
        //    sheet.AutoSizeColumn(1);
        //    sheet.AutoSizeColumn(2);
        //    sheet.AutoSizeColumn(3);
        //    sheet.AutoSizeColumn(4);
        //    sheet.AutoSizeColumn(5);
        //    sheet.AutoSizeColumn(6);
        //    MemoryStream output = new MemoryStream();
        //    workbook.Write(output);
        //    return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleDeviation_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.VisitsByStatus, PermissionActions.ViewList)]
        public ActionResult VisitsByStatus()
        {
            SetExportPermission(ReportPermissions.VisitsByStatus);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/VisitsByStatus", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.VisitsByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitsByStatusContent(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int status, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            var isValidStatus = status > 0;
            SetSortParams(SortParams);
            var scheduleEvents = isValidStatus ? scheduleService.GetScheduledEventsByStatus(BranchId, PatientId, ClinicianId, StartDate, EndDate, status) : new List<TaskLean>();
            return PartialView("Schedule/Content/VisitsByStatus", scheduleEvents);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.VisitsByStatus, PermissionActions.Export)]
        public ActionResult ExportVisitsByStatus(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int status)
        {
            AreDatesValid(StartDate, EndDate);
            var isValidStatus = status > 0;
            var events = isValidStatus ? scheduleService.GetScheduledEventsByStatus(BranchId, PatientId, ClinicianId, StartDate, EndDate, status) : new List<TaskLean>();
            return reportExcelGenerator.ExportScheduleVisitsByStatus(events, BranchId, StartDate, EndDate, status);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.VisitsByType, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByType()
        {
            SetExportPermission(ReportPermissions.VisitsByType);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/VisitsByType", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.VisitsByType, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitsByTypeContent(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int type, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/VisitsByType", scheduleService.GetScheduledEventsByType(BranchId, PatientId, ClinicianId, StartDate, EndDate, type));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.VisitsByType, PermissionActions.Export)]
        public ActionResult ExportVisitsByType(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int type)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleVisitsByType(scheduleService.GetScheduledEventsByType(BranchId, PatientId, ClinicianId, StartDate, EndDate, type), BranchId, StartDate, EndDate, type);
        }

        #endregion

        #region Billing Reports

        //[ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.HHRGReport, PermissionActions.ViewList)]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult HHRG()
        //{
        //    SetBranchWithService();
        //    return PartialView("Billing/HHRG");
        //}

        //[ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnbilledVisitsforManagedClaims, PermissionActions.ViewList)]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult UnbilledVisitsForManagedClaims()
        //{
        //    SetBranchWithService();
        //    return PartialView("Billing/UnbilledVisits");
        //}

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ClaimsHistoryByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimsByStatus()
        {
            SetExportPermission(ReportPermissions.ClaimsHistoryByStatus);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/ManagedClaimByStatus", billingService.GetManagedClaimsWithPaymentData(branchId, (int)ManagedClaimStatus.ClaimCreated, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ClaimsHistoryByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimsByStatusContent(Guid BranchId, string Type, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/ManagedClaimByStatus", billingService.GetManagedClaimsWithPaymentData(BranchId, Status, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ClaimsHistoryByStatus, PermissionActions.Export)]
        public ActionResult ExportClaimsByStatus(Guid BranchId, string Type, int Status, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportManagedClaimsByStatus(billingService.GetManagedClaimsWithPaymentData(BranchId, Status, StartDate, EndDate), Status);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnbilledManagedCareClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledManagedClaims()
        {
            SetExportPermission(ReportPermissions.UnbilledManagedCareClaims);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/UnbilledManagedClaim", billingService.GetManagedClaimsWithPaymentData(branchId, (int)ManagedClaimStatus.ClaimCreated, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnbilledManagedCareClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnbilledManagedClaimsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/UnbilledManagedClaim", billingService.GetManagedClaimsWithPaymentData(BranchId, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnbilledManagedCareClaims, PermissionActions.Export)]
        public ActionResult ExportUnbilledManagedClaims(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportUnbilledManagedClaims(billingService.GetManagedClaimsWithPaymentData(BranchId, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate), StartDate, EndDate);
        }

        #endregion

        #region Employee Reports

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeRoster()
        {
            SetExportPermission(ReportPermissions.EmployeeRoster);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Employee/Roster", reportService.GetEmployeeRoster(branchId, (int)UserStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeRosterContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/Roster", reportService.GetEmployeeRoster(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeRoster, PermissionActions.Export)]
        public ActionResult ExportEmployeeRoster(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportEmployeeRoster(reportService.GetEmployeeRoster(BranchId, StatusId), StatusId);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeBirthdays()
        {
            SetExportPermission(ReportPermissions.EmployeeBirthdayListing);
            var branchId = SetSortParamsWithServiceAndBranch("Name", "ASC", true);
            return PartialView("Employee/BirthdayList", reportService.GetEmployeeBirthdays(branchId, (int)UserStatus.Active, DateTime.Now.Month));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeBirthdaysContent(Guid BranchId, int StatusId, int Month, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/BirthdayList", reportService.GetEmployeeBirthdays(BranchId, StatusId, Month));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeBirthdayListing, PermissionActions.Export)]
        public ActionResult ExportEmployeeBirthdays(Guid BranchId, int StatusId, int Month)
        {
            return reportExcelGenerator.ExportEmployeeBirthdayList(reportService.GetEmployeeBirthdays(BranchId, StatusId, Month), StatusId, Month);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeLicense()
        {
            SetExportPermission(ReportPermissions.EmployeeLicenseListing);
            SetSortParamsWithServiceAndBranch("InitiationDate", "ASC", true);
            return PartialView("Employee/License", new List<License>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeLicenseContent(Guid UserId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/License", UserId.IsEmpty() ? new List<License>() : userService.GetLicenses(UserId, false));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeLicenseListing, PermissionActions.Export)]
        public ActionResult ExportEmployeeLicense(Guid UserId)
        {
            return reportExcelGenerator.ExportEmployeeLicenseListing(userService.GetLicenses(UserId, false), UserId);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.AllEmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllEmployeeLicense()
        {
            SetExportPermission(ReportPermissions.AllEmployeeLicenseListing);
            var branchId = SetSortParamsWithServiceAndBranch("InitiationDate", "ASC", true);
            return PartialView("Employee/AllEmployeeLicense", userService.GetLicenses(branchId, (int)UserStatus.Active, true));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.AllEmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllEmployeeLicenseContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/AllEmployeeLicense", userService.GetLicenses(BranchId, StatusId, true));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.AllEmployeeLicenseListing, PermissionActions.Export)]
        public ActionResult ExportAllEmployeeLicense(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportAllEmployeeLicenseListing(userService.GetLicenses(BranchId, StatusId, true).ToList());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ExpiringLicenses, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExpiringLicense()
        {
            SetExportPermission(ReportPermissions.ExpiringLicenses);
            var branchId = SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Employee/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(branchId, (int)UserStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ExpiringLicenses, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringLicenseContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.ExpiringLicenses, PermissionActions.Export)]
        public ActionResult ExportExpiringLicense(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportEmployeeExpiringLicense(reportService.GetEmployeeExpiringLicenses(BranchId, StatusId), StatusId);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeVisitByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisitByDateRange()
        {
            SetExportPermission(ReportPermissions.EmployeeVisitByDateRange);
            var branchId = SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Employee/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(branchId, DateTime.Now.AddDays(-15), DateTime.Now.AddDays(15)));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeVisitByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeVisitByDateRangeContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Employee/Content/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeVisitByDateRange, PermissionActions.Export)]
        public ActionResult ExportEmployeeVisitByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportEmployeeVisitByDateRange(reportService.GetEmployeeScheduleByDateRange(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeePermissionsReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeePermissions()
        {
            SetExportPermission(ReportPermissions.EmployeePermissionsReport);
            var branchId = SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Employee/Permissions", reportService.GetEmployeePermissions(branchId, (int)UserStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeePermissionsReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeePermissionsContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/Permissions", reportService.GetEmployeePermissions(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeePermissionsReport, PermissionActions.Export)]
        public ActionResult ExportEmployeePermissions(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportEmployeePermissionsListing(userService.GetAllUsers(BranchId, StatusId));
        }

        // BOOKMARK

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PayrollDetailSummary, PermissionActions.ViewList)]
        public ActionResult PayrollDetailSummary()
        {
            SetSortParamsWithServiceAndBranch(string.Empty, string.Empty, true);
            return PartialView("Employee/PayrollDetailSummary");
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PayrollDetailSummary, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayrollDetailSummaryContent(Guid UserId, DateTime StartDate, DateTime EndDate, string PayrollStatus, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            return PartialView("Employee/Content/PayrollDetailSummary", payrollService.GetVisitsSummary(UserId, StartDate, EndDate, PayrollStatus));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PayrollDetailSummary, PermissionActions.Export)]
        public ActionResult ExportPayrollDetailSummary(Guid UserId, DateTime StartDate, DateTime EndDate, string PayrollStatus)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportPayrollDetailSummary(payrollService.GetVisitsSummary(UserId, StartDate, EndDate, PayrollStatus), UserId, StartDate, EndDate);
        }

        #endregion

        #region Statistical Reports

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientAdmissionsByInternalReferralSource, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdmissionsByInternalReferralSource()
        {
            SetExportPermission(ReportPermissions.PatientAdmissionsByInternalReferralSource);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Statistical/PatientAdmissionsByInternalReferralSource", new List<PatientAdmission>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientAdmissionsByInternalReferralSource, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionsByInternalReferralSourceContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/PatientAdmissionsByInternalReferralSource", reportService.GetPatientAdmissionsByInternalSource(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientAdmissionsByInternalReferralSource, PermissionActions.Export)]
        public ActionResult ExportAdmissionsByInternalReferralSource(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalPatientAdmissionsByInternalReferralSource(reportService.GetPatientAdmissionsByInternalSource(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientVisits()
        {
            SetExportPermission(ReportPermissions.PatientVisitHistory);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Statistical/PatientVisitHistory", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientVisitsContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/PatientVisitHistory", PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.PatientVisitHistory, PermissionActions.Export)]
        public ActionResult ExportPatientVisits(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalPatientVisitHistory(reportService.GetPatientScheduleEventsByDateRange(PatientId, StartDate, EndDate), PatientId, StartDate, EndDate, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisits()
        {
            SetExportPermission(ReportPermissions.EmployeeVisitHistory);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Statistical/EmployeeVisitHistory", new List<UserVisit>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeVisitsContent(Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/EmployeeVisitHistory", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetEmployeeVisistList(UserId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.EmployeeVisitHistory, PermissionActions.Export)]
        public ActionResult ExportEmployeeVisits(Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalEmployeeVisitHistory(reportService.GetEmployeeVisistList(UserId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.MonthlyAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MonthlyAdmissions()
        {
            SetExportPermission(ReportPermissions.MonthlyAdmissionReport);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Statistical/MonthlyAdmission", reportService.GetPatientMonthlyAdmission(branchId, (int)PatientStatus.Active, DateTime.Now.Month, DateTime.Now.Year));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.MonthlyAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MonthlyAdmissionsContent(Guid BranchId, int StatusId, int Month, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/MonthlyAdmission", reportService.GetPatientMonthlyAdmission(BranchId, StatusId, Month, Year));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.MonthlyAdmissionReport, PermissionActions.Export)]
        public ActionResult ExportMonthlyAdmissions(Guid BranchId, int StatusId, int Month, int Year)
        {
            return reportExcelGenerator.ExportStatisticalMonthlyAdmission(reportService.GetPatientMonthlyAdmission(BranchId, StatusId, Month, Year), StatusId, Month, Year);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.AnnualAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AnnualAdmissions()
        {
            SetExportPermission(ReportPermissions.AnnualAdmissionReport);
            var branchId = SetSortParamsWithServiceAndBranch("PatientFirstName", "ASC", true);
            return PartialView("Statistical/AnnualAdmission", reportService.GetPatientByAdmissionYear(branchId, (int)PatientStatus.Active, DateTime.Now.Year));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.AnnualAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualAdmissionsContent(Guid BranchId, int StatusId, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/AnnualAdmission", reportService.GetPatientByAdmissionYear(BranchId, StatusId, Year));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.AnnualAdmissionReport, PermissionActions.Export)]
        public ActionResult ExportAnnualAdmissions(Guid BranchId, int StatusId, int Year)
        {
            return reportExcelGenerator.ExportStatisticalAnnualAdmission(reportService.GetPatientByAdmissionYear(BranchId, StatusId, Year), StatusId, Year);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnduplicatedCensusReportByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnduplicatedCensus()
        {
            SetExportPermission(ReportPermissions.UnduplicatedCensusReportByDateRange);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Statistical/UnduplicatedCensusReport", reportService.GetPatientByAdmissionUnduplicatedByDateRange(branchId, (int)PatientStatus.Active, DateTime.Now.AddDays(-59), DateTime.Now.Date));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnduplicatedCensusReportByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnduplicatedCensusContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/UnduplicatedCensusReport", reportService.GetPatientByAdmissionUnduplicatedByDateRange(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.UnduplicatedCensusReportByDateRange, PermissionActions.Export)]
        public ActionResult ExportUnduplicatedCensus(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalUnduplicatedCensusReport(reportService.GetPatientByAdmissionUnduplicatedByDateRange(BranchId, StatusId, StartDate, EndDate), StatusId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CensusByPrimaryInsurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CensusByPrimaryInsurance()
        {
            SetExportPermission(ReportPermissions.CensusByPrimaryInsurance);
            SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Statistical/CensusByPrimaryInsurance", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CensusByPrimaryInsurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CensusByPrimaryInsuranceContent(Guid BranchId, int InsuranceId, int StatusId)
        {
            return PartialView("Statistical/Content/CensusByPrimaryInsurance", InsuranceId <= 0 ? new List<PatientRoster>() : reportService.GetPatientRosterByInsurance(BranchId, InsuranceId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.PrivateDuty, ReportPermissions.CensusByPrimaryInsurance, PermissionActions.Export)]
        public ActionResult ExportCensusByPrimaryInsurance(Guid BranchId, int InsuranceId, int StatusId)
        {
            return reportExcelGenerator.ExportCensusByPrimaryInsurance(InsuranceId > 0 ? reportService.GetPatientRosterByInsurance(BranchId, InsuranceId, StatusId) : new List<PatientRoster>(), InsuranceId, StatusId);
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult PPSEpisodeInformation()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/PPSEpisodeInformation");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult PPSVisitInformation()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/PPSVisitInformation");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult PPSPaymentInformation()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/PPSPaymentInformation");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult PPSChargeInformation()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/PPSChargeInformation");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult DischargesByReason()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/DischargesByReason");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult VisitsByPrimaryPaymentSource()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/VisitsByPrimaryPaymentSource");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult VisitsByStaffType()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/VisitsByStaffType");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult AdmissionsByReferralSource()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/AdmissionsByReferralSource");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult PatientsVisitsByPrincipalDiagnosis()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/PatientsVisitsByPrincipalDiagnosis");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult PatientsAndVisitsByAge()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/PatientsAndVisitsByAge");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult CostReport()
        //{
        //    SetBranchWithService();
        //    return PartialView("Statistical/CostReport");
        //}

        #endregion

        #endregion

        private void SetSortParams(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
        }

        private Guid SetSortParamsWithServiceAndBranch(string sortColumn, string sortDirection, bool isSetBranchNeeded)
        {
            ViewData["SortColumn"] = sortColumn;
            ViewData["SortDirection"] = sortDirection;
            var branchId = Guid.Empty;
            if (isSetBranchNeeded)
            {
                branchId = SetBranchWithService();
            }
            else
            {
                ViewData["Service"] = (int)AgencyServices.PrivateDuty;
            }

            return branchId;
        }

        private void SetExportPermission(ReportPermissions report)
        {
            var permission = UserSessionEngine.ReportPermissions(Current.AgencyId, Current.UserId, Current.AcessibleServices, Current.SessionId);// Current.ReportPermissions;
            if (permission.IsInPermission(AgencyServices.PrivateDuty, (int)report, PermissionActions.Export))
            {
                ViewData["IsUserCanExport"] = 1;
            }
            else
            {
                ViewData["IsUserCanExport"] = 0;
            }
        }


        /// <summary>
        /// Set the service and branch and return branch Id
        /// </summary>
        /// <returns></returns>
        private Guid SetBranchWithService()
        {
            var branchId = agencyService.GetBranchForSelectionId((int)AgencyServices.PrivateDuty);
            ViewData["BranchId"] = branchId;
            ViewData["Service"] = (int)AgencyServices.PrivateDuty;
            return branchId;
        }

        private void IsDateValid(DateTime date)
        {
            if (!date.IsValid())
            {
                throw new ArgumentException("The date used is invalid.");
            }
        }

        private void AreDatesValid(DateTime date, DateTime otherDate)
        {
            if (!date.IsValid() || !otherDate.IsValid())
            {
                throw new ArgumentException("The dates used in the date range are invalid.");
            }
            if (otherDate < date)
            {
                throw new ArgumentException("The End Date must be larger than the Start Date.");
            }
        }
    }
    // ReSharper restore InconsistentNaming
}