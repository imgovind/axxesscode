﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Workflows;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PatientProfileController : BaseController
    {
        #region Constructor

       
        private readonly IPatientService patientService;
        private readonly PrivateDutyTaskService scheduleService;
        private readonly PrivateDutyMediatorService mediatorService;
        private readonly PrivateDutyPatientProfileService profileService;
       
        public PatientProfileController(IPatientService patientService, PrivateDutyPatientProfileService profileService, PrivateDutyTaskService scheduleService, PrivateDutyMediatorService mediatorService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");

            
            this.patientService = patientService;
            this.scheduleService = scheduleService;
            this.mediatorService = mediatorService;
            this.profileService = profileService;
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult All(Guid BranchId, byte StatusId, byte PaymentSourceId, byte servicesId)
        {
            var patientList = profileService.GetPatientSelection(BranchId, StatusId, PaymentSourceId, "", true) ?? new List<PatientSelection>();
            return Json(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllSelectList(Guid BranchId, byte StatusId, byte servicesId)
        {
            var patientList = profileService.GetPatientSelection(BranchId, StatusId, 0, "", true) ?? new List<PatientSelection>();
            patientList = patientList ?? new List<PatientSelection>();
            var patients = patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName);
            var selectList =
                from patient in patients
                select new SelectListItem
                {
                    Text = patient.DisplayNameWithMi.Trim().ToTitleCase(),
                    Value = patient.Id.ToString()
                };
            return Json(selectList);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllSort(Guid BranchId, byte StatusId, byte PaymentSourceId, byte servicesId)
        {
            var patientList = profileService.GetPatientSelection(BranchId, StatusId, PaymentSourceId, "", true) ?? new List<PatientSelection>();
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllInsurancePatient(Guid BranchId, byte StatusId, string Name, int InsuranceId)
        {
            var patientList = profileService.GetPatientSelectionByInsurance(BranchId, StatusId, InsuranceId, Name, false) ?? new List<PatientSelection>();
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewAuthorization()
        {
            return PartialView("~/Views/Patient/Authorization/New.ascx", new Patient() { Profile = new Profile() });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAuthorization(Guid patientId)
        {
            return PartialView("~/Views/Patient/Authorization/New.ascx", profileService.GetPatientWithProfile(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAuthorizationData(Guid patientId)
        {
            return Json(profileService.GetPatientWithProfile(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Info(Guid patientId)
        {
            if (patientId.IsEmpty())
            {
                return PartialView("Charts/Info", new Patient());
            }
            return PartialView("Charts/Info", profileService.GetPatientSnapshotInfo(patientId, true, true, false) ?? new Patient());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid patientId)
        {
            return PartialView("Charts/Data", mediatorService.GetScheduleCenterData(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid PatientId, string Discipline, string DateRangeId, DateTime? RangeStartDate, DateTime? RangeEndDate)
        {
            Check.Argument.IsNotNull(PatientId, "PatientId");
            Check.Argument.IsNotNull(Discipline, "discipline");
            Check.Argument.IsNotNull(DateRangeId, "dateRangeId");

            var dateRange = new DateRange { Id = DateRangeId };
            switch (DateRangeId)
            {
                case "DateRange":
                    dateRange.EndDate = RangeEndDate.HasValue ? RangeEndDate.Value : DateTime.MinValue;
                    dateRange.StartDate = RangeStartDate.HasValue ? RangeStartDate.Value : DateTime.MinValue;
                    break;
                case "NextEpisode":
                case "LastEpisode":
                case "ThisEpisode":
                    break;
                default:
                    dateRange = DateServiceFactory<PatientEpisode>.GetDateRange(DateRangeId, PatientId);
                    break;
            }

            var viewData = scheduleService.GetPatientScheduleEventViewData(PatientId, Discipline, dateRange);
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivitySort(Guid PatientId, string Discipline, string DateRangeId, DateTime? RangeStartDate, DateTime? RangeEndDate)
        {
            Check.Argument.IsNotNull(PatientId, "patientId");
            Check.Argument.IsNotNull(Discipline, "discipline");
            Check.Argument.IsNotNull(DateRangeId, "dateRangeId");

            var dateRange = new DateRange { Id = DateRangeId };
            switch (DateRangeId)
            {
                case "DateRange":
                    dateRange.EndDate = RangeEndDate.HasValue ? RangeEndDate.Value : DateTime.MinValue;
                    dateRange.StartDate = RangeStartDate.HasValue ? RangeStartDate.Value : DateTime.MinValue;
                    break;
                case "NextEpisode":
                case "LastEpisode":
                case "ThisEpisode":
                    break;
                default:
                    dateRange = DateServiceFactory<PatientEpisode>.GetDateRange(DateRangeId, PatientId);
                    break;
            }

            var viewData = scheduleService.GetPatientScheduleEventViewData(PatientId, Discipline, dateRange);
            return View(new GridModel(viewData.ScheduleEvents));
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Profile(Guid id)
        {
            return FileGenerator.PreviewPdf(new PatientProfilePdf(profileService.GetProfileWithOutEpisodeInfo(id)), "PatientProfile", Response);
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ProfilePdf(Guid id)
        {
            return FileGenerator.Pdf(new PatientProfilePdf(profileService.GetProfileWithOutEpisodeInfo(id)), "PatientProfile");
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Triage(Guid id)
        {
            return FileGenerator.PreviewPdf(new TriageClassificationPdf(profileService.GetProfileWithOutEpisodeInfo(id)), "TriageClassification", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TriagePdf(Guid id)
        {
            return FileGenerator.Pdf(new TriageClassificationPdf(profileService.GetProfileWithOutEpisodeInfo(id)), "TriageClassification");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNonAdmit([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");

            var errorMessage = "Patient could not be marked as non-admission.";
            patient.AgencyId = Current.AgencyId;
            var viewData = profileService.NonAdmitPatient(patient);
            if (viewData.isSuccessful)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient has been marked as non-admission successfully.";
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = profileService.DeletePatient(patientId, true);
            viewData.errorMessage = viewData.isSuccessful ? "Your data is successfully deleted." : "Your Data Is Not deleted. Try Again.";
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RestoreDeleted(Guid PatientId)
        {
            const string errorMessage = "The Patient could not be restored.";
            var viewData = new JsonViewData(false, errorMessage);
            if (!PatientId.IsEmpty())
            {
                viewData = profileService.DeletePatient(PatientId, false);
                viewData.errorMessage = viewData.isSuccessful ? "The patient was restored successfully." : errorMessage;
            }
            else
            {
                viewData.errorMessage = "The patient identifier provided is not valid. Please try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Pending(Guid Id)
        {
            return PartialView("Admit/PrivateDutyAdmitContent", profileService.GetProfileOnly(Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Grid()
        {
            var viewData = profileService.GetPatientsViewData(Guid.Empty,true, (int)PatientStatus.Active);
            if (viewData != null)
            {
                viewData.SortColumn = "DisplayName";
                viewData.SortDirection = "ASC";
            }
            return PartialView("List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid BranchId, int Status, string SortParams)
        {
            var viewData = profileService.GetPatientsViewData(BranchId,false, Status);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("ListContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingGrid()
        {
            return PartialView("Pending", profileService.GetServiceAndLocationViewData(ParentPermission.Patient, PermissionActions.ViewList, new List<PermissionActions> { PermissionActions.Add,PermissionActions.Export},true));
        }

        [GridAction]
        public ActionResult PendingList(Guid BranchId)
        {
            return View(new GridModel(profileService.GetPendingPatients(BranchId,false)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonAdmitGrid()
        {
            return PartialView("NonAdmit/List", profileService.GetServiceAndLocationViewData(ParentPermission.Patient, PermissionActions.ViewList, new List<PermissionActions> { PermissionActions.Add, PermissionActions.Export }, false));
        }

        [GridAction]
        public ActionResult NonAdmitList()
        {
            var patientList = profileService.GetNonAdmits(false) ?? new List<NonAdmit>();
            return PartialView(new GridModel(patientList));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DeletedPatientGrid()
        {
            var viewData = profileService.GetDeletedPatientViewData(Guid.Empty, true, false);
            if (viewData != null)
            {
                viewData.SortColumn = "DisplayName";
                viewData.SortDirection = "ASC";
            }
            return View("Deleted/List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedPatientContent(Guid BranchId, string SortParams)
        {
            var viewData = profileService.GetDeletedPatientViewData(BranchId, false, false);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return View("Deleted/ListContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Admit([Bind(Prefix = "profile[2]")]Profile profile)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            var workflow = new AdmitWorkflow(profile, AgencyServices.PrivateDuty);
            if (workflow.IsCommitted)
            {
                viewData.isSuccessful = true;
                viewData.IsCenterRefresh = true;
                viewData.IsPatientListRefresh = true;
                viewData.PatientStatus = (int)PatientStatus.Active;
                viewData.IsReferralListRefresh = false;
                viewData.IsNonAdmitPatientListRefresh = false;
                viewData.IsPendingPatientListRefresh = true;
                viewData.errorMessage = "Patient successfully admitted.";
            }
            else
            {
                viewData.errorMessage = workflow.Message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateStatus([Bind] PendingPatient patient)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "The patient status could not be changed. Please try again.";
            if (!patient.Id.IsEmpty())
            {
                patient.AgencyId = Current.AgencyId;
                if (patient.Status == (int)PatientStatus.Active)
                {
                    viewData = profileService.ActivatePatient(patient.Id);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully activated.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Discharged)
                {
                    // TODO:fix the patient discahrge
                    viewData = profileService.DischargePatient(patient.Id, patient.DateOfDischarge, patient.ReasonId, patient.Comments, Guid.Empty);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully discharged.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Pending)
                {
                    viewData = profileService.SetPatientPending(patient.Id);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to pending successfully.";
                    }
                }
                if (patient.Status == (int)PatientStatus.NonAdmission)
                {
                    viewData = profileService.NonAdmitPatient(patient);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to non-admit successfully.";
                    }
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Readmit(Guid patientId)
        {
            return PartialView(new PatientNameViewData() { PatientId = patientId, Service = AgencyServices.PrivateDuty });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientReadmit(Guid PatientId, DateTime ReadmissionDate)
        {
            var viewData = profileService.ActivatePatient(PatientId, ReadmissionDate);
            var errorMessage = "Patient Re-admission is unsuccessful.";
            if (viewData.isSuccessful)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient Re-admission is successful.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Edit(Guid patientId)
        //{
        //    return PartialView("~/Views/Patient/Edit.ascx", profileService.GetPatientWithProfile(patientId));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult EditPatientProfile(Guid patientId)
        //{
        //    var profile = profileService.GetProfileOnly(patientId);
        //    if (profile != null)
        //    {
        //        profile.IsUnloadButtonVisible = true;
        //    }

        //    return PartialView("~/Views/Patient/Edit/PrivateDuty.ascx", profile);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid PatientId)
        {
            return PartialView("~/Views/Patient/Edit/PrivateDuty.ascx", profileService.GetProfileOnly(PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind(Prefix = "profile[2]")]Profile profile)
        {
            return Json(profileService.EditProfile(profile));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AuthorizationPdf(Guid patientId, Guid id)
        {
            return FileGenerator.Pdf<AuthorizationPdf>(profileService.GetAuthorizationPdf(patientId, id), "Auth");
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult HospitalizationLogPdf(Guid patientId, Guid hospitalizationLogId)
        {
            return FileGenerator.Pdf<HospitalizationLogPdf>(new HospitalizationLogPdf(profileService.GetHospitalizationLogPrint(patientId, hospitalizationLogId)), "HospitalizationLog");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Status(Guid patientId)
        {
            return PartialView(profileService.GetProfileOnly(patientId));
        }

        #region Medication Profile

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.MedicationProfile, PermissionActions.ViewList)]
        public ActionResult MedicationProfile(Guid patientId)
        {
            return PartialView("~/Views/Patient/MedicationProfile/Profile.ascx", mediatorService.GetMedicationProfileViewDataWithDiagnosis(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.MedicationProfile, PermissionActions.Sign)]
        public ActionResult MedicationProfileSnapShot(Guid patientId)
        {
            return PartialView("~/Views/Patient/MedicationProfile/Sign.ascx", mediatorService.GetMedicationProfileViewDataWithDiagnosisForSign(patientId));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.MedicationProfile, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationProfilePrint(Guid id)
        {
            return FileGenerator.PreviewPdf<MedProfilePdf>(new MedProfilePdf(mediatorService.GetMedicationProfilePrint(id), PdfDocs.Get(PDFDOCSList.PrivateDutyMedProfile)), "MedProfile", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.MedicationProfile, PermissionActions.Print)]
        public FileResult MedicationProfilePdf(Guid id)
        {
            return FileGenerator.Pdf<MedProfilePdf>(new MedProfilePdf(mediatorService.GetMedicationProfilePrint(id), PdfDocs.Get(PDFDOCSList.PrivateDutyMedProfile)), "MedProfile");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(Guid BranchId, int StatusId)
        {
            return Json(profileService.GetPatientSelectionList(BranchId, StatusId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.MedicationProfile, PermissionActions.Print)]
        public FileResult MedicationProfileSnapshotPdf(Guid id)
        {
            return FileGenerator.Pdf<MedProfilePdf>(new MedProfilePdf(profileService.GetMedicationSnapshotPrint(id), PdfDocs.Get(PDFDOCSList.PrivateDutyMedProfile)), "MedProfile");
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.MedicationProfile, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationProfileSnapshotPrint(Guid id)
        {
            return FileGenerator.PreviewPdf<MedProfilePdf>(new MedProfilePdf(profileService.GetMedicationSnapshotPrint(id), PdfDocs.Get(PDFDOCSList.PrivateDutyMedProfile)), "MedProfile", Response);
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult DrugDrugInteractionsPdf(Guid patientId, List<string> drugsSelected)
        {
            return FileGenerator.Pdf<DrugDrugInteractionsPdf>(new DrugDrugInteractionsPdf(profileService.GetDrugDrugInteractionsPrint(patientId, drugsSelected)), "DrugDrugInteractions");
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitRates(Guid patientId)
        {
            return PartialView("~/Views/Patient/Edit/VisitRates.ascx", patientId);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitRatesData(Guid PatientId)
        {
            return View(new GridModel(profileService.GetPatientVisitRates(PatientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewVisitRate(Guid patientId)
        {
            return PartialView("~/Views/Patient/Edit/VisitRate/New.ascx", profileService.GetPatientVisitRateForNew(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditVisitRate(Guid patientId, int Id)
        {
            ViewData["PatientId"] = patientId;
            return PartialView("~/Views/Patient/Edit/VisitRate/Edit.ascx", profileService.GetPatientVisitRateForEdit(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddVisitRate(Guid patientId, ChargeRate chargeRate)
        {
            return Json(profileService.AddChargeRate(patientId, chargeRate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateVisitRate(Guid patientId, ChargeRate chargeRate)
        {
            return Json(profileService.UpdateChargeRate(patientId, chargeRate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteVisitRate(Guid patientId, int Id)
        {
            return Json(profileService.DeleteChargeRate(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReplaceVisitRates(Guid patientId, int replacedId)
        {
            return Json(profileService.ReplaceVisitRates(patientId, replacedId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Payors(Guid PatientId)
        {
            return View(new GridModel(profileService.GetPatientBillingInformations(PatientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewPayor(Guid patientId)
        {
            return PartialView("~/Views/Patient/Edit/Payor/New.ascx", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPayor(PrivatePayor payor)
        {
            return Json(profileService.AddPayor(payor));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePayor(Guid PatientId, int Id)
        {
            return Json(profileService.TogglePayor(PatientId, Id, true));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPayor(Guid PatientId, int Id)
        {
            return PartialView("~/Views/Patient/Edit/Payor/Edit.ascx", profileService.GetPayorOnly(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePayor(PrivatePayor payor)
        {
            return Json(profileService.UpdatePayor(payor));
        }

        #region Patient Insurance

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewInsurance(Guid PatientId, Guid AdmissionId, DateTime? StartOfCareDate, DateTime? DischargeDate)
        {
            var viewData= new PatientInsurance { PatientId = PatientId, AdmissionId = AdmissionId, Service = AgencyServices.PrivateDuty };
            if (StartOfCareDate.HasValue)
            {
                viewData.StartDate = StartOfCareDate.Value;
            }
            if (DischargeDate.HasValue)
            {
                viewData.EndDate = DischargeDate.Value;
            }

            return PartialView("Insurance/Edit",viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddInsurance(PatientInsurance insurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to save the patient insurance. Try again." };
            if (insurance.InsuranceId > 0)
            {
                if (!insurance.PatientId.IsEmpty())
                {
                    return  Json(profileService.AddInsurance(insurance));
                }
                else
                {
                    viewData.errorMessage = "Patient is not identified. Try again.";
                }
            }
            else
            {
                viewData.errorMessage = "Insurance is not identified. Try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditInsurance(Guid PatientId, Guid admissionId, int Id)
        {
            return PartialView("Insurance/Edit", profileService.GetPatientInsuranceWithName(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInsurance(PatientInsurance insurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to save the patient insurance. Try again." };
            if (insurance.InsuranceId > 0)
            {
                if (!insurance.PatientId.IsEmpty())
                {
                    return Json(profileService.EditInsurance(insurance));
                }
                else
                {
                    viewData.errorMessage = "Patient is not identified. Try again.";
                }
            }
            else
            {
                viewData.errorMessage = "Insurance is not identified. Try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteInsurance(Guid PatientId, Guid admissionId, int Id)
        {
            return Json(profileService.DeleteOrRestoreInsurance(PatientId, admissionId, Id, true));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Insurances(Guid PatientId, Guid AdmissionId)
        {
            return View(new GridModel(profileService.GetPatientInsurances(PatientId, AdmissionId)));
        }

        public ActionResult TaskPayors(Guid patientId, Guid? admissionId, DateTime startDate, DateTime endDate)
        {
            return Json(profileService.GetPatientTaskInsurances(patientId, admissionId.HasValue ? admissionId.Value : Guid.Empty, startDate, endDate));
        }

        #endregion

    }
}
