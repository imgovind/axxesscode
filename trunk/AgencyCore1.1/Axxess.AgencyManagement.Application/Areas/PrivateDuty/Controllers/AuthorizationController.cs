﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Web.Mvc;

    using Telerik.Web.Mvc;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AuthorizationController : BaseController
    {
        #region Constructor

        private readonly PrivateDutyPatientProfileService profileService;

        public AuthorizationController(PrivateDutyPatientProfileService profileService)
        {
            Check.Argument.IsNotNull(profileService, "profileService");

            this.profileService = profileService;
        }

        #endregion

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView("~/Views/Patient/Authorization/New.ascx", new Patient { Profile = new Profile() });
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            return PartialView("~/Views/Patient/Authorization/New.ascx", profileService.GetPatientWithProfile(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewData(Guid patientId)
        {
            return Json(profileService.GetProfileJsonByColumns(patientId, "AgencyLocationId", "PrimaryInsurance"));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add(Authorization authorization)
        {
            Check.Argument.IsNotNull(authorization, "authorization");
            return Json(profileService.AddAuthorization(authorization));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid patientId, Guid id)
        {
            return PartialView("~/Views/Patient/Authorization/Edit.ascx", profileService.GetAuthorizationWithPatientName(patientId, id));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(Authorization authorization)
        {
            Check.Argument.IsNotNull(authorization, "authorization");
            return Json(profileService.EditAuthorization(authorization));
        }
        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(Guid patientId)
        {
            return PartialView("~/Views/Patient/Authorization/List.ascx", profileService.GetAuthorizationPermission(patientId));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid patientId)
        {
            return View(new GridModel(profileService.GetAuthorizationsWithBranchName(patientId)));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid patientId, Guid id)
        {
            return Json(profileService.DeleteAuthorization(patientId,id));
        }
    }
}
