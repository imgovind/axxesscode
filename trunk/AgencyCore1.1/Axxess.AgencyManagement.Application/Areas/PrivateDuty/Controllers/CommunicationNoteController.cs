﻿
namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Web.Mvc;

    using iTextExtension;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class CommunicationNoteController : BaseController
    {
        #region Constructor

        private readonly PrivateDutyCommunicationNoteService communicationNoteService;
        private readonly PrivateDutyMediatorService mediatorService;

        public CommunicationNoteController(
            PrivateDutyCommunicationNoteService communicationNoteService,
            PrivateDutyMediatorService mediatorService)
        {
            Check.Argument.IsNotNull(mediatorService, "mediatorService");
            Check.Argument.IsNotNull(communicationNoteService, "communicationNoteService");

            this.communicationNoteService = communicationNoteService;
            this.mediatorService = mediatorService;
        }

        #endregion

        #region Communication Note

        [AcceptVerbs(HttpVerbs.Get)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Add)]
        public ActionResult New()
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.PrivateDuty);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Add)]
        public ActionResult New(Guid patientId)
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.PrivateDuty,patientId);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Add)]
        public ActionResult Add([Bind]CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            return Json(mediatorService.AddCommunicationNote(communicationNote));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(communicationNoteService.GetCommunicationNote(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Edit)]
        public ActionResult Edit(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var communicationNote = communicationNoteService.GetCommunicationNoteEdit(patientId, Id);
            return PartialView(communicationNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Edit)]
        public ActionResult Update(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            return Json(mediatorService.UpdateCommunicationNote(communicationNote));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.Delete)]
        public ActionResult Delete(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            var viewData = communicationNoteService.ToggleCommunicationNote(patientId, Id, true);
            if (viewData.isSuccessful)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in Deleting the data.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.ViewList)]
        public ActionResult List()
        {
            var viewData = communicationNoteService.GetCommunicationNotesViewData(Guid.Empty, true, Guid.Empty, Guid.Empty, (int)PatientStatus.Active, DateTime.Now.AddDays(-60), DateTime.Now);
            viewData.SortColumn = "DisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.ViewList)]
        public ActionResult List(Guid PatientId)
        {
            var viewData = communicationNoteService.GetCommunicationNotesViewData(Guid.Empty, true, PatientId, null, 0, DateTime.Now.AddDays(-60), DateTime.Now);
            viewData.SortColumn = "UserDisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.CommunicationNote, PermissionActions.ViewList)]
        public ActionResult ListContent(Guid PatientId, Guid? EpisodeId, Guid BranchId, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            var viewData = communicationNoteService.GetCommunicationNotesViewData(BranchId, false, PatientId, EpisodeId, Status, StartDate, EndDate);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            } return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, new ParentPermission[] { ParentPermission.Schedule, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<ComNotePdf>(new ComNotePdf(communicationNoteService.GetCommunicationNotePrint(eventId, patientId)), "CommunicationNote", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<ComNotePdf>(new ComNotePdf(communicationNoteService.GetCommunicationNotePrint(eventId, patientId)), "CommunicationNote", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<ComNotePdf>(new ComNotePdf(communicationNoteService.GetCommunicationNotePrint(eventId, patientId)), "CommunicationNote");
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your communication note could not be approved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = communicationNoteService.ProcessCommunicationNotes(patientId, eventId, "Approve", null);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your communication note has been successfully approved.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(Guid patientId, Guid eventId, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your communication note could not be returned." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = communicationNoteService.ProcessCommunicationNotes(patientId, eventId, "Return", reason);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your communication note has been successfully returned.";
                }
            }
            return Json(viewData);
        }

        #endregion
    }
}
