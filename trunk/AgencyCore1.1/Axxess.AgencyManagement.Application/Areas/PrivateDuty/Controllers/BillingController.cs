﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Services;
    using ViewData;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.iTextExtension;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Application.Common;
    
    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly PrivateDutyBillingService billingService;
        private readonly IAgencyService agencyService;
        private readonly PrivateDutyEpisodeService epiosdeService;
        private readonly PrivateDutyMediatorService mediatorService;


        public BillingController(IPatientService patientService, PrivateDutyBillingService billingService, IAgencyService agencyService, PrivateDutyEpisodeService epiosdeService, PrivateDutyMediatorService mediatorService)
        {
            this.patientService = patientService;
            this.billingService = billingService;
            this.agencyService = agencyService;
            this.epiosdeService = epiosdeService;
            this.mediatorService = mediatorService;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimsPdf(Guid BranchId, int InsuranceId, string ParentSortType, int ClaimType)
        {
            if (ParentSortType.IsNullOrEmpty())
            {
                ParentSortType = "branch";
            }
            var location = agencyService.AgencyNameWithLocationAddress(BranchId) ?? new LocationPrintProfile();
            var bill = new List<Bill>();
            if (Enum.IsDefined(typeof(ClaimTypeSubCategory), ClaimType))
            {
                bill = billingService.AllUnProcessedBillList(BranchId, InsuranceId, (ClaimTypeSubCategory)ClaimType, ParentSortType, true);
            }
            return FileGenerator.Pdf<BillingClaimsPdf>(new BillingClaimsPdf(bill ?? new List<Bill>(), location), "Claims");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimsXls(Guid BranchId, int InsuranceId, string ParentSortType, string ColumnSortType, int ClaimType)
        {
            if (ParentSortType.IsNullOrEmpty())
            {
                ParentSortType = "branch";
            }
            var bill = new List<Bill>();
            if (Enum.IsDefined(typeof(ClaimTypeSubCategory), ClaimType))
            {
                bill = billingService.AllUnProcessedBillList(BranchId, InsuranceId, (ClaimTypeSubCategory)ClaimType, ParentSortType, true);
            }
            var export = new ClaimsExport(bill ?? new List<Bill>());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ClaimSummary.xls");
        }

        #region Managed Claims

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleManagedANSI(Guid Id)
        {
            return Json(billingService.GenerateManagedClaimSingle(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateANSI(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId, string ClaimType)
        {
            return Json(billingService.GenerateDownload(ClaimSelected, BranchId, InsuranceId, ClaimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitClaimDirectly(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId, string ClaimType)
        {
            return Json(billingService.GenerateDirectly(ClaimSelected, BranchId, InsuranceId,ClaimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSummary(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            var claimsToGenerate = billingService.ClaimToGenerate(ClaimSelected, BranchId, PrimaryInsurance, ClaimTypeSubCategory.ManagedCare.ToString());
            claimsToGenerate.IsElectronicSubmssion = false;
            return PartialView("ClaimSummary", claimsToGenerate);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedCreateClaims()
        {
            return PartialView("Managed/Claims/CreateClaims", billingService.GetManagedCreateClaims() ?? new Bill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedGrid(Guid branchId, int insuranceId)
        {
            return PartialView("Managed/ManagedGrid", billingService.GetClaimsWithInsuranceAndInsuranceInfo(ClaimTypeSubCategory.ManagedCare, branchId, insuranceId,ParentPermission.ManagedCareClaim));
        }

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedHistory()
        {
            var viewData = billingService.GetPayorAndBranchFilterWithPermission(ParentPermission.ManagedCareClaim, PermissionActions.ViewClaimHistory, null);
            viewData.Status = (int)PatientStatus.Active;
            viewData.ViewListPermissions = viewData.AvailableService;
            return PartialView("Managed/Center/Layout", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsActivity(Guid PatientId, int InsuranceId)
        {
            if (PatientId.IsEmpty())
            {
                return View(new GridModel(new List<ManagedClaimLean>()));
            }
            return View(new GridModel(billingService.GetManagedClaimsActivity(PatientId, InsuranceId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaim(Guid patientId)
        {
            return PartialView("Managed/Claim/New", billingService.GetNewMangedClaimViewData(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateManagedClaim(Guid PatientId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            return Json(billingService.AddManagedClaim(PatientId, StartDate, EndDate, InsuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSnapShotClaimInfo(Guid PatientId, Guid ClaimId)
        {
            return PartialView("Managed/Center/Info", billingService.GetManagedClaimSnapShotInfo(PatientId, ClaimId));
        }

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaim(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Verify", billingService.GetManagedClaimForVerify(Id, patientId, AgencyServices.PrivateDuty));
        }

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimInfo(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Info", billingService.GetManagedClaimInfoLatest(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInfoVerify(ManagedClaim claim)
        {
            return Json(billingService.ManagedVerifyInfo(claim));
        }

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimInsurance(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Insurance", billingService.ManagedClaimWithInsurance(Id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimReloadInsurance(Guid Id, Guid PatientId)
        {
            return PartialView("Managed/Claim/Insurance", billingService.UpdateManagedCareForInsuranceReload(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimReloadRates(Guid Id, Guid PatientId)
        {
            return Json(billingService.UpdateManagedCareRatesForReload(Id, PatientId));
        }

        [GridAction]
        public ActionResult ManagedClaimInsuranceRates(Guid Id)
        {
            return View(new GridModel(billingService.ManagedClaimInsuranceRates(Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimNewBillData(Guid Id, int PayorType, int PrimaryInsuranceId)
        {
            return PartialView("BillData/New", billingService.GetClaimNewBillData<ManagedClaimInsurance>(Id, PayorType, PrimaryInsuranceId, ClaimTypeSubCategory.ManagedCare));
        }

        /// <summary>
        /// Id is a claim Id
        /// RateId is the rate id 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="RateId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEditBillData(Guid Id, int RateId, int PayorType, int PrimaryInsuranceId)
        {
            return PartialView("BillData/Edit", billingService.GetClaimBillDataForEdit<ManagedClaimInsurance>(Id, RateId, PayorType,PrimaryInsuranceId, ClaimTypeSubCategory.ManagedCare));
        }

        /// <summary>
        /// Id is a claim Id
        /// RateId is the rate id 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="RateId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimDeleteBillData(Guid Id, int RateId, int PayorType)
        {
            return Json(billingService.ClaimDeleteBillData<ManagedClaimInsurance>(Id, RateId,PayorType,ClaimTypeSubCategory.ManagedCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAddBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            return Json(billingService.ClaimAddBillData<ManagedClaimInsurance>(chargeRate, ClaimId,ClaimTypeSubCategory.ManagedCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimUpdateBillData(ChargeRate chargeRate, Guid ClaimId, int PayorType)
        {
            return Json(billingService.ClaimUpdateBillData<ManagedClaimInsurance>(chargeRate, ClaimId,PayorType,ClaimTypeSubCategory.ManagedCare));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInsuranceVerify(ManagedClaim claim)
        {
            return Json(billingService.ManagedVerifyInsurance(claim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimVisit(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Visit", billingService.GetManagedClaimVisit(Id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> Visit)
        {
            return Json(billingService.ManagedVisitVerify(Id, patientId, Visit));
        }

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSupply(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Supply", billingService.GetManagedClaimSupply(Id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSupplyVerify(Guid Id, Guid PatientId)
        {
            return Json(billingService.ManagedVisitSupply(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSummary(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Summary", billingService.GetManagedClaimSummary(Id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedComplete(Guid Id, Guid PatientId, string Total)
        {
            return Json(billingService.ManagedComplete(Id, PatientId, Total.IsNotNullOrEmpty() ? Total.ToCurrency() : 0));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ManagedClaimPdf(Guid patientId, Guid Id)
        {
            return FileGenerator.GetClaimPDF(billingService.GetManagedClaimInvoiceFormViewData(patientId, Id));
        }
      
        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult ManagedUB04Pdf(Guid patientId, Guid Id)
        //{
        //    var viewData = billingService.GetInvoiceFormViewData(patientId, Id);
        //    return FileGenerator.Pdf<ManagedUB04Pdf>(new ManagedUB04Pdf(viewData), viewData.InvoiceType.GetDescription());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult ManagedHCFA1500Pdf(Guid patientId, Guid Id)
        //{
        //    var viewData = billingService.GetInvoiceFormViewData(patientId, Id);
        //    return FileGenerator.Pdf<HCFA1500Pdf>(new HCFA1500Pdf(viewData), viewData.InvoiceType.GetDescription());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult InvoicePdf(Guid patientId, Guid Id, bool isForPatient)
        //{
        //    var viewData = billingService.GetInvoiceFormViewData(patientId, Id);
        //    return FileGenerator.Pdf<InvoicePdf>(new InvoicePdf(viewData), viewData.InvoiceType.GetDescription());
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaim(Guid patientId, Guid id)
        {
            return Json(billingService.DeleteManagedClaim(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaim(Guid patientId, Guid id)
        {
            return PartialView("Managed/Claim/Update", billingService.GetManagedClaim(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimStatus(Guid Id, Guid PatientId, int Status, string Comment, string ClaimDateValue)
        {
            return Json(billingService.UpdateProccesedManagedClaimStatus(PatientId, Id, ClaimDateValue, Status, Comment));
        }

        #region Managed Claim Payments

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPayments(Guid claimId, Guid patientId)
        {
            var allPermission = Current.CategoryService(AgencyServices.PrivateDuty, ParentPermission.ManagedCareClaim, new int[] { (int)PermissionActions.Edit });
            return PartialView("Managed/Payment/List", new GridViewData() { Id = claimId, Service = AgencyServices.PrivateDuty, EditPermissions = allPermission.GetOrDefault((int)PermissionActions.Edit) });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllManagedClaimPaymentsGrid(Guid patientId)
        {
            return View(new GridModel(billingService.GetManagedClaimPaymentsByPatient(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaimPayment(Guid Id)
        {
            return PartialView("Managed/Payment/New", billingService.GetManagedClaim(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddManagedClaimPayment(ManagedClaimPayment payment)
        {
            return Json(billingService.AddManagedClaimPayment(payment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimPayment(Guid id)
        {
            return PartialView("Managed/Payment/Edit", billingService.GetManagedClaimPayment(id));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPaymentsGrid(Guid claimId)
        {
            return View(new GridModel(billingService.GetManagedClaimPaymentsWithInsurance(claimId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaimPayment(Guid patientId, Guid claimId, Guid id)
        {
            return Json(billingService.DeleteManagedClaimPayment(patientId, claimId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimPaymentDetails(ManagedClaimPayment claimPayment)
        {
            return Json(billingService.UpdateManagedClaimPayment(claimPayment));
        }

        #endregion

        #region Managed Claim Adjustments

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAdjustments(Guid claimId, Guid patientId)
        {
            var allPermission = Current.CategoryService(AgencyServices.PrivateDuty, ParentPermission.ManagedCareClaim, new int[] { (int)PermissionActions.Edit });
            return PartialView("Managed/Adjustment/List", new GridViewData() { Id = claimId, Service = AgencyServices.PrivateDuty, EditPermissions = allPermission.GetOrDefault((int)PermissionActions.Edit) });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAdjustmentsGrid(Guid claimId)
        {
            return View(new GridModel(billingService.GetManagedClaimAdjustments(claimId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaimAdjustment(Guid Id)
        {
            return PartialView("Managed/Adjustment/New", billingService.GetManagedClaim(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddManagedClaimAdjustment(ManagedClaimAdjustment managedClaimAdjustment)
        {
            return Json(billingService.AddManagedClaimAdjustment(managedClaimAdjustment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimAdjustment(Guid Id)
        {
            return PartialView("Managed/Adjustment/Edit", billingService.GetManagedClaimAdjustment(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimAdjustmentDetails(ManagedClaimAdjustment managedClaimAdjustment)
        {
            return Json(billingService.UpdateManagedClaimAdjustment(managedClaimAdjustment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaimAdjustment(Guid patientId, Guid claimId, Guid id)
        {
            return Json(billingService.DeleteManagedClaimAdjustment(patientId, claimId, id));
        }

        #endregion

        #endregion

        #region Invoices

      

        #endregion

        #region ClaimSupplies

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillable(Guid Id, Guid patientId, string Type)
        {
            return View(new GridModel(billingService.GetSuppliesByFlag(Id, patientId, Type, true)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyUnBillable(Guid Id, Guid patientId, string Type)
        {
            return View(new GridModel(billingService.GetSuppliesByFlag(Id, patientId, Type, false)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeSupplyBillStatus(Guid Id, Guid PatientId, List<int> BillingId, bool IsBillable, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply bill status change is unsuccessful." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (BillingId != null && BillingId.Count > 0)
                {
                    if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                    {
                        if (billingService.ChangeStatusOfSuppliesManagedClaim(PatientId, Id, BillingId, IsBillable))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = string.Format("{0} successfuly {1}.", BillingId.Count > 0 ? "Supplies were" : "Supply was", IsBillable ? " marked as billable" : "marked as non-billable");
                        }
                    }
                    else if ("Invoice".Equals(Type))
                    {
                        throw new NotImplementedException();
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupplyBillable(Guid ClaimId, Guid PatientId, int Id, string Type)
        {
            var viewData = new ClaimEditSupplyViewData(ClaimId, PatientId, Type, new Supply(), AgencyServices.PrivateDuty);
            if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
            {
                viewData = billingService.GetSupplyManagedClaim(PatientId, ClaimId, Id);
            }
            else if ("Invoice".Equals(Type))
            {
                throw new NotImplementedException();
            }
            return View("Supply/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillableUpdate(Guid ClaimId, Guid patientId, Supply supply, string Type)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to be updated." };
            var suppliesEdited = new List<Supply>();
            if (!ClaimId.IsEmpty() && !patientId.IsEmpty())
            {
                if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                {
                    if (billingService.EditSupplyManagedClaim(patientId, ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully updated.";
                    }
                }
                else if ("Invoice".Equals(Type))
                {
                    throw new NotImplementedException();
                }
            }
            return Json(viewData);
        }

        //Id is a claim Id
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSupplyBillable(Guid Id, Guid patientId, string Type)
        {
            return View("Supply/New", new ClaimNewSupplyViewData(Id, patientId, Type, AgencyServices.PrivateDuty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillableAdd(Guid ClaimId, Guid patientId, string Type, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to be added." };
            if (!ClaimId.IsEmpty() && !patientId.IsEmpty())
            {

                if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                {
                    if (billingService.AddSupplyManagedClaim(patientId, ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully added.";
                    }
                }
                else if ("Invoice".Equals(Type))
                {
                    throw new NotImplementedException();
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuppliesDelete(Guid Id, Guid PatientId, List<int> BillingId, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supplies were not deleted." };

            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (BillingId != null && BillingId.Count > 0)
                {
                    if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                    {
                        if (billingService.DeleteSupplyManagedClaim(PatientId, Id, BillingId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supplies were successfuly deleted.";
                        }
                    }
                    else if ("Invoice".Equals(Type))
                    {
                        throw new NotImplementedException();
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #endregion
    }
}