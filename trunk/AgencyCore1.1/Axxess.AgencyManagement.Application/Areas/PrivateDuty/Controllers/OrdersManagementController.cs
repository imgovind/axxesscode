﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using Telerik.Web.Mvc;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.ViewData;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OrdersManagementController : BaseController
    {
        #region Constructor

        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly PrivateDutyOrderManagmentService orderManagmentService;
        public OrdersManagementController(IAgencyService agencyService, PrivateDutyOrderManagmentService orderManagmentService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(orderManagmentService, "orderManagmentService");
            this.agencyService = agencyService;
            this.orderManagmentService = orderManagmentService;
            this.patientService = patientService;
        }

        #endregion

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ViewOrderToBeSent)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ToBeSent()
        {
            return PartialView(orderManagmentService.GetOrdersGridViewData(PermissionActions.ViewOrderToBeSent, true));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ViewOrderToBeSent)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ToBeSentContent(Guid BranchId, bool sendAutomatically, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(orderManagmentService.GetOrdersToBeSent(BranchId, sendAutomatically, StartDate, EndDate)));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ViewOrderHistory)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult History()
        {
            return PartialView(orderManagmentService.GetOrdersGridViewData(PermissionActions.ViewOrderHistory, true));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ViewOrderHistory)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryList(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(orderManagmentService.GetOrdersByStatus(BranchId, StartDate, EndDate, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature }, false, true)));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.SendToPhysician)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkAsSent(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            return Json(orderManagmentService.MarkOrdersAsSent(formCollection));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ReceiveFromPhysician)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReceiveOrder(Guid id, Guid patientId, string type)
        {
            return PartialView(orderManagmentService.GetOrderForReceiving(patientId, id, type));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ReceiveFromPhysician)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkAsReturned(Guid id, Guid patientId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            return Json(orderManagmentService.MarkOrderAsReturned(patientId, id, type, receivedDate, physicianSignatureDate));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryEdit(Guid id, Guid patientId, string type)
        {
            return PartialView(orderManagmentService.GetOrderForHistoryEdit(patientId, id, type));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Order order, OrderType type)
        {
            var viewData = Validate<JsonViewData>(
                    new Validation(() => order.Id.IsEmpty() || order.PatientId.IsEmpty() || order.EpisodeId.IsEmpty(), "Order could not be updated. Please Try again."),
                    new Validation(() => !order.ReceivedDate.IsValid(), "Received date must be a valid date."),
                    new Validation(() => !order.SendDate.IsValid(), "Send date must be a valid date."),
                    new Validation(() => !order.PhysicianSignatureDate.IsValid(), "Physician Signature date must be a valid date.")
                );
            if (viewData.isSuccessful)
            {
                return Json(orderManagmentService.UpdateOrderDates(order.Id, order.PatientId, type, order.ReceivedDate, order.SendDate, order.PhysicianSignatureDate));
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ViewOrderSent)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingSignature()
        {
            return PartialView(orderManagmentService.GetOrdersGridViewData(PermissionActions.ViewOrderSent, true));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OrderManagement, PermissionActions.ViewOrderSent)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingSignatureContent(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(orderManagmentService.GetOrdersByStatus(BranchId, StartDate, EndDate, ScheduleStatusFactory.OrdersPendingPhysicianSignature(), true, false).Select(s => new { s.Id, s.PatientId, s.PatientName, s.Text, s.PhysicianName, s.SendDate, s.Service, s.CreatedDate, s.Number, s.Type, s.CanUserEdit })));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Approve(Guid eventId, Guid patientId, string orderType)
        {
            return Json(orderManagmentService.UpdatePhysicianOrderAndPOCStatus(eventId, patientId, orderType, "Approve" , null));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Return(Guid eventId, Guid patientId, string orderType, string reason)
        {
            return Json(orderManagmentService.UpdatePhysicianOrderAndPOCStatus(eventId, patientId, orderType, "Return", reason));
        }

        #region Patient Order History

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHistory(Guid patientId)
        {
            var viewData = new GridViewData { Service = AgencyServices.PrivateDuty, Id = patientId, AvailableService = Current.AcessibleServices };
            viewData.DisplayName = patientService.GetPatientNameById(patientId);
            var actions = new int[] { (int)PermissionActions.Export };
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Orders, actions);
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.EditPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
            }
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHistoryGrid(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(orderManagmentService.GetPatientOrders(patientId, StartDate, EndDate)));
        }

        #endregion
    }
}
