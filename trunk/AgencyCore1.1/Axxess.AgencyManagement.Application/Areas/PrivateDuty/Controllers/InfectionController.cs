﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Telerik.Web.Mvc;
    using iTextExtension;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Helpers;
    
    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
   public class InfectionController : BaseController
    {
        #region Constructor

        private readonly PrivateDutyInfectionService infectionService;
        private readonly PrivateDutyMediatorService mediatorService;
        private readonly IUserService userService;

        public InfectionController(
            PrivateDutyInfectionService infectionService,
            PrivateDutyMediatorService mediatorService,
            IUserService userService)
        {
            Check.Argument.IsNotNull(infectionService, "infectionService");
            Check.Argument.IsNotNull(mediatorService, "mediatorService");
            Check.Argument.IsNotNull(userService, "userService");


            this.infectionService = infectionService;
            this.mediatorService = mediatorService;
            this.userService = userService;
        }

        #endregion

        #region Infection Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.PrivateDuty);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.ManageInfectionReport, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.PrivateDuty, patientId);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.ManageInfectionReport, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");
            var viewData = new JsonViewData();
            if (infection.IsValid())
            {
                var validationResults = infection.SignValidation(ServiceHelper.IsSignatureCorrect(Current.UserId, infection.SignatureText));
                if (validationResults.IsSuccessful)
                {
                    viewData = infectionService.AddInfection(infection, validationResults.IsSigned);
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(infectionService.GetInfectionForEdit(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");
            var viewData = new JsonViewData();
            if (infection.IsValid())
            {
                var validationResults = infection.SignValidation(ServiceHelper.IsSignatureCorrect(Current.UserId, infection.SignatureText));
                if (validationResults.IsSuccessful)
                {
                    viewData = infectionService.UpdateInfection(infection, validationResults.IsSigned);
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, new ParentPermission[] { ParentPermission.ManageInfectionReport, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new InfectionReportPdf(infectionService.GetInfectionReportPrint(patientId, eventId)), "InfectionLog", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<InfectionReportPdf>(new InfectionReportPdf(infectionService.GetInfectionReportPrint(patientId, eventId)), "InfectionLog", Response);
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<InfectionReportPdf>(new InfectionReportPdf(infectionService.GetInfectionReportPrint(patientId, eventId)), "InfectionLog");
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Infection Log could not be approved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = infectionService.ProcessInfections(patientId, eventId, "Approve", null);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your Infection Log has been successfully approved.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(Guid patientId, Guid eventId, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Infection Log could not be returned." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = infectionService.ProcessInfections(patientId, eventId, "Return", reason);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your Infection Log has been successfully returned.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            return Json(infectionService.ToggleInfection(patientId, id,true));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = infectionService.GetServiceAndLocationViewData(ParentPermission.ManageInfectionReport, PermissionActions.ViewList, new List<PermissionActions> { PermissionActions.Add, PermissionActions.Export }, false);
            return PartialView("List", viewData);
        }

        [GridAction]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ListContent()
        {
            return View(new GridModel(infectionService.GetInfections(Current.AgencyId,false)));
        }

        #endregion
    }
}
