﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        //Services
        private readonly PrivateDutyAssessmentService assessmentService;
        private readonly PrivateDutyBillingService billingService;
        private readonly IReferralService referralService;
        private readonly PrivateDutyOrderManagmentService orderManagmentService;
        private readonly PrivateDutyTaskService scheduleService;
        private readonly PrivateDutyInfectionService infectionService;
        private readonly PrivateDutyIncidentAccidentService incidentAccidentService;
        private readonly PrivateDutyCommunicationNoteService communicationNoteService;
       // private readonly IPayrollService payrollService;
        private readonly IUserService userService;
        private readonly IPatientService patientService;
        private readonly IPhysicianService physicianService;
        private readonly IAgencyService agencyService;
        private readonly PrivateDutyPatientProfileService patientProfileService;

        public ExportController(
            PrivateDutyOrderManagmentService orderManagmentService,
            PrivateDutyTaskService scheduleService,
            PrivateDutyAssessmentService assessmentService,
            IUserService userService,
            IPatientService patientService,
           // IPayrollService payrollService,
            IPhysicianService physicianService,
            PrivateDutyBillingService billingService,
            IReferralService referralService,
            PrivateDutyInfectionService infectionService,
            PrivateDutyIncidentAccidentService incidentAccidentService,
            PrivateDutyCommunicationNoteService communicationNoteService,
            IAgencyService agencyService,
            PrivateDutyPatientProfileService patientProfileService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");

            this.assessmentService = assessmentService;
            this.userService = userService;
            this.patientService = patientService;
           // this.payrollService = payrollService;
            this.billingService = billingService;
            this.scheduleService = scheduleService;
            this.orderManagmentService = orderManagmentService;
            this.referralService = referralService;
            this.infectionService = infectionService;
            this.communicationNoteService = communicationNoteService;
            this.incidentAccidentService = incidentAccidentService;
            this.physicianService = physicianService;
            this.agencyService = agencyService;
            this.patientProfileService = patientProfileService;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Contacts()
        {
            var contacts = agencyService.GetContacts();
            var export = new ContactExporter(contacts);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Contacts_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Hospitals()
        {
            var hospitals = agencyService.GetHospitals();
            var export = new HosptialExporter(hospitals);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Hospitals_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Physicians()
        {
            var physicians = physicianService.GetAgencyPhysiciansWithPecosVerification();
            var export = new PhysicianExporter(physicians);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Physicians_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Referrals(int serviceId)
        {
            var list = new List<Referral>();
            if (serviceId == (int)AgencyServices.HomeHealth || serviceId == (int)AgencyServices.PrivateDuty)
            {
                list = referralService.GetReferralByStatus(Current.AgencyId, serviceId == (int)AgencyServices.HomeHealth ? AgencyServices.HomeHealth : AgencyServices.PrivateDuty, ReferralStatus.Pending);
            }
            var export = new ReferralExporter(list ?? new List<Referral>());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Referrals_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Patients(Guid BranchId, int Status)
        {
            var patientList = patientProfileService.GetPatients(BranchId, Status) ?? new List<PatientData>();
            var export = new PatientExporter(patientList);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Patients_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PendingAdmissionsXls(Guid BranchId)
        {
            var patientList = patientProfileService.GetPendingPatients(BranchId,true);
            var export = new PendingAdmissionsExporter(patientList);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "PendingAdmissions.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult NonAdmitExport(int ServiceId)
        {
            var patientList = patientProfileService.GetNonAdmits(true) ?? new List<NonAdmit>();
            var export = new NonAdmitExporter(patientList);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "NonAdmitPatients.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult DeletedPatients(Guid BranchId)
        {
            var patientList =patientProfileService.GetDeletedPatients(BranchId,false);
            var export = new DeletedPatientExporter(patientList);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("DeletedPatients_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Users(int status)
        {
            var users = userService.GetUsersByStatus(Guid.Empty, status);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Users_{0}.xls", DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ScheduleList()
        //{
        //    var export = new UserScheduleExporter(scheduleService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ScheduleList_{0}.xls", DateTime.Now.Ticks));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleList(DateTime startDate, DateTime endDate)
        {
            var export = new PrivateDutyMyScheduleExporter(scheduleService.GetScheduleTasksBetweenDatesToPrint(Current.UserId, Guid.Empty, Current.UserFullName, startDate, endDate));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PrivateDutyScheduleList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supplies()
        {
            var export = new SupplyExporter(agencyService.GetSupplies());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Supplies_{0}.xls", DateTime.Now.Ticks));
        }


        [FileDownload]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult QAScheduleList(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var export = new QAScheduleExporter(scheduleService.GetCaseManagerSchedule(branchId, status, startDate, endDate,true,false).OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("QAScheduleList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLog()
        {
            var patientList = patientService.GetHospitalizationLogs(Current.AgencyId);
            var export = new HospitalizationLogExporter(patientList ?? new List<PatientHospitalizationData>());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("HospitalizationLog_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHospitalizationLog(Guid patientId)
        {
            var data = patientService.GetHospitalizationViewData(patientId, true);
            var export = new PatientHospitalizationLogExporter(data);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("{0}_HospitalizationLog_{1}.xls", data != null ? data.PatientName : string.Empty, DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult PrintQueueList(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    string branchName = "All branches";
        //    if (BranchId != Guid.Empty)
        //    {
        //        branchName = LocationEngine.GetName(BranchId, Current.AgencyId);
        //    }
        //    var export = new PrintQueueExporter<ScheduleEvent>(branchName, scheduleService.GetPrintQueue(BranchId, StartDate, EndDate).OrderBy(p => p.PatientName).ToList());
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PrintQueue_{0}.xls", DateTime.Now.Ticks));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Authorizations(Guid patientId)
        {
            IList<Authorization> authorizations = patientProfileService.GetAuthorizationsWithBranchName(patientId);
            var export = new AuthorizationsExporter(authorizations);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Authorizations_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OrdersHistory(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var orders = orderManagmentService.GetOrdersByStatus(BranchId, StartDate, EndDate, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature }, false, true);
            var export = new OrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Orders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PatientOrderHistory(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            var orders = orderManagmentService.GetPatientOrders(patientId, StartDate, EndDate);
            var export = new PatientOrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PatientOrders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OrdersPendingSignature(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var orders = orderManagmentService.GetOrdersByStatus(BranchId, StartDate, EndDate, ScheduleStatusFactory.OrdersPendingPhysicianSignature(), true, false);
            var export = new PendingSignatureOrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersPendingPhysicianSignature_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime StartDate, DateTime EndDate)
        {
            var orders = orderManagmentService.GetOrdersToBeSent(BranchId, sendAutomatically, StartDate, EndDate);
            var export = new OrdersToBeSentExporter(orders.ToList(), sendAutomatically, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersToBeSent_{0}.xls", DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult RecertsPastDue(Guid BranchId, int insuranceId, DateTime StartDate)
        //{
        //    var recertEvents = scheduleService.GetRecertsPastDue(BranchId, insuranceId, StartDate, DateTime.Now, false, 0);
        //    var export = new PastDueRecertExporter(recertEvents.ToList(), StartDate);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PastDueRecerts_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public FileResult RecertsUpcoming(Guid BranchId, int InsuranceId)
        //{
        //    var recertEvents = scheduleService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24), false, 0);
        //    var export = new UpcomingRecertExporter(recertEvents.ToList());
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("UpcomingRecerts_{0}.xls", DateTime.Now.Ticks));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Templates()
        {
            var templates = agencyService.GetTemplates();
            var export = new TemplateExporter(templates.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Templates_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Incidents()
        {
            var incidents = incidentAccidentService.GetIncidents(Current.AgencyId,true);
            var export = new IncidentExporter(incidents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Incident_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Infections()
        {
            var infections = infectionService.GetInfections(Current.AgencyId,true);
            var export = new InfectionExporter(infections.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Infections_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CommunicationNotes(Guid PatientId, Guid? EpisodeId, Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var communicationNotes = PatientId.IsEmpty() ? communicationNoteService.GetCommunicationNotes(BranchId, Status, StartDate, EndDate) : communicationNoteService.GetCommunicationNotes(PatientId, ref EpisodeId, true);
            var export = new CommunicationNoteExporter(communicationNotes.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("CommunicationNotes_{0}.xls", DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public FileResult ScheduleDeviations(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var scheduleEvents = reportService.GetScheduleDeviation(BranchId, StartDate, EndDate);
        //    var export = new ScheduleDeviationExporter(scheduleEvents.ToList(), StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ScheduleDeviations_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public FileResult PayrollSummary(DateTime StartDate, DateTime EndDate, string payrollStatus)
        //{
        //    var payroll = payrollService.GetSummary(StartDate, EndDate, payrollStatus);
        //    var export = new PayrollSummaryExport(payroll.ToList(), StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollSummary_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public FileResult PayrollDetail(Guid UserId, DateTime StartDate, DateTime EndDate, string payrollStatus)
        //{
        //    var detail = payrollService.GetVisits(UserId, StartDate, EndDate, payrollStatus);
        //    var export = new PayrollDetailExport(detail);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetail_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public FileResult PayrollDetails(DateTime StartDate, DateTime EndDate, string payrollStatus)
        //{
        //    var details = payrollService.GetDetails(StartDate, EndDate, payrollStatus);
        //    var export = new PayrollDetailsExport(details, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetails_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult SubmittedBatchClaims(DateTime StartDate, DateTime EndDate, string ClaimType)
        //{
        //    var claims = billingService.ClaimDatas(StartDate, EndDate, ClaimType);
        //    var export = new SubmittedBatchClaimsExport(claims.ToList(), StartDate, EndDate, ClaimType);
        //    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("SubmittedBatchClaims_{0}.xls", DateTime.Now.Ticks));
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public FileResult SubmittedClaimDetails(int Id)
        //{
        //    var claims = billingService.GetSubmittedBatchClaims(Id);
        //    var export = new SubmittedClaimDetailsExporter(claims,Id);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var visits = scheduleService.GetMissedScheduledEvents(Current.AgencyId, BranchId, StartDate, EndDate, true,false).OrderBy(p => p.PatientName).ToList();
            var export = new MissedVisitsExporter(visits, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("MissedVisits_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AdjustmentCodes()
        {
            var codes = agencyService.GetAdjustmentCodes();
            var export = new AdjustmentCodesExporter(codes.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AdjustmentCodes_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UploadTypes()
        {
            var classes = agencyService.GetUploadTypes();
            var export = new UploadTypesExporter(classes.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("UploadTypes_{0}.xls", DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult PatientsAndVisitsByAgeReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new PatientsAndVisitsByAgeExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult DischargesByReasonReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new DischargesByReasonExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult VisitsByPrimaryPaymentSourceReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new VisitsByPrimaryPaymentSourceExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult VisitsByStaffTypeReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new VisitsByStaffTypeExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult AdmissionsByReferralSourceReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new AdmissionsByReferralSourceExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult PatientsVisitsByPrincipalDiagnosisReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new PatientsVisitsByPrincipalDiagnosisExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        #endregion
    }
}
