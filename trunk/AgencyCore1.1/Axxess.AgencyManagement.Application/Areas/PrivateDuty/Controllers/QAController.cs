﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Services;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.Core.Enums;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class QAController : BaseController
    {
        #region Constructor

        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly PrivateDutyTaskService scheduleService;
        private readonly PrivateDutyMediatorService mediatorService;

        public QAController(PrivateDutyTaskService scheduleService, PrivateDutyMediatorService mediatorService, IAgencyService agencyService, IPatientService patientService)
        {
            this.scheduleService = scheduleService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.mediatorService = mediatorService;
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            var viewData = scheduleService.GetCaseManagerScheduleContent(Guid.Empty, true, 1, DateTime.Today.AddDays(-7), DateTime.Today) ?? new QAViewData();
            viewData.GroupName = "EventDate";
            viewData.SortColumn = "DisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Content(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate, string GroupName, string SortParams)
        {
            var viewData = scheduleService.GetCaseManagerScheduleContent(BranchId,false, Status, StartDate, EndDate) ?? new QAViewData();
            viewData.GroupName = GroupName;
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(viewData);
        }


        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(List<string> CustomValue)
        {
            return Json(mediatorService.BulkUpdate(CustomValue, "Approve"));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(List<string> CustomValue)
        {
            return Json(mediatorService.BulkUpdate(CustomValue, "Return"));
        }

        //Andrew's Code for the new QA Center
        //[AcceptVerbs(HttpVerbs.Post)]
        //public LargeJsonResult CaseManagementGrid(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        //{
        //    return new LargeJsonResult
        //    {
        //        MaxJsonLength = int.MaxValue,
        //        Data = new GridModel(scheduleService.GetCaseManagerSchedule(BranchId, Status, StartDate, EndDate))
        //    };
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult CaseManagementCenter()
        //{
        //    return PartialView("QA/Center", new QaCenterViewData { });
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult CaseManagementData(Guid patientId)
        //{
        //    return PartialView("QA/Data", new QaCenterViewData() { Patient = patientService.GetPatientSnapshotInfo(patientId, false) });
        //}
    }
}
