﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using ViewData;
    using Services;

    using iTextExtension;

    using Telerik.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IAgencyService agencyService;
        private readonly IUserService userService;
        private readonly IPatientService patientService;
        private readonly IMessageService messageService;

        private readonly PrivateDutyMediatorService mediatorService;
        private readonly PrivateDutyTaskService taskService;
        private readonly PrivateDutyEpisodeService episodeService;
        private readonly PrivateDutyPatientProfileService patientProfileService;

        public ScheduleController(
            IAgencyService agencyService, 
            IUserService userService,
            IPatientService patientService,
            IMessageService messageService,
            PrivateDutyMediatorService mediatorService,
            PrivateDutyTaskService taskService,
            PrivateDutyEpisodeService episodeService,
            PrivateDutyPatientProfileService patientProfileService
            )
        {
            this.agencyService = agencyService;
            this.userService = userService;
            this.patientService = patientService;
            this.messageService = messageService;

            this.mediatorService = mediatorService;
            this.taskService = taskService;
            this.episodeService = episodeService;
            this.patientProfileService = patientProfileService;
            
        }

        #endregion

        #region Actions

        #region Schedule Center

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSelector(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = patientProfileService.GetPatientSelection(branchId, statusId, paymentSourceId, "", false);
            return PartialView("Center/PatientSelector", patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserSelector(Guid branchId, int statusId)
        {
            var userList = userService.GetUserSelectionList(branchId, statusId, (int)AgencyServices.PrivateDuty);
            return PartialView("Center/UserSelector", userList.OrderBy(u => u.LastName).ThenBy(u => u.FirstName).ToList());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var viewData = new UserTaskViewData { Service = AgencyServices.PrivateDuty };
            taskService.GetSchedulePermission(viewData);
            ViewData["Patients"] = patientProfileService.GetPatientSelection(Guid.Empty, (int)PatientStatus.Active, 0, "", false).OrderBy(p => p.LastName).ThenBy(p => p.ShortName).ToList();
            return PartialView("Center/Layout", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserSchedule()
        {
            var viewData = new UserTaskViewData {Service = AgencyServices.PrivateDuty};
             taskService.GetSchedulePermission(viewData);

            return PartialView("User/Layout", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = patientProfileService.GetPatientSelection(branchId, statusId, paymentSourceId, "", false);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterUserGrid(Guid branchId, int statusId)
        {
            var userList = userService.GetUserSelectionList(branchId, statusId, (int)AgencyServices.PrivateDuty);
            return View(new GridModel(userList.OrderBy(u => u.LastName).ThenBy(u => u.FirstName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult List(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            return Json(taskService.ActivityEpisodeTasksViewData(patientId, userId, startDate, endDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(Guid patientId, Guid userId, DateTime startDate, DateTime endDate, string sortParams)
        {
            var viewData = taskService.ActivityTaskViewData(patientId, userId, startDate, endDate);
            if (sortParams.IsNotNullOrEmpty())
            {
                var paramArray = sortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            else
            {
                viewData.ScheduleEvents = viewData.ScheduleEvents.OrderBy(o => o.DateIn).ToList();
            }
            return PartialView("Center/Grid", viewData);
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Print(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            var name = string.Empty;
            List<PrivateDutyScheduleTask> tasks = null;
            if (!patientId.IsEmpty())
            {
                var patient = patientService.GetPatientOnly(patientId);
                name = patient.DisplayNameWithMi;
            }
            else if (!userId.IsEmpty())
            {
                name = UserEngine.GetName(userId, Current.AgencyId);
            }
            tasks = taskService.GetScheduleTasksBetweenDatesToPrint(userId, patientId, name, startDate, endDate);


            return FileGenerator.Pdf<PrivateDutyCalendarPdf>(new PrivateDutyCalendarPdf(tasks, startDate, endDate, Current.AgencyName, name, userId.IsEmpty()), "PrivateDutyCalendar");
        }

        #endregion

        #region Schedule Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId, Guid userId, string startDate, string startTime)
        {
            NewTaskViewData viewData = new NewTaskViewData(patientId, userId);
            if (!patientId.IsEmpty())
            {
                viewData.PatientName = patientService.GetPatientNameById(patientId);
            }
            if (!userId.IsEmpty())
            {
                viewData.UserName = UserEngine.GetName(userId, Current.AgencyId);
            }
            viewData.StartDate = startDate.ToDateTimeOrMin();
            viewData.StartTime = startTime.ToDateTimeOrMin();
            return PartialView("Task/New", viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Create(PrivateDutyScheduleTask newTask)
        {
            return Json(mediatorService.AddTask(newTask));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditDetails(Guid Id, Guid patientId)
        {
            return PartialView("Detail/Edit", mediatorService.GetScheduledEventForDetail(patientId, Id));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.ViewDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateDetails([Bind] PrivateDutyScheduleTask editTask)
        {
            var viewData = new JsonViewData(false);

            if (editTask != null)
            {
                if (editTask.IsValid())
                {
                    viewData = mediatorService.UpdateScheduleEventDetails(editTask, Request.Files);
                }
                else
                {
                    viewData.errorMessage = editTask.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, new PermissionActions[] { PermissionActions.Edit, PermissionActions.ViewDetail })]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reschedule(Guid PatientId, Guid Id, DateTime EventStartTime, DateTime EventEndTime)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be moved." };
            if (!PatientId.IsEmpty() && !Id.IsEmpty())
            {
                return Json(mediatorService.RescheduleTask(PatientId, Id, EventStartTime, EventEndTime));
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be deleted." };

            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = mediatorService.ToggleTaskStatus(patientId, id, true);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was deleted successfully.";
                }
            }

            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Restore)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be restored." };

            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = mediatorService.ToggleTaskStatus(patientId, id, false);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was restored successfully.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Reopen)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid patientId, Guid id)
        {
            var viewData = new JsonViewData(false);

            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = mediatorService.Reopen(patientId, id);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was reopened successfully.";
                }
            }

            if (!viewData.isSuccessful && viewData.errorMessage.IsNullOrEmpty())
            {
                viewData.errorMessage = "The task could not be reopened.";
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateMultiple(Guid PatientId, Guid UserId, int DisciplineTask, [ModelBinder(typeof(CommaSeparatedModelBinder))]DateTime[] EventDates, DateTime EventStartTime, DateTime EventEndTime, bool? IsAllDay)
        {
            return Json(mediatorService.AddMultiDaySchedule(PatientId, UserId, DisciplineTask, EventDates.ToList(), EventStartTime, EventEndTime, IsAllDay.HasValue));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewMultiple(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            ViewData["PatientId"] = patientId.ToSafeString();
            ViewData["UserId"] = userId.ToSafeString();
            ViewData["StartDate"] = startDate.ToShortDateString();
            ViewData["EndDate"] = endDate.ToShortDateString();
            return PartialView("Task/NewMultiple");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteAsset(Guid patientId, Guid Id, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your asset was not deleted." };
            if (mediatorService.DeleteScheduleTaskAsset(patientId, Id, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your asset was successfully deleted.";
            }
            return Json(viewData);
        }

        public ActionResult Attachments( Guid patientId, Guid eventId)
        {
            if (patientId.IsEmpty() ||  eventId.IsEmpty())
            {
                return PartialView("Attachments", new AttachmentViewData());
            }
            else
            {
                return PartialView("Attachments", taskService.GetAttachments(patientId, eventId) ?? new AttachmentViewData());
            }
        }

        #endregion

        #region Reassign

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignModal(Guid id, Guid patientId)
        {
            var viewData = taskService.GetReassignViewData(id, patientId);
            return PartialView("Task/Reassign", viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reassign(Guid PatientId, Guid Id, Guid UserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be reassigned." };

            if (!Id.IsEmpty() && !PatientId.IsEmpty() && !UserId.IsEmpty())
            {
                viewData = taskService.Reassign(PatientId, Id, UserId);
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignMultipleModal(Guid patientId, Guid userId, DateTime? startDate, DateTime? endDate)
        {
            var viewData = new ReassignViewData();
            viewData.PatientId = patientId;
            viewData.UserId = userId;
            if (startDate.HasValue && endDate.HasValue)
            {
                viewData.StartDate = startDate.Value;
                viewData.EndDate = endDate.Value;
            }
            else
            {
                viewData.StartDate = DateUtilities.GetStartOfCurrentMonth();
                viewData.EndDate = DateUtilities.GetEndOfCurrentMonth();
            }
            return PartialView("Task/ReassignMultiple", viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReassignMultiple(Guid PatientId, Guid OldUserId, Guid NewUserId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The tasks could not be reassigned." };

            if (!OldUserId.IsEmpty() && !PatientId.IsEmpty() && !NewUserId.IsEmpty())
            {
                viewData = taskService.ReassignSchedules(PatientId, OldUserId, NewUserId, StartDate, EndDate);
                if (viewData.isSuccessful)
                {
                    string userName = UserEngine.GetName(NewUserId, Current.AgencyId);
                    string oldUserName = UserEngine.GetName(OldUserId, Current.AgencyId);
                    viewData.errorMessage = string.Format("The tasks assigned to {0} were reassigned to {1} successfully.", oldUserName, userName);
                }
            }

            return Json(viewData);
        }
       
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ScheduleWidget()
        {
            return Json(taskService.GetScheduleWidget(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTaskHistory(Guid patientId)
        {
            var viewData = patientService.GetPatientNameViewData( patientId);
            if (viewData != null)
            {
                viewData.Service = AgencyServices.PrivateDuty;
            }
            return PartialView("Task/DeletedTaskHistory", viewData);
        }

        private PatientNameViewData GetPatientNameViewData(Guid patientId)
        {
            var viewData = new PatientNameViewData();
            viewData.DisplayName = patientService.GetPatientNameById(patientId);
            viewData.PatientId = patientId;
            viewData.Service = AgencyServices.PrivateDuty;
            return viewData;
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTaskHistoryList(Guid patientId)
        {
            var tasks = taskService.GetDeletedTasks(patientId);
            return View(new GridModel(tasks));
        }

        #region Return Reasons

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReturnReason(Guid eventId,  Guid patientId)
        {
            var viewData = new JsonViewData();
            var comments = taskService.GetReturnComments(eventId, patientId);
            if (comments.IsNotNullOrEmpty())
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = comments;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddReturnReason(Guid eventId, Guid patientId, string comment)
        {
            return Json(taskService.AddReturnComments(patientId, eventId, comment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditReturnReason(int id, string comment)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to save." };
            if (taskService.EditReturnComments(id, comment))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteReturnReason(int id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to delete." };
            if (taskService.DeleteReturnComments(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment deleted successfully";
            }
            return Json(viewData);
        }

        #endregion

        #endregion

    }
}
