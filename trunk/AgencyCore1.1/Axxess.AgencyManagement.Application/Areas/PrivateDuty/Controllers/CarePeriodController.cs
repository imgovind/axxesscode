﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Web.Mvc;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.Core.Enums;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class CarePeriodController : BaseController
    {
        #region Constructor

        private readonly PrivateDutyEpisodeService episodeService;
        private readonly IPatientService patientService;

        public CarePeriodController(IPatientService patientService, PrivateDutyEpisodeService episodeService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(episodeService, "episodeService");
            this.patientService = patientService;
            this.episodeService = episodeService;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView(new NewEpisodeData() { IsFromMainMenu = true });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            //Get data from patient to display when creating the care period
            var episodeViewData = episodeService.GetNewEpisodeData(patientId, true);
            return PartialView(episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewContent(Guid patientId, bool isFromMainMenu)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            //Get data from patient to display when creating the care period
            var episodeViewData = episodeService.GetNewEpisodeData(patientId, true) ?? new NewEpisodeData();
            episodeViewData.IsFromMainMenu = isFromMainMenu;
            return PartialView("Content/New", episodeViewData);
        }
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Episode, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(PrivateDutyCarePeriod carePeriod)
        {
            var viewData = new JsonViewData(false, "Failed to add the care period.");
            if (carePeriod != null)
            {
                if (carePeriod.IsValid())
                {
                    viewData = episodeService.AddEpisodeAndCheckValidation(carePeriod, viewData);
                    viewData.PatientId = carePeriod.PatientId;
                    viewData.EpisodeId = carePeriod.Id;
                    viewData.IsCenterRefresh = true;
                    viewData.Service = AgencyServices.PrivateDuty.ToString();
                    viewData.ServiceId = (int)AgencyServices.PrivateDuty;
                }
                else
                {
                    viewData.errorMessage = carePeriod.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Episode, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            ViewData["PatientName"] = patientService.GetPatientNameById(patientId);
            var episode = episodeService.GetEpisodeOnly(id, patientId) ?? new PrivateDutyCarePeriod();
            return PartialView(episode);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Episode, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(PrivateDutyCarePeriod carePeriod)
        {
            return Json(episodeService.UpdateEpisode(carePeriod));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Episode, PermissionActions.Reactivate)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid id, Guid patientId)
        {
            return Json(episodeService.ActivateEpisode(patientId, id));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Episode, PermissionActions.Deactivate)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deactivate(Guid id, Guid patientId)
        {
            return Json(episodeService.DeactivateEpisode(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(Guid patientId)
        {
            var periods = episodeService.GetPatientEpisodesLean(patientId, null);
            ViewData["PatientName"] = patientService.GetPatientNameById(patientId);
            return PartialView(periods);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid patientId, bool? activeInactive)
        {
            var periods = episodeService.GetPatientEpisodesLean(patientId, activeInactive);
            ViewData["PatientName"] = patientService.GetPatientNameById(patientId);
            ViewData["ActiveInactive"] = activeInactive;
            return PartialView(periods);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RangeList(Guid patientId)
        {
            return Json(episodeService.EpisodeRangeList(patientId));
        }

        #endregion

        #region Logs

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Logs(Guid episodeId, Guid patientId)
        {
            return PartialView(Auditor.GetGeneralLogs(LogDomain.Patient, LogType.Episode, patientId, episodeId.ToString()));
        }

        #endregion
    }
}
