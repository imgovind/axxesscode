﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Telerik.Web.Mvc;
    using iTextExtension;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Helpers;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class IncidentController : BaseController
    {
        #region Constructor

        private readonly PrivateDutyIncidentAccidentService incidentAccidentService;
        private readonly PrivateDutyMediatorService mediatorService;
        private readonly IUserService userService;

        public IncidentController(
            PrivateDutyIncidentAccidentService incidentAccidentService,
            PrivateDutyMediatorService mediatorService,
            IUserService userService)
        {
            Check.Argument.IsNotNull(incidentAccidentService, "incidentAccidentService");
            Check.Argument.IsNotNull(mediatorService, "mediatorService");
            Check.Argument.IsNotNull(userService, "userService");


            this.incidentAccidentService = incidentAccidentService;
            this.mediatorService = mediatorService;
            this.userService = userService;
        }

        #endregion

        #region Incident Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = incidentAccidentService.GetServiceAndLocationViewData(ParentPermission.ManageIncidentAccidentReport, PermissionActions.ViewList, new List<PermissionActions> { PermissionActions.Add,PermissionActions.Export }, false);
            return PartialView("List", viewData);
        }

        [GridAction]
        public ActionResult ListContent()
        {
            return View(new GridModel(incidentAccidentService.GetIncidents(Current.AgencyId,false)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.PrivateDuty);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.ManageIncidentAccidentReport, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.PrivateDuty,patientId);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.ManageIncidentAccidentReport, PermissionActions.Add);
            return PartialView(viewData);
        }

        public ActionResult Add([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");
            var viewData = new JsonViewData();
            if (incident.IsValid())
            {
                var validationResults = incident.SignValidation(ServiceHelper.IsSignatureCorrect(Current.UserId, incident.SignatureText));
                if (validationResults.IsSuccessful)
                {
                    viewData = incidentAccidentService.AddIncident(incident, validationResults.IsSigned);
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(incidentAccidentService.GetIncident(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");
            var viewData = new JsonViewData();
            if (incident.IsValid())
            {
                var validationResults = incident.SignValidation(ServiceHelper.IsSignatureCorrect(Current.UserId, incident.SignatureText));
                if (validationResults.IsSuccessful)
                {
                    viewData = incidentAccidentService.UpdateIncident(incident, validationResults.IsSigned);
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, new ParentPermission[] { ParentPermission.ManageIncidentAccidentReport, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new IncidentReportPdf(incidentAccidentService.GetIncidentReportPrint(patientId, eventId)), "IncidentLog", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<IncidentReportPdf>(new IncidentReportPdf(incidentAccidentService.GetIncidentReportPrint(patientId, eventId)), "IncidentLog", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<IncidentReportPdf>(new IncidentReportPdf(incidentAccidentService.GetIncidentReportPrint(patientId, eventId)), "IncidentLog");
        }


        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Incident / Accident Log could not be approved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = incidentAccidentService.ProcessIncidents(patientId, eventId, "Approve", null);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your Incident / Accident Log has been successfully approved.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(Guid patientId, Guid eventId, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Incident / Accident Log could not be returned." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = incidentAccidentService.ProcessIncidents(patientId, eventId, "Return", reason);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your Incident / Accident Log has been successfully returned.";
                }
            }
            return Json(viewData);
        }

        //TODO: Remove episode id from incident delete url
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            return Json(incidentAccidentService.ToggleIncident(patientId, id, true));
        }

        #endregion
    }
}
