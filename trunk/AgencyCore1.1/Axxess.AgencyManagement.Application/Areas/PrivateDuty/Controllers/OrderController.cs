﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Web.Mvc;

    using iTextExtension;

    using ViewData;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OrderController : BaseController
    {
        #region Constructor

        private readonly PrivateDutyPhysicianOrderService physicianOrderService;
        public OrderController(PrivateDutyPhysicianOrderService physicianOrderService)
        {
            Check.Argument.IsNotNull(physicianOrderService, "physicianOrderService");
            this.physicianOrderService = physicianOrderService;
        }

        #endregion

        #region Order

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var viewData = new OrderViewData(AgencyServices.PrivateDuty);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Add);
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId, Guid episodeId)
        {
            var viewData = physicianOrderService.NewOrderViewData(patientId, episodeId, AgencyServices.PrivateDuty);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Add);
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            return Json(physicianOrderService.AddPhysicianOrder(order));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, new ParentPermission[] { ParentPermission.Orders, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new PhysicianOrderPdf(physicianOrderService.GetPhysicianOrderPrint(patientId, eventId)), "PhysicianOrder", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new PhysicianOrderPdf(physicianOrderService.GetPhysicianOrderPrint(patientId, eventId)), "PhysicianOrder", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf(new PhysicianOrderPdf(physicianOrderService.GetPhysicianOrderPrint(patientId, eventId)), "PhysicianOrder");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Blank()
        {
            return FileGenerator.Pdf(new PhysicianOrderPdf(physicianOrderService.GetPhysicianOrderPrint()), "PhysicianOrder");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PdfBlank()
        {
            return FileGenerator.Pdf(new PhysicianOrderPdf(physicianOrderService.GetPhysicianOrderPrint()), "PhysicianOrder");
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id, Guid patientId)
        {
            return PartialView(physicianOrderService.GetPhysicianOrderForEdit(patientId, id));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be deleted." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = physicianOrderService.TogglePhysicianOrder(patientId, id, true);
                viewData.errorMessage = viewData.isSuccessful ? "Order has been deleted successfully." : "Order could not be deleted! Please try again.";
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Orders, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated." };
            if (order.IsValid())
            {
                viewData = physicianOrderService.UpdatePhysicianOrder(order);
            }
            else
            {
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetOrder(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return Json(physicianOrderService.GetPhysicianOrder(patientId, Id));
        }

        #endregion
    }
}
