﻿namespace Axxess.AgencyManagement.Application.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Linq;
    using System.Web.Script.Serialization;

    using Telerik.Web.Mvc;

    using Services;
    using ViewData;
    using iTextExtension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;


    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Domain;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OasisController : BaseController
    {
        #region Constructor / Member

        private readonly IAgencyService agencyService;
        private readonly PrivateDutyAssessmentService assessmentService;
        private readonly PrivateDutyTaskService scheduleService;
        private readonly PrivateDutyEpisodeService episodeService;
        private readonly PrivateDutyMediatorService mediatorService;

        public OasisController(PrivateDutyTaskService scheduleService,
            PrivateDutyAssessmentService assessmentService,
            IPatientService patientService,
            PrivateDutyEpisodeService episodeService,
            PrivateDutyMediatorService mediatorService,
            IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");

            this.assessmentService = assessmentService;
            this.scheduleService = scheduleService;
            this.episodeService = episodeService;
            this.mediatorService = mediatorService;
            this.agencyService = agencyService;
        }

        #endregion

        #region OasisController Actions

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Assessment(FormCollection formCollection)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            if (formCollection != null)
            {
                var keys = formCollection.AllKeys;
                if (keys != null && keys.Length > 0)
                {
                    if (keys.Contains("assessment"))
                    {
                        var assessmentType = formCollection["assessment"];
                        if (assessmentType.IsNotNullOrEmpty())
                        {
                            oasisViewData = assessmentService.SaveAssessment(formCollection, Request.Files, assessmentType);
                        }
                        else
                        {
                            oasisViewData.errorMessage = "Assessment type is not identfied. Try again.";
                        }
                    }
                    else
                    {
                        oasisViewData.errorMessage = "Assessment type is not identfied. Try again.";
                    }
                }
                else
                {
                    oasisViewData.errorMessage = "There is an error sending the data. Try again.";
                }
            }
            else
            {
                oasisViewData.errorMessage = "There is an error sending the data. Try again.";
            }
            return Json(oasisViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(assessmentService.GetQuestions(Id).ToDictionary());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult View(Guid Id, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            var assessment = assessmentService.GetAssessmentWithComment(PatientId, Id);
            return PartialView(string.Format("Assessments/{0}{1}", assessment != null ? assessment.Type.ToString() : "NoData", assessment != null && assessment.Version > 0 ? assessment.Version.ToString() : string.Empty), assessment);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.LoadPrevious)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadPrevious(Guid episodeId, Guid patientId, Guid assessmentId, Guid previousAssessmentId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(assessmentId, "eventId");
            Check.Argument.IsNotEmpty(previousAssessmentId, "previousAssessmentId");
            return Json(assessmentService.LoadPreviousAssessment(episodeId, patientId, assessmentId, previousAssessmentId));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, new ParentPermission[] { ParentPermission.Schedule, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId)), "OASIS", Response);
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId)), "OASIS", Response);
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId)), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PdfLimited(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId), false), "OASIS");
        }

        [PrivateDutyViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Category(Guid Id, Guid PatientId, string Category, string Version)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return PartialView(string.Format("Assessments/Tabs/{0}{1}", Category, Version), assessmentService.LoadCategory(Id, PatientId, Category) ?? new Assessment());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WoundSetPostion(Guid Id, Guid PatientId, WoundPosition position)
        {
            return Json(this.assessmentService.WoundSetPostion(Id, PatientId, position));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundDelete(Guid Id, Guid PatientId, int WoundNumber, int WoundCount)
        {
            return Json(this.assessmentService.WoundDelete(Id, PatientId, WoundNumber, WoundCount));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundAdd(Guid Id, Guid PatientId, string Type, WoundPosition position)
        {
            return PartialView("Assessments/Wound/Add", new WoundViewData<Question>() { EventId = Id, PatientId = PatientId, Type = Type, Position = position });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundEdit(Guid Id, Guid PatientId, int WoundNumber)
        {
            return PartialView("Assessments/Wound/Edit", assessmentService.GetWound(Id, PatientId, WoundNumber));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WoundUpdate(WoundSaveArguments saveArguments, FormCollection formCollection)
        {
            return Json(assessmentService.UpdateWound(saveArguments, Request.Files, formCollection));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult Submit(Guid Id, Guid patientId,  string actionType, string reason)
        //{
        //    return Json(mediatorService.OASISSubmit(Id, patientId,  actionType, reason));
        //}

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.OASIS, PermissionActions.Reopen)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid Id, Guid patientId, string reason)
        {
            return Json(mediatorService.OASISSubmit(Id, patientId, "ReOpen", reason));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Return(Guid Id, Guid patientId, string reason)
        {
            return Json(mediatorService.OASISSubmit(Id, patientId, "Return", reason));
        }

        [PermissionFilter(AgencyServices.PrivateDuty, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Approve(Guid Id, Guid patientId, string reason)
        {
            return Json(mediatorService.OASISSubmit(Id, patientId, "Approve", reason));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SubmitOnly(AssessmentSubmitArguments arguments)
        {
            return Json(mediatorService.OASISSubmitOnly(arguments));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisSignature(Guid Id, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return PartialView("NonOasisSignature", assessmentService.GetAssessmentWithScheduleType(PatientId, Id, false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The asset could not be deleted." };
            if (this.assessmentService.DeleteWoundCareAsset(patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The asset has been successfully deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supply(Guid patientId, Guid eventId)
        {
            return View(new GridModel(assessmentService.GetAssessmentSupply(patientId, eventId)));
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult AddSupply(Guid episodeId, Guid patientId, Guid eventId,  Supply supply)
        //{
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(eventId, "eventId");

        //    assessmentService.AddSupply(patientId, eventId,  supply);
        //    return View(new GridModel(assessmentService.GetAssessmentSupply(patientId, eventId)));
        //}

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult EditSupply(Guid episodeId, Guid patientId, Guid eventId,  Supply supply)
        //{
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(eventId, "eventId");
        //    Check.Argument.IsNotNull(supply, "supply");

        //    assessmentService.UpdateSupply(patientId, eventId, supply);
        //    return View(new GridModel(assessmentService.GetAssessmentSupply(patientId, eventId)));
        //}

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult DeleteSupply(Guid episodeId, Guid patientId, Guid eventId,  Supply supply)
        //{
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(eventId, "eventId");
        //    Check.Argument.IsNotNull(supply, "supply");

        //    assessmentService.DeleteSupply(patientId, eventId,  supply);
        //    return View(new GridModel(assessmentService.GetAssessmentSupply(patientId, eventId)));
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Validate(Guid Id, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            var viewData = assessmentService.Validate(Id, patientId, episodeId);
            viewData.Service = AgencyServices.PrivateDuty;
            return View("Validation", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSupply(Guid patientId, Guid eventId)
        {
            return PartialView("OasisSupply/New", new NoteSupplyNewViewData { Service = AgencyServices.PrivateDuty, PatientId = patientId, EventId = eventId });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to be added." };
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            if (assessmentService.AddSupply(patientId, eventId, supply))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully added.";
            }
            else
            {
                viewData.errorMessage = "The supply failed to be added.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid patientId, Guid eventId, Guid supplyId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(eventId, "eventId");
            Check.Argument.IsNotEmpty(supplyId, "supplyId");

            var viewData = new NoteSupplyEditViewData { Service = AgencyServices.PrivateDuty, PatientId = patientId, EventId = eventId };
            viewData.Supply = assessmentService.GetSupply(patientId, eventId, supplyId);
            return PartialView("OasisSupply/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to save." };
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");
           
            if (assessmentService.UpdateSupply(patientId, eventId, supply))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully saved.";
            }
            else
            {
                viewData.errorMessage = "The supply failed to save.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSupply(Guid patientId, Guid eventId, Guid supplyId)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to delete." };
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supplyId, "supplyId");

            if (assessmentService.DeleteSupply(patientId, eventId, supplyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully deleted.";
            }
            return Json(viewData);
        }
        #endregion
    }
}
