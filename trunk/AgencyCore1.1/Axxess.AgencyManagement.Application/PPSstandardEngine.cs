﻿namespace Axxess.AgencyManagement.Application
{

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;

    using Axxess.LookUp.Repositories;

    using Axxess.AgencyManagement.Entities;

   public class PPSstandardEngine
    {

        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly PPSstandardEngine instance = new PPSstandardEngine();
        }

        #endregion

        #region Public Instance

        public static PPSstandardEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Members

        private  SafeList<PPSStandard> cache = new SafeList<PPSStandard>();
        private static readonly ILookUpDataProvider lookUpdataProvider = Container.Resolve<ILookUpDataProvider>();

        #endregion

        #region Private Constructor / Methods

        private PPSstandardEngine()
        {
        }

        private void Load()
        {
            var ppsStandards = lookUpdataProvider.LookUpRepository.PPSStandards().ToList();
            if (ppsStandards != null && ppsStandards.Count > 0)
            {
                cache.AddRange(ppsStandards);
            }
        }

        #endregion

        #region Public Methods

        public PPSStandard Get(int year)
        {
            if (this.cache!=null && this.cache.Count>0)
            {
                var ppsStandard = this.cache.Single(s => s.Time.Year == year);
                if (ppsStandard != null)
                {
                    return ppsStandard;
                }
                else
                {
                    ppsStandard = lookUpdataProvider.LookUpRepository.GetPPSStandardByYear(year);
                    if (ppsStandard != null)
                    {
                        this.cache.Add(ppsStandard);
                        return ppsStandard;
                    }
                    else
                    {
                       return null;
                    }
                }
            }
            else
            {
                this.Load();
                if (this.cache != null && this.cache.Count > 0)
                {
                    return this.cache.Single(s => s.Time.Year == year);
                }
               
            }
            return null;
        }

        public void Refresh()
        {
           this.Load();
        }

        public SafeList<PPSStandard> Cache
        {
            get { return cache; }
        }

        #endregion
    }
}
