﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;

    public interface IReferralService
    {
        Referral Get(Guid id);
        Referral GetForNonAdmit(Guid id);
        Referral GetForDelete(Guid id);
        JsonViewData AddReferral(Referral referral);
        JsonViewData Update(Referral referral);
        JsonViewData NonAdmitReferral(Guid Id, List<string> ServiceProvided, List<NonAdmit> profile);
        bool SetStatus(Guid id, Referral referralForStatus);
        JsonViewData Delete(Guid id, List<string> ServiceProvided);
        bool UpdateReferralModal(Referral referral);
        Referral GetReferral(Guid referralId);
        Referral GetReferralForNewOrExisitngPatient(Guid referralId);
        List<ReferralData> GetPending(Guid agencyId, AgencyServices service);
        List<Referral> GetReferralByStatus(Guid agencyId, AgencyServices service, ReferralStatus status);
        bool AddPrimaryEmergencyContact(Referral referral);
        Referral GetReferralWithEmergencyContact(Guid id);
        Referral GetReferralWithEmergencyContactAndProfile(Guid id);
        ReferralPdf GetReferralPdf(Guid id);
        List<ReferalPhysician> GetReferalPhysicians(Guid ReferralId);
        JsonViewData AddReferralPhysician(Guid id, Guid referralId);
        JsonViewData DeleteReferralPhysician(Guid id, Guid referralId);
        JsonViewData SetPrimaryPhysician(Guid id, Guid referralId);
    }
}
