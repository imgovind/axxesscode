﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.LookUp.Domain;

    public interface ILookUpService
    {
        DisciplineTask GetDisciplineTask(int disciplinetaskId);
        DisciplineTask GetDisciplineTaskWithTableName(int disciplinetaskId);
        List<DisciplineTask> GetDisciplineTasks();
        List<DisciplineTask> GetDisciplineTasksWithTableName(int[] disciplinetaskIds);
        List<ReportDescription> GetReportDescriptions();
    }
}
