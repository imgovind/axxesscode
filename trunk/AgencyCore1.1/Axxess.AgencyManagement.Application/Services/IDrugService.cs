﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Entities;

    using LexiData;

    public interface IDrugService
    {
        IList<Drug> DrugsStartsWith(string query, int limit);
        List<string> GetDrugClassifications(string genericDrugId);
        List<DrugDrugInteractionResult> GetDrugDrugInteractions(List<string> drugs);
    }
}
