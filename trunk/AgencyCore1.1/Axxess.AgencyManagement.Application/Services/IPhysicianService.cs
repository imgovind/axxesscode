﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;

    using Axxess.AgencyManagement.Entities;
    using System.Collections.Generic;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.ViewData;

    public interface IPhysicianService
    {
        bool CreatePhysician(AgencyPhysician physician);
        JsonViewData AddPhysician(AgencyPhysician agencyPhysician);
        bool UpdatePhysician(AgencyPhysician physician);
        JsonViewData DeletePhysician(Guid id);
        bool AddLicense(PhysicainLicense license);
        PhysicainLicense GetLicense(Guid id, Guid physicianId);
        bool UpdateLicense(PhysicainLicense license);
        bool DeleteLicense(Guid id, Guid physicianId);

        AgencyPhysician GetAgencyPhysicianForEdit(Guid Id, bool isPecosVerificationNeeded);
        AgencyPhysician GetAgencyPhysician(Guid Id, bool IsPecosVerificationNeeded);
        IList<AgencyPhysician> GetAgencyPhysiciansWithPecosVerification();
        List<PhysicainLicense> GeAgencyPhysicianLicenses(Guid physicianId);
        PhysicianListViewData GetPhysicianListViewData();
    }
}
