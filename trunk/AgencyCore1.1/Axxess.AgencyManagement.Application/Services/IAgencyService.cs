﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    
    using Enums;
    using Domain;
    using ViewData;
    using Axxess.Core.Infrastructure;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Enums;

    public interface IAgencyService
    {
        int MaxAgencyUserCount();
        JsonViewData UpdateAgency(Agency agency, AgencyLocation location);
        bool CreateContact(AgencyContact contact);
        JsonViewData UpdateAgencyWithLocation(Agency agency, AgencyLocation agencyLocation);
        Agency GetAgencyWithMainLocation();
        LocationPrintProfile AgencyNameWithLocationAddress(Guid locationId);
        LocationPrintProfile AgencyNameWithMainLocationAddress();

        IList<AgencyLocation> GetBranches();
        AgencyLocation FindLocation(Guid Id);
        bool CreateLocation(AgencyLocation location);
        JsonViewData UpdateLocation(AgencyLocation location);
        AgencyContact FindContact(Guid Id);
        JsonViewData UpdateContact(AgencyContact contact);
        JsonViewData AddContact(AgencyContact contact);
        JsonViewData DeleteContact(Guid id);
        Guid GetMainLocationId();
        Guid GetBranchForSelectionId(int service);
        AgencyLocation GetBranchForSelection(int service);
        int AgencyPayor();
        AgencyLocation GetLocationOrDefault(Guid branchId);
        AgencyInsurance GetInsuranceOrDefault(int insuranceId);
        List<InsuranceLean> InsuranceList();
        JsonViewData EditBranchLocatorAndMedicareInsuranceInfo(Guid locationId, string locatorXml, AgencyMedicareInsurance agencyLocationMedicare);
        AgencyLocation VisitRateContent(Guid branchId);
        List<ChargeRate> GetInsuranceBillDatas(int InsuranceId);
        JsonViewData SaveBillData(int InsuranceId, ChargeRate chargeRate);
        JsonViewData UpdateBillData(ChargeRate chargeRate);
        ChargeRate GetLocationBillDataChargeRate(Guid locationId, int Id);
        List<ChargeRate> GetLocationBillDataChargeRates(Guid locationId);
        JsonViewData UpdateLocationBillData(ChargeRate chargeRate);
        JsonViewData SaveLocationBillData(Guid locationId, ChargeRate chargeRate);
        JsonViewData DeleteLocationBillData(Guid locationId, int Id);
        JsonViewData DeleteBillData(int InsuranceId, int Id);
        JsonViewData ReplaceInsuranceVisit(int Id, int replacedId);

        ChargeRate GetInsuranceChargeRateForEdit(int InsuranceId, int Id);
        ChargeRate GetInsuranceChargeRateForNew(int InsuranceId);
        AgencyInsurance FindInsurance(int Id);
        JsonViewData AddInsurance(AgencyInsurance insurance);
        AgencyLocation GetMainLocation();
        JsonViewData EditInsurance(AgencyInsurance insurance);
        JsonViewData DeleteInsurance(int Id);

        List<SelectListItem> DisciplineTypes(Guid locationId, int disciplineTask);

        IList<AgencyContact> GetContacts();

        IList<AgencyHospital> GetHospitals();
        AgencyHospital FindHospital(Guid Id);
        JsonViewData DeleteHospital(Guid Id);
        JsonViewData AddHospital(AgencyHospital hospital);
        JsonViewData UpdateHospital(AgencyHospital hospital);

        IList<AgencyTemplate> GetTemplates();
        AgencyTemplate GetTemplate(Guid id);
        AgencyTemplate GetTemplateOrDefault(Guid id);
        JsonViewData AddTemplate(AgencyTemplate template);
        JsonViewData UpdateTemplate(AgencyTemplate template);
        JsonViewData DeleteTemplate(Guid id);

        IList<AgencySupply> GetSupplies();
        AgencySupply GetSupply(Guid id);
        AgencySupply GetSupplyOrDefault(Guid id);
        JsonViewData AddSupply(AgencySupply supply);
        JsonViewData UpdateSupply(AgencySupply supply);
        JsonViewData DeleteSupply(Guid id);
        object SuppliesSearch(string term, int limit);

        IList<AgencyAdjustmentCode> GetAdjustmentCodes();
        AgencyAdjustmentCode FindAdjustmentCode(Guid id);
        JsonViewData AddAdjustmentCode(AgencyAdjustmentCode adjustmentCode);
        JsonViewData UpdateAdjustmentCode(AgencyAdjustmentCode adjustmentCode);
        JsonViewData DeleteAdjustmentCode(Guid id);

        IList<UploadType> GetUploadTypes();
        UploadType FindUploadType(Guid id);
        JsonViewData AddUploadType(UploadType uploadType);
        JsonViewData UpdateUploadType(UploadType uploadType);
        JsonViewData DeleteUploadType(Guid id);

        MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid reportId);
        List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(DateTime startDate, DateTime endDate);
        SingleServiceGridViewData GetGridViewData(ParentPermission category);
    }
}
