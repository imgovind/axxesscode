﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;

    public interface IPayrollService
    {
        bool MarkAsPaid(List<Guid> itemList);
        bool MarkAsUnpaid(List<Guid> itemList);
        List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status);        
        List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status);
        PayrollDetail GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status);
        List<PayrollDetailSummaryItem> GetVisitsSummary(Guid userId, DateTime startDate, DateTime endDate, string Status);
        PayrollSummaryViewData GetPayrollSummaryViewData(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus);
        PayrollDetailsViewData GetPayrollDetailsViewData(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus);
    }
}
