﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.LookUp.Repositories;
    using Axxess.Core;
    using Axxess.LookUp.Domain;

    public class LookUpService : ILookUpService
    {
        private readonly ILookupRepository lookupRepository;

        public LookUpService(ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        public List<DisciplineTask> GetDisciplineTasks()
        {
            return lookupRepository.DisciplineTasks().ToList();
        }

        public DisciplineTask GetDisciplineTask(int disciplinetaskId)
        {
            return lookupRepository.GetDisciplineTask(disciplinetaskId);
        } 

        public DisciplineTask GetDisciplineTaskWithTableName(int disciplinetaskId)
        {
            return lookupRepository.GetDisciplineTasksWithTableName(disciplinetaskId);
        }

        public List<DisciplineTask> GetDisciplineTasksWithTableName(int [] disciplinetaskIds)
        {
            return lookupRepository.GetDisciplineTasksWithTableName(disciplinetaskIds);
        }

        public List<ReportDescription> GetReportDescriptions()
        {
            return lookupRepository.GetReportDescriptions();
        }
    }
}
