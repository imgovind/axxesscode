﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Linq;
    using System.Data.Common;
    using System.Collections.Generic;

    using LexiData;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Entities;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class DrugService : IDrugService
    {
        #region Private Members and Constructor

        private DbConnection dbConnection = null;
        private DbProviderFactory dbProviderFactory = null;

        private string providerInvariantName = "MySql.Data.MySqlClient";

        private void OpenConnection()
        {
            this.dbProviderFactory = DbProviderFactories.GetFactory(this.providerInvariantName);
            if (this.dbProviderFactory == null)
            {
                throw new Exception("Unable to create a DbFactory");
            }
            this.dbConnection = this.dbProviderFactory.CreateConnection();
            this.dbConnection.ConnectionString = Container.Resolve<IWebConfiguration>().ConnectionStrings("LexiDataConnectionString");

            this.dbConnection.Open();
        }

        private void CloseConnection()
        {
            this.dbConnection.Close();
        }

        #endregion

        #region IDrugService Members

        public IList<Drug> DrugsStartsWith(string query, int limit)
        {
            var drugs = new List<Drug>();
            var drugList = new List<string>();

            this.OpenConnection();

            GenericDAL dataLayer = new GenericDAL(this.dbProviderFactory, this.dbConnection, providerInvariantName);
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.SearchType = SEARCHTYPE.WordBeginsWith;
            searchCriteria.SearchMethod = SEARCHMETHOD.Normalized;

            DrugFilter drugFilter = new DrugFilter();
            drugFilter.IncludeBrandName = true;
            drugFilter.IncludeSynonyms = true;

            List<GenericDrug> genericDrugs = dataLayer.SearchGenericDrug(query, searchCriteria, drugFilter);

            int count = 0;
            genericDrugs.ForEach(genericDrug =>
            {
                var drug = new Drug
                {
                    Name = genericDrug.MatchedName.ToUpper(),
                    LexiDrugId = genericDrug.GenDrugID
                };
                if (drug.Name.ToLower().StartsWith(query.ToLower()))
                {
                    if(!drugs.Contains(drug))
                        drugs.Add(drug);
                    if(!drugList.Contains(genericDrug.MatchedName.Trim().ToLower()))
                        drugList.Add(drug.Name.Trim().ToLower());
                    count++;
                }
            });

            this.CloseConnection();

            return drugs.OrderBy(d => d.Name).Take(limit).ToList();
        }

        public List<DrugDrugInteractionResult> GetDrugDrugInteractions(List<string> drugs)
        {
            var result = new List<DrugDrugInteractionResult>();

            this.OpenConnection();

            GenericDAL dataLayer = new GenericDAL(this.dbProviderFactory, this.dbConnection, this.providerInvariantName);
            ScreeningContext screeningContext = new ScreeningContext();

            drugs.ForEach(d =>
            {
                var baseDrug = dataLayer.GetGenericDrug(d);
                screeningContext.Drugs.Add(baseDrug);
            });

            result = dataLayer.GetDrugDrugInteractions(screeningContext, false, 0);

            this.CloseConnection();

            return result;
        }

        public List<string> GetDrugClassifications(string genericDrugId)
        {
            var result = new List<string>();

            this.OpenConnection();

            GenericDAL dataLayer = new GenericDAL(this.dbProviderFactory, this.dbConnection, this.providerInvariantName);
            var drugClassifications = dataLayer.GetDrugClassifications(genericDrugId, 1);
            if (drugClassifications != null)
            {
                drugClassifications.ForEach(dc =>
                {
                    result.Add(dc.ClassName);
                });
            }

            this.CloseConnection();

            return result;
        }

        #endregion
    }
}
