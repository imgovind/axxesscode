﻿namespace Axxess.AgencyManagement.Application.Services
{
    using Axxess.AgencyManagement.Repositories;

    public class HHVitalSignService : VitalSignService
    {
        public HHVitalSignService(HHDataProvider dataProvider)
            : base(dataProvider.VitalSignRepository)
        {
        }
    }
}
