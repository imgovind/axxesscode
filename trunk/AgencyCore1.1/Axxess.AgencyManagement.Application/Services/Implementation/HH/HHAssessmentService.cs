﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Repositories;


    using Axxess.Api;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Extensions;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Exports;

    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Entities.Common;


    public class HHAssessmentService : AssessmentService<ScheduleEvent,PatientEpisode>
    {
        #region Constructor / Members

        private static readonly GrouperAgent grouperAgent = new GrouperAgent();
        private static readonly ValidationAgent validationAgent = new ValidationAgent();
        private readonly HHTaskRepository scheduleRepository;
        private readonly HHEpisodeRepository episodeRepository;
        private readonly HHAssessmentRepository assessmentRepository;

        public HHAssessmentService(HHDataProvider dataProvider, ILookUpDataProvider lookupDataProvider)
            : base(dataProvider.TaskRepository, dataProvider.AssessmentRepository, dataProvider.EpisodeRepository,dataProvider.MongoRepository, dataProvider.VitalSignRepository)
        {

            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.profileRepository = dataProvider.PatientProfileRepository;

            this.assessmentRepository = dataProvider.AssessmentRepository;
            this.planofCareRepository = dataProvider.PlanOfCareRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.HomeHealth;
        }

        #endregion

        #region IAssessmentService Members

        public string OasisHeader(AgencyLocation agencyLocation)
        {
            var submissionGuide = GetOasisHeaderInstructionsNew();
            var submissionFormat = new StringBuilder();
            submissionFormat.Capacity = 1448;
            if (submissionGuide != null && submissionGuide.Count > 0)
            {
                submissionFormat.Append("A1"); //REC_ID :2

                if (submissionGuide.ContainsKey("FED_ID")) //FED_ID :8
                {
                    if (agencyLocation != null && agencyLocation.MedicareProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.MedicareProviderNumber.PadRight(6).Trim().PartOfString(0, 6).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(6));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(6));
                }

                submissionFormat.Append(string.Empty.PadRight(4)); //FILLER1 :12

                if (submissionGuide.ContainsKey("ST_ID")) //ST_ID :27
                {
                    if (agencyLocation.MedicaidProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.MedicaidProviderNumber.Trim().PadRight(15).PartOfString(0, 15).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(15));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(15));
                }

                if (submissionGuide.ContainsKey("HHA_AGENCY_ID")) //HHA_AGENCY_ID :43
                {
                    if (agencyLocation.HomeHealthAgencyId.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.HomeHealthAgencyId.Trim().PadRight(16).PartOfString(0, 16).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(16));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(16));
                }

                if (submissionGuide.ContainsKey("ACY_NAME")) //ACY_NAME :73
                {
                    if (agencyLocation.Name.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.Name.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_ADDR_1")) //ACY_ADDR_1 :103
                {
                    if (agencyLocation != null && agencyLocation.AddressLine1.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressLine1.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_ADDR_2")) //ACY_ADDR_2 :133
                {
                    if (agencyLocation != null && agencyLocation.AddressLine2.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressLine2.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }


                if (submissionGuide.ContainsKey("ACY_CITY")) //ACY_CITY :153
                {
                    if (agencyLocation != null && agencyLocation.AddressCity.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressCity.Trim().PadRight(20).PartOfString(0, 20).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(20));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("ACY_ST")) //ACY_ST :155
                {
                    if (agencyLocation != null && agencyLocation.AddressStateCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressStateCode.Trim().PadRight(2).PartOfString(0, 2).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("ACY_ZIP")) //ACY_ZIP :166
                {
                    if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressZipCode.Trim().PadRight(11).PartOfString(0, 11).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(11));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if (submissionGuide.ContainsKey("ACY_CNTCT")) //ACY_CNTCT :196
                {
                    submissionFormat.Append(string.Format("{0}, {1}", agencyLocation.ContactPersonLastName.IsNotNullOrEmpty() ? agencyLocation.ContactPersonLastName : string.Empty, agencyLocation.ContactPersonFirstName.IsNotNullOrEmpty() ? agencyLocation.ContactPersonFirstName : string.Empty).Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_PHONE")) //ACY_PHONE :206
                {
                    if (agencyLocation.ContactPersonPhone.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.ContactPersonPhone.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("ACY_EXTEN")) //ACY_EXTEN :211
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("AGT_ID")) //AGT_ID :220
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("AGT_NAME")) //AGT_NAME :250
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_ADDR_1")) //AGT_ADDR_1 :280
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_ADDR_2")) //AGT_ADDR_2 :310
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_CITY")) //AGT_CITY :330
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("AGT_ST")) //AGT_ST :332
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("AGT_ZIP")) //AGT_ZIP :343
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if (submissionGuide.ContainsKey("AGT_CNTCT")) //AGT_CNTCT :373
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_PHONE")) //AGT_PHONE :383
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("AGT_EXTEN")) //AGT_EXTEN :388
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("SFW_ID") && submissionGuide["SFW_ID"] != null && submissionGuide["SFW_ID"].DefaultValue.IsNotNullOrEmpty())//SFW_ID :397
                {
                    submissionFormat.Append(submissionGuide["SFW_ID"].DefaultValue.Trim().Trim().PadRight(9).PartOfString(0, 9).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("SFW_NAME") && submissionGuide["SFW_NAME"] != null && submissionGuide["SFW_NAME"].DefaultValue.IsNotNullOrEmpty()) //SFW_NAME : 427
                {
                    submissionFormat.Append(submissionGuide["SFW_NAME"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_ADDR_1") && submissionGuide["SFW_ADDR_1"] != null && submissionGuide["SFW_ADDR_1"].DefaultValue.IsNotNullOrEmpty()) //SFW_ADDR_1 : 457
                {
                    submissionFormat.Append(submissionGuide["SFW_ADDR_1"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_ADDR_2") && submissionGuide["SFW_ADDR_2"] != null && submissionGuide["SFW_ADDR_2"].DefaultValue.IsNotNullOrEmpty()) //SFW_ADDR_1 :487
                {
                    submissionFormat.Append(submissionGuide["SFW_ADDR_2"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_CITY") && submissionGuide["SFW_CITY"] != null && submissionGuide["SFW_CITY"].DefaultValue.IsNotNullOrEmpty()) //SFW_CITY : 507
                {
                    submissionFormat.Append(submissionGuide["SFW_CITY"].DefaultValue.Trim().PadRight(20).PartOfString(0, 20).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("SFW_ST") && submissionGuide["SFW_ST"] != null && submissionGuide["SFW_ST"].DefaultValue.IsNotNullOrEmpty()) //SFW_ST : 509
                {
                    submissionFormat.Append(submissionGuide["SFW_ST"].DefaultValue.Trim().PadRight(2).PartOfString(0, 2).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("SFW_ZIP") && submissionGuide["SFW_ZIP"] != null && submissionGuide["SFW_ZIP"].DefaultValue.IsNotNullOrEmpty()) //SFW_ZIP : 520
                {
                    submissionFormat.Append(submissionGuide["SFW_ZIP"].DefaultValue.Trim().PadRight(11).PartOfString(0, 11).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }


                if (submissionGuide.ContainsKey("SFW_CNTCT") && submissionGuide["SFW_CNTCT"] != null && submissionGuide["SFW_CNTCT"].DefaultValue.IsNotNullOrEmpty()) //SFW_CNTCT : 550
                {
                    submissionFormat.Append(submissionGuide["SFW_CNTCT"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_PHONE") && submissionGuide["SFW_PHONE"] != null && submissionGuide["SFW_PHONE"].DefaultValue.IsNotNullOrEmpty()) //SFW_PHONE :560
                {
                    submissionFormat.Append(submissionGuide["SFW_PHONE"].DefaultValue.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("SFW_EXTEN")) //SFW_EXTEN :565
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("FILE_DT")) //FILE_DT :573
                {
                    submissionFormat.Append(DateTime.Now.ToString("yyyyMMdd").Trim().ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                }

                if (submissionGuide.ContainsKey("TEST_SW") && submissionGuide["TEST_SW"] != null && submissionGuide["TEST_SW"].DefaultValue.IsNotNullOrEmpty()) //TEST_SW :574
                {
                    submissionFormat.Append(submissionGuide["TEST_SW"].DefaultValue.PadRight(1).PartOfString(0, 1).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(1));
                }


                if (submissionGuide.ContainsKey("NATL_PROV_ID")) //NATL_PROV_ID :584
                {
                    if (agencyLocation.NationalProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.NationalProviderNumber.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("HDR_FL")) //HDR_FL : 1445
                {
                    submissionFormat.Append(string.Empty.PadRight(861));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(861));
                }

                if (submissionGuide.ContainsKey("DATA_END")) //HDR_FL :1446
                {
                    submissionFormat.Append("%".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("%".PadRight(1));
                }

                if (submissionGuide.ContainsKey("CRG_RTN")) //CRG_RTN :1447
                {
                    submissionFormat.Append("\r".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("\r".PadRight(1));
                }

                if (submissionGuide.ContainsKey("LN_FD")) //LN_FD :1448
                {
                    submissionFormat.Append("\n".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("\n".PadRight(1));
                }

            }
            return submissionFormat.ToString();
        }

        public string OasisFooter(int totalNumberOfRecord)
        {
            var footerString = new string(' ', 1446);
            var footerStringWithRecId = footerString.Remove(0, 2).Insert(0, "Z1");
            var footerStringWithTotalRec = footerStringWithRecId.Remove(2, 6).Insert(2, totalNumberOfRecord.ToString().PadLeft(6, '0'));
            var footerStringWithDataEnd = footerStringWithTotalRec.Remove(1445, 1).Insert(1445, "%");
            return footerStringWithDataEnd;
        }

        public string OasisInactivateBody(IDictionary<string, Question> assessmentQuestions, Guid agencyLocationId)
        {
            var submissionFormat = new StringBuilder();
            submissionFormat.Capacity = 1446;
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                if (assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"] != null && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty())
                {
                    string type = assessmentQuestions["M0100AssessmentType"].Answer;

                    var submissionGuide = GetInactivateOasisSubmissionFormatInstructionsNew();

                    var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, agencyLocationId);
                    if (agencyLocation != null && !agencyLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }

                    //var agency = agencyRepository.Get(Current.AgencyId);
                    //if (agency != null)
                    //{
                    //    agency.MainLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                    //}
                    submissionFormat.Append("X1"); //REC_ID
                    submissionFormat.Append(string.Empty.PadRight(2)); //ITEM_FILLER1
                    submissionFormat.Append(string.Empty.PadRight(8)); //LOCK_DATE
                    submissionFormat.Append(string.Empty.PadRight(41)); //ITEM_FILLER2
                    if (submissionGuide.ContainsKey("HHA_AGENCY_ID")) //HHA_AGENCY_ID
                    {
                        if (agencyLocation != null && agencyLocation.HomeHealthAgencyId.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(agencyLocation.HomeHealthAgencyId.Trim().PadRight((16)).PartOfString(0, (16)).ToUpper());
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadRight(16));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(16));
                    }
                    submissionFormat.Append(string.Empty.PadRight(27)); //ITEM_FILLER3
                    submissionFormat.Append(string.Empty.PadRight(20)); //MASK_VERSION_CD
                    submissionFormat.Append(string.Empty.PadRight(60)); //ITEM_FILLER4
                    if (submissionGuide.ContainsKey("M0030_START_CARE_DT") && assessmentQuestions.ContainsKey("M0030SocDate") && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0030SocDate"].Answer.IsValidDate()) //M0030_START_CARE_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8));

                    }
                    if (submissionGuide.ContainsKey("M0032_ROC_DT") && assessmentQuestions.ContainsKey("M0032ROCDate") && assessmentQuestions["M0032ROCDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0032ROCDate"].Answer.IsValidDate()) //M0032_ROC_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0032ROCDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8));
                    }
                    submissionFormat.Append(string.Empty.PadRight(1)); //ITEM_FILLER5
                    if (submissionGuide.ContainsKey("M0040_PAT_FNAME") && assessmentQuestions.ContainsKey("M0040FirstName") && assessmentQuestions["M0040FirstName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_FNAME
                    {

                        submissionFormat.Append(assessmentQuestions["M0040FirstName"].Answer.Trim().ToUpper().PartOfString(0, 12).PadRight(12));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(12));
                    }
                    submissionFormat.Append(string.Empty.PadRight(1)); //ITEM_FILLER6
                    if (submissionGuide.ContainsKey("M0040_PAT_LNAME") && assessmentQuestions.ContainsKey("M0040LastName") && assessmentQuestions["M0040LastName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_LNAME
                    {

                        submissionFormat.Append(assessmentQuestions["M0040LastName"].Answer.Trim().ToUpper().PartOfString(0, 18).PadRight(18));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(18));
                    }
                    submissionFormat.Append(string.Empty.PadRight(29)); //ITEM_FILLER7
                    if (submissionGuide.ContainsKey("M0064_SSN") && assessmentQuestions.ContainsKey("M0064PatientSSN") && assessmentQuestions["M0064PatientSSN"].Answer.IsNotNullOrEmpty()) //M0064_SSN
                    {
                        submissionFormat.Append(assessmentQuestions["M0064PatientSSN"].Answer.Trim().ToUpper().PartOfString(0, 9).PadRight(9));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(9));
                    }
                    submissionFormat.Append(string.Empty.PadRight(16)); //ITEM_FILLER8
                    if (submissionGuide.ContainsKey("M0066_PAT_BIRTH_DT") && assessmentQuestions.ContainsKey("M0066PatientDoB") && assessmentQuestions["M0066PatientDoB"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0066PatientDoB"].Answer.IsValidDate()) //M0066_PAT_BIRTH_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0066PatientDoB"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8, '-'));
                    }
                    submissionFormat.Append(string.Empty.PadRight(1)); //ITEM_FILLER9
                    if (submissionGuide.ContainsKey("M0069_PAT_GENDER") && assessmentQuestions.ContainsKey("M0069Gender") && assessmentQuestions["M0069Gender"].Answer.IsNotNullOrEmpty()) //M0069_PAT_GENDER
                    {
                        submissionFormat.Append(assessmentQuestions["M0069Gender"].Answer.Trim().ToUpper().PartOfString(0, 1).PadRight(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadRight(13)); //ITEM_FILLER10
                    if (submissionGuide.ContainsKey("M0090_INFO_COMPLETED_DT") && assessmentQuestions.ContainsKey("M0090AssessmentCompleted") && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsValidDate()) //M0090_INFO_COMPLETED_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0090AssessmentCompleted"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }

                    if (submissionGuide.ContainsKey("M0100_ASSMT_REASON") && assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty()) //M0100_ASSMT_REASON
                    {
                        submissionFormat.Append(assessmentQuestions["M0100AssessmentType"].Answer.Trim().ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    submissionFormat.Append(string.Empty.PadRight(425)); //ITEM_FILLER11
                    if (type.Contains("06") || type.Contains("07") || type.Contains("08") || type.Contains("09"))
                    {
                        if (submissionGuide.ContainsKey("M0906_DC_TRAN_DTH_DT") && assessmentQuestions.ContainsKey("M0906DischargeDate") && assessmentQuestions["M0906DischargeDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0906DischargeDate"].Answer.IsValidDate()) //M0906_DC_TRAN_DTH_DT
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0906DischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    submissionFormat.Append(string.Empty.PadRight(701)); //ITEM_FILLER12
                    submissionFormat.Append("%"); //DATA_END
                }
            }
            return submissionFormat.ToString();
        }

        public OasisViewData UpdateAssessmentCorrectionNumber(Guid Id, Guid patientId, int correctionNumber)
        {
            var viewData = new OasisViewData { isSuccessful = false, errorMessage = "Your Correction Number could not be updated." };
            var assessment = assessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, Id);
            if (assessment != null && assessment.SubmissionFormat.IsNotNullOrEmpty() && assessment.SubmissionFormat.Length >= 1446)
            {
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, Id);
                if (scheduleEvent != null)
                {
                    viewData.IsExportOASISRefresh = correctionNumber != assessment.VersionNumber;
                    assessment.VersionNumber = correctionNumber;
                    assessment.SubmissionFormat = assessment.SubmissionFormat.Trim().Remove(12, 2).Insert(12, correctionNumber.ToString().PadLeft(2, '0'));
                    if (assessmentRepository.UpdateModal(assessment))
                    {
                        if (scheduleEvent.Status > 0)
                        {
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Edit, (DisciplineTasks)scheduleEvent.DisciplineTask, "The correction number of the assessment edited.");
                        }
                        viewData.isSuccessful = true;
                    }
                }
            }
            return viewData;
        }

        protected override void UpdateAssessmentForDetailAppSpecific(ScheduleEvent schedule)
        {
            if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification
                  || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
            {
                if (ScheduleStatusFactory.OASISCompleted(true).Contains(schedule.Status))
                {
                    episodeRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                }
                else
                {
                    episodeRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                }
            }
        }

        public OasisViewData MarkAsExportedOrCompleted(List<Guid> OasisSelected, int status)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            var countChange = 0;
            if (OasisSelected != null && OasisSelected.Count > 0)
            {
                OasisSelected.ForEach(Id =>
                {
                    //string[] data = o.Split('|');
                    //if (data != null && data.Length > 1)
                    //{
                        //if (data[0].IsGuid() && !data[0].ToGuid().IsEmpty() && data[1].IsNotNullOrEmpty())
                        //{
                            var assessment = assessmentRepository.GetAssessmentOnly(Current.AgencyId, Id);// GetAssessment(data[0].ToGuid(), data[1]);
                            if (assessment != null)
                            {
                                assessment.Status = status;
                                assessment.ExportedDate = DateTime.Now;

                                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, assessment.PatientId, assessment.EpisodeId, assessment.Id);
                                if (scheduleEvent != null)
                                {
                                    var oldStatus = scheduleEvent.Status;
                                    scheduleEvent.Status = status;
                                    if (scheduleRepository.UpdateScheduleTask(scheduleEvent))
                                    {
                                        if (assessmentRepository.UpdateModal(assessment))
                                        {
                                            if (oldStatus != scheduleEvent.Status)
                                            {
                                                countChange++;
                                            }
                                            oasisViewData.isSuccessful = true;
                                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                            {
                                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                            }
                                        }
                                        else
                                        {
                                            scheduleEvent.Status = oldStatus;
                                            scheduleRepository.UpdateScheduleTask(scheduleEvent);
                                        }
                                    }
                                }
                            }
                        //}
                    //}
                });
            }
            if (oasisViewData.isSuccessful && countChange > 0)
            {
                oasisViewData.Service = Service.ToString();
                oasisViewData.ServiceId = (int)Service;
                oasisViewData.IsDataCentersRefresh = true;
                oasisViewData.IsExportOASISRefresh = true;
                oasisViewData.IsExportedOASISRefresh = status == (int)ScheduleStatus.OasisExported;
                oasisViewData.IsNotExportedOASISRefresh = status == (int)ScheduleStatus.OasisCompletedNotExported;
            }
            return oasisViewData;
        }

        public OasisAudit Audit(Guid assessmentId, Guid patientId)
        {
            var audit = new OasisAudit();
            audit.LogicalErrors = new List<Axxess.Api.Contracts.LogicalError>();
            if (!assessmentId.IsEmpty() )
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    var patientPrintProfile = profileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                    if (patientPrintProfile != null)
                    {
                        var assessment = GetAssessmentWithQuestions(patientId, assessmentId);
                        if (assessment != null)
                        {
                            var patientLocation = agencyRepository.FindLocation(Current.AgencyId, patientPrintProfile.AgencyLocationId);
                            if (patientLocation != null)
                            {
                                if (!patientLocation.IsLocationStandAlone)
                                {
                                    patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                    patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                    patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                }
                                var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                {
                                    if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                                    {
                                        assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                                    }
                                    else
                                    {
                                        assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                                    }

                                    if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                                    {
                                        assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                                    }
                                    else
                                    {
                                        assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                                    }

                                    //var submissionBodyFormat = GetOasisSubmissionFormatInstructionsNew();
                                    var oasisFormatString = GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                    if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                                    {
                                        audit.LogicalErrors = validationAgent.LogicalInconsistencyCheck(oasisFormatString);
                                    }
                                }
                                audit.AssessmentDate = assessment.AssessmentDate;
                                audit.TypeDescription = assessment.TypeDescription;
                                audit.Agency = agency;
                                audit.Location = patientLocation;
                                audit.PatientProfile = patientPrintProfile;
                            }
                        }
                    }
                }
            }
            return audit;
        }

        public JsonViewData Regenerate(Guid Id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The assessment has an error to validate." };
            var assessment = GetAssessmentWithQuestions(patientId, Id);
            if (assessment != null)
            {
                if (Validate(assessment))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The assessment data successfully generated.";
                }
            }
            return viewData;
        }

        public bool Validate(Assessment assessment)
        {
            var result = false;
            if (assessment != null)
            {
                var validationErrors = new List<Axxess.Api.Contracts.ValidationError>();
                var profile = profileRepository.GetProfileOnly( Current.AgencyId,assessment.PatientId);
                if (profile != null)
                {
                    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, assessment.EpisodeId, assessment.PatientId);
                    if (episode != null)
                    {
                        var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, assessment.PatientId, assessment.EpisodeId, assessment.Id);
                        if (scheduleEvent != null)
                        {
                            var patientLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                            if (patientLocation != null)
                            {
                                if (!patientLocation.IsLocationStandAlone)
                                {
                                    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                                    if (agency != null)
                                    {
                                        patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                        patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                        patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                    }
                                }
                                var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                {
                                    if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                                    {
                                        assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                                    }
                                    else
                                    {
                                        assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                                    }

                                    if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                                    {
                                        assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                                    }
                                    else
                                    {
                                        assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                                    }

                                    var oasisFormatString = this.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                    if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                                    {
                                        validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                        if (validationErrors.Count > 0)
                                        {
                                            validationErrors.RemoveAt(0);
                                        }
                                        validationErrors.AddRange(CustomValidation(assessmentQuestions));
                                        int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                        if (error == 0)
                                        {
                                            if (assessment.TypeName == AssessmentType.StartOfCare.ToString() || assessment.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessment.TypeName == AssessmentType.Recertification.ToString() || assessment.TypeName == AssessmentType.FollowUp.ToString())
                                            {
                                                var hipps = grouperAgent.GetHippsCode(oasisFormatString);
                                                if (hipps != null && hipps.ClaimMatchingKey != string.Empty && hipps.ClaimMatchingKey.Length == 18 && hipps.Code != string.Empty && hipps.Code.Length == 5 && hipps.Version != string.Empty && hipps.Version.Length == 5)
                                                {
                                                    var hippsCode = assessment.Questions.Find(q => q.Name == "HIPPSCODE");
                                                    if (hippsCode == null)
                                                    {
                                                        assessment.Questions.Add(new Question { Name = "HIPPSCODE", Answer = hipps.Code });
                                                    }
                                                    else
                                                    {
                                                        hippsCode.Answer = hipps.Code;
                                                    }

                                                    var hippsVersion = assessment.Questions.Find(q => q.Name == "HIPPSVERSION");
                                                    if (hippsVersion == null)
                                                    {
                                                        assessment.Questions.Add(new Question { Name = "HIPPSVERSION", Answer = hipps.Version });
                                                    }
                                                    else
                                                    {
                                                        hippsVersion.Answer = hipps.Version;
                                                    }
                                                }

                                                var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                                var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                                assessment.HippsCode = hipps.Code;
                                                assessment.HippsVersion = hipps.Version;
                                                assessment.ClaimKey = hipps.ClaimMatchingKey;
                                                assessment.SubmissionFormat = oasisFormatStringComplete;
                                                assessment.IsValidated = true;
                                                if (assessmentRepository.Update(assessment))
                                                {
                                                    result = true;
                                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                }


                                            }
                                            else if (assessment.TypeName == AssessmentType.Discharge.ToString() || assessment.TypeName == AssessmentType.Death.ToString() || assessment.TypeName == AssessmentType.TransferDischarge.ToString() || assessment.TypeName == AssessmentType.Transfer.ToString())
                                            {

                                                assessment.SubmissionFormat = oasisFormatString;
                                                assessment.IsValidated = true;
                                                if (assessmentRepository.Update(assessment))
                                                {
                                                    result = true;
                                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (assessment.TypeName == AssessmentType.StartOfCare.ToString() || assessment.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessment.TypeName == AssessmentType.Recertification.ToString() || assessment.TypeName == AssessmentType.FollowUp.ToString())
                                            {
                                                assessment.ClaimKey = string.Empty;
                                                assessment.HippsCode = string.Empty;
                                                assessment.HippsVersion = string.Empty;
                                                assessment.SubmissionFormat = string.Empty;
                                                assessment.IsValidated = false;
                                                if (assessmentRepository.Update(assessment))
                                                {
                                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not Error free");
                                                }

                                            }
                                            else if (assessment.TypeName == AssessmentType.Discharge.ToString() || assessment.TypeName == AssessmentType.Death.ToString() || assessment.TypeName == AssessmentType.TransferDischarge.ToString() || assessment.TypeName == AssessmentType.Transfer.ToString())
                                            {
                                                assessment.SubmissionFormat = string.Empty;
                                                assessment.IsValidated = false;
                                                if (assessmentRepository.Update(assessment))
                                                {
                                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not Error free");
                                                }
                                            }
                                            result = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        protected override bool Validate(Assessment assessmentOnly, out Assessment assessmentOut)
        {
            var result = false;
            var porfile = profileRepository.GetProfileOnly(Current.AgencyId, assessmentOnly.PatientId);
            if (porfile != null)
            {
                var patientLocation = agencyRepository.FindLocation(Current.AgencyId, porfile.AgencyLocationId);
                if (patientLocation != null)
                {
                    if (!patientLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }
                    var assessmentQuestionData = GetQuestions(assessmentOnly.Id);
                    if (assessmentQuestionData != null)
                    {
                        assessmentOnly.Questions = assessmentQuestionData.Question;
                       var assessmentQuestions = assessmentQuestionData.ToDictionary();
                        if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                        {
                            if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                            {
                                assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                            }
                            else
                            {
                                assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                            }

                            if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                            {
                                assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                            }
                            else
                            {
                                assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                            }
                            var oasisFormatString = this.GetOasisSubmissionFormatNew(assessmentQuestions, assessmentOnly.VersionNumber, patientLocation);
                            if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                            {
                                var validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                if (validationErrors.Count > 0)
                                {
                                    validationErrors.RemoveAt(0);
                                }
                                validationErrors.AddRange(CustomValidation(assessmentQuestions));
                                int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                if (error == 0)
                                {
                                    if (assessmentOnly.TypeName == AssessmentType.StartOfCare.ToString() || assessmentOnly.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessmentOnly.TypeName == AssessmentType.Recertification.ToString() || assessmentOnly.TypeName == AssessmentType.FollowUp.ToString())
                                    {
                                        var hipps = grouperAgent.GetHippsCode(oasisFormatString);
                                        if (hipps != null && hipps.ClaimMatchingKey != string.Empty && hipps.ClaimMatchingKey.Length == 18 && hipps.Code != string.Empty && hipps.Code.Length == 5 && hipps.Version != string.Empty && hipps.Version.Length == 5)
                                        {
                                            var hippsCode = assessmentOnly.Questions.Find(q => q.Name == "HIPPSCODE");
                                            if (hippsCode == null)
                                            {
                                                assessmentOnly.Questions.Add(new Question { Name = "HIPPSCODE", Answer = hipps.Code });
                                            }
                                            else
                                            {
                                                hippsCode.Answer = hipps.Code;
                                            }
                                            var hippsVersion = assessmentOnly.Questions.Find(q => q.Name == "HIPPSVERSION");
                                            if (hippsVersion == null)
                                            {
                                                assessmentOnly.Questions.Add(new Question { Name = "HIPPSVERSION", Answer = hipps.Version });
                                            }
                                            else
                                            {
                                                hippsVersion.Answer = hipps.Version;
                                            }
                                        }
                                        var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                        var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                        assessmentOnly.HippsCode = hipps.Code;
                                        assessmentOnly.HippsVersion = hipps.Version;
                                        assessmentOnly.ClaimKey = hipps.ClaimMatchingKey;
                                        assessmentOnly.SubmissionFormat = oasisFormatStringComplete;
                                        assessmentOnly.IsValidated = true;
                                        result = true;

                                    }
                                    else if (assessmentOnly.TypeName == AssessmentType.Discharge.ToString() || assessmentOnly.TypeName == AssessmentType.Death.ToString() || assessmentOnly.TypeName == AssessmentType.TransferDischarge.ToString() || assessmentOnly.TypeName == AssessmentType.Transfer.ToString())
                                    {
                                        assessmentOnly.SubmissionFormat = oasisFormatString;
                                        assessmentOnly.IsValidated = true;
                                        result = true;
                                    }
                                }
                                else
                                {
                                    if (assessmentOnly.TypeName == AssessmentType.StartOfCare.ToString() || assessmentOnly.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessmentOnly.TypeName == AssessmentType.Recertification.ToString() || assessmentOnly.TypeName == AssessmentType.FollowUp.ToString())
                                    {
                                        assessmentOnly.ClaimKey = string.Empty;
                                        assessmentOnly.HippsCode = string.Empty;
                                        assessmentOnly.HippsVersion = string.Empty;
                                        assessmentOnly.SubmissionFormat = string.Empty;
                                        assessmentOnly.IsValidated = false;
                                        result = false;
                                    }
                                    else if (assessmentOnly.TypeName == AssessmentType.Discharge.ToString() || assessmentOnly.TypeName == AssessmentType.Death.ToString() || assessmentOnly.TypeName == AssessmentType.TransferDischarge.ToString() || assessmentOnly.TypeName == AssessmentType.Transfer.ToString())
                                    {
                                        assessmentOnly.ClaimKey = string.Empty;
                                        assessmentOnly.HippsCode = string.Empty;
                                        assessmentOnly.HippsVersion = string.Empty;
                                        assessmentOnly.SubmissionFormat = string.Empty;
                                        assessmentOnly.IsValidated = false;
                                        result = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            assessmentOut = assessmentOnly;
            return result;
        }

        public ValidationInfoViewData ValidateInactivate(Guid assessmentId)
        {
            var validationInfo = new ValidationInfoViewData();
            validationInfo.isValid = false;
            var validationErrors = new List<Axxess.Api.Contracts.ValidationError>();
            if (!assessmentId.IsEmpty())
            {
                var assessment = GetAssessmentWithQuestions(assessmentId);
                if (assessment != null)
                {
                    var profile = profileRepository.GetProfileOnly(Current.AgencyId, assessment.PatientId);
                    if (profile != null)
                    {
                        validationInfo.AssessmentId = assessmentId;
                        validationInfo.AssessmentType = assessment.Type;
                        var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                        if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                        {
                            var oasisFormatString = OasisInactivateBody(assessmentQuestions, profile.AgencyLocationId);
                            if (oasisFormatString.IsNotNullOrEmpty() && oasisFormatString.Length == 1446)
                            {
                                validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                if (validationErrors.Count > 0)
                                {
                                    validationErrors.RemoveAt(0);
                                }
                                int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                if (error == 0)
                                {
                                    assessment.CancellationFormat = oasisFormatString;
                                    assessmentRepository.Update(assessment);
                                    validationInfo.Count = validationErrors.Count;
                                    validationInfo.ValidationErrors = validationErrors;
                                    validationInfo.Message = "Your OASIS Export Cancellation File generated successfully.";
                                    validationInfo.isValid = true;
                                }
                                else
                                {
                                    assessment.CancellationFormat = string.Empty;
                                    assessmentRepository.Update(assessment);
                                    validationInfo.Count = validationErrors.Count;
                                    validationInfo.ValidationErrors = validationErrors.OrderBy(m => m.ErrorType).ToList();
                                    validationInfo.isValid = false;
                                }
                            }
                            else
                            {
                                validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment submittion format  is not correct. Try again or contact us." });
                                validationInfo.isValid = false;
                                validationInfo.ValidationErrors = validationErrors;
                            }
                        }
                        else
                        {
                            validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment data is not found . Try again or contact us." });
                            validationInfo.isValid = false;
                            validationInfo.ValidationErrors = validationErrors;
                        }
                    }
                    else
                    {
                        validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment information is not found . Try again or contact us." });
                        validationInfo.isValid = false;
                        validationInfo.ValidationErrors = validationErrors;

                    }
                }
                else
                {
                    validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment patient information is not found . Try again or contact us." });
                    validationInfo.isValid = false;
                    validationInfo.ValidationErrors = validationErrors;
                }
            }
            else
            {
                validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment information is not correct . Try again or contact us." });
                validationInfo.isValid = false;
                validationInfo.ValidationErrors = validationErrors;
            }
            return validationInfo;
        }

        public string GenerateOASIS1448ForSelected(Guid BranchId, List<Guid> OasisSelected)
        {
            string allString = "Export File could not be created!";
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, BranchId);
                if ( agencyLocation != null)
                {
                    if (!agencyLocation.IsLocationStandAlone)
                    {
                        agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                        agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                        agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        agencyLocation.Name = agency.Name;
                        agencyLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                        agencyLocation.ContactPersonLastName = agency.ContactPersonLastName;
                        agencyLocation.ContactPersonPhone = agency.ContactPersonPhone;
                        agencyLocation.ContactPersonEmail = agency.ContactPersonEmail;

                        if (!agencyLocation.IsMainOffice)
                        {
                            var mainLocation = agencyRepository.GetMainLocation(agency.Id);
                            if (mainLocation != null)
                            {
                                agencyLocation.AddressLine1 = mainLocation.AddressLine1;
                                agencyLocation.AddressLine2 = mainLocation.AddressLine2;
                                agencyLocation.AddressCity = mainLocation.AddressCity;
                                agencyLocation.AddressStateCode = mainLocation.AddressStateCode;
                                agencyLocation.AddressZipCode = mainLocation.AddressZipCode;
                            }
                        }
                    }
                    else
                    {
                        agencyLocation.Name = agency.Name;
                    }
                    string generateOasisHeader = OasisHeader(agencyLocation);
                    var generateJsonOasis = string.Empty;
                    int count = 0;
                    var hl = generateOasisHeader.Length;
                    if (OasisSelected .IsNotNullOrEmpty())
                    {
                        OasisSelected.ForEach(Id =>
                        {
                            var assessment = GetAssessmentOnly(Id);
                            if (assessment != null && assessment.SubmissionFormat != null)
                            {
                                generateJsonOasis += assessment.SubmissionFormat + "\r\n";
                                count++;
                            }
                        });
                    }
                    var bl = generateJsonOasis.Length;
                    string generateOasisFooter = OasisFooter(count + 2);
                    var fl = generateOasisFooter.Length;
                    allString = generateOasisHeader + generateJsonOasis + generateOasisFooter;
                }
            }

          
            return allString;
        }

        public string GenerateExportFile(Guid assessmentId)
        {
            string exportString = string.Empty;
            var assessment = GetAssessmentWithQuestions(assessmentId);
            if (assessment != null)
            {
                var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                var profile = profileRepository.GetProfileOnly(Current.AgencyId,assessment.PatientId);
                if (profile != null)
                {
                    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                    if (agency != null)
                    {
                        var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, profile != null ? profile.AgencyLocationId : Guid.Empty);
                        if (agencyLocation != null)
                        {
                            if (!agencyLocation.IsLocationStandAlone)
                            {
                                agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                agencyLocation.Name = agency.Name;
                                agencyLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                                agencyLocation.ContactPersonLastName = agency.ContactPersonLastName;
                                agencyLocation.ContactPersonPhone = agency.ContactPersonPhone;
                                agencyLocation.ContactPersonEmail = agency.ContactPersonEmail;

                                if (!agencyLocation.IsMainOffice)
                                {
                                    if (agency.MainLocation != null)
                                    {
                                        agencyLocation.AddressLine1 = agency.MainLocation.AddressLine1;
                                        agencyLocation.AddressLine2 = agency.MainLocation.AddressLine2;
                                        agencyLocation.AddressCity = agency.MainLocation.AddressCity;
                                        agencyLocation.AddressStateCode = agency.MainLocation.AddressStateCode;
                                        agencyLocation.AddressZipCode = agency.MainLocation.AddressZipCode;
                                    }
                                }
                            }
                            else
                            {
                                agencyLocation.Name = agency.Name;
                            }


                            var header = OasisHeader(agencyLocation);

                            // var submissionBodyFormat = assessmentService.GetOasisSubmissionFormatInstructionsNew();
                            var body = GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, agencyLocation);

                            var footer = OasisFooter(3);

                            exportString = header + body + "\r\n" + footer;
                        }
                    }
                }
            }
            return exportString;
        }

        public string GetOASIS1448ForCancel(Guid Id)
        {
            string allString = string.Empty;
            var assessment = GetAssessmentOnly(Id);
            var agencyLocation = new AgencyLocation();
            if (assessment != null)
            {
                var profile = profileRepository.GetProfileOnly(Current.AgencyId, assessment.PatientId);
                if (profile != null)
                {
                    agencyLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                    if (agencyLocation != null && !agencyLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }
                }
            }
            string generateOasisHeader = OasisHeader(agencyLocation);

            var generateJsonOasis = string.Empty;
            var hl = generateOasisHeader.Length;


            if (assessment != null && assessment.CancellationFormat.IsNotNullOrEmpty())
            {
                generateJsonOasis = assessment.CancellationFormat + "\r\n";
            }

            var bl = generateJsonOasis.Length;
            string generateOasisFooter = OasisFooter(3);
            var fl = generateOasisFooter.Length;
            allString = generateOasisHeader + generateJsonOasis + generateOasisFooter;
            return allString;
        }

        public JsonViewData GeneratePpsExport(Guid assessmentId)
        {
            var result = new JsonViewData { isSuccessful = false, errorMessage = "The OASIS Export File could not be sent to PPS Plus." };
            var exportString = "Export File could not be created!";
            if (!assessmentId.IsEmpty() )
            {
                var assessment = GetAssessmentWithQuestions(assessmentId);
                if (assessment != null)
                {
                    var profile = profileRepository.GetProfileOnly(Current.AgencyId, assessment.PatientId);
                    if (profile != null)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            var patientLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                            if (patientLocation != null)
                            {
                                if (!patientLocation.IsLocationStandAlone)
                                {
                                    patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                    patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                    patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                }
                                var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                                //var submissionBodyFormat = assessmentService.GetOasisSubmissionFormatInstructionsNew();
                                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                {
                                    exportString = GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                    if (exportString.IsNotNullOrEmpty())
                                    {
                                        Axxess.AgencyManagement.Application.PpsPlus.PPSPlusServiceSoapClient ppsPlusClient = new Axxess.AgencyManagement.Application.PpsPlus.PPSPlusServiceSoapClient();
                                        var assessmentXml = string.Format(
                                            "<?xml version=\"1.0\" encoding=\"utf-8\"?><Assessments><Assessment><OASIS><Data>{0}</Data><ID>{1}</ID></OASIS></Assessment></Assessments>",
                                            exportString,
                                            assessment.Id);
                                        var serviceResult = ppsPlusClient.SendAssessments(agency.OasisAuditVendorApiKey, assessmentXml);
                                        if (serviceResult != null)
                                        {
                                            if (serviceResult.ResultCode == Axxess.AgencyManagement.Application.PpsPlus.ResultCodes.Success)
                                            {
                                                result.isSuccessful = true;
                                                result.errorMessage = "The OASIS Export file has been successfully sent to PPS Plus.";
                                            }
                                            else
                                            {
                                                result.isSuccessful = false;
                                                result.errorMessage = serviceResult.ErrorMessage;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public FrequenciesViewData GetPatientEpisodeFrequencyData(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var frequencyViewData = new FrequenciesViewData();
            var patientEpisode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessmentAndOasisData(patientEpisode);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                    var tmpString = new List<string>();

                    string[] taskTypes = new string[6] { "SN", "PT", "OT", "ST", "MSW", "HHA" };
                    foreach (string taskType in taskTypes)
                    {
                        string assessmentFrequency = string.Format("485{0}Frequency", taskType);
                        if (assessmentQuestions.ContainsKey(assessmentFrequency) && assessmentQuestions[assessmentFrequency].Answer.IsNotNullOrEmpty())
                        {
                            frequencyViewData.Visits.Add(taskType, new VisitData(assessmentQuestions[assessmentFrequency].Answer));
                        }
                        else
                        {
                            frequencyViewData.Visits.Add(taskType, new VisitData());
                        }
                    }
                }
                else
                {
                    string[] taskTypes = new string[6] { "SN", "PT", "OT", "ST", "MSW", "HHA" };
                    foreach (string taskType in taskTypes)
                    {
                        frequencyViewData.Visits.Add(taskType, new VisitData());
                    }
                }

                var patientActivities = scheduleRepository.GetScheduleEventsVeryLeanNonOptionalPar(Current.AgencyId, patientId, episodeId, new int[] { }, new int[] { }, patientEpisode.StartDate, patientEpisode.EndDate, true, false);
                if (patientActivities != null && patientActivities.Count > 0)
                {
                    foreach (var item in patientActivities)
                    {
                        if (item.IsBillable)
                        {
                            if (DisciplineTaskFactory.AllSkilledNurseDisciplineTasks(true).Contains(item.DisciplineTask))  //(item.IsSkilledNurseNote())
                            {
                                frequencyViewData.Visits["SN"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.HHA.ToString()))
                            {
                                frequencyViewData.Visits["HHA"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.PT.ToString()))
                            {
                                frequencyViewData.Visits["PT"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.OT.ToString()))
                            {
                                frequencyViewData.Visits["OT"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.ST.ToString()))
                            {
                                frequencyViewData.Visits["ST"].Count++;
                            }
                            else if (item.Discipline.IsEqual(Disciplines.MSW.ToString()))
                            {
                                frequencyViewData.Visits["MSW"].Count++;
                            }
                        }
                    }
                }
            }
            return frequencyViewData;
        }

        public object GetDiagnosisData(Guid patientId, Guid episodeId)
        {
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            string diag1 = "";
            string code1 = "";
            string diag2 = "";
            string code2 = "";
            if (episode != null)
            {
                var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessmentAndOasisData(episode);
                if (assessment != null)
                {
                    var questionData = mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id);
                    if (questionData != null && questionData.Question.IsNotNullOrEmpty())
                    {
                        assessment.Questions = questionData.Question;
                        var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
                        diag1 = oasisQuestions.ContainsKey("PrimaryDiagnosis") ? oasisQuestions["PrimaryDiagnosis"].Answer : "";
                        code1 = oasisQuestions.ContainsKey("ICD9M") ? oasisQuestions["ICD9M"].Answer : "";
                        diag2 = oasisQuestions.ContainsKey("PrimaryDiagnosis1") ? oasisQuestions["PrimaryDiagnosis1"].Answer : "";
                        code2 = oasisQuestions.ContainsKey("ICD9M1") ? oasisQuestions["ICD9M1"].Answer : "";
                    }
                }
            }
            bool dataFound = diag1.IsNotNullOrEmpty() && diag2.IsNotNullOrEmpty();
            return new { dataFound, diag1, code1, diag2, code2 };
        }

        public override string GetDiagnosisData(Assessment assessment)
        {
            var data = assessment.ToDictionary();
            string DiagnosisData = "({";
            if (data.AnswerOrEmptyString("M1020PrimaryDiagnosis").IsNotNullOrEmpty()) DiagnosisData += string.Format("'_M1020PrimaryDiagnosis':'{0}','_M1020ICD9M':'{1}','_M1024PaymentDiagnosesA3':'{2}','_M1024ICD9MA3':'{3}','_M1024PaymentDiagnosesA4':'{4}','_M1024ICD9MA4':'{5}','_M1020SymptomControlRating':'{6}','_485ExacerbationOrOnsetPrimaryDiagnosis':'{7}','_M1020PrimaryDiagnosisDate':'{8}',", data.AnswerOrEmptyString("M1020PrimaryDiagnosis").EscapeQuotes(), data.AnswerOrEmptyString("M1020ICD9M").EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnosesA3").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9MA3").EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnosesA4").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9MA4").EscapeQuotes(), data.AnswerOrEmptyString("M1020SymptomControlRating").EscapeQuotes(), data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis").EscapeQuotes(), data.AnswerOrEmptyString("M1020PrimaryDiagnosisDate").EscapeQuotes());
            for (int i = 1; i < 26; i++) if (data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).IsNotNullOrEmpty()) DiagnosisData += string.Format("'_M1022PrimaryDiagnosis{0}':'{2}','_M1022ICD9M{0}':'{3}','_M1024PaymentDiagnoses{1}3':'{4}','_M1022ICD9M{1}3':'{5}','_M1024PaymentDiagnoses{1}4':'{6}','_M1024ICD9M{1}4':'{7}','_M1022OtherDiagnose{0}Rating':'{8}','_485ExacerbationOrOnsetPrimaryDiagnosis{0}':'{9}','_M1022PrimaryDiagnosis{0}Date':'{10}',", i, (char)(i + 65), data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).EscapeQuotes(), data.AnswerOrEmptyString("M1022ICD9M" + i).EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(i + 65) + "3").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9M" + (char)(i + 65) + "3").EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(i + 65) + "4").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9M" + (char)(i + 65) + "4").EscapeQuotes(), data.AnswerOrEmptyString("M1022OtherDiagnose" + i + "Rating").EscapeQuotes(), data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis" + i).EscapeQuotes(), data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i + "Date").EscapeQuotes()); DiagnosisData += "'Assessment':'" + assessment.TypeName + "'})";
            return DiagnosisData;
        }

        public void SetPatientProfileEpisodeInfo(PatientProfile patientProfile)
        {
            if (patientProfile != null && patientProfile.Patient != null)
            {
                var episode = episodeRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientProfile.Patient.Id, DateTime.Now);
                if (episode != null)
                {
                    var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessmentAndOasisData(episode);
                    if (assessment != null)
                    {
                        //var questions = GetQuestions(assessment.Id);
                        //if (questions != null && questions.Question != null)
                        //{
                        //    assessment.Questions = questions.Question;
                        //}
                        var freq = assessment.Questions.ToOASISDictionary().ToFrequencyString();
                        patientProfile.Frequencies = freq;
                        patientProfile.CurrentAssessment = assessment;
                    }
                    patientProfile.CurrentEpisode = episode;
                    
                }
            }
        }

        public ManagedClaimEpisodeData GetManagedClaimEpisodeData(Guid episodeId, Guid patientId)
        {
            var managedClaim = new ManagedClaimEpisodeData();
            var assessmentEvent = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessmentEvent(episodeId, patientId);
            if (assessmentEvent != null)
            {
                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                var assessment = GetAssessmentWithQuestions(patientId, assessmentEvent.Id);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                    managedClaim.DiagnosisCode1 = assessmentQuestions.AnswerOrEmptyString("M1020ICD9M");
                    managedClaim.DiagnosisCode2 = assessmentQuestions.AnswerOrEmptyString("M1022ICD9M1");
                    managedClaim.DiagnosisCode3 = assessmentQuestions.AnswerOrEmptyString("M1022ICD9M2");
                    managedClaim.DiagnosisCode4 = assessmentQuestions.AnswerOrEmptyString("M1022ICD9M3");
                    managedClaim.DiagnosisCode5 = assessmentQuestions.AnswerOrEmptyString("M1022ICD9M4");
                    managedClaim.DiagnosisCode6 = assessmentQuestions.AnswerOrEmptyString("M1022ICD9M5");
                    managedClaim.HippsCode = assessment.HippsCode;
                    managedClaim.ClaimKey = assessment.ClaimKey;
                    managedClaim.AssessmentType = assessmentType;

                }
            }
            return managedClaim;
        }

        private IDictionary<string, NotesQuestion> CombineNoteQuestionsAndOasisQuestions(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            var questions = noteQuestions;
            if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                {
                    noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis"))
                {
                    noteQuestions.Add("PrimaryDiagnosis", oasisQuestions["PrimaryDiagnosis"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                {
                    noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M"))
                {
                    noteQuestions.Add("ICD9M", oasisQuestions["ICD9M"]);
                }
            }

            if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                {
                    noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                {
                    noteQuestions.Add("PrimaryDiagnosis1", oasisQuestions["PrimaryDiagnosis1"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                {
                    noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M1"))
                {
                    noteQuestions.Add("ICD9M1", oasisQuestions["ICD9M1"]);
                }
            }
            return questions;
        }
        
        public AssessmentPrint OASISProfileData(Guid patientId, Guid assessmentId)
        {
            var assessmentPrint = new AssessmentPrint();
            var assessment = GetAssessmentWithQuestions(patientId, assessmentId);
            if (assessment != null)
            {
                var questions = assessment.Questions.ToOASISDictionary();
                var profileWithAddress = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId,assessment.PatientId);
                var location = agencyRepository.FindLocationOrMain(Current.AgencyId, profileWithAddress != null ? profileWithAddress.AgencyLocationId : Guid.Empty);
                var episodeRange =  EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetPlanofCareCertPeriod(assessment.EpisodeId, assessment.PatientId, assessment.Id);
                var prospectivePayment = lookupRepository.GetProspectivePayment(assessment.HippsCode, episodeRange.StartDate, profileWithAddress != null && profileWithAddress.Address!=null ? (profileWithAddress.Address.ZipCode.IsNotNullOrEmpty() ? profileWithAddress.Address.ZipCode : (location != null && profileWithAddress.AgencyLocationId == location.Id ? location.AddressZipCode : string.Empty)) : string.Empty);

                if (!questions.ContainsKey("M0100AssessmentType"))
                {
                    questions.Add("M0100AssessmentType", new Question() { Answer = assessment.TypeDescription });
                }
                else
                {
                    questions["M0100AssessmentType"].Answer = assessment.TypeDescription;
                }

                if (!questions.ContainsKey("HIPPS"))
                {
                    questions.Add("HIPPS", new Question() { Answer = assessment.HippsCode });
                }
                else
                {
                    questions["HIPPS"].Answer = assessment.HippsCode;
                }

                if (!questions.ContainsKey("OASISMatchingKey"))
                {
                    questions.Add("OASISMatchingKey", new Question() { Answer = assessment.ClaimKey });
                }
                else
                {
                    questions["OASISMatchingKey"].Answer = assessment.ClaimKey;
                }

                if (!questions.ContainsKey("EpisodeStartDate"))
                {
                    questions.Add("EpisodeStartDate", new Question() { Answer = episodeRange.StartDate.ToShortDateString() });
                }
                else
                {
                    questions["EpisodeStartDate"].Answer = episodeRange.StartDate.ToShortDateString();
                }

                if (!questions.ContainsKey("EpisodeEndDate"))
                {
                    questions.Add("EpisodeEndDate", new Question() { Answer = episodeRange.EndDate.ToShortDateString() });
                }
                else
                {
                    questions["EpisodeEndDate"].Answer = episodeRange.EndDate.ToShortDateString();
                }

                if (!questions.ContainsKey("CBSACode"))
                {
                    questions.Add("CBSACode", new Question() { Answer = prospectivePayment.CbsaCode });
                }
                else
                {
                    questions["CBSACode"].Answer = prospectivePayment.CbsaCode;
                }

                if (!questions.ContainsKey("HHRG"))
                {
                    questions.Add("HHRG", new Question() { Answer = prospectivePayment.Hhrg });
                }
                else
                {
                    questions["HHRG"].Answer = prospectivePayment.Hhrg;
                }

                if (!questions.ContainsKey("Weight"))
                {
                    questions.Add("Weight", new Question() { Answer = prospectivePayment.Weight });
                }
                else
                {
                    questions["Weight"].Answer = prospectivePayment.Weight;
                }

                if (!questions.ContainsKey("WageIndex"))
                {
                    questions.Add("WageIndex", new Question() { Answer = prospectivePayment.WageIndex });
                }
                else
                {
                    questions["WageIndex"].Answer = prospectivePayment.WageIndex;
                }

                if (!questions.ContainsKey("LaborPortion"))
                {
                    questions.Add("LaborPortion", new Question() { Answer = prospectivePayment.LaborAmount });
                }
                else
                {
                    questions["LaborPortion"].Answer = prospectivePayment.LaborAmount;
                }

                if (!questions.ContainsKey("NonLabor"))
                {
                    questions.Add("NonLabor", new Question() { Answer = prospectivePayment.NonLaborAmount });
                }
                else
                {
                    questions["NonLabor"].Answer = prospectivePayment.NonLaborAmount;
                }

                if (!questions.ContainsKey("NonRoutineSuppliesAmount"))
                {
                    questions.Add("NonRoutineSuppliesAmount", new Question() { Answer = prospectivePayment.NonRoutineSuppliesAmount });
                }
                else
                {
                    questions["NonRoutineSuppliesAmount"].Answer = prospectivePayment.NonRoutineSuppliesAmount;
                }

                if (!questions.ContainsKey("TotalPayment"))
                {
                    questions.Add("TotalPayment", new Question() { Answer = prospectivePayment.TotalProspectiveAmount });
                }
                else
                {
                    questions["TotalPayment"].Answer = prospectivePayment.TotalProspectiveAmount;
                }

                assessmentPrint.Data = questions;
                assessmentPrint.Location = location;
            }
            return assessmentPrint;
        }

        protected override OasisViewData GeneratePOCAndHospitalizationLog(int status, OasisViewData oasisViewData, Assessment assessment, ScheduleEvent scheduleEvent)
        {
            var episode = episodeRepository.GetEpisodeDateRange(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EpisodeId);
            if (episode != null)
            {
                if (DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask))
                {
                    oasisViewData.IsHospitalizationLogRefresh = AddOrUpdateHospitalizationLog(assessment);
                }
                else if (DisciplineTaskFactory.ResumptionOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask))
                {
                    oasisViewData.IsHospitalizationLogRefresh = MarkEndOfHospitalization(assessment.Id, assessment.PatientId, scheduleEvent.EventDate);
                }
                if (DisciplineTaskFactory.RecertDisciplineTasks(true).Contains(scheduleEvent.DisciplineTask) || DisciplineTaskFactory.SOCDisciplineTasks(true).Contains(scheduleEvent.DisciplineTask))
                {
                    bool isRecertComplete = assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview || assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady || assessment.Status == (int)ScheduleStatus.OasisExported || assessment.Status == (int)ScheduleStatus.OasisCompletedNotExported;
                    episodeRepository.SetRecertFlag(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, isRecertComplete);
                }
                if ((status == (int)ScheduleStatus.OasisCompletedPendingReview || (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady && Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA)))
                       && AssessmentTypeFactory.GeneratePlanOfCareAssessments(scheduleEvent.EventDate, episode.EndDate).Contains(AssessmentTypeFactory.ParseString(assessment.Type)))
                {
                    oasisViewData.isSuccessful = this.CreatePlanofCare(assessment, scheduleEvent);
                }
            }
            return oasisViewData;
        }

        /// <summary>
        /// Genereates a plan of care
        /// </summary>
        /// <param name="status"></param>
        /// <param name="oasisViewData"></param>
        /// <param name="assessment"></param>
        /// <param name="scheduleEvent">The Schedule Event of the assessment passed in.</param>
        /// <returns></returns>
        protected override OasisViewData GeneratePOC(int status, OasisViewData oasisViewData, Assessment assessment, ScheduleEvent scheduleEvent)
        {
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
            if (episode != null)
            {
                if (DisciplineTaskFactory.RecertDisciplineTasks(true).Contains(scheduleEvent.DisciplineTask) || DisciplineTaskFactory.SOCDisciplineTasks(true).Contains(scheduleEvent.DisciplineTask))
                {
                    if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview || assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady)
                    {
                        episodeRepository.SetRecertFlag(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, true);
                    }
                    else
                    {
                        episodeRepository.SetRecertFlag(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, false);
                    }
                }

                if ((status == (int)ScheduleStatus.OasisCompletedPendingReview || (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady && Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA))) &&
                        (AssessmentTypeFactory.GeneratePlanOfCareAssessments(scheduleEvent.EventDate, episode.EndDate).Contains(AssessmentTypeFactory.ParseString(assessment.Type))))
                {
                    oasisViewData.isSuccessful = this.CreatePlanofCare(assessment, scheduleEvent);
                }
            }
            return oasisViewData;
        }


        public override Patient GetPatientWithProfileFromAdmissionAppSpecific(Guid patientId, Guid admissionId)
        {
            Patient patient = null;
            if (!admissionId.IsEmpty())
            {
                var admissinData = AdmissionHelperFactory<PatientEpisode>.GetPatientAdmissionDate(patientId, admissionId);
                if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                {
                    patient = admissinData.PatientData.ToObject<Patient>();
                    if (patient != null)
                    {
                        var profile = admissinData.ProfileData.ToObject<Profile>();
                        if (profile!=null)
                        {
                            patient.Profile = profile;
                        }
                    }
                }
            }
            if (patient == null)
            {
                patient = profileRepository.GetPatientWithProfile(Current.AgencyId, patientId);
            }
            return patient;
        }

        #endregion

        #region Private Members

        private List<Axxess.Api.Contracts.ValidationError> CustomValidation(IDictionary<string, Question> assessmentQuestions)
        {
            var submissionFormat = new StringBuilder();
            var oasisValidationRules = new List<OasisValidation>();
            string type = assessmentQuestions["M0100AssessmentType"].Answer;
            if (assessmentQuestions.ContainsKey("M0016BranchId") && assessmentQuestions["M0016BranchId"].Answer.IsNotNullOrEmpty() && (assessmentQuestions["M0016BranchId"].Answer != "N" || assessmentQuestions["M0016BranchId"].Answer != "P"))
            {
                oasisValidationRules.Add(new OasisValidation(() => (assessmentQuestions.ContainsKey("M0014BranchState") && !assessmentQuestions["M0014BranchState"].Answer.IsNotNullOrEmpty()) || (!assessmentQuestions.ContainsKey("M0014BranchState")) ? true : false, new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If the Branch Id(M0060) code  exist branch state needs to be filled.", ErrorDup = "M0014_BRANCH_STATE" }));
            }
            if (type.Contains("01") || type.Contains("03"))
            {
                if (assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening").IsEqual("00")
                    || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening").IsEqual("02")
                    || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening").IsEqual("03"))
                {
                    oasisValidationRules.Add(new OasisValidation(() => assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreeningInterest").IsNotNullOrEmpty() || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreeningHopeless").IsNotNullOrEmpty(), new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If Depression Screening (M1730) was not done using the PHQ-2 Scale, then PHQ Scale (M1730_PHQ2) must be left blank.", ErrorDup = "M1730_PHQ2" }));
                }
            }

            if (type.Contains("01"))
            {
                if (!assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDateUnknown").IsEqual("1"))
                {
                    var startofCareDate = assessmentQuestions.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentQuestions.AnswerOrEmptyString("M0030SocDate").ToDateTime() : DateTime.MinValue;
                    var assessmentDate = assessmentQuestions.AnswerOrEmptyString("M0090AssessmentCompleted").IsDate() ? assessmentQuestions.AnswerOrEmptyString("M0090AssessmentCompleted").ToDateTime() : DateTime.MinValue;
                    var inpatientDischargeDate = assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDate").IsDate() ? assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDate").ToDateTime() : DateTime.MinValue;

                    oasisValidationRules.Add(new OasisValidation(() => inpatientDischargeDate.Date > startofCareDate.Date || inpatientDischargeDate.Date > assessmentDate.Date, new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If (M1005) Most recent inpatient discharge date is completed, then M1005 inpatient discharge must be prior to or the same as M0030 (Start of Care Date) and M0090 (Completion Date).", ErrorDup = "M1005_INP_DISCHARGE_DT" }));
                }
            }

            if (assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderId").IsEqual(string.Empty) &&
                !assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderIdUnknown").IsEqual("1"))
            {
                oasisValidationRules.Add(new OasisValidation(() => assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderId").IsNullOrEmpty(), new Axxess.Api.Contracts.ValidationError { ErrorType = "WARNING", Description = "No NPI Information provided. Enter the NPI of the physician who will sign the Plan of Care or check the Unknown or Not Available option.", ErrorDup = "M0018_PHYSICIAN_ID" }));
            }

            var oasisEntityValidator = new OasisEntityValidator(oasisValidationRules.ToArray());
            oasisEntityValidator.Validate();

            return oasisEntityValidator.ValidationError;
        }

        private Dictionary<string, SubmissionBodyFormat> GetOasisSubmissionFormatInstructionsNew()
        {
            var dictionaryFormat = new Dictionary<string, SubmissionBodyFormat>();
            var format = lookupRepository.GetSubmissionFormatInstructions();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private Dictionary<string, SubmissionHeaderFormat> GetOasisHeaderInstructionsNew()
        {
            var format = lookupRepository.GetSubmissionHeaderFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionHeaderFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private Dictionary<string, SubmissionInactiveBodyFormat> GetInactivateOasisSubmissionFormatInstructionsNew()
        {
            var format = lookupRepository.GetSubmissionInactiveBodyFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionInactiveBodyFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private IDictionary<string, NotesQuestion> CombineOasisQuestionsAndNoteQuestions(IDictionary<string, NotesQuestion> oasisQuestions, IDictionary<string, NotesQuestion> noteQuestions)
        {
            var questions = oasisQuestions;
            noteQuestions.AddIfNotEmpty(noteQuestions, "TimeIn");
            noteQuestions.AddIfNotEmpty(noteQuestions, "TimeOut");
            noteQuestions.AddIfNotEmpty(noteQuestions, "Surcharge");
            noteQuestions.AddIfNotEmpty(noteQuestions, "AssociatedMileage");
            return questions;
        }

        #endregion
    }
}
