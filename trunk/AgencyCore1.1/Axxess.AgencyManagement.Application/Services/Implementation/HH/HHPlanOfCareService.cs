﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Helpers;
    public class HHPlanOfCareService : PlanOfCareService<ScheduleEvent,PatientEpisode>
    {
        private HHTaskRepository scheduleRepository;
        private HHPlanOfCareRepository planofCareRepository;
        private HHEpisodeRepository episodeRepository;
        public HHPlanOfCareService(HHDataProvider dataProvider)
            : base(dataProvider.TaskRepository, dataProvider.PlanOfCareRepository, dataProvider.EpisodeRepository, dataProvider.MongoRepository)
        {
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.planofCareRepository = dataProvider.PlanOfCareRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.HomeHealth;
        }

        protected override void Get485ForEditAppSpecific(PlanofCare planofCare)
        {
            if (planofCare != null)
            {
                if (planofCare.IsStandAlone)
                {
                    var episode = episodeRepository.GetEpisodeByIdWithSOC(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
                    if (episode != null)
                    {
                        planofCare.SOC = episode.StartOfCareDate;
                        planofCare.EpisodeStart = episode.StartDateFormatted;
                        planofCare.EpisodeEnd = episode.EndDateFormatted;
                    }
                }
                else
                {
                    var episodeRange = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
                    if (episodeRange != null)
                    {
                        planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                        planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                        planofCare.IsLinkedToAssessment = episodeRange.IsLinkedToAssessment;
                    }
                }
            }
        }

        protected override PlanofCare POCServiceSpecificPrintData(int disciplineTaskId, PlanofCare planofCare)
        {
            if (disciplineTaskId != (int)DisciplineTasks.HCFA485StandAlone)
            {
                var episodeRange = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
                if (episodeRange != null)
                {
                    planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                    planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                }
            }
            else
            {
                var answers = planofCare.ToDictionary();
                if (answers != null)
                {
                    var episodeAssociatedId = answers.AnswerOrEmptyGuid("EpisodeAssociated");
                    if (!episodeAssociatedId.IsEmpty())
                    {
                        var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeAssociatedId, planofCare.PatientId);
                        if (episode != null)
                        {
                            planofCare.EpisodeEnd = episode.EndDateFormatted;
                            planofCare.EpisodeStart = episode.StartDateFormatted;
                        }
                    }
                }
            }
            return planofCare;
        }

        protected override void POCActiveDateRangeAndSOC(ScheduleEvent scheduledEvent, Dictionary<string, string> dictionary)
        {
            var episode = episodeRepository.GetEpisodeByIdWithSOC(Current.AgencyId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
            if (episode != null)
            {
                dictionary.Add("PatientSoC", episode.StartOfCareDate.ToString("MM/dd/yyyy"));
                dictionary.Add("EpisodeId", episode.Id.ToString());
                dictionary.Add("EpisodeCertPeriod", string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted));
            }
        }

       
       
    }
}
