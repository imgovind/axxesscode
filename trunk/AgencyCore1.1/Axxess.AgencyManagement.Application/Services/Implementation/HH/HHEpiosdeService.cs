﻿using System;
using System.Collections.Generic;
using System.Linq;

using Axxess.Core.Enums;
using Axxess.Core.Extension;

using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Repositories;
using Axxess.Core.Infrastructure;
using Axxess.Log.Enums;
using Axxess.AgencyManagement.Application.ViewData;
using System.Web.Mvc;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.Services
{
    public class HHEpiosdeService : EpisodeService<PatientEpisode>
    {
        private HHEpisodeRepository episodeRepository;
        public HHEpiosdeService(HHDataProvider dataProvider) :
            base(dataProvider.EpisodeRepository)
        {
            base.patientProfileRepository = dataProvider.PatientProfileRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.patientAdmissionRepository = dataProvider.PatientAdmissionRepository;
            this.Service = AgencyServices.HomeHealth;
        }

        public PatientEpisode GetEpisodeForEdit(Guid episodeId, Guid patientId)
        {
            var  patientEpisode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    patientEpisode.DisplayName = profile.DisplayName;
                    patientEpisode.AgencyLocationId = profile.AgencyLocationId;
                }
                patientEpisode.NextEpisode = episodeRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, patientEpisode.EndDate);
                patientEpisode.PreviousEpisode = episodeRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, patientEpisode.StartDate);

                var selection = new List<SelectListItem>();
                var admissiondates = patientAdmissionRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                if (admissiondates != null && admissiondates.Count > 0)
                {
                    selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = patientEpisode.AdmissionId.IsEmpty() ? patientEpisode.StartOfCareDate.Date == a.StartOfCareDate.Date : a.Id == patientEpisode.AdmissionId }).ToList();
                }
                patientEpisode.AdmissionDates = selection;
                patientEpisode.IsUserCanViewLog = Current.HasRight(this.Service, ParentPermission.Episode, PermissionActions.ViewLog);
            }
            return patientEpisode;
        }
        
     

        public PatientEpisode GetPatientEpisodeFluent(Guid episodeId, Guid patientId)
        {
            return episodeRepository.GetPatientEpisodeFluent(Current.AgencyId, episodeId, patientId);
        }

        public CalendarViewData GetCalendarViewData(Guid episodeId, Guid patientId)
        {
            var calendarViewData = new CalendarViewData();
            if (!episodeId.IsEmpty())
            {
                calendarViewData.EpisodeId = episodeId;
                var profile = patientProfileRepository.GetProfileVeryLean(Current.AgencyId, patientId);
                if (profile != null)
                {
                    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        calendarViewData.EpisodeId = episode.Id;
                        calendarViewData.PatientId = patientId;
                        calendarViewData.StartDate = episode.StartDate;
                        calendarViewData.EndDate = episode.EndDate;
                    }
                    calendarViewData.DisplayName = profile.DisplayNameWithMi;
                    calendarViewData.PatientId = patientId;
                    calendarViewData.IsDischarged = profile.IsDischarged;
                }
            }
            return calendarViewData;
        }

        public ReassignViewData GetReassignViewData(Guid episodeId, Guid patientId, string type)
        {
            var viewData = new ReassignViewData { EpisodeId = episodeId, PatientId = patientId, Type = type };
            if (!patientId.IsEmpty())
            {
                viewData.Type = "Patient";
                if (!episodeId.IsEmpty())
                {
                    var patientEpisode = episodeRepository.GetPatientEpisodeFluent(Current.AgencyId, episodeId, patientId);
                    if (patientEpisode != null)
                    {
                        viewData.PatientDisplayName = patientEpisode.DisplayName;
                        viewData.StartDate = patientEpisode.StartDate;
                        viewData.EndDate = patientEpisode.EndDate;
                        viewData.Type = "Episode";
                    }
                }
                else
                {
                    viewData.PatientDisplayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                }
            }
            return viewData;
        }

        public EpisodeLeanViewData GetNonActiveEpisodeViewData(Guid patientId)
        {
            var viewData = new EpisodeLeanViewData();
            viewData.List = episodeRepository.GetPatientDeactivatedAndDischargedEpisodes(Current.AgencyId, patientId);
            if (viewData.List.IsNotNullOrEmpty())
            {
                var permissions = Current.Permissions;
                viewData.IsUserCanEdit = permissions.IsInPermission(this.Service, ParentPermission.Episode, PermissionActions.Edit);
                viewData.IsUserCanReactivate = permissions.IsInPermission(this.Service, ParentPermission.Episode, PermissionActions.Reactivate);

            }
            return viewData;
        }

        public List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid patientId)
        {
            return episodeRepository.GetPatientDeactivatedAndDischargedEpisodes(Current.AgencyId, patientId);
        }

         #region Therapy Exception

       
        #endregion

        #region Episode

        public EpisodeDate GetMasterCalendarEpiosdePeriod(Guid episodeId, Guid patientId, string assessmentType)
        {
            var modelData = new EpisodeDate();
            var episode = episodeRepository.GetPatientEpisodeLean(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                if (assessmentType.IsEqual("Recertification"))
                {
                    var nextEpisode = episodeRepository.GetNextEpisodeByStartDate(Current.AgencyId, patientId, episode.EndDate.AddDays(1));
                    if (nextEpisode != null && episode.EndDate.AddDays(1).Date == nextEpisode.StartDate.Date)
                    {
                        modelData.StartDate = nextEpisode.StartDate;
                        modelData.EndDate = nextEpisode.EndDate;
                    }
                    else
                    {
                        modelData.StartDate = episode.EndDate.AddDays(1);
                        modelData.EndDate = modelData.StartDate.AddDays(59);
                    }
                }
                else
                {
                    modelData.EndDate = episode.EndDate;
                    modelData.StartDate = episode.StartDate;
                }
            }
            return modelData;
        }
     
        public override JsonViewData UpdateEpisode(PatientEpisode patientEpisode)
        {
            patientEpisode.IsActive = !patientEpisode.IsActive;
            return base.UpdateEpisode(patientEpisode);
        }


        //public bool UpdateEpisodeForDischarge(Guid patientId, Guid episodeId, DateTime dischargeDate)
        //{
        //    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
        //    return UpdateEpisodeForDischarge(patientId, dischargeDate, episode);
        //}

        //public bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, out Guid dischargeEpisodeId)
        //{
        //    var result = false;
        //    dischargeEpisodeId = Guid.Empty;
        //    var episode = episodeRepository.GetEpisodeDateInBetween(Current.AgencyId, patientId, dischargeDate);
        //    if (UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
        //    {
        //        if (episode != null)
        //        {
        //            dischargeEpisodeId = episode.Id;
        //        }
        //        result = true;
        //    }
        //    return result;
        //}

        //private bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, PatientEpisode episode)
        //{
        //    var result = false;
            
        //    try
        //    {
        //        var patientEpisodes = new List<PatientEpisode>();
                
        //        if (episode != null)
        //        {
        //            episode.EndDate = dischargeDate;
        //            episode.IsLinkedToDischarge = true;
        //            episode.Modified = DateTime.Now;
        //            patientEpisodes.Add(episode);
        //        }
        //        var episodes = episodeRepository.EpisodesToDischarge(Current.AgencyId, patientId, dischargeDate);
        //        if (episodes != null && episodes.Count > 0)
        //        {
        //            episodes.ForEach(es =>
        //            {
        //                es.Modified = DateTime.Now;
        //                es.IsDischarged = true;
        //            });
        //            patientEpisodes.AddRange(episodes);
        //        }
        //        if (patientEpisodes.Count > 0)
        //        {
        //            if (episodeRepository.UpdateEpisodesForDischarge(Current.AgencyId, patientId, episode, patientEpisodes))
        //            {
        //                if (episode != null)
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeEdited, "Updated for patient discharge");
        //                }
        //                if (episodes != null && episodes.Count > 0)
        //                {
        //                    Auditor.AddGeneralMulitLog(LogDomain.Patient, episode.PatientId, episodes.Select(e => e.Id.ToString()).Distinct().ToList(), LogType.Episode, LogAction.EpisodeDeactivatedForDischarge, string.Empty);
        //                }
        //                result = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //    return result;
        //}

        //public List<PatientEpisode> GetEpisodeDatasBetween( Guid patientId, DateTime startDate, DateTime endDate)
        //{
        //    return episodeRepository.GetEpisodeDatasBetween(Current.AgencyId, patientId, startDate, endDate);
        //}

        #endregion


        public override List<Validation> EpisodeValidation(Patient patient, PatientEpisode patientEpisode)
        {
            var validationRules = new List<Validation>();
            validationRules.Add(new Validation(() => patient == null, "Patient data is not available."));

            var admissionData = patientAdmissionRepository.GetPatientAdmissionDate(Current.AgencyId,patient.Id, patientEpisode.AdmissionId);
            validationRules.Add(new Validation(() => admissionData == null, "Admission data is not available."));

            validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
            validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
            validationRules.Add(new Validation(() => patientEpisode.PrimaryInsurance == 0, "Insurance is required."));
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
            }
            validationRules.Add(new Validation(() => episodeRepository.IsEpisodeExist(Current.AgencyId, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate,patientEpisode.Id), "Episode date is not in the valid date range. Overlapping episode."));
            //if (patientEpisode.Id.IsEmpty())
            //{
            //    validationRules.Add(new Validation(() => !IsValidEpisode(patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
            //}
            //else
            //{
            //    validationRules.Add(new Validation(() => !I sValidEpisode(patientEpisode.Id, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));

            //}
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be greater than start of care date."));
            }
            return validationRules;
        }

        protected override string GetPartialMessageType()
        {
            return "Episode";
        }
    }
}
