﻿using System;
using System.Collections.Generic;
using System.Linq;

using Axxess.Core.Extension;
using Axxess.AgencyManagement.Repositories;
using Axxess.AgencyManagement.Entities;
using Axxess.Log.Enums;
using Axxess.Core;
using Axxess.Core.Enums;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Application.Helpers;
using Axxess.AgencyManagement.Application.iTextExtension;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Text;
using Axxess.Membership.Logging;
using Axxess.LookUp.Repositories;

namespace Axxess.AgencyManagement.Application.Services
{
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Web.Script.Serialization;

    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.LookUp.Repositories;
    using Axxess.Membership.Logging;

    public class HHPatientProfileService : PatientProfileService
    {
        private readonly HHEpisodeRepository episodeRepository;
        private readonly HHBillingRepository billingRepository;
        private readonly HHPatientProfileRepository patientProfileRepository;
        public HHPatientProfileService(HHDataProvider dataProvider):
            base(dataProvider.PatientProfileRepository)
        {
            this.patientProfileRepository = dataProvider.PatientProfileRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.referralRepository = dataProvider.ReferralRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.billingRepository = dataProvider.BillingRepository;
            base.patientAdmissionRepository = dataProvider.PatientAdmissionRepository;
            base.drugService = Container.Resolve<IDrugService>();
            base.Service = AgencyServices.HomeHealth;
        }

        protected override void SetStatus(Patient patient, Profile profile)
        {
            patient.HomeHealthStatus = profile.Status;
        }

        protected override int GetStatus(Patient patient)
        {
            return patient.HomeHealthStatus;
        }

        protected override bool AddPatientAdmissionDateAppSpecific(PatientAdmissionDate admissionData)
        {
            return AdmissionHelperFactory<PatientEpisode>.AddPatientAdmissionDate(admissionData);
        }
        protected override bool UpdatePatientAdmissionDateAppSpecific(PatientAdmissionDate admissionData)
        {
            return AdmissionHelperFactory<PatientEpisode>.UpdatePatientAdmissionDate(admissionData);
        }
        protected override PatientAdmissionDate GetPatientAdmissionDateAppSpecific(Guid patientId, Guid admissionDataId)
        {
            return AdmissionHelperFactory<PatientEpisode>.GetPatientAdmissionDate(patientId, admissionDataId);
        }

        protected override void EditValidateAppSpecific(Patient patient, Profile homeHealthProfile, List<Validation> rules)
        {
            rules.Add(new Validation(() => string.IsNullOrEmpty(homeHealthProfile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
            rules.Add(new Validation(() => !homeHealthProfile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
            if (patient.MedicareNumber.IsNotNullOrEmpty())
            {
                bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
            }
            if (patient.MedicaidNumber.IsNotNullOrEmpty())
            {
                bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
            }
            if (homeHealthProfile.ReferralDate.IsValid())
            {
                rules.Add(new Validation(() => homeHealthProfile.StartofCareDate < homeHealthProfile.ReferralDate, "Referral date must be less than or equal to the start of care."));
            }
            if (homeHealthProfile.Status == (int)PatientStatus.Discharged)
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(homeHealthProfile.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !homeHealthProfile.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
            }
            if (homeHealthProfile.PrimaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
            }
            if (homeHealthProfile.SecondaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
            }

            if (homeHealthProfile.TertiaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
            }
        }

        protected override void EditValidateAppSpecific(Profile homeHealthProfile, List<Validation> rules)
        {
            rules.Add(new Validation(() => string.IsNullOrEmpty(homeHealthProfile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
            rules.Add(new Validation(() => !homeHealthProfile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
            if (homeHealthProfile.ReferralDate.IsValid())
            {
                rules.Add(new Validation(() => homeHealthProfile.StartofCareDate < homeHealthProfile.ReferralDate, "Referral date must be less than or equal to the start of care."));
            }
            if (homeHealthProfile.Status == (int)PatientStatus.Discharged)
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(homeHealthProfile.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !homeHealthProfile.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
            }
            if (homeHealthProfile.PrimaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
            }
            if (homeHealthProfile.SecondaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
            }

            if (homeHealthProfile.TertiaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
            }
        }



        protected override void UpdateEpisodeAndClaimsForDischargePatientAppSpecific(Guid patientId, DateTime dischargeDate, Guid episodeId)
        {
            UpdateEpisodeAndClaimsForDischargePatient(patientId, dischargeDate, episodeId);
        }

        private void UpdateEpisodeAndClaimsForDischargePatient(Guid patientId, DateTime dischargeDate, Guid episodeId)
        {
            var result = false;
            if (episodeId.IsEmpty())
            {
                if (UpdateEpisodeForDischarge(patientId, dischargeDate, out episodeId))
                {
                    result = true;
                }
            }
            else
            {
                if (UpdateEpisodeForDischarge(patientId, episodeId, dischargeDate))
                {
                    result = true;
                }
            }
            if (result && !episodeId.IsEmpty())
            {
                if (!episodeId.IsEmpty() && dischargeDate.IsValid())
                {
                    billingRepository.UpdateFinalForDischarge(Current.AgencyId, episodeId, dischargeDate);
                    billingRepository.UpdateRapForDischarge(Current.AgencyId, episodeId, dischargeDate);
                }
            }
        }


        public bool UpdateEpisodeForDischarge(Guid patientId, Guid episodeId, DateTime dischargeDate)
        {
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            return UpdateEpisodeForDischarge(patientId, dischargeDate, episode);
        }

        public bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, out Guid dischargeEpisodeId)
        {
            var result = false;
            dischargeEpisodeId = Guid.Empty;
            var episode = episodeRepository.GetEpisodeFromDate(Current.AgencyId, patientId, dischargeDate);
            if (UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
            {
                if (episode != null)
                {
                    dischargeEpisodeId = episode.Id;
                }
                result = true;
            }
            return result;
        }

        private bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, PatientEpisode episode)
        {
            var result = false;

            try
            {
                var patientEpisodes = new List<PatientEpisode>();

                if (episode != null)
                {
                    episode.EndDate = dischargeDate;
                    episode.IsLinkedToDischarge = true;
                    episode.Modified = DateTime.Now;
                    //patientEpisodes.Add(episode);
                }
                var episodes = episodeRepository.EpisodesToDischarge(Current.AgencyId, patientId, dischargeDate);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(es =>
                    {
                        es.Modified = DateTime.Now;
                        es.IsDischarged = true;
                    });
                    patientEpisodes.AddRange(episodes);
                }
                if (episode!=null ||( patientEpisodes!=null && patientEpisodes.Count > 0))
                {
                    if (episodeRepository.UpdateEpisodesForDischarge(Current.AgencyId, patientId, episode, patientEpisodes))
                    {
                        if (episode != null)
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeEdited, "Updated for patient discharge");
                        }
                        if (episodes != null && episodes.Count > 0)
                        {
                            Auditor.AddGeneralMulitLog(LogDomain.Patient, episode.PatientId, episodes.Select(e => e.Id.ToString()).Distinct().ToList(), LogType.Episode, LogAction.EpisodeDeactivatedForDischarge, string.Empty);
                        }
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        protected override DateRange GetEpisodeDateRange(Guid patientId, Guid episodeId)
        {
            var dateRange = new DateRange();
            if (!patientId.IsEmpty())
            {
                if (!patientId.IsEmpty())
                {
                    var episode = episodeRepository.GetEpisodeDateRange(Current.AgencyId, patientId, episodeId);
                    if (episode != null)
                    {
                        dateRange.StartDate = episode.StartDate;
                        dateRange.EndDate = episode.EndDate;
                    }
                }
            }
            return dateRange;
        }

        protected override List<PatientSelection> GetPatientSelectionAppSpecific(List<Guid> branchIds, byte statusId)
        {
            return episodeRepository.GetPatientEpisodeDataForList(Current.AgencyId, branchIds, Current.UserId, statusId);
        }

        public MedicareEligibilityPdf MedicareEligibilityReportPdf(PatientEligibility eligibility)
        {
            return new MedicareEligibilityPdf(eligibility, agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId), null);
        }

        public MedicareEligibilityPdf MedicareEligibilityReportPdf(Guid patientId, Guid mcareEligibilityId)
        {
            PatientEligibility pEligibility = null;
            MedicareEligibility eligibility = patientProfileRepository.GetMedicareEligibility(Current.AgencyId, patientId, mcareEligibilityId);
            if (eligibility != null && eligibility.Result.IsNotNullOrEmpty())
            {
                pEligibility = eligibility.Result.FromJson<PatientEligibility>();
            }
            PatientProfileLean profile = patientProfileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
            return new MedicareEligibilityPdf(pEligibility, profile != null ? agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId) : agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId), profile);
        }

        #region Eligibility

        public PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility patientEligibility = null;
            try
            {
                var jsonData = new
                {
                    input_medicare_number = medicareNumber,
                    input_last_name = lastName,
                    input_first_name = firstName.Substring(0, 1),
                    input_date_of_birth = dob.ToString("MM/dd/yyyy"),
                    input_gender_id = gender.Substring(0, 1)
                };

                var javaScriptSerializer = new JavaScriptSerializer();
                var jsonRequest = javaScriptSerializer.Serialize(jsonData);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonRequest);
                byte[] requestData = encoding.GetBytes(postData);

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(UrlSettings.PatientEligibilityUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                var jsonResult = string.Empty;
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jsonResult = streamReader.ReadToEnd();
                    }

                    if (jsonResult.IsNotNullOrEmpty())
                    {
                        patientEligibility = javaScriptSerializer.Deserialize<PatientEligibility>(jsonResult);
                        if (patientEligibility != null && patientEligibility.Episode != null && patientEligibility.Episode.reference_id.IsNotNullOrEmpty())
                        {
                            var npiData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(patientEligibility.Episode.reference_id.Trim());
                            if (npiData != null)
                            {
                                patientEligibility.Other_Agency_Data = new OtherAgencyData()
                                {
                                    name = npiData.ProviderOrganizationName,
                                    address1 = npiData.ProviderFirstLineBusinessMailingAddress,
                                    address2 = npiData.ProviderSecondLineBusinessMailingAddress,
                                    city = npiData.ProviderBusinessMailingAddressCityName,
                                    state = npiData.ProviderBusinessMailingAddressStateName,
                                    zip = npiData.ProviderBusinessMailingAddressPostalCode,
                                    phone = npiData.ProviderBusinessMailingAddressTelephoneNumber,
                                    fax = npiData.ProviderBusinessMailingAddressFaxNumber
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return patientEligibility;
        }

        public PatientEligibility GetMedicareEligibility(Guid patientId, Guid medicareEligibilityId)
        {
            var result = new PatientEligibility();
            var medicareEligibility = patientProfileRepository.GetMedicareEligibility(Current.AgencyId, patientId, medicareEligibilityId);
            if (medicareEligibility != null && medicareEligibility.Result.IsNotNullOrEmpty())
            {
                var javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Deserialize<PatientEligibility>(medicareEligibility.Result);
                string npi = result != null && result.Episode != null && result.Episode.reference_id.IsNotNullOrEmpty() ? result.Episode.reference_id : null;
                if (npi != null)
                {
                    var OtherAgencyData = lookupRepository.GetNpiData(npi);
                    if (OtherAgencyData != null)
                    {
                        result.Other_Agency_Data.name = OtherAgencyData.ProviderOrganizationName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderOrganizationName : string.Empty;
                        result.Other_Agency_Data.address1 = OtherAgencyData.ProviderFirstLineBusinessPracticeLocationAddress.IsNotNullOrEmpty() ? OtherAgencyData.ProviderFirstLineBusinessPracticeLocationAddress : string.Empty;
                        result.Other_Agency_Data.address2 = OtherAgencyData.ProviderSecondLineBusinessPracticeLocationAddress.IsNotNullOrEmpty() ? OtherAgencyData.ProviderSecondLineBusinessPracticeLocationAddress : string.Empty;
                        result.Other_Agency_Data.city = OtherAgencyData.ProviderBusinessPracticeLocationAddressCityName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressCityName : string.Empty;
                        result.Other_Agency_Data.state = OtherAgencyData.ProviderBusinessMailingAddressStateName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessMailingAddressStateName : string.Empty;
                        result.Other_Agency_Data.zip = OtherAgencyData.ProviderBusinessMailingAddressPostalCode.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessMailingAddressPostalCode : string.Empty;
                        result.Other_Agency_Data.phone = OtherAgencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber : string.Empty;
                        result.Other_Agency_Data.fax = OtherAgencyData.ProviderBusinessPracticeLocationAddressFaxNumber.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressFaxNumber : string.Empty;
                    }
                }
            }
            return result;
        }

        public List<MedicareEligibility> GetMedicareEligibilities(Guid patientId)
        {
            return patientProfileRepository.GetMedicareEligibilities(Current.AgencyId, patientId);
        }


        public List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId)
        {
            var eligibilities = GetMedicareEligibilities(patientId);
            if (eligibilities != null && eligibilities.Count > 0)
            {
                var episodes = episodeRepository.GetPatientEpisodesByIds(Current.AgencyId, patientId, eligibilities.Select(el => el.EpisodeId).ToList());
                if (episodes != null && episodes.Count > 0)
                {
                    eligibilities.ForEach(el =>
                    {
                        if (!el.EpisodeId.IsEmpty())
                        {
                            var episode = episodes.FirstOrDefault(e => e.Id == el.EpisodeId);
                            if (episode != null)
                            {
                                el.EpisodeRange = episode.Range;
                            }
                            else
                            {
                                el.EpisodeRange = "Not in an episode";
                            }
                        }
                        el.AssignedTo = "Axxess";
                        el.TaskName = "Medicare Eligibility Report";
                        el.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + el.PatientId + "', 'mcareEligibilityId': '" + el.Id + "' });\"><span class='img icon print'></span></a>";
                    });
                }
            }
            return eligibilities.OrderBy(e => e.Created.ToShortDateString().ToZeroFilled()).ToList();
        }


        #endregion

    }
}
