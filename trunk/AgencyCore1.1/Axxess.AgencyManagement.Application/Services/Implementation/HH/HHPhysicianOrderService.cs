﻿namespace Axxess.AgencyManagement.Application.Services
{
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public class HHPhysicianOrderService : PhysicianOrderService<ScheduleEvent,PatientEpisode>
    {
        private HHTaskRepository scheduleRepository;
        private HHPhysicianOrderRepository noteRepository;
        private HHEpisodeRepository episodeRepository;

        public HHPhysicianOrderService(HHDataProvider agencyManagementDataProvider)
            : base(agencyManagementDataProvider.TaskRepository, agencyManagementDataProvider.PhysicianOrderRepository, agencyManagementDataProvider.EpisodeRepository)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            base.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            base.patientRepository = agencyManagementDataProvider.PatientRepository;
            base.profileRepository = agencyManagementDataProvider.PatientProfileRepository;
            base.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            
            this.scheduleRepository = agencyManagementDataProvider.TaskRepository;
            this.noteRepository = agencyManagementDataProvider.PhysicianOrderRepository;
            this.episodeRepository = agencyManagementDataProvider.EpisodeRepository;
            this.Service = AgencyServices.HomeHealth;

        }
      
        protected override void SetEpisode(PhysicianOrder order)
        {
            var episode = episodeRepository.GetEpisodeDateRange(Current.AgencyId, order.PatientId, order.EpisodeId);
            if (episode != null)
            {
                if (order.IsOrderForNextEpisode)
                {
                    order.EpisodeStartDate = episode.EndDate.AddDays(1).ToShortDateString();
                    order.EpisodeEndDate = episode.EndDate.AddDays(60).ToShortDateString();
                }
                else
                {
                    order.EpisodeRange = episode.StartDateFormatted + " - " + episode.EndDateFormatted;
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
            }
        }

        //public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId)
        //{
        //    var orders = new List<Order>();
        //    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
        //    if (episode != null)
        //    {
        //        var disciplineTasks = DisciplineTaskFactory.AllPhysicianOrders().ToArray();
        //        var schedules = scheduleRepository.GetCurrentAndPreviousOrders(Current.AgencyId, episode, disciplineTasks);
        //        if (schedules != null && schedules.Count > 0)
        //        {
        //            orders = GetOrdersFromScheduleEvents(episode.PatientId, episode.StartDate, episode.EndDate, schedules);
        //        }
        //    }
        //    return orders;
        //}
     
    }
}
