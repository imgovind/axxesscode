﻿using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Repositories;
using Axxess.Core.Extension;
using Axxess.Core;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.Services
{
   public class HHIncidentAccidentService : IncidentAccidentService<ScheduleEvent,PatientEpisode>
    {
        public HHIncidentAccidentService(HHDataProvider dataProvider)
            : base(dataProvider.TaskRepository, dataProvider.IncidentAccidentRepository, dataProvider.EpisodeRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            this.Service = AgencyServices.HomeHealth;
        }
    }
}
