﻿namespace Axxess.AgencyManagement.Application.Services
{
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.Extensions;
   public class HHInfectionService : InfectionService<ScheduleEvent, PatientEpisode>
    {
        private HHTaskRepository scheduleRepository;
        private HHInfectionRepository noteRepository;
        private HHEpisodeRepository episodeRepository;

        public HHInfectionService(HHDataProvider dataProvider)
            : base(dataProvider.TaskRepository, dataProvider.InfectionRepository, dataProvider.EpisodeRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            this.profileRepository = dataProvider.PatientProfileRepository;
            
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.InfectionRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.HomeHealth;

        }

        protected override void GetInfectionEpisodeData(Infection infection)
        {
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, infection.EpisodeId, infection.PatientId);
            if (episode != null)
            {
                infection.EpisodeEndDate = episode.EndDateFormatted;
                infection.EpisodeStartDate = episode.StartDateFormatted;
                var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessmentAndOasisData(episode);
                infection.Diagnosis = new Dictionary<string, string>();
                if (assessment != null)
                {
                    var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
                    infection.Diagnosis.Add("PrimaryDiagnosis", oasisQuestions.AnswerOrEmptyString("PrimaryDiagnosis"));
                    infection.Diagnosis.Add("ICD9M", oasisQuestions.AnswerOrEmptyString("ICD9M"));
                    infection.Diagnosis.Add("SecondaryDiagnosis", oasisQuestions.AnswerOrEmptyString("PrimaryDiagnosis1"));
                    infection.Diagnosis.Add("ICD9M2", oasisQuestions.AnswerOrEmptyString("ICD9M1"));
                }
            }
        }
    }
}
