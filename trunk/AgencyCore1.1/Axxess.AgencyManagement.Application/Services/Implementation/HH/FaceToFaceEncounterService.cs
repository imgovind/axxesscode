﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.Membership.Logging;
    using Axxess.AgencyManagement.Entities.Repositories;

   public class FaceToFaceEncounterService
   {
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly PatientProfileAbstract profileRepository;
        private readonly IPhysicianRepository physicianRepository;

        private readonly HHTaskRepository scheduleRepository;
        private readonly FaceToFaceEncounterRepository noteRepository;
        private readonly HHEpisodeRepository episodeRepository;
        protected AgencyServices Service { get; set; }

        public FaceToFaceEncounterService(HHDataProvider dataProvider)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.profileRepository = dataProvider.PatientProfileRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.FaceToFaceEncounterRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.HomeHealth;
        }

        #region Face To FaceEncounter

        public JsonViewData AddScheduleTaskAndFaceToFaceEncounterHelper(Guid patientId, List<ScheduleEvent> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected Face To Face Encounter report task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var faceToFaceEncounters = this.GetFaceToFaceEncounterFromTasks(patientId, scheduleEvents);
                if (faceToFaceEncounters != null)
                {
                    if (scheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (noteRepository.AddMultipleFaceToFaceEncounter(faceToFaceEncounters))
                        {
                            viewData.isSuccessful = true;
                            viewData.PatientId = patientId;
                            viewData.IsFaceToFaceEncounterRefresh = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            scheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }

                }
            }
            return viewData;
        }

        public bool CreateFaceToFaceEncounterAfterPhysicianInfo(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                faceToFaceEncounter.UserId = Current.UserId;
                faceToFaceEncounter.AgencyId = Current.AgencyId;
                faceToFaceEncounter.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                faceToFaceEncounter.OrderNumber = patientRepository.GetNextOrderNumber();
                faceToFaceEncounter.Modified = DateTime.Now;
                faceToFaceEncounter.Created = DateTime.Now;
               
                var newScheduleEvent = new ScheduleEvent
                {
                    AgencyId = Current.AgencyId,
                    Id = faceToFaceEncounter.Id,
                    UserId = faceToFaceEncounter.UserId,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Status = faceToFaceEncounter.Status,
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = faceToFaceEncounter.RequestDate,
                    VisitDate = faceToFaceEncounter.RequestDate,
                    DisciplineTask = (int)DisciplineTasks.FaceToFaceEncounter
                };
                if (noteRepository.AddFaceToFaceEncounter(faceToFaceEncounter))
                {
                    if (scheduleRepository.AddScheduleTask(newScheduleEvent))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.Id, Actions.Add, DisciplineTasks.FaceToFaceEncounter);
                        result = true;
                    }
                    else
                    {
                        noteRepository.RemoveFaceToFaceEncounter(Current.AgencyId,faceToFaceEncounter.PatientId, faceToFaceEncounter.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public JsonViewData CreateFaceToFaceEncounterBeforePhysicianInfoSet(FaceToFaceEncounter faceToFaceEncounter)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Face-to-face Encounter could not be saved", Service = AgencyServices.HomeHealth.ToString() };
            try
            {
                if (faceToFaceEncounter.IsValid())
                {
                    faceToFaceEncounter.Id = Guid.NewGuid();
                    if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                    {
                        var physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            faceToFaceEncounter.PhysicianData = physician.ToXml();
                        }
                    }
                    if (this.CreateFaceToFaceEncounterAfterPhysicianInfo(faceToFaceEncounter))
                    {

                        viewData.PatientId = faceToFaceEncounter.PatientId;
                        viewData.EpisodeId = faceToFaceEncounter.EpisodeId;
                        viewData.IsDataCentersRefresh = true;
                        viewData.IsFaceToFaceEncounterRefresh = true;
                        viewData.IsOrdersToBeSentRefresh = true;
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Face-to-face Encounter successfully saved";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the Face-to-face Encounter.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = faceToFaceEncounter.ValidationMessage;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return viewData;
            }
            return viewData;
        }

        public bool MarkFaceToFaceEncounterAsReturned(Guid id, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var faceToFaceEncounter = noteRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
            if (faceToFaceEncounter != null)
            {
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;

                    if (scheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        faceToFaceEncounter.ReceivedDate = receivedDate;
                        faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                        if (noteRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter))
                        {
                            result = true;
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                        }
                        else
                        {
                            scheduleEvent.Status = oldStatus;
                            scheduleRepository.UpdateScheduleTask(scheduleEvent);
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateFaceToFaceOrderDates(Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var faceToFaceEncounter = noteRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
            if (faceToFaceEncounter != null)
            {
                faceToFaceEncounter.ReceivedDate = receivedDate;
                faceToFaceEncounter.SentDate = sendDate;
                faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                if (faceToFaceEncounter.SignatureText.IsNullOrEmpty())
                {
                    var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                    {
                        physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                    }
                }
                result = noteRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
            }
            return result;
        }

        public  bool UpdateFaceToFaceEncounterForScheduleDetail(ScheduleEvent schedule)
        {
            var result = false;
            var faceToFaceEncounter = noteRepository.GetFaceToFaceEncounter(schedule.Id, Current.AgencyId);
            if (faceToFaceEncounter != null)
            {
                faceToFaceEncounter.Status = schedule.Status;
                if (faceToFaceEncounter.Status != schedule.Status && faceToFaceEncounter.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        faceToFaceEncounter.PhysicianData = physician.ToXml();
                    }
                }
                faceToFaceEncounter.PhysicianId = schedule.PhysicianId;
                faceToFaceEncounter.IsDeprecated = schedule.IsDeprecated;
                faceToFaceEncounter.RequestDate = schedule.VisitDate;
                if (noteRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter))
                {
                    result = true;
                }
            }
            return result;
        }

        public int MarkFaceToFaceEncounterAsSent(bool sendElectronically, string[] answersArray, ref List<AgencyPhysician> physicians)
        {
            int count = 0;
            AgencyPhysician physician = null;
            var status = (int)ScheduleStatus.OrderSentToPhysician;
            if (sendElectronically)
            {
                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
            }

            foreach (var item in answersArray)
            {
                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                var facetofaceId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                var faceToFaceEncounter = noteRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    if (faceToFaceEncounter.PhysicianData.IsNotNullOrEmpty()) physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = status;
                        if (scheduleRepository.UpdateScheduleTask(scheduleEvent))
                        {
                            if (noteRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
                            {
                                count++;
                                if (sendElectronically)
                                {
                                    if (physician != null && !physicians.Exists(p => physician.Id == p.Id))
                                    {
                                        physicians.Add(physician);
                                    }
                                }
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {

                                scheduleEvent.Status = oldStatus;
                                scheduleRepository.UpdateScheduleTask(scheduleEvent);
                            }
                        }
                    }

                }
            }
            return count;
        }

        public JsonViewData ToggleFaceToFaceEncounter(Guid patientId, Guid Id, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Face To Face Encounter could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            var task = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, Id);
            if (task != null)
            {
                viewData = ToggleFaceToFaceEncounter(task, isDeprecated);
            }
            return viewData;
        }

        public JsonViewData ToggleFaceToFaceEncounter(ScheduleEvent task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Face To Face Encounter could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (scheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (noteRepository.DeleteFaceToFaceEncounter(Current.AgencyId, task.Id, task.PatientId, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.IsFaceToFaceEncounterRefresh = true;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.errorMessage = string.Format("The Face To Face Encounter has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        scheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }


        public JsonViewData ReopenFaceToFaceEncounter(ScheduleEvent scheduleEvent)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleEvent != null)
            {
                var oldUserId = scheduleEvent.UserId;
                var oldStatus = scheduleEvent.Status;
                var description = string.Empty;

                viewData.PatientId = scheduleEvent.PatientId;
                viewData.EpisodeId = scheduleEvent.EpisodeId;

                scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened);
                var faceToFaceOrder = noteRepository.GetFaceToFaceEncounter(scheduleEvent.Id, scheduleEvent.PatientId, Current.AgencyId);
                if (faceToFaceOrder != null)
                {
                    if (scheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        faceToFaceOrder.UserId = scheduleEvent.UserId;
                        faceToFaceOrder.Status = (int)ScheduleStatus.OrderReopened;
                        faceToFaceOrder.SignatureText = string.Empty;
                        faceToFaceOrder.SignatureDate = DateTime.MinValue;
                        faceToFaceOrder.ReceivedDate = DateTime.MinValue;
                        if (noteRepository.UpdateFaceToFaceEncounter(faceToFaceOrder))
                        {
                            viewData.isSuccessful = true;
                            viewData.IsFaceToFaceEncounterRefresh = true;
                            viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);
                            viewData.IsDataCentersRefresh = true;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<ScheduleEvent>.ShouldMyScheduleTaskRefresh(true, scheduleEvent);

                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleEvent.Status = oldStatus;
                            scheduleRepository.UpdateScheduleTask(scheduleEvent);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            return viewData;
        }

        public bool Remove(Guid patientId, Guid Id)
        {
            return noteRepository.RemoveFaceToFaceEncounter(Current.AgencyId, patientId, Id);
        }

        public FaceToFaceEncounter GetFaceToFacePrint()
        {
            var order = new FaceToFaceEncounter { Location = this.agencyRepository.GetMainLocation(Current.AgencyId) };
            return order;
        }

        public FaceToFaceEncounter GetFaceToFaceEncounterOnly(Guid patientId, Guid orderId)
        {
            return noteRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
        }

        public FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId)
        {
            var order = new FaceToFaceEncounter();
            var profileLean = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
            if (profileLean != null)
            {
                order = noteRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
                if (order != null)
                {
                    order.Patient = profileLean;
                    if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.Physician = physician;
                        }
                        else
                        {
                            order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                    }
                    var episode = episodeRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
                    if (episode != null)
                    {
                        order.EpisodeEndDate = episode.EndDateFormatted;
                        order.EpisodeStartDate = episode.StartDateFormatted;
                        if (order.Patient != null)
                        {
                            order.Patient.StartofCareDate = episode.StartOfCareDate;
                        }
                    }
                }
                else
                {
                    order = new FaceToFaceEncounter();
                }
                order.Location = agencyRepository.FindLocation(Current.AgencyId, profileLean.AgencyLocationId);
            }
            // order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public List<FaceToFaceEncounter> GetFaceToFaceEncounterFromTasks(Guid patientId, List<ScheduleEvent> scheduleEvents)
        {
            var faceToFaceEncounters = new List<FaceToFaceEncounter>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var physicianId = Guid.Empty;
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    physicianId = physician.Id;
                }

                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var faceToFaceEncounter = new FaceToFaceEncounter
                    {
                        Id = scheduleEvent.Id,
                        AgencyId = Current.AgencyId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        PatientId = scheduleEvent.PatientId,
                        UserId = scheduleEvent.UserId,
                        RequestDate = scheduleEvent.EventDate,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        Status = scheduleEvent.Status,
                        OrderNumber = patientRepository.GetNextOrderNumber(),
                        PhysicianId = physicianId
                    };
                    faceToFaceEncounters.Add(faceToFaceEncounter);
                });
            }
            return faceToFaceEncounters;
        }
       
        public Order GetFaceToFaceEncounterForHistoryEdit(Guid orderId)
        {
            var order = new Order();
            var faceToFaceEncounter = noteRepository.GetFaceToFaceEncounter(orderId, Current.AgencyId);
            if (faceToFaceEncounter != null)
            {
                order = new Order
                {
                    Id = faceToFaceEncounter.Id,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Type = OrderType.FaceToFaceEncounter,
                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                    Number = faceToFaceEncounter.OrderNumber,
                    PatientName = faceToFaceEncounter.DisplayName,
                    ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
                    SendDate = faceToFaceEncounter.RequestDate,
                    PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
                };
            }
            return order;
        }

        public Order GetFaceToFaceEncounterForReceiving(Guid patientId, Guid orderId)
        {
            var order = new Order();
            var faceToFaceEncounter = noteRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
            if (faceToFaceEncounter != null)
            {
                order = new Order
                {
                    Id = faceToFaceEncounter.Id,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Type = OrderType.FaceToFaceEncounter,
                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                    ReceivedDate = faceToFaceEncounter.ReceivedDate,
                    SendDate = faceToFaceEncounter.RequestDate,
                    PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
                };
            }
            return order;
        }

        public List<Order> GetFaceToFaceEncounterOrders(Guid patientId, List<ScheduleEvent> faceToFaceEncounterSchedules)
        {
            var orders = new List<Order>();
            var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var faceToFaceEncounters = noteRepository.GetPatientFaceToFaceEncounterOrders(Current.AgencyId, patientId, faceToFaceEncounterOrdersIds);
            if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            {
                faceToFaceEncounters.ForEach(ffe =>
                {
                    var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.Id == ffe.Id && s.EpisodeId == ffe.EpisodeId);
                    if (evnt != null)
                    {
                        AgencyPhysician physician = null;
                        if (!ffe.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                        }
                        else
                        {
                            if (ffe.PhysicianData.IsNotNullOrEmpty())
                            {
                                var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                }
                            }
                        }
                        orders.Add(new Order
                        {
                            Id = ffe.Id,
                            PatientId = ffe.PatientId,
                            EpisodeId = ffe.EpisodeId,
                            Type = OrderType.FaceToFaceEncounter,
                            Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                            Number = ffe.OrderNumber,
                            PhysicianAccess = physician != null && physician.PhysicianAccess,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            CreatedDate = evnt.EventDate,
                            DocumentType = DocumentType.FaceToFaceEncounter.ToString().ToLower(),
                            ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate : DateTime.MinValue,
                            SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician) || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? (ffe.SentDate.IsValid() ? ffe.SentDate : ffe.RequestDate) : DateTime.MinValue,
                            Status = ffe.Status,
                            CanUserDelete = false,
                            Service = AgencyServices.HomeHealth
                        });
                    }
                });
            }
            return orders;
        }

        public List<Order> GetFaceToFaceEncounterOrders(List<ScheduleEvent> faceToFaceEncounterSchedules, bool isPending, bool isCompleted)
        {
            var orders = new List<Order>();
            var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var faceToFaceEncounters = noteRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
            if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            {
                var canPrint = Current.Permissions.IsInPermission(Service, ParentPermission.Orders, PermissionActions.Print);
                faceToFaceEncounters.ForEach(ffe =>
                {
                    var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.Id == ffe.Id && s.EpisodeId == ffe.EpisodeId);
                    if (evnt != null)
                    {
                        AgencyPhysician physician = null;
                        if (!ffe.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                        }
                        else
                        {
                            if (ffe.PhysicianData.IsNotNullOrEmpty())
                            {
                                var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                }
                            }
                        }
                        var order = new Order
                        {
                            Id = ffe.Id,
                            PatientId = ffe.PatientId,
                            EpisodeId = ffe.EpisodeId,
                            Type = OrderType.FaceToFaceEncounter,
                            Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                            Number = ffe.OrderNumber,
                            PatientName = ffe.DisplayName,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null && physician.PhysicianAccess,
                            CreatedDate = ffe.RequestDate,
                            Service = this.Service,
                            DocumentType = DocumentType.FaceToFaceEncounter.ToString().ToLower(),
                            CanUserPrint = canPrint
                        };
                        if (isCompleted)
                        {
                            //TODO: make sure it is senddate
                            order.SendDate = ffe.SentDate;//ffe.RequestDate
                            order.ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.SentDate;
                            order.PhysicianSignatureDate = ffe.SignatureDate;
                        }
                        else if (isPending)
                        {
                            order.SendDate = ffe.SentDate;
                            order.ReceivedDate = DateTime.Today;
                            order.PhysicianSignatureDate = ffe.SignatureDate;
                        }
                        orders.Add(order);
                    }
                });
            }
            return orders;
        }

        #endregion

        public JsonViewData ValidateFaceToFaceEncounter(ScheduleEvent scheduleEvent, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            var validationRules = new List<Validation>();
            if (scheduleEvent.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter)
            {
                validationRules.Add(new Validation(() => scheduleEvent.EventDate.Date <= DateTime.MinValue.Date, "Schedule date is required."));
                validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValid(), "Schedule date is not valid."));
                validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValid() || !(scheduleEvent.EventDate.Date >= startDate && scheduleEvent.EventDate.Date <= endDate), "Schedule date is not in the episode range."));
                validationRules.Add(new Validation(() => scheduleEvent.VisitDate.Date <= DateTime.MinValue.Date, "Visit date is required."));
                validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValid(), "Visit date is not valid."));
                validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValid() || !(scheduleEvent.VisitDate.Date >= startDate && scheduleEvent.VisitDate.Date <= endDate), "Visit date is not in the episode range."));
            }
            var entityValidator = new EntityValidator(validationRules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return viewData;
        }

    }
}
