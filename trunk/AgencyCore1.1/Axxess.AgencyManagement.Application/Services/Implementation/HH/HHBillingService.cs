﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Log.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.LookUp.Domain;
    using System.Web.Script.Serialization;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Helpers;

    public class HHBillingService : BillingServiceAbstract<ScheduleEvent>
    {

        #region Constructor
        private readonly HHEpisodeRepository episodeRepository;
        private readonly HHBillingRepository billingRepository;
        private readonly HHTaskRepository scheduleRepository;
        private readonly HHPatientProfileRepository patientProfileRepository;
        private readonly HHMongoRepository mongoRepository;

        public HHBillingService(HHDataProvider dataProvider, ILookUpDataProvider lookUpDataProvider)
            : base(dataProvider.TaskRepository, dataProvider.BillingRepository, dataProvider.PatientProfileRepository, dataProvider.MongoRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            base.patientRepository = dataProvider.PatientRepository;
            this.patientProfileRepository = dataProvider.PatientProfileRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.lookUpRepository = lookUpDataProvider.LookUpRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.mongoRepository = dataProvider.MongoRepository;
            this.Service = AgencyServices.HomeHealth;
        }

        #endregion

        public bool AddRap(Rap rap)
        {
            var result = false;
            if (billingRepository.AddRap(rap))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AddFinal(Final final)
        {
            var result = false;
            if (billingRepository.AddFinal(final))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AddRap(Patient patientWithProfile, PatientEpisode episode, int insuranceId, AgencyPhysician physician)
        {
            bool result = false;
            if (billingRepository.AddRap(patientWithProfile, episode, insuranceId, physician))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientWithProfile.Id, episode.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public BillingJsonViewData CreateClaim(Guid patientId, Guid episodeId, int insuranceId, ClaimTypeSubCategory type)
        {
            var rules = new List<Validation>();
            string claimTypeName = type.GetDescription();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The " + claimTypeName + " could not be created." };
            viewData.Service = this.Service.ToString();
            viewData.Category = type;
            viewData.PatientId = patientId;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && insuranceId > 0)
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient dosn't exist."));
                rules.Add(new Validation(() => !episodeRepository.IsEpisodeExist(Current.AgencyId, episodeId), "Episode dosn't exist for this claim."));
                rules.Add(new Validation(() => billingRepository.IsEpisodeHasClaim(Current.AgencyId, patientId, episodeId, (int)type), "This Episode already has a " + claimTypeName + "."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (this.AddClaim(patientId, episodeId, (int)type, insuranceId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The " + claimTypeName + " has been created successfully.";
                        viewData.IsMedicareHistoryRefresh = true;
                        if (type == ClaimTypeSubCategory.RAP)
                        {
                            viewData.IsRAPRefresh = true;
                        }
                        else
                        {
                            viewData.IsFinalRefresh = true;
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Please verify the information provided.";
            }
            return viewData;
        }

        public bool AddClaim(Guid patientId, Guid episodeId, int type, int insuranceId)
        {
            bool result = false;

            var patientWithProfile = patientProfileRepository.GetPatientWithProfileForClaim(Current.AgencyId, patientId);
            if (patientWithProfile != null)
            {
                patientWithProfile.AgencyId = Current.AgencyId;
                var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientWithProfile.Id);
                    if (type == (int)ClaimTypeSubCategory.RAP)
                    {
                        result = this.AddRap(patientWithProfile, episode, insuranceId, physician);
                    }
                    else if (type == (int)ClaimTypeSubCategory.Final)
                    {
                        result = this.AddFinal(patientWithProfile, episode, insuranceId, physician);
                    }
                }
            }
            return result;
        }

        public BillingJsonViewData RemoveMedicareClaimAfterValidate(Guid patientId, Guid Id, ClaimTypeSubCategory type)
        {
            string claimTypeName = type.GetDescription();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The " + claimTypeName + " could not be deleted. Please try again." };
            viewData.Service = this.Service.ToString();
            var rules = new List<Validation>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                rules.Add(new Validation(() => !this.IsEpisodeHasClaim(Id, patientId, (int)type), "This " + claimTypeName + " does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (this.RemoveMedicareClaim(patientId, Id, (int)type))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The " + claimTypeName + " has been deleted successfully.";
                        viewData.IsMedicareHistoryRefresh = true;
                        if (type == ClaimTypeSubCategory.RAP)
                        {
                            viewData.IsRAPRefresh = true;
                            viewData.Category = ClaimTypeSubCategory.RAP;
                        }
                        else
                        {
                            viewData.IsFinalRefresh = true;
                            viewData.Category = ClaimTypeSubCategory.Final;
                        }
                        viewData.PatientId = patientId;
                    }
                    else
                    {
                        viewData.errorMessage = "A problem occured while deleting the " + claimTypeName + ". Please try again.";
                    }
                }
                else
                {
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return viewData;

        }

        public bool RemoveMedicareClaim(Guid patientId, Guid Id, int type)
        {
            bool result = false;
            if (billingRepository.RemoveMedicareClaim(Current.AgencyId, patientId, Id, type))
            {
                result = true;
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), type == (int)ClaimTypeSubCategory.RAP ? LogType.Rap : LogType.Final, type == (int)ClaimTypeSubCategory.RAP ? LogAction.RAPDeleted : LogAction.FinalDeleted, string.Empty);
            }
            return result;
        }

        public Rap GetRap(Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            var rap = billingRepository.GetRapOnly(Current.AgencyId, patientId, episodeId);
            if (rap != null)
            {
                var patientWithProfile = patientProfileRepository.GetPatientWithProfileForClaim(Current.AgencyId, patientId);
                if (patientWithProfile != null && patientWithProfile.Profile != null)
                {
                    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        var isTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(rap.PrimaryInsuranceId);
                        AgencyInsurance insurance = null;
                        if (rap.ConditionCodes.IsNotNullOrEmpty())
                        {
                            rap.ConditionCodesObject = rap.ConditionCodes.ToObject<ConditionCodes>();
                        }
                        if (rap.DiagnosisCode.IsNotNullOrEmpty())
                        {
                            rap.DiagnosisCodesObject = rap.DiagnosisCode.ToObject<DiagnosisCodes>();
                        }
                        if (isTraditionalMedicare)
                        {
                            rap.IsMedicareHMO = false;
                        }
                        else if (rap.PrimaryInsuranceId >= 1000)
                        {
                            insurance = agencyRepository.GetInsurance(rap.PrimaryInsuranceId, Current.AgencyId);
                            if (insurance != null)
                            {
                                rap.IsMedicareHMO = (insurance.PayorType == (int)PayerTypes.MedicareHMO);
                            }
                        }
                        if (rap.CBSA.IsNullOrEmpty())
                        {
                            rap.CBSA = lookUpRepository.CbsaCodeByZip(rap.AddressZipCode);
                        }
                        if ((rap.Status == (int)BillingStatus.ClaimCreated))
                        {
                            if (!rap.IsVerified)
                            {
                                rap.FirstName = patientWithProfile.FirstName;
                                rap.LastName = patientWithProfile.LastName;
                                rap.MedicareNumber = patientWithProfile.MedicareNumber;
                                rap.PatientIdNumber = patientWithProfile.PatientIdNumber;
                                rap.Gender = patientWithProfile.Gender;
                                rap.DOB = patientWithProfile.DOB;
                                rap.AddressLine1 = patientWithProfile.AddressLine1;
                                rap.AddressLine2 = patientWithProfile.AddressLine2;
                                rap.AddressCity = patientWithProfile.AddressCity;
                                rap.AddressStateCode = patientWithProfile.AddressStateCode;
                                rap.AddressZipCode = patientWithProfile.AddressZipCode;
                                rap.AdmissionSource = patientWithProfile.Profile.AdmissionSource;
                                rap.PatientStatus = patientWithProfile.Profile.Status;
                                rap.UB4PatientStatus = ((int)UB4PatientStatus.StillPatient).ToString();

                                SetClaimWithAdmissionInfo(rap, episode.AdmissionId);

                                rap.EpisodeStartDate = episode.StartDate;
                                rap.EpisodeEndDate = episode.EndDate;

                                SetClaimPhysicianInfo(rap);

                                var evnt = scheduleRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId, episode.StartDate, episode.EndDate);
                                if (evnt != null && evnt.VisitDate.IsValid())
                                {
                                    rap.FirstBillableVisitDateFormat = evnt.VisitDate.ToString("MM/dd/yyyy");
                                    rap.FirstBillableVisitDate = evnt.VisitDate;
                                }
                                else
                                {
                                    rap.FirstBillableVisitDateFormat = string.Empty;
                                }

                                if (isTraditionalMedicare || rap.AddressZipCode.IsNullOrEmpty())
                                {
                                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                    if (agencyLocation != null)
                                    {
                                        rap.LocationZipCode = agencyLocation.AddressZipCode;
                                        if (isTraditionalMedicare)
                                        {
                                            rap.Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
                                        }
                                    }
                                }

                                if (rap.PrimaryInsuranceId >= 1000)
                                {
                                    insurance = insurance ?? agencyRepository.GetInsurance(rap.PrimaryInsuranceId, Current.AgencyId);
                                    if (insurance != null)
                                    {
                                        rap.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                    }

                                    SetClaimAuthorization(rap, episode.StartDate, episode.EndDate);
                                    SetInsurancePlanInfo(rap, patientWithProfile.Profile);
                                }
                                SetClaimWithAssessmentInfo(rap);
                            }
                            else
                            {
                                rap.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                                rap.Authorizations = GetClaimAutorizationList(rap.PatientId, rap.Authorization, rap.PrimaryInsuranceId, episode.StartDate, episode.EndDate, AuthorizationStatusTypes.Active, AgencyServices.HomeHealth);
                            }
                        }
                        else
                        {
                            rap.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                            rap.Authorizations = GetClaimAutorizationList(rap.PatientId, rap.Authorization, rap.PrimaryInsuranceId, episode.StartDate, episode.EndDate, AuthorizationStatusTypes.Active, AgencyServices.HomeHealth);
                        }
                        if (rap.IsMedicareHMO && rap.AuthorizationNumber.IsNullOrEmpty())
                        {
                            rap.AuthorizationNumber = rap.ClaimKey;
                        }
                        rap.EpisodeEndDate = episode.EndDate;
                        rap.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;
                    }
                }
                ClaimWithInsurance(rap);
            }
            return rap;
        }

        public bool VerifyRap(Rap claim)
        {
            var result = false;
            var claimToEdit = billingRepository.GetRapOnly(Current.AgencyId, claim.Id);
            if (claimToEdit != null)
            {
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                //if (profile != null)
                //{
                //var insuranceXml = SerializedInsuranceForClaim<Rap>(claim, profile.AgencyLocationId, claim.PrimaryInsuranceId, cl => true);
                //if (insuranceXml.IsNotNullOrEmpty())
                //{
                //    claim.Insurance = insuranceXml;
                //}
                if (UpdateOrAddPayorForClaimAppSpecific<RapClaimInsurance>(claim))
                {

                    claimToEdit.FirstName = claim.FirstName;
                    claimToEdit.LastName = claim.LastName;
                    claimToEdit.MedicareNumber = claim.MedicareNumber;
                    if (claim.FirstBillableVisitDateFormat.IsNotNullOrEmpty() && claim.FirstBillableVisitDateFormat.IsValidDate())
                    {
                        claimToEdit.FirstBillableVisitDate = claim.FirstBillableVisitDateFormat.ToDateTime();
                    }
                    claimToEdit.EpisodeStartDate = claim.EpisodeStartDate;
                    claimToEdit.StartofCareDate = claim.StartofCareDate;
                    claimToEdit.PatientIdNumber = claim.PatientIdNumber;
                    claimToEdit.Gender = claim.Gender;
                    claimToEdit.DOB = claim.DOB;
                    claimToEdit.AddressCity = claim.AddressLine1;
                    claimToEdit.AddressLine2 = claim.AddressLine2;
                    claimToEdit.AddressCity = claim.AddressCity;
                    claimToEdit.AddressStateCode = claim.AddressStateCode;
                    claimToEdit.AddressZipCode = claim.AddressZipCode;
                    claimToEdit.PhysicianLastName = claim.PhysicianLastName;
                    claimToEdit.PhysicianNPI = claim.PhysicianNPI;
                    claimToEdit.PhysicianFirstName = claim.PhysicianFirstName;
                    claimToEdit.DiagnosisCode = claim.DiagnosisCodesObject.ToXml();//string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>", claim.Primary, claim.Second, claim.Third, claim.Fourth, claim.Fifth, claim.Sixth);
                    claimToEdit.ConditionCodes = claim.ConditionCodesObject.ToXml();//string.Format("<ConditionCodes><ConditionCode18>{0}</ConditionCode18><ConditionCode19>{1}</ConditionCode19><ConditionCode20>{2}</ConditionCode20><ConditionCode21>{3}</ConditionCode21><ConditionCode22>{4}</ConditionCode22><ConditionCode23>{5}</ConditionCode23><ConditionCode24>{6}</ConditionCode24><ConditionCode25>{7}</ConditionCode25><ConditionCode26>{8}</ConditionCode26><ConditionCode27>{9}</ConditionCode27><ConditionCode28>{10}</ConditionCode28></ConditionCodes>", claim.ConditionCode18, claim.ConditionCode19, claim.ConditionCode20, claim.ConditionCode21, claim.ConditionCode22, claim.ConditionCode23, claim.ConditionCode24, claim.ConditionCode25, claim.ConditionCode26, claim.ConditionCode27, claim.ConditionCode28);
                    claimToEdit.HippsCode = claim.HippsCode;
                    claimToEdit.ClaimKey = claim.ClaimKey;
                    claimToEdit.Remark = claim.Remark;
                    claimToEdit.ProspectivePay = claim.ProspectivePay;
                    claimToEdit.AssessmentType = claim.AssessmentType;
                    claimToEdit.AdmissionSource = claim.AdmissionSource;
                    claimToEdit.PatientStatus = claim.PatientStatus;
                    claimToEdit.UB4PatientStatus = claim.UB4PatientStatus;
                    if (claim.UB4PatientStatus != ((int)UB4PatientStatus.StillPatient).ToString())
                    {
                        claimToEdit.DischargeDate = claim.DischargeDate;
                    }
                    claimToEdit.PrimaryInsuranceId = claim.PrimaryInsuranceId;
                    if (claim.PrimaryInsuranceId >= 1000)
                    {
                        claimToEdit.HealthPlanId = claim.HealthPlanId;
                        claimToEdit.GroupName = claim.GroupName;
                        claimToEdit.GroupId = claim.GroupId;
                        claimToEdit.Authorization = claim.Authorization;
                        claimToEdit.AuthorizationNumber = claim.AuthorizationNumber;
                        claimToEdit.AuthorizationNumber2 = claim.AuthorizationNumber2;
                        claimToEdit.AuthorizationNumber3 = claim.AuthorizationNumber3;
                    }
                    claimToEdit.Ub04Locator81cca = claim.Ub04Locator81cca;
                    claimToEdit.Ub04Locator39 = claim.Ub04Locator39;
                    //claimToEdit.Insurance = claim.Insurance;
                    claimToEdit.IsVerified = true;
                    claimToEdit.Modified = DateTime.Now;
                    if (billingRepository.UpdateRapModel(claimToEdit))
                    {
                        result = true;
                        Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Rap, LogAction.RAPVerified, string.Empty);

                    }
                }
            }
            //}
            return result;
        }

        #region Final Claim Steps | Get

        public Final GetFinalInfoLatest(Guid patientId, Guid episodeId)
        {
            var final = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
            if (final != null)
            {
                var patientWithProfile = patientProfileRepository.GetPatientWithProfileForClaim(Current.AgencyId, patientId);
                if (patientWithProfile != null && patientWithProfile.Profile != null)
                {
                    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        if (final.CBSA.IsNullOrEmpty() && final.AddressZipCode.IsNotNullOrEmpty())
                        {
                            final.CBSA = lookUpRepository.CbsaCodeByZip(final.AddressZipCode);
                        }
                        if ((final.Status == (int)BillingStatus.ClaimCreated))
                        {
                            if (!final.IsFinalInfoVerified)
                            {
                                var whereDict = new Dictionary<string, string> { { "IsMainOffice", "1" }, { "IsDeprecated", "0" } };
                                var agencyLocationDictionary = agencyRepository.GetLocation(Current.AgencyId, whereDict, new string[] { "AddressZipCode", "Ub04Locator81cca" });

                                var rap = billingRepository.GetRapOnly(Current.AgencyId, patientId, episodeId);
                                if (rap != null && (rap.IsVerified))
                                {
                                    final.IsRAPExist = true;
                                    final.FirstName = rap.FirstName;
                                    final.LastName = rap.LastName;
                                    final.MedicareNumber = rap.MedicareNumber;
                                    final.PatientIdNumber = rap.PatientIdNumber;
                                    final.DiagnosisCode = rap.DiagnosisCode;
                                    final.ConditionCodes = rap.ConditionCodes;
                                    final.Gender = rap.Gender;
                                    final.DOB = rap.DOB;
                                    final.EpisodeStartDate = rap.EpisodeStartDate;
                                    final.StartofCareDate = rap.StartofCareDate;
                                    final.AddressLine1 = rap.AddressLine1;
                                    final.AddressLine2 = rap.AddressLine2;
                                    final.AddressCity = rap.AddressCity;
                                    final.AddressStateCode = rap.AddressStateCode;
                                    final.AddressZipCode = rap.AddressZipCode;
                                    final.HippsCode = rap.HippsCode;
                                    final.ClaimKey = rap.ClaimKey;
                                    final.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                                    final.PhysicianLastName = rap.PhysicianLastName;
                                    final.PhysicianFirstName = rap.PhysicianFirstName;
                                    final.PhysicianNPI = rap.PhysicianNPI;
                                    final.AdmissionSource = rap.AdmissionSource;
                                    final.AssessmentType = rap.AssessmentType;
                                    final.UB4PatientStatus = patientWithProfile.Profile.Status == (int)PatientStatus.Active ?  ((int)UB4PatientStatus.StillPatient).ToString() : (patientWithProfile.Profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty); //rap.UB4PatientStatus;

                                 
                                    SetClaimWithAdmissionInfo(final, episode.AdmissionId);

                                    final.StartofCareDate = rap.StartofCareDate;

                                    if (final.AssessmentType.IsNotNullOrEmpty())
                                    {
                                        final.ProspectivePay = this.ProspectivePayAmount(final.AssessmentType, final.HippsCode, final.EpisodeStartDate, final.AddressZipCode, agencyLocationDictionary.Get("AddressZipCode"));
                                    }
                                }
                                else if (rap == null)
                                {
                                    final.IsRAPExist = false;
                                    final.FirstName = patientWithProfile.FirstName;
                                    final.LastName = patientWithProfile.LastName;
                                    final.MedicareNumber = patientWithProfile.MedicareNumber;
                                    final.PatientIdNumber = patientWithProfile.PatientIdNumber;
                                    final.Gender = patientWithProfile.Gender;
                                    final.DOB = patientWithProfile.DOB;
                                    final.AddressLine2 = patientWithProfile.AddressLine2;
                                    final.AddressCity = patientWithProfile.AddressCity;
                                    final.AddressStateCode = patientWithProfile.AddressStateCode;
                                    final.AddressZipCode = patientWithProfile.AddressZipCode;
                                    final.UB4PatientStatus = patientWithProfile.Profile.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (patientWithProfile.Profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty);
                                      
                                    SetClaimWithAdmissionInfo(final, episode.AdmissionId);

                                    final.EpisodeStartDate = episode.StartDate;
                                    final.EpisodeEndDate = episode.EndDate;

                                    SetClaimPhysicianInfo(final);

                                    var evnt = scheduleRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId, episode.StartDate, episode.EndDate);
                                    if (evnt != null && evnt.VisitDate.IsValid())
                                    {
                                        final.FirstBillableVisitDateFormat = evnt.VisitDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        final.FirstBillableVisitDateFormat = string.Empty;
                                    }
                                    final.LocationZipCode = agencyLocationDictionary.Get("AddressZipCode");
                                }
                                SetClaimWithAssessmentInfo(final);
                            }
                            else
                            {
                                final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                            }
                        }
                        else
                        {
                            final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                        }
                        if (final.IsMedicareHMO && final.AuthorizationNumber.IsNullOrEmpty())
                        {
                            final.AuthorizationNumber = final.ClaimKey;
                        }
                        final.IsPatientDischarged = patientWithProfile.Profile.IsDischarged;
                        final.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;
                        if (final.ConditionCodes.IsNotNullOrEmpty())
                        {
                            final.ConditionCodesObject = final.ConditionCodes.ToObject<ConditionCodes>();
                        }
                        if (final.DiagnosisCode.IsNotNullOrEmpty())
                        {
                            final.DiagnosisCodesObject = final.DiagnosisCode.ToObject<DiagnosisCodes>();
                        }
                    }
                }

            }
            return final;
        }

        public Final GetFinalWithInsurance(Guid patientId, Guid Id)
        {
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                var isSetLocatorFromLocationOrInsurance = false;
                var isTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId);

                if (claim.Status == (int)BillingStatus.ClaimCreated)
                {
                    if (!claim.IsInsuranceVerified)
                    {
                        var rap = billingRepository.GetRapOnly(Current.AgencyId, claim.PatientId, claim.EpisodeId);
                        if (rap != null && rap.IsVerified && rap.PrimaryInsuranceId == claim.PrimaryInsuranceId)
                        {
                            claim.HealthPlanId = rap.HealthPlanId;
                            claim.Authorization = rap.Authorization;
                            claim.AuthorizationNumber = rap.AuthorizationNumber;
                            claim.AuthorizationNumber2 = rap.AuthorizationNumber2;
                            claim.AuthorizationNumber3 = rap.AuthorizationNumber3;
                            claim.Ub04Locator39 = rap.Ub04Locator39;
                            claim.Ub04Locator81cca = rap.Ub04Locator81cca;
                            claim.Authorizations = GetClaimAutorizationList(claim.PatientId, rap.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, AgencyServices.HomeHealth);
                        }
                        else
                        {
                            var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                            if (profile != null)
                            {
                                claim.AgencyLocationId = profile.AgencyLocationId;
                                if (!isTraditionalMedicare)
                                {
                                    SetInsurancePlanInfo(claim, profile);
                                    if (claim.PrimaryInsuranceId >= 1000)
                                    {
                                        SetClaimAuthorization(claim, claim.EpisodeStartDate, claim.EpisodeEndDate);
                                    }

                                }
                            }
                            isSetLocatorFromLocationOrInsurance = true;
                        }
                    }
                    else
                    {
                        if (!isTraditionalMedicare && claim.PrimaryInsuranceId >= 1000)
                        {
                            claim.Authorizations = GetClaimAutorizationList(claim.PatientId, claim.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, this.Service);
                        }
                    }
                }
                else
                {
                    if (!isTraditionalMedicare && claim.PrimaryInsuranceId >= 1000)
                    {
                        claim.Authorizations = GetClaimAutorizationList(claim.PatientId, claim.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, this.Service);
                    }
                }
                ClaimWithInsuranceAppSpecific<FinalClaimInsurance>(claim, isSetLocatorFromLocationOrInsurance, claim.AgencyLocationId);
                // ClaimWithInsurance(claim);
                //claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
                //claim.AgencyInsurance = ClaimToInsuranceNoLocationId<ManagedBill>(patientId, claim.PrimaryInsuranceId, claim.Insurance, false);
            }
            return claim;
        }

        public Final GetFinalVisit(Guid patientId, Guid episodeId)
        {
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
            if (claim != null)
            {
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                //if (profile != null)
                //{
                var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    claim.EpisodeStartDate = episode.StartDate;
                    claim.EpisodeEndDate = episode.EndDate;
                    //if (episode.Schedule.IsNotNullOrEmpty())
                    //{
                    var notVerifiedVisits = scheduleRepository.GetScheduledEventsOnly(Current.AgencyId, episode.PatientId, episode.Id, claim.PrimaryInsuranceId, episode.StartDate, episode.EndDate);
                    if (notVerifiedVisits.IsNotNullOrEmpty())
                    {
                        // claim.AgencyLocationId = profile.AgencyLocationId;
                        // var agencyInsurance = new AgencyInsurance();
                        var chargeRates = ClaimInsuranceRatesAppSpecific<FinalClaimInsurance>(claim.Id, claim.PayorType);// var chargeRates = FinalToCharegRates(claim, out agencyInsurance, false);
                        claim.BillVisitDatas = BillableVisitsData(notVerifiedVisits, MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId) ? ClaimType.CMS : ClaimType.HMO, chargeRates, false);
                    }
                    //}
                }
                //}
            }
            return claim;
        }

        public ClaimSupplyViewData GetFinalSupply(Guid patientId, Guid episodeId)
        {
            var viewData = new ClaimSupplyViewData();
            viewData.Service = this.Service;
            viewData.Type = ClaimTypeSubCategory.Final;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
                if (claim != null)
                {
                    var supplies = claim.Supply.ToObject<List<Supply>>();
                    viewData.Id = claim.Id;
                    viewData.EpisodeId = episodeId;
                    viewData.PatientId = claim.PatientId;
                    viewData.EpisodeStartDate = claim.EpisodeStartDate;
                    viewData.EpisodeEndDate = claim.EpisodeEndDate;
                    viewData.IsSupplyNotBillable = claim.IsSupplyNotBillable;
                    int id = 0;
                    supplies.ForEach(s =>
                    {
                        s.BillingId = id;
                        id++;
                    });
                    claim.Supply = supplies.ToXml();
                    billingRepository.UpdateFinalModel(claim);

                    viewData.BilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                    viewData.UnbilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => !s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                }
            }
            return viewData;
        }

        public Final GetFinalSummary(Guid patientId, Guid episodeId)
        {
            var claim = GetFinalPrintOrSummary(patientId, episodeId);
            if (claim != null)
            {
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid() && s.EventDate.IsValid()).OrderBy(s => s.VisitDate.Date).ThenBy(s => s.EventDate.Date).ToList();
                    if (visits.IsNotNullOrEmpty())
                    {
                        if (claim.IsVisitVerified)
                        {
                            var chargeRates = ClaimInsuranceRatesAppSpecific<FinalClaimInsurance>(claim.Id, claim.PayorType);// var agencyInsurance = new AgencyInsurance();
                            claim.BillVisitSummaryDatas = BillableVisitSummary(visits, MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId) ? ClaimType.CMS : ClaimType.HMO, chargeRates, false);
                        }
                    }
                    //if (claim.Insurance.IsNotNullOrEmpty())
                    //{
                    //    claim.AgencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    //}
                }
                claim.SupplyTotal = MedicareSupplyTotal(claim);
            }
            return claim;
        }


        #endregion

        #region Final Claim Steps | Verify

        public bool VerifyInfo(Final claim)
        {
            var result = false;
            var currentClaim = billingRepository.GetFinalOnly(Current.AgencyId, claim.Id);
            if (currentClaim != null)
            {
                if (VerifyInfoAppSpecific<FinalClaimInsurance>(currentClaim, claim))
                {
                    currentClaim.FirstName = claim.FirstName;
                    currentClaim.LastName = claim.LastName;
                    currentClaim.PatientIdNumber = claim.PatientIdNumber;
                    currentClaim.Gender = claim.Gender;
                    currentClaim.DOB = claim.DOB;

                    currentClaim.AddressLine1 = claim.AddressLine1;
                    currentClaim.AddressLine2 = claim.AddressLine2;
                    currentClaim.AddressCity = claim.AddressCity;
                    currentClaim.AddressStateCode = claim.AddressStateCode;
                    currentClaim.AddressZipCode = claim.AddressZipCode;

                    currentClaim.PhysicianLastName = claim.PhysicianLastName;
                    currentClaim.PhysicianFirstName = claim.PhysicianFirstName;
                    currentClaim.PhysicianNPI = claim.PhysicianNPI;

                    if (claim.PrimaryInsuranceId != currentClaim.PrimaryInsuranceId)
                    {
                        currentClaim.IsInsuranceVerified = false;
                        currentClaim.IsVisitVerified = false;
                        currentClaim.IsSupplyVerified = false;
                    }
                    currentClaim.MedicareNumber = claim.MedicareNumber;
                    currentClaim.PrimaryInsuranceId = claim.PrimaryInsuranceId;

                    currentClaim.AdmissionSource = claim.AdmissionSource;
                    currentClaim.StartofCareDate = claim.StartofCareDate;
                    currentClaim.UB4PatientStatus = claim.UB4PatientStatus;
                    if (claim.UB4PatientStatus != ((int)UB4PatientStatus.StillPatient).ToString())
                    {
                        currentClaim.DischargeDate = claim.DischargeDate;
                    }
                    currentClaim.EpisodeStartDate = claim.EpisodeStartDate;
                    if (claim.FirstBillableVisitDateFormat.IsNotNullOrEmpty() && claim.FirstBillableVisitDateFormat.IsValidDate())
                    {
                        currentClaim.FirstBillableVisitDate = claim.FirstBillableVisitDateFormat.ToDateTime();
                    }

                    currentClaim.AssessmentType = claim.AssessmentType;
                    currentClaim.HippsCode = claim.HippsCode;
                    currentClaim.ClaimKey = claim.ClaimKey;
                    currentClaim.ProspectivePay = claim.ProspectivePay;
                    currentClaim.AreOrdersComplete = claim.AreOrdersComplete;


                    currentClaim.DiagnosisCode = claim.DiagnosisCodesObject.ToXml();
                    currentClaim.ConditionCodes = claim.ConditionCodesObject.ToXml();
                    currentClaim.Remark = claim.Remark;

                    currentClaim.IsFinalInfoVerified = true;
                    if (billingRepository.UpdateFinalModel(currentClaim))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Final, LogAction.FinalDemographicsVerified, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public BillingJsonViewData VerifyInsurance(Final claim)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified.", Service = this.Service.ToString(), Category = ClaimTypeSubCategory.Final };
            var claimToEdit = billingRepository.GetFinalOnly(Current.AgencyId, claim.PatientId, claim.Id);
            if (claimToEdit != null)
            {
                if (VerifyInsuranceAppSpecific(claimToEdit, claim))
                {
                    claimToEdit.IsInsuranceVerified = true;
                    if (billingRepository.UpdateFinalModel(claimToEdit))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, claimToEdit.PatientId, claimToEdit.Id.ToString(), LogType.Final, LogAction.FinalInsuranceVerified, string.Empty);
                        viewData.errorMessage = "Final Claim Insurance is successfully verified.";
                        viewData.isSuccessful = true;
                        viewData.PatientId = claimToEdit.PatientId;
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> visit)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Final is not verified.", Category = ClaimTypeSubCategory.Final };
            viewData.Service = Service.ToString();
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                if (visit.IsNotNullOrEmpty())
                {
                    var claim = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                    if (claim != null)
                    {
                        var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                        if (episode != null)
                        {
                            var scheduleEvents = scheduleRepository.GetScheduledEventsOnly(Current.AgencyId, episode.PatientId, episode.Id,claim.PrimaryInsuranceId, episode.StartDate, episode.EndDate);// var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (scheduleEvents.IsNotNullOrEmpty())
                            {
                                var visitList = new List<ScheduleEvent>();
                                var finalVisit = claim.VerifiedVisits.IsNotNullOrEmpty() ? claim.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                                visit.ForEach(v =>
                                {
                                    var scheduleVisit = scheduleEvents.FirstOrDefault(s => s.Id == v);
                                    if (scheduleVisit != null)
                                    {
                                        scheduleVisit.IsBillable = true;
                                        visitList.Add(scheduleVisit);
                                    }
                                });
                                if (finalVisit.IsNotNullOrEmpty())
                                {
                                    finalVisit.ForEach(f =>
                                    {
                                        var scheduleEvent = scheduleEvents.FirstOrDefault(e => e.Id == f.Id);
                                        if (scheduleEvent != null && !visit.Contains(f.Id))
                                        {
                                            scheduleEvent.IsBillable = false;
                                        }
                                    });
                                }
                                claim.IsVisitVerified = true;
                                claim.VerifiedVisits = visitList.ToXml();
                                claim.Modified = DateTime.Now;
                                if (visitList.IsNotNullOrEmpty())
                                {
                                    var supplyList = GetSupplies(patientId, claim.Supply, visitList, true);
                                    if (supplyList.IsNotNullOrEmpty())
                                    {
                                        claim.Supply = supplyList.ToXml();
                                    }
                                }
                                if (scheduleRepository.UpdateScheduleEventsForIsBillable(Current.AgencyId, patientId, episodeId, visitList))//(patientRepository.UpdateEpisode(Current.AgencyId, episode))
                                {
                                    if (billingRepository.UpdateFinalModel(claim))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalVisitVerified, string.Empty);
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "Final Visit is successfully verified.";
                                        viewData.IsFinalRefresh = true;
                                        viewData.PatientId = patientId;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData VisitSupply(Guid Id, Guid episodeId, Guid patientId, bool IsSupplyNotBillable)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Final supply is not verified.", Category = ClaimTypeSubCategory.Final, Service = this.Service.ToString() };
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                if (claim != null)
                {
                    claim.SupplyTotal = this.MedicareSupplyTotal(claim);
                    claim.Modified = DateTime.Now;
                    claim.IsSupplyVerified = true;

                    if (IsSupplyNotBillable && claim.HippsCode.IsNotNullOrEmpty() && claim.HippsCode.Length == 5)
                    {
                        claim.IsSupplyNotBillable = true;
                        var hippsCode = claim.HippsCode.GetNrsSeverityLevel();
                        if (hippsCode.IsNotNullOrEmpty() && hippsCode.Length == 5)
                        {
                            claim.HippsCode = hippsCode;
                        }
                    }
                    else if (claim.IsSupplyNotBillable && !IsSupplyNotBillable && claim.HippsCode.Length == 5)
                    {
                        claim.IsSupplyNotBillable = false;
                        var hippsCode = claim.HippsCode.GetReverseNrsSeverityLevel();
                        if (hippsCode.IsNotNullOrEmpty() && hippsCode.Length == 5)
                        {
                            claim.HippsCode = hippsCode;
                        }
                    }
                    else
                    {
                        claim.IsSupplyNotBillable = IsSupplyNotBillable;
                    }

                    if (billingRepository.UpdateFinalModel(claim))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final supply is successfully verified.";
                        viewData.PatientId = patientId;
                    }
                }
            }
            return viewData;
        }

        public bool FinalComplete(Guid Id, Guid patientId)
        {
            Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalSummaryVerified, string.Empty);
            return true;
        }

        #endregion

        public Final GetFinal(Guid patientId, Guid episodeId)
        {
            return billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
        }

        public Final GetFinalPrintOrSummary(Guid patientId, Guid episodeId)
        {
            var final = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
            if (final != null)
            {
                var patientWithProfile = patientProfileRepository.GetPatientWithProfileForClaim(Current.AgencyId, patientId);
                if (patientWithProfile != null && patientWithProfile.Profile != null)
                {
                    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        var isTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(final.PrimaryInsuranceId);
                        if (final.CBSA.IsNullOrEmpty() && final.AddressZipCode.IsNotNullOrEmpty())
                        {
                            final.CBSA = lookUpRepository.CbsaCodeByZip(final.AddressZipCode);
                        }
                        if ((final.Status == (int)BillingStatus.ClaimCreated))
                        {
                            if (!final.IsFinalInfoVerified)
                            {
                                var whereDict = new Dictionary<string, string> { { "IsMainOffice", "1" }, { "IsDeprecated", "0" } };
                                var agencyLocationDictionary = agencyRepository.GetLocation(Current.AgencyId, whereDict, new string[] { "AddressZipCode", "Ub04Locator81cca" });

                                var rap = billingRepository.GetRapOnly(Current.AgencyId, patientId, episodeId);
                                if (rap != null && (rap.IsVerified))
                                {
                                    final.IsRAPExist = true;
                                    final.FirstName = rap.FirstName;
                                    final.LastName = rap.LastName;
                                    final.MedicareNumber = rap.MedicareNumber;
                                    final.PatientIdNumber = rap.PatientIdNumber;
                                    final.DiagnosisCode = rap.DiagnosisCode;
                                    final.ConditionCodes = rap.ConditionCodes;
                                    final.Gender = rap.Gender;
                                    final.DOB = rap.DOB;
                                    final.EpisodeStartDate = rap.EpisodeStartDate;
                                    final.StartofCareDate = rap.StartofCareDate;
                                    final.AddressLine1 = rap.AddressLine1;
                                    final.AddressLine2 = rap.AddressLine2;
                                    final.AddressCity = rap.AddressCity;
                                    final.AddressStateCode = rap.AddressStateCode;
                                    final.AddressZipCode = rap.AddressZipCode;
                                    final.HippsCode = rap.HippsCode;
                                    final.ClaimKey = rap.ClaimKey;
                                    final.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                                    final.PhysicianLastName = rap.PhysicianLastName;
                                    final.PhysicianFirstName = rap.PhysicianFirstName;
                                    final.PhysicianNPI = rap.PhysicianNPI;
                                    final.AdmissionSource = rap.AdmissionSource;
                                    final.AssessmentType = rap.AssessmentType;
                                    final.UB4PatientStatus = patientWithProfile.Profile.Status == (int)PatientStatus.Active ?  ((int)UB4PatientStatus.StillPatient).ToString()  : (patientWithProfile.Profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty); //rap.UB4PatientStatus;
                                  
                                    final.HealthPlanId = rap.HealthPlanId;
                                    final.Authorization = rap.Authorization;
                                    final.AuthorizationNumber = rap.AuthorizationNumber;
                                    final.AuthorizationNumber2 = rap.AuthorizationNumber2;
                                    final.AuthorizationNumber3 = rap.AuthorizationNumber3;
                                    final.Ub04Locator39 = rap.Ub04Locator39;

                                    SetClaimWithAdmissionInfo(final, episode.AdmissionId);
                                    final.StartofCareDate = rap.StartofCareDate;
                                    final.EpisodeStartDate = episode.StartDate;
                                    final.EpisodeEndDate = episode.EndDate;

                                    if (final.AssessmentType.IsNotNullOrEmpty())
                                    {
                                        final.ProspectivePay = this.ProspectivePayAmount(final.AssessmentType, final.HippsCode, final.EpisodeStartDate, final.AddressZipCode, agencyLocationDictionary.Get("AddressZipCode"));
                                    }
                                    if (isTraditionalMedicare)
                                    {
                                        final.Ub04Locator81cca = agencyLocationDictionary.Get("Ub04Locator81cca");
                                    }
                                    else if (final.PrimaryInsuranceId >= 1000)
                                    {
                                        var insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                                        if (insurance != null)
                                        {
                                            final.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                        }
                                    }
                                }
                                else if (rap == null)
                                {
                                    final.IsRAPExist = false;
                                    final.FirstName = patientWithProfile.FirstName;
                                    final.LastName = patientWithProfile.LastName;
                                    final.MedicareNumber = patientWithProfile.MedicareNumber;
                                    final.PatientIdNumber = patientWithProfile.PatientIdNumber;
                                    final.Gender = patientWithProfile.Gender;
                                    final.DOB = patientWithProfile.DOB;
                                    final.AddressLine2 = patientWithProfile.AddressLine2;
                                    final.AddressCity = patientWithProfile.AddressCity;
                                    final.AddressStateCode = patientWithProfile.AddressStateCode;
                                    final.AddressZipCode = patientWithProfile.AddressZipCode;
                                    final.UB4PatientStatus = patientWithProfile.Profile.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (patientWithProfile.Profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty);
                                      
                                    SetClaimWithAdmissionInfo(final, episode.AdmissionId);

                                    final.EpisodeStartDate = episode.StartDate;
                                    final.EpisodeEndDate = episode.EndDate;

                                    SetClaimPhysicianInfo(final);


                                    var evnt = scheduleRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId, episode.StartDate, episode.EndDate);
                                    if (evnt != null && evnt.VisitDate.IsValid())
                                    {
                                        final.FirstBillableVisitDateFormat = evnt.VisitDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        final.FirstBillableVisitDateFormat = string.Empty;
                                    }
                                    final.LocationZipCode = agencyLocationDictionary.Get("AddressZipCode");

                                    if (isTraditionalMedicare)
                                    {
                                        final.Ub04Locator81cca = agencyLocationDictionary.Get("Ub04Locator81cca");
                                    }
                                    else if (final.PrimaryInsuranceId >= 1000)
                                    {
                                        var insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                                        if (insurance != null)
                                        {
                                            final.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                        }
                                        var autorizations = patientProfileRepository.GetAuthorizationsByStatus(Current.AgencyId, patientWithProfile.Id, final.PrimaryInsuranceId, (AuthorizationStatusTypes.Active).ToString(), (int)AgencyServices.HomeHealth, final.EpisodeStartDate, final.EpisodeEndDate);
                                        if (autorizations.IsNotNullOrEmpty())
                                        {
                                            var autorization = autorizations.FirstOrDefault();
                                            if (autorization != null)
                                            {
                                                final.AuthorizationNumber = autorization.Number1;
                                                final.AuthorizationNumber2 = autorization.Number2;
                                                final.AuthorizationNumber3 = autorization.Number3;
                                            }
                                        }

                                    }

                                }

                                if (final.PrimaryInsuranceId >= 1000)
                                {
                                    SetInsurancePlanInfo(rap, patientWithProfile.Profile);
                                }
                                SetClaimWithAssessmentInfo(rap);
                            }
                            else
                            {
                                final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                            }
                        }
                        else
                        {
                            final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                        }
                        if (final.IsMedicareHMO && final.AuthorizationNumber.IsNullOrEmpty())
                        {
                            final.AuthorizationNumber = final.ClaimKey;
                        }
                        final.IsPatientDischarged = patientWithProfile.Profile.IsDischarged;
                        //final.BranchId = patientWithProfile.Profile.AgencyLocationId;
                        final.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;
                        if (final.ConditionCodes.IsNotNullOrEmpty())
                        {
                            final.ConditionCodesObject = final.ConditionCodes.ToObject<ConditionCodes>();
                        }
                        if (final.DiagnosisCode.IsNotNullOrEmpty())
                        {
                            final.DiagnosisCodesObject = final.DiagnosisCode.ToObject<DiagnosisCodes>();
                        }
                    }
                }
            }
            return final;
        }

        public bool AddFinal(Patient patientWithProfile, PatientEpisode episode, int insuranceId, AgencyPhysician physician)
        {
            bool result = false;
            if (billingRepository.AddFinal(patientWithProfile, episode, insuranceId, physician))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientWithProfile.Id, episode.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                result = true;
            }
            return result;
        }



        #region Final Claim Charge rates


        public List<ChargeRate> UpdateFinalRatesForReload(Guid ClaimId, Guid patientId)
        {
            var rates = new List<ChargeRate>();
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, ClaimId);
            if (claim != null)
            {
                rates = UpdateRatesForReloadAppSpecific<FinalClaimInsurance>(claim);
            }
            return rates;
        }

        public List<ChargeRate> FinalClaimInsuranceRates(Guid Id)
        {
            return ClaimInsuranceRatesAppSpecific<FinalClaimInsurance>(Id, (int)PayorTypeMainCategory.NonPrivatePayor);
        }

        #endregion

        public BillingJsonViewData UpdateProccesedClaimStatus(Guid patientId, Guid Id, string type, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, string comment)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            viewData.Service = this.Service.ToString();
            viewData.PatientId = patientId;
            if (IsRap(type))
            {
                viewData.Category = ClaimTypeSubCategory.RAP;
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                //if (profile != null)
                //{
                var rap = billingRepository.GetRapOnly(Current.AgencyId, Id);
                if (rap != null)
                {
                    rap.Payment = paymentAmount;
                    rap.PaymentDate = paymentDate;
                    rap.Comment = comment;
                    //rap.PrimaryInsuranceId = primaryInsuranceId;
                    var isStatusChange = rap.Status != status;
                    var oldStatus = rap.Status;
                    var unProcessed = BillingStatusFactory.UnProcessed();
                    bool oldStatusOpened = unProcessed.Contains(oldStatus);
                    bool newStatusOpened = unProcessed.Contains(status);
                    if (newStatusOpened)
                    {
                        rap.IsVerified = false;
                        rap.IsGenerated = false;
                    }
                    if (status == (int)BillingStatus.ClaimSubmitted && rap.Status != (int)BillingStatus.ClaimSubmitted)
                    {
                        rap.ClaimDate = DateTime.Now;
                    }
                    rap.Status = status;
                    //if (rap.Status == (int)BillingStatus.ClaimPaidClaim ||
                    //    rap.Status == (int)BillingStatus.ClaimPaymentPending ||
                    //    rap.Status == (int)BillingStatus.ClaimAccepted ||
                    //    rap.Status == (int)BillingStatus.ClaimSubmitted)
                    //{
                    //}
                    if (isStatusChange && oldStatusOpened && !newStatusOpened)
                    {
                        if (!rap.IsVerified)
                        {
                            this.UpdateOrAddPayorForClaimAppSpecific<RapClaimInsurance>(rap);
                        }
                        //rap.Insurance = SerializedInsuranceForClaim<Rap>(rap, profile.AgencyLocationId, rap.PrimaryInsuranceId, claim => ((claim.IsVerified && claim.Insurance.IsNullOrEmpty()) || (!claim.IsVerified))) ?? rap.Insurance;
                    }
                    if (billingRepository.UpdateRapModel(rap))
                    {
                        var final = billingRepository.GetFinalOnly(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                        if (final != null)
                        {
                            final.IsRapGenerated = rap.IsGenerated;
                            billingRepository.UpdateFinalModel(final);
                        }
                        viewData.IsMedicareHistoryRefresh = true;
                        var pending = BillingStatusFactory.Pending();
                        if (isStatusChange && (pending.Contains(oldStatus) || pending.Contains(rap.Status)))
                        {
                            viewData.IsPedingRAPRefresh = true;
                        }
                        if (isStatusChange && (oldStatusOpened || newStatusOpened))
                        {
                            viewData.IsRAPRefresh = true;
                        }
                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        viewData.isSuccessful = true;
                    }
                }
                //}
            }
            else if (IsFinal(type))
            {
                viewData.Category = ClaimTypeSubCategory.Final;
                var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    var final = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                    if (final != null)
                    {
                        final.Payment = paymentAmount;
                        final.PaymentDate = paymentDate;
                        final.Comment = comment;
                        // final.PrimaryInsuranceId = primaryInsuranceId;
                        var isStatusChange = final.Status != status;
                        var oldStatus = final.Status;
                        var unProcessed = BillingStatusFactory.UnProcessed();
                        bool oldStatusOpened = unProcessed.Contains(oldStatus);
                        bool newStatusOpened = unProcessed.Contains(status);
                        if (newStatusOpened)
                        {
                            final.IsFinalInfoVerified = false;
                            final.IsSupplyVerified = false;
                            final.IsVisitVerified = false;
                            final.IsGenerated = false;
                        }
                        if (status == (int)BillingStatus.ClaimSubmitted && final.Status != (int)BillingStatus.ClaimSubmitted)
                        {
                            final.ClaimDate = DateTime.Now;
                        }
                        if (isStatusChange && oldStatusOpened && !newStatusOpened)
                        {
                            if (!final.IsFinalInfoVerified || !final.IsInsuranceVerified)
                            {
                                this.UpdateOrAddPayorForClaimAppSpecific<FinalClaimInsurance>(final);
                            }
                            //final.Insurance = SerializedInsuranceForClaim<Final>(final, profile.AgencyLocationId, final.PrimaryInsuranceId, claim => ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))) ?? final.Insurance;
                        }
                        final.Status = status;
                        //if (final.Status == (int)BillingStatus.ClaimPaidClaim ||
                        //    final.Status == (int)BillingStatus.ClaimPaymentPending ||
                        //    final.Status == (int)BillingStatus.ClaimAccepted ||
                        //    final.Status == (int)BillingStatus.ClaimSubmitted)
                        //{
                        //    // final.IsGenerated = true;
                        //}
                        if (billingRepository.UpdateFinalModel(final))
                        {
                            var pending = BillingStatusFactory.Pending();
                            viewData.IsMedicareHistoryRefresh = true;
                            if (oldStatus != final.Status && (pending.Contains(oldStatus) || pending.Contains(final.Status)))
                            {
                                viewData.IsPedingFinalRefresh = true;
                            }
                            if (oldStatus != final.Status && (oldStatusOpened || newStatusOpened))
                            {
                                viewData.IsFinalRefresh = true;
                            }

                            Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                            viewData.isSuccessful = true;
                        }
                    }
                }
            }
            return viewData;
        }

        //public bool UpdatePendingClaimStatus(FormCollection formCollection, string[] rapIds, string[] finalIds)
        //{
        //    bool result = false;
        //    var keys = formCollection.AllKeys;
        //    if (keys != null && keys.Length > 0)
        //    {
        //        if (rapIds != null && rapIds.Length > 0)
        //        {
        //            foreach (var id in rapIds)
        //            {
        //                var guidId = id.ToGuid();
        //                var type = keys.Contains(string.Format("RapType_{0}", id)) && formCollection[string.Format("RapType_{0}", id)].IsNotNullOrEmpty() ? formCollection[string.Format("RapType_{0}", id)].ToString() : string.Empty;
        //                var date = keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() ? formCollection[string.Format("RapPaymentDate_{0}", id)] : string.Empty;
        //                var paymentAmount = keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapPayment_{0}", id)].IsDouble() ? formCollection[string.Format("RapPayment_{0}", id)].ToDouble() : 0;
        //                var status = keys.Contains(string.Format("RapStatus_{0}", id)) && formCollection[string.Format("RapStatus_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapStatus_{0}", id)].IsInteger() ? formCollection[string.Format("RapStatus_{0}", id)].ToInteger() : 0;
        //                if (!guidId.IsEmpty() && type.IsNotNullOrEmpty())
        //                {
        //                    if (IsRap(type))
        //                    {
        //                        var rap = billingRepository.GetRapOnly(Current.AgencyId, guidId);
        //                        if (rap != null)
        //                        {
        //                            var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, rap.PatientId);
        //                            if (profile != null)
        //                            {
        //                                rap.Payment = paymentAmount;
        //                                rap.PaymentDate = date.IsNotNullOrEmpty() ? date.ToDateTime() : rap.PaymentDate;
        //                                if (status == (int)BillingStatus.ClaimSubmitted && rap.Status != (int)BillingStatus.ClaimSubmitted)
        //                                {
        //                                    rap.ClaimDate = DateTime.Now;
        //                                }
        //                                var isStatusChange = rap.Status == status;
        //                                var oldStatus = rap.Status;
        //                                if (!isStatusChange && (oldStatus == (int)BillingStatus.ClaimCreated || oldStatus == (int)BillingStatus.ClaimReOpen))
        //                                {
        //                                    rap.Insurance = SerializedInsuranceForClaim<Rap>(rap, profile.AgencyLocationId, rap.PrimaryInsuranceId, claim => ((claim.IsVerified && claim.Insurance.IsNullOrEmpty()) || (!claim.IsVerified))) ?? rap.Insurance;
        //                                }
        //                                rap.Status = status;
        //                                if (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen)
        //                                {
        //                                    rap.IsVerified = false;
        //                                    rap.IsGenerated = false;
        //                                }
        //                                if (rap.Status == (int)BillingStatus.ClaimPaidClaim ||
        //                                    rap.Status == (int)BillingStatus.ClaimPaymentPending ||
        //                                    rap.Status == (int)BillingStatus.ClaimAccepted ||
        //                                    rap.Status == (int)BillingStatus.ClaimSubmitted)
        //                                {
        //                                    //  rap.IsGenerated = true;
        //                                }
        //                                if (billingRepository.UpdateRapStatus(rap))
        //                                {
        //                                    var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
        //                                    if (final != null)
        //                                    {
        //                                        final.IsRapGenerated = rap.IsGenerated;
        //                                        billingRepository.UpdateFinalStatus(final);
        //                                    }
        //                                    Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
        //                                    result = true;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        if (finalIds != null && finalIds.Length > 0)
        //        {
        //            foreach (var id in finalIds)
        //            {
        //                var guidId = id.ToGuid();
        //                var type = keys.Contains(string.Format("FinalType_{0}", id)) && formCollection[string.Format("FinalType_{0}", id)].IsNotNullOrEmpty() ? formCollection[string.Format("FinalType_{0}", id)].ToString() : string.Empty;
        //                var date = keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() ? formCollection[string.Format("FinalPaymentDate_{0}", id)] : string.Empty;
        //                var paymentAmount = keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() ? formCollection[string.Format("FinalPayment_{0}", id)].ToDouble() : 0;
        //                var status = keys.Contains(string.Format("FinalStatus_{0}", id)) && formCollection[string.Format("FinalStatus_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalStatus_{0}", id)].IsInteger() ? formCollection[string.Format("FinalStatus_{0}", id)].ToInteger() : 0;
        //                if (!guidId.IsEmpty() && type.IsNotNullOrEmpty())
        //                {
        //                    if (IsFinal(type))
        //                    {
        //                        var final = billingRepository.GetFinal(Current.AgencyId, guidId);
        //                        if (final != null)
        //                        {
        //                            var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, final.PatientId);
        //                            if (profile != null)
        //                            {
        //                                final.Payment = paymentAmount;
        //                                final.PaymentDate = date.IsNotNullOrEmpty() ? date.ToDateTime() : final.PaymentDate;
        //                                var isStatusChange = final.Status == status;
        //                                var oldStatus = final.Status;
        //                                if (status == (int)BillingStatus.ClaimSubmitted && final.Status != (int)BillingStatus.ClaimSubmitted)
        //                                {
        //                                    final.ClaimDate = DateTime.Now;
        //                                }
        //                                if (!isStatusChange && (oldStatus == (int)BillingStatus.ClaimCreated || oldStatus == (int)BillingStatus.ClaimReOpen))
        //                                {
        //                                    final.Insurance = SerializedInsuranceForClaim<Final>(final, profile.AgencyLocationId, final.PrimaryInsuranceId, claim => ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))) ?? final.Insurance;
        //                                }
        //                                final.Status = status;
        //                                if (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen)
        //                                {
        //                                    final.IsFinalInfoVerified = false;
        //                                    final.IsSupplyVerified = false;
        //                                    final.IsVisitVerified = false;
        //                                    final.IsGenerated = false;
        //                                }
        //                                if (final.Status == (int)BillingStatus.ClaimPaidClaim ||
        //                                    final.Status == (int)BillingStatus.ClaimPaymentPending ||
        //                                    final.Status == (int)BillingStatus.ClaimAccepted ||
        //                                    final.Status == (int)BillingStatus.ClaimSubmitted)
        //                                {
        //                                    // final.IsGenerated = true;
        //                                }
        //                                if (billingRepository.UpdateFinalStatus(final))
        //                                {
        //                                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
        //                                    result = true;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        public bool UpdateRapClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status)
        {
            bool result = false;

            var rap = billingRepository.GetRapOnly(Current.AgencyId, Id);
            if (rap != null)
            {
                var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, rap.PatientId);
                if (profile != null)
                {
                    rap.Payment = paymentAmount;
                    rap.PaymentDate = paymentDate;
                    var isStatusChange = rap.Status != status;
                    var oldStatus = rap.Status;
                    var unProcessed = BillingStatusFactory.UnProcessed();
                    bool oldStatusOpened = unProcessed.Contains(oldStatus);
                    bool newStatusOpened = unProcessed.Contains(status);
                    if (status == (int)BillingStatus.ClaimSubmitted && rap.Status != (int)BillingStatus.ClaimSubmitted)
                    {
                        rap.ClaimDate = DateTime.Now;
                    }

                    if (isStatusChange && oldStatusOpened && !newStatusOpened)
                    {
                        if (!rap.IsVerified)
                        {
                            this.UpdateOrAddPayorForClaimAppSpecific<RapClaimInsurance>(rap);
                        }
                        //rap.Insurance = SerializedInsuranceForClaim<Rap>(rap, profile.AgencyLocationId, rap.PrimaryInsuranceId, claim => ((claim.IsVerified && claim.Insurance.IsNullOrEmpty()) || (!claim.IsVerified))) ?? rap.Insurance;
                    }
                    rap.Status = status;
                    //if (rap.Status == (int)BillingStatus.ClaimPaidClaim ||
                    //    rap.Status == (int)BillingStatus.ClaimPaymentPending ||
                    //    rap.Status == (int)BillingStatus.ClaimAccepted ||
                    //    rap.Status == (int)BillingStatus.ClaimSubmitted)
                    //{
                    //    // rap.IsGenerated = true;
                    //}
                    if (newStatusOpened)
                    {
                        rap.IsVerified = false;
                        rap.IsGenerated = false;
                    }
                    if (billingRepository.UpdateRapModel(rap))
                    {
                        var final = billingRepository.GetFinalOnly(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                        if (final != null)
                        {
                            final.IsRapGenerated = rap.IsGenerated;
                            billingRepository.UpdateFinalModel(final);
                        }
                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateFinalClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status)
        {
            bool result = false;
            var final = billingRepository.GetFinalOnly(Current.AgencyId, Id);
            if (final != null)
            {
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, final.PatientId);
                //if (profile != null)
                //{
                final.Payment = paymentAmount;
                final.PaymentDate = paymentDate;
                var isStatusChange = final.Status != status;
                var oldStatus = final.Status;
                var unProcessed = BillingStatusFactory.UnProcessed();
                bool oldStatusOpened = unProcessed.Contains(oldStatus);
                bool newStatusOpened = unProcessed.Contains(status);
                if (status == (int)BillingStatus.ClaimSubmitted && final.Status != (int)BillingStatus.ClaimSubmitted)
                {
                    final.ClaimDate = DateTime.Now;
                }

                if (isStatusChange && oldStatusOpened && !newStatusOpened)
                {
                    if (!final.IsFinalInfoVerified || !final.IsInsuranceVerified)
                    {
                        this.UpdateOrAddPayorForClaimAppSpecific<FinalClaimInsurance>(final);
                    }
                    //final.Insurance = SerializedInsuranceForClaim<Final>(final, profile.AgencyLocationId, final.PrimaryInsuranceId, claim => ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))) ?? final.Insurance;
                }
                final.Status = status;
                if (newStatusOpened)
                {
                    final.IsFinalInfoVerified = false;
                    final.IsSupplyVerified = false;
                    final.IsVisitVerified = false;
                    final.IsGenerated = false;
                }
                //if (final.Status == (int)BillingStatus.ClaimPaidClaim ||
                //    final.Status == (int)BillingStatus.ClaimPaymentPending ||
                //    final.Status == (int)BillingStatus.ClaimAccepted ||
                //    final.Status == (int)BillingStatus.ClaimSubmitted)
                //{
                //    // final.IsGenerated = true;
                //}
                if (billingRepository.UpdateFinalModel(final))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                    result = true;
                }
                //}
            }
            return result;
        }

        public bool IsEpisodeHasClaim(Guid episodeId, Guid patientId, int type)
        {
            return billingRepository.IsEpisodeHasClaim(Current.AgencyId, patientId, episodeId, type);
        }

        public NewClaimViewData GetEpisodeNeedsClaimViewData(Guid patientId, int type)
        {
            var newClaimData = new NewClaimViewData();
            newClaimData.EpisodeData = billingRepository.GetEpisodeNeedsClaim(Current.AgencyId, patientId, type);
            newClaimData.Type = type;
            newClaimData.PatientId = patientId;
            return newClaimData;
        }

        //public Bill AllUnProcessedBill(Guid branchId, int insuranceId, string sortType, string claimType, bool isUsersNeeded)
        //{
        //    var bill = new Bill();
        //    bill.Service = this.Service;
        //    if (ClaimTypeSubCategory.RAP.ToString().IsEqual(claimType))
        //    {
        //        bill.ClaimType = ClaimTypeSubCategory.RAP;
        //        var raps = AllUnProcessedRaps(branchId, insuranceId, false, isUsersNeeded);
        //        if (raps != null && raps.Count > 0)
        //        {
        //            if (sortType == "rap-name-invert") raps.Reverse();
        //            else if (sortType == "rap-id") raps = raps.OrderBy(rap => rap.PatientIdNumber).ToList();
        //            else if (sortType == "rap-id-invert") raps = raps.OrderBy(rap => rap.PatientIdNumber).Reverse().ToList();
        //            else if (sortType == "rap-episode") raps = raps.OrderBy(rap => rap.EpisodeStartDate.ToString("yyyyMMdd")).ToList();
        //            else if (sortType == "rap-episode-invert") raps = raps.OrderBy(rap => rap.EpisodeStartDate.ToString("yyyyMMdd")).Reverse().ToList();
        //            bill.Claims = raps != null ? raps : new List<Claim>();
        //        }
        //    }
        //    else if (ClaimTypeSubCategory.Final.ToString().IsEqual(claimType))
        //    {
        //        bill.ClaimType = ClaimTypeSubCategory.Final;
        //        var finals = AllUnProcessedFinals(branchId, insuranceId, false, isUsersNeeded);
        //        if (finals != null && finals.Count > 0)
        //        {
        //            if (sortType == "rap-name-invert") finals.Reverse();
        //            else if (sortType == "rap-id") finals = finals.OrderBy(final => final.PatientIdNumber).ToList();
        //            else if (sortType == "rap-id-invert") finals = finals.OrderBy(final => final.PatientIdNumber).Reverse().ToList();
        //            else if (sortType == "rap-episode") finals = finals.OrderBy(final => final.EpisodeStartDate.ToString("yyyyMMdd")).ToList();
        //            else if (sortType == "rap-episode-invert") finals = finals.OrderBy(final => final.EpisodeStartDate.ToString("yyyyMMdd")).Reverse().ToList();
        //            bill.Claims = finals != null ? finals : new List<Claim>();
        //        }
        //    }
        //    else if (ClaimTypeSubCategory.ManagedCare.ToString().IsEqual(claimType))
        //    {
        //        bill.ClaimType = ClaimTypeSubCategory.ManagedCare;
        //        var manageClaims = billingRepository.
        //            GetManagedClaims(Current.AgencyId, branchId, insuranceId, (int)ManagedClaimStatus.ClaimCreated, false);
        //        if (manageClaims != null && manageClaims.Count > 0)
        //        {
        //            if (sortType == "rap-name-invert") manageClaims.Reverse();
        //            else if (sortType == "rap-id") manageClaims = manageClaims.OrderBy(claim => claim.PatientIdNumber).ToList();
        //            else if (sortType == "rap-id-invert") manageClaims = manageClaims.OrderBy(claim => claim.PatientIdNumber).Reverse().ToList();
        //            else if (sortType == "rap-episode") manageClaims = manageClaims.OrderBy(claim => claim.EpisodeStartDate.ToString("yyyyMMdd")).ToList();
        //            else if (sortType == "rap-episode-invert") manageClaims = manageClaims.OrderBy(claim => claim.EpisodeStartDate.ToString("yyyyMMdd")).Reverse().ToList();
        //            bill.Claims = manageClaims != null ? manageClaims : new List<Claim>();
        //        }
        //    }
        //    bill.BranchId = branchId;
        //    bill.Insurance = insuranceId;
        //    bill.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, insuranceId);
        //    return bill;
        //}

        /// <summary>
        /// rc= raps table representation
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="insuranceId"></param>
        /// <param name="IsZeroInsuraceIdAll"></param>
        /// <param name="isUsersNeeded"></param>
        /// <returns></returns>
        public List<Claim> AllUnProcessedRaps(Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded)
        {
            var processedRaps = new List<Claim>();
            var insurnaceFilter = GetInsuranceFilter(branchId, insuranceId, IsZeroInsuraceIdAll, "rc");
            var raps = billingRepository.GetOutstandingRapClaims(Current.AgencyId, branchId, insuranceId, insurnaceFilter);
            if (raps != null && raps.Count > 0)
            {
                var users = new List<User>();
                if (isUsersNeeded)
                {
                    var userIds = raps.Select(r => r.ClinicianId).ToList();
                    userIds.AddRange(raps.Select(r => r.CaseManagerId));
                    userIds = userIds.Distinct().ToList();
                    users = UserEngine.GetUsers(Current.AgencyId, userIds);
                }
                var visitStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA();// new int[] { (int)ScheduleStatus.NoteCompleted, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.EvalToBeSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
                var socDisciplineTasks = DisciplineTaskFactory.SOCDisciplineTasks(true);// new int[] { (int)DisciplineTasks.OASISCStartOfCare, (int)DisciplineTasks.OASISCStartOfCarePT, (int)DisciplineTasks.OASISCStartOfCareOT };
                var recertOrRocDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT };
                var oasisStatus = ScheduleStatusFactory.OASISAfterQA(); //new int[] { (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedNotExported };
                var schedules = scheduleRepository.GetRAPClaimSchedules(Current.AgencyId, raps.Select(r => r.EpisodeId).ToList());
                raps.ForEach(rap =>
                {
                    if (rap.Status == (int)BillingStatus.ClaimCreated)
                    {
                        rap.IsFirstBillableVisit = rap.IsFirstBillableVisit || schedules.Exists(s => s.EpisodeId == rap.EpisodeId && s.IsBillable && visitStatus.Contains(s.Status));

                        if (!rap.IsOasisComplete)
                        {
                            var soc = schedules.Where(s => !s.EpisodeId.IsEmpty() && s.EpisodeId == rap.EpisodeId && socDisciplineTasks.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate.Date).FirstOrDefault();
                            if (soc != null)
                            {
                                if (oasisStatus.Contains(soc.Status))
                                {
                                    rap.IsOasisComplete = true;
                                }
                            }
                            else
                            {
                                var recertOrRoc = schedules.Where(s => !s.NewEpisodeId.IsEmpty() && s.NewEpisodeId != s.EpisodeId && s.NextEpisodeId == rap.EpisodeId && recertOrRocDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate.Date).FirstOrDefault();
                                if (recertOrRoc != null)
                                {
                                    if (oasisStatus.Contains(recertOrRoc.Status))
                                    {
                                        rap.IsOasisComplete = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        rap.IsFirstBillableVisit = true;
                        rap.IsOasisComplete = true;
                        rap.IsVerified = true;
                    }
                    if (isUsersNeeded)
                    {
                        var clinician = users.FirstOrDefault(u => u.Id == rap.ClinicianId);
                        var casemanager = users.FirstOrDefault(u => u.Id == rap.CaseManagerId);
                        rap.Clinician = clinician != null ? clinician.DisplayName : string.Empty;
                        rap.CaseManager = casemanager != null ? casemanager.DisplayName : string.Empty;
                    }
                    processedRaps.Add(rap);
                });

            }
            return processedRaps;
        }

        private static string GetInsuranceFilter(Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, string table)
        {
            var insurnaceFilter = string.Empty;
            if (insuranceId > 0)
            {
                insurnaceFilter = string.Format(" AND {0}.PrimaryInsuranceId = @insuranceId ", table);
            }
            else
            {
                if (IsZeroInsuraceIdAll && insuranceId == 0)
                {
                    insurnaceFilter = string.Format(" AND {0}.PrimaryInsuranceId > 0 ", table);
                }
                else
                {
                    if (!branchId.IsEmpty())
                    {
                        var payor = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
                        if (payor > 0)
                        {
                            insurnaceFilter = string.Format(" AND {0}.PrimaryInsuranceId <= 0  AND  pr.PrimaryInsurance = {1} ", table, payor);
                        }
                        else
                        {
                            insurnaceFilter = string.Format(" AND {0}.PrimaryInsuranceId <= 0 ", table);
                        }
                    }
                    else
                    {
                        insurnaceFilter = string.Format(" AND {0}.PrimaryInsuranceId <= 0 ", table);
                    }
                }
            }
            return insurnaceFilter;
        }

        private static string GetInsuranceFilter(Guid branchId, int insuranceId, string table, out bool returnEmptyData)
        {
            var insurnaceFilter = string.Empty;
            returnEmptyData = false;
            if (insuranceId < 0)
            {
                returnEmptyData = true;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var payor = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
                    if (payor > 0)
                    {
                        insurnaceFilter = string.Format(" AND ({0}.PrimaryInsuranceId = {1} ||  {0}.PrimaryInsuranceId >= 1000 ) ", table, payor);
                    }
                    else
                    {
                        returnEmptyData = true;
                    }

                }
                else
                {
                    // insuranceScript = " AND raps.PrimaryInsuranceId < 1000";
                }

            }
            else
            {
                insurnaceFilter = string.Format(" AND {0}.PrimaryInsuranceId = @insuranceId", table);
            }
            return insurnaceFilter;
        }

        /// <summary>
        /// fc = finals table representation
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="insuranceId"></param>
        /// <param name="IsZeroInsuraceIdAll"></param>
        /// <param name="isUsersNeeded"></param>
        /// <returns></returns>
        public List<Claim> AllUnProcessedFinals(Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded)
        {
            var processedFinals = new List<Claim>();
            var insurnaceFilter = GetInsuranceFilter(branchId, insuranceId, IsZeroInsuraceIdAll, "fc");
            var finals = billingRepository.GetOutstandingFinalClaims(Current.AgencyId, branchId, insuranceId, insurnaceFilter);

            if (finals.IsNotNullOrEmpty())
            {
                var users = new List<User>();
                if (isUsersNeeded)
                {
                    var userIds = finals.Select(r => r.ClinicianId).ToList();
                    userIds.AddRange(finals.Select(r => r.CaseManagerId));
                    userIds = userIds.Distinct().ToList();
                    users = UserEngine.GetUsers(Current.AgencyId, userIds);
                }
                var visitDisciplines = DisciplineFactory.NonBillableDisciplines().Select(d => d.ToString());// new string[] { Disciplines.ReportsAndNotes.ToString(), Disciplines.Orders.ToString(), Disciplines.Claim.ToString() };
                var visitStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA();// new int[] { (int)ScheduleStatus.NoteCompleted, (int)ScheduleStatus.OasisCompletedNotExported, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.EvalToBeSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
                var orderDisciplineTasks = DisciplineTaskFactory.PhysicianOrdersWithOutFaceToFace();// new int[] { (int)DisciplineTasks.PhysicianOrder, (int)DisciplineTasks.HCFA485, (int)DisciplineTasks.NonOasisHCFA485 };
                var ids = finals.Select(r => r.EpisodeId).ToList();
                var schedules = scheduleRepository.GetFinalClaimSchedules(Current.AgencyId, ids);
                var raps = billingRepository.GetRapsToGenerateByIdsLean(Current.AgencyId, ids);
                var unProcessed = BillingStatusFactory.UnProcessed();
                finals.ForEach(final =>
                {
                    if (final.Status == (int)BillingStatus.ClaimCreated)
                    {
                        final.AreVisitsComplete = final.IsVisitVerified || !schedules.Exists(s => s.EpisodeId == final.EpisodeId && !s.IsDeprecated && !visitDisciplines.Contains(s.Discipline) && !(s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter) && !visitStatus.Contains(s.Status) && !s.IsMissedVisit && s.IsBillable);
                        if (!final.AreOrdersComplete)
                        {
                            var isCurrentOrdersNotComplete = !schedules.Exists(s => !s.Id.IsEmpty() && s.EpisodeId == final.EpisodeId && !s.IsDeprecated && !s.IsMissedVisit && orderDisciplineTasks.Contains(s.DisciplineTask) && !s.IsOrderForNextEpisode && s.Status != (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            if (isCurrentOrdersNotComplete)
                            {
                                var isPreviousOrdersComplete = !schedules.Exists(s => !s.Id.IsEmpty() && s.NextEpisodeId == final.EpisodeId && !s.IsDeprecated && !s.IsMissedVisit && orderDisciplineTasks.Contains(s.DisciplineTask) && s.IsOrderForNextEpisode && s.Status != (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                if (isPreviousOrdersComplete)
                                {
                                    final.AreOrdersComplete = true;
                                }
                                else
                                {
                                    final.AreOrdersComplete = false;
                                }
                            }
                            else
                            {
                                final.AreOrdersComplete = false;
                            }
                        }
                        final.IsRapGenerated = final.IsRapGenerated || raps.Exists(r => r.Id == final.Id && (r.IsGenerated || unProcessed.Contains(r.Status)));
                    }
                    else
                    {
                        final.AreOrdersComplete = true;
                        final.IsRapGenerated = true;
                    }
                    if (isUsersNeeded)
                    {
                        var clinician = users.FirstOrDefault(u => u.Id == final.ClinicianId);
                        var casemanager = users.FirstOrDefault(u => u.Id == final.CaseManagerId);
                        final.Clinician = clinician != null ? clinician.DisplayName : string.Empty;
                        final.CaseManager = casemanager != null ? casemanager.DisplayName : string.Empty;
                    }
                    processedFinals.Add(final);
                });
            }
            return processedFinals;
        }

        public Final GetFinalPrint(Guid episodeId, Guid patientId)
        {
            var final = GetFinalPrintOrSummary(patientId, episodeId);
            if (final != null)
            {
                final.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                //if (profile != null)
                //{
                final.SupplyTotal = this.MedicareSupplyTotal(final);
                if (final.VerifiedVisits.IsNotNullOrEmpty())
                {
                    // final.AgencyLocationId = profile.AgencyLocationId;
                    var visits = final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid() && s.EventDate.IsValid()).OrderBy(s => s.VisitDate.Date).ThenBy(s => s.EventDate.Date).ToList();
                    if (visits.IsNotNullOrEmpty())
                    {
                        // var agencyInsurance = new AgencyInsurance();
                        var chargeRates = ClaimInsuranceRatesAppSpecific<FinalClaimInsurance>(final.Id, final.PayorType); //var chargeRates = this.FinalToCharegRates(final, out agencyInsurance, false);
                        final.BillVisitSummaryDatas = this.BillableVisitSummary(visits, MedicareIntermediaryFactory.Intermediaries().Contains(final.PrimaryInsuranceId) ? ClaimType.CMS : ClaimType.HMO, chargeRates, false);
                    }
                }

                //}
            }
            return final;
        }

        public Final GetFinalWithSupplies(Guid episodeId, Guid patientId)
        {
            Final claim = null;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
                if (claim != null)
                {
                    claim.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                    var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        claim.EpisodeStartDate = episode.StartDate;
                        claim.EpisodeEndDate = episode.EndDate;
                    }
                    //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                    //if (profile != null)
                    //{
                    //    claim.BranchId = profile.AgencyLocationId;
                    //}

                    if (claim.VerifiedVisits.IsNotNullOrEmpty())
                    {
                        var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                        if (visits.IsNotNullOrEmpty())
                        {
                            var supplyList = GetSupplies(patientId, claim.Supply, visits, false);
                            if (supplyList.IsNotNullOrEmpty())
                            {
                                claim.Supply = supplyList.ToXml();
                            }
                        }
                        //var supplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                        //if (visits != null && visits.Count > 0)
                        //{
                        //    visits.ForEach(v =>
                        //    {
                        //        var episodeSupply = GetSupply(v);
                        //        if (episodeSupply != null && episodeSupply.Count > 0)
                        //        {
                        //            episodeSupply.ForEach(s =>
                        //            {
                        //                if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                        //                {
                        //                    supplyList.Add(s);
                        //                }
                        //            });
                        //        }
                        //    });
                        //    claim.Supply = supplyList.ToXml();
                        //}
                    }
                }
            }
            return claim;
        }

        public Rap GetRapPrint(Guid episodeId, Guid patientId)
        {
            var rap = GetRap(patientId, episodeId);
            if (rap != null)
            {
                rap.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                rap.AdmissionSourceDisplay = rap.AdmissionSource.GetAdmissionDescription();
            }
            return rap;
        }

        public string GenerateJsonRAP(List<Guid> rapToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Rap> raps, bool isHMO, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            raps = null;
            try
            {
                #region
                if (branch != null)
                {
                    var rapClaim = billingRepository.GetRapsToGenerateByIds(Current.AgencyId, rapToGenerate);
                    if (rapClaim.IsNotNullOrEmpty())
                    {
                        raps = rapClaim;
                        var patients = new List<AnsiPatient>();
                        var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
                        foreach (var rap in rapClaim)
                        {
                            var claims = new List<AnsiClaim>();
                            claimInfo.Add(new ClaimInfo { ClaimId = rap.Id, PatientId = rap.PatientId, EpisodeId = rap.EpisodeId, ClaimType = rap.Type == 0 ? "322" : "" });

                            var rapObj = ClaimsWithVisitsAndSupplies(rap, null, 0, null, 0, false, ubo4DischargeStatus);
                            rapObj.claim_type = rap.Type == 1 ? "328" : "322";
                            rapObj.claim_supply_isBillable = false;
                            claims.Add(rapObj);

                            var patient = PatientWithClaims(rap, ubo4DischargeStatus, claims);
                            patient.patient_medicare_num = rap.MedicareNumber;
                            patients.Add(patient);
                        }
                        var agencyClaim = MedicareOrMedicareHMOAgencyWithPatients(commandType, isHMO ? ClaimType.HMO : ClaimType.CMS, claimId, payerInfo, branch, patients);
                        var jss = new JavaScriptSerializer();
                        requestArr = jss.Serialize(agencyClaim);
                    }
                }
                #endregion
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return requestArr;
        }

        //public string GenerateJsonFinal(List<Guid> finalToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Final> finlas, bool isHMO, AgencyLocation branch)
        //{
        //    string requestArr = string.Empty;
        //    claimInfo = new List<ClaimInfo>();
        //    finlas = null;
        //    try
        //    {
        //        var finalClaim = billingRepository.GetFinalsToGenerateByIds(Current.AgencyId, finalToGenerate);
        //        finlas = finalClaim;
        //        #region
        //        if (branch != null)
        //        {
        //            var patients = new List<AnsiPatient>();
        //            if (finalClaim != null && finalClaim.Count > 0)
        //            {
        //                var cliamInsurances = mongoRepository.GetManyClaimInsurances<FinalClaimInsurance>(Current.AgencyId, finalToGenerate);
        //                if (cliamInsurances.IsNotNullOrEmpty())
        //                {
        //                    var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
        //                    foreach (var final in finalClaim)
        //                    {
        //                        var claims = new List<AnsiClaim>();

        //                        var visitTotalAmount = 0.00;
        //                        var visits = final.VerifiedVisits.IsNotNullOrEmpty() ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<ScheduleEvent>();
        //                        var visitList = new List<AnsiVisit>();
        //                        if (visits.Count > 0)
        //                        {
        //                            //var agencyInsurance = new AgencyInsurance();
        //                            //var chargeRates = this.FinalToCharegRates(final, out agencyInsurance, false) ?? new List<ChargeRate>();
        //                            List<ChargeRate> chargeRates = null;
        //                            var claimInsurance = cliamInsurances.FirstOrDefault(i => i.Id == final.Id);
        //                            if (claimInsurance != null && claimInsurance.Insurance != null)
        //                            {
        //                                chargeRates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
        //                            }
        //                            else
        //                            {
        //                                var agencyInsurance = final.Insurance.ToObject<AgencyInsurance>();
        //                                if (agencyInsurance != null)
        //                                {
        //                                    chargeRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
        //                                }
        //                            }
        //                            chargeRates = chargeRates ?? new List<ChargeRate>();



        //                            var visitsDictionary = visits.GroupBy(v => v.VisitDate.Date).ToDictionary(v => v.Key, v => v.ToList());
        //                            if (visitsDictionary != null && visitsDictionary.Count > 0)
        //                            {
        //                                visitsDictionary.ForEach((visitPerDayKey, visitPerDayValue) =>
        //                                {
        //                                    var visitByDisciplneDictionary = visitPerDayValue.GroupBy(vd => vd.DisciplineTask).ToDictionary(vd => vd.Key, vd => vd.ToList());
        //                                    if (visitByDisciplneDictionary != null && visitByDisciplneDictionary.Count > 0)
        //                                    {
        //                                        visitByDisciplneDictionary.ForEach((vddk, vddv) =>
        //                                        {
        //                                            var rate = chargeRates.FirstOrDefault(c => c.Id == vddk);
        //                                            if (rate != null)
        //                                            {
        //                                                double amount;
        //                                                var addedVisits = BillableVisitForANSIWithAdjustments(rate, vddv, out amount, isHMO ? ClaimType.HMO : ClaimType.CMS, false,null);
        //                                                if (addedVisits != null && addedVisits.Count > 0)
        //                                                {
        //                                                    visitList.AddRange(addedVisits);
        //                                                    visitTotalAmount += amount;
        //                                                }
        //                                            }
        //                                        });
        //                                    }
        //                                });
        //                            }
        //                        }
        //                        var supplyList = new List<AnsiSupply>();
        //                        if (!final.IsSupplyNotBillable)
        //                        {
        //                            final.SupplyTotal = this.MedicareSupplyTotal(final);
        //                            supplyList = new List<AnsiSupply> { new AnsiSupply{ date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"), revenue = "0272", amount = final.SupplyTotal, units = 1 } };
        //                        }
        //                        else
        //                        {
        //                            final.SupplyTotal = 0;
        //                        }

        //                        var diagnosis = final.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(final.DiagnosisCode) : null;
        //                        var conditionCodes = final.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(final.ConditionCodes) : null;
        //                        var locator31 = ConvertStringLocatorToList(final.Ub04Locator31);
        //                        var locator32 = ConvertStringLocatorToList(final.Ub04Locator32);
        //                        var locator33 = ConvertStringLocatorToList(final.Ub04Locator33);
        //                        var locator34 = ConvertStringLocatorToList(final.Ub04Locator34);
        //                        var locator39 = ConvertStringLocatorToList(final.Ub04Locator39);
        //                        var locator81 = ConvertStringLocatorToList(final.Ub04Locator81cca);

        //                        claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "329" : "" });


        //                        var finalObj = new
        //                       AnsiClaim
        //                       {
        //                            claim_id = final.Id,
        //                            claim_type = final.Type == 1 ? "328" : "329",
        //                            claim_physician_upin = final.PhysicianNPI,
        //                            claim_physician_last_name = final.PhysicianLastName,
        //                            claim_physician_first_name = final.PhysicianFirstName,
        //                            claim_first_visit_date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
        //                            claim_episode_start_date = final.EpisodeStartDate.ToString("MM/dd/yyyy"),
        //                            claim_episode_end_date = final.EpisodeEndDate.ToString("MM/dd/yyyy"),
        //                            claim_hipps_code = final.HippsCode,
        //                            claim_oasis_key = final.ClaimKey,
        //                            hmo_plan_id = final.HealthPlanId,
        //                            claim_group_name = final.GroupName,
        //                            claim_group_Id = final.GroupId,
        //                            claim_hmo_auth_key = final.AuthorizationNumber,
        //                            claim_hmo_auth_key2 = final.AuthorizationNumber2,
        //                            claim_hmo_auth_key3 = final.AuthorizationNumber3,
        //                            claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
        //                            claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
        //                            claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
        //                            claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
        //                            claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
        //                            claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
        //                            claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
        //                            claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
        //                            claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
        //                            claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
        //                            claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
        //                            claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
        //                            claim_admission_source_code = final.AdmissionSource.IsNotNullOrEmpty() && final.AdmissionSource.IsInteger() ? final.AdmissionSource.ToInteger().GetSplitValue() : "9",
        //                            claim_patient_status_code = final.UB4PatientStatus,
        //                            claim_dob = ubo4DischargeStatus.Contains(final.UB4PatientStatus) ? final.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            claim_ub04locator81 = locator81,
        //                            claim_ub04locator39 = locator39,
        //                            claim_ub04locator31 = locator31,
        //                            claim_ub04locator32 = locator32,
        //                            claim_ub04locator33 = locator33,
        //                            claim_ub04locator34 = locator34,
        //                            claim_supply_isBillable = !final.IsSupplyNotBillable,
        //                            claim_supply_value = Math.Round(final.SupplyTotal, 2),
        //                            claim_supplies = supplyList,
        //                            claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
        //                            claim_visits = visitList,

        //                        };


        //                        claims.Add(finalObj);
        //                        var patient = new
        //                       AnsiPatient {
        //                            patient_gender = final.Gender.Substring(0, 1),
        //                            patient_medicare_num = final.MedicareNumber,
        //                            patient_record_num = final.PatientIdNumber,
        //                            patient_dob = final.DOB.ToString("MM/dd/yyyy"),
        //                            patient_doa = final.StartofCareDate.ToString("MM/dd/yyyy"),
        //                            patient_dod = ubo4DischargeStatus.Contains(final.UB4PatientStatus) && final.DischargeDate.Date > DateTime.MinValue.Date ? final.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            patient_address = final.AddressLine1,
        //                            patient_address2 = final.AddressLine2,
        //                            patient_city = final.AddressCity,
        //                            patient_state = final.AddressStateCode,
        //                            patient_zip = final.AddressZipCode,
        //                            patient_cbsa = lookUpRepository.CbsaCodeByZip(final.AddressZipCode),
        //                            patient_last_name = final.LastName,
        //                            patient_first_name = final.FirstName,
        //                            patient_middle_initial = "",
        //                            claims_arr = claims
        //                        };
        //                        patients.Add(patient);
        //                    }

        //                    if (isHMO)
        //                    {
        //                        var agencyClaim = new
        //                        AnsiAgency
        //                        {
        //                            format = "ansi837",
        //                            submit_type = commandType.ToString(),
        //                            user_login_name = Current.User.Name,
        //                            hmo_payer_id = payerInfo.HMOPayerId,
        //                            hmo_payer_name = payerInfo.HMOPayerName,
        //                            hmo_submitter_id = payerInfo.HMOSubmitterId,
        //                            hmo_provider_id = payerInfo.ProviderId,
        //                            hmo_other_provider_id = payerInfo.OtherProviderId,
        //                            hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
        //                            // hmo_additional_codes = additionaCodes,
        //                            payer_id = payerInfo.Code,
        //                            payer_name = payerInfo.Name,
        //                            submitter_id = payerInfo.SubmitterId,
        //                            insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
        //                            clearing_house_id = payerInfo.ClearingHouseSubmitterId,
        //                            provider_claim_type = payerInfo.BillType,
        //                            interchange_receiver_id = payerInfo.InterchangeReceiverId,
        //                            clearing_house = payerInfo.ClearingHouse,
        //                            claim_billtype = ClaimType.HMO.ToString(),
        //                            submitter_name = payerInfo.SubmitterName,
        //                            submitter_phone = payerInfo.Phone,
        //                            submitter_fax = payerInfo.Fax,
        //                            user_agency_name = branch.Name,
        //                            user_tax_id = branch.TaxId,
        //                            user_national_provider_id = branch.NationalProviderNumber,
        //                            user_address_1 = branch.AddressLine1,
        //                            user_address_2 = branch.AddressLine2,
        //                            user_city = branch.AddressCity,
        //                            user_state = branch.AddressStateCode,
        //                            user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
        //                            user_phone = branch.PhoneWork,
        //                            user_fax = branch.FaxNumber,
        //                            user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
        //                            ansi_837_id = claimId,
        //                            patients_arr = patients
        //                        };
        //                        var jss = new JavaScriptSerializer();
        //                        requestArr = jss.Serialize(agencyClaim);
        //                    }
        //                    else
        //                    {
        //                        var agencyClaim = new
        //                       AnsiAgency
        //                       {
        //                            format = "ansi837",
        //                            submit_type = commandType.ToString(),
        //                            user_login_name = Current.User.Name,
        //                            payer_id = payerInfo.Code,
        //                            payer_name = payerInfo.Name,
        //                            submitter_id = payerInfo.SubmitterId,
        //                            claim_billtype = ClaimType.CMS.ToString(),
        //                            submitter_name = payerInfo.SubmitterName,
        //                            submitter_phone = payerInfo.Phone,
        //                            submitter_fax = payerInfo.Fax,
        //                            user_agency_name = branch.Name,
        //                            user_tax_id = branch.TaxId,
        //                            user_national_provider_id = branch.NationalProviderNumber,
        //                            user_address_1 = branch.AddressLine1,
        //                            user_address_2 = branch.AddressLine2,
        //                            user_city = branch.AddressCity,
        //                            user_state = branch.AddressStateCode,
        //                            user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
        //                            user_phone = branch.PhoneWork,
        //                            user_fax = branch.FaxNumber,
        //                            user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
        //                            ansi_837_id = claimId,
        //                            patients_arr = patients
        //                        };
        //                        var jss = new JavaScriptSerializer();
        //                        requestArr = jss.Serialize(agencyClaim);
        //                    }
        //                }
        //            }
        //        }
        //        #endregion
        //    }
        //    catch (Exception)
        //    {
        //        return string.Empty;
        //    }
        //    return requestArr;
        //}

        public string GenerateJsonFinal(List<Guid> finalToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Final> finals, bool isHMO, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            finals = null;
            try
            {
                #region
                if (branch != null)
                {
                    var finalClaim = billingRepository.GetFinalsToGenerateByIds(Current.AgencyId, finalToGenerate);
                    if (finalClaim.IsNotNullOrEmpty())
                    {
                        finals = finalClaim;
                        var patients = new List<AnsiPatient>();
                        var cliamInsurances = mongoRepository.GetManyClaimInsurances<FinalClaimInsurance>(Current.AgencyId, finalToGenerate);
                        if (cliamInsurances.IsNotNullOrEmpty())
                        {
                            var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
                            foreach (var final in finalClaim)
                            {
                                var claims = new List<AnsiClaim>();

                                var visits = final.VerifiedVisits.IsNotNullOrEmpty() ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<ScheduleEvent>();

                                var isVisitExist = visits.IsNotNullOrEmpty();

                                List<ChargeRate> chargeRates = GetChargeRatesFromInsurance(cliamInsurances, final, isVisitExist);
                                var visitTotalAmount = 0.0;
                                var visitList = Visits(visits, chargeRates, out visitTotalAmount, isHMO ? ClaimType.HMO : ClaimType.CMS, null);


                                var supplyList = new List<AnsiSupply>();
                                if (!final.IsSupplyNotBillable)
                                {
                                    final.SupplyTotal = this.MedicareSupplyTotal(final);
                                    supplyList = new List<AnsiSupply> { new AnsiSupply { date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"), revenue = "0272", amount = final.SupplyTotal, units = 1 } };
                                }
                                else
                                {
                                    final.SupplyTotal = 0;
                                }

                                var finalObj = ClaimsWithVisitsAndSupplies(final, visitList, visitTotalAmount, supplyList, final.SupplyTotal, false, ubo4DischargeStatus);
                                finalObj.claim_type = final.Type == 1 ? "328" : "329";
                                finalObj.claim_supply_isBillable = !final.IsSupplyNotBillable;
                                claims.Add(finalObj);

                                var patient = PatientWithClaims(final, ubo4DischargeStatus, claims);
                                patient.patient_medicare_num = final.MedicareNumber;
                                patients.Add(patient);

                                claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "329" : "" });
                            }
                            var agencyClaim = MedicareOrMedicareHMOAgencyWithPatients(commandType, isHMO ? ClaimType.HMO : ClaimType.CMS, claimId, payerInfo, branch, patients);
                            var jss = new JavaScriptSerializer();
                            requestArr = jss.Serialize(agencyClaim);
                        }
                    }
                }
                #endregion
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return requestArr;
        }

        protected override BillingJsonViewData GenerateDirectlyAppSpecific(List<Guid> ClaimSelected, Guid branchId, int insuranceId, string claimType)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim(s) could not be processed." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            viewData.Service = this.Service.ToString();
            if (IsRap(claimType))
            {
                viewData.Category = ClaimTypeSubCategory.RAP;
            }
            else if (IsFinal(claimType))
            {
                viewData.Category = ClaimTypeSubCategory.Final;
            }
            if (this.GenerateMedicareDirectly(ClaimSelected, claimType, ClaimCommandType.direct, out claimDataOut, out billExchage, branchId, insuranceId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) have been processed successfully.";
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return viewData;
        }

        protected bool GenerateMedicareDirectly(List<Guid> ClaimSelected, string Type, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            if (Type.IsNotNullOrEmpty() && (IsRap(Type) || IsFinal(Type)))
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
                    if (agencyLocation != null)
                    {
                        if (!agencyLocation.IsLocationStandAlone)
                        {
                            agencyLocation.Payor = agency.Payor;
                            agencyLocation.SubmitterId = agency.SubmitterId;
                            agencyLocation.SubmitterName = agency.SubmitterName;
                            agencyLocation.SubmitterPhone = agency.SubmitterPhone;
                            agencyLocation.SubmitterFax = agency.SubmitterFax;
                            agencyLocation.Name = agency.Name;
                            agencyLocation.TaxId = agency.TaxId;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;

                        }
                        int payorId;
                        if (int.TryParse(agencyLocation.Payor, out payorId))
                        {
                            var payerInfo = lookUpRepository.SubmitterInfo(payorId);
                            if (payerInfo != null)
                            {
                                if (commandType == ClaimCommandType.download)
                                {
                                    payerInfo.SubmitterId = agencyLocation.SubmitterId;
                                    payerInfo.SubmitterName = agencyLocation.SubmitterName;
                                    payerInfo.Phone = agencyLocation.SubmitterPhone;
                                    payerInfo.Fax = agencyLocation.SubmitterFax;
                                }
                                var isHMO = false;
                                if (insuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                                    if (insurance != null && insurance.PayorType == 2)
                                    {
                                        isHMO = true;
                                        var checkHmoPayorId = false;
                                        var message = string.Empty;
                                        if (insurance.PayorId.IsNotNullOrEmpty() && !(insurance.PayorId == "0"))
                                        {
                                            payerInfo.HMOPayerId = insurance.PayorId;
                                            checkHmoPayorId = true;
                                        }
                                        else
                                        {
                                            checkHmoPayorId = false;
                                            message = "Payor Id information is not correct.";
                                        }
                                        payerInfo.HMOPayerName = insurance.Name;
                                        var checkHmoSubmitterId = false;
                                        if (insurance.SubmitterId.IsNotNullOrEmpty())
                                        {
                                            payerInfo.HMOSubmitterId = insurance.SubmitterId;
                                            checkHmoSubmitterId = true;
                                        }
                                        else
                                        {
                                            checkHmoSubmitterId = false;
                                            if (message.IsNullOrEmpty()) { message = "The submitter Id is not correct."; } else { message = message + "," + "The submitter Id is not correct."; }
                                        }
                                        if (!(checkHmoPayorId && checkHmoSubmitterId))
                                        {
                                            billExchange.Message = message;
                                            return false;
                                        }
                                        payerInfo.Ub04Locator81cca = insurance.Ub04Locator81cca;

                                        payerInfo.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                                        payerInfo.InterchangeReceiverId = insurance.InterchangeReceiverId;
                                        payerInfo.ClearingHouse = insurance.ClearingHouse;
                                        payerInfo.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                                        payerInfo.BillType = insurance.BillType;
                                    }
                                    else
                                    {
                                        billExchange.Message = "Medicare HMO information is not right.";
                                        return false;
                                    }
                                }
                                else if (insuranceId < 1000 && insuranceId > 0)
                                {
                                    isHMO = false;
                                }
                                else
                                {
                                    billExchange.Message = "Payor information is not right.";
                                    return false;
                                }
                                var claimId = GetNextClaimId(claimData);
                                claimData.Id = claimId;

                                var requestArr = string.Empty;
                                if (IsRap(Type))
                                {
                                    billExchange = SendRAPClaims(ClaimSelected, commandType, claimData, out claimInfo, payerInfo, isHMO, agencyLocation, out claimDataOut);
                                }
                                else if (IsFinal(Type))
                                {
                                    billExchange = SendFinalClaims(ClaimSelected, commandType, claimData, out claimInfo, payerInfo, isHMO, agencyLocation, out claimDataOut);
                                }
                                else
                                {
                                    billExchange.isSuccessful = false;
                                    billExchange.Message = "Claims type is not identified. Try again.";
                                }
                            }
                            else
                            {
                                billExchange.Message = "Payer information is not right.";
                            }
                        }
                        else
                        {
                            billExchange.Message = "Payer information is not right.";
                        }
                    }
                    else
                    {
                        billExchange.Message = "Branch information is not found. Try again.";
                    }
                }
                else
                {
                    billExchange.Message = "Claim Information is not correct. Try again.";
                }
            }
            else
            {
                billExchange.Message = "Claim type is not identified. Try again.";
            }
            return billExchange.isSuccessful;
        }

        protected override object GenerateDownloadAppSpecific(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId, string ClaimType)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (this.GenerateMedicareDirectly(ClaimSelected, ClaimType, ClaimCommandType.download, out claimDataOut, out billExchage, BranchId, InsuranceId))
            {
                if (claimDataOut != null)
                {
                    return new { isSuccessful = true, Id = claimDataOut.Id, isDownload = true };
                }
            }
            else
            {
                if (billExchage != null)
                {
                    return new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message, isDownload = true };
                }
            }
            return new { isSuccessful = false, Id = -1, errorMessage = "A problem occured while processing the claim(s).", isDownload = true };
        }

        public BillExchange SendRAPClaims(List<Guid> ClaimSelected, ClaimCommandType commandType, ClaimData claimData, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, bool isHMO, AgencyLocation agencyLocation, out ClaimData claimDataOut)
        {
            var billExchange = new BillExchange { isSuccessful = false, Message = "Error in generating the RAP calim." };
            List<Rap> rapClaims = null;
            claimDataOut = null;
            var requestArr = GenerateJsonRAP(ClaimSelected, commandType, claimData.Id, out claimInfo, payerInfo, out rapClaims, isHMO, agencyLocation);
            if (requestArr.IsNotNullOrEmpty())
            {
                if (billingRepository.AddRapSnapShots(rapClaims, claimData.Id))
                {
                    requestArr = requestArr.Replace("&", "U+0026");
                    billExchange = GenerateANSI(requestArr);

                    if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                    {
                        if (billExchange.Result.IsNotNullOrEmpty())
                        {
                            claimData.Data = billExchange.Result;
                            claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                            claimData.ClaimType = isHMO ? ClaimType.HMO.ToString() : ClaimType.CMS.ToString();
                            billingRepository.UpdateClaimData(claimData);
                            if (commandType == ClaimCommandType.direct)
                            {
                                if (rapClaims != null && rapClaims.Count > 0)
                                {
                                    billingRepository.MarkRapsAsSubmitted(Current.AgencyId, rapClaims);
                                    Auditor.AddGeneralMulitLog(LogDomain.Patient, rapClaims.GroupBy(rap => rap.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.Rap, LogAction.RAPSubmittedElectronically, string.Empty);
                                }
                            }
                            else if (commandType == ClaimCommandType.download)
                            {
                                if (rapClaims != null && rapClaims.Count > 0)
                                {
                                    billingRepository.MarkRapsAsGenerated(Current.AgencyId, rapClaims);
                                    Auditor.AddGeneralMulitLog(LogDomain.Patient, rapClaims.GroupBy(rap => rap.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.Rap, LogAction.RAPGenerated, string.Empty);
                                }
                            }
                            claimDataOut = claimData;
                            billExchange.isSuccessful = true;
                        }
                        else
                        {
                            billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                            billingRepository.DeleteRapSnapShots(claimData.Id);
                            claimDataOut = null;
                            billExchange.isSuccessful = false;
                        }
                    }
                    else
                    {
                        billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                        billingRepository.DeleteRapSnapShots(claimData.Id);
                        billExchange = billExchange ?? new BillExchange { Message = "Error in generating the calim." };
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                    billingRepository.DeleteRapSnapShots(claimData.Id);
                    billExchange.isSuccessful = false;
                }
            }
            else
            {
                billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                billExchange.Message = "There is a problem generating the claim data. Try Again.";
                billExchange.isSuccessful = false;
            }
            return billExchange;
        }

        public BillExchange SendFinalClaims(List<Guid> ClaimSelected, ClaimCommandType commandType, ClaimData claimData, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, bool isHMO, AgencyLocation agencyLocation, out ClaimData claimDataOut)
        {
            var billExchange = new BillExchange { isSuccessful = false, Message = "Error in generating the Final calim." };
            List<Final> finalClaims = null;
            claimDataOut = null;
            var requestArr = GenerateJsonFinal(ClaimSelected, commandType, claimData.Id, out claimInfo, payerInfo, out finalClaims, isHMO, agencyLocation);
            if (requestArr.IsNotNullOrEmpty())
            {
                if (billingRepository.AddFinaSnapShots(finalClaims, claimData.Id))
                {
                    requestArr = requestArr.Replace("&", "U+0026");
                    billExchange = GenerateANSI(requestArr);

                    if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                    {
                        if (billExchange.Result.IsNotNullOrEmpty())
                        {
                            claimData.Data = billExchange.Result;
                            claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                            claimData.ClaimType = isHMO ? ClaimType.HMO.ToString() : ClaimType.CMS.ToString();
                            billingRepository.UpdateClaimData(claimData);
                            if (commandType == ClaimCommandType.direct)
                            {
                                if (finalClaims != null && finalClaims.Count > 0)
                                {
                                    billingRepository.MarkFinalsAsSubmitted(Current.AgencyId, finalClaims);
                                    Auditor.AddGeneralMulitLog(LogDomain.Patient, finalClaims.GroupBy(final => final.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.Final, LogAction.FinalSubmittedElectronically, string.Empty);
                                }
                            }
                            else if (commandType == ClaimCommandType.download)
                            {
                                if (finalClaims != null && finalClaims.Count > 0)
                                {
                                    billingRepository.MarkFinalsAsGenerated(Current.AgencyId, finalClaims);
                                    Auditor.AddGeneralMulitLog(LogDomain.Patient, finalClaims.GroupBy(final => final.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.Final, LogAction.FinalGenerated, string.Empty);
                                }
                            }
                            claimDataOut = claimData;
                            billExchange.isSuccessful = true;
                        }
                        else
                        {
                            billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                            billingRepository.DeleteFinaSnapShots(claimData.Id);
                            claimDataOut = null;
                            billExchange.isSuccessful = false;
                        }
                    }
                    else
                    {
                        billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                        billingRepository.DeleteFinaSnapShots(claimData.Id);
                        billExchange = billExchange ?? new BillExchange { Message = "Error in generating the calim." };
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                    billingRepository.DeleteFinaSnapShots(claimData.Id);
                }
            }
            else
            {
                billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                billExchange.Message = "There is a problem generating the claim data. Try Again.";
                billExchange.isSuccessful = false;
            }
            return billExchange;
        }

        //public object GetVisitRateInstance(string disciplineName, string codeOne, string codeTwo, string amount, int unit)
        //{
        //    switch (disciplineName)
        //    {
        //        case "SN":
        //            return new { SN = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "SNM":
        //            return new { SNM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "SNO":
        //            return new { SNO = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "SNT":
        //            return new { SNT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "HHA":
        //            return new { HHA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "PT":
        //            return new { PT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "PTA":
        //            return new { PTA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "PTM":
        //            return new { PTM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "OT":
        //            return new { OT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "OTA":
        //            return new { OTA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "OTM":
        //            return new { OTM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "ST":
        //            return new { ST = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "STM":
        //            return new { STM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //        case "MSW":
        //            return new { MSW = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
        //    }
        //    return null;
        //}

        public JsonViewData MarkRapSubmitted(List<Guid> rapToGenerate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) were unsuccessful in updating their status." };
            if (rapToGenerate.IsNotNullOrEmpty())
            {
                var raps = billingRepository.GetRapsToGenerateByIdsLean(Current.AgencyId, rapToGenerate);
                if (raps.IsNotNullOrEmpty())
                {
                    var count = billingRepository.MarkRapsAsSubmitted(Current.AgencyId, raps);
                    if (count > 0)
                    {
                        Auditor.AddGeneralMulitLog(LogDomain.Patient, raps.GroupBy(rap => rap.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.Rap, LogAction.RAPMarkedAsSubmitted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = count == rapToGenerate.Count ? "All of the claim(s) were successful in updating their status." : string.Format("{0} of {1} claim(s) were successful in updating their status.", count, rapToGenerate.Count);
                    }
                }
                //int successCounter = 0;
                //rapToGenerate.ForEach(r =>
                //{
                //    var rap = billingRepository.GetRapOnly(Current.AgencyId, r);
                //    if (rap != null)
                //    {
                //        var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, rap.PatientId);
                //        if (profile != null)
                //        {
                //            var oldStatus = rap.Status;
                //            if (statusType == "Submit")
                //            {
                //                if (rap.Status != (int)BillingStatus.ClaimSubmitted)
                //                {
                //                    rap.Status = (int)BillingStatus.ClaimSubmitted;
                //                    rap.ClaimDate = DateTime.Now;
                //                }
                //                //rap.IsGenerated = true;
                //                //rap.IsVerified = true;
                //            }
                //            else if (statusType == "Cancelled")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimCancelledClaim;
                //            }
                //            else if (statusType == "Rejected")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimRejected;
                //            }
                //            else if (statusType == "Accepted")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimAccepted;
                //                rap.IsGenerated = true;
                //            }
                //            else if (statusType == "PaymentPending")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimPaymentPending;
                //                rap.IsGenerated = true;
                //            }
                //            else if (statusType == "Error")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimWithErrors;
                //            }
                //            else if (statusType == "Paid")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimPaidClaim;
                //                // rap.IsGenerated = true;
                //            }
                //            else if (statusType == "ReOpen")
                //            {
                //                rap.Status = (int)BillingStatus.ClaimReOpen;
                //                rap.ClaimDate = DateTime.MinValue;
                //                rap.IsVerified = false;
                //                rap.IsGenerated = false;
                //            }
                //            var isStatusChange = oldStatus == rap.Status;
                //            if (!isStatusChange && (oldStatus == (int)BillingStatus.ClaimCreated || oldStatus == (int)BillingStatus.ClaimReOpen))
                //            {
                //                rap.Insurance = SerializedInsuranceForClaim<Rap>(rap, profile.AgencyLocationId, rap.PrimaryInsuranceId, claim => ((claim.IsVerified && claim.Insurance.IsNullOrEmpty()) || (!claim.IsVerified))) ?? rap.Insurance;
                //            }
                //            if (billingRepository.UpdateRapStatus(rap))
                //            {
                //                var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                //                if (final != null)
                //                {
                //                    final.IsRapGenerated = rap.IsGenerated;
                //                    billingRepository.UpdateFinalStatus(final);
                //                }
                //                successCounter++;
                //                if (!isStatusChange)
                //                {
                //                    Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPUpdatedWithStatus, ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), rap.Status) ? (" To " + ((BillingStatus)rap.Status).GetDescription()) : string.Empty)));
                //                }
                //            }
                //        }
                //    }
                //});
                //if (successCounter > 0)
                //{
                //    viewData.isSuccessful = true;
                //    viewData.errorMessage = successCounter == rapToGenerate.Count ? "All of the claim(s) were successful in updating their status." : string.Format("{0} of {1} claim(s) were successful in updating their status.", successCounter, rapToGenerate.Count);
                //}
            }
            return viewData;
        }

        public JsonViewData MarkFinalSubmitted(List<Guid> finalToGenerate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) were unsuccessful in updating their status." };
            if (finalToGenerate.IsNotNullOrEmpty())
            {
                var finals = billingRepository.GetFinalsToGenerateByIds(Current.AgencyId, finalToGenerate);
                if (finals.IsNotNullOrEmpty())
                {
                    var count = billingRepository.MarkFinalsAsSubmitted(Current.AgencyId, finals);
                    if (count > 0)
                    {
                        Auditor.AddGeneralMulitLog(LogDomain.Patient, finals.GroupBy(rap => rap.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.Final, LogAction.FinalMarkedAsSubmitted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = count == finalToGenerate.Count ? "All of the claim(s) were successful in updating their status." : string.Format("{0} of {1} claim(s) were successful in updating their status.", count, finalToGenerate.Count);

                    }
                }
                //int successCounter = 0;
                //finalToGenerate.ForEach(f =>
                //{
                //    var final = billingRepository.GetFinal(Current.AgencyId, f);
                //    if (final != null)
                //    {
                //        var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, final.PatientId);
                //        if (profile != null)
                //        {
                //            var oldStatus = final.Status;
                //            if (statusType == "Submit")
                //            {
                //                if (final.Status != (int)BillingStatus.ClaimSubmitted)
                //                {
                //                    final.Status = (int)BillingStatus.ClaimSubmitted;
                //                    final.ClaimDate = DateTime.Now;
                //                }
                //                //final.IsGenerated = true;
                //                //final.IsFinalInfoVerified = true;
                //                //final.IsVisitVerified = true;
                //                //final.IsSupplyVerified = true;
                //            }
                //            else if (statusType == "Cancelled")
                //            {
                //                final.Status = (int)BillingStatus.ClaimCancelledClaim;
                //            }
                //            else if (statusType == "Rejected")
                //            {
                //                final.Status = (int)BillingStatus.ClaimRejected;
                //            }
                //            else if (statusType == "Accepted")
                //            {
                //                final.Status = (int)BillingStatus.ClaimAccepted;
                //            }
                //            else if (statusType == "PaymentPending")
                //            {
                //                final.Status = (int)BillingStatus.ClaimPaymentPending;
                //            }
                //            else if (statusType == "Error")
                //            {
                //                final.Status = (int)BillingStatus.ClaimWithErrors;
                //            }
                //            else if (statusType == "Paid")
                //            {
                //                final.Status = (int)BillingStatus.ClaimPaidClaim;
                //            }
                //            else if (statusType == "ReOpen")
                //            {
                //                final.Status = (int)BillingStatus.ClaimReOpen;
                //                final.ClaimDate = DateTime.MinValue;
                //                final.IsFinalInfoVerified = false;
                //                final.IsSupplyVerified = false;
                //                final.IsVisitVerified = false;
                //                final.IsGenerated = false;
                //            }
                //            var isStatusChange = oldStatus == final.Status;
                //            if (!isStatusChange && (oldStatus == (int)BillingStatus.ClaimCreated || oldStatus == (int)BillingStatus.ClaimReOpen))
                //            {
                //                final.Insurance = SerializedInsuranceForClaim<Final>(final, profile.AgencyLocationId, final.PrimaryInsuranceId, claim => ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))) ?? final.Insurance;
                //            }
                //            if (billingRepository.UpdateFinalStatus(final))
                //            {
                //                successCounter++;
                //                if (!isStatusChange)
                //                {
                //                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalUpdatedWithStatus, ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), final.Status) ? (" To " + ((BillingStatus)final.Status).GetDescription()) : string.Empty)));
                //                }
                //            }
                //        }
                //    }
                //});
                //if (successCounter > 0)
                //{
                //    viewData.isSuccessful = true;
                //    viewData.errorMessage = successCounter == finalToGenerate.Count ? "All of the claim(s) were successful in updating their status." : string.Format("{0} of {1} claim(s) were successful in updating their status.", successCounter, finalToGenerate.Count);
                //}
            }
            return viewData;
        }

        public List<RapSnapShot> GetRapSnapShotsByClaimId(Guid Id)
        {
            return billingRepository.GetRapSnapShots(Current.AgencyId, Id);
        }

        public List<FinalSnapShot> GetFinalSnapShotsByClaimId(Guid Id)
        {
            return billingRepository.GetFinalSnapShots(Current.AgencyId, Id);
        }

        public bool UpdateSnapShot(Guid Id, long batchId, double paymentAmount, DateTime paymentDate, int status, string type)
        {
            bool result = false;
            if (IsRap(type))
            {
                var rapSnapShot = billingRepository.GetRapSnapShot(Current.AgencyId, Id, batchId);
                if (rapSnapShot != null)
                {
                    rapSnapShot.Payment = paymentAmount;
                    rapSnapShot.PaymentDate = paymentDate;
                    rapSnapShot.Status = status;
                    result = billingRepository.UpdateRapSnapShots(rapSnapShot);
                    if (result && batchId == billingRepository.GetLastRapSnapShotBatchId(Current.AgencyId, Id))
                    {
                        UpdateRapClaimStatus(Id, paymentDate, paymentAmount, status);
                    }
                }
            }
            else if (IsFinal(type))
            {
                var finalSnapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, Id, batchId);
                if (finalSnapShot != null)
                {
                    finalSnapShot.Payment = paymentAmount;
                    finalSnapShot.PaymentDate = paymentDate;
                    finalSnapShot.Status = status;
                    result = billingRepository.UpdateFinalSnapShots(finalSnapShot);
                    if (result && batchId == billingRepository.GetLastFinalSnapShotBatchId(Current.AgencyId, Id))
                    {
                        UpdateFinalClaimStatus(Id, paymentDate, paymentAmount, status);
                    }
                }
            }
            return result;
        }

        public ClaimSnapShotViewData GetClaimSnapShotForEdit(Guid id, long batchId, string type)
        {
            ClaimSnapShotViewData viewData = new ClaimSnapShotViewData();
            if (IsRap(type))
            {
                var snapShot = billingRepository.GetRapSnapShot(Current.AgencyId, id, batchId);
                if (snapShot != null)
                {
                    viewData.Id = snapShot.Id;
                    viewData.PatientId = snapShot.PatientId;
                    viewData.PaymentDate = snapShot.PaymentDate;
                    viewData.PaymentAmount = snapShot.Payment;
                    viewData.Status = snapShot.Status;
                    viewData.Type = "RAP";
                }
            }
            else if (IsFinal(type))
            {
                var snapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, id, batchId);
                if (snapShot != null)
                {
                    viewData.Id = snapShot.Id;
                    viewData.PatientId = snapShot.PatientId;
                    viewData.PaymentDate = snapShot.PaymentDate;
                    viewData.PaymentAmount = snapShot.Payment;
                    viewData.Status = snapShot.Status;
                    viewData.Type = "FINAL";
                }
            }
            return viewData;
        }

        public IList<ClaimSnapShotViewData> ClaimSnapShots(Guid Id, string type)
        {
            var claims = new List<ClaimSnapShotViewData>();
            if (IsRap(type))
            {
                var rapSnapShots = billingRepository.GetRapSnapShots(Current.AgencyId, Id);
                if (rapSnapShots != null && rapSnapShots.Count > 0)
                {
                    var isUserCanEdit = Current.HasRight(this.Service, ParentPermission.MedicareClaim, PermissionActions.Edit);
                    rapSnapShots.ForEach(rapSnapShot => claims.Add(
                        new ClaimSnapShotViewData
                        {
                            Id = rapSnapShot.Id,
                            BatchId = rapSnapShot.BatchId,
                            PatientId = rapSnapShot.PatientId,
                            EpisodeId = rapSnapShot.EpisodeId,
                            FirstName = rapSnapShot.FirstName,
                            LastName = rapSnapShot.LastName,
                            MedicareNumber = rapSnapShot.MedicareNumber,
                            PaymentDate = rapSnapShot.PaymentDate,
                            PaymentAmount = rapSnapShot.Payment,
                            Type = "Rap",
                            Created = rapSnapShot.Created,
                            Status = rapSnapShot.Status,
                            EpisodeStartDate = rapSnapShot.EpisodeStartDate,
                            EpisodeEndDate = rapSnapShot.EpisodeEndDate,
                            ClaimDate = rapSnapShot.ClaimDate.IsValid() ? rapSnapShot.ClaimDate.ToString("MM/dd/yyyy hh:mm tt") : rapSnapShot.EpisodeStartDate.ToString("MM/dd/yyyy hh:mm tt"),
                            ClaimAmount = rapSnapShot.ProspectivePay,
                            IsUserCanEdit = isUserCanEdit
                        }));
                }
            }
            else if (IsFinal(type))
            {
                var finalSnapShots = billingRepository.GetFinalSnapShots(Current.AgencyId, Id);
                if (finalSnapShots != null && finalSnapShots.Count > 0)
                {
                    var isUserCanEdit = Current.HasRight(this.Service, ParentPermission.MedicareClaim, PermissionActions.Edit);
                    finalSnapShots.ForEach(finalSnapShot => claims.Add(
                        new ClaimSnapShotViewData
                        {
                            PatientId = finalSnapShot.PatientId,
                            BatchId = finalSnapShot.BatchId,
                            Id = finalSnapShot.Id,
                            EpisodeId = finalSnapShot.EpisodeId,
                            Type = "Final",
                            FirstName = finalSnapShot.FirstName,
                            LastName = finalSnapShot.LastName,
                            MedicareNumber = finalSnapShot.MedicareNumber,
                            PaymentDate = finalSnapShot.PaymentDate,
                            Created = finalSnapShot.Created,
                            Status = finalSnapShot.Status,
                            EpisodeStartDate = finalSnapShot.EpisodeStartDate,
                            EpisodeEndDate = finalSnapShot.EpisodeEndDate,
                            PaymentAmount = finalSnapShot.Payment,
                            ClaimDate = finalSnapShot.ClaimDate.IsValid() ? finalSnapShot.ClaimDate.ToString("MM/dd/yyyy hh:mm tt") : finalSnapShot.EpisodeEndDate.ToString("{MM/dd/yyyy hh:mm tt"),
                            ClaimAmount = finalSnapShot.ProspectivePay,
                            IsUserCanEdit = isUserCanEdit
                        }));
                }
            }
            return claims;
        }

        public ClaimInfoSnapShotViewData GetClaimSnapShotInfo(Guid patientId, Guid claimId, ClaimTypeSubCategory type)
        {
            var claimInfo = new ClaimInfoSnapShotViewData();
            var patientWithProfile = patientProfileRepository.GetPatientWithProfileForClaimLean(Current.AgencyId, patientId);
            if (patientWithProfile != null && patientWithProfile.Profile != null)
            {
                var allPermission = Current.CategoryService(this.Service, ParentPermission.MedicareClaim, new int[] { (int)PermissionActions.Add, (int)PermissionActions.ViewDetail, (int)PermissionActions.ViewLog, (int)PermissionActions.ViewList });
                if (allPermission.IsNotNullOrEmpty())
                {
                    claimInfo.IsUserCanAdd = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None).Has(this.Service);
                    claimInfo.IsUserCanViewList = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None).Has(this.Service);
                    claimInfo.IsUserCanViewLog = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewLog, AgencyServices.None).Has(this.Service);
                    claimInfo.IsUserCanViewRemittance = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewDetail, AgencyServices.None).Has(this.Service);
                }
                if (type == ClaimTypeSubCategory.RAP)
                {
                    var rap = billingRepository.GetRapOnly(Current.AgencyId, claimId);
                    if (rap != null)
                    {
                        rap.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;
                        claimInfo.Status = rap.Status;
                        claimInfo.Id = rap.Id;
                        claimInfo.EpisodeId = rap.EpisodeId;
                        claimInfo.PatientId = rap.PatientId;
                        claimInfo.Type = "RAP";
                        if (rap.Status == (int)BillingStatus.ClaimPaidClaim || rap.Status == (int)BillingStatus.ClaimPaymentPending || rap.Status == (int)BillingStatus.ClaimSubmitted || rap.Status == (int)BillingStatus.ClaimAccepted || (rap.Status <= 27 && rap.Status >= 1))
                        {
                            claimInfo.PatientName = string.Format("{0} {1}", rap.FirstName, rap.LastName);
                            claimInfo.PatientIdNumber = rap.PatientIdNumber;
                            claimInfo.MedicareNumber = rap.MedicareNumber;
                            claimInfo.HIPPS = rap.HippsCode;
                            claimInfo.ClaimKey = rap.ClaimKey;
                            claimInfo.ProspectivePayment = new ProspectivePayment();

                            if (rap.HippsCode.IsNotNullOrEmpty() && rap.HippsCode.Length == 5)
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                                if (prospectivePayment != null)
                                {
                                    var claimAmount = rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                                    prospectivePayment.ClaimAmount = "$" + claimAmount;
                                    claimInfo.ProspectivePayment = prospectivePayment;
                                }
                            }
                            if (rap.IsVerified)
                            {
                                claimInfo.Visible = true;
                            }
                        }
                        else
                        {
                            claimInfo.PatientName = string.Format("{0} {1}", patientWithProfile.FirstName, patientWithProfile.LastName);
                            claimInfo.PatientIdNumber = patientWithProfile.PatientIdNumber;
                            claimInfo.MedicareNumber = patientWithProfile.MedicareNumber;
                            claimInfo.Visible = false;
                        }
                        claimInfo.PayorName = InsuranceEngine.GetName(rap.PrimaryInsuranceId, Current.AgencyId);
                        //var agencyInsurance = ClaimToInsuranceForNameAndAddress<Rap>(rap,
                        //    rap.AgencyLocationId,
                        //    rap.Insurance,
                        //    rap.PrimaryInsuranceId,
                        //    (claim) => (claim.IsVerified && claim.Insurance.IsNotNullOrEmpty() || ((claim.Status != (int)BillingStatus.ClaimCreated && claim.Status != (int)BillingStatus.ClaimReOpen))), false);

                        ////var agencyInsurance = this.RapToInsurance(rap);
                        //if (agencyInsurance != null)
                        //{
                        //    claimInfo.PayorName = agencyInsurance.Name;
                        //}
                    }
                }
                else if (type == ClaimTypeSubCategory.Final)
                {
                    var final = billingRepository.GetFinalOnly(Current.AgencyId, claimId);
                    if (final != null)
                    {
                        final.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;
                        //final.BranchId = patientWithProfile.Profile.AgencyLocationId;
                        claimInfo.Id = final.Id;
                        claimInfo.PatientId = final.PatientId;
                        claimInfo.EpisodeId = final.EpisodeId;
                        claimInfo.Type = "Final";
                        if (final.Status == (int)BillingStatus.ClaimPaidClaim || final.Status == (int)BillingStatus.ClaimPaymentPending || final.Status == (int)BillingStatus.ClaimSubmitted || final.Status == (int)BillingStatus.ClaimAccepted || (final.Status <= 27 && final.Status >= 1))
                        {
                            claimInfo.PatientName = string.Format("{0} {1}", final.FirstName, final.LastName);
                            claimInfo.PatientIdNumber = final.PatientIdNumber;
                            claimInfo.MedicareNumber = final.MedicareNumber;
                            if (final.Status == (int)BillingStatus.ClaimPaidClaim || final.Status == (int)BillingStatus.ClaimSubmitted)
                            {
                                claimInfo.IsCreateSecondaryClaimButtonVisible = true;
                            }
                            claimInfo.HIPPS = final.HippsCode;
                            claimInfo.ClaimKey = final.ClaimKey;

                            if (final.HippsCode.IsNotNullOrEmpty() && final.HippsCode.Length == 5)
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                var prospectivePayment = lookUpRepository.GetProspectivePayment(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                                if (prospectivePayment != null)
                                {
                                    var claimAmount = (final.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || final.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || final.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString()) ? prospectivePayment.CalculateFinalClaimAmount(true).ToString() : prospectivePayment.CalculateFinalClaimAmount(false).ToString();
                                    prospectivePayment.ClaimAmount = "$" + claimAmount;
                                    claimInfo.ProspectivePayment = prospectivePayment;
                                    claimInfo.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            claimInfo.PatientName = string.Format("{0} {1}", patientWithProfile.FirstName, patientWithProfile.LastName);
                            claimInfo.PatientIdNumber = patientWithProfile.PatientIdNumber;
                            claimInfo.MedicareNumber = patientWithProfile.MedicareNumber;
                            claimInfo.Visible = false;
                        }
                        claimInfo.PayorName = InsuranceEngine.GetName(final.PrimaryInsuranceId, Current.AgencyId);
                        //var agencyInsurance = ClaimToInsuranceForNameAndAddress<Final>(final,
                        //  final.AgencyLocationId,
                        //  final.Insurance,
                        //  final.PrimaryInsuranceId,
                        //  (claim) => (claim.IsFinalInfoVerified && claim.Insurance.IsNotNullOrEmpty() || ((claim.Status != (int)BillingStatus.ClaimCreated && claim.Status != (int)BillingStatus.ClaimReOpen) && claim.Insurance.IsNotNullOrEmpty())), false);

                        ////var agencyInsurance = this.FinalToInsurance(final);
                        //if (agencyInsurance != null)
                        //{
                        //    claimInfo.PayorName = agencyInsurance.Name;
                        //}
                    }
                }
            }
            return claimInfo;
        }

        public ClaimViewData GetClaimViewData(Guid patientId, Guid Id, ClaimTypeSubCategory type)
        {
            var claimData = new ClaimViewData();
            if (ClaimTypeSubCategory.RAP == type)
            {
                var rap = billingRepository.GetRapOnly(Current.AgencyId, Id);
                if (rap != null)
                {
                    claimData.Id = rap.Id;
                    claimData.PatientId = rap.PatientId;
                    claimData.EpisodeId = rap.EpisodeId;
                    claimData.FirstName = rap.FirstName;
                    claimData.LastName = rap.LastName;
                    claimData.MedicareNumber = rap.MedicareNumber;
                    claimData.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    claimData.PaymentDate = rap.PaymentDate;
                    claimData.PaymentAmount = rap.Payment;
                    claimData.Type = "RAP";
                    claimData.Created = rap.Created;
                    claimData.Status = rap.Status;
                    claimData.EpisodeStartDate = rap.EpisodeStartDate;
                    claimData.EpisodeEndDate = rap.EpisodeEndDate;
                    claimData.ClaimDate = rap.ClaimDate.IsValid() ? rap.ClaimDate.ToString("MM/dd/yyyy") : rap.EpisodeStartDate.ToString("MM/dd/yyyy");
                    claimData.ClaimAmount = rap.ProspectivePay;
                    claimData.Comment = rap.Comment;
                }
            }
            else if (ClaimTypeSubCategory.Final == type)
            {
                var final = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                if (final != null)
                {
                    claimData.PatientId = final.PatientId;
                    claimData.Id = final.Id;
                    claimData.EpisodeId = final.EpisodeId;
                    claimData.Type = "Final";
                    claimData.FirstName = final.FirstName;
                    claimData.LastName = final.LastName;
                    claimData.MedicareNumber = final.MedicareNumber;
                    claimData.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    claimData.PaymentDate = final.PaymentDate;
                    claimData.Created = final.Created;
                    claimData.Status = final.Status;
                    claimData.EpisodeStartDate = final.EpisodeStartDate;
                    claimData.EpisodeEndDate = final.EpisodeEndDate;
                    claimData.PaymentAmount = final.Payment;
                    claimData.ClaimDate = final.ClaimDate.IsValid() ? final.ClaimDate.ToString("MM/dd/yyyy") : final.EpisodeEndDate.ToString("MM/dd/yyyy");
                    claimData.ClaimAmount = final.ProspectivePay;
                    claimData.Comment = final.Comment;
                    claimData.Supply = final.Supply;
                    claimData.SupplyTotal = final.SupplyTotal;
                }
            }
            return claimData;
        }

        protected override Bill ClaimToGenerateAppSpecific(List<Guid> claimSelected, Guid branchId, int primaryInsurance, string type)
        {
            var bill = new Bill();
            bill.Service = this.Service;
            bill.BranchId = branchId;
            bill.Insurance = primaryInsurance;
            if (type.IsNotNullOrEmpty())
            {
                var isElectronicSubmssion = false;
                if (MedicareIntermediaryFactory.Intermediaries().Contains(primaryInsurance))
                {
                    var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                    if (location != null)
                    {
                        bill.BranchName = location.Name;
                        if (location.IsLocationStandAlone)
                        {
                            isElectronicSubmssion = location.IsAxxessTheBiller;
                        }
                        else
                        {
                            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                            if (agency != null)
                            {
                                isElectronicSubmssion = agency.IsAxxessTheBiller;
                            }
                        }
                    }
                    bill.InsuranceName = ((MedicareIntermediary)primaryInsurance).GetDescription();
                }
                else if (primaryInsurance >= 1000)
                {
                    bill.BranchName = LocationEngine.GetName(branchId, Current.AgencyId);
                    var insurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
                    if (insurance != null)
                    {
                        bill.InsuranceName = insurance.Name;
                        isElectronicSubmssion = insurance.IsAxxessTheBiller;
                    }
                }

                bill.IsElectronicSubmssion = isElectronicSubmssion;
                if (IsRap(type))
                {
                    bill.ClaimType = ClaimTypeSubCategory.RAP;
                    bill.Claims = billingRepository.GetRapsByIds(Current.AgencyId, claimSelected);
                }
                else if (IsFinal(type))
                {
                    bill.ClaimType = ClaimTypeSubCategory.Final;
                    bill.Claims = billingRepository.GetFinalsByIds(Current.AgencyId, claimSelected);
                }
            }
            return bill;
        }

        public double MedicareSupplyTotal(Final final)
        {
            if (final != null)
            {
                if (final.Supply.IsNotNullOrEmpty())
                {
                    final.SupplyTotal = final.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && !s.IsDeprecated).Sum(s => s.TotalCost);
                }
                if (final.SupplyTotal <= 0)
                {
                    if (final.HippsCode.IsNotNullOrEmpty() && final.HippsCode.Length == 5)
                    {
                        final.SupplyTotal = this.GetSupplyReimbursement(final.HippsCode[4], final.EpisodeStartDate.Year);
                    }
                }
                return final.SupplyTotal;
            }
            return 0;
        }


        public Bill GetMedicareClaimCenterData(ClaimTypeSubCategory claimType)
        {
            var bill = new Bill();
            var isViewPermission = false;
            var isEditPermission = false;
            var isPrintPermission = false;
            var isExportPermission = false;
            var isGeneratePermission = false;
            var allPermission = Current.CategoryService(this.Service, ParentPermission.MedicareClaim, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Print, (int)PermissionActions.Export, (int)PermissionActions.GenerateElectronicClaim, (int)PermissionActions.ViewOutstandingClaim });
            if (allPermission.IsNotNullOrEmpty())
            {
                isViewPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewOutstandingClaim, AgencyServices.None) != AgencyServices.None;
                if (isViewPermission)
                {
                    isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                    isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                    isExportPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None) != AgencyServices.None;
                    isGeneratePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.GenerateElectronicClaim, AgencyServices.None) != AgencyServices.None;
                }
            }
            bill.IsUserCanView = isViewPermission;
            bill.IsUserCanEdit = isEditPermission;
            bill.IsUserCanExport = isExportPermission;
            bill.IsUserCanPrint = isPrintPermission;
            bill.IsUserCanGenerate = isGeneratePermission;

            bill.ClaimType = claimType;
            bill.Service = this.Service;
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
                    {
                        bill.IsMedicareHMO = false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.BranchName = agencyMainBranch.Name;
                        bill.Insurance = payorType;
                        bill.InsuranceName = Enum.IsDefined(typeof(MedicareIntermediary), payorType) ? ((MedicareIntermediary)payorType).GetDescription() : string.Empty;
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == (int)PayerTypes.MedicareTraditional || i.PayorType == (int)PayerTypes.MedicareHMO).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == (int)PayerTypes.MedicareHMO ? true : false;
                            bill.BranchId = agencyMainBranch.Id;
                            bill.BranchName = agencyMainBranch.Name;
                            bill.Insurance = agencyMedicareInsurance.Id;
                            bill.InsuranceName = agencyMedicareInsurance.Name;
                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == (int)PayerTypes.MedicareTraditional || i.PayorType == (int)PayerTypes.MedicareHMO).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == (int)PayerTypes.MedicareHMO ? true : false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.BranchName = agencyMainBranch.Name;
                        bill.Insurance = agencyMedicareInsurance.Id;
                        bill.InsuranceName = agencyMedicareInsurance.Name;
                    }
                }
                bill.Claims = GetUnProcessed(claimType, bill.BranchId, bill.Insurance, false, false);
            }
            return bill;
        }

        public IList<ClaimHistoryLean> Activity(Guid patientId, int insuranceId)
        {
            var claims = new List<ClaimHistoryLean>();

            var raps = billingRepository.GetRapsHistory(Current.AgencyId, patientId, insuranceId);
            var isRapExist = raps != null && raps.Count > 0;

            var finals = billingRepository.GetFinalsHistory(Current.AgencyId, patientId, insuranceId);
            var isFinalExist = finals != null && finals.Count > 0;
            if (isRapExist || isFinalExist)
            {
                var isEditPermission = false;
                var isPrintPermission = false;
                var isDeletePermission = false;
                var allPermission = Current.CategoryService(this.Service, ParentPermission.MedicareClaim, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Print, (int)PermissionActions.Delete });
                if (allPermission.IsNotNullOrEmpty())
                {
                    isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                    isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                    isDeletePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                }
                if (isRapExist)
                {
                    claims.AddRange(raps);
                }
                if (isFinalExist)
                {
                    claims.AddRange(finals);
                }
                if (claims.Count > 0)
                {
                    AgencyInsurance insurance = null;
                    if (insuranceId > 1000)
                    {
                        insurance = agencyRepository.FindInsurance(Current.AgencyId, insuranceId);
                    }
                    claims.ForEach(claim =>
                    {
                        claim.InvoiceType = insurance == null ? (int)InvoiceType.UB : insurance.InvoiceType;
                        claim.IsUserCanEdit = isEditPermission;
                        claim.IsUserCanDelete = isDeletePermission;
                        claim.IsUserCanPrint = isPrintPermission;
                    });
                }
            }
            return claims;
        }

        public IList<PendingClaimLean> PendingClaimRaps(Guid branchId, string primaryInsurance)
        {
            var claims = new List<PendingClaimLean>();
            if (primaryInsurance.IsInteger())
            {
                // var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var claimsToAddRap = billingRepository.PendingClaimRaps(Current.AgencyId, branchId, primaryInsurance.ToInteger());
                if (claimsToAddRap != null && claimsToAddRap.Count > 0)
                {
                    // claimsToAddRap.ForEach(c => { c.ClaimAmount = c.AssessmentType.IsNotNullOrEmpty() && (c.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString()) ? 0.6 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(claimsToAddRap);
                }
            }
            return claims;
        }

        public IList<PendingClaimLean> PendingClaimFinals(Guid branchId, string primaryInsurance)
        {
            var claims = new List<PendingClaimLean>();
            if (primaryInsurance.IsInteger())
            {
                // var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var claimsToAddFinal = billingRepository.PendingClaimFinals(Current.AgencyId, branchId, primaryInsurance.ToInteger());
                if (claimsToAddFinal != null && claimsToAddFinal.Count > 0)
                {
                    //claimsToAddFinal.ForEach(c => { c.ClaimAmount = Math.Round(lookUpRepository.GetProspectivePaymentAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(claimsToAddFinal);
                }
            }
            return claims;
        }

        public IList<ClaimLean> AccountsReceivables(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var claimsToAddRap = this.AccountsReceivableRaps(branchId, insurance, startDate, endDate);
            if (claimsToAddRap != null && claimsToAddRap.Count > 0)
            {
                claims.AddRange(claimsToAddRap);
            }
            var claimsToAddFinal = AccountsReceivableFinals(branchId, insurance, startDate, endDate);
            if (claimsToAddFinal != null && claimsToAddFinal.Count > 0)
            {
                claims.AddRange(claimsToAddFinal);
            }

            return claims.OrderBy(c => c.ClaimDate.ToShortDateString().ToZeroFilled()).ToList();
        }
        /// <summary>
        /// rc = raps table representation
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="insurance"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>

        public IList<ClaimLean> AccountsReceivableRaps(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var isReturnEmpty = false;
            var insuranceFilter = GetInsuranceFilter(branchId, insurance, "rc", out isReturnEmpty);
            if (isReturnEmpty)
            {
                return claims;
            }
            else
            {
                var claimsToAdd = billingRepository.GetAccountsReceivableRaps(Current.AgencyId, branchId, insurance, startDate, endDate, insuranceFilter);
                if (claimsToAdd != null && claimsToAdd.Count > 0)
                {
                    claims.AddRange(claimsToAdd);
                }
                return claims.OrderBy(c => c.ClaimDate.ToShortDateString().ToZeroFilled()).ToList();
            }
        }
        /// <summary>
        /// fc = finals table representation
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="insurance"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>

        public IList<ClaimLean> AccountsReceivableFinals(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var isReturnEmpty = false;
            var insuranceFilter = GetInsuranceFilter(branchId, insurance, "fc", out isReturnEmpty);
            if (isReturnEmpty)
            {
                return claims;
            }
            else
            {
                var claimsToAdd = billingRepository.GetAccountsReceivableFinals(Current.AgencyId, branchId, insurance, startDate, endDate, insuranceFilter);
                if (claimsToAdd != null && claimsToAdd.Count > 0)
                {
                    claims.AddRange(claimsToAdd);
                }
                return claims.OrderBy(c => c.ClaimDate.ToShortDateString().ToZeroFilled()).ToList();
            }
        }
        /// <summary>
        /// rc = raps table representation
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="insurance"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>

        public IList<Revenue> GetUnearnedRevenue(Guid branchId, int insurance, DateTime endDate)
        {
            var result = new List<Revenue>();
            var isReturnEmpty = false;
            var insuranceFilter = GetInsuranceFilter(branchId, insurance, "rc", out isReturnEmpty);
            if (isReturnEmpty)
            {
                return result;
            }
            else
            {
                var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, new List<int> { (int)BillingStatus.ClaimSubmitted, (int)BillingStatus.ClaimPaidClaim, (int)BillingStatus.ClaimPaymentPending, (int)BillingStatus.ClaimAccepted }, endDate, insuranceFilter, true);
                if (revenueList != null && revenueList.Count > 0)
                {
                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                    //var tasks = scheduleRepository.GetScheduleByEpisodes(Current.AgencyId, revenueList.Select(r => r.EpisodeId).Distinct().ToList(), endDate);
                    revenueList.ForEach(rap =>
                    {
                        var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                        if (prospectivePayment != null)
                        {
                            rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                            rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                            rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                            //var episodeTasks = tasks.Where(t => t.EpisodeId == rap.EpisodeId).ToList();
                            //if (episodeTasks.Count > 0)
                            //{
                            //    rap.BillableVisitCount = episodeTasks.Count;
                            //    rap.CompletedVisitCount = episodeTasks.Count(v => ScheduleStatusFactory.OnAndAfterQAStatus(true).Contains(v.Status) && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date);
                            //}
                            if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount >= 0)
                            {
                                rap.RapVisitCount = (int)Math.Ceiling(0.5 * rap.BillableVisitCount);
                                if (rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString())
                                {
                                    rap.RapVisitCount = (int)Math.Ceiling(0.6 * rap.BillableVisitCount);
                                }
                                if (rap.CompletedVisitCount <= rap.RapVisitCount && rap.CompletedVisitCount <= rap.BillableVisitCount)
                                {
                                    var unearnedVisits = rap.RapVisitCount - rap.CompletedVisitCount;
                                    var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                                    rap.UnearnedRevenueAmount = Math.Round((unearnedVisits) * unitAmount, 2);
                                    rap.UnearnedVisitCount = unearnedVisits;
                                    rap.UnitAmount = unitAmount;
                                }
                            }
                            result.Add(rap);
                        }
                    });
                }
                return result;
            }
        }

        public IList<Revenue> GetUnbilledRevenue(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, new List<int> { (int)BillingStatus.ClaimSubmitted, (int)BillingStatus.ClaimPaidClaim, (int)BillingStatus.ClaimPaymentPending, (int)BillingStatus.ClaimAccepted }, startDate, endDate, true);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                revenueList.ForEach(rap =>
                {
                    var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                        //if (rap.Schedule.IsNotNullOrEmpty())
                        //{
                        //    rap.BillableVisitCount = rap.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && !v.IsMissedVisit && v.IsDeprecated == false).ToList().Count;
                        //    rap.CompletedVisitCount = rap.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && (v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235) && v.IsDeprecated == false && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date).ToList().Count;
                        //}
                        //rap.Schedule = string.Empty;

                        if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount >= 0)
                        {
                            int rapVisits = (int)Math.Ceiling(0.5 * rap.BillableVisitCount);
                            if (rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString())
                            {
                                rapVisits = (int)Math.Ceiling(0.6 * rap.BillableVisitCount);
                            }
                            if (rap.CompletedVisitCount >= rapVisits && rap.CompletedVisitCount <= rap.BillableVisitCount)
                            {
                                var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                                rap.UnbilledRevenueAmount = Math.Round((rap.CompletedVisitCount - rapVisits) * unitAmount, 2);
                                rap.UnbilledVisitCount = rap.CompletedVisitCount - rapVisits;
                                rap.UnitAmount = unitAmount;
                            }
                        }
                        result.Add(rap);
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetEarnedRevenue(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, new List<int> { (int)BillingStatus.ClaimSubmitted, (int)BillingStatus.ClaimPaidClaim, (int)BillingStatus.ClaimPaymentPending, (int)BillingStatus.ClaimAccepted }, startDate, endDate, true);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                revenueList.ForEach(rap =>
                {
                    var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                        //if (rap.Schedule.IsNotNullOrEmpty())
                        //{
                        //    rap.BillableVisitCount = rap.Schedule.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && !v.IsMissedVisit && v.IsDeprecated == false).ToList().Count;
                        //    rap.CompletedVisitCount = rap.Schedule.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && (v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235) && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date).ToList().Count;
                        //}
                        //rap.Schedule = string.Empty;

                        if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount >= 0 && rap.CompletedVisitCount < rap.BillableVisitCount)
                        {
                            var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                            rap.EarnedRevenueAmount = rap.CompletedVisitCount * unitAmount;
                            rap.UnitAmount = unitAmount;
                        }
                        result.Add(rap);
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetEarnedRevenueByEpisodeDays(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, new List<int> { (int)BillingStatus.ClaimSubmitted, (int)BillingStatus.ClaimPaidClaim, (int)BillingStatus.ClaimPaymentPending, (int)BillingStatus.ClaimAccepted }, startDate, endDate, false);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                revenueList.ForEach(rap =>
                {
                    var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                        if (rap.EpisodeStartDate.IsValid() && rap.EpisodeEndDate.IsValid() && rap.EpisodeEndDate > rap.EpisodeStartDate)
                        {
                            if (rap.EpisodeStartDate.Date >= startDate.Date)
                            {
                                if (rap.EpisodeEndDate.Date > endDate.Date)
                                {
                                    rap.CompletedDayCount = endDate.Subtract(rap.EpisodeStartDate).Days;
                                }
                                else
                                {
                                    rap.CompletedDayCount = rap.EpisodeEndDate.Subtract(rap.EpisodeStartDate).Days + 1;
                                }
                            }
                            else
                            {
                                if (rap.EpisodeEndDate.Date > endDate.Date)
                                {
                                    rap.CompletedDayCount = endDate.Subtract(startDate).Days + 1;
                                }
                                else
                                {
                                    rap.CompletedDayCount = rap.EpisodeEndDate.Subtract(startDate).Days;
                                }
                            }
                            if (rap.CompletedDayCount >= 0 && rap.BillableDayCount >= 0)
                            {
                                var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.EpisodeDays, 2);
                                rap.EarnedRevenueAmount = rap.CompletedDayCount * unitAmount;
                                rap.UnitAmount = unitAmount;
                            }
                        }
                        result.Add(rap);
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetRevenueReport(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, new List<int> { (int)BillingStatus.ClaimSubmitted, (int)BillingStatus.ClaimPaidClaim, (int)BillingStatus.ClaimPaymentPending, (int)BillingStatus.ClaimAccepted }, startDate, endDate, true);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                revenueList.ForEach(rap =>
                {
                    if (rap != null)
                    {
                        var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                        if (prospectivePayment != null)
                        {
                            rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                            //rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                            rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                            //if (rap.Schedule.IsNotNullOrEmpty())
                            //{
                            //    rap.BillableVisitCount = rap.Schedule.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && !v.IsMissedVisit).ToList().Count;
                            //    //rap.CompletedVisitCount = rap.Schedule.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "430" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "230" || v.Status == "235") && !v.IsMissedVisit).ToList().Count;
                            //    rap.CompletedVisitCount = rap.Schedule.ToObject<List<ScheduleEvent>>().Where(v => v.IsBillable && (v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235) && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date).ToList().Count;
                            //}
                            //rap.Schedule = string.Empty;

                            if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount >= 0)
                            {
                                rap.UnitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                                rap.EarnedRevenueAmount = Math.Round((rap.CompletedVisitCount) * rap.UnitAmount, 2);

                                rap.RapVisitCount = (int)Math.Ceiling(0.5 * rap.BillableVisitCount);
                                if (rap.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString())
                                {
                                    rap.RapVisitCount = (int)Math.Ceiling(0.6 * rap.BillableVisitCount);
                                }

                                if (rap.ClaimDate.IsDate() && rap.ClaimDate.ToDateTime().Date != DateTime.MinValue.Date && rap.ClaimDate.ToDateTime().Date <= endDate.Date)
                                {
                                    rap.RapAmount = rap.RapVisitCount * rap.UnitAmount;

                                    if (rap.CompletedVisitCount <= rap.RapVisitCount)
                                    {
                                        rap.UnearnedVisitCount = rap.RapVisitCount - rap.CompletedVisitCount;
                                        rap.UnearnedRevenueAmount = rap.UnearnedVisitCount * rap.UnitAmount;
                                    }

                                    if (rap.CompletedVisitCount >= rap.RapVisitCount)
                                    {
                                        rap.UnbilledVisitCount = rap.CompletedVisitCount - rap.RapVisitCount;
                                        rap.UnbilledRevenueAmount = rap.UnbilledVisitCount * rap.UnitAmount;
                                    }
                                }
                                else
                                {
                                    rap.RapAmount = 0.0;
                                    rap.RapVisitCount = 0;

                                    rap.UnbilledVisitCount = rap.CompletedVisitCount;
                                    rap.UnbilledRevenueAmount = rap.EarnedRevenueAmount;

                                    rap.UnearnedVisitCount = 0;
                                    rap.UnearnedRevenueAmount = 0.0;
                                }
                            }
                            result.Add(rap);
                        }
                    }
                });
            }
            return result;
        }

        public IList<Claim> GetPPSRapClaims(Guid branchId)
        {
            return billingRepository.GetPPSRapClaims(Current.AgencyId, branchId);
        }

        public IList<Claim> GetPPSFinalClaims(Guid branchId)
        {
            return billingRepository.GetPPSFinalClaims(Current.AgencyId, branchId);
        }

        public IList<Claim> GetPotentialCliamAutoCancels(Guid branchId)
        {
            return billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, branchId);
        }

        //public IList<Claim> GetPotentialCliamAutoCancels(Guid branchId)
        //{
        //    var finalAutoCancel = new List<Claim>();
        //    var insurnaceFilter = GetInsuranceFilter(branchId, 0, false, "finals");
        //    var finals = billingRepository.GetOutstandingFinalClaims(Current.AgencyId, branchId, 0, insurnaceFilter);
        //    if (finals.IsNotNullOrEmpty())
        //    {
        //        var rapStatus = new int[] { (int)BillingStatus.ClaimAccepted, (int)BillingStatus.ClaimPaidClaim, (int)BillingStatus.ClaimPaymentPending, (int)BillingStatus.ClaimSubmitted };
        //        var finalIds = finals.Select(f => f.Id).Distinct().ToList();
        //        var raps = billingRepository.GetRapsByIdAndStatusLeanForAutoCancel(Current.AgencyId, rapStatus, finalIds, 76);
        //        if (raps.IsNotNullOrEmpty())
        //        {
        //            var finalStatus = new int[] { (int)BillingStatus.ClaimCreated, (int)BillingStatus.ClaimReOpen, (int)BillingStatus.ClaimRejected, (int)BillingStatus.ClaimWithErrors, (int)BillingStatus.ClaimCancelledClaim };

        //            finals.ForEach(final =>
        //            {
        //                var rap = raps.FirstOrDefault(r => r.Id == final.Id);
        //                if (rap != null)
        //                {
        //                    if (finalStatus.Contains(final.Status))
        //                    {
        //                        finalAutoCancel.Add(final);
        //                    }
        //                }
        //            });
        //        }
        //    }
        //    return finalAutoCancel.OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToList();
        //}

        public List<ManagedBill> GetAccountsReceivableManagedClaims(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            return billingRepository.GetAccountsReceivableManagedClaims(Current.AgencyId, branchId, insurance, startDate, endDate);
        }

        public IList<TypeOfBill> GetAllUnProcessedBill()
        {
            var claims = new List<TypeOfBill>();
            var raps = billingRepository.GetOutstandingRaps(Current.AgencyId, true, 5);
            if (raps != null && raps.Count > 0)
            {
                claims.AddRange(raps);
            }

            var finals = billingRepository.GetOutstandingFinals(Current.AgencyId, true, 5);
            if (finals != null && finals.Count > 0)
            {
                claims.AddRange(finals);
            }
            return claims.OrderBy(c => c.SortData).Take(5).ToList();
        }

        public void RemoveMedicareClaims(Guid patientId, Guid episodeId)
        {
            try
            {
                billingRepository.RemoveMedicareClaim(Current.AgencyId, patientId, episodeId, (int)ClaimTypeSubCategory.RAP);
                billingRepository.RemoveMedicareClaim(Current.AgencyId, patientId, episodeId, (int)ClaimTypeSubCategory.Final);
            }
            catch (Exception)
            {
                return;
            }
        }

        public void UpdateMedicareClaimsForDischargeEpisode(Guid episodeId, DateTime dischargeDate)
        {
            try
            {
                billingRepository.UpdateFinalForDischarge(Current.AgencyId, episodeId, dischargeDate);
                billingRepository.UpdateRapForDischarge(Current.AgencyId, episodeId, dischargeDate);
            }
            catch (Exception)
            {
                return;
            }
        }

        public List<ClaimDataLean> ClaimDatas(DateTime startDate, DateTime endDate, string claimType)
        {
            return billingRepository.ClaimDatas(Current.AgencyId, startDate, endDate, claimType);
        }

        #region Secondary Claim Steps | Get

        public SecondaryClaim SecondaryClaimWithInsurance(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                var IsSetLocatorFromLocationOrInsurance = false;
                var isTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(claim.SecondaryInsuranceId);
                if (claim.Status == (int)ManagedClaimStatus.ClaimCreated)
                {
                    if (!claim.IsInsuranceVerified)
                    {
                        var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                        if (profile != null)
                        {
                            claim.AgencyLocationId = profile.AgencyLocationId;
                            if (!isTraditionalMedicare)
                            {
                                SetInsurancePlanInfo(claim, profile);
                                if (claim.PrimaryInsuranceId >= 1000)
                                {
                                    SetClaimAuthorization(claim, claim.EpisodeStartDate, claim.EpisodeEndDate);
                                }

                            }
                        }
                        IsSetLocatorFromLocationOrInsurance = true;
                    }
                    else
                    {
                        if (isTraditionalMedicare && claim.SecondaryInsuranceId >= 1000)
                        {
                            claim.Authorizations = GetClaimAutorizationList(claim.PatientId, claim.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, this.Service);
                        }
                    }
                }
                else
                {
                    if (isTraditionalMedicare && claim.SecondaryInsuranceId >= 1000)
                    {
                        claim.Authorizations = GetClaimAutorizationList(claim.PatientId, claim.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, this.Service);
                    }
                }
                claim.PrimaryInsuranceId = claim.SecondaryInsuranceId;
                ClaimWithInsuranceAppSpecific<SecondaryClaimInsurance>(claim, IsSetLocatorFromLocationOrInsurance, claim.AgencyLocationId);
                //claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
                //claim.AgencyInsurance = ClaimToInsuranceNoLocationId<SecondaryClaim>(claim.PatientId, claim.SecondaryInsuranceId, claim.Insurance, false);
            }
            return claim;
        }
        //TODO: insurance to pull the visit may not be right:
        public SecondaryClaim GetSecondaryClaimVisit(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                //if (profile != null)
                //{

                var visits = scheduleRepository.GetScheduledEventsOnly(Current.AgencyId, claim.PatientId,claim.PrimaryInsuranceId, claim.StartDate, claim.EndDate, new int[] { }, new int[] { }, false);
                if (visits.IsNotNullOrEmpty())
                {
                    //claim.AgencyLocationId = profile.AgencyLocationId;
                    //var agencyInsurance = new AgencyInsurance();
                    //var chargeRates = SecondaryToChargeRates(claim, out agencyInsurance, false);
                    var chargeRates = ClaimInsuranceRatesAppSpecific<SecondaryClaimInsurance>(claim.Id, claim.PayorType);
                    //claim.ChargeRates = chargeRates.Select(c => c.Value).ToList<ChargeRate>();
                    //var claimType = claim.IsManagedClaim ? ClaimType.MAN : (claim.IsMedicareHMO ? ClaimType.HMO : ClaimType.CMS);
                    claim.BillVisitDatas = BillableVisitsDataWithAdjustments(visits, claim.Remittance, claim.Adjustments, ClaimType.MAN, chargeRates, true);
                    //claim.AgencyInsurance = agencyInsurance;
                }
                //}
            }
            return claim;
        }

        //TODO: insurance to pull the visit may not be right:
        public ClaimRemittanceViewData GetSecondaryClaimRemittance(Guid Id, Guid patientId)
        {
            var viewData = new ClaimRemittanceViewData();
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                //var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                //if (profile != null)
                //{
                var visits = scheduleRepository.GetScheduledEventsOnly(Current.AgencyId, claim.PatientId,claim.PrimaryInsuranceId, claim.StartDate, claim.EndDate, new int[] { }, new int[] { }, false);
                if (visits.IsNotNullOrEmpty())
                {
                    //claim.AgencyLocationId = profile.AgencyLocationId;
                    //var agencyInsurance = new AgencyInsurance();
                    //var chargeRates = SecondaryToChargeRates(claim, out agencyInsurance, false);
                    var chargeRates = ClaimInsuranceRatesAppSpecific<SecondaryClaimInsurance>(claim.Id, claim.PayorType);
                    //claim.ChargeRates = chargeRates.Select(c => c.Value).ToList<ChargeRate>();
                    viewData.BillVisitDatas = BillableVisitsDataWithAdjustments(visits, claim.Remittance, claim.Adjustments, ClaimType.MAN, chargeRates, true);

                    //if(claim.Remittance.
                    //claim.RemittanceData = claim.Remittance.ToObject<List<PaymentInformation>>();
                    //claim.AgencyInsurance = agencyInsurance;
                }
                viewData.IsInfoVerified = claim.IsInfoVerified;
                viewData.IsInsuranceVerified = claim.IsInsuranceVerified;
                viewData.IsVisitVerified = claim.IsVisitVerified;
                viewData.IsRemittanceVerified = claim.IsRemittanceVerified;
                viewData.IsSupplyVerified = claim.IsSupplyVerified;
                viewData.Id = claim.Id;
                viewData.PatientId = claim.PatientId;
                viewData.RemitDate = claim.RemitDate;
                viewData.RemitId = claim.RemitId;
                viewData.TotalAdjustmentAmount = claim.TotalAdjustmentAmount;
                //}
            }
            return viewData;
        }

        public ClaimSupplyViewData GetSecondaryClaimSupply(Guid Id, Guid patientId)
        {
            var viewData = new ClaimSupplyViewData();
            viewData.Service = this.Service;
            viewData.Type = ClaimTypeSubCategory.Secondary;
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                viewData.IsInfoVerified = claim.IsInfoVerified;
                viewData.IsInsuranceVerified = claim.IsInsuranceVerified;
                viewData.IsVisitVerified = claim.IsVisitVerified;
                viewData.IsRemittanceVerified = claim.IsRemittanceVerified;
                viewData.IsSupplyVerified = claim.IsSupplyVerified;
                var supplies = claim.Supply.ToObject<List<Supply>>() ?? new List<Supply>();
                if (supplies.IsNotNullOrEmpty())
                {
                    int id = 0;
                    supplies.ForEach(s =>
                    {
                        s.BillingId = id;
                        id++;
                    });
                }
                //claim.Supply = supplies.ToXml();
                //billingRepository.UpdateSecondaryClaimModel(claim);

                viewData.Id = claim.Id;
                viewData.PatientId = claim.PatientId;
                viewData.EpisodeStartDate = claim.EpisodeStartDate;
                viewData.EpisodeEndDate = claim.EpisodeEndDate;
                viewData.BilledSupplies = supplies.Count > 0 ? supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                viewData.UnbilledSupplies = supplies.Count > 0 ? supplies.Where(s => !s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
            }
            return viewData;
        }

        public SecondaryClaim GetSecondaryClaimSummary(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                if (claim.IsInfoVerified)
                {
                    claim.DiagnosisCodesObject = claim.DiagnosisCode.ToObject<DiagnosisCodes>();
                    claim.ConditionCodesObject = claim.ConditionCodes.ToObject<ConditionCodes>();
                }
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ThenBy(s => s.EventDate.Date).ToList();
                    if (visits.IsNotNullOrEmpty())
                    {
                        if (claim.IsVisitVerified)
                        {
                            //var agencyInsurance = new AgencyInsurance();
                            //var chargeRates = SecondaryToChargeRates(claim, out agencyInsurance, false);
                            //var claimType = claim.IsManagedClaim ? ClaimType.MAN : (claim.IsMedicareHMO ? ClaimType.HMO : ClaimType.CMS);
                            var chargeRates = ClaimInsuranceRatesAppSpecific<SecondaryClaimInsurance>(claim.Id, claim.PayorType);
                            claim.BillVisitSummaryDatas = BillableVisitSummaryWithAdjustments(visits, claim.Remittance, claim.Adjustments, ClaimType.MAN, chargeRates, true);
                        }
                    }
                }
            }
            return claim;
        }

        #endregion

        #region Secondary Claim Steps | Verify

        public BillingJsonViewData SecondaryVerifyInfo(SecondaryClaim claim)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Secondary Claim Info was not verified.", PatientId = claim.PatientId, Service = AgencyServices.HomeHealth.ToString(), Category = ClaimTypeSubCategory.Secondary };

            var currentClaim = billingRepository.GetSecondaryClaim(Current.AgencyId, claim.Id);
            if (currentClaim != null && claim != null)
            {
                try
                {
                    claim.PrimaryInsuranceId = claim.SecondaryInsuranceId;
                    if (this.VerifyInfoAppSpecific<SecondaryClaimInsurance>(currentClaim, claim))
                    {
                        currentClaim.Type = claim.Type;
                        currentClaim.FirstName = claim.FirstName;
                        currentClaim.LastName = claim.LastName;
                        currentClaim.InsuranceIdNumber = claim.InsuranceIdNumber;
                        currentClaim.PatientIdNumber = claim.PatientIdNumber;
                        currentClaim.Gender = claim.Gender;
                        currentClaim.DOB = claim.DOB;
                        currentClaim.StartDate = claim.StartDate;
                        currentClaim.EndDate = claim.EndDate;
                        currentClaim.StartofCareDate = claim.StartofCareDate;
                        currentClaim.AddressLine1 = claim.AddressLine1;
                        currentClaim.AddressLine2 = claim.AddressLine2;
                        currentClaim.AddressCity = claim.AddressCity;
                        currentClaim.AddressStateCode = claim.AddressStateCode;
                        currentClaim.AddressZipCode = claim.AddressZipCode;
                        currentClaim.HippsCode = claim.HippsCode;
                        currentClaim.ClaimKey = claim.ClaimKey;
                        currentClaim.FirstBillableVisitDate = claim.FirstBillableVisitDate;
                        currentClaim.PhysicianLastName = claim.PhysicianLastName;
                        currentClaim.PhysicianFirstName = claim.PhysicianFirstName;
                        currentClaim.PhysicianNPI = claim.PhysicianNPI;
                        currentClaim.DiagnosisCode = claim.DiagnosisCodesObject.ToXml();
                        currentClaim.ConditionCodes = claim.ConditionCodesObject.ToXml();
                        currentClaim.Remark = claim.Remark;

                        currentClaim.ProspectivePay = claim.ProspectivePay;
                        currentClaim.AdmissionSource = claim.AdmissionSource;
                        currentClaim.PatientStatus = claim.PatientStatus;
                        currentClaim.UB4PatientStatus = claim.UB4PatientStatus;

                        var oldVerification = currentClaim.IsInfoVerified;

                        currentClaim.IsInfoVerified = true;

                        if (UB4PatientStatusFactory.Discharge().Contains(claim.UB4PatientStatus))
                        {
                            currentClaim.DischargeDate = claim.DischargeDate;
                        }

                        if (claim.SecondaryInsuranceId != currentClaim.SecondaryInsuranceId)
                        {
                            currentClaim.IsInsuranceVerified = false;
                            currentClaim.IsVisitVerified = false;
                            currentClaim.IsRemittanceVerified = false;
                            currentClaim.IsSupplyVerified = false;
                        }

                        if (currentClaim.EpisodeStartDate.Date != claim.EpisodeStartDate.Date || currentClaim.EpisodeEndDate.Date != claim.EpisodeEndDate.Date)
                        {
                            currentClaim.IsVisitVerified = false;
                            currentClaim.IsRemittanceVerified = false;
                            currentClaim.IsSupplyVerified = false;
                        }

                        currentClaim.SecondaryInsuranceId = claim.SecondaryInsuranceId;
                        currentClaim.IsHomeHealthServiceIncluded = claim.IsHomeHealthServiceIncluded;
                        if (billingRepository.UpdateSecondaryClaimModel(currentClaim))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Secondary, LogAction.DemographicsVerified, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Secondary Claim Info was successfully verified.";
                            viewData.PatientId = claim.PatientId;
                            if (!oldVerification)
                            {
                                viewData.IsSecondaryRefresh = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return viewData;
                }
            }
            return viewData;
        }

        public BillingJsonViewData SecondaryClaimInsuranceVerify(SecondaryClaim claim)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified.", Service = this.Service.ToString(), Category = ClaimTypeSubCategory.Secondary };
            var claimToEdit = billingRepository.GetSecondaryClaim(Current.AgencyId, claim.PatientId, claim.Id);
            if (claimToEdit != null)
            {
                if (VerifyInsuranceAppSpecific(claimToEdit, claim))
                {
                    var oldVerification = claimToEdit.IsInsuranceVerified;
                    claimToEdit.IsInsuranceVerified = true;
                    if (billingRepository.UpdateSecondaryClaimModel(claimToEdit))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, claimToEdit.PatientId, claimToEdit.Id.ToString(), LogType.Secondary, LogAction.InsuranceVerified, string.Empty);
                        viewData.errorMessage = "Claim Insurance is successfully verified.";
                        viewData.isSuccessful = true;
                        viewData.PatientId = claimToEdit.PatientId;
                        if (!oldVerification)
                        {
                            viewData.IsSecondaryRefresh = true;
                        }
                    }
                }
            }
            return viewData;
        }

        //TODO: insurance to pull the visit may not be right:
        public BillingJsonViewData SecondaryVerifyVisit(Guid Id, Guid patientId, List<Guid> visit)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Secondary Claim visits were not verified.", Service = AgencyServices.HomeHealth.ToString(), Category = ClaimTypeSubCategory.Secondary };

            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var secondaryClaim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
                if (secondaryClaim != null)
                {
                    var oldVerification = secondaryClaim.IsVisitVerified;
                    var scheduleEvents = scheduleRepository.GetScheduledEventsOnly(Current.AgencyId, patientId,secondaryClaim.PrimaryInsuranceId, secondaryClaim.StartDate, secondaryClaim.EndDate, new int[] { }, new int[] { }, false);
                    if (scheduleEvents != null && scheduleEvents.Count > 0 && visit != null && visit.Count > 0)
                    {
                        var visitList = new List<ScheduleEvent>();
                        var secondaryClaimVisit = secondaryClaim.VerifiedVisits.IsNotNullOrEmpty() ? secondaryClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                        visit.ForEach(v =>
                        {
                            var scheduleVisit = scheduleEvents.FirstOrDefault(s => s.Id == v);
                            if (scheduleVisit != null)
                            {
                                scheduleVisit.IsBillable = true;
                                visitList.Add(scheduleVisit);
                            }
                        });

                        if (secondaryClaimVisit.IsNotNullOrEmpty())
                        {
                            secondaryClaimVisit.ForEach(f =>
                            {
                                if (scheduleEvents.Exists(e => e.Id == f.Id) && !visit.Contains(f.Id))
                                {
                                    scheduleEvents.FirstOrDefault(e => e.Id == f.Id).IsBillable = false;
                                }
                            });
                        }

                        secondaryClaim.IsVisitVerified = true;
                        secondaryClaim.VerifiedVisits = visitList.ToXml();
                        secondaryClaim.Modified = DateTime.Now;
                        if (scheduleRepository.UpdateScheduleEventsForIsBillable(Current.AgencyId, secondaryClaim.PatientId, scheduleEvents))
                        {
                            var supplyList = secondaryClaim.Supply.IsNotNullOrEmpty() ? secondaryClaim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                            if (visitList != null && visitList.Count > 0)
                            {
                                visitList.ForEach(v =>
                                {
                                    //var episodeSupply = this.GetSupply(v);
                                    //if (episodeSupply != null && episodeSupply.Count > 0)
                                    //{
                                    //    episodeSupply.ForEach(s =>
                                    //    {
                                    //        if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                                    //        {
                                    //            s.IsBillable = true;
                                    //            supplyList.Add(s);
                                    //        }
                                    //    });
                                    //}
                                });
                                secondaryClaim.Supply = supplyList.ToXml();
                            }

                            if (billingRepository.UpdateSecondaryClaimModel(secondaryClaim))
                            {
                                viewData.isSuccessful = true;
                            }
                        }
                    }
                    else
                    {
                        secondaryClaim.IsVisitVerified = true;
                        secondaryClaim.VerifiedVisits = new List<ScheduleEvent>().ToXml();
                        secondaryClaim.Modified = DateTime.Now;
                        viewData.isSuccessful = billingRepository.UpdateSecondaryClaimModel(secondaryClaim);
                    }
                    if (viewData.isSuccessful)
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Secondary, LogAction.VisitVerified, string.Empty);
                        viewData.errorMessage = "Secondary Claim visits were successfully verified.";
                        if (!oldVerification)
                        {
                            viewData.IsSecondaryRefresh = true;
                        }
                        viewData.PatientId = patientId;
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData SecondaryRemittanceVerify(Guid Id, Guid patientId, DateTime remitDate, string remitId, Dictionary<Guid, List<ServiceAdjustment>> adjustments)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Secondary Claim remittance was not verified.", Service = AgencyServices.HomeHealth.ToString(), Category = ClaimTypeSubCategory.Secondary };
            var secondaryClaim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            var newAdjustments = new List<ServiceAdjustmentWrapper>();
            if (secondaryClaim != null)
            {
                var oldVerification = secondaryClaim.IsRemittanceVerified;
                if (adjustments.IsNotNullOrEmpty())
                {
                    foreach (var visit in adjustments)
                    {
                        var notEmptyAdjustments = visit.Value.Where(a => a.AdjGroup.IsNotNullOrEmpty()
                            && a.AdjData.AdjAmount.IsNotNullOrEmpty() && a.AdjData.AdjReason.IsNotNullOrEmpty()).ToList();
                        if (notEmptyAdjustments.IsNotNullOrEmpty())
                        {
                            newAdjustments.AddRange(notEmptyAdjustments.Select(adjustment => new ServiceAdjustmentWrapper(visit.Key, adjustment.AdjGroup, adjustment.AdjData)));
                        }
                    }
                }
                secondaryClaim.Adjustments = newAdjustments.ToXml();
                secondaryClaim.TotalAdjustmentAmount = newAdjustments.Count > 0 ? newAdjustments.Sum(s => s.AdjData.AdjAmount.ToDouble()) : 0;
                secondaryClaim.IsRemittanceVerified = true;
                secondaryClaim.RemitId = remitId;
                secondaryClaim.RemitDate = remitDate;
                if (billingRepository.UpdateSecondaryClaimModel(secondaryClaim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, secondaryClaim.PatientId, secondaryClaim.Id.ToString(), LogType.Secondary, LogAction.RemittanceVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Secondary Claim remittance was verified.";
                    viewData.PatientId = patientId;
                    if (!oldVerification)
                    {
                        viewData.IsSecondaryRefresh = true;
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData SecondarySupplyVerify(Guid Id, Guid patientId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Secondary Claim supplies were not verified.", Service = this.Service.ToString(), Category = ClaimTypeSubCategory.Secondary };
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                var oldVerification = claim.IsSupplyVerified;
                claim.Modified = DateTime.Now;
                claim.IsSupplyVerified = true;
                if (billingRepository.UpdateSecondaryClaimModel(claim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Secondary, LogAction.SupplyVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Secondary Claim supplies were successfully verified.";
                    viewData.PatientId = patientId;
                    if (!oldVerification)
                    {
                        viewData.IsSecondaryRefresh = true;
                    }
                }
            }
            return viewData;
        }

        public bool SecondaryClaimComplete(Guid Id, Guid patientId, double total)
        {
            bool result = false;
            var secondaryClaim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (secondaryClaim != null)
            {
                secondaryClaim.ClaimAmount = total;
                result = billingRepository.UpdateSecondaryClaimModel(secondaryClaim);
            }
            return result;
        }

        #endregion

        public SecondaryClaimSnapShotViewData GetSecondaryClaimSnapShotInfo(Guid patientId, Guid claimId)
        {
            var claimInfo = new SecondaryClaimSnapShotViewData();
            var secondaryClaim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, claimId);
            if (secondaryClaim != null)
            {
                claimInfo.Id = secondaryClaim.Id;
                claimInfo.PatientId = secondaryClaim.PatientId;
                claimInfo.ClaimDate = secondaryClaim.ClaimDate;
                claimInfo.PaymentDate = secondaryClaim.PaymentDate;
                claimInfo.PaymentAmount = secondaryClaim.Payment;
                if (secondaryClaim.IsInfoVerified)
                {
                    claimInfo.PatientName = string.Format("{0} {1}", secondaryClaim.FirstName, secondaryClaim.LastName);
                    claimInfo.PatientIdNumber = secondaryClaim.PatientIdNumber;
                    claimInfo.IsuranceIdNumber = secondaryClaim.InsuranceIdNumber;
                }
                else
                {
                    var patient = patientProfileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                    if (patient != null)
                    {
                        claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                        claimInfo.PatientIdNumber = patient.PatientIdNumber;
                        claimInfo.IsuranceIdNumber = patient.MedicareNumber;
                    }
                }
                claimInfo.AuthorizationNumber = secondaryClaim.AuthorizationNumber;
                claimInfo.HealthPlainId = secondaryClaim.HealthPlanId;
                var hhrg = lookUpRepository.GetHHRGByHIPPSCODE(secondaryClaim.HippsCode);
                if (hhrg != null)
                {
                    claimInfo.HHRG = hhrg.HHRG;
                }
                claimInfo.HIPPS = secondaryClaim.HippsCode;
                claimInfo.ClaimKey = secondaryClaim.ClaimKey;

                if (secondaryClaim.HippsCode.IsNotNullOrEmpty())
                {
                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                    PPSStandard ppsStandard;
                    claimInfo.SupplyReimbursement = GetSupplyReimbursement(secondaryClaim.HippsCode[secondaryClaim.HippsCode.Length - 1], secondaryClaim.EpisodeStartDate.Year, out ppsStandard);
                    //claimInfo.StandardEpisodeRate = lookUpRepository.ProspectivePayAmount(managedClaim.HippsCode, managedClaim.EpisodeStartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                    claimInfo.StandardEpisodeRate = Math.Round(lookUpRepository.GetProspectivePaymentAmount(secondaryClaim.HippsCode, secondaryClaim.EpisodeStartDate, secondaryClaim.AddressZipCode.IsNotNullOrEmpty() ? secondaryClaim.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                }
                claimInfo.ProspectivePay = claimInfo.StandardEpisodeRate;
                claimInfo.ClaimAmount = secondaryClaim.ClaimAmount;
                claimInfo.PayorName = InsuranceEngine.GetName(secondaryClaim.SecondaryInsuranceId, Current.AgencyId);
                //var agencyInsurance = this.SecondaryToInsuranceNameAndAddress(secondaryClaim, false);
                //if (agencyInsurance != null)
                //{
                //    claimInfo.PayorName = agencyInsurance.Name;
                //}
                claimInfo.Visible = true;
            }

            return claimInfo;
        }

        public SecondaryClaim GetSecondaryClaim(Guid Id)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                claim.DiagnosisCodesObject = claim.DiagnosisCode.ToObject<DiagnosisCodes>();
                claim.ConditionCodesObject = claim.ConditionCodes.ToObject<ConditionCodes>();
            }
            return claim;
        }

        public JsonViewData AddSecondaryClaim(Guid patientId, Guid primaryClaimId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The secondary claim could not be created." };
            var claim = billingRepository.FisrtSecondaryClaimExistInTheRange(Current.AgencyId, patientId, primaryClaimId, startDate, endDate);
            if (claim == null)
            {
                string errorMessage;
                if (AddSecondaryClaim(patientId, primaryClaimId, startDate, endDate, out errorMessage))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The secondary claim was created.";
                }
                else if (errorMessage.IsNotNullOrEmpty())
                {
                    viewData.errorMessage = errorMessage;
                }
            }
            else
            {
                viewData.errorMessage = string.Format("A claim already exists with the date range {0}.", claim.ClaimRange);
            }
            return viewData;
        }

        public BillingJsonViewData DeleteSecondaryClaim(Guid patientId, Guid claimId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The secondary claim could not be deleted." };
            viewData.Service = this.Service.ToString();
            viewData.Category = ClaimTypeSubCategory.Secondary;
            var rules = new List<Validation>();
            if (!claimId.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingRepository.DeleteSecondaryClaim(Current.AgencyId, patientId, claimId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The secondary claim has been deleted successfully.";
                        viewData.IsSecondaryRefresh = true;
                        viewData.PatientId = patientId;
                    }
                }
            }
            return viewData;
        }

        //private void ResetInsuranceAndLocatorsToDefault(SecondaryClaim claim, AgencyInsurance insurance)
        //{
        //    claim.Insurance = insurance != null ? insurance.ToXml() : "";
        //    claim.Ub04Locator81cca = insurance != null ? insurance.Ub04Locator81cca : "";
        //    claim.Ub04Locator39 = "";
        //    claim.Ub04Locator31 = "";
        //    claim.Ub04Locator32 = "";
        //    claim.Ub04Locator33 = "";
        //    claim.Ub04Locator34 = "";
        //    claim.HCFALocators = "";
        //}

        public bool UpdateSecondaryClaimStatus(SecondaryClaim claim)
        {
            bool result = false;
            if (claim != null)
            {
                var claimToEdit = billingRepository.GetSecondaryClaim(Current.AgencyId, claim.Id);
                if (claimToEdit != null)
                {
                    var oldStatus = claimToEdit.Status;
                    var isStatusChange = claimToEdit.Status != claim.Status;
                    bool oldStatusOpened = ManagedClaimStatusFactory.UnProcessed().Contains(oldStatus);// (oldStatus == (int)ManagedClaimStatus.ClaimCreated || oldStatus == (int)ManagedClaimStatus.ClaimReOpen); //claimToEdit.IsManagedClaim ? (oldStatus == (int)BillingStatus.ClaimCreated || oldStatus == (int)BillingStatus.ClaimReOpen) : (oldStatus == (int)ManagedClaimStatus.ClaimCreated || oldStatus == (int)ManagedClaimStatus.ClaimReOpen);
                    bool newStatusOpened = ManagedClaimStatusFactory.UnProcessed().Contains(claim.Status); //(claim.Status == (int)ManagedClaimStatus.ClaimCreated || claim.Status == (int)ManagedClaimStatus.ClaimReOpen);//claimToEdit.IsManagedClaim ? (claim.Status == (int)BillingStatus.ClaimCreated || claim.Status == (int)BillingStatus.ClaimReOpen) : (claim.Status == (int)ManagedClaimStatus.ClaimCreated || claim.Status == (int)ManagedClaimStatus.ClaimReOpen);
                    if (isStatusChange && newStatusOpened)
                    {
                        claimToEdit.IsInfoVerified = false;
                        claimToEdit.IsSupplyVerified = false;
                        claimToEdit.IsVisitVerified = false;
                    }
                    if (isStatusChange && oldStatusOpened && !newStatusOpened)
                    {
                        if (!claimToEdit.IsInfoVerified || !claimToEdit.IsInsuranceVerified)
                        {
                            claimToEdit.PayorType = (int)PayorTypeMainCategory.NonPrivatePayor;
                            this.UpdateOrAddPayorForClaimAppSpecific<SecondaryClaimInsurance>(claimToEdit);
                        }
                        //var insuranceXml = SerializedInsuranceForClaimByPatient<SecondaryClaim>(claimToEdit, claimToEdit.PatientId, claimToEdit.SecondaryInsuranceId, sclaim => ((sclaim.IsInfoVerified && sclaim.Insurance.IsNullOrEmpty()) || (!sclaim.IsInfoVerified)));
                        //if (insuranceXml.IsNotNullOrEmpty())
                        //{
                        //    claimToEdit.Insurance = insuranceXml;
                        //}
                    }
                    claimToEdit.Status = claim.Status;
                    claimToEdit.Payment = claim.Payment;
                    claimToEdit.PaymentDate = claim.PaymentDate;
                    claimToEdit.ClaimDate = claim.ClaimDate;
                    claimToEdit.Comment = claim.Comment;
                    result = billingRepository.UpdateSecondaryClaimModel(claimToEdit);
                }
            }
            return result;
        }

        public List<ChargeRate> GetSecondaryClaimInsuranceRates(Guid Id)
        {
            return ClaimInsuranceRatesAppSpecific<SecondaryClaimInsurance>(Id, (int)PayorTypeMainCategory.NonPrivatePayor);
            //var claim = billingRepository.GetSecondaryClaimInsurance(Current.AgencyId, Id);
            //List<ChargeRate> rates = new List<ChargeRate>();
            //if (claim != null)
            //{
            //    if (claim.Insurance.IsNullOrEmpty())
            //    {
            //        var agencyInsurance = new AgencyInsurance();
            //        var chargeRates = this.SecondaryToChargeRates(claim, out agencyInsurance, false);
            //        if (chargeRates != null && chargeRates.Count > 0)
            //        {
            //            rates = chargeRates;//.Select(c => c.Value).ToList();
            //        }
            //    }
            //    else
            //    {
            //        var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
            //        if (agencyInsurance != null && agencyInsurance.BillData.IsNotNullOrEmpty())
            //        {
            //            var chargeRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
            //            if (chargeRates != null && chargeRates.Count > 0)
            //            {
            //                rates = chargeRates;
            //            }
            //        }
            //    }
            //}
            //return rates;
        }

        //public List<ChargeRate> SecondaryToChargeRates(SecondaryClaim secondaryClaim, out AgencyInsurance agencyInsurance, bool IsAddressNeeded)
        //{
        //    if (secondaryClaim == null)
        //    {
        //        agencyInsurance = new AgencyInsurance();
        //        return null;
        //    }
        //    return InsuranceWithChargeRates<SecondaryClaim>(secondaryClaim,
        //        secondaryClaim.AgencyLocationId,
        //        secondaryClaim.Insurance,
        //        secondaryClaim.SecondaryInsuranceId,
        //        claim => (claim.IsInfoVerified && claim.Insurance.IsNotNullOrEmpty() || ((claim.Status != (int)ManagedClaimStatus.ClaimCreated && claim.Status != (int)ManagedClaimStatus.ClaimReOpen) && claim.Insurance.IsNotNullOrEmpty())),
        //        out agencyInsurance,
        //        IsAddressNeeded);
        //}

        public Bill SecondaryClaimToGenerate(List<Guid> secondaryClaimToGenerate, Guid branchId, int secondaryInsurance)
        {
            var bill = new Bill();
            bill.Service = this.Service;
            bill.BranchId = branchId;
            bill.Insurance = secondaryInsurance;
            var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (location != null)
            {
                bill.Claims = billingRepository.GetManagedClaimByIds(Current.AgencyId, branchId, secondaryInsurance, secondaryClaimToGenerate);
                bill.BranchName = location.Name;
                var isElectronicSubmssion = false;
                var insurance = agencyRepository.GetInsurance(secondaryInsurance, Current.AgencyId);
                if (insurance != null)
                {
                    isElectronicSubmssion = insurance.IsAxxessTheBiller;
                    bill.InsuranceName = insurance.Name;
                }
                bill.IsElectronicSubmssion = isElectronicSubmssion;
            }
            return bill;
        }

        //public BillingJsonViewData AddSecondaryClaimBillData(ChargeRate chargeRate, Guid ClaimId)
        //{
        //    var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
        //    viewData.Service = this.Service.ToString();
        //    viewData.Category = ClaimTypeSubCategory.Secondary;
        //    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
        //    if (claim != null)
        //    {
        //        if (claim.Insurance.IsNotNullOrEmpty())
        //        {
        //            var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
        //            if (agencyInsurance != null)
        //            {
        //                var rates = agencyInsurance.BillData.IsNotNullOrEmpty() ? agencyInsurance.BillData.ToObject<List<ChargeRate>>() : new List<ChargeRate>();
        //                rates.Add(chargeRate);
        //                agencyInsurance.BillData = rates.ToXml();
        //                claim.Insurance = agencyInsurance.ToXml();
        //                if (billingRepository.UpdateSecondaryClaimModel(claim))
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "Charge rate is successfuly updated.";
        //                }
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public BillingJsonViewData UpdateSecondaryClaimBillData(ChargeRate chargeRate, Guid ClaimId)
        //{
        //    var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
        //    viewData.Service = this.Service.ToString();
        //    viewData.Category = ClaimTypeSubCategory.Secondary;
        //    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
        //    if (claim != null)
        //    {
        //        if (claim.Insurance.IsNotNullOrEmpty())
        //        {
        //            var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
        //            if (agencyInsurance != null)
        //            {
        //                var rates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
        //                if (rates != null && rates.Count > 0)
        //                {
        //                    var oldRate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
        //                    if (oldRate != null)
        //                    {
        //                        oldRate.PreferredDescription = chargeRate.PreferredDescription;
        //                        oldRate.Code = chargeRate.Code;
        //                        oldRate.RevenueCode = chargeRate.RevenueCode;
        //                        oldRate.Charge = chargeRate.Charge;
        //                        oldRate.Modifier = chargeRate.Modifier;
        //                        oldRate.Modifier2 = chargeRate.Modifier2;
        //                        oldRate.Modifier3 = chargeRate.Modifier3;
        //                        oldRate.Modifier4 = chargeRate.Modifier4;
        //                        oldRate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
        //                        oldRate.ChargeType = chargeRate.ChargeType;
        //                        if (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString())
        //                        {
        //                            if (chargeRate.IsTimeLimit)
        //                            {

        //                                oldRate.TimeLimitHour = chargeRate.TimeLimitHour;
        //                                oldRate.TimeLimitMin = chargeRate.TimeLimitMin;
        //                                oldRate.SecondDescription = chargeRate.SecondDescription;
        //                                oldRate.SecondCode = chargeRate.SecondCode;
        //                                oldRate.SecondRevenueCode = chargeRate.SecondRevenueCode;
        //                                oldRate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
        //                                if (chargeRate.IsSecondChargeDifferent)
        //                                {
        //                                    oldRate.SecondCharge = chargeRate.SecondCharge;
        //                                }
        //                                else
        //                                {
        //                                    oldRate.SecondCharge = 0;
        //                                }
        //                                oldRate.SecondModifier = chargeRate.SecondModifier;
        //                                oldRate.SecondModifier2 = chargeRate.SecondModifier2;
        //                                oldRate.SecondModifier3 = chargeRate.SecondModifier3;
        //                                oldRate.SecondModifier4 = chargeRate.SecondModifier4;
        //                                oldRate.SecondChargeType = chargeRate.SecondChargeType;
        //                                if (oldRate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
        //                                {
        //                                    oldRate.SecondUnit = chargeRate.SecondUnit;
        //                                }
        //                                else
        //                                {
        //                                    oldRate.SecondUnit = 0;
        //                                }
        //                                oldRate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
        //                            }
        //                            oldRate.IsTimeLimit = chargeRate.IsTimeLimit;
        //                        }
        //                        else if (oldRate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
        //                        {
        //                            oldRate.Unit = chargeRate.Unit;
        //                        }
        //                        if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO && (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
        //                        {
        //                            oldRate.MedicareHMORate = chargeRate.MedicareHMORate;
        //                        }

        //                        agencyInsurance.BillData = rates.ToXml();
        //                        claim.Insurance = agencyInsurance.ToXml();
        //                        if (billingRepository.UpdateSecondaryClaimModel(claim))
        //                        {
        //                            viewData.isSuccessful = true;
        //                            viewData.errorMessage = "Charge rate is successfuly updated.";
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        public SecondaryClaim UpdateSecondaryClaimReloadInsurance(Guid Id, Guid patientId)
        {
            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be reloaded." };

            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                var agencyInsurance = ClaimToInsuranceNoLocationId(claim.PatientId, claim.SecondaryInsuranceId, true);
                if (agencyInsurance != null)
                {
                    claim.HCFALocators = "";
                    claim.Ub04Locator31 = "";
                    claim.Ub04Locator32 = "";
                    claim.Ub04Locator33 = "";
                    claim.Ub04Locator34 = "";
                    claim.Ub04Locator39 = "";
                    claim.Ub04Locator81cca = agencyInsurance.Ub04Locator81cca;
                    claim.Insurance = agencyInsurance.ToXml();
                    claim.IsInsuranceVerified = false;
                    if (billingRepository.UpdateSecondaryClaimModel(claim))
                    {
                        //viewData.isSuccessful = true;
                        //viewData.errorMessage = "Insurance was successfully reloaded.";
                    }
                    claim.AgencyInsurance = agencyInsurance;
                }
                claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
            }
            return claim;
        }

        //public BillingJsonViewData DeleteSecondaryClaimBillData(Guid ClaimId, int Id)
        //{
        //    var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be deleted." };
        //    viewData.Service = this.Service.ToString();
        //    viewData.Category = ClaimTypeSubCategory.Secondary;
        //    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
        //    if (claim != null && claim.Insurance.IsNotNullOrEmpty())
        //    {
        //        var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
        //        var rates = agencyInsurance.ToBillDataDictionary();
        //        if (rates.ContainsKey(Id.ToString()))
        //        {
        //            if (rates.Remove(Id.ToString()))
        //            {
        //                agencyInsurance.BillData = rates.Select(r => r.Value).ToList().ToXml();
        //                claim.Insurance = agencyInsurance.ToXml();
        //                if (billingRepository.UpdateSecondaryClaimModel(claim))
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "Charge rate is successfuly deleted.";
        //                }
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        public List<SecondaryClaimLean> GetSecondaryClaimLeansOfPrimaryClaim(Guid patientId, Guid primaryClaimId)
        {
            return billingRepository.GetSecondaryClaimLeansOfPrimaryClaim(Current.AgencyId, patientId, primaryClaimId);
        }

        //public string GenerateJsonForSecondary(List<Guid> secondaryClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AgencyInsurance payerInfo, out List<SecondaryClaim> secondaryClaims, AgencyLocation branch)
        //{
        //    string requestArr = string.Empty;
        //    claimInfo = new List<ClaimInfo>();
        //    secondaryClaims = null;
        //    try
        //    {
        //        var secondaryClaimLists = billingRepository.GetSecondaryClaimsToGenerateByIds(Current.AgencyId, secondaryClaimToGenerate);
        //        secondaryClaims = secondaryClaimLists;
        //        if (branch != null)
        //        {
        //            var patients = new List<object>();
        //            if (secondaryClaimLists != null && secondaryClaimLists.Count > 0)
        //            {
        //                var cliamInsurances = mongoRepository.GetManyClaimInsurances<ManagedClaimInsurance>(Current.AgencyId, secondaryClaimToGenerate);
        //                if (cliamInsurances.IsNotNullOrEmpty())
        //                {
        //                    var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
        //                    foreach (var secondaryClaim in secondaryClaimLists)
        //                    {


        //                        var claims = new List<object>();

        //                        var visitTotalAmount = 0.00;
        //                        var visits = secondaryClaim.VerifiedVisits.IsNotNullOrEmpty() ? secondaryClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<ScheduleEvent>();
        //                        var visitList = new List<AnsiVisit>();
        //                        if (visits.IsNotNullOrEmpty())
        //                        {
        //                            //var agencyInsurance = new AgencyInsurance();
        //                            //var chargeRates = this.SecondaryToChargeRates(secondaryClaim, out agencyInsurance, false) ?? new List<ChargeRate>();
        //                            List<ChargeRate> chargeRates = null;
        //                            var claimInsurance = cliamInsurances.FirstOrDefault(i => i.Id == secondaryClaim.Id);
        //                            if (claimInsurance != null && claimInsurance.Insurance != null)
        //                            {
        //                                chargeRates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
        //                            }
        //                            else
        //                            {
        //                                var agencyInsurance = secondaryClaim.Insurance.ToObject<AgencyInsurance>();
        //                                if (agencyInsurance != null)
        //                                {
        //                                    chargeRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
        //                                }
        //                            }
        //                            chargeRates = chargeRates ?? new List<ChargeRate>();
        //                            var adjustments = secondaryClaim.Adjustments.IsNotNullOrEmpty() ? secondaryClaim.Adjustments.ToObject<List<ServiceAdjustmentWrapper>>() : new List<ServiceAdjustmentWrapper>();
        //                            var visitsDictionary = visits.GroupBy(v => v.VisitDate.Date).ToDictionary(v => v.Key, v => v.ToList());
        //                            if (visitsDictionary != null && visitsDictionary.Count > 0)
        //                            {
        //                                visitsDictionary.ForEach((visitPerDayKey, visitPerDayValue) =>
        //                                {
        //                                    var visitByDisciplneDictionary = visitPerDayValue.GroupBy(vd => vd.DisciplineTask).ToDictionary(vd => vd.Key, vd => vd.ToList());
        //                                    if (visitByDisciplneDictionary != null && visitByDisciplneDictionary.Count > 0)
        //                                    {
        //                                        visitByDisciplneDictionary.ForEach((vddk, vddv) =>
        //                                        {
        //                                            var rate = chargeRates.FirstOrDefault(c => c.Id == vddk);
        //                                            if (rate != null)
        //                                            {
        //                                                double amount;
        //                                                var addedVisits = BillableVisitForANSIWithAdjustments(rate, vddv, out amount, ClaimType.MAN, true, adjustments);
        //                                                if (addedVisits != null && addedVisits.Count > 0)
        //                                                {
        //                                                    visitList.AddRange(addedVisits);
        //                                                    visitTotalAmount += amount;
        //                                                }
        //                                            }
        //                                        });
        //                                    }
        //                                });
        //                            }
        //                        }

        //                        var supplies = secondaryClaim.Supply.IsNotNullOrEmpty() ? secondaryClaim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
        //                        var supplyList = new List<object>();
        //                        var supplyTotalAmount = 0.00;
        //                        if (supplies != null && supplies.Count > 0)
        //                        {
        //                            supplies.ForEach(v =>
        //                            {
        //                                supplyList.Add(new { date = v.Date, revenue = v.RevenueCode.IsNotNullOrEmpty() ? v.RevenueCode : string.Empty, hcpcs = v.Code.IsNotNullOrEmpty() ? v.Code : string.Empty, units = v.Quantity, amount = v.UnitCost * v.Quantity, modifier = v.Modifier.IsNotNullOrEmpty() ? v.Modifier : string.Empty });
        //                                supplyTotalAmount += v.UnitCost * v.Quantity;
        //                            });
        //                        }

        //                        claimInfo.Add(new ClaimInfo { ClaimId = secondaryClaim.Id, PatientId = secondaryClaim.PatientId, EpisodeId = secondaryClaim.EpisodeId, ClaimType = secondaryClaim.Type.ToString() });

        //                        var locator31 = ConvertStringLocatorToList(secondaryClaim.Ub04Locator31);
        //                        var locator32 = ConvertStringLocatorToList(secondaryClaim.Ub04Locator32);
        //                        var locator33 = ConvertStringLocatorToList(secondaryClaim.Ub04Locator33);
        //                        var locator34 = ConvertStringLocatorToList(secondaryClaim.Ub04Locator34);
        //                        var locator39 = ConvertStringLocatorToList(secondaryClaim.Ub04Locator39);
        //                        var locator81 = ConvertStringLocatorToList(secondaryClaim.Ub04Locator81cca);
        //                        var hcfaLocators = secondaryClaim.HCFALocators.ToLocatorDictionary();

        //                        var diagnosis = secondaryClaim.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(secondaryClaim.DiagnosisCode) : null;

        //                        var conditionCodes = secondaryClaim.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(secondaryClaim.ConditionCodes) : null;

        //                        var secondaryObj = new
        //                        {
        //                            claim_is_secondary_claim = true,
        //                            claim_id = secondaryClaim.Id,
        //                            claim_type = secondaryClaim.Type,
        //                            claim_physician_upin = secondaryClaim.PhysicianNPI,
        //                            claim_physician_last_name = secondaryClaim.PhysicianLastName,
        //                            claim_physician_first_name = secondaryClaim.PhysicianFirstName,
        //                            claim_first_visit_date = secondaryClaim.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
        //                            claim_episode_start_date = secondaryClaim.EpisodeStartDate.ToString("MM/dd/yyyy"),
        //                            claim_episode_end_date = secondaryClaim.EpisodeEndDate.ToString("MM/dd/yyyy"),
        //                            claim_hipps_code = secondaryClaim.HippsCode,
        //                            claim_oasis_key = secondaryClaim.ClaimKey,
        //                            hmo_plan_id = secondaryClaim.HealthPlanId,
        //                            claim_group_name = secondaryClaim.GroupName,
        //                            claim_group_Id = secondaryClaim.GroupId,
        //                            claim_hmo_auth_key = secondaryClaim.AuthorizationNumber,
        //                            claim_hmo_auth_key2 = secondaryClaim.AuthorizationNumber2,
        //                            claim_hmo_auth_key3 = secondaryClaim.AuthorizationNumber3,
        //                            claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
        //                            claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
        //                            claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
        //                            claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
        //                            claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
        //                            claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
        //                            claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
        //                            claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
        //                            claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
        //                            claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
        //                            claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
        //                            claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
        //                            claim_admission_source_code = secondaryClaim.AdmissionSource.IsNotNullOrEmpty() && secondaryClaim.AdmissionSource.IsInteger() ? secondaryClaim.AdmissionSource.ToInteger().GetSplitValue() : "9",
        //                            claim_patient_status_code = secondaryClaim.UB4PatientStatus,
        //                            claim_dob = ubo4DischargeStatus.Contains(secondaryClaim.UB4PatientStatus) ? secondaryClaim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            claim_ub04locator81 = locator81,
        //                            claim_ub04locator39 = locator39,
        //                            claim_ub04locator31 = locator31,
        //                            claim_ub04locator32 = locator32,
        //                            claim_ub04locator33 = locator33,
        //                            claim_ub04locator34 = locator34,
        //                            claim_hcfalocator33 = hcfaLocators.ContainsKey("33Locatorb") && hcfaLocators["33Locatorb"] != null ? ConvertLocatorToList(hcfaLocators["33Locatorb"]) : new List<object>(),
        //                            claim_supply_isBillable = true,
        //                            claim_supply_value = Math.Round(supplyTotalAmount, 2),
        //                            claim_supplies = supplyList,
        //                            claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
        //                            claim_visits = visitList,
        //                            claim_remit_date = secondaryClaim.RemitDate.ToZeroFilled(),
        //                            claim_remit_id = secondaryClaim.RemitId,
        //                            claim_total_adjustment_amount = secondaryClaim.TotalAdjustmentAmount
        //                        };
        //                        claims.Add(secondaryObj);
        //                        var patient = new
        //                        {
        //                            patient_gender = secondaryClaim.Gender.Substring(0, 1),
        //                            patient_record_num = secondaryClaim.PatientIdNumber,
        //                            patient_dob = secondaryClaim.DOB.ToString("MM/dd/yyyy"),
        //                            patient_doa = secondaryClaim.StartofCareDate.ToString("MM/dd/yyyy"),
        //                            patient_dod = ubo4DischargeStatus.Contains(secondaryClaim.UB4PatientStatus) && secondaryClaim.DischargeDate.Date > DateTime.MinValue.Date ? secondaryClaim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            patient_address = secondaryClaim.AddressLine1,
        //                            patient_address2 = secondaryClaim.AddressLine2,
        //                            patient_city = secondaryClaim.AddressCity,
        //                            patient_state = secondaryClaim.AddressStateCode,
        //                            patient_zip = secondaryClaim.AddressZipCode,
        //                            patient_cbsa = lookUpRepository.CbsaCodeByZip(secondaryClaim.AddressZipCode),
        //                            patient_last_name = secondaryClaim.LastName,
        //                            patient_first_name = secondaryClaim.FirstName,
        //                            patient_middle_initial = "",
        //                            claims_arr = claims
        //                        };
        //                        patients.Add(patient);
        //                    }

        //                    var agencyClaim = new
        //                    {
        //                        format = "ansi837",
        //                        submit_type = commandType.ToString(),
        //                        user_login_name = Current.User.Name,
        //                        hmo_payer_id = payerInfo.PayorId,
        //                        hmo_payer_name = payerInfo.Name,
        //                        hmo_submitter_id = payerInfo.SubmitterId,
        //                        hmo_provider_id = payerInfo.ProviderId,
        //                        hmo_other_provider_id = payerInfo.OtherProviderId,
        //                        hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
        //                        // hmo_additional_codes = additionaCodes,
        //                        // payer_id = payerInfo.Code,
        //                        payer_name = payerInfo.Name,
        //                        insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
        //                        clearing_house_id = payerInfo.ClearingHouseSubmitterId,
        //                        provider_claim_type = payerInfo.BillType,
        //                        interchange_receiver_id = payerInfo.InterchangeReceiverId,
        //                        clearing_house = payerInfo.ClearingHouse,
        //                        claim_billtype = ClaimType.MAN.ToString(),
        //                        submitter_name = payerInfo.SubmitterName,
        //                        submitter_phone = payerInfo.SubmitterPhone,
        //                        // submitter_fax = payerInfo.Fax,
        //                        user_agency_name = branch.Name,
        //                        user_tax_id = branch.TaxId,
        //                        user_national_provider_id = branch.NationalProviderNumber,
        //                        user_address_1 = branch.AddressLine1,
        //                        user_address_2 = branch.AddressLine2,
        //                        user_city = branch.AddressCity,
        //                        user_state = branch.AddressStateCode,
        //                        user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
        //                        user_phone = branch.PhoneWork,
        //                        user_fax = branch.FaxNumber,
        //                        user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
        //                        ansi_837_id = claimId,
        //                        patients_arr = patients
        //                    };
        //                    var jss = new JavaScriptSerializer();
        //                    requestArr = jss.Serialize(agencyClaim);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return string.Empty;
        //    }
        //    return requestArr;
        //}


        public string GenerateJsonForSecondary(List<Guid> secondaryClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AgencyInsurance payerInfo, out List<SecondaryClaim> secondaryClaims, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            secondaryClaims = null;
            try
            {
                if (branch != null)
                {
                    var secondaryClaimLists = billingRepository.GetSecondaryClaimsToGenerateByIds(Current.AgencyId, secondaryClaimToGenerate);
                    secondaryClaims = secondaryClaimLists;
                    if (secondaryClaimLists.IsNotNullOrEmpty())
                    {
                        var cliamInsurances = mongoRepository.GetManyClaimInsurances<SecondaryClaimInsurance>(Current.AgencyId, secondaryClaimToGenerate);
                        if (cliamInsurances.IsNotNullOrEmpty())
                        {
                            var patients = new List<AnsiPatient>();
                            var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
                            foreach (var secondaryClaim in secondaryClaimLists)
                            {

                                claimInfo.Add(new ClaimInfo { ClaimId = secondaryClaim.Id, PatientId = secondaryClaim.PatientId, EpisodeId = secondaryClaim.EpisodeId, ClaimType = secondaryClaim.Type.ToString() });

                                var claims = new List<AnsiClaim>();
                                var visitTotalAmount = 0.0;
                                var visitList = new List<AnsiVisit>();

                                var supplyTotalAmount = 0.0;
                                var supplyList = new List<AnsiSupply>();

                                var visits = secondaryClaim.VerifiedVisits.IsNotNullOrEmpty() ? secondaryClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<ScheduleEvent>();
                                var isVisitExist = visits.IsNotNullOrEmpty();
                                if (isVisitExist)
                                {
                                    var adjustments = secondaryClaim.Adjustments.IsNotNullOrEmpty() ? secondaryClaim.Adjustments.ToObject<List<ServiceAdjustmentWrapper>>() : new List<ServiceAdjustmentWrapper>();
                                    var chargeRates = GetChargeRatesFromInsurance(cliamInsurances, secondaryClaim, isVisitExist);
                                    visitList = Visits(visits, chargeRates, out visitTotalAmount, ClaimType.MAN, adjustments);

                                    var supplies = secondaryClaim.Supply.IsNotNullOrEmpty() ? secondaryClaim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
                                    supplyList = Supplies(supplies, out supplyTotalAmount);

                                }

                                var secondaryObj = ClaimsWithVisitsAndSuppliesForSecondary(secondaryClaim, visitList, visitTotalAmount, supplyList, supplyTotalAmount, ubo4DischargeStatus);
                                secondaryObj.claim_type = secondaryClaim.Type.ToString();
                                secondaryObj.claim_supply_isBillable = true;
                                claims.Add(secondaryObj);

                                var patient = PatientWithClaims(secondaryClaim, ubo4DischargeStatus, claims);
                                patients.Add(patient);
                            }

                            var agencyClaim = AgencyWithPatients(commandType, ClaimType.MAN, claimId, payerInfo, branch, patients);
                            var jss = new JavaScriptSerializer();
                            requestArr = jss.Serialize(agencyClaim);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return requestArr;
        }


        public bool GenerateSecondarySingle(Guid Id, out ClaimData claimDataOut, out BillExchange billExchange)
        {
            var result = false;
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                if (profile != null)
                {
                    var ids = (new List<Guid>());
                    ids.Add(Id);
                    if (GenerateSecondary(ids, ClaimCommandType.download, out claimDataOut, out billExchange, profile.AgencyLocationId, claim.SecondaryInsuranceId))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool GenerateSecondary(List<Guid> secondaryClaimToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            bool result = false;
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (agencyLocation != null)
                {
                    if (!agencyLocation.IsLocationStandAlone)
                    {
                        //agencyLocation.Payor = agency.Payor;
                        //agencyLocation.SubmitterId = agency.SubmitterId;
                        //agencyLocation.SubmitterName = agency.SubmitterName;
                        //agencyLocation.SubmitterPhone = agency.SubmitterPhone;
                        //agencyLocation.SubmitterFax = agency.SubmitterFax;
                        agencyLocation.Name = agency.Name;
                        agencyLocation.TaxId = agency.TaxId;
                        agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;

                    }
                    if (insuranceId >= 1000)
                    {
                        var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                        if (insurance != null)
                        {
                            claimData.ClaimType = ClaimType.MAN.ToString();
                            var claimId = GetNextClaimId(claimData);
                            claimData.Id = claimId;
                            List<SecondaryClaim> secondaryClaims = null;

                            var requestArr = GenerateJsonForSecondary(secondaryClaimToGenerate, commandType, claimId, out claimInfo, insurance, out secondaryClaims, agencyLocation);
                            if (requestArr.IsNotNullOrEmpty())
                            {
                                requestArr = requestArr.Replace("&", "U+0026");
                                billExchange = GenerateANSI(requestArr);
                                if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                                {
                                    if (billExchange.Result.IsNotNullOrEmpty())
                                    {
                                        claimData.Data = billExchange.Result;
                                        claimData.ClaimType = ClaimType.MAN.ToString();
                                        claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                                        billingRepository.UpdateClaimData(claimData);
                                        if (commandType == ClaimCommandType.direct)
                                        {
                                            if (secondaryClaims != null && secondaryClaims.Count > 0)
                                            {
                                                billingRepository.MarkSecondaryClaimsAsSubmitted(Current.AgencyId, secondaryClaims);
                                            }
                                        }
                                        else if (commandType == ClaimCommandType.download)
                                        {
                                            if (secondaryClaims != null && secondaryClaims.Count > 0)
                                            {
                                                billingRepository.MarkSecondaryClaimsAsGenerated(Current.AgencyId, secondaryClaims);
                                            }
                                        }
                                        claimDataOut = claimData;
                                        result = true;
                                    }
                                    else
                                    {
                                        billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                                        claimDataOut = null;
                                        result = false;
                                    }
                                }
                                else
                                {
                                    billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                                    result = false;
                                }
                            }
                            else
                            {
                                billingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                                billExchange.Message = "System problem. Try again.";
                                result = false;
                            }

                        }
                        else
                        {
                            billExchange.Message = "Insurance/Payer information is not right.";
                            return false;
                        }
                    }
                    else
                    {
                        billExchange.Message = "Insurance/Payer information is not right.";
                    }
                }
                else
                {
                    billExchange.Message = "Branch information is not found. Try again.";
                }

            }
            return result;
        }

        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillableVisitsDataWithAdjustments(
          List<ScheduleEvent> visits, string remittance, string adjustments, ClaimType claimType, List<ChargeRate> chargeRates, bool isLimitApplied)
        {
            var billableVisitsData = this.BillableVisitsData(visits, claimType, chargeRates, true);
            if (billableVisitsData.IsNotNullOrEmpty())
            {
                if (adjustments.IsNotNullOrEmpty())
                {
                    var serviceAdjustments = adjustments.ToObject<List<ServiceAdjustmentWrapper>>();
                    if (adjustments != null && serviceAdjustments.Count > 0)
                    {
                        foreach (var discipline in billableVisitsData[BillVisitCategory.Billable])
                        {
                            foreach (var visit in discipline.Value)
                            {
                                var foundAdjustments = serviceAdjustments.Where(w => w.EventId == visit.EventId).Select(s => new ServiceAdjustment()
                                {
                                    AdjData = s.AdjData,
                                    AdjGroup = s.AdjGroup
                                }).ToList();
                                if (foundAdjustments != null)
                                {
                                    visit.Adjustments = foundAdjustments;
                                }
                            }
                        }
                    }
                }
                else
                {
                    List<PaymentInformation> remittanceAdjustments = new List<PaymentInformation>();
                    if (remittance.IsNotNullOrEmpty())
                    {
                        remittanceAdjustments = remittance.ToObject<List<PaymentInformation>>();
                    }
                    foreach (var billVisitCategory in billableVisitsData)
                    {
                        foreach (var billDiscipline in billVisitCategory.Value)
                        {
                            foreach (var billSchedule in billDiscipline.Value)
                            {

                            }
                        }
                    }
                }
            }
            return billableVisitsData;
        }


        //private static double CalculateAdjAmount(Guid visitId, List<ServiceAdjustmentWrapper> adjustments)
        //{
        //    var total = 0.0;
        //    if (adjustments.IsNotNullOrEmpty())
        //    {
        //        var adjustment = adjustments.FirstOrDefault(f => f.EventId == visitId);
        //        if (adjustment != null)
        //        {
        //            total += adjustment.AdjData.AdjAmount.ToSafeDouble();
        //        }
        //    }
        //    return total;
        //}


        public List<BillSchedule> BillableVisitSummaryWithAdjustments(List<ScheduleEvent> visits, string remittance, string adjustments,
             ClaimType claimType, List<ChargeRate> chargeRates, bool isLimitApplied)
        {
            var billVisits = this.BillableVisitSummary(visits, claimType, chargeRates, isLimitApplied);
            if (billVisits != null && billVisits.Count > 0)
            {
                if (adjustments.IsNotNullOrEmpty())
                {
                    var serviceAdjustments = adjustments.ToObject<List<ServiceAdjustmentWrapper>>();
                    if (adjustments != null && serviceAdjustments.Count > 0)
                    {
                        foreach (var visit in billVisits)
                        {
                            var foundAdjustments = serviceAdjustments.Where(w => w.EventId == visit.EventId).Select(s => new ServiceAdjustment()
                            {
                                AdjData = s.AdjData,
                                AdjGroup = s.AdjGroup
                            }).ToList();
                            if (foundAdjustments != null)
                            {
                                visit.Adjustments = foundAdjustments;
                            }
                        }
                    }
                }
                else
                {
                    //TODO Need proper xml data for remittance to do
                }
            }
            return billVisits;
        }


        //public AgencyInsurance SecondaryToInsuranceNameAndAddress(SecondaryClaim secondaryClaim, bool IsAddressNeeded)
        //{
        //    if (secondaryClaim == null)
        //    {
        //        return null;
        //    }
        //    return ClaimToInsuranceForNameAndAddress<SecondaryClaim>(secondaryClaim, secondaryClaim.AgencyLocationId, secondaryClaim.Insurance, secondaryClaim.SecondaryInsuranceId, claim => (claim.IsInfoVerified && claim.Insurance.IsNotNullOrEmpty() || ((claim.Status != (int)ManagedClaimStatus.ClaimCreated && claim.Status != (int)ManagedClaimStatus.ClaimReOpen) && claim.Insurance.IsNotNullOrEmpty())), IsAddressNeeded);
        //}

        public RemittanceViewData GetRemittanceViewData(DateTime StartDate, DateTime EndDate)
        {
            var viewData = new RemittanceViewData { Service=this.Service};
            viewData.List = billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate);
            var allPermission = Current.CategoryService(this.Service, ParentPermission.Remittance, new int[] { (int)PermissionActions.Export, (int)PermissionActions.Print, (int)PermissionActions.Upload, (int)PermissionActions.ViewList, (int)PermissionActions.Delete, (int)PermissionActions.ViewDetail });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.PrintPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None);
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
                viewData.UploadPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Upload, AgencyServices.None);
                viewData.DeletePermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None);
                viewData.ViewDetailPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewDetail, AgencyServices.None);
            }
            return viewData;
        }

        public IList<RemittanceLean> GetRemittances(DateTime StartDate, DateTime EndDate)
        {
            return billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate);
        }

        public bool PostRemittance(Guid Id, List<string> Episodes)
        {
            var dictionary = this.RemittanceDataDictionary(Episodes);
            if (dictionary != null && dictionary.Count > 0)
            {
                var remittance = billingRepository.GetRemittance(Current.AgencyId, Id);
                if (remittance != null && remittance.Data.IsNotNullOrEmpty())
                {
                    remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
                    var remittanceData = remittance.Data.ToObject<RemittanceData>();
                    if (remittanceData != null && remittanceData.Claim != null && remittanceData.Claim.Count > 0)
                    {
                        if (remittanceData.Claim != null && remittanceData.Claim.Count > 0)
                        {
                            remittanceData.Claim.ForEach(c =>
                            {
                                var claimInformations = c.ClaimPaymentInformation;
                                if (claimInformations != null && claimInformations.Count > 0)
                                {
                                    foreach (var info in claimInformations)
                                    {
                                        if (info.PayerClaimControlNumber.IsNotNullOrEmpty() && dictionary.ContainsKey(info.PayerClaimControlNumber) && info.ClaimStatusCode.IsInteger())
                                        {
                                            var datas = dictionary[info.PayerClaimControlNumber];
                                            if (datas != null && datas.Length == 2)
                                            {
                                                var episodeId = datas[0].IsNotNullOrEmpty() && datas[0].IsGuid() ? datas[0].ToGuid() : Guid.Empty;
                                                if (!episodeId.IsEmpty())
                                                {
                                                    info.RemittanceDate = remittance.RemittanceDate;
                                                    if (info.ClaimStatementPeriodStartDate.IsEqual(info.ClaimStatementPeriodEndDate))
                                                    {
                                                        var rap = billingRepository.GetRapOnly(Current.AgencyId, episodeId);
                                                        if (rap != null)
                                                        {
                                                            var claimInfos = rap.Remittance.IsNotNullOrEmpty() ? rap.Remittance.ToObject<List<PaymentInformation>>() : new List<PaymentInformation>();
                                                            if (!claimInfos.Exists(cif => cif.RemittanceDate.Date == remittance.RemittanceDate.Date))
                                                            {
                                                                info.IsPosted = true;
                                                                long batchId = 0;
                                                                var isValidBatchId = datas[1].IsNotNullOrEmpty() ? long.TryParse(datas[1], out batchId) : false;
                                                                var isLatePost = claimInfos.Exists(cif => cif.RemittanceDate.Date > remittance.RemittanceDate.Date);
                                                                if (!isLatePost)
                                                                {
                                                                    rap.Status = info.ClaimStatusCode.ToInteger();
                                                                }
                                                                info.RemitId = remittance.RemitId;
                                                                info.Id = remittance.Id;
                                                                info.BatchId = batchId;
                                                                claimInfos.Add(info);
                                                                var total = claimInfos.Where(ct => ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                rap.Payment = total;
                                                                rap.Remittance = claimInfos.ToXml();
                                                                rap.PaymentDate = remittance.PaymentDate;
                                                                if (billingRepository.UpdateRapModel(rap))
                                                                {
                                                                    if (isValidBatchId)
                                                                    {
                                                                        var rapSnapShot = billingRepository.GetRapSnapShot(Current.AgencyId, episodeId, batchId);
                                                                        if (rapSnapShot != null)
                                                                        {
                                                                            if (!isLatePost)
                                                                            {
                                                                                rapSnapShot.Status = info.ClaimStatusCode.ToInteger();
                                                                            }
                                                                            var totalSnapShot = claimInfos.Where(ct => ct.BatchId == batchId && ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                            rapSnapShot.Payment = totalSnapShot;
                                                                            rapSnapShot.PaymentDate = remittance.PaymentDate;
                                                                            billingRepository.UpdateRapSnapShots(rapSnapShot);
                                                                        }
                                                                    }
                                                                    Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPRemittancePosted, string.Empty);

                                                                }
                                                                else
                                                                {
                                                                    info.IsPosted = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var exisitingPost = claimInfos.FirstOrDefault(cif => cif.RemittanceDate.Date == remittance.RemittanceDate.Date);
                                                                if (exisitingPost != null && exisitingPost.RemitId.IsEqual(remittance.RemitId) && exisitingPost.Id == remittance.Id)
                                                                {
                                                                    info.IsPosted = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var final = billingRepository.GetFinalOnly(Current.AgencyId, episodeId);
                                                        if (final != null)
                                                        {
                                                            var claimInfos = final.Remittance.IsNotNullOrEmpty() ? final.Remittance.ToObject<List<PaymentInformation>>() : new List<PaymentInformation>();
                                                            if (!claimInfos.Exists(cif => cif.RemittanceDate.Date == remittance.RemittanceDate.Date))
                                                            {
                                                                long batchId = 0;
                                                                var isValidBatchId = datas[1].IsNotNullOrEmpty() ? long.TryParse(datas[1], out batchId) : false;
                                                                info.IsPosted = true;
                                                                info.RemitId = remittance.RemitId;
                                                                info.Id = remittance.Id;
                                                                info.BatchId = batchId;
                                                                claimInfos.Add(info);
                                                                final.Remittance = claimInfos.ToXml();
                                                                var isLatePost = claimInfos.Exists(cif => cif.RemittanceDate.Date > remittance.RemittanceDate.Date);
                                                                if (!isLatePost)
                                                                {
                                                                    final.Status = info.ClaimStatusCode.ToInteger();
                                                                }
                                                                var total = claimInfos.Where(ct => ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                final.Payment = total;
                                                                final.PaymentDate = remittance.PaymentDate;
                                                                if (billingRepository.UpdateFinalModel(final))
                                                                {
                                                                    if (isValidBatchId)
                                                                    {
                                                                        var finalSnapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, episodeId, batchId);
                                                                        if (finalSnapShot != null)
                                                                        {
                                                                            if (!isLatePost)
                                                                            {
                                                                                finalSnapShot.Status = info.ClaimStatusCode.ToInteger();
                                                                            }
                                                                            var totalSnapShot = claimInfos.Where(ct => ct.BatchId == batchId && ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                            finalSnapShot.Payment = totalSnapShot;
                                                                            finalSnapShot.PaymentDate = remittance.PaymentDate;
                                                                            billingRepository.UpdateFinalSnapShots(finalSnapShot);
                                                                        }
                                                                    }
                                                                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalRemittancePosted, string.Empty);
                                                                }
                                                                else
                                                                {
                                                                    info.IsPosted = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var exisitingPost = claimInfos.FirstOrDefault(cif => cif.RemittanceDate.Date == remittance.RemittanceDate.Date);
                                                                if (exisitingPost != null && exisitingPost.RemitId.IsEqual(remittance.RemitId) && exisitingPost.Id == remittance.Id)
                                                                {
                                                                    info.IsPosted = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        }
                        remittance.Data = remittanceData.ToXml();
                    }
                    return billingRepository.UpdateRemittance(remittance);
                }
            }
            return false;
        }

        public bool DeleteRemittance(Guid Id)
        {
            return billingRepository.DeleteRemittance(Current.AgencyId, Id);
        }

        public Remittance GetRemittanceWithClaims(Guid Id)
        {
            var remittance = billingRepository.GetRemittance(Current.AgencyId, Id);// database.Single<Remittance>(r => r.AgencyId == agencyId && r.Id == Id);
            if (remittance != null && remittance.Data.IsNotNullOrEmpty())
            {
                var allPermission = Current.CategoryService(this.Service, ParentPermission.Remittance, new int[] { (int)PermissionActions.Print, (int)PermissionActions.PostRemittance });
                if (allPermission.IsNotNullOrEmpty())
                {
                    remittance.IsUserCanPost = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.PostRemittance, AgencyServices.None).Has(this.Service);
                    remittance.IsUserCanPrint = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None).Has(this.Service);
                }
                remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
                var remittanceData = remittance.Data.ToObject<RemittanceData>();
                if (remittanceData != null && remittanceData.Claim != null && remittanceData.Claim.Count > 0)
                {
                    if (remittanceData.Claim != null && remittanceData.Claim.Count > 0)
                    {
                        var allClaimInformations = remittanceData.Claim.SelectMany(rc => rc.ClaimPaymentInformation);
                        if (allClaimInformations != null && allClaimInformations.Count() > 0)
                        {
                            var rapsnapshots = new List<ClaimLean>();
                            var finalsnapshots = new List<ClaimLean>();

                            var rapClaimInformations = allClaimInformations.Where(cr => cr.Patient != null && !cr.IsPosted && cr.Patient.IdQualifierName.IsEqual("MedicareNumber") && cr.ClaimStatementPeriodStartDate.IsValidPHPDate() && cr.Patient.Id.IsNotNullOrEmpty() && cr.ClaimStatementPeriodStartDate.IsEqual(cr.ClaimStatementPeriodEndDate)).ToList();
                            if (rapClaimInformations != null && rapClaimInformations.Count > 0)
                            {
                                rapsnapshots = billingRepository.GetManySnapShots(rapClaimInformations, Current.AgencyId, "RAP");
                            }
                            var finalClaimInformations = allClaimInformations.Where(cr => cr.Patient != null && !cr.IsPosted && cr.Patient.IdQualifierName.IsEqual("MedicareNumber") && cr.ClaimStatementPeriodStartDate.IsValidPHPDate() && cr.Patient.Id.IsNotNullOrEmpty() && !cr.ClaimStatementPeriodStartDate.IsEqual(cr.ClaimStatementPeriodEndDate)).ToList();
                            if (finalClaimInformations != null && finalClaimInformations.Count > 0)
                            {
                                finalsnapshots = billingRepository.GetManySnapShots(finalClaimInformations, Current.AgencyId, "FINAL");
                            }
                            remittanceData.Claim.ForEach(c =>
                            {
                                var claimInformations = c.ClaimPaymentInformation;
                                if (claimInformations != null && claimInformations.Count > 0)
                                {
                                    claimInformations.ForEach(info =>
                                    {
                                        if (info.Patient != null && !info.IsPosted)
                                        {
                                            if (info.Patient.IdQualifierName.IsEqual("MedicareNumber") && info.ClaimStatementPeriodStartDate.IsValidPHPDate() && info.Patient.Id.IsNotNullOrEmpty())
                                            {
                                                if (info.ClaimStatementPeriodStartDate.IsEqual(info.ClaimStatementPeriodEndDate))
                                                {
                                                    info.AssociatedClaims = rapsnapshots.Where(w => w.MedicareNumber.IsNotNullOrEmpty() && w.MedicareNumber.Trim().ToLower() == info.Patient.Id.Trim().ToLower() && w.EpisodeStartDate.Equals(info.ClaimStatementPeriodStartDate.ToDateTimePHP())).ToList();
                                                }
                                                else
                                                {
                                                    info.AssociatedClaims = finalsnapshots.Where(w => w.MedicareNumber.IsNotNullOrEmpty() && w.MedicareNumber.Trim().ToLower() == info.Patient.Id.Trim().ToLower() && w.EpisodeStartDate.Equals(info.ClaimStatementPeriodStartDate.ToDateTimePHP())).ToList();
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                    remittance.RemittanceData = remittanceData;
                }
            }
            return remittance;
        }

        public List<PaymentInformation> GetClaimRemittance(Guid Id, string type)
        {
            var claimInfos = new List<PaymentInformation>();
            if (type.IsNotNullOrEmpty())
            {
                if (IsRap(type))
                {
                    var rap = billingRepository.GetRapOnly(Current.AgencyId, Id);
                    if (rap != null && rap.Remittance.IsNotNullOrEmpty())
                    {
                        claimInfos = rap.Remittance.ToObject<List<PaymentInformation>>();
                    }
                }
                else if (IsFinal(type))
                {
                    var final = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                    if (final != null && final.Remittance.IsNotNullOrEmpty())
                    {
                        claimInfos = final.Remittance.ToObject<List<PaymentInformation>>();
                    }
                }
            }
            return claimInfos;
        }

        public bool AddRemittanceUpload(HttpPostedFileBase file)
        {
            var result = false;
            var streamReader = new StreamReader(file.InputStream);
            var remit = new RemitQueue
            {
                Id = Guid.NewGuid(),
                AgencyId = Current.AgencyId,
                Data = streamReader.ReadToEnd(),
                IsUpload = true,
                Status = RemitQueueStatus.queued.ToString()
            };
            if (billingRepository.AddRemitQueue(remit))
            {
                try
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UrlSettings.RemittanceSchedulerUrl);
                    request.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream receiveStream = response.GetResponseStream();
                        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                        StreamReader readStream = new StreamReader(receiveStream, encode);
                        var strResult = readStream.ReadToEnd();
                    }
                }
                catch (Exception)
                {
                    return true;
                }
                result = true;
            }
            return result;
        }

        public RemittancesPdf GetRemittanceListPdf(DateTime StartDate, DateTime EndDate)
        {
            return new RemittancesPdf(billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate).ToList<RemittanceLean>(), agencyRepository.GetMainLocation(Current.AgencyId));
        }

        public RemittancePdf GetRemittancePdf(Guid Id)
        {
            var remittance = billingRepository.GetRemittance(Current.AgencyId, Id);
            if (remittance != null && remittance.Data.IsNotNullOrEmpty()) remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
            return new RemittancePdf(remittance, agencyRepository.GetMainLocation(Current.AgencyId));
        }

        public ClaimData GetClaimData(int batchId)
        {
            return billingRepository.GetClaimData(Current.AgencyId, batchId);
        }

        public List<ClaimDataLean> GetClaimDatas(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            return billingRepository.ClaimDatas(Current.AgencyId, StartDate, EndDate, ClaimType);
        }

        public List<ClaimInfoDetail> GetSubmittedBatchClaims(int batchId)
        {
            var claimInfos = new List<ClaimInfoDetail>();
            var claimData = billingRepository.GetClaimData(Current.AgencyId, batchId);
            if (claimData != null && claimData.ClaimType.IsNotNullOrEmpty() && claimData.BillIdentifers.IsNotNullOrEmpty())
            {
                var claims = claimData.BillIdentifers.ToObject<List<ClaimInfo>>();
                if (claims != null && claims.Count > 0)
                {
                    if (claimData.ClaimType.ToUpperCase() == ClaimType.MAN.ToString())
                    {
                        var managedClaims = billingRepository.GetManagedClaimInfoDetails(Current.AgencyId, claims.Select(c => c.ClaimId).ToList());
                        if (managedClaims != null && managedClaims.Count > 0)
                        {
                            claimInfos.AddRange(managedClaims);
                        }
                    }
                    else if (claimData.ClaimType.ToUpperCase() == ClaimType.CMS.ToString() || claimData.ClaimType.ToUpperCase() == ClaimType.HMO.ToString())
                    {
                        var raps = claims.Where(c => c.ClaimType == "322" || IsRap(c.ClaimType.ToUpperCase())).ToList();
                        if (raps != null && raps.Count > 0)
                        {
                            var medicareRapClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, raps.Select(c => c.ClaimId).ToList(), "RAP");
                            if (medicareRapClaims != null && medicareRapClaims.Count > 0)
                            {
                                claimInfos.AddRange(medicareRapClaims);
                            }
                        }
                        var finals = claims.Where(c => c.ClaimType == "329" || IsFinal(c.ClaimType.ToUpperCase())).ToList();
                        if (finals != null && finals.Count > 0)
                        {
                            var medicareFinalClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, finals.Select(c => c.ClaimId).ToList(), "Final");
                            if (medicareFinalClaims != null && medicareFinalClaims.Count > 0)
                            {
                                claimInfos.AddRange(medicareFinalClaims);
                            }
                        }
                    }
                }
            }
            return claimInfos;
        }

        public IList<ClaimInfoDetail> BillingBatch(string claimType, DateTime batchDate)
        {
            var claimInfos = new List<ClaimInfoDetail>();
            if (claimType.IsNotNullOrEmpty())
            {
                var batchList = billingRepository.GetClaimDatas(Current.AgencyId, claimType, batchDate);
                if (batchList != null && batchList.Count > 0)
                {
                    batchList.ForEach(batch =>
                    {
                        if (batch.BillIdentifers.IsNotNullOrEmpty())
                        {
                            var claims = batch.BillIdentifers.ToObject<List<ClaimInfo>>();
                            if (claims != null && claims.Count > 0)
                            {
                                if (batch.ClaimType.ToUpperCase() == ClaimType.MAN.ToString())
                                {
                                    var managedClaims = billingRepository.GetManagedClaimInfoDetails(Current.AgencyId, claims.Select(c => c.ClaimId).ToList());
                                    if (managedClaims != null && managedClaims.Count > 0)
                                    {
                                        foreach (var item in managedClaims)
                                        {
                                            item.ClaimAmount = item.ProspectivePay;
                                            var prospectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(item.HippsCode, item.StartDate, item.AddressZipCode));
                                            item.ProspectivePay = prospectivePay;
                                        }
                                        claimInfos.AddRange(managedClaims);
                                    }
                                }
                                else if (batch.ClaimType.ToUpperCase() == ClaimType.CMS.ToString() || batch.ClaimType.ToUpperCase() == ClaimType.HMO.ToString())
                                {
                                    var raps = claims.Where(c => c.ClaimType == "322" || c.ClaimType.ToUpperCase() == "RAP").ToList();
                                    if (raps != null && raps.Count > 0)
                                    {
                                        var medicareRapClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, raps.Select(c => c.ClaimId).ToList(), "RAP");
                                        if (medicareRapClaims != null && medicareRapClaims.Count > 0)
                                        {
                                            foreach (var item in medicareRapClaims)
                                            {
                                                item.ClaimAmount = item.ProspectivePay;
                                                var prospectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(item.HippsCode, item.StartDate, item.AddressZipCode));
                                                item.ProspectivePay = prospectivePay;
                                            }
                                            claimInfos.AddRange(medicareRapClaims);
                                        }
                                    }
                                    var finals = claims.Where(c => c.ClaimType == "329" || c.ClaimType.ToUpperCase() == "FINAL").ToList();
                                    if (finals != null && finals.Count > 0)
                                    {
                                        var medicareFinalClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, finals.Select(c => c.ClaimId).ToList(), "Final");
                                        if (medicareFinalClaims != null && medicareFinalClaims.Count > 0)
                                        {
                                            foreach (var item in medicareFinalClaims)
                                            {
                                                item.ClaimAmount = item.ProspectivePay;
                                                var prospectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(item.HippsCode, item.StartDate, item.AddressZipCode));
                                                item.ProspectivePay = prospectivePay;
                                            }
                                            claimInfos.AddRange(medicareFinalClaims);
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
            return claimInfos;
        }

        public InsuranceAuthorizationViewData InsuranceWithAuthorization(Guid patientId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
            var viewData = new InsuranceAuthorizationViewData();
            if (profile != null)
            {
                if (profile.PrimaryInsurance == insuranceId)
                {
                    viewData.HealthPlanId = profile.PrimaryHealthPlanId;
                    viewData.GroupName = profile.PrimaryGroupName;
                    viewData.GroupId = profile.PrimaryGroupId;
                    viewData.Relationship = profile.PrimaryRelationship;
                }
                else if (profile.SecondaryInsurance == insuranceId)
                {
                    viewData.HealthPlanId = profile.SecondaryHealthPlanId;
                    viewData.GroupName = profile.SecondaryGroupName;
                    viewData.GroupId = profile.SecondaryGroupId;
                    viewData.Relationship = profile.TertiaryRelationship;
                }
                else if (profile.TertiaryInsurance == insuranceId)
                {
                    viewData.HealthPlanId = profile.TertiaryHealthPlanId;
                    viewData.GroupName = profile.TertiaryGroupName;
                    viewData.GroupId = profile.TertiaryGroupId;
                    viewData.Relationship = profile.SecondaryRelationship;
                }
                var autorizations = patientProfileRepository.GetAuthorizationsByStatus(Current.AgencyId, profile.Id, insuranceId, (AuthorizationStatusTypes.Active).ToString(), (int)AgencyServices.HomeHealth, startDate, endDate);
                if (autorizations != null && autorizations.Count > 0)
                {
                    var autorization = autorizations.FirstOrDefault();
                    var autoId = string.Empty;
                    if (autorization != null)
                    {
                        viewData.Authorization = autorization;
                        autoId = autorization.Id.ToString();
                    }
                    viewData.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
                }
                else
                {
                    viewData.Authorizations = new List<SelectListItem>();
                }
            }
            else
            {
                viewData.Authorizations = new List<SelectListItem>();
            }
            return viewData;
        }

        public List<SelectListItem> GetManagedClaimEpisodes(Guid patientId, Guid managedClaimId)
        {
            var list = new List<SelectListItem>();
            var managedClaim = billingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, managedClaimId);
            if (managedClaim != null)
            {
                var episodes = episodeRepository.GetEpisodeDatasBetween(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                if (episodes.IsNotNullOrEmpty())
                {
                    episodes.ForEach(e => list.Add(new SelectListItem
                       {
                           Text = e.StartDate.ToString("MM/dd/yyyy") + " - " + e.EndDate.ToString("MM/dd/yyyy"),
                           Value = e.Id.ToString()
                       }));
                    
                }
            }
            return list;
        }

        protected override List<Supply> GetSuppliesByFlagAppSpecific(Guid Id, Guid patientId, string Type, bool isBillable)
        {
            var supplies = new List<Supply>();
            if (ClaimTypeSubCategory.Final.ToString().Equals(Type))
            {
                var claim = billingRepository.GetFinalSupplies(Current.AgencyId, Id);
                if (claim != null && claim.Supply.IsNotNullOrEmpty())
                {
                    supplies = GetSupplies(claim.Supply, isBillable, claim.IsSupplyVerified);
                }
            }
            else if (ClaimTypeSubCategory.Secondary.ToString().Equals(Type))
            {
                var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
                if (claim != null && claim.Supply.IsNotNullOrEmpty())
                {
                    supplies = GetSupplies(claim.Supply, isBillable, claim.IsSupplyVerified);
                }
            }
            return supplies;
        }

        #region Managed Claim

        protected override bool VerifyInfoAppSpecific<C>(BaseClaim currentClaimFomDB, BaseClaim claimFromClient)
        {
            return VerifyInfoInsuranceHelper<C>(currentClaimFomDB, claimFromClient);
        }

        protected override bool VerifyInsuranceAppSpecific(BaseClaim currentClaimFomDB, BaseClaim claimFromClient)
        {
            currentClaimFomDB.Ub04Locator31 = claimFromClient.Locator31.ToXml();
            currentClaimFomDB.Ub04Locator32 = claimFromClient.Locator32.ToXml();
            currentClaimFomDB.Ub04Locator33 = claimFromClient.Locator33.ToXml();
            currentClaimFomDB.Ub04Locator34 = claimFromClient.Locator34.ToXml();
            currentClaimFomDB.Ub04Locator39 = claimFromClient.Locator39.ToXml();
            currentClaimFomDB.Ub04Locator81cca = claimFromClient.Locator81cca.ToXml();
            currentClaimFomDB.HCFALocators = claimFromClient.LocatorHCFA.ToXml();
            currentClaimFomDB.HealthPlanId = claimFromClient.HealthPlanId;
            currentClaimFomDB.GroupName = claimFromClient.GroupName;
            currentClaimFomDB.GroupId = claimFromClient.GroupId;
            currentClaimFomDB.Relationship = claimFromClient.Relationship;
            currentClaimFomDB.Authorization = claimFromClient.Authorization;
            currentClaimFomDB.AuthorizationNumber = claimFromClient.AuthorizationNumber;
            currentClaimFomDB.AuthorizationNumber2 = claimFromClient.AuthorizationNumber2;
            currentClaimFomDB.AuthorizationNumber3 = claimFromClient.AuthorizationNumber3;
            return true;
        }

        protected override void ClaimWithInsuranceAppSpecific<C>(BaseClaim claim, bool IsSetLocatorFromLocationOrInsurance, Guid optionalLocatonId)
        {
            ClaimWithInsuranceAppSpecificHelper<C>(claim, IsSetLocatorFromLocationOrInsurance, optionalLocatonId);
        }

        protected override bool UpdateOrAddPayorForClaimAppSpecific<C>(BaseClaim claim)
        {
            return UpdateOrAddInsuranceForClaim<C>(claim);
        }

        protected override void SetManagedClaimWithAssessmentInfoAppSpecific(ManagedClaim managedClaim)
        {
            if (managedClaim != null)
            {
                var episodes = episodeRepository.GetEpisodeDatasBetween(Current.AgencyId, managedClaim.PatientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                if (episodes != null && episodes.Count > 0)
                {
                    if (episodes.Count == 1)
                    {
                        var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessmentAndOasisData(episodes[0]);
                        if (assessment != null)
                        {
                            var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                            managedClaim.DiagnosisCode = assessmentQuestions.ToClaimDiagonasisCodesXml();
                            managedClaim.HippsCode = assessment.HippsCode;
                            managedClaim.ClaimKey = assessment.ClaimKey;
                            managedClaim.AssessmentType = assessment.Type;
                            managedClaim.ProspectivePay = LookUpHelper.ProspectivePayAmount(assessment.Type, assessment.HippsCode, episodes[0].StartDate, managedClaim.AddressZipCode, string.Empty);
                        }
                    }
                    else if (episodes.Count > 1)
                    {
                        managedClaim.HasMultipleEpisodes = true;
                    }
                }
            }
        }

        protected override List<Claim> GetUnProcessedAppSpecific(ClaimTypeSubCategory claimType, Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded)
        {
            var unProcessedClaims = new List<Claim>();
            switch (claimType)
            {
                case ClaimTypeSubCategory.RAP:
                    unProcessedClaims = AllUnProcessedRaps(branchId, insuranceId, IsZeroInsuraceIdAll, isUsersNeeded);
                    break;
                case ClaimTypeSubCategory.Final:
                    unProcessedClaims = AllUnProcessedFinals(branchId, insuranceId, IsZeroInsuraceIdAll, isUsersNeeded);
                    break;
            }
            return unProcessedClaims;
        }

        protected override List<ChargeRate> UpdateRatesForReloadAppSpecific<C>(BaseClaim claim)
        {
            return UpdateRatesForReloadFromInsurance<C>(claim);
        }

        protected override void SetInvoiceNumberAppSpecific(ManagedClaim claim)
        {
            //no data 
        }

        protected override void GetInvoiceNumber(ManagedClaim managedClaim, ClaimViewData claimData)
        {
            claimData.ClaimNumber = billingRepository.GetClaimNumber(managedClaim.Id);
        }

        protected override void SetPayorTypeAppSpecific(BaseClaim claim)
        {
            claim.PayorType = (int)PayorTypeMainCategory.NonPrivatePayor;
        }

        #endregion

        public double ProspectivePayAmount(Guid patientId, string hippsCode, string assessmentType, DateTime startDate)
        {
            var patient = patientProfileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
            var agencyLocation = agencyRepository.FindLocationOrMain(Current.AgencyId, patient.AgencyLocationId);
            return this.ProspectivePayAmount(assessmentType, hippsCode, startDate, patient.Address != null ? patient.Address.ZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
        }

        public double ProspectivePayAmount(string assessmentType, string hippsCode, DateTime startDate, string claimZipCode, string locationZipCode)
        {
            var value = 0.0;
            if (assessmentType.IsNotNullOrEmpty())
            {
                if (assessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || assessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || assessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString())
                {
                    value = Math.Round(lookUpRepository.ProspectivePayAmount(hippsCode, startDate, claimZipCode, locationZipCode), 2);
                }
                else
                {
                    value = Math.Round(lookUpRepository.ProspectivePayAmount(hippsCode, startDate, claimZipCode, locationZipCode), 2);
                }
            }
            return value;
        }

        protected override void SetClaimNewBillDataAppSpecific<C>(NewBillDataViewData data, Guid claimId, int payorType)
        {
            SetClaimNewBillData<C>(data, claimId);
        }

        protected override List<ChargeRate> ClaimInsuranceRatesAppSpecific<C>(Guid claimId, int payorType)
        {
            return mongoRepository.GetClaimInsuranceRates<C>(Current.AgencyId, claimId);
        }

        protected override bool ClaimAddBillDataAppSpecific<C>(ChargeRate chargeRate, Guid ClaimId, int payorType)
        {
            return ClaimAddInsuranceBillData<C>(chargeRate, ClaimId);
        }

        protected override bool SetClaimNewBillDataAppSpecific<C>(Guid ClaimId, int Id, int payorType)
        {
            return ClaimDeleteInsuranceBillData<C>(ClaimId, Id);
        }

        protected override BillingJsonViewData ClaimUpdateBillDataAppSpecific<C>(ChargeRate chargeRate, Guid ClaimId, int payorType, ClaimTypeSubCategory claimSubCategory)
        {
            return ClaimUpdateInsuranceBillData<C>(chargeRate, ClaimId, claimSubCategory);
        }

        #region PDF print


        public BaseInvoiceFormViewData GetMedicareClaimInvoiceFormViewData(Guid patientId, Guid claimId, string type)
        {
            var formViewData = new BaseInvoiceFormViewData();
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                formViewData.Agency = agency;
                var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    formViewData.AgencyLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                    if (IsRap(type))
                    {
                        formViewData.ClaimTypeSubCategory = ClaimTypeSubCategory.RAP;
                        var rap = billingRepository.GetRapOnly(Current.AgencyId, claimId);
                        if (rap != null)
                        {
                            rap.AgencyLocationId = profile.AgencyLocationId;
                            var rapClaimData = EntityHelper.GetRAPClaimViewData(rap);
                            rapClaimData.CBSA = lookUpRepository.CbsaCodeByZip(rap.AddressZipCode);
                            rapClaimData.AgencyLocationId = profile.AgencyLocationId;
                            rapClaimData.UB4PatientStatus = ((int)UB4PatientStatus.StillPatient).ToString();
                            rapClaimData.Relationship = ClaimRelationship(rapClaimData.RelationshipId.IsNotNullOrEmpty() ? rapClaimData.RelationshipId : profile.PrimaryRelationship);
                            formViewData.Claim = rapClaimData;
                            SetInvoiceFormAppSpecific<RapClaimInsurance>(formViewData, rap);
                        }
                    }
                    else if (IsFinal(type))
                    {
                        formViewData.ClaimTypeSubCategory = ClaimTypeSubCategory.Final;
                        var final = billingRepository.GetFinalOnly(Current.AgencyId, claimId);
                        if (final != null)
                        {
                            final.AgencyLocationId = profile.AgencyLocationId;
                            var finalClaimData = EntityHelper.GetFinalClaimViewData(final);
                            finalClaimData.CBSA = lookUpRepository.CbsaCodeByZip(final.AddressZipCode);
                            finalClaimData.AgencyLocationId = profile.AgencyLocationId;
                            finalClaimData.Relationship = ClaimRelationship(finalClaimData.RelationshipId.IsNotNullOrEmpty() ? finalClaimData.RelationshipId : profile.PrimaryRelationship);
                            formViewData.Claim = finalClaimData;
                            SetInvoiceFormAppSpecific<FinalClaimInsurance>(formViewData, final);
                            formViewData.Claim.SupplyTotal = this.MedicareSupplyTotal(final);
                            var schedules = formViewData.Claim.VerifiedVisit.IsNotNullOrEmpty() ? formViewData.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<ScheduleEvent>();
                            if (schedules != null && schedules.Count > 0)
                            {
                                formViewData.BillSchedules = BillableVisitSummary(schedules, formViewData.Claim.PrimaryInsuranceId > 0 && formViewData.Claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, formViewData.Claim.ChargeRates, false);
                            }
                        }
                    }
                }
            }
            return formViewData;
        }

        public BaseInvoiceFormViewData GetSecondaryClaimInvoiceFormViewData(Guid patientId, Guid claimId)
        {
            var formViewData = new BaseInvoiceFormViewData();
            formViewData.ClaimTypeSubCategory = ClaimTypeSubCategory.Secondary;
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    var secondaryClaim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, claimId);
                    if (secondaryClaim != null)
                    {
                        var claimData = EntityHelper.GetSecondaryClaimViewData(secondaryClaim);
                        claimData.AgencyLocationId = profile.AgencyLocationId;
                        claimData.CBSA = lookUpRepository.CbsaCodeByZip(secondaryClaim.AddressZipCode);
                        if (formViewData.InvoiceType == InvoiceType.HCFA)
                        {
                            claimData.HCFALocators = secondaryClaim.HCFALocators;
                        }
                        formViewData.Claim = claimData;
                        formViewData.Agency = agency;
                        formViewData.AgencyLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                        SetInvoiceFormAppSpecific<SecondaryClaimInsurance>(formViewData, secondaryClaim);
                        var schedules = secondaryClaim.VerifiedVisits.IsNotNullOrEmpty() ? secondaryClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<ScheduleEvent>();
                        if (schedules.IsNotNullOrEmpty())
                        {
                            formViewData.BillSchedules = BillableVisitSummary(schedules, formViewData.Claim.PrimaryInsuranceId > 0 && formViewData.Claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, formViewData.Claim.ChargeRates, false);

                        }
                    }
                }
            }
            return formViewData;
        }


        protected override void SetInvoiceFormAppSpecific<C>(BaseInvoiceFormViewData claimFormViewData, BaseClaim claim)
        {
            claimFormViewData.InvoiceType = InvoiceType.UB;//set deafult value
            SetInvoiceFormInsuranceInfo<C>(claimFormViewData);
        }


        #endregion

        #region Supply

        public bool AddSupplyFinal(Guid Id, Supply supply)
        {
            bool result = false;
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, Id);
            if (claim != null)
            {
                claim.Supply = AddSupplyToXml(claim.Supply, supply);
                claim.SupplyTotal = MedicareSupplyTotal(claim);
                if (billingRepository.UpdateFinalModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool AddSupplySecondary(Guid patientId, Guid Id, Supply supply)
        {
            bool result = false;
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Supply = AddSupplyToXml(claim.Supply, supply);
                if (billingRepository.UpdateSecondaryClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public Supply GetSupplyFinal(Guid ClaimId, int id)
        {
            var supply = new Supply();
            var claim = billingRepository.GetFinalSupplies(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                supply = GetSupplyFromXml(claim.Supply, id);
            }
            return supply;
        }

        public Supply GetSupplySecondary(Guid ClaimId, int id)
        {
            var supply = new Supply();
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                supply = GetSupplyFromXml(claim.Supply, id);
            }
            return supply;
        }

        public bool EditSupplyFinal(Guid Id, Supply supply)
        {
            bool result = false;
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, Id);
            if (claim != null)
            {
                claim.Supply = EditSupplyInXml(claim.Supply, supply);
                claim.SupplyTotal = MedicareSupplyTotal(claim);
                if (billingRepository.UpdateFinalModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool EditSupplySecondary(Guid patientId, Guid Id, Supply supply)
        {
            bool result = false;
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Supply = EditSupplyInXml(claim.Supply, supply);
                if (billingRepository.UpdateSecondaryClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool ChangeStatusOfSuppliesFinal(Guid patientId, Guid Id, List<int> ids, bool IsBillable)
        {
            bool result = false;
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, Id);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                claim.Supply = ChangeStatusOfSuppliesInXml(claim.Supply, IsBillable, ids);
                claim.SupplyTotal = MedicareSupplyTotal(claim);
                if (billingRepository.UpdateFinalModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool ChangeStatusOfSuppliesSecondary(Guid patientId, Guid Id, List<int> ids, bool IsBillable)
        {
            bool result = false;
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                claim.Supply = ChangeStatusOfSuppliesInXml(claim.Supply, IsBillable, ids);
                if (billingRepository.UpdateSecondaryClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteSupplyFinal(Guid patientId, Guid Id, List<int> ids)
        {
            bool result = false;
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, Id);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                claim.Supply = DeprecateSuppliesInXml(claim.Supply, ids);
                claim.SupplyTotal = MedicareSupplyTotal(claim);
                if (billingRepository.UpdateFinalModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteSupplySecondary(Guid patientId, Guid Id, List<int> ids)
        {
            bool result = false;
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                claim.Supply = DeprecateSuppliesInXml(claim.Supply, ids);
                if (billingRepository.UpdateSecondaryClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion

        protected override void SetAdmissionInfoAppSpecific(ManagedClaim managedClaim, Patient patientWithProfile)
        {
            this.SetAdmissionInfo<PatientEpisode>(managedClaim, patientWithProfile);
        }

        private bool AddSecondaryClaim(Guid patientId, Guid primaryClaimId, DateTime startDate, DateTime endDate, out string errorMessage)
        {
            errorMessage = string.Empty;
            bool result = false;
            Final final = billingRepository.GetFinalOnly(Current.AgencyId, primaryClaimId);
            if (final != null)
            {
                SecondaryClaim claim = new SecondaryClaim();
                claim.Id = Guid.NewGuid();
                claim.PrimaryClaimId = primaryClaimId;
                claim.PatientId = patientId;
                claim.EpisodeId = final.EpisodeId;
                claim.AgencyId = Current.AgencyId;
                claim.FirstName = final.FirstName;
                claim.LastName = final.LastName;
                claim.MedicareNumber = final.MedicareNumber;
                claim.PatientIdNumber = final.PatientIdNumber;
                claim.DiagnosisCode = final.DiagnosisCode;
                claim.ConditionCodes = final.ConditionCodes;
                claim.Gender = final.Gender;
                claim.DOB = final.DOB;
                claim.EpisodeStartDate = final.EpisodeStartDate;
                claim.EpisodeEndDate = final.EpisodeEndDate;
                claim.StartDate = startDate;
                claim.EndDate = endDate;
                claim.StartofCareDate = final.StartofCareDate;
                claim.AddressLine1 = final.AddressLine1;
                claim.AddressLine2 = final.AddressLine2;
                claim.AddressCity = final.AddressCity;
                claim.AddressStateCode = final.AddressStateCode;
                claim.AddressZipCode = final.AddressZipCode;
                claim.HippsCode = final.HippsCode;
                claim.ClaimKey = final.ClaimKey;
                //claim.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                claim.PhysicianLastName = final.PhysicianLastName;
                claim.PhysicianFirstName = final.PhysicianFirstName;
                claim.PhysicianNPI = final.PhysicianNPI;
                claim.AdmissionSource = final.AdmissionSource;
                claim.AssessmentType = final.AssessmentType;
                claim.UB4PatientStatus = final.UB4PatientStatus;
                claim.DischargeDate = final.DischargeDate;
                claim.HealthPlanId = final.HealthPlanId;
                claim.Authorization = final.Authorization;
                claim.AuthorizationNumber = final.AuthorizationNumber;
                claim.AuthorizationNumber2 = final.AuthorizationNumber2;
                claim.AuthorizationNumber3 = final.AuthorizationNumber3;
                claim.Ub04Locator39 = final.Ub04Locator39;
                claim.Ub04Locator81cca = final.Ub04Locator81cca;
                claim.Ub04Locator31 = final.Ub04Locator31;
                claim.Ub04Locator32 = final.Ub04Locator32;
                claim.Ub04Locator33 = final.Ub04Locator33;
                claim.Ub04Locator34 = final.Ub04Locator34;
                claim.Remittance = final.Remittance;
                claim.ProspectivePay = final.ProspectivePay;
                claim.Status = (int)ManagedClaimStatus.ClaimCreated;

                var profiel = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profiel != null && profiel.SecondaryInsurance > 0)
                {
                    claim.SecondaryInsuranceId = profiel.SecondaryInsurance;
                    claim.HealthPlanId = profiel.SecondaryHealthPlanId;
                    claim.GroupName = profiel.SecondaryGroupName;
                    claim.GroupId = profiel.SecondaryGroupId;
                }
                else
                {
                    errorMessage = "This patient does not have a secondary insurnace.";
                    return false;
                }
                result = billingRepository.AddSecondaryClaim(claim);
            }
            return result;
        }

        private Dictionary<string, string[]> RemittanceDataDictionary(List<string> datas)
        {
            var dictionary = new Dictionary<string, string[]>();
            if (datas != null && datas.Count > 0)
            {
                datas.ForEach(data =>
                {
                    var values = data.Split('|');
                    if (values != null && values.Length == 3 && !dictionary.ContainsKey(values[2]))
                    {
                        dictionary.Add(values[2], new[] { values[0], values[1] });
                    }
                });
            }
            return dictionary;
        }

        public bool IsRap(string type)
        {
            return type.IsNotNullOrEmpty() && (type.ToLowerCase().IsEqual("rap") || type == ((int)ClaimTypeSubCategory.RAP).ToString());
        }

        public bool IsFinal(string type)
        {
            return type.IsNotNullOrEmpty() && (type.ToLowerCase().IsEqual("final") || type == ((int)ClaimTypeSubCategory.Final).ToString());
        }

      

    }


}
