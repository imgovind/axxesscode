﻿namespace Axxess.AgencyManagement.Application.Services
{
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

   public class HHCommunicationNoteService:CommunicationNoteService<ScheduleEvent,PatientEpisode>
    {
        private HHTaskRepository scheduleRepository;
        private HHCommunicationNoteRepository noteRepository;
        private HHEpisodeRepository episodeRepository;

        public HHCommunicationNoteService(HHDataProvider dataProvider)
            : base(dataProvider.TaskRepository, dataProvider.CommunicationNoteRepository, dataProvider.EpisodeRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.CommunicationNoteRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.HomeHealth;

        }
        #region Commuincation Notes

        protected override CommunicationNote GetCommunicationNoteEpisodeData(CommunicationNote communicationNote)
        {
            if (!communicationNote.EpisodeId.IsEmpty())
            {
                var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId);
                if (episode != null)
                {
                    communicationNote.EpisodeEndDate = episode.EndDateFormatted;
                    communicationNote.EpisodeStartDate = episode.StartDateFormatted;
                }
            }
            return communicationNote;
        }

        #endregion
    }
}
