﻿using System;
using System.Collections.Generic;
using System.Linq;

using Axxess.AgencyManagement.Entities;
using Axxess.Core.Extension;
using Axxess.Core.Enums;
using Axxess.AgencyManagement.Application.Domain;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Application.Enums;

namespace Axxess.AgencyManagement.Application.Services
{
    public class HHOrderManagmentService : OrderManagmentService<ScheduleEvent, PatientEpisode>
    {
        private readonly HHEpiosdeService episodeService;
        private readonly FaceToFaceEncounterService faceToFaceEncounterService;
        private readonly HHTaskService scheduleService;
        public HHOrderManagmentService(
            IAgencyService agencyService,
            HHTaskService scheduleService,
            HHPhysicianOrderService physicianOrderService,
            HHPlanOfCareService planOfCareService,
            HHNoteService noteService,
            HHEpiosdeService epiosdeService,
            FaceToFaceEncounterService faceToFaceEncounterService)
            : base(agencyService, physicianOrderService, scheduleService, planOfCareService, noteService)
        {
            this.episodeService = epiosdeService;
            this.faceToFaceEncounterService = faceToFaceEncounterService;
            this.scheduleService = scheduleService;
            base.Service = AgencyServices.HomeHealth;
        }

        protected override Order GetOrderForHistoryEditAppSpecific(int typeNum,Guid id)
        {
            var order = new Order();
            switch (typeNum)
            {
                case (int)OrderType.FaceToFaceEncounter:
                    order = faceToFaceEncounterService.GetFaceToFaceEncounterForHistoryEdit(id);
                    break;
            }
            return order;
        }

        protected override Order GetOrderForReceivingAppSpecific(int typeNum, Guid patientId, Guid id)
        {
            var order = new Order();
            switch (typeNum)
            {
                case (int)OrderType.FaceToFaceEncounter:
                    order = faceToFaceEncounterService.GetFaceToFaceEncounterForReceiving(patientId, id);
                    break;
            }
            return order;
            
        }

        protected override int MarkOrdersAsSentAppSpecific(int type, bool sendElectronically, string[] answers, ref List<AgencyPhysician> physicians)
        {
            var successfulCount = 0;
            switch (type)
            {
                case (int)OrderType.FaceToFaceEncounter:
                    successfulCount += faceToFaceEncounterService.MarkFaceToFaceEncounterAsSent(sendElectronically, answers, ref physicians);
                    break;
            }
            return successfulCount;
        }

        protected override void GetOrderStatusAppSpecific(bool isPending, bool isCompleted, List<Order> orders, List<ScheduleEvent> schedules)
        {
            var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
            if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
            {
                var faceToFaceEncounters = faceToFaceEncounterService.GetFaceToFaceEncounterOrders(faceToFaceEncounterSchedules, isPending, isCompleted);
                if (faceToFaceEncounters.IsNotNullOrEmpty())
                {
                    orders.AddRange(faceToFaceEncounters);
                }
            }
        }

        protected override void GetPatientOrdersAppSpecific(Guid patientId, List<Order> orders, List<ScheduleEvent> schedules)
        {
            var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
            if (faceToFaceEncounterSchedules.IsNotNullOrEmpty())
            {
                var faceToFaceEncounters = faceToFaceEncounterService.GetFaceToFaceEncounterOrders(patientId, faceToFaceEncounterSchedules);
                if (faceToFaceEncounters.IsNotNullOrEmpty())
                {
                    orders.AddRange(faceToFaceEncounters);
                }
            }
        }

        protected override bool MarkOrderAsReturnedAppSpecifc(Guid id, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            switch (type)
            {
                case OrderType.FaceToFaceEncounter:
                    result = faceToFaceEncounterService.MarkFaceToFaceEncounterAsReturned(id, receivedDate, physicianSignatureDate);
                    break;
            }
            return result;
        }

        protected override bool UpdateOrderDatesAppSpecifc(OrderType type, Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            switch (type)
            {
                case OrderType.FaceToFaceEncounter:
                    result = faceToFaceEncounterService.UpdateFaceToFaceOrderDates(id, receivedDate, sendDate, physicianSignatureDate);
                    break;
            }
            return result;
        }

        public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId)
        {
            var orders = new List<Order>();
            var episodeRange = episodeService.GetEpisodeDateRange(patientId, episodeId);
            if (episodeRange != null)
            {
                var episode = new PatientEpisode() { Id = episodeId, PatientId = patientId, StartDate = episodeRange.StartDate, EndDate = episodeRange.EndDate };
                var schedules = scheduleService.GetCurrentAndPreviousOrders(episode);
                if (schedules != null && schedules.Count > 0)
                {
                    var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                    if (physicianOrdersSchedules.Count > 0)
                    {
                        var physicianOrders = physicianOrderService.GetPhysicianOrders(patientId, episodeRange.StartDate, episodeRange.EndDate, physicianOrdersSchedules);
                        if (physicianOrders.IsNotNullOrEmpty())
                        {
                            orders.AddRange(physicianOrders);
                        }
                    }
                    var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                    if (planofCareOrdersSchedules.Count > 0)
                    {
                        var planofCareOrders = planOfCareService.GetPlanOfCareOrders(patientId, planofCareOrdersSchedules);
                        if (planofCareOrders.IsNotNullOrEmpty())
                        {
                            orders.AddRange(planofCareOrders);
                        }
                    }

                    var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                    if (faceToFaceEncounterSchedules.Count > 0)
                    {
                        var faceToFaceEncounters = faceToFaceEncounterService.GetFaceToFaceEncounterOrders(patientId, faceToFaceEncounterSchedules);
                        if (faceToFaceEncounters.IsNotNullOrEmpty())
                        {
                            orders.AddRange(faceToFaceEncounters);
                        }
                    }
                    var evalOrdersSchedule = schedules.Where(s => DisciplineTaskFactory.AllNoteOrders().Contains(s.DisciplineTask)).ToList();
                    if (evalOrdersSchedule.Count > 0)
                    {
                        var evalOrders = noteService.GetEvalOrders(patientId, evalOrdersSchedule);
                        if (evalOrders.IsNotNullOrEmpty())
                        {
                            orders.AddRange(evalOrders);
                        }
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }
    }
}
