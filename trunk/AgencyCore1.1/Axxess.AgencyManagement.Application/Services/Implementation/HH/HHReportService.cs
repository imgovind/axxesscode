﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Repositories;

    public class HHReportService : ReportService<ScheduleEvent, PatientEpisode>
    {
        #region Constructor and Private Members

        //private readonly HHPatientProfileRepository profileRepository;
        //private readonly HHPatientAdmissionRepository admissionRepository;
        //private readonly HHPlanOfCareRepository planofCareRepository;
        //private readonly HHPhysicianOrderRepository physicianOrderRepository;

        private readonly HHBillingRepository billingRepository;
        private readonly HHTaskRepository scheduleRepository;
        private readonly HHEpisodeRepository episodeRepository;


        public HHReportService(HHDataProvider dataProvider, ILookUpDataProvider lookUpDataProvider)
            : base(dataProvider.PatientProfileRepository, dataProvider.BillingRepository, dataProvider.PatientAdmissionRepository, dataProvider.PlanOfCareRepository, dataProvider.TaskRepository, dataProvider.EpisodeRepository, dataProvider.PhysicianOrderRepository, dataProvider.MongoRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            base.agencyRepository = dataProvider.AgencyRepository;
            base.userRepository = dataProvider.UserRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.lookUpRepository = lookUpDataProvider.LookUpRepository;

            //this.profileRepository = dataProvider.PatientProfileRepository;
            //this.planofCareRepository = dataProvider.PlanOfCareRepository;
            //this.physicianOrderRepository = dataProvider.PhysicianOrderRepository;
            //this.admissionRepository = dataProvider.PatientAdmissionRepository;
            
            this.billingRepository = dataProvider.BillingRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;

            base.Service = AgencyServices.HomeHealth;
        }

        #endregion

        #region Patient Reports

        public List<PatientSocCertPeriod> GetPatientSocCertPeriod(Guid branchId, int statusId, DateTime startDate, DateTime endDate)
        {
            var socCertPeriod = new List<PatientSocCertPeriod>();
            var patients = profileRepository.GetPatientPhysicianInfos(Current.AgencyId, branchId, statusId);
            if (patients != null && patients.Count > 0)
            {
                var patientIds = patients.Select(r => r.Id).Distinct().ToList();
                var ids = patientIds.Select(Id => string.Format("'{0}'", Id)).ToArray().Join(", ");
                if (ids.IsNotNullOrEmpty())
                {
                    socCertPeriod = episodeRepository.PatientSocCertPeriods(Current.AgencyId, ids, startDate, endDate);
                    if (socCertPeriod != null && socCertPeriod.Count > 0)
                    {
                        var users = new List<User>();
                        var userIds = patients.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (userIds != null && userIds.Count > 0)
                        {
                            users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                        }
                        var physicians = physicianRepository.GetPatientsPrimaryPhysician(Current.AgencyId, patientIds) ?? new List<AgencyPhysician>();
                        
                        socCertPeriod.ForEach(soc =>
                        {
                            var tempPatient = patients.FirstOrDefault(p => p.Id == soc.Id);
                            if (tempPatient != null)
                            {
                                if (soc.PatientData.IsNotNullOrEmpty())
                                {
                                    var patient = soc.PatientData.ToObject<Patient>();
                                    if (patient != null)
                                    {
                                        soc.PatientFirstName = patient.FirstName.ToUpperCase();
                                        soc.PatientLastName = patient.LastName.ToUpperCase();
                                        soc.PatientMiddleInitial = patient.MiddleInitial.ToInitial();
                                        soc.PatientPatientID = patient.PatientIdNumber;
                                       // soc.PatientSoC = patient.StartofCareDate;
                                    }
                                }
                                else
                                {
                                    soc.PatientFirstName = tempPatient.FirstName.ToUpperCase();
                                    soc.PatientLastName = tempPatient.LastName.ToUpperCase();
                                    soc.PatientMiddleInitial = tempPatient.MiddleInitial.ToInitial();
                                    soc.PatientPatientID = tempPatient.PatientIdNumber;
                                }
                                if (!tempPatient.UserId.IsEmpty())
                                {
                                    var user = users.FirstOrDefault(u => u.Id == tempPatient.UserId);
                                    if (user != null)
                                    {
                                        soc.respEmp = user.DisplayName;
                                    }
                                }
                                if (!tempPatient.Id.IsEmpty())
                                {
                                    var physician = physicians.FirstOrDefault(u => u.PatientId == tempPatient.Id);
                                    if (physician != null)
                                    {
                                        soc.PhysicianName = physician.DisplayName;
                                    }
                                }
                            }
                        });
                    }
                }
            }
            return socCertPeriod.OrderBy(r => r.PatientLastName).ThenBy(r => r.PatientFirstName).ToList();
        }

        private List<PatientRoster> GetPatientRoster(List<PatientRoster> rosterList, Guid branchCode, int statusId, int insuranceId, bool isExcel)
        {
            if (rosterList.IsNotNullOrEmpty())
            {
                var physicians = new List<AgencyPhysician>();
                
                    if (isExcel)
                    {
                        var patientIds = rosterList.Select(r => r.Id).Distinct().ToList();
                        physicians = physicianRepository.GetPatientsPrimaryPhysician(Current.AgencyId, patientIds) ?? new List<AgencyPhysician>();
                    }
                    var insuranceIds = rosterList.Where(s => s.PatientInsuranceId.IsInteger() && s.PatientInsuranceId.ToInteger() > 0).Select(i => i.PatientInsuranceId.ToInteger()).Distinct().ToList();
                    var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                    rosterList.ForEach(roster =>
                    {
                        if (isExcel)
                        {
                            //TODO:needs work
                            //var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, roster.Id, DateTime.Now, "Nursing");
                            //IDictionary<string, Question> lastAssessment = null;
                            //if (lastEpisode != null && !lastEpisode.AssessmentId.IsEmpty() && lastEpisode.AssessmentType.IsNotNullOrEmpty())
                            //{
                            //    lastAssessment = assessmentService.GetAssessment(lastEpisode.AssessmentId, lastEpisode.AssessmentType).ToDictionary();
                            //}
                            //if (lastAssessment != null && lastAssessment.ContainsKey("M1020PrimaryDiagnosis"))
                            //{
                            //    roster.PatientPrimaryDiagnosis = lastAssessment["M1020PrimaryDiagnosis"].Answer;
                            //}
                            //else
                            //{
                            //    roster.PatientPrimaryDiagnosis = "";
                            //}
                            //if (lastAssessment != null && lastAssessment.ContainsKey("M1022PrimaryDiagnosis1"))
                            //{
                            //    roster.PatientSecondaryDiagnosis = lastAssessment["M1022PrimaryDiagnosis1"].Answer;
                            //}
                            //else
                            //{
                            //    roster.PatientSecondaryDiagnosis = "";
                            //}
                           
                            if (!roster.Id.IsEmpty())
                            {
                                var physician = physicians.FirstOrDefault(p => p.PatientId == roster.Id);// PhysicianEngine.Get(roster.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    roster.PhysicianNpi = physician.NPI;
                                    roster.PhysicianName = physician.DisplayName;
                                    roster.PhysicianPhone = physician.PhoneWork.ToPhone();
                                    roster.PhysicianFacsimile = physician.FaxNumber;
                                    roster.PhysicianPhoneHome = physician.PhoneAlternate;
                                    roster.PhysicianEmailAddress = physician.EmailAddress;
                                }
                            }
                        }
                        var insurance = insurances.FirstOrDefault(i => i.Id.ToString() == roster.PatientInsuranceId);
                        if (insurance != null)
                        {
                            roster.PatientInsuranceName = insurance.Name;
                        }
                    });
                
            }
            return rosterList.OrderBy(o => o.PatientDisplayName).ToList();
        }

        protected override void SurveyCensusAppSpecific(List<SurveyCensus> surveyCensuses, List<User> users, List<AgencyPhysician> physicians, List<InsuranceLean> insurances, bool isExcel, List<Guid> patientIds)
        {
            var idsQuery = patientIds.Select(Id => string.Format("'{0}'", Id)).ToArray().Join(", ");
            var disciplineTasksOASISSOC = DisciplineTaskFactory.SOCDisciplineTasks(true).ToArray();
            var disciplineTasksOASISROCAndRecert = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();
            var surveyCensuseEpisodesWithEvents = scheduleRepository.GetEpisodesForSurveyCensesAndPatientRoster(Current.AgencyId, idsQuery);
            var assessmentIds = surveyCensuseEpisodesWithEvents.Where(w => disciplineTasksOASISSOC.Contains(w.DisciplineTask) || disciplineTasksOASISROCAndRecert.Contains(w.DisciplineTask)).Select(s => s.Id).Distinct().ToList();
            var assessmentDatas = mongoRepository.GetManyAssessmentQuestions(Current.AgencyId, assessmentIds);
            var isEpisodeEventsExist = surveyCensuseEpisodesWithEvents.IsNotNullOrEmpty();
            surveyCensuses.ForEach(surveyCensus =>
            {
                if (isEpisodeEventsExist)
                {
                    SurveyCensusDiagnosisAndDiscipline(surveyCensus, surveyCensuseEpisodesWithEvents, disciplineTasksOASISSOC, disciplineTasksOASISROCAndRecert, assessmentDatas);
                }
                SetUserInsuranceAndPhysician(surveyCensus, users, physicians, insurances, isExcel);
            });
        }

        private void SurveyCensusDiagnosisAndDiscipline(SurveyCensus surveyCensus, List<ScheduleEvent> surveyCensuseEpisodesWithEvents, int [] disciplineTasksOASISSOC, int [] disciplineTasksOASISROCAndRecert, List<AssessmentQuestionData> assessmentDatas)
        {
            var patientSurveyCensuseEpisodes = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id).GroupBy(s => s.EpisodeId).Select(s => s.First()).ToList();
            // var patientSurveyCensuseEpisodes = surveyCensuseEpisodes.Where(s => s.Id == surveyCensus.Id).OrderByDescending(s => s.StartDate).ToList();
            if (patientSurveyCensuseEpisodes != null && patientSurveyCensuseEpisodes.Count > 0)
            {
                var episode = patientSurveyCensuseEpisodes.OrderByDescending(s => s.StartDate).FirstOrDefault();
                //patientSurveyCensuseEpisodes.FirstOrDefault();//patientRepository.GetCurrentEpisodeOnly(Current.AgencyId, surveyCensus.Id);
                if (episode != null)
                {
                    surveyCensus.CertPeriod = (episode.StartDate.IsValid() ? episode.StartDate.ToString("MM/dd/yyyy") : "") + " - " + (episode.EndDate.IsValid() ? episode.EndDate.ToString("MM/dd/yyyy") : "");
                    var schedules = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == episode.PatientId && s.EpisodeId == episode.EpisodeId).ToList();
                    if (schedules != null && schedules.Count > 0)
                    {
                        var scheduleEvent = schedules.Where(s => s.EpisodeId == episode.EpisodeId && disciplineTasksOASISSOC.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate).FirstOrDefault();
                        if (scheduleEvent != null)
                        {
                            var assessmentData = assessmentDatas.FirstOrDefault(asd => asd.Id == scheduleEvent.Id);
                            if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                            {
                                var dictionary = assessmentData.Question.ToOASISDictionary();
                                if (dictionary != null && dictionary.Count > 0)
                                {
                                    surveyCensus.PrimaryDiagnosis = dictionary.AnswerOrEmptyString("M1020PrimaryDiagnosis");
                                    surveyCensus.SecondaryDiagnosis = dictionary.AnswerOrEmptyString("M1022PrimaryDiagnosis1");
                                }
                            }
                        }
                        else
                        {
                            var previousEpisode = patientSurveyCensuseEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                            if (previousEpisode != null)
                            {
                                var previousSchedule = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
                                {
                                    var assessmentData = assessmentDatas.FirstOrDefault(asd => asd.Id == previousSchedule.Id);
                                    if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                                    {
                                        var dictionary = assessmentData.Question.ToOASISDictionary();
                                        if (dictionary != null && dictionary.Count > 0)
                                        {
                                            surveyCensus.PrimaryDiagnosis = dictionary.AnswerOrEmptyString("M1020PrimaryDiagnosis");
                                            surveyCensus.SecondaryDiagnosis = dictionary.AnswerOrEmptyString("M1022PrimaryDiagnosis1");
                                        }
                                    }
                                }
                            }
                        }
                        surveyCensus.Discipline = schedules.Discipline<ScheduleEvent>().Join(",");
                    }
                    else
                    {
                        var previousEpisode = patientSurveyCensuseEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        if (previousEpisode != null)
                        {
                            var previousSchedule = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                            if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
                            {
                                var assessmentData = assessmentDatas.FirstOrDefault(asd => asd.Id == previousSchedule.Id);
                                if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                                {
                                    var dictionary = assessmentData.Question.ToOASISDictionary();
                                    if (dictionary != null && dictionary.Count > 0)
                                    {
                                        surveyCensus.PrimaryDiagnosis = dictionary.AnswerOrEmptyString("M1020PrimaryDiagnosis");
                                        surveyCensus.SecondaryDiagnosis = dictionary.AnswerOrEmptyString("M1022PrimaryDiagnosis1");
                                    }
                                }
                            }
                        }
                    }
                    //var assessment = GetEpisodeAssessment(episode, patientSurveyCensuseEpisodes.Count >= 2 ? patientSurveyCensuseEpisodes[1] : null);
                    //if (assessment != null)
                    //{
                    //    var diagnosis = assessment.Diagnosis();
                    //    if (diagnosis != null && diagnosis.Count > 0)
                    //    {
                    //        surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                    //        surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                    //    }
                    //}
                    //if (episode.Schedule.IsNotNullOrEmpty())
                    //{
                    //    surveyCensus.Discipline = episode.Schedule.ToObject<List<ScheduleEvent>>().Discipline().Join(",");
                    //}
                }
            }
        }


        #endregion

        #region Clinical Reports

        public IList<OpenOasis> GetAllOpenOasis(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            var openOasisList = new List<OpenOasis>();
            var oasis = DisciplineTaskFactory.AllAssessments(true);
            var scheduleEvents = scheduleRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchCode, startDate, endDate, 0, new string[] { }, oasis.ToArray(), ScheduleStatusFactory.OpenOASISStatus().ToArray(), false); //new List<ScheduleEvent>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds)??new List<User>();
                scheduleEvents.ForEach(e =>
                {
                    var user = users.SingleOrDefault(u => u.Id == e.UserId);
                    var openOasis = new OpenOasis();
                    openOasis.PatientIdNumber = e.PatientIdNumber;
                    openOasis.PatientName = e.PatientName;
                    openOasis.AssessmentName = e.DisciplineTaskName;
                    openOasis.Status = e.StatusName;
                    openOasis.Date = e.EventDate;
                    openOasis.CurrentlyAssigned = user != null ? user.DisplayName : string.Empty;
                    openOasisList.Add(openOasis);
                });
            }
            return openOasisList.OrderBy(o => o.PatientName).ToList();
        }

        //public IList<ClinicalOrder> GetOrders(int statusId)
        //{
        //    IList<ClinicalOrder> orderList = new List<ClinicalOrder>();

        //    var patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
        //    patients.ForEach(patient =>
        //    {
        //        var patientEpisodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patient.Id);
        //        if (patientEpisodes != null && patientEpisodes.Count > 0)
        //        {
        //            patientEpisodes.ForEach(episode =>
        //            {
        //                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
        //                {
        //                    var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
        //                    if (events != null && events.Count > 0)
        //                    {
        //                        events.ForEach(e =>
        //                        {
        //                            if (e.IsOrderAndStatus(statusId))
        //                            {
        //                                var order = patientRepository.GetOrder(e.EventId, patient.Id, Current.AgencyId);
        //                                if (order != null)
        //                                {
        //                                    var clinicalOrder = new ClinicalOrder();
        //                                    clinicalOrder.Id = e.EventId.ToString();
        //                                    clinicalOrder.Type = e.DisciplineTaskName;
        //                                    clinicalOrder.Number = order.OrderNumber.ToString();
        //                                    clinicalOrder.PatientName = patient.DisplayName.ToTitleCase();
        //                                    clinicalOrder.Physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId).DisplayName;
        //                                    clinicalOrder.Status = e.StatusName;
        //                                    clinicalOrder.CreatedDate = order.Created.ToShortDateString();
        //                                    orderList.Add(clinicalOrder);
        //                                }
        //                            }
        //                        });
        //                    }
        //                }
        //            });
        //        }
        //    });

        //    return orderList;
        //}


        public IList<Order> GetPlanOfCareHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var disciplineTasks = DisciplineTaskFactory.POC().ToArray();
            var schedules = scheduleRepository.GetPlanOfCareOrderScheduleEvents(Current.AgencyId,branchCode, startDate, endDate, disciplineTasks, status > 0 ? new int[] { status } : new int[] { });
            if (schedules != null && schedules.Count > 0)
            {
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id);
                            if (evnt != null)
                            {
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate,
                                    ReceivedDate =  poc.ReceivedDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.Id == poc.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate =  evnt.EventDate,
                                    ReceivedDate =  poc.ReceivedDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderBy(o => o.PatientName).ToList();
        }

        #endregion

       

        #region Billing Reports

        public List<TypeOfBill> UnProcessedBillViewData(Guid branchId, string type)
        {
            var listOfbill = new List<TypeOfBill>();
            if (type.IsEqual("RAP"))
            {
                listOfbill = billingRepository.GetRapsByStatus(Current.AgencyId, branchId, (int)BillingStatus.ClaimCreated);
            }
            else if (type.IsEqual("Final"))
            {
                listOfbill = billingRepository.GetFinalsByStatus(Current.AgencyId, branchId, (int)BillingStatus.ClaimCreated);
            }
            else
            {
                var raps = billingRepository.GetRapsByStatus(Current.AgencyId, branchId, (int)BillingStatus.ClaimCreated);
                if (raps != null && raps.Count > 0)
                {
                    listOfbill.AddRange(raps);
                }

                var finals = billingRepository.GetFinalsByStatus(Current.AgencyId, branchId, (int)BillingStatus.ClaimCreated);
                if (finals != null && finals.Count > 0)
                {
                    listOfbill.AddRange(finals);
                }
            }
            return listOfbill.OrderBy(b => b.LastName).ThenBy(b => b.FirstName).ToList();
        }

        public List<ClaimLean> BillViewDataByStatus(Guid branchId, string type, int status, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                var raps = billingRepository.GetRapClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
            }
            else if (type.IsEqual("Final"))
            {
                var finals =billingRepository.GetFinalClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            else
            {
                var raps = billingRepository.GetRapClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
                var finals = billingRepository.GetFinalClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        public List<ClaimLean> SubmittedBillViewDataByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
            }
            else if (type.IsEqual("Final"))
            {
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            else
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        public List<ClaimLean> SubmittedClaimsByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);
                }
            }
            else if (type.IsEqual("Final"))
            {
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            else
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);
                }
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        public List<ClaimLean> ExpectedSubmittedClaimsByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
            if (type.IsEqual("RAP"))
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    raps.ForEach(r => { r.ClaimAmount = r.AssessmentType.IsNotNullOrEmpty() && (r.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || r.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || r.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString()) ? 0.6 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(r.HippsCode, r.EpisodeStartDate, r.AddressZipCode.IsNotNullOrEmpty() ? r.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(r.HippsCode, r.EpisodeStartDate, r.AddressZipCode.IsNotNullOrEmpty() ? r.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(raps);
                }
            }
            else if (type.IsEqual("Final"))
            {
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    finals.ForEach(f => { f.ClaimAmount = f.AssessmentType.IsNotNullOrEmpty() && (f.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || f.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || f.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString()) ? 0.4 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(f.HippsCode, f.EpisodeStartDate, f.AddressZipCode.IsNotNullOrEmpty() ? f.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(f.HippsCode, f.EpisodeStartDate, f.AddressZipCode.IsNotNullOrEmpty() ? f.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(finals);
                }
            }
            else
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    raps.ForEach(r => { r.ClaimAmount = r.AssessmentType.IsNotNullOrEmpty() && (r.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || r.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || r.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString()) ? 0.6 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(r.HippsCode, r.EpisodeStartDate, r.AddressZipCode.IsNotNullOrEmpty() ? r.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(r.HippsCode, r.EpisodeStartDate, r.AddressZipCode.IsNotNullOrEmpty() ? r.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(raps);
                }
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    finals.ForEach(f => { f.ClaimAmount = f.AssessmentType.IsNotNullOrEmpty() && (f.AssessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || f.AssessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || f.AssessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString()) ? 0.4 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(f.HippsCode, f.EpisodeStartDate, f.AddressZipCode.IsNotNullOrEmpty() ? f.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.GetProspectivePaymentAmount(f.HippsCode, f.EpisodeStartDate, f.AddressZipCode.IsNotNullOrEmpty() ? f.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(finals);
                }
            }
            return claims.OrderBy(b => b.DisplayName).ToList();
        }

        //public IList<Claim> GetPotentialCliamAutoCancels(Guid branchId)
        //{
        //    var finalAutoCancel = new List<Claim>();
        //    var insurnaceFilter = GetInsuranceFilter(branchId, 0, false, "finals");
        //    var finals = billingRepository.GetOutstandingFinalClaims(Current.AgencyId, branchId, 0,false);
        //    if (finals != null && finals.Count > 0)
        //    {
        //        finals.ForEach(final =>
        //        {
        //            var rap = billingRepository.GetRap(Current.AgencyId, final.Id);
        //            if (rap != null)
        //            {
        //                if ((rap.Status == (int)BillingStatus.ClaimAccepted || rap.Status == (int)BillingStatus.ClaimPaidClaim || rap.Status == (int)BillingStatus.ClaimPaymentPending || rap.Status == (int)BillingStatus.ClaimSubmitted) && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen || final.Status == (int)BillingStatus.ClaimRejected || final.Status == (int)BillingStatus.ClaimWithErrors || final.Status == (int)BillingStatus.ClaimCancelledClaim) && (rap.ClaimDate.AddDays(76) >= DateTime.Now))
        //                {
        //                    finalAutoCancel.Add(final);
        //                }
        //            }
        //        });
        //    }
        //    return finalAutoCancel.OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToList();
        //}

        #endregion

        #region Employee Reports

        #endregion

        #region Statistical Reports


        #endregion


    }
}
