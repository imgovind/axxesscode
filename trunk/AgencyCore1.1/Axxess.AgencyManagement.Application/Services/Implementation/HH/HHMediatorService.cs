﻿#define DEBUG

namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Extensions;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Helpers;
    using System.Web;


    public class HHMediatorService : MediatorService<ScheduleEvent, PatientEpisode>
    {
        #region Constructor

        private readonly HHEpiosdeService episodeService;
        private readonly HHTaskService scheduleService;
        private readonly HHAssessmentService assessmentService;
        private readonly HHPhysicianOrderService physicianOrderService;
        private readonly HHPlanOfCareService planOfCareService;
        private readonly HHNoteService noteService;
        private readonly FaceToFaceEncounterService faceToFaceEncounterService;
        private readonly HHBillingService billingService;

        public HHMediatorService(
            IPatientService patientService,
            HHPatientProfileService baseProfileService,
            HHEpiosdeService epiosdeService,
            HHTaskService scheduleService,
            HHAssessmentService assessmentService,
            HHPhysicianOrderService physicianOrderService,
            HHPlanOfCareService planOfCareService,
            HHCommunicationNoteService baseCommunicationNoteService,
            HHInfectionService baseInfectionService,
            HHIncidentAccidentService baseIncidentAccidentService,
            FaceToFaceEncounterService faceToFaceEncounterService,
            HHNoteService noteService,
            HHBillingService billingService,
            ILookUpService lookUpService,
            IMessageService messageService)
            : base(messageService, epiosdeService, scheduleService, assessmentService, physicianOrderService, planOfCareService, noteService, baseCommunicationNoteService, baseInfectionService, baseIncidentAccidentService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");

            base.patientService = patientService;
            base.baseProfileService = baseProfileService;
            this.episodeService = epiosdeService;
            this.scheduleService = scheduleService;
            this.assessmentService = assessmentService;
            this.physicianOrderService = physicianOrderService;
            this.planOfCareService = planOfCareService;
            this.noteService = noteService;
            this.faceToFaceEncounterService = faceToFaceEncounterService;
            this.billingService = billingService;
            base.lookUpService = lookUpService;
            Service = AgencyServices.HomeHealth;
        }

        #endregion

#if DEBUG

        public JsonViewData GenerateTestVisits(Guid patientId, Guid episodeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task(s) could not be saved. Please try again." };
            var episode = episodeService.GetEpisodeOnly(episodeId, patientId);
            if (episode != null)
            {
                var disciplineTasks = lookUpService.GetDisciplineTasks();
                DateTime date = episode.StartDate;
                foreach (var disciplineTask in disciplineTasks)
                {
                    if (disciplineTask.IsUsedInDropDown && disciplineTask.Table != 410 && disciplineTask.Discipline != "ReportsAndNotes"
                         && disciplineTask.Discipline != "Orders" && disciplineTask.Discipline != "Claim")
                    {
                        List<ScheduleEvent> tasks = new List<ScheduleEvent>();
                        if (disciplineTask.Version > 0)
                        {
                            for (int i = 0; i < disciplineTask.Version; i++)
                            {
                                tasks.Add(new ScheduleEvent() { UserId = Current.UserId, EventDate = date, DisciplineTask = disciplineTask.Id });
                            }
                        }
                        else
                        {
                            tasks.Add(new ScheduleEvent() { UserId = Current.UserId, EventDate = date, DisciplineTask = disciplineTask.Id });
                        }
                        scheduleService.SetScheduleTasksDefault(episode, disciplineTasks, tasks);
                        if (disciplineTask.Version > 0)
                        {
                            for (int i = 0; i < disciplineTask.Version; i++)
                            {
                                tasks[i].Version = i + 1;
                            }
                        }
                        viewData = this.ProcessSchedule(disciplineTask.Table, episode, tasks);
                    }
                }
            }
            return viewData;
        }

#endif

        public JsonViewData AddMultipleSchedule(Guid patientId, Guid episodeId, List<ScheduleEvent> list)
        {
            var viewData = new JsonViewData { PatientId = patientId, EpisodeId = episodeId, isSuccessful = false, errorMessage = "Task(s) could not be saved. Please try again." };
             
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                var newEvents = list;// JsonExtensions.FromJson<List<ScheduleEvent>>(schedule).OrderBy(e => e.EventDate.Date).ToList();
                if (newEvents != null && newEvents.Count > 0)
                {
                    var patientEpisode = episodeService.GetEpisodeOnly(episodeId, patientId);
                    if (patientEpisode != null)
                    {
                        var disciplineTasks = lookUpService.GetDisciplineTasksWithTableName(newEvents.Select(e => e.DisciplineTask).ToArray());
                        if (disciplineTasks != null && disciplineTasks.Count > 0)
                        {
                            var disciplineTasksDictionary = disciplineTasks.GroupBy(v => v.Table).ToDictionary(v => v.Key, v => v.ToList());
                            if (disciplineTasksDictionary != null && disciplineTasksDictionary.Count > 0)
                            {
                                disciplineTasksDictionary.ForEach((key, Value) =>
                                {
                                    var isAtleastOneSave = false;
                                    var schedules = newEvents.Where(s => Value.Exists(v => v.Id == s.DisciplineTask)).ToList();
                                    if (schedules != null && schedules.Count > 0)
                                    {
                                        scheduleService.SetScheduleTasksDefault(patientEpisode, Value, schedules);
                                        viewData = this.ProcessSchedule(key, patientEpisode, schedules);
                                        if (viewData.isSuccessful)
                                        {
                                            isAtleastOneSave = true;
                                        }
                                    }
                                    if (isAtleastOneSave)
                                    {
                                        viewData.IsDataCentersRefresh = true;
                                    }
                                });
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The episode was not found.";
                    }
                }
            }
            return viewData;
        }

        public JsonViewData AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, List<DateTime> visitDateArray)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to create the tasks. Try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !userId.IsEmpty())
            {
                if (disciplineTaskId > 0)
                {
                    if (visitDateArray != null && visitDateArray.Count > 0)
                    {
                        var disciplineTask = lookUpService.GetDisciplineTaskWithTableName(disciplineTaskId);
                        if (disciplineTask != null)
                        {
                            if (disciplineTask.IsMultiple)
                            {
                                var patientEpisode = episodeService.GetEpisodeOnly(episodeId, patientId);
                                if (patientEpisode != null)
                                {
                                    var isDaysInTheRange = visitDateArray.Exists(d => d.Date < patientEpisode.StartDate.Date || d.Date > patientEpisode.EndDate.Date);
                                    if (!isDaysInTheRange)
                                    {
                                        var scheduledEvents = scheduleService.AddMultiDayScheduleTaskHelper(patientEpisode, disciplineTask, userId, visitDateArray);
                                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                                        {
                                            viewData = this.ProcessSchedule(disciplineTask.Table, patientEpisode, scheduledEvents);
                                            if (viewData.isSuccessful)
                                            {
                                                viewData.errorMessage = "Tasks were successfully Saved.";
                                                return viewData;
                                            }
                                        }
                                    }

                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Adding multiple tasks is not allowed for this discipline.";
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData AddMultiDateRangeScheduleTask(Guid episodeId, Guid patientId, int disciplineTaskId, Guid userId, string StartDate, string EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to create the tasks." };
            var patientEpisode = episodeService.GetEpisodeOnly(episodeId, patientId);
            if (patientEpisode != null)
            {
                viewData = new List<Validation>(){
                              new Validation(() => string.IsNullOrEmpty(StartDate.ToString()), ".Start Date is required."),
                              new Validation(() => !StartDate.IsValidDate(), ". Start Date is not in the valid format."),
                              new Validation(() => string.IsNullOrEmpty(EndDate.ToString()), ". End Date is required."),
                              new Validation(() => !EndDate.IsValidDate(), ". End Date is not in the valid format."),
                              new Validation(() => disciplineTaskId<=0, ". Task is required."),
                }.Validate<JsonViewData>();
                if (viewData.isSuccessful)
                {

                    //  var oldEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (StartDate.ToDateTime().Date >= EndDate.ToDateTime().Date)
                    {
                        viewData.errorMessage = "The start date must be greater than end date.";
                        viewData.isSuccessful = false;
                        return viewData;
                    }
                    else if (StartDate.ToDateTime().Date < patientEpisode.StartDate.Date || StartDate.ToDateTime().Date > patientEpisode.EndDate.Date || EndDate.ToDateTime().Date < patientEpisode.StartDate.Date || EndDate.ToDateTime().Date > patientEpisode.EndDate.Date)
                    {
                        viewData.errorMessage = "The start date and end date has to be in the current episode date range.";
                        viewData.isSuccessful = false;
                        return viewData;
                    }
                    else
                    {
                        var disciplineTask = lookUpService.GetDisciplineTaskWithTableName(disciplineTaskId);
                        if (disciplineTask != null)
                        {
                            var schedules = scheduleService.MultiDateRangeScheduleTaskHelper(patientEpisode, disciplineTask, userId, StartDate.ToDateTime(), EndDate.ToDateTime());
                            if (schedules != null && schedules.Count > 0)
                            {
                                viewData = this.ProcessSchedule(disciplineTask.Table, patientEpisode, schedules);
                                if (viewData.isSuccessful)
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Tasks were successfully Saved.";
                                    return viewData;
                                }
                            }
                        }
                        return viewData;
                    }
                }
                else
                {
                    return viewData;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The episode was not found.";
            }
            return viewData;

        }

        //private bool AddScheduleTaskHelper(PatientEpisode episode, List<ScheduleEvent> scheduleEvents)
        //{
        //    bool result = false;
        //    if (episode != null)
        //    {
        //        if (scheduleEvents != null && scheduleEvents.Count > 0)
        //        {
        //            var patient = patientRepository.GetPatientOnly(episode.PatientId, Current.AgencyId);
        //            if (!episode.AdmissionId.IsEmpty())
        //            {
        //                var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
        //                if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
        //                {
        //                    patient = admissinData.PatientData.ToObject<Patient>();
        //                }
        //            }
        //            if (patient != null)
        //            {
        //                episode.StartOfCareDate = patient.StartofCareDate;
        //                scheduleEvents.ForEach(ev =>
        //                {
        //                    if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
        //                    {
        //                        //ProcessScheduleFactory(ev);
        //                        if (scheduleRepository.AddScheduleTask(ev))
        //                        {
        //                            if (ProcessSchedule(ev, patient, episode))
        //                            {
        //                                result = true;
        //                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, ev.EpisodeId, ev.PatientId, ev.Id, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
        //                            }
        //                            else
        //                            {
        //                                scheduleRepository.RemoveScheduleTaskFully(Current.AgencyId, ev.PatientId, ev.Id);
        //                            }
        //                        }

        //                    }
        //                });
        //            }
        //        }
        //    }
        //    return result;
        //}

        protected override Guid GetScheduledEventForDetailAppSpecific(Guid patientId, Guid eventId, int disciplineTaskId)
        {
            var physicianId = Guid.Empty;
            if (disciplineTaskId == (int)DisciplineTasks.FaceToFaceEncounter)
            {
                var faceToFace = faceToFaceEncounterService.GetFaceToFaceEncounterOnly(patientId, eventId);
                if (faceToFace != null)
                {
                    physicianId = faceToFace.PhysicianId;
                }
            }
            return physicianId;
        }

        public override JsonViewData UpdateScheduleEventDetails(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            var validationRules = new List<Validation>();
            if (scheduleEvent.IsEpisodeReassiged && scheduleEvent.EpisodeId != scheduleEvent.NewEpisodeId)
            {
                var patientEpisode = episodeService.GetEpisodeDateRange(scheduleEvent.PatientId, scheduleEvent.NewEpisodeId);
                if (patientEpisode != null)
                {
                    scheduleEvent.EpisodeId = scheduleEvent.NewEpisodeId;
                    scheduleEvent.StartDate = patientEpisode.StartDate;
                    scheduleEvent.EndDate = patientEpisode.EndDate;
                    viewData = TaskHelperFactory<ScheduleEvent>.ValidateScheduleTaskDate(scheduleEvent, patientEpisode.StartDate, patientEpisode.EndDate);
                    if (viewData.isSuccessful && DisciplineTaskFactory.AllAssessments(false).Contains(scheduleEvent.DisciplineTask))
                    {
                        var oldEvents = scheduleService.GetPatientScheduledEventsOnlyNew(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);// (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => s.EventDate.IsValidDate() && !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime().Date).ToList();
                        viewData = TaskHelperFactory<ScheduleEvent>.Validate(oldEvents, new List<ScheduleEvent> { scheduleEvent }, patientEpisode.EndDate);
                    }
                }
            }
            else
            {
                var episode = episodeService.GetEpisodeDateRange(scheduleEvent.PatientId, scheduleEvent.EpisodeId);
                if (episode != null)
                {
                    viewData = TaskHelperFactory<ScheduleEvent>.ValidateScheduleTaskDate(scheduleEvent, episode.StartDate, episode.EndDate);
                }
            }
            if (viewData.isSuccessful)
            {
                viewData = UpdateScheduleEventDetail(scheduleEvent, httpFiles);
                if (viewData.isSuccessful)
                {
                    viewData.IsCaseManagementRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = true;
                    viewData.IsDataCentersRefresh = true;
                    viewData.PatientId = scheduleEvent.PatientId;
                    viewData.EpisodeId = scheduleEvent.EpisodeId;
                    viewData.errorMessage = "Task details updated successfully.";
                }
            }
            return viewData;
        }

        //public JsonViewData UpdateScheduleEventDetailForMoveToOtherEpisode(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        //{
        //    var assetsSaved = true;
        //    var jsonViewData = new JsonViewData { isSuccessful = false };
        //    if (scheduleEvent != null && !scheduleEvent.NewEpisodeId.IsEmpty())
        //    {
        //        var newPatientEpisode = episodeService.GetEpisodeOnly(scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
        //        if (newPatientEpisode != null)
        //        {
        //            var scheduleEventToEdit = scheduleService.GetScheduleTask(scheduleEvent.PatientId, scheduleEvent.Id); //oldScheduleEvents.FirstOrDefault(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
        //            if (scheduleEventToEdit != null)
        //            {
        //                var oldStatus = scheduleEventToEdit.Status;
        //                var scheduleBuffer = new ScheduleEvent
        //                {
        //                    Asset = scheduleEventToEdit.Asset,
        //                    UserId = scheduleEventToEdit.UserId,
        //                    Comments = scheduleEventToEdit.Comments,
        //                    Status = scheduleEventToEdit.Status,
        //                    EventDate = scheduleEventToEdit.EventDate,
        //                    VisitDate = scheduleEventToEdit.VisitDate,
        //                    IsBillable = scheduleEventToEdit.IsBillable,
        //                    Surcharge = scheduleEventToEdit.Surcharge,
        //                    AssociatedMileage = scheduleEventToEdit.AssociatedMileage,
        //                    TimeIn = scheduleEventToEdit.TimeIn,
        //                    TimeOut = scheduleEventToEdit.TimeOut,
        //                    IsMissedVisit = scheduleEventToEdit.IsMissedVisit,
        //                    DisciplineTask = scheduleEventToEdit.DisciplineTask,
        //                    PhysicianId = scheduleEventToEdit.PhysicianId,
        //                    EpisodeId = scheduleEventToEdit.EpisodeId
        //                };

        //                if (httpFiles != null && httpFiles.Count > 0)
        //                {
        //                    if (httpFiles != null && httpFiles.Count > 0)
        //                    {
        //                        assetsSaved = AssetHelper.AddAssetAndSetToSchedule<ScheduleEvent>(scheduleEventToEdit, httpFiles);
        //                    }
        //                }
        //                Guid userId = scheduleEventToEdit.UserId;
        //                scheduleEventToEdit.EpisodeId = newPatientEpisode.Id;
        //                scheduleEventToEdit.StartDate = newPatientEpisode.StartDate;
        //                scheduleEventToEdit.EndDate = newPatientEpisode.EndDate;
        //                scheduleEventToEdit.UserId = scheduleEvent.UserId;
        //                scheduleEventToEdit.Comments = scheduleEvent.Comments;
        //                scheduleEventToEdit.Status = scheduleEvent.Status;
        //                scheduleEventToEdit.EventDate = scheduleEvent.EventDate;
        //                scheduleEventToEdit.VisitDate = scheduleEvent.VisitDate;
        //                scheduleEventToEdit.IsBillable = scheduleEvent.IsBillable;
        //                scheduleEventToEdit.Surcharge = scheduleEvent.Surcharge;
        //                scheduleEventToEdit.AssociatedMileage = scheduleEvent.AssociatedMileage;
        //                scheduleEventToEdit.TimeIn = scheduleEvent.TimeIn;
        //                scheduleEventToEdit.TimeOut = scheduleEvent.TimeOut;
        //                // schedule.IsMissedVisit = scheduleEvent.IsMissedVisit;
        //                scheduleEventToEdit.DisciplineTask = scheduleEvent.DisciplineTask;
        //                scheduleEventToEdit.PhysicianId = scheduleEvent.PhysicianId;

        //                //if (assetsSaved)
        //                //{
        //                //    scheduleEventToEdit.Asset = scheduleEventToEdit.Assets.ToXml();
        //                //}

        //                if (assetsSaved && scheduleService.UpdateScheduleTask(scheduleEventToEdit))
        //                {
        //                    if (ProcessEditDetailForReassign(scheduleEventToEdit))
        //                    {
        //                        if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEventToEdit.DisciplineTask))
        //                        {
        //                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEventToEdit.EpisodeId, scheduleEventToEdit.PatientId, scheduleEventToEdit.Id, Actions.EditDetail, (DisciplineTasks)scheduleEventToEdit.DisciplineTask, "Reassigned from other episode.");
        //                        }
        //                        var isStatusChange = oldStatus != scheduleEventToEdit.Status;
        //                        jsonViewData.isSuccessful = true;
        //                        jsonViewData.EpisodeId = scheduleBuffer.EpisodeId;
        //                        jsonViewData.PatientId = scheduleBuffer.PatientId;
        //                        jsonViewData.IsCaseManagementRefresh = isStatusChange && (oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview) || oldStatus == ((int)ScheduleStatus.OasisCompletedPendingReview) || oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
        //                        jsonViewData.IsDataCentersRefresh = oldStatus != scheduleEventToEdit.Status;
        //                        jsonViewData.IsMyScheduleTaskRefresh = TaskHelperFactory<ScheduleEvent>.ShouldMyScheduleTaskRefresh(jsonViewData.IsDataCentersRefresh, scheduleEvent);

        //                    }
        //                    else
        //                    {
        //                        scheduleEventToEdit.UserId = scheduleBuffer.UserId;
        //                        scheduleEventToEdit.Comments = scheduleBuffer.Comments;
        //                        scheduleEventToEdit.Status = scheduleBuffer.Status;
        //                        scheduleEventToEdit.EventDate = scheduleBuffer.EventDate;
        //                        scheduleEventToEdit.VisitDate = scheduleBuffer.VisitDate;
        //                        scheduleEventToEdit.IsBillable = scheduleBuffer.IsBillable;
        //                        scheduleEventToEdit.Surcharge = scheduleBuffer.Surcharge;
        //                        scheduleEventToEdit.AssociatedMileage = scheduleBuffer.AssociatedMileage;
        //                        scheduleEventToEdit.TimeIn = scheduleBuffer.TimeIn;
        //                        scheduleEventToEdit.TimeOut = scheduleBuffer.TimeOut;
        //                        scheduleEventToEdit.DisciplineTask = scheduleBuffer.DisciplineTask;
        //                        scheduleEventToEdit.PhysicianId = scheduleBuffer.PhysicianId;
        //                        scheduleEventToEdit.EpisodeId = scheduleBuffer.EpisodeId;
        //                        scheduleEventToEdit.Asset = scheduleBuffer.Asset;
        //                        scheduleService.UpdateScheduleTask(scheduleEventToEdit);
        //                        jsonViewData.isSuccessful = false;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return jsonViewData;
        //}

        protected override bool ProcessEditDetailAppSpecific(ScheduleEvent schedule)
        {
            bool result = false;
            switch (schedule.DisciplineTask)
            {
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    result = faceToFaceEncounterService.UpdateFaceToFaceEncounterForScheduleDetail(schedule);
                    break;
            }
            return result;
        }

        protected override JsonViewData UpdateScheduleEntityAppSpecific(ScheduleEvent task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Task could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            switch (task.DisciplineTask)
            {
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    viewData = faceToFaceEncounterService.ToggleFaceToFaceEncounter(task, isDeprecated);
                    break;
            }
            return viewData;
        }




        public void DeleteEpisodeAndClaims(Guid patientId, Guid episodeId)
        {
            if (!episodeId.IsEmpty())
            {
                episodeService.RemoveEpisode(patientId, episodeId);
                billingService.RemoveMedicareClaims(patientId, episodeId);
            }
        }

        public PatientProfile GetProfileWithEpisodeInfo(Guid patientId)
        {
            var profile = this.baseProfileService.GetProfileWithOutEpisodeInfo(patientId);
            if (profile != null)
            {
                assessmentService.SetPatientProfileEpisodeInfo(profile);
            }
            return profile;
        }

        public ManagedClaimEpisodeData GetEpisodeAssessmentData(Guid episodeId, Guid patientId)
        {
            var managedClaim = assessmentService.GetManagedClaimEpisodeData(episodeId, patientId);
            if (managedClaim != null)
            {
                managedClaim.ProspectivePay = billingService.ProspectivePayAmount(patientId, managedClaim.HippsCode, managedClaim.AssessmentType, managedClaim.StartDate);
                managedClaim.isSuccessful = true;
                managedClaim.errorMessage = "Associated episode information was successfully pulled to the claim.";
            }
            return managedClaim;
        }

        private void DischargePatientByOASIS(Guid Id, Guid patientId, Guid episodeId, int assessmentType, JsonViewData viewData)
        {
            var message = string.Empty;
            if (DisciplineTaskFactory.CMSDischargingOASIS().Contains(assessmentType))
            {
                var schedule = scheduleService.GetScheduleTask(patientId, Id);
                if (schedule != null)
                {
                    var assessment = assessmentService.GetAssessment(patientId, Id);
                    if (assessment != null)
                    {
                        var questionData = assessmentService.GetQuestions(assessment.Id);
                        var assessmentData = questionData.ToDictionary();

                        var date = DateTime.MinValue;
                        var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                        if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate())
                        {
                            date = dateAssessment.ToDateTime();
                        }
                        var eventDateSchedule = schedule.EventDate;
                        if (eventDateSchedule.IsValid())
                        {
                            date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                        }
                        var visitDateSchedule = schedule.VisitDate;
                        if (visitDateSchedule.IsValid())
                        {
                            date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                        }
                        if (date.Date > DateTime.MinValue.Date)
                        {
                            var success = this.baseProfileService.DischargePatient(patientId, date, 0, "Patient discharged due to discharge OASIS.", episodeId);
                            if (success.isSuccessful)
                            {
                                viewData.IsCenterRefresh = success.IsCenterRefresh;
                                viewData.errorMessage = "Your Assessment was approved successfully, and the patient was discharged.";//message = "Your Assessment was approved successfully, and the patient was discharged.";
                            }
                        }
                    }
                    else
                    {

                        var date = DateTime.MinValue;
                        var eventDateSchedule = schedule.EventDate;
                        if (eventDateSchedule.IsValid())
                        {
                            date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                        }
                        var visitDateSchedule = schedule.VisitDate;
                        if (visitDateSchedule.IsValid())
                        {
                            date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                        }
                        if (date.Date > DateTime.MinValue.Date)
                        {
                            var success = this.baseProfileService.DischargePatient(patientId, date, 0, "Patient discharged due to Discharge OASIS.", episodeId);
                            if (success.isSuccessful)
                            {
                                viewData.IsCenterRefresh = success.IsCenterRefresh;
                                viewData.errorMessage = "Your Assessment was approved successfully, and the patient was discharged.";//message
                            }
                        }

                    }
                }
            }
        }

        protected override void DischargePatientByOASIS(Guid Id, Guid patientId, Guid episodeId, DateTime possibleDischargeDate, int assessmentType, JsonViewData viewData)
        {
            if (DisciplineTaskFactory.CMSDischargingOASIS().Contains(assessmentType))
            {
                var assessmentQuestions = assessmentService.GetQuestions(Id);
                if (assessmentQuestions != null)
                {
                    var assessmentData = assessmentQuestions.ToDictionary();
                    var date = DateTime.MinValue;
                    var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                    if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                    var dates = new List<DateTime>() { date.Date, possibleDischargeDate };
                    date = dates.Max();
                    if (date > DateTime.MinValue)
                    {
                        var success = this.baseProfileService.DischargePatient(patientId, date, 0, "Patient discharged due to discharge oasis.", episodeId);
                        if (success.isSuccessful)
                        {
                            viewData.IsCenterRefresh = success.IsCenterRefresh;
                        }
                    }
                }
                else
                {
                    var date = possibleDischargeDate;
                    if (date > DateTime.MinValue)
                    {
                        var success = this.baseProfileService.DischargePatient(patientId, date, 0, "Patient discharged due to discharge oasis.", episodeId);
                        if (success.isSuccessful)
                        {
                            viewData.IsCenterRefresh = success.IsCenterRefresh;
                        }
                    }
                }
            }
        }

        //public MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid Id)
        //{
        //    var profile = patientService.GetMedicationSnapshotPrint(Id);
        //    if (profile != null)
        //    {
        //        if (!profile.EpisodeId.IsEmpty()&& profile.Patient != null && !profile.Patient.Id.IsEmpty())
        //        {
        //            var episode = episodeService.GetEpisodeOnly(profile.EpisodeId, profile.Patient.Id);
        //            if (episode != null)
        //            {
        //                profile.StartDate = episode.StartDate;
        //                profile.EndDate = episode.EndDate;
        //            }
        //        }
        //    }
        //    return profile;
        //}

      
        //public HospitalizationLog GetHospitalizationLogPrintWithEpisodeInfo(Guid patientId, Guid hospitalizationLogId)
        //{
        //    var log = profileService.GetHospitalizationLogPrintWithNoEpisodeInfo(patientId, hospitalizationLogId);
        //    if (log != null)
        //    {
        //        if (!log.PatientId.IsEmpty())
        //        {
        //            if (!log.EpisodeId.IsEmpty())
        //            {
        //                var episode = episodeService.GetEpisodeOnly(log.EpisodeId, log.PatientId);
        //                if (episode != null)
        //                {
        //                    log.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
        //                }
        //            }
        //        }
        //    }
        //    return log;
        //}

        protected override JsonViewData ProcessScheduleAppSpecific(int table, PatientEpisode episode, List<ScheduleEvent> scheduleEvents)
        {
            var viewData = new JsonViewData();
            switch (table)
            {
                case (int)Tables.FaceToFaceEncounters:
                    viewData = faceToFaceEncounterService.AddScheduleTaskAndFaceToFaceEncounterHelper(episode.PatientId, scheduleEvents);
                    break;

            }
            return viewData;
        }

        //public NoteJsonViewData ProcessNoteAction(string button,NoteFormArguments noteBasicData, FormCollection formCollection)
        //{
        //    var viewData = new NoteJsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
        //    if (noteBasicData != null)
        //    {
        //        string type = formCollection["Type"];
        //        if (noteBasicData.Type.IsNotNullOrEmpty())
        //        {
        //            var keys = formCollection.AllKeys;
        //            Guid eventId = keys.Contains(string.Format("{0}_EventId", type)) ? formCollection.Get(string.Format("{0}_EventId", type)).ToGuid() : Guid.Empty;
        //            Guid episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
        //            Guid patientId = keys.Contains(string.Format("{0}_PatientId", type)) ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;
        //            var timeIn = keys.Contains(string.Format("{0}_TimeIn", type)) && formCollection[string.Format("{0}_TimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeIn", type)] : string.Empty;
        //            var timeOut = keys.Contains(string.Format("{0}_TimeOut", type)) && formCollection[string.Format("{0}_TimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeOut", type)] : string.Empty;
        //            var sendAsOrder = keys.Contains(string.Format("{0}_SendAsOrder", type)) && formCollection[string.Format("{0}_SendAsOrder", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_SendAsOrder", type)] : string.Empty;
        //            var surcharge = keys.Contains(string.Format("{0}_Surcharge", type)) && formCollection[string.Format("{0}_Surcharge", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_Surcharge", type)] : string.Empty;
        //            var mileage = keys.Contains(string.Format("{0}_AssociatedMileage", type)) && formCollection[string.Format("{0}_AssociatedMileage", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_AssociatedMileage", type)] : string.Empty;
        //            var date = keys.Contains(string.Format("{0}_VisitDate", type)) ? formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime() : DateTime.MinValue;
        //            int disciplineTask = keys.Contains("DisciplineTask") ? formCollection.Get("DisciplineTask").ToInteger() : 0;

        //            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
        //            {
        //                var episodeDateRange = patientRepository.GetEpisodeDateRange(Current.AgencyId, patientId, episodeId);
        //                var rules = AddNotesValidationRules(type, button, keys, episodeDateRange, formCollection);
        //                var args = new SaveNoteArguments(eventId, patientId, episodeId, date, mileage, surcharge,
        //                    sendAsOrder, timeIn, timeOut, disciplineTask);

        //                if (button == "Save")
        //                {
        //                    var entityValidator = new EntityValidator(rules.ToArray());
        //                    entityValidator.Validate();

        //                    if (entityValidator.IsValid)
        //                    {
        //                        viewData = scheduleService.SaveNotes(args, button, formCollection);
        //                        if (viewData.isSuccessful)
        //                        {
        //                            viewData.isSuccessful = true;
        //                            viewData.errorMessage = "The note was successfully saved.";
        //                        }
        //                        else
        //                        {
        //                            viewData.isSuccessful = false;
        //                            viewData.errorMessage = "The note could not be saved.";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        viewData.isSuccessful = false;
        //                        viewData.errorMessage = entityValidator.Message;
        //                    }
        //                }
        //                else if (button == "Complete")
        //                {
        //                    var entityValidator = new EntityValidator(rules.ToArray());
        //                    entityValidator.Validate();
        //                    if (entityValidator.IsValid)
        //                    {
        //                        viewData = scheduleService.SaveNotes(args, button, formCollection);
        //                        if (viewData.isSuccessful)
        //                        {
        //                            viewData.isSuccessful = true;
        //                            viewData.errorMessage = "The note was successfully Submited.";
        //                        }
        //                        else
        //                        {
        //                            viewData.isSuccessful = false;
        //                            viewData.errorMessage = "The note could not be submited.";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        viewData.isSuccessful = false;
        //                        viewData.errorMessage = entityValidator.Message;
        //                    }
        //                }
        //                else if (button == "Approve")
        //                {
        //                    var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
        //                    if (patientVisitNote != null)
        //                    {
        //                        if ((patientVisitNote.SignatureText.IsNullOrEmpty() || patientVisitNote.SignatureDate == DateTime.MinValue) &&
        //                            (!keys.Contains(type + "_Clinician") || !keys.Contains(type + "_SignatureDate")))
        //                        {
        //                            viewData.isSuccessful = false;
        //                            viewData.errorMessage = "The note could not be approved because the Electronic Signature is missing. Please sign this note before continuing.";
        //                        }
        //                        else
        //                        {
        //                            var entityValidator = new EntityValidator(rules.ToArray());
        //                            entityValidator.Validate();
        //                            if (entityValidator.IsValid)
        //                            {
        //                                viewData = scheduleService.SaveNotes(args, button, formCollection);
        //                                if (viewData.isSuccessful)
        //                                {
        //                                    viewData.isSuccessful = true;
        //                                    viewData.errorMessage = "The note was successfully Approved.";
        //                                }
        //                                else
        //                                {
        //                                    viewData.isSuccessful = false;
        //                                    viewData.errorMessage = "The note could not be Approved.";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                viewData.isSuccessful = false;
        //                                viewData.errorMessage = entityValidator.Message;
        //                            }
        //                        }
        //                    }
        //                }
        //                else if (button == "Return")
        //                {
        //                    viewData = scheduleService.SaveNotes(args, button, formCollection);
        //                    if (viewData.isSuccessful)
        //                    {
        //                        viewData.isSuccessful = true;
        //                        viewData.errorMessage = "The note was successfully returned.";
        //                    }
        //                    else
        //                    {
        //                        viewData.isSuccessful = false;
        //                        viewData.errorMessage = "The note could not be returned.";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return viewData;
        //}



        //public JsonViewData AddEpisodeThruWorkFlow(PatientEpisode patientEpisode)
        //{
        //    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Episode could not be saved" };
        //    var patient = patientService.GetPatientOnly(patientEpisode.PatientId);
        //    var entityValidator = new EntityValidator(episodeService.EpisodeValidation(patient, patientEpisode).ToArray());
        //    entityValidator.Validate();
        //    patientEpisode.Modified = DateTime.MinValue;
        //    if (entityValidator.IsValid && patient != null)
        //    {
        //        var workflow = new CreateEpisodeWorkflow(patientEpisode);
        //        if (workflow.IsCommitted)
        //        {
        //            viewData.PatientId = patient.Id;
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "Episode was created successfully.";
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = workflow.Message;
        //        }
        //    }
        //    else
        //    {
        //        viewData.isSuccessful = false;
        //        viewData.errorMessage = entityValidator.Message;
        //    }
        //    return viewData;
        //}


    }
}
