﻿using Axxess.AgencyManagement.Repositories;

namespace Axxess.AgencyManagement.Application.Services
{
    public class HHAdmissionService : AdmissionServiceAbstract
    {
        public HHAdmissionService(HHDataProvider dataProvider)
            : base(dataProvider.PatientRepository,dataProvider.PatientProfileRepository, dataProvider.PatientAdmissionRepository)
        {
        }
    }
}
