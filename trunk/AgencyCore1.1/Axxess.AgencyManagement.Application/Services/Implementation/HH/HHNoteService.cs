﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Helpers;

    using Telerik.Web.Mvc.UI;

    public class HHNoteService : NoteService<ScheduleEvent, PatientEpisode>
    {
        private HHTaskRepository scheduleRepository;
        private HHNoteRepository noteRepository;
        private HHPlanOfCareRepository planofCareRepository;
        private HHEpisodeRepository episodeRepository;

        public HHNoteService(HHDataProvider dataProvider, ILookUpDataProvider lookupDataProvider)
            : base(dataProvider.TaskRepository, dataProvider.NoteRepository, dataProvider.EpisodeRepository,dataProvider.MongoRepository, dataProvider.VitalSignRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "agencyManagementDataProvider");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");

            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.lookUpRepository = lookupDataProvider.LookUpRepository;

            this.planofCareRepository = dataProvider.PlanOfCareRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.NoteRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;

            base.Service = AgencyServices.HomeHealth;
        }

      

        public List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId)
        {
            var visitNoteviewDatas = new List<VisitNoteViewData>();
            var visits = noteRepository.GetVisitNotesByDisciplineTaskWithStatus(Current.AgencyId, patientId, new int[] { (int)ScheduleStatus.NoteCompleted }, new int[] { (int)DisciplineTasks.SixtyDaySummary });
            if (visits != null && visits.Count > 0)
            {
                var userIds = visits.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                visits.ForEach(v =>
                {
                    var visitNoteviewData = new VisitNoteViewData();
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        visitNoteviewData.UserDisplayName = user.DisplayName;
                    }
                    var questionData = mongoRepository.GetNoteQuestion(Current.AgencyId, v.Id);
                    if (questionData != null && questionData.Question != null)
                    {
                        v.Questions = questionData.Question;
                    }
                    var questions = v.ToDictionary();
                    if (questions != null && questions.Count > 0)
                    {
                        var physicianId = v.PhysicianId.IsEmpty() ? questions.ContainsKey("Physician") && questions["Physician"].Answer.IsNotNullOrEmpty() && questions["Physician"].Answer.IsGuid() ? questions["Physician"].Answer.ToGuid() : Guid.Empty : v.PhysicianId;
                        visitNoteviewData.VisitStartDate = v.VisitDate.Date > DateTime.MinValue.Date ? v.VisitDate : v.EventDate;
                        if (!physicianId.IsEmpty())
                        {
                            var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                visitNoteviewData.PhysicianDisplayName = physician.DisplayName;
                            }
                        }
                        visitNoteviewData.SignatureDate = questions.ContainsKey("SignatureDate") && questions["SignatureDate"].Answer.IsNotNullOrEmpty() && questions["SignatureDate"].Answer.IsValidDate() ? questions["SignatureDate"].Answer : string.Empty;
                    }
                    visitNoteviewData.StartDate = v.StartDate;
                    visitNoteviewData.EndDate = v.EndDate;
                    visitNoteviewData.EventId = v.Id;
                    visitNoteviewData.PatientId = v.PatientId;
                    //visitNoteviewData.PrintUrl = UrlFactory<ScheduleEvent>.Print(new ScheduleEvent { DisciplineTask = (int)DisciplineTasks.SixtyDaySummary, Id = v.Id, EpisodeId = v.EpisodeId, PatientId = v.PatientId }, true);
                    visitNoteviewDatas.Add(visitNoteviewData);
                });
            }
            return visitNoteviewDatas;
        }

        //public OasisPlanOfCareJson GetCarePlanBySelectedEpisode(Guid patientId, Guid eventId, DisciplineTasks discipline)
        //{
        //    var viewData = new OasisPlanOfCareJson { isSuccessful = false, errorMessage = "No Care Plan found for this episode." };
        //    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
        //    if (scheduleEvent != null)
        //    {
        //        var poc = noteService.GetCarePlanBySelectedEpisode(patientId, episodeId, DisciplineTasks.HHAideCarePlan);
        //        if (poc != null)
        //        {
        //            viewData.isSuccessful = true;
        //            viewData.url = "/Note/HHACarePlanPdf";
        //            viewData.episodeId = poc.EpisodeId;
        //            viewData.patientId = poc.PatientId;
        //            viewData.eventId = poc.Id;
        //        }
        //    }
        //    return viewData;
        //}

        #region Vital Signs

        protected void GetBPValuesForGraphing(VitalSign vitalSign)
        {
            int bpSystolic = 0;
            int bpDiastolic = 0;
            var bpVal = new List<string>();
            if (vitalSign.BPLyingLeft.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPLyingLeft.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPLyingRight.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPLyingRight.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPSittingLeft.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPSittingLeft.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPSittingRight.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPSittingRight.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPStandingLeft.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPStandingLeft.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPStandingRight.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPStandingRight.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPLying.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPLying.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPSitting.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPSitting.Split(new Char[] { '/' }).ToList();
            }
            else if (vitalSign.BPStanding.IsNotNullOrEmpty())
            {
                bpVal = vitalSign.BPStanding.Split(new Char[] { '/' }).ToList();
            }
            if (bpVal!=null)
            {
                if (bpVal.Count >= 1)
                {
                    int.TryParse(bpVal[0], out bpSystolic);
                }
                if (bpVal.Count >= 2)
                {
                    int.TryParse(bpVal[1], out bpDiastolic);
                }

                vitalSign.BPSystolic = bpSystolic;
                vitalSign.BPDiastolic = bpDiastolic;
            }
        }

        protected List<VitalSign> VitalSignFromList(List<ScheduleEvent> scheduleEvents)
        {
            var vitalSigns = new List<VitalSign>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var disciplineTasksSNVList = DisciplineTaskFactory.AllSkilledNurseDisciplineTasks(false);
                var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments(true);
                var disciplineTasksUAP = DisciplineTaskFactory.AllUAP();
                var disciplineTasksTherapy = DisciplineTaskFactory.AllTherapy(false);

                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(false);
                var noteIds = scheduleEvents.Where(s => noteDisciplineTasks.Contains(s.DisciplineTask)).Select(s => s.Id).ToList();
                var noteQuestionDatas = mongoRepository.GetManyNoteQuestions(Current.AgencyId, noteIds);
                var assessmentIds = scheduleEvents.Where(s => disciplineTasksOASISList.Contains(s.DisciplineTask)).Select(s => s.Id).ToList();
                var assessmentQuestionDatas = mongoRepository.GetManyAssessmentQuestions(Current.AgencyId, assessmentIds);
                scheduleEvents.ForEach(s =>
                {
                    if (disciplineTasksOASISList.Contains(s.DisciplineTask))
                    {
                        var questionData = assessmentQuestionDatas.SingleOrDefault(nq => nq.Id == s.Id);
                        if (questionData != null)
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = questionData.Question.Distinct().ToDictionary(q => q.Type + q.Name, q => q);
                            vitalSign.VisitDate = s.VisitDate;
                            if (questions.IsNotNullOrEmpty())
                            {
                                vitalSign.Temp = questions.AnswerOrEmptyString("GenericTemp").ToDoubleAfterNonDigitStrip();
                                vitalSign.Resp = questions.AnswerOrEmptyString("GenericResp").ToDoubleAfterNonDigitStrip();

                                var diaList = new List<int>();

                                vitalSign.BPLyingLeft = questions.AnswerOrEmptyString("GenericBPLeftLying");

                                vitalSign.BPLyingRight = questions.AnswerOrEmptyString("GenericBPRightLying");

                                vitalSign.BPSittingLeft = questions.AnswerOrEmptyString("GenericBPLeftSitting");
                                vitalSign.BPSittingRight = questions.AnswerOrEmptyString("GenericBPRightSitting");

                                vitalSign.BPStandingLeft = questions.AnswerOrEmptyString("GenericBPLeftStanding");
                                vitalSign.BPStandingRight = questions.AnswerOrEmptyString("GenericBPRightStanding");

                                vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");
                                GetBPValuesForGraphing(vitalSign);
                               
                                vitalSign.ApicalPulse = questions.AnswerOrEmptyString("GenericPulseApical").ToIntegerAfterNonDigitStrip();
                                vitalSign.RadialPulse = questions.AnswerOrEmptyString("GenericPulseRadial").ToIntegerAfterNonDigitStrip();

                                var bsNoon = questions.AnswerDoubleOrZero("GenericBloodSugarLevelText");
                                vitalSign.BSMax = bsNoon;
                                vitalSign.BSMin = bsNoon;

                                vitalSign.Weight = questions.AnswerOrEmptyString("GenericWeight").ToIntegerAfterNonDigitStrip();
                                vitalSign.PainLevel = questions.AnswerOrEmptyString("GenericIntensityOfPain").ToIntegerAfterNonDigitStrip();

                                var user = users.SingleOrDefault(u => u.Id == s.UserId);
                                if (user != null)
                                {
                                    vitalSign.UserDisplayName = user.DisplayName;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                    }
                    else if (s.DisciplineTask == (int)DisciplineTasks.HHAideVisit || disciplineTasksUAP.Contains(s.DisciplineTask))
                    {
                        var questionData = noteQuestionDatas.SingleOrDefault(nq => nq.Id == s.Id);
                        if (questionData != null)
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = questionData.Question.ToDictionary(q => q.Name, q => q);
                            vitalSign.VisitDate = s.VisitDate;
                            if (questions != null && questions.Count > 0)
                            {
                                var diastolicBP = questions.AnswerOrEmptyString("VitalSignBPVal");
                                if (s.DisciplineTask == (int)DisciplineTasks.HHAideVisit)
                                {
                                    if (diastolicBP.IsNotNullOrEmpty())
                                    {
                                        vitalSign.BPSystolic = diastolicBP.Split(new Char[] { '/', '\\' })[0].ToIntegerAfterNonDigitStrip();
                                        vitalSign.BPDiastolic = diastolicBP.Split(new Char[] { '/', '\\' })[1].ToIntegerAfterNonDigitStrip();
                                        vitalSign.BPSitting = vitalSign.BPDiastolic + "/" + vitalSign.BPSystolic;
                                    }
                                }
                                else
                                {
                                    vitalSign.ApicalPulse = questions.AnswerOrEmptyString("VitalSignHRVal").ToIntegerAfterNonDigitStrip();
                                    vitalSign.BPDiastolic = diastolicBP.ToIntegerAfterNonDigitStrip();
                                    vitalSign.BPSystolic = questions.AnswerOrEmptyString("VitalSignSBPVal").ToIntegerAfterNonDigitStrip();
                                    vitalSign.BPSitting = vitalSign.BPDiastolic + "/" + vitalSign.BPSystolic;
                                }
                                vitalSign.Weight = questions.AnswerOrEmptyString("VitalSignWeightVal").ToIntegerAfterNonDigitStrip();
                                vitalSign.Temp = questions.AnswerOrEmptyString("VitalSignTempVal").ToIntegerAfterNonDigitStrip();
                                vitalSign.Resp = questions.AnswerOrEmptyString("VitalSignRespVal").ToIntegerAfterNonDigitStrip();
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }
                    }
                    else if (disciplineTasksTherapy.Contains(s.DisciplineTask))
                    {
                        var questionData = noteQuestionDatas.SingleOrDefault(nq => nq.Id == s.Id);
                        if (questionData != null)
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = questionData.Question.ToDictionary(q => q.Name, q => q);
                            vitalSign.VisitDate = s.VisitDate;
                            if (questions != null)
                            {
                                string SBP = questions.AnswerOrEmptyString("GenericBloodPressure");
                                string DBP = questions.AnswerOrEmptyString("GenericBloodPressurePer");
                                var sboInt = 0;
                                if (int.TryParse(SBP, out sboInt))
                                {
                                    vitalSign.BPSystolic = sboInt;
                                }
                                var dbpInt = 0;
                                if (int.TryParse(DBP, out dbpInt))
                                {
                                    vitalSign.BPDiastolic = dbpInt;
                                }

                                vitalSign.BPLying = "";
                                vitalSign.BPSitting = sboInt > 0 && dbpInt>0 ? string.Format("{0}/{1}", sboInt, dbpInt) : string.Empty;
                                vitalSign.BPStanding = "";
                                vitalSign.ApicalPulse = questions.AnswerOrEmptyString("GenericPulse").ToIntegerAfterNonDigitStrip();
                                vitalSign.PainLevel = questions.AnswerOrEmptyString("GenericPainLevel").ToIntegerAfterNonDigitStrip();
                                vitalSign.OxygenSaturation = questions.AnswerOrEmptyString("GenericO2Sat");
                                vitalSign.BSMax = questions.AnswerOrEmptyString("GenericBloodSugar").ToIntegerAfterNonDigitStrip();
                                vitalSign.Weight = questions.AnswerOrEmptyString("GenericWeight").ToIntegerAfterNonDigitStrip();
                                vitalSign.Temp = questions.AnswerOrEmptyString("GenericTemp").ToIntegerAfterNonDigitStrip();
                                vitalSign.Resp = questions.AnswerOrEmptyString("GenericResp").ToIntegerAfterNonDigitStrip();
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }
                    }
                    else if (disciplineTasksSNVList.Contains(s.DisciplineTask))
                    {
                        var questionData = noteQuestionDatas.SingleOrDefault(nq => nq.Id == s.Id);
                        if (questionData != null)
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = questionData.Question.ToDictionary(q => q.Name, q => q);
                            vitalSign.VisitDate = s.VisitDate;
                            if (questions != null && questions.Count > 0)
                            {
                                vitalSign.Temp = questions.AnswerOrEmptyString("GenericTemp").ToIntegerAfterNonDigitStrip();
                                vitalSign.Resp = questions.AnswerOrEmptyString("GenericResp").ToIntegerAfterNonDigitStrip();


                                vitalSign.BPLyingLeft = questions.AnswerOrEmptyString("GenericBPLeftLying");
                                vitalSign.BPLyingRight = questions.AnswerOrEmptyString("GenericBPRightLying");

                                vitalSign.BPSittingLeft = questions.AnswerOrEmptyString("GenericBPLeftSitting");
                                vitalSign.BPSittingRight = questions.AnswerOrEmptyString("GenericBPRightSitting");

                                vitalSign.BPStandingLeft = questions.AnswerOrEmptyString("GenericBPLeftStanding");
                                vitalSign.BPStandingRight = questions.AnswerOrEmptyString("GenericBPRightStanding");
                                GetBPValuesForGraphing(vitalSign);

                                vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");
                                GetBPValuesForGraphing(vitalSign);

                                var bs = new List<double>(); //new double[] { BSAM, BSNoon, BSPM, BSHS };
                                var BSAM = questions.AnswerDoubleOrZero("GenericBloodSugarAMLevelText");
                                if (BSAM > 0)
                                {
                                    bs.Add(BSAM);
                                }
                                var BSNoon = questions.AnswerDoubleOrZero("GenericBloodSugarLevelText");
                                if (BSNoon > 0)
                                {
                                    bs.Add(BSNoon);
                                }
                                var BSPM = questions.AnswerDoubleOrZero("GenericBloodSugarPMLevelText");
                                if (BSPM > 0)
                                {
                                    bs.Add(BSPM);
                                }
                                var BSHS = questions.AnswerDoubleOrZero("GenericBloodSugarHSLevelText");
                                if (BSHS > 0)
                                {
                                    bs.Add(BSHS);
                                }
                                var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                vitalSign.ApicalPulse = questions.AnswerOrEmptyString("GenericPulseApical").ToIntegerAfterNonDigitStrip();
                                vitalSign.RadialPulse = questions.AnswerOrEmptyString("GenericPulseRadial").ToIntegerAfterNonDigitStrip();
                                vitalSign.BSMax = maxBs;//> 0 ? maxBs.ToString() : string.Empty;// questions.AnswerOrEmptyString("GenericBloodSugarLevelText");
                                vitalSign.BSMin = minBs;//> 0 ? minBs.ToString() : string.Empty;
                                vitalSign.Weight = questions.AnswerOrEmptyString("GenericWeight").ToDoubleAfterNonDigitStrip();
                                vitalSign.PainLevel = questions.AnswerOrEmptyString("GenericIntensityOfPain").ToIntegerAfterNonDigitStrip();
                                vitalSign.OxygenSaturation = questions.AnswerOrEmptyString("GenericPulseOximetry");
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }
                    }
                });
            }
            return vitalSigns;
        }

        public List<VitalSignLogListItem> GetPatientNewVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSignLogListItem>();
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var scheduleEvents = scheduleRepository.GetScheduledEventsOnlyLean(Current.AgencyId, patientId, startDate, endDate, null, noteDisciplineTasks.ToArray(), false);
            var disciplineTasks = lookUpRepository.DisciplineTasks();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var oldVitalSignTasks = scheduleEvents.Where(s =>
                {
                    var discipline = disciplineTasks.Single(d => d.Id == s.DisciplineTask);
                    return !discipline.NewVitalSignsVersion.HasValue || discipline.NewVitalSignsVersion > s.Version;
                }).ToList();
                var newVitalSignTasks = scheduleEvents.Where(s => disciplineTasks.Single(d => d.Id == s.DisciplineTask).NewVitalSignsVersion <= s.Version).ToList();
                vitalSigns.AddRange(VitalSignFromList(oldVitalSignTasks).Select(v => new VitalSignLogListItem(v)));
                vitalSigns.AddRange(NewVitalSignFromList(patientId, newVitalSignTasks));
            }
            return vitalSigns;
        }

        public List<VitalSignLogListItem> GetPatientNewVitalSigns(Guid patientId, int limit)
        {
            var vitalSigns = new List<VitalSignLogListItem>();
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var scheduleEvents = scheduleRepository.GetScheduledEventsForOASISAndNotesByPatient(Current.AgencyId, patientId, limit, noteDisciplineTasks.ToArray());
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = NewVitalSignFromList(patientId, scheduleEvents);
            }
            return vitalSigns;
        }


        public List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSign>();
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var scheduleEvents = scheduleRepository.GetScheduledEventsOnlyLean(Current.AgencyId, patientId, startDate, endDate, null, noteDisciplineTasks.ToArray(), false);
            
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = VitalSignFromList(scheduleEvents);
            }
            return vitalSigns;
        }

        public List<VitalSign> GetPatientVitalSigns(Guid patientId, int limit)
        {
            var vitalSigns = new List<VitalSign>();
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var scheduleEvents = scheduleRepository.GetScheduledEventsForOASISAndNotesByPatient(Current.AgencyId, patientId, limit, noteDisciplineTasks.ToArray());
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = VitalSignFromList(scheduleEvents);
            }
            return vitalSigns;
        }

        protected override List<VitalSign> GetAppSpecificVitalSigns(Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime eventDate)
        {
            return GetVitalSignsForSixtyDaySummary(patientId, episodeId, startDate, endDate, eventDate);
        }

        protected List<VitalSign> GetVitalSignsForSixtyDaySummary(Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime date)
        {
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var vitalSigns = new List<VitalSign>();
            var scheduleEvents = scheduleRepository.GetScheduledEventsForOASISAndNotesByEpisode(Current.AgencyId, patientId, episodeId, startDate, date > endDate ? endDate : date, noteDisciplineTasks.ToArray());
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = VitalSignFromList(scheduleEvents);
            }
            return vitalSigns;
        }

        #endregion

        protected override ScheduleEvent SetAppSpecificForSave( ScheduleEvent scheduleTask, SaveNoteArguments savingArguments, DisciplineTask disciplineTask)
        {

            if (scheduleTask != null && savingArguments != null)
            {
                if (disciplineTask.IsVisitParameterVisible || disciplineTask.IsTimeIntervalVisible)
                {
                    if (savingArguments.VisitDate.Date > DateTime.MinValue.Date)
                    {
                        scheduleTask.VisitDate = savingArguments.VisitDate;
                    }
                }
                if (disciplineTask.IsTimeIntervalVisible)
                {
                    scheduleTask.TimeIn = savingArguments.TimeIn;
                    scheduleTask.TimeOut = savingArguments.TimeOut;
                }
            }
            return scheduleTask;
        }

        protected override void SetTaskSpecificInformation(VisitNoteViewData noteViewData, ScheduleEvent scheduleEvent, DisciplineTask disciplineTask, bool isNoteStillNotStarted)
        {
            base.SetTaskSpecificInformation(noteViewData, scheduleEvent, disciplineTask, isNoteStillNotStarted);
            noteViewData.TimeIn = scheduleEvent.TimeIn;
            noteViewData.TimeOut = scheduleEvent.TimeOut;
            noteViewData.VisitStartDate = scheduleEvent.VisitDate.IsValid() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;// arguments.EventDate.ToString("MM/dd/yyyy");
            noteViewData.EventStartDate = scheduleEvent.EventDate;
            var viewIdentifier = (int)NoteArgumentsType.View;
            if (((disciplineTask.Diagnosis & viewIdentifier) == viewIdentifier) || disciplineTask.IsOasisCarePlan)//(arguments.IsDiagnosisNeeded )|| (arguments.IsPlanOfCareNeeded)
            {
                var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessment(new PatientEpisode { StartDate = noteViewData.StartDate, EndDate = noteViewData.EndDate, PatientId = noteViewData.PatientId, Id = noteViewData.EpisodeId }, false);
                if (assessment != null)
                {
                    if (isNoteStillNotStarted && ((disciplineTask.Diagnosis & viewIdentifier) == viewIdentifier))//arguments.IsDiagnosisNeeded
                    {
                        IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                        var questionsData = mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id);
                        if (questionsData != null)
                        {
                            assessment.Questions = questionsData.Question;
                            oasisQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses);
                        }
                        noteViewData.Questions = this.GetDiagnosisMergedFromOASIS(noteViewData.Questions, oasisQuestions);
                    }
                    if (disciplineTask.IsOasisCarePlan)//(arguments.IsPlanOfCareNeeded)
                    {
                        var planOfCare = this.GetOasisPlanofCare(noteViewData.EpisodeId, noteViewData.PatientId, assessment.Id);
                        if (planOfCare != null)
                        {
                            noteViewData.CarePlanOrEvalId = planOfCare.Id;
                            noteViewData.CarePlanOrEvalGroup = DocumentType.PlanOfCare.ToString().ToLowerCase();
                        }
                    }
                }
            }
        }


        protected override void SetTaskSpecificInformationPrint(VisitNoteViewData noteViewData, ScheduleEvent scheduleEvent, DisciplineTask disciplineTask)
        {
            noteViewData.VisitStartDate = scheduleEvent.VisitDate.IsValid() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
            var printIdentifier = (int)NoteArgumentsType.Print;
            if ((disciplineTask.Diagnosis & printIdentifier) == printIdentifier)//(arguments.IsAssessmentNeeded)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessment(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventDate);
                if (assessment != null)
                {
                    assessment.Questions = mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id).Question;
                    oasisQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses);
                }
                if (oasisQuestions.IsNotNullOrEmpty())
                {
                    noteViewData.IsDiagnosisVisible = true;
                }
                noteViewData.Questions = this.GetDiagnosisMergedFromOASIS(noteViewData.Questions, oasisQuestions);
            }
            var selectedEpisodeId = noteViewData.Questions != null && noteViewData.Questions.ContainsKey("SelectedEpisodeId") && noteViewData.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? noteViewData.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
            if (!selectedEpisodeId.IsEmpty())
            {
                base.SetEpisodeRange(noteViewData, selectedEpisodeId, scheduleEvent.PatientId);
            }
            else
            {
                base.SetEpisodeRange(noteViewData, scheduleEvent.EpisodeId,scheduleEvent.PatientId);
            }
        }

        protected override PatientEpisode GetEpisodeForNote(SaveNoteArguments savingArguments)
        {
            return episodeRepository.GetEpisodeOnly(Current.AgencyId, savingArguments.EpisodeId, savingArguments.PatientId);
        }
        
        protected PlanofCare GetOasisPlanofCare(Guid episodeId, Guid patientId, Guid assessmentId)
        {
            return planofCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, episodeId, assessmentId);
            //if (planofCare != null)
            //{
            //    return new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({")
            //        .AppendFormat("Url: '/485/PrintPreview/{0}/{1}/{2}',", planofCare.EpisodeId, planofCare.PatientId, planofCare.Id)
            //        .Append("PdfUrl: '/Oasis/PlanOfCarePdf',")
            //        .AppendFormat("PdfData: {{ 'episodeId': '{0}', 'patientId': '{1}', 'eventId': '{2}' }}", planofCare.EpisodeId, planofCare.PatientId, planofCare.Id)
            //        .Append("})\">View Plan of Care</a>").ToString();
            //}
            //return string.Empty;
        }

        protected IDictionary<string, NotesQuestion> GetDiagnosisMergedFromOASIS(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            NotesQuestion primaryDiagnosis = null;
            if (oasisQuestions.TryGetValue("PrimaryDiagnosis", out primaryDiagnosis))
            {
                if (primaryDiagnosis != null)
                {
                    if (noteQuestions.ContainsKey("PrimaryDiagnosis"))
                    {
                        noteQuestions["PrimaryDiagnosis"].Answer = primaryDiagnosis != null ? primaryDiagnosis.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("PrimaryDiagnosis", primaryDiagnosis);
                    }
                }
            }

            NotesQuestion ICD9M = null;
            if (oasisQuestions.TryGetValue("ICD9M", out ICD9M))
            {
                if (ICD9M != null)
                {
                    if (noteQuestions.ContainsKey("ICD9M"))
                    {
                        noteQuestions["ICD9M"].Answer = ICD9M != null ? ICD9M.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("ICD9M", ICD9M);
                    }
                }
            }

            NotesQuestion primaryDiagnosis1 = null;
            if (oasisQuestions.TryGetValue("PrimaryDiagnosis1", out primaryDiagnosis1))
            {
                if (primaryDiagnosis1 != null)
                {
                    if (noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                    {
                        noteQuestions["PrimaryDiagnosis1"].Answer = primaryDiagnosis1 != null ? primaryDiagnosis1.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("PrimaryDiagnosis1", primaryDiagnosis1);
                    }
                }
            }

            NotesQuestion ICD9M1 = null;
            if (oasisQuestions.TryGetValue("ICD9M1", out ICD9M1))
            {
                if (ICD9M1 != null)
                {
                    if (noteQuestions.ContainsKey("ICD9M1"))
                    {
                        noteQuestions["ICD9M1"].Answer = ICD9M1 != null ? ICD9M1.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("ICD9M1", ICD9M1);
                    }
                }
            }

            return noteQuestions;
        }

        protected override List<Validation> AddSaveNotesValidationRules(string button, PatientEpisode episode, SaveNoteArguments noteBasicData, DisciplineTask disciplineTask)
        {
            var rules = new List<Validation>();
            if (disciplineTask.IsVisitParameterVisible)
            {
                rules.Add(new Validation(() => !noteBasicData.VisitDate.IsValid(), "Visit date is not valid."));
                if (!DisciplineTaskFactory.OutOfEpisodeDisciplineTask().Contains(disciplineTask.Id))
                {
                    rules.Add(new Validation(() => !noteBasicData.VisitDate.IsValid() || !(noteBasicData.VisitDate.Date >= episode.StartDate.Date && noteBasicData.VisitDate.Date <= episode.EndDate.Date), "Visit date is not in the episode range."));
                }
            }
            CommonSaveNotesValidationRules(rules, button, noteBasicData, episode);
            if (button == "Complete" || button == "Approve")
            {

            }
            return rules;
        }
    }
}
