﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Application.Helpers;

    public class HHTaskService : TaskService<ScheduleEvent, PatientEpisode>
    {
        private readonly HHTaskRepository scheduleRepository;
        private readonly HHEpisodeRepository episodeRepository;

        public HHTaskService(HHDataProvider dataProvider, ILookUpDataProvider lookupDataProvider)
            : base(dataProvider.TaskRepository, dataProvider.MultiDocument, dataProvider.EpisodeRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "agencyManagementDataProvider");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");

            base.lookupRepository = lookupDataProvider.LookUpRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            
            this.assetRepository = dataProvider.AssetRepository;
            this.userRepository = dataProvider.UserRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            base.patientAdmissionRepository = dataProvider.PatientAdmissionRepository;
            base.Service = AgencyServices.HomeHealth;

        }
       
        #region Therapy Exception

        public List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate,int statusOptional)
        {
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            var aprilStartDate = new DateTime(2011, 04, 01);
            startDate = startDate > aprilStartDate ? startDate : aprilStartDate;
            if (endDate > startDate)
            {
                var allScheduleEvents = scheduleRepository.GetTherapyExceptionScheduleEvents(Current.AgencyId, branchId,patientId, new DateTime(2011, 04, 01), DateTime.Now,statusOptional);
                if (allScheduleEvents != null && allScheduleEvents.Count > 0)
                {
                    var episodeIds = allScheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                    if (episodeIds != null && episodeIds.Count > 0)
                    {
                        var completedNoteStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA();
                        episodeIds.ForEach(Id =>
                        {
                            var scheduleEvents = allScheduleEvents.Where(e => e.EpisodeId == Id).OrderBy(e => e.EventDate.Date).ToList();
                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                var schedule = scheduleEvents.FirstOrDefault();
                                if (schedule != null)
                                {
                                    var episode = new PatientEpisodeTherapyException
                                    {
                                        PatientIdNumber = schedule.PatientIdNumber,
                                        EndDate = schedule.EndDate,
                                        StartDate = schedule.StartDate,
                                        PatientName = schedule.PatientName
                                    };
                                    var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                                    if (disciplines.Length == 1)
                                    {
                                        var evnt19check = true;
                                        var evnt13check = true;
                                        if (scheduleEvents.Count >= 13)
                                        {
                                            var evnt13 = scheduleEvents[12];
                                            if (evnt13.EventDate.IsValid())
                                            {
                                                var date13 = evnt13.EventDate;
                                                var scheduleEvents13 = scheduleEvents.Where(e => (e.EventDate.Date == date13.Date)).ToList();
                                                if (scheduleEvents13 != null)
                                                {
                                                    if (evnt13.Discipline == "PT")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                    }
                                                    else if (evnt13.Discipline == "OT")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                    else if (evnt13.Discipline == "ST")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                }
                                            }
                                            if (evnt13 != null)
                                            {
                                                episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate > DateTime.MinValue ? evnt13.EventDate.ToString("MM/dd") : string.Empty);
                                            }
                                            if (scheduleEvents.Count >= 19)
                                            {
                                                var evnt19 = scheduleEvents[18];
                                                if (evnt19.EventDate.IsValid())
                                                {
                                                    var date19 = evnt19.EventDate;
                                                    var scheduleEvents19 = scheduleEvents.Where(e => (e.EventDate.Date == date19.Date)).ToList();
                                                    if (scheduleEvents19 != null)
                                                    {
                                                        if (evnt19.Discipline == "PT")
                                                        {
                                                            evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                        }
                                                        else if (evnt19.Discipline == "OT")
                                                        {
                                                            evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                        else if (evnt19.Discipline == "ST")
                                                        {
                                                            evnt13check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                    }
                                                }
                                                if (evnt19 != null)
                                                {
                                                    episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate > DateTime.MinValue ? evnt19.EventDate.ToString("MM/dd") : string.Empty);
                                                }

                                            }
                                            else
                                            {
                                                episode.NineteenVisit = "NA";
                                            }
                                            if (!evnt19check || !evnt13check)
                                            {
                                                episode.ScheduledTherapy = scheduleEvents.Count;
                                                var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                                if (completedSchedules != null)
                                                {
                                                    episode.CompletedTherapy = completedSchedules.Count;
                                                }
                                                episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                                episode.Schedule = string.Empty;
                                                therapyEpisodes.Add(episode);
                                            }
                                        }
                                    }
                                    else if (disciplines.Length > 1)
                                    {
                                        var discipline = string.Join(";", disciplines);
                                        var evnt19check = true;
                                        var evnt13check = true;
                                        var first = true;
                                        if (scheduleEvents.Count >= 13 && discipline.IsNotNullOrEmpty())
                                        {
                                            var scheduleEvents13 = scheduleEvents.Take(13).Reverse().ToList();
                                            var scheduleEvents13End = scheduleEvents13.Take(3).ToList();
                                            if (scheduleEvents13End != null)
                                            {
                                                if (discipline.Contains("PT"))
                                                {
                                                    first = false;
                                                    evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                }
                                                if (discipline.Contains("OT"))
                                                {
                                                    if (first)
                                                    {
                                                        first = false;
                                                        evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                    else
                                                    {
                                                        evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                }
                                                if (discipline.Contains("ST"))
                                                {
                                                    if (first)
                                                    {
                                                        first = false;
                                                        evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                    else
                                                    {
                                                        evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                }
                                            }
                                            if (!evnt13check)
                                            {
                                                var evnt13 = scheduleEvents[12];
                                                if (evnt13 != null)
                                                {
                                                    episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate > DateTime.MinValue ? evnt13.EventDate.ToString("MM/dd") : string.Empty);
                                                }
                                            }
                                            first = true;
                                            if (scheduleEvents.Count >= 19)
                                            {
                                                var evnt19 = scheduleEvents[18];
                                                var scheduleEvents19 = scheduleEvents.Take(19).Reverse().ToList();
                                                var scheduleEvents19End = scheduleEvents19.Take(3).ToList();
                                                if (scheduleEvents19End != null)
                                                {
                                                    if (discipline.Contains("PT"))
                                                    {
                                                        first = false;
                                                        evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);

                                                    }

                                                    if (discipline.Contains("OT"))
                                                    {
                                                        if (first)
                                                        {
                                                            first = false;
                                                            evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                        else
                                                        {
                                                            evnt19check = evnt19check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                    }
                                                    if (discipline.Contains("ST"))
                                                    {
                                                        if (first)
                                                        {
                                                            first = false;
                                                            evnt13check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                        else
                                                        {
                                                            evnt13check = evnt13check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                    }
                                                }
                                                if (evnt19 != null)
                                                {
                                                    episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate > DateTime.MinValue ? evnt19.EventDate.ToString("MM/dd") : string.Empty);
                                                }
                                            }
                                            if (!evnt19check || !evnt13check)
                                            {
                                                episode.ScheduledTherapy = scheduleEvents.Count;
                                                var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                                if (completedSchedules != null)
                                                {
                                                    episode.CompletedTherapy = completedSchedules.Count;
                                                }
                                                episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                                episode.Schedule = string.Empty;
                                                therapyEpisodes.Add(episode);
                                            }
                                        }
                                    }
                                }
                            }

                        });
                    }
                }
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate, int count, int statusOptional)
        {
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            var aprilStartDate = new DateTime(2011, 04, 01);
            startDate = startDate > aprilStartDate ? startDate : aprilStartDate;
            if (endDate > startDate)
            {
                var allScheduleEvents = scheduleRepository.GetTherapyExceptionScheduleEvents(Current.AgencyId, branchId,patientId, startDate, endDate,statusOptional);
                if (allScheduleEvents != null && allScheduleEvents.Count > 0)
                {
                    var episodeIds = allScheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                    if (episodeIds != null && episodeIds.Count > 0)
                    {
                        var completedNoteStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA();
                        episodeIds.ForEach(Id =>
                        {
                            var scheduleEvents = allScheduleEvents.Where(e => e.EpisodeId == Id).OrderBy(e => e.EventDate.Date).ToList();
                            if (scheduleEvents != null && scheduleEvents.Count >= count)
                            {
                                var schedule = scheduleEvents.FirstOrDefault();
                                if (schedule != null)
                                {
                                    var episode = new PatientEpisodeTherapyException
                                    {
                                        PatientIdNumber = schedule.PatientIdNumber,
                                        EndDate = schedule.EndDate,
                                        StartDate = schedule.StartDate,
                                        PatientName = schedule.PatientName
                                    };

                                    var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                                    if (disciplines.Length > 0)
                                    {
                                        var discipline = string.Join(";", disciplines);
                                        if (discipline.IsNotNullOrEmpty())
                                        {
                                            var evntCommon = scheduleEvents[count - 1];
                                            var scheduleEventsCommon = scheduleEvents.Take(count).Reverse().ToList();
                                            var scheduleEventsCommonEnd = scheduleEventsCommon.Take(3).ToList();
                                            if (scheduleEventsCommonEnd != null)
                                            {
                                                if (discipline.Contains("PT"))
                                                {
                                                    var ptEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation);
                                                    if (ptEval != null && ptEval.EventDate.IsValid())
                                                    {
                                                        episode.PTEval = ptEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.PTEval = "NA";
                                                }
                                                if (discipline.Contains("OT"))
                                                {
                                                    var otEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation);
                                                    if (otEval != null && otEval.EventDate.IsValid())
                                                    {
                                                        episode.OTEval = otEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.OTEval = "NA";
                                                }
                                                if (discipline.Contains("ST"))
                                                {
                                                    var stEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation);
                                                    if (stEval != null && stEval.EventDate.IsValid())
                                                    {
                                                        episode.STEval = stEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.STEval = "NA";
                                                }
                                            }
                                            episode.ScheduledTherapy = scheduleEvents.Count;
                                            var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                            if (completedSchedules != null)
                                            {
                                                episode.CompletedTherapy = completedSchedules.Count;
                                            }
                                            episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                            therapyEpisodes.Add(episode);
                                        }
                                    }

                                }
                            }
                        });
                    }
                }
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        #endregion

        public CalendarViewData GetPatientScheduleDataForCenter(Guid patientId)
        {
            var viewData = new CalendarViewData();
            viewData.PatientId = patientId;
            var profile = profileRepository.GetProfileOnly(Current.AgencyId, patientId);
            if (profile != null)
            {
                viewData = GetScheduleWithPreviousAfterEpisodeInfo(patientId, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();
                viewData.PatientId = profile.Id;
                viewData.IsDischarged = profile.IsDischarged;
                viewData.DisplayName = profile.DisplayName;
                var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Episode);
                if (permissions.IsNotNullOrEmpty())
                {
                    var isForzen = Current.IsAgencyFrozen;
                    viewData.IsUserCanAddEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Add);
                    viewData.IsUserCanViewEpisodeList = permissions.IsInPermission(this.Service, PermissionActions.ViewList);
                    viewData.IsUserCanEditEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Edit);
                }
            }
            return viewData;
        }

        public CalendarViewData GetPatientScheduleDataForCenter(Guid patientId, Guid episodeId, string discipline)
        {
            var viewData = new CalendarViewData();
            viewData.PatientId = patientId;
            var profile = profileRepository.GetProfileOnly(Current.AgencyId, patientId);
            if (profile != null)
            {
                viewData.LocationId = profile.AgencyLocationId;
                viewData = GetScheduleWithPreviousAfterEpisodeInfo(patientId, episodeId, discipline, true, true, true, false) ?? new CalendarViewData();
                viewData.PatientId = profile.Id;
                viewData.IsDischarged = profile.IsDischarged;
                viewData.DisplayName = profile.DisplayName;
                var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Episode);
                if (permissions.IsNotNullOrEmpty())
                {
                    var isForzen = Current.IsAgencyFrozen;
                    viewData.IsUserCanAddEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Add);
                    viewData.IsUserCanViewEpisodeList = permissions.IsInPermission(this.Service, PermissionActions.ViewList);
                    viewData.IsUserCanEditEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Edit);
                }
            }
            return viewData;
        }

        public CalendarViewData GetScheduleDataForCalendar(Guid patientId, string discipline)
        {
            CalendarViewData calendarViewData = null;
            if (!patientId.IsEmpty())
            {
                var profile = profileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    calendarViewData.LocationId = profile.AgencyLocationId;
                    calendarViewData = GetScheduleWithPreviousAfterEpisodeInfo(patientId, DateTime.Now, discipline, true, true, false) ?? new CalendarViewData();
                    calendarViewData.DisplayName = profile.DisplayName;
                    calendarViewData.IsDischarged = profile.IsDischarged;
                    var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Episode);
                    if (permissions.IsNotNullOrEmpty())
                    {
                        var isForzen = Current.IsAgencyFrozen;
                        calendarViewData.IsUserCanAddEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Add);
                        calendarViewData.IsUserCanViewEpisodeList = permissions.IsInPermission(this.Service, PermissionActions.ViewList);
                        calendarViewData.IsUserCanEditEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Edit);
                    }
                }
            }
            return calendarViewData;
        }

        public CalendarViewData GetScheduleDataForCalendar(Guid patientId, Guid episodeId, string discipline)
        {
            CalendarViewData calendarViewData = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                var profile = profileRepository.GetProfileOnly(Current.AgencyId,patientId);
                if (profile != null)
                {
                    calendarViewData.LocationId = profile.AgencyLocationId;
                    calendarViewData = GetScheduleWithPreviousAfterEpisodeInfo(patientId, episodeId, discipline, true, true, false, false) ?? new CalendarViewData();
                    calendarViewData.PatientId = patientId;
                    calendarViewData.IsDischarged = profile.IsDischarged;
                    var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Episode);
                    if (permissions.IsNotNullOrEmpty())
                    {
                        var isForzen = Current.IsAgencyFrozen;
                        calendarViewData.IsUserCanAddEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Add);
                        calendarViewData.IsUserCanViewEpisodeList = permissions.IsInPermission(this.Service, PermissionActions.ViewList);
                        calendarViewData.IsUserCanEditEpisode = !isForzen && permissions.IsInPermission(this.Service, PermissionActions.Edit);
                    }
                }
            }
            return calendarViewData;
        }

        #region Schedule Center and Master Calendar

        public CalendarViewData GetMasterCalendarViewData(Guid patientId, Guid episodeId, string discipline, bool isAfterEpisode, bool isBeforeEpisode, bool isScheduleEventsActionIncluded, bool isFrequencyIncluded)
        {
            var data = GetScheduleWithPreviousAfterEpisodeInfo(patientId, episodeId, discipline, isAfterEpisode, isBeforeEpisode, isScheduleEventsActionIncluded,isFrequencyIncluded);
            if (data != null)
            {
                var admission = patientAdmissionRepository.GetPatientAdmissionDate(Current.AgencyId,patientId, data.EpisodeAdmissionId);
                if (admission != null)
                {
                    data.StartOfCareDate = admission.StartOfCareDate;
                    if (admission.PatientData.IsNotNullOrEmpty())
                    {
                        var patient = admission.PatientData.ToObject<Patient>();
                        if (patient != null)
                        {
                            data.PatientIdNumber = patient.PatientIdNumber;
                            data.DisplayName = patient.DisplayNameWithMi;
                        }
                    }
                }
            }
            return data;
        }

        public CalendarViewData GetScheduleWithPreviousAfterEpisodeInfo(Guid patientId, DateTime date, string discipline, bool isAfterEpisode, bool isBeforeEpisode, bool isScheduleEventsActionIncluded)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            var calendarData = new CalendarViewData { PatientId = patientId, Discpline = discipline };
            var episode = episodeRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientId, date);
            if (episode != null && !episode.Id.IsEmpty())
            {
                SetPermission(calendarData);
                calendarData.StartDate = episode.StartDate;
                calendarData.EndDate = episode.EndDate;
                calendarData.EpisodeId = episode.Id;
                calendarData.IsEpisodeExist = true;
                calendarData.EpisodeAdmissionId = episode.AdmissionId;
                var events = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { episode.Id }, episode.StartDate, episode.EndDate, discipline, true, false);// FilterForScheduleCenter(episode, discipline, true);
                if (events.IsNotNullOrEmpty())
                {
                    if (isScheduleEventsActionIncluded)
                    {
                        ProcessScheduleEventsActions2(calendarData, episode, events);
                    }
                    else
                    {
                        PrecessScheduleEventForUsersOnly2(calendarData, events);
                    }
                    //calendarData.ScheduleEvents = events;
                }
                if (isAfterEpisode && episode.EndDate.IsValid())
                {
                    var nextEpisode = episodeRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, episode.EndDate);
                    if (nextEpisode != null && !nextEpisode.Id.IsEmpty())
                    {
                        calendarData.NextEpisode = nextEpisode.Id;
                    }
                }
                if (isBeforeEpisode && episode.StartDate.IsValid())
                {
                    var previousEpisode = episodeRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, episode.StartDate);
                    if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                    {
                        calendarData.PreviousEpisode = previousEpisode.Id;
                    }
                }
            }
            else
            {
                SetPermission(calendarData);
                var futureEpisode = episodeRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, date); //allEpisodes.Where(e => e.IsActive).LastOrDefault();
                if (futureEpisode != null)
                {
                    var events = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { futureEpisode.Id }, futureEpisode.StartDate, futureEpisode.EndDate, discipline, true, false); //FilterForScheduleCenter(futureEpisode, discipline, true);
                    if (events.IsNotNullOrEmpty())
                    {
                        if (isScheduleEventsActionIncluded)
                        {
                            ProcessScheduleEventsActions2(calendarData, futureEpisode, events);
                        }
                        else
                        {
                            PrecessScheduleEventForUsersOnly2(calendarData, events);
                        }
                        //calendarData.ScheduleEvents = events.OrderByDescending(s => s.EventDate).ToList();
                    }
                    if (isAfterEpisode)
                    {
                        if (futureEpisode.EndDate.IsValid())
                        {
                            var nextEpisode = episodeRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, futureEpisode.EndDate);
                            if (nextEpisode != null && !nextEpisode.Id.IsEmpty())
                            {
                                calendarData.NextEpisode = nextEpisode.Id;
                            }
                        }
                    }
                    calendarData.PreviousEpisode = Guid.Empty;
                    calendarData.IsEpisodeExist = true;
                    calendarData.StartDate = futureEpisode.StartDate;
                    calendarData.EndDate = futureEpisode.EndDate;
                    calendarData.EpisodeId = futureEpisode.Id;
                    calendarData.EpisodeAdmissionId = futureEpisode.AdmissionId;
                }
            }
            return calendarData;
        }

        public CalendarViewData GetScheduleWithPreviousAfterEpisodeInfo(Guid patientId, Guid episodeId, string discipline, bool isAfterEpisode, bool isBeforeEpisode, bool isScheduleEventsActionIncluded, bool isFrequencyIncluded)
        {
            var calendarData = new CalendarViewData { PatientId = patientId, Discpline = discipline };
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null && !episode.Id.IsEmpty())
            {
                SetPermission(calendarData);
                calendarData.StartDate = episode.StartDate;
                calendarData.EndDate = episode.EndDate;
                calendarData.EpisodeId = episode.Id;
                calendarData.IsEpisodeExist = true;
                calendarData.EpisodeAdmissionId = episode.AdmissionId;
                var events = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { episode.Id }, episode.StartDate, episode.EndDate, discipline, true, false);  //FilterForScheduleCenter(episode, discipline, true);
                if (events != null && events.Count > 0)
                {
                    if (isScheduleEventsActionIncluded)
                    {
                        ProcessScheduleEventsActions2(calendarData, episode, events);
                    }
                    else
                    {
                        PrecessScheduleEventForUsersOnly2(calendarData, events);
                    }
                    //calendarData.ScheduleEvents = events.OrderByDescending(s => s.EventDate).ToList();
                }
                if (isAfterEpisode)
                {
                    if (episode.EndDate.IsValid())
                    {
                        var nextEpisode = episodeRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, episode.EndDate);
                        if (nextEpisode != null && !nextEpisode.Id.IsEmpty())
                        {
                            calendarData.NextEpisode = nextEpisode.Id;
                        }
                    }
                }
                PatientEpisode previousEpisode = null;
                if (isBeforeEpisode)
                {
                    if (episode.StartDate.IsValid())
                    {
                        previousEpisode = episodeRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, episode.StartDate);
                        if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                        {
                            calendarData.PreviousEpisode = previousEpisode.Id;
                        }
                    }
                }
                if (isFrequencyIncluded)
                {
                    calendarData.FrequencyList = EpisodeAssessmentHelperFactory<PatientEpisode,ScheduleEvent>.GetFrequencyForCalendar(episode, previousEpisode);
                }
            }
            return calendarData;
        }

        #endregion

        #region Add Schedule Events Helpers
   
        public List<ScheduleEvent> MultiDateRangeScheduleTaskHelper(PatientEpisode episode, DisciplineTask disciplineTask, Guid userId, DateTime startDate, DateTime endDate)
        {
            var newEvents = new List<ScheduleEvent>();
            if (episode != null)
            {
                int dateDifference = endDate.Subtract(startDate).Days + 1;
                for (int i = 0; i < dateDifference; i++)
                {
                    newEvents.Add(
                        new ScheduleEvent
                        {
                            AgencyId = episode.AgencyId,
                            Id = Guid.NewGuid(),
                            PatientId = episode.PatientId,
                            EpisodeId = episode.Id,
                            UserId = userId,
                            Status = disciplineTask.DefaultStatus,
                            DisciplineTask = disciplineTask.Id,
                            Discipline = disciplineTask.Discipline,
                            Version=disciplineTask.Version,
                            EventDate = startDate.AddDays(i),
                            VisitDate = startDate.AddDays(i),
                            StartDate = episode.StartDate,
                            EndDate = episode.EndDate,
                            IsBillable = disciplineTask.IsBillable,
                            IsDeprecated = false
                        });
                }
            }
            return newEvents;
        }

        public List<ScheduleEvent> AddMultiDayScheduleTaskHelper(PatientEpisode episode, DisciplineTask disciplineTask, Guid userId, List<DateTime> visitDates)
        {
            var scheduledEvents = new List<ScheduleEvent>();
            if (episode != null)
            {
                if (visitDates != null && visitDates.Count > 0)
                {
                    visitDates.ForEach(date =>
                    {
                        var newScheduledEvent = new ScheduleEvent
                        {
                            AgencyId = episode.AgencyId,
                            Id = Guid.NewGuid(),
                            PatientId = episode.PatientId,
                            EpisodeId = episode.Id,
                            UserId = userId,
                            DisciplineTask = disciplineTask.Id,
                            Discipline = disciplineTask.Discipline,
                            Status = disciplineTask.DefaultStatus,
                            Version = disciplineTask.Version,
                            IsBillable = disciplineTask.IsBillable,
                            EventDate = date,
                            VisitDate = date,
                            StartDate = episode.StartDate,
                            EndDate = episode.EndDate,
                            IsDeprecated = false
                        };
                        scheduledEvents.Add(newScheduledEvent);
                    });
                }
            }
            return scheduledEvents;
        }

        protected override void SetScheduleTasksDefaultAppSpecific(ScheduleEvent ev)
        {
            ev.VisitDate = ev.EventDate;
        }

        protected override void SetScheduleTasksDefaultAppSpecific(GridTask task, ScheduleEvent ev)
        {
            task.DateIn = ev.EventDate;
            task.IsAllDay =false;
        }

        #endregion

        #region  Schedule Events

      
        public List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline)
        {
            var patientEvents = scheduleRepository.GetPatientScheduledEventsOnlyLeanNew(Current.AgencyId, episodeId, patientId, new int[] { }, discipline, new int[] { }, true);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        v.UserName = user.DisplayName;
                    }
                    if (v.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        v.UserName = "Axxess";
                    }
                });
            }
            return patientEvents;
        }

        #endregion

        //#region Episode Detail

        //public ScheduleEvent GetScheduledEventForDetail(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, episodeId, eventId);

        //    if (scheduleEvent != null && scheduleEvent.Discipline.IsEqual(Disciplines.Orders.ToString()))
        //    {
        //        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
        //        {
        //            var order = noteRepository.GetPhysicianOrder(eventId, patientId, Current.AgencyId);
        //            if (order != null)
        //            {
        //                scheduleEvent.PhysicianId = order.PhysicianId;
        //            }
        //        }
        //        else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485
        //            || scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)
        //        {
        //            var planofcare = planofCareRepository.Get<PlanofCare>(Current.AgencyId, patientId, eventId);
        //            if (planofcare != null)
        //            {
        //                scheduleEvent.PhysicianId = planofcare.PhysicianId;
        //            }
        //        }
        //        else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)
        //        {
        //            var planofcareStandalone = planofCareRepository.Get<PlanofCare>(Current.AgencyId, patientId, eventId);
        //            if (planofcareStandalone != null)
        //            {
        //                scheduleEvent.PhysicianId = planofcareStandalone.PhysicianId;
        //            }
        //        }
        //        else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter)
        //        {
        //            var faceToFace = noteRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
        //            if (faceToFace != null)
        //            {
        //                scheduleEvent.PhysicianId = faceToFace.PhysicianId;
        //            }
        //        }
        //    }

        //    return scheduleEvent;
        //}

        //#endregion

        public List<ScheduleEvent> GetCurrentAndPreviousOrders(PatientEpisode episode)
        {
            return scheduleRepository.GetCurrentAndPreviousOrders(Current.AgencyId, episode, DisciplineTaskFactory.AllOrders());
        }
      
        #region User Visits

        protected override bool GetDocumentsAndFiltersForWidget(IDictionary<int, Dictionary<int,List<int>>> permission, out int editDocuments, out string aditionalFilter)
        {
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            editDocuments = (int)DocumentType.None;
            aditionalFilter = string.Empty;
            GetViewAndSetEditPermission(permission, ParentPermission.Schedule, ref editDocuments, DocumentType.Notes | DocumentType.OASIS);
            if (GetViewAndSetEditPermission(permission, ParentPermission.Schedule, ref editDocuments, DocumentType.Notes | DocumentType.OASIS))
            {
                var excluded = new List<int>();
                if (!GetViewAndSetEditPermission(permission, ParentPermission.Orders, ref editDocuments, DocumentType.PhysicianOrder | DocumentType.FaceToFaceEncounter | DocumentType.PlanOfCare))
                {
                    excluded.AddRange(DisciplineTaskFactory.AllPhysicianOrders());
                }
                if (!GetViewAndSetEditPermission(permission, ParentPermission.ManageInfectionReport, ref editDocuments, DocumentType.Infection))
                {
                    excluded.Add((int)DisciplineTasks.InfectionReport);
                }
                if (!GetViewAndSetEditPermission(permission, ParentPermission.ManageIncidentAccidentReport, ref editDocuments, DocumentType.IncidentAccident))
                {
                    excluded.Add((int)DisciplineTasks.IncidentAccidentReport);
                }
                if (!GetViewAndSetEditPermission(permission, ParentPermission.CommunicationNote, ref editDocuments, DocumentType.CommunicationNote))
                {
                    excluded.Add((int)DisciplineTasks.CommunicationNote);
                }
                if (excluded.IsNotNullOrEmpty())
                {
                    aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} ) AND st.DisciplineTask NOT IN ({1})", status.Select(s => s.ToString()).ToArray().Join(","), excluded.ToCommaSeperatedList());
                }
                else
                {
                    aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} )", status.Select(s => s.ToString()).ToArray().Join(","));
                }
            }
            else
            {
                var included = new List<int>();
                if (GetViewAndSetEditPermission(permission, ParentPermission.Orders, ref  editDocuments, DocumentType.PhysicianOrder | DocumentType.FaceToFaceEncounter | DocumentType.PlanOfCare))
                {
                    included.AddRange(DisciplineTaskFactory.AllPhysicianOrders());
                }
                if (GetViewAndSetEditPermission(permission, ParentPermission.ManageInfectionReport, ref  editDocuments, DocumentType.Infection))
                {
                    included.Add((int)DisciplineTasks.InfectionReport);
                }
                if (GetViewAndSetEditPermission(permission, ParentPermission.ManageIncidentAccidentReport, ref  editDocuments, DocumentType.IncidentAccident))
                {
                    included.Add((int)DisciplineTasks.IncidentAccidentReport);
                }
                if (GetViewAndSetEditPermission(permission, ParentPermission.CommunicationNote, ref  editDocuments, DocumentType.CommunicationNote))
                {
                    included.Add((int)DisciplineTasks.CommunicationNote);
                }
                if (included.IsNotNullOrEmpty())
                {
                    aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} ) AND st.DisciplineTask IN ({1}) ", status.Select(s => s.ToString()).ToArray().Join(","), included.ToCommaSeperatedList());
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public UserCalendarViewData GetScheduleLean(Guid userId, DateTime from, DateTime to, bool isForPrint)
        {
            var viewData = new UserCalendarViewData { Service = this.Service, FromDate = from, ToDate = to };
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} ) ", status.Select(s => s.ToString()).ToArray().Join(","));
            var scheduleEvents = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, false, aditionalFilter);
            SetUserVisitData(viewData, scheduleEvents, isForPrint);
            return viewData;

            //var userVisits = new List<UserVisit>();
           
            //if (scheduleEvents != null && scheduleEvents.Count > 0)
            //{
            //    var users = new List<User>();
            //    var episodeIds = scheduleEvents.Select(e => e.EpisodeId).Distinct().ToList();
            //    var returnComments = scheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
            //    if (returnComments.IsNotNullOrEmpty())
            //    {
            //        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
            //        if (returnUserIds.IsNotNullOrEmpty())
            //        {
            //            var scheduleUsers = UserEngine.GetUsers(Current.AgencyId, returnUserIds) ?? new List<User>(); //userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, returnUserIds) ?? new List<User>();
            //            if (scheduleUsers.IsNotNullOrEmpty())
            //            {
            //                users.AddRange(scheduleUsers);
            //            }
            //        }
            //    }
            //    scheduleEvents.ForEach(s =>
            //    {
            //        var visitNote = string.Empty;
            //        var episodeDetail = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
            //        if (s.Comments.IsNotNullOrEmpty())
            //        {
            //            visitNote = s.Comments.Clean();
            //        }

            //        var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList();

            //        UrlFactory<ScheduleEvent>.SetEditUrl(s);
            //        userVisits.Add(new UserVisit
            //        {
            //            Id = s.Id,
            //            VisitNotes = visitNote,
            //            Group = s.Url,
            //            Status = s.Status,
            //            PatientName = s.PatientName,
            //            StatusComment =  TaskHelperFactory<ScheduleEvent>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users),
            //            DisciplineTask = s.DisciplineTask,
            //            EpisodeId = s.EpisodeId,
            //            PatientId = s.PatientId,
            //            EpisodeNotes = episodeDetail.Comments.Clean(),
            //            VisitDate = s.VisitDate,
            //            ScheduleDate = s.EventDate,
            //            IsMissedVisit = s.IsMissedVisit
            //        });
            //    });
            //}
            //return userVisits;
        }

        public UserCalendarViewData GetScheduleLeanViewData(Guid userId, DateTime from, DateTime to, bool isForPrint)
        {
            var viewData = new UserCalendarViewData { Service = this.Service, FromDate = from, ToDate = to };
            var scheduledEvents = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, false, string.Empty);
            SetUserVisitData(viewData, scheduledEvents, isForPrint);
            return viewData;
        }

        private void SetUserVisitData(UserCalendarViewData viewData, List<ScheduleEvent> scheduledEvents,bool isForPrint)
        {
            var userVisits = new List<UserVisit>();
            if (scheduledEvents.IsNotNullOrEmpty())
            {
                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);
                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var permission = Current.Permissions;
                var schedulePermission = permission.GetCategoryPermission(ParentPermission.Schedule);

                viewData.IsStickyNote = schedulePermission.IsInPermission(this.Service, PermissionActions.ViewStickyNotes);
                viewData.IsUserCanPrint = schedulePermission.IsInPermission(this.Service, PermissionActions.Print);
                viewData.IsUserCanExport = schedulePermission.IsInPermission(this.Service, PermissionActions.Export);
                viewData.IsUserCanEdit = schedulePermission.IsInPermission(this.Service, PermissionActions.Edit);

                var users = new List<User>();
                var returnComments = new List<ReturnComment>();
                if (viewData.IsStickyNote)
                {
                    var episodeIds = scheduledEvents.Where(r => !r.EpisodeId.IsEmpty()).Select(r => r.EpisodeId).Distinct().ToList();
                    if (episodeIds.IsNotNullOrEmpty())
                    {
                        returnComments = scheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();

                        if (returnComments.IsNotNullOrEmpty())
                        {
                            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                            if (returnUserIds.IsNotNullOrEmpty())
                            {
                                users = UserEngine.GetUsers(Current.AgencyId, returnUserIds) ?? new List<User>();
                            }
                        }
                    }
                }

                scheduledEvents.ForEach(s =>
                {
                    var task = new UserVisit
                    {
                        Status = s.Status,
                        PatientName = s.PatientName,
                        DisciplineTask = s.DisciplineTask,
                        VisitDate = s.VisitDate,
                        ScheduleDate = s.EventDate,
                        IsMissedVisit = s.IsMissedVisit
                    };

                    if (!isForPrint)
                    {
                        task.Id = s.Id;
                        task.EpisodeId = s.EpisodeId;
                        task.PatientId = s.PatientId;
                        if (viewData.IsUserCanEdit)
                        {
                            //UrlFactory<ScheduleEvent>.SetEditUrl(scheduledEvent);
                            var group = DocumentFactory.GetType(s.DisciplineTask,s.IsMissedVisit);
                            if (group != DocumentType.None)
                            {
                                task.Group = group.ToString().ToLowerCase();
                            }
                            if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                            {
                                task.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                            }

                            var isComplete = false;
                            if (s.IsMissedVisit)
                            {
                                isComplete = missedVisitCompletedStatus.Contains(s.Status);
                            }
                            else
                            {
                                isComplete = completedStatus.Contains(s.Status);
                            }

                            task.IsEditable = !isComplete && (int)DisciplineTasks.FaceToFaceEncounter != s.DisciplineTask;
                        }

                        if (viewData.IsStickyNote)
                        {
                            var visitNote = string.Empty;
                            var episodeDetailComment = string.Empty;
                            var statusComment = string.Empty;
                            if (s.EpisodeNotes.IsNotNullOrEmpty())
                            {
                                episodeDetailComment = s.EpisodeNotes.Clean();
                            }
                            if (s.Comments.IsNotNullOrEmpty())
                            {
                                visitNote = s.Comments.Clean();
                            }
                            var eventReturnReasons = returnComments.Where(r => r.EpisodeId == s.EpisodeId && r.EventId == s.Id).ToList();
                            if (eventReturnReasons.IsNotNullOrEmpty())
                            {
                                statusComment = TaskHelperFactory<ScheduleEvent>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                            }

                            task.EpisodeNotes = episodeDetailComment;
                            task.StatusComment = statusComment;
                            task.VisitNotes = visitNote;
                        }
                    }
                    userVisits.Add(task);
                });
            }
            viewData.UserEvents = userVisits;
        }

        #endregion

        public GridViewData GetPastDueAndUpcomingRecertViewData(ParentPermission category, PermissionActions availablePermissionAction, List<PermissionActions> moreActions, bool IsLocationNeeded)
        {
            var viewData = new GridViewData { Service = this.Service };
            if (IsLocationNeeded)
            {
                var branch = agencyRepository.GetBranchForSelection(Current.AgencyId, (int)this.Service);
                if (branch != null)
                {
                    viewData.Id = branch.Id;
                    var insuranceId = 0;
                    if (int.TryParse(branch.IsLocationStandAlone ? branch.Payor : Current.Payor,out insuranceId))
                    {
                        viewData.SubId = insuranceId;
                    }
                }
            }
            moreActions.Add(availablePermissionAction);
            var allPermission = Current.CategoryService(Current.AcessibleServices, category, moreActions.Select(a => (int)a).ToArray());
            if (allPermission.IsNotNullOrEmpty())
            {
                moreActions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == availablePermissionAction)
                    {
                        viewData.AvailableService = permission;
                    }
                    if (a == PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }
                    
                });
            }
            return viewData;
        }


        #region  Past Due Recerts

        public List<RecertEvent> GetRecertsPastDueWidget()
        {
            return GetRecertsPastDue(Guid.Empty, 0, DateTime.Now.AddMonths(-4), DateTime.Now, true, 5);
        }

        public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        {
            var pastDueRecerts = new List<RecertEvent>();
            var payor = 0;
            if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    payor = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
                }
            }
            var scheduleEvents = scheduleRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate, payor);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                if (episodeIds.IsNotNullOrEmpty())
                {
                    var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
                    var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };
                    var recertAndROCStatus = dischargeStatus;
                    var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();
                    var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
                    var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);

                    foreach (var e in episodeIds)
                    {
                        var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
                        if (schedules.IsNotNullOrEmpty())
                        {
                            if (schedules.Count == 1)
                            {
                                var recert = new RecertEvent();
                                var evnt = schedules[0];
                                if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)) || (transferDisciplineTasks.Contains(evnt.DisciplineTask) && transferStatus.Contains(evnt.Status) && evnt.EventDate.IsBetween(evnt.EndDate.AddDays(-5), evnt.EndDate)))
                                {
                                    break;
                                }
                                if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
                                {
                                    if (!evnt.UserId.IsEmpty())
                                    {
                                        var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
                                        recert.AssignedTo = user != null ? user.DisplayName : "Unassigned";
                                    }
                                    else
                                    {
                                        recert.AssignedTo = "Unassigned";
                                    }
                                    if (evnt.EventDate.Date <= DateTime.MinValue.Date)
                                    {
                                        recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
                                        recert.TargetDate = evnt.EndDate;
                                    }
                                    else
                                    {
                                        recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
                                        recert.TargetDate = evnt.EventDate;
                                    }
                                    recert.Status = evnt.Status;
                                    recert.PatientName = evnt.PatientName;
                                    recert.PatientIdNumber = evnt.PatientIdNumber;
                                    recert.EventDate = evnt.EventDate;
                                    pastDueRecerts.Add(recert);
                                }
                            }
                            else if (schedules.Count > 1)
                            {
                                if (!schedules.Exists(s => (dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)) || (transferDisciplineTasks.Contains(s.DisciplineTask) && transferStatus.Contains(s.Status) && s.EventDate.IsBetween(s.EndDate.AddDays(-5), s.EndDate))))
                                {
                                    var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (oasisROCOrRecert != null)
                                    {
                                        if (!recertAndROCStatus.Contains(oasisROCOrRecert.Status))
                                        {
                                            var recert = new RecertEvent();
                                            if (!oasisROCOrRecert.UserId.IsEmpty())
                                            {
                                                var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
                                                recert.AssignedTo = user != null ? user.DisplayName : "Unassigned";
                                            }
                                            else
                                            {
                                                recert.AssignedTo = "Unassigned";
                                            }
                                            if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
                                            {
                                                recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
                                                recert.TargetDate = oasisROCOrRecert.EndDate;
                                            }
                                            else
                                            {
                                                recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
                                                recert.TargetDate = oasisROCOrRecert.EventDate;
                                            }
                                            recert.Status = oasisROCOrRecert.Status;
                                            recert.PatientName = oasisROCOrRecert.PatientName;
                                            recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
                                            recert.EventDate = oasisROCOrRecert.EventDate;
                                            pastDueRecerts.Add(recert);
                                        }
                                    }

                                }
                            }
                        }
                        if (isLimitForWidget && pastDueRecerts.Count >= limit)
                        {
                            break;
                        }
                    }
                }
            }
            return pastDueRecerts;
        }

        #endregion

        #region  Up Coming Recerts

        public List<RecertEvent> GetRecertsUpcomingWidget()
        {
            return GetRecertsUpcoming(Guid.Empty, 0, DateTime.Now, DateTime.Now.AddDays(24), true, 5);

        }

        public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        {
            var upcomingRecets = new List<RecertEvent>();
            var payor = 0;
            if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    payor = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
                }
            }
            var scheduleEvents = scheduleRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate,payor);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                if (episodeIds.IsNotNullOrEmpty())
                {
                    var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
                    var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
                    var recertAndROCStatus = dischargeStatus; //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
                    var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();//new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
                    var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
                    var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    foreach (var e in episodeIds)
                    {
                        var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
                        if (schedules.IsNotNullOrEmpty())
                        {
                            if (schedules.Count == 1)
                            {
                                var recert = new RecertEvent();
                                var evnt = schedules[0];
                                if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)) || (transferDisciplineTasks.Contains(evnt.DisciplineTask) && transferStatus.Contains(evnt.Status) && evnt.EventDate.IsBetween(evnt.EndDate.AddDays(-5), evnt.EndDate)))
                                {
                                }
                                else if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
                                {
                                    if (!evnt.UserId.IsEmpty())
                                    {
                                        var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
                                        recert.AssignedTo = user != null ? user.DisplayName : "Unassigned";
                                    }
                                    else
                                    {
                                        recert.AssignedTo = "Unassigned";
                                    }
                                    if (evnt.EventDate.Date <= DateTime.MinValue.Date)
                                    {
                                        recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
                                        recert.TargetDate = evnt.EndDate;
                                    }
                                    else
                                    {
                                        recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
                                        recert.TargetDate = evnt.EventDate;
                                    }
                                    recert.Status = evnt.Status;
                                    recert.PatientName = evnt.PatientName;
                                    recert.PatientIdNumber = evnt.PatientIdNumber;
                                    recert.EventDate = evnt.EventDate;
                                    upcomingRecets.Add(recert);
                                }
                            }
                            else if (schedules.Count > 1)
                            {
                                if (schedules.Exists(s => (dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)) || (transferDisciplineTasks.Contains(s.DisciplineTask) && transferStatus.Contains(s.Status) && s.EventDate.IsBetween(s.EndDate.AddDays(-5), s.EndDate))))
                                {
                                }
                                else
                                {
                                    var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (oasisROCOrRecert != null)
                                    {
                                        if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
                                        {
                                        }
                                        else
                                        {
                                            var recert = new RecertEvent();
                                            if (!oasisROCOrRecert.UserId.IsEmpty())
                                            {
                                                var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
                                                recert.AssignedTo = user != null ? user.DisplayName : "Unassigned";
                                            }
                                            else
                                            {
                                                recert.AssignedTo = "Unassigned";
                                            }
                                            if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
                                            {
                                                recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
                                                recert.TargetDate = oasisROCOrRecert.EndDate;
                                            }
                                            else
                                            {
                                                recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
                                                recert.TargetDate = oasisROCOrRecert.EventDate;
                                            }
                                            recert.Status = oasisROCOrRecert.Status;
                                            recert.PatientName = oasisROCOrRecert.PatientName;
                                            recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
                                            recert.EventDate = oasisROCOrRecert.EventDate;
                                            upcomingRecets.Add(recert);
                                        }
                                    }
                                }
                            }
                        }
                        if (isLimitForWidget && upcomingRecets.Count >= limit)
                        {
                            break;
                        }
                    }
                }
            }
            return upcomingRecets;
        }

        #endregion

        #region Schedule Reports

        public ServiceTaskViewData GetScheduleDeviationViewData(Guid locationId, bool isLocationNeededIdEmpty, DateTime startDate, DateTime endDate)
        {
            var viewData = new ServiceTaskViewData { Service = this.Service };
            if (isLocationNeededIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            viewData.LocationId = locationId;
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Schedule, new int[] { (int)PermissionActions.ViewDetail });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = this.Service;
                viewData.EditDetailPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewDetail, AgencyServices.None);
            }
            viewData.Tasks = GetScheduleDeviations(locationId, startDate, endDate);
            return viewData;
        }

        public List<TaskLean> GetScheduleDeviations(Guid locationId, DateTime startDate, DateTime endDate)
        {
            var tasks = new List<TaskLean>();
            var scheduleEvents = scheduleRepository.GetScheduleDeviations(Current.AgencyId, locationId, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { }, true); // new List<ScheduleEvent>();;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var task = new TaskLean
                    {
                        PatientIdNumber = s.PatientIdNumber,
                        PatientName = s.PatientName,
                        DisciplineTaskName = s.DisciplineTaskName,
                        StatusName = s.StatusName,
                        EventDate = s.EventDate,
                        VisitDate = s.VisitDate,
                        Id = s.Id
                    };
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        task.UserName = user.DisplayName;
                    }
                    tasks.Add(task);
                });
            }
            return tasks;
        }

        #endregion

        #region Private Methods

        //private void ProcessScheduleFactory(ScheduleEvent scheduleEvent)
        //{
        //    switch (((DisciplineTasks)scheduleEvent.DisciplineTask))
        //    {
        //        case DisciplineTasks.OASISCDeath:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCDeathOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCDeathPT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCDischarge:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCDischargeOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCDischargePT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.NonOASISDischarge:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCFollowUp:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCFollowupPT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCFollowupOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCRecertification:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCRecertificationPT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCRecertificationOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.SNAssessmentRecert:
        //        case DisciplineTasks.NonOASISRecertification:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCResumptionOfCare:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCResumptionOfCarePT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCResumptionOfCareOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCStartOfCare:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCStartOfCarePT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCStartOfCareOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.SNAssessment:
        //        case DisciplineTasks.NonOASISStartOfCare:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.OASISCTransferDischarge:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCTransfer:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCTransferPT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.OASISCTransferOT:
        //            scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.SkilledNurseVisit:
        //        case DisciplineTasks.SNInsulinAM:
        //        case DisciplineTasks.SNInsulinPM:
        //        case DisciplineTasks.FoleyCathChange:
        //        case DisciplineTasks.SNB12INJ:
        //        case DisciplineTasks.SNBMP:
        //        case DisciplineTasks.SNCBC:
        //        case DisciplineTasks.SNHaldolInj:
        //        case DisciplineTasks.PICCMidlinePlacement:
        //        case DisciplineTasks.PRNFoleyChange:
        //        case DisciplineTasks.PRNSNV:
        //        case DisciplineTasks.PRNVPforCMP:
        //        case DisciplineTasks.PTWithINR:
        //        case DisciplineTasks.PTWithINRPRNSNV:
        //        case DisciplineTasks.SkilledNurseHomeInfusionSD:
        //        case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
        //        case DisciplineTasks.SNDC:
        //        case DisciplineTasks.SNEvaluation:
        //        case DisciplineTasks.SNFoleyLabs:
        //        case DisciplineTasks.SNFoleyChange:
        //        case DisciplineTasks.SNInjection:
        //        case DisciplineTasks.SNInjectionLabs:
        //        case DisciplineTasks.SNLabsSN:
        //        case DisciplineTasks.SNVPsychNurse:
        //        case DisciplineTasks.SNVwithAideSupervision:
        //        case DisciplineTasks.SNVDCPlanning:
        //        case DisciplineTasks.SNVTeachingTraining:
        //        case DisciplineTasks.SNVManagementAndEvaluation:
        //        case DisciplineTasks.SNVObservationAndAssessment:
        //        case DisciplineTasks.SNDiabeticDailyVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.LVNSupervisoryVisit:
        //        case DisciplineTasks.HHAideSupervisoryVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.PTEvaluation:
        //        case DisciplineTasks.PTReEvaluation:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        //case DisciplineTasks.PTSupervisoryVisit:
        //        //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //        //    scheduleEvent.Discipline = Disciplines.PT.ToString();
        //        //    scheduleEvent.IsBillable = true;
        //        //    scheduleEvent.Version = 1;
        //        //    break;
        //        case DisciplineTasks.PTVisit:
        //        case DisciplineTasks.PTAVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.Version = 2;
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.PTDischarge:
        //        case DisciplineTasks.PTMaintenance:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.PT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        //case DisciplineTasks.OTSupervisoryVisit:
        //        //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //        //    scheduleEvent.Discipline = Disciplines.OT.ToString();
        //        //    scheduleEvent.IsBillable = true;
        //        //    scheduleEvent.Version = 1;
        //        //    break;
        //        case DisciplineTasks.OTEvaluation:
        //        case DisciplineTasks.OTReEvaluation:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            scheduleEvent.Version = 2;
        //            break;
        //        //case DisciplineTasks.OTReassessment:
        //        //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //        //    scheduleEvent.Discipline = Disciplines.OT.ToString();
        //        //    scheduleEvent.IsBillable = true;
        //        //    break;
        //        case DisciplineTasks.OTVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            scheduleEvent.Version = 2;
        //            break;
        //        case DisciplineTasks.OTDischarge:
        //        case DisciplineTasks.COTAVisit:
        //        case DisciplineTasks.OTMaintenance:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.OT.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.STReEvaluation:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.ST.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.STEvaluation:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.ST.ToString();
        //            scheduleEvent.IsBillable = true;
        //            scheduleEvent.Version = 2;
        //            break;
        //        case DisciplineTasks.STVisit:
        //        case DisciplineTasks.STDischarge:
        //        case DisciplineTasks.STMaintenance:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.ST.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.MSWEvaluationAssessment:
        //        case DisciplineTasks.MSWVisit:
        //        case DisciplineTasks.MSWDischarge:
        //        case DisciplineTasks.MSWAssessment:
        //        case DisciplineTasks.MSWProgressNote:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.MSW.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.DriverOrTransportationNote:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.MSW.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.DieticianVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Dietician.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.HHAideVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.HHA.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.HHAideCarePlan:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.PASVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.HHA.ToString();
        //            scheduleEvent.IsBillable = true;
        //            break;
        //        case DisciplineTasks.PASCarePlan:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.HHA.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.DischargeSummary:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.PhysicianOrder:
        //            scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Orders.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.HCFA485StandAlone:
        //            scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
        //            scheduleEvent.Discipline = Disciplines.Orders.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.NonOasisHCFA485:
        //            scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
        //            scheduleEvent.Discipline = Disciplines.Orders.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.HCFA485:
        //            scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
        //            scheduleEvent.Discipline = Disciplines.Orders.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.HCFA486:
        //        case DisciplineTasks.PostHospitalizationOrder:
        //        case DisciplineTasks.MedicaidPOC:
        //            scheduleEvent.IsBillable = false;
        //            scheduleEvent.Discipline = Disciplines.Orders.ToString();
        //            scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
        //            break;
        //        case DisciplineTasks.IncidentAccidentReport:
        //            scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
        //            scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.InfectionReport:
        //            scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
        //            scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.SixtyDaySummary:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.TransferSummary:
        //        case DisciplineTasks.CoordinationOfCare:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Nursing.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.CommunicationNote:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.FaceToFaceEncounter:
        //            scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.Orders.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //        case DisciplineTasks.UAPWoundCareVisit:
        //        case DisciplineTasks.UAPInsulinPrepAdminVisit:
        //            scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
        //            scheduleEvent.Discipline = Disciplines.HHA.ToString();
        //            scheduleEvent.IsBillable = false;
        //            break;
        //    }
        //}

        #endregion
       
        protected override string TableName(int task)
        {
            var scheduleTask = new ScheduleEvent { DisciplineTask = task };
            var result = string.Empty;
            if (scheduleTask.IsNote())
            {
                result = "homehealth.patientvisitnotes";
            }
            else if (DisciplineTaskFactory.AllAssessments(true).Contains(task))
            {
                result = "homehealth.assessments";
            }
            else
            {
                switch (task)
                {
                    case (int)DisciplineTasks.PhysicianOrder:
                        result = "homehealth.physicianorders";
                        break;
                    case (int)DisciplineTasks.HCFA485:
                    case (int)DisciplineTasks.NonOasisHCFA485:
                        result = "homehealth.planofcares";
                        break;
                    case (int)DisciplineTasks.HCFA485StandAlone:
                        result = "homehealth.planofcares";
                        break;
                    case (int)DisciplineTasks.IncidentAccidentReport:
                        result = "homehealth.incidents";
                        break;
                    case (int)DisciplineTasks.InfectionReport:
                        result = "homehealth.infections";
                        break;
                    case (int)DisciplineTasks.CommunicationNote:
                        result = "homehealth.communicationnotes";
                        break;
                    case (int)DisciplineTasks.FaceToFaceEncounter:
                        result = "homehealth.facetofaceencounters";
                        break;
                }
            }
            return result;
        }

        //protected override bool IsUserAvailable(Guid userId, DateTime from, DateTime to, out string message)
        //{
        //    message = "";
        //    var scheduleTasks = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, true, string.Empty);
        //    if (scheduleTasks.IsNotNullOrEmpty())
        //    {
        //        var task = scheduleTasks.FirstOrDefault() ?? new ScheduleEvent();
        //        message = GenerateUserAvailablilityMessage(task.PatientId);
        //        return false;
        //    }
        //    return true;
        //}
    }
}
