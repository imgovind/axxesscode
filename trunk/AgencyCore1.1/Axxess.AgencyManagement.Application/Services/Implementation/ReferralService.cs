﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Extensions;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Helpers;

    public class ReferralService : IReferralService
    {
        private readonly IReferralRepository referralRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;

        public ReferralService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        public Referral Get(Guid id)
        {
            return referralRepository.Get(Current.AgencyId, id);
        }

        public Referral GetForNonAdmit(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if (referral != null)
            {
                var count = 0;
                if (referral.Services.Has(AgencyServices.HomeHealth))
                {
                    if (ReferralStatusFactory.PossibleToNonAdmit().Contains(referral.HomeHealthStatus) && !referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
                    {
                        count++;
                        referral.CurrentServices = AgencyServices.HomeHealth;
                    }
                }

                if (referral.Services.Has(AgencyServices.PrivateDuty))
                {
                    if (ReferralStatusFactory.PossibleToNonAdmit().Contains(referral.PrivateDutyStatus) && !referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
                    {
                        count++;
                        referral.CurrentServices |= AgencyServices.PrivateDuty;
                    }
                }
                //referral.IsOnlyOneService = count <= 1;
                referral.TotalServiceToProcess = count;
            }
            return referral;
        }

        public Referral GetForDelete(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if (referral != null)
            {
                var count = 0;
                if (referral.Services.IsAlone())
                {
                    if (referral.Services.Has(AgencyServices.HomeHealth))
                    {
                        if (!referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
                        {
                            referral.CurrentServices = AgencyServices.HomeHealth;
                            count++;
                        }
                    }
                    else if (referral.Services.Has(AgencyServices.PrivateDuty))
                    {
                        if (!referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
                        {
                            referral.CurrentServices = AgencyServices.PrivateDuty;
                            count++;
                        }
                    }
                }
                else
                {
                    if (referral.Services.Has(AgencyServices.HomeHealth))
                    {
                        if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.HomeHealthStatus) && !referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
                        {
                            count++;
                            referral.CurrentServices = AgencyServices.HomeHealth;
                        }
                    }

                    if (referral.Services.Has(AgencyServices.PrivateDuty))
                    {

                        if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.PrivateDutyStatus) && !referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
                        {
                            count++;
                            referral.CurrentServices |= AgencyServices.PrivateDuty;
                        }
                    }
                }
                referral.TotalServiceToProcess = count;
                //if (count == 0)
                //{
                //    referral.CurrentServices = Current.PreferredService;
                //}
                //referral.IsOnlyOneService = count <= 1;
            }
            return referral;
        }

        public List<ReferralData> GetPending(Guid agencyId, AgencyServices service)
        {
            var list = new List<ReferralData>();
            if (Current.IsCommunityLiason || Current.IsInRole(AgencyRoles.ExternalReferralSource))
            {
                list = referralRepository.AllByUser(agencyId, Current.UserId, ReferralStatus.Pending, service).ToList();
            }
            else
            {
                list = referralRepository.All(agencyId, ReferralStatus.Pending, service);
            }
            if (list.IsNotNullOrEmpty())
            {
                var allPermission = Current.CategoryService(service, ParentPermission.Referral, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Admit, (int)PermissionActions.NonAdmit, (int)PermissionActions.Print });
                var isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                var isDeletePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                var isAdmitPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Admit, AgencyServices.None) != AgencyServices.None;
                var isNonAdmitPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.NonAdmit, AgencyServices.None) != AgencyServices.None;
                var isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                var users = UserEngine.GetUsers(Current.AgencyId, list.Select(u => u.CreatedById).Distinct().ToList());
                list.ForEach(r =>
                {
                    var user = users.FirstOrDefault(u => u.Id == r.CreatedById);
                    if (user != null)
                    {
                        r.CreatedBy = user.DisplayName;
                    }
                    r.AdmissionSource = r.AdmissionSourceId.ToSourceName();
                    r.IsUserCanEdit = isEditPermission;
                    r.IsUserCanDelete = isDeletePermission;
                    r.IsUserCanAdmit = isAdmitPermission;
                    r.IsUserCanNonAdmit = isNonAdmitPermission;
                    r.IsUserCanPrint = isPrintPermission;
                });
            }

            return list;
        }

        public List<Referral> GetReferralByStatus(Guid agencyId, AgencyServices service, ReferralStatus status)
        {
            var list = new List<Referral>();
            if (Current.IsCommunityLiason || Current.IsInRole(AgencyRoles.ExternalReferralSource))
            {
                list = referralRepository.GetAllByCreatedUser(agencyId, Current.UserId, status, service);
            }
            else
            {
                list = referralRepository.GetAll(agencyId, status, service);
            }
            return list;
        }

        public JsonViewData AddReferral(Referral referral)
        {
            var viewData = new JsonViewData();

            if (referral.IsValid() && (referral.EmergencyContact == null || (referral.EmergencyContact != null && referral.EmergencyContact.IsValid())))
            {
                referral.Id = Guid.NewGuid();
                referral.CreatedById = Current.UserId;
                referral.AgencyId = Current.AgencyId;
                if (referral.ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                {
                    referral.Services |= AgencyServices.HomeHealth;
                    referral.HomeHealthStatus = (int)ReferralStatus.Pending;
                }
                if (referral.ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                {
                    referral.Services |= AgencyServices.PrivateDuty;
                    referral.PrivateDutyStatus = (int)ReferralStatus.Pending;
                }
                
                if (referralRepository.Add(referral))
                {
                    if (referral.EmergencyContact != null)
                    {
                        referral.EmergencyContact.ReferralId = referral.Id;
                        referral.EmergencyContact.AgencyId = Current.AgencyId;
                        AddPrimaryEmergencyContact(referral);
                    }
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Referral was created successfully";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the referral.";
                   
                }
            }
            else
            {
                viewData.isSuccessful = false;
                if (!referral.IsValid())
                {
                    viewData.errorMessage = referral.ValidationMessage;
                }
                else
                {
                    viewData.errorMessage = referral.EmergencyContact.ValidationMessage;
                }
            }
            return viewData;
        }

        public bool AddPrimaryEmergencyContact(Referral referral)
        {
            var result = false;
            if (referralRepository.AddEmergencyContact(referral.EmergencyContact) && referralRepository.SetPrimaryEmergencyContact(Current.AgencyId, referral.Id, referral.EmergencyContact.Id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.EmergencyContactAdded, "The Contact is set as primary.");
                result = true;
            }
            return result;
        }

        public JsonViewData Update(Referral referral)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Referral could not be updated" };
            var rules = new List<Validation>(){
                           new Validation(() => string.IsNullOrEmpty(referral.FirstName), ". Patient first name is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.LastName), ". Patient last name is required. <br>"),
                           new Validation(() => string.IsNullOrEmpty(referral.DOB.ToString()), ". Patient date of birth is required.<br/>"),
                           new Validation(() => !referral.DOB.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.Gender), ". Patient gender has to be selected.<br/>"),
                           new Validation(() => (referral.EmailAddress == null ? !string.IsNullOrEmpty(referral.EmailAddress) : !referral.EmailAddress.IsEmail()), ". Patient e-mail is not in a valid  format.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressLine1), ". Patient address line is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressCity), ". Patient city is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressStateCode), ". Patient state is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressZipCode), ". Patient zip is required.<br/>"),
                           new Validation(() => referral.EmergencyContact != null && referral.EmergencyContact.HasValue && (referral.EmergencyContact.FirstName.IsNullOrEmpty() || referral.EmergencyContact.LastName.IsNullOrEmpty() || !referral.EmergencyContact.PrimaryPhoneHasValue),
                               "Patient emergency contact requires a first name, last name, and a primary phone number.<br/>")
            };
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                var referralToEdit = referralRepository.Get(Current.AgencyId, referral.Id);
                if (referralToEdit != null)
                {
                    referral.Encode();

                    if (ReferralStatusFactory.PossibleToAdmit().Contains(referralToEdit.HomeHealthStatus))
                    {
                        if (referral.ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                        {
                            if (!referralToEdit.Services.Has(AgencyServices.HomeHealth))
                            {
                                referralToEdit.Services |= AgencyServices.HomeHealth;
                            }
                        }
                        else
                        {
                            referralToEdit.Services ^= AgencyServices.HomeHealth;
                        }
                    }

                    if (ReferralStatusFactory.PossibleToAdmit().Contains(referralToEdit.PrivateDutyStatus))
                    {
                        if (referral.ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                        {
                            if (!referralToEdit.Services.Has(AgencyServices.PrivateDuty))
                            {
                                referralToEdit.Services |= AgencyServices.PrivateDuty;
                            }
                        }
                        else
                        {
                            referralToEdit.Services ^= AgencyServices.PrivateDuty;
                        }
                    }

                    referralToEdit.AdmissionSource = referral.AdmissionSource;
                    referralToEdit.ReferrerPhysician = referral.ReferrerPhysician;
                    referralToEdit.ReferralDate = referral.ReferralDate;
                    referralToEdit.OtherReferralSource = referral.OtherReferralSource;
                    referralToEdit.InternalReferral = referral.InternalReferral;
                    referralToEdit.FirstName = referral.FirstName;
                    referralToEdit.MiddleInitial = referral.MiddleInitial;
                    referralToEdit.LastName = referral.LastName;
                    referralToEdit.MedicaidNumber = referral.MedicaidNumber;
                    referralToEdit.MedicareNumber = referral.MedicareNumber;
                    referralToEdit.SSN = referral.SSN;
                    referralToEdit.DOB = referral.DOB;
                    referralToEdit.Gender = referral.Gender;
                    referralToEdit.Height = referral.Height;
                    referralToEdit.HeightMetric = referral.HeightMetric;
                    referralToEdit.Weight = referral.Weight;
                    referralToEdit.WeightMetric = referral.WeightMetric;
                    referralToEdit.MaritalStatus = referral.MaritalStatus;
                    referralToEdit.PhoneHome = referral.PhoneHome;
                    referralToEdit.EmailAddress = referral.EmailAddress;
                    referralToEdit.AddressLine1 = referral.AddressLine1;
                    referralToEdit.AddressLine2 = referral.AddressLine2;
                    referralToEdit.AddressCity = referral.AddressCity;
                    referralToEdit.AddressStateCode = referral.AddressStateCode;
                    referralToEdit.AddressZipCode = referral.AddressZipCode;
                    referralToEdit.ServicesRequired = referral.ServicesRequired;
                    referralToEdit.DME = referral.DME;
                    referralToEdit.OtherDME = referral.OtherDME;
                    referralToEdit.UserId = referral.UserId;
                    referralToEdit.Comments = referral.Comments;
                    referralToEdit.IsDNR = referral.IsDNR;
                    referralToEdit.Modified = DateTime.Now;

                    AddOrEditEmergencyContact(referral);

                    if (referralRepository.UpdateModal(referralToEdit))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your referral has been updated.";
                        viewData.IsReferralListRefresh = true;
                        viewData.IsNonAdmitPatientListRefresh = true;
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Could not update the referral. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Could not found the referral. Please try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return viewData;
        }

        private void AddOrEditEmergencyContact(Referral referral)
        {
            var emergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, referral.Id);
            if (emergencyContact != null)
            {
                if (referral.EmergencyContact != null)
                {
                    referral.EmergencyContact.AgencyId = Current.AgencyId;
                    referral.EmergencyContact.ReferralId = referral.Id;
                    if (referral.EmergencyContact.FirstName.IsNullOrEmpty() && referral.EmergencyContact.LastName.IsNullOrEmpty() &&
                        referral.EmergencyContact.Relationship.IsNullOrEmpty() && referral.EmergencyContact.PrimaryPhone.IsNullOrEmpty() &&
                        referral.EmergencyContact.AlternatePhone.IsNullOrEmpty() && referral.EmergencyContact.EmailAddress.IsNullOrEmpty())
                    {
                        referralRepository.DeleteEmergencyContact(Current.AgencyId, emergencyContact.Id, referral.Id);
                    }
                    else
                    {
                        referralRepository.EditEmergencyContact(Current.AgencyId, referral.EmergencyContact);
                    }
                }
            }
            else if (referral.EmergencyContact != null)
            {
                referral.EmergencyContact.AgencyId = Current.AgencyId;
                referral.EmergencyContact.ReferralId = referral.Id;
                referral.EmergencyContact.IsPrimary = true;
                referralRepository.AddEmergencyContact(referral.EmergencyContact);
            }
        }

        public JsonViewData NonAdmitReferral(Guid Id, List<string> ServiceProvided, List<NonAdmit> profile)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Non-Admission of referral could not be saved." };
            if (ServiceProvided.IsNotNullOrEmpty())
            {
                var serviceCount = ServiceProvided.Count;
                var referral = referralRepository.Get(Current.AgencyId, Id);
                if (referral != null)
                {
                    var serviceChanged = 0;
                    if (ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                    {
                        var homeHealthProfile = profile.Where(p => p.ServiceType == (int)AgencyServices.HomeHealth).FirstOrDefault();
                        if (homeHealthProfile != null)
                        {
                            referral.NonAdmissionDate = homeHealthProfile.NonAdmitDate;
                            referral.NonAdmissionReason = homeHealthProfile.Reason;
                            referral.Comments = homeHealthProfile.Comments;
                            referral.HomeHealthStatus = (int)ReferralStatus.NonAdmission;
                            serviceChanged++;
                        }
                    }
                    if (ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                    {
                        var privateDutyProfile = profile.Where(p => p.ServiceType == (int)AgencyServices.PrivateDuty).FirstOrDefault();
                        if (privateDutyProfile != null)
                        {
                            referral.NonAdmissionDate = privateDutyProfile.NonAdmitDate;
                            referral.NonAdmissionReason = privateDutyProfile.Reason;
                            referral.Comments = privateDutyProfile.Comments;
                            referral.PrivateDutyStatus = (int)ReferralStatus.NonAdmission;
                            serviceChanged++;
                        }
                    }
                    if (serviceChanged == serviceCount)
                    {
                        referral.AgencyId = Current.AgencyId;
                        if (referralRepository.UpdateModal(referral))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralNonAdmitted, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Referral non-admission successful.";
                            if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.HomeHealthStatus) || ReferralStatusFactory.PossibleToAdmit().Contains(referral.PrivateDutyStatus))
                            {
                                viewData.IsReferralListRefresh = true;
                                viewData.IsNonAdmitPatientListRefresh = true;
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public bool SetStatus(Guid id, Referral referralForStatus)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if (referral != null)
            {
                if (referralForStatus.Services.Has(AgencyServices.HomeHealth))
                {
                    referral.HomeHealthStatus = referralForStatus.HomeHealthStatus;
                }
                if (referralForStatus.Services.Has(AgencyServices.PrivateDuty))
                {
                    referral.PrivateDutyStatus = referralForStatus.PrivateDutyStatus;
                }
                if (referralRepository.UpdateModal(referral))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.Referral, LogAction.ReferralAdmitted, string.Empty);
                    return true;
                }
            }
            return false;
        }

        public JsonViewData Delete(Guid id, List<string> ServiceProvided)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this Referral. Please try again." };
            if (ServiceProvided.IsNotNullOrEmpty())
            {
                var serviceCount = ServiceProvided.Count;
                var referral = referralRepository.Get(Current.AgencyId, id);
                if (referral != null)
                {
                    var serviceChanged = 0;
                    if (ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                    {

                        if (referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
                        {
                           
                        }
                        else
                        {
                            referral.DeprecatedServices |= AgencyServices.HomeHealth;
                        }
                        //referral.Services ^= AgencyServices.HomeHealth;
                        serviceChanged++;
                    }
                    if (ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                    {
                        if (referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
                        {
                        }
                        else
                        {
                            referral.DeprecatedServices |= AgencyServices.PrivateDuty;
                        }
                        //referral.Services ^= AgencyServices.PrivateDuty;
                        serviceChanged++;
                    }
                    if (serviceChanged == serviceCount)
                    {
                        referral.Modified = DateTime.Now;
                        if (referralRepository.UpdateModal(referral))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.Referral, LogAction.ReferralDeleted, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Referral has been deleted.";
                            if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.HomeHealthStatus) || ReferralStatusFactory.PossibleToAdmit().Contains(referral.PrivateDutyStatus))
                            {
                                viewData.IsReferralListRefresh = true;
                                viewData.IsNonAdmitPatientListRefresh = true;
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        #region Referral

        public bool UpdateReferralModal(Referral referral)
        {
            return referralRepository.UpdateModal(referral);
        }

        public Referral GetReferral(Guid referralId)
        {
            return referralRepository.Get(Current.AgencyId, referralId);
        }

        public Referral GetReferralForNewOrExisitngPatient(Guid referralId)
        {
            var referral = referralRepository.Get(Current.AgencyId, referralId);
            if (referral != null)
            {
                var totalPossibleAdmit = 0;
                var referralServices = 0;
                var totalAdmittedOrDeprecated = 0;
                var patient = patientRepository.GetPatientOnly(referralId, Current.AgencyId);
                if (patient != null)
                {
                    referral.IsPatientExist = true;
                }
                referral.IsUserCanAddPhysicain = Current.HasRight(Current.AcessibleServices, ParentPermission.Physician, PermissionActions.Add);
                if (referral.IsPatientExist)
                {
                    EntityHelper.SetReferralDataFromPatient(referral, patient);
                    if (referral.Services.Has(AgencyServices.HomeHealth))
                    {
                        referralServices++;
                        if (patient != null && !patient.Services.Has(AgencyServices.HomeHealth))
                        {
                            if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.HomeHealthStatus) && !referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
                            {
                                totalPossibleAdmit++;
                                referral.CurrentServices = AgencyServices.HomeHealth;
                            }
                            else
                            {
                                totalAdmittedOrDeprecated++;
                            }
                        }
                        else
                        {
                            totalAdmittedOrDeprecated++;
                        }
                    }
                    if (referral.Services.Has(AgencyServices.PrivateDuty))
                    {
                        referralServices++;
                        if (patient != null && !patient.Services.Has(AgencyServices.PrivateDuty))
                        {
                            if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.PrivateDutyStatus) && !referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
                            {
                                totalPossibleAdmit++;
                                referral.CurrentServices = AgencyServices.PrivateDuty;
                            }
                            else
                            {
                                totalAdmittedOrDeprecated++;
                            }
                        }
                        else
                        {
                            totalAdmittedOrDeprecated++;
                        }
                    }
                }
                else
                {
                    if (referral.Services.Has(AgencyServices.HomeHealth))
                    {
                        referralServices++;
                        if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.HomeHealthStatus) && !referral.DeprecatedServices.Has(AgencyServices.HomeHealth))
                        {
                            totalPossibleAdmit++;
                            referral.CurrentServices = AgencyServices.HomeHealth;
                        }
                        else
                        {
                            totalAdmittedOrDeprecated++;
                        }
                    }
                    if (referral.Services.Has(AgencyServices.PrivateDuty))
                    {
                        referralServices++;
                        if (ReferralStatusFactory.PossibleToAdmit().Contains(referral.PrivateDutyStatus) && !referral.DeprecatedServices.Has(AgencyServices.PrivateDuty))
                        {
                            totalPossibleAdmit++;
                            referral.CurrentServices |= AgencyServices.PrivateDuty;
                        }
                        else
                        {
                            totalAdmittedOrDeprecated++;
                        }
                    }
                }

                if (referralServices > 0 && (referralServices == (totalAdmittedOrDeprecated + totalPossibleAdmit)))
                {
                    referral.TotalServiceToProcess = totalPossibleAdmit > 0 ? totalPossibleAdmit : 0;
                }
                else
                {
                    if (Current.AcessibleServices.IsAlone())
                    {
                        if (Current.PreferredService == AgencyServices.HomeHealth)
                        {
                            referral.TotalServiceToProcess = ReferralStatusFactory.PossibleToAdmit().Contains(referral.HomeHealthStatus) && !referral.DeprecatedServices.Has(AgencyServices.HomeHealth) ? 1 : 0;
                        }
                        else if (Current.PreferredService == AgencyServices.PrivateDuty)
                        {
                            referral.TotalServiceToProcess = ReferralStatusFactory.PossibleToAdmit().Contains(referral.PrivateDutyStatus) && !referral.DeprecatedServices.Has(AgencyServices.PrivateDuty) ? 1 : 0;
                        }
                        else
                        {
                            referral.TotalServiceToProcess = 0;
                        }
                    }
                    else
                    {
                        referral.TotalServiceToProcess = 0;
                    }
                }
                referral.IsOnlyOneService = referral.TotalServiceToProcess == 1;
                if (referral.TotalServiceToProcess > 0)
                {
                    referral.Profile = new Profile
                    {
                        Id = referral.Id,
                        StartofCareDate = DateTime.Now,
                        ReferralDate = !referral.ReferralDate.IsValid() ? DateTime.Now : referral.ReferralDate,
                        OtherReferralSource = referral.OtherReferralSource,
                        AdmissionSource = referral.AdmissionSource.ToString(),
                        InternalReferral = referral.InternalReferral,
                        UserId = referral.UserId
                    };

                    referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, referralId);
                    referral.LastUsedPatientId = patientRepository.LastPatientId(Current.AgencyId);
                }
            }

            return referral;
        }

        #endregion 



        public Referral GetReferralWithEmergencyContact(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if (referral != null)
            {
                var permission = Current.Permissions;
                referral.IsUserCanAddPhysicain = permission.IsInPermission(Current.AcessibleServices, ParentPermission.Physician, PermissionActions.Add);
                referral.IsUserCanViewLog = permission.IsInPermission(Current.AcessibleServices, ParentPermission.Referral, PermissionActions.ViewLog);
                referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, id);
                var count = 0;
                if (referral.Services.IsAlone())
                {
                    if (referral.Services.Has(AgencyServices.HomeHealth))
                    {
                        referral.CurrentServices = AgencyServices.HomeHealth;
                        count++;
                    }
                    else if (referral.Services.Has(AgencyServices.PrivateDuty))
                    {
                        referral.CurrentServices = AgencyServices.PrivateDuty;
                        count++;
                    }
                }
                else
                {
                    if (referral.Services.Has(AgencyServices.HomeHealth))
                    {
                        referral.CurrentServices = AgencyServices.HomeHealth;
                        if (!referral.DeprecatedServices.Has(AgencyServices.HomeHealth) && !ReferralStatusFactory.NonPending().Contains(referral.HomeHealthStatus))
                        {
                            count++;
                        }
                       
                    }
                    if (referral.Services.Has(AgencyServices.PrivateDuty))
                    {
                        referral.CurrentServices |= AgencyServices.PrivateDuty;
                        if (!referral.DeprecatedServices.Has(AgencyServices.PrivateDuty) && !ReferralStatusFactory.NonPending().Contains(referral.PrivateDutyStatus))
                        {
                            count++;
                        }
                       
                    }
                }
                if (count == 0)
                {
                    referral.CurrentServices = Current.PreferredService;
                }
                referral.IsOnlyOneService = count <= 1;

            }
            return referral;
        }

        public Referral GetReferralWithEmergencyContactAndProfile(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if (referral != null)
            {
                referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, id);
                referral.Profile = new Profile
                {
                    Id = referral.Id,
                    StartofCareDate = DateTime.Now,
                    ReferralDate = !referral.ReferralDate.IsValid() ? DateTime.Now : referral.ReferralDate,
                    OtherReferralSource = referral.OtherReferralSource,
                    AdmissionSource = referral.AdmissionSource.ToString(),
                    InternalReferral = referral.InternalReferral,
                    UserId=referral.UserId
                };

            }
            return referral;
        }

        public ReferralPdf GetReferralPdf(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if (referral != null)
            {
                if (referral.Physicians.IsNotNullOrEmpty())
                {
                    var physicians = referral.Physicians.ToObject<List<Physician>>();
                    if (physicians != null && physicians.Count > 0)
                    {
                        var primary = physicians.FirstOrDefault(p => p.IsPrimary);
                        if (primary != null)
                        {
                            referral.PrimaryPhysician = physicianRepository.Get(primary.Id, Current.AgencyId);
                        }
                    }
                }
                referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, id);
            }
            return new ReferralPdf(referral, agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId));
        }

        public List<ReferalPhysician> GetReferalPhysicians(Guid ReferralId)
        {
            var list = new List<ReferalPhysician>();
            var referral = referralRepository.Get(Current.AgencyId, ReferralId);
            if (referral != null && referral.Physicians.IsNotNullOrEmpty())
            {
                var physicians = referral.Physicians.ToObject<List<Physician>>();
                if (physicians.IsNotNullOrEmpty())
                {
                    var agencyPhysicians = physicianRepository.GetPhysiciansByIdsLean(Current.AgencyId, physicians.Select(p => p.Id).Distinct().ToList());
                    if (agencyPhysicians.IsNotNullOrEmpty())
                    {
                        foreach (var agencyPhysician in agencyPhysicians)
                        {
                            var referalPhysician = new ReferalPhysician
                                {
                                    Id = agencyPhysician.Id,
                                    FirstName = agencyPhysician.FirstName,
                                    LastName = agencyPhysician.LastName,
                                    WorkPhone = agencyPhysician.PhoneWorkFormatted,
                                    FaxNumber = agencyPhysician.FaxNumberFormatted,
                                    EmailAddress = agencyPhysician.EmailAddress,
                                    
                                };
                            var physician = physicians.FirstOrDefault(p => p.Id == agencyPhysician.Id);
                            if (physician != null)
                            {
                                referalPhysician.IsPrimary = physician.IsPrimary;
                            }
                            list.Add(referalPhysician);
                        }
                    }
                }
            }
            return list;
        }

        public JsonViewData AddReferralPhysician(Guid id, Guid referralId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician could not be added. Please try again." };
            var referral = referralRepository.Get(Current.AgencyId, referralId);
            if (referral != null)
            {
                var physicians = referral.Physicians.IsNotNullOrEmpty() ? (referral.Physicians.ToObject<List<Physician>>() ?? new List<Physician>()) : new List<Physician>();
                if (!physicians.Exists(p => p.Id == id))
                {
                    var isPrimary = physicians.Count == 0;
                    physicians.Add(new Physician { Id = id, IsPrimary = isPrimary });
                    referral.Physicians = physicians.ToXml();
                    if (referralRepository.UpdateModal(referral))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, "Physician was added");
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The physician has been added successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The physician already exists in the list.";
                }
            }
            return viewData;
        }


        public JsonViewData DeleteReferralPhysician(Guid id, Guid referralId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician could not be deleted. Please try again." };
            var referral = referralRepository.Get(Current.AgencyId, referralId);
            if (referral != null && referral.Physicians.IsNotNullOrEmpty())
            {
                var physicians = referral.Physicians.ToObject<List<Physician>>();
                for (int i = 0; i < physicians.Count; i++)
                {
                    if (physicians[i].Id.Equals(id))
                    {
                        if (physicians[i].IsPrimary && physicians.Count > 1)
                        {
                            physicians[i != 0 ? 0 : 1].IsPrimary = true;
                        }
                        physicians.RemoveAt(i--);
                    }
                }
                referral.Physicians = physicians.ToXml();
                if (referralRepository.UpdateModal(referral))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, "Physician was removed");
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician was removed successfully.";
                }
            }
            return viewData;
        }

        public JsonViewData SetPrimaryPhysician(Guid id, Guid referralId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician was not set as primary. Try Again." };
            var referral = referralRepository.Get(Current.AgencyId, referralId);
            if (referral != null)
            {
                var physicians = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() ?? new List<Physician>() : new List<Physician>();
                var isExist = false;
                foreach (var physician in physicians)
                {
                    physician.IsPrimary = physician.Id == id;
                    if (physician.Id == id)
                    {
                        isExist = true;
                    }
                }
                if (!isExist)
                {
                    physicians.Add(new Physician { Id = id, IsPrimary = true });
                }
                referral.Physicians = physicians.ToXml();
                if (referralRepository.UpdateModal(referral))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, "Physician was set as primary");
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician was successfully set as primary.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in setting physician as primary.";
                }
            }
            return viewData;
        }


    }
}
