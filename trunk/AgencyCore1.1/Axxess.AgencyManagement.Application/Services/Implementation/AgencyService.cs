﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Application.Extensions;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;
    using Axxess.Log.Enums;

    using Enums;
    using Domain;
    using Common;
    using ViewData;
    using System.Text;
    using Axxess.LookUp.Domain;
   

    public class AgencyService : IAgencyService
    {
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookUpRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookUpRepository = lookUpDataProvider.LookUpRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }
       
        #region Agency

        //public bool CreateAgency(Agency agency)
        //{
        //    try
        //    {
        //        agency.IsSuspended = false;
        //        if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
        //        {
        //            agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
        //        }

        //        if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
        //        {
        //            agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
        //        }

        //        if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
        //        {
        //            agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
        //        }

        //        if (agencyRepository.Add(agency))
        //        {
        //            var location = new AgencyLocation();
        //            location.Name = agency.LocationName;
        //            location.AddressLine1 = agency.AddressLine1;
        //            location.AddressLine2 = agency.AddressLine2;
        //            location.AddressCity = agency.AddressCity;
        //            location.AddressStateCode = agency.AddressStateCode;
        //            location.AddressZipCode = agency.AddressZipCode;
        //            location.AgencyId = agency.Id;
        //            location.IsMainOffice = true;
        //            location.IsDeprecated = false;

        //            if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
        //            {
        //                location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
        //            }
        //            if (agency.FaxArray != null && agency.FaxArray.Count > 0)
        //            {
        //                location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
        //            }

        //            var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
        //            if (zipCode != null)
        //            {
        //                location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
        //                location.MedicareProviderNumber = agency.MedicareProviderNumber;
        //            }
        //            var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
        //            if (defaultDisciplineTasks != null)
        //            {
        //                var costRates = new List<ChargeRate>();
        //                defaultDisciplineTasks.ForEach(r =>
        //                {
        //                    if (r.Discipline.IsEqual("Nursing") || r.Discipline.IsEqual("PT") || r.Discipline.IsEqual("OT") || r.Discipline.IsEqual("ST") || r.Discipline.IsEqual("HHA") || r.Discipline.IsEqual("MSW"))
        //                    {
        //                        costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(), Charge = r.Rate });
        //                    }
        //                });
        //                location.BillData = costRates.ToXml();
        //            }
        //            //location.Cost = "<ArrayOfCostRate><CostRate><RateDiscipline>SkilledNurse</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseTeaching</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseObservation</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseManagement</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>MedicareSocialWorker</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>HomeHealthAide</RateDiscipline><PerUnit>120</PerUnit></CostRate><CostRate><RateDiscipline>Attendant</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>CompanionCare</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>HomemakerServices</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>PrivateDutySitter</RateDiscipline><PerUnit>0</PerUnit></CostRate></ArrayOfCostRate>";
        //            location.Id = Guid.NewGuid();
        //            if (agencyRepository.AddLocation(location))
        //            {
        //                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
        //                var user = new User
        //                {
        //                    AgencyId = agency.Id,
        //                    AllowWeekendAccess = true,
        //                    AgencyName = agency.Name,
        //                    AgencyLocationId = location.Id,
        //                    Status = (int)UserStatus.Active,
        //                    LastName = agency.AgencyAdminLastName,
        //                    FirstName = agency.AgencyAdminFirstName,
        //                    PermissionsArray = GeneratePermissions(),
        //                    EmailAddress = agency.AgencyAdminUsername,
        //                    TitleType = TitleTypes.Administrator.GetDescription(),
        //                    Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
        //                    AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
        //                };

        //                IUserService userService = Container.Resolve<IUserService>();
        //                return userService.CreateUser(user);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Exception(ex);
        //    }

        //    return false;
        //}

        //public Agency GetAgency(Guid Id)
        //{
        //    Agency agencyViewData = agencyRepository.Get(Id);
        //    if (agencyViewData != null)
        //    {
        //        return agencyViewData;
        //    }
        //    return null;
        //}

        public int MaxAgencyUserCount()
        {
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                return agency.Package == 0 ? 100000000 : agency.Package;
            }
            return 0;
        }

        public JsonViewData UpdateAgencyWithLocation(Agency agency, AgencyLocation location)
        {
            var viewData = new JsonViewData();
            var existingAgency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (existingAgency != null)
            {
                var existingLocation = agencyRepository.FindLocation(Current.AgencyId, location.Id);
                if (existingLocation != null)
                {
                    existingAgency.Name = agency.Name;
                    existingAgency.TaxId = agency.TaxId;
                    existingAgency.TaxIdType = agency.TaxIdType;
                    existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                    if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count > 0)
                    {
                        existingAgency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                    }
                    existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                    existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                    existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                    existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                    existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                    existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                    existingAgency.CahpsVendor = agency.CahpsVendor;

                    if (existingLocation.IsLocationStandAlone)
                    {
                        existingLocation.Name = location.Name;
                        existingLocation.AddressLine1 = location.AddressLine1;
                        existingLocation.AddressLine2 = location.AddressLine2;
                        existingLocation.AddressCity = location.AddressCity;
                        existingLocation.AddressStateCode = location.AddressStateCode;
                        existingLocation.AddressZipCode = location.AddressZipCode;
                        if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                        {
                            existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                        }
                        if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                        {
                            existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                        }
                    }
                    else
                    {
                        existingLocation.Name = location.Name;
                        existingLocation.AddressLine1 = location.AddressLine1;
                        existingLocation.AddressLine2 = location.AddressLine2;
                        existingLocation.AddressCity = location.AddressCity;
                        existingLocation.AddressStateCode = location.AddressStateCode;
                        existingLocation.AddressZipCode = location.AddressZipCode;
                        if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                        {
                            existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                        }
                        if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                        {
                            existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                        }
                    }

                    if (agencyRepository.Update(existingAgency))
                    {
                        viewData.errorMessage += " Agency was updated successfully. ";
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                        if (agencyRepository.UpdateLocation(existingLocation))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, existingLocation.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage += "Agency Location was updated successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage += "Agency Location could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Agency could not be updated.";
                    }
                }
            }
            return viewData;
        }

        public JsonViewData UpdateAgency(Agency agency, AgencyLocation location)
        {
            var viewData = new JsonViewData();
            var existingAgency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (existingAgency != null)
            {
                var existingLocation = agencyRepository.FindLocation(Current.AgencyId, location.Id);
                if (existingLocation != null)
                {
                    existingAgency.Name = agency.Name;
                    existingAgency.TaxId = agency.TaxId;
                    existingAgency.TaxIdType = agency.TaxIdType;
                    existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                    if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count > 0)
                    {
                        existingAgency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                    }
                    existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                    existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                    existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                    existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                    existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                    existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                    existingAgency.CahpsVendor = agency.CahpsVendor;
                    if (existingLocation.IsLocationStandAlone)
                    {
                        existingLocation.Name = location.Name;
                        existingLocation.AddressLine1 = location.AddressLine1;
                        existingLocation.AddressLine2 = location.AddressLine2;
                        existingLocation.AddressCity = location.AddressCity;
                        existingLocation.AddressStateCode = location.AddressStateCode;
                        existingLocation.AddressZipCode = location.AddressZipCode;
                        if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                        {
                            existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                        }
                        if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                        {
                            existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                        }
                    }
                    else
                    {
                        existingLocation.Name = location.Name;
                        existingLocation.AddressLine1 = location.AddressLine1;
                        existingLocation.AddressLine2 = location.AddressLine2;
                        existingLocation.AddressCity = location.AddressCity;
                        existingLocation.AddressStateCode = location.AddressStateCode;
                        existingLocation.AddressZipCode = location.AddressZipCode;
                        if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                        {
                            existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                        }
                        if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                        {
                            existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                        }
                    }
                    if (!agencyRepository.Update(existingAgency))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Agency could not be updated.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Agency was updated successfully.";
                    }
                    if (!agencyRepository.UpdateLocation(existingLocation))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage += "Agency Location could not be updated.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.AgencyId.ToString(), LogType.AgencyInformation, LogAction.AgencyInformationUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage += "Agency Location was updated successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Location could not be found.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Agency could not be found.";
            }
            return viewData;
        }

        public Agency GetAgencyWithMainLocation()
        {
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                agency.MainLocation = agencyRepository.GetMainLocation(agency.Id);
            }
            return agency;
        }

        #endregion

        #region Location

        public IList<AgencyLocation> GetBranches()
        {
            return agencyRepository.GetBranches(Current.AgencyId);
        }

        public AgencyLocation FindLocation(Guid Id)
        {
            return agencyRepository.FindLocation(Current.AgencyId, Id);
        }

        public AgencyLocation GetMainLocation()
        {
            var viewdata = agencyRepository.GetMainLocation(Current.AgencyId);
            var permission = Current.Permissions;
            var insurancePermission = permission.GetCategoryPermission(ParentPermission.Insurance);
            viewdata.IsUserCanDelete = insurancePermission.IsInPermission(Current.Services, PermissionActions.Delete);
            viewdata.IsUserCanEdit = insurancePermission.IsInPermission(Current.Services, PermissionActions.Edit);
            viewdata.IsUserCanAdd = insurancePermission.IsInPermission(Current.Services, PermissionActions.Add);
            return viewdata;
        }

        public LocationPrintProfile AgencyNameWithLocationAddress(Guid locationId)
        {
            return agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, locationId);
        }

        public LocationPrintProfile AgencyNameWithMainLocationAddress()
        {
            return agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookUpRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;
            location.Id = Guid.NewGuid();
            if (agencyRepository.AddLocation(location))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                result = true;
            }

            return result;
        }

        public Guid GetMainLocationId()
        {
            return agencyRepository.GetMainLocationId(Current.AgencyId);
        }

        public Guid GetBranchForSelectionId(int service)
        {
            return agencyRepository.GetBranchForSelectionId(Current.AgencyId, service);
        }

        public AgencyLocation GetBranchForSelection(int service)
        {
            return agencyRepository.GetBranchForSelection(Current.AgencyId, service);
        }

        public int AgencyPayor()
        {
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            var payorId = 0;
            if (agency != null)
            {

                if (int.TryParse(agency.Payor, out payorId))
                {
                    return payorId;
                }
            }
            return payorId;
        }

       
        public AgencyLocation GetLocationOrDefault(Guid branchId)
        {
            return agencyRepository.FindLocation(Current.AgencyId, branchId) ?? new AgencyLocation();
        }

        public JsonViewData EditBranchLocatorAndMedicareInsuranceInfo(Guid locationId, string locatorXml, AgencyMedicareInsurance agencyLocationMedicare)
        {
            var viewData = new JsonViewData();
            var existingLocation = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (existingLocation != null)
            {
                //existingLocation.Cost = location.Cost;
                existingLocation.Ub04Locator81cca = locatorXml;
                if (agencyRepository.EditLocationModal(existingLocation))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Branch information is saved. ";
                    var medicareInfoViewData = new JsonViewData { isSuccessful = false };
                    var payorId = 0;
                    if (int.TryParse(existingLocation.Payor, out payorId))
                    {
                        agencyLocationMedicare.MedicareId = payorId;
                        medicareInfoViewData = AddOrUpdateMedicareInsuranceInfo(agencyLocationMedicare, payorId);
                    }
                    else
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            if (int.TryParse(agency.Payor, out payorId))
                            {
                                agencyLocationMedicare.MedicareId = payorId;
                                medicareInfoViewData = AddOrUpdateMedicareInsuranceInfo(agencyLocationMedicare, payorId);
                            }
                            else
                            {
                                medicareInfoViewData.errorMessage = " Agency payor infromation is not valid in order to save payor address.";
                            }
                        }
                        else
                        {
                            medicareInfoViewData.errorMessage = " Agency payor infromation is not correct in order to save payor address.";
                        }
                    }
                    if (!medicareInfoViewData.isSuccessful)
                    {
                        viewData.errorMessage += medicareInfoViewData.errorMessage;    
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Branch information could not be updated. try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected branch don't exist.";
            }
            return viewData;
        }

        private JsonViewData AddOrUpdateMedicareInsuranceInfo(AgencyMedicareInsurance agencyLocationMedicare, int payorId)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Branch payor address information could not be updated." };
            if (Enum.IsDefined(typeof(MedicareIntermediary), payorId))
            {
                var existingLocationLocationMedicare = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, payorId);
                if (existingLocationLocationMedicare != null)
                {
                    existingLocationLocationMedicare.AddressLine1 = agencyLocationMedicare.AddressLine1;
                    existingLocationLocationMedicare.AddressLine2 = agencyLocationMedicare.AddressLine2;
                    existingLocationLocationMedicare.AddressCity = agencyLocationMedicare.AddressCity;
                    existingLocationLocationMedicare.AddressStateCode = agencyLocationMedicare.AddressStateCode;
                    existingLocationLocationMedicare.AddressZipCode = agencyLocationMedicare.AddressZipCode;
                    existingLocationLocationMedicare.AgencyId = Current.AgencyId;
                    existingLocationLocationMedicare.MedicareId = agencyLocationMedicare.MedicareId;

                    if (agencyRepository.UpdateAgencyLocationMedicare(existingLocationLocationMedicare))
                    {
                        viewData.isSuccessful = true;
                    }
                }
                else
                {
                    if (agencyRepository.AddAgencyLocationMedicare(agencyLocationMedicare))
                    {
                        viewData.isSuccessful = true;
                    }
                }
            }
            return viewData;
        }

        public JsonViewData UpdateLocation(AgencyLocation location)
        {
            var viewData = new JsonViewData();

            if (location.IsValid())
            {
                if (agencyRepository.FindLocation(Current.AgencyId, location.Id) != null)
                {
                    location.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditLocation(location))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the location.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Location was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected location don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }
            return viewData;
        }

        #endregion

        #region Contact

        public IList<AgencyContact> GetContacts()
        {
            return agencyRepository.GetContacts(Current.AgencyId);
        }

        public AgencyContact FindContact( Guid Id)
        {
            var contact = agencyRepository.FindContact(Current.AgencyId, Id);
            if (contact != null)
            {
                contact.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.Contact, PermissionActions.ViewLog);
            }
            return contact;
        }

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            contact.Id = Guid.NewGuid();
            try
            {
                if (agencyRepository.AddContact(contact))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, contact.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public JsonViewData AddContact(AgencyContact contact)
        {
            var viewData = new JsonViewData();

            if (contact.IsValid())
            {
                if (!CreateContact(contact))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the contact.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Contact was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateContact(AgencyContact contact)
        {
            var viewData = new JsonViewData();

            if (contact.IsValid())
            {
                if (agencyRepository.FindContact(Current.AgencyId, contact.Id) != null)
                {
                    contact.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditContact(contact))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the contact.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Contact was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected contact don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }
            return viewData;
        }


        public JsonViewData DeleteContact(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Contact could not be deleted. Please try again." };
            if (agencyRepository.DeleteContact(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyContact, LogAction.AgencyContactDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Contact has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Contact could not be deleted.";
            }
            return viewData;
        }

        #endregion

        public AgencyLocation VisitRateContent(Guid branchId)
        {
            var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (location != null)
            {
                if (!location.IsLocationStandAlone)
                {
                    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                    if (agency != null)
                    {
                        int payor;
                        if (int.TryParse(agency.Payor, out payor))
                        {
                            var medicareInsurance = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, payor);
                            if (medicareInsurance != null)
                            {
                                location.MedicareInsurance = medicareInsurance;
                            }
                            if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
                            {
                                var name = ((MedicareIntermediary)payor).GetDescription();
                                if (name.IsNotNullOrEmpty())
                                {
                                    location.InsuranceName = name;
                                    location.SubmitterId = agency.SubmitterId;
                                    location.SubmitterName = agency.SubmitterName;
                                    location.SubmitterPhone = agency.SubmitterPhone;
                                    location.ContactPersonFirstName = agency.ContactPersonFirstName;
                                    location.ContactPersonLastName = agency.ContactPersonLastName;
                                    location.ContactPersonPhone = agency.ContactPersonPhone;
                                }
                            }

                        }
                    }
                }
                else
                {
                    int payor;
                    if (int.TryParse(location.Payor, out payor))
                    {
                        var medicareInsurance = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, payor);
                        if (medicareInsurance != null)
                        {
                            location.MedicareInsurance = medicareInsurance;
                        }
                        if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
                        {
                            var name = ((MedicareIntermediary)payor).GetDescription();
                            if (name.IsNotNullOrEmpty())
                            {
                                location.InsuranceName = name;
                                location.SubmitterId = location.SubmitterId;
                            }
                        }
                    }
                }
            }
            return location;
        }

        public ChargeRate GetLocationBillDataChargeRate(Guid locationId, int Id)
        {
            var chargeRate = new ChargeRate();
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (location != null && location.BillData.IsNotNullOrEmpty())
            {
                chargeRate = location.BillData.ToObject<List<ChargeRate>>().FirstOrDefault(r => r.Id == Id);
                if (chargeRate != null)
                {
                    chargeRate.LocationId = location.Id;
                }
            }
            return chargeRate;
        }

        public List<ChargeRate> GetLocationBillDataChargeRates(Guid locationId)
        {
            var billDatas = new List<ChargeRate>();
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (location != null && location.BillData.IsNotNullOrEmpty())
            {
                billDatas = location.BillData.ToObject<List<ChargeRate>>();
            }
            return billDatas;
        }

        public List<ChargeRate> GetInsuranceBillDatas(int InsuranceId)
        {
            var billDatas = new List<ChargeRate>();
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                billDatas = insurance.BillData.ToObject<List<ChargeRate>>();
            }
            return billDatas;
        }

        public JsonViewData SaveBillData(int InsuranceId, ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this insurance rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (chargeRate.IsValid())
            {
                if (insurance != null)
                {
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Insurance rate already exist.";
                            }
                            else
                            {
                                rates.Add(chargeRate);
                                insurance.BillData = rates.ToXml();
                                if (agencyRepository.EditInsuranceModal(insurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate saved successfully";
                                }
                            }
                        }
                        else
                        {
                            rates = new List<ChargeRate>();
                            rates.Add(chargeRate);
                            insurance.BillData = rates.ToXml();
                            if (agencyRepository.EditInsuranceModal(insurance))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance rate saved successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<ChargeRate>();
                        rates.Add(chargeRate);
                        insurance.BillData = rates.ToXml();
                        if (agencyRepository.EditInsuranceModal(insurance))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Insurance rate saved successfully";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = chargeRate.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateBillData(ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this insurance rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(chargeRate.InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                if (chargeRate.IsValid())
                {
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                rate.PreferredDescription = chargeRate.PreferredDescription;
                                rate.Code = chargeRate.Code;
                                rate.RevenueCode = chargeRate.RevenueCode;
                                rate.Charge = chargeRate.Charge;
                                rate.ExpectedRate = chargeRate.ExpectedRate;
                                rate.Modifier = chargeRate.Modifier;
                                rate.Modifier2 = chargeRate.Modifier2;
                                rate.Modifier3 = chargeRate.Modifier3;
                                rate.Modifier4 = chargeRate.Modifier4;
                                rate.ChargeType = chargeRate.ChargeType;
                                rate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                                if (rate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || rate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                                {
                                    if (chargeRate.IsTimeLimit)
                                    {

                                        rate.TimeLimitHour = chargeRate.TimeLimitHour;
                                        rate.TimeLimitMin = chargeRate.TimeLimitMin;
                                        rate.SecondDescription = chargeRate.SecondDescription;
                                        rate.SecondCode = chargeRate.SecondCode;
                                        rate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                        rate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                        rate.SecondCharge = chargeRate.IsSecondChargeDifferent ? chargeRate.SecondCharge : 0;
                                        rate.SecondModifier = chargeRate.SecondModifier;
                                        rate.SecondModifier2 = chargeRate.SecondModifier2;
                                        rate.SecondModifier3 = chargeRate.SecondModifier3;
                                        rate.SecondModifier4 = chargeRate.SecondModifier4;
                                        rate.SecondChargeType = chargeRate.SecondChargeType;
                                        rate.SecondUnit = rate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString() ? chargeRate.SecondUnit : 0;
                                        rate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                                    }
                                    rate.IsTimeLimit = chargeRate.IsTimeLimit;
                                }
                                else if (rate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                                {
                                    rate.Unit = chargeRate.Unit;
                                }
                                if (insurance.PayorType == (int)PayerTypes.MedicareHMO && (rate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || rate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
                                {
                                    rate.MedicareHMORate = chargeRate.MedicareHMORate;
                                }
                                insurance.BillData = rates.ToXml();
                                if (agencyRepository.EditInsuranceModal(insurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate updated successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }
            return viewData;
        }

        public JsonViewData UpdateLocationBillData(ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this medicare insurance rate. Please try again." };
            var location = agencyRepository.FindLocation(Current.AgencyId, chargeRate.LocationId);
            if (location != null && chargeRate != null)
            {
                chargeRate.ChargeType = ((int)BillUnitType.Per15Min).ToString();
                if (chargeRate.IsValid())
                {
                    if (location.BillData.IsNotNullOrEmpty())
                    {
                        var rates = location.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                rate.PreferredDescription = chargeRate.PreferredDescription;
                                rate.Code = chargeRate.Code;
                                rate.RevenueCode = chargeRate.RevenueCode;
                                rate.Charge = chargeRate.Charge;
                                rate.Modifier = chargeRate.Modifier;
                                rate.Modifier2 = chargeRate.Modifier2;
                                rate.Modifier3 = chargeRate.Modifier3;
                                rate.Modifier4 = chargeRate.Modifier4;
                                rate.ChargeType = chargeRate.ChargeType;
                                // rate.Unit = chargeRate.Unit;
                                location.BillData = rates.ToXml();
                                if (agencyRepository.UpdateLocation(location))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate updated successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }
            return viewData;
        }

        public JsonViewData SaveLocationBillData(Guid locationId, ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this medicare insurance rate. Please try again." };
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (chargeRate != null)
            {
                chargeRate.ChargeType = ((int)BillUnitType.Per15Min).ToString();
                if (chargeRate != null && chargeRate.IsValid())
                {
                    if (location != null)
                    {
                        if (location.BillData.IsNotNullOrEmpty())
                        {
                            var rates = location.BillData.ToObject<List<ChargeRate>>();
                            if (rates != null && rates.Count > 0)
                            {
                                var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                                if (rate != null)
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Insurance rate already exist.";
                                }
                                else
                                {
                                    rates.Add(chargeRate);
                                    location.BillData = rates.ToXml();
                                    if (agencyRepository.UpdateLocation(location))
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "Insurance rate saved successfully";
                                    }
                                }
                            }
                            else
                            {
                                rates = new List<ChargeRate> { chargeRate };
                                location.BillData = rates.ToXml();
                                if (agencyRepository.UpdateLocation(location))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate saved successfully";
                                }
                            }
                        }
                        else
                        {
                            var rates = new List<ChargeRate> { chargeRate };
                            location.BillData = rates.ToXml();
                            if (agencyRepository.UpdateLocation(location))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance rate saved successfully";
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }
            return viewData;
        }

        public JsonViewData DeleteLocationBillData(Guid locationId, int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            if (location != null)
            {
                if (location.BillData.IsNotNullOrEmpty())
                {
                    var rates = location.BillData.ToObject<List<ChargeRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == Id);
                        if (removed > 0)
                        {
                            location.BillData = rates.ToXml();
                            if (agencyRepository.UpdateLocation(location))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance visit rate deleted successfully";
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData DeleteBillData(int InsuranceId, int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                if (insurance.BillData.IsNotNullOrEmpty())
                {
                    var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == Id);
                        if (removed > 0)
                        {
                            insurance.BillData = rates.ToXml();
                            if (agencyRepository.EditInsuranceModal(insurance))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance visit rate deleted successfully";
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ReplaceInsuranceVisit(int Id, int replacedId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error in editing the insurance visit information." };
            var insurance = agencyRepository.FindInsurance(Current.AgencyId, Id);
            if (insurance != null)
            {
                var selectedInsurance = agencyRepository.FindInsurance(Current.AgencyId, replacedId);
                if (selectedInsurance != null)
                {
                    if (selectedInsurance.BillData.IsNotNullOrEmpty())
                    {
                        insurance.BillData = selectedInsurance.BillData;
                        if (!agencyRepository.EditInsuranceModal(insurance))
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in editing the insurance visit information.";
                        }
                        else
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceVisitInfoOverWrite, string.Empty);
                            //InsuranceEngine.Instance.Refresh(Current.AgencyId);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Insurance was edited successfully";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Selected insurance bill information is empty.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }

            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The insurance to be updated don't exist.";
            }
            return viewData;
        }

        public ChargeRate GetInsuranceChargeRateForEdit(int InsuranceId, int Id)
        {
            var chargeRate = new ChargeRate();
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                chargeRate = insurance.BillData.ToObject<List<ChargeRate>>().FirstOrDefault(r => r.Id == Id);
                if (chargeRate != null)
                {
                    chargeRate.InsuranceId = insurance.Id;
                    chargeRate.IsMedicareHMO = insurance.PayorType == (int)PayerTypes.MedicareHMO;
                    chargeRate.IsExpectedRateNeeded = insurance.HasContractWithAgency;
                }
            }
            return chargeRate;
        }

        public ChargeRate GetInsuranceChargeRateForNew(int InsuranceId)
        {
            var chargeRate = new ChargeRate();
            var insurance = agencyRepository.GetInsurance(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                    chargeRate.InsuranceId = insurance.Id;
                    chargeRate.IsMedicareHMO = insurance.PayorType == (int)PayerTypes.MedicareHMO;
                    chargeRate.IsExpectedRateNeeded = insurance.HasContractWithAgency;
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        chargeRate.ExistingNotes = insurance.BillData.ToObject<List<ChargeRate>>().Select(r => r.Id).ToList();
                    }
            }
            return chargeRate;
        }

        public AgencyInsurance FindInsurance(int Id)
        {
            var viewdata = agencyRepository.FindInsurance(Current.AgencyId, Id);
            var permission = Current.Permissions;
            var insurancePermission = permission.GetCategoryPermission(ParentPermission.Insurance);
            var acessibleServices = Current.AcessibleServices;
            var isFrozen = Current.IsAgencyFrozen;
            viewdata.IsUserCanDelete = !isFrozen && insurancePermission.IsInPermission(acessibleServices, PermissionActions.Delete);
            viewdata.IsUserCanEdit = !isFrozen && insurancePermission.IsInPermission(acessibleServices, PermissionActions.Edit);
            viewdata.IsUserCanAdd = !isFrozen && insurancePermission.IsInPermission(acessibleServices, PermissionActions.Add);
            viewdata.IsUserCanViewLog = insurancePermission.IsInPermission(acessibleServices, PermissionActions.ViewLog);
            return viewdata;
        }

        public JsonViewData AddInsurance(AgencyInsurance insurance)
        {
            var viewData = new JsonViewData();
            if (!agencyRepository.AddInsurance(insurance))
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in saving the insurance.";
            }
            else
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceAdded, string.Empty);
                //InsuranceEngine.Refresh(Current.AgencyId);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Insurance was saved successfully";
            }
            return viewData;
        }

        public JsonViewData EditInsurance(AgencyInsurance insurance)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error in editing the insurance." };
            if (insurance != null)
            {
                
                    insurance.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditInsurance(insurance))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the insurance.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceUpdated, string.Empty);
                       
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was edited successfully";
                    }
               
            }
            return viewData;
        }

        public JsonViewData DeleteInsurance(int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance. Please try again." };
            if (agencyRepository.DeleteInsurance(Current.AgencyId, Id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceDeleted, string.Empty);
               
                viewData.isSuccessful = true;
                viewData.errorMessage = "The insurance was successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in deleting the insurance.";
            }

            return viewData;
        }

        public AgencyInsurance GetInsuranceOrDefault(int insuranceId)
        {
            return agencyRepository.GetInsurance(insuranceId, Current.AgencyId) ?? new AgencyInsurance();
        }

        public List<InsuranceLean> InsuranceList()
        {
            var insurances = new List<InsuranceLean>();
            int payor;
            if (int.TryParse(Current.Payor, out payor))
            {
                if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
                {
                    var name = ((MedicareIntermediary)payor).GetDescription();
                    if (name.IsNotNullOrEmpty())
                    {
                        insurances.Add(new InsuranceLean { Name = name, PayorType = (int)PayerTypes.MedicareTraditional, PayorId = "", InvoiceType = 1, IsTradtionalMedicare = true });
                    }
                }
            }
            var data = agencyRepository.GetLeanInsurances(Current.AgencyId);
            if (data != null && data.Count > 0)
            {
                insurances.AddRange(data.OrderBy(i => i.Name));
            }
            return insurances;
        }


        public List<SelectListItem> DisciplineTypes(Guid locationId, int disciplineTask)
        {
            var items = new List<SelectListItem>();
            if (Enum.IsDefined(typeof(DisciplineTasks), disciplineTask))
            {
                var task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var formGroup = task.GetFormGroup();
                if (formGroup.IsNotNullOrEmpty())
                {
                    var disciplineTaskArray = Enum.GetValues(typeof(DisciplineTasks));
                    var disciplineTasks = lookUpRepository.DisciplineTasks() ?? new List<DisciplineTask>();
                    var agencyChargeRates = new List<ChargeRate>();
                    var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.BillData.IsNotNullOrEmpty())
                        {
                            agencyChargeRates = location.BillData.ToObject<List<ChargeRate>>();
                        }
                    }
                    else
                    {
                        var mainLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                        if (mainLocation != null && mainLocation.BillData.IsNotNullOrEmpty())
                        {
                            agencyChargeRates = mainLocation.BillData.ToObject<List<ChargeRate>>();
                        }
                    }
                    var gcode = string.Empty;
                    if (agencyChargeRates.IsNotNullOrEmpty())
                    {
                        foreach (DisciplineTasks disciplineTaskItem in disciplineTaskArray)
                        {
                            if (formGroup.IsEqual(disciplineTaskItem.GetFormGroup()))
                            {
                                var rate = agencyChargeRates.SingleOrDefault(r => r.Id == (int)disciplineTaskItem);
                                if (rate != null && rate.Code.IsNotNullOrEmpty())
                                {
                                    gcode = rate.Code;
                                }
                                else
                                {
                                    var data = disciplineTasks.SingleOrDefault(d => d.Id == (int)disciplineTaskItem);
                                    if (data != null)
                                    {
                                        gcode = data.GCode;
                                    }
                                }
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("{0} ({1})", disciplineTaskItem.GetDescription(), gcode),
                                    Value = ((int)disciplineTaskItem).ToString(),
                                    Selected = (int)disciplineTaskItem == disciplineTask
                                });
                            }
                        }
                    }
                }
            }
            return items;
        }

        #region Case Manager

        //public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate)
        //{
        //    var events = new List<PatientEpisodeEvent>();
        //    var scheduleEvents = scheduleRepository.GetScheduleByBranchDateRangeAndStatus(Current.AgencyId, branchId,startDate,endDate, status, ScheduleStatusFactory.CaseManagerStatus().ToArray(), true);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        var episodeIds = scheduleEvents.Select(e => e.EpisodeId).Distinct().ToList();
        //        var userIds = new List<Guid>();
        //        var users = new List<User>();
        //        var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
        //        if (returnComments != null && returnComments.Count > 0)
        //        {
        //            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //            if (returnUserIds != null && returnUserIds.Count > 0)
        //            {
        //                userIds.AddRange(returnUserIds);
        //            }
        //        }
        //        var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //        if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //        {
        //            userIds.AddRange(scheduleUserIds);

        //        }
        //        if (userIds != null && userIds.Count > 0)
        //        {
        //            users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>(); //userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
        //        }
        //        scheduleEvents.ForEach(s =>
        //        {
        //            var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id && r.EpisodeId == s.EpisodeId).ToList() ?? new List<ReturnComment>();
        //            var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

        //            var userName = string.Empty;
        //            if (!s.UserId.IsEmpty())
        //            {
        //                var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //                if (user != null)
        //                {
        //                    userName = user.DisplayName;
        //                }
        //            }
        //            var details = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    
        //            events.Add(new PatientEpisodeEvent
        //            {
        //                EventId = s.Id,
        //                EpisodeId = s.EpisodeId,
        //                PatientId = s.PatientId,
        //                PrintUrl = Common.Url.Print(s, false),
        //                Status = s.StatusName,
        //                EpisodeRange = string.Format("{0} - {1}", s.StartDate, s.EndDate),
        //                PatientName = s.PatientName,
        //                TaskName = s.DisciplineTaskName,
        //                UserName = userName,
                        
        //                YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                RedNote = redNote,
        //                BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                EventDate = s.EventDate.IsValid() ? s.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.Id, s.DisciplineTask)

        //            });
        //        });
        //    }

        //    //var agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, branchId, status, startDate, endDate);
        //    //if (agencyPatientEpisodes != null && agencyPatientEpisodes.Count > 0)
        //    //{
        //    //    var episodeWithEventsDictionary = agencyPatientEpisodes.ToDictionary(g => g.Id,
        //    //        g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
        //    //              !s.IsDeprecated
        //    //              && s.EventDate.IsValidDate()
        //    //              && s.EventDate.ToDateTime().IsBetween(g.StartDate, g.EndDate)
        //    //              && s.EventDate.ToDateTime().IsBetween(startDate, endDate)
        //    //              && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //    //                            || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //    //                            || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //    //                            || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
        //    //                            || s.IsMissedVisit)
        //    //          ).ToList() : new List<ScheduleEvent>()
        //    //         );
        //    //    if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
        //    //    {
        //    //        var episodeIds = episodeWithEventsDictionary.Keys.ToList();
        //    //        var userIds = new List<Guid>();
        //    //        var users = new List<User>();
        //    //        var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
        //    //        if (returnComments != null && returnComments.Count > 0)
        //    //        {
        //    //            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //    //            if (returnUserIds != null && returnUserIds.Count > 0)
        //    //            {
        //    //                userIds.AddRange(returnUserIds);
        //    //            }
        //    //        }
        //    //        var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //    //        if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //    //        {
        //    //            userIds.AddRange(scheduleUserIds);

        //    //        }
        //    //        if (userIds != null && userIds.Count > 0)
        //    //        {
        //    //            users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>(); //userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
        //    //        }
        //    //        var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
        //    //        var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

        //    //        episodeWithEventsDictionary.ForEach((key, value) =>
        //    //        {
        //    //            if (value != null && value.Count > 0)
        //    //            {
        //    //                var episode = agencyPatientEpisodes.FirstOrDefault(e => e.Id == key);
        //    //                if (episode != null)
        //    //                {
        //    //                    value.ForEach(s =>
        //    //                    {
        //    //                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
        //    //                        var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

        //    //                        var userName = string.Empty;
        //    //                        if (!s.UserId.IsEmpty())
        //    //                        {
        //    //                            var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //    //                            if (user != null)
        //    //                            {
        //    //                                userName = user.DisplayName;
        //    //                            }
        //    //                        }
        //    //                        if (s.IsMissedVisit)
        //    //                        {
        //    //                            var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
        //    //                            if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
        //    //                            {
        //    //                                var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //    //                                Common.Url.Set(s, false, false);
        //    //                                events.Add(new PatientEpisodeEvent
        //    //                                {
        //    //                                    EventId = s.EventId,
        //    //                                    EpisodeId = s.EpisodeId,
        //    //                                    PatientId = s.PatientId,
        //    //                                    PrintUrl = s.PrintUrl,
        //    //                                    Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
        //    //                                    PatientName = episode.DisplayName,
        //    //                                    TaskName = s.DisciplineTaskName,
        //    //                                    UserName = userName,
        //    //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //    //                                    RedNote = redNote,
        //    //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //    //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //    //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //    //                                });
        //    //                            }
        //    //                        }
        //    //                        else
        //    //                        {
        //    //                            if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //    //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //    //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //    //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
        //    //                            {
        //    //                                var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //    //                                Common.Url.Set(s, false, false);
        //    //                                events.Add(new PatientEpisodeEvent
        //    //                                {
        //    //                                    EventId = s.EventId,
        //    //                                    EpisodeId = s.EpisodeId,
        //    //                                    PatientId = s.PatientId,
        //    //                                    PrintUrl = s.PrintUrl,
        //    //                                    Status = s.StatusName,
        //    //                                    PatientName = episode.DisplayName,
        //    //                                    TaskName = s.DisciplineTaskName,
        //    //                                    UserName = userName,
        //    //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //    //                                    RedNote = redNote,
        //    //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //    //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //    //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //    //                                });
        //    //                            }
        //    //                        }
        //    //                    });
        //    //                }
        //    //            }
        //    //        });
        //    //    }
        //    //}
        //    return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        //}

        //public List<PatientEpisodeEvent> GetPatientCaseManagerSchedule(Guid patientId)
        //{
        //    var events = new List<PatientEpisodeEvent>();
        //    var patientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, patientId);
        //    if (patientEpisodes != null && patientEpisodes.Count > 0)
        //    {
        //        var episodeWithEventsDictionary = patientEpisodes.ToDictionary(g => g.Id,
        //            g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
        //                  !s.IsDeprecated
        //                  && s.EventDate.IsValidDate()
        //                  && s.EventDate.ToDateTime().IsBetween(g.StartDate, g.EndDate)
        //                  && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
        //                                || s.IsMissedVisit)
        //              ).ToList() : new List<ScheduleEvent>()
        //             );
        //        if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
        //        {
        //            var episodeIds = episodeWithEventsDictionary.Keys.ToList();
        //            var userIds = new List<Guid>();
        //            var users = new List<User>();
        //            var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
        //            if (returnComments != null && returnComments.Count > 0)
        //            {
        //                var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //                if (returnUserIds != null && returnUserIds.Count > 0)
        //                {
        //                    userIds.AddRange(returnUserIds);
        //                }
        //            }
        //            var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //            if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //            {
        //                userIds.AddRange(scheduleUserIds);

        //            }
        //            if (userIds != null && userIds.Count > 0)
        //            {
        //                users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();// userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
        //            }
        //            var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
        //            var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

        //            episodeWithEventsDictionary.ForEach((key, value) =>
        //            {
        //                if (value != null && value.Count > 0)
        //                {
        //                    var episode = patientEpisodes.FirstOrDefault(e => e.Id == key);
        //                    if (episode != null)
        //                    {
        //                        value.ForEach(s =>
        //                        {
        //                            var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
        //                            var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

        //                            var userName = string.Empty;
        //                            if (!s.UserId.IsEmpty())
        //                            {
        //                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //                                if (user != null)
        //                                {
        //                                    userName = user.DisplayName;
        //                                }
        //                            }
        //                            if (s.IsMissedVisit)
        //                            {
        //                                var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
        //                                if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
        //                                {
        //                                    var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                    Common.Url.Set(s, false, false);
        //                                    events.Add(new PatientEpisodeEvent
        //                                    {
        //                                        EventId = s.EventId,
        //                                        EpisodeId = s.EpisodeId,
        //                                        PatientId = s.PatientId,
        //                                        PrintUrl = s.PrintUrl,
        //                                        Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
        //                                        TaskName = s.DisciplineTaskName,
        //                                        UserName = userName,
        //                                        YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                                        RedNote = redNote,
        //                                        BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                                        EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                                    });
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //                                    || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //                                    || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //                                    || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
        //                                {
        //                                    var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                    Common.Url.Set(s, false, false);
        //                                    events.Add(new PatientEpisodeEvent
        //                                    {
        //                                        EventId = s.EventId,
        //                                        EpisodeId = s.EpisodeId,
        //                                        PatientId = s.PatientId,
        //                                        PrintUrl = s.PrintUrl,
        //                                        Status = s.StatusName,
        //                                        TaskName = s.DisciplineTaskName,
        //                                        UserName = userName,
        //                                        YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                                        RedNote = redNote,
        //                                        BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                                        EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                                    });
        //                                }
        //                            }
        //                        });
        //                    }
        //                }
        //            });
        //        }
        //    }
        //    return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        //}

        #endregion

        #region  Past Due Recerts

        //public List<RecertEvent> GetRecertsPastDueWidget()
        //{
        //    return this.GetRecertsPastDue(Guid.Empty, 0, DateTime.Now.AddMonths(-4), DateTime.Now, true, 5);
        //    //return patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
        //    //var pastDueRecerts = new List<RecertEvent>();
        //    //var recertEpisodes = patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
        //    //if (recertEpisodes != null && recertEpisodes.Count > 0)
        //    //{
        //    //    recertEpisodes.ForEach(r =>
        //    //    {
        //    //        if (r.Schedule.IsNotNullOrEmpty())
        //    //        {
        //    //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
        //    //            if (schedule != null && schedule.Count > 0)
        //    //            {
        //    //                var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) &&
        //    //                    (s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath ||
        //    //                     s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge) && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() ||
        //    //                    s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() ||
        //    //                    s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
        //    //                if (dischargeSchedules != null && dischargeSchedules.Count > 0)
        //    //                {
        //    //                }
        //    //                else
        //    //                {
        //    //                    var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionOfCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionOfCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionOfCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionOfCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
        //    //                    if (episodeRecertSchedule != null)
        //    //                    {
        //    //                        if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
        //    //                        {
        //    //                        }
        //    //                        else
        //    //                        {
        //    //                            if (episodeRecertSchedule.EventDate.ToDateTime().Date < DateTime.Today.Date)
        //    //                            {
        //    //                                if (!episodeRecertSchedule.UserId.IsEmpty())
        //    //                                {
        //    //                                    r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
        //    //                                }
        //    //                                r.Status = episodeRecertSchedule.StatusName;
        //    //                                r.Schedule = string.Empty;
        //    //                                r.DateDifference = DateTime.Today.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
        //    //                                pastDueRecerts.Add(r);
        //    //                            }
        //    //                        }
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        if (r.TargetDate.Date < DateTime.Today.Date)
        //    //                        {
        //    //                            r.AssignedTo = "Unassigned";
        //    //                            r.Status = "Not Scheduled";
        //    //                            r.Schedule = string.Empty;
        //    //                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
        //    //                            pastDueRecerts.Add(r);
        //    //                        }
        //    //                    }
        //    //                }
        //    //            }
        //    //            else
        //    //            {
        //    //                if (r.TargetDate.Date < DateTime.Today.Date)
        //    //                {
        //    //                    r.AssignedTo = "Unassigned";
        //    //                    r.Status = "Not Scheduled";
        //    //                    r.Schedule = string.Empty;
        //    //                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
        //    //                    pastDueRecerts.Add(r);
        //    //                }
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            if (r.TargetDate.Date < DateTime.Today.Date)
        //    //            {
        //    //                r.AssignedTo = "Unassigned";
        //    //                r.Status = "Not Scheduled";
        //    //                r.Schedule = string.Empty;
        //    //                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
        //    //                pastDueRecerts.Add(r);
        //    //            }
        //    //        }
        //    //    });
        //    //}
        //    //return pastDueRecerts.Take(5).ToList();
        //}

        //public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        //{
        //    var pastDueRecerts = new List<RecertEvent>();
        //    var scheduleEvents = scheduleRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
        //        if (episodeIds != null && episodeIds.Count > 0)
        //        {
        //            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
        //            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };
        //            var recertAndROCStatus = dischargeStatus;
        //            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();
        //            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
        //            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

        //            var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //            var users = UserEngine.GetUsers(Current.AgencyId, userIds);
        //            episodeIds.ForEach(e =>
        //            {
        //                var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
        //                if (schedules != null)
        //                {
        //                    if (schedules.Count == 1)
        //                    {
        //                        var recert = new RecertEvent();
        //                        var evnt = schedules.FirstOrDefault();
        //                        if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)) || (transferDisciplineTasks.Contains(evnt.DisciplineTask) && transferStatus.Contains(evnt.Status) && evnt.EventDate.IsBetween(evnt.EndDate.AddDays(-5),evnt.EndDate)))
        //                        {
        //                        }
        //                        else if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
        //                        {
        //                            if (!evnt.UserId.IsEmpty())
        //                            {
        //                                var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
        //                                if (user != null)
        //                                {
        //                                    recert.AssignedTo = user.DisplayName;
        //                                }
        //                                else
        //                                {
        //                                    recert.AssignedTo = "Unassigned";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                recert.AssignedTo = "Unassigned";
        //                            }
        //                            if (evnt.EventDate.Date <= DateTime.MinValue.Date)
        //                            {
        //                                recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
        //                                recert.TargetDate = evnt.EndDate;
        //                            }
        //                            else
        //                            {
        //                                recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
        //                                recert.TargetDate = evnt.EventDate;
        //                            }
        //                            recert.Status = evnt.Status;
        //                            recert.PatientName = evnt.PatientName;
        //                            recert.PatientIdNumber = evnt.PatientIdNumber;
        //                            recert.EventDate = evnt.EventDate;
        //                            pastDueRecerts.Add(recert);
        //                        }
        //                    }
        //                    else if (schedules.Count > 1)
        //                    {
        //                        if (schedules.Exists(s => (dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)) || (transferDisciplineTasks.Contains(s.DisciplineTask) && transferStatus.Contains(s.Status)&& s.EventDate.IsBetween(s.EndDate.AddDays(-5),s.EndDate))))
        //                        {
        //                        }
        //                        else
        //                        {
        //                            var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
        //                            if (oasisROCOrRecert != null)
        //                            {
        //                                if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
        //                                {
        //                                }
        //                                else
        //                                {
        //                                    var recert = new RecertEvent();
        //                                    if (!oasisROCOrRecert.UserId.IsEmpty())
        //                                    {
        //                                        var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
        //                                        if (user != null)
        //                                        {
        //                                            recert.AssignedTo = user.DisplayName;
        //                                        }
        //                                        else
        //                                        {
        //                                            recert.AssignedTo = "Unassigned";
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        recert.AssignedTo = "Unassigned";
        //                                    }
        //                                    if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
        //                                    {
        //                                        recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
        //                                        recert.TargetDate = oasisROCOrRecert.EndDate;
        //                                    }
        //                                    else
        //                                    {
        //                                        recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
        //                                        recert.TargetDate = oasisROCOrRecert.EventDate;
        //                                    }
        //                                    recert.Status = oasisROCOrRecert.Status;
        //                                    recert.PatientName = oasisROCOrRecert.PatientName;
        //                                    recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
        //                                    recert.EventDate = oasisROCOrRecert.EventDate;
        //                                    pastDueRecerts.Add(recert);
        //                                }
        //                            }

        //                        }
        //                    }
        //                }
        //                if (isLimitForWidget && pastDueRecerts.Count >= limit)
        //                {
        //                    return;
        //                }
        //            });

        //        }
        //    }
        //    return pastDueRecerts;

        //    //var recertEpisodes = patientRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate.AddDays(5));
        //    //var pastDueRecerts = new List<RecertEvent>();
        //    //if (recertEpisodes != null && recertEpisodes.Count > 0)
        //    //{
        //    //    recertEpisodes.ForEach(r =>
        //    //    {
        //    //        if (r.Schedule.IsNotNullOrEmpty())
        //    //        {
        //    //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
        //    //            if (schedule != null && schedule.Count > 0)
        //    //            {
        //    //                var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false 
        //    //                    && s.EventDate.IsValidDate() 
        //    //                    && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date)
        //    //                    &&((s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge ||
        //    //                    s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
        //    //                    || (s.EventDate.ToDateTime().IsBetween(r.TargetDate.AddDays(-5), r.TargetDate) && (s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer)))
        //    //                    && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() ||
        //    //                    s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
        //    //                if (dischargeSchedules != null && dischargeSchedules.Count > 0)
        //    //                {
        //    //                }
        //    //                else
        //    //                {
        //    //                    var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionOfCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionOfCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionOfCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionOfCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
        //    //                    if (episodeRecertSchedule != null)
        //    //                    {
        //    //                        if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
        //    //                        {
        //    //                        }
        //    //                        else
        //    //                        {
        //    //                            if (episodeRecertSchedule.EventDate.ToDateTime().Date < endDate.Date)
        //    //                            {
        //    //                                if (!episodeRecertSchedule.UserId.IsEmpty())
        //    //                                {
        //    //                                    r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
        //    //                                }
        //    //                                r.Status = episodeRecertSchedule.StatusName;
        //    //                                r.Schedule = string.Empty;
        //    //                                r.DateDifference = endDate.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
        //    //                                pastDueRecerts.Add(r);
        //    //                            }
        //    //                        }
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        if (r.TargetDate.Date < endDate.Date)
        //    //                        {
        //    //                            r.AssignedTo = "Unassigned";
        //    //                            r.Status = "Not Scheduled";
        //    //                            r.Schedule = string.Empty;
        //    //                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
        //    //                            pastDueRecerts.Add(r);
        //    //                        }
        //    //                    }
        //    //                }
        //    //            }
        //    //            else
        //    //            {
        //    //                if (r.TargetDate.Date < endDate.Date)
        //    //                {
        //    //                    r.AssignedTo = "Unassigned";
        //    //                    r.Status = "Not Scheduled";
        //    //                    r.Schedule = string.Empty;
        //    //                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
        //    //                    pastDueRecerts.Add(r);
        //    //                }
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            if (r.TargetDate.Date < endDate.Date)
        //    //            {
        //    //                r.AssignedTo = "Unassigned";
        //    //                r.Status = "Not Scheduled";
        //    //                r.Schedule = string.Empty;
        //    //                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
        //    //                pastDueRecerts.Add(r);
        //    //            }
        //    //        }
        //    //    });
        //    //}
        //    //return pastDueRecerts;
        //}

        #endregion

        #region  Up Coming Recerts

        //public List<RecertEvent> GetRecertsUpcomingWidget()
        //{
        //    return this.GetRecertsUpcoming(Guid.Empty, 0, DateTime.Now, DateTime.Now.AddDays(24), true, 5);

        //    //return patientRepository.GetUpcomingRecertsWidgetLean(Current.AgencyId);
        //}

        //public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        //{
        //    var upcomingRecets = new List<RecertEvent>();
        //    var scheduleEvents = scheduleRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
        //        if (episodeIds != null && episodeIds.Count > 0)
        //        {
        //            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
        //            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
        //            var recertAndROCStatus = dischargeStatus; //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
        //            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();//new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
        //            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
        //            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

        //            var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //            var users = UserEngine.GetUsers(Current.AgencyId, userIds);
        //            episodeIds.ForEach(e =>
        //            {
        //                var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
        //                if (schedules != null)
        //                {
        //                    if (schedules.Count == 1)
        //                    {
        //                        var recert = new RecertEvent();
        //                        var evnt = schedules.FirstOrDefault();
        //                        if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)) || (transferDisciplineTasks.Contains(evnt.DisciplineTask) && transferStatus.Contains(evnt.Status) && evnt.EventDate.IsBetween(evnt.EndDate.AddDays(-5), evnt.EndDate)))
        //                        {
        //                        }
        //                        else if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
        //                        {
        //                            if (!evnt.UserId.IsEmpty())
        //                            {
        //                                var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
        //                                if (user != null)
        //                                {
        //                                    recert.AssignedTo = user.DisplayName;
        //                                }
        //                                else
        //                                {
        //                                    recert.AssignedTo = "Unassigned";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                recert.AssignedTo = "Unassigned";
        //                            }
        //                            if (evnt.EventDate.Date <= DateTime.MinValue.Date)
        //                            {
        //                                recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
        //                                recert.TargetDate = evnt.EndDate;
        //                            }
        //                            else
        //                            {
        //                                recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
        //                                recert.TargetDate = evnt.EventDate;
        //                            }
        //                            recert.Status = evnt.Status;
        //                            recert.PatientName = evnt.PatientName;
        //                            recert.PatientIdNumber = evnt.PatientIdNumber;
        //                            recert.EventDate = evnt.EventDate;
        //                            upcomingRecets.Add(recert);
        //                        }
        //                    }
        //                    else if (schedules.Count > 1)
        //                    {
        //                        if (schedules.Exists(s => (dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)) || (transferDisciplineTasks.Contains(s.DisciplineTask) && transferStatus.Contains(s.Status) && s.EventDate.IsBetween(s.EndDate.AddDays(-5), s.EndDate))))
        //                        {
        //                        }
        //                        else
        //                        {
        //                            var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
        //                            if (oasisROCOrRecert != null)
        //                            {
        //                                if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
        //                                {
        //                                }
        //                                else
        //                                {
        //                                    var recert = new RecertEvent();
        //                                    if (!oasisROCOrRecert.UserId.IsEmpty())
        //                                    {
        //                                        var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
        //                                        if (user != null)
        //                                        {
        //                                            recert.AssignedTo = user.DisplayName;
        //                                        }
        //                                        else
        //                                        {
        //                                            recert.AssignedTo = "Unassigned";
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        recert.AssignedTo = "Unassigned";
        //                                    }
        //                                    if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
        //                                    {
        //                                        recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
        //                                        recert.TargetDate = oasisROCOrRecert.EndDate;
        //                                    }
        //                                    else
        //                                    {
        //                                        recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
        //                                        recert.TargetDate = oasisROCOrRecert.EventDate;
        //                                    }
        //                                    recert.Status = oasisROCOrRecert.Status;
        //                                    recert.PatientName = oasisROCOrRecert.PatientName;
        //                                    recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
        //                                    recert.EventDate = oasisROCOrRecert.EventDate;
        //                                    upcomingRecets.Add(recert);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                if (isLimitForWidget && upcomingRecets.Count >= limit)
        //                {
        //                    return;
        //                }
        //            });
        //        }
        //    }
        //    return upcomingRecets;
        //}

        #endregion

        #region Orders

        //public Order GetOrder(Guid id, Guid patientId, Guid episodeId, string type)
        //{
        //    var order = new Order();
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty() && type.IsInteger() && Enum.IsDefined(typeof(OrderType), type.ToInteger()))
        //    {
        //        var typeEnum = ((OrderType)type.ToInteger()).ToString();
        //        if (typeEnum.IsNotNullOrEmpty())
        //        {
        //            switch (typeEnum)
        //            {
        //                case "PhysicianOrder":
        //                    var physicianOrder = patientRepository.GetOrder(id, Current.AgencyId);
        //                    if (physicianOrder != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = physicianOrder.Id,
        //                            PatientId = physicianOrder.PatientId,
        //                            EpisodeId = physicianOrder.EpisodeId,
        //                            Type = OrderType.PhysicianOrder,
        //                            Text = DisciplineTasks.PhysicianOrder.GetDescription(),
        //                            Number = physicianOrder.OrderNumber,
        //                            PatientName = physicianOrder.DisplayName,
        //                            PhysicianName = physicianOrder.PhysicianName,
        //                            ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
        //                            SendDate = physicianOrder.SentDate,
        //                            PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "FaceToFaceEncounter":
        //                    var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
        //                    if (faceToFaceEncounter != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = faceToFaceEncounter.Id,
        //                            PatientId = faceToFaceEncounter.PatientId,
        //                            EpisodeId = faceToFaceEncounter.EpisodeId,
        //                            Type = OrderType.FaceToFaceEncounter,
        //                            Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
        //                            Number = faceToFaceEncounter.OrderNumber,
        //                            PatientName = faceToFaceEncounter.DisplayName,
        //                            ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
        //                            SendDate = faceToFaceEncounter.RequestDate,
        //                            PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "HCFA485":
        //                case "NonOasisHCFA485":
        //                    var planofCare = planofCareRepository.Get(Current.AgencyId,  patientId, id);
        //                    if (planofCare != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = planofCare.Id,
        //                            PatientId = planofCare.PatientId,
        //                            EpisodeId = planofCare.EpisodeId,
        //                            Type = OrderType.HCFA485,
        //                            Text = "HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : ("NonOasisHCFA485" == type ? DisciplineTasks.NonOasisHCFA485.GetDescription() : string.Empty),
        //                            Number = planofCare.OrderNumber,
        //                            PatientName = planofCare.PatientName,
        //                            ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
        //                            SendDate = planofCare.SentDate,
        //                            PhysicianSignatureDate = planofCare.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "HCFA485StandAlone":
        //                    var planofCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, patientId, id);
        //                    if (planofCareStandAlone != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = planofCareStandAlone.Id,
        //                            PatientId = planofCareStandAlone.PatientId,
        //                            EpisodeId = planofCareStandAlone.EpisodeId,
        //                            Type = OrderType.HCFA485StandAlone,
        //                            Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
        //                            Number = planofCareStandAlone.OrderNumber,
        //                            PatientName = planofCareStandAlone.PatientName,
        //                            ReceivedDate = planofCareStandAlone.ReceivedDate > DateTime.MinValue ? planofCareStandAlone.ReceivedDate : planofCareStandAlone.SentDate,
        //                            SendDate = planofCareStandAlone.SentDate,
        //                            PhysicianSignatureDate = planofCareStandAlone.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "PtEvaluation":
        //                    var ptEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (ptEval != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = ptEval.Id,
        //                            PatientId = ptEval.PatientId,
        //                            EpisodeId = ptEval.EpisodeId,
        //                            Type = OrderType.PtEvaluation,
        //                            Text = DisciplineTasks.PTEvaluation.GetDescription(),
        //                            Number = ptEval.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = ptEval.ReceivedDate > DateTime.MinValue ? ptEval.ReceivedDate : ptEval.SentDate,
        //                            SendDate = ptEval.SentDate,
        //                            PhysicianSignatureDate = ptEval.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "PtReEvaluation":
        //                    var ptReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (ptReEval != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = ptReEval.Id,
        //                            PatientId = ptReEval.PatientId,
        //                            EpisodeId = ptReEval.EpisodeId,
        //                            Type = OrderType.PtReEvaluation,
        //                            Text = DisciplineTasks.PTReEvaluation.GetDescription(),
        //                            Number = ptReEval.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = ptReEval.ReceivedDate > DateTime.MinValue ? ptReEval.ReceivedDate : ptReEval.SentDate,
        //                            SendDate = ptReEval.SentDate,
        //                            PhysicianSignatureDate = ptReEval.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "OtEvaluation":
        //                    var otEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (otEval != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = otEval.Id,
        //                            PatientId = otEval.PatientId,
        //                            EpisodeId = otEval.EpisodeId,
        //                            Type = OrderType.OtEvaluation,
        //                            Text = DisciplineTasks.OTEvaluation.GetDescription(),
        //                            Number = otEval.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = otEval.ReceivedDate > DateTime.MinValue ? otEval.ReceivedDate : otEval.SentDate,
        //                            SendDate = otEval.SentDate,
        //                            PhysicianSignatureDate = otEval.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "OtReEvaluation":
        //                    var otReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (otReEval != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = otReEval.Id,
        //                            PatientId = otReEval.PatientId,
        //                            EpisodeId = otReEval.EpisodeId,
        //                            Type = OrderType.OtReEvaluation,
        //                            Text = DisciplineTasks.OTReEvaluation.GetDescription(),
        //                            Number = otReEval.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = otReEval.ReceivedDate > DateTime.MinValue ? otReEval.ReceivedDate : otReEval.SentDate,
        //                            SendDate = otReEval.SentDate,
        //                            PhysicianSignatureDate = otReEval.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "StEvaluation":
        //                    var stEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (stEval != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = stEval.Id,
        //                            PatientId = stEval.PatientId,
        //                            EpisodeId = stEval.EpisodeId,
        //                            Type = OrderType.StEvaluation,
        //                            Text = DisciplineTasks.STEvaluation.GetDescription(),
        //                            Number = stEval.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = stEval.ReceivedDate > DateTime.MinValue ? stEval.ReceivedDate : stEval.SentDate,
        //                            SendDate = stEval.SentDate,
        //                            PhysicianSignatureDate = stEval.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "StReEvaluation":
        //                    var stReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (stReEval != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = stReEval.Id,
        //                            PatientId = stReEval.PatientId,
        //                            EpisodeId = stReEval.EpisodeId,
        //                            Type = OrderType.StReEvaluation,
        //                            Text = DisciplineTasks.STReEvaluation.GetDescription(),
        //                            Number = stReEval.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = stReEval.ReceivedDate > DateTime.MinValue ? stReEval.ReceivedDate : stReEval.SentDate,
        //                            SendDate = stReEval.SentDate,
        //                            PhysicianSignatureDate = stReEval.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "PTDischarge":
        //                    var ptDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (ptDischarge != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = ptDischarge.Id,
        //                            PatientId = ptDischarge.PatientId,
        //                            EpisodeId = ptDischarge.EpisodeId,
        //                            Type = OrderType.PTDischarge,
        //                            Text = DisciplineTasks.PTDischarge.GetDescription(),
        //                            Number = ptDischarge.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = ptDischarge.ReceivedDate > DateTime.MinValue ? ptDischarge.ReceivedDate : ptDischarge.SentDate,
        //                            SendDate = ptDischarge.SentDate,
        //                            PhysicianSignatureDate = ptDischarge.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //                case "SixtyDaySummary":
        //                    var sixtyDaySummary = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //                    if (sixtyDaySummary != null)
        //                    {
        //                        order = new Order
        //                        {
        //                            Id = sixtyDaySummary.Id,
        //                            PatientId = sixtyDaySummary.PatientId,
        //                            EpisodeId = sixtyDaySummary.EpisodeId,
        //                            Type = OrderType.SixtyDaySummary,
        //                            Text = DisciplineTasks.SixtyDaySummary.GetDescription(),
        //                            Number = sixtyDaySummary.OrderNumber,
        //                            PatientName = patient != null ? patient.DisplayName : string.Empty,
        //                            ReceivedDate = sixtyDaySummary.ReceivedDate > DateTime.MinValue ? sixtyDaySummary.ReceivedDate : sixtyDaySummary.SentDate,
        //                            SendDate = sixtyDaySummary.SentDate,
        //                            PhysicianSignatureDate = sixtyDaySummary.PhysicianSignatureDate
        //                        };
        //                    }
        //                    break;
        //            }
        //        }
        //    }

        //    return order;
        //}

        //public List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        //{
        //    var orders = new List<Order>();
        //    var status = new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician };
        //    var schedules = scheduleRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
        //    if (schedules != null && schedules.Count > 0)
        //    {
        //        var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
        //        if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
        //        {
        //            var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
        //            if (physicianOrders != null && physicianOrders.Count > 0)
        //            {
        //                physicianOrders.ForEach(po =>
        //                {
        //                    var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = po.Id,
                                
        //                        PatientId = po.PatientId,
        //                        EpisodeId = po.EpisodeId,
        //                        Type = OrderType.PhysicianOrder,
        //                        Text = DisciplineTasks.PhysicianOrder.GetDescription(),
        //                        Number = po.OrderNumber,
        //                        PatientName = po.DisplayName,
        //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                        PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        CreatedDate = po.OrderDateFormatted

        //                    });
        //                });
        //            }
        //        }
        //        var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
        //        if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
        //        {
        //            var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

        //            if (planofCareOrders != null && planofCareOrders.Count > 0)
        //            {
        //                planofCareOrders.ForEach(poc =>
        //                {
        //                    var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        AgencyPhysician physician = null;
        //                        if (!poc.PhysicianId.IsEmpty())
        //                        {
        //                            physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                        }
        //                        else
        //                        {
        //                            if (poc.PhysicianData.IsNotNullOrEmpty())
        //                            {
        //                                var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
        //                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
        //                                {
        //                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
        //                                }
        //                            }
        //                        }
        //                        orders.Add(new Order
        //                        {
        //                            Id = poc.Id,
        //                            PatientId = poc.PatientId,
        //                            EpisodeId = poc.EpisodeId,
        //                            Type = OrderType.HCFA485,
        //                            Text = DisciplineTasks.HCFA485.GetDescription(),
        //                            Number = poc.OrderNumber,
        //                            PatientName = poc.PatientName,
        //                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                            PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
        //        if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
        //        {
        //            var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

        //            if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
        //            {
        //                planofCareStandAloneOrders.ForEach(poc =>
        //                {
        //                    var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        AgencyPhysician physician = null;
        //                        var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
        //                        if (!poc.PhysicianId.IsEmpty())
        //                        {
        //                            physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                        }
        //                        else
        //                        {
        //                            if (poc.PhysicianData.IsNotNullOrEmpty())
        //                            {
        //                                var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
        //                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
        //                                {
        //                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
        //                                }
        //                            }
        //                        }
        //                        orders.Add(new Order
        //                        {
        //                            Id = poc.Id,
        //                            PatientId = poc.PatientId,
        //                            EpisodeId = poc.EpisodeId,
        //                            Type = OrderType.HCFA485StandAlone,
        //                            Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
        //                            Number = poc.OrderNumber,
        //                            PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
        //                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                            PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            CreatedDate =evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
        //        if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
        //        {
        //            var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
        //            if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
        //            {
        //                faceToFaceEncounters.ForEach(ffe =>
        //                {
        //                    var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.Id == ffe.Id && s.EpisodeId == ffe.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        AgencyPhysician physician = null;
        //                        if (!ffe.PhysicianId.IsEmpty())
        //                        {
        //                            physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
        //                        }
        //                        else
        //                        {
        //                            if (ffe.PhysicianData.IsNotNullOrEmpty())
        //                            {
        //                                var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
        //                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
        //                                {
        //                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
        //                                }
        //                            }
        //                        }
        //                        orders.Add(new Order
        //                        {
        //                            Id = ffe.Id,
        //                            PatientId = ffe.PatientId,
        //                            EpisodeId = ffe.EpisodeId,
        //                            Type = OrderType.FaceToFaceEncounter,
        //                            Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
        //                            Number = ffe.OrderNumber,
        //                            PatientName = ffe.DisplayName,
        //                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
        //                            CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
        //                        });
        //                    }
        //                });
        //            }
        //        }
        //        var evalOrdersSchedule = schedules.Where(s =>
        //                (s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReassessment
        //                || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReassessment
        //                || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary
        //                || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment) && s.Status==status[1]).ToList();

        //        if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
        //        {
        //            var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds, startDate, endDate);
        //            if (evalOrders != null && evalOrders.Count > 0)
        //            {
        //                evalOrders.ForEach(eval =>
        //                {
        //                    var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = eval.Id,
        //                        PatientId = eval.PatientId,
        //                        EpisodeId = eval.EpisodeId,
        //                        Type = GetOrderType(eval.NoteType),
        //                        Text = GetDisciplineType(eval.NoteType).GetDescription(),
        //                        Number = eval.OrderNumber,
        //                        PatientName = eval.DisplayName,
        //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
        //                        CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
        //                        ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
        //                        SendDate = eval.SentDate
        //                    });
        //                });
        //            }
        //        }
        //    }
        //    return orders.Where(o => o.PhysicianAccess == sendAutomatically).OrderByDescending(o => o.CreatedDate).ToList();
        //}

        //public List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status)
        //{
        //    var orders = new List<Order>();
        //    var schedules = scheduleRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
        //    if (schedules != null && schedules.Count > 0 && status.Count > 1)
        //    {
        //        var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
        //        if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
        //        {
        //            var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, status[0], physicianOrdersIds, startDate, endDate);
        //            if (physicianOrders != null && physicianOrders.Count > 0)
        //            {
        //                physicianOrders.ForEach(po =>
        //                {
        //                    var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = po.Id,
        //                        PatientId = po.PatientId,
        //                        EpisodeId = po.EpisodeId,
        //                        Type = OrderType.PhysicianOrder,
        //                        Text = DisciplineTasks.PhysicianOrder.GetDescription(),
        //                        Number = po.OrderNumber,
        //                        PatientName = po.DisplayName,
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        PhysicianName = po.PhysicianName.IsNotNullOrEmpty() ? po.PhysicianName : physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                        PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
        //                        CreatedDate = po.OrderDate.ToShortDateString().ToZeroFilled(),
        //                        ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
        //                        SendDate = po.SentDate,
        //                        PhysicianSignatureDate = po.PhysicianSignatureDate
        //                    });
        //                });
        //            }
        //        }
        //        var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
        //        if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
        //        {
        //            var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, status[0], planofCareOrdersIds);
        //            if (planofCareOrders != null && planofCareOrders.Count > 0)
        //            {
        //                planofCareOrders.ForEach(poc =>
        //                {
        //                    var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                        orders.Add(new Order
        //                        {
        //                            Id = poc.Id,
        //                            PatientId = poc.PatientId,
        //                            EpisodeId = poc.EpisodeId,
        //                            Type = OrderType.HCFA485,
        //                            Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
        //                            Number = poc.OrderNumber,
        //                            PatientName = poc.PatientName,
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                            PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
        //                            CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
        //                            SendDate = poc.SentDate,
        //                            PhysicianSignatureDate = poc.PhysicianSignatureDate
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
        //        if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
        //        {
        //            var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, status[0], planofCareStandAloneOrdersIds);
        //            if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
        //            {
        //                planofCareStandAloneOrders.ForEach(poc =>
        //                {
        //                    var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                        orders.Add(new Order
        //                        {
        //                            Id = poc.Id,
        //                            PatientId = poc.PatientId,
        //                            EpisodeId = poc.EpisodeId,
        //                            Type = OrderType.HCFA485StandAlone,
        //                            Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
        //                            Number = poc.OrderNumber,
        //                            PatientName = poc.PatientName,
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                            CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
        //                            SendDate = poc.SentDate,
        //                            PhysicianSignatureDate = poc.PhysicianSignatureDate
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
        //        if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
        //        {
        //            var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, status[0], faceToFaceEncounterOrdersIds);

        //            if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
        //            {
        //                faceToFaceEncounters.ForEach(ffe =>
        //                {
        //                    var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.Id == ffe.Id && s.EpisodeId == ffe.EpisodeId);

        //                    if (evnt != null)
        //                    {
        //                        var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
        //                        orders.Add(new Order
        //                        {
        //                            Id = ffe.Id,
        //                            PatientId = ffe.PatientId,
        //                            EpisodeId = ffe.EpisodeId,
        //                            Type = OrderType.FaceToFaceEncounter,
        //                            Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
        //                            Number = ffe.OrderNumber,
        //                            PatientName = ffe.DisplayName,
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                            PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
        //                            CreatedDate =evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
        //                            SendDate = ffe.RequestDate,
        //                            PhysicianSignatureDate = ffe.SignatureDate
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var evalOrdersSchedule = schedules.Where(s =>
        //                s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary).ToList();
        //        if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
        //        {
        //            var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int> { status[1] }, evalOrdersIds, startDate, endDate);
        //            if (evalOrders != null && evalOrders.Count > 0)
        //            {
        //                evalOrders.ForEach(eval =>
        //                {
        //                    var evnt = evalOrdersSchedule.FirstOrDefault(s => s.Id == eval.Id && s.EpisodeId == eval.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
        //                        orders.Add(new Order
        //                        {
        //                            Id = eval.Id,
        //                            PatientId = eval.PatientId,
        //                            EpisodeId = eval.EpisodeId,
        //                            Type = GetOrderType(eval.NoteType),
        //                            Text = GetDisciplineType(eval.NoteType).GetDescription(),
        //                            Number = eval.OrderNumber,
        //                            PatientName = eval.DisplayName,
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                            PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
        //                            CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
        //                            SendDate = eval.SentDate,
        //                            PhysicianSignatureDate = eval.PhysicianSignatureDate
        //                        });
        //                    }
        //                });
        //            }
        //        }
        //    }
        //    return orders.OrderByDescending(o => o.CreatedDate).ToList();
        //}

        //public List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate)
        //{
        //    var orders = new List<Order>();
        //    var schedules = scheduleRepository.GetPendingSignatureOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
        //    if (schedules != null && schedules.Count > 0)
        //    {
        //        var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
        //        if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
        //        {
        //            var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var physicianOrders = patientRepository.GetPendingPhysicianSignatureOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
        //            if (physicianOrders != null && physicianOrders.Count > 0)
        //            {
        //                physicianOrders.ForEach(po =>
        //                {
        //                    var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = po.Id,
        //                        PatientId = po.PatientId,
        //                        EpisodeId = po.EpisodeId,
        //                        Type = OrderType.PhysicianOrder,
        //                        Text = DisciplineTasks.PhysicianOrder.GetDescription(),
        //                        Number = po.OrderNumber,
        //                        PatientName = po.DisplayName,
        //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        SendDate = po.SentDate,
        //                        CreatedDate = po.OrderDateFormatted,
        //                        ReceivedDate = DateTime.Today,
        //                        PhysicianSignatureDate = po.PhysicianSignatureDate
        //                    });
        //                });
        //            }
        //        }

        //        var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
        //        if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
        //        {
        //            var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var planofCareOrders = planofCareRepository.GetPendingSignaturePlanofCares(Current.AgencyId, planofCareOrdersIds);

        //            if (planofCareOrders != null && planofCareOrders.Count > 0)
        //            {
        //                planofCareOrders.ForEach(poc =>
        //                {
        //                    var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        AgencyPhysician physician = null;
        //                        if (!poc.PhysicianId.IsEmpty())
        //                        {
        //                            physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                        }
        //                        else
        //                        {
        //                            if (poc.PhysicianData.IsNotNullOrEmpty())
        //                            {
        //                                var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
        //                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
        //                                {
        //                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
        //                                }
        //                            }
        //                        }
        //                        orders.Add(new Order
        //                        {
        //                            Id = poc.Id,
        //                            PatientId = poc.PatientId,
        //                            EpisodeId = poc.EpisodeId,
        //                            Type = OrderType.HCFA485,
        //                            Text = DisciplineTasks.HCFA485.GetDescription(),
        //                            Number = poc.OrderNumber,
        //                            PatientName = poc.PatientName,
        //                            PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            CreatedDate =evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
        //                            SendDate = poc.SentDate,
        //                            PhysicianSignatureDate = poc.PhysicianSignatureDate
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
        //        if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
        //        {
        //            var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var planofCareStandAloneOrders = planofCareRepository.GetPendingSignaturePlanofCaresStandAlone(Current.AgencyId, planofCareStandAloneOrdersIds);
        //            if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
        //            {
        //                planofCareStandAloneOrders.ForEach(poc =>
        //                {
        //                    var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
        //                    if (evnt != null)
        //                    {
        //                        AgencyPhysician physician = null;
        //                        if (!poc.PhysicianId.IsEmpty())
        //                        {
        //                            physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                        }
        //                        else
        //                        {
        //                            if (poc.PhysicianData.IsNotNullOrEmpty())
        //                            {
        //                                var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
        //                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
        //                                {
        //                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
        //                                }
        //                            }
        //                        }
        //                        orders.Add(new Order
        //                        {
        //                            Id = poc.Id,
        //                            PatientId = poc.PatientId,
        //                            EpisodeId = poc.EpisodeId,
        //                            Type = OrderType.HCFA485StandAlone,
        //                            Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
        //                            Number = poc.OrderNumber,
        //                            PatientName = poc.PatientName,
        //                            PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            CreatedDate = evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
        //                            SendDate = poc.SentDate,
        //                            PhysicianSignatureDate = poc.PhysicianSignatureDate
        //                        });
        //                    }
        //                });
        //            }
        //        }

        //        var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
        //        if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
        //        {
        //            var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var faceToFaceEncounters = patientRepository.GetPendingSignatureFaceToFaceEncounterOrders(Current.AgencyId, faceToFaceEncounterOrdersIds);

        //            if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
        //            {
        //                faceToFaceEncounters.ForEach(ffe =>
        //                    {
        //                        var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
        //                        orders.Add(new Order
        //                        {
        //                            Id = ffe.Id,
        //                            PatientId = ffe.PatientId,
        //                            EpisodeId = ffe.EpisodeId,
        //                            Type = OrderType.FaceToFaceEncounter,
        //                            Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
        //                            Number = ffe.OrderNumber,
        //                            PatientName = ffe.DisplayName,
        //                            PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            SendDate = ffe.SentDate,
        //                            CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
        //                            ReceivedDate = DateTime.Today,
        //                            PhysicianSignatureDate = ffe.SignatureDate

        //                        });
        //                    });
        //            }
        //        }

        //        var evalSchedules = schedules.Where(s =>
        //                s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
        //                || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment || s.DisciplineTask == (int)DisciplineTasks.PTDischarge
        //                || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary).ToList();

        //        if (evalSchedules != null && evalSchedules.Count > 0)
        //        {
        //            var evalIds = evalSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
        //            var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int>() { (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically }, evalIds, startDate, endDate);
        //            if (evalOrders != null && evalOrders.Count > 0)
        //            {
        //                evalOrders.ForEach(eval =>
        //                {
        //                    var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = eval.Id,
        //                        PatientId = eval.PatientId,
        //                        EpisodeId = eval.EpisodeId,
        //                        Type = GetOrderType(eval.NoteType),
        //                        Text = GetDisciplineType(eval.NoteType).GetDescription(),
        //                        Number = eval.OrderNumber,
        //                        PatientName = eval.DisplayName,
        //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        SendDate = eval.SentDate,
        //                        CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
        //                        ReceivedDate = DateTime.Today,
        //                        PhysicianSignatureDate = eval.PhysicianSignatureDate
        //                    });
        //                });
        //            }
        //        }
        //    }
        //    return orders.OrderByDescending(o => o.CreatedDate).ToList();
        //}

        //public bool MarkOrdersAsSent(FormCollection formCollection)
        //{
        //    var result = true;

        //    var sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
        //    formCollection.Remove("SendAutomatically");

        //    var status = (int)ScheduleStatus.OrderSentToPhysician;
        //    var physician = new AgencyPhysician();

        //    foreach (var key in formCollection.AllKeys)
        //    {
        //        string answers = formCollection.GetValues(key).Join(",");
        //        string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //        if (key.IsInteger() && key.ToInteger() == (int)OrderType.PhysicianOrder)
        //        {
        //            status = (int)ScheduleStatus.OrderSentToPhysician;
        //            if (sendElectronically)
        //            {
        //                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
        //            }
        //            answersArray.ForEach(item =>
        //            {
        //                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
        //                var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
        //                var order = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
        //                if (order.PhysicianData.IsNotNullOrEmpty())
        //                {
        //                    physician = order.PhysicianData.ToObject<AgencyPhysician>();
        //                }
        //                if (order != null)
        //                {
        //                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
        //                    if (scheduleEvent != null)
        //                    {
        //                        var oldStatus = scheduleEvent.Status;
        //                        scheduleEvent.Status = status;
        //                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                        {
        //                            if (patientRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue, DateTime.Now))
        //                            {
        //                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                                {
        //                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
        //                                }

        //                            }
        //                            else
        //                            {
        //                                result = false;
        //                                scheduleEvent.Status = oldStatus;
        //                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                                return;
        //                            }
        //                        }

        //                    }

        //                }

        //            });
        //        }
        //        if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485)
        //        {
        //            status = (int)ScheduleStatus.OrderSentToPhysician;
        //            if (sendElectronically)
        //            {
        //                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
        //            }
        //            answersArray.ForEach(item =>
        //            {
        //                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
        //                var pocId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
        //                var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
        //                var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
        //                var planofCare = planofCareRepository.Get(Current.AgencyId,  patientId, pocId);
        //                if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
        //                if (planofCare != null)
        //                {
        //                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
        //                    if (scheduleEvent != null)
        //                    {
        //                        var oldStatus = scheduleEvent.Status;
        //                        scheduleEvent.Status = status;

        //                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                        {
        //                            planofCare.Status = status;
        //                            planofCare.SentDate = DateTime.Now;
        //                            if (planofCareRepository.Update(planofCare))
        //                            {
        //                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                                {
        //                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                                }

        //                            }
        //                            else
        //                            {
        //                                result = false;
        //                                scheduleEvent.Status = oldStatus;
        //                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                                return;
        //                            }
        //                        }
        //                    }
        //                }
        //            });
        //        }
        //        if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485StandAlone)
        //        {
        //            status = (int)ScheduleStatus.OrderSentToPhysician;
        //            if (sendElectronically)
        //            {
        //                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
        //            }
        //            answersArray.ForEach(item =>
        //            {
        //                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
        //                var pocOrderId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
        //                var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
        //                var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
        //                var planofCare = planofCareRepository.GetStandAlone(Current.AgencyId,  patientId, pocOrderId);
        //                if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
        //                if (planofCare != null)
        //                {

        //                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
        //                    if (scheduleEvent != null)
        //                    {
        //                        var oldStatus = scheduleEvent.Status;
        //                        scheduleEvent.Status = status;
        //                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                        {
        //                            planofCare.Status = status;
        //                            planofCare.SentDate = DateTime.Now;
        //                            if (planofCareRepository.UpdateStandAlone(planofCare))
        //                            {
        //                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                                {
        //                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                result = false;
        //                                scheduleEvent.Status = oldStatus;
        //                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                                return;
        //                            }
        //                        }
        //                    }

        //                }
        //            });
        //        }
        //        if (key.IsInteger() && key.ToInteger() == (int)OrderType.FaceToFaceEncounter)
        //        {
        //            status = (int)ScheduleStatus.OrderSentToPhysician;
        //            if (sendElectronically)
        //            {
        //                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
        //            }

        //            answersArray.ForEach(item =>
        //            {
        //                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
        //                var facetofaceId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
        //                var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
        //                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
        //                if (faceToFaceEncounter.PhysicianData.IsNotNullOrEmpty()) physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
        //                if (faceToFaceEncounter != null)
        //                {

        //                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
        //                    if (scheduleEvent != null)
        //                    {
        //                        var oldStatus = scheduleEvent.Status;
        //                        scheduleEvent.Status = status;
        //                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                        {
        //                            if (patientRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
        //                            {
        //                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                                {
        //                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                result = false;
        //                                scheduleEvent.Status = oldStatus;
        //                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                                return;

        //                            }
        //                        }
        //                    }

        //                }
        //            });
        //        }
        //        if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtEvaluation || key.ToInteger() == (int)OrderType.PtReEvaluation || key.ToInteger() == (int)OrderType.PTReassessment
        //            || key.ToInteger() == (int)OrderType.OtEvaluation || key.ToInteger() == (int)OrderType.OtReEvaluation || key.ToInteger() == (int)OrderType.OTReassessment
        //            || key.ToInteger() == (int)OrderType.StEvaluation || key.ToInteger() == (int)OrderType.StReEvaluation
        //            || key.ToInteger() == (int)OrderType.MSWEvaluation) || key.ToInteger() == (int)OrderType.PTDischarge
        //            || key.ToInteger() == (int)OrderType.SixtyDaySummary)
        //        {
        //            status = (int)ScheduleStatus.EvalSentToPhysician;
        //            if (sendElectronically)
        //            {
        //                status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
        //            }
        //            answersArray.ForEach(item =>
        //            {
        //                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
        //                var evalId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
        //                var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
        //                var evalOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
        //                if (evalOrder.PhysicianData.IsNotNullOrEmpty()) physician = evalOrder.PhysicianData.ToObject<AgencyPhysician>();
        //                if (evalOrder != null)
        //                {
        //                    var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, evalOrder.PatientId, evalOrder.EpisodeId, evalOrder.Id);
        //                    if (scheduleEvent != null)
        //                    {
        //                        var oldStatus = scheduleEvent.Status;
        //                        scheduleEvent.Status = status;
        //                        if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                        {
        //                            evalOrder.Status = status;
        //                            evalOrder.SentDate = DateTime.Now;
        //                            if (patientRepository.UpdateVisitNote(evalOrder))
        //                            {
        //                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                                {
        //                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                result = false;
        //                                scheduleEvent.Status = oldStatus;
        //                                scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                                return;
        //                            }
        //                        }
        //                    }

        //                }
        //            });
        //        }
        //        if (result && physician != null)
        //        {
        //            string subject = string.Format("{0} has sent you an order", Current.AgencyName);
        //            var pLastName = physician.LastName.IsNotNullOrEmpty() && physician.LastName.Trim().IsNotNullOrEmpty() && physician.LastName.Trim().Length >= 1 ? physician.LastName.Trim().Substring(0, 1) : string.Empty;
        //            string lastName = pLastName.ToUpperCase() + pLastName.ToLowerCase();//physician.LastName.Trim().Substring(0, 1).ToUpper() + physician.LastName.Trim().Substring(1).ToLower();
        //            string bodyText = MessageBuilder.PrepareTextFrom("PhysicianOrderNotification", "recipientlastname", lastName, "senderfullname", Current.AgencyName);
        //            if (physician.EmailAddress.IsNotNullOrEmpty() && physician.EmailAddress.Trim().IsNotNullOrEmpty())
        //            {
        //                Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
        //            }
        //        }
        //    }

        //    return result;
        //}

        //public void MarkOrderAsReturned(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        //{
        //    if (!physicianSignatureDate.IsValid())
        //    {
        //        physicianSignatureDate = DateTime.Today;
        //    }
        //    if (type == OrderType.PhysicianOrder)
        //    {
        //        var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
        //        if (order != null)
        //        {
        //            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
        //            if (scheduleEvent != null)
        //            {
        //                var oldStatus = scheduleEvent.Status;
        //                scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;

        //                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                {
        //                    var physician = order.PhysicianData.ToObject<AgencyPhysician>();
        //                    if (physician != null)
        //                    {
        //                        order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                    else if (!order.PhysicianId.IsEmpty())
        //                    {
        //                        physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                        }
        //                    }
        //                    order.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
        //                    order.ReceivedDate = receivedDate;
        //                    order.PhysicianSignatureDate = physicianSignatureDate;
        //                    if (patientRepository.UpdateOrderModel(order))
        //                    {
        //                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                        {
        //                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        scheduleEvent.Status = oldStatus;
        //                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    if (type == OrderType.HCFA485)
        //    {
        //        var planofCare = planofCareRepository.Get(Current.AgencyId,  patientId, id);
        //        if (planofCare != null)
        //        {
        //            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
        //            if (scheduleEvent != null)
        //            {
        //                var oldStatus = scheduleEvent.Status;
        //                scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
        //                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                {
        //                    var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
        //                    if (physician != null)
        //                    {
        //                        planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                    else if (!planofCare.PhysicianId.IsEmpty())
        //                    {
        //                        physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                        }
        //                    }
        //                    planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
        //                    planofCare.ReceivedDate = receivedDate;
        //                    planofCare.PhysicianSignatureDate = physicianSignatureDate;
        //                    if (planofCareRepository.Update(planofCare))
        //                    {
        //                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                        {
        //                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        scheduleEvent.Status = oldStatus;
        //                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    if (type == OrderType.HCFA485StandAlone)
        //    {
        //        var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId,  patientId, id);
        //        if (pocStandAlone != null)
        //        {
        //            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, pocStandAlone.PatientId, pocStandAlone.EpisodeId, pocStandAlone.Id);
        //            if (scheduleEvent != null)
        //            {
        //                var oldStatus = scheduleEvent.Status;
        //                scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
        //                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                {
        //                    var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
        //                    if (physician != null)
        //                    {
        //                        pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                    else if (!pocStandAlone.PhysicianId.IsEmpty())
        //                    {
        //                        physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                        }
        //                    }
        //                    pocStandAlone.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
        //                    pocStandAlone.ReceivedDate = receivedDate;
        //                    pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
        //                    if (planofCareRepository.UpdateStandAlone(pocStandAlone))
        //                    {
        //                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                        {
        //                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        scheduleEvent.Status = oldStatus;
        //                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    if (type == OrderType.FaceToFaceEncounter)
        //    {
        //        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
        //        if (faceToFaceEncounter != null)
        //        {
        //            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
        //            if (scheduleEvent != null)
        //            {
        //                var oldStatus = scheduleEvent.Status;
        //                scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        
        //                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                {
        //                    var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
        //                    if (physician != null)
        //                    {
        //                        faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                    else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
        //                    {
        //                        physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                        }
        //                    }
        //                    faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
        //                    faceToFaceEncounter.ReceivedDate = receivedDate;
        //                    faceToFaceEncounter.SignatureDate = physicianSignatureDate;
        //                    if (patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter))
        //                    {
        //                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                        {
        //                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        scheduleEvent.Status = oldStatus;
        //                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
        //        || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
        //        || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
        //        || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge
        //        || type == OrderType.SixtyDaySummary)
        //    {
        //        var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //        if (eval != null)
        //        {
        //            var scheduleEvent = scheduleRepository.GetScheduleEventNew(Current.AgencyId, eval.PatientId, eval.EpisodeId, eval.Id);
        //            if (scheduleEvent != null)
        //            {
        //                var oldStatus = scheduleEvent.Status;
        //                scheduleEvent.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
        //                if (scheduleRepository.UpdateScheduleEventNew(scheduleEvent))
        //                {
        //                    var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
        //                    if (physician != null)
        //                    {
        //                        eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                    else if (!eval.PhysicianId.IsEmpty())
        //                    {
        //                        physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                        }
        //                    }
        //                    eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
        //                    eval.ReceivedDate = receivedDate;
        //                    eval.PhysicianSignatureDate = physicianSignatureDate;
        //                    if (patientRepository.UpdateVisitNote(eval))
        //                    {
        //                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //                        {
        //                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        scheduleEvent.Status = oldStatus;
        //                        scheduleRepository.UpdateScheduleEventNew(scheduleEvent);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //public bool UpdateOrderDates(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        //{
        //    bool result = false;
        //    if (type == OrderType.PhysicianOrder)
        //    {
        //        var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
        //        if (order != null)
        //        {
        //            order.ReceivedDate = receivedDate;
        //            order.SentDate = sendDate;
        //            order.PhysicianSignatureDate = physicianSignatureDate;
        //            if (order.PhysicianSignatureText.IsNullOrEmpty())
        //            {
        //                var physician = order.PhysicianData.ToObject<AgencyPhysician>();
        //                if (physician != null)
        //                {
        //                    order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                }
        //                else if (!order.PhysicianId.IsEmpty())
        //                {
        //                    physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
        //                    if (physician != null)
        //                    {
        //                        order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                }
        //            }
        //            result = patientRepository.UpdateOrderModel(order);
        //        }
        //    }
        //    else if (type == OrderType.HCFA485)
        //    {
        //        var planofCare = planofCareRepository.Get(Current.AgencyId,  patientId, id);
        //        if (planofCare != null)
        //        {
        //            planofCare.ReceivedDate = receivedDate;
        //            planofCare.SentDate = sendDate;
        //            planofCare.PhysicianSignatureDate = physicianSignatureDate;
        //            if (planofCare.PhysicianSignatureText.IsNullOrEmpty())
        //            {
        //                var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
        //                if (physician != null)
        //                {
        //                    planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                }
        //                else if (!planofCare.PhysicianId.IsEmpty())
        //                {
        //                    physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
        //                    if (physician != null)
        //                    {
        //                        planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                }
        //            }
        //            result = planofCareRepository.Update(planofCare);
        //        }
        //    }
        //    else if (type == OrderType.HCFA485StandAlone)
        //    {
        //        var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, patientId, id);
        //        if (pocStandAlone != null)
        //        {
        //            pocStandAlone.ReceivedDate = receivedDate;
        //            pocStandAlone.SentDate = sendDate;
        //            pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
        //            if (pocStandAlone.PhysicianSignatureText.IsNullOrEmpty())
        //            {
        //                var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
        //                if (physician != null)
        //                {
        //                    pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                }
        //                else if (!pocStandAlone.PhysicianId.IsEmpty())
        //                {
        //                    physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
        //                    if (physician != null)
        //                    {
        //                        pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                }
        //            }
        //            result = planofCareRepository.UpdateStandAlone(pocStandAlone);
        //        }
        //    }
        //    else if (type == OrderType.FaceToFaceEncounter)
        //    {
        //        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
        //        if (faceToFaceEncounter != null)
        //        {
        //            faceToFaceEncounter.ReceivedDate = receivedDate;
        //            faceToFaceEncounter.RequestDate = sendDate;
        //            faceToFaceEncounter.SignatureDate = physicianSignatureDate;
        //            if (faceToFaceEncounter.SignatureText.IsNullOrEmpty())
        //            {
        //                var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
        //                if (physician != null)
        //                {
        //                    faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                }
        //                else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
        //                {
        //                    physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
        //                    if (physician != null)
        //                    {
        //                        faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                }
        //            }
        //            result = patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
        //        }
        //    }
        //    else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
        //        || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
        //        || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
        //        || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge
        //        || type == OrderType.SixtyDaySummary)
        //    {
        //        var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
        //        if (eval != null)
        //        {
        //            eval.ReceivedDate = receivedDate;
        //            eval.SentDate = sendDate;
        //            eval.PhysicianSignatureDate = physicianSignatureDate;
        //            if (eval.PhysicianSignatureText.IsNullOrEmpty())
        //            {
        //                var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
        //                if (physician != null)
        //                {
        //                    eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                }
        //                else if (!eval.PhysicianId.IsEmpty())
        //                {
        //                    physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
        //                    if (physician != null)
        //                    {
        //                        eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
        //                    }
        //                }
        //            }
        //            result = patientRepository.UpdateVisitNote(eval);
        //        }
        //    }
        //    return result;
        //}

        #endregion

        #region Hospitals

        public IList<AgencyHospital> GetHospitals()
        {
            return agencyRepository.GetHospitals(Current.AgencyId);
        }

        public AgencyHospital FindHospital(Guid Id)
        {
            var hospital = agencyRepository.FindHospital(Current.AgencyId, Id);
            if (hospital != null)
            {
                hospital.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.Hospital, PermissionActions.ViewLog);
            }
            return hospital;
        }

        public JsonViewData DeleteHospital(Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this hospital. Please try again." };
            if (agencyRepository.FindHospital(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteHospital(Current.AgencyId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the hospital.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected Hospital don't exist.";
            }
            return viewData;
        }

        public JsonViewData AddHospital(AgencyHospital hospital)
        {
            var viewData = new JsonViewData();

            if (hospital.IsValid())
            {
                hospital.AgencyId = Current.AgencyId;
                hospital.Id = Guid.NewGuid();
                if (!agencyRepository.AddHospital(hospital))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the hospital.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateHospital(AgencyHospital hospital)
        {
            var viewData = new JsonViewData();

            if (hospital.IsValid())
            {
                if (agencyRepository.FindHospital(Current.AgencyId, hospital.Id) != null)
                {
                    hospital.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditHospital(hospital))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the hospital.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Hospital was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected hospital does not exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }
            return viewData;
        }

        #endregion

        #region Templates

        public IList<AgencyTemplate> GetTemplates()
        {
            return TemplateEngine.GetTemplates(Current.AgencyId);
        }

        public AgencyTemplate GetTemplate(Guid id)
        {
            var template = agencyRepository.GetTemplate(Current.AgencyId, id);
            if (template != null)
            {
                template.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.Template, PermissionActions.ViewLog);
            }
            return template;
        }

        public AgencyTemplate GetTemplateOrDefault(Guid id)
        {
            return TemplateEngine.GetTemplate(id, Current.AgencyId) ?? new AgencyTemplate();
        }

        public JsonViewData AddTemplate(AgencyTemplate template)
        {
            var viewData = new JsonViewData();

            if (template.IsValid())
            {
                template.AgencyId = Current.AgencyId;
                template.Id = Guid.NewGuid();
                if (!agencyRepository.AddTemplate(template))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "A problem occured while saving the template.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Template has been saved successfully.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateTemplate(AgencyTemplate template)
        {
            var viewData = new JsonViewData();
            if (template.IsValid())
            {
                var existingTemplate = agencyRepository.GetTemplate(Current.AgencyId, template.Id);
                if (existingTemplate != null)
                {
                    existingTemplate.Text = template.Text;
                    existingTemplate.Title = template.Title;
                    if (!agencyRepository.UpdateTemplate(existingTemplate))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "A problem occured while editing the template.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Template has been edited successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected template don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData DeleteTemplate(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Template could not be deleted. Please try again." };
            if (agencyRepository.DeleteTemplate(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Template has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Template could not be deleted.";
            }
            return viewData;
        }

        #endregion

        #region Supplies

        public IList<AgencySupply> GetSupplies()
        {
            return agencyRepository.GetSupplies(Current.AgencyId);
        }

        public AgencySupply GetSupply(Guid id)
        {
            var supply = agencyRepository.GetSupply(Current.AgencyId, id);
            if (supply != null)
            {
                supply.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.Supply, PermissionActions.ViewLog);
            }
            return supply;
        }

        public AgencySupply GetSupplyOrDefault(Guid id)
        {
            return agencyRepository.GetSupply(Current.AgencyId, id) ?? new AgencySupply();
        }

        public JsonViewData AddSupply(AgencySupply supply)
        {
            var viewData = new JsonViewData();

            if (supply.IsValid())
            {
                supply.Id = Guid.NewGuid();
                supply.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddSupply(supply))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the supply.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, supply.Id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Supply was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = supply.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateSupply(AgencySupply supply)
        {
            var viewData = new JsonViewData();

            if (supply.IsValid())
            {
                var existingSupply = agencyRepository.GetSupply(Current.AgencyId, supply.Id);
                if (existingSupply != null)
                {
                    existingSupply.Code = supply.Code;
                    existingSupply.Description = supply.Description;
                    existingSupply.RevenueCode = supply.RevenueCode;
                    existingSupply.UnitCost = supply.UnitCost;
                    if (!agencyRepository.UpdateSupply(existingSupply))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the supply.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, supply.Id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Supply was updated successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected supply does not exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = supply.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData DeleteSupply(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply could not be deleted. Please try again." };
            if (agencyRepository.DeleteSupply(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Supply has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Supply could not be deleted.";
            }
            return viewData;
        }

        public object SuppliesSearch(string term, int limit)
        {
            return  agencyRepository.GetSupplies(Current.AgencyId, term, limit).Select(p => new { p.Description, p.Code, p.Id, p.RevenueCode, p.UnitCost }).ToList();
        }

        #endregion

        #region Adjustment Codes

        public IList<AgencyAdjustmentCode> GetAdjustmentCodes()
        {
            return agencyRepository.GetAdjustmentCodes(Current.AgencyId);
        }

        public AgencyAdjustmentCode FindAdjustmentCode(Guid id)
        {
            var adjustmentCode = agencyRepository.FindAdjustmentCode(Current.AgencyId, id);
            if (adjustmentCode != null)
            {
                adjustmentCode.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.AdjustmentCode, PermissionActions.ViewLog);
            }
            return adjustmentCode;
        }

        public JsonViewData AddAdjustmentCode(AgencyAdjustmentCode adjustmentCode)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the code." };
            if (adjustmentCode.IsValid())
            {
                adjustmentCode.AgencyId = Current.AgencyId;
                adjustmentCode.Id = Guid.NewGuid();
                if (agencyRepository.AddAdjustmentCode(adjustmentCode))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Code was saved successfully";
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, adjustmentCode.Id.ToString(), LogType.AgencyAdjustmentCode, LogAction.AgencyAdjustmentCodeAdded, string.Empty);
                }
            }
            return viewData;
        }

        public JsonViewData UpdateAdjustmentCode(AgencyAdjustmentCode adjustmentCode)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the code." };

            if (adjustmentCode.IsValid())
            {
                var existingCode = agencyRepository.FindAdjustmentCode(Current.AgencyId, adjustmentCode.Id);
                if (existingCode != null)
                {
                    existingCode.Code = adjustmentCode.Code;
                    existingCode.Description = adjustmentCode.Description;
                    if (agencyRepository.UpdateAdjustmentCode(existingCode))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Adjustment Code was updated successfully";
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, adjustmentCode.Id.ToString(), LogType.AgencyAdjustmentCode, LogAction.AgencyAdjustmentCodeUpdated, string.Empty);
                    }
                }
            }
            return viewData;
        }

        public JsonViewData DeleteAdjustmentCode(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Adjustment Code could not be deleted. Please try again." };

            if (agencyRepository.DeleteAdjustmentCode(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Adjustment Code was updated successfully";
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyAdjustmentCode, LogAction.AgencyAdjustmentCodeDeleted, string.Empty);
            }
            return viewData;
        }

        #endregion

        #region Upload Types

        public IList<UploadType> GetUploadTypes()
        {
            return agencyRepository.GetUploadTypes(Current.AgencyId);
        }

        public UploadType FindUploadType(Guid id)
        {
            var uploadType = agencyRepository.FindUploadType(Current.AgencyId, id);
            if (uploadType != null)
            {
                uploadType.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.UploadType, PermissionActions.ViewLog);
            }
            return uploadType;
        }

        public JsonViewData AddUploadType(UploadType uploadType)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the upload type." };
            if (uploadType.IsValid())
            {
                uploadType.AgencyId = Current.AgencyId;
                uploadType.Id = Guid.NewGuid();
                if (agencyRepository.AddUploadType(uploadType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Upload type was saved successfully";
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, uploadType.Id.ToString(), LogType.AgencyUploadType, LogAction.AgencyUploadTypeAdded, string.Empty);
                }
            }
            return viewData;
        }

        public JsonViewData UpdateUploadType(UploadType uploadType)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Error saving the Upload Type." };

            if (uploadType.IsValid())
            {
                var existingUploadType = agencyRepository.FindUploadType(Current.AgencyId, uploadType.Id);
                if (existingUploadType != null)
                {
                    existingUploadType.Type = uploadType.Type;
                    if (agencyRepository.UpdateUploadType(existingUploadType))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Upload Type updated successfully";
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, uploadType.Id.ToString(), LogType.AgencyUploadType, LogAction.AgencyUploadTypeUpdated, string.Empty);
                    }
                }
            }
            return viewData;
        }

        public JsonViewData DeleteUploadType(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Upload Type could not be deleted. Please try again." };

            if (agencyRepository.DeleteUploadType(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Upload Type was deleted successfully";
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyUploadType, LogAction.AgencyUploadTypeDeleted, string.Empty);
            }
            return viewData;
        }

        #endregion

        public MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid reportId)
        {
            var report = agencyRepository.GetMedicareEligibilitySummary(Current.AgencyId, reportId);
            if (report != null)
            {
                report.AgencyName = Current.AgencyName;
                report.Data = report.Report.IsNotNullOrEmpty() ? report.Report.ToObject<MedicareEligibilitySummaryData>() : new MedicareEligibilitySummaryData();
                if (report.Data == null)
                {
                    report.Data = new MedicareEligibilitySummaryData();
                }
            }
            return report;
        }

        public List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(DateTime startDate, DateTime endDate)
        {
            return agencyRepository.GetMedicareEligibilitySummariesBetweenDates(Current.AgencyId, startDate, endDate);
        }

        public SingleServiceGridViewData GetGridViewData(ParentPermission category)
        {
            var viewData = new SingleServiceGridViewData();
            var isFrozen = Current.IsAgencyFrozen;
            var actions = new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Export, (int)PermissionActions.Add, (int)PermissionActions.Delete, (int)PermissionActions.Edit };
            var allPermission = Current.CategoryService(Current.AcessibleServices, category, actions);
            if (allPermission.IsNotNullOrEmpty())
            {
                actions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == (int)PermissionActions.ViewList)
                    {
                        viewData.AvailableService = permission;
                        viewData.IsUserCanViewList = permission.HasValidValue();
                    }
                    else if (a == (int)PermissionActions.Export)
                    {
                        viewData.IsUserCanExport = permission.HasValidValue();
                    }
                    else if (a == (int)PermissionActions.Add)
                    {
                        viewData.IsUserCanAdd =!isFrozen && permission.HasValidValue();
                    }
                    else if (a == (int)PermissionActions.Edit)
                    {
                        viewData.IsUserCanEdit = !isFrozen && permission.HasValidValue();
                    }
                    else if (a == (int)PermissionActions.Delete)
                    {
                        viewData.IsUserCanDelete = !isFrozen && permission.HasValidValue();
                    }
                });
            }
           return viewData;
        }


        #region Private 

        //private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        //{
        //    //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
        //    //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
        //    foreach (ReturnComment comment in newComments)
        //    {
        //        if (comment.IsDeprecated) continue;
        //        if (scheduleCommentString.IsNotNullOrEmpty())
        //        {
        //            scheduleCommentString += "<hr/>";
        //        }
        //        if (comment.UserId == Current.UserId)
        //        {
        //            scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
        //        }
        //        var userName = string.Empty;
        //        if (!comment.UserId.IsEmpty())
        //        {
        //            var user = users.FirstOrDefault(u => u.Id == comment.UserId);
        //            if (user != null)
        //            {
        //                userName = user.DisplayName;
        //            }
        //        }
        //        scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
        //    }
        //    return scheduleCommentString;
        //}

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }
            return list;
        }

        //private OrderType GetOrderType(string disciplineTask)
        //{
        //    var orderType = OrderType.PtEvaluation;
        //    if (disciplineTask.IsNotNullOrEmpty())
        //    {
        //        switch (disciplineTask)
        //        {
        //            case "MSWEvaluationAssessment":
        //                orderType = OrderType.MSWEvaluation;
        //                break;
        //            case "PTReEvaluation":
        //                orderType = OrderType.PtReEvaluation;
        //                break;
        //            case "OTEvaluation":
        //                orderType = OrderType.OtEvaluation;
        //                break;
        //            case "OTReEvaluation":
        //                orderType = OrderType.OtReEvaluation;
        //                break;
        //            case "STEvaluation":
        //                orderType = OrderType.StEvaluation;
        //                break;
        //            case "STReEvaluation":
        //                orderType = OrderType.StReEvaluation;
        //                break;
        //            case "PTDischarge":
        //                orderType = OrderType.PTDischarge;
        //                break;
        //            case "SixtyDaySummary":
        //                orderType = OrderType.SixtyDaySummary;
        //                break;
        //        }
        //    }
        //    return orderType;
        //}

        //private DisciplineTasks GetDisciplineType(string disciplineTask)
        //{
        //    var task = DisciplineTasks.PTEvaluation;
        //    if (disciplineTask.IsNotNullOrEmpty())
        //    {
        //        switch (disciplineTask)
        //        {
        //            case "MSWEvaluationAssessment":
        //                task = DisciplineTasks.MSWEvaluationAssessment;
        //                break;
        //            case "PTReEvaluation":
        //                task = DisciplineTasks.PTReEvaluation;
        //                break;
        //            case "OTEvaluation":
        //                task = DisciplineTasks.OTEvaluation;
        //                break;
        //            case "OTReEvaluation":
        //                task = DisciplineTasks.OTReEvaluation;
        //                break;
        //            case "STEvaluation":
        //                task = DisciplineTasks.STEvaluation;
        //                break;
        //            case "STReEvaluation":
        //                task = DisciplineTasks.STReEvaluation;
        //                break;
        //            case "PTDischarge":
        //                task = DisciplineTasks.PTDischarge;
        //                break;
        //            case "SixtyDaySummary":
        //                task = DisciplineTasks.SixtyDaySummary;
        //                break;
        //            case "PTReassessment":
        //                task = DisciplineTasks.PTReassessment;
        //                break;
        //            case "OTReassessment":
        //                task = DisciplineTasks.OTReassessment;
        //                break;
        //        }
        //    }
        //    return task;
        //}

        #endregion
    }
}
