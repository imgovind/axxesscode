﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;

    using Axxess.LookUp.Repositories;

    using Axxess.AgencyManagement.Repositories;

    public class PrivateDutyReportService : ReportService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
        #region Constructor and Private Members

        //private readonly HHPatientProfileRepository profileRepository;
        //private readonly HHPatientAdmissionRepository admissionRepository;
        //private readonly HHPlanOfCareRepository planofCareRepository;
        //private readonly HHPhysicianOrderRepository physicianOrderRepository;

        private readonly PrivateDutyBillingRepository billingRepository;
        private readonly PrivateDutyTaskRepository scheduleRepository;
        private readonly PrivateDutyEpisodeRepository episodeRepository;


        public PrivateDutyReportService(PrivateDutyDataProvider dataProvider, ILookUpDataProvider lookUpDataProvider)
            : base(dataProvider.PatientProfileRepository, dataProvider.BillingRepository, dataProvider.PatientAdmissionRepository, dataProvider.PlanOfCareRepository, dataProvider.TaskRepository, dataProvider.EpisodeRepository, dataProvider.PhysicianOrderRepository, dataProvider.MongoRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            base.agencyRepository = dataProvider.AgencyRepository;
            base.userRepository = dataProvider.UserRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.lookUpRepository = lookUpDataProvider.LookUpRepository;

            
            this.billingRepository = dataProvider.BillingRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;

            base.Service = AgencyServices.PrivateDuty;
        }

        #endregion

        #region Patient Reports

        private List<PatientRoster> GetPatientRoster(List<PatientRoster> rosterList, Guid branchCode, int statusId, int insuranceId, bool isExcel)
        {
            if (rosterList.IsNotNullOrEmpty())
            {
                var physicians = new List<AgencyPhysician>();
                if (isExcel)
                {
                    var patientIds = rosterList.Select(r => r.Id).Distinct().ToList();
                    physicians = physicianRepository.GetPatientsPrimaryPhysician(Current.AgencyId, patientIds) ?? new List<AgencyPhysician>();
                }
                var insuranceIds = rosterList.Where(s => s.PatientInsuranceId.IsInteger() && s.PatientInsuranceId.ToInteger() > 0).Select(i => i.PatientInsuranceId.ToInteger()).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                rosterList.ForEach(roster =>
                {
                    if (isExcel)
                    {
                        //TODO:needs work
                        //var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, roster.Id, DateTime.Now, "Nursing");
                        //IDictionary<string, Question> lastAssessment = null;
                        //if (lastEpisode != null && !lastEpisode.AssessmentId.IsEmpty() && lastEpisode.AssessmentType.IsNotNullOrEmpty())
                        //{
                        //    lastAssessment = assessmentService.GetAssessment(lastEpisode.AssessmentId, lastEpisode.AssessmentType).ToDictionary();
                        //}
                        //if (lastAssessment != null && lastAssessment.ContainsKey("M1020PrimaryDiagnosis"))
                        //{
                        //    roster.PatientPrimaryDiagnosis = lastAssessment["M1020PrimaryDiagnosis"].Answer;
                        //}
                        //else
                        //{
                        //    roster.PatientPrimaryDiagnosis = "";
                        //}
                        //if (lastAssessment != null && lastAssessment.ContainsKey("M1022PrimaryDiagnosis1"))
                        //{
                        //    roster.PatientSecondaryDiagnosis = lastAssessment["M1022PrimaryDiagnosis1"].Answer;
                        //}
                        //else
                        //{
                        //    roster.PatientSecondaryDiagnosis = "";
                        //}

                        if (!roster.Id.IsEmpty())
                        {
                            var physician = physicians.FirstOrDefault(p => p.PatientId == roster.Id);// PhysicianEngine.Get(roster.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                roster.PhysicianNpi = physician.NPI;
                                roster.PhysicianName = physician.DisplayName;
                                roster.PhysicianPhone = physician.PhoneWork.ToPhone();
                                roster.PhysicianFacsimile = physician.FaxNumber;
                                roster.PhysicianPhoneHome = physician.PhoneAlternate;
                                roster.PhysicianEmailAddress = physician.EmailAddress;
                            }
                        }
                    }
                    var insurance = insurances.FirstOrDefault(i => i.Id.ToString() == roster.PatientInsuranceId);
                    if (insurance != null)
                    {
                        roster.PatientInsuranceName = insurance.Name;
                    }
                });

            }
            return rosterList.OrderBy(o => o.PatientDisplayName).ToList();
        }

        protected override void SurveyCensusAppSpecific(List<SurveyCensus> surveyCensuses, List<User> users, List<AgencyPhysician> physicians, List<InsuranceLean> insurances, bool isExcel, List<Guid> patientIds)
        {
            //var idsQuery = patientIds.Select(Id => string.Format("'{0}'", Id)).ToArray().Join(", ");
            //var disciplineTasksOASISSOC = DisciplineTaskFactory.SOCDisciplineTasks(true).ToArray();
            //var disciplineTasksOASISROCAndRecert = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();
            //var surveyCensuseEpisodesWithEvents = scheduleRepository.GetEpisodesForSurveyCensesAndPatientRoster(Current.AgencyId, idsQuery);
            //var isEpisodeEventsExist = surveyCensuseEpisodesWithEvents.IsNotNullOrEmpty();
            //surveyCensuses.ForEach(surveyCensus =>
            //{
            //    if (isEpisodeEventsExist)
            //    {
            //        SurveyCensusDiagnosisAndDiscipline(surveyCensus, surveyCensuseEpisodesWithEvents, disciplineTasksOASISSOC, disciplineTasksOASISROCAndRecert);
            //    }
            //    SetUserInsuranceAndPhysician(surveyCensus, users, physicians, insurances, isExcel);
            //});
        }

        private void SurveyCensusDiagnosisAndDiscipline(SurveyCensus surveyCensus, List<ScheduleEvent> surveyCensuseEpisodesWithEvents, int [] disciplineTasksOASISSOC, int [] disciplineTasksOASISROCAndRecert)
        {
            //var patientSurveyCensuseEpisodes = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id).GroupBy(s => s.EpisodeId).Select(s => s.First()).ToList();
            //// var patientSurveyCensuseEpisodes = surveyCensuseEpisodes.Where(s => s.Id == surveyCensus.Id).OrderByDescending(s => s.StartDate).ToList();
            //if (patientSurveyCensuseEpisodes != null && patientSurveyCensuseEpisodes.Count > 0)
            //{
            //    var episode = patientSurveyCensuseEpisodes.OrderByDescending(s => s.StartDate).FirstOrDefault();
            //    //patientSurveyCensuseEpisodes.FirstOrDefault();//patientRepository.GetCurrentEpisodeOnly(Current.AgencyId, surveyCensus.Id);
            //    if (episode != null)
            //    {
            //        surveyCensus.CertPeriod = (episode.StartDate.IsValid() ? episode.StartDate.ToString("MM/dd/yyyy") : "") + " - " + (episode.EndDate.IsValid() ? episode.EndDate.ToString("MM/dd/yyyy") : "");
            //        var schedules = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == episode.PatientId && s.EpisodeId == episode.EpisodeId).ToList();
            //        if (schedules != null && schedules.Count > 0)
            //        {
            //            var scheduleEvent = schedules.Where(s => s.EpisodeId == episode.EpisodeId && disciplineTasksOASISSOC.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate).FirstOrDefault();
            //            if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
            //            {
            //                var diagnosis = scheduleEvent.Diagnosis();
            //                if (diagnosis != null && diagnosis.Count > 0)
            //                {
            //                    surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
            //                    surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
            //                }
            //            }
            //            else
            //            {
            //                var previousEpisode = patientSurveyCensuseEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
            //                if (previousEpisode != null)
            //                {
            //                    var previousSchedule = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
            //                    if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
            //                    {
            //                        var diagnosis = previousSchedule.Diagnosis();
            //                        if (diagnosis != null && diagnosis.Count > 0)
            //                        {
            //                            surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
            //                            surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
            //                        }
            //                    }
            //                }
            //            }
            //            surveyCensus.Discipline = schedules.Discipline<ScheduleEvent>().Join(",");
            //        }
            //        else
            //        {
            //            var previousEpisode = patientSurveyCensuseEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
            //            if (previousEpisode != null)
            //            {
            //                var previousSchedule = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
            //                if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
            //                {
            //                    var diagnosis = previousSchedule.Diagnosis();
            //                    if (diagnosis != null && diagnosis.Count > 0)
            //                    {
            //                        surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
            //                        surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
            //                    }
            //                }
            //            }
            //        }
            //        //var assessment = GetEpisodeAssessment(episode, patientSurveyCensuseEpisodes.Count >= 2 ? patientSurveyCensuseEpisodes[1] : null);
            //        //if (assessment != null)
            //        //{
            //        //    var diagnosis = assessment.Diagnosis();
            //        //    if (diagnosis != null && diagnosis.Count > 0)
            //        //    {
            //        //        surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
            //        //        surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
            //        //    }
            //        //}
            //        //if (episode.Schedule.IsNotNullOrEmpty())
            //        //{
            //        //    surveyCensus.Discipline = episode.Schedule.ToObject<List<ScheduleEvent>>().Discipline().Join(",");
            //        //}
            //    }
            //}
        }


        #endregion

        #region Clinical Reports

        #endregion

        #region Schedule Reports

        #endregion

        #region Billing Reports

        #endregion

        #region Employee Reports

        #endregion

        #region Statistical Reports


        #endregion


    }
}
