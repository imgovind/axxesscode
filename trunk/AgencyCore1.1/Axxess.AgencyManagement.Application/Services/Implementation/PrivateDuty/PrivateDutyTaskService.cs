﻿
namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.AgencyManagement.Application.ViewData;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;


    public class PrivateDutyTaskService : TaskService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
        private PrivateDutyTaskRepository scheduleRepository;
        private PrivateDutyMultiDocument noteRepository;
        private PrivateDutyPlanOfCareRepository planofCareRepository;
        private PrivateDutyEpisodeRepository episodeRepository;

        public PrivateDutyTaskService(PrivateDutyDataProvider dataProvider, ILookUpDataProvider lookupDataProvider) :
            base(dataProvider.TaskRepository, dataProvider.MultiDocument, dataProvider.EpisodeRepository)
        {
            base.lookupRepository = lookupDataProvider.LookUpRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.patientAdmissionRepository = dataProvider.PatientAdmissionRepository;
            this.assetRepository = dataProvider.AssetRepository;
            this.userRepository = dataProvider.UserRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.MultiDocument;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.planofCareRepository = dataProvider.PlanOfCareRepository;
            base.Service = AgencyServices.PrivateDuty;

        }

        //public EpisodeAndTaskViewData<PrivateDutyCarePeriod> GetScheduleTasksBetweenDates(Guid userId, DateTime startDate, DateTime endDate)
        //{
        //    return GetScheduleTasksBetweenDates(Guid.Empty, userId, startDate, endDate);
        //}

        private EpisodeAndTaskViewData<PrivateDutyScheduleTask, PrivateDutyCarePeriod> GetScheduleTasksBetweenDates(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
           
            var tasks = scheduleRepository.GetScheduleTasksBetweenDates(Current.AgencyId, patientId, userId, startDate, endDate, false);
            List<PrivateDutyCarePeriod> episodes = null;
            if ( patientId.IsEmpty())
            {
                if (tasks.IsNotNullOrEmpty())
                {
                    episodes = episodeRepository.GetPatientsActiveEpisodesLeanByDateRange(Current.AgencyId, tasks.Select(t => t.PatientId).ToList(), startDate, endDate);
                }
            }
            else 
            {
                episodes = episodeRepository.GetPatientActiveEpisodesLeanByDateRange(Current.AgencyId, patientId, startDate, endDate);
            }

            return new EpisodeAndTaskViewData<PrivateDutyScheduleTask, PrivateDutyCarePeriod>(tasks, episodes);
        }

        public PrivateDutySchedulerViewData ActivityEpisodeTasksViewData(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            var viewData = new UserTaskViewData();
            SetPermission(viewData);
            var data = GetScheduleTasksBetweenDates(patientId, userId, startDate, endDate);
            if (data.Tasks.IsNotNullOrEmpty() && data.CarePeriods.IsNotNullOrEmpty())
            {
                ProcessScheduleEventsActions(viewData, data.CarePeriods, data.Tasks, patientId.IsEmpty());
                if (!userId.IsEmpty())
                {
                    data.CarePeriods.Clear();
                }
            }
            var returnData = new PrivateDutySchedulerViewData(viewData.ScheduleEvents, data.CarePeriods);
            returnData.IsStickyNote=viewData.IsStickyNote;
            returnData.IsUserCanEdit = viewData.IsUserCanEdit;
            returnData.IsUserCanDelete = viewData.IsUserCanDelete;
            returnData.IsUserCanPrint = viewData.IsUserCanPrint;
            returnData.IsUserCanReassign = viewData.IsUserCanReassign;
            returnData.IsUserCanRestore = viewData.IsUserCanRestore;
            returnData.IsUserCanReopen = viewData.IsUserCanReopen;
            returnData.IsUserCanEditDetail = viewData.IsUserCanEditDetail;

            return returnData;
        }

        public UserTaskViewData ActivityTaskViewData(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            var viewData = new UserTaskViewData();
            SetPermission(viewData);
            var data = GetScheduleTasksBetweenDates(patientId, userId, startDate, endDate);
            if (data != null && data.Tasks.IsNotNullOrEmpty())
            {
                ProcessUserScheduleEventsActions(viewData, data.CarePeriods, data.Tasks, true);
            }
            return viewData;
        }

       
       
        public List<PrivateDutyScheduleTask> GetScheduleTasksBetweenDatesToPrint(Guid userId, Guid patientId, string name, DateTime startDate, DateTime endDate)
        {
            var tasks = scheduleRepository.GetScheduleTasksBetweenDates(Current.AgencyId, patientId, userId, startDate, endDate, false);
            List<User> users = null;
            Dictionary<Guid, string> patients = null;

            //Schedule Center is currently viewing a patient's tasks
            if (userId.IsEmpty())
            {
                var scheduleUserIds = tasks.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                users = UserEngine.GetUsers(Current.AgencyId, scheduleUserIds);
            }
            //Schedule Center is currently viewing the tasks assigned to a specific user
            else if (patientId.IsEmpty())
            {
                var schedulePatientIds = tasks.Where(s => !s.PatientId.IsEmpty()).Select(s => s.PatientId).Distinct().ToList();
                patients = patientRepository.GetPatientNamesByIds(Current.AgencyId, schedulePatientIds);
            }
            if (tasks.IsNotNullOrEmpty())
            {
                foreach (var task in tasks)
                {
                    if (userId.IsEmpty())
                    {
                        task.PatientName = name;
                        var user = users.FirstOrDefault(u => u.Id == task.UserId);
                        task.UserName = user != null ? user.DisplayName : string.Empty;
                    }
                    else if (patientId.IsEmpty())
                    {
                        task.UserName = name;
                        task.PatientName = patients.ContainsKey(task.PatientId) ? patients[task.PatientId] : string.Empty;
                    }
                }
            }
            return tasks;
        }

        /// <summary>
        /// Creates Private Duty Schedule Tasks based on the list of date times given to it.
        /// </summary>
        /// <param name="episode"></param>
        /// <param name="disciplineTask"></param>
        /// <param name="userId"></param>
        /// <param name="visitDates"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="isAllDay"></param>
        /// <returns></returns>
        public List<PrivateDutyScheduleTask> AddMultiDayScheduleTaskHelper(PrivateDutyCarePeriod episode, DisciplineTask disciplineTask, Guid userId, List<DateTime> visitDates, DateTime startTime, DateTime endTime, bool isAllDay)
        {
            var scheduledEvents = new List<PrivateDutyScheduleTask>();
            if (episode != null)
            {
                if (visitDates != null && visitDates.Count > 0)
                {
                    visitDates.ForEach(date =>
                    {
                        var eventStartTime = new DateTime(date.Year, date.Month, date.Day, startTime.Hour, startTime.Minute, startTime.Second, startTime.Millisecond);
                        var eventEndTime = new DateTime(date.Year, date.Month, date.Day, endTime.Hour, endTime.Minute, endTime.Second, endTime.Millisecond);
                        var newScheduledEvent = new PrivateDutyScheduleTask
                        {
                            AgencyId = episode.AgencyId,
                            Id = Guid.NewGuid(),
                            PatientId = episode.PatientId,
                            EpisodeId = episode.Id,
                            UserId = userId,
                            DisciplineTask = disciplineTask.Id,
                            Discipline = disciplineTask.Discipline,
                            Status = disciplineTask.DefaultStatus,
                            Version = disciplineTask.Version,
                            IsBillable = disciplineTask.IsBillable,
                            EventStartTime = eventStartTime,
                            EventEndTime = eventEndTime,
                            VisitStartTime = eventStartTime,
                            VisitEndTime = eventEndTime,
                            StartDate = episode.StartDate,
                            EndDate = episode.EndDate,
                            IsAllDay = isAllDay,
                            IsDeprecated = false
                        };
                        scheduledEvents.Add(newScheduledEvent);
                    });
                }
            }
            return scheduledEvents;
        }

        protected override bool GetDocumentsAndFiltersForWidget(IDictionary<int, Dictionary<int, List<int>>> permission, out int editDocuments, out string aditionalFilter)
        {
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            editDocuments = (int)DocumentType.None;
            aditionalFilter = string.Empty;
            GetViewAndSetEditPermission(permission, ParentPermission.Schedule, ref editDocuments, DocumentType.Notes | DocumentType.OASIS);
            if (GetViewAndSetEditPermission(permission, ParentPermission.Schedule, ref editDocuments, DocumentType.Notes | DocumentType.OASIS))
            {
                var excluded = new List<int>();
                //if (!GetViewAndSetEditPermission(permission, ParentPermission.Orders, ref editDocuments, DocumentType.PhysicianOrder | DocumentType.FaceToFaceEncounter | DocumentType.PlanOfCare))
                //{
                    //excluded.AddRange(DisciplineTaskFactory.AllPhysicianOrders());
                excluded.Add((int)DisciplineTasks.FaceToFaceEncounter);
                excluded.Add((int)DisciplineTasks.PhysicianOrder);

                //}
                //if (!GetViewAndSetEditPermission(permission, ParentPermission.ManageInfectionReport, ref editDocuments, DocumentType.Infection))
                //{
                    excluded.Add((int)DisciplineTasks.InfectionReport);
                //}
                //if (!GetViewAndSetEditPermission(permission, ParentPermission.ManageIncidentAccidentReport, ref editDocuments, DocumentType.IncidentAccident))
                //{
                    excluded.Add((int)DisciplineTasks.IncidentAccidentReport);
                //}
                //if (!GetViewAndSetEditPermission(permission, ParentPermission.CommunicationNote, ref editDocuments, DocumentType.CommunicationNote))
                //{
                    excluded.Add((int)DisciplineTasks.CommunicationNote);
                //}
                if (excluded.IsNotNullOrEmpty())
                {
                    aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} ) AND st.DisciplineTask NOT IN ({1})", status.Select(s => s.ToString()).ToArray().Join(","), excluded.ToCommaSeperatedList());
                }
                else
                {
                    aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} )", status.Select(s => s.ToString()).ToArray().Join(","));
                }
            }
            else
            {
                var included = new List<int>();
                included.Add((int)DisciplineTasks.HCFA485StandAlone);
                included.Add((int)DisciplineTasks.HCFA485);
                included.Add((int)DisciplineTasks.HCFA486);
                //if (GetViewAndSetEditPermission(permission, ParentPermission.Orders, ref  editDocuments, DocumentType.PhysicianOrder | DocumentType.FaceToFaceEncounter | DocumentType.PlanOfCare))
                //{
                //    included.AddRange(DisciplineTaskFactory.AllPhysicianOrders());
                //}
                //if (GetViewAndSetEditPermission(permission, ParentPermission.ManageInfectionReport, ref  editDocuments, DocumentType.Infection))
                //{
                //    included.Add((int)DisciplineTasks.InfectionReport);
                //}
                //if (GetViewAndSetEditPermission(permission, ParentPermission.ManageIncidentAccidentReport, ref  editDocuments, DocumentType.IncidentAccident))
                //{
                //    included.Add((int)DisciplineTasks.IncidentAccidentReport);
                //}
                //if (GetViewAndSetEditPermission(permission, ParentPermission.CommunicationNote, ref  editDocuments, DocumentType.CommunicationNote))
                //{
                //    included.Add((int)DisciplineTasks.CommunicationNote);
                //}
                if (included.IsNotNullOrEmpty())
                {
                    aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} ) AND st.DisciplineTask IN ({1})", status.Select(s => s.ToString()).ToArray().Join(","), included.ToCommaSeperatedList());
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        //public bool UpdateTaskDetails(PrivateDutyScheduleTask editedTask, HttpFileCollectionBase httpFiles)
        //{
        //    bool result = false;
        //    if (editedTask != null)
        //    {
        //        var task = !editedTask.PatientId.IsEmpty() ? scheduleRepository.GetScheduleTask(Current.AgencyId, editedTask.PatientId, editedTask.Id) :
        //            scheduleRepository.GetScheduleTaskByUserId(Current.AgencyId, editedTask.UserId, editedTask.Id);
        //        if (task != null)
        //        {
        //            //TODO Update assets

        //            if (!editedTask.UserId.IsEmpty())
        //            {
        //                task.UserId = editedTask.UserId;
        //            }
        //            task.Comments = editedTask.Comments;
        //            //task.Status = editedTask.Status;
        //            task.EventStartTime = editedTask.EventStartTime;
        //            task.EventEndTime = editedTask.EventEndTime;
        //            task.VisitStartTime = editedTask.VisitStartTime.IsValid() ? editedTask.VisitStartTime : editedTask.EventStartTime;
        //            task.VisitEndTime = editedTask.VisitEndTime.IsValid() ? editedTask.VisitEndTime : editedTask.EventEndTime;
        //            //task.IsBillable = editedTask.IsBillable;
        //            //task.Surcharge = editedTask.Surcharge;
        //            //task.AssociatedMileage = editedTask.AssociatedMileage;
        //            //task.IsMissedVisit = editedTask.IsMissedVisit;
        //            task.IsAllDay = editedTask.IsAllDay;
        //            //task.DisciplineTask = editedTask.DisciplineTask;
        //            //task.PhysicianId = editedTask.PhysicianId;
        //            if (ProcessDetailUpdate(task))
        //            {
        //                if (scheduleRepository.UpdateScheduleTask(task))
        //                {
        //                    result = true;
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public List<PrivateDutyPrintQueueTask> GetPrintQueue(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var events = new List<PrivateDutyPrintQueueTask>();
        //    var scheduleEvents = scheduleRepository.GetPrintQueueTasks(Current.AgencyId, BranchId, StartDate, EndDate);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        var users = new List<User>();
        //        var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //        if (userIds != null && userIds.Count > 0)
        //        {
        //            users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
        //        }
        //        scheduleEvents.ForEach(e =>
        //        {
        //            var user = users.FirstOrDefault(u => u.Id == e.UserId);
        //            events.Add(new PrivateDutyPrintQueueTask
        //            {
        //                Status = e.StatusName,
        //                Patient = e.PatientName,
        //                TaskName = e.DisciplineTaskName,
        //                PrintUrl = UrlFactory<PrivateDutyScheduleTask>.Download(e),
        //                Employee = user != null ? user.DisplayName : string.Empty,
        //                EventDate = e.EventDate,
        //                Id = e.Id,
        //                TimeIn = e.EventStartTime.ToShortTimeString(),
        //                TimeOut = e.EventEndTime.ToShortTimeString()
        //            });
        //        });
        //    }
        //    return events;
        //}

        //protected void ProcessScheduleEventsActions(Guid patientId, List<PrivateDutyScheduleTask > scheduleTasks, PrivateDutyCarePeriod patientEpisode, string discipline)
        //{
        //    if (scheduleTasks != null && scheduleTasks.Count > 0)
        //    {
        //        var userIds = new List<Guid>();
        //        var returnComments = scheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, patientEpisode.Id) ?? new List<ReturnComment>();
        //        if (returnComments != null && returnComments.Count > 0)
        //        {
        //            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //            if (returnUserIds != null && returnUserIds.Count > 0)
        //            {
        //                userIds.AddRange(returnUserIds);
        //            }
        //        }
        //        var scheduleUserIds = scheduleTasks.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //        if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //        {
        //            userIds.AddRange(scheduleUserIds);
        //        }
        //        if (userIds != null && userIds.Count > 0)
        //        {
        //            userIds = userIds.Distinct().ToList();
        //        }
        //        var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
        //        var eventIds = scheduleTasks.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
        //        var missedVisits = scheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);
        //        var detail = new EpisodeDetail();
        //        if (patientEpisode.Details.IsNotNullOrEmpty())
        //        {
        //            detail = patientEpisode.Details.ToObject<EpisodeDetail>();
        //        }
        //        if (scheduleTasks != null && scheduleTasks.Count > 0)
        //        {
        //            scheduleTasks.ForEach(s =>
        //            {
        //                var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList() ?? new List<ReturnComment>();
        //                s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
        //                var hideTask = false;

        //                if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
        //                {
        //                    hideTask = true;

        //                    if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA)
        //                    {
        //                        hideTask = false;
        //                    }
        //                }

        //                if (!hideTask)
        //                {
        //                    if (!s.UserId.IsEmpty())
        //                    {
        //                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //                        if (user != null)
        //                        {
        //                            s.UserName = user.DisplayName;
        //                        }
        //                    }

        //                    s.EpisodeNotes = detail.Comments.Clean();
        //                    if (s.IsMissedVisit)
        //                    {
        //                        var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.Id);
        //                        if (missedVisit != null)
        //                        {
        //                            s.MissedVisitComments = missedVisit.ToString();
        //                            s.Status = missedVisit.Status;
        //                        }
        //                    }
        //                    s.PatientId = patientId;
        //                    s.EndDate = patientEpisode.EndDate;
        //                    s.StartDate = patientEpisode.StartDate;
        //                    UrlFactory<PrivateDutyScheduleTask>.SetNew(s, true, true, true, true);
        //                }
        //            });
        //        }
        //    }
        //}

        //public void CurrentEpisodePatientWithScheduleEvent(PatientScheduleTaskViewData patientScheduleEventViewData, Guid patientId, string discipline)
        //{
        //    if (patientScheduleEventViewData != null)
        //    {
        //        var patientEpisode = episodeRepository.GetCurrentEpisodeLeanNoPatientInfo(Current.AgencyId, patientId);
        //        if (patientEpisode != null)
        //        {
        //            patientScheduleEventViewData.FilterStartDate = patientEpisode.StartDate;
        //            patientScheduleEventViewData.FilterEndDate = patientEpisode.EndDate;
        //            var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, true);
        //            if (patientEvents != null && patientEvents.Count > 0)
        //            {
        //                ProcessScheduleEventsActions(patientId, patientEvents, patientEpisode);
        //                patientScheduleEventViewData.ScheduleEvents = patientEvents;
        //            }

        //        }
        //    }
        //}

        //public PatientScheduleTaskViewData CurrentEpisodePatientWithScheduleEvent(Patient patient, string discipline)
        //{
        //    var patientScheduleEventViewData = new PatientScheduleTaskViewData();
        //    patientScheduleEventViewData.DateFilterType = "ThisEpisode";
        //    if (patient != null)
        //    {
        //        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
        //        if (physician != null)
        //        {
        //            patient.Physician = physician;
        //        }
        //        var emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patient.Id);
        //        if (emergencyContact != null)
        //        {
        //            patient.EmergencyContact = emergencyContact;
        //        }

        //        this.SetInsurance(patient);
        //        patientScheduleEventViewData.Patient = patient;
        //        patientScheduleEventViewData.DisciplineFilterType = discipline;
        //        var patientEpisode = episodeRepository.GetCurrentEpisodeLeanNoPatientInfo(Current.AgencyId, patient.Id);
        //        if (patientEpisode != null)
        //        {
        //            patientScheduleEventViewData.FilterStartDate = patientEpisode.StartDate;
        //            patientScheduleEventViewData.FilterEndDate = patientEpisode.EndDate;
        //            var patientEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patient.Id, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, true);
        //            if (patientEvents != null && patientEvents.Count > 0)
        //            {
        //                //this.ProcessScheduleEventsActions(patient.Id, patientEvents, patientEpisode, discipline);
        //                patientScheduleEventViewData.ScheduleEvents = patientEvents;// this.GetScheduledEventsForCurrentEpisode(patientEpisode, discipline);
        //            }

        //        }
        //    }
        //    return patientScheduleEventViewData;
        //}

        #region Private Methods

        //private bool ProcessNewTask(PrivateDutyScheduleTask task)
        //{
        //    bool result = false;
        //    var discipline = lookupRepository.GetPrivateDutyDisciplineTask(task.DisciplineTask);
        //    if (discipline != null)
        //    {
        //        task.Discipline = discipline.Discipline;
        //        task.IsBillable = discipline.IsBillable;
        //        task.Version = discipline.Version;
        //        if (task.IsNote())
        //        {
        //            task.Status = ((int)ScheduleStatus.NoteNotYetDue);
        //            var patientVisitNote = new PrivateDutyPatientVisitNote { AgencyId = Current.AgencyId, Id = task.Id, PatientId = task.PatientId, NoteType = ((DisciplineTasks)task.DisciplineTask).ToString(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = task.UserId, IsBillable = task.IsBillable };
        //            result = noteRepository.AddVisitNote(patientVisitNote);
        //        }
        //        else if (task.IsAssessment())
        //        {
        //            task.Status = ((int)ScheduleStatus.OasisNotYetDue);
        //            result = true;
        //        }
        //        else if (task.IsOrder())
        //        {
        //            task.Status = ((int)ScheduleStatus.OrderNotYetDue);
        //            if (task.IsPlanOfCare())
        //            {
        //                task.Status = ((int)ScheduleStatus.OrderSaved);
        //                if (task.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)
        //                {
        //                    var planofCare = new PlanofCareStandAlone { Id = task.Id, AgencyId = Current.AgencyId, PatientId = task.PatientId, UserId = task.UserId, Status = task.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
        //                    planofCare.Questions = new List<Question>();
        //                    planofCare.Data = planofCare.Questions.ToXml();
        //                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, task.PatientId);
        //                    if (physician != null)
        //                    {
        //                        planofCare.PhysicianId = physician.Id;
        //                    }
        //                    result = planofCareRepository.Add(planofCare);
        //                }
        //            }
        //            else if (task.IsPhysicianOrder())
        //            {
        //                var order = new PhysicianOrder { Id = task.Id, AgencyId = Current.AgencyId, PatientId = task.PatientId, UserId = task.UserId, OrderDate = task.EventStartTime, Created = DateTime.Now, Text = "", Summary = "", Status = task.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
        //                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, task.PatientId);
        //                if (physician != null)
        //                {
        //                    order.PhysicianId = physician.Id;
        //                }
        //                result = noteRepository.AddOrder(order);
        //            }
        //            else if (task.IsFaceToFaceEncounter())
        //            {
        //                return false;
        //            }
        //            else
        //            {
        //                result = true;
        //            }
        //        }
        //        else if (task.IsReport())
        //        {
        //            task.Status = ((int)ScheduleStatus.ReportAndNotesCreated);
        //            if (task.IsInfectionLog())
        //            {
        //                var infectionLog = new Infection { AgencyId = Current.AgencyId, Id = task.Id, PatientId = task.PatientId, InfectionDate = task.EventStartTime, Created = DateTime.Now, Modified = DateTime.Now, Status = task.Status, UserId = task.UserId };
        //                result = noteRepository.AddInfection(infectionLog);
        //            }
        //            else if (task.IsIncidentLog())
        //            {
        //                var incidentLog = new Incident { AgencyId = Current.AgencyId, Id = task.Id, PatientId = task.PatientId, IncidentDate = task.EventStartTime, Created = DateTime.Now, Modified = DateTime.Now, Status = task.Status, UserId = task.UserId };
        //                result = noteRepository.AddIncident(incidentLog);
        //            }
        //            else if (task.IsCommunicationNote())
        //            {
        //                var comNote = new CommunicationNote { Id = task.Id, PatientId = task.PatientId, AgencyId = Current.AgencyId, UserId = task.UserId, Status = ((int)ScheduleStatus.NoteNotYetDue), Created = task.EventStartTime, Modified = DateTime.Now };
        //                result = noteRepository.AddCommunicationNote(comNote);
        //            }
        //            else
        //            {
        //                result = true;
        //            }
        //        }
        //    }
        //    return result;
        //}

        //private bool ProcessDetailUpdate(PrivateDutyScheduleTask task)
        //{
        //    bool result = false;
        //    var type = ((DisciplineTasks)task.DisciplineTask).ToString();
        //    var discipline = lookupRepository.GetPrivateDutyDisciplineTask(task.DisciplineTask);
        //    if (discipline != null)
        //    {
        //        if (task.IsNote())
        //        {
        //            var visitNote = noteRepository.GetVisitNote(Current.AgencyId, task.PatientId, task.Id);
        //            if (visitNote != null)
        //            {
        //                visitNote.UserId = task.UserId;

        //                //var notesQuestions = visitNote.ToDictionary();
        //                //if (notesQuestions != null)
        //                //{
        //                //if (notesQuestions.ContainsKey("VisitStartTime")) { notesQuestions["VisitStartTime"].Answer = task.VisitStartTime; } else { notesQuestions.Add("VisitStartTime", new NotesQuestion { Answer = task.VisitStartTime.ToJavascriptFormat(), Name = "VisitStartTime", Type = type }); }
        //                //if (notesQuestions.ContainsKey("VisitEndTime")) { notesQuestions["VisitEndTime"].Answer = task.VisitEndTime; } else { notesQuestions.Add("VisitEndTime", new NotesQuestion { Answer = task.VisitEndTime.ToJavascriptFormat(), Name = "TimeIn", Type = type }); }
        //                //if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = task.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = task.Surcharge, Name = "Surcharge", Type = type }); }
        //                //if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = task.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = task.AssociatedMileage, Name = "AssociatedMileage", Type = type }); }

        //                //if (discipline.IsTypeChangeable)
        //                //{
        //                //    visitNote.NoteType = type;
        //                //}

        //                //visitNote.Note = notesQuestions.Values.ToList().ToXml();
        //                //visitNote.IsDeprecated = task.IsDeprecated;

        //                //visitNote.Status = task.Status.IsNotNullOrEmpty() && task.Status.IsInteger() ? task.Status.ToInteger() : visitNote.Status;
        //                result = noteRepository.UpdateVisitNote(visitNote);
        //                //}
        //                //else
        //                //{
        //                //    result = true;
        //                //}
        //            }
        //            else
        //            {
        //                result = true;
        //            }

        //        }
        //        else if (task.IsAssessment())
        //        {

        //        }
        //        else if (task.IsOrder())
        //        {
        //            if (task.IsPlanOfCare())
        //            {

        //            }
        //            else if (task.IsPhysicianOrder())
        //            {
        //                var physicianOrder = noteRepository.GetPhysicianOrderOnly(task.Id, task.PatientId, Current.AgencyId);
        //                if (physicianOrder != null)
        //                {
        //                    physicianOrder.Status = task.Status;
        //                    if (task.VisitStartTime.IsValid())
        //                    {
        //                        physicianOrder.OrderDate = task.VisitStartTime;
        //                    }
        //                    if (physicianOrder.Status != task.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !task.PhysicianId.IsEmpty())
        //                    {
        //                        var physician = PhysicianEngine.Get(task.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            physicianOrder.PhysicianData = physician.ToXml();
        //                        }
        //                    }
        //                    physicianOrder.PhysicianId = task.PhysicianId;
        //                    physicianOrder.IsDeprecated = task.IsDeprecated;
        //                    result = noteRepository.UpdateOrderModel(physicianOrder);
        //                }
        //            }
        //            else if (task.IsFaceToFaceEncounter())
        //            {
        //                return false;
        //            }
        //        }
        //        else if (task.IsReport())
        //        {
        //            if (task.IsInfectionLog())
        //            {
        //                var infectionReport = noteRepository.GetInfectionReport(Current.AgencyId, task.Id);
        //                if (infectionReport != null)
        //                {
        //                    if (task.VisitStartTime.IsValid())
        //                    {
        //                        infectionReport.InfectionDate = task.VisitStartTime;
        //                    }
        //                    infectionReport.IsDeprecated = task.IsDeprecated;
        //                    infectionReport.Status = task.Status;
        //                    result = noteRepository.UpdateInfectionModal(infectionReport);
        //                }
        //            }
        //            else if (task.IsIncidentLog())
        //            {
        //                var accidentReport = noteRepository.GetIncidentReport(Current.AgencyId, task.Id);
        //                if (accidentReport != null)
        //                {
        //                    if (task.VisitStartTime.IsValid())
        //                    {
        //                        accidentReport.IncidentDate = task.VisitStartTime;
        //                    }
        //                    accidentReport.IsDeprecated = task.IsDeprecated;
        //                    accidentReport.Status = task.Status;
        //                    result = noteRepository.UpdateIncidentModal(accidentReport);
        //                }
        //            }
        //            else if (task.IsCommunicationNote())
        //            {
        //                var comNote = noteRepository.GetCommunicationNote(task.Id, task.PatientId, Current.AgencyId);
        //                if (comNote != null)
        //                {
        //                    comNote.IsDeprecated = task.IsDeprecated;
        //                    comNote.Status = task.Status;
        //                    if (task.VisitStartTime.IsValid())
        //                    {
        //                        comNote.Created = task.VisitStartTime;
        //                    }
        //                    if (comNote.Status != task.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
        //                    {
        //                        var physician = PhysicianEngine.Get(comNote.PhysicianId, Current.AgencyId);
        //                        if (physician != null)
        //                        {
        //                            comNote.PhysicianData = physician.ToXml();
        //                        }
        //                    }
        //                    result = noteRepository.UpdateCommunicationNoteModal(comNote);
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        #endregion

        protected override string TableName(int task)
        {
            var result = string.Empty;
            PrivateDutyScheduleTask scheduleTask = new PrivateDutyScheduleTask() { DisciplineTask = task };
            if (scheduleTask.IsNote())
            {
                result = "privateduty.patientvisitnotes";
            }
            else if (DisciplineTaskFactory.AllAssessments(true).Contains(task))
            {
                result = "privateduty.assessments";
            }
            else
            {
                switch (task)
                {
                    case (int)DisciplineTasks.PhysicianOrder:
                        result = "privateduty.physicianorders";
                        break;
                    case (int)DisciplineTasks.HCFA485:
                    case (int)DisciplineTasks.NonOasisHCFA485:
                        result = "privateduty.planofcares";
                        break;
                    case (int)DisciplineTasks.HCFA485StandAlone:
                        result = "privateduty.planofcares";
                        break;
                    case (int)DisciplineTasks.IncidentAccidentReport:
                        result = "privateduty.incidents";
                        break;
                    case (int)DisciplineTasks.InfectionReport:
                        result = "privateduty.infections";
                        break;
                    case (int)DisciplineTasks.CommunicationNote:
                        result = "privateduty.communicationnotes";
                        break;
                }
            }
            return result;
        }

        //protected override bool IsUserAvailable(Guid userId, DateTime from, DateTime to, out string message)
        //{
        //    message = "";
        //    var scheduleTasks = scheduleRepository.GetScheduleTasksBetweenDates(Current.AgencyId, Guid.Empty, userId, from, to, false);
        //    if (scheduleTasks.IsNotNullOrEmpty())
        //    {
        //        var task = scheduleTasks.FirstOrDefault() ?? new PrivateDutyScheduleTask();
        //        message = GenerateUserAvailablilityMessage(task.PatientId);
        //        return false;
        //    }
        //    return true;
        //}

        protected override void SetScheduleTasksDefaultAppSpecific(PrivateDutyScheduleTask ev)
        {
            ev.VisitStartTime = ev.EventStartTime;
            ev.VisitEndTime = ev.EventEndTime;
        }

        protected override void SetScheduleTasksDefaultAppSpecific(GridTask task, PrivateDutyScheduleTask ev)
        {
            task.DateIn = ev.EventStartTime;
            task.DateOut = ev.EventEndTime;
            task.IsAllDay = ev.IsAllDay;
        }
    }
}
