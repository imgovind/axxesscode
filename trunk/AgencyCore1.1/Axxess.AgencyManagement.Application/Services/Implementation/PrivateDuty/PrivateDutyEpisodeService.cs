﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    public class PrivateDutyEpisodeService : EpisodeService<PrivateDutyCarePeriod>
    {
        private PrivateDutyEpisodeRepository episodeRepository;
        public PrivateDutyEpisodeService(PrivateDutyDataProvider dataProvider) :
            base(dataProvider.EpisodeRepository)
        {
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.patientProfileRepository = dataProvider.PatientProfileRepository;
            this.patientAdmissionRepository = dataProvider.PatientAdmissionRepository;
            this.Service = AgencyServices.PrivateDuty;
        }

        public bool IsEpisodeExist(Guid episodeId)
        {
            return episodeRepository.IsEpisodeExist(Current.AgencyId, episodeId);
        }

        public override List<Validation> EpisodeValidation(Patient patient, PrivateDutyCarePeriod patientEpisode)
        {
            var validationRules = new List<Validation>();
            validationRules.Add(new Validation(() => patient == null, "Patient could not be found. Close window and try again."));
            if (patient != null)
            {
                var admissionData = patientAdmissionRepository.GetPatientAdmissionDate(Current.AgencyId,patient.Id, patientEpisode.AdmissionId);
                validationRules.Add(new Validation(() => admissionData == null, "Admission data is not available."));
            }
            //validationRules.Add(new Validation(() => patientEpisode.Detail.PrimaryInsurance.IsEqual("0"), "Insurance is required."));
            validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Start Date is not valid date."));
            validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "End Date is not valid date."));
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Start Date must be before the End Date."));
            }
            const string InvalidDateRangeMessage = "The Start Date or End Date overlap with another Care Period.";
            validationRules.Add(new Validation(() => episodeRepository.IsEpisodeExist(Current.AgencyId, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate, patientEpisode.Id), InvalidDateRangeMessage));
            //if (patientEpisode.Id.IsEmpty())
            //{
            //    validationRules.Add(new Validation(() => !IsValidEpisode(patient.Id, patientEpisode.StartDate, patientEpisode.EndDate), InvalidDateRangeMessage));
            //}
            //else
            //{
            //    validationRules.Add(new Validation(() => !IsValidEpisode(patientEpisode.Id, patient.Id, patientEpisode.StartDate, patientEpisode.EndDate), InvalidDateRangeMessage));
            //}
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Start Date must be after the Start of Care Date."));
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "End Date must be after the Start of Care Date."));
            }
            return validationRules;
        }

        protected override string GetPartialMessageType()
        {
            return "Care Period";
        }
    }
}
