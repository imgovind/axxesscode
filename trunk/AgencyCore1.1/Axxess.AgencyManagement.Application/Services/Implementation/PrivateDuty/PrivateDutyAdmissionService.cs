﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.AgencyManagement.Repositories;

namespace Axxess.AgencyManagement.Application.Services
{
    public class PrivateDutyAdmissionService : AdmissionServiceAbstract
    {
        public PrivateDutyAdmissionService(PrivateDutyDataProvider dataProvider)
            : base(dataProvider.PatientRepository, dataProvider.PatientProfileRepository, dataProvider.PatientAdmissionRepository)
        {
        }
    }
}