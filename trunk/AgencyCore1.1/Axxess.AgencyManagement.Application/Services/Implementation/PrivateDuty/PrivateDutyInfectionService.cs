﻿namespace Axxess.AgencyManagement.Application.Services
{
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Enums;
    public class PrivateDutyInfectionService : InfectionService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
        private PrivateDutyTaskRepository scheduleRepository;
        private PrivateDutyInfectionRepository noteRepository;
        private PrivateDutyEpisodeRepository episodeRepository;

        public PrivateDutyInfectionService(PrivateDutyDataProvider dataProvider) :
            base(dataProvider.TaskRepository, dataProvider.InfectionRepository, dataProvider.EpisodeRepository)
        {
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            this.profileRepository = dataProvider.PatientProfileRepository; 
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.InfectionRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.PrivateDuty;
        }
        protected override void GetInfectionEpisodeData(Infection infection)
        {
        }
    }
}
