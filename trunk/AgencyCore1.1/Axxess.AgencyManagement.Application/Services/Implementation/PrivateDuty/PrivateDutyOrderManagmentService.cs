﻿using System;
using System.Collections.Generic;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Enums;
using Axxess.AgencyManagement.Application.Domain;
using Axxess.AgencyManagement.Application.Enums;

namespace Axxess.AgencyManagement.Application.Services
{
    public class PrivateDutyOrderManagmentService : OrderManagmentService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
        private readonly PrivateDutyEpisodeService episodeService;
        private readonly PrivateDutyTaskService scheduleService;
        public PrivateDutyOrderManagmentService(
            IAgencyService agencyService,
            PrivateDutyTaskService scheduleService,
            PrivateDutyPhysicianOrderService physicianOrderService,
            PrivateDutyPlanOfCareService planOfCareService,
            PrivateDutyNoteService noteService,
            PrivateDutyEpisodeService epiosdeService)
            : base(agencyService, physicianOrderService, scheduleService, planOfCareService, noteService)
        {
            this.episodeService = epiosdeService;
            this.scheduleService = scheduleService;
            base.Service = AgencyServices.PrivateDuty;
        }

        protected override Order GetOrderForHistoryEditAppSpecific(int typeNum,Guid id)
        {
            return new Order();
        }

        protected override Order GetOrderForReceivingAppSpecific(int typeNum, Guid patientId, Guid id)
        {
            return new Order();
        }

        protected override int MarkOrdersAsSentAppSpecific(int type, bool sendElectronically, string[] answers, ref List<AgencyPhysician> physicians)
        {
            return 0;
        }

        protected override void GetOrderStatusAppSpecific(bool isPending, bool isCompleted, List<Order> orders, List<PrivateDutyScheduleTask> schedules)
        {
        }

        protected override void GetPatientOrdersAppSpecific(Guid patientId, List<Order> orders, List<PrivateDutyScheduleTask> schedules)
        {
        }

        protected override bool MarkOrderAsReturnedAppSpecifc(Guid id, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            return false;
        }

        protected override bool UpdateOrderDatesAppSpecifc(OrderType type, Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            return false;
        }

    }
}
