﻿using System;
using System.Collections.Generic;
using System.Linq;
using Axxess.AgencyManagement.Repositories;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Application.Helpers;
using Axxess.Core;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.Services
{
    public class PrivateDutyPatientProfileService : PatientProfileService
    {
        private readonly PrivateDutyEpisodeRepository episodeRepository;
        private readonly PrivateDutyMongoRepository mongoRepository;
        private readonly PrivateDutyPatientProfileRepository patientProfileRepository;
        public PrivateDutyPatientProfileService(PrivateDutyDataProvider dataProvider):
            base(dataProvider.PatientProfileRepository)
        {
            this.patientProfileRepository = dataProvider.PatientProfileRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.referralRepository = dataProvider.ReferralRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.drugService = Container.Resolve<IDrugService>();
            base.Service = AgencyServices.PrivateDuty;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.mongoRepository = dataProvider.MongoRepository;
            base.patientAdmissionRepository = dataProvider.PatientAdmissionRepository;
        }

        protected override void SetStatus(Patient patient, Profile profile)
        {
            patient.PrivateDutyStatus = profile.Status;
        }

        protected override int GetStatus(Patient patient)
        {
            return patient.PrivateDutyStatus;
        }

        protected override bool AddPatientAdmissionDateAppSpecific(PatientAdmissionDate admissionData)
        {
            return AdmissionHelperFactory<PrivateDutyCarePeriod>.AddPatientAdmissionDate(admissionData);
        }

        protected override bool UpdatePatientAdmissionDateAppSpecific(PatientAdmissionDate admissionData)
        {
            return AdmissionHelperFactory<PrivateDutyCarePeriod>.UpdatePatientAdmissionDate(admissionData);
        }

        protected override PatientAdmissionDate GetPatientAdmissionDateAppSpecific(Guid patientId, Guid admissionDataId)
        {
            return AdmissionHelperFactory<PrivateDutyCarePeriod>.GetPatientAdmissionDate(patientId, admissionDataId);
        }

        protected override void EditValidateAppSpecific(Patient patient, Profile privateDutyProfile, List<Validation> rules)
        {
            rules.Add(new Validation(() => string.IsNullOrEmpty(privateDutyProfile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
            rules.Add(new Validation(() => !privateDutyProfile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
            if (privateDutyProfile.Status == (int)PatientStatus.Discharged)
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(privateDutyProfile.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !privateDutyProfile.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
            }
            if (privateDutyProfile.PrimaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
            }
            if (privateDutyProfile.SecondaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
            }

            if (privateDutyProfile.TertiaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
            }
        }

        protected override void EditValidateAppSpecific(Profile privateDutyProfile, List<Validation> rules)
        {
            rules.Add(new Validation(() => string.IsNullOrEmpty(privateDutyProfile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
            rules.Add(new Validation(() => !privateDutyProfile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
            if (privateDutyProfile.Status == (int)PatientStatus.Discharged)
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(privateDutyProfile.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !privateDutyProfile.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
            }
            if (privateDutyProfile.PrimaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
            }
            if (privateDutyProfile.SecondaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
            }

            if (privateDutyProfile.TertiaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
            }
        }

        protected override void UpdateEpisodeAndClaimsForDischargePatientAppSpecific(Guid patientId, DateTime dischargeDate, Guid episodeId)
        {
           
        }

        protected override Axxess.Core.DateRange GetEpisodeDateRange(Guid patientId, Guid episodeId)
        {
            var dateRange = new DateRange();
            if (!patientId.IsEmpty())
            {
                if (!patientId.IsEmpty())
                {
                    var episode = episodeRepository.GetEpisodeDateRange(Current.AgencyId, patientId, episodeId);
                    if (episode != null)
                    {
                        dateRange.StartDate = episode.StartDate;
                        dateRange.EndDate = episode.EndDate;
                    }
                }
            }
            return dateRange;
        }

        public PatientBillRate GetPatientVisitRate(Guid patientId)
        {
            return mongoRepository.GetPatientVisitRate(Current.AgencyId, patientId);
        }

        public List<ChargeRate> GetPatientVisitRates(Guid patientId)
        {
            return mongoRepository.GetPatientVisitRates(Current.AgencyId,patientId);
        }

        public ChargeRate GetPatientVisitRateForEdit(Guid patientId, int disciplineTaskId)
        {
            var rates = GetPatientVisitRates(patientId);
            if (rates.IsNotNullOrEmpty())
            {
                return rates.SingleOrDefault(r => r.Id == disciplineTaskId);
            }
            return new ChargeRate();
        }

        public NewBillDataViewData GetPatientVisitRateForNew(Guid patientId)
        {
            var viewData = new NewBillDataViewData { PatientId = patientId };
            viewData.Service = this.Service;
            var rates = GetPatientVisitRates(patientId);
            if (rates.IsNotNullOrEmpty())
            {
                viewData.ExistingNotes= rates.Select(r => r.Id).ToList();
            }
            return viewData;
        }


        public JsonViewData AddChargeRate(Guid patientId, ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem happened trying to add this patient visit rate. Please try again." };
            if (chargeRate.IsValid())
            {
                var billRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, patientId);
                if (billRate != null)
                {
                    var rates = billRate.VisitRates ?? new List<ChargeRate>();
                    if (rates.Exists(r => r.Id == chargeRate.Id))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Patient visit rate already exist.";
                    }
                    else
                    {
                        rates.Add(chargeRate);
                        billRate.VisitRates = rates;
                        if (mongoRepository.UpdatePatientVisitRates(billRate))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Patient visit rate saved successfully";
                        }
                    }
                }
                else
                {
                    viewData = AddPatientBillData(patientId, new List<ChargeRate>() { chargeRate });
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = chargeRate.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateChargeRate(Guid patientId,  ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem happened trying to update this patient visit rate. Please try again." };
           
             var billRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, patientId);
             if (billRate != null)
             {
                 var rates = billRate.VisitRates ?? new List<ChargeRate>();
                 if (chargeRate.IsValid())
                 {
                     var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                     if (rate != null)
                     {
                         rate.PreferredDescription = chargeRate.PreferredDescription;
                         rate.Code = chargeRate.Code;
                         rate.RevenueCode = chargeRate.RevenueCode;
                         rate.Charge = chargeRate.Charge;
                         rate.ExpectedRate = chargeRate.ExpectedRate;
                         rate.Modifier = chargeRate.Modifier;
                         rate.Modifier2 = chargeRate.Modifier2;
                         rate.Modifier3 = chargeRate.Modifier3;
                         rate.Modifier4 = chargeRate.Modifier4;
                         rate.ChargeType = chargeRate.ChargeType;
                         rate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                         if (rate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || rate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                         {
                             if (chargeRate.IsTimeLimit)
                             {
                                 rate.TimeLimitHour = chargeRate.TimeLimitHour;
                                 rate.TimeLimitMin = chargeRate.TimeLimitMin;
                                 rate.SecondDescription = chargeRate.SecondDescription;
                                 rate.SecondCode = chargeRate.SecondCode;
                                 rate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                 rate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                 if (chargeRate.IsSecondChargeDifferent)
                                 {
                                     rate.SecondCharge = chargeRate.SecondCharge;
                                 }
                                 else
                                 {
                                     rate.SecondCharge = 0;
                                 }
                                 rate.SecondModifier = chargeRate.SecondModifier;
                                 rate.SecondModifier2 = chargeRate.SecondModifier2;
                                 rate.SecondModifier3 = chargeRate.SecondModifier3;
                                 rate.SecondModifier4 = chargeRate.SecondModifier4;
                                 rate.SecondChargeType = chargeRate.SecondChargeType;
                                 if (rate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                                 {
                                     rate.SecondUnit = chargeRate.SecondUnit;
                                 }
                                 else
                                 {
                                     rate.SecondUnit = 0;
                                 }
                                 rate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                             }
                             rate.IsTimeLimit = chargeRate.IsTimeLimit;
                         }
                         else if (rate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                         {
                             rate.Unit = chargeRate.Unit;
                         }
                         if (mongoRepository.UpdatePatientVisitRates(billRate))
                         {
                             viewData.isSuccessful = true;
                             viewData.errorMessage = "Insurance rate updated successfully";
                         }
                     }
                 }
                 else
                 {
                     viewData.isSuccessful = false;
                     viewData.errorMessage = chargeRate.ValidationMessage;
                 }
             }
             else
             {
                 viewData = AddPatientBillData(patientId, new List<ChargeRate>() { chargeRate });
             }
            return viewData;
        }

        public JsonViewData DeleteChargeRate(Guid patientId, int disciplineTaskId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem happened trying to delete this patient visit rate. Please try again." };
              var billRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, patientId);
              if (billRate != null)
              {
                  var rates = billRate.VisitRates ?? new List<ChargeRate>();
                  if (rates.IsNotNullOrEmpty())
                  {
                      var removed = rates.RemoveAll(r => r.Id == disciplineTaskId);
                      if (removed > 0)
                      {
                          if (mongoRepository.UpdatePatientVisitRates(billRate))
                          {
                              viewData.isSuccessful = true;
                              viewData.errorMessage = "Patient visit rate deleted successfully";
                          }
                      }
                  }
              }
            return viewData;
        }

        public JsonViewData ReplaceVisitRates(Guid patientId, int replacedId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem occured while replacing the patient's visit information." };
            var selectedInsurance = agencyRepository.FindInsurance(Current.AgencyId, replacedId);
            if (selectedInsurance != null)
            {
                var rates = selectedInsurance.BillData.ToObject<List<ChargeRate>>();
                if (rates.IsNotNullOrEmpty())
                {
                    var billRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, patientId);
                    if (billRate != null)
                    {
                        if (selectedInsurance.BillData.IsNotNullOrEmpty())
                        {
                            billRate.VisitRates = rates;
                            if (mongoRepository.UpdatePatientVisitRates(billRate))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance was edited successfully";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Selected insurance rate information is empty.";
                        }
                    }
                    else
                    {
                        viewData = AddPatientBillData(patientId, rates);
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance does not have visit rates.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected insurance does not exist.";
            }

            return viewData;
        }

        private JsonViewData AddPatientBillData(Guid patientId, List<ChargeRate> chargeRates)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem happened trying to update this patient visit rate. Please try again." };
            var patientBillData = new PatientBillRate
            {
                AgencyId = Current.AgencyId,
                PatientId = patientId,
                VisitRates = chargeRates,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsDeprecated = false
            };
            if (mongoRepository.AddPatientVisitRate(patientBillData))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Insurance rate added successfully";
            }
            return viewData;
        }

        public IList<PrivatePayor> GetPatientBillingInformations(Guid patientId)
        {
            return patientProfileRepository.GetPatientBillingInformations(Current.AgencyId, patientId);
        }


        public JsonViewData AddPayor(PrivatePayor payor)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem happened trying to add this patient payor information. Please try again." };
            if (payor != null)
            {
                if (!payor.PatientId.IsEmpty())
                {
                    payor.Encode();
                    if (payor.IsValid())
                    {
                        payor.AgencyId = Current.AgencyId;
                        payor.Created = DateTime.Now;
                        payor.Modified = DateTime.Now;
                        if (patientProfileRepository.AddPayor(payor))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The payor information  saved successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "A problem happened trying to add this patient payor information. Please try again.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = payor.ValidationMessage;
                    }
                }
                else
                {
                    viewData.errorMessage = "Patient is not identified.";
                }
            }
            else
            {
                viewData.errorMessage = "The payor information sent is empty. Please try again.";
            }
            return viewData;
        }

        public PrivatePayor GetPayorOnly(Guid patientId, int Id)
        {
            return patientProfileRepository.GetPayorOnly(Current.AgencyId, patientId, Id);
        }

        public JsonViewData UpdatePayor(PrivatePayor payor)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem happened trying to edit this patient payor information. Please try again." };
            if (payor != null)
            {
                if (!payor.PatientId.IsEmpty())
                {
                    if (payor.Id > 0)
                    {
                        payor.Encode();
                        if (payor.IsValid())
                        {
                            var payorToEdit = patientProfileRepository.GetPayorOnly(Current.AgencyId, payor.PatientId, payor.Id);
                            if (payorToEdit != null)
                            {
                                EntityHelper.SetPatientPrivatePayor(payorToEdit, payor);
                                if (patientProfileRepository.UpdatePayorModal(payorToEdit))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "The payor information  saved successfully.";
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "A problem happened trying to add this patient payor information. Please try again.";
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "The payor information  to edit is not found. Please try again.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = payor.ValidationMessage;
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "The payor information is not identified.";
                    }
                }
                else
                {
                    viewData.errorMessage = "Patient is not identified.";
                }
            }
            else
            {
                viewData.errorMessage = "The payor information sent is empty. Please try again.";
            }

            return viewData;
        }

        public JsonViewData TogglePayor(Guid patientId, int Id, bool isDeprecated)
        {
            var action = isDeprecated ? "delete" : "restore";
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("A problem happened trying to {0} this patient payor information. Please try again.", action) };
            if (patientProfileRepository.TogglePayor(Current.AgencyId, patientId, Id, isDeprecated))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = string.Format("The payor information  {0} successfully.", isDeprecated ? "deleted" : "restored");
            }
            return viewData;
        }

        protected override List<PatientSelection> GetPatientSelectionAppSpecific(List<Guid> branchIds, byte statusId)
        {
            return null;
        }
    }
}
