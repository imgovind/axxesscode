﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;

    public class PrivateDutyCommunicationNoteService : CommunicationNoteService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
         private PrivateDutyTaskRepository scheduleRepository;
         private PrivateDutyCommunicationNoteRepository noteRepository;
         private PrivateDutyEpisodeRepository episodeRepository;

         public PrivateDutyCommunicationNoteService(PrivateDutyDataProvider dataProvider) :
             base(dataProvider.TaskRepository, dataProvider.CommunicationNoteRepository, dataProvider.EpisodeRepository)
        {
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;

            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.CommunicationNoteRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.PrivateDuty;
        }

         protected override CommunicationNote GetCommunicationNoteEpisodeData(CommunicationNote communicationNote)
         {
             if (!communicationNote.EpisodeId.IsEmpty())
             {
                 var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId);
                 if (episode != null)
                 {
                     communicationNote.EpisodeEndDate = episode.EndDateFormatted;
                     communicationNote.EpisodeStartDate = episode.StartDateFormatted;
                 }
             }
             return communicationNote;
         }
    }
}
