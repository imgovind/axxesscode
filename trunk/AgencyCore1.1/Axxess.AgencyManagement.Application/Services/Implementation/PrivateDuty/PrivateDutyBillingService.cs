﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Application.ViewData;

    using Axxess.AgencyManagement.Repositories;

    public class PrivateDutyBillingService : BillingServiceAbstract<PrivateDutyScheduleTask>
    {

        # region Constructor
        private readonly PrivateDutyEpisodeRepository episodeRepository;
        private readonly PrivateDutyBillingRepository billingRepository;
        private readonly PrivateDutyTaskRepository scheduleRepository;
        private readonly PrivateDutyPatientProfileRepository patientProfileRepository;
        private readonly PrivateDutyMongoRepository mongoRepository;

        public PrivateDutyBillingService(PrivateDutyDataProvider dataProvider, ILookUpDataProvider lookUpDataProvider)
            : base(dataProvider.TaskRepository, dataProvider.BillingRepository, dataProvider.PatientProfileRepository, dataProvider.MongoRepository)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            this.patientProfileRepository = dataProvider.PatientProfileRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.lookUpRepository = lookUpDataProvider.LookUpRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.mongoRepository = dataProvider.MongoRepository;
            this.Service = AgencyServices.PrivateDuty;
        }

        #endregion

        protected override void SetAdmissionInfoAppSpecific(ManagedClaim managedClaim, Patient patientWithProfile)
        {
            this.SetAdmissionInfo<PrivateDutyCarePeriod>(managedClaim, patientWithProfile);
        }

        protected override void SetManagedClaimWithAssessmentInfoAppSpecific(ManagedClaim managedClaim)
        {

        }

        protected override List<Claim> GetUnProcessedAppSpecific(ClaimTypeSubCategory claimType, Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded)
        {
            throw new NotImplementedException();
            //var unProcessedClaims = new List<Claim>();
            //switch (claimType)
            //{

            //}
            //return unProcessedClaims;
        }

        protected override List<Supply> GetSuppliesByFlagAppSpecific(Guid Id, Guid patientId, string Type, bool isBillable)
        {
            throw new NotImplementedException();
        }

        protected override Bill ClaimToGenerateAppSpecific(List<Guid> claimSelected, Guid branchId, int primaryInsurance, string type)
        {
            return null;
        }

        protected override BillingJsonViewData GenerateDirectlyAppSpecific(List<Guid> claimToGenerate, Guid branchId, int insuranceId, string claimType)
        {
            throw new NotImplementedException();
        }

        protected override object GenerateDownloadAppSpecific(List<Guid> claimToGenerate, Guid branchId, int insuranceId, string claimType)
        {
            throw new NotImplementedException();
        }

        protected override bool VerifyInfoAppSpecific<C>(BaseClaim currentClaimFomDB, BaseClaim claimFromClient)
        {
            var save = false;
            if (claimFromClient.PayorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                var payorBillingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claimFromClient.Id);
                if (payorBillingAddress != null)
                {
                    bool isPrivatePayorDifferent = (claimFromClient.PrivatePayorId != currentClaimFomDB.PrivatePayorId) || (claimFromClient.PayorType != currentClaimFomDB.PayorType) || (claimFromClient.IsBillingAddressDifferent != currentClaimFomDB.IsBillingAddressDifferent);
                    save = UpdateOrAddBillingAddressHelper(claimFromClient, payorBillingAddress, isPrivatePayorDifferent, !payorBillingAddress.Rates.IsNotNullOrEmpty());
                }
                else
                {
                    payorBillingAddress = new ManagedClaimBillingAddress { AgencyId = Current.AgencyId, PatientId = claimFromClient.PatientId, Id = claimFromClient.Id };
                    save = UpdateOrAddBillingAddressHelper(claimFromClient, payorBillingAddress, true,true);
                }
                claimFromClient.PrimaryInsuranceId = 18;
            }
            else
            {
                save = VerifyInfoInsuranceHelper<C>(currentClaimFomDB, claimFromClient);
            }
            return save;
        }

        private bool UpdateOrAddBillingAddressHelper(BaseClaim claim, ManagedClaimBillingAddress payorBillingAddress, bool IsPrivatePayorDifferentOrNeedReload, bool IsRateNeedReload)
        {
            var isChangeNeeded = false;
            if (claim.IsBillingAddressDifferent)
            {
                if (payorBillingAddress.Payor == null || IsPrivatePayorDifferentOrNeedReload)
                {
                    var payor = patientProfileRepository.GetPayorOnly(Current.AgencyId, claim.PatientId, claim.PrivatePayorId);
                    if (payor != null)
                    {
                        isChangeNeeded = true;
                        payorBillingAddress.Payor = payor;
                    }
                }
            }
            if (IsRateNeedReload)
            {
                var patientVisitRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, claim.PatientId);
                if (patientVisitRate != null)
                {
                    isChangeNeeded = true;
                    payorBillingAddress.Rates = patientVisitRate.VisitRates;
                }
            }
            return isChangeNeeded ? mongoRepository.UpdateManagedClaimBillingAddress(payorBillingAddress) : true;
        }

        protected override void ClaimWithInsuranceAppSpecific<C>(BaseClaim claim, bool IsSetLocatorFromLocationOrInsurance, Guid optionalLocatonId)
        {
            if (claim.PayorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                CliamWithPrivatePayRatesAndAddress(claim);
            }
            else if (claim.PayorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                ClaimWithInsuranceAppSpecificHelper<C>(claim,IsSetLocatorFromLocationOrInsurance, optionalLocatonId);
            }
        }

        private void CliamWithPrivatePayRatesAndAddress(BaseClaim claim)
        {
            var payorBillingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claim.Id);
            if (payorBillingAddress != null)
            {
                claim.VisitRates = payorBillingAddress.Rates;
                claim.PrivatePayor = payorBillingAddress.Payor;
            }
            else
            {
                var claimAddressWithRate = new ManagedClaimBillingAddress { AgencyId = Current.AgencyId, PatientId = claim.PatientId, Id = claim.Id };
                var patientRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, claim.PatientId);
                if (patientRate != null)
                {
                    claimAddressWithRate.Rates = patientRate.VisitRates;
                }
                if (claim.IsBillingAddressDifferent)
                {
                    claimAddressWithRate.Payor = patientProfileRepository.GetPayorOnly(Current.AgencyId, claim.PatientId, claim.PrivatePayorId);
                }
                if (mongoRepository.AddManagedClaimBillingAddress(claimAddressWithRate))
                {
                    claim.VisitRates = claimAddressWithRate.Rates;
                    claim.PrivatePayor = claimAddressWithRate.Payor;
                }
            }
        }

        protected override bool VerifyInsuranceAppSpecific(BaseClaim currentClaimFomDB, BaseClaim claimFromClient)
        {
            var result = false;
            if (currentClaimFomDB.PayorType == (int)PayorTypeMainCategory.PrivatePayor && currentClaimFomDB.IsBillingAddressDifferent)
            {
                var payorBillingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claimFromClient.Id);
                if (payorBillingAddress != null)
                {
                    if (payorBillingAddress.Payor == null)
                    {
                        payorBillingAddress.Payor = new PrivatePayor { AgencyId = Current.AgencyId, PatientId = claimFromClient.PatientId, Id = claimFromClient.PrivatePayor.Id };
                    }
                    result = ManagedVerifyInsuranceAppSpecificHelper(claimFromClient, payorBillingAddress);
                }
                else
                {
                    payorBillingAddress = new ManagedClaimBillingAddress { AgencyId = Current.AgencyId, PatientId = claimFromClient.PatientId, Id = claimFromClient.Id };
                    result = ManagedVerifyInsuranceAppSpecificHelper(claimFromClient, payorBillingAddress);
                }
            }
            else
            {
                result = true;
            }
            return result;
        }


        protected override bool UpdateOrAddPayorForClaimAppSpecific<C>(BaseClaim claim)
        {
            var result = false;
            if (claim.PayorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                var payorBillingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claim.Id);
                if (payorBillingAddress != null)
                {
                    result = UpdateOrAddBillingAddressHelper(claim, payorBillingAddress, true, true);
                }
                else
                {
                    payorBillingAddress = new ManagedClaimBillingAddress { AgencyId = Current.AgencyId, PatientId = claim.PatientId, Id = claim.Id };
                    result = UpdateOrAddBillingAddressHelper(claim, payorBillingAddress, true, true);
                }
            }
            else if (claim.PayorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                return UpdateOrAddInsuranceForClaim<C>(claim);
            }
            return result;
        }

        private bool ManagedVerifyInsuranceAppSpecificHelper(BaseClaim claimFromClient, ManagedClaimBillingAddress payorBillingAddress)
        {
            try
            {
                payorBillingAddress.Payor.FirstName = claimFromClient.PrivatePayor.FirstName;
                payorBillingAddress.Payor.LastName = claimFromClient.PrivatePayor.LastName;
                payorBillingAddress.Payor.MI = claimFromClient.PrivatePayor.MI;
                payorBillingAddress.Payor.AddressLine1 = claimFromClient.PrivatePayor.AddressLine1;
                payorBillingAddress.Payor.AddressLine2 = claimFromClient.PrivatePayor.AddressLine2;
                payorBillingAddress.Payor.AddressCity = claimFromClient.PrivatePayor.AddressCity;
                payorBillingAddress.Payor.AddressStateCode = claimFromClient.PrivatePayor.AddressStateCode;
                payorBillingAddress.Payor.AddressZipCode = claimFromClient.PrivatePayor.AddressZipCode;
                payorBillingAddress.Payor.Relationship = claimFromClient.PrivatePayor.Relationship;
                payorBillingAddress.Payor.OtherRelationship = claimFromClient.PrivatePayor.OtherRelationship;
                claimFromClient.PrivatePayor.Encode();
                payorBillingAddress.Payor.PhoneHome = claimFromClient.PrivatePayor.PhoneHome;
                payorBillingAddress.Payor.PhoneMobile = claimFromClient.PrivatePayor.PhoneMobile;
                payorBillingAddress.Payor.FaxNumber = claimFromClient.PrivatePayor.FaxNumber;
                payorBillingAddress.Payor.EmailAddress = claimFromClient.PrivatePayor.EmailAddress;

                return mongoRepository.UpdateManagedClaimBillingAddress(payorBillingAddress);
            }
            catch (Exception)
            {
                return false;
            }


        }

        protected override List<ChargeRate> ClaimInsuranceRatesAppSpecific<C>(Guid claimId, int payorType)
        {
            var rates = new List<ChargeRate>();
            if (payorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                var privatePayor = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claimId);
                if (privatePayor != null)
                {
                    rates = privatePayor.Rates;
                }
            }
            else if (payorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                rates = mongoRepository.GetClaimInsuranceRates<C>(Current.AgencyId, claimId);
            }
            return rates;
        }

        public bool ManagedClaimAddPrivatePayorBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            var result = false;
            var billingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, ClaimId);
            if (billingAddress != null)
            {
                var rates = new List<ChargeRate>();
                //rates = billingAddress.Insurance.BillData.ToObject<List<ChargeRate>>();
                if (billingAddress.Rates.IsNotNullOrEmpty())
                {
                    if (billingAddress.Rates.Exists(r => r.Id == chargeRate.Id))
                    {
                        billingAddress.Rates.RemoveAll(r => r.Id == chargeRate.Id);

                    }
                    billingAddress.Rates.Add(chargeRate);
                }
                else
                {
                    billingAddress.Rates = new List<ChargeRate> { chargeRate };
                }
                result = mongoRepository.UpdateManagedClaimBillingAddress(billingAddress);
            }
            return result;

        }

        public bool ManagedClaimDeletePrivatePayorBillData(Guid ClaimId, int Id)
        {
            var result = false;
            var billingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, ClaimId);
            if (billingAddress != null)
            {
                if (billingAddress.Rates.IsNotNullOrEmpty())
                {
                    if (billingAddress.Rates.Exists(r => r.Id == Id))
                    {
                        billingAddress.Rates.RemoveAll(r => r.Id == Id);

                    }
                }
                result = mongoRepository.UpdateManagedClaimBillingAddress(billingAddress);
            }
            return result;

        }

        protected override void SetClaimNewBillDataAppSpecific<C>(NewBillDataViewData data, Guid claimId, int payorType)
        {
            if (payorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                var payorBillingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claimId);
                if (payorBillingAddress != null)
                {
                    if (payorBillingAddress.Rates.IsNotNullOrEmpty())
                    {
                       data.ExistingNotes=payorBillingAddress.Rates.Select(r => r.Id).ToList();
                    }
                }
            }
            else if (payorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                 SetClaimNewBillData<C>(data, claimId);
            }
        }

        protected override bool ClaimAddBillDataAppSpecific<C>(ChargeRate chargeRate, Guid ClaimId, int payorType)
        {
            var result = false;
            if (payorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                result = ManagedClaimAddPrivatePayorBillData(chargeRate, ClaimId);
            }
            else if (payorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                result = ClaimAddInsuranceBillData<C>(chargeRate, ClaimId);
            }
            return result;
        }

        protected override bool SetClaimNewBillDataAppSpecific<C>(Guid ClaimId, int Id, int payorType)
        {
            var result = false;

            if (payorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                result = ManagedClaimDeletePrivatePayorBillData(ClaimId, Id);
            }
            else if (payorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                result = ClaimDeleteInsuranceBillData<C>(ClaimId, Id);
            }
            return result;
        }

        protected override BillingJsonViewData ClaimUpdateBillDataAppSpecific<C>(ChargeRate chargeRate, Guid ClaimId, int payorType, ClaimTypeSubCategory claimSubCategory)
        {
            if (payorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                return UpdateBillingAddressBillData(chargeRate, ClaimId);
            }
            else if (payorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                return ClaimUpdateInsuranceBillData<C>(chargeRate, ClaimId,claimSubCategory);
            }
            return new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated. Try again." };
        }

        protected BillingJsonViewData UpdateBillingAddressBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            viewData.Service = this.Service.ToString();
            viewData.Category = ClaimTypeSubCategory.ManagedCare;
            var payorBillingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, ClaimId);
            if (payorBillingAddress != null)
            {
                var rates = payorBillingAddress.Rates;
                if (rates != null && rates.Count > 0)
                {
                    var oldRate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                    if (oldRate != null)
                    {
                        oldRate.PreferredDescription = chargeRate.PreferredDescription;
                        oldRate.Code = chargeRate.Code;
                        oldRate.RevenueCode = chargeRate.RevenueCode;
                        oldRate.Charge = chargeRate.Charge;
                        oldRate.Modifier = chargeRate.Modifier;
                        oldRate.Modifier2 = chargeRate.Modifier2;
                        oldRate.Modifier3 = chargeRate.Modifier3;
                        oldRate.Modifier4 = chargeRate.Modifier4;
                        if (!MedicareIntermediaryFactory.Intermediaries().Contains(chargeRate.InsuranceId))
                        {
                            oldRate.ChargeType = chargeRate.ChargeType;
                            oldRate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                            if (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                            {
                                if (chargeRate.IsTimeLimit)
                                {

                                    oldRate.TimeLimitHour = chargeRate.TimeLimitHour;
                                    oldRate.TimeLimitMin = chargeRate.TimeLimitMin;
                                    oldRate.SecondDescription = chargeRate.SecondDescription;
                                    oldRate.SecondCode = chargeRate.SecondCode;
                                    oldRate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                    oldRate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                    if (chargeRate.IsSecondChargeDifferent)
                                    {
                                        oldRate.SecondCharge = chargeRate.SecondCharge;
                                    }
                                    else
                                    {
                                        oldRate.SecondCharge = 0;
                                    }
                                    oldRate.SecondModifier = chargeRate.SecondModifier;
                                    oldRate.SecondModifier2 = chargeRate.SecondModifier2;
                                    oldRate.SecondModifier3 = chargeRate.SecondModifier3;
                                    oldRate.SecondModifier4 = chargeRate.SecondModifier4;
                                    oldRate.SecondChargeType = chargeRate.SecondChargeType;
                                    if (oldRate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                                    {
                                        oldRate.SecondUnit = chargeRate.SecondUnit;
                                    }
                                    else
                                    {
                                        oldRate.SecondUnit = 0;
                                    }
                                    oldRate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                                }
                                oldRate.IsTimeLimit = chargeRate.IsTimeLimit;
                            }
                            else if (oldRate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                            {
                                oldRate.Unit = chargeRate.Unit;
                            }
                            //if (payorBillingAddress.Insurance.PayorType == (int)PayerTypes.MedicareHMO && (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
                            //{
                            //    oldRate.MedicareHMORate = chargeRate.MedicareHMORate;
                            //}

                            //claimInsurance.Insurance = agencyInsurance.ToXml();
                        }
                        if (mongoRepository.UpdateManagedClaimBillingAddress(payorBillingAddress))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Charge rate is successfuly updated.";
                        }
                    }
                }

            }
            return viewData;
        }

        protected override List<ChargeRate> UpdateRatesForReloadAppSpecific<C>(BaseClaim claim)
        {
            var rates = new List<ChargeRate>();
            if (claim.PayorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                rates = UpdateBillingAddressRatesForReload(claim);

            }
            else if (claim.PayorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                rates = UpdateRatesForReloadFromInsurance<C>(claim);
            }
            return rates;
        }

        private List<ChargeRate> UpdateBillingAddressRatesForReload(BaseClaim claim)
        {
            var rates = new List<ChargeRate>();
            var billingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claim.Id);
            if (billingAddress == null)
            {
                billingAddress = new ManagedClaimBillingAddress { AgencyId = Current.AgencyId, PatientId = claim.PatientId, Id = claim.Id };
            }
            var patientVisitRate = mongoRepository.GetPatientVisitRate(Current.AgencyId, claim.PatientId);
            if (patientVisitRate != null)
            {
                billingAddress.Rates = patientVisitRate.VisitRates;
                if (mongoRepository.UpdateManagedClaimBillingAddress(billingAddress))
                {
                    rates = billingAddress.Rates;
                }
            }
            return rates;
        }

        protected override void SetInvoiceFormAppSpecific<C>(BaseInvoiceFormViewData claimFormViewData, BaseClaim claim)
        {
            claimFormViewData.InvoiceType = InvoiceType.Invoice;//set deafult value
            if (claim.PayorType == (int)PayorTypeMainCategory.PrivatePayor)
            {
                var billingAddress = mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claim.Id);
                if (billingAddress != null)
                {
                    claimFormViewData.Claim.PayorType = (int)PayerTypes.Selfpay;
                    if (claim.IsBillingAddressDifferent)
                    {
                        claimFormViewData.Claim.PayorName = billingAddress.Payor.DisplayNameWithMi;
                        claimFormViewData.PayorName = billingAddress.Payor.DisplayNameWithMi;
                        claimFormViewData.PayorAddressLine1 = billingAddress.Payor.AddressFirstRow;
                        claimFormViewData.PayorAddressLine2 = billingAddress.Payor.AddressSecondRow;
                    }
                    else
                    {
                        claimFormViewData.Claim.PayorName = claim.DisplayName;
                        claimFormViewData.PayorName = claim.DisplayName;
                        claimFormViewData.PayorAddressLine1 = claim.AddressFirstRow;
                        claimFormViewData.PayorAddressLine2 = claim.AddressSecondRow;
                    }
                    claimFormViewData.Claim.ChargeRates = billingAddress.Rates;
                }
            }
            else if (claimFormViewData.Claim.PayorType == (int)PayorTypeMainCategory.NonPrivatePayor)
            {
                SetInvoiceFormInsuranceInfo<C>(claimFormViewData);
            }
        }

        protected override void SetInvoiceNumberAppSpecific(ManagedClaim claim)
        {
            claim.InvoiceNumber = billingRepository.CreateNextClaimNumber(claim.Id);
        }

        protected override void GetInvoiceNumber(ManagedClaim managedClaim, ClaimViewData claimData)
        {
            claimData.ClaimNumber = managedClaim.InvoiceNumber;
        }

        protected override void SetPayorTypeAppSpecific(BaseClaim claim)
        {
            if (claim.PrimaryInsuranceId == 18)
            {
                claim.PayorType = (int)PayorTypeMainCategory.PrivatePayor;
            }
        }
      
    }
}
