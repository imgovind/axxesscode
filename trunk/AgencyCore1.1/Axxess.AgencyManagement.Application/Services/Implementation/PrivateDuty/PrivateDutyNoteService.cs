﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.LookUp.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Helpers;

    public class PrivateDutyNoteService : NoteService<PrivateDutyScheduleTask,PrivateDutyCarePeriod>
    {
        private PrivateDutyTaskRepository scheduleRepository;
        private PrivateDutyNoteRepository noteRepository;
        private PrivateDutyPlanOfCareRepository planofCareRepository;
        private PrivateDutyEpisodeRepository episodeRepository;

        public PrivateDutyNoteService(PrivateDutyDataProvider dataProvider,ILookUpDataProvider lookupDataProvider) :
            base(dataProvider.TaskRepository, dataProvider.NoteRepository, dataProvider.EpisodeRepository,dataProvider.MongoRepository, dataProvider.VitalSignRepository)
        {
            
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.lookUpRepository = lookupDataProvider.LookUpRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.NoteRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.planofCareRepository = dataProvider.PlanOfCareRepository;

            base.Service = AgencyServices.PrivateDuty;
        }

        protected override PrivateDutyScheduleTask SetAppSpecificForSave(PrivateDutyScheduleTask scheduleTask, SaveNoteArguments savingArguments, DisciplineTask disciplineTask)
        {
            if (scheduleTask != null)
            {
                if (savingArguments != null)
                {
                    if (disciplineTask.IsVisitParameterVisible || disciplineTask.IsTimeIntervalVisible)
                    {
                        if (savingArguments.VisitStartTime > DateTime.MinValue)
                        {
                            scheduleTask.VisitStartTime = savingArguments.VisitStartTime;
                        }
                    }
                    if (disciplineTask.IsTimeIntervalVisible)
                    {
                        if (savingArguments.VisitEndTime > DateTime.MinValue)
                        {
                            scheduleTask.VisitEndTime = savingArguments.VisitEndTime;
                        }
                    }
                }
               
            }
            return scheduleTask;
        }

        protected override List<VitalSign> GetAppSpecificVitalSigns(Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime eventDate)
        {
            return new List<VitalSign>();

        }

        /// <summary>
        /// Gets the vital signs for private duty using the new vital signs system.
        /// Only gets the vitals for tasks between the start date and end date
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<VitalSignLogListItem> GetVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSignLogListItem>();
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var scheduleEvents = scheduleRepository.GetScheduledEventsOnlyLean(Current.AgencyId, patientId, startDate, endDate, null, noteDisciplineTasks.ToArray(), false);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = NewVitalSignFromList(patientId, scheduleEvents);
            }
            return vitalSigns;
        }

        /// <summary>
        /// Gets the vital signs for private duty using the new vital signs system.
        /// Gets a limit of the vital signs            
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public List<VitalSignLogListItem> GetVitalSigns(Guid patientId, int limit)
        {
            var vitalSigns = new List<VitalSignLogListItem>();
            var noteDisciplineTasks = DisciplineTaskFactory.VitalSignsDisciplineTasks(true);
            var scheduleEvents = scheduleRepository.GetScheduleEvents(Current.AgencyId, patientId, limit, noteDisciplineTasks.ToArray());
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                vitalSigns = NewVitalSignFromList(patientId, scheduleEvents);
            }
            return vitalSigns;
        }

        protected override void SetTaskSpecificInformationPrint(VisitNoteViewData noteViewData, PrivateDutyScheduleTask scheduleEvent, DisciplineTask disciplineTask)
        {
            noteViewData.VisitStartDate = scheduleEvent.VisitStartTime.IsValid() ? scheduleEvent.VisitStartTime : scheduleEvent.EventStartTime;
            noteViewData.VisitEndDate = scheduleEvent.VisitEndTime.IsValid() ? scheduleEvent.VisitEndTime : scheduleEvent.EventEndTime;
            base.SetEpisodeRange(noteViewData, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
        }
        protected override void SetTaskSpecificInformation(VisitNoteViewData noteViewData, PrivateDutyScheduleTask scheduleEvent, DisciplineTask disciplineTask, bool isNoteStillNotStarted)
        {
            noteViewData.VisitStartDate = scheduleEvent.VisitStartTime.IsValid() ? scheduleEvent.VisitStartTime : scheduleEvent.EventStartTime;
            noteViewData.VisitEndDate = scheduleEvent.VisitEndTime.IsValid() ? scheduleEvent.VisitEndTime : scheduleEvent.EventEndTime;
            noteViewData.EventStartDate = scheduleEvent.EventStartTime;
            base.SetTaskSpecificInformation(noteViewData, scheduleEvent, disciplineTask, isNoteStillNotStarted);
        }

        protected override PrivateDutyCarePeriod GetEpisodeForNote(SaveNoteArguments savingArguments)
        {
            return episodeRepository.GetEpisodeFromDate(Current.AgencyId, savingArguments.PatientId,savingArguments.VisitStartTime);
        }


        protected override List<Validation> AddSaveNotesValidationRules(string button, PrivateDutyCarePeriod episode, SaveNoteArguments noteBasicData, DisciplineTask disciplineTask)
        {
            var rules = new List<Validation>();
            rules.Add(new Validation(() => episode.Id != noteBasicData.EpisodeId, "The care period found for this visit start date don't much scheduled care period."));

            if (disciplineTask.IsVisitParameterVisible)
            {
                rules.Add(new Validation(() => noteBasicData.VisitStartTime.Date <= DateTime.MinValue.Date, "Visit start date is required."));
                rules.Add(new Validation(() => noteBasicData.VisitEndTime.Date <= DateTime.MinValue.Date, "Visit end date is required."));
                rules.Add(new Validation(() => !noteBasicData.VisitEndTime.IsBetween(episode.StartDate.Date, episode.EndDate.AddDays(1).Date), "The event end date is out of the current care period. We didn't find a care period that can match with this event start and end date."));
            }

            rules.Add(new Validation(() => !Enum.IsDefined(typeof(DisciplineTasks), noteBasicData.DisciplineTask), "Select the right task."));
            CommonSaveNotesValidationRules(rules, button, noteBasicData,episode);
            if (button == "Complete" || button == "Approve")
            {
               
            }
            return rules;
        }


    }
}
