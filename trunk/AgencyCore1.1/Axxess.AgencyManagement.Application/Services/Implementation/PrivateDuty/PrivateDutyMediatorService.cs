﻿
namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.LookUp.Domain;

    public class PrivateDutyMediatorService : MediatorService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
        #region Constructor

        private readonly PrivateDutyTaskService scheduleService;
        private readonly PrivateDutyPlanOfCareService planOfCareService;
        private readonly PrivateDutyAssessmentService assessmentService;
        private readonly PrivateDutyEpisodeService episodeService;
        public PrivateDutyMediatorService(IPatientService patientService,
             PrivateDutyPatientProfileService baseProfileService,
             PrivateDutyEpisodeService epiosdeService,
             PrivateDutyTaskService scheduleService,
             PrivateDutyAssessmentService assessmentService,
             PrivateDutyPhysicianOrderService physicianOrderService,
             PrivateDutyPlanOfCareService planOfCareService,
             PrivateDutyNoteService noteService,
             PrivateDutyCommunicationNoteService baseCommunicationNoteService,
             PrivateDutyInfectionService baseInfectionService,
             PrivateDutyIncidentAccidentService baseIncidentService,
             ILookUpService lookUpService,
            IMessageService messageService)
            : base(messageService, epiosdeService, scheduleService, assessmentService, physicianOrderService, planOfCareService, noteService, baseCommunicationNoteService, baseInfectionService, baseIncidentService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");

            base.patientService = patientService;
            base.baseProfileService = baseProfileService;
            this.assessmentService = assessmentService;
            this.scheduleService = scheduleService;
            this.planOfCareService = planOfCareService;
            this.episodeService = epiosdeService;
            this.planOfCareService = planOfCareService;
            base.lookUpService = lookUpService;
            base.Service = AgencyServices.PrivateDuty;
        }

        #endregion

        protected override bool ProcessEditDetailAppSpecific(PrivateDutyScheduleTask schedule)
        {
            return false;
        }

        //public ManagedClaim GetManagedClaimInfo(Guid patientId, Guid Id)
        //{
        //    var managedClaim = billingService.GetManagedClaimInfo(patientId, Id);
        //    if (managedClaim != null)
        //    {
        //        if (managedClaim.Status == (int)ManagedClaimStatus.ClaimCreated)
        //        {
        //            if (!managedClaim.IsInfoVerified)
        //            {
        //                var episodes = episodeService.GetEpisodeDatasBetween(patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
        //                if (episodes != null && episodes.Count > 0)
        //                {
        //                    if (episodes.Count == 1)
        //                    {
        //                        var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodes[0].Id, patientId);
        //                        if (assessmentEvent != null)
        //                        {
        //                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
        //                            var assessment = assessmentService.GetAssessment(patientId, assessmentEvent.Id);
        //                            if (assessment != null)
        //                            {
        //                                var assessmentQuestions = assessment.ToDictionary();
        //                                managedClaim.DiagnosisCode = assessmentQuestions.ToClaimDiagonasisCodesXml();
        //                                managedClaim.HippsCode = assessment.HippsCode;
        //                                managedClaim.ClaimKey = assessment.ClaimKey;
        //                                managedClaim.AssessmentType = assessmentType;
        //                                managedClaim.ProspectivePay = billingService.ProspectivePayAmount(assessmentType, assessment.HippsCode, episodes[0].StartDate, managedClaim.AddressZipCode, managedClaim.LocationZipCode);
        //                            }
        //                        }
        //                    }
        //                    else if (episodes.Count > 1)
        //                    {
        //                        managedClaim.HasMultipleEpisodes = true;
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    return managedClaim;
        //}

        //public PlanofCare Get485ForEdit(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    var planofCare = planOfCareService.GetPlanOfCareOnly(patientId, eventId);
        //    if (planofCare != null)
        //    {
        //        var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
        //        planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
        //        if (planofCare.Data.IsNotNullOrEmpty())
        //        {
        //            planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
        //        }
        //        var episodeRange = assessmentService.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
        //        if (episodeRange != null)
        //        {
        //            planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
        //            planofCare.EpisodeStart = episodeRange.StartDateFormatted;
        //            planofCare.IsLinkedToAssessment = episodeRange.IsLinkedToAssessment;
        //        }
        //        planofCare.StatusComment = scheduleService.GetReturnComments(eventId, episodeId, patientId);
        //        if (planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
        //        {
        //            var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
        //            if (oldPhysician != null && !oldPhysician.Id.IsEmpty()) planofCare.PhysicianId = oldPhysician.Id;
        //        }

        //    }
        //    return planofCare;
        //}

        //public OasisPlanOfCareJson GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    var viewData = new OasisPlanOfCareJson { isSuccessful = false, errorMessage = "No Plan of Care (485) found for this episode." };
        //    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, episodeId, eventId);
        //    if (scheduleEvent != null)
        //    {
        //        var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduleEvent.EventDate);
        //        if (assessment != null)
        //        {
        //            var pocScheduleEvent = assessmentService.GetPlanofCareScheduleEvent(assessment.EpisodeId, patientId, assessment.Id, assessment.Type.ToString());
        //            if (pocScheduleEvent != null)
        //            {
        //                viewData.isSuccessful = true;
        //                viewData.url = "/Oasis/PlanOfCarePdf";
        //                viewData.episodeId = pocScheduleEvent.EpisodeId;
        //                viewData.patientId = pocScheduleEvent.PatientId;
        //                viewData.eventId = pocScheduleEvent.Id;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public PlanofCare GetPlanOfCareWithAssessment(Guid patientId, Guid eventId)
        //{
        //    var planofCare = planOfCareService.GetPlanOfCareOnly(patientId, eventId);
        //    if (planofCare != null)
        //    {
        //        //var assessment = oasisAssessmentRepository.Get(planofCare.AssessmentId, planofCare.AssessmentType, Current.AgencyId);
        //        var questionData = assessmentService.GetQuestions(planofCare.AssessmentId);
        //        if (questionData != null)
        //        {
        //            planofCare.Questions = planOfCareService.Get485FromAssessment(questionData.PatientId, questionData.Question);
        //        }
        //    }
        //    return planofCare;
        //}

        //public PatientCenterViewData GetPatientCenterDataWithSchedules(Guid patientId, int status)
        //{
        //    Patient patient = null;
        //    var viewData = new PatientCenterViewData();
        //    var selectedViewData = patientService.GetPatientCenterDataWithOutSchedules(patientId, status, ref patient);
        //    if (selectedViewData != null)
        //    {
        //        viewData.SelectionViewData = selectedViewData;
        //        if (patient != null)
        //        {
        //            patientService.SetPatientSnapshotInfo(patient, true, true, true);
        //            viewData.PatientScheduleEvent.Patient = patient;
        //        }
        //        if (viewData.PatientScheduleEvent != null)
        //        {
        //            viewData.PatientScheduleEvent.DateFilterType = "ThisEpisode";
        //            viewData.PatientScheduleEvent.DisciplineFilterType = "All";
        //            scheduleService.CurrentEpisodePatientWithScheduleEvent(viewData.PatientScheduleEvent, viewData.SelectionViewData.CurrentPatientId, "All");
        //        }
        //    }
        //    //var viewData = patientService.GetPatientCenterDataWithOutSchedules(patientId, status);
        //    //if (viewData != null)
        //    //{
        //    //    if (viewData.PatientScheduleEvent != null)
        //    //    {
        //    //        viewData.PatientScheduleEvent.DateFilterType = "ThisEpisode";
        //    //        viewData.PatientScheduleEvent.DisciplineFilterType = "All";
        //    //        //scheduleService.CurrentEpisodePatientWithScheduleEvent(viewData.PatientScheduleEvent, viewData.CurrentPatientId, "All");
        //    //    }
        //    //}
        //    return viewData;
        //}

        //public PatientScheduleTaskViewData GetPatientChartsData(Guid patientId)
        //{

        //    var viewData = new PatientScheduleEventViewData();
        //    viewData.Service = this.Service;
        //    var patient = profileService.GetPatientSnapshotInfo(patientId, true, true, true);
        //    if (patient != null)
        //    {
        //        viewData.Patient = patient;
        //        scheduleService.CurrentEpisodePatientWithScheduleEvent(viewData, patientId, "All");
        //    }
        //    //var patient = patientService.GetPatientOnly(patientId);
        //    //if (patient != null)
        //    //{
        //    //    var temp = patientService.GetPatientScheduleEventViewData(patient);
        //    //    viewData.Patient = temp.Patient;
        //    //    viewData.DateFilterType = temp.DateFilterType;
        //    //    viewData.DisciplineFilterType = temp.DisciplineFilterType;
        //    //    viewData.FilterStartDate = temp.FilterStartDate;
        //    //    viewData.FilterEndDate = temp.FilterEndDate;
        //    //    if (viewData != null)
        //    //    {
        //    //        scheduleService.CurrentEpisodePatientWithScheduleEvent(viewData, patientId, "All");
        //    //    }
        //    //}
        //    //return viewData;
        //    return null;
        //}

        public JsonViewData AddMultiDaySchedule(Guid patientId, Guid userId, int disciplineTaskId, List<DateTime> visitDateArray, DateTime startTime, DateTime endTime, bool isAllDay)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to create the tasks. Please try again." };
            int tasksAdded = 0;

            if (!patientId.IsEmpty() && !userId.IsEmpty())
            {
                if (disciplineTaskId > 0)
                {
                    if (visitDateArray != null && visitDateArray.Count > 0)
                    {
                        var disciplineTask = lookUpService.GetDisciplineTaskWithTableName(disciplineTaskId);
                        if (disciplineTask != null)
                        {
                            if (disciplineTask.IsMultiple)
                            {
                                var episodes = episodeService.GetPatientActiveEpisodesLeanByDateRange(patientId, visitDateArray.Min(), visitDateArray.Max());
                                if (episodes.IsNotNullOrEmpty())
                                {
                                    foreach (var patientEpisode in episodes)
                                    {
                                        var daysInTheRange = visitDateArray.Where(d => d.Date >= patientEpisode.StartDate.Date && d.Date <= patientEpisode.EndDate.Date).ToList();
                                        if (daysInTheRange.IsNotNullOrEmpty())
                                        {
                                            var scheduledEvents = scheduleService.AddMultiDayScheduleTaskHelper(patientEpisode, disciplineTask, userId, daysInTheRange, startTime, endTime, isAllDay);
                                            if (scheduledEvents != null && scheduledEvents.Count > 0)
                                            {
                                                scheduleService.SetScheduleTasksDefault(patientEpisode, new List<DisciplineTask>() { disciplineTask }, scheduledEvents);
                                                viewData = this.ProcessSchedule(disciplineTask.Table, patientEpisode, scheduledEvents);
                                                if (viewData.isSuccessful)
                                                {
                                                    if (episodes.Count == 1)
                                                    {
                                                        viewData.errorMessage = "The tasks have been successfully saved.";
                                                    }
                                                    else
                                                    {
                                                        tasksAdded += scheduledEvents.Count;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!viewData.isSuccessful && tasksAdded > 0)
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = string.Format("{0} tasks were successfully saved.", tasksAdded);
                                    }
                                }
                                else
                                {
                                    viewData.errorMessage = "No care periods could be found.";
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Adding mutiple tasks are not allowed for this discipline.";
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData AddTask(PrivateDutyScheduleTask newTask)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be created." };

            if (newTask != null)
            {
                if (newTask.IsValid())
                {
                    var episode = episodeService.GetEpisodeFromDate(newTask.PatientId, newTask.EventStartTime);
                    if (episode != null)
                    {
                        if (!(newTask.EventEndTime.Date >= episode.StartDate.Date && newTask.EventEndTime.Date < episode.EndDate.AddDays(1).Date))
                        {
                            viewData.errorMessage = "The event end date is out of the current care period. We didn't find a care period that can match with this event start and end date.";
                            return viewData;
                        }
                        newTask.EpisodeId = episode.Id;
                        var discipline = lookUpService.GetDisciplineTaskWithTableName(newTask.DisciplineTask);
                        if (discipline != null)
                        {
                            //episode.PatientId = newTask.PatientId;
                            scheduleService.SetScheduleTaskDefault(episode, discipline, newTask);
                            viewData = this.ProcessSchedule(discipline.Table, episode, new List<PrivateDutyScheduleTask>() { newTask });
                            if (viewData.isSuccessful)
                            {
                                viewData.errorMessage = "The task was successfully created.";
                                viewData.Service = this.Service.ToString();
                                viewData.ServiceId = (int)this.Service;
                                viewData.UserIds = new List<Guid> { newTask.UserId };
                            }
                            else if (viewData.errorMessage.IsNullOrEmpty())
                            {
                                viewData.errorMessage = "Task could not be created.";
                            }
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "There is no matching care period for the current event start and end date.";
                        return viewData;
                    }
                }
                else
                {
                    viewData.errorMessage = newTask.ValidationMessage;
                }
            }

            return viewData;
        }

        public JsonViewData RescheduleTask(Guid PatientId, Guid Id, DateTime EventStartTime, DateTime EventEndTime)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be moved."};
            try
            {
                var taskToEdit = scheduleService.GetScheduleTask(PatientId, Id);
                if (taskToEdit != null)
                {
                    if (taskToEdit.IsAllDay)
                    {
                        if (EventStartTime.IsValid())
                        {
                            if (EventEndTime.Date < EventStartTime.Date)
                            {
                                EventEndTime = EventStartTime;
                            }
                        }
                        else
                        {
                            viewData.errorMessage = "The event date is not valid.";
                            return viewData;
                        }
                    }
                    var carePeriod = episodeService.GetEpisodeFromDate(PatientId, EventStartTime.Date);
                    if (carePeriod != null)
                    {
                        var taskBackup = taskToEdit.DeepClone();// TaskHelperFactory<PrivateDutyScheduleTask>.CopyTaskValues(taskToEdit);
                        var isWarning = false;
                        var warning = string.Empty;

                        if (!EventEndTime.IsBetween(carePeriod.StartDate.Date, carePeriod.EndDate.AddDays(1).Date))
                        {
                            viewData.errorMessage = "The event end date is out of the current care period. We didn't find a care period that can match with this event start and end date.";
                            return viewData;
                        }
                        if (carePeriod.Id != taskToEdit.EpisodeId)
                        {
                            taskToEdit.EpisodeId = carePeriod.Id;
                            taskToEdit.StartDate = carePeriod.StartDate;
                            taskToEdit.EndDate = carePeriod.EndDate;
                            isWarning = true;
                            warning = " The event task has been moved to another care period.";
                            viewData.UserIds = new List<Guid> { taskToEdit.UserId, Current.UserId };
                        }


                        taskToEdit.EventStartTime = EventStartTime;
                        taskToEdit.EventEndTime = EventEndTime;
                        taskToEdit.VisitStartTime = EventStartTime;
                        taskToEdit.VisitEndTime = EventEndTime;

                        viewData = CheckTaskValidation(carePeriod, taskToEdit);
                        if (viewData.isSuccessful)
                        {
                            if (scheduleService.UpdateScheduleTask(taskToEdit))
                            {
                                if (ProcessEditDetail(taskToEdit))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.isWarning = isWarning;
                                    viewData.errorMessage = "Task moved successfully. " + warning;
                                    viewData.Service = this.Service.ToString();
                                    viewData.ServiceId = (int)this.Service;
                                }
                                else
                                {
                                    scheduleService.UpdateScheduleTask(taskBackup);
                                }
                            }
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "There is no matching care period for the current event start and end date.";
                        return viewData;
                    }
                }
            }
            catch
            {
                return viewData;
            }
            return viewData;
        }

        protected override JsonViewData UpdateScheduleEntityAppSpecific(PrivateDutyScheduleTask task, bool isDeprecated)
        {
            throw new NotImplementedException();
        }

        public override JsonViewData UpdateScheduleEventDetails(PrivateDutyScheduleTask scheduleEvent, System.Web.HttpFileCollectionBase httpFiles)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task detail could not be updated." };
            var carePeriod = episodeService.GetEpisodeFromDate(scheduleEvent.PatientId, scheduleEvent.EventStartTime.Date);
            if (carePeriod != null)
            {
                var isWarning = false;
                var warning = string.Empty;

                if (!scheduleEvent.EventEndTime.IsBetween(carePeriod.StartDate.Date, carePeriod.EndDate.AddDays(1).Date))
                {
                    viewData.errorMessage = "The event end date is out of the current care period. We didn't find a care period that can match with this event start and end date.";
                    return viewData;
                }
                else
                {
                    if (carePeriod.Id != scheduleEvent.EpisodeId)
                    {
                        scheduleEvent.EpisodeId = carePeriod.Id;
                        scheduleEvent.StartDate = carePeriod.StartDate;
                        scheduleEvent.EndDate = carePeriod.EndDate;
                        isWarning = true;
                        warning = " The event task moved to other care period.";
                        viewData.UserIds = new List<Guid> { scheduleEvent.UserId, Current.UserId };
                    }
                }
                viewData = CheckTaskValidation(carePeriod, scheduleEvent);
                if (viewData.isSuccessful)
                {
                    viewData = UpdateScheduleEventDetail(scheduleEvent, httpFiles);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Task details updated successfully." + warning;
                        viewData.isWarning = isWarning;
                        viewData.Service = this.Service.ToString();
                        viewData.ServiceId = (int)this.Service;
                    }
                }
            }
            else
            {
                viewData.errorMessage = string.Format("Could not locate an episode. Either create a new episode containing the date {0} or use a different date.", scheduleEvent.EventStartTime.ToString("MM/dd/yyyy"));
            }
            return viewData;
        }

        protected override Guid GetScheduledEventForDetailAppSpecific(Guid patientId, Guid eventId, int disciplineTaskId)
        {
            throw new NotImplementedException();
        }

        protected override JsonViewData ProcessScheduleAppSpecific(int table, PrivateDutyCarePeriod episode, List<PrivateDutyScheduleTask> scheduleEvents)
        {
            var viewData = new JsonViewData();
            return viewData;
        }

        protected override void DischargePatientByOASIS(Guid Id, Guid patientId, Guid episodeId, DateTime possibleDischargeDate, int assessmentType, JsonViewData viewData)
        {
        }


        private JsonViewData CheckTaskValidation(PrivateDutyCarePeriod episode, PrivateDutyScheduleTask scheduleTask)
        {
            var viewData = new JsonViewData(false, "Task details could not be saved.");
           // var episode = episodeService.GetEpisodeFromDate(scheduleTask.PatientId, scheduleTask.EventStartTime.Date);
            if (episode != null)
            {
                scheduleTask.EpisodeId = episode.Id;
                viewData = TaskHelperFactory<PrivateDutyScheduleTask>.ValidateScheduleTaskDate(scheduleTask, episode.StartDate, episode.EndDate);
                if (viewData.isSuccessful && DisciplineTaskFactory.AllAssessments(false).Contains(scheduleTask.DisciplineTask))
                {
                    var oldEvents = scheduleService.GetPatientScheduledEventsOnlyNew(Current.AgencyId, episode.Id, scheduleTask.PatientId);
                    viewData = TaskHelperFactory<PrivateDutyScheduleTask>.Validate(oldEvents, new List<PrivateDutyScheduleTask> { scheduleTask }, episode.EndDate);
                }
            }
            else
            {
                viewData.errorMessage = string.Format("Could not locate an episode. Either create a new episode containing the date {0} or use a different date.", scheduleTask.EventStartTime.ToString("MM/dd/yyyy"));
            }
            return viewData;
        }
    }
}
