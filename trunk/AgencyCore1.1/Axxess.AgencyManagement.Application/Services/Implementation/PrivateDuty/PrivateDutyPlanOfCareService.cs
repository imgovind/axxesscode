﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Application.Helpers;
    public class PrivateDutyPlanOfCareService : PlanOfCareService<PrivateDutyScheduleTask,PrivateDutyCarePeriod>
    {
        private PrivateDutyTaskRepository scheduleRepository;
        private PrivateDutyEpisodeRepository episodeRepository;
        public PrivateDutyPlanOfCareService(PrivateDutyDataProvider dataProvider)
            : base(dataProvider.TaskRepository, dataProvider.PlanOfCareRepository, dataProvider.EpisodeRepository, dataProvider.MongoRepository)
        {
            base.physicianRepository = dataProvider.PhysicianRepository;
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.PrivateDuty;
        }

        protected override PlanofCare POCServiceSpecificPrintData(int disciplineTaskId, PlanofCare planofCare)
        {
            return planofCare;
        }

        protected override void POCActiveDateRangeAndSOC(PrivateDutyScheduleTask scheduledEvent, Dictionary<string, string> dictionary)
        {
           
        }

        protected override void Get485ForEditAppSpecific(PlanofCare planofCare)
        {
            if (planofCare != null)
            {
                if (planofCare.IsStandAlone)
                {
                    var episode = episodeRepository.GetEpisodeByIdWithSOC(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
                    if (episode != null)
                    {
                        planofCare.SOC = episode.StartOfCareDate;
                        planofCare.EpisodeStart = episode.StartDateFormatted;
                        planofCare.EpisodeEnd = episode.EndDateFormatted;
                    }
                }
                else
                {
                    var episodeRange = EpisodeAssessmentHelperFactory<PrivateDutyCarePeriod, PrivateDutyScheduleTask>.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
                    if (episodeRange != null)
                    {
                        planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                        planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                        planofCare.IsLinkedToAssessment = episodeRange.IsLinkedToAssessment;
                        planofCare.SOC = episodeRange.StartOfCareDate;
                    }
                }
            }
        }
    }
}
