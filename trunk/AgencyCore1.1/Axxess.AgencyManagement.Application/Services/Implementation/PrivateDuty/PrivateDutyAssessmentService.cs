﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;

    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Enums;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Repositories;


    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core.Extension;
    using Axxess.LookUp.Repositories;


    using Axxess.AgencyManagement.Entities.Common;


    public class PrivateDutyAssessmentService : AssessmentService<PrivateDutyScheduleTask,PrivateDutyCarePeriod>
    {
        #region Constructor / Members

        private readonly PrivateDutyTaskRepository scheduleRepository;
        private readonly PrivateDutyEpisodeRepository episodeRepository;
        private readonly PrivateDutyAssessmentRepository assessmentRepository;

        public PrivateDutyAssessmentService(PrivateDutyDataProvider dataProvider, ILookUpDataProvider lookupDataProvider)
            : base(dataProvider.TaskRepository, dataProvider.AssessmentRepository, dataProvider.EpisodeRepository,dataProvider.MongoRepository, dataProvider.VitalSignRepository)
        {

            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.profileRepository = dataProvider.PatientProfileRepository;


            this.assessmentRepository = dataProvider.AssessmentRepository;
            this.planofCareRepository = dataProvider.PlanOfCareRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.PrivateDuty;
        }

        //public void SetPatientProfileEpisodeInfo(PatientProfile patientProfile)
        //{
        //    if (patientProfile != null && patientProfile.Patient != null)
        //    {
        //        var episode = episodeRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientProfile.Patient.Id, DateTime.Now);
        //        if (episode != null)
        //        {
        //            var assessment = GetEpisodeAssessment(episode);
        //            var questions = GetQuestions(assessment.Id);
        //            if (questions != null && questions.Question != null)
        //            {
        //                assessment.Questions = questions.Question;
        //            }
        //            var freq = this.GetFrequencyForAssessment(assessment);
        //            patientProfile.Frequencies = freq;
        //            patientProfile.CurrentEpisode = episode;
        //            patientProfile.CurrentAssessment = assessment;
        //        }
        //    }
        //}

        //public Assessment GetEpisodeAssessment(PrivateDutyCarePeriod episode)
        //{
        //    return GetEpisodeAssessment(episode, true);
        //}

        //public Assessment GetEpisodeAssessment(PrivateDutyCarePeriod episode, bool isAssessmentCompleted)
        //{
        //    Assessment assessment = null;
        //    var assessmentEvent = this.GetEpisodeAssessmentEvent(episode, isAssessmentCompleted);
        //    if (assessmentEvent != null)
        //    {
        //        assessment = assessmentRepository.Get(assessmentEvent.Id, Current.AgencyId);
        //    }
        //    return assessment;
        //}

        //public PrivateDutyScheduleTask GetEpisodeAssessmentEvent(PrivateDutyCarePeriod episode, bool isAssessmentCompleted)
        //{
        //    PrivateDutyScheduleTask result = null;
        //    if (episode != null)
        //    {
        //        var isSOCExist = false;
        //        var socSchedule = this.GetSOCEpisodeAssessmentEvent(episode, isAssessmentCompleted, out isSOCExist);
        //        if (socSchedule != null)
        //        {
        //            result = socSchedule;
        //        }
        //        else
        //        {
        //            if (!isSOCExist)
        //            {
        //                var previousEpisode = episodeRepository.GetPreviousEpisodeByEndDate(Current.AgencyId, episode.PatientId, episode.StartDate.AddDays(-1));
        //                if (previousEpisode != null)
        //                {
        //                    result = this.GetRecetOrROCEpisodeAssessmentEvent(previousEpisode, isAssessmentCompleted);
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public PrivateDutyScheduleTask GetSOCEpisodeAssessmentEvent(PrivateDutyCarePeriod episode, bool isAssessmentCompleted, out bool isSOCExist)
        //{
        //    PrivateDutyScheduleTask result = null;
        //    isSOCExist = false;
        //    if (episode != null)
        //    {
        //        var socSchedule = scheduleRepository.GetFirstScheduledEvent(Current.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, episode.StartDate, episode.EndDate.AddDays(-6), DisciplineTaskFactory.SOCDisciplineTasks(true).ToArray());
        //        if (socSchedule != null)
        //        {
        //            isSOCExist = true;
        //            if (isAssessmentCompleted)
        //            {
        //                if (ScheduleStatusFactory.OASISAfterQA().Exists(s => s == socSchedule.Status))
        //                {
        //                    result = socSchedule;
        //                }
        //            }
        //            else
        //            {
        //                result = socSchedule;
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public PrivateDutyScheduleTask GetRecetOrROCEpisodeAssessmentEvent(PrivateDutyCarePeriod previousEpisode, bool isNeedProcessed)
        //{
        //    PrivateDutyScheduleTask result = null;
        //    if (previousEpisode != null)
        //    {
        //        var recetOrROCSchedule = scheduleRepository.GetLastScheduledEvent(Current.AgencyId, previousEpisode.Id, previousEpisode.PatientId, previousEpisode.StartDate, previousEpisode.EndDate, previousEpisode.EndDate.AddDays(-5), previousEpisode.EndDate, DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray());
        //        if (recetOrROCSchedule != null)
        //        {
        //            if (isNeedProcessed)
        //            {
        //                if (ScheduleStatusFactory.OASISAfterQA().Exists(s => s == recetOrROCSchedule.Status))
        //                {
        //                    result = recetOrROCSchedule;
        //                }
        //            }
        //            else
        //            {
        //                result = recetOrROCSchedule;
        //            }
        //        }
        //    }
        //    return result;
        //}

        protected override void UpdateAssessmentForDetailAppSpecific(PrivateDutyScheduleTask schedule)
        {
        }

        protected override bool Validate(Assessment assessment, out Assessment assessmentOut)
        {
            assessmentOut = assessment;
            return true;
        }

        protected override OasisViewData GeneratePOCAndHospitalizationLog(int status, OasisViewData oasisViewData, Assessment assessment, PrivateDutyScheduleTask scheduleEvent)
        {
            var episode = episodeRepository.GetEpisodeDateRange(Current.AgencyId, assessment.PatientId, assessment.EpisodeId);
            if (episode != null)
            {
                if (DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask))
                {
                    oasisViewData.IsHospitalizationLogRefresh = AddOrUpdateHospitalizationLog(assessment);
                }
                else if (DisciplineTaskFactory.ResumptionOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask))
                {
                    oasisViewData.IsHospitalizationLogRefresh = MarkEndOfHospitalization(assessment.Id, assessment.PatientId, scheduleEvent.EventDate);
                }

                if ((status == (int)ScheduleStatus.OasisCompletedPendingReview || (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady && Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA)))
                        && AssessmentTypeFactory.GeneratePlanOfCareNonOasis().Contains(AssessmentTypeFactory.ParseString(assessment.Type)))
                {
                    oasisViewData.isSuccessful = this.CreatePlanofCare(assessment, scheduleEvent);
                }
            }
            return oasisViewData;
        }

        protected override OasisViewData GeneratePOC(int status, OasisViewData oasisViewData, Assessment assessment, PrivateDutyScheduleTask scheduleEvent)
        {
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
            if (episode != null)
            {
                if ((status == (int)ScheduleStatus.OasisCompletedPendingReview || (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady && Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA))) &&
                            (AssessmentTypeFactory.GeneratePlanOfCareAssessments(scheduleEvent.EventDate, episode.EndDate).Contains(AssessmentTypeFactory.ParseString(assessment.Type))))
                {
                    oasisViewData.isSuccessful = this.CreatePlanofCare(assessment, scheduleEvent);
                }
            }
            return oasisViewData;
        }

        public override Patient GetPatientWithProfileFromAdmissionAppSpecific(Guid patientId, Guid admissionId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var profile = profileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    patient.Profile = profile;
                }
            }
            return patient;
        }

        public override string GetDiagnosisData(Assessment assessment)
        {
            var data = assessment.ToDictionary();
            string DiagnosisData = "({";
            if (data.AnswerOrEmptyString("M1020PrimaryDiagnosis").IsNotNullOrEmpty()) DiagnosisData += string.Format("'_M1020PrimaryDiagnosis':'{0}','_M1020ICD9M':'{1}','_M1024PaymentDiagnosesA3':'{2}','_M1024ICD9MA3':'{3}','_M1024PaymentDiagnosesA4':'{4}','_M1024ICD9MA4':'{5}','_M1020SymptomControlRating':'{6}','_485ExacerbationOrOnsetPrimaryDiagnosis':'{7}','_M1020PrimaryDiagnosisDate':'{8}',", data.AnswerOrEmptyString("M1020PrimaryDiagnosis").EscapeQuotes(), data.AnswerOrEmptyString("M1020ICD9M").EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnosesA3").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9MA3").EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnosesA4").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9MA4").EscapeQuotes(), data.AnswerOrEmptyString("M1020SymptomControlRating").EscapeQuotes(), data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis").EscapeQuotes(), data.AnswerOrEmptyString("M1020PrimaryDiagnosisDate").EscapeQuotes());
            for (int i = 1; i < 26; i++) if (data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).IsNotNullOrEmpty()) DiagnosisData += string.Format("'_M1022PrimaryDiagnosis{0}':'{2}','_M1022ICD9M{0}':'{3}','_M1024PaymentDiagnoses{1}3':'{4}','_M1022ICD9M{1}3':'{5}','_M1024PaymentDiagnoses{1}4':'{6}','_M1024ICD9M{1}4':'{7}','_M1022OtherDiagnose{0}Rating':'{8}','_485ExacerbationOrOnsetPrimaryDiagnosis{0}':'{9}','_M1022PrimaryDiagnosis{0}Date':'{10}',", i, (char)(i + 65), data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).EscapeQuotes(), data.AnswerOrEmptyString("M1022ICD9M" + i).EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(i + 65) + "3").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9M" + (char)(i + 65) + "3").EscapeQuotes(), data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(i + 65) + "4").EscapeQuotes(), data.AnswerOrEmptyString("M1024ICD9M" + (char)(i + 65) + "4").EscapeQuotes(), data.AnswerOrEmptyString("M1022OtherDiagnose" + i + "Rating").EscapeQuotes(), data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis" + i).EscapeQuotes(), data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i + "Date").EscapeQuotes()); DiagnosisData += "'Assessment':'" + assessment.TypeName + "'})";
            return DiagnosisData;
        }
        
        #endregion
    }
}
