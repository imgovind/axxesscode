﻿namespace Axxess.AgencyManagement.Application.Services
{
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.Entities;


    using Axxess.Core.Enums;


    public class PrivateDutyPhysicianOrderService : PhysicianOrderService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
        private PrivateDutyTaskRepository scheduleRepository;
        private PrivateDutyPhysicianOrderRepository noteRepository;
        private PrivateDutyEpisodeRepository episodeRepository;

        public PrivateDutyPhysicianOrderService(PrivateDutyDataProvider dataProvider) :
            base(dataProvider.TaskRepository, dataProvider.PhysicianOrderRepository, dataProvider.EpisodeRepository)
        {
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            base.physicianRepository = dataProvider.PhysicianRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.noteRepository = dataProvider.PhysicianOrderRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.Service = AgencyServices.PrivateDuty;
        }

        protected override void SetEpisode(PhysicianOrder order)
        {
            if (order.IsOrderForNextEpisode)
            {
                order.EpisodeRange = "Next Care Period";
            }
            else
            {
                var episode = episodeRepository.GetEpisodeDateRange(Current.AgencyId, order.PatientId, order.EpisodeId);
                if (episode != null)
                {
                    order.EpisodeRange = episode.StartDateFormatted + " - " + episode.EndDateFormatted;
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
            }
        }
    }
}
