﻿using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Repositories;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.Services
{
    public class PrivateDutyIncidentAccidentService : IncidentAccidentService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>
    {
         public PrivateDutyIncidentAccidentService(PrivateDutyDataProvider dataProvider) :
             base(dataProvider.TaskRepository, dataProvider.IncidentAccidentRepository, dataProvider.EpisodeRepository)
        {
            base.agencyRepository = dataProvider.AgencyRepository;
            base.patientRepository = dataProvider.PatientRepository;
            base.profileRepository = dataProvider.PatientProfileRepository;
            this.Service = AgencyServices.PrivateDuty;
        }
    }
}
