﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Common;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Enums;
    using Axxess.Log.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.Membership.Logging;

    using AAE = Axxess.AgencyManagement.Entities;

    public class PatientService : IPatientService
    {
        #region Private Members

        private readonly IDrugService drugService;
        // private readonly ILogRepository logRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPhysicianRepository physicianRepository;

        #endregion

        #region Constructor

        public PatientService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider,  IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            //this.assessmentService = assessmentService;
            //this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            // this.planofCareRepository = agencyManagementDataProvider.PlanofCareRepository;
            // this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;

            //this.mongoRepository = mongoDataProvider.MongoRepository;
        }

        #endregion

        #region Patient

        public bool AddPatient(Patient patient)
        {
            var result = false;
            if (patientRepository.Add(patient))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public Dictionary<string, string> GetPatientJsonByColumns(Guid id, params string[] columns)
        {
            return patientRepository.GetPatientJsonByColumns(Current.AgencyId, id, columns);
        }

        //public JsonViewData AddNewPatient(Patient patient, List<Profile> profiles, int status)
        //{
        //    var rules = new List<Validation>();
        //    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be saved" };
        //    if (patient != null)
        //    {
        //        if (patient.ServiceProvided != null && patient.ServiceProvided.Count > 0)
        //        {
        //            if (profiles.IsNotNullOrEmpty())
        //            {
        //                if (status == (int)PatientStatus.Active || status == (int)PatientStatus.Pending)
        //                {
        //                    var workflow = new CreatePatientWorkflow(patient, profiles, status == (int)PatientStatus.Pending);
        //                    if (workflow.IsCommitted)
        //                    {
        //                        viewData.isSuccessful = true;
        //                        viewData.errorMessage = status == (int)PatientStatus.Active ? "Patient was created successfully." : "Patient was saved successfully.";
        //                        viewData.IsCenterRefresh = status == (int)PatientStatus.Active ? true : false;
        //                        viewData.IsPendingPatientListRefresh = status == (int)PatientStatus.Pending ? true : false;
        //                        viewData.IsPatientListRefresh = true;
        //                        viewData.PatientStatus = status;
        //                    }
        //                    else
        //                    {
        //                        viewData.isSuccessful = false;
        //                        viewData.errorMessage = workflow.Message;
        //                    }
        //                    if (viewData.isSuccessful && !patient.ReferralId.IsEmpty())
        //                    {
        //                        if (referralRepository.SetStatus(Current.AgencyId, patient.ReferralId, new Referral { HomeHealthStatus = (int)ReferralStatus.Admitted, PrivateDutyStatus = (int)ReferralStatus.Admitted, Services = patient.Services }))
        //                        {
        //                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, patient.ReferralId.ToString(), LogType.Referral, LogAction.ReferralAdmitted, string.Empty);
        //                            viewData.IsReferralListRefresh = true;
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public bool AddPatient(Patient patient)
        //{
        //    var result = false;
        //    Patient patientOut = null;
        //    var admissionDateId = Guid.NewGuid();
        //    var service=0;
        //    patient.Services.ForEach(s =>
        //    {
        //        service |= s;
        //    });
        //    patient.Service = service;
        //    patient.AdmissionId = admissionDateId;
        //    if (patientRepository.Add(patient, out patientOut))
        //    {
        //        if (patientOut != null &&
        //            AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(
        //            new PatientAdmissionDate {
        //                Id = admissionDateId,
        //                AgencyId = Current.AgencyId,
        //                PatientId = patient.Id,
        //                PatientData = patientOut != null ? patientOut.ToXml() : string.Empty, 
        //                StartOfCareDate = patient.StartofCareDate,
        //                IsActive = true,
        //                IsDeprecated = false,
        //                Status = patientOut.Status }))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, (patient.ShouldCreateEpisode && !patient.UserId.IsEmpty()) && patient.Status == (int)PatientStatus.Active ? string.Empty : "Pending for admission");
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        public bool AdmitPatient(PendingPatient pending, out Patient patientOut)
        {
            return patientRepository.AdmitPatient(pending, out patientOut);
        }

        //public JsonViewData NonAdmitPatient(PendingPatient pending)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    Patient patientOut = null;
        //    var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var admissionDateId = Guid.NewGuid();
        //        pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.NonAdmitPatient(pending, out patientOut))
        //        {
        //            if (patient.AdmissionId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patient.Id, patient.AdmissionId);
        //                if (admissionData != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patientOut.Status;
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {

        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
        //                        viewData.isSuccessful = true;

        //                    }
        //                    else
        //                    {
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = false;
        //                viewData.IsNonAdmitPatientListRefresh = (int)PatientStatus.NonAdmission != patient.Status;
        //                viewData.IsPatientListRefresh = viewData.IsNonAdmitPatientListRefresh;
        //                viewData.PatientStatus = (int)PatientStatus.NonAdmission;
        //                viewData.IsPendingPatientListRefresh = false;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        public bool UpdateModal(Patient patient)
        {
            return patientRepository.UpdateModal(patient);
        }

        //public JsonViewData DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var admissionDateId = Guid.NewGuid();
        //        var oldStatus = patient.Status;
        //        var oldDischargeDate = patient.DischargeDate;
        //        var oldDischargeReason = patient.DischargeReason;

        //        patient.DischargeDate = dischargeDate;
        //        patient.Status = (int)PatientStatus.Discharged;
        //        patient.DischargeReasonId = dischargeReasonId;
        //        patient.DischargeReason = dischargeReason;
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        Patient patientOut = null;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (oldAdmissionDateId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, DischargeReasonId = dischargeReasonId, IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patient.DischargeDate = oldDischargeDate;
        //                    patient.Status = oldStatus;
        //                    patient.DischargeReason = oldDischargeReason;
        //                    patient.AdmissionId = oldAdmissionDateId;
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patient.Id, patient.AdmissionId);
        //                if (admissionData != null && patientOut != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patient.Status;
        //                    admissionData.StartOfCareDate = patient.StartofCareDate;
        //                    admissionData.DischargedDate = patient.DischargeDate;
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                        viewData.isSuccessful = true;

        //                    }
        //                    else
        //                    {
        //                        patient.DischargeDate = oldDischargeDate;
        //                        patient.Status = oldStatus;
        //                        patient.DischargeReason = oldDischargeReason;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, DischargeReasonId = dischargeReasonId, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                        viewData.isSuccessful = true;

        //                    }
        //                    else
        //                    {
        //                        patient.DischargeDate = oldDischargeDate;
        //                        patient.Status = oldStatus;
        //                        patient.DischargeReason = oldDischargeReason;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }

        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Discharged;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public JsonViewData ActivatePatient(Guid patientId)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    Patient patientOut = null;
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var oldStatus = patient.Status;
        //        patient.Status = (int)PatientStatus.Active;
        //        var admissionDateId = Guid.NewGuid();
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (patientOut != null)
        //            {
        //                if (oldAdmissionDateId.IsEmpty())
        //                {
        //                    if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
        //                    {
        //                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                        viewData.isSuccessful = true;
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);

        //                    }
        //                    else
        //                    {
        //                        patient.Status = oldStatus;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patient.Id, patient.AdmissionId);
        //                    if (admissionData != null)
        //                    {
        //                        admissionData.PatientData = patientOut.ToXml();
        //                        admissionData.Status = patient.Status;
        //                        admissionData.StartOfCareDate = patient.StartofCareDate;
        //                        if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                        {
        //                            viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                            viewData.isSuccessful = true;
        //                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
        //                        }
        //                        else
        //                        {
        //                            patient.Status = oldStatus;
        //                            patient.AdmissionId = oldAdmissionDateId;
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
        //                        {
        //                            viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                            viewData.isSuccessful = true;
        //                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
        //                        }
        //                        else
        //                        {
        //                            patient.Status = oldStatus;
        //                            patient.AdmissionId = oldAdmissionDateId;
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                patient.Status = oldStatus;
        //                patient.AdmissionId = oldAdmissionDateId;
        //                patientRepository.Update(patient);
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Active;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public JsonViewData ActivatePatient(Guid patientId, DateTime startOfCareDate)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    Patient patientOut = null;
        //    if (patient != null)
        //    {
        //        var oldStatus = patient.Status;
        //        var oldStartOfCareDate = patient.StartofCareDate;
        //        patient.Status = (int)PatientStatus.Active;
        //        patient.StartofCareDate = startOfCareDate;
        //        var admissionDateId = Guid.NewGuid();
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = admissionDateId;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
        //            {
        //                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
        //                viewData.isSuccessful = true;
        //            }
        //            else
        //            {
        //                patient.Status = oldStatus;
        //                patient.StartofCareDate = oldStartOfCareDate;
        //                patient.AdmissionId = oldAdmissionDateId;
        //                patientRepository.Update(patient);
        //            }

        //            //else
        //            //{
        //            //    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
        //            //    if (admissionData != null && patientOut != null)
        //            //    {
        //            //        admissionData.PatientData = patientOut.ToXml();
        //            //        admissionData.Status = patient.Status;
        //            //        admissionData.StartOfCareDate = patient.StartofCareDate;
        //            //        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
        //            //        {
        //            //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
        //            //            result = true;
        //            //        }
        //            //        else
        //            //        {
        //            //            patient.Status = oldStatus;
        //            //            patient.StartofCareDate = oldStartOfCareDate;
        //            //            patient.AdmissionId = oldAdmissionDateId;
        //            //            patientRepository.Update(patient);
        //            //        }
        //            //    }
        //            //    else
        //            //    {
        //            //        patient.Status = oldStatus;
        //            //        patient.StartofCareDate = oldStartOfCareDate;
        //            //        patient.AdmissionId = oldAdmissionDateId;
        //            //        patientRepository.Update(patient);
        //            //    }
        //            //}
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Active;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        public bool RemovePatient(Guid id)
        {
            return patientRepository.RemovePatient(Current.AgencyId, id);
        }

        public bool SetStatus(Guid patientId, PatientStatus status, AgencyServices service)
        {
            return patientRepository.SetStatus(Current.AgencyId, patientId, status, service);
        }

        //public JsonViewData SetPatientPending(Guid patientId)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        Patient patientOut = null;
        //        var oldStatus = patient.Status;
        //        patient.Status = (int)PatientStatus.Pending;
        //        var admissionDateId = Guid.NewGuid();
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (oldAdmissionDateId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Set Pending.", IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patient.Status = oldStatus;
        //                    patient.AdmissionId = oldAdmissionDateId;
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate(patient.Id, patient.AdmissionId);
        //                if (admissionData != null && patientOut != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patient.Status;
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patient.Status = oldStatus;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patient.Status = oldStatus;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPendingPatientListRefresh = (int)PatientStatus.Pending != oldStatus;
        //                viewData.IsPatientListRefresh = viewData.IsPendingPatientListRefresh;
        //                viewData.PatientStatus = (int)PatientStatus.Pending;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        public Patient GetPatientOnly(Guid patientId)
        {
            return patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        }

      

        public Patient GetPatientForPossibleNonAdmit(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var count = 0;
                if (patient.Services.IsAlone())
                {
                    if (patient.Services.Has(AgencyServices.HomeHealth))
                    {
                        patient.CurrentServices = AgencyServices.HomeHealth;
                    }
                    else if (patient.Services.Has(AgencyServices.PrivateDuty))
                    {
                        patient.CurrentServices = AgencyServices.PrivateDuty;
                    }
                    count++;
                }
                else
                {
                    if (patient.Services.Has(AgencyServices.HomeHealth) && PatientStatusFactory.PossibleNonAdmit().Contains(patient.HomeHealthStatus))
                    {
                        patient.CurrentServices = AgencyServices.HomeHealth;
                        count++;
                    }

                    if (patient.Services.Has(AgencyServices.PrivateDuty) && PatientStatusFactory.PossibleNonAdmit().Contains(patient.PrivateDutyStatus))
                    {
                        patient.CurrentServices |= AgencyServices.PrivateDuty;
                        count++;
                    }
                }
                if (count == 0)
                {
                    patient.CurrentServices = Current.PreferredService;
                }
                patient.IsOnlyOneService = count <= 1;
            }
            return patient;
        }

        public Patient GetForDelete(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var count = 0;
                patient.FormAction = FormAction.Delete;
                if (patient.Services.Has(AgencyServices.HomeHealth))
                {
                    if (patient.HomeHealthStatus != (int)PatientStatus.Deprecated)
                    {
                        patient.CurrentServices = AgencyServices.HomeHealth;
                        count++;
                    }
                }
                if (patient.Services.Has(AgencyServices.PrivateDuty))
                {
                    if (patient.PrivateDutyStatus != (int)PatientStatus.Deprecated)
                    {
                        patient.CurrentServices |= AgencyServices.PrivateDuty;
                        count++;
                    }
                }
                patient.TotalServiceToProcess = count;
            }
            return patient;
        }

        public Patient GetForRestore(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var count = 0;
                patient.FormAction = FormAction.Restore;
                if (patient.Services.Has(AgencyServices.HomeHealth))
                {
                    if ((patient.HomeHealthStatus == (int)PatientStatus.Deprecated))
                    {
                        patient.CurrentServices = AgencyServices.HomeHealth;
                        count++;
                    }
                }
                if (patient.Services.Has(AgencyServices.PrivateDuty))
                {
                    if ((patient.PrivateDutyStatus == (int)PatientStatus.Deprecated))
                    {
                        patient.CurrentServices |= AgencyServices.PrivateDuty;
                        count++;
                    }
                }
                patient.TotalServiceToProcess = count;
            }
            return patient;
        }

        //public PatientProfile GetProfileWithOutEpisodeInfo(Guid patientId)
        //{
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var patientProfile = new PatientProfile();
        //        patientProfile.Patient = patient;
        //        patientProfile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

        //        patientProfile.Allergies = GetAllergiesText(patientId);
        //        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
        //        if (physician != null)
        //        {
        //            patientProfile.Physician = physician;
        //        }
        //        patient.EmergencyContacts = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId);
        //        if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
        //        {
        //            patientProfile.EmergencyContact = patient.EmergencyContacts.OrderByDescending(e => e.IsPrimary).FirstOrDefault();
        //        }
        //        SetInsurance(patient);

        //        if (!patient.CaseManagerId.IsEmpty())
        //        {
        //            patient.CaseManagerName = UserEngine.GetName(patient.CaseManagerId, Current.AgencyId);
        //        }
        //        if (!patient.UserId.IsEmpty())
        //        {
        //            patientProfile.Clinician = UserEngine.GetName(patient.UserId, Current.AgencyId);
        //        }
        //        return patientProfile;
        //    }

        //    return null;
        //}


        //public PendingPatient GetPendingPatient(Guid id, NonAdmitTypes type)
        //{
        //    PendingPatient pending = null;
        //    if (type == NonAdmitTypes.Patient)
        //    {
        //        var patient = patientRepository.GetPatientOnly(id, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            pending = new PendingPatient
        //            {
        //                Id = patient.Id,
        //                DisplayName = patient.DisplayName,
        //                PatientIdNumber = patient.PatientIdNumber,
        //                StartofCareDate = patient.StartofCareDate,
        //                ReferralDate = patient.ReferralDate,
        //                CaseManagerId = patient.CaseManagerId,
        //                PrimaryInsurance = patient.PrimaryInsurance,
        //                SecondaryInsurance = patient.SecondaryInsurance,
        //                TertiaryInsurance = patient.TertiaryInsurance,
        //                Type = NonAdmitTypes.Patient,
        //                Payer = patient.Payer,
        //                UserId = patient.UserId,
        //                PrimaryPhysician = GetPrimaryPhysicianId(patient.Id, Current.AgencyId)
        //            };
        //        }
        //    }
        //    else
        //    {
        //        var referral = referralRepository.Get(Current.AgencyId, id);
        //        if (referral != null)
        //        {
        //            var physicians = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
        //            Physician primaryPhysician = physicians.FirstOrDefault(p => p.IsPrimary);
        //            pending = new PendingPatient
        //            {
        //                Id = referral.Id,
        //                DisplayName = referral.DisplayName,
        //                PatientIdNumber = string.Empty,
        //                StartofCareDate = DateTime.Today,
        //                ReferralDate = referral.ReferralDate,
        //                PrimaryInsurance = string.Empty,
        //                SecondaryInsurance = string.Empty,
        //                TertiaryInsurance = string.Empty,
        //                Type = NonAdmitTypes.Referral,
        //                UserId = referral.UserId,
        //                PrimaryPhysician = primaryPhysician != null ? primaryPhysician.Id : Guid.Empty
        //            };
        //        }
        //    }
        //    return pending;
        //}

        //public List<PatientData> GetPatients(Guid agencyId, Guid branchId, int serviceId, int status)
        //{
        //    var patients = patientRepository.All(agencyId, branchId,serviceId, status);
        //    patients.ForEach((PatientData patient) =>
        //    {
        //        patient.InsuranceName = AgencyInformationHelper.GetInsurance(patient.InsuranceId);
        //        if (patient.InsuranceName.IsNotNullOrEmpty())
        //        {
        //            patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
        //        }
        //    });
        //    return patients.ToList();
        //}

        //public List<PatientData> GetDeletedPatients(Guid branchId, int serviceId, bool IsInsuranceNameNeeded)
        //{
        //    var patients = patientRepository.AllDeleted(Current.AgencyId, branchId,serviceId);
        //    if (IsInsuranceNameNeeded)
        //    {
        //        patients.ForEach((PatientData patient) =>
        //        {
        //            patient.InsuranceName = AgencyInformationHelper.GetInsurance(patient.InsuranceId);
        //            if (patient.InsuranceName.IsNotNullOrEmpty())
        //            {
        //                patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
        //            }
        //        });
        //    }
        //    return patients.ToList();
        //}

        //public List<NonAdmit> GetNonAdmits()
        //{
        //    var list = new List<NonAdmit>();
        //    var patients = patientRepository.FindPatientOnly((int)PatientStatus.NonAdmission, Current.AgencyId);
        //    var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.NonAdmission).ToList();
        //    if (patients != null && patients.Count > 0)
        //    {
        //        patients.ForEach(p =>
        //        {
        //            InsuranceCache cache = null;
        //            if (p.PrimaryInsurance.IsNotNullOrEmpty() && p.PrimaryInsurance.IsInteger())
        //            {
        //                cache = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
        //            }
        //            list.Add(new NonAdmit
        //            {
        //                Id = p.Id,
        //                LastName = p.LastName,
        //                FirstName = p.FirstName,
        //                DateOfBirth = p.DOB,
        //                Phone = p.PhoneHomeFormatted,
        //                Type = NonAdmitTypes.Patient,
        //                MiddleInitial = p.MiddleInitial,
        //                PatientIdNumber = p.PatientIdNumber,
        //                NonAdmissionReason = p.NonAdmissionReason,
        //                NonAdmitDate = p.NonAdmissionDate,
        //                Gender = p.Gender,
        //                AddressCity = p.AddressCity,
        //                AddressLine1 = p.AddressLine1,
        //                AddressLine2 = p.AddressLine2,
        //                AddressStateCode = p.AddressStateCode,
        //                AddressZipCode = p.AddressZipCode,
        //                MedicareNumber = p.MedicaidNumber,
        //                InsuranceName = cache != null ? cache.Name : string.Empty,
        //                InsuranceNumber = p.PrimaryHealthPlanId,
        //                Comments = p.Comments
        //            });
        //        });
        //    }

        //    if (referrals != null && referrals.Count > 0)
        //    {
        //        referrals.ForEach(r =>
        //        {
        //            list.Add(new NonAdmit
        //            {
        //                Id = r.Id,
        //                LastName = r.LastName,
        //                FirstName = r.FirstName,
        //                DateOfBirth = r.DOB,
        //                Phone = r.PhoneHomeFormatted,
        //                Type = NonAdmitTypes.Referral,
        //                PatientIdNumber = string.Empty,
        //                NonAdmissionReason = r.NonAdmissionReason,
        //                NonAdmitDate = r.NonAdmissionDate,
        //                Comments = r.Comments
        //            });
        //        });
        //    }

        //    return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        //}

        //public List<PendingPatient> GetPendingPatients(Guid branchId, int serviceId)
        //{
        //    var patients = patientRepository.GetPendingByAgencyId(Current.AgencyId, branchId, serviceId);
        //    if (patients != null && patients.Count > 0)
        //    {
        //        var locations = agencyRepository.AgencyLocations(Current.AgencyId, patients.Select(p => p.AgencyLocationId).Distinct().ToList()) ?? new List<AgencyLocation>();
        //        patients.ForEach(p =>
        //        {
        //            if (p.PrimaryInsurance.IsNotNullOrEmpty())
        //            {
        //                var insurance = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
        //                if (insurance != null && insurance.Name.IsNotNullOrEmpty())
        //                {
        //                    p.PrimaryInsuranceName = insurance.Name;
        //                }
        //            }
        //            var location = locations.FirstOrDefault(l => l.Id == p.AgencyLocationId);
        //            if (location != null)
        //            {
        //                p.Branch = location.Name;
        //            }
        //        });
        //    }

        //    return patients;
        //}

        public bool IsPatientExist(Guid patientId)
        {
            return patientRepository.IsPatientExist(Current.AgencyId, patientId);
        }

        public bool IsPatientIdExist(string patientIdNumber)
        {
            return patientRepository.IsPatientIdExist(Current.AgencyId, patientIdNumber);
        }

        public bool IsMedicareExist(string medicareNumber)
        {
            return patientRepository.IsMedicareExist(Current.AgencyId, medicareNumber);
        }

        public bool IsMedicaidExist(string medicaidNumber)
        {
            return patientRepository.IsMedicaidExist(Current.AgencyId, medicaidNumber);
        }

        public bool IsMedicareExistForEdit(Guid patientId, string medicareNumber)
        {
            return patientRepository.IsMedicareExistForEdit(Current.AgencyId, patientId, medicareNumber);
        }

        public bool IsMedicaidExistForEdit(Guid patientId, string medicaidNumber)
        {
            return patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patientId, medicaidNumber);
        }

        public bool IsPatientIdExistForEdit(Guid patientId, string patientIdNumber)
        {
            return patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patientId, patientIdNumber);
        }

        public bool IsSSNExistForEdit(Guid patientId, string ssn)
        {
            return patientRepository.IsSSNExistForEdit(Current.AgencyId, patientId, ssn);
        }

        public string LastPatientId()
        {
            return patientRepository.LastPatientId(Current.AgencyId);
        }

        public string GetPatientNameById(Guid patientId)
        {
            return patientRepository.GetPatientNameById(patientId, Current.AgencyId);
        }

        public PatientNameViewData GetPatientNameViewData(Guid patientId)
        {
            var viewData = new PatientNameViewData();
             var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
             if (patient != null)
             {
                 viewData.DisplayName = patient.DisplayName;
                 viewData.PatientId = patientId;
                 viewData.AvailableService = patient.Services;
             }
            return viewData;
        }

        public void LinkPendingPatientToPhysician(PendingPatient pending, Patient patient)
        {
            if (!pending.PrimaryPhysician.IsEmpty())
            {
                var physicians = patient.PhysicianContacts.ToList();
                var physician = physicians.FirstOrDefault(p => p.Id == pending.PrimaryPhysician);
                if (physician != null)
                {
                    if (!physician.Primary)
                    {
                        SetPrimary(patient.Id, pending.PrimaryPhysician);
                    }
                }
                else
                {
                    patientRepository.Link(patient.Id, pending.PrimaryPhysician, true);
                }
            }
        }

        public AgencyServices GetAccessibleServices(Guid patientId)
        {
            return patientRepository.GetAccessibleServices(Current.AgencyId, patientId);
        }

        public Patient GetPatientForPending(Guid Id)
        {
            var patient = patientRepository.GetPatientOnly(Id, Current.AgencyId);
            if (patient != null)
            {
                var physicianId = physicianRepository.GetPatientPrimaryOrFirstPhysicianId(Current.AgencyId, Id);
                if (!physicianId.IsEmpty())
                {
                    patient.Physician = new AgencyPhysician();
                    patient.Physician.Id = physicianId;
                }
            }
            return patient;
        }

        //public JsonViewData AdmitPending(PendingPatient pending)
        //{
        //    var data = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        Patient patientOut = null;
        //        var admissionDateId = Guid.NewGuid();

        //        patient.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.AdmitPatient(pending, out patientOut))
        //        {
        //            if (patientOut != null)
        //            {
        //                LinkPendingPatientToPhysicain(pending, patient);

        //                if (oldAdmissionDateId.IsEmpty())
        //                {
        //                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
        //                    {
        //                        var medId = Guid.NewGuid();
        //                        patient.EpisodeStartDate = pending.EpisodeStartDate;
        //                        if (this.CreateMedicationProfile(patient, medId))
        //                        {
        //                            if (this.CreateEpisodeAndClaims(patient))
        //                            {
        //                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
        //                                data.isSuccessful = true;
        //                            }
        //                            else
        //                            {
        //                                patientRepository.Update(patient);
        //                                patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
        //                                patientRepository.RemoveMedicationProfile(Current.AgencyId, patient.Id, medId);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
        //                    if (admissionData != null)
        //                    {
        //                        var oldPatientData = admissionData.PatientData;
        //                        var oldSoc = admissionData.StartOfCareDate;
        //                        var oldAdmissionStatus = admissionData.Status;

        //                        admissionData.PatientData = patientOut.ToXml();
        //                        admissionData.StartOfCareDate = pending.StartofCareDate;
        //                        admissionData.Status = (int)PatientStatus.Active;
        //                        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
        //                        {
        //                            var medId = Guid.NewGuid();
        //                            patient.EpisodeStartDate = pending.EpisodeStartDate;
        //                            if (this.CreateMedicationProfile(patient, medId))
        //                            {
        //                                if (this.CreateEpisodeAndClaims(patient))
        //                                {
        //                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
        //                                    data.isSuccessful = true;
        //                                }
        //                                else
        //                                {
        //                                    patientRepository.Update(patient);
        //                                    admissionData.Status = oldAdmissionStatus;
        //                                    admissionData.PatientData = oldPatientData;
        //                                    admissionData.StartOfCareDate = oldSoc;
        //                                    patientRepository.UpdatePatientAdmissionDate(admissionData);
        //                                    patientRepository.RemoveMedicationProfile(Current.AgencyId, patient.Id, medId);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                admissionData.Status = oldAdmissionStatus;
        //                                admissionData.PatientData = oldPatientData;
        //                                admissionData.StartOfCareDate = oldSoc;
        //                                patientRepository.UpdatePatientAdmissionDate(admissionData);
        //                                patientRepository.Update(patient);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
        //                        {
        //                            var medId = Guid.NewGuid();
        //                            patient.EpisodeStartDate = pending.EpisodeStartDate;
        //                            if (this.CreateMedicationProfile(patient, medId))
        //                            {
        //                                if (this.CreateEpisodeAndClaims(patient))
        //                                {
        //                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
        //                                    data.isSuccessful = true;
        //                                }
        //                                else
        //                                {
        //                                    patientRepository.Update(patient);
        //                                    patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
        //                                    patientRepository.RemoveMedicationProfile(Current.AgencyId, patient.Id, medId);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
        //                                patientRepository.Update(patient);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                patientRepository.Update(patient);
        //            }
        //        }
        //        if (data.isSuccessful)
        //        {
        //            data.IsCenterRefresh = true;

        //            data.IsPatientListRefresh = true;
        //            data.PatientStatus = (int)PatientStatus.Active;
        //            data.IsReferralListRefresh = false;
        //            data.IsNonAdmitPatientListRefresh = patient.Status == (int)PatientStatus.NonAdmission;
        //            data.IsPendingPatientListRefresh = patient.Status == (int)PatientStatus.Pending;
        //        }
        //    }

        //    return data;
        //}






        //public JsonViewData AdmitReferral(PendingPatient pending)
        //{
        //    var data = new JsonViewData { isSuccessful = false };
        //    var referral = referralRepository.Get(Current.AgencyId, pending.Id);
        //    if (referral != null)
        //    {
        //        Patient patientOut = null;
        //        var admissionDateId = Guid.NewGuid();

        //        pending.AdmissionId = admissionDateId;
        //        if (patientRepository.AdmitReferral(pending, out patientOut))
        //        {
        //            if (patientOut != null)
        //            {
        //                LinkReferralPhysician(pending, referral);
        //                if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Referral Admitted.", IsDeprecated = false, IsActive = true }))
        //                {
        //                    var medId = Guid.NewGuid();

        //                    if (this.CreateMedicationProfile(patientOut, medId))
        //                    {
        //                        patientOut.EpisodeStartDate = pending.EpisodeStartDate;
        //                        patientOut.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
        //                        if (this.CreateEpisodeAndClaims(patientOut))
        //                        {
        //                            Auditor.AddGeneralLog(LogDomain.Patient, referral.Id, referral.Id.ToString(), LogType.Patient, LogAction.ReferralAdmitted, string.Empty);
        //                            data.isSuccessful = true;
        //                        }
        //                        else
        //                        {

        //                            referralRepository.UpdateModal(referral);
        //                            patientRepository.RemovePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
        //                            patientRepository.RemoveMedicationProfile(Current.AgencyId, referral.Id, medId);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        patientRepository.RemovePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
        //                        referralRepository.UpdateModal(referral);
        //                    }
        //                }
        //                else
        //                {
        //                    referralRepository.UpdateModal(referral);
        //                }
        //            }
        //            else
        //            {
        //                referralRepository.UpdateModal(referral);
        //            }
        //        }
        //        if (data.isSuccessful)
        //        {
        //            data.IsCenterRefresh = true;
        //            data.IsNonAdmitPatientListRefresh = false;
        //            data.IsPatientListRefresh = true;
        //            data.PatientStatus = (int)PatientStatus.Active;
        //            data.IsReferralListRefresh = true;
        //        }
        //    }
        //    return data;
        //}







        //public JsonViewData DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReason)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var admissionDateId = Guid.NewGuid();
        //        var oldStatus = patient.Status;
        //        var oldDischargeDate = patient.DischargeDate;
        //        var oldDischargeReason = patient.DischargeReason;
        //        patient.DischargeDate = dischargeDate;
        //        patient.Status = (int)PatientStatus.Discharged;
        //        //patient.DischargeReason = dischargeReason;
        //        patient.DischargeReason = dischargeReason;
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        Patient patientOut = null;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (oldAdmissionDateId.IsEmpty())
        //            {
        //                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
        //                {

        //                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patient.DischargeDate = oldDischargeDate;
        //                    patient.Status = oldStatus;
        //                    patient.DischargeReason = oldDischargeReason;
        //                    patient.AdmissionId = oldAdmissionDateId;
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
        //                if (admissionData != null && patientOut != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patient.Status;
        //                    admissionData.StartOfCareDate = patient.StartofCareDate;
        //                    admissionData.DischargedDate = patient.DischargeDate;
        //                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
        //                    {

        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patient.DischargeDate = oldDischargeDate;
        //                        patient.Status = oldStatus;
        //                        patient.DischargeReason = oldDischargeReason;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patient.DischargeDate = oldDischargeDate;
        //                        patient.Status = oldStatus;
        //                        patient.DischargeReason = oldDischargeReason;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Discharged;
        //            }
        //        }

        //    }
        //    return viewData;
        //}

        #endregion

        #region Insurance







        //public PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType)
        //{
        //    var viewData = new PatientInsuranceInfoViewData();
        //    viewData.InsuranceType = insuranceType.ToTitleCase();
        //    if (!patientId.IsEmpty())
        //    {
        //        var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            if (insuranceType.IsEqual("Primary"))
        //            {
        //                if (patient.PrimaryInsurance == insuranceId)
        //                {
        //                    viewData.HealthPlanId = patient.PrimaryHealthPlanId;
        //                    viewData.GroupName = patient.PrimaryGroupName;
        //                    viewData.GroupId = patient.PrimaryGroupId;
        //                    viewData.Relationship = patient.PrimaryRelationship;
        //                }
        //                viewData.InsuranceType = "Primary";
        //            }
        //            else if (insuranceType.IsEqual("Secondary"))
        //            {
        //                if (patient.SecondaryInsurance == insuranceId)
        //                {
        //                    viewData.HealthPlanId = patient.SecondaryHealthPlanId;
        //                    viewData.GroupName = patient.SecondaryGroupName;
        //                    viewData.GroupId = patient.SecondaryGroupId;
        //                    viewData.Relationship = patient.SecondaryRelationship;
        //                }
        //                viewData.InsuranceType = "Secondary";

        //            }
        //            else if (insuranceType.IsEqual("Tertiary"))
        //            {
        //                if (patient.TertiaryInsurance == insuranceId)
        //                {
        //                    viewData.HealthPlanId = patient.TertiaryHealthPlanId;
        //                    viewData.GroupId = patient.TertiaryGroupId;
        //                    viewData.GroupName = patient.TertiaryGroupName;
        //                    viewData.Relationship = patient.TertiaryRelationship;
        //                }
        //                viewData.InsuranceType = "Tertiary";
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        #endregion

        #region Referral

        public bool AdmitReferral(PendingPatient pending, out Patient patientOut)
        {
            return patientRepository.AdmitReferral(pending, out patientOut);
        }

        public void LinkReferralPhysician(Referral referral)
        {
            var physicains = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
            //if (!pending.PrimaryPhysician.IsEmpty() && !physicains.Exists(p => p.Id == pending.PrimaryPhysician))
            //{
            //    physicains.ForEach(p => { p.IsPrimary = false; });
            //    physicains.Add(new Physician() { Id = pending.PrimaryPhysician, IsPrimary = true });
            //}
            if (physicains != null && physicains.Count > 0)
            {
                var patientPhysicians = new List<PatientPhysician>();
                physicains.ForEach(p =>
                {
                    patientPhysicians.Add(new PatientPhysician { IsPrimary = p.IsPrimary, PhysicianId = p.Id, PatientId = referral.Id });
                });
                patientRepository.UpdateMultiplePatientPhysicians(patientPhysicians);
                //physicains.ForEach(p =>
                //{
                //    patientRepository.Link(referral.Id, p.Id, p.IsPrimary);
                //});
            }
        }


        #endregion

        #region Patient Photo

        public bool AddPhoto(Guid patientId, HttpPostedFileBase httpFile)
        {
            bool result = false;
            Guid assetId;
            var isAssetSaved = AssetHelper.AddAsset(httpFile, out assetId, true);
            if (isAssetSaved)
            {
                if (patientRepository.SetPhotoId(Current.AgencyId, patientId, assetId))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                    result = true;
                }
                else
                {
                    AssetHelper.Delete(assetId);
                }
            }

            return result;
        }

        public bool IsValidImage(HttpPostedFileBase file)
        {
            var result = false;
           
                
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var s = System.IO.Path.GetExtension(file.FileName);
                    if (s != null)
                    {
                        var fileExtension = s.ToLower();
                        if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
                        {
                            var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (allowedExtensions.IsNotNullOrEmpty())
                            {
                                allowedExtensions.ForEach(extension =>
                                    {
                                        if (fileExtension.IsEqual(extension))
                                        {
                                            result = true;
                                            return;
                                        }
                                    });
                            }
                        }
                    }
                }
            
            return result;
        }

        public bool UpdatePatientForPhotoRemove(Guid patientId)
        {
            var result = false;
            Guid photoId = patientRepository.GetPhotoId(Current.AgencyId, patientId);
            if (!photoId.IsEmpty())
            {
                if (AssetHelper.Delete(photoId))
                {
                    if (patientRepository.SetPhotoId(Current.AgencyId, patientId, Guid.Empty))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                        result = true;
                    }
                    else
                    {
                        AssetHelper.Restore(photoId);
                    }
                }
            }
            return result;
        }

        #endregion


        //#region Admission

        //public JsonViewData AdmissionPatientNew([Bind] Patient patient)
        //{
        //    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient Admission information could not be edited" };
        //    if (patient != null && !patient.Id.IsEmpty())
        //    {
        //        var rules = new List<Validation>();

        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
        //        rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of Birth  for the patient is not in the valid range.  <br/>"));
        //        rules.Add(new Validation(() => patient.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
        //        if (patient.PatientIdNumber.IsNotNullOrEmpty())
        //        {
        //            bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
        //            rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
        //        }
        //        if (patient.MedicareNumber.IsNotNullOrEmpty())
        //        {
        //            bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
        //            rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
        //        }
        //        if (patient.MedicaidNumber.IsNotNullOrEmpty())
        //        {
        //            bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
        //            rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
        //        }
        //        if (patient.SSN.IsNotNullOrEmpty())
        //        {
        //            bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
        //            rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
        //        }
        //        rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
        //        rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DischargeDate.ToString()), "Patient discharge date is required.  <br/>"));
        //        rules.Add(new Validation(() => !patient.DischargeDate.ToString().IsValidDate(), "Patient discharge date is not in valid format.  <br/>"));

        //        if (patient.Ethnicities.IsNullOrEmpty())
        //        {
        //            rules.Add(new Validation(() => patient.EthnicRaces.Count == 0, "Patient Race/Ethnicity is required."));
        //        }

        //        if (patient.PaymentSource.IsNullOrEmpty())
        //        {
        //            rules.Add(new Validation(() => patient.PaymentSources.Count == 0, "Patient Payment Source is required."));
        //        }
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
        //        rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));


        //        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
        //        {
        //            rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
        //        }
        //        if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
        //        {
        //            rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
        //        }

        //        if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
        //        {
        //            rules.Add(new Validation(() => patient.TertiaryInsurance.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
        //        }
        //        rules.Add(new Validation(() => !this.IsValidAdmissionPeriod(patient.Id, patient.StartofCareDate, patient.DischargeDate), "Admission period (SOC and DC date range)  is not in the valid date range."));
        //        var entityValidator = new EntityValidator(rules.ToArray());
        //        entityValidator.Validate();
        //        if (entityValidator.IsValid)
        //        {

        //            patient.AgencyId = Current.AgencyId;
        //            patient.Encode();// setting string arrays to one field
        //            if (patientRepository.PatientAdmissionAdd(patient))
        //            {
        //                viewData.isSuccessful = true;
        //                viewData.errorMessage = "Your Data successfully added";
        //            }
        //            else
        //            {
        //                viewData.isSuccessful = false;
        //                viewData.errorMessage = "Error in editing the data.";
        //            }
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = entityValidator.Message;
        //        }
        //    }
        //  return  viewData;
        //}


        //public JsonViewData AdmissionPatientEdit(Patient patient)
        //{
        //    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient Admission information could not be edited" };
        //    if (patient != null && !patient.Id.IsEmpty() && !patient.AdmissionId.IsEmpty())
        //    {
        //        var currentPatientData = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
        //        if (currentPatientData != null)
        //        {
        //            var rules = new List<Validation>();
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
        //            rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
        //            rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
        //            rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
        //            rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));

        //            if (patient.PatientIdNumber.IsNotNullOrEmpty())
        //            {
        //                bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
        //                rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
        //            }
        //            if (patient.MedicareNumber.IsNotNullOrEmpty())
        //            {
        //                bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
        //                rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
        //            }
        //            if (patient.MedicaidNumber.IsNotNullOrEmpty())
        //            {
        //                bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
        //                rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
        //            }
        //            if (patient.SSN.IsNotNullOrEmpty())
        //            {
        //                bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
        //                rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
        //            }
        //            if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
        //            {
        //                rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
        //            }
        //            if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
        //            {
        //                rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
        //            }

        //            if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
        //            {
        //                rules.Add(new Validation(() => patient.TertiaryInsurance.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
        //            }
        //            if (!currentPatientData.AdmissionId.IsEmpty() && (currentPatientData.AdmissionId != patient.AdmissionId || (currentPatientData.AdmissionId == patient.AdmissionId && currentPatientData.Status == (int)PatientStatus.Discharged)))
        //            {
        //                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DischargeDate.ToString()), "Patient discharge date is required.  <br/>"));
        //                rules.Add(new Validation(() => !patient.DischargeDate.ToString().IsValidDate(), "Patient discharge date is not in valid format.  <br/>"));
        //                rules.Add(new Validation(() => patient.StartofCareDate.Date > patient.DischargeDate.Date, "Patient discharge date has to be greater than Start of care date .  <br/>"));
        //            }
        //            rules.Add(new Validation(() => !IsValidAdmissionPeriod(patient.AdmissionId, patient.Id, patient.StartofCareDate, patient.DischargeDate), "Admission period (SOC and DC date range)  is not in the valid date range."));

        //            var entityValidator = new EntityValidator(rules.ToArray());
        //            entityValidator.Validate();
        //            if (entityValidator.IsValid)
        //            {

        //                patient.AgencyId = Current.AgencyId;
        //                patient.Encode();// setting string arrays to one field
        //                if (patientRepository.PatientAdmissionEdit(patient))
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "Your data successfully edited.";
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = "Error in editing the data.";
        //                }
        //            }
        //            else
        //            {
        //                viewData.isSuccessful = false;
        //                viewData.errorMessage = entityValidator.Message;
        //            }
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = "Patient data not found. Try again.";
        //        }
        //    }
        //    return viewData;
        //}


        //public bool AddPatientAdmissionDate(PatientAdmissionDate managedDate)
        //{
        //    return patientRepository.AddPatientAdmissionDate(managedDate);
        //}

        //public bool UpdatePatientAdmissionDate(PatientAdmissionDate admissionData)
        //{
        //    return patientRepository.UpdatePatientAdmissionDate(admissionData);
        //}

        //public bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id)
        //{
        //    var result = false;
        //    try
        //    {
        //        var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            var oldAdmissionId = patient.AdmissionId;
        //            var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
        //            if (admission != null)
        //            {
        //                patient.AdmissionId = admission.Id;
        //                admission.PatientData = patient.ToXml();
        //                if (patientRepository.Update(patient))
        //                {
        //                    if (patientRepository.UpdatePatientAdmissionDateModal(admission))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmissionPeriodEdited, string.Empty);
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodSetCurrent, string.Empty);

        //                        return true;
        //                    }
        //                    else
        //                    {
        //                        patient.AdmissionId = oldAdmissionId;
        //                        patientRepository.Update(patient);
        //                        return false;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //    return result;
        //}

        //public JsonViewData DeletePatientAdmission(Guid patientId, Guid Id)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be deleted." };
        //    var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
        //    if (admission != null)
        //    {
        //        admission.IsDeprecated = true;
        //        admission.IsActive = false;
        //        if (patientRepository.UpdatePatientAdmissionDateModal(admission))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodDeleted, string.Empty);
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "The patient admission period is deleted ";
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = "The patient admission period is not deleted ";
        //        }
        //    }
        //    else
        //    {
        //        viewData.isSuccessful = false;
        //        viewData.errorMessage = "The patient admission period not found to delete. Try again.";
        //    }
        //    return viewData;
        //}

        //public bool RemovePatientAdmissionDate(Guid patientId, Guid Id)
        //{
        //    return patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patientId, Id);
        //}

        //public bool RemovePatientAdmissionDates(Guid patientId)
        //{
        //    return patientRepository.RemovePatientAdmissionDates(Current.AgencyId, patientId);
        //}

        //public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        //{
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    var admission = new PatientAdmissionDate();
        //    if (patient != null)
        //    {
        //        admission = GetIfExitOrCreate(patient);
        //    }
        //    return admission;
        //}

        //public PatientAdmissionDate GetIfExitOrCreate(Patient patient)
        //{
        //    var admission = new PatientAdmissionDate();
        //    if (patient != null)
        //    {
        //        var admissionId = Guid.NewGuid();
        //        var oldAdmissionId = patient.AdmissionId;
        //        patient.AdmissionId = admissionId;
        //        admission = new PatientAdmissionDate
        //        {
        //            Id = admissionId,
        //            AgencyId = Current.AgencyId,
        //            PatientId = patient.Id,
        //            StartOfCareDate = patient.StartofCareDate,
        //            DischargedDate = patient.DischargeDate,
        //            Status = patient.Status,
        //            IsActive = true,
        //            IsDeprecated = false,
        //            Created = DateTime.Now,
        //            Modified = DateTime.Now,
        //            PatientData = patient.ToXml()
        //        };
        //        if (patientRepository.Update(patient))
        //        {
        //            if (patientRepository.AddPatientAdmissionDate(admission))
        //            {
        //                return admission;
        //            }
        //            else
        //            {
        //                patient.AdmissionId = oldAdmissionId;
        //                patientRepository.Update(patient);
        //            }
        //        }
        //    }
        //    return admission;
        //}

        //public Patient GetAdmissionPatientInfo(Guid patientId, Guid Id, string Type)
        //{
        //    var admissionPatient = new Patient();
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        if (Type.IsEqual("edit") && !Id.IsEmpty())
        //        {
        //            var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
        //            if (admission != null && admission.PatientData.IsNotNullOrEmpty())
        //            {
        //                admissionPatient = admission.PatientData.ToObject<Patient>() ?? new Patient();
        //                admissionPatient.AdmissionId = admission.Id;
        //                admissionPatient.Id = admission.PatientId;
        //                admissionPatient.StartofCareDate = admission.StartOfCareDate;
        //                admissionPatient.DischargeDate = admission.DischargedDate;
        //                admissionPatient.Status = (!patient.AdmissionId.IsEmpty() && (admissionPatient.AdmissionId != patient.AdmissionId || (admissionPatient.AdmissionId == patient.AdmissionId && patient.Status == (int)PatientStatus.Discharged))) ? (int)PatientStatus.Discharged : (int)PatientStatus.Active;

        //            }
        //        }
        //        else if (Type.IsEqual("new"))
        //        {
        //            admissionPatient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //            admissionPatient.AdmissionId = Guid.Empty;
        //        }
        //    }
        //    return admissionPatient;
        //}

        //public PatientAdmissionDate GetPatientAdmissionDate(Guid patientId,  Guid Id)
        //{
        //    return patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
        //}



        //public IList<PatientAdmissionDate> GetPatientAdmissionPeriods(Guid patientId)
        //{
        //    return patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
        //}

        //public bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        //{
        //    bool result = true;
        //    var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
        //    if (admissionPeriods != null && admissionPeriods.Count > 0)
        //    {
        //        foreach (var a in admissionPeriods)
        //        {
        //            if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
        //            {
        //                if (a.Id == admissionId)
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
        //                    {
        //                        result = false;
        //                        break;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                continue;
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        //{
        //    bool result = true;
        //    var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
        //    if (admissionPeriods != null && admissionPeriods.Count > 0)
        //    {
        //        foreach (var a in admissionPeriods)
        //        {
        //            if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
        //            {
        //                if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //            else
        //            {
        //                continue;
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public void SetMasterCalendarAdmissionInfo(CalendarViewData data)
        //{
        //    var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, data.EpisodeAdmissionId);
        //    if (admission != null)
        //    {
        //        data.StartOfCareDate = admission.StartOfCareDate;
        //        if (admission.PatientData.IsNotNullOrEmpty())
        //        {
        //            var patient = admission.PatientData.ToObject<Patient>();
        //            if (patient != null)
        //            {
        //                data.PatientIdNumber = patient.PatientIdNumber;
        //                data.DisplayName = patient.DisplayNameWithMi;
        //            }
        //        }
        //    }
        //}

        //public IList<SelectListItem> GetPatientAdmissionDateSelectList(Guid patientId)
        //{
        //    return patientRepository.GetPatientAdmissionDateSelectList(Current.AgencyId, patientId);
        //}

        //#endregion

        #region Emergency Contact

        public bool AddPrimaryEmergencyContact(Patient patient)
        {
            var result = false;
            if (patientRepository.AddEmergencyContact(patient.EmergencyContact) && patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, patient.EmergencyContact.Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, "The Contact is set as primary.");
                result = true;
            }
            return result;
        }

        public bool AddEmergencyContact(PatientEmergencyContact emergencyContact)
        {
            var result = false;
            if (patientRepository.AddEmergencyContact(emergencyContact))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, emergencyContact.PatientId, emergencyContact.PatientId.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                result = true;
            }
            return result;
        }


        public JsonViewData AddNewEmergencyContact(Guid patientId, PatientEmergencyContact emergencyContact)
        {
            var viewData = new List<Validation>(){
                              new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. <br/>"),
                              new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required.  <br/>"),
                              new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format.  <br/>"),
                              new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required.  <br/>")
                             }.Validate<JsonViewData>();
            if (viewData.isSuccessful)
            {
                if (patientRepository.IsPatientExist(Current.AgencyId, patientId))
                {
                    emergencyContact.PatientId = patientId;
                    emergencyContact.AgencyId = Current.AgencyId;
                    if (patientRepository.AddEmergencyContact(emergencyContact))
                    {
                        if (emergencyContact.IsPrimary)
                        {
                            patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patientId, emergencyContact.Id);
                        }
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "New emergency contact created successful.";
                    }
                    else
                    {
                        viewData.errorMessage = "A problem occured while editing the data. Try Again.";
                    }
                }
                else
                {
                    viewData.errorMessage = "The Patient could not be found. Try Again.";
                }
            }
            return viewData;
        }

        public JsonViewData EditEmergencyContact(PatientEmergencyContact emergencyContact)
        {
            var viewData = new List<Validation>(){
                             new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. <br/>"),
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required.  <br/>"),
                           new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format.  <br/>"),
                           new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required.  <br/>")
                             }.Validate<JsonViewData>();
            if (viewData.isSuccessful)
            {
                if (patientRepository.EditEmergencyContact(Current.AgencyId, emergencyContact))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, emergencyContact.PatientId, emergencyContact.PatientId.ToString(), LogType.Patient, LogAction.EmergencyContactEdited, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Emergency contact successfully updated.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Emergency contact could not be updated. Please try again.";
                }
            }
            return viewData;
        }

        public bool UpdateEmergencyContact(Guid patientId, PatientEmergencyContact emergencyContact)
        {
            return patientRepository.UpdateEmergencyContact(Current.AgencyId, patientId, emergencyContact);
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            if (patientRepository.RemoveEmergencyContact(Current.AgencyId, patientId, Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.EmergencyContactDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public bool RemoveEmergencyContacts(Guid patientId)
        {
            return patientRepository.RemoveEmergencyContacts(Current.AgencyId, patientId);
        }

        public PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid Id)
        {
            return patientRepository.GetEmergencyContact(patientId, Id);
        }

        public IList<PatientEmergencyContact> GetEmergencyContacts(Guid PatientId)
        {
            return patientRepository.GetEmergencyContacts(Current.AgencyId, PatientId);
        }


        #endregion

        #region Medication Profile

        public bool CreateMedicationProfile(Patient patient, Guid medId)
        {
            var medicationProfile = new MedicationProfile { AgencyId = Current.AgencyId, Id = medId, Medication = new List<Medication>().ToXml(), PharmacyName = patient.PharmacyName, PharmacyPhone = patient.PharmacyPhone, PatientId = patient.Id, Created = DateTime.Now, Modified = DateTime.Now };
            if (CreateMedicationProfile(medicationProfile))
            {
                return true;
            }
            return false;
        }

        public bool CreateMedicationProfile(MedicationProfile medicationProfile)
        {
            if (patientRepository.AddNewMedicationProfile(medicationProfile))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfile, LogAction.MedicationProfileAdded, string.Empty);
                return true;
            }
            return false;
        }

        public JsonViewData SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Medication Profile could not be updated. Try Again." };
            medicationProfile.AgencyId = Current.AgencyId;
            if (patientRepository.SaveMedicationProfile(medicationProfile))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Medication Profile has been updated successfully.";
            }
            return viewData;
        }

        public bool RemoveMedicationProfile(Guid patientId, Guid Id)
        {
            return patientRepository.RemoveMedicationProfile(Current.AgencyId, patientId, Id);
        }

        public MedicationProfile GetMedicationProfileByPatient(Guid patientId)
        {
            return patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
        }

        public MedicationProfile GetMedicationProfile(Guid medicationProfileId)
        {
            return patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
        }

        public MedicationProfile GetMedicationProfileWithPermission(Guid medicationProfileId)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                var permissions = Current.CategoryService(Current.AcessibleServices, ParentPermission.MedicationProfile,
                    new int[] { (int)PermissionActions.ViewList,
                    (int)PermissionActions.Add,
                    (int)PermissionActions.Edit,
                    (int)PermissionActions.Sign, 
                    (int)PermissionActions.Delete, 
                    (int)PermissionActions.Reactivate,
                    (int)PermissionActions.Deactivate 
                    });
                if (permissions.IsNotNullOrEmpty())
                {
                    var isNotFrozen = !Current.IsAgencyFrozen;
                    medicationProfile.IsUserCanViewList = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None).HasValidValue();
                    medicationProfile.IsUserCanAdd = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None).HasValidValue();
                    medicationProfile.IsUserCanEdit = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None).HasValidValue();
                    medicationProfile.IsUserCanSign = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Sign, AgencyServices.None).HasValidValue();
                    medicationProfile.IsUserCanDelete = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None).HasValidValue();
                    medicationProfile.IsUserCanReactivate = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Reactivate, AgencyServices.None).HasValidValue();
                    medicationProfile.IsUserCanDiscontinue = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Deactivate, AgencyServices.None).HasValidValue();
                }
            }
            return medicationProfile;
        }

        public MedicationListViewData GetMedicationListViewData(Guid medicationProfileId)
        {
            var viewData = new MedicationListViewData();
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                viewData.Id = medicationProfileId;
                viewData.PatientId = medicationProfile.PatientId;
                var list = medicationProfile.Medication.ToObject<List<Medication>>() ?? new List<Medication>();
                if (list.IsNotNullOrEmpty())
                {
                    viewData.List = list.OrderByDescending(m => m.StartDateSortable).ToList();
                }
                var permissions = Current.CategoryService(Current.AcessibleServices, ParentPermission.MedicationProfile, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Add, (int)PermissionActions.Edit, (int)PermissionActions.Sign, (int)PermissionActions.Delete, (int)PermissionActions.Reactivate, (int)PermissionActions.Deactivate });
                if (permissions.IsNotNullOrEmpty())
                {
                    var isNotFrozen = !Current.IsAgencyFrozen;
                    viewData.IsUserCanViewList = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None).HasValidValue();
                    viewData.IsUserCanAdd = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None).HasValidValue();
                    if (viewData.List.IsNotNullOrEmpty())
                    {
                        viewData.IsUserCanEdit = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None).HasValidValue();
                        viewData.IsUserCanSign = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Sign, AgencyServices.None).HasValidValue();
                        viewData.IsUserCanDelete = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None).HasValidValue();
                        viewData.IsUserCanReactivate = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Reactivate, AgencyServices.None).HasValidValue();
                        viewData.IsUserCanDiscontinue = isNotFrozen && permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Deactivate, AgencyServices.None).HasValidValue();
                    }
                }
            }
            return viewData;
        }

        public string GetMedicationProfileText(Guid patientId)
        {
            var medications = string.Empty;
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medications = medicationProfile.ToString();
            }
            return medications;
        }

        public bool IsMedicationProfileExist(Guid patientId)
        {
            return patientRepository.IsMedicationProfileExist(Current.AgencyId, patientId);
        }

        #endregion

        #region Medication

        public bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    medication.Id = Guid.NewGuid();
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                    if (medicationProfile.Medication.IsNullOrEmpty())
                    {
                        var newList = new List<Medication>() { medication };
                        medicationProfile.Medication = newList.ToXml();
                    }
                    else
                    {
                        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                        existingList.Add(medication);
                        medicationProfile.Medication = existingList.ToXml();
                    }
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
                {
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == medication.Id))
                    {
                        var exisitingMedication = existingList.Single(m => m.Id == medication.Id);
                        if (exisitingMedication != null)
                        {
                            var logAction = LogAction.MedicationUpdated;
                            if (exisitingMedication.StartDate != medication.StartDate || exisitingMedication.Route != medication.Route || exisitingMedication.Frequency != medication.Frequency || exisitingMedication.MedicationDosage != medication.MedicationDosage)
                            {
                                medication.Id = Guid.NewGuid();
                                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                                medication.MedicationType = new MedicationType { Value = "C", Text = MedicationTypeEnum.C.GetDescription() };
                                medication.MedicationCategory = exisitingMedication.MedicationCategory;
                                existingList.Add(medication);

                                exisitingMedication.MedicationCategory = "DC";
                                exisitingMedication.DCDate = DateTime.Now;
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                                logAction = LogAction.MedicationUpdatedWithDischarge;
                            }
                            else
                            {
                                exisitingMedication.IsLongStanding = medication.IsLongStanding;
                                exisitingMedication.MedicationType = medication.MedicationType;
                                exisitingMedication.Classification = medication.Classification;
                                if (exisitingMedication.MedicationCategory == "DC")
                                {
                                    exisitingMedication.DCDate = medication.DCDate;
                                    logAction = LogAction.MedicationDischarged;
                                }
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            }

                            if (patientRepository.UpdateMedication(medicationProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                var medication = existingMedications != null ? existingMedications.SingleOrDefault(m => m.Id == medicationId) : null;
                if (medication != null)
                {
                    var logAction = new LogAction();
                    if (medicationCategory.IsEqual(MedicationCategoryEnum.DC.ToString()))
                    {
                        medication.DCDate = dischargeDate;
                        medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        logAction = LogAction.MedicationDischarged;
                    }
                    else
                    {
                        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                        logAction = LogAction.MedicationActivated;
                    }
                    medication.LastChangedDate = DateTime.Now;
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateMedicationForDischarge(Guid medId, Guid Id, DateTime dischargeDate)
        {
            bool result = false;
            try
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    if (medicationProfile.Medication.IsNotNullOrEmpty())
                    {
                        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                        if (existingList.Exists(m => m.Id == Id))
                        {
                            var med = existingList.Single(m => m.Id == Id);
                            med.DCDate = dischargeDate;
                            med.LastChangedDate = DateTime.Now;
                            med.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                            medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            result = patientRepository.UpdateMedication(medicationProfile);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool DeleteMedication(Guid medicationProfileId, Guid medicationId)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medicationId))
                {
                    existingList.RemoveAll(m => m.Id == medicationId);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                }
                if (patientRepository.UpdateMedication(medicationProfile))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public Medication GetMedication(Guid medProfileId, Guid medicationId)
        {
            var medication = new Medication();
            var medicationProfile = patientRepository.GetMedicationProfile(medProfileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medication = medicationProfile.Medication.ToObject<List<Medication>>().SingleOrDefault(m => m.Id == medicationId);
                medication.ProfileId = medicationProfile.Id;
            }
            return medication;
        }

        //public List<Medication> GetMedications(Guid medId, string medicationCategory)
        //{
        //    var list = new List<Medication>();
        //    var medicationProfile = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
        //    if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
        //    {
        //        list = medicationProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == medicationCategory).OrderByDescending(l => l.StartDateSortable).ToList();
        //    }
        //    return list ?? new List<Medication>();
        //}

        //public List<Medication> InsertThenReturnMedications(Guid medId, Medication medication, string medicationType, string medicationCategory)
        //{
        //    var list = new List<Medication>();
        //    var medProfile = patientRepository.InsertMedication(medId, Current.AgencyId, medication, medicationType);
        //    if (medProfile != null && medProfile.Medication.IsNotNullOrEmpty())
        //    {
        //        list = medProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == medicationCategory).OrderByDescending(l => l.StartDateSortable).ToList();
        //    }
        //    return list ?? new List<Medication>();
        //}

        //public List<Medication> UpdateThenReturnMedications(Guid medId, Medication medication, string medicationType, string medicationCategory)
        //{
        //    patientRepository.UpdateMedication(medId, Current.AgencyId, medication, medicationType);
        //    return this.GetMedications(medId, medicationCategory) ?? new List<Medication>();
        //}

        //public List<Medication> DeleteThenReturnMedications(Guid medId, Medication medication, string medicationCategory)
        //{
        //    patientRepository.DeleteMedication(medId, Current.AgencyId, medication);
        //    return this.GetMedications(medId, medicationCategory) ?? new List<Medication>();
        //}



        //public List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory)
        //{
        //    var medicationHistory = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
        //    var medicationList = new List<Medication>();
        //    if (medicationHistory != null)
        //    {
        //        medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == medicationCategory).ToList();
        //    }
        //    return medicationList;
        //}

        #endregion

        #region Medication Profile History

        public bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileHistory.ProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                medicationProfileHistory.Id = Guid.NewGuid();
                medicationProfileHistory.UserId = Current.UserId;
                medicationProfileHistory.AgencyId = Current.AgencyId;
                medicationProfileHistory.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                medicationProfileHistory.Created = DateTime.Now;
                medicationProfileHistory.Modified = DateTime.Now;
                medicationProfileHistory.Medication = medicationProfile.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList().ToXml();
                if (medicationProfileHistory.PharmacyPhoneArray != null && medicationProfileHistory.PharmacyPhoneArray.Count == 3)
                {
                    medicationProfileHistory.PharmacyPhone = medicationProfileHistory.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                var physician = physicianRepository.Get(medicationProfileHistory.PhysicianId, Current.AgencyId);
                if (physician != null)
                {
                    medicationProfileHistory.PhysicianData = physician.ToXml();
                }
                if (patientRepository.AddNewMedicationHistory(medicationProfileHistory))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationProfileSigned, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationHistoryForPatient(patientId, Current.AgencyId);
            if (medProfile != null && medProfile.Count > 0)
            {
                var users = new List<User>();
                var userIds = medProfile.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                }
                medProfile.ForEach(m =>
                {
                    if (!m.UserId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == m.UserId);
                        if (user != null)
                        {
                            m.UserName = user.DisplayName;
                        }
                    }
                }
                );
            }
            return medProfile.OrderByDescending(m => m.SignedDate.ToShortDateString().ToOrderedDate()).ToList();
        }

        public IList<MedicationProfileHistory> UpdateMedicationSnapshotHistoryThenReturn(Guid Id, Guid patientId, DateTime signedDate)
        {
            IList<MedicationProfileHistory> list = new List<MedicationProfileHistory>();
            var medicationProfileSnapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (medicationProfileSnapShot != null)
            {
                medicationProfileSnapShot.SignedDate = signedDate;
                if (patientRepository.UpdateMedicationProfileHistory(medicationProfileSnapShot))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, medicationProfileSnapShot.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationHistoryUpdated, string.Empty);
                    list = GetMedicationHistoryForPatient(patientId);
                }
            }
            return list;
        }

        public IList<MedicationProfileHistory> DeleteMedicationSnapshotHistoryThenReturn(Guid Id, Guid patientId)
        {
            IList<MedicationProfileHistory> list = new List<MedicationProfileHistory>();
            var medicationProfileSnapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (medicationProfileSnapShot != null)
            {
                medicationProfileSnapShot.IsDeprecated = true;
                if (patientRepository.UpdateMedicationProfileHistory(medicationProfileSnapShot))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, medicationProfileSnapShot.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationHistoryDeleted, string.Empty);
                    list = GetMedicationHistoryForPatient(patientId);
                }
            }
            return list;
        }

        #endregion

        #region Allergy

        public JsonViewData AddAllergy(Allergy allergy)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new allergy could not be added to the allergy profile." };
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    allergy.Id = Guid.NewGuid();
                    if (allergyProfile.Allergies.IsNullOrEmpty())
                    {
                        var newList = new List<Allergy>() { allergy };
                        allergyProfile.Allergies = newList.ToXml();
                    }
                    else
                    {
                        var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                        existingList.Add(allergy);
                        allergyProfile.Allergies = existingList.ToXml();
                    }
                    if (patientRepository.UpdateAllergyProfile(allergyProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyAdded, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The new allergy has been added to the allergy profile successfully.";
                    }
                }
            }
            return viewData;
        }

        public JsonViewData UpdateAllergy(Allergy allergy)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy could not be updated. Try again." };
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
                {
                    var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                    if (existingList.Exists(a => a.Id == allergy.Id))
                    {
                        var exisitingAllergy = existingList.Single(m => m.Id == allergy.Id);
                        if (exisitingAllergy != null)
                        {
                            exisitingAllergy.Name = allergy.Name;
                            exisitingAllergy.Type = allergy.Type;
                            allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                            if (patientRepository.UpdateAllergyProfile(allergyProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyUpdated, string.Empty);
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The Allergy has been updated successfully.";
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy status could not be updated. Try again." };
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
            {
                var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                if (existingList.Exists(a => a.Id == allergyId))
                {
                    var allergy = existingList.Single(m => m.Id == allergyId);
                    if (allergy != null)
                    {
                        allergy.IsDeprecated = isDeleted;
                        allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                        if (patientRepository.UpdateAllergyProfile(allergyProfile))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfileId.ToString(), LogType.AllergyProfile, LogAction.AllergyDeleted, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Allergy has been updated successfully.";
                        }
                    }
                }
            }
            return viewData;
        }

        public Allergy GetAllergy(Guid allergyProfileId, Guid allergyId)
        {
            var allergy = new Allergy();
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null)
            {
                allergy = allergyProfile.Allergies.ToObject<List<Allergy>>().SingleOrDefault(a => a.Id == allergyId);
                allergy.ProfileId = allergyProfile.Id;
            }
            return allergy;
        }

        public string GetAllergiesText(Guid patientId)
        {
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                return allergyProfile.ToString();
            }
            return string.Empty;
        }

        public AllergyProfile GetAllergyProfile(Guid allergyProfileId, string prefix)
        {
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null)
            {
                allergyProfile.Type = prefix;
                var allergyPermissions = Current.Permissions.GetOrDefault((int)ParentPermission.AllergyProfile);
                if (allergyPermissions != null)
                {
                    var isNotFrozen = !Current.IsAgencyFrozen;
                    allergyProfile.IsUserCanEdit = allergyPermissions.GetOrDefault((int)PermissionActions.Edit) != null && isNotFrozen;
                    allergyProfile.IsUserCanDelete = allergyPermissions.GetOrDefault((int)PermissionActions.Delete) != null && isNotFrozen;
                    allergyProfile.IsUserCanRestore = allergyPermissions.GetOrDefault((int)PermissionActions.Restore) != null && isNotFrozen;
                }
            }
            return allergyProfile;
        }

        public AllergyProfileViewData GetPatientAllergyProfileViewData(Guid patientId)
        {
            var viewData = new AllergyProfileViewData();
            var patient = patientRepository.GetPatientNameAndServicesById(Current.AgencyId, patientId);
            if (patient != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    var allergyPermissions = Current.Permissions.GetOrDefault((int)ParentPermission.AllergyProfile);
                    if (allergyPermissions != null)
                    {
                        var isNotFrozen = !Current.IsAgencyFrozen;
                        allergyProfile.IsUserCanEdit = allergyPermissions.GetOrDefault((int)PermissionActions.Edit) != null && isNotFrozen;
                        allergyProfile.IsUserCanDelete = allergyPermissions.GetOrDefault((int)PermissionActions.Delete) != null && isNotFrozen;
                        allergyProfile.IsUserCanRestore = allergyPermissions.GetOrDefault((int)PermissionActions.Restore) != null && isNotFrozen;
                        viewData.IsCanUserAdd = allergyPermissions.GetOrDefault((int)PermissionActions.Add) != null && isNotFrozen;
                        viewData.IsCanUserPrint = allergyPermissions.GetOrDefault((int)PermissionActions.Print) != null;
                    }
                    allergyProfile.Type = "AllergyProfile";
                    viewData.AllergyProfile = allergyProfile;
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = PhysicianEngine.GetName(physician.Id, Current.AgencyId);
                    }
                }
                viewData.PatientProfile = new PatientProfileLean { FirstName = patient.FirstName, LastName = patient.LastName, MiddleInitial = patient.MiddleInitial };
            }
            return viewData;
        }

        #endregion

        #region Physician

        public bool LinkPhysicians(Patient patient)
        {
            var result = true;
            if (patient != null && patient.AgencyPhysicians.Any())
            {
                int i = 0;
                bool isPrimary = false;
                foreach (var agencyPhysicianId in patient.AgencyPhysicians)
                {
                    if (i == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    if (!agencyPhysicianId.IsEmpty() && !patientRepository.Link(patient.Id, agencyPhysicianId, isPrimary))
                    {
                        result = false;
                        break;
                    }
                    i++;
                }
                if (i > 0 && result)
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PhysicianLinked, "One of the physician is set as primary.");
                }
            }
            return result;
        }

        public bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary)
        {
            if (patientRepository.Link(patientId, physicianId, isPrimary))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianLinked, isPrimary ? "Primary" : string.Empty);
                return true;
            }
            return false;
        }

        public bool UnlinkAll(Guid patientId)
        {
            return patientRepository.UnlinkAll(patientId);
        }

        public bool UnlinkPhysician(Guid patientId, Guid physicianId)
        {
            if (patientRepository.DeletePhysicianContact(physicianId, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianUnLinked, string.Empty);
                return true;
            }
            return false;
        }

        public JsonViewData AddPatientPhysician(Guid physicianId, Guid patientId)
        {
            string physicianName = PhysicianEngine.GetName(physicianId, Current.AgencyId).Trim();
            string messageHeader = "Physician, " + physicianName + ", ";
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = messageHeader + "could not be added. Try Again." };
            if (!patientRepository.DoesPhysicianExist(patientId, physicianId))
            {
                if (LinkPhysician(patientId, physicianId, false))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = messageHeader + " has been successfully added.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = messageHeader + " is already assigned to this patient.";
            }
            return viewData;
        }

      

        public AgencyPhysician GetPatientPrimaryPhysician(Guid patientId)
        {
            return physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
        }

        public IList<AgencyPhysician> GetPatientPhysicians(Guid patientId)
        {
            return physicianRepository.GetPatientPhysicians(Current.AgencyId, patientId);
        }

        public bool SetPrimaryIfExistOrAddNew(Guid patientId, Guid physicianId)
        {
            bool result = false;
            var patientPhysicians = patientRepository.GetPatientPhysicians(patientId);
            if (patientPhysicians.IsNotNullOrEmpty())
            {
                if (patientPhysicians.Exists(p => p.PhysicianId == physicianId))
                {
                    result = SetPrimary(patientPhysicians, physicianId);
                }
                else
                {
                    if (patientRepository.Link(patientId, physicianId, true))
                    {
                        patientPhysicians.ForEach(p => p.IsPrimary = false);
                        if (patientRepository.UpdateMultiplePatientPhysicians(patientPhysicians))
                        {
                            result = true;
                        }
                        else
                        {
                            patientRepository.Unlink(patientId, physicianId);
                        }
                    }
                    //patientPhysicians.Add(new
                }
            }
            else
            {
                result = patientRepository.Link(patientId, physicianId, true);
            }
            return result;
        }

        public bool SetPrimary(Guid patientId, Guid physicianId)
        {
            bool result = false;
            var patientPhysicians = patientRepository.GetPatientPhysicians(patientId);
            if (patientPhysicians.IsNotNullOrEmpty())
            {
                result = SetPrimary(patientPhysicians, physicianId);
            }
            return result;
        }

        private bool SetPrimary(List<PatientPhysician> patientPhysicians, Guid physicianId)
        {
            bool result = false;
            bool flag = false;
            if (patientPhysicians.IsNotNullOrEmpty())
            {
                foreach (PatientPhysician contat in patientPhysicians)
                {
                    if (contat.PhysicianId == physicianId)
                    {
                        contat.IsPrimary = true;
                        flag = true;
                    }
                    else
                    {
                        contat.IsPrimary = false;
                    }
                }
                if (flag)
                {
                    result = patientRepository.UpdateMultiplePatientPhysicians(patientPhysicians);
                }
            }
            return result;
        }

        #endregion

      

        //#region Authorization

        //public JsonViewData AddAuthorization(AAE.Authorization authorization)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New authorization could not be saved." };
        //    if (authorization.IsValid())
        //    {
        //        authorization.UserId = Current.UserId;
        //        authorization.AgencyId = Current.AgencyId;
        //        authorization.Id = Guid.NewGuid();
        //        if (patientRepository.AddAuthorization(authorization))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationAdded, string.Empty);
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "Authorization was saved successfully.";
        //            viewData.PatientId = authorization.PatientId;
        //            viewData.ServiceId = (int)authorization.Service;
        //        }
        //    }
        //    else
        //    {
        //        viewData.isSuccessful = false;
        //        viewData.errorMessage = authorization.ValidationMessage;
        //    }
        //    return viewData;
        //}

        //public JsonViewData EditAuthorization(AAE.Authorization authorization)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Edit authorization could not be saved." };

        //    if (authorization.IsValid())
        //    {
        //        authorization.AgencyId = Current.AgencyId;
        //        if (patientRepository.EditAuthorization(authorization))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationEdited, string.Empty);
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "Authorization was saved successfully.";
        //            viewData.PatientId = authorization.PatientId;
        //            viewData.ServiceId = (int)authorization.Service;
        //        }
        //    }
        //    else
        //    {
        //        viewData.isSuccessful = false;
        //        viewData.errorMessage = authorization.ValidationMessage;
        //    }
        //    return viewData;
        //}

        //public JsonViewData DeleteAuthorization(Guid Id, Guid patientId)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Delete authorization is not Successful." };
        //    if (!Id.IsEmpty() && !patientId.IsEmpty())
        //    {
        //        if (patientRepository.DeleteAuthorization(Current.AgencyId, patientId, Id))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Authorization, LogAction.AuthorizationDeleted, string.Empty);
        //            viewData.PatientId = patientId;
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "Authorization was deleted successfully.";
        //        }
        //    }
        //    return viewData;
        //}

        //public AAE.Authorization GetAuthorization(Guid authorizationId)
        //{
        //    return patientRepository.GetAuthorization(Current.AgencyId, authorizationId);
        //}

        //public AAE.Authorization GetAuthorizationWithPatientName(Guid patientId, Guid Id)
        //{
        //    var auth = patientRepository.GetAuthorization(Current.AgencyId, patientId, Id);
        //    if (auth != null)
        //    {
        //        var patient = patientRepository.GetPatientNameAndServicesById(patientId, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            auth.DisplayName = patient.DisplayName;
        //            auth.PatientServices = patient.Services;
        //        }
        //    }
        //    return auth;
        //}

        //public IList<AAE.Authorization> GetAuthorizations(Guid patientId, int service)
        //{
        //    return patientRepository.GetAuthorizations(Current.AgencyId, patientId, service);
        //}

        //public IList<AAE.Authorization> GetAuthorizationsWithBranchName(Guid patientId, int service)
        //{
        //    var authorizations = patientRepository.GetAuthorizations(Current.AgencyId, patientId, service);
        //    if (authorizations != null && authorizations.Count > 0)
        //    {
        //        var locatons = agencyRepository.AgencyLocations(Current.AgencyId, authorizations.Select(a => a.AgencyLocationId).Distinct().ToList()) ?? new List<AgencyLocation>();
        //        authorizations.ForEach(a =>
        //        {
        //            var location = locatons.FirstOrDefault(l => l.Id == a.AgencyLocationId);
        //            if (location != null)
        //            {
        //                a.Branch = location.Name;
        //            }
        //        });
        //    }
        //    return authorizations;
        //}

        //#endregion

        #region Hospitalization Log

        public bool AddHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var hospitalizationLog = new HospitalizationLog
            {
                Id = Guid.NewGuid(),
                UserId = formCollection.GetGuid("UserId"),
                PatientId = formCollection.GetGuid("PatientId"),
                EpisodeId = formCollection.GetGuid("EpisodeId"),
                HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue,
                LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue,
                Data = this.ProcessHospitalizationData(formCollection),
                Created = DateTime.Now,
                AgencyId = Current.AgencyId,
                SourceId = (int)TransferSourceTypes.User,
                Modified = DateTime.Now
            };

            if (patientRepository.AddHospitalizationLog(hospitalizationLog))
            {
                var patient = patientRepository.GetPatientOnly(hospitalizationLog.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsHospitalized = true;
                    patient.HospitalizationId = hospitalizationLog.Id;
                    if (patientRepository.UpdateModal(patient))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var logId = formCollection.GetGuid("Id");
            var patientId = formCollection.GetGuid("PatientId");
            var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, logId);
            if (hospitalizationLog != null)
            {
                hospitalizationLog.UserId = formCollection.GetGuid("UserId");
                hospitalizationLog.EpisodeId = formCollection.GetGuid("EpisodeId");
                hospitalizationLog.Modified = DateTime.Now;
                hospitalizationLog.HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.Data = ProcessHospitalizationData(formCollection);

                if (patientRepository.UpdateHospitalizationLog(hospitalizationLog))
                {
                    result = true;
                }
            }

            return result;
        }

        public JsonViewData UpdateHospitalizationLogStatus(Guid patientId, Guid hospitalizationLogId, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Hospitalization Log could not be deleted." };
            var transferLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (transferLog != null)
            {
                transferLog.IsDeprecated = isDeprecated;
                if (patientRepository.UpdateHospitalizationLog(transferLog))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospitalization Log was updated successfully.";
                }
            }
            return viewData;
        }

        public HospitalizationLog GetHospitalizationLogOnly(Guid patientId, Guid hospitalizationLogId)
        {
            var log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (log != null)
            {
                log.PatientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
            }
            return log;
        }

        //public HospitalizationLog GetHospitalizationLogPrintWithNoEpisodeInfo(Guid patientId, Guid hospitalizationLogId)
        //{
        //    var log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
        //    if (log != null)
        //    {
        //        var patient = patientRepository.GetPatientOnly(log.PatientId, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            log.Patient = patient;
        //            log.Location = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
        //        }
        //        var user = UserEngine.GetUser(log.UserId, Current.AgencyId);
        //        if (user != null)
        //        {
        //            log.UserName = user.DisplayName;
        //        }
        //    }
        //    return log;
        //}

        //public HospitalizationViewData GetHospitalizationViewData(Guid patientId, bool isPrintUrlNeeded)
        //{
        //    var viewData = new HospitalizationViewData();
        //    viewData.PatientId = patientId;
        //    viewData.Logs = GetHospitalizationLogsWithUserName(Current.AgencyId, patientId,null);
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        viewData.PatientName = patient.DisplayName;
        //    }
        //    return viewData;
        //}

        public HospitalizationViewData GetHospitalizationViewData(Guid patientId, bool isExport)
        {
            var viewData = new HospitalizationViewData();
            viewData.PatientId = patientId;
            var isUserCanDelete = false;
            var isUserCanEdit = false;
            var isUserCanPrint = false;
            if (!isExport)
            {
                var allPermission = new Dictionary<int, AgencyServices>();
                allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.HospitalizationLog, new int[] { (int)PermissionActions.Export, (int)PermissionActions.Add, (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Print });
                if (allPermission.IsNotNullOrEmpty())
                {
                    viewData.IsUserCanExport = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None) != AgencyServices.None;
                    viewData.IsUserCanAdd = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None) != AgencyServices.None;

                    isUserCanDelete = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                    isUserCanEdit = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                    isUserCanPrint = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                }
            }
            viewData.Logs = GetHospitalizationLogsWithUserName(Current.AgencyId,patientId, (HospitalizationLog log) =>
            {
                log.IsUserCanDelete = isUserCanDelete;
                log.IsUserCanEdit = isUserCanEdit;
                log.IsUserCanPrint = isUserCanPrint;
            });
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                viewData.PatientName = patient.DisplayName;
            }
            return viewData;
        }

       

        //public List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId)
        //{
        //    var logs = patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId);
        //    if (logs.IsNotNullOrEmpty())
        //    {
        //        logs.ForEach(l =>
        //        {
        //            l.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/HospitalizationLogPdf', { 'patientId': '" + l.PatientId + "', 'hospitalizationLogId': '" + l.Id + "' });\"><span class=\"img icon print\"></span></a>";
        //        });
        //    }
        //    return logs;
        //}

        public List<HospitalizationLog> GetHospitalizationLogsWithUserName(Guid agencyId, Guid patientId)
        {
           var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.HospitalizationLog, new int[] {  (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Print });
           var isUserCanDelete = false;
           var isUserCanEdit = false;
           var isUserCanPrint = false;
            if (allPermission.IsNotNullOrEmpty())
            {
                isUserCanDelete = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                isUserCanEdit = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                isUserCanPrint = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
            }
            return GetHospitalizationLogsWithUserName(Current.AgencyId, patientId, (HospitalizationLog log) =>
            {
                log.IsUserCanDelete = isUserCanDelete;
                log.IsUserCanEdit = isUserCanEdit;
                log.IsUserCanPrint = isUserCanPrint;
            });
        }

        private List<HospitalizationLog> GetHospitalizationLogsWithUserName(Guid agencyId, Guid patientId, Action<HospitalizationLog> expression)
        {
            var logs = patientRepository.GetHospitalizationLogs(Current.AgencyId, patientId);
            if (logs.IsNotNullOrEmpty())
            {
                var userIds = logs.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                if (userIds.IsNotNullOrEmpty())
                {
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                    logs.ForEach(d =>
                    {
                        if (!d.UserId.IsEmpty())
                        {
                            var user = users.FirstOrDefault(u => u.Id == d.UserId);
                            if (user != null)
                            {
                                d.UserName = user.DisplayName;
                            }
                        }
                        if (expression != null)
                        {
                            expression(d);
                        }
                    });
                }
            }
            return logs;
        }

      

        public List<PatientHospitalizationData> GetHospitalizationLogs(Guid agencyId)
        {
            var patientList = patientRepository.GetHospitalizedPatients(Current.AgencyId);
            if (patientList != null && patientList.Count > 0)
            {
                var userIds = patientList.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                if (userIds.IsNotNullOrEmpty())
                {
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    if (users.IsNotNullOrEmpty())
                    {
                        patientList.ForEach(d =>
                        {
                            if (!d.UserId.IsEmpty())
                            {
                                var user = users.FirstOrDefault(u => u.Id == d.UserId);
                                if (user != null)
                                {
                                    d.User = user.DisplayName;
                                }
                            }
                        });
                    }
                }
            }
            return patientList;
        }

        #endregion

        #region Patient Asset

        public bool AddDocument(PatientDocument patientDocument, HttpPostedFileBase asset)
        {
            var result = false;
            Guid assetId;
            if (AssetHelper.AddAsset(asset, out assetId))
            {
                result = assetRepository.AddPatientDocument(assetId, patientDocument);
            }
            return result;
        }

        public bool EditDocument(PatientDocument patientDocument)
        {
            var result = false;
            var existingPatientDocument = assetRepository.GetPatientDocument(patientDocument.Id, patientDocument.PatientId, Current.AgencyId);
            if (existingPatientDocument != null)
            {
                existingPatientDocument.Modified = DateTime.Now;
                existingPatientDocument.Filename = patientDocument.Filename;
                existingPatientDocument.UploadTypeId = patientDocument.UploadTypeId;
                result = assetRepository.UpdatePatientDocument(existingPatientDocument);
            }
            return result;
        }

        public bool DeleteDocument(Guid Id, Guid patientId)
        {
            return assetRepository.DeletePatientDocument(Id, patientId, Current.AgencyId);
        }

        public PatientDocument GetPatientDocumentWithName(Guid patientId, Guid documentId)
        {
            var patientDocument = assetRepository.GetPatientDocument(documentId, patientId, Current.AgencyId);
            if (patientDocument != null)
            {
                patientDocument.PatientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
            }
            return patientDocument;
        }

        public List<PatientDocument> GetDocuments(Guid patientId)
        {
            var list = new List<PatientDocument>();
            list = assetRepository.GetPatientDocuments(patientId);
            list.ForEach(a =>
            {
                var type = agencyRepository.FindUploadType(a.AgencyId, a.UploadTypeId);
                if (type != null)
                {
                    a.TypeName = type.Type;
                }
            });
            return list;
        }

        #endregion

        #region User Access

        public JsonViewData AddUserAccess(Guid patientId, Guid userId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User access was not granted." };
            var patientUser = new PatientUser
            {
                UserId = userId,
                PatientId = patientId
            };
            if (patientRepository.AddPatientUser(patientUser))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User access was granted successfully.";
                viewData.PatientId = patientUser.Id;
            }
            return viewData;
        }

        public JsonViewData RemoveUserAccess(Guid patientUserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User access was not removed." };
            var patientUser = new PatientUser
            {
                Id = patientUserId
            };
            if (patientRepository.RemovePatientUser(patientUser))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User access was granted successfully.";
            }
            return viewData;
        }

        public List<SelectedPatient> GetPatientAccess(Guid userId)
        {
            var patients = patientRepository.GetPatientAccessList(Current.AgencyId, userId);
            foreach (var patient in patients)
            {
                if (patient.PatientUserId != Guid.Empty)
                {
                    patient.Selected = true;
                }
                else
                {
                    patient.Selected = false;
                }
            }
            return patients;


        }



        #endregion

        //#region More Members
        ////TODO:fix
        //public object GetPatientSelectionList(Guid branchId, int status)
        //{
        //    return null;// return patientRepository.Find(status, branchId, Current.AgencyId).OrderBy(s => s.DisplayName.ToUpperCase()).Select(p => new { Id = p.Id, Name = p.DisplayName }).ToList();
        //}

        #region Private Methods


        

        private string ProcessHospitalizationData(FormCollection formCollection)
        {
            formCollection.Remove("Id");
            formCollection.Remove("PatientId");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("UserId");
            formCollection.Remove("Templates");
            formCollection.Remove("M0903LastHomeVisitDate");
            formCollection.Remove("M0906DischargeDate");

            var questions = new List<Question>();
            foreach (var key in formCollection.AllKeys)
            {
                questions.Add(QuestionFactory<Question>.Create(key, formCollection.GetValues(key).Join(",")));
            }
            return questions.ToXml();
        }

        #endregion

    }
}
