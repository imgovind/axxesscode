﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;
    using Axxess.LookUp.Repositories;
    using Axxess.Core.Enums;

    public class PhysicianService : IPhysicianService
    {
        #region Constructor

        private readonly ILoginRepository loginRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly ILookupRepository lookupRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Public Methods

        public bool CreatePhysician(AgencyPhysician physician)
        {
            physician.Id = Guid.NewGuid();
            if (physician.PhysicianAccess)
            {
                var isNewLogin = false;
                var login = loginRepository.Find(physician.EmailAddress.Trim());
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = physician.DisplayName;
                    login.EmailAddress = physician.EmailAddress.Trim();
                    login.Role = Roles.Physician.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                physician.LoginId = login.Id;

                if (physicianRepository.Add(physician))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", Current.AgencyName);

                    if (isNewLogin)
                    {
                        var parameters = string.Format("loginid={0}", physician.LoginId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else
                    {
                        bodyText = MessageBuilder.PrepareTextFrom("ExistingPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName);
                    }

                    Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianAdded, string.Empty);
                    return true;
                }
            }
            else
            {
                if (physicianRepository.Add(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianAdded, string.Empty);
                    return true;
                }
            }

            return false;
        }

        public JsonViewData AddPhysician(AgencyPhysician agencyPhysician)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be added" };

            bool exist = physicianRepository.DoesPhysicianExistInAgency(Current.AgencyId, agencyPhysician.NPI, agencyPhysician.AddressZipCode);
            if (exist)
            {
                viewData.errorMessage = "The physician, " + agencyPhysician.DisplayName + ", has already been added.";
                return viewData;
            }

            if (agencyPhysician.PhysicianAccess && agencyPhysician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "An E-mail Address is required for Physician Access";
                return viewData;
            }

            if (agencyPhysician.IsValid())
            {
                agencyPhysician.AgencyId = Current.AgencyId;
                if (!CreatePhysician(agencyPhysician))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "A problem occured while saving the physician" + agencyPhysician.DisplayName + ".";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician, " + agencyPhysician.DisplayName + ", has been successfully added.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agencyPhysician.ValidationMessage;
            }
            return viewData;
        }

        public bool UpdatePhysician(AgencyPhysician physician)
        {
            if (physician.PhysicianAccess)
            {
                var isNewLogin = false;
                var login = loginRepository.Find(physician.EmailAddress.Trim());
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = physician.DisplayName;
                    login.EmailAddress = physician.EmailAddress.Trim();
                    login.Role = Roles.Physician.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                physician.LoginId = login.Id;

                if (physicianRepository.Update(physician))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use Axxess Home Health Software", Current.AgencyName);

                    if (isNewLogin)
                    {
                        var parameters = string.Format("loginid={0}", physician.LoginId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else
                    {
                        bodyText = MessageBuilder.PrepareTextFrom("ExistingPhysicianConfirmation", "firstname", physician.DisplayName,
                            "agencyname", Current.AgencyName);
                    }

                    Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianUpdated, string.Empty);
                    return true;
                }
            }
            else
            {
                if (physicianRepository.Update(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianUpdated, string.Empty);
                    return true;
                }
            }

            return false;
        }

        public JsonViewData DeletePhysician(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be deleted. Please try again." };
            if (physicianRepository.Delete(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician could not be deleted.";
            }
            return viewData;
        }

        public bool AddLicense(PhysicainLicense license)
        {
            var result = false;
            var physician = physicianRepository.Get(license.PhysicianId, Current.AgencyId);
            if (physician != null)
            {
                license.Id = Guid.NewGuid();
                license.Created = DateTime.Now;
                license.Modified = DateTime.Now;
                if (physician.Licenses.IsNotNullOrEmpty())
                {
                    physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                }
                else
                {
                    physician.LicensesArray = new List<PhysicainLicense>();
                }
                physician.LicensesArray.Add(license);
                physician.Licenses = physician.LicensesArray.ToXml();
                if (physicianRepository.Update(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public PhysicainLicense GetLicense(Guid id, Guid physicianId)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            return GetLicense(id, physician);
        }

        public PhysicainLicense GetLicense(Guid id, AgencyPhysician physician)
        {
            PhysicainLicense license = null;
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    license = physician.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        license.PhysicianId = physician.Id;
                    }
                }
            }
            return license;
        }

        public bool UpdateLicense(PhysicainLicense license)
        {
            var physician = physicianRepository.Get(license.PhysicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                var licenseToEdit = GetLicense(license.Id, physician);
                if (license != null)
                {
                    licenseToEdit.ExpirationDate = license.ExpirationDate;
                    licenseToEdit.State = license.State;
                    licenseToEdit.LicenseNumber = license.LicenseNumber;
                    licenseToEdit.InitiationDate = license.InitiationDate;
                    physician.Licenses = physician.LicensesArray.ToXml();
                    if (physicianRepository.Update(physician))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid physicianId)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    var license = physician.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        physician.LicensesArray.Remove(license);
                        physician.Licenses = physician.LicensesArray.ToXml();
                        if (physicianRepository.Update(physician))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianLicenseDeleted, string.Empty);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public Guid GetPrimaryPhysicianId(Guid patientId, Guid agencyId)
        {
            return physicianRepository.GetPatientPrimaryOrFirstPhysicianId(Current.AgencyId, patientId);
        }

        public AgencyPhysician GetAgencyPhysicianForEdit(Guid Id, bool isPecosVerificationNeeded)
        {
            var physician = GetAgencyPhysician(Id, isPecosVerificationNeeded);
            if (physician != null)
            {
                physician.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.Physician, PermissionActions.ViewLog);
            }
            return physician;
        }

        public AgencyPhysician GetAgencyPhysician(Guid Id, bool isPecosVerificationNeeded)
        {
            var physician = physicianRepository.Get(Id, Current.AgencyId);
            if (physician != null && isPecosVerificationNeeded)
            {
                physician.IsPecosVerified = lookupRepository.VerifyPecos(physician.NPI);
            }
            return physician;
        }

        public IList<AgencyPhysician> GetAgencyPhysiciansWithPecosVerification()
        {
            var physicians = physicianRepository.GetAgencyPhysiciansWithOutPecosVerification(Current.AgencyId);
            if (physicians.IsNotNullOrEmpty())
            {
                var npis = physicians.Select(s => s.NPI).Distinct().ToList();
                if (npis.IsNotNullOrEmpty())
                {
                    var pecosNPIs = lookupRepository.GetPecoVerified(npis);
                    if (pecosNPIs.IsNotNullOrEmpty())
                    {
                        physicians.ForEach(physician =>
                        {
                            physician.IsPecosVerified = pecosNPIs.Exists(p => p == physician.NPI);
                        });
                    }
                }
            }
            return physicians;
        }

        public PhysicianListViewData GetPhysicianListViewData()
        {
            var viewData = new PhysicianListViewData();
            var physicians = physicianRepository.GetAgencyPhysiciansForGrid(Current.AgencyId);
            if (physicians.IsNotNullOrEmpty())
            {
                var npis = physicians.Select(s => s.NPI).Distinct().ToList();
                if (npis.IsNotNullOrEmpty())
                {
                    var pecosNPIs = lookupRepository.GetPecoVerified(npis);
                    if (pecosNPIs.IsNotNullOrEmpty())
                    {
                        physicians.ForEach(physician =>
                        {
                            physician.PECOS = pecosNPIs.Exists(p => p == physician.NPI);
                        });
                    }
                }
                viewData.List = physicians;
                var permissions = Current.CategoryService(Current.AcessibleServices, ParentPermission.Physician, new int[] { (int)PermissionActions.Add, (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Export });
                if (permissions.IsNotNullOrEmpty())
                {
                    viewData.IsUserCanExport = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None) != AgencyServices.None;
                    if (!Current.IsAgencyFrozen)
                    {
                        viewData.IsUserCanAdd = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None) != AgencyServices.None;
                        viewData.IsUserCanEdit = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                        viewData.IsUserCanDelete = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                    }
                }
            }
            return viewData;
        }

        public List<PhysicainLicense> GeAgencyPhysicianLicenses(Guid physicianId)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    return physician.LicensesArray;
                }
            }
            return new List<PhysicainLicense>();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
