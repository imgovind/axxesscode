﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
   
    using Axxess.Log.Enums;

    using Domain;
    using Axxess.AgencyManagement.Application.ViewData;
using Axxess.Core.Enums;
   

    public class PayrollService : IPayrollService
    {
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly HHTaskRepository scheduleRepository;
        private AgencyServices Service { get; set; }

        public PayrollService(HHDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.scheduleRepository = agencyManagementDataProvider.TaskRepository;
            this.Service = AgencyServices.HomeHealth;
        }

        public bool MarkAsPaid(List<Guid> itemList)
        {
            var result = false;

            if (itemList != null && itemList.Count > 0)
            {
                if (scheduleRepository.ToggleScheduledEventsPaid(Current.AgencyId, itemList, true))
                {
                    result = true;
                    Auditor.MultiLogOnlyUpdate(Current.AgencyId, itemList, Actions.MarkedPaid, ScheduleStatus.NoStatus, "This visit is marked as paid.");
                }
            }
            return result;
        }

        public bool MarkAsUnpaid(List<Guid> itemList)
        {
            var result = false;

            if (itemList != null && itemList.Count > 0)
            {
                if (scheduleRepository.ToggleScheduledEventsPaid(Current.AgencyId, itemList, false))
                {
                    result = true;
                    Auditor.MultiLogOnlyUpdate(Current.AgencyId, itemList, Actions.MarkedUnpaid, ScheduleStatus.NoStatus, "The visit is marked as unpaid.");
                }
            }
            return result;
        }

        public List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status)
        {
            var scheduleStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
            var list = scheduleRepository.GetPayrollSummmaryLean(Current.AgencyId, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
            if (list != null && list.Count > 0)
            {
                var userIds = list.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                list.ForEach(p =>
                {
                    var user = users.SingleOrDefault(u => u.Id == p.UserId);
                    if (user != null)
                    {
                        p.UserName = user.DisplayName;
                        p.FirstName = user.FirstName;
                        p.LastName = user.LastName;
                    }
                });
            }
            var result = list.OrderBy(k => k.LastName);
            list = new List<VisitSummary>(result);

            return list;
        }

        public List<PayrollDetailSummaryItem> GetVisitsSummary(Guid userId, DateTime startDate, DateTime endDate, string Status)
        {
            PayrollDetail detail = GetVisits(userId, startDate, endDate, Status);

            List<PayrollDetailSummaryItem> summary = new List<PayrollDetailSummaryItem>();
            string lastTask = string.Empty;
            int taskCount = 1;
            double totalMileage = 0.0;
            double totalSurcharges = 0.0;

            TimeSpan totalTaskTime = new TimeSpan(0);
            PayrollDetailSummaryItem item = new PayrollDetailSummaryItem();

            var visitList = detail.Visits.OrderBy(v => v.TaskName);
            visitList.ForEach<UserVisit>(v =>
            {
                if (lastTask != v.TaskName) // new task or first task
                {
                    if (lastTask.Length != 0)
                    {
                        item.TotalTaskTime = totalTaskTime;
                        item.TotalMileage = totalMileage;
                        item.TaskCount = taskCount.ToString();
                        item.TaskName = lastTask;
                        item.TotalSurcharges = totalSurcharges;
                        summary.Add(item);
                    }

                    // reset to new summary item
                    item = new PayrollDetailSummaryItem();
                    item.TaskName = v.TaskName;
                    lastTask = item.TaskName;
                    taskCount = 0;
                    totalMileage = 0;
                    totalTaskTime = new TimeSpan(0);
                    totalSurcharges = 0.0;
                }

                if (v.TimeIn.IsNotNullOrEmpty() && v.TimeOut.IsNotNullOrEmpty())
                {
                    // get total task time
                    DateTime timeIn = DateTime.Parse(v.TimeIn);
                    DateTime timeOut = DateTime.Parse(v.TimeOut);
                    TimeSpan totalTime = timeOut.Subtract(timeIn);
                    totalTaskTime += totalTime;
                    totalMileage += v.MileageRate.ToDouble();
                    totalSurcharges += v.Surcharge.ToDouble();
                }
                taskCount++;

                if (visitList.Last<UserVisit>().Equals(v))
                {
                    item.TotalTaskTime = totalTaskTime;
                    item.TotalMileage = totalMileage;
                    item.TaskCount = taskCount.ToString();
                    item.TotalSurcharges = totalSurcharges;
                    summary.Add(item);
                }
            });

            return summary;
        }

        public List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status) 
        {
            var list = new List<PayrollDetail>();
            if (Status.IsEqual("false") || Status.IsEqual("true"))
            {
                var scheduleStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
                var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
                var payrollVisits = scheduleRepository.GetPayrollSummmaryVisits(Current.AgencyId, Guid.Empty, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
                if (payrollVisits != null && payrollVisits.Count > 0)
                {
                    var userIds = payrollVisits.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    if (userIds != null && userIds.Count > 0)
                    {
                        var users = UserEngine.GetUsers(Current.AgencyId, userIds)?? new List<User>();
                        userIds.ForEach(u =>
                        {
                            var user = users.SingleOrDefault(us => us.Id == u);
                            list.Add(new PayrollDetail
                            {
                                Id = u,
                                Name = user != null ? user.DisplayName : string.Empty,
                                Visits = payrollVisits.Where(p => p.UserId == u).ToList(),
                                StartDate = startDate,
                                EndDate = endDate,
                                PayrollStatus = Status 
                            });
                        });
                    }
                }
            }
            return list;
        }

        public PayrollDetail GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status)
        {
            var payrollDetail = new PayrollDetail { Service=this.Service};
            if (!userId.IsEmpty())
            {
                var user = userRepository.GetUserOnly(Current.AgencyId, userId);
                if (user != null)
                {
                    payrollDetail.Id = userId;
                    payrollDetail.Name = user.DisplayName;
                    payrollDetail.StartDate = startDate;
                    payrollDetail.EndDate = endDate;
                    payrollDetail.PayrollStatus = Status;
                    var userVisits = new List<UserVisit>();
                    if (Status.IsEqual("false") || Status.IsEqual("true"))
                    {
                        var scheduleStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
                        var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
                        userVisits = scheduleRepository.GetPayrollSummmaryVisits(Current.AgencyId, userId, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
                        if (userVisits.IsNotNullOrEmpty())
                        {
                            var userRates = user.Rates.IsNotNullOrEmpty() ? user.Rates.ToObject<List<UserRate>>() : new List<UserRate>();
                            userVisits.ForEach(uv =>
                            {
                                var userRate = new UserRate();
                                if (uv.PrimaryInsurance != 0)
                                {
                                    userRate = userRates.FirstOrDefault(r => r.Insurance == uv.PrimaryInsurance && r.Id == uv.DisciplineTask);
                                }
                                uv.VisitRate = userRate != null ? userRate.ToString() : "none";
                                uv.MileageRate = userRate != null && userRate.MileageRate > 0 ? string.Format("${0:#0.00} / mile", userRate.MileageRate) : "0";
                                uv.Surcharge = uv.Surcharge.IsNotNullOrEmpty() && uv.Surcharge.IsDouble() ? string.Format("${0:#0.00}", uv.Surcharge.ToDouble()) : "0";

                                var visitPay = GetVisitPayment(uv, userRate);
                                uv.VisitPayment = string.Format("${0:#0.00}", visitPay);

                                uv.Total = uv.Surcharge.IsNotNullOrEmpty() && uv.Surcharge.IsDouble() ? uv.Surcharge.ToDouble() + visitPay : visitPay;
                                uv.TotalPayment = string.Format("${0:#0.00}", uv.Total);
                            });
                        }
                    }
                    payrollDetail.Visits = userVisits;
                }
            }

            return payrollDetail;
        }

        public PayrollSummaryViewData GetPayrollSummaryViewData(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var viewData = new PayrollSummaryViewData { VisitSummary = GetSummary(payrollStartDate, payrollEndDate, payrollStatus), EndDate = payrollEndDate, StartDate = payrollStartDate, PayrollStatus = payrollStatus, Service = this.Service };
            viewData.IsUserCanExport = Current.HasRight(this.Service, ParentPermission.Payroll, PermissionActions.Export); 
            return viewData;
        }

        public PayrollDetailsViewData GetPayrollDetailsViewData(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var viewData = new  PayrollDetailsViewData { StartDate = payrollStartDate, EndDate = payrollEndDate, Details = GetDetails(payrollStartDate, payrollEndDate, payrollStatus), PayrollStatus = payrollStatus };
            viewData.IsUserCanExport = Current.HasRight(this.Service, ParentPermission.Payroll, PermissionActions.Export); 
            return viewData;
        }

       
       
        
        private static double GetVisitPayment(UserVisit visit, UserRate rate)
        {
            var total = 0.0;
            var hours = (double)visit.MinSpent / 60;
            if (visit != null && rate != null)
            {
                if (rate.Rate > 0)
                {
                    if (rate.RateType == (int)UserRateTypes.Hourly)
                    {
                        total = rate.Rate * hours;
                    }
                    else if (rate.RateType == (int)UserRateTypes.PerVisit)
                    {
                        total = rate.Rate;
                    }
                }
                if (visit.AssociatedMileage.IsNotNullOrEmpty() && visit.AssociatedMileage.IsDouble() && rate.MileageRate > 0)
                {
                    total += visit.AssociatedMileage.ToDouble() * rate.MileageRate;
                }
            }
            return total;
        }
    }
}
