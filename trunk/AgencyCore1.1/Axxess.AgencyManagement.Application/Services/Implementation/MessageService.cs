﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Web;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;

    public class MessageService : IMessageService
    {
        #region Private Members/Constructor

        private readonly ILoginRepository loginRepository;
        private readonly IMessageRepository messageRepository;
        private readonly IPatientRepository patientRepository;

        public MessageService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        #endregion

        #region IMessageService Members

        public IList<MessageFolder> FolderList()
        {
            return messageRepository.FolderList(Current.User.Id, Current.AgencyId).ToList();
        }

        public bool AddSystemMessage(Message message)
        {
            var result = false;

            return result;
        }

        public int GetTotalMessagesCount(string inboxType)
        {
            if (inboxType.IsNotNullOrEmpty())
            {
                if (inboxType.IsEqual("sent"))
                {
                    return messageRepository.GetSentMessageCount(Current.UserId, Current.AgencyId);
                }
                else if (inboxType.IsEqual("bin"))
                {
                    return messageRepository.GetUserMessageCount(Current.UserId, Current.AgencyId, true);
                }
                else
                {
                    return messageRepository.GetUserMessageCount(Current.UserId, Current.AgencyId, false);
                }
            }
            return 0;
        }

        public Message GetMessage(Guid messageId, int messageTypeId)
        {
            var message = new Message();
            if (messageTypeId == (int)MessageType.User)
            {
                message = messageRepository.GetUserMessage(messageId, Current.UserId, Current.AgencyId);
                if (message != null)
                {
                    if (!message.PatientId.IsEmpty())
                    {
                        message.PatientName = patientRepository.GetPatientNameById(message.PatientId, Current.AgencyId);
                    }
                    if (message.IsDeprecated)
                    {
                        message.MarkAsRead = true;
                    }
                    if (!message.MarkAsRead)
                    {
                        messageRepository.Read(messageId, Current.UserId, Current.AgencyId);
                    }
                }
            }
            else if (messageTypeId == (int)MessageType.System)
            {
                message = messageRepository.GetSystemMessage(messageId, Current.UserId, Current.AgencyId);
                if (message != null)
                {
                    message.Body = message.Body.ReplaceTokens();
                    if (!message.MarkAsRead)
                    {
                        messageRepository.Read(messageId, Current.UserId, Current.AgencyId);
                    }
                    message.RecipientNames = Current.UserFullName;
                }
            }
            else
            {
                message = messageRepository.GetSentMessage(messageId, Current.UserId, Current.AgencyId);
                if (message != null)
                {
                    message.MarkAsRead = true;
                    if (!message.PatientId.IsEmpty())
                    {
                        message.PatientName = patientRepository.GetPatientNameById(message.PatientId, Current.AgencyId);
                    }
                }
            }
            return message;
        }

        public DashboardMessage GetCurrentDashboardMessage()
        {
            return messageRepository.GetCurrentDashboardMessage();
        }

        public bool DeleteMessage(Guid messageId, int messageTypeId)
        {
            var result = false;
            if (messageTypeId == (int)MessageType.User || messageTypeId == (int)MessageType.System)
            {
                result = messageRepository.Delete(messageId, Current.UserId, Current.AgencyId);
            }
            return result;
        }

        public JsonViewData DeleteMany(List<Guid> messageList)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Messages could not be deleted." };
            if (messageRepository.DeleteMany(messageList, Current.UserId, Current.AgencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The selected messages have been deleted successfully.";
            }
            return viewData;
        }


        public IList<Message> GetMessages(string inboxType, int pageNumber)
        {
            var messages = new List<Message>();

            if (inboxType.IsNotNullOrEmpty())
            {
                Guid messageFolderId = inboxType.Length == 36 ? inboxType.ToGuid() : Guid.Empty;

                if (inboxType.IsEqual("sent"))
                {
                    var sentMessages = messageRepository.GetSentMessages(Current.UserId, Current.AgencyId, pageNumber);
                    sentMessages.ForEach(sentMessage =>
                    {
                        sentMessage.FromName = sentMessage.RecipientNames.IsNotNullOrEmpty() ? sentMessage.RecipientNames.Length > 25 ? sentMessage.RecipientNames.Substring(0, 25) + " ..." : sentMessage.RecipientNames : string.Empty;
                        sentMessage.Subject = sentMessage.Subject.IsNotNullOrEmpty() ? sentMessage.Subject.Length > 35 ? sentMessage.Subject.Substring(0, 35) + " ..." : sentMessage.Subject : string.Empty;
                        sentMessage.MarkAsRead = true;
                        messages.Add(sentMessage);
                    });
                }
                else if (inboxType.IsEqual("bin"))
                {
                    var deletedmessages = messageRepository.GetMessages(Current.UserId, Current.AgencyId, messageFolderId, pageNumber, true).ToList();
                    deletedmessages.ForEach(deletedMessage =>
                    {
                        deletedMessage.MarkAsRead = true;
                        messages.Add(deletedMessage);
                    });
                }
                else
                {
                    messages = messageRepository.GetMessages(Current.UserId, Current.AgencyId, messageFolderId, pageNumber, false).ToList();
                }
            }
            return messages.OrderByDescending(m => m.Created).ToList();
        }

        public JsonViewData SendMessage(Message message, HttpPostedFileBase attachment1)
        {
            var viewData = new JsonViewData(false, "Your message could not be sent.");
            var recipientNames = new StringBuilder();
            var carbonCopyNames = new StringBuilder();
            var recipients = new Dictionary<Guid, string>();
            var emailAddresses = new Dictionary<Guid, string>();
            Guid assetId = Guid.Empty;
            if (attachment1 == null || AssetHelper.AddAsset(attachment1, out assetId))
            {
                var agencyId = Current.AgencyId;
                var currentUserId = Current.UserId;
                var currentUserName = Current.UserFullName;
                try
                {
                    new Thread(
                        () =>
                        {
                            message.AttachmentId = assetId;
                            var userIds = message.CarbonCopyRecipients.IsNotNullOrEmpty() ? message.Recipients.Union(message.CarbonCopyRecipients).Distinct() : message.Recipients;
                            var users = UserEngine.GetUsers(agencyId, userIds.ToList());
                            var logins = loginRepository.GetLogins(users.Select(s => s.LoginId).Distinct().ToList());

                            message.Recipients.ForEach(
                                u =>
                                {
                                    var user = users.FirstOrDefault(f => f.Id == u);
                                    if (user != null && !user.LoginId.IsEmpty())
                                    {
                                        var login = logins.FirstOrDefault(f => f.Id == user.LoginId);
                                        if (login != null && login.IsActive)
                                        {
                                            emailAddresses.Add(u, login.EmailAddress);
                                            recipients.Add(u, user.FirstName);
                                            recipientNames.AppendFormat("{0}; ", user.DisplayName);
                                        }
                                    }
                                });

                            if (message.CarbonCopyRecipients != null)
                            {
                                message.CarbonCopyRecipients.ForEach(
                                    u =>
                                    {
                                        var user = users.FirstOrDefault(f => f.Id == u);
                                        if (user != null && !user.LoginId.IsEmpty())
                                        {
                                            var login = logins.FirstOrDefault(f => f.Id == user.LoginId);
                                            if (login != null && login.IsActive)
                                            {
                                                emailAddresses.Add(u, login.EmailAddress);
                                                recipients.Add(u, user.FirstName);
                                                carbonCopyNames.AppendFormat("{0}; ", user.DisplayName);
                                            }
                                        }
                                    });
                            }

                            var messageDetail = new MessageDetail(message, agencyId, currentUserId, currentUserName, recipientNames.ToString(), carbonCopyNames.ToString());
                            var userMessages = new List<UserMessage>();
                            message.Recipients.ForEach(
                                recipient =>
                                {
                                    var userMessage = new UserMessage(agencyId, recipient, messageDetail.Id, MessageType.User);
                                    userMessages.Add(userMessage);
                                });

                            if (messageRepository.AddMessageDetail(messageDetail) && messageRepository.AddUserMessage(userMessages))
                            {
                                message.Recipients.ForEach(
                                    recipientId =>
                                    {
                                        if (recipients.ContainsKey(recipientId))
                                        {
                                            var parameters = new string[4];
                                            parameters[0] = "recipientfirstname";
                                            parameters[1] = recipients[recipientId];
                                            parameters[2] = "senderfullname";
                                            parameters[3] = currentUserName;
                                            var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                                            Notify.User(CoreSettings.NoReplyEmail, emailAddresses[recipientId], string.Format("{0} sent you a message.", currentUserName), bodyText);
                                        }
                                    });
                            }
                            else if (!assetId.IsEmpty())
                            {
                                AssetHelper.Delete(assetId);
                            }
                        }).Start();
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your message has been sent.";
                }
                catch (Exception e)
                {
                    
                }
            }
            else
            {
                viewData.errorMessage = "The file could not be attached.";
            }
            return viewData;
        }

        public JsonViewData AddMessageFolder(string newFolderName)
        {
            var jsonData = new JsonViewData() { isSuccessful = false, errorMessage = "Error adding folder." };
            if (messageRepository.AddMessageFolder(Current.UserId, Current.AgencyId, newFolderName))
            {
                jsonData.isSuccessful = true;
                jsonData.errorMessage = "Added new folder: " + newFolderName;
            }
            return jsonData;
        }

        public JsonViewData MoveMessageFolder(Guid id, Guid folderId, string folderName, string messageType)
        {
            JsonViewData jsonData = new JsonViewData();
            jsonData.isSuccessful = messageRepository.MoveMessageToFolder(Current.UserId, id, folderId, messageType);
            jsonData.errorMessage = (jsonData.isSuccessful ? "Message has been moved to " + folderName + "." : "There was an error moving this message to " + folderName + ".");
            return jsonData;
        }

        #endregion

    }
}
