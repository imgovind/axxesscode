﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Transactions;
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Helpers;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;
    using Axxess.Membership.Logging;
    using System.Web;
    using System.IO;
    using Axxess.AgencyManagement.Application.iTextExtension;

    using SubSonic.DataProviders;

    public abstract class InfectionService<T, E> : BaseService
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        //protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;

        private TaskScheduleAbstract<T> baseScheduleRepository;
        private InfectionAbstract baseNoteRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;

        protected InfectionService(TaskScheduleAbstract<T> baseScheduleRepository,
            InfectionAbstract baseNoteRepository,
            EpisodeAbstract<E> baseEpisodeRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseNoteRepository = baseNoteRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
        }

        #region Infections

        public JsonViewData AddScheduleTaskAndInfectionHelper(Guid patientId, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected infection report task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var infections = this.GetInfectionFromTasks(patientId, scheduleEvents);
                if (infections != null)
                {
                    if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (baseNoteRepository.AddMultipleInfection(infections))
                        {
                            viewData.isSuccessful = true;
                            viewData.IsInfectionRefresh = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }

                }
            }
            return viewData;
        }

        public JsonViewData AddInfection(Infection infection, bool isSigned)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The Infection Log could not be created." };
            if (isSigned)
            {
                infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                viewData.IsCaseManagementRefresh = true;
                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                if (isQaByPass)
                {
                    viewData.IsCaseManagementRefresh = false;
                    infection.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                }
            }
            else
            {
                infection.SignatureText = string.Empty;
            }
            infection.Id = Guid.NewGuid();
            infection.UserId = Current.UserId;
            infection.AgencyId = Current.AgencyId;
            //infection.EpisodeId = GetReportNoteEpisodeId(infection.PatientId, infection.InfectionDate);
            if (baseNoteRepository.AddInfection(infection))
            {
                var newTask = TaskHelperFactory<T>.CreateTask(infection.Id,
                    infection.UserId,
                    infection.EpisodeId,
                    infection.PatientId,
                    infection.Status,
                    Disciplines.ReportsAndNotes,
                     infection.InfectionDate,
                    (int)DisciplineTasks.InfectionReport
                );

                if (baseScheduleRepository.AddScheduleTask(newTask))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, infection.EpisodeId, infection.PatientId, infection.Id, Actions.Add, DisciplineTasks.InfectionReport);
                    viewData.PatientId = newTask.PatientId;
                    viewData.EpisodeId = infection.EpisodeId;
                    viewData.UserIds = new List<Guid> { infection.UserId };
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, newTask);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Infection Log has been created successfully";
                }
                else
                {
                    baseNoteRepository.RemoveInfection(Current.AgencyId, infection.PatientId, infection.Id);
                }
            }
            return viewData;
        }

        public JsonViewData UpdateInfection(Infection infection, bool isSigned)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The Infection Log could not be updated. Try again." };
            infection.AgencyId = Current.AgencyId;
            infection.UserId = infection.UserId.IsEmpty() ? Current.UserId : infection.UserId;
            if (isSigned)
            {
                viewData.IsCaseManagementRefresh = true;
                infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                if (isQaByPass)
                {
                    infection.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    viewData.IsCaseManagementRefresh = false;
                }
            }
            else
            {
                infection.SignatureText = string.Empty;
            }
            var editTask = baseScheduleRepository.GetScheduleTask(Current.AgencyId, infection.PatientId, infection.Id);
            if (editTask != null)
            {
                var oldTaskValues = editTask.DeepClone();// TaskHelperFactory<T>.CopyTaskValues(editTask);
                editTask.EpisodeId = infection.EpisodeId;
                editTask = TaskHelperFactory<T>.SetNoteProperties(editTask, infection.InfectionDate, infection.Status);
                if (baseScheduleRepository.UpdateScheduleTask(editTask))
                {
                    if (baseNoteRepository.UpdateInfection(infection))
                    {
                        if (Enum.IsDefined(typeof(ScheduleStatus), editTask.Status))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, infection.EpisodeId, editTask.PatientId, editTask.Id, Actions.Add, (ScheduleStatus)editTask.Status, DisciplineTasks.InfectionReport, string.Empty);
                        }
                        viewData.PatientId = infection.PatientId;
                        viewData.EpisodeId = infection.EpisodeId;
                        viewData.UserIds = new List<Guid> { infection.UserId };
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.IsCaseManagementRefresh = viewData.IsCaseManagementRefresh;
                        viewData.IsDataCentersRefresh = true;
                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, editTask);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Infection Log has been updated successfully.";

                    }
                    else
                    {
                        //editTask.EpisodeId = oldTaskValues.EpisodeId;
                        //editTask = TaskHelperFactory<T>.SetNoteProperties(editTask, oldTaskValues.VisitDate, oldTaskValues.Status);
                        baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                    }
                }
            }
            return viewData;
        }

        public bool UpdateInfectionForScheduleDetail(T schedule)
        {
            bool result;
            var infectionReport = baseNoteRepository.GetInfectionReport(Current.AgencyId, schedule.Id);
            if (infectionReport != null)
            {
                if (schedule.VisitDate.IsValid())
                {
                    infectionReport.InfectionDate = schedule.VisitDate;
                }
                infectionReport.EpisodeId = schedule.EpisodeId;
                infectionReport.IsDeprecated = schedule.IsDeprecated;
                infectionReport.Status = schedule.Status;
                result = baseNoteRepository.UpdateInfectionModal(infectionReport);
            }
            else
            {
                result = true;
            }
            return result;
        }

        public JsonViewData ProcessInfections(Guid patientId, Guid eventId, string button, string reason)
        {
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                return ProcessInfections(scheduleEvent, button, reason);
            }
            return new JsonViewData(false, "Unable to find incident. Please try again.");
        }

        public JsonViewData ProcessInfections(T task, string button, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            bool isInfectionToBeUpdated = true;
            bool isActionSet = false;
            if (task != null)
            {
                var description = string.Empty;
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var infection = baseNoteRepository.GetInfectionReport(Current.AgencyId, task.Id);
                        if (infection != null)
                        {
                            if (!infection.EpisodeId.IsEmpty())
                            {
                                var oldStatus = task.Status;

                                if (button == "Approve")
                                {
                                    infection.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                                    task.Status = infection.Status;
                                    task.InPrintQueue = true;
                                    description = "Approved By:" + Current.UserFullName;
                                    isActionSet = true;
                                }
                                else if (button == "Return")
                                {
                                    infection.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                                    task.Status = infection.Status;
                                    infection.SignatureText = string.Empty;
                                    infection.SignatureDate = DateTime.MinValue;
                                    description = "Returned By:" + Current.UserFullName;
                                    isActionSet = true;
                                }
                                else if (button == "Print")
                                {
                                    task.InPrintQueue = false;
                                    isInfectionToBeUpdated = false;
                                    isActionSet = true;
                                }
                                if (isActionSet)
                                {
                                    if (baseScheduleRepository.UpdateScheduleTask(task))
                                    {
                                        if (isInfectionToBeUpdated)
                                        {
                                            bool returnCommentStatus = true;
                                            if (reason.IsNotNullOrEmpty())
                                            {
                                                returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                            }
                                            if (returnCommentStatus)
                                            {
                                                infection.Modified = DateTime.Now;
                                                if (baseNoteRepository.UpdateInfectionModal(infection))
                                                {
                                                    viewData.Service = Service.ToString();
                                                    viewData.ServiceId = (int)Service;
                                                    viewData.IsDataCentersRefresh = oldStatus != task.Status;
                                                    viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature;
                                                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, task);
                                                    viewData.IsInfectionRefresh = viewData.IsDataCentersRefresh;
                                                    viewData.EpisodeId = task.EpisodeId;
                                                    viewData.PatientId = task.PatientId;
                                                    viewData.UserIds = new List<Guid> { task.UserId };
                                                    viewData.isSuccessful = true;
                                                    ts.Complete();
                                                }
                                                else
                                                {
                                                    viewData.errorMessage = "A problem occured while updating the Infection Log.";
                                                    ts.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                viewData.errorMessage = "A problem occured while adding the return comment.";
                                                ts.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            viewData.isSuccessful = true;
                                            ts.Complete();
                                        }
                                    }
                                    else
                                    {
                                        viewData.errorMessage = "A problem occured while updating the task.";
                                        ts.Dispose();
                                    }
                                }
                            }
                        }
                    }
                }
                if (viewData.isSuccessful && isInfectionToBeUpdated && Enum.IsDefined(typeof(ScheduleStatus), task.Status))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, Actions.StatusChange, (DisciplineTasks)task.DisciplineTask, description);
                }
            }
            return viewData;
        }

        public JsonViewData ReopenInfectionReport(T scheduleTask)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleTask != null)
            {
                var oldUserId = scheduleTask.UserId;
                var oldStatus = scheduleTask.Status;
                var description = string.Empty;

                viewData.PatientId = scheduleTask.PatientId;
                viewData.EpisodeId = scheduleTask.EpisodeId;
                viewData.UserIds = new List<Guid> { scheduleTask.UserId };


                scheduleTask.Status = ((int)ScheduleStatus.ReportAndNotesReopen);
                var infection = baseNoteRepository.GetInfectionReport(Current.AgencyId, scheduleTask.Id);
                if (infection != null)
                {
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                    {

                        infection.UserId = scheduleTask.UserId;
                        infection.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                        infection.SignatureText = string.Empty;
                        infection.SignatureDate = DateTime.MinValue;
                        infection.Modified = DateTime.Now;
                        if (baseNoteRepository.UpdateInfectionModal(infection))
                        {
                            viewData.isSuccessful = true;
                            viewData.IsInfectionRefresh = true;
                            viewData.IsDataCentersRefresh = true;
                            viewData.Service = this.Service.ToString();
                            viewData.ServiceId = (int)this.Service;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduleTask);
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleTask.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleTask.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleTask);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ToggleInfection(Guid patientId, Guid Id, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Infection Log could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, Id);
            if (task != null)
            {
                viewData = ToggleInfection(task, isDeprecated);
            }
            return viewData;
        }


        public JsonViewData ToggleInfection(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Infection Log could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (baseNoteRepository.MarkInfectionsAsDeleted(task.Id, task.PatientId, Current.AgencyId, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.IsInfectionRefresh = true;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.UserIds = new List<Guid> { task.UserId };
                        viewData.errorMessage = string.Format("The Infection Log has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        public Infection GetInfectionReportPrint(Guid patientId, Guid eventId)
        {
            var infection = baseNoteRepository.GetInfectionReport(Current.AgencyId, eventId);
            if (infection != null)
            {
                var profile = profileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                if (profile != null)
                {
                    infection.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
                    infection.PatientProfile = profile;
                }
                if (!infection.PhysicianId.IsEmpty())
                {
                    infection.PhysicianName = PhysicianEngine.GetName(infection.PhysicianId, Current.AgencyId);
                }

                if (!infection.EpisodeId.IsEmpty())
                {
                    GetInfectionEpisodeData(infection);
                }
            }
            return infection;
        }

        public Infection GetInfectionForEdit(Guid Id)
        {
            var infection = baseNoteRepository.GetInfectionReport(Current.AgencyId, Id);
            if (infection != null)
            {
                infection.Service = this.Service;
                var permission = Current.Permissions;
                if (permission.IsNotNullOrEmpty())
                {
                    infection.IsUserCanEdit = permission.IsInPermission(this.Service, ParentPermission.ManageInfectionReport, PermissionActions.Edit);
                    infection.IsUserCanAddPhysicain = !Current.IsAgencyFrozen && permission.IsInPermission(this.Service, ParentPermission.Physician, PermissionActions.Add);

                    // not used anywhere
                    infection.IsUserCanSeeSticky = !Current.IsAgencyFrozen && permission.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.ViewStickyNotes);
                    if (infection.IsUserCanSeeSticky)
                    {
                        var scheduledEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, infection.PatientId, Id);
                        if (scheduledEvent != null)
                        {
                            infection.StatusComment = scheduledEvent.StatusComment;
                        }
                    }
                }

                infection.PatientName = patientRepository.GetPatientNameById(infection.PatientId, Current.AgencyId);

                if (!infection.EpisodeId.IsEmpty())
                {
                    GetInfectionEpisodeData(infection);
                }
            }
            else
            {
                infection = new Infection();
            }
            return infection;
        }


        protected abstract void GetInfectionEpisodeData(Infection infection);

        public List<Infection> GetInfections(Guid agencyId, bool isExcel)
        {
            var infections = baseNoteRepository.GetInfections(Current.AgencyId).ToList();
            if (infections.IsNotNullOrEmpty())
            {
                var allPermission = new Dictionary<int, AgencyServices>();
                var isEditPermission = false;
                var isDeletePermission = false;
                var isPrintPermission = false;
                if (!isExcel)
                {
                    allPermission = Current.CategoryService(this.Service, ParentPermission.ManageInfectionReport, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Print });
                    if (allPermission.IsNotNullOrEmpty())
                    {
                        isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                        isDeletePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                        isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                    }
                }
                var patientNames = patientRepository.GetPatientNamesByIds(Current.AgencyId, infections.Select(i => i.PatientId).Distinct().ToList());
                infections.ForEach(i =>
                {
                    if (patientNames.ContainsKey(i.PatientId))
                    {
                        i.PatientName = patientNames[i.PatientId];
                    }
                    i.PhysicianName = PhysicianEngine.GetName(i.PhysicianId, Current.AgencyId);
                    i.Service = this.Service;
                    i.StatusName = ScheduleStatusFactory.StatusName(i.Status, i.InfectionDate);
                    if (!isExcel)
                    {
                        i.IsUserCanEdit = isEditPermission;
                        i.IsUserCanDelete = isDeletePermission;
                        i.IsUserCanPrint = isPrintPermission;
                    }
                });
            }
            return infections;
        }

        public List<Infection> GetInfectionFromTasks(Guid patientId, List<T> scheduleEvents)
        {
            var infections = new List<Infection>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var infection = new Infection
                    {
                        AgencyId = Current.AgencyId,
                        Id = scheduleEvent.Id,
                        PatientId = scheduleEvent.PatientId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        InfectionDate = scheduleEvent.EventDate,
                        Status = scheduleEvent.Status,
                        UserId = scheduleEvent.UserId,
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };
                    infections.Add(infection);

                });
            }
            return infections;
        }

        #endregion


    }
}
