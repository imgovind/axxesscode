﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Transactions;
    using System.Web.Mvc;
    using System.Linq;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Extensions;

    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Helpers;

    using SubSonic.DataProviders;
    using Axxess.AgencyManagement.Entities.Common;

    public abstract class PlanOfCareService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {

        #region Constructor / Members

        protected IPhysicianRepository physicianRepository;
        protected PlanOfCareAbstract basePlanOfCareRepository;
        protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;
        protected MongoAbstract mongoRepository;
        private TaskScheduleAbstract<T> baseScheduleRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;
        protected AgencyServices Service { get; set; }


        protected PlanOfCareService(TaskScheduleAbstract<T> scheduleRepository, PlanOfCareAbstract basePlanOfCareRepository, EpisodeAbstract<E> baseEpisodeRepository, MongoAbstract mongoRepository)
        {
            this.baseScheduleRepository = scheduleRepository;
            this.basePlanOfCareRepository = basePlanOfCareRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
            this.mongoRepository = mongoRepository;
        }

        #endregion

        #region IAssessmentService Members

        public JsonViewData AddScheduleTaskAndPlanOfCaresHelper(Guid patientId, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected plan of care task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var planOfCareOrders = this.GetPlanOfCareFromTasks(patientId, scheduleEvents);
                if (planOfCareOrders != null)
                {
                    if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (basePlanOfCareRepository.AddMultiple(planOfCareOrders))
                        {
                            viewData.isSuccessful = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            viewData.PatientId = patientId;
                            viewData.UserIds = new List<Guid> { scheduleEvents[0].UserId };
                            viewData.EpisodeId = scheduleEvents.IsNotNullOrEmpty() ? scheduleEvents[0].EpisodeId : Guid.Empty;
                            viewData.IsDataCentersRefresh = true;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduleEvents);
                            viewData.errorMessage = "Successfully added the plan of care task.";
                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }

                }
            }
            return viewData;
        }

        public List<PlanofCare> GetPlanOfCareFromTasks(Guid patientId, List<T> scheduleEvents)
        {
            var planOfCareOrders = new List<PlanofCare>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var physicianId = Guid.Empty;
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    physicianId = physician.Id;
                }
                var questions = new List<Question>();
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var planofCare = new PlanofCare
                    {
                        Id = scheduleEvent.Id,
                        AgencyId = Current.AgencyId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        PatientId = scheduleEvent.PatientId,
                        UserId = scheduleEvent.UserId,
                        Status = scheduleEvent.Status,
                        OrderNumber = patientRepository.GetNextOrderNumber(),
                        PhysicianId = physicianId,
                        Questions = questions,
                        IsStandAlone = scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone,
                        IsNonOasis = scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485,
                        DisciplineTask = scheduleEvent.DisciplineTask,
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };
                    planOfCareOrders.Add(planofCare);
                });
            }
            return planOfCareOrders;
        }

        public Order GetPlanOfCareOrderForHistoryEdit(Guid patientId, Guid id, string type)
        {
            var order = new Order();
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, id);
            if (planofCare != null)
            {
                order = new Order
                {
                    Id = planofCare.Id,
                    PatientId = planofCare.PatientId,
                    EpisodeId = planofCare.EpisodeId,
                    Type = ("HCFA485" == type || "NonOasisHCFA485" == type) ? OrderType.HCFA485 : OrderType.HCFA485StandAlone,
                    Text = ("HCFA485" == type || "NonOasisHCFA485" == type) ? ("HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription()) : DisciplineTasks.HCFA485StandAlone.GetDescription(),
                    Number = planofCare.OrderNumber,
                    PatientName = planofCare.PatientName,
                    ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
                    SendDate = planofCare.SentDate,
                    PhysicianSignatureDate = planofCare.PhysicianSignatureDate
                };
            }
            return order;
        }

        public Order GetPlanOfCareOrderForReceiving(Guid patientId, Guid id, string type)
        {
            var order = new Order();
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, id);
            if (planofCare != null)
            {
                order = new Order
                {
                    Id = planofCare.Id,
                    PatientId = planofCare.PatientId,
                    EpisodeId = planofCare.EpisodeId,
                    Type = ("HCFA485" == type || "NonOasisHCFA485" == type) ? OrderType.HCFA485 : OrderType.HCFA485StandAlone,
                    Text = ("HCFA485" == type || "NonOasisHCFA485" == type) ? ("HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription()) : DisciplineTasks.HCFA485StandAlone.GetDescription(),
                    Number = planofCare.OrderNumber,
                    PatientName = planofCare.PatientName,
                    ReceivedDate = planofCare.ReceivedDate,
                    SendDate = planofCare.SentDate,
                    PhysicianSignatureDate = planofCare.PhysicianSignatureDate
                };
            }
            return order;
        }

        public bool MarkPlanOfCareOrderAsReturned(Guid patientId, Guid id, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, id);
            if (planofCare != null)
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, planofCare.PatientId, planofCare.Id);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!planofCare.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        planofCare.ReceivedDate = receivedDate;
                        planofCare.PhysicianSignatureDate = physicianSignatureDate;
                        if (basePlanOfCareRepository.Update(planofCare))
                        {
                            result = true;
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                        }
                        else
                        {
                            scheduleEvent.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdatePlanOfCareOrderDates(Guid patientId, Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, id);
            if (planofCare != null)
            {
                planofCare.ReceivedDate = receivedDate;
                planofCare.SentDate = sendDate;
                planofCare.PhysicianSignatureDate = physicianSignatureDate;
                if (planofCare.PhysicianSignatureText.IsNullOrEmpty())
                {
                    var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    else if (!planofCare.PhysicianId.IsEmpty())
                    {
                        physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                    }
                }
                result = basePlanOfCareRepository.Update(planofCare);
            }
            return result;
        }

        public List<Order> GetPlanOfCareOrders(List<T> planofCareOrdersSchedules, bool isPending, bool isCompleted)
        {
            var orders = new List<Order>();
            var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var planofCareOrders = basePlanOfCareRepository.GetPlanofCares(Current.AgencyId, planofCareOrdersIds);

            if (planofCareOrders != null && planofCareOrders.Count > 0)
            {
                var canPrint = Current.Permissions.IsInPermission(Service, ParentPermission.Schedule, PermissionActions.Print);
                planofCareOrders.ForEach(poc =>
                {
                    var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
                    if (evnt != null)
                    {
                        AgencyPhysician physician = null;
                        if (!poc.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                        }
                        else
                        {
                            if (poc.PhysicianData.IsNotNullOrEmpty())
                            {
                                var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                }
                            }
                        }
                        var isNonStandAlone = (evnt.DisciplineTask == (int)DisciplineTasks.HCFA485 || evnt.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485);
                        var order = new Order
                        {
                            Id = poc.Id,
                            PatientId = poc.PatientId,
                            EpisodeId = poc.EpisodeId,
                            Type = isNonStandAlone ? OrderType.HCFA485 : OrderType.HCFA485StandAlone,
                            Text = isNonStandAlone ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.HCFA485StandAlone.GetDescription(),
                            Number = poc.OrderNumber,
                            PatientName = poc.PatientName,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null && physician.PhysicianAccess,
                            CreatedDate = evnt.EventDate,
                            Service = this.Service,
                            DocumentType = DocumentType.PlanOfCare.ToString().ToLower(),
                            CanUserPrint = canPrint
                        };
                        if (isCompleted)
                        {
                            order.SendDate = poc.SentDate;
                            order.ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate;
                            order.PhysicianSignatureDate = poc.PhysicianSignatureDate;
                        }
                        else if (isPending)
                        {
                            order.SendDate = poc.SentDate;
                            order.ReceivedDate = DateTime.Today;
                            order.PhysicianSignatureDate = poc.PhysicianSignatureDate;
                        }
                        orders.Add(order);
                    }
                });
            }
            return orders;
        }

        public List<Order> GetPlanOfCareOrders(Guid patientId, List<T> planofCareOrdersSchedules)
        {
            var orders = new List<Order>();
            var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var planofCareOrders = basePlanOfCareRepository.GetPatientPlanofCares(Current.AgencyId, patientId, planofCareOrdersIds);
            if (planofCareOrders != null && planofCareOrders.Count > 0)
            {
                var canPrint = Current.Permissions.IsInPermission(Service, ParentPermission.Schedule, PermissionActions.Print);
                planofCareOrders.ForEach(poc =>
                {
                    var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.Id == poc.Id && s.EpisodeId == poc.EpisodeId);
                    if (evnt != null)
                    {
                        AgencyPhysician physician = null;
                        if (!poc.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                        }
                        else
                        {
                            if (poc.PhysicianData.IsNotNullOrEmpty())
                            {
                                var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                }
                            }
                        }
                        var isNonStandAlone = (evnt.DisciplineTask == (int)DisciplineTasks.HCFA485 || evnt.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485);
                        orders.Add(new Order
                        {
                            Id = poc.Id,
                            PatientId = poc.PatientId,
                            EpisodeId = poc.EpisodeId,
                            Type = isNonStandAlone ? OrderType.HCFA485 : OrderType.HCFA485StandAlone,
                            Text = isNonStandAlone ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.HCFA485StandAlone.GetDescription(),
                            Number = poc.OrderNumber,
                            PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null && physician.PhysicianAccess,
                            DocumentType = DocumentType.PhysicianOrder.ToString().ToLower(),
                            CreatedDate = evnt.EventDate,
                            ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? (poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate) : DateTime.MinValue,
                            SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician) || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? poc.SentDate : DateTime.MinValue,
                            Status = poc.Status,
                            Service = Service,
                            CanUserPrint = canPrint
                        });
                    }
                });
            }
            return orders;
        }

        public int MarkPlanOfCareAsSent(bool sendElectronically, string[] answersArray, ref List<AgencyPhysician> physicians)
        {
            AgencyPhysician physician = null;
            var count = 0;
            var status = (int)ScheduleStatus.OrderSentToPhysician;
            if (sendElectronically)
            {
                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
            }
            foreach (var item in answersArray)
            {
                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                var pocId = answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                var patientId = answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, pocId);
                if (planofCare != null)
                {
                    if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, planofCare.PatientId, planofCare.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = status;

                        if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                        {
                            planofCare.Status = status;
                            planofCare.SentDate = DateTime.Now;
                            if (basePlanOfCareRepository.Update(planofCare))
                            {
                                count++;
                                if (sendElectronically)
                                {
                                    if (physician != null && !physicians.Exists(p => physician.Id == p.Id))
                                    {
                                        physicians.Add(physician);
                                    }
                                }
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }

                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                            }
                        }
                    }
                }
            }
            return count;
        }

        public PlanofCare GetPlanOfCareOnly(Guid patientId, Guid eventId)
        {
            return basePlanOfCareRepository.Get(Current.AgencyId, patientId, eventId);
        }

        public PlanofCare GetPlanOfCareWithQuestions(Guid patientId, Guid eventId)
        {
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, eventId);
            if (planofCare != null)
            {
                SetQuestions(planofCare);
            }
            return planofCare;
        }

        protected abstract void Get485ForEditAppSpecific(PlanofCare planofCare);

        public PlanofCareViewData GetPlanOfCareWithPatientAndAgencyInfo(Guid patientId, Guid eventId)
        {
            var viewData = new PlanofCareViewData { Service = this.Service };
            var planofcare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, eventId);
            if (planofcare != null)
            {
                var isFrozen = Current.IsAgencyFrozen;
                if (!isFrozen)
                {
                    var permissions = Current.Permissions;
                    viewData.IsUserCanAddPhysicain = permissions.IsInPermission(Current.AcessibleServices, ParentPermission.Physician, PermissionActions.Add);
                    viewData.IsUserCanEditPatient = permissions.IsInPermission(this.Service, ParentPermission.Patient, PermissionActions.Edit);
                    viewData.IsUserCanLoadPrevious = permissions.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.LoadPrevious);
                    viewData.IsUserCanSeeSticky = permissions.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.ViewStickyNotes);
                }

                SetQuestions(planofcare);
                var dictionary = new Dictionary<string, string>();
                var patient = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
                if (patient != null)
                {
                    viewData.PatientData.Add("PatientId", patientId.ToString());
                    viewData.PatientData.Add("PatientName", patient.DisplayName);
                    viewData.PatientData.Add("PatientIdNumber", patient.PatientIdNumber);
                    viewData.PatientData.Add("PatientPolicyNumber", patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : patient.PrimaryHealthPlanId);
                    viewData.PatientData.Add("PatientAddressFirstRow", patient.Address.FirstRow);
                    viewData.PatientData.Add("PatientAddressSecondRow", patient.Address.SecondRow);
                    viewData.PatientData.Add("PatientPhone", patient.Phone);
                    viewData.PatientData.Add("PatientDoB", patient.DOBFormatted);
                    viewData.PatientData.Add("PatientGender", patient.Gender);
                    if (!patient.AgencyId.IsEmpty())
                    {
                        var agency = agencyRepository.GetAgencyOnly(patient.AgencyId);
                        if (agency != null)
                        {
                            dictionary.Add("AgencyId", agency.Id.ToString());
                            dictionary.Add("AgencyMedicareProviderNumber", agency.MedicareProviderNumber);
                            dictionary.Add("AgencyName", agency.Name);
                            var location = agencyRepository.FindLocationOrMain(agency.Id, patient.AgencyLocationId);
                            if (location != null)
                            {
                                dictionary.Add("AgencyAddressFirstRow", location.AddressFirstRow);
                                dictionary.Add("AgencyAddressSecondRow", location.AddressSecondRow);
                                dictionary.Add("AgencyPhone", location.PhoneWorkFormatted);
                                dictionary.Add("AgencyFax", location.FaxNumberFormatted);
                            }
                        }
                    }

                    if (planofcare.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = planofcare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            planofcare.PhysicianId = physician.Id;
                            //dictionary.Add("PhysicianId", physician.Id.ToString());
                            //dictionary.Add("PhysicianName", physician.DisplayName);
                            //dictionary.Add("PhysicianNpi", physician.NPI);
                        }
                    }
                    else if (!planofcare.PhysicianId.IsEmpty())
                    {
                        var physician = PhysicianEngine.Get(planofcare.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            planofcare.PhysicianId = physician.Id;
                            //dictionary.Add("PhysicianId", physician.Id.ToString());
                            //dictionary.Add("PhysicianName", physician.DisplayName);
                            //dictionary.Add("PhysicianNpi", physician.NPI);
                        }
                    }
                    else
                    {
                        var primaryPhysician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (primaryPhysician != null)
                        {
                            planofcare.PhysicianId = primaryPhysician.Id;
                            //dictionary.Add("PhysicianId", primaryPhysician.Id.ToString());
                            //dictionary.Add("PhysicianName", primaryPhysician.DisplayName);
                            //dictionary.Add("PhysicianNpi", primaryPhysician.NPI);
                        }
                    }
                }
                Get485ForEditAppSpecific(planofcare);
                planofcare.StatusComment = baseScheduleRepository.GetReturnReason(Current.AgencyId, patientId, eventId, Current.UserId);
                viewData.PlanOfCare = planofcare;
                viewData.DisplayData = dictionary;
            }
            return viewData;
        }

        protected abstract void POCActiveDateRangeAndSOC(T scheduledEvent, Dictionary<string, string> dictionary);

        public JsonViewData UpdatePlanofCare(FormCollection formCollection)
        {
            var viewData = new JsonViewData(false, "The Plan of Care has failed to update.");
            var planofCareId = formCollection.Get("Id").ToGuid();
            var patientId = formCollection.Get("PatientId").ToGuid();
            if (!planofCareId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, planofCareId);
                if (scheduleEvent != null)
                {
                    var planofCare = GetPlanOfCareWithQuestions(patientId, planofCareId);
                    if (planofCare != null)
                    {
                        var oldPhysicianId = planofCare.PhysicianId;
                        var oldPhysicianData = planofCare.PhysicianData;
                        var oldSignatureText = planofCare.SignatureText;
                        var oldSignatureDate = planofCare.SignatureDate;

                        var isCaseManagementRefresh = false;
                        Guid physicianGuidId;
                        var physicianStringId = formCollection.Get("PhysicianId");
                        if (physicianStringId.IsNotNullOrEmpty() && physicianStringId.GuidTryParse(out physicianGuidId))
                        {
                            planofCare.PhysicianId = physicianGuidId;
                        }
                        var status = formCollection.Get("Status");
                        if (status.IsNotNullOrEmpty() && status.IsInteger())
                        {
                            planofCare.Status = status.ToInteger();
                            if (planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                            {
                                isCaseManagementRefresh = true;
                                planofCare.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                planofCare.SignatureDate = DateTime.Parse(formCollection.Get("SignatureDate"));
                                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                                if (isQaByPass)
                                {
                                    isCaseManagementRefresh = false;
                                    planofCare.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                                }
                                if (!planofCare.PhysicianId.IsEmpty())
                                {
                                    var physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        planofCare.PhysicianData = physician.ToXml();
                                    }
                                }
                            }
                            else
                            {
                                planofCare.SignatureText = string.Empty;
                                planofCare.SignatureDate = DateTime.MinValue;
                            }
                            if (baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, patientId, planofCare.Id, planofCare.Status))
                            {
                                ProcessPlanofCare(formCollection, planofCare, planofCare.IsStandAlone);
                                if (basePlanOfCareRepository.Update(planofCare))
                                {
                                    if (mongoRepository.UpdatePlanofcareQuestion(new AssessmentQuestionData(planofCare)))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)planofCare.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        var isStatusChange = scheduleEvent.Status != planofCare.Status;
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "The Plan of Care has been updated successfully.";
                                        if (scheduleEvent.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                                        {
                                            viewData.errorMessage = "The Plan of Care has been completed and pending QA Review.";
                                        }
                                        viewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                                        viewData.Service = Service.ToString();
                                        viewData.ServiceId = (int)Service;
                                        viewData.IsDataCentersRefresh = isStatusChange;
                                        viewData.IsCaseManagementRefresh = isCaseManagementRefresh;
                                        viewData.PatientId = scheduleEvent.PatientId;
                                        viewData.EpisodeId = scheduleEvent.EpisodeId;
                                        scheduleEvent.Status = planofCare.Status;
                                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(isStatusChange, scheduleEvent);
                                        viewData.IsOrdersToBeSentRefresh = isStatusChange && scheduleEvent.Status == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                    }
                                    else
                                    {
                                        baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, planofCare.PatientId, planofCare.Id, scheduleEvent.Status);
                                        planofCare.SignatureDate = oldSignatureDate;
                                        planofCare.SignatureText = oldSignatureText;
                                        planofCare.Status = scheduleEvent.Status;
                                        planofCare.PhysicianData = oldPhysicianData;
                                        planofCare.PhysicianId = oldPhysicianId;
                                        basePlanOfCareRepository.Update(planofCare);
                                    }
                                }
                                else
                                {
                                    baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, planofCare.PatientId, planofCare.Id, scheduleEvent.Status);
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData Reopen(T scheduleEvent)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleEvent != null)
            {
                var oldUserId = scheduleEvent.UserId;
                var oldStatus = scheduleEvent.Status;
                var description = string.Empty;

                scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened);
                var planOfCare = basePlanOfCareRepository.Get(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                if (planOfCare != null)
                {
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        planOfCare.UserId = scheduleEvent.UserId;
                        planOfCare.Status = (int)ScheduleStatus.OrderReopened;
                        planOfCare.SignatureText = string.Empty;
                        planOfCare.SignatureDate = DateTime.MinValue;
                        planOfCare.PhysicianSignatureText = string.Empty;
                        planOfCare.PhysicianSignatureDate = DateTime.MinValue;
                        planOfCare.ReceivedDate = DateTime.MinValue;
                        if (basePlanOfCareRepository.Update(planOfCare))
                        {
                            viewData.PatientId = scheduleEvent.PatientId;
                            viewData.EpisodeId = scheduleEvent.EpisodeId;
                            viewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                            viewData.isSuccessful = true;
                            viewData.IsPhysicianOrderPOCRefresh = true;
                            viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);
                            viewData.Service = this.Service.ToString();
                            viewData.ServiceId = (int)this.Service;
                            viewData.IsDataCentersRefresh = true;
                            viewData.IsMyScheduleTaskRefresh = Current.UserId == scheduleEvent.UserId && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleEvent.UserId = oldUserId;
                            scheduleEvent.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                            viewData.isSuccessful = false;
                        }
                    }

                }
            }
            return viewData;
        }

        public bool UpdatePlanOfCareForDetail(T schedule)
        {
            //TODO: Needs review
            bool result;
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, schedule.PatientId, schedule.Id);//planofCareRepository.Get(Current.AgencyId, oldEpisodeId, schedule.PatientId, schedule.Id);
            if (planofCare != null)
            {
                planofCare.Status = schedule.Status;
                if (planofCare.Status != schedule.Status && planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        planofCare.PhysicianData = physician.ToXml();
                    }
                }
                planofCare.EpisodeId = schedule.EpisodeId;
                planofCare.PhysicianId = schedule.PhysicianId;
                planofCare.IsDeprecated = schedule.IsDeprecated;
                result = basePlanOfCareRepository.Update(planofCare);
            }
            else
            {
                result = true;
            }
            return result;
        }

        public JsonViewData UpdatePlanofCareStatus(Guid patientId, Guid eventId, string actionType, string reason)
        {
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                return UpdatePlanofCareStatus(scheduleEvent, actionType, reason);
            }
            return new JsonViewData(false, "Unable to find plan of care. Please try again.");
        }

        public JsonViewData UpdatePlanofCareStatus(T task, string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            bool isOrderUpdates = true;
            bool isActionSet = false;
            if (task != null)
            {
                var description = string.Empty;
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, task.PatientId, task.Id);
                        if (planofCare != null)
                        {
                            viewData.PatientId = task.PatientId;
                            viewData.EpisodeId = task.EpisodeId;
                            var oldStatus = task.Status;
                            if (actionType == "Approve")
                            {
                                planofCare.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                task.InPrintQueue = true;
                                task.Status = planofCare.Status;
                                description = "Approved By:" + Current.UserFullName;
                                isActionSet = true;
                            }
                            else if (actionType == "Return")
                            {
                                planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                                task.Status = planofCare.Status;
                                description = "Returned By:" + Current.UserFullName;
                                isActionSet = true;
                            }
                            else if (actionType == "Print")
                            {
                                task.InPrintQueue = false;
                                isOrderUpdates = false;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (baseScheduleRepository.UpdateScheduleTask(task))
                                {
                                    if (isOrderUpdates)
                                    {
                                        bool returnCommentStatus = true;
                                        if (reason.IsNotNullOrEmpty())
                                        {
                                            returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                        }
                                        if (returnCommentStatus)
                                        {
                                            if (basePlanOfCareRepository.Update(planofCare))
                                            {
                                                viewData.IsDataCentersRefresh = oldStatus != task.Status;
                                                viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview);
                                                viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == task.UserId && !task.IsComplete && task.EventDate.Date >= DateTime.Now.AddDays(-89) && task.EventDate.Date <= DateTime.Now.AddDays(14);
                                                viewData.IsOrdersToBeSentRefresh = viewData.IsDataCentersRefresh && task.Status == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                                viewData.IsPhysicianOrderPOCRefresh = viewData.IsDataCentersRefresh;
                                                viewData.ServiceId = (int)Service;
                                                viewData.Service = Service.ToString();
                                                viewData.isSuccessful = true;
                                                viewData.UserIds = new List<Guid> { task.UserId };
                                                ts.Complete();
                                            }
                                            else
                                            {
                                                viewData.errorMessage = "A problem occured while updating the Plan of Care.";
                                                ts.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            viewData.errorMessage = "A problem occured while adding the return comment.";
                                            ts.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                        ts.Complete();
                                    }
                                }
                                else
                                {
                                    viewData.errorMessage = "A problem occured while updating the task.";
                                    ts.Dispose();
                                }
                            }
                        }
                    }
                }
                if (viewData.isSuccessful && isOrderUpdates && Enum.IsDefined(typeof(ScheduleStatus), task.Status))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, Actions.StatusChange, (ScheduleStatus)task.Status, (DisciplineTasks)task.DisciplineTask, description);
                }
            }
            return viewData;
        }

        public JsonViewData TogglePlanofCare(Guid patientId, Guid Id, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Plan Of Care could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, Id);
            if (task != null)
            {
                viewData = TogglePlanofCare(task, isDeprecated);
            }
            return viewData;
        }

        public JsonViewData TogglePlanofCare(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Plan Of Care could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (basePlanOfCareRepository.MarkAsDeleted(Current.AgencyId, task.PatientId, task.Id, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.IsDataCentersRefresh = true;
                        viewData.IsPhysicianOrderPOCRefresh = true;
                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, task);
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.UserIds = new List<Guid> { task.UserId };
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.errorMessage = string.Format("The Plan Of Care has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        public PlanofCare GetPlanOfCarePrint(Guid patientId, Guid eventId)
        {
            var planofCare = basePlanOfCareRepository.Get(Current.AgencyId, patientId, eventId);
            if (planofCare != null)
            {
                return GetPlanOfCarePrint(planofCare);
            }
            return new PlanofCare();
        }

        //TODO: more rework specially on the agency print information
        public PlanofCare GetPlanOfCarePrint(PlanofCare planofCare)
        {
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, planofCare.PatientId, planofCare.Id);
            if (scheduleEvent != null)
            {
                SetQuestions(planofCare);
                planofCare.EpisodeId = scheduleEvent.EpisodeId;
                planofCare = POCServiceSpecificPrintData(scheduleEvent.DisciplineTask, planofCare);
                planofCare.EpisodeId = scheduleEvent.EpisodeId;
                var profile = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, planofCare.PatientId);
                if (!planofCare.EpisodeId.IsEmpty())
                {
                    var episode = baseEpisodeRepository.GetEpisodeOnly(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
                    if (episode != null && !episode.AdmissionId.IsEmpty())
                    {
                        var admission = AdmissionHelperFactory<E>.GetPatientAdmissionDate(planofCare.PatientId, episode.AdmissionId);
                        if (admission != null && admission.PatientData.IsNotNullOrEmpty())
                        {
                            EntityHelper.SetPatientPrintProfile(profile, admission);
                        }
                    }
                    if (profile != null)
                    {
                        planofCare.LocationProfile = agencyRepository.AgencyNameWithAddressAndMore(Current.AgencyId, profile.AgencyLocationId);
                    }
                    else
                    {
                        planofCare.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                        profile = new PatientProfileLean();
                    }
                }
                planofCare.PatientProfile = profile;
                if (planofCare.PhysicianData.IsNullOrEmpty() && !planofCare.PhysicianId.IsEmpty())
                {
                    var physician = PhysicianEngine.Get(planofCare.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        planofCare.PhysicianData = physician.ToXml();
                    }
                }
            }
            return planofCare;
        }

        protected abstract PlanofCare POCServiceSpecificPrintData(int disciplineTaskId, PlanofCare planofCare);

        public T GetPlanofCareScheduleEvent(Guid episodeId, Guid patientId, Guid assessmentId)
        {
            var planofCare = basePlanOfCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, episodeId, assessmentId);
            if (planofCare != null)
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, planofCare.PatientId, planofCare.Id);
                if (scheduleEvent != null)
                {
                    return scheduleEvent;
                }
            }
            return null;
        }


        #endregion

        #region Private Members

        private void ProcessPlanofCare(FormCollection formCollection, PlanofCare planofCare, bool isStandAlone)
        {
            if (planofCare != null && formCollection != null && planofCare.Questions != null && formCollection.Count > 0)
            {
                var questionDictionary = planofCare.Questions.ToPOCDictionary();
                RemovePrimaryDiagnosis(questionDictionary);
                planofCare.Questions = formCollection.ProcessPOCQuestions(questionDictionary, isStandAlone);
            }
        }

        private void RemovePrimaryDiagnosis(IDictionary<string, Question> questions)
        {
            if (questions != null && questions.Count > 0)
            {
                questions.RemoveAll<string, Question>(i => i.Code == "M1020" || i.Code == "M1022");
            }
        }

        public List<Question> Get485FromAssessment(Guid patientId, List<Question> questions)
        {
            var planofCareQuestions = new List<Question>();
            if (questions.IsNotNullOrEmpty())
            {
                var assessmentQuestions = questions.ToOASISDictionary();
                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                {
                    // 10. Medications
                    var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
                    if (medicationProfile != null)
                    {
                        planofCareQuestions.Add(QuestionFactory<Question>.Create("485Medications", medicationProfile.ToString()));
                    }

                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("M0102PhysicianOrderedDate"));

                    // 11. Principal Diagnosis
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("M1020PrimaryDiagnosis"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("M1020ICD9M"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485ExacerbationOrOnsetPrimaryDiagnosis"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("M1020PrimaryDiagnosisDate"));

                    // 12. Surgical Procedure
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureDescription1"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode1"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode1Date"));

                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureDescription2"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode2"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SurgicalProcedureCode2Date"));

                    // 13. Other Pertinent Diagnosis
                    for (int count = 1; count <= 14; count++)
                    {
                        planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("M1022PrimaryDiagnosis{0}", count)));
                        planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("M1022ICD9M{0}", count)));
                        planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("485ExacerbationOrOnsetPrimaryDiagnosis{0}", count)));
                        planofCareQuestions.Add(assessmentQuestions.GetQuestion(string.Format("M1022PrimaryDiagnosis{0}Date", count)));
                    }

                    // 14. DME and Supplies
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485DME"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485DMEComments"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485Supplies"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SuppliesComment"));

                    // 15. Safety Measures
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485SafetyMeasures"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485OtherSafetyMeasures"));

                    // 16. Nutritional Requirements
                    if (assessmentQuestions.ContainsKey("485NutritionalReqs") && assessmentQuestions["485NutritionalReqs"] != null)
                    {
                        planofCareQuestions.Add(QuestionFactory<Question>.Create("485NutritionalReqs", PlanofCareXml.ExtractText("NutritionalRequirements", assessmentQuestions)));
                    }

                    // 17. Allergies
                    var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485Allergies"));
                    if (assessmentQuestions.ContainsKey("485AllergiesDescription") && assessmentQuestions["485AllergiesDescription"] != null)
                    {
                        planofCareQuestions.Add(assessmentQuestions.GetQuestion("485AllergiesDescription"));
                    }
                    else
                    {
                        planofCareQuestions.Add(QuestionFactory<Question>.Create("485AllergiesDescription", allergyProfile != null ? allergyProfile.ToString() : string.Empty));
                    }

                    // 18.A. Functional Limitations
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485FunctionLimitations"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485FunctionLimitationsOther"));

                    // 18.B. Activities Permitted
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485ActivitiesPermitted"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485ActivitiesPermittedOther"));


                    // 19. Mental Status
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485MentalStatus"));
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485MentalStatusOther"));

                    // 20. Prognosis
                    planofCareQuestions.Add(assessmentQuestions.GetQuestion("485Prognosis"));

                    // 21. Interventions
                    var interventions = string.Empty;
                    if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"] != null && assessmentQuestions["485SNFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("SN Frequency: {0}. ", assessmentQuestions["485SNFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"] != null && assessmentQuestions["485PTFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("PT Frequency: {0}. ", assessmentQuestions["485PTFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"] != null && assessmentQuestions["485OTFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("OT Frequency: {0}. ", assessmentQuestions["485OTFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"] != null && assessmentQuestions["485STFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("ST Frequency: {0}. ", assessmentQuestions["485STFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"] != null && assessmentQuestions["485MSWFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("MSW Frequency: {0}. ", assessmentQuestions["485MSWFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"] != null && assessmentQuestions["485HHAFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("HHA Frequency: {0}. ", assessmentQuestions["485HHAFrequency"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("485OrdersDisciplineInterventionComments") && assessmentQuestions["485OrdersDisciplineInterventionComments"] != null && assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer;
                    }

                    var baseVitalSignFormat = "{0} greater than (>) {1} or less than (<) {2}. ";

                    var vitalSignParameters = string.Empty;
                    if (assessmentQuestions.ContainsKey("GenericTempGreaterThan")
                       && assessmentQuestions["GenericTempGreaterThan"] != null
                       && assessmentQuestions["GenericTempGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericTempLessThan")
                       && assessmentQuestions["GenericTempLessThan"] != null
                       && assessmentQuestions["GenericTempLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Temperature", assessmentQuestions["GenericTempGreaterThan"].Answer, assessmentQuestions["GenericTempLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericPulseGreaterThan")
                       && assessmentQuestions["GenericPulseGreaterThan"] != null
                       && assessmentQuestions["GenericPulseGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericPulseLessThan")
                       && assessmentQuestions["GenericPulseLessThan"] != null
                       && assessmentQuestions["GenericPulseLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Pulse", assessmentQuestions["GenericPulseGreaterThan"].Answer, assessmentQuestions["GenericPulseLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericRespirationGreaterThan")
                       && assessmentQuestions["GenericRespirationGreaterThan"] != null
                       && assessmentQuestions["GenericRespirationGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericRespirationLessThan")
                       && assessmentQuestions["GenericRespirationLessThan"] != null
                       && assessmentQuestions["GenericRespirationLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Respirations", assessmentQuestions["GenericRespirationGreaterThan"].Answer, assessmentQuestions["GenericRespirationLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericSystolicBPGreaterThan")
                       && assessmentQuestions["GenericSystolicBPGreaterThan"] != null
                       && assessmentQuestions["GenericSystolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericSystolicBPLessThan")
                       && assessmentQuestions["GenericSystolicBPLessThan"] != null
                       && assessmentQuestions["GenericSystolicBPLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Systolic BP", assessmentQuestions["GenericSystolicBPGreaterThan"].Answer, assessmentQuestions["GenericSystolicBPLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericDiastolicBPGreaterThan")
                       && assessmentQuestions["GenericDiastolicBPGreaterThan"] != null
                       && assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericDiastolicBPLessThan")
                       && assessmentQuestions["GenericDiastolicBPLessThan"] != null
                       && assessmentQuestions["GenericDiastolicBPLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Diastolic BP", assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer, assessmentQuestions["GenericDiastolicBPLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("Generic02SatLessThan")
                       && assessmentQuestions["Generic02SatLessThan"] != null
                       && assessmentQuestions["Generic02SatLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format("O2 Sat (percent) less than (<) {0}. ", assessmentQuestions["Generic02SatLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericFastingBloodSugarGreaterThan")
                       && assessmentQuestions["GenericFastingBloodSugarGreaterThan"] != null
                       && assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericFastingBloodSugarLessThan")
                       && assessmentQuestions["GenericFastingBloodSugarLessThan"] != null
                       && assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Fasting blood sugar", assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer, assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericRandomBloddSugarGreaterThan")
                       && assessmentQuestions["GenericRandomBloddSugarGreaterThan"] != null
                       && assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericRandomBloodSugarLessThan")
                       && assessmentQuestions["GenericRandomBloodSugarLessThan"] != null
                       && assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Random blood sugar", assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer, assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericWeightGreaterThan")
                       && assessmentQuestions["GenericWeightGreaterThan"] != null
                       && assessmentQuestions["GenericWeightGreaterThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format("Weight Gain/Loss (lbs/7 days) Greater than {0}. ", assessmentQuestions["GenericWeightGreaterThan"].Answer);
                    }

                    if (vitalSignParameters.IsNotNullOrEmpty())
                    {
                        interventions += "SN to notify MD of: " + vitalSignParameters;
                    }

                    interventions += PlanofCareXml.ExtractText("Intervention", assessmentQuestions);
                    var interventionQuestion = QuestionFactory<Question>.Create("485Interventions", interventions);
                    planofCareQuestions.Add(interventionQuestion);

                    var goals = string.Empty;
                    goals += PlanofCareXml.ExtractText("Goal", assessmentQuestions);
                    if (assessmentQuestions.ContainsKey("485SensoryStatusGoalComments") && assessmentQuestions["485SensoryStatusGoalComments"] != null)
                    {
                        goals += assessmentQuestions["485SensoryStatusGoalComments"].Answer;
                    }
                    if (assessmentQuestions.ContainsKey("485RehabilitationPotential") && assessmentQuestions["485RehabilitationPotential"] != null)
                    {
                        if (assessmentQuestions["485RehabilitationPotential"].Answer == "1")
                        {
                            goals += "Rehab Potential: Good for stated goals.";
                        }
                        if (assessmentQuestions["485RehabilitationPotential"].Answer == "2")
                        {
                            goals += "Rehab Potential: Fair for stated goals.";
                        }
                        if (assessmentQuestions["485RehabilitationPotential"].Answer == "3")
                        {
                            goals += "Rehab Potential: Poor for stated goals.";
                        }
                    }

                    if (assessmentQuestions.ContainsKey("485AchieveGoalsComments") && assessmentQuestions["485AchieveGoalsComments"] != null && assessmentQuestions["485AchieveGoalsComments"].Answer.IsNotNullOrEmpty())
                    {
                        goals += assessmentQuestions["485AchieveGoalsComments"].Answer;
                    }

                    if (assessmentQuestions.ContainsKey("485DischargePlans") && assessmentQuestions["485DischargePlans"] != null)
                    {
                        if (assessmentQuestions["485DischargePlans"].Answer == "1")
                        {
                            goals += "Discharge Plan: Patient to be discharged to the care of Physician. ";
                        }
                        if (assessmentQuestions["485DischargePlans"].Answer == "2")
                        {
                            goals += "Discharge Plan: Patient to be discharged to the care of Caregiver. ";
                        }
                        if (assessmentQuestions["485DischargePlans"].Answer == "3")
                        {
                            goals += "Discharge Plan: Patient to be discharged to Self care. ";
                        }
                    }

                    if (assessmentQuestions.ContainsKey("485DischargePlansReason") && assessmentQuestions["485DischargePlansReason"] != null && assessmentQuestions["485DischargePlansReason"].Answer.IsNotNullOrEmpty())
                    {
                        var reasons = assessmentQuestions["485DischargePlansReason"].Answer.Split(',');
                        foreach (string reason in reasons)
                        {
                            if (reason.IsEqual("1"))
                            {
                                goals += "Discharge when caregiver willing and able to manage all aspects of patient's care. ";
                            }
                            if (reason.IsEqual("2"))
                            {
                                goals += "Discharge when goals met. ";
                            }
                            if (reason.IsEqual("3"))
                            {
                                goals += "Discharge when wound(s) healed. ";
                            }
                        }
                    }

                    if (assessmentQuestions.ContainsKey("485DischargePlanComments") && assessmentQuestions["485DischargePlanComments"] != null && assessmentQuestions["485DischargePlanComments"].Answer.IsNotNullOrEmpty())
                    {
                        goals += assessmentQuestions["485DischargePlanComments"].Answer;
                    }

                    var goalQuestion = QuestionFactory<Question>.Create("485Goals", goals);
                    planofCareQuestions.Add(goalQuestion);
                }
            }
            return planofCareQuestions;
        }


        private void SetQuestions(PlanofCare planOfCare)
        {
            var questionData = mongoRepository.GetPlanofcareQuestion(Current.AgencyId, planOfCare.Id);
            if (questionData != null && questionData.Question.IsNotNullOrEmpty())
            {
                planOfCare.Questions = questionData.Question;
            }
        }

        //private bool CreatePlanofCare(Assessment assessment,T scheduleEvent)
        //{
        //    var result = false;
        //    var planofCare = planofCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, assessment.EpisodeId, assessment.Id, assessment.Type.ToString());
        //    if (planofCare == null)
        //    {
        //        var isNonOasis = assessment.Type == AssessmentType.NonOASISStartOfCare.ToString() || assessment.Type == AssessmentType.NonOASISRecertification.ToString();
        //        var pocScheduleEvent = new ScheduleEvent
        //        {
        //            AgencyId = !scheduleEvent.AgencyId.IsEmpty() ? scheduleEvent.AgencyId : Current.AgencyId,
        //            Id = Guid.NewGuid(),
        //            EndDate = scheduleEvent.EndDate,
        //            EventDate = scheduleEvent.EventDate,
        //            VisitDate = scheduleEvent.EventDate,
        //            UserId = scheduleEvent.UserId,
        //            StartDate = scheduleEvent.StartDate,
        //            PatientId = assessment.PatientId,
        //            EpisodeId = assessment.EpisodeId,
        //            Discipline = Disciplines.Orders.ToString(),
        //            DisciplineTask = isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
        //            Status = ((int)ScheduleStatus.OrderSaved),
        //            IsOrderForNextEpisode = assessment.Type.IsEqual(AssessmentType.Recertification.ToString()) ? true : false
        //        };

        //        if (CreatePlanofCare(pocScheduleEvent, assessment, isNonOasis))
        //        {
        //            result = true;
        //        }
        //    }
        //    else if (planofCare != null)
        //    {
        //        var planofCareScheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.Id);// scheduleEvents.Where(s => s.EpisodeId == planofCare.EpisodeId && s.PatientId == planofCare.PatientId && s.EventId == planofCare.Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
        //        if (planofCareScheduleEvent == null)
        //        {
        //            var isNonOasis = assessment.Type.IsEqual(AssessmentType.NonOASISStartOfCare.ToString()) || assessment.Type.IsEqual(AssessmentType.NonOASISRecertification.ToString());

        //            var pocScheduleEvent = new ScheduleEvent
        //            {
        //                AgencyId = !planofCare.AgencyId.IsEmpty() ? planofCare.AgencyId : Current.AgencyId,
        //                Id = planofCare.Id,
        //                EndDate = scheduleEvent.EndDate,
        //                EventDate = scheduleEvent.EventDate,
        //                VisitDate = scheduleEvent.EventDate,
        //                UserId = scheduleEvent.UserId,
        //                StartDate = scheduleEvent.StartDate,
        //                PatientId = assessment.PatientId,
        //                EpisodeId = assessment.EpisodeId,
        //                Discipline = Disciplines.Orders.ToString(),
        //                DisciplineTask = isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
        //                Status = ((int)ScheduleStatus.OrderSaved),
        //                IsOrderForNextEpisode = assessment.Type.IsEqual(AssessmentType.Recertification.ToString()) ? true : false
        //            };
        //            if(baseScheduleRepository.AddScheduleEvent(pocScheduleEvent))
        //            {
        //                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.Id, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
        //                result = true;
        //            }
        //            //scheduleEvents.Add(pocScheduleEvent);
        //            // episode.Schedule = scheduleEvents.ToXml();
        //            //if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, new List<ScheduleEvent> { pocScheduleEvent }))
        //            //{
        //            //    result = true;
        //            //    Auditor.Log(pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.EventId, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
        //            //}
        //        }
        //        else
        //        {
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        //private bool CreatePlanofCare(T scheduleEvent,Assessment assessment, bool isNonOasis)
        //{
        //    var result = false;
        //    var planofCare = new PlanofCare();
        //    planofCare.Id = scheduleEvent.Id;
        //    planofCare.AssessmentId = assessment.Id;
        //    planofCare.AgencyId = Current.AgencyId;
        //    planofCare.PatientId = scheduleEvent.PatientId;
        //    planofCare.EpisodeId = scheduleEvent.EpisodeId;
        //    planofCare.Status = scheduleEvent.Status;
        //    planofCare.AssessmentType = assessment.Type.ToString();
        //    planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
        //    planofCare.IsNonOasis = isNonOasis;
        //    var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, scheduleEvent.PatientId);
        //    if (physician != null)
        //    {
        //        planofCare.PhysicianId = physician.Id;
        //    }
        //    planofCare.UserId = scheduleEvent.UserId;
        //    planofCare.Questions = this.Get485FromAssessment(assessment);
        //    planofCare.Data = planofCare.Questions.ToXml();

        //    if (baseScheduleRepository.AddScheduleEvent(scheduleEvent))
        //    {
        //        if (planofCareRepository.Add(planofCare))
        //        {
        //            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
        //            {
        //                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
        //            }
        //            result = true;
        //        }
        //        else
        //        {
        //            baseScheduleRepository.RemoveScheduleEventNew(Current.AgencyId,scheduleEvent.PatientId, scheduleEvent.Id);
        //            result = false;
        //        }
        //    }
        //    return result;
        //}

        #endregion

        public PlanofCare GetPlanOfCareByAssessment(Guid episodeId, Guid assessmentId)
        {
            return basePlanOfCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, episodeId, assessmentId);
        }
    }
}
