﻿

namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.ViewData;

    public abstract class BaseService
    {
        protected IAgencyRepository agencyRepository;
        protected AgencyServices Service { get; set; }

        //protected BaseService(IAgencyRepository agencyRepository)
        //{
        //    this.agencyRepository = agencyRepository;
        //}

        public GridViewData GetServiceAndLocationViewData(ParentPermission category, PermissionActions availablePermissionAction, List<PermissionActions> moreActions, bool IsLocationNeeded)
        {
            var viewData = new GridViewData { Service = this.Service };
            if (IsLocationNeeded)
            {
                viewData.Id = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
            }
            moreActions.Add(availablePermissionAction);
            var allPermission = Current.CategoryService(Current.AcessibleServices, category, moreActions.Select(a => (int)a).ToArray());
            if (allPermission.IsNotNullOrEmpty())
            {
                moreActions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == availablePermissionAction)
                    {
                        viewData.AvailableService = permission;
                    }
                    if (a == PermissionActions.Add)
                    {
                        viewData.NewPermissions = permission;
                    }
                    else if (a == PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }
                    else if (a == PermissionActions.ViewList)
                    {
                        viewData.ViewListPermissions = permission;
                    }
                });
            }
            return viewData;
        }

        
    }
}
