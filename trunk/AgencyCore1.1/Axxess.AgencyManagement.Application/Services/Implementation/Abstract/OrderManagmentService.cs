﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core;
using Axxess.Core.Enums;
using Axxess.Core.Infrastructure;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Entities.Extensions;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Application.Domain;
using Axxess.AgencyManagement.Application.Enums;
using System.Web.Mvc;
using Axxess.AgencyManagement.Application.ViewData;

namespace Axxess.AgencyManagement.Application.Services
{
    public abstract class OrderManagmentService<T, E> 
       where T: ITask, new()
       where E : CarePeriod, new()
    {
        #region Constructor
        protected readonly IAgencyService agencyService;
        private readonly  TaskService<T,E> baseScheduleService;
        protected  PhysicianOrderService<T, E> physicianOrderService;
        protected  PlanOfCareService<T,E> planOfCareService;
        protected  NoteService<T, E> noteService;
        protected  AgencyServices Service;


        public OrderManagmentService(
            IAgencyService agencyService,
            PhysicianOrderService<T, E> physicianOrderService, 
            TaskService<T,E> scheduleService,
            PlanOfCareService<T,E> planOfCareService,
            NoteService<T,E> noteService)
        {
            this.physicianOrderService = physicianOrderService;
            this.baseScheduleService = scheduleService;
            this.planOfCareService = planOfCareService;
            this.noteService = noteService;
            this.agencyService = agencyService;
        }

        #endregion

     
        public Order GetOrderForHistoryEdit(Guid patientId, Guid id, string type)
        {
            Order order = new Order();
            int typeNum = 0;
            if (!id.IsEmpty() && !patientId.IsEmpty() && int.TryParse(type, out typeNum) && Enum.IsDefined(typeof(OrderType), typeNum))
            {
                switch (typeNum)
                {
                    case (int)OrderType.PhysicianOrder:
                        order = physicianOrderService.GetPhysicianOrderForHistoryEdit(patientId, id);
                        break;
                    case (int)OrderType.HCFA485:
                    case (int)OrderType.NonOasisHCFA485:
                    case (int)OrderType.HCFA485StandAlone:
                        order = planOfCareService.GetPlanOfCareOrderForHistoryEdit(patientId, id, type);
                        break;
                    case (int)OrderType.PtEvaluation:
                    case (int)OrderType.PtReEvaluation:
                    case (int)OrderType.OtEvaluation:
                    case (int)OrderType.OtReEvaluation:
                    case (int)OrderType.StEvaluation:
                    case (int)OrderType.StReEvaluation:
                    case (int)OrderType.PTDischarge:
                    case (int)OrderType.SixtyDaySummary:
                        order = noteService.GetVisitNoteOrderForHistoryEdit(patientId, id, type);
                        break;
                    default:
                        order = GetOrderForHistoryEditAppSpecific(typeNum,id);
                        break;
                }
            }

            return order;
        }

        protected abstract Order GetOrderForHistoryEditAppSpecific(int typeNum, Guid id);

        public Order GetOrderForReceiving(Guid patientId, Guid id, string type)
        {
            Order order = new Order();
            order.Service = this.Service;
            int typeNum = 0;
            if (!id.IsEmpty() && !patientId.IsEmpty() && int.TryParse(type, out typeNum) && Enum.IsDefined(typeof(OrderType), typeNum))
            {

                switch (typeNum)
                {
                    case (int)OrderType.PhysicianOrder:
                        order = physicianOrderService.GetPhysicianOrderForReceiving(patientId, id);
                        break;
                    case (int)OrderType.HCFA485:
                    case (int)OrderType.NonOasisHCFA485:
                    case (int)OrderType.HCFA485StandAlone:
                        order = planOfCareService.GetPlanOfCareOrderForReceiving(patientId, id, type);
                        break;
                    case (int)OrderType.PtEvaluation:
                    case (int)OrderType.PtReEvaluation:
                    case (int)OrderType.OtEvaluation:
                    case (int)OrderType.OtReEvaluation:
                    case (int)OrderType.StEvaluation:
                    case (int)OrderType.StReEvaluation:
                    case (int)OrderType.PTDischarge:
                    case (int)OrderType.SixtyDaySummary:
                        order = noteService.GetVisitNoteOrderForReceiving(patientId, id, type);
                        break;
                    default:
                        order = GetOrderForReceivingAppSpecific(typeNum, patientId, id);
                        break;
                }
            }

            return order;
        }

        protected abstract Order GetOrderForReceivingAppSpecific(int typeNum, Guid patientId, Guid id);

        //TODO: If I have the time I should rework MarkOrdersAsSent. It is slooooooowwwwwwwwwwww.
        public JsonViewData MarkOrdersAsSent(FormCollection formCollection)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = true, errorMessage = "Orders could not be marked as sent to Physician" };
            viewData.ServiceId =(int) this.Service;
            viewData.Service = this.Service.ToString();
            if (formCollection != null)
            {
                viewData.Service = this.Service.ToString();
                bool sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
                formCollection.Remove("SendAutomatically");
                string[] keys = formCollection.AllKeys;
                if (keys != null && keys.Count() > 0)
                {
                    List<AgencyPhysician> physicians = new List<AgencyPhysician>();
                    int totalCount = 0;
                    int successfulCount = 0;
                    foreach (string key in keys)
                    {
                        int type;
                        if (int.TryParse(key, out type))
                        {
                            string[] answers = formCollection.GetValues(key);//.Join(",");
                            //string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (answers != null && answers.Length > 0)
                            {
                                totalCount += answers.Length;
                                switch (type)
                                {
                                    case (int)OrderType.PhysicianOrder:
                                        successfulCount += physicianOrderService.MarkPhysicianOrderAsSent(sendElectronically, answers, ref physicians);
                                        break;
                                    case (int)OrderType.HCFA485:
                                    case (int)OrderType.HCFA485StandAlone:
                                        successfulCount += planOfCareService.MarkPlanOfCareAsSent(sendElectronically, answers, ref physicians);
                                        break;
                                    case (int)OrderType.PtEvaluation:
                                    case (int)OrderType.PtReEvaluation:
                                    case (int)OrderType.PTReassessment:
                                    case (int)OrderType.OtEvaluation:
                                    case (int)OrderType.OtReEvaluation:
                                    case (int)OrderType.OTReassessment:
                                    case (int)OrderType.StEvaluation:
                                    case (int)OrderType.StReEvaluation:
                                    case (int)OrderType.MSWEvaluation:
                                    case (int)OrderType.PTDischarge:
                                    case (int)OrderType.SixtyDaySummary:
                                        successfulCount += noteService.MarkEvalNoteAsSent(sendElectronically, answers, ref physicians);
                                        break;
                                    default:
                                        successfulCount += MarkOrdersAsSentAppSpecific(type, sendElectronically, answers, ref physicians);
                                        break;
                                }
                                if (sendElectronically)
                                {
                                    if (successfulCount > 0 && physicians != null && physicians.Count > 0)
                                    {
                                        string subject = string.Format("{0} has sent you an order", Current.AgencyName);
                                        string bodyTemplate = MessageBuilder.ReadTextFrom("PhysicianOrderNotification");
                                        if (bodyTemplate.IsNotNullOrEmpty())
                                        {
                                            physicians.ForEach(physician =>
                                            {
                                                if (physician.EmailAddress.IsNotNullOrEmpty() && physician.EmailAddress.Trim().IsNotNullOrEmpty() && physician.EmailAddress.IsEmail())
                                                {
                                                    string pLastName = physician.LastName.IsNotNullOrEmpty() && physician.LastName.Trim().IsNotNullOrEmpty() && physician.LastName.Trim().Length >= 1 ? physician.LastName.Trim().Substring(0, 1) : string.Empty;
                                                    string lastName = pLastName.ToUpperCase() + pLastName.ToLowerCase();//physician.LastName.Trim().Substring(0, 1).ToUpper() + physician.LastName.Trim().Substring(1).ToLower();
                                                    string bodyText = MessageBuilder.PrepareTextFromTemplateBody(bodyTemplate, "recipientlastname", lastName, "senderfullname", Current.AgencyName);
                                                    Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                                                }
                                            });
                                        }
                                    }
                                }
                            }

                        }
                    }
                    if (totalCount == 0)
                    {
                        viewData.errorMessage = "No Orders were selected.";
                    }
                    else if (totalCount > 0)
                    {
                        if (successfulCount == totalCount)
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Orders have been marked as sent.";
                            viewData.IsOrdersToBeSentRefresh = true;
                            viewData.IsOrdersPendingRefresh = true;
                            viewData.IsDataCentersRefresh = true;
                        }
                        else if (successfulCount == 0)
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "A proble occurred while marking the orders as sent. Please try again.";
                        }
                        else
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = string.Format("{0} out of {1} orders have been marked as sent.", successfulCount, totalCount);
                            viewData.IsOrdersToBeSentRefresh = true;
                            viewData.IsOrdersPendingRefresh = true;
                            viewData.IsDataCentersRefresh = true;
                        }
                    }
                }
                else
                {
                    viewData.errorMessage = "No Orders were selected";
                }
            }
            else
            {
                viewData.errorMessage = "No Orders were selected";
            }

            return viewData;
        }

        protected abstract int MarkOrdersAsSentAppSpecific(int type, bool sendElectronically, string[] answers, ref List<AgencyPhysician> physicians);

        public JsonViewData MarkOrderAsReturned(Guid patientId, Guid id, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            JsonViewData viewData = new JsonViewData(false, "The Order failed to be updated.");
            bool result = false;
            if (!physicianSignatureDate.IsValid())
            {
                physicianSignatureDate = DateTime.Today;
            }
            switch (type)
            {
                case OrderType.PhysicianOrder:
                    result = physicianOrderService.MarkPhysicianOrderAsReturned(id, receivedDate, physicianSignatureDate);
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    result = planOfCareService.MarkPlanOfCareOrderAsReturned(patientId, id, receivedDate, physicianSignatureDate);
                    break;
               
                    //result = planOfCareService.MarkPlanOfCareOrderAsReturned(patientId, id, receivedDate, physicianSignatureDate);
                    //break;
                case OrderType.PtEvaluation:
                case OrderType.PtReEvaluation:
                case OrderType.OtEvaluation:
                case OrderType.OtReEvaluation:
                case OrderType.StEvaluation:
                case OrderType.StReEvaluation:
                case OrderType.MSWEvaluation:
                case OrderType.PTDischarge:
                case OrderType.SixtyDaySummary:
                    result = noteService.MarkEvalOrderAsReturned(patientId, id, receivedDate, physicianSignatureDate);
                    break;
                default:
                    result = MarkOrderAsReturnedAppSpecifc(id, type, receivedDate, physicianSignatureDate);
                    break;
            }
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.ServiceId = (int)this.Service;
                viewData.Service = this.Service.ToString();
                viewData.IsOrdersPendingRefresh = true;
                viewData.IsOrdersHistoryRefresh = true;
                viewData.errorMessage = "The Order was updated successfully.";

            }
            return viewData;
        }

        protected abstract bool MarkOrderAsReturnedAppSpecifc(Guid id, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate);

        public JsonViewData UpdateOrderDates(Guid id, Guid patientId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated. Please try again.", Service = this.Service.ToString() };
            bool result = false;
            switch (type)
            {
                case OrderType.PhysicianOrder:
                    result = physicianOrderService.UpdatePhysicianOrderDates(id, receivedDate, sendDate, physicianSignatureDate);
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    result = planOfCareService.UpdatePlanOfCareOrderDates(patientId, id, receivedDate, sendDate, physicianSignatureDate);
                    break;
                //case OrderType.HCFA485StandAlone:
                //    result = planOfCareService.UpdatePlanOfCareOrderDates(patientId, id, receivedDate, sendDate, physicianSignatureDate);
                //    break;
               
                case OrderType.PtEvaluation:
                case OrderType.PtReEvaluation:
                case OrderType.OtEvaluation:
                case OrderType.OtReEvaluation:
                case OrderType.StEvaluation:
                case OrderType.StReEvaluation:
                case OrderType.MSWEvaluation:
                case OrderType.PTDischarge:
                case OrderType.SixtyDaySummary:
                    result = noteService.UpdateEvalOrderDates(patientId, id, receivedDate, sendDate, physicianSignatureDate);
                    break;
                default:
                    result = UpdateOrderDatesAppSpecifc(type, id, receivedDate,sendDate, physicianSignatureDate);
                    break;
            }
            
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.IsOrdersHistoryRefresh = true;
                viewData.PatientId = patientId;
                viewData.errorMessage = "Order successfully updated";
            }
            return viewData;
        }

        protected abstract bool UpdateOrderDatesAppSpecifc(OrderType type, Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate);


        public OrderGridViewData GetOrdersGridViewData(PermissionActions availablePermissionAction, bool isLocationNeeded)
        {
            OrderGridViewData viewData = new OrderGridViewData { Service = this.Service };
            if (isLocationNeeded)
            {
                AgencyLocation branch = agencyService.GetBranchForSelection((int)this.Service);
                if (branch != null)
                {
                    viewData.Id = branch.Id;
                }
            }
            int[] actions = new int[] { (int)availablePermissionAction,  (int)PermissionActions.Edit, (int)PermissionActions.Export, (int)PermissionActions.SendToPhysician, (int)PermissionActions.ReceiveFromPhysician };
            Dictionary<int, AgencyServices> allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.OrderManagement, actions);

            if (allPermission.IsNotNullOrEmpty())
            {
                actions.ForEach(a =>
                {
                    AgencyServices permission = allPermission.GetOrDefault<int, AgencyServices>(a, AgencyServices.None);
                    if (a == (int)availablePermissionAction)
                    {
                        viewData.AvailableService = permission;
                        viewData.ViewListPermissions = permission;
                    }
                    if (a == (int)PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }
                    if (a == (int)PermissionActions.SendToPhysician)
                    {
                        viewData.SentPermissions = permission;
                    }
                    if (a == (int)PermissionActions.ReceiveFromPhysician)
                    {
                        viewData.ReceivePermissions = permission;
                    }
                    if (a == (int)PermissionActions.Edit)
                    {
                        viewData.EditPermissions = permission;
                    }
                });
            }
            return viewData;
        }

        
        public List<Order> GetOrdersToBeSent(Guid branchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            List<Order> orders = GetOrdersByStatus(branchId, startDate, endDate, new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician }, false, false) ?? new List<Order>();
            return orders.Where(o => o.PhysicianAccess == sendAutomatically).ToList();
        }

        public List<Order> GetOrdersByStatus(Guid branchId, DateTime startDate, DateTime endDate, List<int> status, bool isPending, bool isCompleted)
        {
            List<Order> orders = new List<Order>();
            List<T> schedules = baseScheduleService.GetOrderScheduleEvents(branchId, startDate, endDate, status);
            if (schedules.IsNotNullOrEmpty())
            {
                Dictionary<int, List<int>> permissions = Current.Permissions.GetCategoryPermission(ParentPermission.OrderManagement);
                bool canEdit = false;
                if (isPending)
                {
                    canEdit = permissions.GetOrDefault((int)PermissionActions.ReceiveFromPhysician, new List<int>()).Contains((int)Service);
                }
                if (isCompleted)
                {
                    canEdit = permissions.GetOrDefault((int)PermissionActions.Edit, new List<int>()).Contains((int)Service);
                }
                List<T> physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules.Count > 0)
                {
                    List<Order> physicianOrders = physicianOrderService.GetPhysicianOrders(startDate, endDate, physicianOrdersSchedules, isPending, isCompleted);
                    if (physicianOrders.IsNotNullOrEmpty())
                    {
                        orders.AddRange(physicianOrders);
                    }
                }
                List<T> planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareOrdersSchedules.Count > 0)
                {
                    List<Order> planofCareOrders = planOfCareService.GetPlanOfCareOrders(planofCareOrdersSchedules, isPending, isCompleted);
                    if (planofCareOrders.IsNotNullOrEmpty())
                    {
                        orders.AddRange(planofCareOrders);
                    }
                }

                GetOrderStatusAppSpecific(isPending, isCompleted, orders, schedules);

                List<T> evalOrdersSchedule = schedules.Where(s => DisciplineTaskFactory.AllNoteOrders().Contains(s.DisciplineTask) && s.Status == status[1]).ToList();
                if (evalOrdersSchedule.IsNotNullOrEmpty())
                {
                    List<Order> evalOrders = noteService.GetEvalOrders(evalOrdersSchedule, isPending, isCompleted);
                    if (evalOrders.IsNotNullOrEmpty())
                    {
                        orders.AddRange(evalOrders);
                    }
                }
                orders.ForEach(o => { o.CanUserEdit = canEdit; });
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        protected abstract void GetOrderStatusAppSpecific(bool isPending, bool isCompleted, List<Order> orders, List<T> schedules);

        public List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate)
        {
            List<Order> orders = new List<Order>();
            List<T> schedules = baseScheduleService.GetPatientOrderScheduleEvents(patientId, startDate, endDate);
            if (schedules.IsNotNullOrEmpty())
            {
                var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Orders);
                var canPrint = permissions.GetOrDefault((int)PermissionActions.Print, new List<int>()).Contains((int)Service);
                var canDelete = permissions.GetOrDefault((int)PermissionActions.Delete, new List<int>()).Contains((int)Service);
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules.Count > 0)
                {
                    List<Order> physicianOrders = physicianOrderService.GetPhysicianOrders(patientId, startDate, endDate, physicianOrdersSchedules);
                    if (physicianOrders.IsNotNullOrEmpty())
                    {
                        orders.AddRange(physicianOrders);
                    }
                }
                List<T> planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareOrdersSchedules.Count > 0)
                {
                    List<Order> planofCareOrders = planOfCareService.GetPlanOfCareOrders(patientId, planofCareOrdersSchedules);
                    if (planofCareOrders.IsNotNullOrEmpty())
                    {
                        orders.AddRange(planofCareOrders);
                    }
                }
                GetPatientOrdersAppSpecific(patientId, orders, schedules);
                List<T> evalOrdersSchedule = schedules.Where(s => DisciplineTaskFactory.AllNoteOrders().Contains(s.DisciplineTask)).ToList();
                if (evalOrdersSchedule.Count > 0)
                {
                    List<Order> evalOrders = noteService.GetEvalOrders(patientId, evalOrdersSchedule);
                    if (evalOrders.IsNotNullOrEmpty())
                    {
                        orders.AddRange(evalOrders);
                    }
                }
                orders.ForEach(o => { 
                    o.CanUserDelete = o.CanUserDelete && canDelete;
                    o.CanUserPrint = canPrint;
                });
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        protected abstract void GetPatientOrdersAppSpecific(Guid patientId, List<Order> orders, List<T> schedules);

        public JsonViewData UpdatePhysicianOrderAndPOCStatus(Guid eventId, Guid patientId, string orderType, string actionType, string reason)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false };
            string errorMessage = "Your Order could not be updated.";
            string messageType = actionType.ToLower() + (actionType.EndsWith("e") ? "d" : "ed");
            if (orderType == "PhysicianOrder")
            {
                viewData = physicianOrderService.ProcessPhysicianOrder(patientId, eventId, actionType, reason);
                viewData.errorMessage = viewData.isSuccessful ? "The order has been " + messageType + " successfully." : errorMessage;
            }
            else if (orderType == "PlanofCare" || orderType == "PlanofCareStandAlone")
            {
                viewData = planOfCareService.UpdatePlanofCareStatus(patientId, eventId, actionType, reason);
                viewData.errorMessage = viewData.isSuccessful ? "The plan of care has been " + messageType + " successfully." : errorMessage;
            }
            return viewData;
        }
      
    }
}
