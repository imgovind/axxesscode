﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;

    using  AAE=Axxess.AgencyManagement.Entities;

    public abstract class AdmissionServiceAbstract 
    {
        #region Private Members

        protected readonly IPatientRepository patientRepository;
        protected readonly PatientAdmissionAbstract admissionRepository;
        protected readonly PatientProfileAbstract patientProfileRepository;

        #endregion

        #region Constructor

        public AdmissionServiceAbstract(IPatientRepository patientRepository,PatientProfileAbstract patientProfileRepository, PatientAdmissionAbstract admissionRepository)
        {
            this.patientRepository = patientRepository;
            this.patientProfileRepository = patientProfileRepository;
            this.admissionRepository = admissionRepository;
        }

        #endregion


        #region Admission

        public JsonViewData AdmissionPatientNew(Patient patient, Profile profile)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient Admission information could not be created." };
            if (patient != null)
            {
                if (profile != null)
                {
                    if (!patient.Id.IsEmpty())
                    {
                        var exisitingPatientData = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
                        if (exisitingPatientData != null)
                        {
                            var exisitingProfileData = patientProfileRepository.GetProfileOnly(Current.AgencyId, patient.Id);
                            if (exisitingProfileData != null)
                            {
                                var rules = new List<Validation>();

                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
                                rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of Birth  for the patient is not in the valid range.  <br/>"));
                                rules.Add(new Validation(() => profile.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
                                if (patient.PatientIdNumber.IsNotNullOrEmpty())
                                {
                                    bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
                                    rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                                }
                                if (patient.MedicareNumber.IsNotNullOrEmpty())
                                {
                                    bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                                }
                                if (patient.MedicaidNumber.IsNotNullOrEmpty())
                                {
                                    bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                                }
                                if (patient.SSN.IsNotNullOrEmpty())
                                {
                                    bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
                                    rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
                                }
                                rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) && !patient.SSN.IsSSN(), "Patient SSN is not in valid format.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(profile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
                                rules.Add(new Validation(() => !profile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(profile.DischargeDate.ToString()), "Patient discharge date is required.  <br/>"));
                                rules.Add(new Validation(() => !profile.DischargeDate.ToString().IsValidDate(), "Patient discharge date is not in valid format.  <br/>"));

                                if (patient.Ethnicities.IsNullOrEmpty())
                                {
                                    rules.Add(new Validation(() => patient.EthnicRaces.IsNullOrEmpty(), "Patient Race/Ethnicity is required."));
                                }

                                if (profile.PaymentSource.IsNullOrEmpty())
                                {
                                    rules.Add(new Validation(() => profile.PaymentSources.IsNullOrEmpty(), "Patient Payment Source is required."));
                                }
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
                                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
                                rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));


                                if (profile.PrimaryInsurance >= 1000)
                                {
                                    rules.Add(new Validation(() => profile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                                }
                                if (profile.SecondaryInsurance >= 1000)
                                {
                                    rules.Add(new Validation(() => profile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                                }
                                if (profile.TertiaryInsurance >= 1000)
                                {
                                    rules.Add(new Validation(() => profile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                                }
                                rules.Add(new Validation(() => !this.IsValidAdmissionPeriod(patient.Id, profile.StartofCareDate, profile.DischargeDate), "Admission period (SOC and DC date range)  is not in the valid date range."));
                                var entityValidator = new EntityValidator(rules.ToArray());
                                entityValidator.Validate();
                                if (entityValidator.IsValid)
                                {
                                    patient.AgencyId = Current.AgencyId;
                                    patient.Encode();// setting string arrays to one field
                                    profile.Encode();
                                    if (admissionRepository.PatientAdmissionAdd(patient, profile, exisitingPatientData,exisitingProfileData))
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "Patient Admission has been successfully created.";
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "A problem occured while saving the admission.";
                                    }
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = entityValidator.Message;
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Patient profile could not be found.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Patient could not be found.";
                        }
                    }
                }
            }
          return  viewData;
        }

        public JsonViewData AdmissionPatientEdit(Patient patient, Profile profile)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient Admission information could not be saved." };
            if (patient != null)
            {
                if (profile != null)
                {
                    if (!patient.Id.IsEmpty() && !profile.AdmissionId.IsEmpty())
                    {
                        var currentProfileData = patientProfileRepository.GetProfileOnly( Current.AgencyId,patient.Id);
                        if (currentProfileData != null)
                        {
                            var rules = new List<Validation>();
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
                            rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
                            rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
                            rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) && !patient.SSN.IsSSN(), "Patient SSN is not in valid format.  <br/>"));
                            rules.Add(new Validation(() => string.IsNullOrEmpty(profile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
                            rules.Add(new Validation(() => !profile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));

                            if (patient.PatientIdNumber.IsNotNullOrEmpty())
                            {
                                bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
                                rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                            }
                            if (patient.MedicareNumber.IsNotNullOrEmpty())
                            {
                                bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                                rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                            }
                            if (patient.MedicaidNumber.IsNotNullOrEmpty())
                            {
                                bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                                rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                            }
                            if (patient.SSN.IsNotNullOrEmpty())
                            {
                                bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
                                rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
                            }
                            if ( profile.PrimaryInsurance >= 1000)
                            {
                                rules.Add(new Validation(() => profile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance's Health Plan Id is required."));
                            }
                            if (profile.SecondaryInsurance >= 1000)
                            {
                                rules.Add(new Validation(() => profile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance's Health Plan Id is required."));
                            }
                            if ( profile.TertiaryInsurance >= 1000)
                            {
                                rules.Add(new Validation(() => profile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance's Health Plan Id is required."));
                            }
                            if (!currentProfileData.AdmissionId.IsEmpty() && (currentProfileData.AdmissionId != profile.AdmissionId || (currentProfileData.AdmissionId == profile.AdmissionId && currentProfileData.Status == (int)PatientStatus.Discharged)))
                            {
                                rules.Add(new Validation(() => string.IsNullOrEmpty(profile.DischargeDate.ToString()), "Patient's discharge date is required.  <br/>"));
                                rules.Add(new Validation(() => !profile.DischargeDate.ToString().IsValidDate(), "Patient's discharge date is not in valid format.  <br/>"));
                                rules.Add(new Validation(() => profile.StartofCareDate.Date > profile.DischargeDate.Date, "Patient's discharge date has to be greater than Start of care date .  <br/>"));
                            }
                            rules.Add(new Validation(() => !IsValidAdmissionPeriod(profile.AdmissionId, patient.Id, profile.StartofCareDate, profile.DischargeDate), "Admission period (SOC and DC date range) is not in the valid date range."));

                            var entityValidator = new EntityValidator(rules.ToArray());
                            entityValidator.Validate();
                            if (entityValidator.IsValid)
                            {
                                patient.AgencyId = Current.AgencyId;
                                patient.Encode();// setting string arrays to one field
                                profile.Encode();
                                if (admissionRepository.PatientAdmissionEdit(patient,profile))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Patient Admission has been successfully updated.";
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "A problem occured while saving the admission.";
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = entityValidator.Message;
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Patient data not found. Try again.";
                        }
                    }
                }
            }
            return viewData;
        }

        public bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                    if (profile != null)
                    {
                        var oldAdmissionId = profile.AdmissionId;
                        var admission = admissionRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                        if (admission != null)
                        {
                            profile.AdmissionId = admission.Id;
                            admission.PatientData = patient.ToXml();
                            admission.ProfileData = profile.ToXml();
                            if (patientProfileRepository.UpdateModal(profile))
                            {
                                if (admissionRepository.UpdatePatientAdmissionDateModal(admission))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, profile.Id, profile.Id.ToString(), LogType.Patient, LogAction.PatientAdmissionPeriodEdited, string.Empty);
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodSetCurrent, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    profile.AdmissionId = oldAdmissionId;
                                    patientProfileRepository.UpdateModal(profile);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public JsonViewData DeletePatientAdmission(Guid patientId, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be deleted." };
            var admission = admissionRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
            if (admission != null)
            {
                admission.IsDeprecated = true;
                admission.IsActive = false;
                if (admissionRepository.UpdatePatientAdmissionDateModal(admission))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The patient admission period is deleted ";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The patient admission period is not deleted ";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The patient admission period not found to delete. Try again.";
            }
            return viewData;
        }

        //public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        //{
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    var admission = new PatientAdmissionDate();
        //    if (patient != null)
        //    {
        //        admission = GetIfExitOrCreate(patient);
        //    }
        //    return admission;
        //}

        //public PatientAdmissionDate GetIfExitOrCreate(Patient patient)
        //{
        //    var admission = new PatientAdmissionDate();
        //    if (patient != null)
        //    {
        //        var admissionId = Guid.NewGuid();
        //        var oldAdmissionId = patient.AdmissionId;
        //        patient.AdmissionId = admissionId;
        //        admission = new PatientAdmissionDate
        //        {
        //            Id = admissionId,
        //            AgencyId = Current.AgencyId,
        //            PatientId = patient.Id,
        //            StartOfCareDate = patient.StartofCareDate,
        //            DischargedDate = patient.DischargeDate,
        //            Status = patient.Status,
        //            IsActive = true,
        //            IsDeprecated = false,
        //            Created = DateTime.Now,
        //            Modified = DateTime.Now,
        //            PatientData = patient.ToXml()
        //        };
        //        if (patientRepository.Update(patient))
        //        {
        //            if (admissionRepository.AddPatientAdmissionDate(admission))
        //            {
        //                return admission;
        //            }
        //            else
        //            {
        //                patient.AdmissionId = oldAdmissionId;
        //                patientRepository.Update(patient);
        //            }
        //        }
        //    }
        //    return admission;
        //}



        public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        {
            var patientWithProfile = patientProfileRepository.GetPatientWithProfile(Current.AgencyId, patientId);
            var admission = new PatientAdmissionDate();
            if (patientWithProfile != null)
            {
                admission = GetIfExitOrCreate(patientWithProfile);
            }
            return admission;
        }

        public PatientAdmissionDate GetIfExitOrCreate(Patient patientWithProfile)
        {
            var admission = new PatientAdmissionDate();
            if (patientWithProfile != null)
            {
                var admissionId = Guid.NewGuid();
                var oldAdmissionId = patientWithProfile.Profile.AdmissionId;
                patientWithProfile.Profile.AdmissionId = admissionId;
                admission = new PatientAdmissionDate
                {
                    Id = admissionId,
                    AgencyId = Current.AgencyId,
                    PatientId = patientWithProfile.Id,
                    StartOfCareDate = patientWithProfile.Profile.StartofCareDate,
                    DischargedDate = patientWithProfile.Profile.DischargeDate,
                    Status = patientWithProfile.Profile.Status,
                    IsActive = true,
                    IsDeprecated = false,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    PatientData = patientWithProfile.ToXml(),
                    ProfileData = patientWithProfile.Profile.ToXml()
                };
                if (patientProfileRepository.UpdatePatientForAdmissionDateId(Current.AgencyId,patientWithProfile.Id, patientWithProfile.Profile.AdmissionId))
                {
                    if (!admissionRepository.AddPatientAdmissionDate(admission))
                    {
                        patientProfileRepository.UpdatePatientForAdmissionDateId(Current.AgencyId, patientWithProfile.Id, oldAdmissionId);
                    }
                }
            }
            return admission;
        }

        public Patient GetAdmissionPatientInfo(Guid patientId, Guid Id, string Type)
        {
            var admissionPatient = new Patient();
            var profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
            if (profile != null)
            {
                if (Type.IsEqual("edit") && !Id.IsEmpty())
                {
                    var admission = admissionRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                    if (admission != null && admission.PatientData.IsNotNullOrEmpty())
                    {
                        admissionPatient = admission.PatientData.ToObject<Patient>() ?? patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                        admissionPatient.Profile = admission.ProfileData.ToObject<Profile>() ?? profile;
                        admissionPatient.Profile.AdmissionId = admission.Id;
                        admissionPatient.Profile.Id = admission.PatientId;
                        admissionPatient.Profile.StartofCareDate = admission.StartOfCareDate;
                        admissionPatient.Profile.DischargeDate = admission.DischargedDate;
                        admissionPatient.Profile.Status = (!profile.AdmissionId.IsEmpty() && (admissionPatient.Profile.AdmissionId != profile.AdmissionId || (admissionPatient.Profile.AdmissionId == profile.AdmissionId && profile.Status == (int)PatientStatus.Discharged))) ? (int)PatientStatus.Discharged : (int)PatientStatus.Active;

                    }
                }
                else if (Type.IsEqual("new"))
                {
                    admissionPatient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    admissionPatient.Profile = profile;
                    //admissionPatient.Profile.AdmissionId = Guid.Empty;
                }
            }
            return admissionPatient;
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid patientId,  Guid Id)
        {
            return admissionRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
        }

        public IList<PatientAdmissionDate> GetPatientAdmissionPeriods(Guid patientId)
        {
            return admissionRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
        }

        private bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = admissionRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                result = !admissionPeriods
                    .Where(a => a.Id != admissionId && a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    .Any(a => (startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || 
                        (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || 
                        (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || 
                        (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || 
                        (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date));
            }
            return result;
        }

        private bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = admissionRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                result = !admissionPeriods
                    .Where(a => a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    .Any(a => (startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || 
                        (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || 
                        (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || 
                        (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || 
                        (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date));
            }
            return result;
        }

        public IList<SelectListItem> GetPatientAdmissionDateSelectList(Guid patientId)
        {
            return admissionRepository.GetPatientAdmissionDateSelectList(Current.AgencyId, patientId);
        }

        #endregion
    }
}
