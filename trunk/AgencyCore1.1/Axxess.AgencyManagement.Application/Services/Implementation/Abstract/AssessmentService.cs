﻿using Axxess.AgencyManagement.Application.Services.Implementation.Abstract;
namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.Api;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using System.Web.Mvc;
    using System.Web;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.AgencyManagement.Entities.Common;
    using Axxess.AgencyManagement.Application.Helpers;

    using Telerik.Web.Mvc.Infrastructure;
    using MySql.Data.MySqlClient;
    using SubSonic.DataProviders;
    using System.Transactions;
    using SubSonic.Query;

    public abstract class AssessmentService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {

        #region Constructor / Members

        private static readonly GrouperAgent grouperAgent = new GrouperAgent();
        private static readonly ValidationAgent validationAgent = new ValidationAgent();

        protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;
        protected IPhysicianRepository physicianRepository;
        protected ILookupRepository lookupRepository;
        protected PlanOfCareAbstract planofCareRepository;
        private AssessmentAbstract baseAssessmentRepository;
        protected MongoAbstract mongoRepository;
        protected VitalSignAbstract vitalSignRepository;

        private TaskScheduleAbstract<T> baseScheduleRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;
        protected AgencyServices Service { get; set; }


        protected AssessmentService(TaskScheduleAbstract<T> baseScheduleRepository, AssessmentAbstract assessmentRepository, EpisodeAbstract<E> baseEpisodeRepository, MongoAbstract mongoRepository, VitalSignAbstract vitalSignRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseAssessmentRepository = assessmentRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
            this.mongoRepository = mongoRepository;
            this.vitalSignRepository = vitalSignRepository;
        }

        #endregion

        #region IAssessmentService Members

        public OasisViewData SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles, string assessmentType)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            string action = "{0}_Action".FormatWith(assessmentType);
            string assessmentAction = formCollection.Get(action);
            if (assessmentAction.IsNotNullOrEmpty())
            {
                switch (assessmentAction)
                {
                    case "New":
                        oasisViewData = AddAssessment(formCollection, assessmentType);
                        break;
                    case "Edit":
                        oasisViewData = UpdateAssessment(formCollection, httpFiles, assessmentType);
                        break;
                }
            }
            return oasisViewData;
        }

        public OasisViewData UpdateWound(WoundSaveArguments saveArguments, HttpFileCollectionBase httpFiles, FormCollection formCollection)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false, errorMessage = "Unable to " + saveArguments.Action + " wound information. Please try again." };
            var assessment = GetAssessmentWithQuestions(saveArguments.PatientGuid, saveArguments.Id);
            if (assessment != null)
            {
                IDictionary<string, Question> questions = assessment.Questions.ToOASISDictionary();
                var taskAssets = new List<Guid>();
                List<Guid> savedAssetIds = null;
                var assetXml = string.Empty;
                bool hasAssets = AssetHelper.ContainsAssets(httpFiles);
                if (hasAssets)
                {
                    var data = baseScheduleRepository.GetScheduleTaskByColumns(Current.AgencyId, saveArguments.PatientGuid, saveArguments.Id, "Asset");
                    if (data.IsNotNullOrEmpty())
                    {
                        assetXml = data["Asset"];
                        taskAssets = (assetXml.IsNotNullOrEmpty() ? assetXml.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();
                    }
                }
                assessment.Questions = WoundHelper<Question>.Update(saveArguments, httpFiles, formCollection, questions, taskAssets, out savedAssetIds);
                if (hasAssets && savedAssetIds.IsNullOrEmpty())
                {
                    oasisViewData.errorMessage = "A problem occured while saving the attached asset.";
                    return oasisViewData;
                }
                if (!hasAssets || baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, assessment.PatientId, assessment.Id, 0, taskAssets.ToXml(), null))
                {
                    if(mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                    {
                        oasisViewData.isSuccessful = true;
                        oasisViewData.errorMessage = "Wound has been successfully saved.";
                        oasisViewData.assessmentId = assessment.Id;
                        oasisViewData.PatientId = assessment.PatientId;
                        oasisViewData.Service = Service.ToString();
                        oasisViewData.ServiceId = (int)Service;
                        oasisViewData.IsDataCentersRefresh = hasAssets;
                        oasisViewData.IsMyScheduleTaskRefresh = hasAssets;
                    }
                    //Undo changes since mongo failed to save questions
                    else if (hasAssets)
                    {
                        baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, assessment.PatientId, assessment.Id, 0, assetXml, (bool?)null);
                        foreach (var assetId in savedAssetIds)
                        {
                            AssetHelper.Remove(assetId);
                        }
                    }
                }
            }
            return oasisViewData;
        }

        public OasisViewData WoundSetPostion(Guid Id, Guid PatientId, WoundPosition woundToPositon)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false, errorMessage = "Unable to update wound information. Please try again." };
            var assessment = GetAssessmentWithQuestions(PatientId, Id);
            if (assessment != null)
            {
                WoundHelper<Question>.CheckAndSetPosition(assessment.Questions, woundToPositon);
                if (mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                {
                    oasisViewData.isSuccessful = true;
                    oasisViewData.errorMessage = "Wound's position has been successfully updated.";
                    oasisViewData.assessmentId = assessment.Id;
                    oasisViewData.Service = Service.ToString();
                    oasisViewData.ServiceId = (int)Service;
                }
            }
            return oasisViewData;
        }

        public OasisViewData WoundDelete(Guid Id, Guid PatientId, int WoundNumber, int WoundCount)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false, errorMessage = "Unable to delete wound. Please try again." };
            var assessment = GetAssessmentWithQuestions(PatientId, Id);
            if (assessment != null)
            {
                IDictionary<string, Question> questions = assessment.Questions.ToOASISDictionary();
                Guid assetId;
                List<Guid> oldAssetIds = null;
                if (WoundHelper<Question>.Delete(WoundNumber, WoundCount, questions, out assetId))
                {
                    Func<Guid, Guid, Guid, Guid, bool> assetDeletion = (id, pid, agencyid, assetid) =>
                    {
                        var assetIds = baseScheduleRepository.GetScheduleTaskAssets(agencyid, pid, id);
                        oldAssetIds = assetIds.DeepClone();
                        if (AssetHelper.Delete(assetId, assetIds))
                        {
                            return baseScheduleRepository.UpdateScheduleTaskPartial(agencyid, pid, id, 0, assetIds.ToXml());
                        }
                        return false;
                    };
                    if (assetId.IsEmpty() || assetDeletion(Current.AgencyId, PatientId, Id, assetId))
                    {
                        assessment.Questions = questions.Values.ToList();
                        if (mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                        {
                            oasisViewData.isSuccessful = true;
                            oasisViewData.errorMessage = "Wound has been successfully deleted.";
                            oasisViewData.assessmentId = assessment.Id;
                            oasisViewData.Service = Service.ToString();
                            oasisViewData.ServiceId = (int)Service;
                        }
                        else if (!assetId.IsEmpty() && oldAssetIds != null)
                        {
                            baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, PatientId, Id, 0, oldAssetIds.ToXml());
                            AssetHelper.Restore(assetId);
                        }
                    }
                }
            }
            return oasisViewData;
        }

        public WoundViewData<Question> GetWound(Guid Id, Guid PatientId, int WoundNumber)
        {
            var viewData = new WoundViewData<Question>();
            var assessment = GetAssessmentWithQuestions(PatientId, Id);
            if (assessment != null)
            {
                viewData.Position = new WoundPosition(WoundNumber, 0, 0);
                viewData.EventId = assessment.Id;
                viewData.PatientId = assessment.PatientId;
                viewData.Type = assessment.TypeName;
                viewData.Data = WoundHelper<Question>.GetWound(assessment.ToDictionary(), WoundNumber);
            }
            else
            {
                throw new HttpException("500");
            }
            return viewData;
        }

        public bool AddScheduleTaskAndAssessment(Assessment assessment, T scheduleEvent)
        {
            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add the tak and assessment." };
            var result = false;
            if (baseScheduleRepository.AddScheduleTask(scheduleEvent))
            {
                if (baseAssessmentRepository.Add(assessment))
                {
                    result = true;
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
                }
                else
                {
                    result = false;
                    baseScheduleRepository.RemoveScheduleTaskFully(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                }
            }
            return result;
        }

        public abstract string GetDiagnosisData(Assessment assessment);

        public Assessment LoadCategory(Guid Id, Guid PatientId, string categoryName)
        {
            Assessment assessment = null;
            using (var scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
            {
                assessment = GetAssessmentWithScheduleType(PatientId, Id, true);
                if (assessment != null)
                {
                    var isFrozen = Current.IsAgencyFrozen;
                    var permission = Current.Permissions;
                    assessment.IsUserCanApprove = !isFrozen && !ScheduleStatusFactory.OASISAfterQA().Contains(assessment.Status) && permission.IsInPermission(this.Service, ParentPermission.QA, PermissionActions.Approve);
                    assessment.IsUserCanReturn = !isFrozen && assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview && permission.IsInPermission(this.Service, ParentPermission.QA, PermissionActions.Return);
                    if (categoryName.IsNotNullOrEmpty())
                    {
                        Action vitalSignFunc = () =>
                        {
                            var vitalSign = vitalSignRepository.GetEntitiesVitalSign(Id, PatientId, Current.AgencyId);
                            if (vitalSign != null)
                            {
                                assessment.VitalSignLog = vitalSign;
                            }
                        };
                        if (categoryName.IsEqual(AssessmentCategory.Demographics.ToString()))
                        {

                            assessment.IsUserCanLoadPrevious = !isFrozen && !ScheduleStatusFactory.OASISCompleted(true).Contains(assessment.Status) && permission.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.LoadPrevious);
                            assessment.IsUserCanSeeSticky = !isFrozen && (int)ScheduleStatus.OasisReturnedForClinicianReview == assessment.Status && permission.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.ViewStickyNotes);
                            if (assessment.IsUserCanSeeSticky)
                            {
                                assessment.StatusComment = baseScheduleRepository.GetReturnReason(Current.AgencyId, assessment.PatientId, assessment.Id, Current.UserId);
                            }
                        }
                        else if (categoryName.IsEqual(AssessmentCategory.Medications.ToString()))
                        {
                            scs.CurrentConnection.ChangeDatabase("agencymanagement");
                            var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                            if (medicationProfile != null)
                            {
                                var permissions = Current.CategoryService(this.Service, ParentPermission.MedicationProfile, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Add, (int)PermissionActions.Edit, (int)PermissionActions.Sign, (int)PermissionActions.Delete, (int)PermissionActions.Reactivate, (int)PermissionActions.Deactivate });
                                if (permissions.IsNotNullOrEmpty())
                                {
                                    var isNotFrozen = !Current.IsAgencyFrozen;
                                    medicationProfile.IsUserCanAdd = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    medicationProfile.IsUserCanEdit = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    medicationProfile.IsUserCanSign = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Sign, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    medicationProfile.IsUserCanDelete = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    medicationProfile.IsUserCanReactivate = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Reactivate, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    medicationProfile.IsUserCanDiscontinue = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Deactivate, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    medicationProfile.IsUserCanViewList = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None).Has(this.Service);

                                }
                                assessment.MedicationProfile = medicationProfile.ToXml();
                            }
                        }
                        else if (categoryName.Contains(AssessmentCategory.PatientHistory.ToString()))
                        {
                            vitalSignFunc.Invoke();
                            scs.CurrentConnection.ChangeDatabase("agencymanagement");
                            var allergyProfile = patientRepository.GetAllergyProfileByPatient(PatientId, Current.AgencyId);
                            if (allergyProfile != null)
                            {
                                var permissions = Current.CategoryService(this.Service, ParentPermission.AllergyProfile, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Restore });
                                if (permissions.IsNotNullOrEmpty())
                                {
                                    var isNotFrozen = !Current.IsAgencyFrozen;
                                    allergyProfile.IsUserCanEdit = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    allergyProfile.IsUserCanDelete = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                    allergyProfile.IsUserCanRestore = permissions.GetOrDefault<int, AgencyServices>((int)PermissionActions.Restore, AgencyServices.None).Has(this.Service) && isNotFrozen;
                                }
                                assessment.AllergyProfile = allergyProfile.ToXml();
                            }
                            assessment.DiagnosisDataJson = GetDiagnosisData(assessment);
                        }
                        else if (categoryName.Contains(AssessmentCategory.SuppliesDme.ToString()) || categoryName.Contains(AssessmentCategory.Supplies.ToString()))
                        {
                            //assessment.Supplies = assessment.Supply.ToObject<List<Supply>>() ?? new List<Supply>();
                        }
                        else if (categoryName.Contains(AssessmentCategory.Pain.ToString()) || categoryName.Contains(AssessmentCategory.Endocrine.ToString()))
                        {
                            vitalSignFunc.Invoke();
                        }
                        else if (categoryName.Contains(AssessmentCategory.Integumentary.ToString()))
                        {
                            assessment.WoundCareJson = WoundHelper<Question>.GetPositions(assessment.ToDictionary());
                        }
                        if (categoryName.IsEqual(AssessmentCategory.TransferDischargeDeath.ToString()) || categoryName.IsEqual(AssessmentCategory.OrdersDisciplineTreatment.ToString()) || (assessment.Type.IsEqual(AssessmentType.FollowUp.ToString()) && categoryName.IsEqual(AssessmentCategory.TherapyNeed.ToString())))
                        {
                            assessment.IsLastTab = true;
                        }
                    }
                }
            }
            return assessment;
        }

        public Assessment GetAssessmentWithQuestions(Guid assessmentId)
        {
            var assessment = baseAssessmentRepository.Get(Current.AgencyId, assessmentId);
            if (assessment != null)
            {
                SetQuestions(assessment);
            }
            return assessment;
        }

        public Assessment GetAssessmentWithQuestions(Guid patientId, Guid eventId)
        {
            var assessment = baseAssessmentRepository.Get(Current.AgencyId, patientId, eventId);
            if (assessment != null)
            {
                SetQuestions(assessment);
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid assessmentId)
        {
            return baseAssessmentRepository.Get(Current.AgencyId, assessmentId);
        }

        public Assessment GetAssessment(Guid patientId, Guid eventId)
        {
            return baseAssessmentRepository.Get(Current.AgencyId, patientId, eventId);
        }

        public Assessment GetAssessmentWithComment(Guid PatientId, Guid Id)
        {
            var assessment = GetAssessmentWithScheduleType(PatientId, Id, true);
            if (assessment != null)
            {
                var permission = Current.Permissions;
                if (permission.IsNotNullOrEmpty())
                {
                    var isFrozen = Current.IsAgencyFrozen;
                    assessment.IsUserCanApprove = !isFrozen && !ScheduleStatusFactory.OASISAfterQA().Contains(assessment.Status) && permission.IsInPermission(this.Service, ParentPermission.QA, PermissionActions.Approve);
                    assessment.IsUserCanReturn = !isFrozen && assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview && permission.IsInPermission(this.Service, ParentPermission.QA, PermissionActions.Return);
                    assessment.IsUserCanLoadPrevious = !isFrozen && !ScheduleStatusFactory.OASISCompleted(true).Contains(assessment.Status) && permission.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.LoadPrevious);
                    assessment.IsUserCanSeeSticky = !isFrozen && (int)ScheduleStatus.OasisReturnedForClinicianReview == assessment.Status && permission.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.ViewStickyNotes);
                    if (assessment.IsUserCanSeeSticky)
                    {
                        assessment.StatusComment = baseScheduleRepository.GetReturnReason(Current.AgencyId, assessment.PatientId, assessment.Id, Current.UserId);
                    }
                }
            }
            return assessment;
        }

        public Assessment GetAssessmentWithScheduleType(Guid patientId, Guid assessmentId, bool includeData)
        {
            var assessment = baseAssessmentRepository.Get(Current.AgencyId, patientId, assessmentId);
            if (assessment != null)
            {
                assessment.Service = this.Service;
                if (includeData)
                {
                    SetQuestions(assessment);
                }
                // assessment.ShowOasisVendorButton = Current.OasisVendorExist;
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, assessment.PatientId, assessmentId);
                if (scheduleEvent != null)
                {
                    assessment.TaskName = scheduleEvent.DisciplineTaskName;
                    assessment.Status = scheduleEvent.Status;
                    if (scheduleEvent.EventDate.IsValid())
                    {
                        assessment.ScheduleDate = scheduleEvent.EventDate;
                    }
                    if (scheduleEvent.VisitDate.IsValid())
                    {
                        assessment.VisitDate = scheduleEvent.VisitDate;
                    }
                    assessment.Version = scheduleEvent.Version;
                    if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                    {
                        switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
                        {
                            case DisciplineTasks.OASISCStartOfCare:
                            case DisciplineTasks.SNAssessment:
                            case DisciplineTasks.NonOASISStartOfCare:
                            case DisciplineTasks.OASISCResumptionOfCare:
                            case DisciplineTasks.OASISCFollowUp:
                            case DisciplineTasks.OASISCRecertification:
                            case DisciplineTasks.SNAssessmentRecert:
                            case DisciplineTasks.NonOASISRecertification:
                            case DisciplineTasks.OASISCTransfer:
                            case DisciplineTasks.OASISCTransferDischarge:
                            case DisciplineTasks.OASISCDeath:
                            case DisciplineTasks.OASISCDischarge:
                            case DisciplineTasks.NonOASISDischarge:
                                assessment.Discipline = "Nursing";
                                break;
                            case DisciplineTasks.OASISCStartOfCarePT:
                            case DisciplineTasks.OASISCResumptionOfCarePT:
                            case DisciplineTasks.OASISCFollowupPT:
                            case DisciplineTasks.OASISCRecertificationPT:
                            case DisciplineTasks.OASISCTransferPT:
                            case DisciplineTasks.OASISCDeathPT:
                            case DisciplineTasks.OASISCDischargePT:
                            case DisciplineTasks.OASISCTransferDischargePT:
                                assessment.Discipline = "PT";
                                break;
                            case DisciplineTasks.OASISCStartOfCareOT:
                            case DisciplineTasks.OASISCResumptionOfCareOT:
                            case DisciplineTasks.OASISCFollowupOT:
                            case DisciplineTasks.OASISCRecertificationOT:
                            case DisciplineTasks.OASISCTransferOT:
                            case DisciplineTasks.OASISCDeathOT:
                            case DisciplineTasks.OASISCDischargeOT:
                                assessment.Discipline = "OT";
                                break;
                        }
                    }
                    else
                    {
                        assessment.Discipline = scheduleEvent.Discipline;
                    }
                }
            }
            return assessment;
        }

        public bool UsePreviousAssessment(Guid patientId, Guid assessmentId, Guid previousAssessmentId)
        {
            bool result = false;
            var currentAssessment = GetQuestions(assessmentId);//this.FindAny<Assessment>(agencyId, patientId, assessmentId);
            if (currentAssessment != null)
            {
                var previousAssessment = GetQuestions(previousAssessmentId);//this.FindAny<Assessment>(agencyId, patientId, previousAssessmentId);
                if (previousAssessment != null && previousAssessment.Question.IsNotNullOrEmpty())
                {
                    var tempAssessmentData = new List<Question>();
                    string type = AssessmentTypeFactory.AssessmentTypeNumber(currentAssessment.Type);
                    if (!type.Equals("10") && !type.Equals("00"))
                    {
                        var fields = baseAssessmentRepository.FindAssessmentFields("RFA" + type);
                        if (fields != null && fields.Count > 0)
                        {
                            tempAssessmentData.AddRange(fields.Select(field => previousAssessment.Question.FirstOrDefault(f => f.Name == field)).Where(answer => answer != null));
                            if (type == "01" || type == "03" || type == "04")
                            {
                                previousAssessment.Question.Where(f => f.Type == QuestionType.Generic || f.Type == QuestionType.PlanofCare)
                                    .ForEach(tempAssessmentData.Add);
                            }
                            else if (type == "05")
                            {
                                var listOfGenericPlanOfCareFieldsType05 = baseAssessmentRepository.AssessmentType05Generics();
                                previousAssessment.Question.Where(f => listOfGenericPlanOfCareFieldsType05.Contains(f.Name)).ForEach(tempAssessmentData.Add);
                            }
                            currentAssessment.Question.Where(field => !tempAssessmentData.Exists(t => t.Name == field.Name)).ForEach(tempAssessmentData.Add);
                        }
                    }
                    else if (type == "10")
                    {
                        List<string> fields = new List<string>();
                        fields.Add("PrimaryDiagnosis");
                        fields.Add("PrimaryDiagnosisDate");
                        fields.Add("ICD9M");
                        fields.Add("SymptomControlRating");
                        int character = 65;
                        for (int diagnosisCount = 1; diagnosisCount < 26; diagnosisCount++)
                        {
                            fields.Add("PrimaryDiagnosis" + diagnosisCount);
                            fields.Add("PrimaryDiagnosis" + diagnosisCount + "Date");
                            fields.Add("PaymentDiagnoses" + (char)character + "3");
                            fields.Add("PaymentDiagnoses" + (char)character + "4");
                            character++;
                        }
                        character = 65;
                        for (int diagCount2 = 1; diagCount2 < 26; diagCount2++)
                        {
                            fields.Add("ICD9M" + (char)character + "3");
                            fields.Add("ICD9M" + (char)character + "4");
                            fields.Add("ICD9M" + diagCount2);
                            fields.Add("ExacerbationOrOnsetPrimaryDiagnosis" + diagCount2);
                            fields.Add("OtherDiagnose" + diagCount2 + "Rating");
                            character++;
                        }
                        fields.Add("PaymentDiagnosesZ3");
                        fields.Add("PaymentDiagnosesZ4");
                        previousAssessment.Question.ForEach(f =>
                        {
                            if (f.Type == QuestionType.Generic || f.Type == QuestionType.PlanofCare)
                            {
                                tempAssessmentData.Add(f);
                            }
                            else if (f.Type == QuestionType.Moo && fields.Contains(f.Name))
                            {
                                tempAssessmentData.Add(f);
                            }
                        });
                        currentAssessment.Question.Where(field => !tempAssessmentData.Exists(t => t.Name == field.Name)).ForEach(field => tempAssessmentData.Add(field));
                    }
                    else
                    {
                        return false;
                    }
                    currentAssessment.Question = tempAssessmentData;
                    return mongoRepository.UpdateAssessmentQuestion(currentAssessment);
                }
            }
            return result;
        }

        public OasisViewData UpdateAssessmentStatus(T scheduleEvent, int status, string reason)
        {
            var oasisViewData = new OasisViewData();
            if (scheduleEvent != null)
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                        if (assessment != null)
                        {
                            var oldScheduleEventStatus = scheduleEvent.Status;
                            if (status == (int)ScheduleStatus.OasisReopened && assessment.Status == (int)ScheduleStatus.OasisExported)
                            {
                                assessment.VersionNumber += 1;
                            }
                            if (status == (int)ScheduleStatus.OasisReopened || status == (int)ScheduleStatus.OasisReturnedForClinicianReview)
                            {
                                assessment.SignatureText = string.Empty;
                                assessment.SignatureDate = DateTime.MinValue;
                            }
                            assessment.Status = status;
                            if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview && Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA))
                            {
                                assessment.Status = (int)ScheduleStatus.OasisCompletedExportReady;
                            }
                            if (scheduleEvent.EventDate.IsValid())
                            {
                                assessment.AssessmentDate = scheduleEvent.EventDate;
                            }
                            if (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady)
                            {
                                scheduleEvent.InPrintQueue = true;
                            }
                            scheduleEvent.Status = assessment.Status;
                            if (baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id, scheduleEvent.Status, scheduleEvent.InPrintQueue))
                            {
                                bool returnCommentStatus = true;
                                if (reason.IsNotNullOrEmpty())
                                {
                                    returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, assessment.EpisodeId, Current.UserId, assessment.Id, reason));
                                }
                                if (returnCommentStatus)
                                {
                                    if (baseAssessmentRepository.Update(assessment))
                                    {
                                        oasisViewData.isSuccessful = true;
                                        oasisViewData = GeneratePOC(status, oasisViewData, assessment, scheduleEvent);
                                        if (oasisViewData.isSuccessful)
                                        {
                                            oasisViewData.Service = Service.ToString();
                                            oasisViewData.ServiceId = (int)Service;
                                            oasisViewData.EpisodeId = scheduleEvent.EpisodeId;
                                            oasisViewData.PatientId = scheduleEvent.PatientId;
                                            oasisViewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                                            oasisViewData.IsDataCentersRefresh = oldScheduleEventStatus != scheduleEvent.Status;
                                            oasisViewData.IsCaseManagementRefresh = oasisViewData.IsDataCentersRefresh && (scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview) || oldScheduleEventStatus == ((int)ScheduleStatus.OasisCompletedPendingReview));
                                            oasisViewData.IsMyScheduleTaskRefresh = oasisViewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                                            oasisViewData.IsExportOASISRefresh = oasisViewData.IsDataCentersRefresh && (scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady) || oldScheduleEventStatus == ((int)ScheduleStatus.OasisCompletedExportReady));
                                            oasisViewData.IsExportedOASISRefresh = oasisViewData.IsDataCentersRefresh && (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported) || oldScheduleEventStatus == ((int)ScheduleStatus.OasisExported));
                                            oasisViewData.IsNotExportedOASISRefresh = oasisViewData.IsDataCentersRefresh && (scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedNotExported) || oldScheduleEventStatus == ((int)ScheduleStatus.OasisCompletedNotExported));
                                            ts.Complete();
                                        }
                                        else
                                        {
                                            oasisViewData.isSuccessful = false;
                                            ts.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        ts.Dispose();
                                    }
                                }
                                else
                                {
                                    ts.Dispose();
                                }
                            }
                        }
                    }
                }
                if (oasisViewData.isSuccessful && Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                }
            }
            return oasisViewData;
        }

        public JsonViewData Reopen(T scheduleEvent)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleEvent != null)
            {
                var oldStatus = scheduleEvent.Status;
                scheduleEvent.Status = ((int)ScheduleStatus.OasisReopened);
                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduleEvent.DisciplineTask)).ToString();
                var data = UpdateAssessmentStatus(scheduleEvent, (int)ScheduleStatus.OasisReopened, string.Empty);
                viewData.isSuccessful = data.isSuccessful;

                if (viewData.isSuccessful)
                {
                    viewData.PatientId = data.PatientId;
                    viewData.EpisodeId = data.EpisodeId;
                    viewData.UserIds = data.UserIds;
                    viewData.Service = this.Service.ToString();
                    viewData.ServiceId = (int)this.Service;
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = Current.UserId == scheduleEvent.UserId && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                    if (!(assessmentType.IsEqual("NonOASIS")))
                    {
                        viewData.IsExportOASISRefresh = oldStatus == ((int)ScheduleStatus.OasisCompletedExportReady);
                        viewData.IsExportedOASISRefresh = oldStatus == ((int)ScheduleStatus.OasisExported);
                    }
                }
            }

            return viewData;
        }

        public JsonViewData ToggleAssessment(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Assessment could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (baseAssessmentRepository.MarkAsDeleted(Current.AgencyId, task.Id, task.PatientId, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.IsDataCentersRefresh = true;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.UserIds = new List<Guid> { task.UserId };
                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, task);
                        viewData.errorMessage = string.Format("The Assessment has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        public bool Remove(Guid Id)
        {
            return baseAssessmentRepository.RemoveModel<Assessment>(Id);
        }
        /// <summary>
        /// POC Stands for plan of care
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="episodeId"></param>
        /// <param name="status"></param>
        /// <param name="oasisViewData"></param>
        /// <param name="assessment"></param>
        /// <param name="episode"></param>
        /// <param name="scheduleEvent"></param>
        protected abstract OasisViewData GeneratePOC(int status, OasisViewData oasisViewData, Assessment assessment, T scheduleEvent);

        public OasisViewData UpdateAssessmentStatusForSubmit(T scheduleEvent, int status, string signature, DateTime date, string timeIn, string timeOut)
        {
            var oasisViewData = new OasisViewData();
            oasisViewData.isSuccessful = false;
            var isCaseManagementRefresh = false;
            if (scheduleEvent != null)
            {
                var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                if (assessment != null)
                {
                    var oldSignatureText = assessment.SignatureText;
                    var oldSignatureDate = assessment.SignatureDate;
                    var oldAssessmentStatus = assessment.Status;
                    var oldAssessmentDate = assessment.AssessmentDate;

                    var oldScheduleEventStatus = scheduleEvent.Status;
                    var oldScheduleEventTimeIn = scheduleEvent.TimeIn;
                    var oldScheduleEventTimeOut = scheduleEvent.TimeOut;

                    assessment.Status = status;
                    isCaseManagementRefresh = true;

                    if (Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA))
                    {
                        isCaseManagementRefresh = false;
                        oasisViewData.IsExportOASISRefresh = true;
                        assessment.Status = (int)ScheduleStatus.OasisCompletedExportReady;
                    }

                    assessment.SignatureDate = date;
                    assessment.SignatureText = signature;
                    assessment.TimeIn = timeIn;
                    assessment.TimeOut = timeOut;

                    if (scheduleEvent.EventDate.IsValid())
                    {
                        assessment.AssessmentDate = scheduleEvent.EventDate;
                    }
                    var oldStatus = scheduleEvent.Status;

                    scheduleEvent.Status = assessment.Status;
                    scheduleEvent.TimeIn = timeIn;
                    scheduleEvent.TimeOut = timeOut;
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        if (baseAssessmentRepository.UpdateModal(assessment))
                        {
                            oasisViewData.isSuccessful = true;
                            //The assessment should not have its data yet.
                            oasisViewData = GeneratePOCAndHospitalizationLog(status, oasisViewData, assessment, scheduleEvent);
                            if (oasisViewData.isSuccessful)
                            {
                                oasisViewData.EpisodeId = scheduleEvent.EpisodeId;
                                oasisViewData.PatientId = scheduleEvent.PatientId;
                                oasisViewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                                oasisViewData.IsDataCentersRefresh = oldStatus != scheduleEvent.Status;
                                oasisViewData.IsCaseManagementRefresh = oasisViewData.IsDataCentersRefresh && isCaseManagementRefresh;
                                oasisViewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(oasisViewData.IsDataCentersRefresh, scheduleEvent);
                                oasisViewData.IsExportOASISRefresh = oasisViewData.IsDataCentersRefresh && !isCaseManagementRefresh;

                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                assessment.SignatureText = oldSignatureText;
                                assessment.SignatureDate = oldSignatureDate;
                                assessment.Status = oldAssessmentStatus;
                                assessment.AssessmentDate = oldAssessmentDate;

                                baseAssessmentRepository.Update(assessment);

                                scheduleEvent.Status = oldScheduleEventStatus;
                                scheduleEvent.TimeIn = oldScheduleEventTimeIn;
                                scheduleEvent.TimeOut = oldScheduleEventTimeOut;

                                baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                                oasisViewData.isSuccessful = false;
                            }
                        }
                        else
                        {
                            oasisViewData.IsCaseManagementRefresh = false;

                            scheduleEvent.Status = oldScheduleEventStatus;
                            scheduleEvent.TimeIn = oldScheduleEventTimeIn;
                            scheduleEvent.TimeOut = oldScheduleEventTimeOut;

                            baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                            oasisViewData.isSuccessful = false;
                        }
                    }
                }
            }
            return oasisViewData;
        }

        protected abstract OasisViewData GeneratePOCAndHospitalizationLog(int status, OasisViewData oasisViewData, Assessment assessment, T scheduleEvent);

        public bool UpdateAssessmentForDetail(T schedule)
        {
            bool result = false;
            if (schedule != null)
            {
                UpdateAssessmentForDetailAppSpecific(schedule);
                var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, schedule.Id);
                if (assessment != null)
                {
                    assessment.TimeIn = schedule.TimeIn;
                    assessment.TimeOut = schedule.TimeOut;
                    if (schedule.EventDate.IsValid())
                    {
                        assessment.AssessmentDate = schedule.EventDate;
                    }

                    assessment.IsDeprecated = schedule.IsDeprecated;
                    assessment.EpisodeId = schedule.EpisodeId;
                    assessment.UserId = schedule.UserId;
                    assessment.Status = schedule.Status;
                    result = baseAssessmentRepository.UpdateModal(assessment);
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        protected abstract void UpdateAssessmentForDetailAppSpecific(T schedule);

        public bool DeleteWoundCareAsset(Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var assessmentData = GetQuestions(eventId);
                if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                {
                    var question = assessmentData.Question.FirstOrDefault(q => string.Format("{0}{1}", q.Type, q.Name) == name);
                    if (question != null)
                    {
                        question.Answer = Guid.Empty.ToString();
                        if (mongoRepository.UpdateAssessmentQuestion(assessmentData))
                        {
                            if (AssetHelper.Delete(assetId))
                            {
                                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                                if (scheduleEvent != null && scheduleEvent.Asset.IsNotNullOrEmpty())
                                {
                                    scheduleEvent.Assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                                    if (scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0 && scheduleEvent.Assets.Contains(assetId))
                                    {
                                        scheduleEvent.Assets.Remove(assetId);
                                        scheduleEvent.Asset = scheduleEvent.Assets.ToXml();
                                        result = baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                                    }
                                    else
                                    {
                                        result = true;
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool DeleteOnlyWoundCareAsset(Guid patientId, Guid eventId, Guid assetId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(assetId, "assetId");
            var result = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var assessmentData = GetQuestions(eventId);
                if (assessmentData != null && assessmentData.Question.IsNotNullOrEmpty())
                {
                    if (assessmentData.Question.Exists(q => q.Answer == assetId.ToString()))
                    {
                        assessmentData.Question.SingleOrDefault(q => q.Answer == assetId.ToString()).Answer = Guid.Empty.ToString();
                        if (mongoRepository.UpdateAssessmentQuestion(assessmentData))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, eventId);
            if (assessment != null)
            {
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    return false;
                }
                var supplies = new List<Supply>();
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    supplies = assessment.Supply.ToObject<List<Supply>>();
                    var existingSupply = supplies.Find(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                    if (existingSupply != null)
                    {
                        return result;
                    }
                    else
                    {
                        if (supplies != null && supplies.Count > 0)
                        {
                            supplies.Add(supply);
                        }
                        else
                        {
                            supplies = new List<Supply> { supply };
                        }
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                assessment.Supply = supplies.ToXml();
                if (baseAssessmentRepository.UpdateModal(assessment))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, eventId);
            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.OldUniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.OldUniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            editSupply.UnitCost = supply.UnitCost;
                            editSupply.Description = supply.Description;
                            editSupply.DateForEdit = supply.DateForEdit;
                            editSupply.Code = supply.Code;
                            assessment.Supply = supplies.ToXml();
                            if (baseAssessmentRepository.UpdateModal(assessment))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteSupply(Guid patientId, Guid eventId, Guid supplyId)
        {
            var result = false;
            var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, eventId);
            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supplyId))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supplyId)
                            {
                                supplies.Remove(s);
                            }
                        });
                        assessment.Supply = supplies.ToXml();
                        if (baseAssessmentRepository.UpdateModal(assessment))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public List<Supply> GetAssessmentSupply(Guid patientId, Guid eventId)
        {
            var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, eventId);
            var list = new List<Supply>();
            if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
            {
                list = assessment.Supply.ToObject<List<Supply>>();
            }
            return list;
        }

        public Supply GetSupply(Guid patientId, Guid eventId, Guid supplyId)
        {
            var supply = new Supply();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, eventId);
                if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
                {
                    var list = assessment.Supply.ToObject<List<Supply>>() ?? new List<Supply>();
                    if (list.IsNotNullOrEmpty())
                    {
                        supply = list.FirstOrDefault(s => s.UniqueIdentifier == supplyId);
                        if (supply != null)
                        {
                            supply.OldUniqueIdentifier = supply.UniqueIdentifier;
                        }
                    }
                }
            }
            return supply;
        }


        public AssessmentPrint GetAssessmentPrint(AssessmentType Type)
        {
            var assessment = new AssessmentPrint();
            assessment.Type = Type;
            assessment.Location = agencyRepository.GetMainLocation(Current.AgencyId);
            return assessment;
        }

        public AssessmentPrint GetAssessmentPrint(Guid patientId, Guid eventId)
        {
            var assessmentPrint = new AssessmentPrint();
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (scheduleEvent != null)
            {
                assessmentPrint.TaskName = scheduleEvent.DisciplineTaskName;
                var assessment = baseAssessmentRepository.GetAssessmentOnly(Current.AgencyId, patientId, eventId);
                if (assessment != null)
                {
                    SetQuestions(assessment);
                    assessmentPrint.AssessmentDate = assessment.AssessmentDate;
                    assessmentPrint.SignatureText = assessment.SignatureText;
                    assessmentPrint.SignatureDate = assessment.SignatureDate;
                    assessmentPrint.Discipline = scheduleEvent.Discipline;
                    assessmentPrint.Type = Enum.IsDefined(typeof(AssessmentType), assessment.Type) ? (AssessmentType)Enum.Parse(typeof(AssessmentType), assessment.Type) : AssessmentType.None;
                    var profile = profileRepository.GetProfileOnly(Current.AgencyId, assessment.PatientId);
                    assessmentPrint.Location = agencyRepository.FindLocationOrMain(Current.AgencyId, profile != null ? profile.AgencyLocationId : Guid.Empty);
                    var questions = assessment.Questions.ToOASISDictionary();
                    //var questions = assessment.ToDictionary();
                    assessmentPrint.TimeIn = scheduleEvent.TimeIn;
                    assessmentPrint.TimeOut = scheduleEvent.TimeOut;
                    assessmentPrint.Version = scheduleEvent.Version;
                    if (scheduleEvent.VisitDate.IsValid())
                    {
                        assessmentPrint.VisitDate = scheduleEvent.VisitDate;
                    }
                    assessmentPrint.Data = questions;
                }
            }
            return assessmentPrint;
        }

        private IDictionary<string, NotesQuestion> CombineNoteQuestionsAndOasisQuestions(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            var questions = noteQuestions;
            if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                {
                    noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis"))
                {
                    noteQuestions.Add("PrimaryDiagnosis", oasisQuestions["PrimaryDiagnosis"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                {
                    noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M"))
                {
                    noteQuestions.Add("ICD9M", oasisQuestions["ICD9M"]);
                }
            }

            if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                {
                    noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                {
                    noteQuestions.Add("PrimaryDiagnosis1", oasisQuestions["PrimaryDiagnosis1"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                {
                    noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M1"))
                {
                    noteQuestions.Add("ICD9M1", oasisQuestions["ICD9M1"]);
                }
            }
            return questions;
        }

        public GridViewData ExporteAndNonExportedGridViewData(ParentPermission category, PermissionActions availablePermissionAction, List<PermissionActions> moreActions, bool IsLocationNeeded)
        {
            var viewData = new GridViewData { Service = this.Service };
            if (IsLocationNeeded)
            {
                viewData.Id = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
            }
            moreActions.Add(availablePermissionAction);
            var allPermission = Current.CategoryService(Current.AcessibleServices, category, moreActions.Select(a => (int)a).ToArray());
            if (allPermission.IsNotNullOrEmpty())
            {
                moreActions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == availablePermissionAction)
                    {
                        viewData.AvailableService = permission;
                    }

                    if (a == PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }

                    if (a == PermissionActions.ViewExported)
                    {
                        viewData.ViewListPermissions = permission;
                    }
                });
            }
            return viewData;
        }

        public List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, int patientStatus, DateTime startDate, DateTime endDate, bool permissionCheckNeeded)
        {
            var result = new List<AssessmentExport>();
            var assessments = baseAssessmentRepository.GetOnlyCMSOasisByStatusLean(Current.AgencyId, branchId, ((int)status), patientStatus, startDate, endDate);
            if (assessments.IsNotNullOrEmpty())
            {
                var allPermission = new Dictionary<int, AgencyServices>();
                var isGeneratePermission = false;
                var isReopenPermission = false;
                if (permissionCheckNeeded)
                {
                    allPermission = Current.CategoryService(this.Service, ParentPermission.OASIS, new int[] { (int)PermissionActions.GenerateOASISExport, (int)PermissionActions.Reopen });
                    if (allPermission.IsNotNullOrEmpty())
                    {
                        isGeneratePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.GenerateOASISExport, AgencyServices.None) != AgencyServices.None;
                        isReopenPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Reopen, AgencyServices.None) != AgencyServices.None;
                    }
                }
                result = GetAssessmentByStatusHelper(assessments, (AssessmentExport assessment) =>
                {
                    assessment.IsUserCanGenerate = isGeneratePermission;
                    assessment.IsUserCanReopen = isReopenPermission;
                });
            }
            return result;

        }

        public List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, List<int> paymentSources)
        {
            var result = new List<AssessmentExport>();
            var assessments = baseAssessmentRepository.GetOnlyCMSOasisByStatusLean(Current.AgencyId, branchId, (int)status, paymentSources);
            if (assessments.IsNotNullOrEmpty())
            {
                result = GetAssessmentByStatusHelper(assessments, null);
            }
            return result;
        }

        private List<AssessmentExport> GetAssessmentByStatusHelper(List<AssessmentExport> assessments, Action<AssessmentExport> expression)
        {
            var result = new List<AssessmentExport>();
            int count = 1;
            var insuranceIds = assessments.Where(s => s.InsuranceId > 0).Select(i => i.InsuranceId).Distinct().ToList();
            var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
            assessments.ForEach(a =>
            {
                a.Service = this.Service;
                var insurance = insurances.FirstOrDefault(i => i.Id == a.InsuranceId);
                if (insurance != null)
                {
                    a.Insurance = insurance.Name;
                }
                if (AssessmentTypeFactory.NextEpisodeAssessments().Contains(AssessmentTypeFactory.ParseString(a.AssessmentType)) && (a.EventDate.Date >= a.EpisodeEndDate.AddDays(-5).Date && a.EventDate.Date <= a.EpisodeEndDate.Date))
                {
                    a.EpisodeStartDate = a.EpisodeEndDate.AddDays(1);
                    a.EpisodeEndDate = a.EpisodeEndDate.AddDays(60);
                }
                var type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(a.AssessmentType);
                a.AssessmentType = type.ToString();
                a.TaskName = AssessmentTypeFactory.ToDisciplineTask(type).ToString();
                a.Index = count;
                if (expression != null)
                {
                    expression(a);
                }

                result.Add(a);
                count++;
            });
            return result;
        }

        public AssessmentExportViewData GetAssessmentByStatusViewData(Guid locationId, bool isLocationNeededIdEmpty, ScheduleStatus status, List<int> paymentSources)
        {
            if (isLocationNeededIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            var viewData = new AssessmentExportViewData { Service = this.Service, LocationId = locationId };
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.OASIS, new int[] { (int)PermissionActions.ViewToExport, (int)PermissionActions.Export, (int)PermissionActions.GenerateOASISExport });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewToExport, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
                viewData.GeneratePermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.GenerateOASISExport, AgencyServices.None);
            }

            viewData.Assessments = GetAssessmentByStatus(locationId, status, paymentSources);

            return viewData;
        }

        protected abstract bool Validate(Assessment assessment, out Assessment assessmentOut);

        public AssessmentQuestionData GetQuestions(Guid assessmentId)
        {
            return mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessmentId);
        }

        public void SetQuestions(Assessment assessment)
        {
            var questionData = mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id);
            if (questionData != null && questionData.Question.IsNotNullOrEmpty())
            {
                assessment.Questions = questionData.Question;
            }
        }

        public JsonViewData AddScheduleTaskAndAssessmentHelper(E patientEpisode, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected assessment task(s)." };
            if (patientEpisode != null)
            {
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    viewData = ValidateAssessmentTask(patientEpisode.Id, patientEpisode.PatientId, scheduleEvents, patientEpisode.EndDate, viewData);
                    if (viewData.isSuccessful)
                    {
                        var patient = GetPatientWithProfileFromAdmissionAppSpecific(patientEpisode.PatientId, patientEpisode.AdmissionId);
                        if (patient != null)
                        {
                            patient.Profile = patient.Profile ?? new Profile();
                            patient.Profile.EpisodeStartDate = patientEpisode.StartDate;
                            patient.Profile.StartofCareDate = patient.Profile.StartofCareDate.IsValid() ? patient.Profile.StartofCareDate : patientEpisode.StartDate;
                            var assessments = GetAssessmentsFromTasks(patient, scheduleEvents);
                            if (assessments != null)
                            {
                                if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                                {
                                    if (baseAssessmentRepository.AddMultiple(assessments))
                                    {
                                        var questionDatas = assessments.Select(a => new AssessmentQuestionData(a)).ToList();
                                        if (mongoRepository.AddManyAssessmentQuestions(questionDatas))
                                        {
                                            viewData.isSuccessful = true;
                                            viewData.PatientId = patient.Id;
                                            viewData.EpisodeId = patientEpisode.Id;
                                            viewData.UserIds = new List<Guid>();
                                            viewData.UserIds.AddRange(scheduleEvents.Select(s => s.UserId));
                                            viewData.Service = Service.ToString();
                                            viewData.ServiceId = (int)Service;
                                            viewData.IsDataCentersRefresh = true;
                                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduleEvents);
                                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                                        }
                                        else
                                        {
                                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patient.Id, scheduleEvents.Select(s => s.Id).ToList());
                                            baseAssessmentRepository.RemoveMultiple(assessments);
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = false;
                                        baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patient.Id, scheduleEvents.Select(s => s.Id).ToList());
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return viewData;
        }

        protected List<Assessment> GetAssessmentsFromTasks(Patient patient, List<T> scheduleEvents)
        {
            var assessments = new List<Assessment>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var assessmentNeedsMedicaton = DisciplineTaskFactory.AllAssessments(true).Union(DisciplineTaskFactory.SNAssessments());
                var patientSpecificQuestions = PatientSpecificDemographics(patient, patient.Profile).Values.ToList();
                var medicationProfile = string.Empty;
                if (scheduleEvents.Exists(s => assessmentNeedsMedicaton.Contains(s.DisciplineTask)))
                {
                    var medicationHistory = patientRepository.GetMedicationProfileByPatient(patient.Id, Current.AgencyId);
                    var medicationList = new List<Medication>();
                    if (medicationHistory != null)
                    {
                        medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList();
                    }
                    medicationProfile = medicationList.ToXml();
                }

                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var assessment = EntityHelper.CreateAssessment<T>(scheduleEvent, medicationProfile);

                    assessment.Questions = CreateOASISPatientDemographics(patient, patient.Profile, scheduleEvent.DisciplineTask);
                    assessments.Add(assessment);
                });
            }
            return assessments;
        }

        public List<Question> CreateOASISPatientDemographics(List<Question> patientSpecificQuestions, DateTime startofCareDate, DateTime episodeStartDate, int disciplineTask)
        {
            var assessmentSpecifcDemographics = ScheduleAndEpisodeSpecificDemographics(disciplineTask, episodeStartDate, startofCareDate).Values.ToList();
            return assessmentSpecifcDemographics.Union(patientSpecificQuestions).ToList();
        }

        public List<Question> CreateOASISPatientDemographics(Patient patient, Profile profile, int disciplineTask)
        {
            var patientSpecificQuestions = PatientSpecificDemographics(patient, profile).Values.ToList();
            var assessmentSpecifcDemographics = ScheduleAndEpisodeSpecificDemographics(disciplineTask, profile.EpisodeStartDate, profile.StartofCareDate).Values.ToList();
            return assessmentSpecifcDemographics.Union(patientSpecificQuestions).ToList();
        }

        public JsonViewData ValidateAssessmentTask(Guid episodeId, Guid patientId, List<T> newEvents, DateTime endDate, JsonViewData viewData)
        {
            var oldEvents = baseScheduleRepository.GetPatientScheduledEventsOnlyNew(Current.AgencyId, episodeId, patientId);
            return ValidateAssessmentTask(oldEvents, newEvents, endDate, viewData);
        }

        public JsonViewData ValidateAssessmentTask(List<T> oldEvents, List<T> newEvents, DateTime endDate, JsonViewData viewData)
        {
            try
            {
                if ((oldEvents != null && oldEvents.Count > 0))
                {
                    var recertTasks = DisciplineTaskFactory.RecertDisciplineTasks(false);
                    var transferTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks();
                    var rocTasks = DisciplineTaskFactory.ROCDisciplineTasks();
                    var socTasks = DisciplineTaskFactory.SOCDisciplineTasks(false);
                    var dischargeTasks = DisciplineTaskFactory.DischargeOASISDisciplineTasks(false);

                    foreach (var evnt in newEvents)
                    {
                        if (recertTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date < evnt.EventDate.Date));
                            T roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return (viewData);
                            }
                            else if (transfer != null && roc != null && roc.EventDate.Date <= transfer.EventDate.Date)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return (viewData);
                            }
                            else if (oldEvents.Exists(oe => recertTasks.Contains(oe.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return (viewData);
                            }
                            else if (evnt.EventDate.Date < endDate.AddDays(-5).Date || evnt.EventDate.Date > endDate.Date)
                            {
                                viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                                return (viewData);
                            }
                        }
                        else if (socTasks.Contains(evnt.DisciplineTask))
                        {
                            if (oldEvents.Exists(oe => socTasks.Contains(oe.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return (viewData);
                            }
                            else if (oldEvents.Exists(oe => dischargeTasks.Contains(oe.DisciplineTask)))
                            {
                            }
                        }
                        else if (rocTasks.Contains(evnt.DisciplineTask))
                        {
                            var roc = oldEvents.FirstOrDefault(oe => rocTasks.Contains(oe.DisciplineTask));
                            var transfers = oldEvents.Where(oe => transferTasks.Contains(oe.DisciplineTask)).ToList();
                            if (roc == null)
                            {
                                if (transfers.IsNullOrEmpty())
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return (viewData);
                                }
                                else if (transfers.IsNotNullOrEmpty())
                                {
                                    if (transfers.Exists(t => t.EventDate.Date > evnt.EventDate.Date))
                                    {
                                        viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                        return (viewData);
                                    }
                                }
                            }
                            else if (roc != null && transfers != null)
                            {
                                var transfersAfterEvent = transfers.Where(t => t.EventDate.Date > evnt.EventDate.Date).ToList();
                                var transfersBeforeEvent = transfers.Where(t => t.EventDate.Date < evnt.EventDate.Date);
                                if ((transfersAfterEvent.IsNotNullOrEmpty() && transfersAfterEvent.Exists(t => roc.EventDate.Date > t.EventDate.Date)) && !transfersBeforeEvent.Any())
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return (viewData);
                                }
                            }
                        }
                        else if (transferTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask));
                            T roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                                return (viewData);
                            }
                        }
                    }
                    viewData.isSuccessful = true;
                }
                else
                {
                    viewData.isSuccessful = true;
                }
            }
            catch (Exception)
            {
                return viewData;
            }
            return viewData;
        }

        public abstract Patient GetPatientWithProfileFromAdmissionAppSpecific(Guid patientId, Guid admissionId);

        public JsonViewData LoadPreviousAssessment(Guid episodeId, Guid patientId, Guid assessmentId, Guid previousAssessmentId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Previous Assessment data could not be saved." };
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, episodeId, assessmentId);
            if (scheduleEvent != null)
            {
                if (UsePreviousAssessment(patientId, assessmentId, previousAssessmentId))
                {
                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisSaved);
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Assessment was successfully loaded from the previous assessment.";
                        viewData.IsDataCentersRefresh = oldStatus != scheduleEvent.Status;
                        viewData.PatientId = scheduleEvent.PatientId;
                        viewData.EpisodeId = scheduleEvent.EpisodeId;
                        viewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                    }
                }
            }
            return viewData;
        }

        /// <summary>
        /// Will check for an existing plan of care related to the passed in assessment. If one cannot be found it will create it.
        /// </summary>
        /// <param name="assessment">An assessment</param>
        /// <param name="scheduleEvent">The task that represents the assessment</param>
        /// <returns></returns>
        public bool CreatePlanofCare(Assessment assessment, T scheduleEvent)
        {
            var result = false;
            var planofCare = planofCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, assessment.EpisodeId, assessment.Id);
            if (planofCare == null)
            {
                var isNonOasis = assessment.Type == AssessmentType.NonOASISStartOfCare.ToString() || assessment.Type == AssessmentType.NonOASISRecertification.ToString();
                var pocScheduleEvent = CreatePlanOfCareTask(Guid.NewGuid(), scheduleEvent, assessment, isNonOasis);
                if (CreatePlanofCare(pocScheduleEvent, assessment, isNonOasis))
                {
                    result = true;
                }
            }
            else
            {
                var planofCareScheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);// scheduleEvents.Where(s => s.EpisodeId == planofCare.EpisodeId && s.PatientId == planofCare.PatientId && s.EventId == planofCare.Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                if (planofCareScheduleEvent == null)
                {
                    var isNonOasis = assessment.Type.IsEqual(AssessmentType.NonOASISStartOfCare.ToString()) || assessment.Type.IsEqual(AssessmentType.NonOASISRecertification.ToString());
                    var pocScheduleEvent = CreatePlanOfCareTask(planofCare.Id, scheduleEvent, assessment, isNonOasis);
                    if (baseScheduleRepository.AddScheduleTask(pocScheduleEvent))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.Id, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Actually creates the plan of care object and saves it to the database
        /// </summary>
        /// <param name="scheduleEvent">The task created to represent the Plan of Care</param>
        /// <param name="assessment">The assessment that will have data loaded from it</param>
        /// <param name="isNonOasis"></param>
        /// <returns></returns>
        private bool CreatePlanofCare(T scheduleEvent, Assessment assessment, bool isNonOasis)
        {
            var result = false;
            var planofCare = new PlanofCare();
            planofCare.Id = scheduleEvent.Id;
            planofCare.AssessmentId = assessment.Id;
            planofCare.AgencyId = Current.AgencyId;
            planofCare.PatientId = scheduleEvent.PatientId;
            planofCare.EpisodeId = scheduleEvent.EpisodeId;
            planofCare.Status = scheduleEvent.Status;
            planofCare.AssessmentType = assessment.Type.ToString();
            planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
            planofCare.IsNonOasis = isNonOasis;
            var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, scheduleEvent.PatientId);
            if (physician != null)
            {
                planofCare.PhysicianId = physician.Id;
            }
            planofCare.UserId = scheduleEvent.UserId;
            SetQuestions(assessment);
            planofCare.Questions = this.Get485FromAssessment(assessment);
            if (baseScheduleRepository.AddScheduleTask(scheduleEvent))
            {
                if (planofCareRepository.Add(planofCare))
                {
                    if (mongoRepository.AddPlanofcareQuestion(new AssessmentQuestionData(planofCare)))
                    {
                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
                        }
                        result = true;
                    }
                    else
                    {
                        baseScheduleRepository.RemoveScheduleTaskFully(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                        planofCareRepository.RemoveFully(planofCare.Id);
                    }
                }
                else
                {
                    baseScheduleRepository.RemoveScheduleTaskFully(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                }
            }
            return result;
        }

        public ValidationInfoViewData Validate(Guid assessmentId, Guid patientId, Guid episodeId)
        {
            var validationInfo = new ValidationInfoViewData();
            validationInfo.Service = this.Service;
            if (!assessmentId.IsEmpty())
            {
                var assessment = GetAssessment(patientId, assessmentId);
                if (assessment != null)
                {
                    var assessmentType = assessment.Type.ToString();
                    validationInfo.AssessmentId = assessmentId;
                    validationInfo.AssessmentType = assessmentType;
                    validationInfo.EpisodeId = episodeId;
                    validationInfo.PatientId = patientId;
                    var validationErrors = new List<Axxess.Api.Contracts.ValidationError>();
                    var profile = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
                    if (profile != null)
                    {
                        var episode = baseEpisodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                        if (episode != null)
                        {
                            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, episodeId, assessmentId);// schedules.FirstOrDefault(s => s.EpisodeId == episode.Id && s.EventId == assessment.Id);
                            if (scheduleEvent != null)
                            {
                                validationInfo.Version = scheduleEvent.Version;
                                validationInfo.Status = scheduleEvent.Status.ToString();
                                var patientLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                                if (patientLocation != null)
                                {
                                    if (!patientLocation.IsLocationStandAlone)
                                    {
                                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                                        if (agency != null)
                                        {
                                            patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                            patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                            patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                        }
                                    }
                                    var questionData = GetQuestions(assessmentId);
                                    if (questionData != null && questionData.Question.IsNotNullOrEmpty())
                                    {
                                        assessment.Questions = questionData.Question;
                                        var assessmentQuestions = assessment.Questions.ToOASISDictionary();
                                        if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                        {
                                            if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                                            {
                                                assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                                            }
                                            else
                                            {
                                                assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                                            }

                                            if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                                            {
                                                assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                                            }
                                            else
                                            {
                                                assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                                            }

                                            var oasisFormatString = this.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                            if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                                            {
                                                validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                                if (validationErrors.Count > 0)
                                                {
                                                    validationErrors.RemoveAt(0);
                                                }
                                                validationErrors.AddRange(CustomValidation(assessmentQuestions));
                                                if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString())
                                                {
                                                    var signedMedicationProfile = patientRepository.GetSignedMedicationAssocatiedToAssessment(assessment.PatientId, assessment.Id);
                                                    if (signedMedicationProfile == null)
                                                    {
                                                        validationErrors.Add(new Axxess.Api.Contracts.ValidationError() { ErrorDup = "M0001", ErrorType = "WARNING", Description = "WARNING: You have not signed the medication profile" });
                                                    }
                                                }
                                                var oldHippsCode = assessment.HippsCode;
                                                var oldHippsVersion = assessment.HippsVersion;
                                                var oldClaimKey = assessment.ClaimKey;
                                                var oldSubmissionFormat = assessment.SubmissionFormat;
                                                var oldIsValidated = assessment.IsValidated;
                                                int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                                if (error == 0)
                                                {
                                                    validationInfo.EpisodeStartDate = episode.StartDate;
                                                    validationInfo.EpisodeEndDate = episode.EndDate;
                                                    validationInfo.TimeIn = assessment.TimeIn;
                                                    validationInfo.TimeOut = assessment.TimeOut;
                                                    validationInfo.IsErrorFree = true;
                                                    if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                                                    {
                                                        var hipps = grouperAgent.GetHippsCode(oasisFormatString);
                                                        if (hipps.ClaimMatchingKey != string.Empty && hipps.ClaimMatchingKey.Length == 18 && hipps.Code != string.Empty && hipps.Code.Length == 5 && hipps.Version != string.Empty && hipps.Version.Length == 5)
                                                        {
                                                            var hippsCode = assessment.Questions.Find(q => q.Name == "HIPPSCODE");
                                                            if (hippsCode == null)
                                                            {
                                                                assessment.Questions.Add(new Question { Name = "HIPPSCODE", Answer = hipps.Code });
                                                            }
                                                            else
                                                            {
                                                                hippsCode.Answer = hipps.Code;
                                                            }
                                                            var hippsVersion = assessment.Questions.Find(q => q.Name == "HIPPSVERSION");
                                                            if (hippsVersion == null)
                                                            {
                                                                assessment.Questions.Add(new Question { Name = "HIPPSVERSION", Answer = hipps.Version });
                                                            }
                                                            else
                                                            {
                                                                hippsVersion.Answer = hipps.Version;
                                                            }
                                                        }
                                                        var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                                        var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                                        assessment.HippsCode = hipps.Code;
                                                        assessment.HippsVersion = hipps.Version;
                                                        assessment.ClaimKey = hipps.ClaimMatchingKey;
                                                        assessment.SubmissionFormat = oasisFormatStringComplete;
                                                        assessment.IsValidated = true;
                                                        if (baseAssessmentRepository.Update(assessment))
                                                        {
                                                            if (mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                                                            {
                                                                validationInfo.HIPPSCODE = hipps.Code;
                                                                validationInfo.HIPPSKEY = hipps.ClaimMatchingKey;
                                                                var episodeDateRange = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetPlanofCareCertPeriod(episode.Id, episode.PatientId, assessmentId);
                                                                if (episodeDateRange != null)
                                                                {
                                                                    episodeDateRange.StartDate = episodeDateRange.StartDate.IsValid() ? episodeDateRange.StartDate : episode.StartDate;
                                                                    if (episodeDateRange.StartDate.IsValid())
                                                                    {
                                                                        validationInfo.StandardPaymentRate = lookupRepository.GetProspectivePaymentAmount(hipps.Code, episodeDateRange.StartDate, profile.Address.ZipCode.IsNotNullOrEmpty() ? profile.Address.ZipCode : patientLocation.AddressZipCode);
                                                                    }
                                                                }
                                                                if (hipps.Code.IsNotNullOrEmpty())
                                                                {
                                                                    var hhrg = lookupRepository.GetHHRGByHIPPSCODE(hipps.Code.Trim());
                                                                    validationInfo.HHRG = hhrg != null ? hhrg.HHRG : string.Empty;
                                                                }
                                                                validationInfo.Count = validationErrors.Count;
                                                                validationInfo.ValidationErrors = validationErrors;
                                                                validationInfo.Message = "Your HIPPS Code generated successfully.";
                                                                Auditor.Log(episodeId, patientId, assessmentId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                            }
                                                            else
                                                            {
                                                                validationInfo.Message = "A problem occured while saving the OASIS assessment. Try again.";
                                                                assessment.HippsCode = oldHippsCode;
                                                                assessment.HippsVersion = oldHippsVersion;
                                                                assessment.ClaimKey = oldClaimKey;
                                                                assessment.SubmissionFormat = oldSubmissionFormat;
                                                                assessment.IsValidated = oldIsValidated;
                                                                baseAssessmentRepository.Update(assessment);
                                                            }
                                                        }
                                                    }
                                                    else if (assessmentType == AssessmentType.Discharge.ToString() || assessmentType == AssessmentType.Death.ToString() || assessmentType == AssessmentType.TransferDischarge.ToString() || assessmentType == AssessmentType.Transfer.ToString())
                                                    {
                                                        assessment.SubmissionFormat = oasisFormatString;
                                                        assessment.IsValidated = true;
                                                        if (baseAssessmentRepository.Update(assessment))
                                                        {
                                                            if (mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                                                            {
                                                                validationInfo.Count = validationErrors.Count;
                                                                validationInfo.ValidationErrors = validationErrors;
                                                                validationInfo.Message = "Your OASIS assessment was successfully saved.";
                                                                Auditor.Log(episodeId, patientId, assessmentId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");

                                                            }
                                                            else
                                                            {
                                                                validationInfo.Message = "A problem occured while saving the OASIS assessment. Try again.";
                                                                assessment.SubmissionFormat = oldSubmissionFormat;
                                                                assessment.IsValidated = oldIsValidated;
                                                                baseAssessmentRepository.Update(assessment);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                                                    {
                                                        assessment.ClaimKey = string.Empty;
                                                        assessment.HippsCode = string.Empty;
                                                        assessment.HippsVersion = string.Empty;
                                                        assessment.SubmissionFormat = string.Empty;
                                                        assessment.IsValidated = false;
                                                        if (baseAssessmentRepository.Update(assessment))
                                                        {
                                                            if (mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                                                            {
                                                                Auditor.Log(episodeId, patientId, assessmentId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not error free");
                                                            }
                                                            else
                                                            {
                                                                validationInfo.Message = "A problem occured while saving the OASIS assessment. Try again.";
                                                                assessment.HippsCode = oldHippsCode;
                                                                assessment.HippsVersion = oldHippsVersion;
                                                                assessment.ClaimKey = oldClaimKey;
                                                                assessment.SubmissionFormat = oldSubmissionFormat;
                                                                assessment.IsValidated = oldIsValidated;
                                                                baseAssessmentRepository.Update(assessment);
                                                            }
                                                        }
                                                    }
                                                    else if (assessmentType == AssessmentType.Discharge.ToString() || assessmentType == AssessmentType.Death.ToString() || assessmentType == AssessmentType.TransferDischarge.ToString() || assessmentType == AssessmentType.Transfer.ToString())
                                                    {
                                                        assessment.SubmissionFormat = string.Empty;
                                                        assessment.IsValidated = false;
                                                        if (baseAssessmentRepository.Update(assessment))
                                                        {
                                                            if (mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                                                            {
                                                                Auditor.Log(episodeId, patientId, assessmentId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not error free");
                                                            }
                                                            else
                                                            {
                                                                validationInfo.Message = "A problem occured while saving the OASIS assessment. Try again.";
                                                                assessment.SubmissionFormat = oldSubmissionFormat;
                                                                assessment.IsValidated = oldIsValidated;
                                                                baseAssessmentRepository.Update(assessment);
                                                            }
                                                        }
                                                    }
                                                    validationInfo.Count = validationErrors.Count;
                                                    validationInfo.ValidationErrors = validationErrors.OrderBy(m => m.ErrorType).ToList();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return validationInfo;
        }

        public string GetOasisSubmissionFormatNew(IDictionary<string, Question> assessmentQuestions, int versionNumber, AgencyLocation patientLocation)
        {
            return OasisFormatter.Format(assessmentQuestions, versionNumber, patientLocation, GetOasisSubmissionFormatInstructionsNew());
        }

        public void SetEpisodeInfoForMedicationProfilePrint(Guid patientId, MedicationProfileSnapshotViewData profile)
        {
            if (profile != null)
            {
                var currentEpisode = baseEpisodeRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientId, DateTime.Now);
                if (currentEpisode != null)
                {
                    profile.EpisodeId = currentEpisode.Id;
                    profile.StartDate = currentEpisode.StartDate;
                    profile.EndDate = currentEpisode.EndDate;

                    if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                    {
                        var assessment = EpisodeAssessmentHelperFactory<E, T>.GetEpisodeAssessmentAndOasisData(currentEpisode);
                        if (assessment != null)
                        {
                            var questions = assessment.Questions.ToOASISDictionary();
                            if (questions != null)
                            {
                                profile.PrimaryDiagnosis = questions.AnswerOrEmptyString("M1020PrimaryDiagnosis");
                                profile.SecondaryDiagnosis = questions.AnswerOrEmptyString("M1022PrimaryDiagnosis1");
                            }
                        }
                    }
                }
            }
        }

        public void SetAssessmentAndEpisodeInfoForMedicationProfile(Guid patientId, MedicationProfileViewData viewData)
        {
            var currentEpisode = baseEpisodeRepository.GetLastOrCurrentEpisode(Current.AgencyId, patientId, DateTime.Now.Date);
            if (currentEpisode != null)
            {
                viewData.EpisodeId = currentEpisode.Id;
                viewData.StartDate = currentEpisode.StartDate;
                viewData.EndDate = currentEpisode.EndDate;

                if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                {
                    var assessment = EpisodeAssessmentHelperFactory<E, T>.GetEpisodeAssessmentAndOasisData(currentEpisode);
                    if (assessment != null)
                    {
                        var questions = assessment.Questions.ToOASISDictionary();
                        if (questions != null && questions.Count > 0)
                        {
                            var diagnosis = questions.ToDiagnosis();
                            if (diagnosis != null)
                            {
                                viewData.PrimaryDiagnosis = diagnosis.AnswerOrEmptyString("M1020PrimaryDiagnosis");
                                viewData.SecondaryDiagnosis = diagnosis.AnswerOrEmptyString("M1022PrimaryDiagnosis1");
                            }
                        }
                    }
                }
            }
        }

        #endregion

        public List<Question> Get485FromAssessment(Assessment assessment)
        {
            var planofCareQuestions = new List<Question>();
            if (assessment != null)
            {
                var oasisQuestions = assessment.Questions;
                var assessmentQuestions = oasisQuestions.ToOASISDictionary();
                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                {
                    // 10. Medications
                    var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                    if (medicationProfile != null)
                    {
                        planofCareQuestions.Add(QuestionFactory<Question>.Create("485Medications", medicationProfile.ToString()));
                    }

                    planofCareQuestions.Add(GetQuestion("M0102PhysicianOrderedDate", assessmentQuestions));

                    // 11. Principal Diagnosis
                    planofCareQuestions.Add(GetQuestion("M1020PrimaryDiagnosis", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("M1020ICD9M", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485ExacerbationOrOnsetPrimaryDiagnosis", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("M1020PrimaryDiagnosisDate", assessmentQuestions));

                    // 12. Surgical Procedure
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureDescription1", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode1", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode1Date", assessmentQuestions));

                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureDescription2", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode2", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode2Date", assessmentQuestions));

                    // 13. Other Pertinent Diagnosis
                    for (int count = 1; count <= 14; count++)
                    {
                        planofCareQuestions.Add(GetQuestion(string.Format("M1022PrimaryDiagnosis{0}", count), assessmentQuestions));
                        planofCareQuestions.Add(GetQuestion(string.Format("M1022ICD9M{0}", count), assessmentQuestions));
                        planofCareQuestions.Add(GetQuestion(string.Format("485ExacerbationOrOnsetPrimaryDiagnosis{0}", count), assessmentQuestions));
                        planofCareQuestions.Add(GetQuestion(string.Format("M1022PrimaryDiagnosis{0}Date", count), assessmentQuestions));
                    }

                    // 14. DME and Supplies
                    planofCareQuestions.Add(GetQuestion("485DME", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485DMEComments", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485Supplies", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SuppliesComment", assessmentQuestions));

                    // 15. Safety Measures
                    planofCareQuestions.Add(GetQuestion("485SafetyMeasures", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485OtherSafetyMeasures", assessmentQuestions));

                    // 16. Nutritional Requirements
                    if (assessmentQuestions.Get("485NutritionalReqs") != null)
                    {
                        planofCareQuestions.Add(QuestionFactory<Question>.Create("485NutritionalReqs", PlanofCareXml.ExtractText("NutritionalRequirements", assessmentQuestions)));
                    }

                    // 17. Allergies
                    var allergyProfile = patientRepository.GetAllergyProfileByPatient(assessment.PatientId, Current.AgencyId);
                    planofCareQuestions.Add(GetQuestion("485Allergies", assessmentQuestions));
                    var allergyQuestion = assessmentQuestions.Get("485AllergiesDescription");
                    if (allergyQuestion != null)
                    {
                        planofCareQuestions.Add(allergyQuestion);
                    }
                    else
                    {
                        planofCareQuestions.Add(QuestionFactory<Question>.Create("485AllergiesDescription", allergyProfile != null ? allergyProfile.ToString() : string.Empty));
                    }

                    // 18.A. Functional Limitations
                    planofCareQuestions.Add(GetQuestion("485FunctionLimitations", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485FunctionLimitationsOther", assessmentQuestions));

                    // 18.B. Activities Permitted
                    planofCareQuestions.Add(GetQuestion("485ActivitiesPermitted", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485ActivitiesPermittedOther", assessmentQuestions));


                    // 19. Mental Status
                    planofCareQuestions.Add(GetQuestion("485MentalStatus", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485MentalStatusOther", assessmentQuestions));

                    // 20. Prognosis
                    planofCareQuestions.Add(GetQuestion("485Prognosis", assessmentQuestions));

                    // 21. Interventions
                    var interventions = string.Empty;
                    var snFreq = assessmentQuestions.Get("485SNFrequency");
                    var ptFreq = assessmentQuestions.Get("485PTFrequency");
                    var otFreq = assessmentQuestions.Get("485OTFrequency");
                    var stFreq = assessmentQuestions.Get("485STFrequency");
                    var mswFreq = assessmentQuestions.Get("485MSWFrequency");
                    var hhaFreq = assessmentQuestions.Get("485HHAFrequency");

                    if (snFreq != null && snFreq.Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("SN Frequency: {0}. ", snFreq.Answer);
                    }
                    if (ptFreq != null && ptFreq.Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("PT Frequency: {0}. ", ptFreq.Answer);
                    }
                    if (otFreq != null && otFreq.Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("OT Frequency: {0}. ", otFreq.Answer);
                    }
                    if (stFreq != null && stFreq.Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("ST Frequency: {0}. ", stFreq.Answer);
                    }
                    if (mswFreq != null && mswFreq.Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("MSW Frequency: {0}. ", mswFreq.Answer);
                    }
                    if (hhaFreq != null && hhaFreq.Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("HHA Frequency: {0}. ", hhaFreq.Answer);
                    }
                    var orderInterventionQuestion = assessmentQuestions.Get("485OrdersDisciplineInterventionComments");
                    if (orderInterventionQuestion != null && orderInterventionQuestion.Answer.IsNotNullOrEmpty())
                    {
                        interventions += orderInterventionQuestion.Answer;
                    }

                    var vitalSignParameters = PlanofCareXml.ExtractVitalSigns(assessmentQuestions);
                    if (vitalSignParameters.IsNotNullOrEmpty())
                    {
                        interventions += "SN to notify MD of: " + vitalSignParameters;
                    }

                    interventions += PlanofCareXml.ExtractText("Intervention", assessmentQuestions);
                    var interventionQuestion = QuestionFactory<Question>.Create("485Interventions", interventions);
                    planofCareQuestions.Add(interventionQuestion);

                    //22. Goals
                    var goals = string.Empty;
                    goals += PlanofCareXml.ExtractText("Goal", assessmentQuestions);
                    //The following items are from the Orders for Discipline and Treatment tab.
                    //Due to none of them following the standard used to extract goals they need to be done manually.
                    var rehabPotentialQuestion = assessmentQuestions.Get("485RehabilitationPotential");
                    if (rehabPotentialQuestion != null)
                    {
                        if (rehabPotentialQuestion.Answer == "1")
                        {
                            goals += "Rehab Potential: Good for stated goals.";
                        }
                        if (rehabPotentialQuestion.Answer == "2")
                        {
                            goals += "Rehab Potential: Fair for stated goals.";
                        }
                        if (rehabPotentialQuestion.Answer == "3")
                        {
                            goals += "Rehab Potential: Poor for stated goals.";
                        }
                    }
                    var rehabilitationPotentialOther = assessmentQuestions.Get("485AchieveGoalsComments");
                    if (rehabilitationPotentialOther != null && rehabilitationPotentialOther.Answer.IsNotNullOrEmpty())
                    {
                        goals += rehabilitationPotentialOther.Answer;
                    }
                    var dischargeQuestion = assessmentQuestions.Get("485DischargePlans");
                    if (dischargeQuestion != null)
                    {
                        if (dischargeQuestion.Answer == "1")
                        {
                            goals += "Discharge Plan: Patient to be discharged to the care of Physician. ";
                        }
                        if (dischargeQuestion.Answer == "2")
                        {
                            goals += "Discharge Plan: Patient to be discharged to the care of Caregiver. ";
                        }
                        if (dischargeQuestion.Answer == "3")
                        {
                            goals += "Discharge Plan: Patient to be discharged to Self care. ";
                        }
                    }

                    var dischargeReasonQuestion = assessmentQuestions.Get("485DischargePlansReason");
                    if (dischargeReasonQuestion != null && dischargeReasonQuestion.Answer.IsNotNullOrEmpty())
                    {
                        var reasons = dischargeReasonQuestion.Answer.Split(',');
                        foreach (string reason in reasons)
                        {
                            if (reason.IsEqual("1"))
                            {
                                goals += "Discharge when caregiver willing and able to manage all aspects of patient's care. ";
                            }
                            if (reason.IsEqual("2"))
                            {
                                goals += "Discharge when goals met. ";
                            }
                            if (reason.IsEqual("3"))
                            {
                                goals += "Discharge when wound(s) healed. ";
                            }
                        }
                    }
                    var dischargeCommentsQuestion = assessmentQuestions.Get("485DischargePlanComments");
                    if (dischargeCommentsQuestion != null && dischargeCommentsQuestion.Answer.IsNotNullOrEmpty())
                    {
                        goals += dischargeCommentsQuestion.Answer;
                    }

                    var goalQuestion = QuestionFactory<Question>.Create("485Goals", goals);
                    planofCareQuestions.Add(goalQuestion);
                }
            }
            return planofCareQuestions;
        }

        protected IDictionary<string, Question> PatientSpecificDemographics(Patient patient, Profile profile)
        {
            var questions = new Dictionary<string, Question>();
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);// AgencyEngine.Get(Current.AgencyId);
            questions.Add("M0020PatientIdNumber", new Question { Name = "PatientIdNumber", Code = "M0020", Answer = patient.PatientIdNumber, Type = QuestionType.Moo });

            questions.Add("M0010CertificationNumber", new Question { Name = "CertificationNumber", Code = "M0010", Answer = agency.MedicareProviderNumber, Type = QuestionType.Moo });
            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
            if (physician != null)
            {
                if (physician.NPI.IsNotNullOrEmpty() && physician.NPI.Length == 10)
                {
                    questions.Add("M0018NationalProviderId", new Question { Name = "NationalProviderId", Code = "M0018", Answer = physician.NPI, Type = QuestionType.Moo });
                }
            }

            questions.Add("M0040FirstName", new Question { Name = "FirstName", Code = "M0040", Answer = patient.FirstName, Type = QuestionType.Moo });
            string val = patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToString() : "";
            questions.Add("M0040MI", new Question { Name = "MI", Code = "M0040", Answer = val, Type = QuestionType.Moo });
            questions.Add("M0040LastName", new Question { Name = "LastName", Code = "M0040", Answer = patient.LastName, Type = QuestionType.Moo });
            questions.Add("M0050PatientState", new Question { Name = "PatientState", Code = "M0050", Answer = patient.AddressStateCode, Type = QuestionType.Moo });
            questions.Add("M0060PatientZipCode", new Question { Name = "PatientZipCode", Code = "M0060", Answer = patient.AddressZipCode, Type = QuestionType.Moo });
            questions.Add("M0063PatientMedicareNumber", new Question { Name = "PatientMedicareNumber", Code = "M0063", Answer = patient.MedicareNumber, Type = QuestionType.Moo });
            questions.Add("M0064PatientSSN", new Question { Name = "PatientSSN", Code = "M0064", Answer = patient.SSN, Type = QuestionType.Moo });
            questions.Add("M0065PatientMedicaidNumber", new Question { Name = "PatientMedicaidNumber", Code = "M0065", Answer = patient.MedicaidNumber, Type = QuestionType.Moo });
            questions.Add("M0066PatientDoB", new Question { Name = "PatientDoB", Code = "M0066", Answer = patient.DOBFormatted, Type = QuestionType.Moo });
            if (patient.Gender == "Male")
            {
                questions.Add("M0069Gender", new Question { Name = "Gender", Code = "M0069", Answer = "1", Type = QuestionType.Moo });
            }
            else if (patient.Gender == "Female")
            {
                questions.Add("M0069Gender", new Question { Name = "Gender", Code = "M0069", Answer = "2", Type = QuestionType.Moo });
            }

            if (profile.ReferralDate > DateTime.MinValue)
            {
                questions.Add("M0104ReferralDate", new Question { Name = "ReferralDate", Code = "M0104", Answer = profile.ReferralDateFormatted, Type = QuestionType.Moo });
            }

            if (patient.Ethnicities != null)
            {
                var races = patient.Ethnicities.Split(';');
                if (races != null && races.Length > 0)
                {
                    //foreach (var race in races)
                    //{
                    if (races.Contains("0"))
                    {
                        questions.Add("M0140RaceAMorAN", new Question { Name = "RaceAMorAN", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("1"))
                    {
                        questions.Add("M0140RaceAsia", new Question { Name = "RaceAsia", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("2"))
                    {
                        questions.Add("M0140RaceBalck", new Question { Name = "RaceBalck", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("3"))
                    {
                        questions.Add("M0140RaceHispanicOrLatino", new Question { Name = "RaceHispanicOrLatino", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("4"))
                    {
                        questions.Add("M0140RaceNHOrPI", new Question { Name = "RaceNHOrPI", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("5"))
                    {
                        questions.Add("M0140RaceWhite", new Question { Name = "RaceWhite", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    //}
                }
            }

            if (profile != null && profile.PaymentSource.IsNotNullOrEmpty())
            {
                var paymentSources = profile.PaymentSource.Split(';');
                if (paymentSources != null && paymentSources.Length > 0)
                {
                    //foreach (var paymentSource in paymentSources)
                    //{
                    if (paymentSources.Contains("0"))
                    {
                        questions.Add("M0150PaymentSourceNone", new Question { Name = "PaymentSourceNone", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("1"))
                    {
                        questions.Add("M0150PaymentSourceMCREFFS", new Question { Name = "PaymentSourceMCREFFS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("2"))
                    {
                        questions.Add("M0150PaymentSourceMCREHMO", new Question { Name = "PaymentSourceMCREHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("3"))
                    {
                        questions.Add("M0150PaymentSourceMCAIDFFS", new Question { Name = "PaymentSourceMCAIDFFS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("4"))
                    {
                        questions.Add("M0150PaymentSourceMACIDHMO", new Question { Name = "PaymentSourceMACIDHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("5"))
                    {
                        questions.Add("M0150PaymentSourceWRKCOMP", new Question { Name = "PaymentSourceWRKCOMP", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("6"))
                    {
                        questions.Add("M0150PaymentSourceTITLPRO", new Question { Name = "PaymentSourceTITLPRO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("7"))
                    {
                        questions.Add("M0150PaymentSourceOTHGOVT", new Question { Name = "PaymentSourceOTHGOVT", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("8"))
                    {
                        questions.Add("M0150PaymentSourcePRVINS", new Question { Name = "PaymentSourcePRVINS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("9"))
                    {
                        questions.Add("M0150PaymentSourcePRVHMO", new Question { Name = "PaymentSourcePRVHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("10"))
                    {
                        questions.Add("M0150PaymentSourceSelfPay", new Question { Name = "PaymentSourceSelfPay", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("11"))
                    {
                        questions.Add("M0150PaymentSourceUnknown", new Question { Name = "PaymentSourceUnknown", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("12"))
                    {
                        questions.Add("M0150PaymentSourceOtherSRS", new Question { Name = "PaymentSourceOtherSRS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                        questions.Add("M0150PaymentSourceOther", new Question { Name = "PaymentSourceOther", Code = "M0150", Answer = profile.OtherPaymentSource, Type = QuestionType.Moo });
                    }
                    // }
                }
            }


            questions.Add("GenericPatientDNR", new Question { Name = "PatientDNR", Answer = patient != null ? (patient.IsDNR ? "Yes" : "No") : "No", Type = QuestionType.Generic });

            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patient.Id, Current.AgencyId);
            if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
            {
                var allergyList = allergyProfile.Allergies.ToObject<List<Allergy>>().Where(a => a.IsDeprecated == false).ToList();
                if (allergyList != null && allergyList.Count > 0)
                {
                    questions.Add("485Allergies", new Question { Name = "Allergies", Answer = "Yes", Type = QuestionType.PlanofCare });
                    questions.Add("485AllergiesDescription", new Question { Name = "AllergiesDescription", Answer = allergyProfile.ToString(), Type = QuestionType.PlanofCare });
                }
            }
            return questions;
        }

        protected static Dictionary<string, Question> ScheduleAndEpisodeSpecificDemographics(int disciplineTask, DateTime startDate, DateTime startOfCareDate)
        {
            var questions = new Dictionary<string, Question>();
            questions.Add("M0030SocDate", new Question { Name = "SocDate", Code = "M0030", Answer = startOfCareDate.ToString("MM/dd/yyyy"), Type = QuestionType.Moo });
            if (DisciplineTaskFactory.SOCDisciplineTasks(false).Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "01", Type = QuestionType.Moo });
                questions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = "00000" });
                questions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = "00000" });
            }
            else if (DisciplineTaskFactory.ROCDisciplineTasks().Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "03", Type = QuestionType.Moo });
                questions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = "00000" });
                questions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = "00000" });
            }
            else if (DisciplineTaskFactory.RecertDisciplineTasks(false).Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "04", Type = QuestionType.Moo });
                questions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = "00000" });
                questions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = "00000" });
            }
            else if (DisciplineTaskFactory.DischargeOASISDisciplineTasks(false).Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "09", Type = QuestionType.Moo });
            }
            else if (DisciplineTaskFactory.DeathOASISDisciplineTasks().Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "08", Type = QuestionType.Moo });
            }
            else if (DisciplineTaskFactory.FollowUpDisciplineTasks().Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "05", Type = QuestionType.Moo });
                questions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = "00000" });
                questions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = "00000" });
            }
            else if (DisciplineTaskFactory.TransferDischargeOASISDisciplineTasks().Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "07", Type = QuestionType.Moo });
            }
            else if (DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().Contains(disciplineTask))
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "06", Type = QuestionType.Moo });
            }
            questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = startDate.ToShortDateString(), Type = QuestionType.Generic });
            return questions;
        }

        protected bool AddOrUpdateHospitalizationLog(Assessment assessment)
        {
            var result = false;

            var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, assessment.PatientId, assessment.Id);
            if (hospitalizationLog == null)
            {
                hospitalizationLog = new HospitalizationLog()
                {
                    Id = assessment.Id,
                    PatientId = assessment.PatientId,
                    EpisodeId = assessment.EpisodeId,
                    UserId = Current.UserId,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    AgencyId = Current.AgencyId,
                    SourceId = (int)TransferSourceTypes.Oasis
                };
                AddOrUpdateHospitalizationLogHelper(assessment, hospitalizationLog);
                result = patientRepository.AddHospitalizationLog(hospitalizationLog);
            }
            else
            {
                hospitalizationLog.EpisodeId = assessment.EpisodeId;
                hospitalizationLog.UserId = Current.UserId;
                hospitalizationLog.Modified = DateTime.Now;
                AddOrUpdateHospitalizationLogHelper(assessment, hospitalizationLog);
                result = patientRepository.UpdateHospitalizationLog(hospitalizationLog);
            }
            if (result)
            {
                result = patientRepository.ToggleHosipitalization(assessment.PatientId, Current.AgencyId, hospitalizationLog.Id, true);
            }
            return result;
        }

        private void AddOrUpdateHospitalizationLogHelper(Assessment assessment, HospitalizationLog hospitalizationLog)
        {
            var assessmentQuestionData = mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id);
            if (assessmentQuestionData != null)
            {
                var assessmentData = assessmentQuestionData.Question.ToOASISDictionary();
                if (assessmentData != null)
                {
                    hospitalizationLog.HospitalizationDate = assessmentData.AnswerOrMinDate("M0906DischargeDate");
                    hospitalizationLog.LastHomeVisitDate = assessmentData.AnswerOrMinDate("M0903LastHomeVisitDate");
                    var hospitalizationData = assessmentData.GetHospitalizationData();
                    if (hospitalizationData != null)
                    {
                        hospitalizationLog.Data = hospitalizationData.ToXml();
                    }
                }
            }
        }

        protected bool MarkEndOfHospitalization(Guid id, Guid patientId, DateTime returnDate)
        {
            // TODO: The hospitalization logs are badly broken. Need time to redesign.
            //var currentHospitalizationLog = patientRepository.GetCurrentPatientHospitalizationLog(patientId, Current.AgencyId);
            //if (patientRepository.MarkEndOfHospitalization(id, patientId, Current.AgencyId, returnDate))
            //{
            //    if (patientRepository.ToggleHosipitalization(patientId, Current.AgencyId, id, false))
            //    {
            //        return true;
            //    }
            //    patientRepository.MarkEndOfHospitalization(id, patientId, Current.AgencyId, DateTime.MinValue);
            //}
            return false;
        }

        #region Private Members

        private T CreatePlanOfCareTask(Guid id, T task, Assessment assessment, bool isNonOasis)
        {
            return TaskHelperFactory<T>.CreatePlanOfCareTask(id, task.UserId, assessment.EpisodeId,
                assessment.PatientId, (int)ScheduleStatus.OrderSaved, Disciplines.Orders,
                task.EventDate, isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
                assessment.Type == AssessmentType.Recertification.ToString());
        }

        private OasisViewData AddAssessment(FormCollection formCollection, string assessmentType)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            Assessment assessment = new Assessment();
            assessment.Id = Guid.NewGuid();
            assessment.AgencyId = Current.AgencyId;
            assessment.Status = (int)ScheduleStatus.OasisSaved;
            assessment.Type = assessmentType;
            assessment.PatientId = formCollection.Get(string.Format("{0}_PatientGuid", assessmentType)).ToGuid();
            this.Process(formCollection, assessment, out assessment);
            if (baseAssessmentRepository.Add(assessment))
            {
                var data = new AssessmentQuestionData(assessment);
                if (mongoRepository.AddAssessmentQuestion(data))
                {
                    oasisViewData.isSuccessful = true;
                    oasisViewData.assessmentId = assessment.Id;
                    oasisViewData.Assessment = assessment;
                }
                else
                {
                    baseAssessmentRepository.RemoveModel<Assessment>(assessment.Id);
                }
            }
            return oasisViewData;
        }

        private OasisViewData UpdateAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles, string assessmentType)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false, errorMessage = "There is an error sending the data. Try again." };
            Guid assessmentId = formCollection.Get(string.Format("{0}_Id", assessmentType)).ToGuid();
            string category = formCollection["categoryType"];
            string action = formCollection.Get(string.Format("{0}_Button", assessmentType));
            Guid patientId = formCollection.Get(string.Format("{0}_PatientGuid", assessmentType)).ToGuid();
            var assessment = GetAssessmentWithQuestions(patientId, assessmentId);
            if (assessment != null)
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, assessment.PatientId, assessment.Id);
                if (scheduleEvent != null)
                {
                    VitalSignLog vitalSigns = null;
                    assessment.PatientId = patientId;
                    assessment.Type = assessmentType;
                    var oldStatus = scheduleEvent.Status;
                    var oldSignature = assessment.SignatureText;
                    var oldSignatureDate = assessment.SignatureDate;
                    var oldIsValidated = assessment.IsValidated;
                    var isActionForStatusChange = false;
                    var isCaseManagementRefresh = false;
                    bool updateAssessmentData = false;
                    switch (action)
                    {
                        case "Approve":
                            {
                                isCaseManagementRefresh = oldStatus == (int)ScheduleStatus.OasisCompletedPendingReview;
                                var aproveValidation = true;
                                assessment.Status = (int)ScheduleStatus.OasisCompletedExportReady;
                                if (assessment.SignatureText.IsNullOrEmpty() || assessment.SignatureDate <= DateTime.MinValue)
                                {
                                    assessment.Status = oldStatus;
                                    oasisViewData.errorMessage = string.Format("This OASIS Assessment could not be approved because the Electronic Signature is missing.{0}", assessment.TypeName.Contains("NonOasis") ? string.Empty : " Click on 'Check on Errors' to complete the OASIS Assessment. ");
                                    aproveValidation = false;
                                }
                                if (!assessment.TypeName.Contains("NonOasis"))
                                {
                                    var checkError = this.Validate(assessment, out assessment);
                                    if (!checkError)
                                    {
                                        assessment.Status = oldStatus;
                                        oasisViewData.errorMessage += "This OASIS Assessment could not be approved due to validation errors. Click on 'Check on Errors' and resolve all errors before continuing. ";
                                        aproveValidation = false;
                                    }
                                }
                                isActionForStatusChange = true;
                                if (!aproveValidation)
                                {
                                    oasisViewData.isSuccessful = false;
                                    return oasisViewData;
                                }
                            }
                            break;
                        case "Return":
                            {
                                isCaseManagementRefresh = oldStatus == (int)ScheduleStatus.OasisCompletedPendingReview;
                                assessment.Status = (int)ScheduleStatus.OasisReturnedForClinicianReview;
                                assessment.SignatureText = string.Empty;
                                assessment.SignatureDate = DateTime.MinValue;
                                isActionForStatusChange = true;
                            }
                            break;
                        default:
                            {
                                vitalSigns = vitalSignRepository.GetEntitiesVitalSign(assessmentId, patientId, Current.AgencyId) ?? new VitalSignLog()
                                                                                                                                    {
                                                                                                                                        Id = Guid.NewGuid(),
                                                                                                                                        IsNew = true,
                                                                                                                                        AgencyId = Current.AgencyId,
                                                                                                                                        PatientId = patientId,
                                                                                                                                        EntityId = assessmentId
                                                                                                                                    };
                                this.ProcessVitalSigns(formCollection, vitalSigns);
                                this.Process(formCollection, assessment, out assessment);
                                updateAssessmentData = true;
                                if (oldStatus == (int)ScheduleStatus.OasisCompletedPendingReview || oldStatus == (int)ScheduleStatus.OasisCompletedExportReady)
                                {
                                    assessment.IsValidated = false;
                                }
                                else
                                {
                                    assessment.Status = (int)ScheduleStatus.OasisSaved;
                                }
                            }
                            break;
                    }
                    if (CompleteAssessmentUpdate(assessment, scheduleEvent, isActionForStatusChange, oldStatus, oldSignature, oldSignatureDate, oldIsValidated, (category.IsNotNullOrEmpty() && category.IsEqual(AssessmentCategory.Integumentary.ToString()) && !isActionForStatusChange), updateAssessmentData, httpFiles, vitalSigns))
                    {
                        oasisViewData.isSuccessful = true;
                        oasisViewData.assessmentId = assessment.Id;
                        oasisViewData.Service = Service.ToString();
                        oasisViewData.ServiceId = (int)Service;
                        oasisViewData.Assessment = assessment;
                        oasisViewData.IsDataCentersRefresh = oldStatus != assessment.Status;
                        oasisViewData.IsCaseManagementRefresh = oasisViewData.IsDataCentersRefresh && isCaseManagementRefresh;
                        oasisViewData.IsMyScheduleTaskRefresh = oasisViewData.IsDataCentersRefresh && scheduleEvent.UserId == Current.UserId && !scheduleEvent.IsComplete && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
                    }
                }
                else
                {
                    oasisViewData.errorMessage = "Schedule Data is not found. Try again.";
                }
            }
            else
            {
                oasisViewData.errorMessage = "Assessment Data is not found. Try again.";
            }
            return oasisViewData;
        }

        private bool CompleteAssessmentUpdate(Assessment assessment, T scheduleEvent, bool isActionForStatusChange, int oldStatus, string oldSignature, DateTime oldSignatureDate, bool oldIsValidated, bool IsSaveAsset, bool updateAssessmentData, HttpFileCollectionBase httpFiles, VitalSignLog vitalSignLog)
        {
            bool result = false;
            var oldAsset = scheduleEvent.Asset;
            scheduleEvent.Status = assessment.Status;
            var isAssetNotConsidered = true;
            var isAssetReady = false;

            if (IsSaveAsset && AssetHelper.ContainsAssets(httpFiles))
            {
                isAssetNotConsidered = false;
                isAssetReady = SaveAsset(httpFiles, ref assessment, ref scheduleEvent).IsNotNullOrEmpty();
                updateAssessmentData = true;
            }
            if (isAssetNotConsidered || isAssetReady)
            {
                scheduleEvent.Asset = scheduleEvent.Assets.ToXml();
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        try
                        {
                            if (baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id, scheduleEvent.Status, scheduleEvent.Asset))
                            {
                                if (baseAssessmentRepository.Update(assessment))
                                {
                                    VitalSignLog existingVitalSign = null;
                                    bool isVitalSignUpdated = true;
                                    if (updateAssessmentData && vitalSignLog != null && vitalSignLog.VitalSignsChanged)
                                    {
                                        isVitalSignUpdated = vitalSignRepository.AddOrUpdate(vitalSignLog, ref existingVitalSign);
                                    }
                                    if (isVitalSignUpdated)
                                    {
                                        if (!updateAssessmentData || mongoRepository.UpdateAssessmentQuestion(new AssessmentQuestionData(assessment)))
                                        {
                                            result = true;
                                            ts.Complete();
                                        }
                                        else
                                        {
                                            throw new Exception("Failed to update the assessment's questions.");
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Failed to update the vital signs.");
                                    }
                                }
                                else
                                {
                                    throw new Exception("Failed to update the assessment.");
                                    //Need to delete the assets that were added
                                }
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }
                        catch (Exception e)
                        {
                            ts.Dispose();
                        }
                    }
                }
                if (result)
                {
                    if (isActionForStatusChange)
                    {
                        if (scheduleEvent.Status > 0)
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                        }
                    }
                    else
                    {
                        Auditor.Log(
                            Current.AgencyId,
                            Current.UserId,
                            Current.UserFullName,
                            scheduleEvent.EpisodeId,
                            scheduleEvent.PatientId,
                            scheduleEvent.Id,
                            oldStatus == (int)ScheduleStatus.OasisCompletedPendingReview ? Actions.EditAssessmentForQAReview : (oldStatus == (int)ScheduleStatus.OasisCompletedExportReady ? Actions.EditAssessmentForExportReady : Actions.EditAssessment),
                            (ScheduleStatus)scheduleEvent.Status,
                            (DisciplineTasks)scheduleEvent.DisciplineTask,
                            string.Empty);
                    }
                }
            }
            return result;
        }

        //This dosen't save the assessment to the database. Instead it adds or removes from both the list of asset from scedule event and oasis, but it add to asset database
        private List<Guid> SaveAsset(HttpFileCollectionBase httpFiles, ref Assessment assessment, ref T scheduleEvent)
        {
            var questions = assessment.Questions.ToOASISDictionary();
            scheduleEvent.Assets = (scheduleEvent.Asset.IsNotNullOrEmpty() ? scheduleEvent.Asset.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();
            var assetsAdded = AssetHelper.SaveAssetAndAddToXml<Question>(httpFiles, scheduleEvent.Assets, questions);
            assessment.Questions = questions.Values.ToList();
            return assetsAdded;
        }

        private void ProcessVitalSigns(FormCollection formCollection, VitalSignLog vitalSignLog)
        {
            var category = formCollection["categoryType"];
            var assessment = formCollection.Get("assessment");
            if (category == AssessmentCategory.PatientHistory.ToString())
            {
                vitalSignLog.VitalSignsChanged = GetValueAndRemoveFromFormCollection<Boolean>(formCollection, "vitalSignLog.VitalSignsChanged");
                vitalSignLog.Resp = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.Resp");
                vitalSignLog.RespStatus = GetValueAndRemoveFromFormCollection<int>(formCollection, "vitalSignLog.RespStatus");
                vitalSignLog.Temp = GetValueAndRemoveFromFormCollection<double?>(formCollection, "vitalSignLog.Temp");
                vitalSignLog.TempLocation = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.TempLocation");
                vitalSignLog.TempUnit = GetValueAndRemoveFromFormCollection<int>(formCollection, "vitalSignLog.TempUnit");
                vitalSignLog.Weight = GetValueAndRemoveFromFormCollection<double?>(formCollection, "vitalSignLog.Weight");
                vitalSignLog.WeightUnit = GetValueAndRemoveFromFormCollection<int>(formCollection, "vitalSignLog.WeightUnit");
                vitalSignLog.Height = GetValueAndRemoveFromFormCollection<double?>(formCollection, "vitalSignLog.Height");
                vitalSignLog.HeightUnit = GetValueAndRemoveFromFormCollection<int>(formCollection, "vitalSignLog.HeightUnit");
                vitalSignLog.Pulse = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.Pulse");
                vitalSignLog.PulseLocation = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.PulseLocation");
                vitalSignLog.PulseStatus = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.PulseStatus");
                vitalSignLog.Pulse2 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.Pulse2");
                vitalSignLog.PulseLocation2 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.PulseLocation2");
                vitalSignLog.PulseStatus2 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.PulseStatus2");
                vitalSignLog.BloodGlucose = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodGlucose");
                vitalSignLog.BloodGlucoseType = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodGlucoseType");
                vitalSignLog.OxygenSaturation = GetValueAndRemoveFromFormCollection<double?>(formCollection, "vitalSignLog.OxygenSaturation");
                vitalSignLog.OxygenSaturationType = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.OxygenSaturationType");
                vitalSignLog.BloodPressure = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.BloodPressure");
                vitalSignLog.BloodPressureSide = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressureSide");
                vitalSignLog.BloodPressurePosition = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressurePosition");
                vitalSignLog.BloodPressure2 = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.BloodPressure2");
                vitalSignLog.BloodPressureSide2 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressureSide2");
                vitalSignLog.BloodPressurePosition2 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressurePosition2");
                vitalSignLog.BloodPressure3 = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.BloodPressure3");
                vitalSignLog.BloodPressureSide3 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressureSide3");
                vitalSignLog.BloodPressurePosition3 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressurePosition3");
                vitalSignLog.BloodPressure4 = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.BloodPressure4");
                vitalSignLog.BloodPressureSide4 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressureSide4");
                vitalSignLog.BloodPressurePosition4 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressurePosition4");
                vitalSignLog.BloodPressure5 = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.BloodPressure5");
                vitalSignLog.BloodPressureSide5 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressureSide5");
                vitalSignLog.BloodPressurePosition5 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressurePosition5");
                vitalSignLog.BloodPressure6 = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.BloodPressure6");
                vitalSignLog.BloodPressureSide6 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressureSide6");
                vitalSignLog.BloodPressurePosition6 = GetValueAndRemoveFromFormCollection<int?>(formCollection, "vitalSignLog.BloodPressurePosition6");
                vitalSignLog.Comments = GetValueAndRemoveFromFormCollection<string>(formCollection, "vitalSignLog.Comments");
                formCollection.Remove("vitalSignLog.Id");
                formCollection.Remove("vitalSignLog.IsNew");
            }
            else if (category == AssessmentCategory.Pain.ToString())
            {
                if (assessment.IsNotNullOrEmpty())
                {
                    int? pain = GetValueAndRemoveFromFormCollection<int?>(formCollection, assessment + "_GenericIntensityOfPain");
                    vitalSignLog.VitalSignsChanged = vitalSignLog.Pain != pain;
                    vitalSignLog.Pain = pain;

                }
            }
            else if (category == AssessmentCategory.Endocrine.ToString())
            {
                if (assessment.IsNotNullOrEmpty())
                {
                    var bloodGlucose = GetValueAndRemoveFromFormCollection<int?>(formCollection, assessment + "_GenericBloodSugarLevelText");
                    int? statusNum = null;
                    var status = formCollection.Get(assessment + "_GenericBloodSugarLevel").Trim(',');
                    if (status == BloodGlucoseType.Fasting.ToString())
                    {
                        statusNum = (int)BloodGlucoseType.Fasting;
                    }
                    else if (status == BloodGlucoseType.Random.ToString())
                    {
                        statusNum = (int)BloodGlucoseType.Random;
                    }
                    vitalSignLog.VitalSignsChanged = vitalSignLog.BloodGlucose != bloodGlucose || vitalSignLog.BloodGlucoseType != statusNum;
                    vitalSignLog.BloodGlucose = bloodGlucose;
                    vitalSignLog.BloodGlucoseType = statusNum;
                    formCollection.Remove(assessment + "_GenericBloodSugarLevel");
                }
            }
        }

        private TZ GetValueAndRemoveFromFormCollection<TZ>(FormCollection collection, string key)
        {
            string value = collection.Get(key);
            collection.Remove(key);
            if (value.IsNotNullOrEmpty())
            {
                Type t = Nullable.GetUnderlyingType(typeof(TZ)) ?? typeof(TZ);
                return (TZ)Convert.ChangeType(value, t);
            }
            return default(TZ);
        }

        private void Process(FormCollection formCollection, Assessment assessment, out Assessment recentAssessment)
        {
            //TODO This 
            var assessmentType = formCollection["assessment"];
            formCollection.Remove(assessmentType + "_Id");
            formCollection.Remove(assessmentType + "_Action");
            formCollection.Remove(assessmentType + "_PatientGuid");
            formCollection.Remove(assessmentType + "_EpisodeId");
            formCollection.Remove(assessmentType + "_Button");
            formCollection.Remove("assessment");
            var category = formCollection["categoryType"];
            formCollection.Remove("categoryType");

            recentAssessment = assessment;

            var oasisQuestions = assessment.Questions;
            IDictionary<string, Question> questions = oasisQuestions.ToOASISDictionary();
            //IDictionary<string, Question> questions = assessment.ToDictionary();

            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray != null && nameArray.Length == 3 && nameArray[2] == "text") continue;
                if (nameArray != null && nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    questions[name] = QuestionFactory<Question>.Create(name, formCollection.GetValues(key).Join(","));
                }
            }
            recentAssessment.Questions = questions.Values.ToList();
        }

        private Question GetQuestion(string questionName, IDictionary<string, Question> assessmentQuestions)
        {
            return assessmentQuestions.Get(questionName) ?? new Question();
        }

        private List<Axxess.Api.Contracts.ValidationError> CustomValidation(IDictionary<string, Question> assessmentQuestions)
        {
            var submissionFormat = new StringBuilder();
            var oasisValidationRules = new List<OasisValidation>();
            string type = assessmentQuestions["M0100AssessmentType"].Answer;
            var branchId = assessmentQuestions.Get("M0016BranchId");
            if (branchId != null && branchId.Answer.IsNotNullOrEmpty() && (branchId.Answer != "N" || branchId.Answer != "P"))
            {
                var branchState = assessmentQuestions.Get("M0014BranchState");
                oasisValidationRules.Add(new OasisValidation(() => (branchState != null && !branchState.Answer.IsNotNullOrEmpty()) || (branchState == null), new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If the Branch Id(M0060) code  exist branch state needs to be filled.", ErrorDup = "M0014_BRANCH_STATE" }));
            }
            if (type.Contains("01") || type.Contains("03"))
            {
                var depressionScreening = assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening");
                if (depressionScreening.IsEqual("00")
                    || depressionScreening.IsEqual("02")
                    || depressionScreening.IsEqual("03"))
                {
                    oasisValidationRules.Add(new OasisValidation(() => assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreeningInterest").IsNotNullOrEmpty() || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreeningHopeless").IsNotNullOrEmpty(), new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If Depression Screening (M1730) was not done using the PHQ-2 Scale, then PHQ Scale (M1730_PHQ2) must be left blank.", ErrorDup = "M1730_PHQ2" }));
                }
            }

            if (type.Contains("01"))
            {
                if (!assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDateUnknown").IsEqual("1"))
                {
                    var startofCareDate = assessmentQuestions.AnswerOrMinDate("M0030SocDate");
                    var assessmentDate = assessmentQuestions.AnswerOrMinDate("M0090AssessmentCompleted");
                    var inpatientDischargeDate = assessmentQuestions.AnswerOrMinDate("M1005InpatientDischargeDate");
                    oasisValidationRules.Add(new OasisValidation(() => inpatientDischargeDate.Date > startofCareDate.Date || inpatientDischargeDate.Date > assessmentDate.Date, new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If (M1005) Most recent inpatient discharge date is completed, then M1005 inpatient discharge must be prior to or the same as M0030 (Start of Care Date) and M0090 (Completion Date).", ErrorDup = "M1005_INP_DISCHARGE_DT" }));
                }
            }

            var providerId = assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderId");
            if (providerId == string.Empty && !assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderIdUnknown").IsEqual("1"))
            {
                oasisValidationRules.Add(new OasisValidation(() => providerId.IsNullOrEmpty(), new Axxess.Api.Contracts.ValidationError { ErrorType = "WARNING", Description = "No NPI Information provided. Enter the NPI of the physician who will sign the Plan of Care or check the Unknown or Not Available option.", ErrorDup = "M0018_PHYSICIAN_ID" }));
            }

            var oasisEntityValidator = new OasisEntityValidator(oasisValidationRules.ToArray());
            oasisEntityValidator.Validate();

            return oasisEntityValidator.ValidationError;
        }

        private Dictionary<string, SubmissionBodyFormat> GetOasisSubmissionFormatInstructionsNew()
        {
            var dictionaryFormat = new Dictionary<string, SubmissionBodyFormat>();
            var format = lookupRepository.GetSubmissionFormatInstructions();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f => dictionaryFormat.Add(f.Item, f));
            }
            return dictionaryFormat;
        }

        private Dictionary<string, SubmissionHeaderFormat> GetOasisHeaderInstructionsNew()
        {
            var format = lookupRepository.GetSubmissionHeaderFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionHeaderFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private Dictionary<string, SubmissionInactiveBodyFormat> GetInactivateOasisSubmissionFormatInstructionsNew()
        {
            var format = lookupRepository.GetSubmissionInactiveBodyFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionInactiveBodyFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private IDictionary<string, NotesQuestion> CombineOasisQuestionsAndNoteQuestions(IDictionary<string, NotesQuestion> oasisQuestions, IDictionary<string, NotesQuestion> noteQuestions)
        {
            var questions = oasisQuestions;
            if (noteQuestions.ContainsKey("TimeIn") && noteQuestions["TimeIn"] != null)
            {
                questions.Add("TimeIn", noteQuestions["TimeIn"]);
            }
            if (noteQuestions.ContainsKey("TimeOut") && noteQuestions["TimeOut"] != null)
            {
                questions.Add("TimeOut", noteQuestions["TimeOut"]);
            }
            if (noteQuestions.ContainsKey("Surcharge") && noteQuestions["Surcharge"] != null)
            {
                questions.Add("Surcharge", noteQuestions["Surcharge"]);
            }
            if (noteQuestions.ContainsKey("AssociatedMileage") && noteQuestions["AssociatedMileage"] != null)
            {
                questions.Add("AssociatedMileage", noteQuestions["AssociatedMileage"]);
            }
            return questions;
        }

        #endregion
    }
}
