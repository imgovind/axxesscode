﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Extensions;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;

    public abstract class ReportService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        #region Constructor and Private Members

        protected MongoAbstract mongoRepository;
        protected IUserRepository userRepository;
        protected IPatientRepository patientRepository;
        protected IPhysicianRepository physicianRepository;
        protected IAgencyRepository agencyRepository;
        protected ILookupRepository lookUpRepository;

        protected PatientProfileAbstract profileRepository;
        protected PatientAdmissionAbstract admissionRepository;
        protected PhysicianOrderAbstract physicianOrderRepository;
        protected PlanOfCareAbstract planofCareRepository;

        private readonly BillingAbstract baseBillingRepository;
        private readonly TaskScheduleAbstract<T> baseScheduleRepository;
        private readonly EpisodeAbstract<E> episodeRepository;

        protected AgencyServices Service { get; set; }

        protected ReportService(PatientProfileAbstract profileRepository,
          BillingAbstract billingRepository,
          PatientAdmissionAbstract admissionRepository,
          PlanOfCareAbstract planofCareRepository,
          TaskScheduleAbstract<T> scheduleRepository,
          EpisodeAbstract<E> episodeRepository,
          PhysicianOrderAbstract physicianOrderRepository,
          MongoAbstract mongoRepository)
        {
            this.mongoRepository = mongoRepository;
            this.profileRepository = profileRepository;
            this.admissionRepository = admissionRepository;
            this.physicianOrderRepository = physicianOrderRepository;
            this.planofCareRepository = planofCareRepository;

            this.baseBillingRepository = billingRepository;
            this.baseScheduleRepository = scheduleRepository;
            this.episodeRepository = episodeRepository;
        }

        #endregion

        #region Patient Reports

        public List<BirthdayWidget> GetCurrentBirthdays()
        {
            var birthdays = new List<BirthdayWidget>();

            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsOfficeManager || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                birthdays = profileRepository.GetCurrentPatientBirthdays(Current.AgencyId);
            }
            else if (Current.IsClinicianOrHHA)
            {
                birthdays = episodeRepository.GetUserBirthDayReport(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active, DateTime.Now.Month);
            }

            return birthdays;
        }

        public List<EmergencyContactInfo> GetPatientEmergencyContacts(Guid branchCode, int statusId)
        {
            return profileRepository.GetEmergencyContactInfos(Current.AgencyId, branchCode, statusId);
        }

        public List<PatientRoster> GetPatientRosterByDateRange(Guid branchCode, int statusId, int insuranceId, DateTime startDate, DateTime endDate, bool isExcel)
        {
            var payor = 0;
            if (insuranceId == 0)
            {
                if (!branchCode.IsEmpty())
                {
                    payor = AgencyInformationHelper.Payor(Current.AgencyId, branchCode);
                }
            }
            var rosterList = profileRepository.GetPatientRosterByDateRange(Current.AgencyId, branchCode, statusId, insuranceId, startDate, endDate, payor);
            return GetPatientRoster(rosterList, branchCode, statusId, insuranceId, isExcel);
        }

        public List<PatientRoster> GetPatientRoster(Guid branchCode, int statusId, int insuranceId, bool isExcel)
        {
            var payor = 0;
            if (insuranceId == 0)
            {
                if (!branchCode.IsEmpty())
                {
                    payor = AgencyInformationHelper.Payor(Current.AgencyId, branchCode);
                }
            }
            var rosterList = profileRepository.GetPatientRoster(Current.AgencyId, branchCode, statusId, insuranceId, payor);
            return GetPatientRoster(rosterList, branchCode, statusId, insuranceId, isExcel);
        }

        private List<PatientRoster> GetPatientRoster(List<PatientRoster> rosterList, Guid branchCode, int statusId, int insuranceId, bool isExcel)
        {
            if (rosterList.IsNotNullOrEmpty())
            {
                var physicians = new List<AgencyPhysician>();

                if (isExcel)
                {
                    var patientIds = rosterList.Select(r => r.Id).Distinct().ToList();
                    physicians = physicianRepository.GetPatientsPrimaryPhysician(Current.AgencyId, patientIds) ?? new List<AgencyPhysician>();
                }
                var insuranceIds = rosterList.Where(s => s.PatientInsuranceId.IsInteger() && s.PatientInsuranceId.ToInteger() > 0).Select(i => i.PatientInsuranceId.ToInteger()).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                rosterList.ForEach(roster =>
                {
                    if (isExcel)
                    {
                        //TODO:needs work
                        //var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, roster.Id, DateTime.Now, "Nursing");
                        //IDictionary<string, Question> lastAssessment = null;
                        //if (lastEpisode != null && !lastEpisode.AssessmentId.IsEmpty() && lastEpisode.AssessmentType.IsNotNullOrEmpty())
                        //{
                        //    lastAssessment = assessmentService.GetAssessment(lastEpisode.AssessmentId, lastEpisode.AssessmentType).ToDictionary();
                        //}
                        //if (lastAssessment != null && lastAssessment.ContainsKey("M1020PrimaryDiagnosis"))
                        //{
                        //    roster.PatientPrimaryDiagnosis = lastAssessment["M1020PrimaryDiagnosis"].Answer;
                        //}
                        //else
                        //{
                        //    roster.PatientPrimaryDiagnosis = "";
                        //}
                        //if (lastAssessment != null && lastAssessment.ContainsKey("M1022PrimaryDiagnosis1"))
                        //{
                        //    roster.PatientSecondaryDiagnosis = lastAssessment["M1022PrimaryDiagnosis1"].Answer;
                        //}
                        //else
                        //{
                        //    roster.PatientSecondaryDiagnosis = "";
                        //}

                        if (!roster.Id.IsEmpty())
                        {
                            var physician = physicians.FirstOrDefault(p => p.PatientId == roster.Id);// PhysicianEngine.Get(roster.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                roster.PhysicianNpi = physician.NPI;
                                roster.PhysicianName = physician.DisplayName;
                                roster.PhysicianPhone = physician.PhoneWork.ToPhone();
                                roster.PhysicianFacsimile = physician.FaxNumber;
                                roster.PhysicianPhoneHome = physician.PhoneAlternate;
                                roster.PhysicianEmailAddress = physician.EmailAddress;
                            }
                        }
                    }
                    var insurance = insurances.FirstOrDefault(i => i.Id.ToString() == roster.PatientInsuranceId);
                    if (insurance != null)
                    {
                        roster.PatientInsuranceName = insurance.Name;
                    }
                });

            }
            return rosterList.OrderBy(o => o.PatientDisplayName).ToList();
        }

        public List<PatientRoster> GetPatientRosterByInsurance(Guid branchCode, int insurance, int statusId)
        {
            var rosterList = new List<PatientRoster>();
            if (insurance > 0)
            {
                rosterList = profileRepository.GetPatientByInsurance(Current.AgencyId, branchCode, insurance, statusId);
            }
            return rosterList.OrderBy(r => r.PatientDisplayName).ToList();
        }

        public List<Authorization> GetExpiringAuthorizaton(Guid branchId, int status)
        {

            var autorizations = profileRepository.GetExpiringAuthorizaton(Current.AgencyId, branchId, status, DateTime.Now.AddDays(14)); // new List<Authorization>();
            //var patients = profileRepository.Find(Current.AgencyId, branchId, status);
            //if (patients != null && patients.Count > 0)
            //{
            //    patients.ForEach(patient =>
            //    {
            //        var allAuthorization = patientRepository.GetAuthorizations(Current.AgencyId, patient.Id, (int)this.Service);
            //        if (allAuthorization != null && allAuthorization.Count > 0)
            //        {
            //            allAuthorization.ForEach(auto =>
            //            {
            //                if (auto.EndDate <= DateTime.Now.AddDays(14))
            //                {
            //                    auto.DisplayName = patient.DisplayNameWithMi;
            //                    autorizations.Add(auto);
            //                }
            //            });
            //        }
            //    }
            //    );
            //}
            return autorizations;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid physicianId, int statusId, AgencyServices service)
        {
            return patientRepository.GetPatientByPhysician(Current.AgencyId, physicianId, statusId, service);
        }

        public List<SurveyCensus> GetPatientSurveyCensus(Guid branchId, int statusId, int insuranceId, bool isExcel)
        {
            var payor = 0;
            if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    payor = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
                }
            }
            var surveyCensuses = profileRepository.GetSurveyCensesByStatus(Current.AgencyId, branchId, statusId, insuranceId, payor);
            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                var patientIds = surveyCensuses.Select(p => p.Id).Distinct().ToList();
                var users = new List<User>();
                var physicians = new List<AgencyPhysician>();
                var insurances = new List<InsuranceLean>();
                if (isExcel)
                {
                    var userIds = surveyCensuses.Where(s => !s.CaseManagerId.IsEmpty()).Select(s => s.CaseManagerId).Distinct().ToList();
                    users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    insurances = agencyRepository.GetLeanInsurances(Current.AgencyId, surveyCensuses.Where(s => s.InsuranceId.IsNotNullOrEmpty() && s.InsuranceId.IsInteger() && s.InsuranceId.ToInteger() >= 1000).Select(i => i.InsuranceId.ToInteger()).Distinct().ToArray());
                    physicians = physicianRepository.GetPatientsPrimaryPhysician(Current.AgencyId, patientIds) ?? new List<AgencyPhysician>();
                }
                SurveyCensusAppSpecific(surveyCensuses, users, physicians, insurances, isExcel, patientIds);
            }
            return surveyCensuses;
        }

        protected abstract void SurveyCensusAppSpecific(List<SurveyCensus> surveyCensuses, List<User> users, List<AgencyPhysician> physicians, List<InsuranceLean> insurances, bool isExcel, List<Guid> patientIds);

        protected void SetUserInsuranceAndPhysician(SurveyCensus surveyCensus, List<User> users, List<AgencyPhysician> physicians, List<InsuranceLean> insurances, bool isExcel)
        {
            if (isExcel)
            {
                var user = users.SingleOrDefault(u => u.Id == surveyCensus.CaseManagerId);
                if (user != null)
                {
                    surveyCensus.CaseManagerDisplayName = user.DisplayName;
                }
                if (!surveyCensus.Id.IsEmpty())
                {
                    var physician = physicians.FirstOrDefault(p => p.PatientId == surveyCensus.Id);
                    if (physician != null)
                    {
                        surveyCensus.PhysicianNPI = physician.NPI;
                        surveyCensus.PhysicianDisplayName = physician.DisplayName;
                        surveyCensus.PhysicianPhone = physician.PhoneWork.ToPhone();
                        surveyCensus.PhysicianFax = physician.FaxNumber;
                        surveyCensus.PhysicianPhone = physician.PhoneAlternate;
                    }
                }
            }

            if (surveyCensus.InsuranceId.IsNotNullOrEmpty() && surveyCensus.InsuranceId.IsInteger())
            {
                if (surveyCensus.InsuranceId.ToInteger() >= 1000)
                {
                    var insurance = insurances.SingleOrDefault(i => i.Id.ToString() == surveyCensus.InsuranceId);
                    if (insurance != null)
                    {
                        surveyCensus.InsuranceName = insurance.Name;
                    }
                }
                else if (surveyCensus.InsuranceId.ToInteger() < 1000 && surveyCensus.InsuranceId.ToInteger() > 0)
                {
                    if (Enum.IsDefined(typeof(MedicareIntermediary), surveyCensus.InsuranceId.ToInteger()))
                    {
                        var name = ((MedicareIntermediary)surveyCensus.InsuranceId.ToInteger()).GetDescription();
                        if (name.IsNotNullOrEmpty())
                        {
                            surveyCensus.InsuranceName = name;
                        }
                    }
                }
            }
        }

        public List<PatientRoster> GetPatientMonthlyAdmission(Guid branchCode, int statusId, int month, int year)
        {
            var rosterList = admissionRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, branchCode, statusId, month, year);
            if (rosterList != null && rosterList.Count > 0)
            {
                var internalReferralUsers = new List<User>();
                var userIds = rosterList.Where(r => !r.InternalReferralId.IsEmpty()).Select(r => r.InternalReferralId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    internalReferralUsers = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                }
                var physicians = new List<AgencyPhysician>();
                var physicianIds = rosterList.Where(r => !r.ReferrerPhysicianId.IsEmpty()).Select(r => r.ReferrerPhysicianId).Distinct().ToList();
                if (physicianIds != null && physicianIds.Count > 0)
                {
                    physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds) ?? new List<AgencyPhysician>();
                }
                rosterList.ForEach(rl =>
                {
                    if (!rl.InternalReferralId.IsEmpty())
                    {
                        var user = internalReferralUsers.FirstOrDefault(u => u.Id == rl.InternalReferralId);
                        if (user != null)
                        {
                            rl.InternalReferral = user.DisplayName;
                        }
                    }
                    if (!rl.ReferrerPhysicianId.IsEmpty())
                    {
                        var physician = physicians.FirstOrDefault(u => u.Id == rl.ReferrerPhysicianId);
                        if (physician != null)
                        {
                            rl.ReferrerPhysician = physician.DisplayName;
                        }
                    }
                });

            }
            return rosterList;
        }

        public List<PatientRoster> GetPatientAnnualAdmission(Guid branchCode, int statusId, int year)
        {
            var rosterList = admissionRepository.GetPatientByAdmissionYear(Current.AgencyId, branchCode, statusId, year);
            return rosterList.OrderBy(o => o.PatientFirstName).ToList();
        }

        public IList<Birthday> GetPatientBirthdays(Guid branchId, int month)
        {
            return profileRepository.GetPatientBirthdays(Current.AgencyId, branchId, month);
        }

        public List<AddressBookEntry> GetPatientAddressListing(Guid branchId, int status)
        {
            return profileRepository.GetPatientAddressListing(Current.AgencyId, branchId, status);
        }

        public List<DischargePatient> GetDischargePatients(Guid branchId, DateTime startDate, DateTime endDate)
        {
            return profileRepository.GetDischargePatients(Current.AgencyId, branchId, startDate, endDate);
        }

        public List<PatientRoster> GetPatientByResponsiableEmployee(Guid branchId, Guid userId, int status)
        {
            return profileRepository.GetPatientByResponsiableEmployee(Current.AgencyId, branchId, userId, status);
        }

        public List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid branchId, Guid caseManagerId)
        {
            return profileRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, branchId, caseManagerId);
        }


        #endregion

        #region Clinical Reports


        //public IList<ClinicalOrder> GetOrders(int statusId)
        //{
        //    IList<ClinicalOrder> orderList = new List<ClinicalOrder>();

        //    var patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
        //    patients.ForEach(patient =>
        //    {
        //        var patientEpisodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patient.Id);
        //        if (patientEpisodes != null && patientEpisodes.Count > 0)
        //        {
        //            patientEpisodes.ForEach(episode =>
        //            {
        //                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
        //                {
        //                    var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
        //                    if (events != null && events.Count > 0)
        //                    {
        //                        events.ForEach(e =>
        //                        {
        //                            if (e.IsOrderAndStatus(statusId))
        //                            {
        //                                var order = patientRepository.GetOrder(e.EventId, patient.Id, Current.AgencyId);
        //                                if (order != null)
        //                                {
        //                                    var clinicalOrder = new ClinicalOrder();
        //                                    clinicalOrder.Id = e.EventId.ToString();
        //                                    clinicalOrder.Type = e.DisciplineTaskName;
        //                                    clinicalOrder.Number = order.OrderNumber.ToString();
        //                                    clinicalOrder.PatientName = patient.DisplayName.ToTitleCase();
        //                                    clinicalOrder.Physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId).DisplayName;
        //                                    clinicalOrder.Status = e.StatusName;
        //                                    clinicalOrder.CreatedDate = order.Created.ToShortDateString();
        //                                    orderList.Add(clinicalOrder);
        //                                }
        //                            }
        //                        });
        //                    }
        //                }
        //            });
        //        }
        //    });

        //    return orderList;
        //}

        public List<MissedVisit> GetAllMissedVisit(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            var missedVisitList = new List<MissedVisit>();
            var schedules = baseScheduleRepository.GetMissedVisitSchedulesLean(Current.AgencyId, branchCode, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { });
            if (schedules != null && schedules.Count > 0)
            {
                var userIds = schedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                schedules.ForEach(e =>
                {
                    var user = users.SingleOrDefault(u => u.Id == e.UserId);
                    var missedVisit = new MissedVisit();
                    missedVisit.PatientIdNumber = e.PatientIdNumber;
                    missedVisit.PatientName = e.PatientName;
                    missedVisit.Date = e.EventDate;
                    missedVisit.DisciplineTaskName = e.DisciplineTaskName;
                    missedVisit.UserName = user != null ? user.DisplayName : string.Empty;
                    missedVisitList.Add(missedVisit);
                });
            }
            return missedVisitList;

            //var missedVisitList = new List<MissedVisit>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchCode, startDate, endDate);
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    patientEpisodes.ForEach(patientEpisode =>
            //    {
            //        if (patientEpisode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && e.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && e.EventDate.ToDateTime().Date <= endDate.Date && e.EventDate.ToDateTime().Date >= startDate.Date && e.IsMissedVisit).ToList();
            //            if (events != null && events.Count > 0)
            //            {
            //                events.ForEach(e =>
            //                {
            //                    var missedVisit = new MissedVisit();
            //                    missedVisit.PatientIdNumber = patientEpisode.PatientIdNumber;
            //                    missedVisit.PatientName = patientEpisode.PatientName.ToUpperCase();
            //                    missedVisit.Date = e.EventDate.IsNotNullOrEmpty() && e.EventDate.IsValidDate() ? e.EventDate.ToDateTime() : DateTime.MinValue;
            //                    missedVisit.DisciplineTaskName = e.DisciplineTaskName;
            //                    if (!e.UserId.IsEmpty())
            //                    {
            //                        missedVisit.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
            //                    }
            //                    missedVisitList.Add(missedVisit);

            //                });
            //            }
            //        }
            //    });
            //}
            //return missedVisitList;
        }

        public IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var branchPhysicianOrders = new List<PhysicianOrder>();
            var schedules = baseScheduleRepository.GetScheduleTasksByStatusDisciplineAndRange(Current.AgencyId, branchCode, 0, startDate, endDate, status > 0 ? new int[] { status } : new int[] { }, new int[] { (int)DisciplineTasks.PhysicianOrder });
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersIds = schedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                branchPhysicianOrders = physicianOrderRepository.GetPhysicianOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
                if (branchPhysicianOrders != null && branchPhysicianOrders.Count > 0)
                {
                    //var userIds = schedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                    //var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                    var physicians = new List<AgencyPhysician>();
                    var physicianIds = branchPhysicianOrders.Where(r => !r.PhysicianId.IsEmpty()).Select(r => r.PhysicianId).Distinct().ToList();
                    if (physicianIds.Count > 0)
                    {
                        physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds) ?? new List<AgencyPhysician>();
                    }
                    branchPhysicianOrders.ForEach(po =>
                    {
                        var evnt = schedules.FirstOrDefault(e => e.Id == po.Id);
                        if (evnt != null)
                        {
                            po.DisplayName = evnt.PatientName;
                            po.StatusName = evnt.StatusName;
                        }

                        if (!po.PhysicianId.IsEmpty())
                        {
                            var physician = physicians.FirstOrDefault(u => u.Id == po.PhysicianId);
                            if (physician != null)
                            {
                                po.PhysicianName = physician.DisplayName;
                            }
                        }
                    });
                    branchPhysicianOrders = branchPhysicianOrders.OrderBy(o => o.DisplayName).ToList();
                }
            }
            return branchPhysicianOrders;
        }


        #endregion

        #region Schedule Reports

        public List<TaskLean> GetPatientScheduleEventsByDateRange(Guid patientId, DateTime fromDate, DateTime toDate)
        {
            var list = new List<TaskLean>();
            var scheduleEvents = baseScheduleRepository.GetScheduledEventsOnlyLean(Current.AgencyId, patientId, fromDate, toDate, new int[] { }, new int[] { }, true);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var tl = new TaskLean
                    {
                        PatientName = s.PatientName,
                        StatusName = s.StatusName,
                        DisciplineTaskName = s.DisciplineTaskName,
                        EventDateTimeRange = s.EventDateTimeRange,
                        EventDate = s.EventDate
                    };
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        tl.UserName = user.DisplayName;
                    }
                    if (ScheduleStatusFactory.AllNotStarted().Contains(s.Status) && s.EventDate.IsValid() && s.EventDate.Date <= DateTime.Now.Date)
                    {
                        tl.VisitDate = DateTime.MinValue;
                        tl.VisitDateTimeRange = string.Empty;
                    }
                    else
                    {
                        tl.VisitDate = s.VisitDate;
                        tl.VisitDateTimeRange = s.VisitDateTimeRange;
                    }
                    list.Add(tl);

                });
            }
            return list;

            //var outputEvents = new List<ScheduleEvent>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, patientId, fromDate, toDate);
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    patientEpisodes.ForEach(patientEpisode =>
            //    {
            //        if (patientEpisode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
            //            events = events.Where(e => e.EventDate.IsValidDate() 
            //                && e.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date 
            //                && e.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date 
            //                && e.EventDate.ToDateTime().Date >= fromDate.Date 
            //                && e.EventDate.ToDateTime().Date <= toDate.Date).ToList();
            //            if (events != null && events.Count > 0)
            //            {
            //                events.ForEach(e =>
            //                {
            //                    e.PatientName = patientEpisode.PatientName.ToUpperCase();
            //                    e.PatientIdNumber = patientEpisode.PatientIdNumber;
            //                    e.EventDate = e.EventDate.ToZeroFilled();
            //                    if (!e.UserId.IsEmpty())
            //                    {
            //                        e.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
            //                    }
            //                    if (e.StatusName.IsEqual("Not Yet Started") || e.StatusName.IsEqual("Not Yet Due"))
            //                    {
            //                        e.VisitDate = string.Empty;
            //                    }
            //                    outputEvents.Add(e);
            //                });
            //            }
            //        }
            //    });
            //}

            //return outputEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<UserVisit> GetUserScheduleEventsByDateRange(Guid userId, Guid branchCode, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            if (!userId.IsEmpty())
            {
                userVisits = baseScheduleRepository.GetUserVisitLean(Current.AgencyId, userId, from, to, 0, new int[] { }, false);
                if (userVisits != null && userVisits.Count > 0)
                {
                    userVisits.ForEach(v =>
                    {
                        if (ScheduleStatusFactory.AllNotStarted().Contains(v.Status) && v.ScheduleDate.IsValid() && v.ScheduleDate.Date <= DateTime.Now.Date)
                        {
                            v.VisitDate = DateTime.MinValue;
                        }
                    });
                }
            }
            return userVisits;

            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchCode, from, to);
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    patientEpisodes.ForEach(episode =>
            //    {
            //        if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
            //        {
            //            var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
            //                s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
            //               && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
            //               && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
            //               && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
            //                ).ToList();

            //            if (scheduledEvents != null && scheduledEvents.Count > 0)
            //            {
            //                scheduledEvents.ForEach(scheduledEvent =>
            //                {
            //                    if (scheduledEvent != null)
            //                    {
            //                        scheduledEvent.EndDate = episode.EndDate.ToDateTime();
            //                        scheduledEvent.StartDate = episode.StartDate.ToDateTime();

            //                        var userVisit = new UserVisit
            //                        {
            //                            Status = scheduledEvent.Status,
            //                            StatusName = scheduledEvent.StatusName,
            //                            PatientName = episode.PatientName,
            //                            TaskName = scheduledEvent.DisciplineTaskName,
            //                            UserDisplayName = UserEngine.GetName(scheduledEvent.UserId, Current.AgencyId),
            //                            VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty()
            //                           && scheduledEvent.VisitDate.IsValidDate()
            //                           && scheduledEvent.VisitDate.ToDateTime().Date <= DateTime.Now.Date ? scheduledEvent.VisitDate.ToZeroFilled() : "",
            //                            ScheduleDate = scheduledEvent.EventDate.IsNotNullOrEmpty()
            //                           && scheduledEvent.EventDate.IsValidDate() ? scheduledEvent.EventDate.ToZeroFilled() : ""
            //                        };

            //                        if (scheduledEvent.StatusName.IsEqual("Not Yet Started") || scheduledEvent.StatusName.IsEqual("Not Yet Due"))
            //                        {
            //                            userVisit.VisitDate = string.Empty;
            //                        }

            //                        userVisits.Add(userVisit);

            //                    }
            //                });
            //            }
            //        }
            //    });
            //}

            //return userVisits.OrderBy(v => v.PatientName).ToList();
        }

        public List<TaskLean> GetPastDueScheduleEvents(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var list = new List<TaskLean>();
            if (endDate.Date >= startDate && startDate.Date < DateTime.Now.Date)
            {
                endDate = endDate.Date >= DateTime.Now.Date ? DateTime.Now.AddDays(-1) : endDate;
                var scheduleEvents = baseScheduleRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { }, ScheduleStatusFactory.AllNoteNotYetStarted().ToArray(), false);
                //new List<ScheduleEvent>();new int[] { (int)ScheduleStatus.NoteNotYetDue, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OrderNotYetDue }
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    scheduleEvents.ForEach(s =>
                    {
                        var tl = new TaskLean
                        {
                            PatientIdNumber = s.PatientIdNumber,
                            PatientName = s.PatientName,
                            StatusName = s.StatusName,
                            DisciplineTaskName = s.DisciplineTaskName,
                            EventDateTimeRange = s.EventDateTimeRange,
                            VisitDateTimeRange = s.VisitDateTimeRange,
                            EventDate = s.EventDate
                        };
                        var user = users.SingleOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            tl.UserName = user.DisplayName;
                        }
                        list.Add(tl);
                    });
                }
            }
            return list.OrderBy(s => s.PatientName).ToList();

            //var episodeByBranch = patientRepository.GetEpisodeByBranch(branchId, Current.AgencyId);
            //var scheduleEvents = new List<ScheduleEvent>();
            //if (episodeByBranch != null && episodeByBranch.Count > 0)
            //{
            //    episodeByBranch.ForEach(e =>
            //    {
            //        if (e.StartDate.IsValidDate() && e.EndDate.IsValidDate() && e.Schedule.IsNotNullOrEmpty())
            //        {
            //            var schedules = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date <= e.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= e.StartDate.ToDateTime().Date && s.IsDeprecated == false && s.IsPastDue && !s.IsMissedVisit).ToList();
            //            if (schedules != null && schedules.Count > 0)
            //            {
            //                schedules.ForEach(s =>
            //                {
            //                    s.PatientName = e.PatientName.ToUpper();
            //                    s.PatientIdNumber = e.PatientIdNumber;

            //                    if (!s.UserId.IsEmpty())
            //                    {
            //                        s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    }
            //                    s.EventDate = s.EventDate.ToZeroFilled();

            //                    scheduleEvents.Add(s);
            //                });
            //            }
            //        }
            //    });
            //}
            //return scheduleEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<TaskLean> GetPastDueScheduleEventsByDiscipline(Guid branchId, string discipline, DateTime startDate, DateTime endDate)
        {
            var list = new List<TaskLean>();
            if (endDate.Date >= startDate && startDate.Date < DateTime.Now.Date)
            {
                endDate = endDate.Date >= DateTime.Now.Date ? DateTime.Now.AddDays(-1) : endDate;
                var scheduleEvents = baseScheduleRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { discipline }, new int[] { }, ScheduleStatusFactory.AllNoteNotYetStarted().ToArray(), false); //new int[] { (int)ScheduleStatus.NoteNotYetDue, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OrderNotYetDue }
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    scheduleEvents.ForEach(s =>
                    {
                        var tl = new TaskLean
                        {
                            PatientIdNumber = s.PatientIdNumber,
                            PatientName = s.PatientName,
                            StatusName = s.StatusName,
                            DisciplineTaskName = s.DisciplineTaskName,
                            EventDateTimeRange = s.EventDateTimeRange,
                            VisitDateTimeRange = s.VisitDateTimeRange,
                            EventDate = s.EventDate
                        };
                        var user = users.SingleOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            tl.UserName = user.DisplayName;
                        }
                        list.Add(tl);
                    });
                }
            }
            return list.OrderBy(s => s.PatientName).ToList();

            //var episodeByBranch = patientRepository.GetEpisodeByBranch(branchId, Current.AgencyId);
            //var scheduleEvents = new List<ScheduleEvent>();
            //if (episodeByBranch != null && episodeByBranch.Count > 0)
            //{
            //    episodeByBranch.ForEach(e =>
            //    {
            //        if (e.StartDate.IsValidDate() && e.EndDate.IsValidDate())
            //        {
            //            var schedules = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date <= e.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= e.StartDate.ToDateTime().Date && s.IsDeprecated == false && s.Discipline == discipline && s.IsPastDue && !s.IsMissedVisit).ToList();
            //            if (schedules != null && schedules.Count > 0)
            //            {
            //                schedules.ForEach(s =>
            //                {
            //                    s.PatientName = e.PatientName.ToUpper();
            //                    s.PatientIdNumber = e.PatientIdNumber;
            //                    if (!s.UserId.IsEmpty())
            //                    {
            //                        s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    }
            //                    s.EventDate = s.EventDate.ToZeroFilled();

            //                    scheduleEvents.Add(s);
            //                });
            //            }
            //        }
            //    });
            //}
            //return scheduleEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<TaskLean> GetScheduleEventsByDateRange(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var list = new List<TaskLean>();
            var scheduleEvents = baseScheduleRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { }, true);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var tl = new TaskLean
                    {
                        PatientIdNumber = s.PatientIdNumber,
                        PatientName = s.PatientName,
                        StatusName = s.StatusName,
                        DisciplineTaskName = s.DisciplineTaskName,
                        EventDateTimeRange = s.EventDateTimeRange,
                        VisitDateTimeRange = s.VisitDateTimeRange,
                        EventDate = s.EventDate
                    };
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        tl.UserName = user.DisplayName;
                    }
                    list.Add(tl);
                });
            }
            return list;

            //var episodeByBranch = patientRepository.GetEpisodeByBranch(branchId, Current.AgencyId);
            //var scheduleEvents = new List<ScheduleEvent>();
            //if (episodeByBranch != null)
            //{
            //    episodeByBranch.ForEach(e =>
            //    {
            //        if (e.StartDate.IsValidDate() && e.EndDate.IsValidDate() && e.Schedule.IsNotNullOrEmpty())
            //        {
            //            var schedules = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date <= e.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= e.StartDate.ToDateTime().Date && s.IsDeprecated == false).ToList();
            //            if (schedules != null && schedules.Count > 0)
            //            {
            //                schedules.ForEach(s =>
            //                {
            //                    s.PatientName = e.PatientName.ToUpperCase();
            //                    s.PatientIdNumber = e.PatientIdNumber;
            //                    if (!s.UserId.IsEmpty())
            //                    {
            //                        s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
            //                    }
            //                    scheduleEvents.Add(s);
            //                });
            //            }
            //        }
            //    });
            //}
            //return scheduleEvents;
        }

        public List<TaskLean> GetCaseManagerScheduleByBranch(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var list = new List<TaskLean>();
            var scheduleEvents = baseScheduleRepository.GetScheduleByBranchDateRangeAndStatusLean(Current.AgencyId, branchId, startDate, endDate, 0, ScheduleStatusFactory.CaseManagerStatus().ToArray(), false);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var tl = new TaskLean
                    {
                        PatientIdNumber = s.PatientIdNumber,
                        PatientName = s.PatientName,
                        StatusName = s.StatusName,
                        DisciplineTaskName = s.DisciplineTaskName,
                        EventDateTimeRange = s.EventDateTimeRange,
                        VisitDateTimeRange = s.VisitDateTimeRange,
                        EventDate = s.EventDate
                    };
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        tl.UserName = user.DisplayName;
                    }
                    list.Add(tl);
                });
            }
            return list;

            //var schedule = new List<ScheduleEvent>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchId, startDate, endDate);
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    patientEpisodes.ForEach(episode =>
            //    {
            //        if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
            //        {
            //            var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
            //              s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsMissedVisit == false
            //             && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
            //             && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
            //             && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.NoteReopened).ToString() || s.Status == ((int)ScheduleStatus.OasisReopened).ToString()) && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
            //              ).ToList();
            //            scheduleEvents.ForEach(scheduleEvent =>
            //            {
            //                scheduleEvent.PatientName = episode.PatientName;
            //                scheduleEvent.PatientIdNumber = episode.PatientIdNumber;
            //                scheduleEvent.EventDate = scheduleEvent.EventDate.ToZeroFilled();
            //                if (!scheduleEvent.UserId.IsEmpty())
            //                {
            //                    scheduleEvent.UserName = UserEngine.GetName(scheduleEvent.UserId, Current.AgencyId);
            //                }
            //                schedule.Add(scheduleEvent);
            //            });
            //        }
            //    });
            //}
            //return schedule.OrderBy(o => o.PatientName).ToList();
        }

        #endregion

        #region Employee Reports

        public List<User> GetEmployeeRoster(Guid branchCode, int status)
        {
            var users = userRepository.GetEmployeeRoster(Current.AgencyId, branchCode, status);
            return users.OrderBy(e => e.DisplayName).ToList();
        }

        public List<Birthday> GetEmployeeBirthdays(Guid branchCode, int status, int month)
        {
            var birthdays = new List<Birthday>();
            var users = userRepository.GetUsersByStatusAndDOB(Current.AgencyId, branchCode, status, month);
            if (users != null && users.Count > 0)
            {
                users.ForEach(user =>
                {
                    
                    if (user.DOB.IsValid())
                    {
                        var birthDay = new Birthday { Name = user.DisplayName, Date = user.DOB };
                        if (user.Profile != null)
                        {
                            birthDay.AddressLine1 = user.Profile.AddressLine1; 
                            birthDay.AddressLine2 = user.Profile.AddressLine2;
                            birthDay.AddressCity = user.Profile.AddressCity; 
                            birthDay.AddressStateCode = user.Profile.AddressZipCode;
                            birthDay.PhoneHome = user.Profile.PhoneHome;
                        }
                        birthdays.Add(birthDay);
                    }
                });
            }
            return birthdays;
        }

        public List<License> GetEmployeeExpiringLicenses(Guid branchCode, int status)
        {
            return userRepository.GetLicensesByBranchAndStatus(Current.AgencyId, branchCode, status).ToList();
        }

        public List<UserVisit> GetEmployeeScheduleByDateRange(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var userschedules = baseScheduleRepository.GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { }, true);
            if (userschedules != null && userschedules.Count > 0)
            {
                var userIds = userschedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                userschedules.ForEach(s =>
                {
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        s.UserDisplayName = user.DisplayName;
                    }
                    if (ScheduleStatusFactory.AllNotStarted().Contains(s.Status) && s.ScheduleDate.IsValid() && s.ScheduleDate.Date <= DateTime.Now.Date)
                    {
                        s.VisitDate = DateTime.MinValue;
                    }
                });
            }
            return userschedules.OrderBy(e => e.UserDisplayName).ToList();

            //var schedules = new List<UserVisit>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchId, startDate, endDate);

            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    var users = userRepository.GetAllUsers(Current.AgencyId) ?? new Dictionary<string, User>();
            //    patientEpisodes.ForEach(episode =>
            //    {

            //        if (episode.StartDate.IsValidDate() && episode.EndDate.IsValidDate() && episode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.UserId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date && !s.IsDeprecated ).ToList();
            //            if (scheduleList != null && scheduleList.Count > 0)
            //            {
            //                scheduleList.ForEach(s =>
            //                {
            //                    var user=users.ContainsKey(s.UserId.ToString())?users[s.UserId.ToString()]:new User();
            //                    schedules.Add(new UserVisit
            //                     {
            //                         ScheduleDate = s.EventDate.ToZeroFilled(),
            //                         VisitDate = s.StatusName.IsEqual("Not Yet Started") || s.StatusName.IsEqual("Not Yet Due") ? string.Empty : s.VisitDate.ToZeroFilled(),
            //                         PatientName = episode.PatientName.ToUpperCase(),
            //                         UserDisplayName = user!=null ?user.DisplayName:string.Empty,  // UserEngine.GetName(s.UserId, Current.AgencyId).ToUpperCase(),
            //                         StatusName = s.StatusName,
            //                         TaskName = Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask) ? ((DisciplineTasks)s.DisciplineTask).GetDescription() : ""
            //                     });
            //                });
            //            }
            //        }
            //    });
            //}

            //return schedules.OrderBy(e => e.UserDisplayName).ToList();
        }

        public List<UserPermissionViewData> GetEmployeePermissions(Guid branchId, int status)
        {
            var userPermissions = new List<UserPermissionViewData>();
            var users = userRepository.GetUsersByStatusAndPermissions(Current.AgencyId, branchId, status);
            Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
            if (users != null && users.Count > 0)
            {
                users.ForEach(user =>
                {

                    if (user.Permissions.IsNotNullOrEmpty())
                    {
                        foreach (var permission in permissions)
                        {
                            ulong id = (ulong)permission;
                            user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                            if (user.PermissionsArray.Contains(id.ToString()))
                            {
                                var up = new UserPermissionViewData();
                                string userId = user.CustomId.IsNotNullOrEmpty() ? " - " + user.CustomId : "";
                                up.Employee = user.DisplayName + userId;
                                up.Permission = permission.GetDescription();
                                userPermissions.Add(up);
                            }
                        }
                    }
                });
            }

            return userPermissions;
        }

        #endregion

        #region Statistical Reports

        public List<UserVisit> GetEmployeeVisistList(Guid userId, DateTime startDate, DateTime endDate)
        {
            var userSchedules = baseScheduleRepository.GetUserVisitLean(Current.AgencyId, userId, startDate, endDate, 0, new int[] { }, true);
            if (userSchedules != null && userSchedules.Count > 0)
            {
                userSchedules.ForEach(v =>
                {
                    if (ScheduleStatusFactory.AllNotStarted().Contains(v.Status) && v.ScheduleDate.IsValid() && v.ScheduleDate.Date <= DateTime.Now.Date)
                    {
                        v.VisitDate = DateTime.MinValue;
                    }
                });
            }
            return userSchedules;

            //var schedules = new List<UserVisit>();
            //var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            //if (patientEpisodes != null && patientEpisodes.Count > 0)
            //{
            //    patientEpisodes.ForEach(episode =>
            //    {
            //        if (episode.StartDate.IsValidDate() && episode.EndDate.IsValidDate() && episode.Schedule.IsNotNullOrEmpty())
            //        {
            //            var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.UserId.IsEmpty() && s.UserId == userId && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date && !s.IsDeprecated && s.Discipline != Disciplines.Claim.ToString()).ToList();
            //            if (scheduleList != null && scheduleList.Count > 0)
            //            {
            //                scheduleList.ForEach(s =>
            //                {
            //                    string statusName = s.StatusName;
            //                    if (s.IsMissedVisit)
            //                    {
            //                        MissedVisit mv = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
            //                        statusName = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription();
            //                    }
            //                    schedules.Add(new UserVisit
            //                    {
            //                        ScheduleDate = s.EventDate.ToZeroFilled(),
            //                        VisitDate = s.StatusName.IsEqual("Not Yet Started") || s.StatusName.IsEqual("Not Yet Due") ? string.Empty : s.VisitDate.ToZeroFilled(),
            //                        PatientName = episode.PatientName.ToUpperCase(),
            //                        StatusName = statusName,
            //                        TaskName = Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), s.DisciplineTask.ToString())).GetDescription() : ""
            //                    });
            //                });
            //            }
            //        }
            //    });
            //}

            //return schedules.ToList();
        }

        //Duplicate with the assessment service
        //public Assessment GetEpisodeAssessment(PatientEpisode currentEpisode, PatientEpisode previousEpisode)
        //{
        //    Assessment assessment = null;
        //    if (currentEpisode != null && currentEpisode.Schedule.IsNotNullOrEmpty())
        //    {
        //        var scheduleEvents = currentEpisode.Schedule.ToObject<List<ScheduleEvent>>();
        //        scheduleEvents.ForEach(e =>
        //        {
        //            if (e.IsStartofCareAssessment())
        //            {
        //                assessment = assessmentRepository.Get(e.EventId, "StartOfCare", Current.AgencyId);
        //                return;
        //            }
        //        });

        //        if (assessment == null && previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
        //        {
        //            var prevEpisodeEvents = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= currentEpisode.StartDate.AddDays(-5).Date) && (s.EventDate.ToDateTime().Date < currentEpisode.StartDate.Date) && !s.IsDeprecated && !s.IsMissedVisit).ToList();
        //            if (assessment == null)
        //            {
        //                prevEpisodeEvents.ForEach(e =>
        //                {
        //                    if (e.IsRecertificationAssessment())
        //                    {
        //                        assessment = assessmentRepository.Get(e.EventId, "Recertification", Current.AgencyId);
        //                        return;
        //                    }
        //                });

        //                if (assessment == null)
        //                {
        //                    prevEpisodeEvents.ForEach(e =>
        //                    {
        //                        if (e.IsResumptionofCareAssessment())
        //                        {
        //                            assessment = assessmentRepository.Get(e.EventId, "ResumptionOfCare", Current.AgencyId);
        //                            return;
        //                        }
        //                    });

        //                }
        //            }
        //        }
        //    }
        //    return assessment;
        //}

        public List<PatientAdmission> GetPatientAdmissionsByInternalSource(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            //var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId);
            // var users = agencyRepository.GetUserNames(Current.AgencyId);

            var admissionList = admissionRepository.GetPatientAdmissionsByDateRange(Current.AgencyId, branchCode, startDate, endDate);
            if (admissionList.IsNotNullOrEmpty())
            {
                var patientIds = admissionList.Where(s => !s.Id.IsEmpty()).Select(p => p.Id).Distinct().ToList();
                var admits = admissionRepository.GetPatientAdmits(Current.AgencyId, branchCode, patientIds);
                var userIds = admissionList.Where(s => !s.InternalReferral.IsEmpty()).Select(s => s.InternalReferral).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                var physicians = physicianRepository.GetPatientsPrimaryPhysician(Current.AgencyId, patientIds) ?? new List<AgencyPhysician>();
                admissionList.ForEach(admission =>
                {
                    if (admission.InsuranceId.IsNotNullOrEmpty() && admission.InsuranceId.IsInteger())
                    {
                        var insurance = InsuranceEngine.Get(admission.InsuranceId.ToInteger(), Current.AgencyId);
                        if (insurance != null)
                        {
                            admission.InsuranceName = insurance.Name;
                        }
                    }
                    if (!admission.PhysicianId.IsEmpty())
                    {
                        var physician = physicians.FirstOrDefault(p => p.Id == admission.PhysicianId);
                        if (physician != null)
                        {
                            admission.PhysicianName = physician.DisplayName;
                        }
                    }
                    if (!admission.InternalReferral.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == admission.InternalReferral);
                        if (user != null)
                        {
                            admission.InternalReferralName = user.DisplayName;
                        }
                    }
                    if (admits.ContainsKey(admission.Id.ToString()))
                    {
                        admission.Admit = admits[admission.Id.ToString()];
                    }
                });
            }
            return admissionList;
        }

        public List<PatientRoster> GetPatientByAdmissionYear(Guid branchId, int StatusId, int Year)
        {
            return admissionRepository.GetPatientByAdmissionYear(Current.AgencyId, branchId, StatusId, Year);
        }

        public List<PatientRoster> GetPatientByAdmissionUnduplicatedByDateRange(Guid branchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            return admissionRepository.GetPatientByAdmissionUnduplicatedByDateRange(Current.AgencyId, branchId, StatusId, StartDate, EndDate);
        }

        #endregion

        public IList<ReportLite> GetRequestedReports(Guid agencyId, int pageSize, int page)
        {
            IList<ReportLite> reports = null;
            int total = agencyRepository.GetTotalNumberOfReports(Current.AgencyId);
            if (total != 0)
            {
                reports = agencyRepository.GetReports(agencyId, pageSize, page);
                if (reports != null && reports.Count > 0)
                {
                    var users = new List<User>();
                    var userIds = reports.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (userIds != null && userIds.Count > 0)
                    {
                        users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                    }
                    reports.ForEach(r =>
                    {
                        r.Name = r.Status.IsEqual("Completed") && !r.AssetId.IsEmpty() ? string.Format("<a href=\"/Asset/{0}\">{1}</a>", r.AssetId, r.Name) : r.Name;
                        if (!r.UserId.IsEmpty())
                        {
                            var user = users.FirstOrDefault(u => u.Id == r.UserId);
                            if (user != null)
                            {
                                r.UserName = user.DisplayName;
                            }
                        }
                    });
                }
            }
            return reports ?? new List<ReportLite>();
        }

        public ReportViewData GetAllReport()
        {
            var viewData = new ReportViewData { Service = this.Service };
            var incomigReports = lookUpRepository.GetReportDescriptions();
            var outgoingReports = new List<ReportDescription>();
            var permissions = UserSessionEngine.ReportPermissions(Current.AgencyId, Current.UserId, Current.AcessibleServices, Current.SessionId);
            viewData.AvailableService = Current.AcessibleServices;
            var acessibleServices = (int)viewData.AvailableService;
            permissions.ForEach((key, value) =>
            {
                if (value.Keys.Contains((int)PermissionActions.ViewList))
                {
                    var viewService = 0;
                    value[(int)PermissionActions.ViewList].ForEach(s => { viewService |= s; });
                    viewService = viewService & acessibleServices;
                    if (viewService > 0)
                    {
                        var item = incomigReports.SingleOrDefault(i => i.Id == key);
                        if (item != null)
                        {
                            item.ViewService = viewService & item.Service;
                            if (item.Service > 0)
                            {
                                if (!item.IsOnclick)
                                {
                                    //var report = item.Link.Split('/').LastOrDefault();
                                    //item.Link = report;
                                }
                                //if (value.Keys.Contains((int)PermissionActions.Export))
                                //{
                                //    var exportService = 0;
                                //    value[(int)PermissionActions.ViewList].ForEach(s => { exportService |= s; });
                                //    exportService = exportService & acessibleServices & item.Service;
                                //    if (exportService > 0)
                                //    {
                                //        item.ExportService = exportService;
                                //    }
                                //}
                                outgoingReports.Add(item);
                            }
                        }

                    }

                }

            });
            viewData.Reports = outgoingReports.GroupBy(d => d.CategoryId).ToDictionary(k => k.Key, k => k.ToList());
            return viewData;
        }
    }
}
