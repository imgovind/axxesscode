﻿namespace Axxess.AgencyManagement.Application.Services
{
    #region

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Infrastructure.Web.Base;
    using Axxess.Log.Enums;
    using Axxess.LookUp.Domain;

    using StructureMap.Query;

    #endregion

    public abstract class MediatorService<T, E>
        where T : ITask, new() where E : CarePeriod, new()
    {
        #region Fields

        protected ILookUpService lookUpService;
        protected IPatientService patientService;
        protected PatientProfileService baseProfileService;
        protected IMessageService messageService;
        private readonly AssessmentService<T, E> baseAssessmentService;
        private readonly EpisodeService<E> baseEpisodeService;
        private readonly NoteService<T, E> baseNoteService;
        private readonly PlanOfCareService<T, E> basePlanOfCareService;
        private readonly TaskService<T, E> baseScheduleService;
        private readonly PhysicianOrderService<T, E> basePhysicianOrderService;
        private readonly CommunicationNoteService<T, E> baseCommunicationNoteService;
        private readonly IncidentAccidentService<T, E> baseIncidentAccidentService;
        private readonly InfectionService<T, E> baseInfectionService;

        #endregion

        #region Constructors and Destructors

        public MediatorService(
            IMessageService messageService,
            EpisodeService<E> baseEpisodeService, 
            TaskService<T, E> scheduleService, 
            AssessmentService<T, E> baseAssessmentService, 
            PhysicianOrderService<T, E> physicianOrderService, 
            PlanOfCareService<T, E> planOfCareService, 
            NoteService<T, E> noteService, 
            CommunicationNoteService<T, E> baseCommunicationNoteService, 
            InfectionService<T, E> baseInfectionService, 
            IncidentAccidentService<T, E> baseIncidentAccidentService)
        {
            this.baseEpisodeService = baseEpisodeService;
            this.basePhysicianOrderService = physicianOrderService;
            this.baseScheduleService = scheduleService;
            this.basePlanOfCareService = planOfCareService;
            this.baseNoteService = noteService;
            this.baseAssessmentService = baseAssessmentService;
            this.baseCommunicationNoteService = baseCommunicationNoteService;
            this.baseInfectionService = baseInfectionService;
            this.baseIncidentAccidentService = baseIncidentAccidentService;
            this.messageService = messageService;
        }

        #endregion

        #region Properties

        protected AgencyServices Service { get; set; }

        #endregion

        #region Public Methods and Operators

        public JsonViewData BulkUpdate(List<string> CustomValue, string CommandType)
        {
            var viewData = new JsonViewData(false, "At least one item must be selected.");
            if ( CommandType.IsNotNullOrEmpty()&& CommandType.IsEqual("Approve") || CommandType.IsEqual("Return"))
            {
                if (Current.HasRight(this.Service, ParentPermission.QA, CommandType.IsEqual("Approve") ? PermissionActions.Approve : PermissionActions.Return))
                {
                    if (CustomValue != null && CustomValue.Count > 0)
                    {
                        viewData = new JsonViewData(false, "The item(s) you selected could not be " + (CommandType == "Approve" ? CommandType.ToLower() + "d" : CommandType.ToLower() + "ed") + ".");

                        int total = CustomValue.Count;
                        int count = 0;
                        CustomValue.ForEach(
                            v =>
                            {
                                var infos = v.Split('|');

                                if (infos.Length == 4 && infos[0].IsGuid() && infos[1].IsGuid() && infos[2].IsGuid() && infos[3].IsInteger())
                                {
                                    var scheduleEvent = this.baseScheduleService.GetScheduleTask(infos[1].ToGuid(), infos[2].ToGuid());
                                    if (scheduleEvent != null && scheduleEvent.DisciplineTask == infos[3].ToInteger())
                                    {
                                        if (scheduleEvent.IsMissedVisit)
                                        {
                                            if (this.baseScheduleService.ProcessMissedVisitNotes(scheduleEvent, CommandType, null))
                                            {
                                                count++;
                                            }
                                        }
                                        else
                                        {
                                            var eventType = scheduleEvent.TypeOfEvent();
                                            if (eventType.IsNotNullOrEmpty())
                                            {
                                                switch (eventType)
                                                {
                                                    case "Notes":
                                                        var notesViewData = this.baseNoteService.ProcessNotes(scheduleEvent, CommandType, null);
                                                        if (notesViewData.isSuccessful)
                                                        {
                                                            count++;
                                                        }
                                                        break;
                                                    case "OASIS":
                                                        if (Enum.IsDefined(typeof(DisciplineTasks), infos[3].ToInteger()))
                                                        {
                                                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), infos[3].ToInteger())).ToString();
                                                            if (CommandType == "Approve")
                                                            {
                                                                var oasisViewData = this.baseAssessmentService.UpdateAssessmentStatus(scheduleEvent, (int)ScheduleStatus.OasisCompletedExportReady, null);
                                                                if (oasisViewData.isSuccessful)
                                                                {
                                                                    DischargePatientByOASIS(scheduleEvent.Id, scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.VisitDate > scheduleEvent.EventDate ? scheduleEvent.VisitDate : scheduleEvent.EventDate, scheduleEvent.DisciplineTask, viewData);
                                                                    count++;
                                                                    viewData.IsExportOASISRefresh = Service != AgencyServices.PrivateDuty;
                                                                }
                                                            }
                                                            else if (CommandType == "Return")
                                                            {
                                                                var data = this.baseAssessmentService.UpdateAssessmentStatus(scheduleEvent, (int)ScheduleStatus.OasisReturnedForClinicianReview, null);
                                                                if (data.isSuccessful)
                                                                {
                                                                    count++;
                                                                }
                                                            }
                                                            else if (CommandType == "Print")
                                                            {
                                                                scheduleEvent.InPrintQueue = false;
                                                                if (this.baseScheduleService.UpdateScheduleTask(scheduleEvent))
                                                                {
                                                                    count++;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case "PhysicianOrder":
                                                        var physicianOrderViewData = this.basePhysicianOrderService.ProcessPhysicianOrder(scheduleEvent, CommandType, null);
                                                        if (physicianOrderViewData.isSuccessful)
                                                        {
                                                            viewData.IsOrdersToBeSentRefresh = true;
                                                            count++;
                                                        }
                                                        break;
                                                    case "PlanOfCare":
                                                        var planOfCareViewData = this.basePlanOfCareService.UpdatePlanofCareStatus(scheduleEvent, CommandType, null);
                                                        if (planOfCareViewData.isSuccessful)
                                                        {
                                                            viewData.IsOrdersToBeSentRefresh = true;
                                                            count++;
                                                        }
                                                        break;
                                                    case "Incident":
                                                        var incidentAccidentViewData = this.baseIncidentAccidentService.ProcessIncidents(scheduleEvent, CommandType, null);
                                                        if (incidentAccidentViewData.isSuccessful)
                                                        {
                                                            viewData.IsIncidentAccidentRefresh = true;
                                                            count++;
                                                        }
                                                        break;
                                                    case "Infection":
                                                        var infectionViewData = this.baseInfectionService.ProcessInfections(scheduleEvent, CommandType, null);
                                                        if (infectionViewData.isSuccessful)
                                                        {
                                                            count++;
                                                        }
                                                        break;
                                                    case "CommunicationNote":
                                                        var communicationNoteViewData = this.baseCommunicationNoteService.ProcessCommunicationNotes(scheduleEvent, CommandType, null);
                                                        if (communicationNoteViewData.isSuccessful)
                                                        {
                                                            viewData.IsCommunicationNoteRefresh = true;
                                                            count++;
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        if (count > 0)
                        {
                            viewData.isSuccessful = true;
                            if (count == total)
                            {
                                viewData.errorMessage = string.Format("All ({0}) items were updated successfully.", count);
                            }
                            else if (count < total)
                            {
                                viewData.errorMessage = string.Format("{0} out of {1} items were updated successfully. Try them again.", count, total);
                            }
                            viewData.IsCaseManagementRefresh = true;
                            viewData.IsDataCentersRefresh = !viewData.IsCenterRefresh;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "At least one item must be selected.";
                    }
                }
                else
                {
                    viewData.errorMessage = "You have no enough permission for this action.";
                }
            }
            else
            {
                viewData.errorMessage = "The action is not identified. Try again.";
            }
            return viewData;
        }

        public bool DeleteScheduleTaskAsset(Guid patientId, Guid eventId, Guid assetId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var result = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty() && !assetId.IsEmpty())
            {
                var scheduleEvent = this.baseScheduleService.GetScheduleTask(patientId, eventId);
                if (scheduleEvent != null)
                {
                    var oldAsset = scheduleEvent.Asset;
                    if (this.baseScheduleService.DeleteScheduleTaskAsset(scheduleEvent, assetId))
                    {
                        if (DisciplineTaskFactory.EpisodeAllAssessments(true).Contains(scheduleEvent.DisciplineTask) || (DisciplineTaskFactory.TransferDischargeOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask) && scheduleEvent.Version == 2))
                        {
                            if (this.baseAssessmentService.DeleteOnlyWoundCareAsset(patientId, eventId, assetId))
                            {
                                result = true;
                                AssetHelper.Delete(assetId);
                            }
                            else
                            {
                                result = false;
                                scheduleEvent.Asset = oldAsset;
                                this.baseScheduleService.UpdateScheduleTask(scheduleEvent);
                            }
                        }
                        else
                        {
                            result = true;
                            AssetHelper.Delete(assetId);
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid patientId)
        {
            var profile = this.baseProfileService.GetMedicationProfilePrint(patientId);
            if (profile != null)
            {
                if (profile.PatientProfile != null)
                {
                    this.baseAssessmentService.SetEpisodeInfoForMedicationProfilePrint(patientId, profile);
                }
            }
            return profile;
        }

        public MedicationProfileViewData GetMedicationProfileViewDataWithDiagnosis(Guid patientId)
        {
            var viewData = this.baseProfileService.GetMedicationProfileViewDataWithOutDiagnosis(patientId);
            if (viewData != null)
            {
                this.baseAssessmentService.SetAssessmentAndEpisodeInfoForMedicationProfile(patientId, viewData);
                var mainPermissions = Current.Permissions.GetOrDefault((int)ParentPermission.MedicationProfile);
                if (mainPermissions != null)
                {
                    if (viewData.MedicationProfile != null)
                    {
                        viewData.MedicationProfile.IsUserCanEdit = mainPermissions.GetOrDefault((int)PermissionActions.Edit, new List<int>()).Contains((int)Service);
                        viewData.MedicationProfile.IsUserCanDelete = mainPermissions.GetOrDefault((int)PermissionActions.Delete, new List<int>()).Contains((int)Service);
                        viewData.MedicationProfile.IsUserCanDiscontinue = mainPermissions.GetOrDefault((int)PermissionActions.Deactivate, new List<int>()).Contains((int)Service);
                        viewData.MedicationProfile.IsUserCanReactivate = mainPermissions.GetOrDefault((int)PermissionActions.Reactivate, new List<int>()).Contains((int)Service);
                    }
                    viewData.IsUserCanAdd = mainPermissions.GetOrDefault((int)PermissionActions.Add, new List<int>()).Contains((int)Service);
                    viewData.IsUserCanSign = mainPermissions.GetOrDefault((int)PermissionActions.Sign, new List<int>()).Contains((int)Service);
                    viewData.IsUserCanPrint = mainPermissions.GetOrDefault((int)PermissionActions.Print, new List<int>()).Contains((int)Service);
                }
            }
            return viewData;
        }

        public MedicationProfileViewData GetMedicationProfileViewDataWithDiagnosisForSign(Guid patientId)
        {
            var viewData = this.baseProfileService.GetMedicationProfileViewDataWithOutDiagnosisForSign(patientId);
            if (viewData != null)
            {
                this.baseAssessmentService.SetAssessmentAndEpisodeInfoForMedicationProfile(patientId, viewData);
            }
            return viewData;
        }

        public PatientChartsViewData GetPatientChartsDataWithSchedules(Guid patientId, int status)
        {
            Profile profile = null;
            var viewData = new PatientChartsViewData();
            var selectedViewData = this.baseProfileService.GetPatientChartsDataWithOutSchedules(patientId, status, ref profile);
            if (selectedViewData != null)
            {
                viewData.SelectionViewData = selectedViewData;
                if (profile != null)
                {
                    var patient = patientService.GetPatientOnly(selectedViewData.CurrentPatientId);
                    if (patient != null)
                    {
                        patient.RequestedService = this.Service;
                        patient.Profile = profile;
                        this.baseProfileService.SetPatientSnapshotInfo(patient, true, true, true);
                        viewData.PatientScheduleEvent.Patient = patient;
                    }
                }
                if (viewData.PatientScheduleEvent != null)
                {
                    viewData.PatientScheduleEvent.Service = Service;
                    viewData.PatientScheduleEvent.DateFilterType = "ThisEpisode";
                    viewData.PatientScheduleEvent.DisciplineFilterType = "All";
                    this.baseScheduleService.CurrentEpisodePatientWithScheduleEvent(viewData.PatientScheduleEvent, viewData.SelectionViewData.CurrentPatientId, "All");
                }
                else
                {
                    // TODO: This will fail
                    viewData.PatientScheduleEvent.Service = Service;
                }
            }
            else
            {
                viewData.PatientScheduleEvent.Service = Service;
            }

            return viewData;
        }

        public PatientScheduleEventViewData GetScheduleCenterData(Guid patientId)
        {
            var viewData = new PatientScheduleEventViewData();
            viewData.Service = this.Service;
            var patient = this.baseProfileService.GetPatientSnapshotInfo(patientId, true, true, true);
            if (patient != null)
            {
                patient.RequestedService = this.Service;
                viewData.Patient = patient;
                viewData.DateFilterType = "ThisEpisode";
                viewData.DisciplineFilterType = "All";
                baseScheduleService.CurrentEpisodePatientWithScheduleEvent(viewData, patientId, "All");
            }
            return viewData;
        }

        public PlanofCare GetPlanOfCareWithAssessment(Guid patientId, Guid eventId)
        {
            var planofCare = this.basePlanOfCareService.GetPlanOfCareOnly(patientId, eventId);
            if (planofCare != null)
            {
                var questionData = this.baseAssessmentService.GetQuestions(planofCare.AssessmentId);
                if (questionData != null)
                {
                    planofCare.Questions = this.basePlanOfCareService.Get485FromAssessment(questionData.PatientId, questionData.Question);
                }
            }
            return planofCare;
        }

        public T GetScheduledEventForDetail(Guid patientId, Guid eventId)
        {
            var scheduleEvent = this.baseScheduleService.GetScheduleTask(patientId, eventId);
            return GetScheduledEventForDetail(scheduleEvent);
        }

        public T GetScheduledEventForDetail(T scheduleEvent)
        {
            if (scheduleEvent != null)
            {
                if (scheduleEvent.PatientName.IsNullOrEmpty())
                {
                    var profile = this.baseProfileService.GetProfileJsonByColumns(scheduleEvent.PatientId, "FirstName", "LastName", "MiddleInitial", "PatientIdNumber", "AgencyLocationId");
                    if (profile != null)
                    {

                        scheduleEvent.LocationId = profile["AgencyLocationId"].ToGuid();
                        var middleInitial = profile["MiddleInitial"];
                        scheduleEvent.PatientName = profile["LastName"].ToUpperCase() + ", " + profile["FirstName"].ToUpperCase() + (middleInitial.IsNotNullOrEmpty() ? " " + middleInitial.ToInitial() : string.Empty);
                        scheduleEvent.PatientIdNumber = profile["PatientIdNumber"];
                    }
                }
                if (scheduleEvent.Asset.IsNotNullOrEmpty())
                {
                    scheduleEvent.Assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                }
                //This condition determines if the view displays a drop down to change who the note is assigned to.
                //So if this condition is 
                //if (scheduleEvent.IsComplete)
                //{
                    scheduleEvent.UserName = UserEngine.GetName(scheduleEvent.UserId, Current.AgencyId);
                //}
                var episodeRange = this.baseEpisodeService.GetEpisodeDateRange(scheduleEvent.PatientId, scheduleEvent.EpisodeId);
                if (episodeRange != null)
                {
                    scheduleEvent.StartDate = episodeRange.StartDate;
                    scheduleEvent.EndDate = episodeRange.EndDate;
                }
                var discipline = lookUpService.GetDisciplineTask(scheduleEvent.DisciplineTask);
                if (discipline != null)
                {
                    scheduleEvent.IsTaskChangeable = discipline.IsTypeChangeable;
                    if (discipline.FooterSettings.IsNotNullOrEmpty())
                    {
                        var footerSettings = discipline.FooterSettings.ToObject<FooterSettings>();
                        if (footerSettings != null)
                        {
                            scheduleEvent.CanAddSupplies = footerSettings.CanAddSupplies;
                        }
                    }
                }
                if (scheduleEvent.Discipline.IsEqual(Disciplines.Orders.ToString()))
                {
                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
                    {
                        var order = this.basePhysicianOrderService.GetPhysicianOrder(scheduleEvent.PatientId, scheduleEvent.Id);
                        if (order != null)
                        {
                            scheduleEvent.PhysicianId = order.PhysicianId;
                        }
                    }
                    else if (DisciplineTaskFactory.POC().Contains(scheduleEvent.DisciplineTask))
                    {
                        var planofcare = this.basePlanOfCareService.GetPlanOfCareOnly(scheduleEvent.PatientId, scheduleEvent.Id);
                        if (planofcare != null)
                        {
                            scheduleEvent.PhysicianId = planofcare.PhysicianId;
                        }
                    }
                    else
                    {
                        scheduleEvent.PhysicianId = GetScheduledEventForDetailAppSpecific(scheduleEvent.PatientId, scheduleEvent.Id, scheduleEvent.DisciplineTask);
                    }
                }
            }
            return scheduleEvent;
        }

        public JsonViewData OASISSubmit(Guid Id, Guid patientId, string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var clientAction = ClientAction.Update;
            if (actionType.IsNotNullOrEmpty())
            {
                var scheduleEvent = this.baseScheduleService.GetScheduleTask(patientId, Id);
                if (scheduleEvent != null)
                {
                    if (actionType == "Approve")
                    {
                        clientAction = ClientAction.Approve;
                        viewData = this.baseAssessmentService.UpdateAssessmentStatus(scheduleEvent, (int)ScheduleStatus.OasisCompletedExportReady, string.Empty);
                        if (viewData.isSuccessful)
                        {
                            DischargePatientByOASIS(Id, patientId, scheduleEvent.EpisodeId, scheduleEvent.VisitDate > scheduleEvent.EventDate ? scheduleEvent.VisitDate : scheduleEvent.EventDate, scheduleEvent.DisciplineTask, viewData);
                            viewData.IsDataCentersRefresh = true;
                            
                            viewData.IsExportOASISRefresh = Service != AgencyServices.PrivateDuty;
                        }
                    }
                    else if (actionType == "Return")
                    {
                        clientAction = ClientAction.Return;
                        viewData = this.baseAssessmentService.UpdateAssessmentStatus(scheduleEvent, (int)ScheduleStatus.OasisReturnedForClinicianReview, reason);
                        if (viewData.isSuccessful)
                        {
                            viewData.IsDataCentersRefresh = true;
                        }
                    }
                    else if (actionType == "ReOpen")
                    {
                        clientAction = ClientAction.ReOpen;
                        viewData = this.baseAssessmentService.UpdateAssessmentStatus(scheduleEvent, (int)ScheduleStatus.OasisReopened, string.Empty);
                        if (viewData.isSuccessful)
                        {
                            viewData.IsDataCentersRefresh = true;
                        }
                    }
                }
                viewData.Service = Service.ToString();
                viewData.ServiceId = (int)Service;
            }
            viewData.errorMessage = MessageFactory.CreateMessage<Assessment>(clientAction, viewData.isSuccessful);
            return viewData;
        }

        public JsonViewData OASISSubmitOnly(AssessmentSubmitArguments arguments)
        {
            var viewData = new JsonViewData(false);
            string message = "Your Assessment could not be submitted.";
            var rules = new List<Validation>();
                if (arguments.OasisValidationType.IsNotNullOrEmpty())
                {
                    if (!arguments.Id.IsEmpty() && !arguments.EpisodeId.IsEmpty() && !arguments.PatientId.IsEmpty())
                    {
                        var episode = this.baseEpisodeService.GetEpisodeOnly(arguments.EpisodeId, arguments.PatientId);
                        if (episode != null)
                        {
                            rules.Add(new Validation(() => arguments.ValidationClinician.IsNullOrEmpty(), "User Signature can't be empty.\n"));
                            rules.Add(new Validation(() => arguments.ValidationClinician.IsNotNullOrEmpty() && !ServiceHelper.IsSignatureCorrect(Current.UserId, arguments.ValidationClinician), "User Signature is not correct.\n"));
                            rules.Add(new Validation(() => !arguments.ValidationSignatureDate.IsValid() || !arguments.ValidationSignatureDate.IsBetween(episode.StartDate, DateTime.Now),
                                    string.Format("The signature date must be within {0} - {1}.\n", episode.StartDateFormatted, DateTime.Now.ToZeroFilled())));
                            if (arguments.OasisValidationType == AssessmentType.StartOfCare.ToString() || arguments.OasisValidationType == AssessmentType.ResumptionOfCare.ToString() || arguments.OasisValidationType == AssessmentType.Recertification.ToString() || arguments.OasisValidationType == AssessmentType.FollowUp.ToString())
                            {
                                rules.Add(new Validation(() => arguments.TimeIn.IsNullOrEmpty(), "Time-In can't be empty. \n"));
                                rules.Add(new Validation(() => arguments.TimeOut.IsNullOrEmpty(), "Time-Out can't be empty. \n"));
                            }
                            var entityValidator = new EntityValidator(rules.ToArray());
                            entityValidator.Validate();
                            if (entityValidator.IsValid)
                            {
                                var scheduleEvent = this.baseScheduleService.GetScheduleTask(arguments.PatientId, arguments.Id);
                                if (scheduleEvent != null)
                                {
                                    viewData = this.baseAssessmentService.UpdateAssessmentStatusForSubmit(
                                        scheduleEvent, (int)ScheduleStatus.OasisCompletedPendingReview, string.Format("Electronically Signed by: {0}", Current.UserFullName), arguments.ValidationSignatureDate, arguments.TimeIn, arguments.TimeOut);
                                    if (viewData.isSuccessful)
                                    {
                                        message = "Your Assessment was submitted successfully.";
                                        if (Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA))
                                        {
                                            DischargePatientByOASIS(arguments.Id, arguments.PatientId, arguments.EpisodeId, scheduleEvent.VisitDate > scheduleEvent.EventDate ? scheduleEvent.VisitDate : scheduleEvent.EventDate, scheduleEvent.DisciplineTask, viewData);
                                        }
                                        viewData.Service = Service.ToString();
                                        viewData.ServiceId = (int)Service;
                                        viewData.UserIds = new List<Guid> { scheduleEvent.UserId }; 
                                        viewData.PatientId = scheduleEvent.PatientId;
                                        viewData.Id = scheduleEvent.Id;
                                        viewData.errorMessage = message;
                                    }
                                    else
                                    {
                                        viewData.errorMessage = message;
                                    }
                                }
                            }
                            else
                            {
                                viewData.errorMessage = entityValidator.Message;
                            }
                        }
                        else
                        {
                            viewData.errorMessage = message;
                        }
                    }
                    else
                    {
                        viewData.errorMessage = message;
                    }
                }
                else
                {
                    viewData.errorMessage = message;
                }

            return viewData;
        }

        public JsonViewData Reopen(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var scheduleEvent = this.baseScheduleService.GetScheduleTask(patientId, eventId);
            if (scheduleEvent != null)
            {
                if (scheduleEvent.IsNote())
                {
                    viewData = this.baseNoteService.ReopenVisitNote(scheduleEvent);
                }
                else if (DisciplineTaskFactory.AllAssessments(true).Contains(scheduleEvent.DisciplineTask))
                {
                    viewData = this.baseAssessmentService.Reopen(scheduleEvent);
                }
                else
                {
                    switch (scheduleEvent.DisciplineTask)
                    {
                        case (int)DisciplineTasks.HCFA485:
                        case (int)DisciplineTasks.NonOasisHCFA485:
                        case (int)DisciplineTasks.HCFA485StandAlone:
                            viewData = this.basePlanOfCareService.Reopen(scheduleEvent);
                            break;
                        case (int)DisciplineTasks.PhysicianOrder:
                            viewData = this.basePhysicianOrderService.ReopenPhysicianOrder(scheduleEvent);
                            break;
                        case (int)DisciplineTasks.CommunicationNote:
                            viewData = this.baseCommunicationNoteService.ReopenCommunicationNote(scheduleEvent);
                            break;
                        case (int)DisciplineTasks.IncidentAccidentReport:
                            viewData = this.baseIncidentAccidentService.ReopenIncidentReport(scheduleEvent);
                            break;
                        case (int)DisciplineTasks.InfectionReport:
                            viewData = this.baseInfectionService.ReopenInfectionReport(scheduleEvent);
                            break;
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ToggleTaskStatus(Guid patientId, Guid eventId, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Task could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            var task = this.baseScheduleService.GetScheduleTask(patientId, eventId);
            if (task != null)
            {
                if (task.IsNote())
                {
                    viewData = this.baseNoteService.ToggleVisitNote(task, isDeprecated);
                }
                else if (DisciplineTaskFactory.AllAssessments(true).Contains(task.DisciplineTask))
                {
                    viewData = this.baseAssessmentService.ToggleAssessment(task, isDeprecated);
                }
                else
                {
                    switch (task.DisciplineTask)
                    {
                        case (int)DisciplineTasks.HCFA485:
                        case (int)DisciplineTasks.NonOasisHCFA485:
                        case (int)DisciplineTasks.HCFA485StandAlone:
                            viewData = this.basePlanOfCareService.TogglePlanofCare(task, isDeprecated);
                            break;
                            // viewData = planOfCareService.TogglePlanofCare(task.PatientId, task.Id, isDeprecated);
                            // break;
                        case (int)DisciplineTasks.PhysicianOrder:
                            viewData = this.basePhysicianOrderService.TogglePhysicianOrder(task, isDeprecated);
                            break;
                        case (int)DisciplineTasks.IncidentAccidentReport:
                            viewData = this.baseIncidentAccidentService.ToggleIncident(task, isDeprecated);
                            break;
                        case (int)DisciplineTasks.InfectionReport:
                            viewData = this.baseInfectionService.ToggleInfection(task, isDeprecated);
                            break;
                        case (int)DisciplineTasks.CommunicationNote:
                            viewData = this.baseCommunicationNoteService.ToggleCommunicationNote(task, isDeprecated);
                            break;
                        default:
                            viewData = UpdateScheduleEntityAppSpecific(task, isDeprecated);
                            break;
                    }
                } 
            }
            return viewData;
        }

        public abstract JsonViewData UpdateScheduleEventDetails(T scheduleEvent, HttpFileCollectionBase httpFiles);

        public JsonViewData AddCommunicationNote(CommunicationNote communicationNote)
        {
            var viewData = baseCommunicationNoteService.AddCommunicationNote(communicationNote);
            if (viewData.isSuccessful)
            {
                this.SendCommunicationNoteMessage(communicationNote, viewData);
            }
            return viewData;
        }

        public JsonViewData UpdateCommunicationNote(CommunicationNote communicationNote)
        {
            var viewData = baseCommunicationNoteService.UpdateCommunicationNote(communicationNote);
            if (viewData.isSuccessful)
            {
                this.SendCommunicationNoteMessage(communicationNote, viewData);
            }
            return viewData;
        }

        public FileJsonViewData GetCarePlan(Guid patientId, Guid eventId)
        {
            var viewData = new FileJsonViewData { isSuccessful = false, errorMessage = "No Care Plan found for this period." };
            var scheduleEvent = baseScheduleService.GetScheduleTask(patientId, eventId);
            if (scheduleEvent != null)
            {
                if (DisciplineTaskFactory.SkilledNurseSharedFile().Contains(scheduleEvent.DisciplineTask))
                {
                    var assessment = EpisodeAssessmentHelperFactory<E, T>.GetEpisodeAssessment(scheduleEvent.EpisodeId, patientId, scheduleEvent.EventDate);
                    if (assessment != null)
                    {
                        var planOfCare = basePlanOfCareService.GetPlanOfCareByAssessment(assessment.EpisodeId, assessment.Id);
                        if (planOfCare != null && !planOfCare.IsDeprecated)
                        {
                            viewData.isSuccessful = true;
                            viewData.File = FileGenerator.Pdf(new PlanOfCarePdf(basePlanOfCareService.GetPlanOfCarePrint(planOfCare)), "PlanOfCare");
                        }
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HHAideVisit)
                {
                    var poc = baseNoteService.GetCarePlanBySelectedEpisode(patientId, scheduleEvent.EpisodeId, DisciplineTasks.HHAideCarePlan);
                    if (poc != null)
                    {
                        viewData.isSuccessful = true;
                        viewData.File = FileGenerator.GetPrintFile(baseNoteService.GetVisitNotePrint(poc), new FileStreamResult(new MemoryStream(), "application/pdf"));
                    }
                }
            }
            return viewData;
        }

        #endregion

        #region Methods

        private void SendCommunicationNoteMessage(CommunicationNote communicationNote, JsonViewData viewData)
        {
            if (communicationNote.SendAsMessage)
            {
                var message = baseCommunicationNoteService.CreateCommunicationNoteMessage(communicationNote);
                if (message != null)
                {
                    if (messageService.SendMessage(message, null).isSuccessful)
                    {
                        viewData.errorMessage = "Communication note successfully saved and sent to recipients";
                    }
                    else
                    {
                        viewData.errorMessage = "Communication note saved but notification could not be sent to the recipients";
                    }
                }
            }
        }

        protected abstract void DischargePatientByOASIS(Guid Id, Guid patientId, Guid episodeId, DateTime possibleDischargeDate, int assessmentType, JsonViewData viewData);

        protected abstract Guid GetScheduledEventForDetailAppSpecific(Guid patientId, Guid eventId, int disciplineTaskId);

        protected bool ProcessEditDetail(T schedule)
        {
            bool result;
            var type = schedule.DisciplineTask;
            if (DisciplineTaskFactory.AllAssessments(true).Contains(schedule.DisciplineTask))
            {
                result = this.baseAssessmentService.UpdateAssessmentForDetail(schedule);
            }
            else if (schedule.IsNote())
            {
                result = this.baseNoteService.UpdateVisitNoteFromTaskDetail(schedule, type);
            }
            else if (schedule.IsPlanOfCare())
            {
                result = this.basePlanOfCareService.UpdatePlanOfCareForDetail(schedule);
            }
            else
            {
                switch (type)
                {
                    case (int)DisciplineTasks.PhysicianOrder:
                        {
                            result = this.basePhysicianOrderService.UpdatePhysicianOrderForScheduleDetail(schedule);
                        }
                        break;
                    case (int)DisciplineTasks.IncidentAccidentReport:
                        {
                            result = this.baseIncidentAccidentService.UpdateIncidentForScheduleDetail(schedule);
                        }
                        break;
                    case (int)DisciplineTasks.InfectionReport:
                        {
                            result = this.baseInfectionService.UpdateInfectionForScheduleDetail(schedule);
                        }
                        break;
                    case (int)DisciplineTasks.CommunicationNote:
                        {
                            result = this.baseCommunicationNoteService.UpdateCommunicationNoteForScheduleDetail(schedule);
                        }
                        break;
                    default:
                        result = ProcessEditDetailAppSpecific(schedule);
                        break;
                }
            }
            return result;
        }

        protected abstract bool ProcessEditDetailAppSpecific(T schedule);

        protected JsonViewData ProcessSchedule(int table, E episode, List<T> scheduleEvents)
        {
            JsonViewData viewData;
            switch (table)
            {
                case (int)Tables.PatientVisitNotes:
                    viewData = this.baseNoteService.AddScheduleTaskAndNoteHelper(episode, scheduleEvents);
                    break;
                case (int)Tables.Assessments:
                    viewData = this.baseAssessmentService.AddScheduleTaskAndAssessmentHelper(episode, scheduleEvents);
                    break;
                case (int)Tables.PhysicianOrders:
                    viewData = this.basePhysicianOrderService.AddScheduleTaskAndPhysicianOrderHelper(episode.PatientId, scheduleEvents);
                    break;
                case (int)Tables.PlanOfCares:
                    viewData = this.basePlanOfCareService.AddScheduleTaskAndPlanOfCaresHelper(episode.PatientId, scheduleEvents);
                    break;
                case (int)Tables.CommunicationNotes:
                    viewData = this.baseCommunicationNoteService.AddScheduleTaskAndCommunicationNoteHelper(episode.PatientId, scheduleEvents);
                    break;
                case (int)Tables.Infections:
                    viewData = this.baseInfectionService.AddScheduleTaskAndInfectionHelper(episode.PatientId, scheduleEvents);
                    break;
                case (int)Tables.Incidents:
                    viewData = this.baseIncidentAccidentService.AddScheduleTaskAndIncidentHelper(episode.PatientId, scheduleEvents);
                    break;
                default:
                    viewData = ProcessScheduleAppSpecific(table, episode, scheduleEvents);
                    break;
            }
            return viewData;
        }

        protected abstract JsonViewData ProcessScheduleAppSpecific(int table, E episode, List<T> scheduleEvents);

        protected abstract JsonViewData UpdateScheduleEntityAppSpecific(T task, bool isDeprecated);

        protected JsonViewData UpdateScheduleEventDetail(T scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var assetsSaved = true;
            var jsonViewData = new JsonViewData(false);
            T scheduleEventToEdit = this.baseScheduleService.GetScheduleTask(scheduleEvent.PatientId, scheduleEvent.Id);
            if (scheduleEventToEdit != null)
            {
                T scheduleBuffer = scheduleEventToEdit.DeepClone();  // TaskHelperFactory<T>.CopyTaskValues(scheduleEventToEdit);
                if (httpFiles != null && httpFiles.Count > 0 && AssetHelper.ContainsAssets(httpFiles))
                {
                    assetsSaved = AssetHelper.AddAssetAndSetToSchedule(scheduleEventToEdit, httpFiles);
                }
                scheduleEvent.Asset = scheduleEventToEdit.Asset;
                TaskHelperFactory<T>.CopyTaskValues(scheduleEventToEdit, scheduleEvent);
                if (assetsSaved && this.baseScheduleService.UpdateScheduleTask(scheduleEventToEdit))
                {
                    if (ProcessEditDetail(scheduleEventToEdit))
                    {
                        if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEventToEdit.DisciplineTask))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEventToEdit.EpisodeId, scheduleEventToEdit.PatientId, scheduleEventToEdit.Id, Actions.EditDetail, (DisciplineTasks)scheduleEventToEdit.DisciplineTask, scheduleBuffer.EpisodeId != scheduleEventToEdit.EpisodeId ? "Reassigned from other episode." : string.Empty);
                        }
                        var isStatusChange = scheduleBuffer.Status != scheduleEventToEdit.Status;
                        jsonViewData.Service = this.Service.ToString();
                        jsonViewData.ServiceId = (int)this.Service;
                        jsonViewData.isSuccessful = true;
                        jsonViewData.EpisodeId = scheduleEventToEdit.EpisodeId;
                        jsonViewData.PatientId = scheduleEventToEdit.PatientId;
                        jsonViewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                        jsonViewData.IsCaseManagementRefresh = isStatusChange && (ScheduleStatusFactory.CaseManagerStatus().Contains(scheduleEventToEdit.Status) || ScheduleStatusFactory.CaseManagerStatus().Contains(scheduleBuffer.Status));
                        jsonViewData.IsDataCentersRefresh = TaskHelperFactory<T>.ShouldDataCentersRefresh(scheduleBuffer, scheduleEventToEdit);
                        jsonViewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(jsonViewData.IsDataCentersRefresh, scheduleEventToEdit);
                    }
                    else
                    {
                        //TaskHelperFactory<T>.CopyTaskValues(scheduleEventToEdit, scheduleBuffer);
                        this.baseScheduleService.UpdateScheduleTask(scheduleBuffer);
                        jsonViewData.isSuccessful = false;
                        jsonViewData.errorMessage = "Failed to process the task.";
                    }
                }
            }
            return jsonViewData;
        }


        #endregion
    }
}