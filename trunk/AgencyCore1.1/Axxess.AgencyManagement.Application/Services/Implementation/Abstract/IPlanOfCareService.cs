﻿using System;
using Axxess.AgencyManagement.Entities;
using Axxess.Core;
using Axxess.AgencyManagement.Application.ViewData;
using System.Collections.Generic;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.AgencyManagement.Repositories;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.Log.Enums;
using System.Web.Mvc;
namespace Axxess.AgencyManagement.Application.Services.Implementation.Abstract
{
    //interface IPlanOfCareService<T>
    // where T : ITask, new()
    //{
    //    IDictionary<string,Question> Allergies(Assessment assessment);
    //    IDictionary<string, Question> Diagnosis(Assessment assessment);
    //    List<Question> Get485FromAssessment(Guid patientId, List<Question> questions);
    //    PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId);
    //    PlanofCare GetPlanOfCarePrint(Guid patientId, Guid eventId) ;
    //    //IDictionary<string, Question> LocatorQuestions(Assessment assessment);
    //    JsonViewData UpdatePlanofCare(System.Web.Mvc.FormCollection formCollection);
    //    bool UpdatePlanOfCareForDetail(T schedule);
    //    bool UpdatePlanofCareStandAlone(System.Web.Mvc.FormCollection formCollection);
    //    bool UpdatePlanOfCareStandAloneForDetail(T schedule);
    //    JsonViewData UpdatePlanofCareStatus(Guid patientId, Guid eventId, string actionType);
    //}
    ////public abstract class PlanOfCareService<T> : IPlanOfCareService<T> where T : ITask, new()
    ////{

    ////    #region Constructor / Members

    ////    protected IPhysicianRepository physicianRepository;
    ////    protected IPlanOfCareRepo basePlanOfCareRepository;
    ////    protected IAgencyRepository agencyRepository;
    ////    protected IPatientRepository patientRepository;
    ////    private TaskScheduleAbstract<T> baseScheduleRepository;

    ////    protected PlanOfCareService(TaskScheduleAbstract<T> scheduleRepository, IPlanOfCareRepo basePlanOfCareRepository)
    ////    {
    ////        this.baseScheduleRepository = scheduleRepository;
    ////        this.basePlanOfCareRepository = basePlanOfCareRepository;
    ////    }

    ////    #endregion

    ////    #region IAssessmentService Members


    ////    public T GetPlanofCareScheduleEvent(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType)
    ////    {
    ////        var planofCare = basePlanOfCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, episodeId, assessmentId, assessmentType);
    ////        if (planofCare != null)
    ////        {
    ////            var scheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.Id);
    ////            if (scheduleEvent != null)
    ////            {
    ////                return scheduleEvent;
    ////            }
    ////        }
    ////        return null;
    ////    }

    ////    public PlanofCareViewData GetPatientAndAgencyInfo<P>(Guid episodeId, Guid patientId, Guid eventId) where P : IPlanOfCare, new()
    ////    {
    ////        var viewData = new PlanofCareViewData();
    ////        var planofcare = basePlanOfCareRepository.Get<P>(Current.AgencyId, patientId, eventId);
    ////        if (planofcare != null)
    ////        {
    ////            if (planofcare.Data.IsNotNullOrEmpty())
    ////            {
    ////                planofcare.Questions = planofcare.Data.ToObject<List<Question>>();
    ////            }
    ////            viewData.EditData = planofcare.ToDictionary();
    ////            var dictionary = new Dictionary<string, string>();
    ////            dictionary.Add("Id", eventId.ToString());
    ////            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
    ////            if (patient != null)
    ////            {
    ////                dictionary.Add("PatientId", patient.Id.ToString());
    ////                dictionary.Add("PatientName", patient.DisplayName);
    ////                dictionary.Add("PatientIdNumber", patient.PatientIdNumber);
    ////                dictionary.Add("PatientMedicareNumber", patient.MedicareNumber);
    ////                dictionary.Add("PatientAddressFirstRow", patient.AddressFirstRow);
    ////                dictionary.Add("PatientAddressSecondRow", patient.AddressSecondRow);
    ////                dictionary.Add("PatientPhone", patient.PhoneHomeFormatted);
    ////                dictionary.Add("PatientDoB", patient.DOBFormatted);
    ////                dictionary.Add("PatientGender", patient.Gender);
    ////                if (!patient.AgencyId.IsEmpty())
    ////                {
    ////                    var agency = agencyRepository.GetAgencyOnly(patient.AgencyId);
    ////                    if (agency != null)
    ////                    {
    ////                        dictionary.Add("AgencyId", agency.Id.ToString());
    ////                        dictionary.Add("AgencyMedicareProviderNumber", agency.MedicareProviderNumber);
    ////                        dictionary.Add("AgencyName", agency.Name);
    ////                        var mainLocation = agencyRepository.GetMainLocation(agency.Id);
    ////                        if (mainLocation != null)
    ////                        {
    ////                            dictionary.Add("AgencyAddressFirstRow", mainLocation.AddressFirstRow);
    ////                            dictionary.Add("AgencyAddressSecondRow", mainLocation.AddressSecondRow);
    ////                            dictionary.Add("AgencyPhone", mainLocation.PhoneWorkFormatted);
    ////                            dictionary.Add("AgencyFax", mainLocation.FaxNumberFormatted);
    ////                        }
    ////                    }
    ////                }
    ////                if (planofcare.PhysicianData.IsNotNullOrEmpty())
    ////                {
    ////                    var physician = planofcare.PhysicianData.ToObject<AgencyPhysician>();
    ////                    if (physician != null)
    ////                    {
    ////                        dictionary.Add("PhysicianId", physician.Id.ToString());
    ////                        dictionary.Add("PhysicianName", physician.DisplayName);
    ////                        dictionary.Add("PhysicianNpi", physician.NPI);
    ////                    }
    ////                }
    ////                else
    ////                {
    ////                    var primaryPhysician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
    ////                    if (primaryPhysician != null)
    ////                    {
    ////                        dictionary.Add("PhysicianId", primaryPhysician.Id.ToString());
    ////                        dictionary.Add("PhysicianName", primaryPhysician.DisplayName);
    ////                        dictionary.Add("PhysicianNpi", primaryPhysician.NPI);
    ////                    }
    ////                }
    ////            }
    ////            var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, episodeId, patientId);
    ////            if (episode != null)
    ////            {
    ////                dictionary.Add("PatientSoC", episode.StartOfCareDate.ToString("MM/dd/yyyy"));
    ////                dictionary.Add("EpisodeId", episode.Id.ToString());
    ////                dictionary.Add("EpisodeCertPeriod", string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted));
    ////            }
    ////            var scheduledEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, eventId);
    ////            if (scheduledEvent != null && scheduledEvent.EventDate.IsValid())
    ////                dictionary.Add("StatusComment", scheduledEvent.StatusComment);
    ////            viewData.DisplayData = dictionary;
    ////        }
    ////        return viewData;
    ////    }

    ////    public JsonViewData UpdatePlanofCare<P>(FormCollection formCollection) where P : IPlanOfCare, new()
    ////    {
    ////        var result = false;
    ////        var viewData = new JsonViewData { isSuccessful = false };
    ////        var planofCareId = formCollection.Get("Id").ToGuid();
    ////        var patientId = formCollection.Get("PatientId").ToGuid();

    ////        var planofCare = basePlanOfCareRepository.Get<P>(Current.AgencyId, patientId, planofCareId);
    ////        if (planofCare != null)
    ////        {
    ////            var isCaseManagementRefresh = false;
    ////            var isStatusChange = false;

    ////            Guid physicianGuidId;
    ////            var physicianStringId = formCollection.Get("PhysicianId");
    ////            if (physicianStringId.IsNotNullOrEmpty() && physicianStringId.GuidTryParse(out physicianGuidId))
    ////            {
    ////                planofCare.PhysicianId = physicianGuidId;
    ////            }

    ////            var status = formCollection.Get("Status");
    ////            if (status.IsNotNullOrEmpty() && status.IsInteger())
    ////            {
    ////                planofCare.Status = status.ToInteger();
    ////                if (planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
    ////                {
    ////                    isCaseManagementRefresh = true;
    ////                    planofCare.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
    ////                    planofCare.SignatureDate = DateTime.Parse(formCollection.Get("SignatureDate"));
    ////                    if (Current.HasRight(Permissions.BypassCaseManagement))
    ////                    {
    ////                        isCaseManagementRefresh = false;
    ////                        planofCare.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
    ////                    }
    ////                    if (!planofCare.PhysicianId.IsEmpty())
    ////                    {
    ////                        var physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
    ////                        if (physician != null)
    ////                        {
    ////                            planofCare.PhysicianData = physician.ToXml();
    ////                        }
    ////                    }
    ////                }
    ////                else
    ////                {
    ////                    planofCare.SignatureText = string.Empty;
    ////                    planofCare.SignatureDate = DateTime.MinValue;
    ////                }

    ////                var scheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, planofCareId);
    ////                if (scheduleEvent != null)
    ////                {
    ////                    var oldStatus = scheduleEvent.Status;
    ////                    scheduleEvent.Status = planofCare.Status;
    ////                    if (baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent))
    ////                    {
    ////                        ProcessPlanofCare(formCollection, planofCare);
    ////                        planofCare.Data = planofCare.Questions.ToXml();
    ////                        if (planofCareId.IsEmpty())
    ////                        {
    ////                            result = basePlanOfCareRepository.Add(planofCare);
    ////                            if (result)
    ////                            {
    ////                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
    ////                                result = true;
    ////                            }
    ////                            else
    ////                            {
    ////                                scheduleEvent.Status = oldStatus;
    ////                                baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent);
    ////                                result = false;
    ////                            }

    ////                        }
    ////                        else
    ////                        {
    ////                            result = basePlanOfCareRepository.Update(planofCare);
    ////                            if (result)//(planofCareRepository.Update(planofCare))
    ////                            {
    ////                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)planofCare.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
    ////                                result = true;
    ////                            }
    ////                            else
    ////                            {
    ////                                scheduleEvent.Status = oldStatus;
    ////                                baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent);
    ////                                result = false;
    ////                            }
    ////                        }
    ////                        if (result)
    ////                        {
    ////                            isStatusChange = scheduleEvent.Status != oldStatus;
    ////                            viewData.isSuccessful = true;
    ////                            viewData.errorMessage = "The 485 Plan of Care has been updated successfully.";
    ////                            if (scheduleEvent.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
    ////                            {
    ////                                viewData.errorMessage = "The 485 Plan of Care has been completed and pending QA Review.";
    ////                            }
    ////                            viewData.IsDataCentersRefresh = isStatusChange;
    ////                            viewData.IsCaseManagementRefresh = isCaseManagementRefresh;
    ////                            viewData.PatientId = scheduleEvent.PatientId;
    ////                            viewData.EpisodeId = scheduleEvent.EpisodeId;
    ////                            viewData.IsMyScheduleTaskRefresh = isStatusChange && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsCompleted && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
    ////                            viewData.IsOrdersToBeSentRefresh = isStatusChange && scheduleEvent.Status == ((int)ScheduleStatus.OrderToBeSentToPhysician);
    ////                        }
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        return viewData;
    ////    }

    ////    public bool UpdatePlanofCareStandAlone<P>(FormCollection formCollection) where P : IPlanOfCare, new()
    ////    {
    ////        var result = false;
    ////        var planofCareId = formCollection.Get("Id").ToGuid();
    ////        var patientId = formCollection.Get("PatientId").ToGuid();

    ////        var planofCare = basePlanOfCareRepository.Get<P>(Current.AgencyId, patientId, planofCareId);
    ////        if (planofCare != null)
    ////        {
    ////            Guid physicianGuidId;
    ////            var physicianStringId = formCollection.Get("PhysicianId");
    ////            if (physicianStringId.IsNotNullOrEmpty() && physicianStringId.GuidTryParse(out physicianGuidId))
    ////            {
    ////                planofCare.PhysicianId = physicianGuidId;
    ////            }

    ////            var status = formCollection.Get("Status");
    ////            if (status.IsNotNullOrEmpty() && status.IsInteger())
    ////            {
    ////                planofCare.Status = int.Parse(status);
    ////                if (planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
    ////                {
    ////                    if (Current.HasRight(Permissions.BypassCaseManagement))
    ////                    {
    ////                        planofCare.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
    ////                    }
    ////                    if (!planofCare.PhysicianId.IsEmpty())
    ////                    {
    ////                        var physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
    ////                        if (physician != null)
    ////                        {
    ////                            planofCare.PhysicianData = physician.ToXml();
    ////                        }
    ////                    }
    ////                    planofCare.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
    ////                    planofCare.SignatureDate = DateTime.Parse(formCollection.Get("SignatureDate"));

    ////                }


    ////                var scheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, planofCareId);
    ////                if (scheduleEvent != null)
    ////                {
    ////                    var oldStatus = scheduleEvent.Status;
    ////                    scheduleEvent.Status = planofCare.Status;
    ////                    if (baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent))
    ////                    {
    ////                        ProcessPlanofCare(formCollection, planofCare);
    ////                        planofCare.Data = planofCare.Questions.ToXml();

    ////                        if (basePlanOfCareRepository.Update<P>(planofCare))
    ////                        {
    ////                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id, Actions.StatusChange, (ScheduleStatus)planofCare.Status, DisciplineTasks.HCFA485StandAlone, string.Empty);
    ////                            result = true;
    ////                        }
    ////                        else
    ////                        {
    ////                            scheduleEvent.Status = oldStatus;
    ////                            baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent);
    ////                            result = false;
    ////                        }
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        return result;
    ////    }

    ////    public bool UpdatePlanOfCareForDetail<P>(T schedule, Guid oldEpisodeId) where P : IPlanOfCare, new()
    ////    {
    ////        //TODO: Needs review
    ////        bool result = false;
    ////        var planofCare = basePlanOfCareRepository.Get<P>(Current.AgencyId, schedule.PatientId, schedule.Id);//planofCareRepository.Get(Current.AgencyId, oldEpisodeId, schedule.PatientId, schedule.Id);
    ////        if (planofCare != null)
    ////        {
    ////            planofCare.Status = schedule.Status;
    ////            if (planofCare.Status != schedule.Status && planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
    ////            {
    ////                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
    ////                if (physician != null)
    ////                {
    ////                    planofCare.PhysicianData = physician.ToXml();
    ////                }
    ////            }
    ////            planofCare.EpisodeId = schedule.EpisodeId;
    ////            planofCare.PhysicianId = schedule.PhysicianId;
    ////            planofCare.IsDeprecated = schedule.IsDeprecated;
    ////            result = basePlanOfCareRepository.Update(planofCare);
    ////        }
    ////        else
    ////        {
    ////            result = true;
    ////        }
    ////        return result;
    ////    }

    ////    public bool UpdatePlanOfCareStandAloneForDetail<P>(T schedule) where P : IPlanOfCare, new()
    ////    {
    ////        bool result = false;
    ////        var planofCare = basePlanOfCareRepository.Get<P>(Current.AgencyId, schedule.PatientId, schedule.Id);
    ////        if (planofCare != null)
    ////        {
    ////            planofCare.Status = schedule.Status;
    ////            if (planofCare.Status != schedule.Status && planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
    ////            {
    ////                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
    ////                if (physician != null)
    ////                {
    ////                    planofCare.PhysicianData = physician.ToXml();
    ////                }
    ////            }
    ////            planofCare.PhysicianId = schedule.PhysicianId;
    ////            planofCare.EpisodeId = schedule.EpisodeId;
    ////            planofCare.IsDeprecated = schedule.IsDeprecated;
    ////            result = basePlanOfCareRepository.Update<P>(planofCare);
    ////        }
    ////        else
    ////        {
    ////            result = true;
    ////        }
    ////        return result;
    ////    }

    ////    public void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis)
    ////    {
    ////        var planofCare = basePlanOfCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, scheduleEvent.EpisodeId, assessment.Id, assessment.Type.ToString());
    ////        if (planofCare == null)
    ////        {
    ////            planofCare = new PlanofCare();
    ////            planofCare.Id = scheduleEvent.Id;
    ////            planofCare.AssessmentId = assessment.Id;
    ////            planofCare.AgencyId = Current.AgencyId;
    ////            planofCare.PatientId = scheduleEvent.PatientId;
    ////            planofCare.EpisodeId = scheduleEvent.EpisodeId;
    ////            planofCare.Status = scheduleEvent.Status;
    ////            planofCare.AssessmentType = assessment.Type.ToString();
    ////            planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
    ////            planofCare.IsNonOasis = isNonOasis;
    ////            var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patient.Id);
    ////            if (physician != null)
    ////            {
    ////                planofCare.PhysicianId = physician.Id;
    ////            }
    ////            planofCare.UserId = scheduleEvent.UserId;
    ////            planofCare.Questions = Get485FromAssessment(assessment);
    ////            planofCare.Data = planofCare.Questions.ToXml();
    ////            basePlanOfCareRepository.Add(planofCare);
    ////        }
    ////    }

    ////    public JsonViewData UpdatePlanofCareStatus<P>(Guid patientId, Guid eventId, string actionType) where P : IPlanOfCare, new()
    ////    {
    ////        var viewData = new JsonViewData { isSuccessful = false };
    ////        bool isOrderUpdates = true;
    ////        bool isActionSet = false;
    ////        if (!eventId.IsEmpty() && !patientId.IsEmpty())
    ////        {
    ////            var scheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, eventId);
    ////            if (scheduleEvent != null)
    ////            {
    ////                viewData.PatientId = patientId;
    ////                viewData.EpisodeId = scheduleEvent.EpisodeId;


    ////                var oldStatus = scheduleEvent.Status;
    ////                var oldPrintQueue = scheduleEvent.InPrintQueue;
    ////                var description = string.Empty;

    ////                var planofCare = basePlanOfCareRepository.Get<P>(Current.AgencyId, patientId, eventId);
    ////                if (planofCare != null)
    ////                {
    ////                    if (actionType == "Approve")
    ////                    {
    ////                        planofCare.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
    ////                        scheduleEvent.InPrintQueue = true;
    ////                        scheduleEvent.Status = planofCare.Status;
    ////                        description = "Approved By:" + Current.UserFullName;
    ////                        isActionSet = true;
    ////                    }
    ////                    else if (actionType == "Return")
    ////                    {
    ////                        planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
    ////                        scheduleEvent.Status = planofCare.Status;
    ////                        description = "Returned By:" + Current.UserFullName;
    ////                        isActionSet = true;
    ////                    }
    ////                    else if (actionType == "Print")
    ////                    {
    ////                        scheduleEvent.InPrintQueue = false;
    ////                        isOrderUpdates = false;
    ////                        isActionSet = true;
    ////                    }
    ////                    if (isActionSet)
    ////                    {
    ////                        if (baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent))
    ////                        {
    ////                            if (isOrderUpdates)
    ////                            {
    ////                                if (basePlanOfCareRepository.Update<P>(planofCare))
    ////                                {
    ////                                    viewData.IsDataCentersRefresh = oldStatus != scheduleEvent.Status;
    ////                                    viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == ((int)ScheduleStatus.OrderSubmittedPendingReview);
    ////                                    viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == scheduleEvent.UserId && !scheduleEvent.IsCompleted && scheduleEvent.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleEvent.EventDate.Date <= DateTime.Now.AddDays(14);
    ////                                    viewData.IsOrdersToBeSentRefresh = viewData.IsDataCentersRefresh && scheduleEvent.Status == ((int)ScheduleStatus.OrderToBeSentToPhysician);
    ////                                    viewData.IsPhysicianOrderPOCRefresh = viewData.IsDataCentersRefresh;
    ////                                    viewData.isSuccessful = true;

    ////                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
    ////                                    {
    ////                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
    ////                                    }
    ////                                }
    ////                                else
    ////                                {
    ////                                    scheduleEvent.Status = oldStatus;
    ////                                    scheduleEvent.InPrintQueue = oldPrintQueue;
    ////                                    baseScheduleRepository.UpdateScheduleEventNew(scheduleEvent);
    ////                                    viewData.isSuccessful = false;
    ////                                }
    ////                            }
    ////                            else
    ////                            {
    ////                                viewData.isSuccessful = true;
    ////                            }
    ////                        }
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        return viewData;
    ////    }

    ////    public P GetPlanOfCarePrint<P>(Guid patientId, Guid eventId) where P : IPlanOfCare, new()
    ////    {
    ////        var isStandAlone = false;
    ////        P planofCare;
    ////        var scheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, patientId, eventId);
    ////        if (scheduleEvent != null)
    ////        {
    ////            planofCare = basePlanOfCareRepository.Get<P>(Current.AgencyId, patientId, eventId);
    ////            if (planofCare != null)
    ////            {
    ////                isStandAlone = true;

    ////                if (planofCare.Data.IsNotNullOrEmpty())
    ////                {
    ////                    planofCare.EpisodeId = scheduleEvent.EpisodeId;
    ////                    var agency = agencyRepository.GetWithBranches(planofCare.AgencyId);
    ////                    planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
    ////                    planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
    ////                    planofCare = POCServiceSpecificPrintData(isStandAlone, planofCare);
    ////                    if (ScheduleStatusFactory.OrdersCompleted(true).Contains(planofCare.Status) && !planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
    ////                    {
    ////                    }
    ////                    else
    ////                    {
    ////                        var physician = PhysicianEngine.Get(planofCare.PhysicianId, Current.AgencyId);
    ////                        if (physician != null)
    ////                        {
    ////                            planofCare.PhysicianData = physician.ToXml();
    ////                        }
    ////                    }
    ////                    var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
    ////                    if (allergyProfile != null)
    ////                    {
    ////                        planofCare.Allergies = allergyProfile.ToString();
    ////                    }
    ////                    bool ifAllergies = false;
    ////                    foreach (Question q in planofCare.Questions)
    ////                    {
    ////                        if (q.Name == "AllergiesDescription" && q.Answer.Trim().IsNotNullOrEmpty())
    ////                        {
    ////                            ifAllergies = true;
    ////                            break;
    ////                        }
    ////                    }
    ////                    if (!ifAllergies)
    ////                    {
    ////                        planofCare.Questions.RemoveAll(x => x.Name == "AllergiesDescription");
    ////                        Question allergiesDescription = new Question { Name = "AllergiesDescription", Answer = allergyProfile.ToString(), Type = QuestionType.PlanofCare };
    ////                        planofCare.Questions.Add(allergiesDescription);
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        return planofCare;
    ////    }

    ////    protected abstract PlanofCare POCServiceSpecificPrintData(bool isStandAlone, PlanofCare planofCare);

    ////    public IDictionary<string, Question> Allergies(Assessment assessment)
    ////    {
    ////        var allergies = new Dictionary<string, Question>();
    ////        if (assessment != null)
    ////        {
    ////            var questions = assessment.ToDictionary();
    ////            if (questions != null && questions.Count > 0)
    ////            {
    ////                if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
    ////                {
    ////                    allergies.Add("485Allergies", questions["485Allergies"]);
    ////                }
    ////                if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
    ////                {
    ////                    allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
    ////                }
    ////            }
    ////        }
    ////        return allergies;
    ////    }

    ////    public IDictionary<string, Question> Diagnosis(Assessment assessment)
    ////    {
    ////        var diagnosis = new Dictionary<string, Question>();
    ////        if (assessment != null)
    ////        {
    ////            var questions = assessment.ToDictionary();
    ////            if (questions != null && questions.Count > 0)
    ////            {
    ////                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
    ////                {
    ////                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
    ////                }
    ////                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
    ////                {
    ////                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
    ////                }

    ////                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
    ////                {
    ////                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
    ////                }

    ////                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
    ////                {
    ////                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
    ////                }

    ////                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
    ////                {
    ////                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
    ////                }

    ////                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
    ////                {
    ////                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
    ////                }

    ////                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
    ////                {
    ////                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
    ////                }

    ////                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
    ////                {
    ////                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
    ////                }

    ////                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
    ////                {
    ////                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
    ////                }

    ////                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
    ////                {
    ////                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
    ////                }

    ////                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
    ////                {
    ////                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
    ////                }

    ////                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
    ////                {
    ////                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
    ////                }
    ////            }

    ////        }
    ////        return diagnosis;
    ////    }

    ////    public IDictionary<string, Question> LocatorQuestions(Assessment assessment)
    ////    {
    ////        IDictionary<string, Question> locators = new Dictionary<string, Question>();
    ////        if (assessment != null)
    ////        {
    ////            var planofCare = new PlanofCare();
    ////            planofCare.Questions = Get485FromAssessment(assessment);
    ////            locators = planofCare.ToDictionary();
    ////        }
    ////        return locators;
    ////    }

    ////    #endregion

    ////    #region Private Members


    ////    private void ProcessPlanofCare(FormCollection formCollection, PlanofCare planofCare)
    ////    {
    ////        formCollection.Remove("Id");
    ////        formCollection.Remove("EpisodeId");
    ////        formCollection.Remove("PatientId");
    ////        formCollection.Remove("Status");
    ////        formCollection.Remove("SignatureText");
    ////        formCollection.Remove("SignatureDate");
    ////        formCollection.Remove("PhysicianId_text");
    ////        formCollection.Remove("PhysicianId");

    ////        if (planofCare.Data.IsNotNullOrEmpty())
    ////        {
    ////            planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
    ////            IDictionary<string, Question> questions = planofCare.ToDictionary();
    ////            questions = RemovePrimaryDiagnosis(questions);

    ////            foreach (var key in formCollection.AllKeys)
    ////            {
    ////                string keyName = key;
    ////                string[] nameArray = key.Split('_');
    ////                if (nameArray != null && nameArray.Length > 2)
    ////                {
    ////                    nameArray.Reverse();
    ////                    keyName = nameArray[0];
    ////                }

    ////                string answer = formCollection.GetValues(key).Join(",");
    ////                if (questions.ContainsKey(keyName))
    ////                {
    ////                    questions[keyName].Answer = answer;
    ////                }
    ////                else
    ////                {
    ////                    questions.Add(keyName, Question.Create(key, answer));
    ////                }
    ////            }
    ////            planofCare.Questions = questions.Values.ToList();
    ////        }



    ////    }

    ////    private void ProcessPlanofCare(FormCollection formCollection, PlanofCareStandAlone planofCare)
    ////    {
    ////        formCollection.Remove("Id");
    ////        formCollection.Remove("EpisodeId");
    ////        formCollection.Remove("PatientId");
    ////        formCollection.Remove("PhysicianId");
    ////        formCollection.Remove("Status");
    ////        formCollection.Remove("SignatureText");
    ////        formCollection.Remove("SignatureDate");
    ////        formCollection.Remove("PhysicianId_text");

    ////        if (planofCare.Data.IsNotNullOrEmpty())
    ////        {
    ////            planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
    ////            IDictionary<string, Question> questions = planofCare.ToDictionary();
    ////            questions = RemovePrimaryDiagnosis(questions);

    ////            foreach (var key in formCollection.AllKeys)
    ////            {
    ////                string nonGenericKey = key.Replace("Generic", "");
    ////                string answer = formCollection.GetValues(key).Join(",");
    ////                if (questions.ContainsKey(key))
    ////                {
    ////                    questions[key].Answer = answer;
    ////                }
    ////                else if (questions.ContainsKey(nonGenericKey))
    ////                {
    ////                    questions[nonGenericKey].Answer = answer;
    ////                }
    ////                else
    ////                {
    ////                    questions.Add(key, Question.Create(key, answer));
    ////                }
    ////            }
    ////            planofCare.Questions = questions.Values.ToList();
    ////        }
    ////    }

    ////    private IDictionary<string, Question> RemovePrimaryDiagnosis(IDictionary<string, Question> questions)
    ////    {
    ////        if (questions != null && questions.Count > 0)
    ////        {
    ////            questions.RemoveAll<string, Question>(i => i.Code == "M1020" || i.Code == "M1022");
    ////            return questions;
    ////        }
    ////        else
    ////        {
    ////            return new Dictionary<string, Question>();
    ////        }
    ////    }

    ////    public List<Question> Get485FromAssessment(Assessment assessment)
    ////    {
    ////        var planofCareQuestions = new List<Question>();
    ////        if (assessment != null)
    ////        {
    ////            var assessmentQuestions = assessment.ToDictionary();
    ////            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
    ////            {
    ////                // 10. Medications
    ////                var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
    ////                if (medicationProfile != null)
    ////                {
    ////                    planofCareQuestions.Add(Question.Create("485Medications", medicationProfile.ToString()));
    ////                }

    ////                planofCareQuestions.Add(GetQuestion("M0102PhysicianOrderedDate", assessmentQuestions));

    ////                // 11. Principal Diagnosis
    ////                planofCareQuestions.Add(GetQuestion("M1020PrimaryDiagnosis", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("M1020ICD9M", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485ExacerbationOrOnsetPrimaryDiagnosis", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("M1020PrimaryDiagnosisDate", assessmentQuestions));

    ////                // 12. Surgical Procedure
    ////                planofCareQuestions.Add(GetQuestion("485SurgicalProcedureDescription1", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode1", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode1Date", assessmentQuestions));

    ////                planofCareQuestions.Add(GetQuestion("485SurgicalProcedureDescription2", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode2", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode2Date", assessmentQuestions));

    ////                // 13. Other Pertinent Diagnosis
    ////                for (int count = 1; count <= 14; count++)
    ////                {
    ////                    planofCareQuestions.Add(GetQuestion(string.Format("M1022PrimaryDiagnosis{0}", count), assessmentQuestions));
    ////                    planofCareQuestions.Add(GetQuestion(string.Format("M1022ICD9M{0}", count), assessmentQuestions));
    ////                    planofCareQuestions.Add(GetQuestion(string.Format("485ExacerbationOrOnsetPrimaryDiagnosis{0}", count), assessmentQuestions));
    ////                    planofCareQuestions.Add(GetQuestion(string.Format("M1022PrimaryDiagnosis{0}Date", count), assessmentQuestions));
    ////                }

    ////                // 14. DME and Supplies
    ////                planofCareQuestions.Add(GetQuestion("485DME", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485DMEComments", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485Supplies", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485SuppliesComment", assessmentQuestions));

    ////                // 15. Safety Measures
    ////                planofCareQuestions.Add(GetQuestion("485SafetyMeasures", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485OtherSafetyMeasures", assessmentQuestions));

    ////                // 16. Nutritional Requirements
    ////                if (assessmentQuestions.ContainsKey("485NutritionalReqs") && assessmentQuestions["485NutritionalReqs"] != null)
    ////                {
    ////                    planofCareQuestions.Add(Question.Create("485NutritionalReqs", PlanofCareXml.ExtractText("NutritionalRequirements", assessmentQuestions)));
    ////                }

    ////                // 17. Allergies
    ////                var allergyProfile = patientRepository.GetAllergyProfileByPatient(assessment.PatientId, Current.AgencyId);
    ////                planofCareQuestions.Add(GetQuestion("485Allergies", assessmentQuestions));
    ////                if (assessmentQuestions.ContainsKey("485AllergiesDescription") && assessmentQuestions["485AllergiesDescription"] != null)
    ////                {
    ////                    planofCareQuestions.Add(GetQuestion("485AllergiesDescription", assessmentQuestions));
    ////                }
    ////                else
    ////                {
    ////                    planofCareQuestions.Add(Question.Create("485AllergiesDescription", allergyProfile != null ? allergyProfile.ToString() : string.Empty));
    ////                }

    ////                // 18.A. Functional Limitations
    ////                planofCareQuestions.Add(GetQuestion("485FunctionLimitations", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485FunctionLimitationsOther", assessmentQuestions));

    ////                // 18.B. Activities Permitted
    ////                planofCareQuestions.Add(GetQuestion("485ActivitiesPermitted", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485ActivitiesPermittedOther", assessmentQuestions));


    ////                // 19. Mental Status
    ////                planofCareQuestions.Add(GetQuestion("485MentalStatus", assessmentQuestions));
    ////                planofCareQuestions.Add(GetQuestion("485MentalStatusOther", assessmentQuestions));

    ////                // 20. Prognosis
    ////                planofCareQuestions.Add(GetQuestion("485Prognosis", assessmentQuestions));

    ////                // 21. Interventions
    ////                var interventions = string.Empty;
    ////                if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"] != null && assessmentQuestions["485SNFrequency"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += string.Format("SN Frequency: {0}. ", assessmentQuestions["485SNFrequency"].Answer);
    ////                }
    ////                if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"] != null && assessmentQuestions["485PTFrequency"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += string.Format("PT Frequency: {0}. ", assessmentQuestions["485PTFrequency"].Answer);
    ////                }
    ////                if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"] != null && assessmentQuestions["485OTFrequency"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += string.Format("OT Frequency: {0}. ", assessmentQuestions["485OTFrequency"].Answer);
    ////                }
    ////                if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"] != null && assessmentQuestions["485STFrequency"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += string.Format("ST Frequency: {0}. ", assessmentQuestions["485STFrequency"].Answer);
    ////                }
    ////                if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"] != null && assessmentQuestions["485MSWFrequency"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += string.Format("MSW Frequency: {0}. ", assessmentQuestions["485MSWFrequency"].Answer);
    ////                }
    ////                if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"] != null && assessmentQuestions["485HHAFrequency"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += string.Format("HHA Frequency: {0}. ", assessmentQuestions["485HHAFrequency"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("485OrdersDisciplineInterventionComments") && assessmentQuestions["485OrdersDisciplineInterventionComments"] != null && assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer;
    ////                }

    ////                var baseVitalSignFormat = "{0} greater than (>) {1} or less than (<) {2}. ";

    ////                var vitalSignParameters = string.Empty;
    ////                if (assessmentQuestions.ContainsKey("GenericTempGreaterThan")
    ////                   && assessmentQuestions["GenericTempGreaterThan"] != null
    ////                   && assessmentQuestions["GenericTempGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericTempLessThan")
    ////                   && assessmentQuestions["GenericTempLessThan"] != null
    ////                   && assessmentQuestions["GenericTempLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Temperature", assessmentQuestions["GenericTempGreaterThan"].Answer, assessmentQuestions["GenericTempLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericPulseGreaterThan")
    ////                   && assessmentQuestions["GenericPulseGreaterThan"] != null
    ////                   && assessmentQuestions["GenericPulseGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericPulseLessThan")
    ////                   && assessmentQuestions["GenericPulseLessThan"] != null
    ////                   && assessmentQuestions["GenericPulseLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Pulse", assessmentQuestions["GenericPulseGreaterThan"].Answer, assessmentQuestions["GenericPulseLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericRespirationGreaterThan")
    ////                   && assessmentQuestions["GenericRespirationGreaterThan"] != null
    ////                   && assessmentQuestions["GenericRespirationGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericRespirationLessThan")
    ////                   && assessmentQuestions["GenericRespirationLessThan"] != null
    ////                   && assessmentQuestions["GenericRespirationLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Respirations", assessmentQuestions["GenericRespirationGreaterThan"].Answer, assessmentQuestions["GenericRespirationLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericSystolicBPGreaterThan")
    ////                   && assessmentQuestions["GenericSystolicBPGreaterThan"] != null
    ////                   && assessmentQuestions["GenericSystolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericSystolicBPLessThan")
    ////                   && assessmentQuestions["GenericSystolicBPLessThan"] != null
    ////                   && assessmentQuestions["GenericSystolicBPLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Systolic BP", assessmentQuestions["GenericSystolicBPGreaterThan"].Answer, assessmentQuestions["GenericSystolicBPLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericDiastolicBPGreaterThan")
    ////                   && assessmentQuestions["GenericDiastolicBPGreaterThan"] != null
    ////                   && assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericDiastolicBPLessThan")
    ////                   && assessmentQuestions["GenericDiastolicBPLessThan"] != null
    ////                   && assessmentQuestions["GenericDiastolicBPLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Diastolic BP", assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer, assessmentQuestions["GenericDiastolicBPLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("Generic02SatLessThan")
    ////                   && assessmentQuestions["Generic02SatLessThan"] != null
    ////                   && assessmentQuestions["Generic02SatLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format("O2 Sat (percent) less than (<) {0}. ", assessmentQuestions["Generic02SatLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericFastingBloodSugarGreaterThan")
    ////                   && assessmentQuestions["GenericFastingBloodSugarGreaterThan"] != null
    ////                   && assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericFastingBloodSugarLessThan")
    ////                   && assessmentQuestions["GenericFastingBloodSugarLessThan"] != null
    ////                   && assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Fasting blood sugar", assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer, assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericRandomBloddSugarGreaterThan")
    ////                   && assessmentQuestions["GenericRandomBloddSugarGreaterThan"] != null
    ////                   && assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer.IsNotNullOrEmpty()
    ////                   && assessmentQuestions.ContainsKey("GenericRandomBloodSugarLessThan")
    ////                   && assessmentQuestions["GenericRandomBloodSugarLessThan"] != null
    ////                   && assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format(baseVitalSignFormat, "Random blood sugar", assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer, assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer);
    ////                }

    ////                if (assessmentQuestions.ContainsKey("GenericWeightGreaterThan")
    ////                   && assessmentQuestions["GenericWeightGreaterThan"] != null
    ////                   && assessmentQuestions["GenericWeightGreaterThan"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    vitalSignParameters += string.Format("Weight Gain/Loss (lbs/7 days) Greater than {0}. ", assessmentQuestions["GenericWeightGreaterThan"].Answer);
    ////                }

    ////                if (vitalSignParameters.IsNotNullOrEmpty())
    ////                {
    ////                    interventions += "SN to notify MD of: " + vitalSignParameters;
    ////                }

    ////                interventions += PlanofCareXml.ExtractText("Intervention", assessmentQuestions);
    ////                var interventionQuestion = Question.Create("485Interventions", interventions);
    ////                planofCareQuestions.Add(interventionQuestion);

    ////                var goals = string.Empty;
    ////                goals += PlanofCareXml.ExtractText("Goal", assessmentQuestions);
    ////                if (assessmentQuestions.ContainsKey("485SensoryStatusGoalComments") && assessmentQuestions["485SensoryStatusGoalComments"] != null)
    ////                {
    ////                    goals += assessmentQuestions["485SensoryStatusGoalComments"].Answer;
    ////                }
    ////                if (assessmentQuestions.ContainsKey("485RehabilitationPotential") && assessmentQuestions["485RehabilitationPotential"] != null)
    ////                {
    ////                    if (assessmentQuestions["485RehabilitationPotential"].Answer == "1")
    ////                    {
    ////                        goals += "Rehab Potential: Good for stated goals.";
    ////                    }
    ////                    if (assessmentQuestions["485RehabilitationPotential"].Answer == "2")
    ////                    {
    ////                        goals += "Rehab Potential: Fair for stated goals.";
    ////                    }
    ////                    if (assessmentQuestions["485RehabilitationPotential"].Answer == "3")
    ////                    {
    ////                        goals += "Rehab Potential: Poor for stated goals.";
    ////                    }
    ////                }

    ////                if (assessmentQuestions.ContainsKey("485AchieveGoalsComments") && assessmentQuestions["485AchieveGoalsComments"] != null && assessmentQuestions["485AchieveGoalsComments"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    goals += assessmentQuestions["485AchieveGoalsComments"].Answer;
    ////                }

    ////                if (assessmentQuestions.ContainsKey("485DischargePlans") && assessmentQuestions["485DischargePlans"] != null)
    ////                {
    ////                    if (assessmentQuestions["485DischargePlans"].Answer == "1")
    ////                    {
    ////                        goals += "Discharge Plan: Patient to be discharged to the care of Physician. ";
    ////                    }
    ////                    if (assessmentQuestions["485DischargePlans"].Answer == "2")
    ////                    {
    ////                        goals += "Discharge Plan: Patient to be discharged to the care of Caregiver. ";
    ////                    }
    ////                    if (assessmentQuestions["485DischargePlans"].Answer == "3")
    ////                    {
    ////                        goals += "Discharge Plan: Patient to be discharged to Self care. ";
    ////                    }
    ////                }

    ////                if (assessmentQuestions.ContainsKey("485DischargePlansReason") && assessmentQuestions["485DischargePlansReason"] != null && assessmentQuestions["485DischargePlansReason"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    var reasons = assessmentQuestions["485DischargePlansReason"].Answer.Split(',');
    ////                    foreach (string reason in reasons)
    ////                    {
    ////                        if (reason.IsEqual("1"))
    ////                        {
    ////                            goals += "Discharge when caregiver willing and able to manage all aspects of patient's care. ";
    ////                        }
    ////                        if (reason.IsEqual("2"))
    ////                        {
    ////                            goals += "Discharge when goals met. ";
    ////                        }
    ////                        if (reason.IsEqual("3"))
    ////                        {
    ////                            goals += "Discharge when wound(s) healed. ";
    ////                        }
    ////                    }
    ////                }

    ////                if (assessmentQuestions.ContainsKey("485DischargePlanComments") && assessmentQuestions["485DischargePlanComments"] != null && assessmentQuestions["485DischargePlanComments"].Answer.IsNotNullOrEmpty())
    ////                {
    ////                    goals += assessmentQuestions["485DischargePlanComments"].Answer;
    ////                }

    ////                var goalQuestion = Question.Create("485Goals", goals);
    ////                planofCareQuestions.Add(goalQuestion);
    ////            }
    ////        }
    ////        return planofCareQuestions;
    ////    }

    ////    private Question GetQuestion(string questionName, IDictionary<string, Question> assessmentQuestions)
    ////    {
    ////        var question = new Question();

    ////        if (assessmentQuestions.ContainsKey(questionName) && assessmentQuestions[questionName] != null)
    ////        {
    ////            question = assessmentQuestions[questionName];
    ////        }
    ////        return question;
    ////    }


    ////    //private bool CreatePlanofCare(Assessment assessment,T scheduleEvent)
    ////    //{
    ////    //    var result = false;
    ////    //    var planofCare = planofCareRepository.GetPlanOfCareByAssessment(Current.AgencyId, assessment.EpisodeId, assessment.Id, assessment.Type.ToString());
    ////    //    if (planofCare == null)
    ////    //    {
    ////    //        var isNonOasis = assessment.Type == AssessmentType.NonOASISStartOfCare.ToString() || assessment.Type == AssessmentType.NonOASISRecertification.ToString();
    ////    //        var pocScheduleEvent = new ScheduleEvent
    ////    //        {
    ////    //            AgencyId = !scheduleEvent.AgencyId.IsEmpty() ? scheduleEvent.AgencyId : Current.AgencyId,
    ////    //            Id = Guid.NewGuid(),
    ////    //            EndDate = scheduleEvent.EndDate,
    ////    //            EventDate = scheduleEvent.EventDate,
    ////    //            VisitDate = scheduleEvent.EventDate,
    ////    //            UserId = scheduleEvent.UserId,
    ////    //            StartDate = scheduleEvent.StartDate,
    ////    //            PatientId = assessment.PatientId,
    ////    //            EpisodeId = assessment.EpisodeId,
    ////    //            Discipline = Disciplines.Orders.ToString(),
    ////    //            DisciplineTask = isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
    ////    //            Status = ((int)ScheduleStatus.OrderSaved),
    ////    //            IsOrderForNextEpisode = assessment.Type.IsEqual(AssessmentType.Recertification.ToString()) ? true : false
    ////    //        };

    ////    //        if (CreatePlanofCare(pocScheduleEvent, assessment, isNonOasis))
    ////    //        {
    ////    //            result = true;
    ////    //        }
    ////    //    }
    ////    //    else if (planofCare != null)
    ////    //    {
    ////    //        var planofCareScheduleEvent = baseScheduleRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.Id);// scheduleEvents.Where(s => s.EpisodeId == planofCare.EpisodeId && s.PatientId == planofCare.PatientId && s.EventId == planofCare.Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
    ////    //        if (planofCareScheduleEvent == null)
    ////    //        {
    ////    //            var isNonOasis = assessment.Type.IsEqual(AssessmentType.NonOASISStartOfCare.ToString()) || assessment.Type.IsEqual(AssessmentType.NonOASISRecertification.ToString());

    ////    //            var pocScheduleEvent = new ScheduleEvent
    ////    //            {
    ////    //                AgencyId = !planofCare.AgencyId.IsEmpty() ? planofCare.AgencyId : Current.AgencyId,
    ////    //                Id = planofCare.Id,
    ////    //                EndDate = scheduleEvent.EndDate,
    ////    //                EventDate = scheduleEvent.EventDate,
    ////    //                VisitDate = scheduleEvent.EventDate,
    ////    //                UserId = scheduleEvent.UserId,
    ////    //                StartDate = scheduleEvent.StartDate,
    ////    //                PatientId = assessment.PatientId,
    ////    //                EpisodeId = assessment.EpisodeId,
    ////    //                Discipline = Disciplines.Orders.ToString(),
    ////    //                DisciplineTask = isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
    ////    //                Status = ((int)ScheduleStatus.OrderSaved),
    ////    //                IsOrderForNextEpisode = assessment.Type.IsEqual(AssessmentType.Recertification.ToString()) ? true : false
    ////    //            };
    ////    //            if(baseScheduleRepository.AddScheduleEvent(pocScheduleEvent))
    ////    //            {
    ////    //                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.Id, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
    ////    //                result = true;
    ////    //            }
    ////    //            //scheduleEvents.Add(pocScheduleEvent);
    ////    //            // episode.Schedule = scheduleEvents.ToXml();
    ////    //            //if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, new List<ScheduleEvent> { pocScheduleEvent }))
    ////    //            //{
    ////    //            //    result = true;
    ////    //            //    Auditor.Log(pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.EventId, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
    ////    //            //}
    ////    //        }
    ////    //        else
    ////    //        {
    ////    //            result = true;
    ////    //        }
    ////    //    }
    ////    //    return result;
    ////    //}

    ////    //private bool CreatePlanofCare(T scheduleEvent,Assessment assessment, bool isNonOasis)
    ////    //{
    ////    //    var result = false;
    ////    //    var planofCare = new PlanofCare();
    ////    //    planofCare.Id = scheduleEvent.Id;
    ////    //    planofCare.AssessmentId = assessment.Id;
    ////    //    planofCare.AgencyId = Current.AgencyId;
    ////    //    planofCare.PatientId = scheduleEvent.PatientId;
    ////    //    planofCare.EpisodeId = scheduleEvent.EpisodeId;
    ////    //    planofCare.Status = scheduleEvent.Status;
    ////    //    planofCare.AssessmentType = assessment.Type.ToString();
    ////    //    planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
    ////    //    planofCare.IsNonOasis = isNonOasis;
    ////    //    var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, scheduleEvent.PatientId);
    ////    //    if (physician != null)
    ////    //    {
    ////    //        planofCare.PhysicianId = physician.Id;
    ////    //    }
    ////    //    planofCare.UserId = scheduleEvent.UserId;
    ////    //    planofCare.Questions = this.Get485FromAssessment(assessment);
    ////    //    planofCare.Data = planofCare.Questions.ToXml();

    ////    //    if (baseScheduleRepository.AddScheduleEvent(scheduleEvent))
    ////    //    {
    ////    //        if (planofCareRepository.Add(planofCare))
    ////    //        {
    ////    //            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
    ////    //            {
    ////    //                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
    ////    //            }
    ////    //            result = true;
    ////    //        }
    ////    //        else
    ////    //        {
    ////    //            baseScheduleRepository.RemoveScheduleEventNew(Current.AgencyId,scheduleEvent.PatientId, scheduleEvent.Id);
    ////    //            result = false;
    ////    //        }
    ////    //    }
    ////    //    return result;
    ////    //}

    ////    #endregion
    ////}


    ////public class HHPlanOfCareService : PlanOfCareService<ScheduleEvent>
    ////{
    ////    private HHTaskRepository scheduleRepository;
    ////    public HHPlanOfCareService(HHDataProvider agencyManagementDataProvider, HHTaskRepository scheduleRepository)
    ////        : base(agencyManagementDataProvider.TaskRepository)
    ////    {
    ////        base.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
    ////        base.agencyRepository = agencyManagementDataProvider.AgencyRepository;
    ////        base.patientRepository = agencyManagementDataProvider.PatientRepository;
    ////        this.scheduleRepository = agencyManagementDataProvider.TaskRepository;
    ////        // this.planofCareRepository = agencyManagementDataProvider.PlanofCareRepository;
    ////    }

    ////    public DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId)
    ////    {
    ////        var dateRange = new DateRange();
    ////        dateRange.IsLinkedToAssessment = false;
    ////        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
    ////        if (episode != null)
    ////        {
    ////            var scheduleEvent = scheduleRepository.GetLastScheduledEvent(Current.AgencyId, episodeId, patientId, episode.StartDate, episode.EndDate, episode.EndDate.AddDays(-5), episode.EndDate, DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray());
    ////            if (scheduleEvent != null && scheduleEvent.Id == assessmentId)
    ////            {
    ////                var nextEpisode = patientRepository.GetNextEpisodeByStartDate(Current.AgencyId, patientId, episode.EndDate.AddDays(1));
    ////                if (nextEpisode != null)
    ////                {
    ////                    dateRange.StartDate = nextEpisode.StartDate;
    ////                    dateRange.EndDate = nextEpisode.EndDate;
    ////                }
    ////                else
    ////                {
    ////                    dateRange.StartDate = episode.EndDate.AddDays(1);
    ////                    dateRange.EndDate = episode.EndDate.AddDays(60);
    ////                }
    ////                dateRange.IsLinkedToAssessment = true;
    ////            }
    ////            else
    ////            {
    ////                dateRange.StartDate = episode.StartDate;
    ////                dateRange.EndDate = episode.EndDate;
    ////            }
    ////        }
    ////        return dateRange;
    ////    }

    ////    protected override IPlanOfCare POCServiceSpecificPrintData(bool isStandAlone, IPlanOfCare planofCare)
    ////    {
    ////        var patient = patientRepository.GetPatientOnly(planofCare.PatientId, Current.AgencyId) ?? new Patient();
    ////        if (!planofCare.EpisodeId.IsEmpty())
    ////        {
    ////            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
    ////            if (episode != null && !episode.AdmissionId.IsEmpty())
    ////            {
    ////                var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
    ////                if (admission != null && admission.PatientData.IsNotNullOrEmpty() && admission.StartOfCareDate > DateTime.MinValue)
    ////                {
    ////                    if (planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || planofCare.Status == (int)ScheduleStatus.OrderSavedByPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || planofCare.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
    ////                    {
    ////                        patient = admission.PatientData.ToObject<Patient>();
    ////                    }
    ////                    if (patient != null)
    ////                    {
    ////                        patient.StartofCareDate = admission.StartOfCareDate;
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
    ////        if (!isStandAlone)
    ////        {
    ////            var episodeRange = GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
    ////            if (episodeRange != null)
    ////            {
    ////                planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
    ////                planofCare.EpisodeStart = episodeRange.StartDateFormatted;
    ////            }
    ////        }
    ////        else
    ////        {
    ////            var answers = planofCare.ToDictionary();
    ////            if (answers != null)
    ////            {
    ////                var episodeAssociatedId = answers.AnswerOrEmptyGuid("EpisodeAssociated");
    ////                if (!episodeAssociatedId.IsEmpty())
    ////                {
    ////                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeAssociatedId, planofCare.PatientId);
    ////                    if (episode != null)
    ////                    {
    ////                        planofCare.EpisodeEnd = episode.EndDateFormatted;
    ////                        planofCare.EpisodeStart = episode.StartDateFormatted;
    ////                    }
    ////                }
    ////            }
    ////        }
    ////        return planofCare;
    ////    }


    ////}

}
