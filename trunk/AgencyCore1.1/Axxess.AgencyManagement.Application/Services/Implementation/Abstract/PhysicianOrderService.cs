﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Transactions;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Membership.Logging;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.ViewData;

    using SubSonic.DataProviders;

    public abstract class PhysicianOrderService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;
        protected IPhysicianRepository physicianRepository;

        private PhysicianOrderAbstract baseNoteRepository;
        private TaskScheduleAbstract<T> baseScheduleRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;
        protected AgencyServices Service { get; set; }

        protected PhysicianOrderService(TaskScheduleAbstract<T> baseScheduleRepository,
            PhysicianOrderAbstract baseNoteRepository,
            EpisodeAbstract<E> baseEpisodeRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseNoteRepository = baseNoteRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
        }

        #region Physician Order

        public OrderViewData NewOrderViewData(Guid patientId, Guid episodeId, AgencyServices service)
        {
            var viewdata = new OrderViewData(service);
            viewdata.PatientId = patientId;
            viewdata.EpisodeId = episodeId;
            if (!patientId.IsEmpty())
            {
                var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    viewdata.PhysicianId = physician.Id;
                }
            }
            return viewdata;
        }

        public JsonViewData AddScheduleTaskAndPhysicianOrderHelper(Guid patientId, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected physician order task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var physicianOrders = this.GetPhysicianOrderFromTasks(patientId, scheduleEvents);
                if (physicianOrders != null)
                {
                    if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (baseNoteRepository.AddMultipleOrder(physicianOrders))
                        {
                            viewData.isSuccessful = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            viewData.IsPhysicianOrderPOCRefresh = true;
                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }

                }
            }
            return viewData;
        }

        public JsonViewData ProcessPhysicianOrder(Guid patientId, Guid eventId, string actionType, string reason)
        {
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                return ProcessPhysicianOrder(scheduleEvent, actionType, reason);
            }
            return new JsonViewData(false, "Unable to find order. Please try again.");
        }

        public JsonViewData ProcessPhysicianOrder(T task, string actionType, string reason)
        {
            var viewData = new JsonViewData(false);
            bool isOrderUpdates = true;
            bool isActionSet = false;
            if (task != null)
            {
                var description = string.Empty;
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var physicianOrder = baseNoteRepository.GetPhysicianOrder(task.Id, task.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            viewData.PatientId = task.PatientId;
                            viewData.EpisodeId = task.EpisodeId;
                            viewData.UserIds = new List<Guid> { task.UserId };

                            var oldStatus = task.Status;
                            if (actionType == "Approve")
                            {
                                physicianOrder.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                                task.Status = physicianOrder.Status;
                                task.InPrintQueue = true;
                                description = "Approved By:" + Current.UserFullName;
                                isActionSet = true;
                            }
                            else if (actionType == "Return")
                            {
                                physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                                task.Status = physicianOrder.Status;
                                description = "Returned By: " + Current.UserFullName;
                                isActionSet = true;
                            }
                            else if (actionType == "Print")
                            {
                                task.InPrintQueue = false;
                                isOrderUpdates = false;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (baseScheduleRepository.UpdateScheduleTask(task))
                                {
                                    if (isOrderUpdates)
                                    {
                                        bool returnCommentStatus = true;
                                        if (reason.IsNotNullOrEmpty())
                                        {
                                            returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                        }
                                        if (returnCommentStatus)
                                        {
                                            physicianOrder.Modified = DateTime.Now;
                                            if (baseNoteRepository.UpdateOrderModel(physicianOrder))
                                            {
                                                viewData.IsDataCentersRefresh = oldStatus != task.Status;
                                                viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == (int)ScheduleStatus.OrderSubmittedPendingReview;
                                                viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == task.UserId && !task.IsComplete && task.EventDate.Date >= DateTime.Now.AddDays(-89) && task.EventDate.Date <= DateTime.Now.AddDays(14);
                                                viewData.IsOrdersToBeSentRefresh = viewData.IsDataCentersRefresh && task.Status == (int)ScheduleStatus.OrderToBeSentToPhysician;
                                                viewData.IsPhysicianOrderPOCRefresh = viewData.IsDataCentersRefresh;
                                                viewData.Service = Service.ToString();
                                                viewData.ServiceId = (int)Service;
                                                viewData.isSuccessful = true;
                                                ts.Complete();
                                            }
                                            else
                                            {
                                                ts.Dispose();
                                                viewData.errorMessage = "A problem occured while updating the Plan of Care.";
                                            }
                                        }
                                        else
                                        {
                                            viewData.errorMessage = "A problem occured while adding the return comment.";
                                            ts.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                        ts.Complete();
                                    }
                                }
                                else
                                {
                                    viewData.errorMessage = "A problem occured while updating the task.";
                                }
                            }
                        }
                    }
                }
                if (viewData.isSuccessful && isOrderUpdates && Enum.IsDefined(typeof(ScheduleStatus), task.Status))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, Actions.StatusChange, (ScheduleStatus)task.Status, DisciplineTasks.PhysicianOrder, description);
                }
            }
            return viewData;
        }

        public JsonViewData ReopenPhysicianOrder(T scheduleTask)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleTask != null)
            {
                var oldUserId = scheduleTask.UserId;
                var oldStatus = scheduleTask.Status;
                var description = string.Empty;

                viewData.PatientId = scheduleTask.PatientId;
                viewData.EpisodeId = scheduleTask.EpisodeId;
                viewData.UserIds = new List<Guid> { scheduleTask.UserId };
                scheduleTask.Status = ((int)ScheduleStatus.OrderReopened);
                var physicianOrder = baseNoteRepository.GetPhysicianOrder(scheduleTask.Id, scheduleTask.PatientId, Current.AgencyId);
                if (physicianOrder != null)
                {
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                    {
                        physicianOrder.UserId = scheduleTask.UserId;
                        physicianOrder.Status = (int)ScheduleStatus.OrderReopened;
                        physicianOrder.SignatureText = string.Empty;
                        physicianOrder.SignatureDate = DateTime.MinValue;
                        physicianOrder.PhysicianSignatureText = string.Empty;
                        physicianOrder.PhysicianSignatureDate = DateTime.MinValue;
                        physicianOrder.ReceivedDate = DateTime.MinValue;
                        if (baseNoteRepository.UpdateOrderModel(physicianOrder))
                        {
                            viewData.isSuccessful = true;
                            viewData.Service = this.Service.ToString();
                            viewData.ServiceId = (int)this.Service;
                            viewData.IsPhysicianOrderPOCRefresh = true;
                            viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.OrderSentToPhysician) || oldStatus == ((int)ScheduleStatus.OrderSentToPhysicianElectronically);
                            viewData.IsDataCentersRefresh = true;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduleTask);

                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleTask.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleTask.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleTask);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData TogglePhysicianOrder(Guid patientId, Guid orderId, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Physician Order could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };

            try
            {
                var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, orderId);
                if (task != null)
                {
                    viewData = TogglePhysicianOrder(task, isDeprecated);
                }
                return viewData;

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return viewData;
            }
        }


        //TODO: Remove episode id from delete url of physician orders
        public JsonViewData TogglePhysicianOrder(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Physician Order could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };

            try
            {
                if (task != null)
                {
                    if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                    {
                        if (baseNoteRepository.MarkOrderAsDeleted(task.Id, task.PatientId, Current.AgencyId, isDeprecated))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                            viewData.isSuccessful = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            viewData.PatientId = task.PatientId;
                            viewData.EpisodeId = task.EpisodeId;
                            viewData.UserIds = new List<Guid> { task.UserId };
                            viewData.IsPhysicianOrderPOCRefresh = true;
                            viewData.IsOrdersPendingRefresh = ScheduleStatusFactory.OrdersPendingPhysicianSignature().Contains(task.Status);
                            viewData.IsOrdersToBeSentRefresh = task.Status == (int)ScheduleStatus.OrderToBeSentToPhysician;
                            viewData.IsOrdersHistoryRefresh = task.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            viewData.errorMessage = string.Format("The Physician Order has been {0}.", isDeprecated ? "deleted" : "restored");
                        }
                        else
                        {
                            baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                            viewData.isSuccessful = false;
                        }
                    }
                }
                return viewData;

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return viewData;
            }
        }

        public JsonViewData AddPhysicianOrder(PhysicianOrder order)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New order could not be saved." };
            if (order.IsValid())
            {
                if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    if (order.SignatureText.IsNullOrEmpty() || !ServiceHelper.IsSignatureCorrect(Current.UserId, order.SignatureText))
                    {
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return viewData;
                    }
                    else
                    {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                        if (isQaByPass)
                        {
                            order.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        }
                    }
                }
                else
                {
                    order.SignatureText = string.Empty;
                }
                viewData = this.AddPhysicianOrderAfterValidation(order);
            }
            else
            {
                viewData.errorMessage = order.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData AddPhysicianOrderAfterValidation(PhysicianOrder order)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New order could not be saved." };
            order.Id = Guid.NewGuid();
            order.UserId = Current.UserId;
            order.AgencyId = Current.AgencyId;
            order.OrderNumber = patientRepository.GetNextOrderNumber();
            order.Created = DateTime.Now;
            T newScheduleEvent = TaskHelperFactory<T>.CreateTask(order.Id, order.UserId, order.EpisodeId, order.PatientId, order.Status, Disciplines.Orders, order.OrderDate, (int)DisciplineTasks.PhysicianOrder, order.IsOrderForNextEpisode);
            if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
            {
                if (!order.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        order.PhysicianData = physician.ToXml();
                    }
                }
            }
            if (baseScheduleRepository.AddScheduleTask(newScheduleEvent))
            {
                if (baseNoteRepository.AddOrder(order))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, order.EpisodeId, order.PatientId, order.Id, Actions.Add, (DisciplineTasks)newScheduleEvent.DisciplineTask);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Order has been saved successfully.";
                    if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                    {
                        viewData.errorMessage = "Order has been completed successfully.";
                    }
                    viewData.UserIds = new List<Guid> { newScheduleEvent.UserId };
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                    viewData.PatientId = newScheduleEvent.PatientId;
                    viewData.EpisodeId = newScheduleEvent.EpisodeId;
                }
                else
                {
                    baseScheduleRepository.RemoveScheduleTaskFully(Current.AgencyId, order.PatientId, order.Id);
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Order could not be saved! Please try again.";
            }
            return viewData;
        }

        public JsonViewData UpdatePhysicianOrder(PhysicianOrder order)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated." };
            order.AgencyId = Current.AgencyId;
            if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
            {
                if (order.SignatureText.IsNullOrEmpty() || !ServiceHelper.IsSignatureCorrect(Current.UserId, order.SignatureText))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                    return viewData;
                }
                else
                {
                    order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                    if (isQaByPass)
                    {
                        order.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                    }

                }
            }
            else
            {
                order.SignatureText = string.Empty;
            }
            viewData = this.UpdatePhysicianOrderAfterValidation(order);
            return viewData;
        }

        public bool UpdatePhysicianOrderForScheduleDetail(T schedule)
        {
            var result = false;
            var physicianOrder = baseNoteRepository.GetPhysicianOrderOnly(schedule.Id, schedule.PatientId, Current.AgencyId);
            if (physicianOrder != null)
            {
                physicianOrder.Status = schedule.Status;
                if (schedule.VisitDate.IsValid())
                {
                    physicianOrder.OrderDate = schedule.VisitDate;
                }
                if (physicianOrder.Status != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        physicianOrder.PhysicianData = physician.ToXml();
                    }
                }
                physicianOrder.EpisodeId = schedule.EpisodeId;
                physicianOrder.PhysicianId = schedule.PhysicianId;
                physicianOrder.IsDeprecated = schedule.IsDeprecated;
                result = baseNoteRepository.UpdateOrderModel(physicianOrder);
            }
            else
            {
                result = true;
            }
            return result;
        }

        public JsonViewData UpdatePhysicianOrderAfterValidation(PhysicianOrder order)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated." };

            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, order.PatientId, order.Id);
            if (scheduleEvent != null)
            {
                var oldStatus = scheduleEvent.Status;
                var oldIsOrderForNextEpisode = scheduleEvent.IsOrderForNextEpisode;
                var oldVisitDate = scheduleEvent.VisitDate;

                scheduleEvent = TaskHelperFactory<T>.SetNoteProperties(scheduleEvent, order.OrderDate, order.Status);
                scheduleEvent.IsOrderForNextEpisode = order.IsOrderForNextEpisode;

                if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                {
                    if (!order.PhysicianId.IsEmpty())
                    {
                        var physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            order.PhysicianData = physician.ToXml();
                        }
                    }
                }
                if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                {
                    if (baseNoteRepository.UpdateOrder(order))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Order has been updated successfully.";
                        if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                        {
                            viewData.errorMessage = "Order has been completed and pending QA Review.";
                        }
                        var isStatusChange = oldStatus != scheduleEvent.Status;
                        var isMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(isStatusChange, scheduleEvent);
                        viewData.UserIds = new List<Guid> { scheduleEvent.UserId };
                        viewData.IsDataCentersRefresh = isStatusChange;
                        viewData.IsCaseManagementRefresh = isStatusChange && (oldStatus == (int)ScheduleStatus.OrderSubmittedPendingReview || scheduleEvent.Status == (int)ScheduleStatus.OrderSubmittedPendingReview);
                        viewData.PatientId = order.PatientId;
                        viewData.EpisodeId = order.EpisodeId;
                        viewData.IsMyScheduleTaskRefresh = isMyScheduleTaskRefresh;
                        viewData.IsOrdersToBeSentRefresh = isStatusChange && order.Status == ((int)ScheduleStatus.OrderToBeSentToPhysician);
                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Edit, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
                        }
                    }
                    else
                    {
                        scheduleEvent = TaskHelperFactory<T>.SetNoteProperties(scheduleEvent, oldVisitDate, oldStatus);
                        scheduleEvent.IsOrderForNextEpisode = oldIsOrderForNextEpisode;
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Order could not be saved! Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Order could not be saved! Please try again.";
            }

            return viewData;
        }

        public PhysicianOrder GetPhysicianOrder(Guid patientId, Guid Id)
        {
            return baseNoteRepository.GetPhysicianOrder(Id, patientId, Current.AgencyId);
        }

        public PhysicianOrder GetPhysicianOrderForEdit(Guid patientId, Guid Id)
        {
            var order = baseNoteRepository.GetPhysicianOrder(Id, patientId, Current.AgencyId);
            if (order != null)
            {
                order.Service = this.Service;
                if (order.Status == (int)ScheduleStatus.OrderNotYetDue)
                {
                    order.PhysicianId = physicianRepository.GetPatientPrimaryOrFirstPhysicianId(Current.AgencyId, patientId);
                }
                order.DisplayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                if (!order.EpisodeId.IsEmpty())
                {
                    var episode = baseEpisodeRepository.GetEpisodeDateRange(Current.AgencyId, order.PatientId, order.EpisodeId);
                    if (episode != null)
                    {
                        order.EpisodeEndDate = episode.EndDateFormatted;
                        order.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                var permission = Current.Permissions;
                if (permission.IsNotNullOrEmpty())
                {
                    order.IsUserCanEdit = permission.IsInPermission(this.Service, ParentPermission.Orders, PermissionActions.Edit);
                    order.IsUserCanAddPhysicain = !Current.IsAgencyFrozen && permission.IsInPermission(this.Service, ParentPermission.Physician, PermissionActions.Add);

                    // not used anywhere
                    order.IsUserCanSeeSticky = !Current.IsAgencyFrozen && permission.IsInPermission(this.Service, ParentPermission.Schedule, PermissionActions.ViewStickyNotes);
                    if (order.IsUserCanSeeSticky)
                    {
                        order.StatusComment = baseScheduleRepository.GetReturnReason(Current.AgencyId, order.PatientId, order.Id, Current.UserId);
                    }
                }


            }
            return order;
        }

        public PhysicianOrder GetPhysicianOrderPrint()
        {
            var order = new PhysicianOrder();
            order.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
            return order;
        }

        public PhysicianOrder GetPhysicianOrderPrint(Guid patientId, Guid orderId)
        {
            var order = baseNoteRepository.GetPhysicianOrder(orderId, patientId, Current.AgencyId);
            if (order == null)
            {
                order = new PhysicianOrder();
            }
            order.PatientProfile = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
            if (order.PatientProfile != null)
            {
                order.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, order.PatientProfile.AgencyLocationId);
                if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
                {
                    var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        order.Physician = physician;
                    }
                    else
                    {
                        order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                    }
                }
                else
                {
                    order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                }
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    order.Allergies = allergyProfile.ToString();
                }
                SetEpisode(order);
            }
            return order;
        }

        protected abstract void SetEpisode(PhysicianOrder order);

        public List<PhysicianOrder> GetPhysicianOrderFromTasks(Guid patientId, List<T> scheduleEvents)
        {
            var physicianOrders = new List<PhysicianOrder>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var physicianId = Guid.Empty;
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    physicianId = physician.Id;
                }
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var order = new PhysicianOrder
                    {
                        Id = scheduleEvent.Id,
                        AgencyId = Current.AgencyId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        PatientId = scheduleEvent.PatientId,
                        UserId = scheduleEvent.UserId,
                        OrderDate = scheduleEvent.EventDate,
                        Created = DateTime.Now,
                        Text = "",
                        Summary = "",
                        Status = scheduleEvent.Status,
                        OrderNumber = patientRepository.GetNextOrderNumber(),
                        PhysicianId = physicianId,
                        Modified = DateTime.Now

                    };
                    physicianOrders.Add(order);

                });
            }
            return physicianOrders;
        }

        #endregion

        #region Orders

        public bool MarkPhysicianOrderAsReturned(Guid id, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var order = baseNoteRepository.GetPhysicianOrderOnly(id, Current.AgencyId);
            if (order != null)
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, order.PatientId, order.Id);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;

                    if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!order.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        order.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        order.ReceivedDate = receivedDate;
                        order.PhysicianSignatureDate = physicianSignatureDate;
                        if (baseNoteRepository.UpdateOrderModel(order))
                        {
                            result = true;
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                        }
                        else
                        {
                            result = false;
                            scheduleEvent.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                        }
                    }
                }
            }
            return result;
        }

        public int MarkPhysicianOrderAsSent(bool sendElectronically, string[] answersArray, ref List<AgencyPhysician> physicians)
        {
            var count = 0;
            AgencyPhysician physician = null;
            var status = (int)ScheduleStatus.OrderSentToPhysician;
            if (sendElectronically)
            {
                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
            }
            foreach (var item in answersArray)
            {
                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                var order = baseNoteRepository.GetPhysicianOrderOnly(orderId, Current.AgencyId);
                if (order.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = order.PhysicianData.ToObject<AgencyPhysician>();
                }
                if (order != null)
                {
                    var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, order.PatientId, order.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = status;
                        if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                        {
                            if (baseNoteRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue, DateTime.Now))
                            {
                                count++;
                                if (sendElectronically)
                                {
                                    if (physician != null && !physicians.Exists(p => physician.Id == p.Id))
                                    {
                                        physicians.Add(physician);
                                    }
                                }
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
                                }
                            }
                            else
                            {

                                scheduleEvent.Status = oldStatus;
                                baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                                //return;
                            }
                        }

                    }

                }

            }
            return count;
        }

        public bool UpdatePhysicianOrderDates(Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            var result = false;
            var order = baseNoteRepository.GetPhysicianOrderOnly(id, Current.AgencyId);
            if (order != null)
            {
                order.ReceivedDate = receivedDate;
                order.SentDate = sendDate;
                order.PhysicianSignatureDate = physicianSignatureDate;
                if (order.PhysicianSignatureText.IsNullOrEmpty())
                {
                    var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    else if (!order.PhysicianId.IsEmpty())
                    {
                        physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                    }
                }
                result = baseNoteRepository.UpdateOrderModel(order);
            }
            return result;
        }

        public Order GetPhysicianOrderForHistoryEdit(Guid patientId, Guid id)
        {
            var order = new Order();
            var physicianOrder = baseNoteRepository.GetPhysicianOrder(id, Current.AgencyId);
            if (physicianOrder != null)
            {
                order = new Order
                {
                    Id = physicianOrder.Id,
                    PatientId = physicianOrder.PatientId,
                    EpisodeId = physicianOrder.EpisodeId,
                    Type = OrderType.PhysicianOrder,
                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                    Number = physicianOrder.OrderNumber,
                    PatientName = physicianOrder.DisplayName,
                    PhysicianName = physicianOrder.PhysicianName,
                    ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
                    SendDate = physicianOrder.SentDate,
                    PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
                };
            }
            return order;
        }

        public Order GetPhysicianOrderForReceiving(Guid patientId, Guid id)
        {
            var order = new Order();
            var physicianOrder = baseNoteRepository.GetPhysicianOrder(id, patientId, Current.AgencyId);
            if (physicianOrder != null)
            {
                order = new Order
                {
                    Id = physicianOrder.Id,
                    PatientId = physicianOrder.PatientId,
                    EpisodeId = physicianOrder.EpisodeId,
                    Type = OrderType.PhysicianOrder,
                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                    PhysicianName = physicianOrder.PhysicianName,
                    ReceivedDate = physicianOrder.ReceivedDate,
                    PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
                };
            }
            return order;
        }

        public List<Order> GetPhysicianOrders(DateTime startDate, DateTime endDate, List<T> physicianOrdersSchedules, bool isPending, bool isCompleted)
        {
            var orders = new List<Order>();
            var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var physicianOrders = baseNoteRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                var canPrint = Current.Permissions.IsInPermission(Service, ParentPermission.Orders, PermissionActions.Print);
                physicianOrders.ForEach(po =>
                {
                    var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                    var order = new Order
                    {
                        Id = po.Id,
                        PatientId = po.PatientId,
                        EpisodeId = po.EpisodeId,
                        Type = OrderType.PhysicianOrder,
                        Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                        Number = po.OrderNumber,
                        PatientName = po.DisplayName,
                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                        PhysicianAccess = physician != null && physician.PhysicianAccess,
                        CreatedDate = po.OrderDate,
                        Service = this.Service,
                        DocumentType = DocumentType.PhysicianOrder.ToString().ToLower(),
                        CanUserPrint = canPrint

                    };
                    if (isCompleted)
                    {
                        order.SendDate = po.SentDate;
                        order.ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate;
                        order.PhysicianSignatureDate = po.PhysicianSignatureDate;
                    }
                    else if (isPending)
                    {
                        order.SendDate = po.SentDate;
                        order.ReceivedDate = DateTime.Today;
                        order.PhysicianSignatureDate = po.PhysicianSignatureDate;
                    }
                    orders.Add(order);
                });
            }
            return orders;
        }

        public List<Order> GetPhysicianOrders(Guid patientId, DateTime startDate, DateTime endDate, List<T> physicianOrdersSchedules)
        {
            var orders = new List<Order>();
            var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var physicianOrders = baseNoteRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, physicianOrdersIds, startDate, endDate);
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Orders);
                var canPrint = permissions.GetOrDefault((int)PermissionActions.Print, new List<int>()).Contains((int)Service);
                var canDelete = permissions.GetOrDefault((int)PermissionActions.Delete, new List<int>()).Contains((int)Service);
                physicianOrders.ForEach(po =>
                {
                    var task = physicianOrdersSchedules.FirstOrDefault(t => t.Id == po.Id);
                    if (task != null)
                    {
                        AgencyPhysician physician = null;
                        if (!po.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                        }
                        else
                        {
                            if (po.PhysicianData.IsNotNullOrEmpty())
                            {
                                var oldPhysician = po.PhysicianData.ToObject<AgencyPhysician>();
                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                }
                            }
                        }
                        orders.Add(new Order
                        {
                            Id = po.Id,
                            PatientId = po.PatientId,
                            EpisodeId = po.EpisodeId,
                            Type = OrderType.PhysicianOrder,
                            Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                            Number = po.OrderNumber,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null && physician.PhysicianAccess,
                            DocumentType = DocumentType.PhysicianOrder.ToString().ToLower(),
                            CreatedDate = po.OrderDate,
                            ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue,
                            SendDate = po.Status == (int)ScheduleStatus.OrderSentToPhysician || po.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || po.Status == (int)ScheduleStatus.OrderSavedByPhysician ? po.SentDate : DateTime.MinValue,
                            Status = po.Status,
                            Service = Service,
                            CanUserPrint = canPrint,
                            CanUserDelete = canDelete
                        });
                    }
                });
            }
            return orders;
        }

        #endregion
    }
}
