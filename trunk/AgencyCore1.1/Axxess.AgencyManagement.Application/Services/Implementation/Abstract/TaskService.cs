﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Transactions;

    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Helpers;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;

    using SubSonic.DataProviders;

    public abstract class TaskService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected ILookupRepository lookupRepository;
        protected IUserRepository userRepository;
        protected IPhysicianRepository physicianRepository;
        protected IAssetRepository assetRepository;
        protected AgencyServices Service { get; set; }

        protected PatientProfileAbstract profileRepository;
        protected PatientAdmissionAbstract patientAdmissionRepository;
        private TaskScheduleAbstract<T> baseScheduleRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;
        private MultiDocumentAbstract baseNoteRepository;

        protected TaskService(TaskScheduleAbstract<T> baseScheduleRepository,
           MultiDocumentAbstract baseNoteRepository,
            EpisodeAbstract<E> baseEpisodeRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseNoteRepository = baseNoteRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
        }

        #region Public Methods

        public T GetScheduleTask(Guid patientId, Guid eventId)
        {
            return baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
        }

        public bool UpdateScheduleTask(T scheduleEvent)
        {
            return baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
        }

        #region Patient Center Schedule Activities

        public PatientScheduleEventViewData GetPatientScheduleEventViewData(Guid patientId, string discipline, DateRange range)
        {
            var patientWithSchedule = new PatientScheduleEventViewData();
            SetPermission(patientWithSchedule);
            patientWithSchedule.DisciplineFilterType = discipline;
            patientWithSchedule.DateFilterType = range.Id;
            patientWithSchedule.Service = this.Service;
            if (range.Id.IsEqual("ThisEpisode") || range.Id.IsEqual("NextEpisode") || range.Id.IsEqual("LastEpisode"))
            {
                E patientEpisode = null;
                if (range.Id.IsEqual("ThisEpisode"))
                {
                    patientEpisode = baseEpisodeRepository.GetCurrentEpisodeLean(Current.AgencyId, patientId);
                }
                else if (range.Id.IsEqual("NextEpisode"))
                {
                    patientEpisode = baseEpisodeRepository.GetNextEpisodeDataLean(Current.AgencyId, patientId, DateTime.Now);
                }
                else if (range.Id.IsEqual("LastEpisode"))
                {
                    patientEpisode = baseEpisodeRepository.GetPreviousEpisodeDataLean(Current.AgencyId, patientId, DateTime.Now);
                }
                if (patientEpisode != null)
                {
                    patientEpisode.PatientId = patientId;
                    patientWithSchedule.StartDate = patientEpisode.StartDate;
                    patientWithSchedule.EndDate = patientEpisode.EndDate;
                    var patientEvents = baseScheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, false);
                    if (patientEvents.IsNotNullOrEmpty())
                    {
                        ProcessScheduleEventsActions2(patientWithSchedule, patientEpisode, patientEvents);
                    }
                }
            }
            else if (range.Id.IsEqual("all"))
            {
                var patientEpisodes = baseEpisodeRepository.GetPatientActiveEpisodesLean(Current.AgencyId, patientId);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    var patientEvents = baseScheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, patientEpisodes.Select(e => e.Id).Distinct().ToArray(), range.StartDate, range.EndDate, discipline, false, false);
                    if (patientEvents != null && patientEvents.Count > 0)
                    {
                        ProcessScheduleEventsActions2(patientWithSchedule, patientEpisodes, patientEvents, false);
                    }
                }
            }
            else
            {
                if ((range.StartDate.Date.IsValid() && range.EndDate.Date.IsValid()))
                {
                    patientWithSchedule.StartDate = range.StartDate;
                    patientWithSchedule.EndDate = range.EndDate;
                    var patientEpisodes = baseEpisodeRepository.GetPatientActiveEpisodesLeanByDateRange(Current.AgencyId, patientId, range.StartDate, range.EndDate);
                    if (patientEpisodes != null && patientEpisodes.Count > 0)
                    {
                        var patientEvents = baseScheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, patientEpisodes.Select(e => e.Id).Distinct().ToArray(), range.StartDate, range.EndDate, discipline, false, true);
                        if (patientEvents != null && patientEvents.Count > 0)
                        {
                            ProcessScheduleEventsActions2(patientWithSchedule, patientEpisodes, patientEvents, false);
                        }
                    }
                }
            }
            return patientWithSchedule;
        }

        protected void SetPermission(TaskActivityViewData patientWithSchedule)
        {
            var permission = Current.Permissions;
            var schedulePermission = permission.GetCategoryPermission(ParentPermission.Schedule);
            if (schedulePermission.IsNotNullOrEmpty())
            {
                var scheduleInt = (int)(DocumentType.Notes | DocumentType.OASIS);
                SetDocumentTypePermission(patientWithSchedule, schedulePermission, scheduleInt);
                if ((patientWithSchedule.ViewDocuments & scheduleInt) == scheduleInt)
                {
                    patientWithSchedule.IsUserCanPrint = (patientWithSchedule.PrintDocuments & scheduleInt) == scheduleInt;
                    patientWithSchedule.IsUserCanDelete = (patientWithSchedule.DeleteDocuments & scheduleInt) == scheduleInt;
                    patientWithSchedule.IsUserCanRestore = (patientWithSchedule.RestoreDocuments & scheduleInt) == scheduleInt;
                    patientWithSchedule.IsUserCanEdit = (patientWithSchedule.EditDocuments & scheduleInt) == scheduleInt;
                    patientWithSchedule.IsOASISProfileExist = permission.IsInPermission(this.Service, ParentPermission.OASIS, PermissionActions.ViewOASISProfile);
                    patientWithSchedule.IsUserCanAdd = schedulePermission.IsInPermission(this.Service, PermissionActions.Add);
                }
                patientWithSchedule.IsUserCanEditDetail = schedulePermission.IsInPermission(this.Service, PermissionActions.ViewDetail);
                patientWithSchedule.IsStickyNote = schedulePermission.IsInPermission(this.Service, PermissionActions.ViewStickyNotes);
                patientWithSchedule.IsUserCanReassign = schedulePermission.IsInPermission(this.Service, PermissionActions.Reassign);
                patientWithSchedule.IsUserCanReopen = schedulePermission.IsInPermission(this.Service, PermissionActions.Reopen);
            }

            SetDocumentTypePermission(patientWithSchedule, permission.GetCategoryPermission(ParentPermission.Orders), (int)(DocumentType.PhysicianOrder | DocumentType.FaceToFaceEncounter | DocumentType.PlanOfCare));
            SetDocumentTypePermission(patientWithSchedule, permission.GetCategoryPermission(ParentPermission.ManageInfectionReport), (int)DocumentType.Infection);
            SetDocumentTypePermission(patientWithSchedule, permission.GetCategoryPermission(ParentPermission.ManageIncidentAccidentReport), (int)DocumentType.IncidentAccident);
            SetDocumentTypePermission(patientWithSchedule, permission.GetCategoryPermission(ParentPermission.CommunicationNote), (int)DocumentType.CommunicationNote);
        }

        private void SetDocumentTypePermission(TaskActivityViewData patientWithSchedule, Dictionary<int, List<int>> permissions, int aggregateDocuments)
        {
            if (permissions.IsNotNullOrEmpty())
            {
                if (permissions.IsInPermission(this.Service, PermissionActions.ViewList))
                {
                    var isUserCanPrint = permissions.IsInPermission(this.Service, PermissionActions.Print);
                    if (isUserCanPrint)
                    {
                        patientWithSchedule.PrintDocuments |= aggregateDocuments;
                    }
                    var isUserCanDelete = permissions.IsInPermission(this.Service, PermissionActions.Delete);
                    if (isUserCanDelete)
                    {
                        patientWithSchedule.DeleteDocuments |= aggregateDocuments;
                    }
                    var isUserCanRestore = permissions.IsInPermission(this.Service, PermissionActions.Restore);
                    if (isUserCanRestore)
                    {
                        patientWithSchedule.RestoreDocuments |= aggregateDocuments;
                    }
                    var isUserCanEdit = permissions.IsInPermission(this.Service, PermissionActions.Edit);
                    if (isUserCanEdit)
                    {
                        patientWithSchedule.EditDocuments |= aggregateDocuments;
                    }
                    patientWithSchedule.ViewDocuments |= aggregateDocuments;
                }
            }
        }

        public void CurrentEpisodePatientWithScheduleEvent(PatientScheduleEventViewData patientScheduleEventViewData, Guid patientId, string discipline)
        {
            if (patientScheduleEventViewData != null)
            {
                SetPermission(patientScheduleEventViewData);
                var patientEpisode = baseEpisodeRepository.GetCurrentEpisodeLeanNoPatientInfo(Current.AgencyId, patientId);
                if (patientEpisode != null)
                {
                    patientEpisode.PatientId = patientId;
                    patientScheduleEventViewData.StartDate = patientEpisode.StartDate;
                    patientScheduleEventViewData.EndDate = patientEpisode.EndDate;
                    var patientEvents = baseScheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, new Guid[] { patientEpisode.Id }, patientEpisode.StartDate, patientEpisode.EndDate, discipline, false, true);
                    if (patientEvents.IsNotNullOrEmpty())
                    {
                        ProcessScheduleEventsActions2(patientScheduleEventViewData, patientEpisode, patientEvents);
                    }
                }
            }
        }

        public void GetSchedulePermission(UserTaskViewData viewData)
        {
            var permissions = Current.Permissions;
            var isForzen = Current.IsAgencyFrozen;
            var schedulePermission = permissions.GetCategoryPermission(ParentPermission.Schedule);
            if (schedulePermission.IsNotNullOrEmpty())
            {
                viewData.IsStickyNote = schedulePermission.IsInPermission(this.Service, PermissionActions.ViewStickyNotes);
                viewData.IsUserCanPrint = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Print);
                viewData.IsUserCanExport = schedulePermission.IsInPermission(this.Service, PermissionActions.Export);
                viewData.IsUserCanAdd = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Add);
                viewData.IsUserCanEdit = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Edit);
                viewData.IsUserCanDelete = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Delete);
                viewData.IsUserCanReassign = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Reassign);
                viewData.IsUserCanEditDetail = schedulePermission.IsInPermission(this.Service, PermissionActions.ViewDetail);
                viewData.IsUserCanReopen = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Reopen);
                viewData.IsUserCanRestore = !isForzen && schedulePermission.IsInPermission(this.Service, PermissionActions.Restore);
            }

            var episodePermissions = permissions.GetCategoryPermission(ParentPermission.Episode);
            if (episodePermissions.IsNotNullOrEmpty())
            {
                viewData.IsUserCanAddEpisode = !isForzen && episodePermissions.IsInPermission(this.Service, PermissionActions.Add);
                viewData.IsUserCanViewEpisodeList = episodePermissions.IsInPermission(this.Service, PermissionActions.ViewList);
                viewData.IsUserCanEditEpisode = !isForzen && episodePermissions.IsInPermission(this.Service, PermissionActions.Edit);
            }
        }

        #endregion

        public List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task)
        {
            var taskLogs = new List<TaskLog>();
            var taskAudit = Auditor.GetTaskAudit(patientId, eventId, task);
            if (taskAudit != null && taskAudit.Log.IsNotNullOrEmpty())
            {
                var logs = taskAudit.Log.ToObject<List<TaskLog>>();
                if (logs != null && logs.Count > 0)
                {
                    var userIds = logs.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                    logs.ForEach(log =>
                    {

                        if (!log.UserId.IsEmpty())
                        {
                            var user = users.FirstOrDefault(u => u.Id == log.UserId);
                            if (user != null)
                            {
                                log.UserName = user.DisplayName;
                            }
                        }
                        taskLogs.Add(log);
                    });
                }
            }
            return taskLogs;
        }

        public ReassignViewData GetReassignViewData(Guid id, Guid patientId)
        {
            var viewData = new ReassignViewData();
            viewData.Service = this.Service;
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, id);
            if (task != null)
            {
                viewData.Id = id;
                viewData.UserId = task.UserId;
                viewData.PatientId = patientId;
                viewData.Type = task.DisciplineTaskName;
                TaskHelperFactory<T>.ReassignViewDataEventDate(viewData, task);
                viewData.OldUserName = UserEngine.GetName(task.UserId, Current.AgencyId);
                viewData.PatientDisplayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
            }
            return viewData;
        }

        public JsonViewData Reassign(Guid patientId, Guid eventId, Guid employeeId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            try
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    var oldUserId = scheduleEvent.UserId;
                    var isUserDifferent = oldUserId != employeeId;
                    scheduleEvent.UserId = employeeId;
                    var table = this.TableName(scheduleEvent.DisciplineTask);
                    var script = string.Empty;
                    if (table.IsNotNullOrEmpty())
                    {
                        script = string.Format(@"UPDATE {0} set UserId = @userId WHERE AgencyId = @agencyid AND PatientId = @patientId AND Id IN ( {1});", table, string.Format("'{0}'", scheduleEvent.Id));
                    }
                    if (script.IsNotNullOrEmpty())
                    {
                        if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                        {
                            if (baseNoteRepository.UpdateEntityForReassigningUser(Current.AgencyId, patientId, employeeId, script))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, patientId, scheduleEvent.Id, Actions.Reassigned, (DisciplineTasks)scheduleEvent.DisciplineTask, "( From " + UserEngine.GetName(oldUserId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                                viewData.isSuccessful = true;
                                if (isUserDifferent)
                                {
                                    viewData.PatientId = patientId;
                                    viewData.EpisodeId = scheduleEvent.EpisodeId;
                                    viewData.IsDataCentersRefresh = true;
                                    viewData.Service = Service.ToString();
                                    viewData.ServiceId = (int)Service;
                                    viewData.UserIds = new List<Guid> { employeeId, oldUserId };
                                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, scheduleEvent);
                                    viewData.IsCaseManagementRefresh = (scheduleEvent.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview) || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview) || scheduleEvent.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature) || scheduleEvent.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
                                    viewData.IsCommunicationNoteRefresh = scheduleEvent.DisciplineTask == (int)DisciplineTasks.CommunicationNote;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                                viewData.isSuccessful = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return viewData;
            }
            return viewData;
        }

        public JsonViewData ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var scheduleEvents = baseScheduleRepository.GetUsersScheduleTasksBetweenDatesByStatus(Current.AgencyId, patientId, employeeOldId, startDate, endDate, ScheduleStatusFactory.AllNoteNotYetStarted().ToArray());
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var eventsDictionary = scheduleEvents.GroupBy(s => s.DisciplineTask).ToDictionary(g => g.Key, g => g.ToList());
                if (eventsDictionary.IsNotNullOrEmpty())
                {
                    var script = string.Empty;
                    eventsDictionary.ForEach(
                        (key, value) =>
                        {
                            var table = this.TableName(key);
                            if (table.IsNotNullOrEmpty())
                            {
                                script += string.Format(@"UPDATE {0} set UserId = @userId WHERE AgencyId = @agencyid AND Id IN ( {1});", table, value.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                            }
                        });
                    if (script.IsNotNullOrEmpty())
                    {
                        var ids = scheduleEvents.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                        if (baseScheduleRepository.UpdateScheduleTasksForReassign(Current.AgencyId, employeeId, ids))
                        {
                            if (baseNoteRepository.UpdateEntityForReassigningUser(Current.AgencyId, employeeId, script))
                            {
                                viewData.UserIds = new List<Guid> { employeeId, employeeOldId };
                                viewData.Service = Service.ToString();
                                viewData.ServiceId = (int)Service;
                                viewData.PatientId = patientId;
                                viewData.IsDataCentersRefresh = true;
                                viewData.isSuccessful = true;
                                viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(employeeOldId, employeeId, scheduleEvents);
                                viewData.IsCommunicationNoteRefresh = scheduleEvents.Exists(s => s.DisciplineTask == (int)DisciplineTasks.CommunicationNote);
                                Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Reassigned, ScheduleStatus.NoStatus, string.Format("( From {0} To {1} )", UserEngine.GetName(employeeOldId, Current.AgencyId), UserEngine.GetName(employeeId, Current.AgencyId)));
                            }
                            else
                            {
                                baseScheduleRepository.UpdateScheduleTasksForReassign(Current.AgencyId, employeeOldId, ids);
                            }
                        }
                    }

                }
            }
            else
            {
                var patientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                viewData.errorMessage = string.Format("There are no reassignable tasks assigned to {0} for the patient {1} between the dates {2} and {3}. Try changing the parameters of the reassign.", UserEngine.GetName(employeeOldId, Current.AgencyId), patientName, startDate.ToZeroFilled(), endDate.ToZeroFilled());
            }
            return viewData;
        }

        public JsonViewData DeleteSchedules(Guid patientId, List<Guid> eventsToBeDeletedIds)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The selected task(s) could not be deleted." };
            if (!patientId.IsEmpty() && eventsToBeDeletedIds.IsNotNullOrEmpty())
            {
                var toBeDeletedEvents = baseScheduleRepository.GetScheduledEventsLeanWithId(Current.AgencyId, patientId, eventsToBeDeletedIds);
                if (toBeDeletedEvents.IsNotNullOrEmpty())
                {
                    var eventsDictionary = toBeDeletedEvents.GroupBy(s => s.DisciplineTask).ToDictionary(g => g.Key, g => g.ToList());
                    if (eventsDictionary.Count > 0)
                    {
                        var script = string.Empty;
                        viewData.UserIds = new List<Guid>();
                        eventsDictionary.ForEach((key, value) =>
                        {
                            viewData.UserIds.AddRange(value.Select(s => s.UserId));
                            var table = this.TableName(key);
                            if (table.IsNotNullOrEmpty())
                            {
                                script += string.Format(@"UPDATE {0} set IsDeprecated = 1 , Modified = @modified WHERE AgencyId = @agencyid AND  PatientId = @patientId   AND Id IN ( {1});", table, value.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                            }
                            if (key == (int)DisciplineTasks.CommunicationNote)
                            {
                                viewData.IsCommunicationNoteRefresh = true;
                            }
                        });
                        if (script.IsNotNullOrEmpty())
                        {
                            var filteredEventsToBeDeletedIds = toBeDeletedEvents.Select(s => s.Id).Distinct().ToList();
                            if (filteredEventsToBeDeletedIds.IsNotNullOrEmpty())
                            {
                                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, patientId, filteredEventsToBeDeletedIds, true))
                                {
                                    if (baseNoteRepository.UpdateEntityForDelete(Current.AgencyId, patientId, script))
                                    {
                                        viewData.PatientId = patientId;
                                        viewData.IsDataCentersRefresh = true;
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "The selected task(s) sucessfully deleted.";
                                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, toBeDeletedEvents);
                                        Auditor.MultiLog(Current.AgencyId, toBeDeletedEvents, Actions.Deleted, ScheduleStatus.NoStatus, string.Empty);
                                    }
                                    else
                                    {
                                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, patientId, filteredEventsToBeDeletedIds, false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!viewData.isSuccessful)
            {
                viewData.UserIds = null;
            }
            return viewData;
        }

        public JsonViewData MarkScheduleTasksAsPrinted(List<Guid> ids)
        {
            var viewData = new JsonViewData { errorMessage = "Failed to mark any tasks as printed.", isSuccessful = false };
            if (ids != null && ids.Count > 0)
            {
                int numberOfTasks = baseScheduleRepository.MarkScheduleTasksAsPrinted(Current.AgencyId, ids);
                if (numberOfTasks == ids.Count)
                {
                    viewData.errorMessage = string.Format("All ({0}) tasks were marked as printed successfully.", numberOfTasks, ids.Count);
                    viewData.isSuccessful = true;
                }
                else if (numberOfTasks > 0)
                {
                    viewData.errorMessage = string.Format("{0} out of {1} tasks were marked successfully. Try them again.", numberOfTasks, ids.Count);
                }
            }
            return viewData;
        }

        public List<TaskLean> GetPrintQueue(Guid BranchId, DateTime StartDate, DateTime EndDate, bool isPrintUrlNeeded)
        {
            var events = new List<TaskLean>();
            var scheduleEvents = baseScheduleRepository.GetPrintQueueTasks(Current.AgencyId, BranchId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var users = new List<User>();
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                }
                scheduleEvents.ForEach(e =>
                {
                    var user = users.FirstOrDefault(u => u.Id == e.UserId);
                    events.Add(new TaskLean
                    {
                        Id = e.Id,
                        PatientId = e.PatientId,
                        StatusName = e.StatusName,
                        PatientName = e.PatientName,
                        DisciplineTaskName = e.DisciplineTaskName,
                        Type = isPrintUrlNeeded ? DocumentFactory.GetType(e.DisciplineTask, e.IsMissedVisit).ToString().ToLowerCase() : string.Empty,
                        UserName = user != null ? user.DisplayName : string.Empty,
                        EventDate = e.EventDate,
                        EventDateTimeRange = e.EventDateTimeRange,
                        VisitDateTimeRange = e.VisitDateTimeRange
                    });
                });
            }
            return events;
        }

        public ServiceTaskViewData GetPrintQueue(Guid locationId, bool isLocationNeededIdEmpty, DateTime StartDate, DateTime EndDate, bool isPrintUrlNeeded)
        {
            var viewData = new ServiceTaskViewData { Service = this.Service };
            if (isLocationNeededIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            viewData.LocationId = locationId;
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Schedule, new int[] { (int)PermissionActions.Print, (int)PermissionActions.Export });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None);
                viewData.PrintPermissions = viewData.AvailableService;
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
            }
            viewData.Tasks = GetPrintQueue(locationId, StartDate, EndDate, isPrintUrlNeeded && viewData.PrintPermissions.Has(this.Service));

            return viewData;
        }

        /// <summary>
        /// Only removes the asset from the schedule task
        /// </summary>
        /// <param name="scheduleEvent"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public bool DeleteScheduleTaskAsset(T scheduleEvent, Guid assetId)
        {
            var result = false;
            if (!assetId.IsEmpty())
            {
                if (scheduleEvent != null && scheduleEvent.Asset.IsNotNullOrEmpty())
                {
                    var assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                    if (assets != null && assets.Count > 0 && assets.Exists(id => id == assetId))
                    {
                        assets.Remove(assetId);
                        scheduleEvent.Asset = assets.ToXml();
                        if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                        {
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.EditDetail, (DisciplineTasks)scheduleEvent.DisciplineTask, "Uploaded asset is deleted.");
                            result = true;
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public List<DeletedTaskViewData> GetDeletedTasks(Guid patientId)
        {

            var viewTasks = new List<DeletedTaskViewData>();
            var deletedTasks = baseScheduleRepository.GetDeletedTasks(Current.AgencyId, patientId);
            if (deletedTasks != null && deletedTasks.Count > 0)
            {
                var userIds = deletedTasks.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                deletedTasks.ForEach(s =>
                {
                    var task = new DeletedTaskViewData
                    {
                        Id = s.Id,
                        PatientId = s.PatientId,
                        EpisodeId = s.EpisodeId,
                        TaskName = s.DisciplineTaskName,
                        Status = s.StatusName,
                        Date = s.EventDate,
                        Service = this.Service
                    };
                    if (!s.UserId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            task.User = user.DisplayName;
                        }
                    }
                    if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        task.User = "Axxess";
                    }
                    viewTasks.Add(task);
                });
            }
            return viewTasks;
        }

        public void SetScheduleTasksDefault(E episode, List<DisciplineTask> disciplineTasks, List<T> scheduleEvents)
        {
            if (episode != null)
            {
                if (disciplineTasks != null && disciplineTasks.Count > 0)
                {
                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                    {
                        scheduleEvents.ForEach(ev =>
                        {
                            var disciplineTask = disciplineTasks.FirstOrDefault(d => d.Id == ev.DisciplineTask);
                            if (disciplineTask != null)
                            {
                                SetScheduleTaskDefault(episode, disciplineTask, ev);
                            }
                        });
                    }
                }
            }
        }

        public void SetScheduleTaskDefault(E episode, DisciplineTask disciplineTask, T ev)
        {
            ev.Id = Guid.NewGuid();
            ev.AgencyId = Current.AgencyId;
            ev.PatientId = episode.PatientId;
            ev.EpisodeId = episode.Id;
            ev.StartDate = episode.StartDate;
            ev.EndDate = episode.EndDate;
            ev.Status = disciplineTask.DefaultStatus;
            ev.DisciplineTask = disciplineTask.Id;
            ev.Discipline = disciplineTask.Discipline;
            ev.Version = disciplineTask.Version;
            ev.IsBillable = disciplineTask.IsBillable;
            ev.IsDeprecated = false;
            SetScheduleTasksDefaultAppSpecific(ev);
        }

        public AttachmentViewData GetAttachments(Guid patientId, Guid eventId)
        {
            var viewData = new AttachmentViewData();
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (scheduleEvent != null)
            {
                viewData.DisciplineTaskName = scheduleEvent.DisciplineTaskName;
                if (scheduleEvent.Asset.IsNotNullOrEmpty())
                {
                    viewData.Assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                }
            }
            viewData.PatientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
            return viewData;
        }

        public bool Remove(Guid patientId, Guid Id)
        {
            return baseScheduleRepository.RemoveScheduleTaskFully(Current.AgencyId, patientId, Id);
        }


        #endregion

        #region Return Comment

        public string GetReturnComments(Guid eventId, Guid patientId)
        {
            return baseScheduleRepository.GetReturnReason(Current.AgencyId, patientId, eventId, Current.UserId);
        }

        public JsonViewData AddReturnComments(Guid patientId, Guid eventId, string comment)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comment failed to save." };
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (task != null)
            {
                var returnComment = new ReturnComment { AgencyId = Current.AgencyId, Comments = comment, Created = DateTime.Now, EpisodeId = task.EpisodeId, EventId = eventId, Modified = DateTime.Now, UserId = Current.UserId };
                if (baseScheduleRepository.AddReturnComment(returnComment))
                {
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsCaseManagementRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = true;
                    viewData.PatientId = patientId;
                    viewData.EpisodeId = task.EpisodeId;
                    viewData.UserIds = new List<Guid> { task.UserId };
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Return comment saved successfully";
                }
            }
            return viewData;
        }

        public bool EditReturnComments(int id, string comment)
        {
            var existingComment = baseScheduleRepository.GetReturnComment(Current.AgencyId, id);
            existingComment.Modified = DateTime.Now;
            existingComment.Comments = comment;
            return baseScheduleRepository.UpdateReturnComment(existingComment);
        }

        public bool DeleteReturnComments(int id)
        {
            return baseScheduleRepository.DeleteReturnComments(Current.AgencyId, id);
        }

        #endregion

        #region  Missed Visit

        public JsonViewData ProcessMissedVisitNotes(Guid patientId, Guid eventId, string button, string reason)
        {
            var messageButton = button.ToLower() + (button.EndsWith("e") ? "d" : "ed");
            var viewData = new JsonViewData (false, "Missed visit could not be " + messageButton + ".");
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                if (ProcessMissedVisitNotes(task, button, reason))
                {
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                    viewData.IsDataCentersRefresh = true;
                    viewData.PatientId = task.PatientId;
                    viewData.EpisodeId = task.EpisodeId;
                    viewData.UserIds = new List<Guid> { task.UserId };
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsCommunicationNoteRefresh = task.DisciplineTask == (int)DisciplineTasks.CommunicationNote;
                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, task);
                    viewData.IsIncidentAccidentRefresh = task.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport;
                    viewData.IsInfectionRefresh = task.DisciplineTask == (int)DisciplineTasks.InfectionReport;
                    viewData.IsCaseManagementRefresh = true;
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Missed visit has been successfully " + messageButton + ".";
                }
            }
            return viewData;
        }

        public bool ProcessMissedVisitNotes(T task, string button, string reason)
        {
            bool result = false;
            if (task != null)
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var missedVisit = baseScheduleRepository.GetMissedVisit(Current.AgencyId, task.Id);
                        if (missedVisit != null)
                        {
                            if (button == "Approve")
                            {
                                missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                            }
                            else if (button == "Return")
                            {
                                missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
                            }
                            task.Status = missedVisit.Status;
                            if (baseScheduleRepository.UpdateScheduleTask(task))
                            {
                                if (baseScheduleRepository.UpdateMissedVisit(missedVisit))
                                {
                                    bool isReasonAdd = true;
                                    if (reason.IsNotNullOrEmpty())
                                    {
                                       isReasonAdd = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                    }
                                    if (isReasonAdd)
                                    {
                                        result = true;
                                        ts.Complete();
                                    }
                                    else
                                    {
                                        ts.Dispose();
                                    }
                                }
                                else
                                {
                                    ts.Dispose();
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public JsonViewData AddOrUpdateMissedVisit(MissedVisit missedVisit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed visit could not be saved." };
            if (!missedVisit.Id.IsEmpty() && !missedVisit.PatientId.IsEmpty() && !missedVisit.EpisodeId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, missedVisit.PatientId, missedVisit.Id);
                if (scheduleEvent != null)
                {
                    if (scheduleEvent.EventDate.IsValid() && scheduleEvent.EventDate.Date <= DateTime.Now.Date)
                    {
                        var validationRules = new List<Validation>();
                        validationRules.Add(new Validation(() => !missedVisit.SignatureDate.IsValid(), "Signature date is not a valid date."));
                        validationRules.Add(new Validation(() => string.IsNullOrEmpty(missedVisit.Signature), "The Staff Signature can't be empty."));
                        validationRules.Add(new Validation(() => missedVisit.Signature.IsNotNullOrEmpty() && !ServiceHelper.IsSignatureCorrect(Current.UserId, missedVisit.Signature), "User Signature is not correct."));

                        if (missedVisit.SignatureDate.IsValid())
                        {
                            var episodeDateRange = baseEpisodeRepository.GetEpisodeDateRange(Current.AgencyId, missedVisit.PatientId, missedVisit.EpisodeId);
                            validationRules.Add(new Validation(() => (missedVisit.SignatureDate.Date < episodeDateRange.StartDate.Date), string.Format("The signature date must be greater or equal to the episode start date({0}).", episodeDateRange.StartDate.ToZeroFilled())));
                            validationRules.Add(new Validation(() => (missedVisit.SignatureDate.Date > episodeDateRange.EndDate.Date), string.Format("The signature date must be must be less than or equal to the episode end date({0}).", episodeDateRange.EndDate.ToZeroFilled())));
                        }
                        var entityValidator = new EntityValidator(validationRules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            missedVisit.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                            viewData = this.AddOrUpdateMissedVisit(scheduleEvent, missedVisit);

                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The schedule event don't to be future date . Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The event isn't found. Try again.";
                }
            }
            return viewData;
        }

        protected JsonViewData AddOrUpdateMissedVisit(T scheduledEvent, MissedVisit missedVisit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed visit could not be saved." };
            if (missedVisit != null)
            {
                if (scheduledEvent != null)
                {
                    missedVisit.AgencyId = Current.AgencyId;
                    SetMissedVisitStatus(missedVisit);
                    var oldStaus = scheduledEvent.Status;
                    var oldIsMissedVisit = scheduledEvent.IsMissedVisit;
                    var oldBeforeMissedVisitStatus = scheduledEvent.BeforeMissedVisitStatus;
                    scheduledEvent.IsMissedVisit = true;
                    scheduledEvent.Status = missedVisit.Status;
                    //scheduledEvent.Status = !ScheduleStatusFactory.MissedVisitStatus().Contains(oldStaus) ? missedVisit.Status : oldStaus;
                    //set the schedule tasks BeforeMissedVisitStatus if the old status is not a missed visit status
                    if (!ScheduleStatusFactory.MissedVisitStatus().Contains(oldStaus))
                    {
                        scheduledEvent.BeforeMissedVisitStatus = oldStaus;
                        
                    }
                    if (baseScheduleRepository.UpdateScheduleTask(scheduledEvent))
                    {
                        var existing = baseScheduleRepository.GetMissedVisit(Current.AgencyId, scheduledEvent.Id);
                        bool result = false;
                        if (existing != null)
                        {
                            existing.EventDate = missedVisit.EventDate;
                            existing.IsOrderGenerated = missedVisit.IsOrderGenerated;
                            existing.IsPhysicianOfficeNotified = missedVisit.IsPhysicianOfficeNotified;
                            existing.Reason = missedVisit.Reason;
                            existing.SignatureDate = missedVisit.SignatureDate;
                            existing.SignatureText = missedVisit.SignatureText;
                            existing.Comments = missedVisit.Comments;
                            existing.Status = missedVisit.Status;
                            result = baseScheduleRepository.UpdateMissedVisit(existing);
                        }
                        else
                        {
                            result = baseScheduleRepository.AddMissedVisit(missedVisit);
                        }
                        if (result)
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Missed visit successfully saved.";
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            viewData.PatientId = scheduledEvent.PatientId;
                            viewData.EpisodeId = scheduledEvent.EpisodeId;
                            viewData.UserIds = new List<Guid> { scheduledEvent.UserId };
                            viewData.IsDataCentersRefresh = true;
                            viewData.IsCommunicationNoteRefresh = scheduledEvent.DisciplineTask == (int)DisciplineTasks.CommunicationNote;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduledEvent);
                            viewData.IsIncidentAccidentRefresh = scheduledEvent.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport;
                            viewData.IsInfectionRefresh = scheduledEvent.DisciplineTask == (int)DisciplineTasks.InfectionReport;
                            viewData.IsCaseManagementRefresh = scheduledEvent.Status == (int)ScheduleStatus.NoteMissedVisitPending;
                            if (Enum.IsDefined(typeof(DisciplineTasks), scheduledEvent.DisciplineTask))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduledEvent.DisciplineTask), "Set as a missed visit by " + Current.UserFullName);
                            }
                        }
                        else
                        {
                            scheduledEvent.IsMissedVisit = oldIsMissedVisit;
                            scheduledEvent.Status = oldStaus;
                            scheduledEvent.BeforeMissedVisitStatus = oldBeforeMissedVisitStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduledEvent);
                        }
                    }
                }
            }
            return viewData;
        }

        private void SetMissedVisitStatus(MissedVisit missedVisit)
        {
            if (Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA))
            {
                missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else
            {
                missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitPending;
            }
        }

        public JsonViewData MissedVisitRestore(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (task != null)
            {
                task.IsMissedVisit = false;
                task.Status = task.BeforeMissedVisitStatus;
                if (baseScheduleRepository.UpdateScheduleTask(task))
                {
                    viewData.isSuccessful = true;
                    viewData.EpisodeId = task.EpisodeId;
                    viewData.PatientId = task.PatientId;
                    viewData.UserIds = new List<Guid> { task.UserId };
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                    viewData.IsCaseManagementRefresh = task.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview) || task.Status == ((int)ScheduleStatus.OasisCompletedPendingReview) || task.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature) || task.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && task.UserId == Current.UserId && !task.IsComplete && task.EventDate.Date >= DateTime.Now.AddDays(-89) && task.EventDate.Date <= DateTime.Now.AddDays(14);

                }
                else
                {
                    viewData.isSuccessful = false;
                }
            }
            return viewData;
        }

        public MissedVisit GetMissedVisit(Guid agencyId, Guid Id)
        {
            var missedVisit = baseScheduleRepository.GetMissedVisit(agencyId, Id);
            if (missedVisit != null)
            {
                missedVisit.PatientName = patientRepository.GetPatientNameById(missedVisit.PatientId, agencyId);
                var scheduledEvent = baseScheduleRepository.GetScheduleTask(agencyId, missedVisit.PatientId, Id);
                if (scheduledEvent != null)
                {
                    missedVisit.EventDate = scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                    missedVisit.EndDate = scheduledEvent.EndDate;
                    missedVisit.StartDate = scheduledEvent.StartDate;
                    missedVisit.VisitType = scheduledEvent.DisciplineTaskName;
                    var user = UserEngine.GetUser(scheduledEvent.UserId, agencyId);
                    if (user != null)
                    {
                        missedVisit.UserName = user.DisplayName;
                    }
                }
            }
            return missedVisit;
        }

        public MissedVisit GetMissedVisitPrint()
        {
            var note = new MissedVisit();
            note.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
            return note;
        }

        public MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId)
        {
            var missedVisit = baseScheduleRepository.GetMissedVisitForPrint(Current.AgencyId, patientId, eventId);
            if (missedVisit != null)
            {
                missedVisit.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, missedVisit.PatientProfile.AgencyLocationId);
            }
            return missedVisit;
            //PatientProfileLean profile = null;
            //MissedVisit note = null;
            //using (TransactionScope ts = new TransactionScope())
            //{
            //    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
            //    {
            //        note = baseScheduleRepository.GetMissedVisit(Current.AgencyId, eventId);
            //        if (note != null)
            //        {
            //            profile = profileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
            //            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            //            if (scheduleEvent != null)
            //            {
            //                note.EventDate = scheduleEvent.EventDate.ToString("MM/dd/yyyy");
            //                note.DisciplineTaskName = scheduleEvent.DisciplineTaskName.IsNotNullOrEmpty() ? scheduleEvent.DisciplineTaskName : string.Empty;
            //                ts.Complete();
            //            }
            //        }
            //    }
            //}
            //if(profile != null)
            //{
            //    note.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
            //    note.PatientProfile = profile;
            //}
            //return note;
        }

        public List<PatientEpisodeEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, bool isExcel, bool stickyNoteNeeded)
        {
            var missedVisits = new List<PatientEpisodeEvent>();
            var patientEvents = baseScheduleRepository.GetMissedScheduledEvents(agencyId, branchId, startDate, endDate);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var completeStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                //PatientAccessInformation patientAccessInfo = null;
                //if(Current.IsClinicianOrHHA)
                //{
                //    patientAccessInfo = new PatientAccessInformation();
                //    patientAccessInfo.PatientUsers = patientRepository.GetPatientUsersByUser(Current.UserId);
                //    patientAccessInfo.TeamPatients = agencyRepository.GetTeamAccess(Current.AgencyId, Current.UserId);
                //    patientAccessInfo.PatientUserIds = patientEvents.ToDictionary(k => k.PatientId, e => e.PatientUserId);
                //}
                patientEvents.ForEach(v =>
                {
                    var task = new PatientEpisodeEvent
                    {
                        MRN = v.PatientIdNumber,
                        Status = v.StatusName,
                        //EpisodeRange = string.Format("{0} - {1}", v.StartDate, v.EndDate),
                        EventDate = v.EventDate,
                        Time = string.Format("{0} - {1}", v.TimeIn, v.TimeOut),
                        PatientName = v.PatientName,
                        TaskName = v.DisciplineTaskName
                    };
                    if (v.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        task.UserName = "Axxess";
                    }
                    else
                    {
                        var user = users.SingleOrDefault(u => u.Id == v.UserId);
                        if (user != null)
                        {
                            task.UserName = user.DisplayName;
                        }
                    }
                    if (!isExcel)
                    {
                        //if (Enum.IsDefined(typeof(DisciplineTasks), v.DisciplineTask))
                        //{
                        //    task.Type = ((DisciplineTasks)v.DisciplineTask).ToString();
                        //}
                        task.EventId = v.Id;
                        task.PatientId = v.PatientId;

                        //if (ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true).Contains(v.Status))
                        //{
                        //    TaskUrl = string.Format("MissedVisit.{0}.Open($(this), '{1}');", prefix, task.Id);
                        //}
                        //else
                        //{
                        //    TaskUrl = string.Format("MissedVisit.{0}.Edit('{1}');", prefix, task.Id);
                        //}
                        task.IsComplete = completeStatus.Contains(v.Status);
                        //task.Group = DocumentType.MissedVisit.ToString().ToLowerCase();
                        task.Task = v.DisciplineTask;

                        if (stickyNoteNeeded)
                        {
                            task.YellowNote = v.Comments.IsNotNullOrEmpty() ? v.Comments.Clean() : string.Empty;
                            task.RedNote = v.StatusComment;
                        }
                    }

                    //events.Add(task);
                    //if (!isExcel)
                    //{
                    //   UrlFactory<T>.SetNew(v, true, true, false, false, patientAccessInfo);
                    //}
                    //TaskHelperFactory<T>.CreateMissedTaskViewDataFromTask(v);
                    missedVisits.Add(task);
                });
            }
            return missedVisits;
        }

        public MissedVisitTaskViewData GetMissedVisitViewData(Guid agencyId, Guid locationId, bool isLocationNeededIfIdEmpty, DateTime startDate, DateTime endDate, bool isExcel)
        {
            var viewData = new MissedVisitTaskViewData();
            viewData.Service = this.Service;
            if (isLocationNeededIfIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Schedule, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.ViewStickyNotes, (int)PermissionActions.Export, (int)PermissionActions.Delete, (int)PermissionActions.Restore });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.IsUserCanRestore = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Restore, AgencyServices.None).Has(this.Service);
                viewData.IsUserCanDelete = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None).Has(this.Service);
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
                viewData.IsUserCanSeeStickyNote = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewStickyNotes, AgencyServices.None).Has(this.Service);
            }
            viewData.Activities = GetMissedScheduledEvents(Current.AgencyId, locationId, startDate, endDate, isExcel, viewData.IsUserCanSeeStickyNote);
            return viewData;

        }

        #endregion

        #region Case Manager

        public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate, bool IsToExcel, bool stickyNoteNeeded)
        {
            var events = new List<PatientEpisodeEvent>();
            List<T> scheduleEvents = baseScheduleRepository.GetScheduleByBranchDateRangeAndStatus(Current.AgencyId, branchId, startDate, endDate, status, ScheduleStatusFactory.CaseManagerStatus().ToArray(), true);
            if (scheduleEvents.IsNotNullOrEmpty())
            {

                var userIds = new List<Guid>();
                var users = new List<User>();

                List<ReturnComment> returnComments = null;
                if (!IsToExcel)
                {
                    if (stickyNoteNeeded)
                    {
                        var episodeIds = scheduleEvents.Select(e => e.EpisodeId).Distinct().ToList();
                        if (episodeIds.IsNotNullOrEmpty())
                        {
                            returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                            if (returnComments.IsNotNullOrEmpty())
                            {
                                var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                                if (returnUserIds.Count > 0)
                                {
                                    userIds.AddRange(returnUserIds);
                                }
                            }
                        }
                    }
                }

                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds.IsNotNullOrEmpty())
                {
                    userIds.AddRange(scheduleUserIds);
                }

                if (userIds.IsNotNullOrEmpty())
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                }
                var tasksNeedPlanOfCare = DisciplineTaskFactory.SkilledNurseSharedFile();
                tasksNeedPlanOfCare.Add((int)DisciplineTasks.HHAideVisit);

                scheduleEvents.ForEach(s =>
                {
                    var userName = string.Empty;
                    if (!s.UserId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            userName = user.DisplayName;
                        }
                    }

                    var redNote = string.Empty;
                    var detailNote = string.Empty;
                    if (!IsToExcel)
                    {
                        if (stickyNoteNeeded)
                        {
                            detailNote = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.Clean() : string.Empty;
                            if (returnComments.IsNotNullOrEmpty())
                            {
                                var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id && r.EpisodeId == s.EpisodeId).ToList();
                                if (eventReturnReasons.IsNotNullOrEmpty())
                                {
                                    redNote = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                                }
                            }
                        }
                    }

                    var task = new PatientEpisodeEvent
                    {

                        Status = s.StatusName,
                        EpisodeRange = string.Format("{0} - {1}", s.StartDate, s.EndDate),
                        EventDate = s.EventDate,
                        Time = string.Format("{0} - {1}", s.TimeIn, s.TimeOut),
                        PatientName = s.PatientName,
                        TaskName = s.DisciplineTaskName,
                        UserName = userName,
                    };
                    if (!IsToExcel)
                    {
                        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                        {
                            task.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                        }
                        task.EventId = s.Id;
                        task.EpisodeId = s.EpisodeId;
                        task.PatientId = s.PatientId;
                        var group = DocumentFactory.GetType(s.DisciplineTask, s.IsMissedVisit);
                        if (group != DocumentType.None)
                        {
                            task.Group = group.ToString().ToLowerCase();
                        }
                        task.Task = s.DisciplineTask;
                        task.IsPOCNeeded = tasksNeedPlanOfCare.Contains(s.DisciplineTask) && !s.IsMissedVisit;

                        if (stickyNoteNeeded)
                        {
                            task.YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty;
                            task.RedNote = redNote;
                            task.BlueNote = detailNote;
                        }
                    }

                    events.Add(task);

                });
            }
            return events.OrderByDescending(e => e.EventDate).ToList();
        }

        public QAViewData GetCaseManagerScheduleContent(Guid locationId, bool isLocationNeededIdEmpty, int Status, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new QAViewData();
            viewData.Service = this.Service;
            if (isLocationNeededIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.QA, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Approve, (int)PermissionActions.Return, (int)PermissionActions.Export, (int)PermissionActions.EditApproved });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.ApprovePermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Approve, AgencyServices.None);
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
                viewData.ReturnPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Return, AgencyServices.None);
                viewData.IsUserCanEditApproved = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.EditApproved, AgencyServices.None).Has(this.Service);
            }
            viewData.IsUserCanPrint = Current.HasRight(this.Service, ParentPermission.Schedule, PermissionActions.Print);
            viewData.IsUserCanSeeStickyNote = Current.HasRight(this.Service, ParentPermission.Schedule, PermissionActions.ViewStickyNotes);
            viewData.Activities = this.GetCaseManagerSchedule(locationId, Status, StartDate, EndDate, false, viewData.IsUserCanSeeStickyNote);
            return viewData;
        }

        #endregion

        public List<T> GetPatientOrderScheduleEvents(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return baseScheduleRepository.GetPatientOrderScheduleEvents(Current.AgencyId, patientId, startDate, endDate, DisciplineTaskFactory.AllOrders());
        }

        public List<T> GetOrderScheduleEvents(Guid branchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            return baseScheduleRepository.GetOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate, DisciplineTaskFactory.AllOrders(), status);
        }

        //Method was not being used
        //public List<T> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        //{
        //    return baseScheduleRepository.GetScheduleEvents(Current.AgencyId, branchId, startDate, endDate, DisciplineTaskFactory.AllOrders(), ScheduleStatusFactory.OrdersPendingPhysicianSignature());
        //}

        public List<T> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return baseScheduleRepository.GetPatientScheduledEventsOnlyNew(Current.AgencyId, episodeId, patientId);
        }

        public List<TaskLean> GetScheduledEventsByStatus(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int status)
        {
            bool isMissedVisitsIncluded = ScheduleStatusFactory.MissedVisitStatus().Contains(status);
            int usedStatus = status;
            if (status == (int)ScheduleStatus.NoteNotStarted)
            {
                usedStatus = (int)ScheduleStatus.NoteNotYetDue;
            }
            var patientEvents = baseScheduleRepository.GetPatientScheduledEventsLeanWithUserIdAndCheckMissedVisitStatus(Current.AgencyId, patientId, clinicianId, new int[] { usedStatus }, new string[] { }, new int[] { }, true, startDate, endDate, isMissedVisitsIncluded, false, -1);
            var filteredPatientEvents = new List<TaskLean>();
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    if ((status != (int)ScheduleStatus.NoteNotStarted && status != (int)ScheduleStatus.NoteNotYetDue) ||
                        (v.EventDate.Date > DateTime.Now.Date && status == (int)ScheduleStatus.NoteNotYetDue) ||
                        (v.EventDate.Date <= DateTime.Now.Date && status == (int)ScheduleStatus.NoteNotStarted))
                    {
                        var tl = new TaskLean
                        {
                            PatientIdNumber = v.PatientIdNumber,
                            PatientName = v.PatientName,
                            StatusName = v.StatusName,
                            DisciplineTaskName = v.DisciplineTaskName,
                            EventDateTimeRange = v.EventDateTimeRange,
                            VisitDateTimeRange = v.VisitDateTimeRange,
                            EventDate = v.EventDate
                        };
                        var user = users.SingleOrDefault(u => u.Id == v.UserId);
                        if (user != null)
                        {
                            tl.UserName = user.DisplayName;
                        }
                        filteredPatientEvents.Add(tl);
                    }
                });
            }
            return filteredPatientEvents;
        }

        public List<TaskLean> GetScheduledEventsByType(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int type)
        {
            var filteredPatientEvents = new List<TaskLean>();
            var patientEvents = baseScheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, clinicianId, new int[] { }, new string[] { }, new int[] { type }, true, startDate, endDate, true, false, -1);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                var userIds = patientEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                patientEvents.ForEach(v =>
                {
                    var tl = new TaskLean
                    {
                        PatientIdNumber = v.PatientIdNumber,
                        PatientName = v.PatientName,
                        StatusName = v.StatusName,
                        DisciplineTaskName = v.DisciplineTaskName,
                        EventDateTimeRange = v.EventDateTimeRange,
                        VisitDateTimeRange = v.VisitDateTimeRange,
                        EventDate = v.EventDate
                    };
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        tl.UserName = user.DisplayName;
                    }
                    filteredPatientEvents.Add(tl);
                });
            }
            return filteredPatientEvents;
        }

        public IList<UserVisitWidget> GetScheduleWidget(Guid userId, DateTime from, DateTime to)
        {
            int editDocuments;
            string aditionalFilter;
            GetDocumentsAndFiltersForWidget(Current.Permissions, out editDocuments, out aditionalFilter);
            var userVisits = baseScheduleRepository.GetScheduleWidget(Current.AgencyId, userId, from, to, 4, false, aditionalFilter);
            if (userVisits != null && userVisits.Count > 0)
            {
                userVisits.ForEach(uv =>
                {
                    uv.Service = this.Service;
                    var task = DisciplineTasks.NoDiscipline;
                    if (Enum.IsDefined(typeof(DisciplineTasks), uv.DisciplineTask))
                    {
                        task = (DisciplineTasks)uv.DisciplineTask;
                        uv.TaskName = task.GetDescription();
                    }
                    var documentType = DocumentFactory.GetType(uv.DisciplineTask, uv.IsMissedVisit);
                    var documentTypeNumber = (int)documentType;
                    if (documentTypeNumber != (int)DocumentType.None)
                    {
                        if ((editDocuments & documentTypeNumber) == documentTypeNumber)
                        {
                            if (task != DisciplineTasks.NoDiscipline)
                            {
                                uv.Type = task.ToString();
                            }
                            uv.IsUserCanEdit = true;
                        }
                    }
                });
            }
            return userVisits;
        }

        #region Protected Methods

        protected abstract bool GetDocumentsAndFiltersForWidget(IDictionary<int, Dictionary<int, List<int>>> permission, out int editDocuments, out string aditionalFilter);

        protected void ProcessScheduleEventsActions(TaskActivityViewData taskActivityViewData, List<E> patientEpisodes, List<T> scheduleEvents, bool needsPatientNames)
        {
            var list = new List<GridTask>();
            var episodeWithEventsDictionary = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.Key, g => g.ToList());
            if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
            {
                var isFrozen = Current.IsAgencyFrozen;

                var caseManagmentStatus = ScheduleStatusFactory.CaseManagerStatus();

                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);

                var completelyFinishedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(false);

                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var episodeIds = episodeWithEventsDictionary.Keys.ToList();
                var users = new List<User>();
                var userIds = new List<Guid>();
                var patientNames = new Dictionary<Guid, string>();

                var returnComments = new List<ReturnComment>();
                var missedVisits = new List<MissedVisit>();
                if (!isFrozen && taskActivityViewData.IsStickyNote)
                {
                    returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds);
                    if (returnComments.IsNotNullOrEmpty())
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds.IsNotNullOrEmpty())
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }

                    var eventIds = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
                    if (eventIds.IsNotNullOrEmpty())
                    {
                        missedVisits = baseScheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();
                    }
                }


                userIds = userIds.Union((from evnt in scheduleEvents where !evnt.UserId.IsEmpty() select evnt.UserId)).ToList();
                if (userIds.IsNotNullOrEmpty())
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds);
                }
                //PatientAccessInformation patientAccessInfo = null;
                //if (Current.IsClinicianOrHHA)
                //{
                //    patientAccessInfo = new PatientAccessInformation();
                //    patientAccessInfo.PatientUsers = patientRepository.GetPatientUsersByUser(Current.UserId);
                //    patientAccessInfo.TeamPatients = agencyRepository.GetTeamAccess(Current.AgencyId, Current.UserId);
                //    var patients = profileRepository.GetPatientLeanByUserId(Current.AgencyId, Current.UserId, needsPatientNames);
                //    patientAccessInfo.PatientUserIds = patients.ToDictionary(k => k.Id, e => Current.UserId);
                //    if (needsPatientNames)
                //    {
                //        patientNames = patients.ToDictionary(k => k.Id, e => e.DisplayNameWithMi);
                //    }
                //}
                if (needsPatientNames)
                {
                    var schedulePatientIds = scheduleEvents.Where(s => !s.PatientId.IsEmpty()).Select(s => s.PatientId).Distinct().ToList();
                    patientNames = patientRepository.GetPatientNamesByIds(Current.AgencyId, schedulePatientIds);
                }
                // var eventIds = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();

                episodeWithEventsDictionary.ForEach((key, value) =>
                {
                    if (value != null && value.Count > 0)
                    {
                        var episode = patientEpisodes.FirstOrDefault(e => e.Id == key);
                        if (episode != null)
                        {
                            var detailComment = string.Empty;
                            if (!isFrozen && taskActivityViewData.IsStickyNote)
                            {
                                if (episode.Comments.IsNotNullOrEmpty())
                                {
                                    detailComment = episode.Comments.Clean();
                                }
                            }
                            value.ForEach(s =>
                            {
                                var documentType = DocumentFactory.GetType(s.DisciplineTask, false);
                                var documentTypeNumber = (int)documentType;
                                if (documentTypeNumber != (int)DocumentType.None)
                                {
                                    if ((taskActivityViewData.ViewDocuments & documentTypeNumber) == documentTypeNumber)
                                    {
                                        s.StartDate = episode.StartDate;
                                        s.EndDate = episode.EndDate;
                                        var isMER = s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport;
                                        var isCaseManagmentStatus = caseManagmentStatus.Contains(s.Status);
                                        var isComplete = s.IsMissedVisit ? missedVisitCompletedStatus.Contains(s.Status) : completedStatus.Contains(s.Status);
                                        var gridItem = new GridTask
                                        {
                                            Service = this.Service,
                                            Id = s.Id,
                                            PatientId = episode.PatientId,
                                            EpisodeId = s.EpisodeId,
                                            DateIn = s.EventDate,
                                            Range = s.EventDateTimeRange,
                                            StartDate = episode.StartDate,
                                            EndDate = episode.EndDate,
                                            TaskName = s.DisciplineTaskName,
                                            StatusName = s.StatusName,
                                            IsUserCanPrint = (taskActivityViewData.PrintDocuments & documentTypeNumber) == documentTypeNumber,
                                            IsUserCanDelete = !isFrozen && (taskActivityViewData.DeleteDocuments & documentTypeNumber) == documentTypeNumber && !s.IsMissedVisit && !isMER && !isCaseManagmentStatus,
                                            IsUserCanReassign = !isFrozen && taskActivityViewData.IsUserCanReassign && !s.IsMissedVisit && !isComplete && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                            IsUserCanReopen = !isFrozen && isComplete && taskActivityViewData.IsUserCanReopen && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                            IsUserCanEditDetail = taskActivityViewData.IsUserCanEditDetail && !s.IsMissedVisit && !isMER,
                                            IsUserCanRestore = !isFrozen && s.IsMissedVisit && taskActivityViewData.IsUserCanRestore,
                                            IsUserCanEdit = !isFrozen && (taskActivityViewData.EditDocuments & documentTypeNumber) == documentTypeNumber && !s.IsOrphaned && !isComplete,
                                            IsOrphaned = s.IsOrphaned,
                                            IsMissedVisit = s.IsMissedVisit,
                                            IsInQA = isCaseManagmentStatus

                                        };
                                        SetScheduleTasksDefaultAppSpecific(gridItem, s);

                                        if (needsPatientNames)
                                        {
                                            if (patientNames.ContainsKey((s.PatientId)))
                                            {
                                                gridItem.PatientName = patientNames[s.PatientId];
                                            }
                                        }

                                        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                                        {
                                            gridItem.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                                        }
                                        if (gridItem.IsUserCanPrint)
                                        {
                                            gridItem.Group = s.IsMissedVisit ? DocumentType.MissedVisit.ToString().ToLower() : documentType.ToString().ToLowerCase();
                                        }
                                        if (!s.PatientId.IsEmpty() && needsPatientNames)
                                        {
                                            gridItem.PatientName = patientNames.Get(s.PatientId);
                                        }
                                        gridItem.IsComplete = isComplete;
                                        if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                                        {
                                            gridItem.UserName = "Axxess";
                                        }
                                        else
                                        {
                                            if (!s.UserId.IsEmpty())
                                            {
                                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                                if (user != null)
                                                {
                                                    gridItem.UserName = user.DisplayName;
                                                }
                                            }
                                        }

                                        if (!isFrozen)
                                        {
                                            if (documentTypeNumber == (int)DocumentType.OASIS && taskActivityViewData.IsOASISProfileExist)
                                            {
                                                if (DisciplineTaskFactory.EpisodeAllAssessments(false).Contains(s.DisciplineTask) && ScheduleStatusFactory.OASISCompleted(true).Contains(s.Status))
                                                {
                                                    gridItem.IsOASISProfileExist = true;
                                                }
                                            }
                                            if (s.Asset.IsNotNullOrEmpty())
                                            {
                                                var assets = s.Asset.ToObject<List<Guid>>();
                                                if (assets != null && assets.Count > 0)
                                                {
                                                    s.Asset = string.Empty;
                                                    gridItem.IsAttachmentExist = true;
                                                }
                                            }
                                            if (taskActivityViewData.IsStickyNote)
                                            {
                                                var statusComment = string.Empty;
                                                if (s.IsMissedVisit)
                                                {
                                                    var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.Id);
                                                    if (missedVisit != null)
                                                    {
                                                        statusComment = missedVisit.ToString().Clean() + "\r\n";
                                                    }
                                                }
                                                if (s.ReturnReason.IsNotNullOrEmpty() && !completelyFinishedStatus.Contains(s.Status))
                                                {
                                                    statusComment += s.ReturnReason;
                                                }
                                                gridItem.StatusComment = statusComment;
                                                gridItem.EpisodeNotes = detailComment;
                                                var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList();
                                                if (eventReturnReasons.IsNotNullOrEmpty())
                                                {
                                                    gridItem.ReturnReason = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                                                }
                                                gridItem.Comments = s.Comments;
                                            }
                                        }
                                        list.Add(gridItem);

                                    }
                                }
                            });
                        }
                    }
                });
            }
            taskActivityViewData.ScheduleEvents = list;
        }

        protected void ProcessScheduleEventsActions2(TaskActivityViewData taskActivityViewData, E episode, List<T> scheduleEvents)
        {
            var list = new List<GridTask>();
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var isFrozen = Current.IsAgencyFrozen;

                var caseManagmentStatus = ScheduleStatusFactory.CaseManagerStatus();

                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);

                var completelyFinishedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(false);

                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var userIds = new List<Guid>();
                var returnComments = new List<ReturnComment>();
                var missedVisits = new List<MissedVisit>();
                if (taskActivityViewData.IsStickyNote)
                {
                    returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episode.Id) ?? new List<ReturnComment>();
                    if (returnComments.IsNotNullOrEmpty())
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds.IsNotNullOrEmpty())
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }
                    var eventIds = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
                    if (eventIds.IsNotNullOrEmpty())
                    {
                        missedVisits = baseScheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);
                    }
                }
                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds.IsNotNullOrEmpty())
                {
                    userIds.AddRange(scheduleUserIds);
                }
                if (userIds.IsNotNullOrEmpty())
                {
                    userIds = userIds.Distinct().ToList();
                }
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();

                var detailComment = string.Empty;
                if (taskActivityViewData.IsStickyNote)
                {
                    if (episode.Comments.IsNotNullOrEmpty())
                    {
                        detailComment = episode.Comments.Clean();
                    }
                }
                //PatientAccessInformation patientAccessInfo = null;
                //if (Current.IsClinicianOrHHA)
                //{
                //    patientAccessInfo = new PatientAccessInformation();
                //    patientAccessInfo.PatientUsers = patientRepository.GetPatientUsersByUser(Current.UserId);
                //    patientAccessInfo.TeamPatients = agencyRepository.GetTeamAccess(Current.AgencyId, Current.UserId);
                //    patientAccessInfo.PatientUserIds.Add(episode.PatientId, profileRepository.GetPatientAssignedUserId(Current.AgencyId, episode.PatientId));
                //}
                if (scheduleEvents.IsNotNullOrEmpty())
                {
                    scheduleEvents.ForEach(s =>
                    {
                        var documentType = DocumentFactory.GetType(s.DisciplineTask, false);
                        var documentTypeNumber = (int)documentType;
                        if (documentTypeNumber != (int)DocumentType.None)
                        {
                            if ((taskActivityViewData.ViewDocuments & documentTypeNumber) == documentTypeNumber)
                            {
                                //set episode range to schedule
                                s.StartDate = episode.StartDate;
                                s.EndDate = episode.EndDate;

                                var isMER = s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport;
                                var isCaseManagmentStatus = caseManagmentStatus.Contains(s.Status);
                                var isComplete = false;
                                if (s.IsMissedVisit)
                                {
                                    isComplete = missedVisitCompletedStatus.Contains(s.Status);
                                }
                                else
                                {
                                    isComplete = completedStatus.Contains(s.Status);
                                }
                                var gridItem = new GridTask
                                {
                                    Service = this.Service,
                                    Id = s.Id,
                                    PatientId = episode.PatientId,
                                    EpisodeId = s.EpisodeId,
                                    DateIn = s.EventDate,
                                    Range = s.EventDateTimeRange,
                                    StartDate = episode.StartDate,
                                    EndDate = episode.EndDate,
                                    TaskName = s.DisciplineTaskName,
                                    StatusName = s.StatusName,
                                    IsUserCanPrint = (taskActivityViewData.PrintDocuments & documentTypeNumber) == documentTypeNumber,
                                    IsUserCanDelete = !isFrozen && (taskActivityViewData.DeleteDocuments & documentTypeNumber) == documentTypeNumber && !s.IsMissedVisit && !isMER && !isCaseManagmentStatus,
                                    IsUserCanReassign = !isFrozen && taskActivityViewData.IsUserCanReassign && !isComplete && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                    IsUserCanReopen = !isFrozen && isComplete && taskActivityViewData.IsUserCanReopen && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                    IsUserCanEditDetail = taskActivityViewData.IsUserCanEditDetail && !s.IsMissedVisit && !isMER,
                                    IsUserCanRestore = !isFrozen && s.IsMissedVisit && taskActivityViewData.IsUserCanRestore,
                                    IsUserCanEdit = !isFrozen && (taskActivityViewData.EditDocuments & documentTypeNumber) == documentTypeNumber && !s.IsOrphaned && !isComplete,
                                    IsOrphaned = s.IsOrphaned,
                                    IsMissedVisit = s.IsMissedVisit
                                };
                                SetScheduleTasksDefaultAppSpecific(gridItem, s);
                                if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                                {
                                    gridItem.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                                }
                                if (gridItem.IsUserCanPrint)
                                {
                                    gridItem.Group = s.IsMissedVisit ? DocumentType.MissedVisit.ToString().ToLower() : documentType.ToString().ToLowerCase();
                                }
                                gridItem.IsComplete = isComplete;

                                if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                                {
                                    gridItem.UserName = "Axxess";
                                }
                                else
                                {
                                    if (!s.UserId.IsEmpty())
                                    {
                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                        if (user != null)
                                        {
                                            gridItem.UserName = user.DisplayName;
                                        }
                                    }
                                }
                                if (!isFrozen)
                                {
                                    if (documentTypeNumber == (int)DocumentType.OASIS && taskActivityViewData.IsOASISProfileExist)
                                    {
                                        if (DisciplineTaskFactory.EpisodeAllAssessments(false).Contains(s.DisciplineTask) && ScheduleStatusFactory.OASISCompleted(true).Contains(s.Status))
                                        {
                                            gridItem.IsOASISProfileExist = true;
                                        }
                                    }
                                    if (s.Asset.IsNotNullOrEmpty())
                                    {
                                        var assets = s.Asset.ToObject<List<Guid>>();
                                        if (assets != null && assets.Count > 0)
                                        {
                                            s.Asset = string.Empty;
                                            gridItem.IsAttachmentExist = true;
                                        }
                                    }
                                    if (taskActivityViewData.IsStickyNote)
                                    {
                                        var statusComment = string.Empty;
                                        if (s.IsMissedVisit)
                                        {
                                            var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.Id);
                                            if (missedVisit != null)
                                            {
                                                statusComment = missedVisit.ToString().Clean() + "\r\n";

                                            }
                                        }
                                        if (s.ReturnReason.IsNotNullOrEmpty() && !completelyFinishedStatus.Contains(s.Status))
                                        {
                                            statusComment += s.ReturnReason;
                                        }
                                        gridItem.StatusComment = statusComment;
                                        gridItem.EpisodeNotes = detailComment;
                                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList();
                                        if (eventReturnReasons.IsNotNullOrEmpty())
                                        {
                                            gridItem.ReturnReason = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                                        }
                                        gridItem.Comments = s.Comments;
                                    }
                                }
                                list.Add(gridItem);
                            }
                        }

                    });
                }
            }
            taskActivityViewData.ScheduleEvents = list;
        }

        protected void ProcessScheduleEventsActions2(TaskActivityViewData taskActivityViewData, List<E> patientEpisodes, List<T> scheduleEvents, bool needsPatientNames)
        {
            var list = new List<GridTask>();
            var episodeWithEventsDictionary = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.Key, g => g.ToList());
            if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
            {
                var isFrozen = Current.IsAgencyFrozen;

                var caseManagmentStatus = ScheduleStatusFactory.CaseManagerStatus();

                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);

                var completelyFinishedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(false);

                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var episodeIds = episodeWithEventsDictionary.Keys.ToList();
                var users = new List<User>();
                var patientNames = new Dictionary<Guid, string>();
                var userIds = new List<Guid>();
                var returnComments = new List<ReturnComment>();
                var missedVisits = new List<MissedVisit>();
                if (taskActivityViewData.IsStickyNote)
                {
                    returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds);
                    if (returnComments.IsNotNullOrEmpty())
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds.IsNotNullOrEmpty())
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }

                    var eventIds = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
                    if (eventIds.IsNotNullOrEmpty())
                    {
                        missedVisits = baseScheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();
                    }
                }

                userIds = userIds.Union((from evnt in scheduleEvents where !evnt.UserId.IsEmpty() select evnt.UserId)).ToList();
                if (userIds.IsNotNullOrEmpty())
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds);
                }
                //PatientAccessInformation patientAccessInfo = null;
                //if (Current.IsClinicianOrHHA)
                //{
                //    patientAccessInfo = new PatientAccessInformation();
                //    patientAccessInfo.PatientUsers = patientRepository.GetPatientUsersByUser(Current.UserId);
                //    patientAccessInfo.TeamPatients = agencyRepository.GetTeamAccess(Current.AgencyId, Current.UserId);
                //    var patients = profileRepository.GetPatientLeanByUserId(Current.AgencyId, Current.UserId, needsPatientNames);
                //    patientAccessInfo.PatientUserIds = patients.ToDictionary(k => k.Id, e => Current.UserId);
                //    if (needsPatientNames)
                //    {
                //        patientNames = patients.ToDictionary(k => k.Id, e => e.DisplayNameWithMi);
                //    }
                //}
                //else if (needsPatientNames)
                //{
                //    var schedulePatientIds = scheduleEvents.Where(s => !s.PatientId.IsEmpty()).Select(s => s.PatientId).Distinct().ToList();
                //    patientNames = patientRepository.GetPatientNamesByIds(Current.AgencyId, schedulePatientIds);
                //}


                episodeWithEventsDictionary.ForEach((key, value) =>
                {
                    if (value != null && value.Count > 0)
                    {
                        var episode = patientEpisodes.FirstOrDefault(e => e.Id == key);
                        if (episode != null)
                        {
                            var detailComment = string.Empty;
                            if (taskActivityViewData.IsStickyNote)
                            {
                                if (episode.Comments.IsNotNullOrEmpty())
                                {
                                    detailComment = episode.Comments.Clean();
                                }
                            }
                            value.ForEach(s =>
                            {
                                var documentType = DocumentFactory.GetType(s.DisciplineTask, false);
                                var documentTypeNumber = (int)documentType;
                                if (documentTypeNumber != (int)DocumentType.None)
                                {

                                    if ((taskActivityViewData.ViewDocuments & documentTypeNumber) == documentTypeNumber)
                                    {
                                        //set episode range to schedule
                                        s.StartDate = episode.StartDate;
                                        s.EndDate = episode.EndDate;

                                        var isMER = s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport;
                                        var isCaseManagmentStatus = caseManagmentStatus.Contains(s.Status);
                                        var isComplete = false;
                                        if (s.IsMissedVisit)
                                        {
                                            isComplete = missedVisitCompletedStatus.Contains(s.Status);
                                        }
                                        else
                                        {
                                            isComplete = completedStatus.Contains(s.Status);
                                        }
                                        var gridItem = new GridTask
                                        {
                                            Service = this.Service,
                                            Id = s.Id,
                                            PatientId = episode.PatientId,
                                            EpisodeId = s.EpisodeId,
                                            DateIn = s.EventDate,
                                            Range = s.EventDateTimeRange,
                                            StartDate = episode.StartDate,
                                            EndDate = episode.EndDate,
                                            TaskName = s.DisciplineTaskName,
                                            StatusName = s.StatusName,
                                            IsUserCanPrint = (taskActivityViewData.PrintDocuments & documentTypeNumber) == documentTypeNumber,
                                            IsUserCanDelete = !isFrozen && (taskActivityViewData.DeleteDocuments & documentTypeNumber) == documentTypeNumber && !s.IsMissedVisit && !isMER && !isCaseManagmentStatus,
                                            IsUserCanReassign = !isFrozen && taskActivityViewData.IsUserCanReassign && !s.IsMissedVisit && !isComplete && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                            IsUserCanReopen = !isFrozen && isComplete && taskActivityViewData.IsUserCanReopen && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                            IsUserCanEditDetail = taskActivityViewData.IsUserCanEditDetail && !s.IsMissedVisit && !isMER,
                                            IsUserCanRestore = !isFrozen && s.IsMissedVisit && taskActivityViewData.IsUserCanRestore,
                                            IsUserCanEdit = !isFrozen && (taskActivityViewData.EditDocuments & documentTypeNumber) == documentTypeNumber && !s.IsOrphaned && !isComplete,
                                            IsOrphaned = s.IsOrphaned,
                                            IsMissedVisit = s.IsMissedVisit,
                                            IsInQA = isCaseManagmentStatus

                                        };
                                        SetScheduleTasksDefaultAppSpecific(gridItem, s);
                                        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                                        {
                                            gridItem.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                                        }
                                        if (gridItem.IsUserCanPrint)
                                        {
                                            gridItem.Group = s.IsMissedVisit ? DocumentType.MissedVisit.ToString().ToLower() : documentType.ToString().ToLowerCase();
                                        }
                                        gridItem.IsComplete = isComplete;
                                        if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                                        {
                                            gridItem.UserName = "Axxess";
                                        }
                                        else
                                        {
                                            if (!s.UserId.IsEmpty())
                                            {
                                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                                if (user != null)
                                                {
                                                    gridItem.UserName = user.DisplayName;
                                                }
                                            }
                                        }

                                        if (!isFrozen)
                                        {
                                            if (documentTypeNumber == (int)DocumentType.OASIS && taskActivityViewData.IsOASISProfileExist)
                                            {
                                                if (DisciplineTaskFactory.EpisodeAllAssessments(false).Contains(s.DisciplineTask) && ScheduleStatusFactory.OASISCompleted(true).Contains(s.Status))
                                                {
                                                    gridItem.IsOASISProfileExist = true;
                                                }
                                            }
                                            if (s.Asset.IsNotNullOrEmpty())
                                            {
                                                var assets = s.Asset.ToObject<List<Guid>>();
                                                if (assets != null && assets.Count > 0)
                                                {
                                                    s.Asset = string.Empty;
                                                    gridItem.IsAttachmentExist = true;
                                                }
                                            }
                                            if (taskActivityViewData.IsStickyNote)
                                            {
                                                var statusComment = string.Empty;
                                                if (s.IsMissedVisit)
                                                {
                                                    var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.Id);
                                                    if (missedVisit != null)
                                                    {
                                                        statusComment = missedVisit.ToString().Clean() + "\r\n";
                                                    }
                                                }
                                                if (s.ReturnReason.IsNotNullOrEmpty() && !completelyFinishedStatus.Contains(s.Status))
                                                {
                                                    statusComment += s.ReturnReason;
                                                }
                                                gridItem.StatusComment = statusComment;
                                                gridItem.EpisodeNotes = detailComment;
                                                var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList();
                                                if (eventReturnReasons.IsNotNullOrEmpty())
                                                {
                                                    gridItem.ReturnReason = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                                                }
                                                gridItem.Comments = s.Comments;
                                            }
                                        }
                                        list.Add(gridItem);
                                    }
                                }
                            });
                        }
                    }
                });
            }
            taskActivityViewData.ScheduleEvents = list;
        }

        protected void PrecessScheduleEventForUsersOnly2(TaskActivityViewData taskActivityViewData, List<T> scheduleEvents)
        {
            var list = new List<GridTask>();
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);

                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var userIds = new List<Guid>();

                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds.IsNotNullOrEmpty())
                {
                    userIds.AddRange(scheduleUserIds);
                }
                if (userIds.IsNotNullOrEmpty())
                {
                    userIds = userIds.Distinct().ToList();
                }
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();

                if (scheduleEvents.IsNotNullOrEmpty())
                {
                    scheduleEvents.ForEach(s =>
                    {
                        var documentType = (int)DocumentFactory.GetType(s.DisciplineTask, false);
                        if (documentType != (int)DocumentType.None)
                        {
                            if ((taskActivityViewData.ViewDocuments & documentType) == documentType)
                            {
                                var isComplete = false;
                                if (s.IsMissedVisit)
                                {
                                    isComplete = missedVisitCompletedStatus.Contains(s.Status);
                                }
                                else
                                {
                                    isComplete = completedStatus.Contains(s.Status);
                                }
                                var gridItem = new GridTask
                                {
                                    Service = this.Service,
                                    Id = s.Id,
                                    PatientId = s.PatientId,
                                    EpisodeId = s.EpisodeId,
                                    DateIn = s.EventDate,
                                    Range = s.EventDateTimeRange,
                                    StartDate = s.StartDate,
                                    EndDate = s.EndDate,
                                    TaskName = s.DisciplineTaskName,
                                    StatusName = s.StatusName,
                                    IsOrphaned = s.IsOrphaned,
                                    IsMissedVisit = s.IsMissedVisit,
                                };
                                SetScheduleTasksDefaultAppSpecific(gridItem, s);
                                gridItem.IsComplete = isComplete;
                                if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                                {
                                    gridItem.UserName = "Axxess";
                                }
                                else
                                {
                                    if (!s.UserId.IsEmpty())
                                    {
                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                        if (user != null)
                                        {
                                            gridItem.UserName = user.DisplayName;
                                        }
                                    }
                                }
                                list.Add(gridItem);
                            }
                        }
                    });
                }
            }
            taskActivityViewData.ScheduleEvents = list;
        }

        protected void ProcessUserScheduleEventsActions(UserTaskViewData userTaskViewData, E episode, List<T> scheduleEvents)
        {
            var list = new List<GridTask>();
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var isFrozen = Current.IsAgencyFrozen;

                var caseManagmentStatus = ScheduleStatusFactory.CaseManagerStatus();

                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);

                var completelyFinishedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(false);

                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var userIds = new List<Guid>();
                var returnComments = new List<ReturnComment>();
                var missedVisits = new List<MissedVisit>();
                if (userTaskViewData.IsStickyNote)
                {
                    returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episode.Id) ?? new List<ReturnComment>();
                    if (returnComments.IsNotNullOrEmpty())
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds.IsNotNullOrEmpty())
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }
                    var eventIds = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
                    if (eventIds.IsNotNullOrEmpty())
                    {
                        missedVisits = baseScheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);
                    }
                }
                var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (scheduleUserIds.IsNotNullOrEmpty())
                {
                    userIds.AddRange(scheduleUserIds);
                }
                if (userIds.IsNotNullOrEmpty())
                {
                    userIds = userIds.Distinct().ToList();
                }
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                // userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds);

                var detailComment = string.Empty;
                if (userTaskViewData.IsStickyNote)
                {
                    if (episode.Comments.IsNotNullOrEmpty())
                    {
                        detailComment = episode.Comments.Clean();
                    }
                }

                if (scheduleEvents.IsNotNullOrEmpty())
                {
                    scheduleEvents.ForEach(s =>
                    {
                        var documentType = DocumentFactory.GetType(s.DisciplineTask, false);
                        var documentTypeNumber = (int)documentType;
                        if (documentTypeNumber != (int)DocumentType.None)
                        {

                            if ((userTaskViewData.ViewDocuments & documentTypeNumber) == documentTypeNumber)
                            {
                                //set episode range to schedule
                                s.StartDate = episode.StartDate;
                                s.EndDate = episode.EndDate;

                                var isMER = s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport;
                                var isCaseManagmentStatus = caseManagmentStatus.Contains(s.Status);
                                var isComplete = false;
                                if (s.IsMissedVisit)
                                {
                                    isComplete = missedVisitCompletedStatus.Contains(s.Status);
                                }
                                else
                                {
                                    isComplete = completedStatus.Contains(s.Status);
                                }
                                var gridItem = new GridTask
                                {
                                    Service = this.Service,
                                    Id = s.Id,
                                    PatientId = episode.PatientId,
                                    EpisodeId = s.EpisodeId,
                                    DateIn = s.EventDate,
                                    Range = s.EventDateTimeRange,
                                    StartDate = episode.StartDate,
                                    EndDate = episode.EndDate,
                                    TaskName = s.DisciplineTaskName,
                                    StatusName = s.StatusName,
                                    IsUserCanPrint = (userTaskViewData.PrintDocuments & documentTypeNumber) == documentTypeNumber,
                                    IsUserCanDelete = !isFrozen && (userTaskViewData.DeleteDocuments & documentTypeNumber) == documentTypeNumber && !s.IsMissedVisit && !isMER && !isCaseManagmentStatus,
                                    IsUserCanReassign = !isFrozen && userTaskViewData.IsUserCanReassign && !isComplete && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                    IsUserCanReopen = !isFrozen && isComplete && userTaskViewData.IsUserCanReopen && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                    IsUserCanEditDetail = userTaskViewData.IsUserCanEditDetail && !s.IsMissedVisit && !isMER,
                                    IsUserCanRestore = !isFrozen && s.IsMissedVisit && userTaskViewData.IsUserCanRestore,
                                    IsUserCanEdit = !isFrozen && (userTaskViewData.EditDocuments & documentTypeNumber) == documentTypeNumber && !s.IsOrphaned && !isComplete,
                                    IsOrphaned = s.IsOrphaned,
                                    IsMissedVisit = s.IsMissedVisit,
                                    IsInQA = isCaseManagmentStatus
                                };
                                SetScheduleTasksDefaultAppSpecific(gridItem, s);
                                if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                                {
                                    gridItem.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                                }
                                if (gridItem.IsUserCanPrint)
                                {
                                    gridItem.Group = s.IsMissedVisit ? DocumentType.MissedVisit.ToString().ToLower() : documentType.ToString().ToLowerCase();
                                }
                                gridItem.IsComplete = isComplete;

                                if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                                {
                                    gridItem.UserName = "Axxess";
                                }
                                else
                                {
                                    if (!s.UserId.IsEmpty())
                                    {
                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                        if (user != null)
                                        {
                                            gridItem.UserName = user.DisplayName;
                                        }
                                    }
                                }

                                if (!isFrozen)
                                {
                                    if (s.Asset.IsNotNullOrEmpty())
                                    {
                                        var assets = s.Asset.ToObject<List<Guid>>();
                                        if (assets != null && assets.Count > 0)
                                        {
                                            s.Asset = string.Empty;
                                            gridItem.IsAttachmentExist = true;
                                        }
                                    }
                                    if (userTaskViewData.IsStickyNote)
                                    {
                                        var statusComment = string.Empty;
                                        if (s.IsMissedVisit)
                                        {
                                            var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.Id);
                                            if (missedVisit != null)
                                            {
                                                statusComment = missedVisit.ToString().Clean() + "\r\n";

                                            }
                                        }
                                        if (s.ReturnReason.IsNotNullOrEmpty() && !completelyFinishedStatus.Contains(s.Status))
                                        {
                                            statusComment += s.ReturnReason;
                                        }
                                        gridItem.StatusComment = statusComment;
                                        gridItem.EpisodeNotes = detailComment;
                                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList();
                                        if (eventReturnReasons.IsNotNullOrEmpty())
                                        {
                                            gridItem.ReturnReason = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                                        }
                                        gridItem.Comments = s.Comments;
                                    }
                                }
                                list.Add(gridItem);
                            }
                        }
                    });
                }
            }
            userTaskViewData.ScheduleEvents = list;
        }

        protected void ProcessUserScheduleEventsActions(UserTaskViewData userTaskViewData, List<E> patientEpisodes, List<T> scheduleEvents, bool needsPatientNames)
        {
            var list = new List<GridTask>();
            var episodeWithEventsDictionary = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.Key, g => g.ToList());
            if (episodeWithEventsDictionary.IsNotNullOrEmpty())
            {
                var isFrozen = Current.IsAgencyFrozen;

                var caseManagmentStatus = ScheduleStatusFactory.CaseManagerStatus();

                var completedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(true);

                var completelyFinishedStatus = ScheduleStatusFactory.OnAndAfterQAStatus(false);

                var missedVisitCompletedStatus = ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true);

                var episodeIds = episodeWithEventsDictionary.Keys.ToList();
                var users = new List<User>();
                var patientNames = new Dictionary<Guid, string>();
                var userIds = new List<Guid>();
                var returnComments = new List<ReturnComment>();
                var missedVisits = new List<MissedVisit>();
                if (userTaskViewData.IsStickyNote)
                {
                    returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds);
                    if (returnComments.IsNotNullOrEmpty())
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds.IsNotNullOrEmpty())
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }

                    var eventIds = scheduleEvents.Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
                    if (eventIds.IsNotNullOrEmpty())
                    {
                        missedVisits = baseScheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();
                    }
                }

                userIds = userIds.Union((from evnt in scheduleEvents where !evnt.UserId.IsEmpty() select evnt.UserId)).ToList();
                if (userIds.IsNotNullOrEmpty())
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds);
                }
                //PatientAccessInformation patientAccessInfo = null;
                //if (Current.IsClinicianOrHHA)
                //{
                //    patientAccessInfo = new PatientAccessInformation();
                //    patientAccessInfo.PatientUsers = patientRepository.GetPatientUsersByUser(Current.UserId);
                //    patientAccessInfo.TeamPatients = agencyRepository.GetTeamAccess(Current.AgencyId, Current.UserId);
                //    var patients = profileRepository.GetPatientLeanByUserId(Current.AgencyId, Current.UserId, needsPatientNames);
                //    patientAccessInfo.PatientUserIds = patients.ToDictionary(k => k.Id, e => Current.UserId);
                //    if (needsPatientNames)
                //    {
                //        patientNames = patients.ToDictionary(k => k.Id, e => e.DisplayNameWithMi);
                //    }
                //}
                if (needsPatientNames)
                {
                    var schedulePatientIds = scheduleEvents.Where(s => !s.PatientId.IsEmpty()).Select(s => s.PatientId).Distinct().ToList();
                    patientNames = patientRepository.GetPatientNamesByIds(Current.AgencyId, schedulePatientIds);
                }


                episodeWithEventsDictionary.ForEach((key, value) =>
                {
                    if (value != null && value.Count > 0)
                    {
                        var episode = patientEpisodes.FirstOrDefault(e => e.Id == key);
                        if (episode != null)
                        {
                            var detailComment = string.Empty;
                            if (userTaskViewData.IsStickyNote)
                            {
                                if (episode.Comments.IsNotNullOrEmpty())
                                {
                                    detailComment = episode.Comments.Clean();
                                }
                            }
                            value.ForEach(s =>
                            {
                                var documentType = DocumentFactory.GetType(s.DisciplineTask, false);
                                var documentTypeNumber = (int)documentType;
                                if (documentTypeNumber != (int)DocumentType.None)
                                {
                                    if ((userTaskViewData.ViewDocuments & documentTypeNumber) == documentTypeNumber)
                                    {
                                        //set episode range to schedule
                                        s.StartDate = episode.StartDate;
                                        s.EndDate = episode.EndDate;

                                        var isMER = s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport;
                                        var isCaseManagmentStatus = caseManagmentStatus.Contains(s.Status);
                                        var isComplete = s.IsMissedVisit ? missedVisitCompletedStatus.Contains(s.Status) : completedStatus.Contains(s.Status);
                                        var gridItem = new GridTask
                                        {
                                            Service = this.Service,
                                            Id = s.Id,
                                            PatientId = episode.PatientId,
                                            EpisodeId = s.EpisodeId,
                                            DateIn = s.EventDate,
                                            Range = s.EventDateTimeRange,
                                            StartDate = episode.StartDate,
                                            EndDate = episode.EndDate,
                                            TaskName = s.DisciplineTaskName,
                                            StatusName = s.StatusName,
                                            IsUserCanPrint = (userTaskViewData.PrintDocuments & documentTypeNumber) == documentTypeNumber,
                                            IsUserCanDelete = !isFrozen && (userTaskViewData.DeleteDocuments & documentTypeNumber) == documentTypeNumber && !isMER && !isCaseManagmentStatus,
                                            IsUserCanReassign = !isFrozen && userTaskViewData.IsUserCanReassign && !s.IsMissedVisit && !isComplete && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                            IsUserCanReopen = !isFrozen && isComplete && userTaskViewData.IsUserCanReopen && !s.IsMissedVisit && !s.IsOrphaned && !isMER && !isCaseManagmentStatus,
                                            IsUserCanEditDetail = userTaskViewData.IsUserCanEditDetail && !s.IsMissedVisit && !isMER,
                                            IsUserCanRestore = !isFrozen && s.IsMissedVisit && userTaskViewData.IsUserCanRestore,
                                            IsUserCanEdit = !isFrozen && (userTaskViewData.EditDocuments & documentTypeNumber) == documentTypeNumber && !s.IsOrphaned && !isComplete,
                                            IsOrphaned = s.IsOrphaned,
                                            IsMissedVisit = s.IsMissedVisit,
                                            IsInQA = isCaseManagmentStatus

                                        };
                                        SetScheduleTasksDefaultAppSpecific(gridItem, s);
                                        if (needsPatientNames)
                                        {
                                            if (patientNames.ContainsKey((s.PatientId)))
                                            {
                                                gridItem.PatientName = patientNames[s.PatientId];
                                            }
                                        }

                                        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                                        {
                                            gridItem.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
                                        }
                                        if (gridItem.IsUserCanPrint)
                                        {
                                            gridItem.Group = s.IsMissedVisit ? DocumentType.MissedVisit.ToString().ToLower() : documentType.ToString().ToLowerCase();
                                        }
                                        gridItem.IsComplete = isComplete;
                                        if (s.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                                        {
                                            gridItem.UserName = "Axxess";
                                        }
                                        else
                                        {
                                            if (!s.UserId.IsEmpty())
                                            {
                                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                                if (user != null)
                                                {
                                                    gridItem.UserName = user.DisplayName;
                                                }
                                            }
                                        }

                                        if (!isFrozen)
                                        {

                                            if (s.Asset.IsNotNullOrEmpty())
                                            {
                                                var assets = s.Asset.ToObject<List<Guid>>();
                                                if (assets != null && assets.Count > 0)
                                                {
                                                    s.Asset = string.Empty;
                                                    gridItem.IsAttachmentExist = true;
                                                }
                                            }
                                            if (userTaskViewData.IsStickyNote)
                                            {
                                                var statusComment = string.Empty;
                                                if (s.IsMissedVisit)
                                                {
                                                    var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.Id);
                                                    if (missedVisit != null)
                                                    {
                                                        statusComment = missedVisit.ToString().Clean() + "\r\n";
                                                    }
                                                }
                                                if (s.ReturnReason.IsNotNullOrEmpty() && !completelyFinishedStatus.Contains(s.Status))
                                                {
                                                    statusComment += s.ReturnReason;
                                                }
                                                gridItem.StatusComment = statusComment;
                                                gridItem.EpisodeNotes = detailComment;
                                                var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id).ToList();
                                                if (eventReturnReasons.IsNotNullOrEmpty())
                                                {
                                                    gridItem.ReturnReason = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
                                                }
                                                gridItem.Comments = s.Comments;
                                            }
                                        }
                                        list.Add(gridItem);
                                    }
                                }
                            });
                        }
                    }
                });
            }
            userTaskViewData.ScheduleEvents = list;
        }



        protected abstract string TableName(int task);

        //protected abstract bool IsUserAvailable(Guid userId, DateTime from, DateTime to, out string message);

        protected string GenerateUserAvailablilityMessage(Guid patientId)
        {
            string message = "This user has already been scheduled at this time.";
            if (!patientId.IsEmpty())
            {
                string patientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                if (patientName.IsNotNullOrEmpty())
                {
                    message = string.Format("This user has already been scheduled for {0} at this time.", patientName);
                }
            }
            return message;
        }

        protected abstract void SetScheduleTasksDefaultAppSpecific(T ev);

        protected abstract void SetScheduleTasksDefaultAppSpecific(GridTask task, T ev);

        protected bool GetViewAndSetEditPermission(IDictionary<int, Dictionary<int, List<int>>> permission, ParentPermission parent,ref int editDocuments, DocumentType documentType)
        {
            var canView = false;
           
            var parentPermission = permission.GetCategoryPermission(parent);
            if (parentPermission.IsInPermission(this.Service, PermissionActions.ViewList))
            {
                if (parentPermission.IsInPermission(this.Service, PermissionActions.Edit))
                {
                    editDocuments |= (int)documentType;
                }
                canView = true;
            }
            return canView;
        }

        #endregion
    }
}
