﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI.WebControls;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.Workflows;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Entities.Enums;

    public abstract class EpisodeService<E> where E : CarePeriod, new()
    {
        protected IPhysicianRepository physicianRepository;
        protected IPatientRepository patientRepository;
        protected PatientAdmissionAbstract patientAdmissionRepository;
        protected PatientProfileAbstract patientProfileRepository;

        private EpisodeAbstract<E> baseEpisodeRepository;

        protected AgencyServices Service;

        protected EpisodeService(EpisodeAbstract<E> episodeRepository)
        {
            this.baseEpisodeRepository = episodeRepository;
        }

        public List<EpisodeDate> EpisodeRangeList(Guid patientId)
        {
            var viewData = new List<EpisodeDate>();
            if (!patientId.IsEmpty())
            {
                var episodes = baseEpisodeRepository.GetPatientActiveEpisodesLean(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate).ToList();
                if (episodes != null && episodes.Count > 0)
                {
                    viewData = episodes.Select(e => new EpisodeDate { Id = e.Id, StartDate = e.StartDate, EndDate = e.EndDate }).ToList();
                }
            }
            return viewData;
        }

        public DateRange GetEpisodeDateRange(Guid patientId, Guid episodeId)
        {
            return baseEpisodeRepository.GetEpisodeDateRange(Current.AgencyId, patientId, episodeId);
        }

        public List<E> GetPatientActiveEpisodesLeanByDateRange(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return baseEpisodeRepository.GetPatientActiveEpisodesLeanByDateRange(Current.AgencyId,patientId,startDate,endDate);
        }



        public bool AddEpisode(E patientEpisode)
        {
            bool result = false;
            patientEpisode.AgencyId = Current.AgencyId;
            if (baseEpisodeRepository.AddEpisode(patientEpisode))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public E GetEpisodeOnly(Guid episodeId, Guid patientId)
        {
            return baseEpisodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
        }

        public E GetLastEpisodeLean(Guid patientId)
        {
            return baseEpisodeRepository.GetLastEpisodeLean(Current.AgencyId, patientId);
        }

        public JsonViewData AddEpisodeAndCheckValidation(E patientEpisode, JsonViewData viewData)
        {
            var patient = patientRepository.GetPatientOnly(patientEpisode.PatientId, Current.AgencyId);
            if (patient != null)
            {
                var rules = EpisodeValidation(patient, patientEpisode);
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
               
                if (entityValidator.IsValid)
                {
                    patientEpisode.Id = Guid.NewGuid();
                    if (AddEpisode(patientEpisode))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The episode was successfully added.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return viewData;
        }

        //public bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        //{
        //    var episodes = baseEpisodeRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
        //    bool result = true;
        //    if (episodes != null)
        //    {
        //        foreach (var e in episodes)
        //        {
        //            if (e.Id != episodeId)
        //            {
        //                if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
        //                {
        //                    result = false;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate)
        //{

        //    return !baseEpisodeRepository.IsEpisodeExist(Current.AgencyId, patientId, startDate, endDate);
        //    //var episodes = baseEpisodeRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
        //    //bool result = true;
        //    //if (episodes != null)
        //    //{
        //    //    if (episodes.Any(e => (startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date)))
        //    //    {
        //    //        result = false;
        //    //    }
        //    //}
        //    //return result;
        //}

        public bool IsEpisodeExist(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return baseEpisodeRepository.IsEpisodeExist(Current.AgencyId, patientId, startDate, endDate);
        }

        public bool IsEpisodeExist(Guid patientId, DateTime startDate, DateTime endDate, Guid excludedEpisodeIdOptional)
        {
            return baseEpisodeRepository.IsEpisodeExist(Current.AgencyId, patientId, startDate, endDate, excludedEpisodeIdOptional);
        }


        public abstract List<Validation> EpisodeValidation(Patient patient, E patientEpisode);
        
        public JsonViewData ActivateEpisode(Guid patientId, Guid episodeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = GetPartialMessageType() + " could not be activated. Try again." };
            var patientEpisode = baseEpisodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var validationRules = new List<Validation>();
                validationRules.Add(new Validation(() => IsEpisodeExist(patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate,patientEpisode.Id), GetPartialMessageType() + " date is not in the valid date range."));
                var entityValidator = new EntityValidator(validationRules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (baseEpisodeRepository.ActivateEpisode(Current.AgencyId, patientId, episodeId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeActivated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.IsDataCentersRefresh = true;
                        viewData.PatientId = patientId;
                        viewData.EpisodeId = episodeId;
                        viewData.ServiceId = (int)Service;
                        viewData.Service = Service.ToString();
                        viewData.errorMessage = GetPartialMessageType() + " has been successfully activated.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            else
            {
                viewData.errorMessage = GetPartialMessageType() + "  could not be found. Try again.";
            }
            return viewData;
        }

        public JsonViewData DeactivateEpisode(Guid patientId, Guid episodeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = GetPartialMessageType() + " could not be deactivated. Try again." };
            if (baseEpisodeRepository.DeactivateEpisode(Current.AgencyId, patientId, episodeId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, episodeId.ToString(), LogType.Episode, LogAction.EpisodeDeactivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.IsDataCentersRefresh = true;
                viewData.PatientId = patientId;
                viewData.EpisodeId = episodeId;
                viewData.ServiceId = (int)Service;
                viewData.Service = Service.ToString();
                viewData.errorMessage = GetPartialMessageType() + " has been successfully deactivated.";
            }
            return viewData;
        }

        public bool RemoveEpisode(Guid patientId, Guid episodeId)
        {
            return baseEpisodeRepository.RemoveEpisode(Current.AgencyId, patientId, episodeId);
        }

        protected abstract string GetPartialMessageType();

        public virtual JsonViewData UpdateEpisode(E episode)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = GetPartialMessageType() + " could not be saved.";
            var patient = patientRepository.GetPatientOnly(episode.PatientId, Current.AgencyId);
            var entityValidator = new EntityValidator(EpisodeValidation(patient, episode).ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                var episodeToEdit = baseEpisodeRepository.GetEpisodeOnly(Current.AgencyId, episode.Id, episode.PatientId);
                if (episodeToEdit != null)
                {
                    var oldEpisodeStatus = episodeToEdit.IsActive;
                    var oldStartDate = episodeToEdit.StartDate;
                    var oldEndDate = episodeToEdit.EndDate;
                    var oldComments = episodeToEdit.Comments;

                    var logAction = episodeToEdit.IsActive == episode.IsActive ? LogAction.EpisodeEdited : (!episode.IsActive ? LogAction.EpisodeDeactivated : LogAction.EpisodeActivated);
                    episodeToEdit.IsActive = episode.IsActive;
                    episodeToEdit.StartDate = episode.StartDate;
                    episodeToEdit.PhysicianId = episode.PhysicianId;
                    episodeToEdit.CaseManagerId = episode.CaseManagerId;
                    episodeToEdit.PrimaryInsurance = episode.PrimaryInsurance;
                    episodeToEdit.SecondaryInsurance = episode.SecondaryInsurance;
                    episodeToEdit.Comments = episode.Comments;
                    episodeToEdit.EndDate = episode.EndDate;
                    if (episode.StartOfCareDate.IsValid())
                    {
                        episodeToEdit.StartOfCareDate = episode.StartOfCareDate;
                    }
                    episodeToEdit.AdmissionId = episode.AdmissionId;
                    if (baseEpisodeRepository.UpdateEpisode(episodeToEdit))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, episodeToEdit.PatientId, episodeToEdit.Id.ToString(), LogType.Episode, logAction, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.PatientId = episodeToEdit.PatientId;
                        viewData.EpisodeId = episodeToEdit.Id;
                        viewData.ServiceId = (int)Service;
                        viewData.Service = Service.ToString();
                        viewData.IsDataCentersRefresh = oldEpisodeStatus != episodeToEdit.IsActive || oldStartDate.Date != episodeToEdit.StartDate.Date || oldEndDate.Date != episodeToEdit.EndDate.Date || oldComments != episodeToEdit.Comments;
                        viewData.errorMessage = GetPartialMessageType() + " has been successfully updated.";
                    }
                    else
                    {
                        viewData.errorMessage = errorMessage;
                    }
                }
                else
                {
                    viewData.errorMessage = GetPartialMessageType() + " could not be found. Try again.";
                }

            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return viewData;
        }

        public List<E> GetPatientEpisodesLean(Guid patientId, bool? isActive)
        {
            return baseEpisodeRepository.GetPatientEpisodesLean(Current.AgencyId, patientId, isActive);
        }

        public List<EpisodeLean> GetPatientEpisodesByIds(Guid patientId, List<Guid> episodeIds)
        {
            return baseEpisodeRepository.GetPatientEpisodesByIds(Current.AgencyId, patientId, episodeIds);
        }

        public List<AdmissionEpisode> GetPatientAdmissonPeriods(Guid patientId)
        {
            return baseEpisodeRepository.GetPatientAdmissonPeriods(Current.AgencyId, patientId);
        }

        public List<EpisodeLean> GetPatientAdmissonPeriodEpisodes(Guid patientId, Guid admissionId)
        {
            return baseEpisodeRepository.GetPatientAdmissonPeriodEpisodes(Current.AgencyId, patientId, admissionId);
        }

        public NewEpisodeData GetNewEpisodeData(Guid patientId, bool createNewAdmissionIfNotExist)
        {
            var episodeViewData = new NewEpisodeData { Service = this.Service };
            if (!patientId.IsEmpty())
            {
                var profile = patientProfileRepository.GetProfileOnly( Current.AgencyId,patientId);
                if (profile != null)
                {
                    episodeViewData.PatientId = patientId;
                    episodeViewData.DisplayName = profile.DisplayName;
                    episodeViewData.StartOfCareDate = profile.StartofCareDate;
                    episodeViewData.CaseManager = profile.CaseManagerId.ToString();
                    episodeViewData.PrimaryInsurance = profile.PrimaryInsurance.ToString();
                    episodeViewData.SecondaryInsurance = profile.SecondaryInsurance.ToString();
                    episodeViewData.AdmissionId = profile.AdmissionId;
                    episodeViewData.AgencyLocationId = profile.AgencyLocationId;
                    var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        episodeViewData.PrimaryPhysician = physician.Id.ToString();
                    }
                    var selection = AdmissionHelperFactory<E>.GetAdmissionSelectList( profile, createNewAdmissionIfNotExist);
                    episodeViewData.AdmissionDates = selection;
                    var episode = GetLastEpisodeLean(patientId);
                    if (episode != null)
                    {
                        episodeViewData.EndDate = episode.EndDate;
                        episodeViewData.StartDate = episode.StartDate;
                    }
                    else
                    {
                        episodeViewData.EndDate = DateTime.MinValue.Date;
                        episodeViewData.StartDate = DateTime.MinValue.Date;
                    }
                }
            }
            return episodeViewData;
        }

        public E GetEpisodeFromDate(Guid patientId, DateTime date)
        {
            return baseEpisodeRepository.GetEpisodeFromDate(Current.AgencyId, patientId, date);
        }
    }
}
