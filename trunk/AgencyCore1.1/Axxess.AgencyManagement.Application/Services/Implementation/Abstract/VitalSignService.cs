﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;
    using System;

    public abstract class VitalSignService
    {
        protected VitalSignAbstract vitalSignRepository;

        public VitalSignService(VitalSignAbstract vitalSignRepository)
        {
            this.vitalSignRepository = vitalSignRepository;
        }

        public JsonViewData Add(VitalSignLog log)
        {
            var viewData = new JsonViewData(false, "A problem occured while creating the vital signs.");
            if (log != null)
            {
                if(log.IsValid())
                {
                    log.AgencyId = Current.AgencyId;
                    if (vitalSignRepository.Add(log))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The vital signs have been created successfully.";
                    }
                }
                else
                {
                    viewData.errorMessage = log.ValidationMessage;
                }
            }
            return viewData;
        }

        public JsonViewData Update(VitalSignLog log)
        {
            var viewData = new JsonViewData(false, "A problem occured while updating the vital signs.");
            if (log != null)
            {
                if (log.IsValid())
                {
                    var oldLog = vitalSignRepository.Get(log.Id, log.EntityId, log.PatientId, Current.AgencyId);
                    if (vitalSignRepository.UpdateModal(oldLog, log))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The vital signs have been updated successfully.";
                    }
                }
                else
                {
                    viewData.errorMessage = log.ValidationMessage;
                }
            }
            return viewData;
        }

        public VitalSignLog Get(Guid id, Guid entityId, Guid patientId)
        {
            return vitalSignRepository.Get(id, entityId, patientId, Current.AgencyId);
        }

        public List<VitalSignLogLean> GetEntitiesVitalSignLogsLean(Guid entityId, Guid patientId)
        {
            return vitalSignRepository.GetEntitiesVitalSignLogsLean(entityId, patientId, Current.AgencyId);
        }

        public JsonViewData ToggleDeprecation(Guid patientId, Guid id, bool isDeprecated)
        {
            var viewData = new JsonViewData(false, "A problem occured while " + (isDeprecated ? "deleting" : "restoring") + " the vital sign.");
            if (vitalSignRepository.ToggleDeprecation(Current.AgencyId, patientId, id, isDeprecated))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The vital sign has been successfully " + (isDeprecated ? "deleted" : "resto") + ".";
            }
            return viewData;
        }
    }
}
