﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Transactions;

    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.AgencyManagement.Application;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Helpers;

    using Axxess.Log.Enums;

    using SubSonic.DataProviders;

    public abstract class CommunicationNoteService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;

        private CommunicationNoteAbstract baseNoteRepository;
        private TaskScheduleAbstract<T> baseScheduleRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;
        protected AgencyServices Service { get; set; }


        protected CommunicationNoteService(TaskScheduleAbstract<T> baseScheduleRepository,
            CommunicationNoteAbstract baseNoteRepository,
            EpisodeAbstract<E> baseEpisodeRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseNoteRepository = baseNoteRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
        }


        #region Public Methods

        public JsonViewData AddScheduleTaskAndCommunicationNoteHelper(Guid patientId, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected communication note task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var communicationNotes = this.GetCommunicationNoteFromTasks(scheduleEvents);
                if (communicationNotes != null)
                {
                    if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (baseNoteRepository.AddMultipleCommunicationNote(communicationNotes))
                        {
                            viewData.isSuccessful = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            viewData.IsCommunicationNoteRefresh = true;
                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }

                }
            }
            return viewData;
        }

        public JsonViewData ProcessCommunicationNotes(Guid patientId, Guid eventId, string button, string reason)
        {
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                return ProcessCommunicationNotes(scheduleEvent, button, reason);
            }
            return new JsonViewData(false, "Unable to find communication note. Please try again.");
        }

        public JsonViewData ProcessCommunicationNotes(T task, string button, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            bool isCommunicationNoteToBeUpdated = true;
            bool isActionSet = false;
            if (task != null)
            {
                var description = string.Empty;
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var communicationNote = baseNoteRepository.GetCommunicationNote(task.Id, task.PatientId, Current.AgencyId);
                        if (communicationNote != null)
                        {
                            if (!communicationNote.EpisodeId.IsEmpty())
                            {
                                viewData.PatientId = task.PatientId;
                                viewData.EpisodeId = task.EpisodeId;
                                viewData.UserIds = new List<Guid> { task.UserId };

                                var oldStatus = task.Status;

                                if (button == "Approve")
                                {
                                    communicationNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                    task.InPrintQueue = true;
                                    task.Status = communicationNote.Status;
                                    isActionSet = true;
                                    description = "Approved By:" + Current.UserFullName;
                                }
                                else if (button == "Return")
                                {
                                    communicationNote.Status = ((int)ScheduleStatus.NoteReturned);
                                    communicationNote.SignatureText = string.Empty;
                                    communicationNote.SignatureDate = DateTime.MinValue;
                                    task.Status = communicationNote.Status;
                                    isActionSet = true;
                                    description = "Returned By:" + Current.UserFullName;
                                }
                                else if (button == "Print")
                                {
                                    task.InPrintQueue = false;
                                    isCommunicationNoteToBeUpdated = false;
                                    isActionSet = true;
                                }
                                if (isActionSet)
                                {
                                    if (baseScheduleRepository.UpdateScheduleTask(task))
                                    {
                                        if (isCommunicationNoteToBeUpdated)
                                        {
                                            bool returnCommentStatus = true;
                                            if (reason.IsNotNullOrEmpty())
                                            {
                                                returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                            }
                                            if (returnCommentStatus)
                                            {
                                                communicationNote.Modified = DateTime.Now;
                                                if (baseNoteRepository.UpdateCommunicationNoteModal(communicationNote))
                                                {
                                                    viewData.Service = Service.ToString();
                                                    viewData.ServiceId = (int)Service;
                                                    viewData.IsDataCentersRefresh = oldStatus != task.Status;
                                                    viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature);
                                                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, task);
                                                    viewData.IsCommunicationNoteRefresh = viewData.IsDataCentersRefresh;
                                                    viewData.isSuccessful = true;
                                                    ts.Complete();
                                                }
                                                else
                                                {
                                                    viewData.errorMessage = "A problem occured while updating the communication note.";
                                                    ts.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                viewData.errorMessage = "A problem occured while adding the return comment.";
                                                ts.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            viewData.isSuccessful = true;
                                            ts.Complete();
                                        }
                                    }
                                    else
                                    {
                                        viewData.errorMessage = "A problem occured while updating the task.";
                                        ts.Dispose();
                                    }
                                }
                            }
                        }
                    }
                }
                if (viewData.isSuccessful)
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, Actions.StatusChange, DisciplineTasks.CommunicationNote, description);
                }
            }
            return viewData;
        }

        public JsonViewData ReopenCommunicationNote(T scheduleTask)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleTask != null)
            {
                var oldUserId = scheduleTask.UserId;
                var oldStatus = scheduleTask.Status;
                var description = string.Empty;

                viewData.PatientId = scheduleTask.PatientId;
                viewData.EpisodeId = scheduleTask.EpisodeId;

                scheduleTask.Status = ((int)ScheduleStatus.NoteReopened);
                var communicationNote = baseNoteRepository.GetCommunicationNote(scheduleTask.Id, scheduleTask.PatientId, Current.AgencyId);
                if (communicationNote != null)
                {
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                    {
                        communicationNote.UserId = scheduleTask.UserId;
                        communicationNote.Status = (int)ScheduleStatus.NoteReopened;
                        communicationNote.SignatureText = string.Empty;
                        communicationNote.SignatureDate = DateTime.MinValue;
                        communicationNote.Modified = DateTime.Now;

                        if (baseNoteRepository.UpdateCommunicationNoteModal(communicationNote))
                        {
                            viewData.isSuccessful = true;
                            viewData.IsCommunicationNoteRefresh = true;
                            viewData.Service = this.Service.ToString();
                            viewData.ServiceId = (int)this.Service;
                            viewData.UserIds = new List<Guid> { communicationNote.UserId };

                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleTask.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleTask.UserId = oldUserId;
                            scheduleTask.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleTask);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ToggleCommunicationNote(Guid patientId, Guid Id, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Communication Note could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, Id);
            if (task != null)
            {
                viewData = ToggleCommunicationNote(task, isDeprecated);
            }
            return viewData;
        }

        public JsonViewData ToggleCommunicationNote(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Communication Note could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (baseNoteRepository.DeleteCommunicationNote(Current.AgencyId, task.Id, task.PatientId, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.IsCommunicationNoteRefresh = true;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.UserIds = new List<Guid> { task.UserId };
                        viewData.errorMessage = string.Format("The Communication Note has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        public CommunicationNote GetCommunicationNote(Guid patientId, Guid Id)
        {
            var communicationNote = baseNoteRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            if (communicationNote != null && !communicationNote.PhysicianId.IsEmpty())
            {
                if ((communicationNote.Status == (int)ScheduleStatus.NoteCompleted || communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !communicationNote.PhysicianId.IsEmpty() && communicationNote.PhysicianData.IsNotNullOrEmpty())
                {
                    AgencyPhysician physician = communicationNote.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        communicationNote.PhysicianName = physician.DisplayName;
                    }
                    else
                    {
                        communicationNote.PhysicianName = PhysicianEngine.GetName(communicationNote.PhysicianId, Current.AgencyId);
                    }
                }
                else
                {
                    communicationNote.PhysicianName = PhysicianEngine.GetName(communicationNote.PhysicianId, Current.AgencyId);
                }
            }
            return communicationNote;
        }

        public CommunicationNote GetCommunicationNoteEdit(Guid patientId, Guid Id)
        {
            var communicationNote = baseNoteRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            if (communicationNote != null)
            {
                communicationNote.Service = this.Service;
                communicationNote.DisplayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                communicationNote.RecipientArray = communicationNote.Recipients.IsNotNullOrEmpty() ? communicationNote.Recipients.ToObject<List<Guid>>() : new List<Guid>();
                communicationNote.SignatureDate = DateTime.Now;
                communicationNote = GetCommunicationNoteEpisodeData(communicationNote);
                communicationNote.StatusComment = baseScheduleRepository.GetReturnReason(Current.AgencyId, communicationNote.PatientId, communicationNote.Id, Current.UserId);
                var permission = Current.Permissions;
                if (permission.IsNotNullOrEmpty())
                {
                    communicationNote.IsUserCanEdit = permission.IsInPermission(this.Service, ParentPermission.CommunicationNote, PermissionActions.Edit);
                    communicationNote.IsUserCanAddPhysicain = permission.IsInPermission(this.Service, ParentPermission.Physician, PermissionActions.Add);
                }

            }
            else
            {
                communicationNote = new CommunicationNote { Service = this.Service };
            }
            return communicationNote;
        }

        public CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId)
        {
            var note = new CommunicationNote();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                note = baseNoteRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (note != null)
                {
                    if ((note.Status == (int)ScheduleStatus.NoteCompleted || note.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !note.PhysicianId.IsEmpty() && note.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = note.PhysicianData.ToObject<AgencyPhysician>();
                        note.Physician = physician ?? PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                    }
                    else
                    {
                        note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                    }
                    var profile = profileRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
                    if (profile != null)
                    {
                        note.PatientProfile = profile;
                        note.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
                    }
                }
            }
            return note;
        }

        public JsonViewData AddCommunicationNote(CommunicationNote communicationNote)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be created." };
            if (communicationNote.IsValid())
            {
                var validationResults = communicationNote.SignValidation(ServiceHelper.IsSignatureCorrect(Current.UserId, communicationNote.SignatureText));
                if (validationResults.IsSuccessful)
                {
                    viewData = this.AddCommunicationNote(communicationNote, validationResults.IsSigned);
                    if (viewData.isSuccessful)
                    {
                        viewData.IsCommunicationNoteRefresh = true;
                        viewData.errorMessage = "Communication Note has been successfully saved.";
                    }
                }
                else
                {
                    viewData.errorMessage = validationResults.ValidationMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateCommunicationNote(CommunicationNote communicationNote)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved." };
            if (communicationNote.IsValid())
            {
                var validationResults = communicationNote.SignValidation(ServiceHelper.IsSignatureCorrect(Current.UserId, communicationNote.SignatureText));
                if (validationResults.IsSuccessful)
                {
                    viewData = this.UpdateCommunicationNote(communicationNote, validationResults.IsSigned);
                    if (viewData.isSuccessful)
                    {
                        viewData.IsCommunicationNoteRefresh = true;
                        viewData.errorMessage = "Communication Note has been successfully saved.";
                    }
                }
                else
                {
                    viewData.errorMessage = validationResults.ValidationMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return viewData;
        }

        public bool UpdateCommunicationNoteForScheduleDetail(T schedule)
        {
            var result = false;
            var communicationNote = baseNoteRepository.GetCommunicationNote(schedule.Id, schedule.PatientId, Current.AgencyId);
            if (communicationNote != null)
            {
                communicationNote.EpisodeId = schedule.EpisodeId;
                communicationNote.IsDeprecated = schedule.IsDeprecated;
                communicationNote.Status = schedule.Status;
                if (schedule.VisitDate.IsValid())
                {
                    communicationNote.Created = schedule.VisitDate;
                }
                if (communicationNote.Status != schedule.Status && communicationNote.Status == (int)ScheduleStatus.NoteCompleted && !communicationNote.PhysicianId.IsEmpty())
                {
                    var physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        communicationNote.PhysicianData = physician.ToXml();
                    }
                }
                result = baseNoteRepository.UpdateCommunicationNoteModal(communicationNote);
            }
            else
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Gets the communication notes of the patient
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="episodeId">
        /// If null the method will get the current care period of the patient and filter the communication notes by it
        /// If it is empty then it will return an empty list.
        /// </param>
        /// <returns></returns>
        public List<CommunicationNote> GetCommunicationNotes(Guid patientId, ref Guid? episodeId, bool isExport)
        {
            List<CommunicationNote> commNotes = null;
            if (!episodeId.HasValue)
            {
                var episode = baseEpisodeRepository.GetCurrentEpisodeLean(Current.AgencyId, patientId);
                if (episode != null)
                {
                    episodeId = episode.Id;
                    commNotes = baseNoteRepository.GetCommunicationNotes(Current.AgencyId, patientId, episode.Id);
                }
            }
            else if (episodeId.Value != Guid.Empty)
            {
                commNotes = baseNoteRepository.GetCommunicationNotes(Current.AgencyId, patientId, episodeId.Value);
            }
            else
            {
                commNotes = new List<CommunicationNote>();
            }
            if (commNotes.IsNotNullOrEmpty())
            {
                var ids = commNotes.Select(cn => cn.Id).Distinct().ToList();
                var schedules = baseScheduleRepository.GetScheduledEventsLeanWithId(Current.AgencyId, patientId, ids);
                var userIds = commNotes.Select(cn => cn.UserId).Distinct().ToList();
                var physicianIds = commNotes.Select(cn => cn.PhysicianId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                var physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds) ?? new List<AgencyPhysician>();
                var tempCommNotes = new List<CommunicationNote>();
                string patientDisplayName = string.Empty;
                if (isExport) patientDisplayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                commNotes.ForEach(c =>
                {
                    var task = schedules.FirstOrDefault(s => s.Id == c.Id);
                    if (task != null)
                    {
                        if (!c.UserId.IsEmpty())
                        {
                            var user = users.FirstOrDefault(u => u.Id == c.UserId);
                            if (user != null)
                            {
                                c.UserDisplayName = user.DisplayName;
                            }
                        }
                        if (!c.PhysicianId.IsEmpty())
                        {
                            var physician = physicians.FirstOrDefault(p => p.Id == c.PhysicianId);
                            if (physician != null)
                            {
                                c.PhysicianName = physician.DisplayName;
                            }
                        }
                        c.DisplayName = patientDisplayName;
                        tempCommNotes.Add(c);
                    }
                });
                commNotes = tempCommNotes;
            }
            return commNotes;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var communicationNotes = new List<CommunicationNote>();
            var schedules = baseScheduleRepository.GetScheduleTasksByStatusDisciplineAndRange(Current.AgencyId, branchId, patientStatus, startDate, endDate, new int[] { }, new int[] { (int)DisciplineTasks.CommunicationNote });
            if (schedules.IsNotNullOrEmpty())
            {
                var communicationNoteIds = schedules.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                communicationNotes = baseNoteRepository.GetCommunicationNoteByIds(Current.AgencyId, communicationNoteIds);
                if (communicationNotes != null && communicationNotes.Count > 0)
                {
                    var userIds = schedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                    var users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
                    communicationNotes.ForEach(c =>
                    {
                        var evnt = schedules.FirstOrDefault(e => e.Id == c.Id);
                        if (evnt != null)
                        {
                            c.DisplayName = evnt.PatientName;
                            if (!evnt.UserId.IsEmpty())
                            {
                                var user = users.FirstOrDefault(u => u.Id == evnt.UserId);
                                if (user != null)
                                {
                                    c.UserDisplayName = user.DisplayName;
                                }
                            }
                        }
                    });
                }
            }
            return communicationNotes;
        }

        public CommunicationNoteViewData GetCommunicationNotesViewData(Guid optionalBranchId, bool isLocationNeededIfIdEmpty, Guid optionalPatientId, Guid? optionalEpisodeId, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var viewData = new CommunicationNoteViewData
            {
                PatientId = optionalPatientId,
                LocationId = optionalBranchId,
                IsSinglePatient = true,
                Service = this.Service
            };

            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.CommunicationNote, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Add, (int)PermissionActions.Export, (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Print });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.NewPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None);
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);

                viewData.IsUserCanEdit = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None).Has(this.Service);
                viewData.IsUserCanDelete = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None).Has(this.Service);
                viewData.IsUserCanPrint = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None).Has(this.Service);
            }
            if (!optionalPatientId.IsEmpty())
            {
                viewData.IsSinglePatient = true;
                viewData.Notes = GetCommunicationNotes(optionalPatientId, ref optionalEpisodeId, false);
                viewData.EpisodeId = optionalEpisodeId.HasValue ? optionalEpisodeId.Value : Guid.Empty;
                var patient = patientRepository.GetPatientNameAndServicesById(optionalPatientId, Current.AgencyId);
                if (patient != null)
                {
                    viewData.AvailableService = viewData.AvailableService & patient.Services;
                    viewData.DisplayName = patient.DisplayName;
                }
            }
            else
            {
                if (optionalBranchId.IsEmpty())
                {
                    if (isLocationNeededIfIdEmpty)
                    {
                        optionalBranchId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                    }
                }
                viewData.IsSinglePatient = false;
                viewData.Notes = GetCommunicationNotes(optionalBranchId, patientStatus, startDate, endDate);
            }
            return viewData;
        }

        public Message CreateCommunicationNoteMessage(CommunicationNote communicationNote)
        {
            Message message = null;
            if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature || communicationNote.Status == (int)ScheduleStatus.NoteCompleted)
            {
                if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
                {
                    message = new Message
                    {
                        Type = MessageType.User,
                        AgencyId = Current.AgencyId,
                        Body = communicationNote.Text,
                        Subject = "Communication Note Message",
                        PatientId = communicationNote.PatientId,
                        Recipients = communicationNote.RecipientArray
                    };
                }
            }
            return message;
        }

        #endregion

        protected abstract CommunicationNote GetCommunicationNoteEpisodeData(CommunicationNote communicationNote);

        private JsonViewData AddCommunicationNote(CommunicationNote communicationNote, bool isSigned)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be created." };
            if (isSigned)
            {
                communicationNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                if (isQaByPass)
                {
                    communicationNote.Status = (int)ScheduleStatus.NoteCompleted;
                }
                else
                {
                    viewData.IsCaseManagementRefresh = true;
                }
                if (!communicationNote.PhysicianId.IsEmpty())
                {
                    var physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        communicationNote.PhysicianData = physician.ToXml();
                    }
                }
            }
            else
            {
                communicationNote.SignatureText = string.Empty;
            }
            communicationNote.Id = Guid.NewGuid();
            communicationNote.UserId = Current.UserId;
            communicationNote.AgencyId = Current.AgencyId;
            if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
            {
                communicationNote.Recipients = communicationNote.RecipientArray.ToXml();
            }
            communicationNote.Modified = DateTime.Now;
            var newTask = TaskHelperFactory<T>.CreateTask(communicationNote.Id,
                communicationNote.UserId,
                communicationNote.EpisodeId,
                communicationNote.PatientId,
                communicationNote.Status,
                Disciplines.ReportsAndNotes,
                communicationNote.Created,
                (int)DisciplineTasks.CommunicationNote);

            if (newTask != null && baseNoteRepository.AddCommunicationNote(communicationNote))
            {
                if (baseScheduleRepository.AddScheduleTask(newTask))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, Actions.Add, DisciplineTasks.CommunicationNote);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Communication note has been successfully created.";
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsCommunicationNoteRefresh = viewData.IsDataCentersRefresh;
                    viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && (communicationNote.Status == (int)ScheduleStatus.NoteMissedVisitPending);
                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, newTask);
                    viewData.PatientId = newTask.PatientId;
                    viewData.UserIds = new List<Guid> { newTask.UserId };
                    viewData.EpisodeId = communicationNote.EpisodeId;
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                }
                else
                {
                    baseNoteRepository.RemoveModel<CommunicationNote>(communicationNote.Id);
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "A problem occured while saving the communication note.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "A problem occured while saving the communication note.";
            }

            return viewData;
        }

        private List<CommunicationNote> GetCommunicationNoteFromTasks(List<T> scheduleEvents)
        {
            var communicationNotes = new List<CommunicationNote>();
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var communicationNote = new CommunicationNote
                    {
                        Id = scheduleEvent.Id,
                        PatientId = scheduleEvent.PatientId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        AgencyId = Current.AgencyId,
                        UserId = scheduleEvent.UserId,
                        Status = scheduleEvent.Status,
                        Created = scheduleEvent.EventDate,
                        Modified = DateTime.Now

                    };
                    communicationNotes.Add(communicationNote);

                });
            }
            return communicationNotes;
        }

        private JsonViewData UpdateCommunicationNote(CommunicationNote communicationNote, bool isSigned)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved." };
            if (isSigned)
            {
                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);

                communicationNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                if (isQaByPass)
                {
                    communicationNote.Status = (int)ScheduleStatus.NoteCompleted;
                }
                else
                {
                    viewData.IsCaseManagementRefresh = true;
                }
                if (!communicationNote.PhysicianId.IsEmpty())
                {
                    var physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                    if (physician != null) communicationNote.PhysicianData = physician.ToXml();
                }
            }
            else
            {
                communicationNote.SignatureText = string.Empty;
            }
            communicationNote.AgencyId = Current.AgencyId;
            if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
            {
                communicationNote.Recipients = communicationNote.RecipientArray.ToXml();
            }
            var editTask = baseScheduleRepository.GetScheduleTask(Current.AgencyId, communicationNote.PatientId, communicationNote.Id);
            if (editTask != null)
            {
                var oldTaskValues = editTask.DeepClone();
                editTask = TaskHelperFactory<T>.SetNoteProperties(editTask, communicationNote.Created, communicationNote.Status);
                editTask.Discipline = Disciplines.ReportsAndNotes.ToString();
                // editTask.EpisodeId = communicationNote.EpisodeId;
                if (baseScheduleRepository.UpdateScheduleTask(editTask))
                {
                    if (baseNoteRepository.EditCommunicationNote(communicationNote))
                    {
                        var isStatusChanged = editTask.Status != oldTaskValues.Status;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Communication note has been successfully saved.";
                        viewData.IsDataCentersRefresh = isStatusChanged;
                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(isStatusChanged, editTask);
                        viewData.IsCommunicationNoteRefresh = viewData.IsDataCentersRefresh;
                        viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature);
                        viewData.PatientId = communicationNote.PatientId;
                        viewData.UserIds = new List<Guid> { communicationNote.UserId };
                        viewData.EpisodeId = communicationNote.EpisodeId;
                        if (Enum.IsDefined(typeof(ScheduleStatus), editTask.Status))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, communicationNote.EpisodeId, editTask.PatientId, editTask.Id, Actions.Add, (ScheduleStatus)editTask.Status, DisciplineTasks.CommunicationNote, string.Empty);
                        }
                    }
                    else
                    {
                        baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "A problem occured while saving the communication note.";
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "A problem occured while saving the communication note.";
            }
            return viewData;
        }
    }
}
