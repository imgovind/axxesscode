﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Enums;
    using Axxess.LookUp.Repositories;

    using AAE = Axxess.AgencyManagement.Entities;

    public abstract class PatientProfileService
    {
        #region Private Members

        protected IAgencyRepository agencyRepository;
        protected IDrugService drugService;
        protected ILookupRepository lookupRepository;
        private readonly PatientProfileAbstract basePatientProfileRepository;
        protected PatientAdmissionAbstract patientAdmissionRepository;
        protected IPatientRepository patientRepository;
        protected IPhysicianRepository physicianRepository;
        protected IReferralRepository referralRepository;
        protected AgencyServices Service { get; set; }

        #endregion

        #region Constructor

        public PatientProfileService(PatientProfileAbstract basePatientProfileRepository)
        {
            this.basePatientProfileRepository = basePatientProfileRepository;
        }

        #endregion

        //#region Patient

        //public JsonViewData AddNewPatient(Patient patient,List<Profile> profiles)
        //{
        //    var rules = new List<Validation>();
        //    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be saved" };
        //    if (patient != null)
        //    {
        //        if (patient.PatientIdNumber.IsNotNullOrEmpty())
        //        {
        //            bool patientIdCheck = patientRepository.IsPatientIdExist(Current.AgencyId, patient.PatientIdNumber);
        //            rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
        //        }
        //        if (patient.Services.Contains((int)AgencyServices.HomeHealth))
        //        {
        //            var homeHealthProfile = profiles[(int)AgencyServices.HomeHealth - 1];
        //            if (homeHealthProfile != null)
        //            {
        //                if (patient.MedicareNumber.IsNotNullOrEmpty())
        //                {
        //                    bool medicareNumberCheck = patientRepository.IsMedicareExist(Current.AgencyId, patient.MedicareNumber.Trim());
        //                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
        //                }
        //                if (patient.MedicaidNumber.IsNotNullOrEmpty())
        //                {
        //                    bool medicaidNumberCheck = patientRepository.IsMedicaidExist(Current.AgencyId, patient.MedicaidNumber.Trim());
        //                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
        //                }
        //                if (homeHealthProfile.PrimaryInsurance >= 1000)
        //                {
        //                    rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
        //                }
        //                if (homeHealthProfile.SecondaryInsurance >= 1000)
        //                {
        //                    rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
        //                }
        //                if (homeHealthProfile.TertiaryInsurance >= 1000)
        //                {
        //                    rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
        //                }
        //            }
        //        }
        //        if (patient.Services.Contains((int)AgencyServices.PrivateDuty))
        //        {
        //             var privateDutyProfile = profiles[(int)AgencyServices.PrivateDuty - 1];
        //             if (privateDutyProfile != null)
        //             {
        //                 if (privateDutyProfile.PrimaryInsurance >= 1000)
        //                 {
        //                     rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
        //                 }
        //                 if (privateDutyProfile.SecondaryInsurance >= 1000)
        //                 {
        //                     rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
        //                 }
        //                 if (privateDutyProfile.TertiaryInsurance >= 1000)
        //                 {
        //                     rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
        //                 }
        //             }
        //        }


        //        var entityValidator = new EntityValidator(rules.ToArray());
        //        entityValidator.Validate();
        //        if (patient.IsValid() && entityValidator.IsValid)
        //        {
        //            patient.AgencyId = Current.AgencyId;
        //            patient.Id = Guid.NewGuid();
        //            patient.Encode(); // setting string arrays to one field

        //            if (patient.Status == (int)PatientStatus.Active)
        //            {
        //                var workflow = new CreatePatientWorkflow(patient);
        //                if (workflow.IsCommitted)
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "Patient was created successfully.";
        //                    viewData.IsCenterRefresh = true;
        //                    viewData.IsPendingPatientListRefresh = true;
        //                    viewData.IsPatientListRefresh = true;
        //                    viewData.PatientStatus = (int)PatientStatus.Active;
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = workflow.Message;
        //                }
        //            }
        //            else
        //            {
        //                var workflow = new PendingPatientWorkFlow(patient);
        //                if (workflow.IsCommitted)
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "Patient was created successfully.";
        //                    viewData.IsCenterRefresh = true;
        //                    viewData.IsPendingPatientListRefresh = true;
        //                    viewData.IsPatientListRefresh = true;
        //                    viewData.PatientStatus = (int)PatientStatus.Active;
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                }
        //            }
        //            if (viewData.isSuccessful && !patient.ReferralId.IsEmpty())
        //            {
        //                if (referralRepository.SetStatus(Current.AgencyId, patient.ReferralId, ReferralStatus.Admitted))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, patient.ReferralId.ToString(), LogType.Referral, LogAction.ReferralAdmitted, string.Empty);
        //                    viewData.IsReferralListRefresh = true;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = patient.ValidationMessage + "\n" + entityValidator.Message;
        //        }
        //    }
        //    return viewData;
        //}

        //public bool AddPatient(Patient patient)
        //{
        //    var result = false;
        //    Patient patientOut = null;
        //    var admissionDateId = Guid.NewGuid();
        //    var service=0;
        //    patient.Services.ForEach(s =>
        //    {
        //        service |= s;
        //    });
        //    patient.Service = service;
        //    patient.AdmissionId = admissionDateId;
        //    if (patientRepository.Add(patient, out patientOut))
        //    {
        //        if (patientOut != null &&
        //            AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(
        //            new PatientAdmissionDate {
        //                Id = admissionDateId,
        //                AgencyId = Current.AgencyId,
        //                PatientId = patient.Id,
        //                PatientData = patientOut != null ? patientOut.ToXml() : string.Empty, 
        //                StartOfCareDate = patient.StartofCareDate,
        //                IsActive = true,
        //                IsDeprecated = false,
        //                Status = patientOut.Status }))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, (patient.ShouldCreateEpisode && !patient.UserId.IsEmpty()) && patient.Status == (int)PatientStatus.Active ? string.Empty : "Pending for admission");
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        //public JsonViewData EditPatient(Patient patient)
        //{
        //    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be edited" };
        //    if (patient != null && !patient.Id.IsEmpty())
        //    {
        //        var rules = new List<Validation>();

        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
        //        rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
        //        rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
        //        rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
        //        rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
        //        rules.Add(new Validation(() => patient.Triage <= 0, "Emergency Triage is required."));
        //        if (patient.Status == (int)PatientStatus.Discharged)
        //        {
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
        //            rules.Add(new Validation(() => !patient.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
        //        }
        //        if (patient.PatientIdNumber.IsNotNullOrEmpty())
        //        {
        //            bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
        //            rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
        //        }
        //        if (patient.MedicareNumber.IsNotNullOrEmpty())
        //        {
        //            bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
        //            rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
        //        }
        //        if (patient.MedicaidNumber.IsNotNullOrEmpty())
        //        {
        //            bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
        //            rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
        //        }
        //        if (patient.SSN.IsNotNullOrEmpty())
        //        {
        //            bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
        //            rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
        //        }
        //        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
        //        {
        //            rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
        //        }
        //        if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
        //        {
        //            rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
        //        }

        //        if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
        //        {
        //            rules.Add(new Validation(() => patient.TertiaryInsurance.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
        //        }

        //        var entityValidator = new EntityValidator(rules.ToArray());
        //        entityValidator.Validate();
        //        if (entityValidator.IsValid)
        //        {

        //            patient.AgencyId = Current.AgencyId;
        //            patient.Encode();// setting string arrays to one field
        //            if (this.EditPatientSave(patient))
        //            {
        //                viewData.isSuccessful = true;
        //                viewData.errorMessage = "Your Data successfully edited";
        //            }
        //            else
        //            {
        //                viewData.isSuccessful = false;
        //                viewData.errorMessage = "Error in editing the data.";
        //            }
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = entityValidator.Message;
        //        }
        //    }
        //    return viewData;
        //}

        //public bool EditPatientSave(Patient patient)
        //{
        //    var result = false;
        //    Patient patientOut = null;
        //    var admissionDateId = Guid.NewGuid();
        //    var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
        //    if (patientToEdit != null)
        //    {
        //        var oldAdmissionDateId = patientToEdit.AdmissionId;
        //        patient.AdmissionId = patientToEdit.AdmissionId.IsEmpty() ? admissionDateId : patientToEdit.AdmissionId;
        //        if (patientRepository.Edit(patient, out patientOut))
        //        {
        //            if (oldAdmissionDateId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
        //                    result = true;
        //                }
        //                else
        //                {
        //                    patientToEdit.AdmissionId = oldAdmissionDateId;
        //                    patientRepository.Update(patientToEdit);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate(patientToEdit.Id, oldAdmissionDateId);
        //                if (admissionData != null && patientOut != null)
        //                {
        //                    admissionData.StartOfCareDate = patientOut.StartofCareDate;
        //                    if (patient.Status == (int)PatientStatus.Discharged)
        //                    {
        //                        admissionData.DischargedDate = patientOut.DischargeDate;
        //                    }
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
        //                        result = true;
        //                    }
        //                    else
        //                    {
        //                        patientToEdit.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patientToEdit);
        //                    }
        //                }
        //                else
        //                {
        //                    if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
        //                        result = true;
        //                    }
        //                    else
        //                    {
        //                        patientToEdit.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patientToEdit);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public JsonViewData DeletePatient(Guid Id, bool isDeprecated)
        //{
        //    var data = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(Id, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        patient.IsDeprecated = isDeprecated;
        //        if (patientRepository.Update(patient))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, Id, Id.ToString(), LogType.Patient, isDeprecated ? LogAction.PatientDeleted : LogAction.PatientRestored, string.Empty);
        //            data.isSuccessful = true;
        //            data.PatientId = patient.Id;
        //            data.IsDeletedPatientListRefresh = true;
        //            data.IsCenterRefresh = patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged;
        //            data.IsPatientListRefresh = true;
        //            data.PatientStatus = patient.Status;
        //            data.IsNonAdmitPatientListRefresh = patient.Status == (int)PatientStatus.NonAdmission;
        //            data.IsPendingPatientListRefresh = patient.Status == (int)PatientStatus.Pending;
        //        }
        //    }
        //    return data;
        //}

        //public bool AdmitPatient(PendingPatient pending, out Patient patientOut)
        //{
        //    return patientRepository.AdmitPatient(pending, out patientOut);
        //}


        //public JsonViewData NonAdmitPatient(PendingPatient pending)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    Patient patientOut = null;
        //    var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var admissionDateId = Guid.NewGuid();
        //        pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.NonAdmitPatient(pending, out patientOut))
        //        {
        //            if (patient.AdmissionId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patient.Id, patient.AdmissionId);
        //                if (admissionData != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patientOut.Status;
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {

        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
        //                        viewData.isSuccessful = true;

        //                    }
        //                    else
        //                    {
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = false;
        //                viewData.IsNonAdmitPatientListRefresh = (int)PatientStatus.NonAdmission != patient.Status;
        //                viewData.IsPatientListRefresh = viewData.IsNonAdmitPatientListRefresh;
        //                viewData.PatientStatus = (int)PatientStatus.NonAdmission;
        //                viewData.IsPendingPatientListRefresh = false;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public bool Update(Patient patient)
        //{
        //    return patientRepository.Update(patient);
        //}

        //public JsonViewData DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var admissionDateId = Guid.NewGuid();
        //        var oldStatus = patient.Status;
        //        var oldDischargeDate = patient.DischargeDate;
        //        var oldDischargeReason = patient.DischargeReason;

        //        patient.DischargeDate = dischargeDate;
        //        patient.Status = (int)PatientStatus.Discharged;
        //        patient.DischargeReasonId = dischargeReasonId;
        //        patient.DischargeReason = dischargeReason;
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        Patient patientOut = null;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (oldAdmissionDateId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, DischargeReasonId = dischargeReasonId, IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patient.DischargeDate = oldDischargeDate;
        //                    patient.Status = oldStatus;
        //                    patient.DischargeReason = oldDischargeReason;
        //                    patient.AdmissionId = oldAdmissionDateId;
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patient.Id, patient.AdmissionId);
        //                if (admissionData != null && patientOut != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patient.Status;
        //                    admissionData.StartOfCareDate = patient.StartofCareDate;
        //                    admissionData.DischargedDate = patient.DischargeDate;
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                        viewData.isSuccessful = true;

        //                    }
        //                    else
        //                    {
        //                        patient.DischargeDate = oldDischargeDate;
        //                        patient.Status = oldStatus;
        //                        patient.DischargeReason = oldDischargeReason;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, DischargeReasonId = dischargeReasonId, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        //                        viewData.isSuccessful = true;

        //                    }
        //                    else
        //                    {
        //                        patient.DischargeDate = oldDischargeDate;
        //                        patient.Status = oldStatus;
        //                        patient.DischargeReason = oldDischargeReason;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }

        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Discharged;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public JsonViewData ActivatePatient(Guid patientId)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    Patient patientOut = null;
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var oldStatus = patient.Status;
        //        patient.Status = (int)PatientStatus.Active;
        //        var admissionDateId = Guid.NewGuid();
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (patientOut != null)
        //            {
        //                if (oldAdmissionDateId.IsEmpty())
        //                {
        //                    if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
        //                    {
        //                        viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                        viewData.isSuccessful = true;
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);

        //                    }
        //                    else
        //                    {
        //                        patient.Status = oldStatus;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patient.Id, patient.AdmissionId);
        //                    if (admissionData != null)
        //                    {
        //                        admissionData.PatientData = patientOut.ToXml();
        //                        admissionData.Status = patient.Status;
        //                        admissionData.StartOfCareDate = patient.StartofCareDate;
        //                        if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                        {
        //                            viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                            viewData.isSuccessful = true;
        //                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
        //                        }
        //                        else
        //                        {
        //                            patient.Status = oldStatus;
        //                            patient.AdmissionId = oldAdmissionDateId;
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
        //                        {
        //                            viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                            viewData.isSuccessful = true;
        //                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
        //                        }
        //                        else
        //                        {
        //                            patient.Status = oldStatus;
        //                            patient.AdmissionId = oldAdmissionDateId;
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                patient.Status = oldStatus;
        //                patient.AdmissionId = oldAdmissionDateId;
        //                patientRepository.Update(patient);
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Active;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public JsonViewData ActivatePatient(Guid patientId, DateTime startOfCareDate)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    Patient patientOut = null;
        //    if (patient != null)
        //    {
        //        var oldStatus = patient.Status;
        //        var oldStartOfCareDate = patient.StartofCareDate;
        //        patient.Status = (int)PatientStatus.Active;
        //        patient.StartofCareDate = startOfCareDate;
        //        var admissionDateId = Guid.NewGuid();
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = admissionDateId;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
        //            {
        //                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
        //                viewData.isSuccessful = true;
        //            }
        //            else
        //            {
        //                patient.Status = oldStatus;
        //                patient.StartofCareDate = oldStartOfCareDate;
        //                patient.AdmissionId = oldAdmissionDateId;
        //                patientRepository.Update(patient);
        //            }

        //            //else
        //            //{
        //            //    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
        //            //    if (admissionData != null && patientOut != null)
        //            //    {
        //            //        admissionData.PatientData = patientOut.ToXml();
        //            //        admissionData.Status = patient.Status;
        //            //        admissionData.StartOfCareDate = patient.StartofCareDate;
        //            //        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
        //            //        {
        //            //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
        //            //            result = true;
        //            //        }
        //            //        else
        //            //        {
        //            //            patient.Status = oldStatus;
        //            //            patient.StartofCareDate = oldStartOfCareDate;
        //            //            patient.AdmissionId = oldAdmissionDateId;
        //            //            patientRepository.Update(patient);
        //            //        }
        //            //    }
        //            //    else
        //            //    {
        //            //        patient.Status = oldStatus;
        //            //        patient.StartofCareDate = oldStartOfCareDate;
        //            //        patient.AdmissionId = oldAdmissionDateId;
        //            //        patientRepository.Update(patient);
        //            //    }
        //            //}
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        //                viewData.PatientStatus = (int)PatientStatus.Active;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public bool RemovePatient(Guid id)
        //{
        //    return patientRepository.RemovePatient(Current.AgencyId, id);
        //}

        //public bool SetStatus(Guid patientId, PatientStatus status)
        //{
        //    return patientRepository.SetStatus(Current.AgencyId, patientId, status);
        //}

        //public JsonViewData SetPatientPending(Guid patientId)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        Patient patientOut = null;
        //        var oldStatus = patient.Status;
        //        patient.Status = (int)PatientStatus.Pending;
        //        var admissionDateId = Guid.NewGuid();
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.Update(patient, out patientOut))
        //        {
        //            if (oldAdmissionDateId.IsEmpty())
        //            {
        //                if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Set Pending.", IsDeprecated = false, IsActive = true }))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
        //                    viewData.isSuccessful = true;
        //                }
        //                else
        //                {
        //                    patient.Status = oldStatus;
        //                    patient.AdmissionId = oldAdmissionDateId;
        //                    patientRepository.Update(patient);
        //                }
        //            }
        //            else
        //            {
        //                var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate(patient.Id, patient.AdmissionId);
        //                if (admissionData != null && patientOut != null)
        //                {
        //                    admissionData.PatientData = patientOut.ToXml();
        //                    admissionData.Status = patient.Status;
        //                    if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patient.Status = oldStatus;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
        //                        viewData.isSuccessful = true;
        //                    }
        //                    else
        //                    {
        //                        patient.Status = oldStatus;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //            }
        //            if (viewData.isSuccessful)
        //            {
        //                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        //                viewData.IsPendingPatientListRefresh = (int)PatientStatus.Pending != oldStatus;
        //                viewData.IsPatientListRefresh = viewData.IsPendingPatientListRefresh;
        //                viewData.PatientStatus = (int)PatientStatus.Pending;
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //public Patient GetPatientOnly(Guid patientId)
        //{
        //    return patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //}

        //public PatientProfile GetProfileWithOutEpisodeInfo(Guid patientId)
        //{
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var patientProfile = new PatientProfile();
        //        patientProfile.Patient = patient;
        //        patientProfile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

        //        patientProfile.Allergies = GetAllergiesText(patientId);
        //        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
        //        if (physician != null)
        //        {
        //            patientProfile.Physician = physician;
        //        }
        //        patient.EmergencyContacts = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId);
        //        if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
        //        {
        //            patientProfile.EmergencyContact = patient.EmergencyContacts.OrderByDescending(e => e.IsPrimary).FirstOrDefault();
        //        }
        //        SetInsurance(patient);

        //        if (!patient.CaseManagerId.IsEmpty())
        //        {
        //            patient.CaseManagerName = UserEngine.GetName(patient.CaseManagerId, Current.AgencyId);
        //        }
        //        if (!patient.UserId.IsEmpty())
        //        {
        //            patientProfile.Clinician = UserEngine.GetName(patient.UserId, Current.AgencyId);
        //        }


        //        return patientProfile;
        //    }

        //    return null;
        //}

        //public Patient GetPatientSnapshotInfo(Guid patientId, bool isPrimaryInsuranceSet, bool isEmergencyContactSet, bool isInsuranceSet)
        //{
        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        SetPatientSnapshotInfo(patient,true,true, isInsuranceSet);
        //    }
        //    return patient;
        //}

        //public void SetPatientSnapshotInfo(Patient patient, bool isPrimaryInsuranceSet, bool isEmergencyContactSet, bool isInsuranceSet)
        //{
        //    if (patient != null)
        //    {
        //        if (isPrimaryInsuranceSet)
        //        {
        //            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
        //            if (physician != null)
        //            {
        //                patient.Physician = physician;
        //            }
        //        }
        //        if (isEmergencyContactSet)
        //        {
        //            var emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patient.Id);
        //            if (emergencyContact != null)
        //            {
        //                patient.EmergencyContact = emergencyContact;
        //            }
        //        }
        //        if (isInsuranceSet)
        //        {
        //            this.SetInsurance(patient);
        //        }
        //    }
        //}


        //public PendingPatient GetPendingPatient(Guid id, NonAdmitTypes type)
        //{
        //    PendingPatient pending = null;
        //    if (type == NonAdmitTypes.Patient)
        //    {
        //        var patient = patientRepository.GetPatientOnly(id, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            pending = new PendingPatient
        //            {
        //                Id = patient.Id,
        //                DisplayName = patient.DisplayName,
        //                PatientIdNumber = patient.PatientIdNumber,
        //                StartofCareDate = patient.StartofCareDate,
        //                ReferralDate = patient.ReferralDate,
        //                CaseManagerId = patient.CaseManagerId,
        //                PrimaryInsurance = patient.PrimaryInsurance,
        //                SecondaryInsurance = patient.SecondaryInsurance,
        //                TertiaryInsurance = patient.TertiaryInsurance,
        //                Type = NonAdmitTypes.Patient,
        //                Payer = patient.Payer,
        //                UserId = patient.UserId,
        //                PrimaryPhysician = GetPrimaryPhysicianId(patient.Id, Current.AgencyId)
        //            };
        //        }
        //    }
        //    else
        //    {
        //        var referral = referralRepository.Get(Current.AgencyId, id);
        //        if (referral != null)
        //        {
        //            var physicians = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
        //            Physician primaryPhysician = physicians.FirstOrDefault(p => p.IsPrimary);
        //            pending = new PendingPatient
        //            {
        //                Id = referral.Id,
        //                DisplayName = referral.DisplayName,
        //                PatientIdNumber = string.Empty,
        //                StartofCareDate = DateTime.Today,
        //                ReferralDate = referral.ReferralDate,
        //                PrimaryInsurance = string.Empty,
        //                SecondaryInsurance = string.Empty,
        //                TertiaryInsurance = string.Empty,
        //                Type = NonAdmitTypes.Referral,
        //                UserId = referral.UserId,
        //                PrimaryPhysician = primaryPhysician != null ? primaryPhysician.Id : Guid.Empty
        //            };
        //        }
        //    }
        //    return pending;
        //}

        //public List<PatientData> GetPatients(Guid agencyId, Guid branchId, int serviceId, int status)
        //{
        //    var patients = patientRepository.All(agencyId, branchId,serviceId, status);
        //    patients.ForEach((PatientData patient) =>
        //    {
        //        patient.InsuranceName = AgencyInformationHelper.GetInsurance(patient.InsuranceId);
        //        if (patient.InsuranceName.IsNotNullOrEmpty())
        //        {
        //            patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
        //        }
        //    });
        //    return patients.ToList();
        //}

        //public List<PatientData> GetDeletedPatients(Guid branchId, int serviceId, bool IsInsuranceNameNeeded)
        //{
        //    var patients = patientRepository.AllDeleted(Current.AgencyId, branchId,serviceId);
        //    if (IsInsuranceNameNeeded)
        //    {
        //        patients.ForEach((PatientData patient) =>
        //        {
        //            patient.InsuranceName = AgencyInformationHelper.GetInsurance(patient.InsuranceId);
        //            if (patient.InsuranceName.IsNotNullOrEmpty())
        //            {
        //                patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
        //            }
        //        });
        //    }
        //    return patients.ToList();
        //}

        //public List<NonAdmit> GetNonAdmits()
        //{
        //    var list = new List<NonAdmit>();
        //    var patients = patientRepository.FindPatientOnly((int)PatientStatus.NonAdmission, Current.AgencyId);
        //    var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.NonAdmission).ToList();
        //    if (patients != null && patients.Count > 0)
        //    {
        //        patients.ForEach(p =>
        //        {
        //            InsuranceCache cache = null;
        //            if (p.PrimaryInsurance.IsNotNullOrEmpty() && p.PrimaryInsurance.IsInteger())
        //            {
        //                cache = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
        //            }
        //            list.Add(new NonAdmit
        //            {
        //                Id = p.Id,
        //                LastName = p.LastName,
        //                FirstName = p.FirstName,
        //                DateOfBirth = p.DOB,
        //                Phone = p.PhoneHomeFormatted,
        //                Type = NonAdmitTypes.Patient,
        //                MiddleInitial = p.MiddleInitial,
        //                PatientIdNumber = p.PatientIdNumber,
        //                NonAdmissionReason = p.NonAdmissionReason,
        //                NonAdmitDate = p.NonAdmissionDate,
        //                Gender = p.Gender,
        //                AddressCity = p.AddressCity,
        //                AddressLine1 = p.AddressLine1,
        //                AddressLine2 = p.AddressLine2,
        //                AddressStateCode = p.AddressStateCode,
        //                AddressZipCode = p.AddressZipCode,
        //                MedicareNumber = p.MedicaidNumber,
        //                InsuranceName = cache != null ? cache.Name : string.Empty,
        //                InsuranceNumber = p.PrimaryHealthPlanId,
        //                Comments = p.Comments
        //            });
        //        });
        //    }

        //    if (referrals != null && referrals.Count > 0)
        //    {
        //        referrals.ForEach(r =>
        //        {
        //            list.Add(new NonAdmit
        //            {
        //                Id = r.Id,
        //                LastName = r.LastName,
        //                FirstName = r.FirstName,
        //                DateOfBirth = r.DOB,
        //                Phone = r.PhoneHomeFormatted,
        //                Type = NonAdmitTypes.Referral,
        //                PatientIdNumber = string.Empty,
        //                NonAdmissionReason = r.NonAdmissionReason,
        //                NonAdmitDate = r.NonAdmissionDate,
        //                Comments = r.Comments
        //            });
        //        });
        //    }

        //    return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        //}

        //public List<PendingPatient> GetPendingPatients(Guid branchId, int serviceId)
        //{
        //    var patients = patientRepository.GetPendingByAgencyId(Current.AgencyId, branchId, serviceId);
        //    if (patients != null && patients.Count > 0)
        //    {
        //        var locations = agencyRepository.AgencyLocations(Current.AgencyId, patients.Select(p => p.AgencyLocationId).Distinct().ToList())??new List<AgencyLocation>();
        //        patients.ForEach(p =>
        //        {
        //            if (p.PrimaryInsurance.IsNotNullOrEmpty())
        //            {
        //                var insurance = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
        //                if (insurance != null && insurance.Name.IsNotNullOrEmpty())
        //                {
        //                    p.PrimaryInsuranceName = insurance.Name;
        //                }
        //            }
        //            var location = locations.FirstOrDefault(l => l.Id == p.AgencyLocationId);
        //            if (location != null)
        //            {
        //                p.Branch = location.Name;
        //            }
        //        });
        //    }

        //    return patients;
        //}

        //public bool IsPatientExist(Guid patientId)
        //{
        //    return patientRepository.IsPatientExist(Current.AgencyId, patientId);
        //}

        //public string LastPatientId()
        //{
        //    return patientRepository.LastPatientId(Current.AgencyId);
        //}

        //public string GetPatientNameById(Guid patientId)
        //{
        //    return  patientRepository.GetPatientNameById(Current.AgencyId, patientId);
        //}

        //public void LinkPendingPatientToPhysicain(PendingPatient pending, Patient patient)
        //{
        //    if (!pending.PrimaryPhysician.IsEmpty())
        //    {
        //        var physicians = patient.PhysicianContacts.ToList();
        //        var physician = physicians.FirstOrDefault(p => p.Id == pending.PrimaryPhysician);
        //        if (physician != null)
        //        {
        //            if (!physician.Primary)
        //            {
        //                physicianRepository.SetPrimary(patient.Id, pending.PrimaryPhysician);
        //            }
        //        }
        //        else
        //        {
        //            physicianRepository.Link(patient.Id, pending.PrimaryPhysician, true);
        //        }
        //    }
        //}

        //public JsonViewData AdmitPending(Profile profile)
        //{
        //    var data = new JsonViewData { isSuccessful = false };
        //    var patient = patientRepository.GetPatientOnly(profile.Id, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        Patient patientOut = null;
        //        var admissionDateId = Guid.NewGuid();

        //        profile.IsFaceToFaceEncounterCreated = profile.IsFaceToFaceEncounterCreated;
        //        var oldAdmissionDateId = patient.AdmissionId;
        //        profile.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //        if (patientRepository.AdmitPatient(profile, out patientOut))
        //        {
        //            if (patientOut != null)
        //            {
        //                LinkPendingPatientToPhysicain(profile, patient);

        //                if (oldAdmissionDateId.IsEmpty())
        //                {
        //                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = profile.Id, StartOfCareDate = profile.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
        //                    {
        //                        var medId = Guid.NewGuid();
        //                        patient.EpisodeStartDate = profile.EpisodeStartDate;
        //                        if (this.CreateMedicationProfile(patient, medId))
        //                        {
        //                            if (this.CreateEpisodeAndClaims(patient))
        //                            {
        //                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
        //                                data.isSuccessful = true;
        //                            }
        //                            else
        //                            {
        //                                patientRepository.Update(patient);
        //                                patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, profile.AdmissionId);
        //                                patientRepository.RemoveMedicationProfile(Current.AgencyId, patient.Id, medId);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, profile.AdmissionId);
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
        //                    if (admissionData != null)
        //                    {
        //                        var oldPatientData = admissionData.PatientData;
        //                        var oldSoc = admissionData.StartOfCareDate;
        //                        var oldAdmissionStatus = admissionData.Status;

        //                        admissionData.PatientData = patientOut.ToXml();
        //                        admissionData.StartOfCareDate = profile.StartofCareDate;
        //                        admissionData.Status = (int)PatientStatus.Active;
        //                        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
        //                        {
        //                            var medId = Guid.NewGuid();
        //                            patient.EpisodeStartDate = profile.EpisodeStartDate;
        //                            if (this.CreateMedicationProfile(patient, medId))
        //                            {
        //                                if (this.CreateEpisodeAndClaims(patient))
        //                                {
        //                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
        //                                    data.isSuccessful = true;
        //                                }
        //                                else
        //                                {
        //                                    patientRepository.Update(patient);
        //                                    admissionData.Status = oldAdmissionStatus;
        //                                    admissionData.PatientData = oldPatientData;
        //                                    admissionData.StartOfCareDate = oldSoc;
        //                                    patientRepository.UpdatePatientAdmissionDate(admissionData);
        //                                    patientRepository.RemoveMedicationProfile(Current.AgencyId, patient.Id, medId);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                admissionData.Status = oldAdmissionStatus;
        //                                admissionData.PatientData = oldPatientData;
        //                                admissionData.StartOfCareDate = oldSoc;
        //                                patientRepository.UpdatePatientAdmissionDate(admissionData);
        //                                patientRepository.Update(patient);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = profile.Id, StartOfCareDate = profile.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
        //                        {
        //                            var medId = Guid.NewGuid();
        //                            patient.EpisodeStartDate = profile.EpisodeStartDate;
        //                            if (this.CreateMedicationProfile(patient, medId))
        //                            {
        //                                if (this.CreateEpisodeAndClaims(patient))
        //                                {
        //                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
        //                                    data.isSuccessful = true;
        //                                }
        //                                else
        //                                {
        //                                    patientRepository.Update(patient);
        //                                    patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, profile.AdmissionId);
        //                                    patientRepository.RemoveMedicationProfile(Current.AgencyId, patient.Id, medId);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                patientRepository.RemovePatientAdmissionDate(Current.AgencyId, patient.Id, profile.AdmissionId);
        //                                patientRepository.Update(patient);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                patientRepository.Update(patient);
        //            }
        //        }
        //        if (data.isSuccessful)
        //        {
        //            data.IsCenterRefresh = true;

        //            data.IsPatientListRefresh = true;
        //            data.PatientStatus = (int)PatientStatus.Active;
        //            data.IsReferralListRefresh = false;
        //            data.IsNonAdmitPatientListRefresh = patient.Status == (int)PatientStatus.NonAdmission;
        //            data.IsPendingPatientListRefresh = patient.Status == (int)PatientStatus.Pending;
        //        }
        //    }

        //    return data;
        //}


        ////public JsonViewData AdmitReferral(PendingPatient pending)
        ////{
        ////    var data = new JsonViewData { isSuccessful = false };
        ////    var referral = referralRepository.Get(Current.AgencyId, pending.Id);
        ////    if (referral != null)
        ////    {
        ////        Patient patientOut = null;
        ////        var admissionDateId = Guid.NewGuid();

        ////        pending.AdmissionId = admissionDateId;
        ////        if (patientRepository.AdmitReferral(pending, out patientOut))
        ////        {
        ////            if (patientOut != null)
        ////            {
        ////                LinkReferralPhysician(pending, referral);
        ////                if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Referral Admitted.", IsDeprecated = false, IsActive = true }))
        ////                {
        ////                    var medId = Guid.NewGuid();

        ////                    if (this.CreateMedicationProfile(patientOut, medId))
        ////                    {
        ////                        patientOut.EpisodeStartDate = pending.EpisodeStartDate;
        ////                        patientOut.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
        ////                        if (this.CreateEpisodeAndClaims(patientOut))
        ////                        {
        ////                            Auditor.AddGeneralLog(LogDomain.Patient, referral.Id, referral.Id.ToString(), LogType.Patient, LogAction.ReferralAdmitted, string.Empty);
        ////                            data.isSuccessful = true;
        ////                        }
        ////                        else
        ////                        {

        ////                            referralRepository.UpdateModal(referral);
        ////                            patientRepository.RemovePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
        ////                            patientRepository.RemoveMedicationProfile(Current.AgencyId, referral.Id, medId);
        ////                        }
        ////                    }
        ////                    else
        ////                    {
        ////                        patientRepository.RemovePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
        ////                        referralRepository.UpdateModal(referral);
        ////                    }
        ////                }
        ////                else
        ////                {
        ////                    referralRepository.UpdateModal(referral);
        ////                }
        ////            }
        ////            else
        ////            {
        ////                referralRepository.UpdateModal(referral);
        ////            }
        ////        }
        ////        if (data.isSuccessful)
        ////        {
        ////            data.IsCenterRefresh = true;
        ////            data.IsNonAdmitPatientListRefresh = false;
        ////            data.IsPatientListRefresh = true;
        ////            data.PatientStatus = (int)PatientStatus.Active;
        ////            data.IsReferralListRefresh = true;
        ////        }
        ////    }
        ////    return data;
        ////}


        ////public JsonViewData DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReason)
        ////{
        ////    var viewData = new JsonViewData { isSuccessful = false };
        ////    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        ////    if (patient != null)
        ////    {
        ////        var admissionDateId = Guid.NewGuid();
        ////        var oldStatus = patient.Status;
        ////        var oldDischargeDate = patient.DischargeDate;
        ////        var oldDischargeReason = patient.DischargeReason;
        ////        patient.DischargeDate = dischargeDate;
        ////        patient.Status = (int)PatientStatus.Discharged;
        ////        //patient.DischargeReason = dischargeReason;
        ////        patient.DischargeReason = dischargeReason;
        ////        var oldAdmissionDateId = patient.AdmissionId;
        ////        patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        ////        Patient patientOut = null;
        ////        if (patientRepository.Update(patient, out patientOut))
        ////        {
        ////            if (oldAdmissionDateId.IsEmpty())
        ////            {
        ////                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
        ////                {

        ////                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        ////                    viewData.isSuccessful = true;
        ////                }
        ////                else
        ////                {
        ////                    patient.DischargeDate = oldDischargeDate;
        ////                    patient.Status = oldStatus;
        ////                    patient.DischargeReason = oldDischargeReason;
        ////                    patient.AdmissionId = oldAdmissionDateId;
        ////                    patientRepository.Update(patient);
        ////                }
        ////            }
        ////            else
        ////            {
        ////                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
        ////                if (admissionData != null && patientOut != null)
        ////                {
        ////                    admissionData.PatientData = patientOut.ToXml();
        ////                    admissionData.Status = patient.Status;
        ////                    admissionData.StartOfCareDate = patient.StartofCareDate;
        ////                    admissionData.DischargedDate = patient.DischargeDate;
        ////                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
        ////                    {

        ////                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        ////                        viewData.isSuccessful = true;
        ////                    }
        ////                    else
        ////                    {
        ////                        patient.DischargeDate = oldDischargeDate;
        ////                        patient.Status = oldStatus;
        ////                        patient.DischargeReason = oldDischargeReason;
        ////                        patient.AdmissionId = oldAdmissionDateId;
        ////                        patientRepository.Update(patient);
        ////                    }
        ////                }
        ////                else
        ////                {
        ////                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
        ////                    {
        ////                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
        ////                        viewData.isSuccessful = true;
        ////                    }
        ////                    else
        ////                    {
        ////                        patient.DischargeDate = oldDischargeDate;
        ////                        patient.Status = oldStatus;
        ////                        patient.DischargeReason = oldDischargeReason;
        ////                        patient.AdmissionId = oldAdmissionDateId;
        ////                        patientRepository.Update(patient);
        ////                    }
        ////                }
        ////            }
        ////            if (viewData.isSuccessful)
        ////            {
        ////                viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
        ////                viewData.IsCenterRefresh = oldStatus != patient.Status && (patient.Status == (int)PatientStatus.Active || patient.Status == (int)PatientStatus.Discharged);
        ////                viewData.IsPatientListRefresh = oldStatus != patient.Status;
        ////                viewData.PatientStatus = (int)PatientStatus.Discharged;
        ////            }
        ////        }

        ////    }
        ////    return viewData;
        ////}

        //#endregion

        //#region Insurance


        //public void SetInsurance(Patient patient)
        //{
        //    if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
        //    {
        //        if (patient.PrimaryInsurance.ToInteger() < 1000)
        //        {
        //            patient.PrimaryInsuranceName = Enum.IsDefined(typeof(MedicareIntermediary), patient.PrimaryInsurance.ToInteger()) ? ((MedicareIntermediary)patient.PrimaryInsurance.ToInteger()).GetDescription() : string.Empty;
        //        }
        //        else
        //        {
        //            var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
        //            if (standardInsurance != null)
        //            {
        //                patient.PrimaryInsuranceName = standardInsurance.Name;
        //            }
        //        }
        //    }
        //    if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger())
        //    {
        //        if (patient.SecondaryInsurance.ToInteger() < 1000)
        //        {
        //            patient.SecondaryInsuranceName = Enum.IsDefined(typeof(MedicareIntermediary), patient.SecondaryInsurance.ToInteger()) ? ((MedicareIntermediary)patient.SecondaryInsurance.ToInteger()).GetDescription() : string.Empty;

        //        }
        //        else
        //        {
        //            var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.SecondaryInsurance.ToInteger());
        //            if (standardInsurance != null)
        //            {
        //                patient.SecondaryInsuranceName = standardInsurance.Name;
        //            }
        //        }
        //    }
        //    if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger())
        //    {
        //        if (patient.TertiaryInsurance.ToInteger() < 1000)
        //        {
        //            patient.TertiaryInsuranceName = Enum.IsDefined(typeof(MedicareIntermediary), patient.TertiaryInsurance.ToInteger()) ? ((MedicareIntermediary)patient.TertiaryInsurance.ToInteger()).GetDescription() : string.Empty;
        //        }
        //        else
        //        {
        //            var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.TertiaryInsurance.ToInteger());
        //            if (standardInsurance != null)
        //            {
        //                patient.TertiaryInsuranceName = standardInsurance.Name;
        //            }
        //        }
        //    }
        //}


        //public PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType)
        //{
        //    var viewData = new PatientInsuranceInfoViewData();
        //    viewData.InsuranceType = insuranceType.ToTitleCase();
        //    if (!patientId.IsEmpty())
        //    {
        //        var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            if (insuranceType.IsEqual("Primary"))
        //            {
        //                if (patient.PrimaryInsurance == insuranceId)
        //                {
        //                    viewData.HealthPlanId = patient.PrimaryHealthPlanId;
        //                    viewData.GroupName = patient.PrimaryGroupName;
        //                    viewData.GroupId = patient.PrimaryGroupId;
        //                    viewData.Relationship = patient.PrimaryRelationship;
        //                }
        //                viewData.InsuranceType = "Primary";
        //            }
        //            else if (insuranceType.IsEqual("Secondary"))
        //            {
        //                if (patient.SecondaryInsurance == insuranceId)
        //                {
        //                    viewData.HealthPlanId = patient.SecondaryHealthPlanId;
        //                    viewData.GroupName = patient.SecondaryGroupName;
        //                    viewData.GroupId = patient.SecondaryGroupId;
        //                    viewData.Relationship = patient.SecondaryRelationship;
        //                }
        //                viewData.InsuranceType = "Secondary";

        //            }
        //            else if (insuranceType.IsEqual("Tertiary"))
        //            {
        //                if (patient.TertiaryInsurance == insuranceId)
        //                {
        //                    viewData.HealthPlanId = patient.TertiaryHealthPlanId;
        //                    viewData.GroupId = patient.TertiaryGroupId;
        //                    viewData.GroupName = patient.TertiaryGroupName;
        //                    viewData.Relationship = patient.TertiaryRelationship;
        //                }
        //                viewData.InsuranceType = "Tertiary";
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        //#endregion

        //#region Referral

        //public bool UpdateReferralModal(Referral referral)
        //{
        //    return referralRepository.UpdateModal(referral);
        //}

        //public bool AdmitReferral(PendingPatient pending, out Patient patientOut)
        //{
        //    return patientRepository.AdmitReferral(pending, out patientOut);
        //}

        //public Referral GetReferral(Guid referralId)
        //{
        //    return referralRepository.Get(Current.AgencyId, referralId);
        //}

        //public Referral GetReferralForNewPatient(Guid referralId)
        //{
        //    var referral = new Referral();
        //    var lastId = patientRepository.LastPatientId(Current.AgencyId);
        //    if (!referralId.IsEmpty())
        //    {
        //        referral = referralRepository.Get(Current.AgencyId, referralId);
        //        if (referral != null)
        //        {
        //            referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, referralId);
        //            referral.LastUsedPatientId = lastId;
        //        }
        //    }
        //    return referral ?? new Referral { LastUsedPatientId = lastId };
        //}

        //public void LinkReferralPhysician(PendingPatient pending, Referral referral)
        //{
        //    var physicains = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
        //    if (!pending.PrimaryPhysician.IsEmpty() && !physicains.Exists(p => p.Id == pending.PrimaryPhysician))
        //    {
        //        physicains.ForEach(p => { p.IsPrimary = false; });
        //        physicains.Add(new Physician() { Id = pending.PrimaryPhysician, IsPrimary = true });
        //    }
        //    if (physicains != null && physicains.Count > 0)
        //    {
        //        physicains.ForEach(p =>
        //        {
        //            physicianRepository.Link(referral.Id, p.Id, p.IsPrimary);
        //        });
        //    }
        //}

        //#endregion 

        //#region Patient Photo

        //public bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles)
        //{
        //    var result = false;

        //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //    if (patient != null)
        //    {
        //        var assetId = Guid.Empty;
        //        var isAssetSaved = AssetHelper.AddAssetByName(httpFiles, "Photo1", out assetId, true);
        //        if (isAssetSaved)
        //        {
        //            Patient patientOut = null;
        //            var oldPhotoId = patient.PhotoId;
        //            var admissionDateId = Guid.NewGuid();
        //            patient.PhotoId = assetId;
        //            var oldAdmissionDateId = patient.AdmissionId;
        //            patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
        //            if (patientRepository.Update(patient, out patientOut))
        //            {
        //                if (oldAdmissionDateId.IsEmpty())
        //                {
        //                    if (patientOut != null && AdmissionHelperFactory<PatientAdmissionDate>.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
        //                        result = true;
        //                    }
        //                    else
        //                    {
        //                        patient.PhotoId = oldPhotoId;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }
        //                else
        //                {
        //                    var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate(patient.Id, patient.AdmissionId);
        //                    if (admissionData != null && patientOut != null)
        //                    {
        //                        admissionData.PatientData = patientOut.ToXml();
        //                        admissionData.Status = patient.Status;
        //                        admissionData.StartOfCareDate = patient.StartofCareDate;
        //                        if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                        {
        //                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
        //                            result = true;
        //                        }
        //                        else
        //                        {
        //                            patient.PhotoId = oldPhotoId;
        //                            patient.AdmissionId = oldAdmissionDateId;
        //                            patientRepository.Update(patient);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        patient.PhotoId = oldPhotoId;
        //                        patient.AdmissionId = oldAdmissionDateId;
        //                        patientRepository.Update(patient);
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    return result;
        //}

        //public bool IsValidImage(HttpFileCollectionBase httpFiles)
        //{
        //    var result = false;
        //    if (httpFiles.Count > 0)
        //    {
        //        HttpPostedFileBase file = httpFiles.Get("Photo1");
        //        if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
        //        {
        //            var fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
        //            if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
        //            {
        //                var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        //                if (allowedExtensions != null && allowedExtensions.Length > 0)
        //                {
        //                    allowedExtensions.ForEach(extension =>
        //                    {
        //                        if (fileExtension.IsEqual(extension))
        //                        {
        //                            result = true;
        //                            return;
        //                        }
        //                    });
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public bool UpdatePatientForPhotoRemove(Guid patientId)
        //{
        //    var result = false;
        //     var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
        //     if (patient != null)
        //     {
        //         Patient patientOut = null;
        //         var photoId = patient.PhotoId;
        //         patient.PhotoId = Guid.Empty;
        //         if (patientRepository.Update(patient, out patientOut))
        //         {
        //             var admissionData = AdmissionHelperFactory<PatientAdmissionDate>.GetPatientAdmissionDate( patientOut.Id, patientOut.AdmissionId);
        //             if (admissionData != null && patientOut != null)
        //             {
        //                 admissionData.PatientData = patientOut.ToXml();
        //                 if (AdmissionHelperFactory<PatientAdmissionDate>.UpdatePatientAdmissionDate(admissionData))
        //                 {
        //                     Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
        //                     result = true;
        //                 }
        //                 else
        //                 {
        //                     patient.PhotoId = photoId;
        //                     patientRepository.Update(patient);
        //                 }
        //             }
        //             else
        //             {
        //                 patient.PhotoId = photoId;
        //                 patientRepository.Update(patient);
        //             }
        //         }
        //     }
        //    return result;
        //}

        //#endregion

        //#region More Members

        //public object GetPatientSelectionList(Guid branchId, int status)
        //{
        //    return patientRepository.Find(status, branchId, Current.AgencyId).OrderBy(s => s.DisplayName.ToUpperCase()).Select(p => new { Id = p.Id, Name = p.DisplayName }).ToList();
        //}

        //#endregion

        public bool Add(Profile profile)
        {
            return basePatientProfileRepository.Add(profile);
        }

        public bool Remove(Guid id)
        {
            return basePatientProfileRepository.Remove(Current.AgencyId, id);
        }

        public bool UpdateModal(Profile profile)
        {
            return basePatientProfileRepository.UpdateModal(profile);
        }

        public Profile GetProfileOnly(Guid patientId)
        {
            var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
            if (profile != null)
            {
                profile.Service = this.Service;
            }
            return profile;
        }

        public JsonViewData EditProfile(Profile profile)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient Profile could not be edited" };
            if (profile != null && !profile.Id.IsEmpty())
            {
                var rules = new List<Validation>();
                EditValidateAppSpecific(profile, rules);
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    profile.AgencyId = Current.AgencyId;
                    profile.Encode(); // setting string arrays to one field
                    if (EditProfileSave(profile))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Data successfully edited";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the data.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return viewData;
        }

        public bool EditProfileSave(Profile profile)
        {
            bool result = false;
            var profileToEdit = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, profile.Id);
            if (profileToEdit != null)
            {
                profile.Encode();
                var profileCloned = profileToEdit.DeepClone();
                EntityHelper.SetProfile(profileToEdit, profile);
                if (basePatientProfileRepository.UpdateModal(profileToEdit))
                {
                    Guid admissionDateId = Guid.NewGuid();
                    Guid oldAdmissionDateId = profileToEdit.AdmissionId;
                    profile.AdmissionId = profileToEdit.AdmissionId.IsEmpty() ? admissionDateId : profileToEdit.AdmissionId;

                    if (oldAdmissionDateId.IsEmpty())
                    {
                        var patient = patientRepository.GetPatientOnly(profile.Id, Current.AgencyId);
                        if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profileToEdit, 0, string.Empty, admissionDateId)))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, profile.Id, profile.Id.ToString(), LogType.Patient, LogAction.PatientEdited, this.Service.GetDescription() );
                            result = true;
                        }
                        else
                        {
                            basePatientProfileRepository.UpdateModal(profileCloned);
                        }
                    }
                    else
                    {
                        PatientAdmissionDate admissionData = GetPatientAdmissionDateAppSpecific(profile.Id, oldAdmissionDateId);
                        if (admissionData != null)
                        {
                            admissionData.StartOfCareDate = profileToEdit.StartofCareDate;
                            if (profile.Status == (int)PatientStatus.Discharged)
                            {
                                admissionData.DischargedDate = profileToEdit.DischargeDate;
                            }
                            admissionData.ProfileData = profileToEdit.ToXml();
                            if (UpdatePatientAdmissionDateAppSpecific(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, profile.Id, profile.Id.ToString(), LogType.Patient, LogAction.PatientEdited, this.Service.GetDescription() );
                                result = true;
                            }
                            else
                            {
                                basePatientProfileRepository.UpdateModal(profileCloned);
                            }
                        }
                        else
                        {
                            var patient = patientRepository.GetPatientOnly(profile.Id, Current.AgencyId);
                            if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profileToEdit, 0, string.Empty, oldAdmissionDateId)))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, profile.Id, profile.Id.ToString(), LogType.Patient, LogAction.PatientEdited, this.Service.GetDescription());
                                result = true;
                            }
                            else
                            {
                                basePatientProfileRepository.UpdateModal(profileCloned);
                            }
                        }
                    }
                }
            }
            return result;
        }

        //public JsonViewData EditPatient(Patient patient, Profile profile)
        //{
        //    var viewData = new JsonViewData {isSuccessful = false, errorMessage = "Patient could not be edited"};
        //    if (patient != null && !patient.Id.IsEmpty())
        //    {
        //        var rules = new List<Validation>
        //            {
        //                new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"),
        //                new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"),
        //                new Validation(() => !patient.DOB.IsValid(), "A valid date is required for the patient's Date Of birth. <br/>"),
        //                new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"),
        //                new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid format.  <br/>"),
        //                new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"),
        //                new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"),
        //                new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"),
        //                new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"),
        //                new Validation(() => !string.IsNullOrEmpty(patient.SSN) && !patient.SSN.IsSSN(), "Patient SSN is not in valid format.  <br/>"),
        //                new Validation(() => patient.Triage <= 0, "Emergency Triage is required.")
        //            };
        //        if (patient.SSN.IsNotNullOrEmpty())
        //        {
        //            bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
        //            rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
        //        }
        //        if (patient.PatientIdNumber.IsNotNullOrEmpty())
        //        {
        //            bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
        //            rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
        //        }
        //        EditValidateAppSpecific(patient, profile, rules);

        //        var entityValidator = new EntityValidator(rules.ToArray());
        //        entityValidator.Validate();
        //        if (entityValidator.IsValid)
        //        {
        //            patient.AgencyId = Current.AgencyId;
        //            patient.Encode(); // setting string arrays to one field
        //            if (EditPatientSave(patient, profile))
        //            {
        //                viewData.isSuccessful = true;
        //                viewData.errorMessage = "Your Data successfully edited";
        //            }
        //            else
        //            {
        //                viewData.isSuccessful = false;
        //                viewData.errorMessage = "Error in editing the data.";
        //            }
        //        }
        //        else
        //        {
        //            viewData.isSuccessful = false;
        //            viewData.errorMessage = entityValidator.Message;
        //        }
        //    }
        //    return viewData;
        //}

        //public bool EditPatientSave(Patient patient, Profile profile)
        //{
        //    bool result = false;

        //    Patient patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
        //    if (patientToEdit != null)
        //    {
        //        Patient patientOut;
        //        if (patientRepository.Edit(patientToEdit, patient, out patientOut) && patientOut != null)
        //        {
        //            Profile profileToEdit = patientProfileRepository.GetProfileOnly(Current.AgencyId, patient.Id);
        //            if (profileToEdit != null)
        //            {
        //                Profile profileOut;
        //                profile.FirstName = patient.FirstName;
        //                profile.LastName = patient.LastName;
        //                profile.MiddleInitial = patient.MiddleInitial;

        //                profile.Encode();
        //                if (patientProfileRepository.Edit(profileToEdit, profile, out profileOut) && profileOut != null)
        //                {
        //                    Guid admissionDateId = Guid.NewGuid();
        //                    Guid oldAdmissionDateId = profileToEdit.AdmissionId;
        //                    profile.AdmissionId = profileToEdit.AdmissionId.IsEmpty() ? admissionDateId : profileToEdit.AdmissionId;

        //                    if (oldAdmissionDateId.IsEmpty())
        //                    {
        //                        if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patientOut, profileOut, 0, string.Empty, admissionDateId)))
        //                        {
        //                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
        //                            result = true;
        //                        }
        //                        else
        //                        {
        //                            profileToEdit.AdmissionId = oldAdmissionDateId;
        //                            patientProfileRepository.UpdateModal(profileToEdit);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        PatientAdmissionDate admissionData = GetPatientAdmissionDateAppSpecific(patientToEdit.Id, oldAdmissionDateId);
        //                        if (admissionData != null)
        //                        {
        //                            admissionData.StartOfCareDate = profileOut.StartofCareDate;
        //                            if (profile.Status == (int) PatientStatus.Discharged)
        //                            {
        //                                admissionData.DischargedDate = profileOut.DischargeDate;
        //                            }
        //                            admissionData.PatientData = patientOut.ToXml();
        //                            admissionData.ProfileData = profileOut.ToXml();
        //                            if (UpdatePatientAdmissionDateAppSpecific(admissionData))
        //                            {
        //                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
        //                                result = true;
        //                            }
        //                            else
        //                            {
        //                                profileToEdit.AdmissionId = oldAdmissionDateId;
        //                                patientProfileRepository.UpdateModal(profileToEdit);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patientOut, profileOut, 0, string.Empty, oldAdmissionDateId)))
        //                            {
        //                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
        //                                result = true;
        //                            }
        //                            else
        //                            {
        //                                profileToEdit.AdmissionId = oldAdmissionDateId;
        //                                patientProfileRepository.UpdateModal(profileToEdit);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        public JsonViewData ActivatePatient(Guid patientId)
        {
            var viewData = new JsonViewData {isSuccessful = false};
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    int oldStatus = GetStatus(patient);
                    profile.Status = (int) PatientStatus.Active;

                    Guid admissionDateId = Guid.NewGuid();
                    Guid oldAdmissionDateId = profile.AdmissionId;

                    profile.AdmissionId = profile.AdmissionId.IsEmpty() ? admissionDateId : profile.AdmissionId;

                    SetStatus(patient, profile);
                    if (patientRepository.UpdateModal(patient))
                    {
                        if (basePatientProfileRepository.UpdateModal(profile))
                        {
                            if (oldAdmissionDateId.IsEmpty())
                            {
                                if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, "Patient Activated.", admissionDateId)))
                                {
                                    viewData.IsCenterRefresh = oldStatus != profile.Status && (profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged);
                                    viewData.isSuccessful = true;
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                }
                                else
                                {
                                    profile.Status = oldStatus;
                                    profile.AdmissionId = oldAdmissionDateId;

                                    basePatientProfileRepository.UpdateModal(profile);
                                    SetStatus(patient, profile);
                                    patientRepository.UpdateModal(patient);
                                }
                            }
                            else
                            {
                                PatientAdmissionDate admissionData = GetPatientAdmissionDateAppSpecific(profile.Id, profile.AdmissionId);
                                if (admissionData != null)
                                {
                                    admissionData.PatientData = patient.ToXml();
                                    admissionData.ProfileData = profile.ToXml();
                                    admissionData.Status = profile.Status;
                                    admissionData.StartOfCareDate = profile.StartofCareDate;
                                    if (UpdatePatientAdmissionDateAppSpecific(admissionData))
                                    {
                                        viewData.IsCenterRefresh = oldStatus != profile.Status && (profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged);
                                        viewData.isSuccessful = true;
                                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    }
                                    else
                                    {
                                        profile.Status = oldStatus;
                                        profile.AdmissionId = oldAdmissionDateId;

                                        basePatientProfileRepository.UpdateModal(profile);
                                        SetStatus(patient, profile);
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                                else
                                {
                                    if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, "Patient Activated.", admissionDateId)))
                                    {
                                        viewData.IsCenterRefresh = oldStatus != profile.Status && (profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged);
                                        viewData.isSuccessful = true;
                                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    }
                                    else
                                    {
                                        profile.Status = oldStatus;
                                        profile.AdmissionId = oldAdmissionDateId;

                                        basePatientProfileRepository.UpdateModal(profile);
                                        SetStatus(patient, profile);
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                            }

                            if (viewData.isSuccessful)
                            {
                                viewData.IsCenterRefresh = oldStatus != profile.Status && (profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged);
                                viewData.IsPatientListRefresh = oldStatus != profile.Status;
                                viewData.PatientStatus = (int)PatientStatus.Active;
                            }
                        }
                        else
                        {
                            profile.Status = oldStatus;
                            SetStatus(patient, profile);
                            patientRepository.UpdateModal(patient);
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ActivatePatient(Guid patientId, DateTime startOfCareDate)
        {
            var viewData = new JsonViewData {isSuccessful = false};
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    int oldStatus = GetStatus(patient);
                    DateTime oldStartOfCareDate = profile.StartofCareDate;

                    profile.Status = (int) PatientStatus.Active;
                    profile.StartofCareDate = startOfCareDate;

                    Guid admissionDateId = Guid.NewGuid();
                    Guid oldAdmissionDateId = profile.AdmissionId;
                    profile.AdmissionId = admissionDateId;

                    SetStatus(patient, profile);
                    if (patientRepository.UpdateModal(patient))
                    {
                        if (basePatientProfileRepository.UpdateModal(profile))
                        {
                            if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, "Patient Activated.", admissionDateId)))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                profile.Status = oldStatus;
                                profile.StartofCareDate = oldStartOfCareDate;
                                profile.AdmissionId = oldAdmissionDateId;

                                basePatientProfileRepository.UpdateModal(profile);
                                SetStatus(patient, profile);
                                patientRepository.UpdateModal(patient);
                            }
                            if (viewData.isSuccessful)
                            {
                                viewData.IsCenterRefresh = oldStatus != profile.Status && (profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged);
                                viewData.IsPatientListRefresh = oldStatus != profile.Status;
                                viewData.PatientStatus = (int) PatientStatus.Active;
                            }
                        }
                    }
                    else
                    {
                        profile.Status = oldStatus;
                        SetStatus(patient, profile);
                        patientRepository.UpdateModal(patient);
                    }
                }
            }
            return viewData;
        }

        public JsonViewData SetPatientPending(Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    int oldStatus = GetStatus(patient);
                    profile.Status = (int)PatientStatus.Pending;

                    Guid admissionDateId = Guid.NewGuid();
                    Guid oldAdmissionDateId = profile.AdmissionId;

                    profile.AdmissionId = profile.AdmissionId.IsEmpty() ? admissionDateId : profile.AdmissionId;
                    SetStatus(patient, profile);
                    if (patientRepository.UpdateModal(patient))
                    {
                        if (basePatientProfileRepository.UpdateModal(profile))
                        {
                            if (oldAdmissionDateId.IsEmpty())
                            {
                                if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, (int) DischargeReasons.Other, "Patient Set Pending.", admissionDateId)))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                    viewData.isSuccessful = true;
                                }
                                else
                                {
                                    profile.Status = oldStatus;
                                    profile.AdmissionId = oldAdmissionDateId;

                                    basePatientProfileRepository.UpdateModal(profile);
                                    SetStatus(patient, profile);
                                    patientRepository.UpdateModal(patient);
                                }
                            }
                            else
                            {
                                PatientAdmissionDate admissionData = GetPatientAdmissionDateAppSpecific(patient.Id, profile.AdmissionId);
                                if (admissionData != null)
                                {
                                    admissionData.PatientData = patient.ToXml();
                                    admissionData.ProfileData = profile.ToXml();
                                    admissionData.Status = profile.Status;
                                    if (UpdatePatientAdmissionDateAppSpecific(admissionData))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                        viewData.isSuccessful = true;
                                    }
                                    else
                                    {
                                        profile.Status = oldStatus;
                                        profile.AdmissionId = oldAdmissionDateId;

                                        basePatientProfileRepository.UpdateModal(profile);
                                        SetStatus(patient, profile);
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                                else
                                {
                                    if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, (int) DischargeReasons.Other, "Patient Set Pending.", admissionDateId)))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                        viewData.isSuccessful = true;
                                    }
                                    else
                                    {
                                        profile.Status = oldStatus;
                                        profile.AdmissionId = oldAdmissionDateId;

                                        basePatientProfileRepository.UpdateModal(profile);
                                        SetStatus(patient, profile);
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                            }
                            if (viewData.isSuccessful)
                            {
                                viewData.IsCenterRefresh = oldStatus != profile.Status && ((profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged) || (oldStatus == (int) PatientStatus.Active || oldStatus == (int) PatientStatus.Discharged));
                                viewData.IsPendingPatientListRefresh = (int) PatientStatus.Pending != oldStatus;
                                viewData.IsPatientListRefresh = viewData.IsPendingPatientListRefresh;
                                viewData.PatientStatus = (int) PatientStatus.Pending;
                            }
                        }
                        else
                        {
                            profile.Status = oldStatus;
                            SetStatus(patient, profile);
                            patientRepository.UpdateModal(patient);
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData NonAdmitPatient(PendingPatient pending)
        {
            var viewData = new JsonViewData {isSuccessful = false};
            Patient patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
            if (patient != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, pending.Id);
                if (profile != null)
                {
                    string oldComment = patient.Comments;

                    int oldStatus = GetStatus(patient);
                    DateTime oldNonAdmissionDate = profile.NonAdmissionDate;
                    string oldNonAdmissionReason = profile.NonAdmissionReason;
                    Guid oldAdmissionDateId = profile.AdmissionId;

                    Guid admissionDateId = Guid.NewGuid();

                    profile.AdmissionId = profile.AdmissionId.IsEmpty() ? admissionDateId : profile.AdmissionId;
                    profile.NonAdmissionDate = pending.NonAdmitDate;
                    profile.NonAdmissionReason = pending.Reason;
                    profile.Status = (int) PatientStatus.NonAdmission;

                    patient.Comments = pending.Comments;

                    SetStatus(patient, profile);
                    if (patientRepository.UpdateModal(patient))
                    {
                        if (basePatientProfileRepository.UpdateModal(profile))
                        {
                            if (profile.AdmissionId.IsEmpty())
                            {
                                if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, "Patient Set Non-admitted.", admissionDateId)))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                    viewData.isSuccessful = true;
                                }
                                else
                                {
                                    profile.NonAdmissionDate = oldNonAdmissionDate;
                                    profile.Status = oldStatus;
                                    profile.NonAdmissionReason = oldNonAdmissionReason;
                                    profile.AdmissionId = oldAdmissionDateId;

                                    basePatientProfileRepository.UpdateModal(profile);

                                    SetStatus(patient, profile);
                                    patient.Comments = oldComment;
                                    patientRepository.UpdateModal(patient);
                                }
                            }
                            else
                            {
                                PatientAdmissionDate admissionData = GetPatientAdmissionDateAppSpecific(patient.Id, profile.AdmissionId);
                                if (admissionData != null)
                                {
                                    admissionData.PatientData = patient.ToXml();
                                    admissionData.ProfileData = profile.ToXml();
                                    admissionData.Status = profile.Status;
                                    if (UpdatePatientAdmissionDateAppSpecific(admissionData))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                        viewData.isSuccessful = true;
                                    }
                                    else
                                    {
                                        profile.NonAdmissionDate = oldNonAdmissionDate;
                                        profile.Status = oldStatus;
                                        profile.NonAdmissionReason = oldNonAdmissionReason;
                                        profile.AdmissionId = oldAdmissionDateId;

                                        basePatientProfileRepository.UpdateModal(profile);

                                        SetStatus(patient, profile);
                                        patient.Comments = oldComment;
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                                else
                                {
                                    if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, "Patient Set Non-admitted.", admissionDateId)))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                        viewData.isSuccessful = true;
                                    }
                                    else
                                    {
                                        profile.NonAdmissionDate = oldNonAdmissionDate;
                                        profile.Status = oldStatus;
                                        profile.NonAdmissionReason = oldNonAdmissionReason;
                                        profile.AdmissionId = oldAdmissionDateId;

                                        basePatientProfileRepository.UpdateModal(profile);

                                        SetStatus(patient, profile);
                                        patient.Comments = oldComment;
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                            }
                            if (viewData.isSuccessful)
                            {
                                viewData.IsCenterRefresh = oldStatus != profile.Status && ((profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged) || (oldStatus == (int) PatientStatus.Active || oldStatus == (int) PatientStatus.Discharged));
                                viewData.IsNonAdmitPatientListRefresh = (int) PatientStatus.NonAdmission != profile.Status;
                                viewData.IsPatientListRefresh = viewData.IsNonAdmitPatientListRefresh;
                                viewData.PatientStatus = (int) PatientStatus.NonAdmission;
                                viewData.IsPendingPatientListRefresh = false;
                            }
                        }
                        else
                        {
                            patient.Comments = oldComment;
                            profile.Status = oldStatus;
                            SetStatus(patient, profile);
                            patientRepository.UpdateModal(patient);
                        }
                    }
                }
            }
            return viewData;
        }


        public List<PatientData> GetPatients(Guid branchId, int status)
        {
            var patients = basePatientProfileRepository.All(Current.AgencyId, branchId, status);
            if (patients.IsNotNullOrEmpty())
            {
                var insuranceIds = patients.Where(s => s.InsuranceId.IsInteger() && s.InsuranceId.ToInteger() > 0).Select(i => i.InsuranceId.ToInteger()).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                patients.ForEach((PatientData patient) =>
                {
                    var insurance = insurances.FirstOrDefault(i => i.Id.ToString() == patient.InsuranceId);
                    if (insurance != null)
                    {
                        patient.InsuranceName = insurance.Name;
                    }
                    if (patient.InsuranceName.IsNotNullOrEmpty())
                    {
                        patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
                    }
                });
            }
            return patients;
        }

        public PatientDataViewData GetPatientsViewData(Guid locationId, bool isLocationNeededIdEmpty, int status)
        {
            var viewData = new PatientDataViewData { Service = this.Service };
            if (isLocationNeededIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            viewData.LocationId = locationId;
            viewData.Patients = this.GetPatients(locationId, status);
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Patient, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Edit, (int)PermissionActions.Export, (int)PermissionActions.Add, (int)PermissionActions.Delete });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.DeletePermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None);
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
                viewData.NewPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None);
                viewData.EditPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None);
            }
            return viewData;
        }

        public List<NonAdmit> GetNonAdmits(bool isExcel)
        {
            var list = new List<NonAdmit>();
            var allPermission = new Dictionary<int, AgencyServices>();
            var isAdmitPermission = false;
            if (!isExcel)
            {
                allPermission = Current.CategoryService(this.Service, ParentPermission.Patient, new int[] {  (int)PermissionActions.Admit });
                if (allPermission.IsNotNullOrEmpty())
                {
                    isAdmitPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Admit, AgencyServices.None) != AgencyServices.None;
                }
            }
            var patients = basePatientProfileRepository.GetPatientsWithProfile(Current.AgencyId, (int)PatientStatus.NonAdmission);
            if (patients != null && patients.Count > 0)
            {
                var insuranceIds = patients.Where(p => p.Profile.PrimaryInsurance > 0).Select(p => p.Profile.PrimaryInsurance).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                patients.ForEach(p =>
                {
                    var insuranceName = string.Empty;
                    if (p.Profile.PrimaryInsurance > 0)
                    {
                        var insurance = insurances.FirstOrDefault(i => i.Id == p.Profile.PrimaryInsurance);
                        if (insurance != null)
                        {
                            insuranceName = insurance.Name;
                        }
                    }
                    list.Add(new NonAdmit
                    {
                        Id = p.Id,
                        PatientIdNumber = p.PatientIdNumber,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        MiddleInitial = p.MiddleInitial,
                        DateOfBirth = p.DOB,
                        Phone = p.PhoneHomeFormatted,
                        Gender = p.Gender,

                        AddressCity = p.AddressCity,
                        AddressLine1 = p.AddressLine1,
                        AddressLine2 = p.AddressLine2,
                        AddressStateCode = p.AddressStateCode,
                        AddressZipCode = p.AddressZipCode,
                        MedicareNumber = p.MedicaidNumber,

                        Type = NonAdmitTypes.Patient,
                        NonAdmitTypeName = NonAdmitTypes.Patient.ToString(),

                        NonAdmissionReason = p.Profile.NonAdmissionReason,
                        NonAdmitDate = p.Profile.NonAdmissionDate,
                        InsuranceName = insuranceName,
                        InsuranceNumber = p.Profile.PrimaryHealthPlanId,
                        Comments = p.Comments,
                        ServiceType = (int)Service,
                        ServiceName = Service.ToString(),
                        IsUserCanAdmit = isAdmitPermission
                    });
                });
            }
            var referrals = referralRepository.GetNonAdmitReferral(Current.AgencyId, ReferralStatus.NonAdmission, this.Service,isAdmitPermission);
            if (referrals.IsNotNullOrEmpty())
            {
                list.AddRange(referrals);
            }
            return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        }

        public JsonViewData DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason, Guid optionalEpisodeId)
        {
            var viewData = new JsonViewData {isSuccessful = false};
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    int oldStatus = GetStatus(patient);
                    DateTime oldDischargeDate = profile.DischargeDate;
                    string oldDischargeReason = profile.DischargeReason;
                    Guid oldAdmissionDateId = profile.AdmissionId;

                    profile.DischargeDate = dischargeDate;
                    profile.Status = (int) PatientStatus.Discharged;
                    profile.DischargeReasonId = dischargeReasonId;
                    profile.DischargeReason = dischargeReason;

                    Guid admissionDateId = Guid.NewGuid();
                    profile.AdmissionId = profile.AdmissionId.IsEmpty() ? admissionDateId : profile.AdmissionId;

                    SetStatus(patient, profile);
                    if (patientRepository.UpdateModal(patient))
                    {
                        if (basePatientProfileRepository.UpdateModal(profile))
                        {
                            if (oldAdmissionDateId.IsEmpty())
                            {
                                if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, dischargeReasonId, dischargeReason, admissionDateId)))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                    viewData.isSuccessful = true;
                                }
                                else
                                {
                                    profile.DischargeDate = oldDischargeDate;
                                    profile.Status = oldStatus;
                                    profile.DischargeReason = oldDischargeReason;
                                    profile.AdmissionId = oldAdmissionDateId;
                                    basePatientProfileRepository.UpdateModal(profile);
                                    SetStatus(patient, profile);
                                    patientRepository.UpdateModal(patient);
                                }
                            }
                            else
                            {
                                PatientAdmissionDate admissionData = GetPatientAdmissionDateAppSpecific(profile.Id, profile.AdmissionId);
                                if (admissionData != null)
                                {
                                    admissionData.PatientData = patient.ToXml();
                                    admissionData.Status = profile.Status;
                                    admissionData.StartOfCareDate = profile.StartofCareDate;
                                    admissionData.DischargedDate = profile.DischargeDate;
                                    if (UpdatePatientAdmissionDateAppSpecific(admissionData))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                        viewData.isSuccessful = true;
                                    }
                                    else
                                    {
                                        profile.DischargeDate = oldDischargeDate;
                                        profile.Status = oldStatus;
                                        profile.DischargeReason = oldDischargeReason;
                                        profile.AdmissionId = oldAdmissionDateId;
                                        basePatientProfileRepository.UpdateModal(profile);
                                        SetStatus(patient, profile);
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                                else
                                {
                                    if (AddPatientAdmissionDateAppSpecific(EntityHelper.CreatePatientAdmissionDate(patient, profile, dischargeReasonId, dischargeReason, admissionDateId)))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                        viewData.isSuccessful = true;
                                    }
                                    else
                                    {
                                        profile.DischargeDate = oldDischargeDate;
                                        profile.Status = oldStatus;
                                        profile.DischargeReason = oldDischargeReason;
                                        profile.AdmissionId = oldAdmissionDateId;
                                        basePatientProfileRepository.UpdateModal(profile);
                                        SetStatus(patient, profile);
                                        patientRepository.UpdateModal(patient);
                                    }
                                }
                            }
                            if (viewData.isSuccessful)
                            {
                                UpdateEpisodeAndClaimsForDischargePatientAppSpecific(patientId, dischargeDate, optionalEpisodeId);
                                viewData.isSuccessful = DischargeAllMedicationOfPatient(patientId, dischargeDate);
                                viewData.IsCenterRefresh = oldStatus != profile.Status && ((profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged) || (oldStatus == (int) PatientStatus.Active || oldStatus == (int) PatientStatus.Discharged));
                                viewData.IsPatientListRefresh = oldStatus != profile.Status;
                                viewData.PatientStatus = (int) PatientStatus.Discharged;
                            }
                        }
                        else
                        {
                            profile.Status = oldStatus;
                            SetStatus(patient, profile);
                            patientRepository.UpdateModal(patient);
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData DeletePatient(Guid Id, bool isDeprecated)
        {
            var data = new JsonViewData {isSuccessful = false};
            Patient patient = patientRepository.GetPatientOnly(Id, Current.AgencyId);
            if (patient != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, Id);
                if (profile != null)
                {
                    int oldStatus = GetStatus(patient);
                    int previousStatus = profile.OldStatus;

                    if (isDeprecated)
                    {
                        profile.OldStatus = oldStatus;
                        profile.Status = (int) PatientStatus.Deprecated;
                    }
                    else
                    {
                        profile.Status = previousStatus;
                    }

                    SetStatus(patient, profile);
                    if (patientRepository.UpdateModal(patient))
                    {
                        profile.IsDeprecated = isDeprecated;
                        if (basePatientProfileRepository.UpdateModal(profile))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, Id, Id.ToString(), LogType.Patient, isDeprecated ? LogAction.PatientDeleted : LogAction.PatientRestored, string.Empty);
                            data.isSuccessful = true;
                            data.PatientId = profile.Id;
                            data.IsDeletedPatientListRefresh = true;
                            data.IsCenterRefresh = (profile.Status == (int) PatientStatus.Active || profile.Status == (int) PatientStatus.Discharged) || (oldStatus == (int) PatientStatus.Active || oldStatus == (int) PatientStatus.Discharged);
                            data.IsPatientListRefresh = true;
                            data.PatientStatus = profile.Status;
                            data.IsNonAdmitPatientListRefresh = profile.Status == (int) PatientStatus.NonAdmission;
                            data.IsPendingPatientListRefresh = profile.Status == (int) PatientStatus.Pending;
                        }
                        else
                        {
                            profile.Status = oldStatus;
                            SetStatus(patient, profile);
                            patientRepository.UpdateModal(patient);
                        }
                    }
                }
            }
            return data;
        }

        public PendingPatient GetPendingPatient(Guid id, NonAdmitTypes type)
        {
            PendingPatient pending = null;
            if (type == NonAdmitTypes.Patient)
            {
                Patient patient = patientRepository.GetPatientOnly(id, Current.AgencyId);
                if (patient != null)
                {
                    Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, id);
                    if (profile != null)
                    {
                        pending = new PendingPatient
                            {
                                Id = patient.Id,
                                DisplayName = patient.DisplayName,
                                PatientIdNumber = patient.PatientIdNumber,
                                ReferralDate = profile.ReferralDate,
                                StartofCareDate = profile.StartofCareDate,
                                CaseManagerId = profile.CaseManagerId,
                                PrimaryInsurance = profile.PrimaryInsurance,
                                SecondaryInsurance = profile.SecondaryInsurance,
                                TertiaryInsurance = profile.TertiaryInsurance,
                                UserId = profile.UserId,
                                Type = NonAdmitTypes.Patient,
                                CurrentService = Service,
                                HomeHealthStatus = patient.HomeHealthStatus,
                                PrivateDutyStatus = patient.PrivateDutyStatus
                                //Payer = patient.Payer,
                            };
                        SetServiceProperty(pending, patient.Services, patient.HomeHealthStatus, patient.PrivateDutyStatus);
                        AgencyPhysician physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            pending.PrimaryPhysician = physician.Id;
                        }
                    }
                }
            }
            else
            {
                Referral referral = referralRepository.Get(Current.AgencyId, id);
                if (referral != null)
                {
                    List<Physician> physicians = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
                    Physician primaryPhysician = physicians.FirstOrDefault(p => p.IsPrimary);
                    pending = new PendingPatient
                        {
                            Id = referral.Id,
                            DisplayName = referral.DisplayName,
                            PatientIdNumber = string.Empty,
                            StartofCareDate = DateTime.Today,
                            ReferralDate = referral.ReferralDate,
                            Type = NonAdmitTypes.Referral,
                            UserId = referral.UserId,
                            CurrentService = Service,
                            HomeHealthStatus = referral.HomeHealthStatus,
                            PrivateDutyStatus = referral.PrivateDutyStatus,
                            PrimaryPhysician = primaryPhysician != null ? primaryPhysician.Id : Guid.Empty
                        };
                    SetServiceProperty(pending, referral.Services, referral.HomeHealthStatus, referral.PrivateDutyStatus);
                }
            }
            return pending;
        }

        public List<PendingPatient> GetPendingPatients(Guid branchId, bool isExcel)
        {
            var patients = basePatientProfileRepository.GetPendingByAgencyId(Current.AgencyId, branchId);
            if (patients.IsNotNullOrEmpty())
            {
                var insuranceIds = patients.Where(s => s.PrimaryInsurance > 0).Select(i => i.PrimaryInsurance).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                var locations = LocationEngine.GetLocations(Current.AgencyId, patients.Select(p => p.AgencyLocationId).Distinct().ToList()) ?? new List<AgencyLocation>();

                var allPermission = new Dictionary<int, AgencyServices>();
                var isEditPermission = false;
                var isAdmitPermission = false;
                var isNonAdmitPermission = false;
                if (!isExcel)
                {
                    allPermission = Current.CategoryService(this.Service, ParentPermission.Patient, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Admit, (int)PermissionActions.NonAdmit });
                    if (allPermission.IsNotNullOrEmpty())
                    {
                        isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                        isAdmitPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Admit, AgencyServices.None) != AgencyServices.None;
                        isNonAdmitPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.NonAdmit, AgencyServices.None) != AgencyServices.None;
                    }
                }

                patients.ForEach(p =>
                {
                    if (p.PrimaryInsurance > 0)
                    {
                        if (p.PrimaryInsurance == 18) //TODO: Replace 18 with an enum value that represents Self Pay
                        {
                            p.PrimaryInsuranceName = "Self Pay";
                        }
                        else
                        {
                            var insurance = insurances.FirstOrDefault(i => i.Id == p.PrimaryInsurance);
                            if (insurance != null)
                            {
                                p.PrimaryInsuranceName = insurance.Name;
                            }
                        }
                    }
                    var location = locations.FirstOrDefault(l => l.Id == p.AgencyLocationId);
                    if (location != null)
                    {
                        p.Branch = location.Name;
                    }
                    if (!isExcel)
                    {
                        p.IsUserCanEdit = isEditPermission;
                        p.IsUserCanAdmit = isAdmitPermission;
                        p.IsUserCanNonAdmit = isNonAdmitPermission;
                    }
                });
            }

            return patients;
        }

        public List<PatientData> GetDeletedPatients(Guid branchId, bool IsInsuranceNameNeeded)
        {
            var patients = basePatientProfileRepository.AllDeleted(Current.AgencyId, branchId);
            if (IsInsuranceNameNeeded)
            {
                if (patients.IsNotNullOrEmpty())
                {
                    var insuranceIds = patients.Where(s => s.InsuranceId.IsInteger() && s.InsuranceId.ToInteger() > 0).Select(i => i.InsuranceId.ToInteger()).Distinct().ToList();
                    var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                    patients.ForEach((patient) =>
                    {
                        var insurance = insurances.FirstOrDefault(i => i.Id.ToString() == patient.InsuranceId);
                        if (insurance != null)
                        {
                            patient.InsuranceName = insurance.Name;
                        }
                        if (patient.InsuranceName.IsNotNullOrEmpty())
                        {
                            patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
                        }
                    });
                }
            }
            return patients.ToList();
        }

        public PatientDataViewData GetDeletedPatientViewData(Guid locationId,bool isLocationNeededIdEmpty, bool IsInsuranceNameNeeded)
        {
            var viewData = new PatientDataViewData { Service = this.Service };
            if (isLocationNeededIdEmpty)
            {
                if (locationId.IsEmpty())
                {
                    locationId = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
                }
            }
            viewData.LocationId = locationId;
            viewData.Patients = this.GetDeletedPatients(locationId, IsInsuranceNameNeeded);
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Patient, new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Export, (int)PermissionActions.Restore });
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.AvailableService = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None);
                viewData.ViewListPermissions = viewData.AvailableService;
                viewData.RestorePermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Restore, AgencyServices.None);
                viewData.ExportPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
            }
            return viewData;
        }


        public void SetServiceProperty(PendingPatient pending, AgencyServices services, int homeHealthStatus, int privateDutyStatus)
        {
            int count = 0;
            if (services.IsAlone())
            {
                if (services.Has(AgencyServices.HomeHealth))
                {
                    pending.Services = AgencyServices.HomeHealth;
                    count++;
                }
                else if (services.Has(AgencyServices.PrivateDuty))
                {
                    pending.Services = AgencyServices.PrivateDuty;
                    count++;
                }
            }
            else
            {
                if (services.Has(AgencyServices.HomeHealth) && PatientStatusFactory.PossibleAdmit().Contains(homeHealthStatus))
                {
                    pending.Services = AgencyServices.HomeHealth;
                    count++;
                }
                if (services.Has(AgencyServices.PrivateDuty) && PatientStatusFactory.PossibleAdmit().Contains(privateDutyStatus))
                {
                    pending.Services |= AgencyServices.PrivateDuty;
                    count++;
                }
            }
            if (count == 0)
            {
                pending.Services = Current.PreferredService;
            }
            pending.IsOnlyOneService = count <= 1;
        }

        public ServiceAndGuidViewData GetServiceAndLocationViewData()
        {
            return new ServiceAndGuidViewData(this.Service, agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service));
        }

        public GridViewData GetServiceAndLocationViewData(ParentPermission category, PermissionActions availablePermissionAction, List<PermissionActions> moreActions, bool IsLocationNeeded)
        {
            var viewData = new GridViewData { Service = this.Service };
            if (IsLocationNeeded)
            {
                viewData.Id = agencyRepository.GetBranchForSelectionId(Current.AgencyId, (int)this.Service);
            }
            moreActions.Add(availablePermissionAction);
            var allPermission = Current.CategoryService(Current.AcessibleServices, category, moreActions.Select(a => (int)a).ToArray());
            if (allPermission.IsNotNullOrEmpty())
            {
                moreActions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == availablePermissionAction)
                    {
                        viewData.AvailableService = permission;
                    }
                    if (a == PermissionActions.Add)
                    {
                        viewData.NewPermissions = permission;
                    }

                    if (a == PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }

                    if (a == PermissionActions.ViewList)
                    {
                        viewData.ViewListPermissions = permission;
                    }
                });
            }
            return viewData;
        }

        public PatientProfile GetProfileWithOutEpisodeInfo(Guid patientId)
        {
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var patientProfile = new PatientProfile();
                AllergyProfile allerg = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                patientProfile.Allergies = allerg != null ? allerg.ToString() : string.Empty;
                AgencyPhysician physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    patientProfile.Physician = physician;
                }
                patient.EmergencyContacts = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId);
                if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
                {
                    patientProfile.EmergencyContact = patient.EmergencyContacts.OrderByDescending(e => e.IsPrimary).FirstOrDefault();
                }
                patientProfile.Patient = patient;
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    patientProfile.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
                    SetInsurance(profile);

                    if (!profile.CaseManagerId.IsEmpty())
                    {
                        profile.CaseManagerName = UserEngine.GetName(profile.CaseManagerId, Current.AgencyId);
                    }
                    if (!profile.UserId.IsEmpty())
                    {
                        patientProfile.Clinician = UserEngine.GetName(profile.UserId, Current.AgencyId);
                    }
                    patientProfile.Patient.Profile = profile;
                }
                else
                {
                    patientProfile.LocationProfile = patientProfile.LocationProfile ?? new LocationPrintProfile();
                }

                return patientProfile;
            }

            return null;
        }

        public PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType)
        {
            var viewData = new PatientInsuranceInfoViewData();
            viewData.InsuranceType = insuranceType.ToTitleCase();
            if (!patientId.IsEmpty())
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    if (insuranceType.IsEqual("Primary"))
                    {
                        if (profile.PrimaryInsurance.ToString(CultureInfo.InvariantCulture) == insuranceId)
                        {
                            viewData.HealthPlanId = profile.PrimaryHealthPlanId;
                            viewData.GroupName = profile.PrimaryGroupName;
                            viewData.GroupId = profile.PrimaryGroupId;
                            viewData.Relationship = profile.PrimaryRelationship;
                        }
                        viewData.InsuranceType = "Primary";
                    }
                    else if (insuranceType.IsEqual("Secondary"))
                    {
                        if (profile.SecondaryInsurance.ToString() == insuranceId)
                        {
                            viewData.HealthPlanId = profile.SecondaryHealthPlanId;
                            viewData.GroupName = profile.SecondaryGroupName;
                            viewData.GroupId = profile.SecondaryGroupId;
                            viewData.Relationship = profile.SecondaryRelationship;
                        }
                        viewData.InsuranceType = "Secondary";
                    }
                    else if (insuranceType.IsEqual("Tertiary"))
                    {
                        if (profile.TertiaryInsurance.ToString() == insuranceId)
                        {
                            viewData.HealthPlanId = profile.TertiaryHealthPlanId;
                            viewData.GroupId = profile.TertiaryGroupId;
                            viewData.GroupName = profile.TertiaryGroupName;
                            viewData.Relationship = profile.TertiaryRelationship;
                        }
                        viewData.InsuranceType = "Tertiary";
                    }
                }
            }
            return viewData;
        }

        public Patient GetPatientWithProfile(Guid patientId)
        {
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (patient.Services.Has(Service))
                {
                    patient.Profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId) ?? new Profile();
                }
                patient.RequestedService = Service;
            }
            return patient;
        }

        public Patient GetPatientSnapshotInfo(Guid patientId, bool isPrimaryInsuranceSet, bool isEmergencyContactSet, bool isInsuranceSet)
        {
            Patient patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                patient.Profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId) ?? new Profile();
                SetPatientSnapshotInfo(patient, true, true, isInsuranceSet);
            }
            return patient;
        }

        public IList<Patient> Find(Guid branchId, int statusId)
        {
            return basePatientProfileRepository.Find(Current.AgencyId, branchId, statusId);
        }

        public void SetPatientSnapshotInfo(Patient patient, bool isPrimaryInsuranceSet, bool isEmergencyContactSet, bool isInsuranceSet)
        {
            if (patient != null)
            {
                if (isPrimaryInsuranceSet)
                {
                    AgencyPhysician physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                    if (physician != null)
                    {
                        patient.Physician = physician;
                    }
                }
                if (isEmergencyContactSet)
                {
                    PatientEmergencyContact emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patient.Id);
                    if (emergencyContact != null)
                    {
                        patient.EmergencyContact = emergencyContact;
                    }
                }
                if (isInsuranceSet)
                {
                    SetInsurance(patient);
                }
            }
        }

        public void SetInsurance(Patient patient)
        {
            if (patient.Profile != null)
            {
                SetInsurance(patient.Profile);
            }
        }

        public void SetInsurance(Profile profile)
        {
            if (profile != null)
            {
                if (profile.PrimaryInsurance > 0)
                {
                    profile.PrimaryInsuranceName = InsuranceEngine.GetName(profile.PrimaryInsurance, Current.AgencyId);
                }
                if (profile.SecondaryInsurance > 0)
                {
                    profile.SecondaryInsuranceName = InsuranceEngine.GetName(profile.SecondaryInsurance, Current.AgencyId);
                }
                if (profile.TertiaryInsurance > 0)
                {
                    profile.TertiaryInsuranceName = InsuranceEngine.GetName(profile.TertiaryInsurance, Current.AgencyId);
                }
            }
        }

        public AllergyProfileViewData GetAllergyProfilePrint(Guid id)
        {
            var allergyProfile = new AllergyProfileViewData();
            AllergyProfile allergies = patientRepository.GetAllergyProfileByPatient(id, Current.AgencyId);
            if (allergies != null)
            {
                allergyProfile.AllergyProfile = allergies;
            }
            PatientProfileLean profile = basePatientProfileRepository.GetPatientPrintProfile(Current.AgencyId, id);
            if (profile != null)
            {
                allergyProfile.PatientProfile = profile;

                allergyProfile.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
            }
            return allergyProfile;
        }

        public DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected)
        {
            var viewData = new DrugDrugInteractionsViewData();
            if (drugsSelected != null && drugsSelected.Count > 0)
            {
                viewData.DrugDrugInteractions = drugService.GetDrugDrugInteractions(drugsSelected);
            }
            PatientProfileLean profile = basePatientProfileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
            if (profile != null)
            {
                viewData.PatientProfile = profile;
                viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
            }
            return viewData;
        }

      
        public bool DischargeAllMedicationOfPatient(Guid patientId, DateTime dischargeDate)
        {
            bool result = false;
            MedicationProfile medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingMedications.Count > 0)
                {
                    var logAction = new LogAction();
                    existingMedications.ForEach((Medication medication) =>
                        {
                            if (medication.MedicationCategory == MedicationCategoryEnum.Active.ToString())
                            {
                                medication.DCDate = dischargeDate;
                                medication.LastChangedDate = DateTime.Now;
                                medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                                logAction = LogAction.MedicationDischarged;
                            }
                        });
                    medicationProfile.Medication = existingMedications.ToXml();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid patientId)
        {
            var profile = new MedicationProfileSnapshotViewData();
            MedicationProfile med = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (med != null)
            {
                profile.MedicationProfile = med;

                AllergyProfile allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    profile.Allergies = allergyProfile.ToString();
                }
                profile.PatientProfile = basePatientProfileRepository.GetPatientPrintProfileWithPharmacy(Current.AgencyId, patientId);

                if (profile.PatientProfile != null)
                {
                    AgencyPhysician physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        profile.PhysicianName = physician.DisplayName;
                    }
                    profile.PharmacyName = med.PharmacyName.IsNotNullOrEmpty() ? med.PharmacyName : profile.PatientProfile.PharmacyName;
                    profile.PharmacyPhone = med.PharmacyPhone.IsNotNullOrEmpty() ? med.PharmacyPhone : profile.PatientProfile.PharmacyPhone;
                    profile.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.PatientProfile.AgencyLocationId);
                }
                else
                {
                    profile.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                }
            }
            return profile;
        }

        public MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid Id)
        {
            var viewData = new MedicationProfileSnapshotViewData();
            MedicationProfileHistory snapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (snapShot != null)
            {
                viewData.MedicationProfile = snapShot.ToProfile();

                viewData.Allergies = snapShot.Allergies;
                viewData.PrimaryDiagnosis = snapShot.PrimaryDiagnosis;
                viewData.SecondaryDiagnosis = snapShot.SecondaryDiagnosis;
                AgencyPhysician physician;
                if (snapShot.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = snapShot.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        viewData.PhysicianName = physician.DisplayName;
                    }
                    else
                    {
                        if (!snapShot.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                viewData.PhysicianName = physician.DisplayName;
                            }
                        }
                    }
                }
                else
                {
                    if (!snapShot.PhysicianId.IsEmpty())
                    {
                        physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            viewData.PhysicianName = physician.DisplayName;
                        }
                    }
                }

                if (snapShot.SignatureText.IsNotNullOrEmpty())
                {
                    viewData.SignatureText = snapShot.SignatureText;
                    viewData.SignatureDate = snapShot.SignedDate;
                }
                else
                {
                    if (!snapShot.UserId.IsEmpty())
                    {
                        string displayName = UserEngine.GetName(snapShot.UserId, Current.AgencyId);
                        if (displayName.IsNotNullOrEmpty())
                        {
                            viewData.SignatureText = string.Format("Electronically Signed by: {0}", displayName);
                            viewData.SignatureDate = snapShot.SignedDate;
                        }
                    }
                }
                viewData.PatientProfile = basePatientProfileRepository.GetPatientPrintProfileWithPharmacy(Current.AgencyId, snapShot.PatientId);
                if (viewData.PatientProfile != null)
                {
                    viewData.PharmacyName = snapShot.PharmacyName.IsNotNullOrEmpty() ? snapShot.PharmacyName : viewData.PatientProfile.PharmacyName;
                    viewData.PharmacyPhone = snapShot.PharmacyPhone.IsNotNullOrEmpty() ? snapShot.PharmacyPhone : viewData.PatientProfile.PharmacyPhone;
                    viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
                }
                else
                {
                    viewData.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                }
                viewData.EpisodeId = snapShot.EpisodeId;

                if (!viewData.EpisodeId.IsEmpty() && !snapShot.PatientId.IsEmpty())
                {
                    DateRange episode = GetEpisodeDateRange(snapShot.PatientId, viewData.EpisodeId);
                    if (episode != null)
                    {
                        viewData.StartDate = episode.StartDate;
                        viewData.EndDate = episode.EndDate;
                    }
                }
            }
            return viewData;
        }

        public HospitalizationLog GetHospitalizationLogPrint(Guid patientId, Guid hospitalizationLogId)
        {
            HospitalizationLog log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (log != null)
            {
                Profile profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, log.PatientId);
                if (profile != null)
                {
                    log.PatientId = profile.Id;
                    log.PatientName = profile.DisplayName;
                    log.Location = this.agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
                }
                else
                {
                    log.Location = this.agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                }
                User user = UserEngine.GetUser(log.UserId, Current.AgencyId);
                if (user != null)
                {
                    log.UserName = user.DisplayName;
                }
                DateRange dateRange = GetEpisodeDateRange(log.PatientId, log.EpisodeId);
                if (dateRange != null)
                {
                    log.EpisodeRange = string.Format("{0:MM/dd/yyyy} - {1:MM/dd/yyyy}", dateRange.StartDate, dateRange.EndDate);
                }
            }
            return log;
        }

        public AuthorizationPdf GetAuthorizationPdf(Guid patientId, Guid id)
        {
            Authorization auth = basePatientProfileRepository.GetAuthorization(Current.AgencyId, patientId, id);
            var location = new LocationPrintProfile();
            Patient patientWithProfile = basePatientProfileRepository.GetPatientWithProfile(Current.AgencyId, patientId);
            location = patientWithProfile != null ? this.agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, patientWithProfile.Profile.AgencyLocationId) : this.agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
            if (auth != null)
            {
                if (!auth.AgencyLocationId.IsEmpty())
                {
                    if (location != null && patientWithProfile != null && patientWithProfile.Profile.AgencyLocationId == auth.AgencyLocationId)
                    {
                        auth.Branch = location.Name;
                    }
                    else
                    {
                        AgencyLocation branch = agencyRepository.FindLocation(Current.AgencyId, auth.AgencyLocationId);
                        if (branch != null)
                        {
                            auth.Branch = branch.Name;
                        }
                    }
                }
                if (auth.InsuranceId > 0)
                {
                    auth.InsuranceName = InsuranceEngine.GetName(auth.InsuranceId, Current.AgencyId);
                }
            }
            return new AuthorizationPdf(auth, patientWithProfile, location);
        }

        public object GetPatientSelectionList(Guid branchId, int status)
        {
            return basePatientProfileRepository.Find(Current.AgencyId, branchId, status).OrderBy(s => s.DisplayName.ToUpperCase()).Select(p => new {p.Id, Name = p.DisplayName}).ToList();
        }

        public MedicationProfileViewData GetMedicationProfileViewDataWithOutDiagnosis(Guid patientId)
        {
            var viewData = new MedicationProfileViewData {PatientId = patientId};
            MedicationProfile medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                viewData.MedicationProfile = medicationProfile;
                viewData.AllergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                PatientProfileLean patient = basePatientProfileRepository.GetPatientPrintProfileWithPharmacy(Current.AgencyId, patientId);
                if (patient != null)
                {
                    viewData.DisplayName = patient.DisplayName;
                    viewData.PharmacyName = medicationProfile.PharmacyName.IsNotNullOrEmpty() ? medicationProfile.PharmacyName : patient.PharmacyName;
                    viewData.PharmacyPhone = medicationProfile.PharmacyPhone.IsNotNullOrEmpty() ? medicationProfile.PharmacyPhone : patient.PharmacyPhone;
                }
            }
            return viewData;
        }

        public MedicationProfileViewData GetMedicationProfileViewDataWithOutDiagnosisForSign(Guid patientId)
        {
            var viewData = new MedicationProfileViewData {PatientId = patientId};
            MedicationProfile medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                viewData.MedicationProfile = medicationProfile;
                viewData.AllergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                PatientProfileLean patient = basePatientProfileRepository.GetPatientPrintProfileWithPharmacy(Current.AgencyId, patientId);
                if (patient != null)
                {
                    viewData.DisplayName = patient.DisplayName;
                    AgencyPhysician physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                    }
                    viewData.PharmacyName = patient.PharmacyName;
                    viewData.PharmacyPhone = patient.PharmacyPhone;
                }
            }
            return viewData;
        }

        public Dictionary<string, string> GetProfileJsonByColumns(Guid id, params string[] columns)
        {
            return basePatientProfileRepository.GetProfileJsonByColumns(Current.AgencyId, id, columns);
        }

        protected abstract void UpdateEpisodeAndClaimsForDischargePatientAppSpecific(Guid patientId, DateTime dischargeDate, Guid episodeId);

        protected abstract void EditValidateAppSpecific(Patient patient, Profile profile, List<Validation> rules);

        protected abstract void EditValidateAppSpecific(Profile profile, List<Validation> rules);


        protected abstract DateRange GetEpisodeDateRange(Guid patientId, Guid episodeId);

        protected abstract void SetStatus(Patient patient, Profile profile);

        protected abstract int GetStatus(Patient patient);

        protected abstract bool AddPatientAdmissionDateAppSpecific(PatientAdmissionDate admissionData);

        protected abstract bool UpdatePatientAdmissionDateAppSpecific(PatientAdmissionDate admissionData);

        protected abstract PatientAdmissionDate GetPatientAdmissionDateAppSpecific(Guid patientId, Guid admissionDataId);

        private List<PatientSelection> GetPatientSelection(List<Guid> branchIds, byte statusId, byte paymentSourceId, string name, bool checkAuditor)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = basePatientProfileRepository.GetPatientSelectionPaymentSource(Current.AgencyId, branchIds, statusId, paymentSourceId, name);
            }
            else if (Current.IsClinicianOrHHA)
            {
                GetPatientSelectionAppSpecific(branchIds, statusId);
            }
            else if (checkAuditor && Current.IfOnlyRole(AgencyRoles.Auditor))
            {
                patientList = basePatientProfileRepository.GetAuditorPatientSelectionPaymentSource(Current.AgencyId, branchIds, statusId, paymentSourceId, name, Current.UserId);
                if (patientList == null)
                {
                    patientList = new List<PatientSelection>();
                }
            }
            else
            {
            }

            return patientList;
        }


        public List<PatientSelection> GetPatientSelection(Guid branchId, byte statusId, byte paymentSourceId, string name, bool checkAuditor)
        {
            var patientList = new List<PatientSelection>();
            var locations = new List<Guid>();
            if (branchId.IsEmpty())
            {
                locations = UserSessionEngine.LocationIds(Current.AgencyId, Current.UserId, Current.AcessibleServices, this.Service, Current.SessionId);
            }
            else
            {
                locations.Add(branchId);
            }
            if (locations.Count > 0)
            {
                patientList = GetPatientSelection(locations, statusId, paymentSourceId, name, checkAuditor);
            }
            return patientList;
        }

        private List<PatientSelection> GetPatientSelectionByInsurance(List<Guid> branchIds, byte statusId, int insuranceId, string name, bool checkAuditor)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = basePatientProfileRepository.GetPatientSelectionInsurance(Current.AgencyId, branchIds, statusId, insuranceId, name);
            }
            else if (Current.IsClinicianOrHHA)
            {
                GetPatientSelectionAppSpecific(branchIds, statusId);
            }
            else if (checkAuditor && Current.IfOnlyRole(AgencyRoles.Auditor))
            {
                patientList = basePatientProfileRepository.GetAuditorPatientSelectionInsurance(Current.AgencyId, branchIds, statusId, insuranceId, name, Current.UserId);
                if (patientList == null)
                {
                    patientList = new List<PatientSelection>();
                }
            }
            else
            {
            }

            return patientList;
        }

        public List<PatientSelection> GetPatientSelectionByInsurance(Guid branchId, byte statusId, int insuranceId, string name, bool checkAuditor)
        {
            var patientList = new List<PatientSelection>();
             var locations = new List<Guid>();
            if (branchId.IsEmpty())
            {
                locations = UserSessionEngine.LocationIds(Current.AgencyId, Current.UserId, Current.AcessibleServices, this.Service, Current.SessionId);
            }
            else
            {
                locations.Add(branchId);
            }
            if (locations.Count > 0)
            {
                patientList = GetPatientSelectionByInsurance(locations, statusId, insuranceId, name, checkAuditor);
            }
            return patientList;
        }

      
        public PatientSelectionViewData GetPatientChartsDataWithOutSchedules(Guid patientId, int status, ref  Profile profile)
        {
            var viewData = new PatientSelectionViewData();
            var patients = GetPatientSelection(patientId, status, ref profile);
            if (patients.IsNotNullOrEmpty())
            {
                viewData.Count = patients.Count;
                viewData.Patients = patients;
                var currentPatientId = profile != null ? profile.Id : Guid.Empty;
                if (profile != null && !currentPatientId.IsEmpty() && patients.Exists(p => p.Id == currentPatientId))
                {
                    viewData.CurrentPatientId = profile.Id;
                    viewData.PatientListStatus = profile.Status;
                    viewData.IsDischarged = profile.IsDischarged;
                }
                else
                {
                    var patientForID = patients.FirstOrDefault();
                    if (patientForID != null && !patientForID.Id.IsEmpty())
                    {
                        profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientForID.Id);
                        if (profile != null)
                        {
                            viewData.CurrentPatientId = patientForID.Id;
                            viewData.IsDischarged = profile.IsDischarged;

                        }
                    }
                    viewData.PatientListStatus = status;
                }
            }
            else
            {
                viewData.PatientListStatus = status;
                viewData.Count = 0;
            }
            return viewData;
        }

        public PatientSelectionViewData GetScheduleCenterDataWithOutSchedules(Guid patientId, int status)
        {
            var viewData = new PatientSelectionViewData();
            Profile profile = null;
            var patients = GetPatientSelection(patientId, status, ref profile);
            if (patients.IsNotNullOrEmpty())
            {
                viewData.Count = patients.Count;
                viewData.Patients = patients;
                if (profile != null && patients.Exists(p => p.Id == profile.Id))
                {
                    // viewData.CalendarData = scheduleService.GetScheduleWithPreviousAfterEpisodeInfo(patient.Id, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();

                    viewData.CurrentPatientId = profile.Id;
                    viewData.IsDischarged = profile.IsDischarged;
                    viewData.PatientListStatus = profile.Status;
                    viewData.DisplayName = string.Format("{0}, {1}", profile.LastName.ToUpperCase(), profile.FirstName.ToUpperCase());
                }
                else
                {
                    var patientForID = patients.FirstOrDefault();
                    if (patientForID != null && !patientForID.Id.IsEmpty())
                    {
                        // viewData.CalendarData = scheduleService.GetScheduleWithPreviousAfterEpisodeInfo(patientForID.Id, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();
                        viewData.CurrentPatientId = patientForID.Id;
                        viewData.IsDischarged = patientForID.IsDischarged;
                        viewData.DisplayName = string.Format("{0}, {1}", patientForID.LastName.ToUpperCase(), patientForID.FirstName.ToUpperCase());

                    }
                    viewData.PatientListStatus = status;
                }
            }
            else
            {
                viewData.PatientListStatus = status;
                viewData.Count = 0;
            }
            return viewData;
        }

        private List<PatientSelection> GetPatientSelection(Guid patientId, int status, ref Profile profile)
        {
            var patients = new List<PatientSelection>();
            var locations = UserSessionEngine.LocationIds(Current.AgencyId, Current.UserId, Current.AcessibleServices, this.Service, Current.SessionId);
            if (locations.Count > 0)
            {
                if (!patientId.IsEmpty())
                {
                    profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                    if (profile != null && PatientStatusFactory.CenterStatus().Contains(profile.Status))
                    {
                        status = profile.Status;
                    }
                }

                patients = this.GetPatientSelection(locations, (byte)status, 0, "", false) ?? new List<PatientSelection>();

                //var patientsAccess = basePatientProfileRepository.GetPatientsWithUserAccess(Current.UserId, Current.AgencyId, Guid.Empty, status, 0, "");
                //if (patientsAccess.IsNotNullOrEmpty())
                //{
                //    foreach (var p in patientsAccess)
                //    {
                //        var search = patients.Find(pa => pa.DisplayName == p.DisplayName);
                //        if (search == null)
                //        {
                //            patients.Add(p);
                //        }
                //        search = null;
                //    }
                //}
            }

            return patients;
        }

        protected abstract List<PatientSelection> GetPatientSelectionAppSpecific(List<Guid> branchIds, byte statusId);

        public JsonViewData AddInsurance(PatientInsurance patientInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to save the patient insurance. Try again." };
            var agencyId = Current.AgencyId;
            var admission = patientAdmissionRepository.GetPatientAdmissionDate(agencyId, patientInsurance.PatientId, patientInsurance.AdmissionId);
            if (admission != null)
            {
                var rules = new List<Validation>();
                if (patientInsurance.IsApplicableRange)
                {
                    rules.Add(new Validation(() => !patientInsurance.ActualStartDate.IsValid(), "A valid start date required."));
                    if (patientInsurance.IsEndDateSet)
                    {
                        rules.Add(new Validation(() => !patientInsurance.ActualEndDate.IsValid(), "A valid end date required."));
                        rules.Add(new Validation(() => !(patientInsurance.ActualStartDate < patientInsurance.ActualEndDate), "End date has to be greater than start date."));
                        rules.Add(new Validation(() => !(patientInsurance.ActualEndDate.Date >  admission.StartOfCareDate.Date), "The end date has to be greater than the addmission date"));
                        if (!admission.IsActive)
                        {
                            rules.Add(new Validation(() => !admission.DischargedDate.IsValid(), "The admission discharge date is not valid. Put a valid discharge date before you add this payor."));
                            rules.Add(new Validation(() => !(admission.StartOfCareDate.Date < admission.DischargedDate), "The admission start of care date needs to be greater than discharge date . Put a valid  start of care and discharge date before you add this payor."));
                            rules.Add(new Validation(() => !(patientInsurance.StartDate.Date <= admission.DischargedDate.Date), "The satrt date has to be less than the addmission discharge date"));
                        }
                    }
                }
                var isClearToSave = false;
                if (rules.IsNotNullOrEmpty())
                {
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        isClearToSave = true;
                    }
                    else
                    {
                        isClearToSave = false;
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
                else
                {
                    isClearToSave = true;
                }
                if (isClearToSave)
                {
                    var insuranceIds = string.Empty;
                    if (admission.InsuranceIds.IsNullOrEmpty())
                    {
                        insuranceIds = patientInsurance.InsuranceId.ToString();
                    }
                    else
                    {
                        var ids = admission.InsuranceIds.Split(',').ToList();
                        ids.Add(patientInsurance.InsuranceId.ToString());
                        insuranceIds = ids.ToArray().Join(",");
                    }
                    patientInsurance.AgencyId = agencyId;

                    if (patientInsurance.IsApplicableRange)
                    {
                        if (patientInsurance.ActualStartDate.Date < admission.StartOfCareDate.Date)
                        {
                            patientInsurance.StartDate = admission.StartOfCareDate;
                            patientInsurance.IsOpenStartOfAdmission = true;
                        }
                        else
                        {
                            patientInsurance.IsOpenStartOfAdmission = false;
                            patientInsurance.StartDate = patientInsurance.ActualStartDate.Date;
                        }
                        if (!admission.IsActive)
                        {
                            if (patientInsurance.IsEndDateSet)
                            {
                                if (patientInsurance.ActualEndDate.Date <= admission.DischargedDate.Date)
                                {
                                    patientInsurance.EndDate = patientInsurance.ActualEndDate;
                                    patientInsurance.IsOpenEndOfAdmission = false;
                                }
                                else
                                {
                                    patientInsurance.EndDate = admission.DischargedDate;
                                    patientInsurance.IsOpenEndOfAdmission = true;
                                }
                            }
                            else
                            {
                                patientInsurance.EndDate = admission.DischargedDate;
                                patientInsurance.IsOpenEndOfAdmission = true;
                            }

                        }
                        else
                        {
                            if (patientInsurance.IsEndDateSet)
                            {
                                patientInsurance.EndDate = patientInsurance.ActualEndDate;
                                patientInsurance.IsOpenEndOfAdmission = false;
                            }
                            else
                            {
                                patientInsurance.IsOpenEndOfAdmission = true;
                            }
                        }
                    }
                    else
                    {
                        patientInsurance.StartDate = admission.StartOfCareDate;
                        if (!admission.IsActive)
                        {
                            patientInsurance.EndDate = admission.DischargedDate;
                        }
                        patientInsurance.IsOpenEndOfAdmission = true;
                        patientInsurance.IsOpenStartOfAdmission = true;
                    }
                    if (!basePatientProfileRepository.IsPatientInsuranceExist(agencyId, patientInsurance.PatientId, patientInsurance.AdmissionId, patientInsurance.InsuranceId, patientInsurance.Type, patientInsurance.StartDate, patientInsurance.EndDate,patientInsurance.Id))
                    {

                        if (basePatientProfileRepository.AddInsurance(patientInsurance))
                        {
                            if (patientAdmissionRepository.UpdateAdmissionForInsuranceIds(agencyId, patientInsurance.PatientId, patientInsurance.AdmissionId, insuranceIds))
                            {
                                if (!admission.IsActive)
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Patient Insurance added successfully.";
                                }
                                else
                                {
                                    if (basePatientProfileRepository.UpdatePatientForInsuranceIds(agencyId, patientInsurance.PatientId, insuranceIds))
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "Patient Insurance added successfully.";
                                    }
                                    else
                                    {
                                        patientAdmissionRepository.UpdateAdmissionForInsuranceIds(agencyId, patientInsurance.PatientId, patientInsurance.AdmissionId, admission.InsuranceIds);
                                        basePatientProfileRepository.RemoveInsurance(patientInsurance.Id);
                                    }
                                }

                            }
                            else
                            {
                                basePatientProfileRepository.RemoveInsurance(patientInsurance.Id);
                            }
                        }
                    }
                    else
                    {
                        var type = string.Empty;
                        if (Enum.IsDefined(typeof(InsuranceType), patientInsurance.Type))
                        {
                            type = ((InsuranceType)patientInsurance.Type).ToString();
                        }
                        viewData.isSuccessful = false;
                        viewData.errorMessage = string.Format("There is an overlaping payor for this payor for a particular insurance type {0}.", type.IsNotNullOrEmpty() ? "(" + type + ")" : string.Empty);
                    }
                }
            }
            return viewData;
        }

        public JsonViewData EditInsurance(PatientInsurance patientInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to update the patient insurance. Try again." };
            var agencyId = Current.AgencyId;
            var patientInsuranceToEdit = basePatientProfileRepository.GetPatientInsuranceOnly(agencyId, patientInsurance.PatientId, patientInsurance.Id);
            if (patientInsuranceToEdit != null)
            {
                var admission = patientAdmissionRepository.GetPatientAdmissionDate(agencyId, patientInsurance.PatientId, patientInsurance.AdmissionId);
                if (admission != null)
                {
                    var result = ValidatePatientInsurance(patientInsurance,admission);
                    if (result.isSuccessful)
                    {
                        SetPatientInsuranceInput(patientInsurance, admission);

                        patientInsuranceToEdit.Type = patientInsurance.Type;
                        patientInsuranceToEdit.HealthPlanId = patientInsurance.HealthPlanId;
                        patientInsuranceToEdit.GroupName = patientInsurance.GroupName;
                        patientInsuranceToEdit.GroupId = patientInsurance.GroupId;
                        patientInsuranceToEdit.RelationId = patientInsurance.RelationId;
                        patientInsuranceToEdit.ActualEndDate = patientInsurance.ActualEndDate;
                        patientInsuranceToEdit.ActualStartDate = patientInsurance.ActualStartDate;
                        patientInsuranceToEdit.EndDate = patientInsurance.EndDate;
                        patientInsuranceToEdit.StartDate = patientInsurance.StartDate;
                        patientInsuranceToEdit.IsOpenEndOfAdmission = patientInsurance.IsOpenEndOfAdmission;
                        patientInsuranceToEdit.IsOpenStartOfAdmission = patientInsurance.IsOpenStartOfAdmission;

                        if (!basePatientProfileRepository.IsPatientInsuranceExist(agencyId, patientInsuranceToEdit.PatientId, patientInsuranceToEdit.AdmissionId, patientInsuranceToEdit.InsuranceId, patientInsuranceToEdit.Type, patientInsuranceToEdit.StartDate, patientInsuranceToEdit.EndDate, patientInsuranceToEdit.Id))
                        {
                            if (basePatientProfileRepository.UpdateInsuranceModel(patientInsuranceToEdit))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Patient Insurance update successfully.";
                            }
                        }
                        else
                        {
                            var type = string.Empty;
                            if (Enum.IsDefined(typeof(InsuranceType), patientInsurance.Type))
                            {
                                type = ((InsuranceType)patientInsurance.Type).ToString();
                            }
                            viewData.isSuccessful = false;
                            viewData.errorMessage = string.Format("There is an overlaping payor for this payor for a particular insurance type {0}.", type.IsNotNullOrEmpty() ? "(" + type + ")" : string.Empty);
                        }
                    }
                    else
                    {
                        viewData.errorMessage = result.errorMessage;
                    }
                }
            }
            return viewData;
        }

        private void SetPatientInsuranceInput(PatientInsurance patientInsurance, PatientAdmissionDate admission)
        {
            if (patientInsurance.IsApplicableRange)
            {
                if (patientInsurance.ActualStartDate.Date < admission.StartOfCareDate.Date)
                {
                    patientInsurance.StartDate = admission.StartOfCareDate;
                    patientInsurance.IsOpenStartOfAdmission = true;
                }
                else
                {
                    patientInsurance.StartDate = patientInsurance.ActualStartDate.Date;
                    patientInsurance.IsOpenStartOfAdmission = false;
                }
                if (!admission.IsActive)
                {
                    if (patientInsurance.IsEndDateSet)
                    {
                        if (patientInsurance.ActualEndDate.Date <= admission.DischargedDate.Date)
                        {
                            patientInsurance.EndDate = patientInsurance.ActualEndDate;
                            patientInsurance.IsOpenEndOfAdmission = false;
                        }
                        else
                        {
                            patientInsurance.EndDate = admission.DischargedDate;
                            patientInsurance.IsOpenEndOfAdmission = true;
                        }
                    }
                    else
                    {
                        patientInsurance.EndDate = admission.DischargedDate;
                        patientInsurance.IsOpenEndOfAdmission = true;
                    }

                }
                else
                {
                    if (patientInsurance.IsEndDateSet)
                    {
                        patientInsurance.EndDate = patientInsurance.ActualEndDate;
                        patientInsurance.IsOpenEndOfAdmission = false;
                    }
                    else
                    {
                        patientInsurance.IsOpenEndOfAdmission = true;
                    }
                }
            }
            else
            {
                patientInsurance.StartDate = admission.StartOfCareDate;
                if (!admission.IsActive)
                {
                    patientInsurance.EndDate = admission.DischargedDate;
                }
                else
                {
                    patientInsurance.EndDate = DateTime.MaxValue;
                }
                patientInsurance.IsOpenEndOfAdmission = true;
                patientInsurance.IsOpenStartOfAdmission = true;
            }
        }

        private JsonViewData ValidatePatientInsurance(PatientInsurance patientInsurance, PatientAdmissionDate admission)
        {
            var viewData = new JsonViewData();
            var rules = new List<Validation>();
            if (patientInsurance.IsApplicableRange)
            {
                rules.Add(new Validation(() => !patientInsurance.ActualStartDate.IsValid(), "A valid start date required."));
                if (patientInsurance.IsEndDateSet)
                {
                    rules.Add(new Validation(() => !patientInsurance.ActualEndDate.IsValid(), "A valid end date required."));
                    rules.Add(new Validation(() => !(patientInsurance.ActualStartDate < patientInsurance.ActualEndDate), "End date has to be greater than start date."));
                    rules.Add(new Validation(() => !(patientInsurance.ActualEndDate.Date > admission.StartOfCareDate.Date), "The end date has to be greater than the addmission date"));
                    if (!admission.IsActive)
                    {
                        rules.Add(new Validation(() => !admission.DischargedDate.IsValid(), "The admission discharge date is not valid. Put a valid discharge date before you add this payor."));
                        rules.Add(new Validation(() => !(admission.StartOfCareDate.Date < admission.DischargedDate), "The admission start of care date needs to be greater than discharge date . Put a valid  start of care and discharge date before you add this payor."));
                        rules.Add(new Validation(() => !(patientInsurance.StartDate.Date <= admission.DischargedDate.Date), "The satrt date has to be less than the addmission discharge date"));
                    }
                }
            }
            if (rules.IsNotNullOrEmpty())
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    viewData.isSuccessful = true;
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            else
            {
                viewData.isSuccessful = true;
            }
            return viewData;
        }

        //public bool EditInsurance(PatientInsurance patientInsurance)
        //{
        //    var result = false;
        //    var patientInsuranceToEdit = basePatientProfileRepository.GetPatientInsuranceOnly(Current.AgencyId, patientInsurance.PatientId, patientInsurance.Id);
        //    if (patientInsuranceToEdit != null)
        //    {
        //        patientInsuranceToEdit.Type = patientInsurance.Type;
        //        patientInsuranceToEdit.HealthPlanId = patientInsurance.HealthPlanId;
        //        patientInsuranceToEdit.GroupName = patientInsurance.GroupName;
        //        patientInsuranceToEdit.GroupId = patientInsurance.GroupId;
        //        patientInsuranceToEdit.RelationId = patientInsurance.RelationId;
        //        patientInsuranceToEdit.StartDate = patientInsurance.StartDate;
        //        if (basePatientProfileRepository.UpdateInsuranceModel(patientInsuranceToEdit))
        //        {
        //            return true;
        //        }
        //    }

        //    return result;
        //}
        /// <summary>
        /// IsDelete:  set true for deleting, false to restore
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="admissionId"></param>
        /// <param name="Id"></param>
        /// <param name="IsRestore"></param>
        /// <returns></returns>
        public JsonViewData DeleteOrRestoreInsurance(Guid patientId, Guid admissionId, int Id, bool IsDelete)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("Unable to {0} the patient insurance. Try again.", IsDelete ? "delete" : "restore") };
            var agencyId = Current.AgencyId;
            var patientInsuranceToDeleteOrRestore = basePatientProfileRepository.GetPatientInsuranceOnly(agencyId, patientId, Id);
            if (patientInsuranceToDeleteOrRestore != null)
            {
                if (!IsDelete)
                {
                    if (basePatientProfileRepository.IsPatientInsuranceExist(agencyId, patientInsuranceToDeleteOrRestore.PatientId, patientInsuranceToDeleteOrRestore.AdmissionId, patientInsuranceToDeleteOrRestore.InsuranceId, patientInsuranceToDeleteOrRestore.Type, patientInsuranceToDeleteOrRestore.StartDate, patientInsuranceToDeleteOrRestore.EndDate, patientInsuranceToDeleteOrRestore.Id))
                    {
                        var type = string.Empty;
                        if (Enum.IsDefined(typeof(InsuranceType), patientInsuranceToDeleteOrRestore.Type))
                        {
                            type = ((InsuranceType)patientInsuranceToDeleteOrRestore.Type).ToString();
                        }
                        viewData.isSuccessful = false;
                        viewData.errorMessage = string.Format("There is an overlaping payor for this payor for a particular insurance type {0}.", type.IsNotNullOrEmpty() ? "(" + type + ")" : string.Empty);
                    }
                }

                var oldStatus = patientInsuranceToDeleteOrRestore.IsDeprecated;
                patientInsuranceToDeleteOrRestore.IsDeprecated = IsDelete;

                var admission = patientAdmissionRepository.GetPatientAdmissionDate(Current.AgencyId, patientInsuranceToDeleteOrRestore.PatientId, patientInsuranceToDeleteOrRestore.AdmissionId);
                if (admission != null)
                {
                    var insuranceIds = string.Empty;
                    var ids = new List<string>();
                    if (admission.InsuranceIds.IsNotNullOrEmpty())
                    {
                        ids = admission.InsuranceIds.Split(',').ToList() ?? new List<string>();
                    }
                    if (IsDelete)
                    {
                        ids.Remove(patientInsuranceToDeleteOrRestore.InsuranceId.ToString());
                    }
                    else
                    {
                        ids.Add(patientInsuranceToDeleteOrRestore.InsuranceId.ToString());
                    }

                    insuranceIds = ids.ToArray().Join(",");

                    if (basePatientProfileRepository.UpdateInsuranceModel(patientInsuranceToDeleteOrRestore))
                    {
                        if (patientAdmissionRepository.UpdateAdmissionForInsuranceIds(Current.AgencyId, patientInsuranceToDeleteOrRestore.PatientId, patientInsuranceToDeleteOrRestore.AdmissionId, insuranceIds))
                        {
                            if (!admission.IsActive)
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = string.Format("Patient Insurance successfully {0}.", IsDelete ? "delete" : "restore");
                            }
                            else
                            {
                                if (basePatientProfileRepository.UpdatePatientForInsuranceIds(Current.AgencyId, patientInsuranceToDeleteOrRestore.PatientId, insuranceIds))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = string.Format("Patient Insurance successfully {0}.", IsDelete ? "delete" : "restore");
                                }
                                else
                                {
                                    patientAdmissionRepository.UpdateAdmissionForInsuranceIds(Current.AgencyId, patientInsuranceToDeleteOrRestore.PatientId, patientInsuranceToDeleteOrRestore.AdmissionId, admission.InsuranceIds);
                                    patientInsuranceToDeleteOrRestore.IsDeprecated = oldStatus;
                                    basePatientProfileRepository.UpdateInsuranceModel(patientInsuranceToDeleteOrRestore);
                                }
                            }

                        }
                        else
                        {
                            patientInsuranceToDeleteOrRestore.IsDeprecated = oldStatus;
                            basePatientProfileRepository.UpdateInsuranceModel(patientInsuranceToDeleteOrRestore);
                        }
                    }
                }

            }
            return viewData;
        }

        public PatientInsurance GetPatientInsuranceOnly(Guid patientId, int Id)
        {
            var insurance = basePatientProfileRepository.GetPatientInsuranceOnly(Current.AgencyId, patientId, Id) ?? new PatientInsurance();
            insurance.Service = this.Service;
            return insurance;
        }

        public PatientInsurance GetPatientInsuranceWithName(Guid patientId, int Id)
        {
            var insurance = basePatientProfileRepository.GetPatientInsuranceOnly(Current.AgencyId, patientId, Id) ?? new PatientInsurance();
            if (insurance.InsuranceId == 18)
            {
                insurance.Name = "Private Payor";
            }
            else
            {
                insurance.Name = InsuranceEngine.GetName(insurance.InsuranceId, Current.AgencyId);
            }
            insurance.Service = this.Service;
            return insurance;
        }

        public List<PatientInsurance> GetPatientInsurances(Guid patientId, Guid admissionId)
        {
            var insurances = new List<PatientInsurance>();
            var insuranceIds = basePatientProfileRepository.GetPatientInsuranceIds(Current.AgencyId, patientId);
            insurances = basePatientProfileRepository.GetPatientInsurances(Current.AgencyId, patientId, admissionId);
            if (insurances.IsNotNullOrEmpty())
            {
                var ids = insurances.Where(i => i.Id != 18).Select(i => i.InsuranceId).Distinct().ToList();
                var insuranceData = InsuranceEngine.GetInsurances(Current.AgencyId, ids);
                insurances.ForEach(i =>
                    {
                        if (i.InsuranceId == 18)
                        {
                            i.Name = "Private Payor";
                        }
                        else
                        {
                            var insurance = insuranceData.FirstOrDefault(ic => ic.Id == i.InsuranceId);
                            if (insurance != null)
                            {
                                i.Name = insurance.Name.ToUpperCase();
                            }
                        }
                        i.IsOpenEndOfAdmission = i.IsOpenEndOfAdmission && insuranceIds.Contains(i.InsuranceId.ToString());

                    });
            }
            return insurances;
        }

        public List<PatientInsuranceViewData> GetPatientInsurancesViewData(Guid patientId, Guid admissionId)
        {
            var insurances = new List<PatientInsuranceViewData>();
           // var insuranceIds = basePatientProfileRepository.GetPatientInsuranceIds(Current.AgencyId, patientId);
            var patientInsurances = basePatientProfileRepository.GetPatientInsurances(Current.AgencyId, patientId, admissionId);
            if (patientInsurances.IsNotNullOrEmpty())
            {
                insurances = patientInsurances.GroupBy(i => i.InsuranceId).Select(g => new PatientInsuranceViewData { InsuranceId = g.Key, Insurances = g.ToList() }).ToList();
            }
            return insurances;
        }

        public List<PatientInsurance> GetPatientTaskInsurances(Guid patientId, Guid admissionId, DateTime startDate, DateTime endDate)
        {
            var insurances = basePatientProfileRepository.GetPatientInsurancesLean(Current.AgencyId, patientId, admissionId, startDate, endDate);
            if (insurances.IsNotNullOrEmpty())
            {
                var insuranceNames = InsuranceEngine.GetInsurances(Current.AgencyId, insurances.Select(i => i.InsuranceId).Distinct().ToList());
                insurances.ForEach(i =>
                {
                    var insurance = insuranceNames.FirstOrDefault(it => it.Id == i.InsuranceId);
                    if (insurance != null)
                    {
                        i.Name = insurance.Name;
                    }
                });
            }
            return insurances;
        }


        #region Authorization

        public SingleServiceGridViewData GetAuthorizationPermission(Guid patientId)
        {
            var viewdata = new SingleServiceGridViewData { Service = this.Service };
            viewdata.DisplayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
            viewdata.Id = patientId;
            var permission = Current.Permissions;
            var insurancePermission = permission.GetCategoryPermission(ParentPermission.Insurance);
            viewdata.IsUserCanDelete = insurancePermission.IsInPermission(this.Service, PermissionActions.Delete);
            viewdata.IsUserCanEdit = insurancePermission.IsInPermission(this.Service, PermissionActions.Edit);
            viewdata.IsUserCanAdd = insurancePermission.IsInPermission(this.Service, PermissionActions.Add);
            viewdata.IsUserCanPrint = insurancePermission.IsInPermission(this.Service, PermissionActions.Print);
            viewdata.IsUserCanExport = insurancePermission.IsInPermission(this.Service, PermissionActions.Export);
            return viewdata;
        }

        public JsonViewData AddAuthorization(AAE.Authorization authorization)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New authorization could not be saved." };
            if (authorization.IsValid())
            {
                authorization.UserId = Current.UserId;
                authorization.AgencyId = Current.AgencyId;
                authorization.Id = Guid.NewGuid();
                if (basePatientProfileRepository.AddAuthorization(authorization))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was saved successfully.";
                    viewData.PatientId = authorization.PatientId;
                    viewData.ServiceId = (int)authorization.Service;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = authorization.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData EditAuthorization(AAE.Authorization authorization)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Edit authorization could not be saved." };

            if (authorization.IsValid())
            {
                authorization.AgencyId = Current.AgencyId;
                if (basePatientProfileRepository.EditAuthorization(authorization))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was saved successfully.";
                    viewData.PatientId = authorization.PatientId;
                    viewData.ServiceId = (int)authorization.Service;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = authorization.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData DeleteAuthorization( Guid patientId,Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Delete authorization is not Successful.", Service = (this.Service).ToString(), ServiceId = (int)this.Service };
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                if (basePatientProfileRepository.DeleteAuthorization(Current.AgencyId, patientId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Authorization, LogAction.AuthorizationDeleted, string.Empty);
                    viewData.PatientId = patientId;
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was deleted successfully.";
                }
            }
            return viewData;
        }

        public AAE.Authorization GetAuthorization(Guid authorizationId)
        {
            return basePatientProfileRepository.GetAuthorization(Current.AgencyId, authorizationId);
        }

        public AAE.Authorization GetAuthorizationWithPatientName(Guid patientId, Guid Id)
        {
            var auth = basePatientProfileRepository.GetAuthorization(Current.AgencyId, patientId, Id);
            if (auth != null)
            {
                var patient = patientRepository.GetPatientNameAndServicesById( Current.AgencyId,patientId);
                if (patient != null)
                {
                    auth.DisplayName = patient.DisplayName;
                    auth.PatientServices = patient.Services;
                }
            }
            return auth;
        }

        //public AAE.Authorization GetAuthorizationWithPatientName(Guid patientId, Guid Id)
        //{
        //    var auth = basePatientProfileRepository.GetAuthorization(Current.AgencyId, patientId, Id, insuranceId);
        //    if (auth != null)
        //    {
        //        var patient = patientRepository.GetPatientNameAndServicesById(patientId, Current.AgencyId);
        //        if (patient != null)
        //        {
        //            auth.DisplayName = patient.DisplayName;
        //            auth.PatientServices = patient.Services;
        //        }
        //    }
        //    return auth;
        //}

        public IList<AAE.Authorization> GetAuthorizations(Guid patientId)
        {
            return basePatientProfileRepository.GetAuthorizations(Current.AgencyId, patientId);
        }

        public IList<AAE.Authorization> GetAuthorizationsWithBranchName(Guid patientId)
        {
            var authorizations = basePatientProfileRepository.GetAuthorizations(Current.AgencyId, patientId);
            if (authorizations != null && authorizations.Count > 0)
            {
                var locatons = LocationEngine.GetLocations(Current.AgencyId, authorizations.Select(a => a.AgencyLocationId).Distinct().ToList());
                authorizations.ForEach(a =>
                {
                    var location = locatons.FirstOrDefault(l => l.Id == a.AgencyLocationId);
                    if (location != null)
                    {
                        a.Branch = location.Name;
                    }
                });
            }
            return authorizations;
        }

        #endregion
    }
}