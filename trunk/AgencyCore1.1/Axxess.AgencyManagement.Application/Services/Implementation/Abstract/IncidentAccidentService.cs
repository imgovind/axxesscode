﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Transactions;

    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Application.Helpers;

    using Axxess.Log.Enums;

    using SubSonic.DataProviders;

    public abstract class IncidentAccidentService<T, E> : BaseService
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        //protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;

        private IncidentAccidentAbstract baseNoteRepository;
        private TaskScheduleAbstract<T> baseScheduleRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;
        // protected AgencyServices Service { get; set; }


        protected IncidentAccidentService(TaskScheduleAbstract<T> baseScheduleRepository,
            IncidentAccidentAbstract baseNoteRepository,
            EpisodeAbstract<E> baseEpisodeRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseNoteRepository = baseNoteRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
        }

        #region Incidents

        public JsonViewData AddScheduleTaskAndIncidentHelper(Guid patientId, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected incident report task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var incidents = this.GetIncidentFromTasks(patientId, scheduleEvents);
                if (incidents != null)
                {
                    if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (baseNoteRepository.AddMultipleIncident(incidents))
                        {
                            viewData.PatientId = patientId;
                            viewData.isSuccessful = true;
                            viewData.IsIncidentAccidentRefresh = true;
                            viewData.Service = Service.ToString();
                            viewData.ServiceId = (int)Service;
                            Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, "");
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, patientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }

                }
            }
            return viewData;
        }

        public List<Incident> GetIncidentFromTasks(Guid patientId, List<T> scheduleEvents)
        {
            var incidents = new List<Incident>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var incident = new Incident
                    {
                        AgencyId = Current.AgencyId,
                        Id = scheduleEvent.Id,
                        PatientId = scheduleEvent.PatientId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        IncidentDate = scheduleEvent.EventDate,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        Status = scheduleEvent.Status,
                        UserId = scheduleEvent.UserId
                    };
                    incidents.Add(incident);

                });
            }
            return incidents;
        }


        public JsonViewData AddIncident(Incident incident, bool isSigned)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Incident / Accident Log could not be saved." };
            if (isSigned)
            {
                incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                viewData.IsCaseManagementRefresh = true;
                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                if (isQaByPass)
                {
                    viewData.IsCaseManagementRefresh = false;
                    incident.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                }
            }
            else
            {
                incident.SignatureText = string.Empty;
            }
            incident.Id = Guid.NewGuid();
            incident.UserId = Current.UserId;
            incident.AgencyId = Current.AgencyId;
            //incident.EpisodeId = GetReportNoteEpisodeId(incident.PatientId, incident.IncidentDate);
            if (baseNoteRepository.AddIncident(incident))
            {
                var newTask = TaskHelperFactory<T>.CreateTask(incident.Id, incident.UserId, incident.EpisodeId, incident.PatientId,
                incident.Status, Disciplines.ReportsAndNotes, incident.IncidentDate, (int)DisciplineTasks.IncidentAccidentReport);

                if (baseScheduleRepository.AddScheduleTask(newTask))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, incident.EpisodeId, incident.PatientId, incident.Id, Actions.Add, DisciplineTasks.IncidentAccidentReport);
                    viewData.PatientId = newTask.PatientId;
                    viewData.EpisodeId = incident.EpisodeId;
                    viewData.UserIds = new List<Guid> { incident.UserId };
                    viewData.IsDataCentersRefresh = true;
                    viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, newTask);
                    viewData.Service = Service.ToString();
                    viewData.ServiceId = (int)Service;
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Incident / Accident Log was saved successfully";
                }
                else
                {
                    baseNoteRepository.RemoveIncident(Current.AgencyId, incident.PatientId, incident.Id);
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Incident / Accident Log.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in saving the Incident / Accident Log.";
            }
            return viewData;
        }

        public Incident GetIncident(Guid Id)
        {
            var incident = baseNoteRepository.GetIncidentReport(Current.AgencyId, Id);
            if (incident != null)
            {
                var permission = Current.Permissions;
                if (permission.IsNotNullOrEmpty())
                {
                    incident.IsUserCanEdit = permission.IsInPermission(this.Service, ParentPermission.ManageIncidentAccidentReport, PermissionActions.Edit);
                    incident.IsUserCanAddPhysicain = !Current.IsAgencyFrozen && permission.IsInPermission(this.Service, ParentPermission.Physician, PermissionActions.Add);
                }
                incident.Service = this.Service;
                var patientName = patientRepository.GetPatientNameById(incident.PatientId, Current.AgencyId);
                if (patientName != null)
                {
                    incident.PatientName = patientName;
                }
                var dateRange = baseEpisodeRepository.GetEpisodeDateRange(Current.AgencyId, incident.PatientId, incident.EpisodeId);
                if (dateRange != null)
                {
                    incident.EpisodeStartDate = dateRange.StartDateFormatted;
                    incident.EpisodeEndDate = dateRange.EndDateFormatted;
                }
            }
            else { incident = new Incident { Service = this.Service }; }
            return incident;
        }

        public JsonViewData UpdateIncident(Incident incident, bool isSigned)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Incident / Accident Log could not be saved" };
            incident.AgencyId = Current.AgencyId;
            incident.UserId = incident.UserId.IsEmpty() ? Current.UserId : incident.UserId;
            if (isSigned)
            {
                viewData.IsCaseManagementRefresh = true;
                incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                if (isQaByPass)
                {
                    incident.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    viewData.IsCaseManagementRefresh = false;
                }
            }
            else
            {
                incident.SignatureText = string.Empty;
            }
            //incident.EpisodeId = GetReportNoteEpisodeId(incident.PatientId, incident.IncidentDate);
            var editTask = baseScheduleRepository.GetScheduleTask(Current.AgencyId, incident.PatientId, incident.Id);
            if (editTask != null)
            {
                var oldTaskValues = editTask.DeepClone();// TaskHelperFactory<T>.CopyTaskValues(editTask);
                editTask.EpisodeId = incident.EpisodeId;
                editTask = TaskHelperFactory<T>.SetNoteProperties(editTask, incident.IncidentDate, incident.Status);
                if (baseScheduleRepository.UpdateScheduleTask(editTask))
                {
                    if (baseNoteRepository.UpdateIncident(incident))
                    {
                        if (Enum.IsDefined(typeof(ScheduleStatus), editTask.Status))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, incident.EpisodeId, editTask.PatientId, editTask.Id, Actions.Add, (ScheduleStatus)editTask.Status, DisciplineTasks.IncidentAccidentReport, string.Empty);
                        }
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = incident.PatientId;
                        viewData.EpisodeId = incident.EpisodeId;
                        viewData.UserIds = new List<Guid> { incident.UserId };
                        viewData.IsCaseManagementRefresh = viewData.IsCaseManagementRefresh;
                        viewData.IsDataCentersRefresh = true;
                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, editTask);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Incident / Accident Log has been updated successfully.";
                    }
                    else
                    {
                        //editTask.EpisodeId = oldTaskValues.EpisodeId;
                        //editTask = TaskHelperFactory<T>.SetNoteProperties(editTask, oldTaskValues.VisitDate, oldTaskValues.Status);
                        baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Incident / Accident Log could not be updated.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "A problem occured while updating the data. Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "A problem occured while updating the data. Try again.";
            }
            return viewData;
        }

        public bool UpdateIncidentForScheduleDetail(T schedule)
        {
            var result = false;
            var accidentReport = baseNoteRepository.GetIncidentReport(Current.AgencyId, schedule.Id);
            if (accidentReport != null)
            {
                if (schedule.VisitDate.IsValid())
                {
                    accidentReport.IncidentDate = schedule.VisitDate;
                }
                accidentReport.EpisodeId = schedule.EpisodeId;
                accidentReport.IsDeprecated = schedule.IsDeprecated;
                accidentReport.Status = schedule.Status;
                result = baseNoteRepository.UpdateIncidentModal(accidentReport);
            }
            else
            {
                result = true;
            }
            return result;
        }


        public List<Incident> GetIncidents(Guid agencyId, bool isExcel)
        {
            var incidents = baseNoteRepository.GetIncidents(Current.AgencyId).ToList();
            if (incidents.IsNotNullOrEmpty())
            {
                var patientNames = patientRepository.GetPatientNamesByIds(Current.AgencyId, incidents.Select(i => i.PatientId).Distinct().ToList());
                var allPermission = new Dictionary<int, AgencyServices>();
                var isEditPermission = false;
                var isDeletePermission = false;
                var isPrintPermission = false;
                if (!isExcel)
                {
                    allPermission = Current.CategoryService(this.Service, ParentPermission.ManageIncidentAccidentReport, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Delete, (int)PermissionActions.Print });
                    if (allPermission.IsNotNullOrEmpty())
                    {
                        isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                        isDeletePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                        isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                    }
                }
                incidents.ForEach(i =>
                {
                    if (patientNames.ContainsKey(i.PatientId))
                    {
                        i.PatientName = patientNames[i.PatientId];
                    }
                    i.PhysicianName = PhysicianEngine.GetName(i.PhysicianId, Current.AgencyId);
                    i.Service = this.Service;
                    i.StatusName = ScheduleStatusFactory.StatusName(i.Status, i.IncidentDate);
                    if (!isExcel)
                    {
                        i.IsUserCanEdit = isEditPermission;
                        i.IsUserCanDelete = isDeletePermission;
                        i.IsUserCanPrint = isPrintPermission;
                    }
                });
            }
            return incidents;
        }

        public Incident GetIncidentReportPrint(Guid patientId, Guid eventId)
        {
            var incident = baseNoteRepository.GetIncidentReport(Current.AgencyId, eventId);
            if (incident != null)
            {
                var profile = profileRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                if (profile != null)
                {
                    incident.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, profile.AgencyLocationId);
                    incident.PatientProfile = profile;
                    incident.PatientName = incident.PatientProfile.LastName + ", " + incident.PatientProfile.FirstName;
                }
                var dateRange = baseEpisodeRepository.GetEpisodeDateRange(Current.AgencyId, incident.PatientId, incident.EpisodeId);
                if (dateRange != null)
                {
                    incident.EpisodeStartDate = dateRange.StartDateFormatted;
                    incident.EpisodeEndDate = dateRange.EndDateFormatted;
                } 
                if (!incident.PhysicianId.IsEmpty())
                {
                    incident.PhysicianName = PhysicianEngine.GetName(incident.PhysicianId, Current.AgencyId);
                }
            }
            return incident;
        }

        public JsonViewData ProcessIncidents(Guid patientId, Guid eventId, string actionType, string reason)
        {
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                return ProcessIncidents(scheduleEvent, actionType, reason);
            }
            return new JsonViewData(false, "Unable to find incident. Please try again.");
        }

        public JsonViewData ProcessIncidents(T task, string button, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            bool isIncidentToBeUpdated = true;
            bool isActionSet = false;
            if (task != null)
            { var description = string.Empty;
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var incident = baseNoteRepository.GetIncidentReport(Current.AgencyId, task.Id);
                        if (incident != null)
                        {
                            if (!incident.EpisodeId.IsEmpty())
                            {
                                var oldStatus = task.Status;
                                if (button == "Approve")
                                {
                                    incident.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                                    task.InPrintQueue = true;
                                    task.Status = incident.Status;
                                    description = "Approved by " + Current.UserFullName;
                                    isActionSet = true;
                                }
                                else if (button == "Return")
                                {
                                    incident.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                                    incident.SignatureText = string.Empty;
                                    incident.SignatureDate = DateTime.MinValue;
                                    task.Status = incident.Status;
                                    description = "Returned by " + Current.UserFullName;
                                    isActionSet = true;
                                }
                                else if (button == "Print")
                                {
                                    task.InPrintQueue = false;
                                    isIncidentToBeUpdated = false;
                                    isActionSet = true;
                                }
                                if (isActionSet)
                                {
                                    if (baseScheduleRepository.UpdateScheduleTask(task))
                                    {
                                        if (isIncidentToBeUpdated)
                                        {
                                            bool returnCommentStatus = true;
                                            if (reason.IsNotNullOrEmpty())
                                            {
                                                returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                            }
                                            if (returnCommentStatus)
                                            {
                                                incident.Modified = DateTime.Now;
                                                if (baseNoteRepository.UpdateIncidentModal(incident))
                                                {
                                                    viewData.IsDataCentersRefresh = oldStatus != task.Status;
                                                    viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
                                                    viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == task.UserId && !task.IsComplete && task.EventDate.Date >= DateTime.Now.AddDays(-89) && task.EventDate.Date <= DateTime.Now.AddDays(14);
                                                    viewData.IsIncidentAccidentRefresh = viewData.IsDataCentersRefresh;
                                                    viewData.EpisodeId = task.EpisodeId;
                                                    viewData.PatientId = task.PatientId;
                                                    viewData.UserIds = new List<Guid> { task.UserId };
                                                    viewData.Service = Service.ToString();
                                                    viewData.ServiceId = (int)Service;
                                                    viewData.isSuccessful = true;
                                                    ts.Complete();
                                                }
                                                else
                                                {
                                                    viewData.errorMessage = "A problem occured while updating the Incident / Accident Log.";
                                                    ts.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                viewData.errorMessage = "A problem occured while adding the return comment.";
                                                ts.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            viewData.isSuccessful = true;
                                            ts.Complete();
                                        }
                                    }
                                    else
                                    {
                                        viewData.errorMessage = "A problem occured while updating the task.";
                                        ts.Dispose();
                                    }
                                }
                            }
                        }
                    }
                }
                if (viewData.isSuccessful && isIncidentToBeUpdated && Enum.IsDefined(typeof(ScheduleStatus), task.Status))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, Actions.StatusChange, (DisciplineTasks)task.DisciplineTask, description);
                }
            }
            return viewData;
        }

        public JsonViewData ReopenIncidentReport(T scheduleTask)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleTask != null)
            {
                var oldUserId = scheduleTask.UserId;
                var oldStatus = scheduleTask.Status;
                var description = string.Empty;

                viewData.PatientId = scheduleTask.PatientId;
                viewData.EpisodeId = scheduleTask.EpisodeId;
                viewData.UserIds = new List<Guid> { scheduleTask.UserId };


                scheduleTask.Status = ((int)ScheduleStatus.ReportAndNotesReopen);
                var incident = baseNoteRepository.GetIncidentReport(Current.AgencyId, scheduleTask.Id);
                if (incident != null)
                {
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                    {
                        incident.UserId = scheduleTask.UserId;
                        incident.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                        incident.SignatureText = string.Empty;
                        incident.SignatureDate = DateTime.MinValue;
                        incident.Modified = DateTime.Now;
                        if (baseNoteRepository.UpdateIncidentModal(incident))
                        {
                            viewData.isSuccessful = true;
                            viewData.IsIncidentAccidentRefresh = true;
                            viewData.IsDataCentersRefresh = true;
                            viewData.Service = this.Service.ToString();
                            viewData.ServiceId = (int)this.Service;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduleTask);

                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleTask.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleTask.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleTask);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ToggleIncident(Guid patientId, Guid Id, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Incident / Accident Log could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            var task = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, Id);
            if (task != null)
            {
                viewData = ToggleIncident(task, isDeprecated);
            }
            return viewData;
        }


        public JsonViewData ToggleIncident(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Incident / Accident Log could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (baseNoteRepository.MarkIncidentAsDeleted(task.Id, task.PatientId, Current.AgencyId, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.IsIncidentAccidentRefresh = true;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.UserIds = new List<Guid> { task.UserId };
                        viewData.errorMessage = string.Format("The Incident / Accident Log has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        #endregion
    }
}
