﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Transactions;
    using System.Web;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    using Axxess.AgencyManagement.Application.Common;

    using Telerik.Web.Mvc.Extensions;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Helpers;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;

    using FormCollection = System.Web.Mvc.FormCollection;
    using SubSonic.DataProviders;
    using Axxess.AgencyManagement.Entities.Common;

    public abstract class NoteService<T, E>
        where T : ITask, new()
        where E : CarePeriod, new()
    {
        readonly string[] WoundKeys;

        protected IAgencyRepository agencyRepository;
        protected IPatientRepository patientRepository;
        protected PatientProfileAbstract profileRepository;
        protected IPhysicianRepository physicianRepository;
        protected MongoAbstract mongoRepository;
        protected ILookupRepository lookUpRepository;
        protected VitalSignAbstract vitalSignRepository;

        private TaskScheduleAbstract<T> baseScheduleRepository;
        private VisitNoteAbstract baseNoteRepository;
        private EpisodeAbstract<E> baseEpisodeRepository;

        protected AgencyServices Service { get; set; }

        protected NoteService(TaskScheduleAbstract<T> baseScheduleRepository, VisitNoteAbstract baseNoteRepository, EpisodeAbstract<E> baseEpisodeRepository, MongoAbstract mongoRepository, VitalSignAbstract vitalSignRepository)
        {
            this.baseScheduleRepository = baseScheduleRepository;
            this.baseNoteRepository = baseNoteRepository;
            this.baseEpisodeRepository = baseEpisodeRepository;
            this.mongoRepository = mongoRepository;
            this.vitalSignRepository = vitalSignRepository;
        }

        public PatientVisitNote GetVisitNote(Guid patientId, Guid eventId)
        {
            return baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
        }

        #region  Visit Note

        public VisitNoteViewData GetVisitNoteView(Guid patientId, Guid eventId)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Service = Service;
            var scheduledEvent = baseScheduleRepository.GetScheduleTaskForNoteEdit(Current.AgencyId, patientId, eventId);
            if (scheduledEvent != null)
            {
                // var argument = GetArgumentNoteWithView(scheduledEvent, false);
                var permissions = Current.Permissions;
                noteViewData.IsUserCanLoadPreviousNotes = permissions.IsInPermission(Service, ParentPermission.Schedule, PermissionActions.LoadPrevious);
                //argument.IsPreviousNoteNeeded = argument.IsPreviousNoteNeeded && noteViewData.IsUserCanLoadPreviousNotes;
               
                noteViewData.IsUserCanReturn = Current.UserId != noteViewData.UserId && scheduledEvent.Status == (int)ScheduleStatus.NoteSubmittedWithSignature && permissions.IsInPermission(Service, ParentPermission.QA, PermissionActions.Return);
                noteViewData.IsUserCanApprove = permissions.IsInPermission(Service, ParentPermission.QA, PermissionActions.Approve);
                noteViewData.IsUserCanCreateOrder = permissions.IsInPermission(Service, ParentPermission.Orders, PermissionActions.Add);
                noteViewData.IsUserCanCreateIncident = permissions.IsInPermission(Service, ParentPermission.ManageIncidentAccidentReport, PermissionActions.Add);
                noteViewData.IsUserCanCreateInfection = permissions.IsInPermission(Service, ParentPermission.ManageInfectionReport, PermissionActions.Add);
                GetVisitNoteHelper(noteViewData, scheduledEvent);
                noteViewData.IsUserCanScheduleVisits = permissions.IsInPermission(Service, ParentPermission.Schedule, PermissionActions.Add) && noteViewData.Service == AgencyServices.HomeHealth && noteViewData.FooterSettings.CanScheduleSupVisit;

                //noteViewData.Arguments = argument;
            }
            return noteViewData;
        }

        public void GetVisitNoteHelper(VisitNoteViewData noteViewData, T scheduledEvent)//, NoteArguments arguments)
        {
            //noteViewData.LocationProfile = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (scheduledEvent != null)
            {
                var disciplineTask = lookUpRepository.GetDisciplineTask(scheduledEvent.DisciplineTask);
                if (disciplineTask != null)
                {
                    var profile = profileRepository.GetPatientPrintProfile(Current.AgencyId, scheduledEvent.PatientId);
                    if (profile != null)
                    {
                        noteViewData.LocationId = profile.AgencyLocationId;
                        var patientvisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id);
                        if (patientvisitNote != null)
                        {
                            var viewIdentifier = (int)NoteArgumentsType.View;
                            var saveIdentifier = (int)NoteArgumentsType.Save;

                            noteViewData.SelectedEpisodeId = patientvisitNote.SelectedEpisodeId != Guid.Empty ? patientvisitNote.SelectedEpisodeId : patientvisitNote.EpisodeId;
                            noteViewData.Version = scheduledEvent.Version > 0 ? scheduledEvent.Version : 1;
                            noteViewData.ContentPath = disciplineTask.ParentPath + (disciplineTask.IsVersionAppended ? (scheduledEvent.Version > 0 ? scheduledEvent.Version.ToString() : "1") : string.Empty);
                            noteViewData.Surcharge = scheduledEvent.Surcharge;
                            noteViewData.AssociatedMileage = scheduledEvent.AssociatedMileage;

                            noteViewData.IsVisitParameterVisible = disciplineTask.IsVisitParameterVisible;
                            noteViewData.IsTimeIntervalVisible = disciplineTask.IsTimeIntervalVisible;
                            noteViewData.IsTravelParameterVisible = disciplineTask.IsTravelParameterVisible;

                            noteViewData.IsUserCanMarkAsOrder = DisciplineTaskFactory.AllNoteOrdersButEvals().Contains(scheduledEvent.DisciplineTask) || (DisciplineTaskFactory.OnlyEvalOrders(false).Contains(scheduledEvent.DisciplineTask) && scheduledEvent.Version >= 2);
                            noteViewData.IsDNRVisible = disciplineTask.IsDNRVisible;
                            noteViewData.IsPhysicianVisible = (disciplineTask.Physician & viewIdentifier) == viewIdentifier;
                            noteViewData.IsPhysicianDateVisible = disciplineTask.HasPhysicianDate;
                            noteViewData.IsAllergiesVisible = (disciplineTask.Allergy & viewIdentifier) == viewIdentifier;
                            noteViewData.IsAllergiesSavable = noteViewData.IsAllergiesVisible && ((disciplineTask.Allergy & saveIdentifier) == saveIdentifier);


                            if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.HHAideCarePlan)
                            {
                                noteViewData.IsSelectedEpisodeVisible = true;
                                noteViewData.IsFrequencyVisible = true;
                            }
                            if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.HHAideVisit)
                            {
                                noteViewData.IsFrequencyVisible = true;
                            }

                            //IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                            //TaskHelperFactory<T>.FormatReturnComments
                            //noteViewData.HasReturnReasons = scheduledEvent.ReturnReason.IsNotNullOrEmpty() ? true : baseScheduleRepository.HasReturnReasons(Current.AgencyId, arguments.EpisodeId, arguments.EventId);
                            noteViewData.HasReturnReasons = scheduledEvent.HasReturnComments;
                            noteViewData.StatusComment = scheduledEvent.StatusComment;
                            noteViewData.UserId = scheduledEvent.UserId;


                            bool isNoteStillNotStarted = ScheduleStatusFactory.NursingNoteNotYetStarted().Exists(s => s == patientvisitNote.Status);
                            if (isNoteStillNotStarted)
                            {
                                noteViewData.Questions = noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                                if (noteViewData.IsPhysicianVisible)// arguments.IsPhysicianNeeded)
                                {
                                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, scheduledEvent.PatientId);
                                    if (physician != null)
                                    {
                                        noteViewData.PhysicianId = physician.Id;
                                        noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                }
                                if (noteViewData.IsDNRVisible)
                                {
                                    noteViewData.Questions.Add("DNR", QuestionFactory<NotesQuestion>.Create("DNR", profile.IsDNR.ToInteger().ToString()));
                                }
                                if (noteViewData.IsAllergiesSavable)
                                {
                                    var allergyProfile = patientRepository.GetAllergyProfileByPatient(scheduledEvent.PatientId, Current.AgencyId);
                                    if (allergyProfile != null)
                                    {
                                        noteViewData.Allergies = allergyProfile.ToString();
                                        noteViewData.Questions.Add("AllergiesDescription", QuestionFactory<NotesQuestion>.Create("AllergiesDescription", allergyProfile.ToString()));
                                    }
                                }
                            }
                            else
                            {
                                SetQuestions(patientvisitNote);
                                noteViewData.Questions = patientvisitNote.ToDictionary() ?? new Dictionary<string, NotesQuestion>();
                                if (noteViewData.IsPhysicianVisible)//arguments.IsPhysicianNeeded)
                                {
                                    if (patientvisitNote.PhysicianId.IsEmpty())
                                    {
                                        var physicianId = this.GetPhysicianFromNotes(scheduledEvent.DisciplineTask, noteViewData.Questions);
                                        if (!physicianId.IsEmpty())
                                        {
                                            noteViewData.PhysicianId = physicianId;
                                        }
                                    }
                                    else
                                    {
                                        noteViewData.PhysicianId = patientvisitNote.PhysicianId;
                                    }
                                }
                            }

                            noteViewData.PatientId = patientvisitNote.PatientId;
                            noteViewData.EpisodeId = scheduledEvent.EpisodeId;
                            noteViewData.EventId = patientvisitNote.Id;
                            noteViewData.DisciplineTask = scheduledEvent.DisciplineTask;
                            noteViewData.TaskName = scheduledEvent.DisciplineTaskName;

                            disciplineTask.IsNotOasisCarePlan = !disciplineTask.IsOasisCarePlan && disciplineTask.IsNotOasisCarePlan;
                            disciplineTask.IsOasisCarePlan = disciplineTask.IsOasisCarePlan && ((disciplineTask.CarePlan & viewIdentifier) == viewIdentifier);
                            disciplineTask.IsNotOasisCarePlan = disciplineTask.IsNotOasisCarePlan && ((disciplineTask.CarePlan & viewIdentifier) == viewIdentifier);

                            var episode = baseEpisodeRepository.GetEpisodeDateRange(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.EpisodeId);
                            if (episode != null)
                            {
                                scheduledEvent.StartDate = episode.StartDate;
                                scheduledEvent.EndDate = episode.EndDate;
                                noteViewData.EndDate = episode.EndDate;
                                noteViewData.StartDate = episode.StartDate;
                            }

                            SetTaskSpecificInformation(noteViewData, scheduledEvent, disciplineTask, isNoteStillNotStarted);

                            noteViewData.IsCarePlanVisible = (disciplineTask.IsNotOasisCarePlan || disciplineTask.IsOasisCarePlan) && !noteViewData.CarePlanOrEvalId.IsEmpty();
                            noteViewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                            noteViewData.IsSupplyExist = patientvisitNote.IsSupplyExist;

                            noteViewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                            noteViewData.TypeName = scheduledEvent.DisciplineTask.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetCustomDescription();
                            noteViewData.TypeName = noteViewData.TypeName.IsNotNullOrEmpty() && noteViewData.TypeName != DisciplineTasks.NoDiscipline.GetDescription() ? noteViewData.TypeName : string.Empty;
                            if (DisciplineTaskFactory.SkilledNurseSharedFile().Contains(scheduledEvent.DisciplineTask))
                            {
                                noteViewData.TypeName += " Note";
                            }

                            noteViewData.CanChangeDisciplineType = disciplineTask.IsTypeChangeable;
                            if (disciplineTask.FooterSettings.IsNotNullOrEmpty())
                            {
                                noteViewData.FooterSettings = disciplineTask.FooterSettings.FromJson<FooterSettings>();
                            }
                            if (!DisciplineTaskFactory.DisciplineTasksWithMultiVitalSigns().Contains(disciplineTask.Id) && disciplineTask.NewVitalSignsVersion.HasValue && scheduledEvent.Version >= disciplineTask.NewVitalSignsVersion.Value)
                            {
                                noteViewData.VitalSignLog = vitalSignRepository.GetEntitiesVitalSign(scheduledEvent.Id, scheduledEvent.PatientId, Current.AgencyId);
                            }
                            noteViewData.IsUserCanLoadPreviousNotes = noteViewData.IsUserCanLoadPreviousNotes && ((disciplineTask.PreviousNote & viewIdentifier) == viewIdentifier);
                            //if (noteViewData.IsUserCanLoadPreviousNotes)
                            //{
                            //    noteViewData.PreviousNotes = this.GetPreviousNotes(scheduledEvent.PatientId, scheduledEvent);
                            //}

                            noteViewData.PatientProfile = profile;
                        }
                        else
                        {
                            noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                        }
                    }
                    else
                    {
                        noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                    }
                }
                else
                {
                    noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            // return noteViewData;
        }

        public VisitNoteViewData GetVisitNoteForContent(Guid patientId, Guid noteId, Guid previousNoteId, string type)
        {
            var viewData = new VisitNoteViewData();
            viewData.Service = this.Service;
            viewData.Type = type;
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, noteId);
            if (scheduleEvent != null)
            {
                var previousVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
                if (previousVisitNote != null)
                {
                    var displineTask = DisciplineTasks.NoDiscipline;
                    viewData.PatientId = previousVisitNote.PatientId;
                    //TODO Need exlude everywhere episode used
                    // viewData.EpisodeId = scheduleEvent.EpisodeId;
                    viewData.EventId = scheduleEvent.Id;
                    viewData.Version = scheduleEvent.Version > 0 ? scheduleEvent.Version : 1;
                    if (viewData.Type.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), viewData.Type))
                    {
                        displineTask = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), viewData.Type);
                        viewData.TypeName = displineTask.GetDescription();
                    }
                    if (DisciplineTaskFactory.SkilledNurseSharedFile().Contains(scheduleEvent.DisciplineTask))
                    {
                        viewData = SkilledNurseVisitContent(scheduleEvent, previousVisitNote, viewData);
                    }
                    else
                    {
                        viewData.Questions = previousVisitNote.ToDictionary();
                    }
                    var disciplineTask = lookUpRepository.GetDisciplineTask(scheduleEvent.DisciplineTask);
                    if (disciplineTask != null)
                    {
                        viewData.ContentPath = disciplineTask.ContentPath + (disciplineTask.IsVersionAppended ? (scheduleEvent.Version > 0 ? scheduleEvent.Version.ToString() : "1") : string.Empty); //GetViewForContent(new T { Version = viewData.Version, DisciplineTask = (int)displineTask });
                    }
                }
            }
            return viewData;
        }

        public VisitNoteViewData GetVisitNotePrint()
        {
            var note = new VisitNoteViewData();
            note.Service = this.Service;
            note.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(int type)
        {
            var note = new VisitNoteViewData();
            note.Service = this.Service;
            note.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
            var task = Enum.IsDefined(typeof(DisciplineTasks), type) ? ((DisciplineTasks)type) : DisciplineTasks.NoDiscipline;
            var discipline = lookUpRepository.GetDisciplineTask(type);
            if (discipline != null)
            {
                note.Version = discipline.Version;
            }
            note.Type = task.ToString();
            note.TypeName = task.GetDescription();
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(Guid patientId, Guid eventId)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Service = this.Service;
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                noteViewData = GetVisitNotePrintHelper(scheduleEvent) ?? new VisitNoteViewData();
                SetVisitNoteViewData(scheduleEvent, noteViewData);
            }
            return noteViewData;
        }

        public VisitNoteViewData GetVisitNotePrint(PatientVisitNote patientvisitNote)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Service = this.Service;
            var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientvisitNote.PatientId, patientvisitNote.Id);
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                noteViewData = GetVisitNotePrintHelper(scheduleEvent, patientvisitNote) ?? new VisitNoteViewData();
                SetVisitNoteViewData(scheduleEvent, noteViewData);
            }
            return noteViewData;
        }

        protected void SetVisitNoteViewData(T scheduleEvent, VisitNoteViewData noteViewData)
        {
            noteViewData.Surcharge = scheduleEvent.Surcharge;
            noteViewData.AssociatedMileage = scheduleEvent.AssociatedMileage;
            noteViewData.TimeIn = scheduleEvent.TimeIn;
            noteViewData.TimeOut = scheduleEvent.TimeOut;
            noteViewData.Version = scheduleEvent.Version;
            noteViewData.DisciplineTask = scheduleEvent.DisciplineTask;
        }

        protected VisitNoteViewData GetVisitNotePrintHelper(T scheduledEvent) //NoteArguments arguments)
        {
            var patientvisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id);
            if (patientvisitNote != null)
            {
                return GetVisitNotePrintHelper(scheduledEvent, patientvisitNote);
            }
            return new VisitNoteViewData();
        }

        protected VisitNoteViewData GetVisitNotePrintHelper(T scheduledEvent, PatientVisitNote patientvisitNote)//NoteArguments arguments)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Service = this.Service;
            if (patientvisitNote != null)
            {
                var disciplineTask = lookUpRepository.GetDisciplineTask(scheduledEvent.DisciplineTask);
                if (disciplineTask != null)
                {
                    var printIdentifier = (int)NoteArgumentsType.View;
                    if ((disciplineTask.Allergy & printIdentifier) == printIdentifier)//(arguments.IsAllergyNeeded)
                    {
                        var allergyProfile = patientRepository.GetAllergyProfileByPatient(scheduledEvent.PatientId, Current.AgencyId);
                        if (allergyProfile != null)
                        {
                            noteViewData.Allergies = allergyProfile.ToString();
                        }
                    }
                    noteViewData.SignatureText = patientvisitNote.SignatureText;
                    noteViewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                    SetQuestions(patientvisitNote);
                    noteViewData.Questions = patientvisitNote.ToDictionary() ?? new Dictionary<string, NotesQuestion>();
                    if (ScheduleStatusFactory.NursingNoteNotYetStarted().Exists(s => s == patientvisitNote.Status))
                    {
                        if ((disciplineTask.Physician & printIdentifier) == printIdentifier)// (arguments.IsPhysicianNeeded)
                        {
                            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, scheduledEvent.PatientId);
                            if (physician != null)
                            {
                                noteViewData.PhysicianId = physician.Id;
                                noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                            }
                        }
                    }
                    else
                    {
                        if ((disciplineTask.Physician & printIdentifier) == printIdentifier)// (arguments.IsPhysicianNeeded)
                        {
                            var physicianInfoSet = false;
                            if (patientvisitNote.PhysicianData.IsNotNullOrEmpty())
                            {
                                var physician = patientvisitNote.PhysicianData.ToObject<AgencyPhysician>();
                                if (physician != null)
                                {
                                    noteViewData.PhysicianId = physician.Id;
                                    noteViewData.PhysicianDisplayName = physician.DisplayName;
                                    physicianInfoSet = true;
                                }
                            }
                            if (!physicianInfoSet)
                            {
                                if (patientvisitNote.PhysicianId.IsEmpty())
                                {
                                    var physicianId = GetPhysicianFromNotes(scheduledEvent.DisciplineTask, noteViewData.Questions);
                                    if (!physicianId.IsEmpty())
                                    {
                                        noteViewData.PhysicianId = physicianId;
                                        // note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                }
                                else
                                {
                                    noteViewData.PhysicianId = patientvisitNote.PhysicianId;
                                }
                                if (!noteViewData.PhysicianId.IsEmpty())
                                {
                                    var physician = PhysicianEngine.Get(noteViewData.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        noteViewData.PhysicianId = physician.Id;
                                        noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                }
                            }
                        }

                        if (!DisciplineTaskFactory.DisciplineTasksWithMultiVitalSigns().Contains(disciplineTask.Id) && disciplineTask.NewVitalSignsVersion.HasValue && ((scheduledEvent.Version > 0 ? scheduledEvent.Version : 1) >= disciplineTask.NewVitalSignsVersion.Value))
                        {
                            noteViewData.VitalSignLog = vitalSignRepository.GetEntitiesVitalSign(scheduledEvent.Id, scheduledEvent.PatientId, Current.AgencyId);
                            if (noteViewData.VitalSignLog != null)
                            {
                                noteViewData.Questions.AddRange(noteViewData.VitalSignLog.ToQuestionDictionary());
                            }
                        }
                    }
                    noteViewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                    if (noteViewData.IsWoundCareExist)
                    {
                        noteViewData.WoundCare = patientvisitNote.ToWoundCareDictionary();
                    }
                    SetTaskSpecificInformationPrint(noteViewData, scheduledEvent, disciplineTask);
                    noteViewData.PatientId = patientvisitNote.PatientId;
                    noteViewData.EpisodeId = scheduledEvent.EpisodeId;
                    noteViewData.EventId = patientvisitNote.Id;
                    noteViewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    noteViewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    noteViewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : string.Empty;
                    noteViewData.Version = patientvisitNote.Version;
                    noteViewData.PatientProfile = profileRepository.GetPatientPrintProfile(Current.AgencyId, scheduledEvent.PatientId);
                    if (noteViewData.PatientProfile != null && !noteViewData.PatientProfile.AgencyLocationId.IsEmpty())
                    {
                        noteViewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, noteViewData.PatientProfile.AgencyLocationId);
                    }
                    else
                    {
                        noteViewData.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
                    }
                }
                else
                {
                    noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return noteViewData;
        }

        protected List<PatientVisitNote> CreatePatientVisitNotesFromTasks(Guid patientId, List<T> scheduleEvents)
        {
            var patientVisitNotes = new List<PatientVisitNote>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var noteOrders = DisciplineTaskFactory.AllNoteOrders();
                var dischargeSummaries = DisciplineTaskFactory.NoteDischargeSummaries();
                var physicianId = Guid.Empty;
                if (scheduleEvents.Exists(s => dischargeSummaries.Contains(s.DisciplineTask)))
                {
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        physicianId = physician.Id;
                    }
                }
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var note = new PatientVisitNote
                    {
                        AgencyId = Current.AgencyId,
                        Id = scheduleEvent.Id,
                        PatientId = scheduleEvent.PatientId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(),
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        Status = (int)ScheduleStatus.NoteNotYetDue,
                        UserId = scheduleEvent.UserId,
                        IsBillable = scheduleEvent.IsBillable,
                        Version = scheduleEvent.Version
                    };
                    if (noteOrders.Contains(scheduleEvent.DisciplineTask))
                    {
                        note.OrderNumber = patientRepository.GetNextOrderNumber();
                    }
                    if (!physicianId.IsEmpty())
                    {
                        note.PhysicianId = physicianId;
                        note.Questions = new List<NotesQuestion>() { new NotesQuestion { Name = "Physician", Answer = physicianId.ToString(), Type = "DischargeSummary" } };
                    }
                    else
                    {
                        note.Questions = new List<NotesQuestion>();
                    }
                    patientVisitNotes.Add(note);
                });
            }
            return patientVisitNotes;
        }

        public JsonViewData AddScheduleTaskAndNoteHelper(E episode, List<T> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to add selected patient visit note task." };
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var patientVisitNotes = this.CreatePatientVisitNotesFromTasks(episode.PatientId, scheduleEvents);
                if (patientVisitNotes != null)
                {
                    if (baseScheduleRepository.AddMultipleScheduleTask(scheduleEvents))
                    {
                        if (baseNoteRepository.AddMultipleVisitNote(patientVisitNotes))
                        {
                            if (mongoRepository.AddManyNoteQuestions(patientVisitNotes.Select(ptv => new QuestionData(ptv)).ToList()))
                            {
                                viewData.isSuccessful = true;
                                viewData.PatientId = episode.PatientId;
                                viewData.EpisodeId = episode.Id;
                                viewData.UserIds = new List<Guid>();
                                viewData.UserIds.AddRange(scheduleEvents.Select(s => s.UserId));
                                viewData.IsDataCentersRefresh = true;
                                viewData.Service = Service.ToString();
                                viewData.ServiceId = (int)Service;
                                viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(true, scheduleEvents);
                                Auditor.MultiLog(Current.AgencyId, scheduleEvents, Actions.Add, string.Empty);
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                baseNoteRepository.RemoveManyModels<PatientVisitNote>(Current.AgencyId, episode.PatientId, patientVisitNotes.Select(s => s.Id).ToList());
                                baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, episode.PatientId, scheduleEvents.Select(s => s.Id).ToList());
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            baseScheduleRepository.RemoveMultipleScheduleTaskFully(Current.AgencyId, episode.PatientId, scheduleEvents.Select(s => s.Id).ToList());
                        }
                    }
                }
            }
            return viewData;
        }

        public PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline)
        {
            return baseNoteRepository.GetCarePlanBySelectedEpisodeAndDisciplineTask(Current.AgencyId, patientId, episodeId, discipline);
        }

        #endregion

        #region Note Orders

        public int MarkEvalNoteAsSent(bool sendElectronically, string[] answersArray, ref List<AgencyPhysician> physicians)
        {
            int count = 0;
            AgencyPhysician physician = null;
            var status = (int)ScheduleStatus.EvalSentToPhysician;
            if (sendElectronically)
            {
                status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
            }
            foreach (var item in answersArray)
            {
                string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                var evalId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                var evalOrder = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
                if (evalOrder != null)
                {
                    if (evalOrder.PhysicianData.IsNotNullOrEmpty())
                    {
                        physician = evalOrder.PhysicianData.ToObject<AgencyPhysician>();
                    }
                    var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, evalOrder.PatientId, evalOrder.Id);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = status;
                        if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                        {
                            evalOrder.Status = status;
                            evalOrder.SentDate = DateTime.Now;
                            if (baseNoteRepository.UpdateVisitNote(evalOrder))
                            {
                                count++;
                                if (sendElectronically)
                                {
                                    if (physician != null && !physicians.Exists(p => physician.Id == p.Id))
                                    {
                                        physicians.Add(physician);
                                    }
                                }
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                            else
                            {
                                scheduleEvent.Status = oldStatus;
                                baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                            }
                        }
                    }
                }
            }
            return count;
        }

        public bool MarkEvalOrderAsReturned(Guid patientId, Guid id, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var eval = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, id);
            if (eval != null)
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, eval.PatientId, eval.Id);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                    {
                        var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!eval.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                        eval.ReceivedDate = receivedDate;
                        eval.PhysicianSignatureDate = physicianSignatureDate;
                        if (baseNoteRepository.UpdateVisitNote(eval))
                        {
                            result = true;
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                        }
                        else
                        {
                            scheduleEvent.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEvalOrderDates(Guid patientId, Guid id, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            var eval = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, id);
            if (eval != null)
            {
                eval.ReceivedDate = receivedDate;
                eval.SentDate = sendDate;
                eval.PhysicianSignatureDate = physicianSignatureDate;
                if (eval.PhysicianSignatureText.IsNullOrEmpty())
                {
                    var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    else if (!eval.PhysicianId.IsEmpty())
                    {
                        physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                    }
                }
                result = baseNoteRepository.UpdateVisitNote(eval);
            }
            return result;
        }

        public Order GetVisitNoteOrderForHistoryEdit(Guid patientId, Guid id, string typeEnumName)
        {
            var order = new Order();
            if (typeEnumName.IsNotNullOrEmpty())
            {
                var note = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (note != null)
                {
                    order = new Order
                    {
                        Id = note.Id,
                        PatientId = note.PatientId,
                        EpisodeId = note.EpisodeId,
                        Type = OrderType.PtReEvaluation,
                        Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                        Number = note.OrderNumber,
                        PatientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId),
                        ReceivedDate = note.ReceivedDate > DateTime.MinValue ? note.ReceivedDate : note.SentDate,
                        SendDate = note.SentDate,
                        PhysicianSignatureDate = note.PhysicianSignatureDate
                    };
                    SetOrderTypeTextForVisitNotes(typeEnumName, order);
                }
            }
            return order;
        }

        private static void SetOrderTypeTextForVisitNotes(string typeEnumName, Order order)
        {
            switch (typeEnumName)
            {
                case "PtEvaluation":
                    order.Type = OrderType.PtEvaluation;
                    order.Text = DisciplineTasks.PTEvaluation.GetDescription();
                    break;
                case "PtReEvaluation":
                    order.Type = OrderType.PtReEvaluation;
                    order.Text = DisciplineTasks.PTReEvaluation.GetDescription();
                    break;
                case "OtEvaluation":
                    order.Type = OrderType.OtEvaluation;
                    order.Text = DisciplineTasks.OTEvaluation.GetDescription();
                    break;
                case "OtReEvaluation":
                    order.Type = OrderType.OtReEvaluation;
                    order.Text = DisciplineTasks.OTReEvaluation.GetDescription();
                    break;
                case "StEvaluation":
                    order.Type = OrderType.StEvaluation;
                    order.Text = DisciplineTasks.STEvaluation.GetDescription();
                    break;
                case "StReEvaluation":
                    order.Type = OrderType.StReEvaluation;
                    order.Text = DisciplineTasks.STReEvaluation.GetDescription();
                    break;
                case "PTDischarge":
                    order.Type = OrderType.PTDischarge;
                    order.Text = DisciplineTasks.PTDischarge.GetDescription();
                    break;
                case "SixtyDaySummary":
                    order.Type = OrderType.SixtyDaySummary;
                    order.Text = DisciplineTasks.SixtyDaySummary.GetDescription();
                    break;
            }
        }

        public Order GetVisitNoteOrderForReceiving(Guid patientId, Guid id, string typeEnumName)
        {
            var order = new Order();
            {
                if (typeEnumName.IsNotNullOrEmpty())
                {
                    var note = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, id);
                    if (note != null)
                    {
                        order = new Order
                        {
                            Id = note.Id,
                            PatientId = note.PatientId,
                            EpisodeId = note.EpisodeId,
                            Type = OrderType.PtReEvaluation,
                            Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                            Number = note.OrderNumber,
                            ReceivedDate = note.ReceivedDate,
                            SendDate = note.SentDate,
                            PhysicianSignatureDate = note.PhysicianSignatureDate
                        };
                        SetOrderTypeTextForVisitNotes(typeEnumName, order);
                    }
                }
            }
            return order;
        }

        public List<Order> GetEvalOrders(List<T> evalOrdersSchedule, bool isPending, bool isCompleted)
        {
            var orders = new List<Order>();
            var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var evalOrders = baseNoteRepository.GetEvalOrders(Current.AgencyId, Guid.Empty, evalOrdersIds);
            if (evalOrders != null && evalOrders.Count > 0)
            {
                var canPrint = Current.Permissions.IsInPermission(Service, ParentPermission.Schedule, PermissionActions.Print);
                evalOrders.ForEach(eval =>
                {
                    var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                    var task = evalOrdersSchedule.SingleOrDefault(s => s.Id == eval.Id);
                    if (task != null)
                    {
                        var order = new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                EpisodeId = eval.EpisodeId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PhysicianAccess = physician != null && physician.PhysicianAccess,
                                CreatedDate = task.VisitDate,
                                Service = this.Service,
                                DocumentType = DocumentType.Notes.ToString().ToLower(),
                                CanUserPrint = canPrint
                            };
                        if (isCompleted)
                        {
                            order.SendDate = eval.SentDate;
                            order.ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate;
                            order.PhysicianSignatureDate = eval.PhysicianSignatureDate;
                        }
                        else if (isPending)
                        {
                            order.SendDate = eval.SentDate;
                            order.ReceivedDate = DateTime.Today;
                            order.PhysicianSignatureDate = eval.PhysicianSignatureDate;
                        }
                        orders.Add(order);
                    }
                });
            }
            return orders;
        }

        public List<Order> GetEvalOrders(Guid patientId, List<T> evalOrdersSchedule)
        {
            var orders = new List<Order>();
            var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
            var evalOrders = baseNoteRepository.GetEvalOrders(Current.AgencyId, patientId, evalOrdersIds);
            if (evalOrders != null && evalOrders.Count > 0)
            {
                var canPrint = Current.Permissions.IsInPermission(Service, ParentPermission.Schedule, PermissionActions.Print);
                evalOrders.ForEach(eval =>
                {
                    var task = evalOrdersSchedule.SingleOrDefault(s => s.Id == eval.Id);
                    if (task != null)
                    {

                        AgencyPhysician physician = null;
                        if (!eval.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                        }
                        else
                        {
                            if (eval.PhysicianData.IsNotNullOrEmpty())
                            {
                                var oldPhysician = eval.PhysicianData.ToObject<AgencyPhysician>();
                                if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                }
                            }
                        }
                        orders.Add(new Order
                        {
                            Id = eval.Id,
                            PatientId = eval.PatientId,
                            EpisodeId = eval.EpisodeId,
                            Type = GetEvaluationOrderType(eval.NoteType),
                            Text = GetEvaluationDisciplineType(eval.NoteType).GetDescription(),
                            Number = eval.OrderNumber,
                            PatientName = eval.DisplayName,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null && physician.PhysicianAccess,
                            DocumentType = DocumentType.Notes.ToString().ToLower(),
                            CreatedDate = task.VisitDate,
                            ReceivedDate = task.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature) ? eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate : DateTime.MinValue,
                            SendDate = task.Status == ((int)ScheduleStatus.EvalSentToPhysician) || task.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically) || task.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature) || task.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? eval.SentDate : DateTime.MinValue,
                            Status = eval.Status,
                            Service = Service,
                            CanUserPrint = canPrint
                        });
                    }
                });
            }
            return orders;
        }

        #endregion

        #region Note Supply

        public bool AddNoteSupply(Guid patientId, Guid eventId, Supply supply, out string errorMessage)
        {
            var result = false;
            errorMessage = "";
            var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
            if (patientVisitNote != null)
            {
                var supplies = new List<Supply>();
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    errorMessage = "A supply was not chosen. Either select one in the auto complete or create a new supply from the supply list.";
                    return result;
                }
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.IsNotNullOrEmpty())
                    {
                        var existingSupply = supplies.Find(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (existingSupply != null)
                        {
                            errorMessage = "This supply already exists.";
                            return result;
                        }
                        else
                        {
                            supplies.Add(supply);
                        }
                    }
                    else
                    {
                        supplies = new List<Supply> { supply };
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                patientVisitNote.Supply = supplies.ToXml();
                patientVisitNote.IsSupplyExist = true;
                if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid patientId, Guid eventId, Supply supply, out string errorMessage)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            errorMessage = "";
            var result = false;
            var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (!supplies.Exists(s => s.UniqueIdentifier != supply.OldUniqueIdentifier && s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        if (supplies.Exists(s => s.UniqueIdentifier == supply.OldUniqueIdentifier))
                        {
                            var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.OldUniqueIdentifier);
                            if (editSupply != null)
                            {
                                editSupply.UniqueIdentifier = supply.UniqueIdentifier;
                                editSupply.Quantity = supply.Quantity;
                                editSupply.UnitCost = supply.UnitCost;
                                editSupply.Description = supply.Description;
                                editSupply.DateForEdit = supply.DateForEdit;
                                editSupply.Code = supply.Code;
                                editSupply.RevenueCode = supply.RevenueCode;
                                patientVisitNote.Supply = supplies.ToXml();
                                patientVisitNote.IsSupplyExist = true;
                                if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    result = true;
                                }
                            }
                        }
                        else
                        {
                            errorMessage = "The supply cannot be found.";
                            result = false;
                        }
                    }
                    else
                    {
                        errorMessage = "The supply you have changed this to already exists.";
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteNoteSupply(Guid patientId, Guid eventId, Guid id)
        {
            var result = false;
            var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == id))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == id)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public List<Supply> GetNoteSupplies(Guid patientId, Guid eventId)
        {
            var list = new List<Supply>();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    list = patientVisitNote.Supply.ToObject<List<Supply>>();
                }
            }
            return list;
        }

        public Supply GetNoteSupply(Guid patientId, Guid eventId, Guid id)
        {
            var supply = new Supply();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var list = patientVisitNote.Supply.ToObject<List<Supply>>() ?? new List<Supply>();
                    if (list.IsNotNullOrEmpty())
                    {
                        supply = list.FirstOrDefault(s => s.UniqueIdentifier == id);
                        if (supply != null)
                        {
                            supply.OldUniqueIdentifier = supply.UniqueIdentifier;
                        }
                    }
                }
            }
            return supply;
        }

        #endregion

        #region Wound Care

        public VisitNoteViewData GetWoundCareNoteViewData(Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData { Service = this.Service, WoundCareJson = "[]" };
            var patientVisitNote = GetVisitNoteWithQuestions(eventId, patientId);
            if (patientVisitNote != null)
            {
                viewData.Questions = patientVisitNote.ToWoundCareDictionary();
                viewData.WoundCareJson = WoundHelper<NotesQuestion>.GetPositions(viewData.Questions);
                viewData.PatientId = patientVisitNote.PatientId;
                viewData.EpisodeId = patientVisitNote.EpisodeId;
                viewData.EventId = patientVisitNote.Id;
                viewData.Version = patientVisitNote.Version;
            }
            else
            {
                viewData.PatientId = patientId;
                viewData.EventId = eventId;
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return viewData;
        }

        public JsonViewData SaveWoundCare(WoundSaveArguments saveArguments, HttpFileCollectionBase httpFiles, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care could not be saved." };
            var patientVisitNote = GetVisitNoteWithQuestions(saveArguments.Id, saveArguments.PatientGuid);
            if (patientVisitNote != null)
            {
                IDictionary<string, NotesQuestion> questions = patientVisitNote.ToWoundCareDictionary();
                var taskAssets = new List<Guid>();
                List<Guid> savedAssetIds = null;
                var assetXml = string.Empty;
                bool hasAssets = saveArguments.Action != "MainSave" && AssetHelper.ContainsAssets(httpFiles);
                if (hasAssets)
                {
                    var data = baseScheduleRepository.GetScheduleTaskByColumns(Current.AgencyId, saveArguments.PatientGuid, saveArguments.Id, "Asset");
                    if (data.IsNotNullOrEmpty())
                    {
                        assetXml = data["Asset"];
                        taskAssets = (assetXml.IsNotNullOrEmpty() ? assetXml.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();
                    }
                }
                patientVisitNote.WoundQuestions = WoundHelper<NotesQuestion>.Update(saveArguments, httpFiles, formCollection, questions, taskAssets, out savedAssetIds);
                if (hasAssets && savedAssetIds.IsNullOrEmpty())
                {
                    viewData.errorMessage = "A problem occured while saving the attached asset.";
                    return viewData;
                }
                bool isTransactionSuccessful = false;
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        if (!hasAssets || baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, saveArguments.PatientGuid, saveArguments.Id, 0, taskAssets.ToXml(), null))
                        {
                            if (patientVisitNote.IsWoundCare || baseNoteRepository.UpdateVisitNoteWoundCare(Current.AgencyId, saveArguments.PatientGuid, saveArguments.Id, true))
                            {
                                if (mongoRepository.UpdateNoteQuestion(new QuestionData(patientVisitNote)))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Wound has been successfully saved.";
                                    viewData.PatientId = saveArguments.PatientGuid;
                                    viewData.Service = Service.ToString();
                                    viewData.ServiceId = (int)Service;
                                    viewData.IsDataCentersRefresh = hasAssets;
                                    viewData.IsMyScheduleTaskRefresh = hasAssets;
                                    isTransactionSuccessful = true;
                                    ts.Complete();
                                }
                                else
                                {
                                    ts.Dispose();
                                }
                            }
                            else
                            {
                                ts.Dispose();
                            }
                        }
                        else
                        {
                            ts.Dispose();
                        }
                    }
                }
                if (isTransactionSuccessful)
                {
                    Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, EnumExtensions.ToEnum(patientVisitNote.NoteType, DisciplineTasks.NoDiscipline), "Wound Care is added/updated.");
                }
                else if (hasAssets)
                {
                    foreach (var assetId in savedAssetIds)
                    {
                        AssetHelper.Remove(assetId);
                    }
                }
            }
            return viewData;
        }

        public bool DeleteWoundCareAsset(Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            var scheduleEventDictionary = baseScheduleRepository.GetScheduleTaskByColumns(Current.AgencyId, patientId, eventId, "Asset", "DisciplineTask", "EpisodeId");
            if (scheduleEventDictionary.IsNotNullOrEmpty())
            {
                var questionData = mongoRepository.GetNoteQuestion(Current.AgencyId, eventId);
                if (questionData != null && questionData.WoundNoteQuestion.IsNotNullOrEmpty())
                {
                    var question = questionData.WoundNoteQuestion.FirstOrDefault();
                    if (question != null)
                    {
                        question.Answer = Guid.Empty.ToString();
                        if (scheduleEventDictionary["Asset"].IsNotNullOrEmpty())
                        {
                            var assets = scheduleEventDictionary["Asset"].ToObject<List<Guid>>();
                            if (AssetHelper.Delete(assetId, assets))
                            {
                                if (baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, patientId, eventId, 0, assets.ToXml()))
                                {
                                    if (mongoRepository.UpdateNoteQuestion(questionData))
                                    {
                                        int discipineTask = scheduleEventDictionary["DisciplineTask"].ToInteger();
                                        Auditor.Log(scheduleEventDictionary["EpisodeId"].ToGuid(), patientId, eventId, Actions.Edit, (DisciplineTasks)discipineTask, "Wound Care Asset was deleted.");
                                        result = true;
                                    }
                                    else
                                    {
                                        AssetHelper.Restore(assetId);
                                        assets.Add(assetId);
                                        baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, patientId, eventId, 0, assets.ToXml());
                                    }
                                }
                                else
                                {
                                    AssetHelper.Restore(assetId);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public NoteJsonViewData WoundSetPostion(Guid Id, Guid PatientId, WoundPosition woundToPositon)
        {
            var result = new NoteJsonViewData { isSuccessful = false, errorMessage = "The wound's position could not be saved." };
            var patientVisitNote = GetVisitNoteWithQuestions(Id, PatientId);
            if (patientVisitNote != null)
            {
                if (WoundHelper<NotesQuestion>.CheckAndSetPosition(patientVisitNote.WoundQuestions, woundToPositon))
                {
                    if (mongoRepository.UpdateNoteQuestion(new QuestionData(patientVisitNote)))
                    {
                        result.isSuccessful = true;
                        result.errorMessage = "Wound's position has been successfully updated.";
                    }
                }
            }
            return result;
        }

        public NoteJsonViewData WoundDelete(Guid Id, Guid PatientId, int WoundNumber, int WoundCount)
        {
            var result = new NoteJsonViewData { isSuccessful = false, errorMessage = "Unable to delete wound. Please try again." };
            var patientVisitNote = GetVisitNoteWithQuestions(Id, PatientId);
            if (patientVisitNote != null)
            {
                var woundCareQuestions = patientVisitNote.ToWoundCareDictionary();
                Guid assetId;
                List<Guid> oldAssetIds = null;
                if (WoundHelper<NotesQuestion>.Delete(WoundNumber, WoundCount, woundCareQuestions, out assetId))
                {
                    Func<Guid, Guid, Guid, Guid, bool> assetDeletion = (id, pid, agencyid, assetid) =>
                    {
                        var assetIds = baseScheduleRepository.GetScheduleTaskAssets(agencyid, pid, id);
                        oldAssetIds = assetIds.DeepClone();
                        if (AssetHelper.Delete(assetId, assetIds))
                        {
                            return baseScheduleRepository.UpdateScheduleTaskPartial(agencyid, pid, id, 0, assetIds.ToXml());
                        }
                        return false;
                    };
                    if (assetId.IsEmpty() || assetDeletion(Current.AgencyId, PatientId, Id, assetId))
                    {
                        if (mongoRepository.UpdateNoteQuestion(new QuestionData(patientVisitNote)))
                        {
                            result.isSuccessful = true;
                            result.errorMessage = "Wound has been deleted.";
                        }
                        else if (!assetId.IsEmpty())
                        {
                            AssetHelper.Restore(assetId);
                            baseScheduleRepository.UpdateScheduleTaskPartial(Current.AgencyId, PatientId, Id, 0, oldAssetIds.ToXml());
                        }
                    }
                }
            }
            return result;
        }

        public WoundViewData<NotesQuestion> GetWound(Guid Id, Guid PatientId, int WoundNumber)
        {
            var result = new WoundViewData<NotesQuestion>();
            var patientVisitNote = GetVisitNoteWithQuestions(Id, PatientId);
            if (patientVisitNote != null)
            {
                result.Position = new WoundPosition(WoundNumber, 0, 0);
                result.EventId = Id;
                result.PatientId = PatientId;
                result.Type = patientVisitNote.NoteType;
                result.Data = WoundHelper<NotesQuestion>.GetWound(patientVisitNote.ToWoundCareDictionary(), WoundNumber);
            }
            else
            {
                throw new HttpException("500");
            }
            return result;
        }

        #endregion

        #region Previous Notes

        public List<PreviousNote> GetPreviousNotes(Guid patientId, T scheduledEvent)
        {
            var schedules = new List<T>();
            if (DisciplineTaskFactory.SkilledNurseSharedFile().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.SkilledNurseSharedFile().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.PTNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.PTNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.PTEvalDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.PTEvalDisciplineTasks().ToArray(), ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.PTDischargeDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.PTDischargeDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.OTNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.OTNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.OTEvalDisciplineTasks(true).Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.OTEvalDisciplineTasks(true).ToArray(), ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.STNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.STNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.STEvalDisciplineTasks(true).Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.STEvalDisciplineTasks(true).ToArray(), ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().ToArray(), ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (scheduledEvent.Discipline == Disciplines.PT.ToString() || scheduledEvent.Discipline == Disciplines.OT.ToString() || scheduledEvent.Discipline == Disciplines.ST.ToString())
            {
                schedules = this.FindPreviousNotes(scheduledEvent, new string[] { scheduledEvent.Discipline }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.EvalNoteCompleted().ToArray());
            }
            else if (scheduledEvent.Discipline == Disciplines.HHA.ToString())
            {
                schedules = this.FindPreviousNotes(scheduledEvent, new string[] { Disciplines.HHA.ToString() }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (scheduledEvent.Discipline == Disciplines.Nursing.ToString())
            {
                schedules = this.FindPreviousNotes(scheduledEvent, new string[] { Disciplines.Nursing.ToString() }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            else if (DisciplineTaskFactory.DietDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = this.FindPreviousNotes(scheduledEvent, new string[] { Disciplines.Dietician.ToString() }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFactory.NoteCompleted(true).ToArray());
            }
            List<PreviousNote> notes = new List<PreviousNote>();
            if (schedules.IsNotNullOrEmpty())
            {
                foreach (var task in schedules)
                {
                    notes.Add(TaskHelperFactory<T>.CreatePreviousNote(task));
                }
            }
            return notes;
        }


        private List<T> FindPreviousNotes(T scheduledEvent, int[] disciplineTasks, int[] status)
        {
            return baseScheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, disciplineTasks, status);
        }

        private List<T> FindPreviousNotes(T scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status)
        {
            return baseScheduleRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, disciplines, disciplineTasks, status);
        }

        #endregion

        public NoteJsonViewData ProcessNoteAction(string button, FormCollection formCollection, SaveNoteArguments noteArguments, VitalSignLog vitalSignLog)
        {
            var viewData = new NoteJsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };

            if (noteArguments != null && noteArguments.Type.IsNotNullOrEmpty())
            {

                if (!noteArguments.EventId.IsEmpty() && !noteArguments.PatientId.IsEmpty())
                {
                    if (button == "save" || button == "complete" || button == "approve" || button == "return")
                    {
                        viewData = SaveNotes(noteArguments, button, formCollection, vitalSignLog);
                    }
                    else if (button == "return")
                    {
                        viewData = ReturnNotes(noteArguments);
                    }
                }
            }
            return viewData;
        }

        protected abstract E GetEpisodeForNote(SaveNoteArguments savingArguments);

        public NoteJsonViewData SaveNotes(SaveNoteArguments savingArguments, string button, FormCollection formCollection, VitalSignLog vitalSignLog)
        {
            var returnData = new NoteJsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
            var keys = formCollection.AllKeys;
            if (keys.IsNotNullOrEmpty())
            {
                if (savingArguments.Type.IsNotNullOrEmpty())
                {
                    var disciplineTask = lookUpRepository.GetDisciplineTask(savingArguments.DisciplineTask);
                    if (disciplineTask != null)
                    {
                        var episode = this.GetEpisodeForNote(savingArguments);
                        if (episode != null)
                        {
                            var rules = AddSaveNotesValidationRules(button, episode, savingArguments, disciplineTask);
                            var entityValidator = new EntityValidator(rules.ToArray());
                            entityValidator.Validate();
                            if (entityValidator.IsValid)
                            {
                                var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, savingArguments.PatientId, savingArguments.EventId);
                                if (patientVisitNote != null)
                                {
                                    bool isActionSet = false;
                                    var scheduleTask = baseScheduleRepository.GetScheduleTask(Current.AgencyId, savingArguments.PatientId, savingArguments.EventId);//this.GetScheduleTask(savingArguments.EpisodeId, savingArguments.PatientId, savingArguments.Id, savingArguments.DataType);
                                    if (scheduleTask != null)
                                    {
                                        CleanFormCollection(formCollection);
                                        PullExtraVitalsFromFormCollection(formCollection, savingArguments.Type, vitalSignLog);

                                        var oldPatientVisitNote = patientVisitNote.DeepClone();


                                        var questions = formCollection.ProcessNoteQuestions();
                                        patientVisitNote.Questions = questions;
                                        patientVisitNote.SelectedEpisodeId = savingArguments.SelectedEpisodeId;
                                        patientVisitNote.PhysicianId = savingArguments.PhysicianId;

                                        var oldTaskValues = scheduleTask.DeepClone();

                                        scheduleTask.SendAsOrder = savingArguments.SendAsOrder;
                                        scheduleTask.DisciplineTask = savingArguments.DisciplineTask;

                                        patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), savingArguments.DisciplineTask)).ToString();
                                        var isCaseManagementRefresh = false;
                                        //if (button == "return")
                                        //{
                                        //    isCaseManagementRefresh = ScheduleStatusFactory.CaseManagerStatus().Contains(oldTaskValues.Status);
                                        //    patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                        //    if (savingArguments.ReturnForSignature.IsNotNullOrEmpty())
                                        //    {
                                        //        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                                        //    }
                                        //    isActionSet = true;
                                        //}
                                        //else
                                        if (button == "save" || button == "complete" || button == "approve")
                                        {
                                            isActionSet = true;
                                            patientVisitNote.PhysicianData = GetPhysicianForVisitNote(patientVisitNote.PhysicianId, scheduleTask.DisciplineTask);
                                            scheduleTask.DisciplineTask = savingArguments.DisciplineTask;

                                            if (disciplineTask.IsTravelParameterVisible)
                                            {
                                                scheduleTask.Surcharge = savingArguments.Surcharge;
                                                scheduleTask.AssociatedMileage = savingArguments.AssociatedMileage;
                                            }
                                            scheduleTask = SetAppSpecificForSave(scheduleTask, savingArguments, disciplineTask);

                                            if (episode.Id != scheduleTask.EpisodeId)
                                            {
                                                scheduleTask.EpisodeId = episode.Id;
                                                scheduleTask.StartDate = episode.StartDate;
                                                scheduleTask.EndDate = episode.EndDate;
                                                patientVisitNote.EpisodeId = scheduleTask.EpisodeId;
                                            }

                                            if (button == "save")
                                            {
                                                var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                                                if (!(isQaByPass && oldTaskValues.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature)))
                                                {
                                                    patientVisitNote.Status = (int)ScheduleStatus.NoteSaved;
                                                }
                                            }
                                            else
                                            {

                                                scheduleTask.InPrintQueue = true;
                                                if (button == "complete")
                                                {
                                                    patientVisitNote.SignatureDate = savingArguments.SignatureDate;
                                                    patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);

                                                    var isQaByPass = Current.HasRight(this.Service, ParentPermission.QA, PermissionActions.ByPassQA);
                                                    isCaseManagementRefresh = !isQaByPass;
                                                    patientVisitNote.Status = (int)ScheduleStatus.NoteSubmittedWithSignature;
                                                    if (isQaByPass)
                                                    {
                                                        patientVisitNote.Status = SetNoteToCompletionStatus(patientVisitNote.Status, scheduleTask.Version, scheduleTask.DisciplineTask, savingArguments.SendAsOrder);
                                                    }
                                                    patientVisitNote.UserId = Current.UserId;
                                                }
                                                else if (button == "approve")
                                                {

                                                    if ((patientVisitNote.SignatureText.IsNullOrEmpty() || patientVisitNote.SignatureDate == DateTime.MinValue) &&
                                                    (savingArguments.Clinician.IsNullOrEmpty() || !savingArguments.SignatureDate.IsValid()))
                                                    {
                                                        returnData.isSuccessful = false;
                                                        returnData.errorMessage = "The note could not be approved because the Electronic Signature is missing. Please sign this note before continuing.";
                                                        return returnData;
                                                    }

                                                    patientVisitNote.SignatureDate = savingArguments.SignatureDate;
                                                    patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);

                                                    isCaseManagementRefresh = ScheduleStatusFactory.CaseManagerStatus().Contains(oldTaskValues.Status);
                                                    patientVisitNote.Status = SetNoteToCompletionStatus(patientVisitNote.Status, scheduleTask.Version, scheduleTask.DisciplineTask, savingArguments.SendAsOrder);

                                                    //TODO: make sure convert the return reason from the scheduletask as one entry to the returncomments table
                                                    //if (scheduleTask is ScheduleEvent)
                                                    //{
                                                    //    (scheduleTask as ScheduleEvent).ReturnReason = string.Empty;
                                                    //}
                                                }
                                                else if (button == "return")
                                                {
                                                    patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                                    if (savingArguments.ReturnForSignature.IsNotNullOrEmpty())
                                                    {
                                                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                                                    }
                                                }
                                            }
                                        }
                                        scheduleTask.Status = patientVisitNote.Status;
                                        if (isActionSet)
                                        {
                                            if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                                            {
                                                patientVisitNote.Modified = DateTime.Now;
                                                VitalSignLog oldVitalSign = null;
                                                EntityHelper.SetVitalSignLog(vitalSignLog, Current.AgencyId, scheduleTask.PatientId, scheduleTask.Id, scheduleTask.VisitDate, scheduleTask.TimeIn);
                                                if (!vitalSignLog.VitalSignsChanged || vitalSignRepository.AddOrUpdate(vitalSignLog, ref oldVitalSign))
                                                {
                                                    int returnCommentId = 0;
                                                    if (savingArguments.ReturnReason.IsNullOrEmpty() || baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, scheduleTask.EpisodeId, Current.UserId, scheduleTask.Id, savingArguments.ReturnReason), out returnCommentId))
                                                    {
                                                        if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                                                        {
                                                            if (mongoRepository.UpdateNoteQuestion(new QuestionData(patientVisitNote)))
                                                            {
                                                                returnData.Service = Service.ToString();
                                                                returnData.ServiceId = (int)Service;
                                                                returnData.PatientId = savingArguments.PatientId;
                                                                returnData.EpisodeId = savingArguments.EpisodeId;
                                                                returnData.UserIds = new List<Guid> { scheduleTask.UserId };
                                                                returnData.Command = button;
                                                                returnData.IsDataCentersRefresh = oldTaskValues.Status != (scheduleTask.Status) || oldTaskValues.DisciplineTask != scheduleTask.DisciplineTask;
                                                                returnData.IsCaseManagementRefresh = isCaseManagementRefresh;
                                                                returnData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(returnData.IsDataCentersRefresh, scheduleTask);
                                                                returnData.isSuccessful = true;
                                                                string messageWord = GenerateMessageWordBasedOnButton(button);
                                                                returnData.errorMessage = string.Format("The note was successfully {0}.", messageWord);
                                                                if (oldTaskValues.EpisodeId != scheduleTask.EpisodeId)
                                                                {
                                                                    returnData.isWarning = true;
                                                                    returnData.errorMessage += " The event task moved to other care period.";
                                                                }
                                                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                                                                {
                                                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, savingArguments.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, string.Empty);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                returnData.errorMessage = "A problem occured while saving the questions. Please try again.";
                                                                baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                                                                baseNoteRepository.UpdateVisitNote(oldPatientVisitNote);
                                                                vitalSignRepository.RestoreData(oldVitalSign, vitalSignLog);
                                                                if (returnCommentId > 0) baseScheduleRepository.RemoveReturnComment(returnCommentId);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            returnData.errorMessage = "A problem occured while saving the note. Please try again.";
                                                            baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                                                            vitalSignRepository.RestoreData(oldVitalSign, vitalSignLog);
                                                            if (returnCommentId > 0) baseScheduleRepository.RemoveReturnComment(returnCommentId);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnData.errorMessage = "A problem occured while saving the return comment. Please try again.";
                                                        baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                                                        vitalSignRepository.RestoreData(oldVitalSign, vitalSignLog);
                                                    }
                                                }
                                                else
                                                {
                                                    returnData.errorMessage = "A problem occured while saving the vital signs. Please try again.";
                                                    baseScheduleRepository.UpdateScheduleTask(oldTaskValues);
                                                }
                                            }
                                            else
                                            {
                                                returnData.errorMessage = "A problem occured while saving the task. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            returnData.errorMessage = "Unable to identify user action. Try again.";
                                        }
                                    }
                                    else
                                    {
                                        returnData.errorMessage = "There is no matching schedule for this visit. Try again.";
                                    }
                                }
                                else
                                {
                                    returnData.errorMessage = "There is no matching note for this visit. Try again.";
                                }
                            }
                            else
                            {
                                returnData.isSuccessful = false;
                                returnData.errorMessage = entityValidator.Message;
                            }
                        }
                        else
                        {
                            if (AgencyServices.PrivateDuty == this.Service)
                            {
                                returnData.errorMessage = "There is no matching care period for  this visit start .";
                            }
                            else
                            {
                                returnData.errorMessage = "There is no matching episode for  this visit.";
                            }
                        }
                    }
                }
            }
            return returnData;
        }

        public NoteJsonViewData ReturnNotes(SaveNoteArguments savingArguments)
        {
            var returnData = new NoteJsonViewData { isSuccessful = false, errorMessage = "The note could not be returned." };

            var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, savingArguments.PatientId, savingArguments.EventId);
            if (patientVisitNote != null)
            {
                var scheduleTask = baseScheduleRepository.GetScheduleTask(Current.AgencyId, savingArguments.PatientId, savingArguments.EventId);
                if (scheduleTask != null)
                {
                    var oldStatus = scheduleTask.Status;
                    var isCaseManagementRefresh = ScheduleStatusFactory.CaseManagerStatus().Contains(oldStatus);

                    patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                    if (savingArguments.ReturnForSignature.IsNotNullOrEmpty())
                    {
                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                    }

                    scheduleTask.Status = patientVisitNote.Status;

                    if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                    {
                        patientVisitNote.Modified = DateTime.Now;

                        if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                        {

                            returnData.Service = Service.ToString();
                            returnData.ServiceId = (int)Service;
                            returnData.PatientId = savingArguments.PatientId;
                            returnData.EpisodeId = savingArguments.EpisodeId;
                            returnData.UserIds = new List<Guid> { scheduleTask.UserId };
                            returnData.Command = "return";
                            returnData.IsDataCentersRefresh = oldStatus != (scheduleTask.Status);
                            returnData.IsCaseManagementRefresh = isCaseManagementRefresh;
                            returnData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(returnData.IsDataCentersRefresh, scheduleTask);
                            returnData.isSuccessful = true;
                            returnData.errorMessage = "The note was successfully returned.";
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, savingArguments.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, string.Empty);
                            }

                        }
                        else
                        {
                            returnData.errorMessage = "A problem occured while saving the note. Please try again.";
                            scheduleTask.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleTask);
                        }

                    }
                }
            }

            return returnData;
        }

        private void PullExtraVitalsFromFormCollection(FormCollection formCollection, string type, VitalSignLog vitalSignLog)
        {
            var pain = formCollection[type + "_GenericIntensityOfPain"] ?? formCollection[type + "_GenericPainLevel"];
            if (pain != null)
            {
                int painValue;
                vitalSignLog.Pain = int.TryParse(pain, out painValue) ? (int?)painValue : null;
                vitalSignLog.VitalSignsChanged = true;
            }
        }

        /// <summary>
        /// Cleans the note arguements and vital signs from the note
        /// </summary>
        /// <param name="formCollection"></param>
        private void CleanFormCollection(FormCollection formCollection)
        {
            formCollection.Remove("EventId");
            formCollection.Remove("PatientId");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("SelectedEpisodeId");
            formCollection.Remove("VisitDate");
            formCollection.Remove("EventDate");
            formCollection.Remove("AssociatedMileage");
            formCollection.Remove("Surcharge");
            formCollection.Remove("SendAsOrder");
            formCollection.Remove("TimeIn");
            formCollection.Remove("TimeOut");
            formCollection.Remove("Type");
            formCollection.Remove("DisciplineTask");
            formCollection.Remove("Clinician");
            formCollection.Remove("SignatureDate");
            formCollection.Remove("PhysicianId");
            formCollection.Remove("ReturnForSignature");
            formCollection.Remove("ReturnReason");

            formCollection.Remove("vitalSignLog.Id");
            formCollection.Remove("vitalSignLog.IsNew");
            formCollection.Remove("vitalSignLog.VitalSignsChanged");
            formCollection.Remove("vitalSignLog.Resp");
            formCollection.Remove("vitalSignLog.RespStatus");
            formCollection.Remove("vitalSignLog.Temp");
            formCollection.Remove("vitalSignLog.TempUnit");
            formCollection.Remove("vitalSignLog.TempLocation");
            formCollection.Remove("vitalSignLog.Weight");
            formCollection.Remove("vitalSignLog.WeightUnit");
            formCollection.Remove("vitalSignLog.Height");
            formCollection.Remove("vitalSignLog.HeightUnit");
            formCollection.Remove("vitalSignLog.Pulse");
            formCollection.Remove("vitalSignLog.PulseLocation");
            formCollection.Remove("vitalSignLog.PulseStatus");
            formCollection.Remove("vitalSignLog.Pulse2");
            formCollection.Remove("vitalSignLog.PulseLocation2");
            formCollection.Remove("vitalSignLog.PulseStatus2");
            formCollection.Remove("vitalSignLog.BloodGlucose");
            formCollection.Remove("vitalSignLog.BloodGlucoseType");
            formCollection.Remove("vitalSignLog.OxygenSaturation");
            formCollection.Remove("vitalSignLog.OxygenSaturationType");
            formCollection.Remove("vitalSignLog.BloodPressure");
            formCollection.Remove("vitalSignLog.BloodPressureSide");
            formCollection.Remove("vitalSignLog.BloodPressurePosition");
            formCollection.Remove("vitalSignLog.BloodPressure2");
            formCollection.Remove("vitalSignLog.BloodPressureSide2");
            formCollection.Remove("vitalSignLog.BloodPressurePosition2");
            formCollection.Remove("vitalSignLog.BloodPressure3");
            formCollection.Remove("vitalSignLog.BloodPressureSide3");
            formCollection.Remove("vitalSignLog.BloodPressurePosition3");
            formCollection.Remove("vitalSignLog.BloodPressure4");
            formCollection.Remove("vitalSignLog.BloodPressureSide4");
            formCollection.Remove("vitalSignLog.BloodPressurePosition4");
            formCollection.Remove("vitalSignLog.BloodPressure5");
            formCollection.Remove("vitalSignLog.BloodPressureSide5");
            formCollection.Remove("vitalSignLog.BloodPressurePosition5");
            formCollection.Remove("vitalSignLog.BloodPressure6");
            formCollection.Remove("vitalSignLog.BloodPressureSide6");
            formCollection.Remove("vitalSignLog.BloodPressurePosition6");
            formCollection.Remove("vitalSignLog.Comments");
        }

        private string GenerateMessageWordBasedOnButton(string button)
        {
            string messageWord = string.Empty;

            if (button == "complete")
            {
                messageWord = "submited";
            }
            else
            {
                messageWord = button + (button.EndsWith("e") ? "d" : "ed");
            }
            return messageWord;
        }

        // ReSharper disable ImplicitlyCapturedClosure
        protected abstract List<Validation> AddSaveNotesValidationRules(string button, E episode, SaveNoteArguments noteBasicData, DisciplineTask disciplineTask);
        //{
        //    var rules = new List<Validation>();


        //    if (button == "Complete" || button == "Approve")
        //    {
        //        rules.Add(new Validation(() => string.IsNullOrEmpty(noteBasicData.Clinician), "Clinician Signature is required."));
        //        rules.Add(new Validation(() => noteBasicData.Clinician.IsNotNullOrEmpty() && !ServiceHelper.IsSignatureCorrect(Current.UserId, noteBasicData.Clinician), "User Signature is not correct."));
        //        if (noteBasicData.DisciplineTask != (int)DisciplineTasks.DriverOrTransportationNote)
        //        {
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(noteBasicData.TimeIn), "Time-In is required. "));
        //        }
        //        if (noteBasicData.DisciplineTask != (int)DisciplineTasks.DriverOrTransportationNote)
        //        {
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(noteBasicData.TimeOut), "Time-Out is required. "));
        //        }
        //        rules.Add(new Validation(() => !noteBasicData.SignatureDate.IsValid(), "Signature date is not valid."));
        //        rules.Add(new Validation(() => !noteBasicData.SignatureDate.IsValid() || !(noteBasicData.SignatureDate.Date >= episode.StartDate.Date && noteBasicData.SignatureDate.Date <= DateTime.Now.Date), "Signature date is not the in valid range."));
        //        if (DisciplineTaskFactory.AllNoteOrders().Contains(noteBasicData.DisciplineTask) || noteBasicData.SendAsOrder)
        //        {
        //            rules.Add(new Validation(() => noteBasicData.PhysicianId.IsEmpty(), "Physician is required. "));
        //        }
        //    }
        //    return rules;
        //}

        protected void CommonSaveNotesValidationRules(List<Validation> rules, string button, SaveNoteArguments noteBasicData, E episode)
        {
            rules.Add(new Validation(() => !Enum.IsDefined(typeof(DisciplineTasks), noteBasicData.DisciplineTask), "Select the right task."));
            if (button == "Complete" || button == "Approve")
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(noteBasicData.Clinician), "Clinician Signature is required."));
                rules.Add(new Validation(() => noteBasicData.Clinician.IsNotNullOrEmpty() && !ServiceHelper.IsSignatureCorrect(Current.UserId, noteBasicData.Clinician), "User Signature is not correct."));
                if (noteBasicData.DisciplineTask != (int)DisciplineTasks.DriverOrTransportationNote)
                {
                    rules.Add(new Validation(() => string.IsNullOrEmpty(noteBasicData.TimeIn), "Time-In is required. "));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(noteBasicData.TimeOut), "Time-Out is required. "));
                }

                rules.Add(new Validation(() => !noteBasicData.SignatureDate.IsValid(), "Signature date is not valid."));
                rules.Add(new Validation(() => !noteBasicData.SignatureDate.IsValid() || !(noteBasicData.SignatureDate.Date >= episode.StartDate.Date && noteBasicData.SignatureDate.Date <= DateTime.Now.Date), "Signature date is not the in valid range."));
                if (DisciplineTaskFactory.AllNoteOrders().Contains(noteBasicData.DisciplineTask) || noteBasicData.SendAsOrder)
                {
                    rules.Add(new Validation(() => noteBasicData.PhysicianId.IsEmpty(), "Physician is required. "));
                }
            }
        }
        // ReSharper restore ImplicitlyCapturedClosure

        public JsonViewData ProcessNotes(Guid patientId, Guid eventId, string button, string reason)
        {
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = baseScheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                return ProcessNotes(scheduleEvent, button, reason);
            }
            return new JsonViewData(false, "Unable to find task. Please try again.");
        }

        public JsonViewData ProcessNotes(T task, string button, string reason)
        {
            //var result = false;
            var viewData = new JsonViewData(false);
            bool isNoteUpdates = true;
            bool isActionSet = false;
            viewData.Service = Service.ToString();
            if (task != null)
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (SharedDbConnectionScope scs = new SharedDbConnectionScope(ProviderFactory.GetProvider(baseScheduleRepository.connectionStringName)))
                    {
                        var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, task.PatientId, task.Id);
                        if (patientVisitNote != null)
                        {
                            viewData.PatientId = task.PatientId;
                            viewData.EpisodeId = task.EpisodeId;
                            viewData.UserIds = new List<Guid> { task.UserId };
                            var oldStatus = task.Status;
                            if (button == "Approve")
                            {
                                patientVisitNote.Status = SetNoteToCompletionStatus(patientVisitNote.Status, task.Version, task.DisciplineTask, task.SendAsOrder);
                                task.Status = patientVisitNote.Status;
                                task.InPrintQueue = true;
                                isActionSet = true;
                                patientVisitNote.Modified = DateTime.Now;

                            }
                            else if (button == "Return")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                task.Status = patientVisitNote.Status;
                                isActionSet = true;

                            }
                            else if (button == "Print")
                            {
                                task.InPrintQueue = false;
                                isNoteUpdates = false;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (baseScheduleRepository.UpdateScheduleTask(task))
                                {
                                    if (isNoteUpdates)
                                    {
                                        bool returnCommentStatus = true;
                                        if (reason.IsNotNullOrEmpty())
                                        {
                                            returnCommentStatus = baseScheduleRepository.AddReturnComment(new ReturnComment(Current.AgencyId, task.EpisodeId, Current.UserId, task.Id, reason));
                                        }
                                        if (returnCommentStatus)
                                        {
                                            patientVisitNote.Modified = DateTime.Now;
                                            if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                                            {
                                                viewData.Service = Service.ToString();
                                                viewData.ServiceId = (int)Service;
                                                viewData.IsDataCentersRefresh = oldStatus != task.Status;
                                                viewData.IsCaseManagementRefresh = viewData.IsDataCentersRefresh && (oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature) || oldStatus == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature));
                                                viewData.IsMyScheduleTaskRefresh = viewData.IsDataCentersRefresh && Current.UserId == task.UserId && !task.IsComplete && task.EventDate.Date >= DateTime.Now.AddDays(-89) && task.EventDate.Date <= DateTime.Now.AddDays(14);
                                                viewData.isSuccessful = true;
                                                ts.Complete();
                                            }
                                            else
                                            {
                                                viewData.errorMessage = "A problem occured while updating the " + task.DisciplineTaskName + ".";
                                                ts.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            viewData.errorMessage = "A problem occured while adding the return comment.";
                                            ts.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        viewData.isSuccessful = true;
                                        ts.Complete();
                                    }
                                }
                                else
                                {
                                    viewData.errorMessage = "A problem occured while updating the task.";
                                    ts.Dispose();
                                }
                            }
                        }
                    }
                }
                if (viewData.isSuccessful && isNoteUpdates && Enum.IsDefined(typeof(ScheduleStatus), task.Status))
                {
                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, Actions.StatusChange, (ScheduleStatus)task.Status, (DisciplineTasks)task.DisciplineTask, string.Empty);
                }
            }
            return viewData;
        }

        public JsonViewData ReopenVisitNote(T scheduleTask)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            if (scheduleTask != null)
            {
                var oldStatus = scheduleTask.Status;
                var description = string.Empty;

                viewData.PatientId = scheduleTask.PatientId;
                viewData.EpisodeId = scheduleTask.EpisodeId;
                viewData.UserIds = new List<Guid> { scheduleTask.UserId };

                scheduleTask.Status = ((int)ScheduleStatus.NoteReopened);
                var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, scheduleTask.PatientId, scheduleTask.Id);
                if (patientVisitNote != null)
                {
                    if (baseScheduleRepository.UpdateScheduleTask(scheduleTask))
                    {
                        patientVisitNote.UserId = scheduleTask.UserId;
                        patientVisitNote.Status = (int)ScheduleStatus.NoteReopened;
                        patientVisitNote.SignatureText = string.Empty;
                        patientVisitNote.SignatureDate = DateTime.MinValue;
                        patientVisitNote.PhysicianSignatureText = string.Empty;
                        patientVisitNote.ReceivedDate = DateTime.MinValue;

                        if (baseNoteRepository.UpdateVisitNote(patientVisitNote))
                        {
                            viewData.isSuccessful = true;
                            viewData.IsOrdersHistoryRefresh = oldStatus == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature);
                            viewData.IsOrdersToBeSentRefresh = oldStatus == ((int)ScheduleStatus.EvalToBeSentToPhysician);
                            viewData.IsOrdersPendingRefresh = oldStatus == ((int)ScheduleStatus.EvalSentToPhysician) || oldStatus == ((int)ScheduleStatus.EvalSentToPhysicianElectronically);
                            viewData.IsDataCentersRefresh = true;
                            viewData.Service = this.Service.ToString();
                            viewData.ServiceId = (int)this.Service;
                            viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, scheduleTask);
                            if (Enum.IsDefined(typeof(ScheduleStatus), scheduleTask.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleTask.EpisodeId, scheduleTask.PatientId, scheduleTask.Id, Actions.StatusChange, (ScheduleStatus)scheduleTask.Status, (DisciplineTasks)scheduleTask.DisciplineTask, description);
                            }
                        }
                        else
                        {
                            scheduleTask.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleTask);
                            viewData.isSuccessful = false;
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData ToggleVisitNote(T task, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("The Visit Note could not be {0}. Please try again.", isDeprecated ? "deleted" : "restored") };
            if (task != null)
            {
                if (baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, isDeprecated))
                {
                    if (baseNoteRepository.MarkVisitNoteAsDeleted(Current.AgencyId, task.PatientId, task.Id, isDeprecated))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, task.EpisodeId, task.PatientId, task.Id, isDeprecated ? Actions.Deleted : Actions.Restored, (DisciplineTasks)task.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.Service = Service.ToString();
                        viewData.ServiceId = (int)Service;
                        viewData.PatientId = task.PatientId;
                        viewData.EpisodeId = task.EpisodeId;
                        viewData.UserIds = new List<Guid> { task.UserId };
                        viewData.IsDataCentersRefresh = true;
                        viewData.IsMyScheduleTaskRefresh = TaskHelperFactory<T>.ShouldMyScheduleTaskRefresh(viewData.IsDataCentersRefresh, task);
                        viewData.errorMessage = string.Format("The Visit Note has been {0}.", isDeprecated ? "deleted" : "restored");
                    }
                    else
                    {
                        baseScheduleRepository.ToggleScheduledTasksDelete(Current.AgencyId, task.PatientId, new List<Guid>() { task.Id }, !isDeprecated);
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        public bool UpdateVisitNoteFromTaskDetail(T schedule, int type)
        {
            var result = false;
            var visitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.Id);
            if (visitNote != null)
            {
                visitNote.NoteType = ((DisciplineTasks)type).ToString();
                visitNote.EpisodeId = schedule.EpisodeId;
                visitNote.IsDeprecated = schedule.IsDeprecated;
                visitNote.Status = schedule.Status;
                result = baseNoteRepository.UpdateVisitNote(visitNote);
            }
            return result;
        }

        private PatientVisitNote GetVisitNoteWithQuestions(Guid eventId, Guid patientId)
        {
            var patientVisitNote = baseNoteRepository.GetVisitNote(Current.AgencyId, patientId, eventId);
            if (patientVisitNote != null)
            {
                SetQuestions(patientVisitNote);
            }
            return patientVisitNote;
        }

        protected string GetPhysicianForVisitNote(Guid physicianId, int type)
        {
            string physicianXml = string.Empty;
            if (DisciplineTaskFactory.AllNoteOrders().Contains(type))
            {
                var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                if (physician != null)
                {
                    physicianXml = physician.ToXml();
                }
            }
            return physicianXml;
        }

        protected abstract T SetAppSpecificForSave(T task, SaveNoteArguments savingArguments, DisciplineTask disciplineTask);

        protected virtual void SetTaskSpecificInformation(VisitNoteViewData noteViewData, T scheduleEvent, DisciplineTask disciplineTask, bool isNoteStillNotStarted)
        {
            if (isNoteStillNotStarted)
            {
                if (disciplineTask.IsNotOasisCarePlan)// (arguments.IsHHACarePlanNeeded || arguments.IsPASCarePlanNeeded)
                {
                    T pocEvent = GetNoteCarePlan(scheduleEvent, disciplineTask.CarePlanTaskId);
                    if (pocEvent != null)
                    {
                        noteViewData.CarePlanOrEvalId = pocEvent.Id;
                        noteViewData.CarePlanOrEvalGroup = DocumentType.Notes.ToString().ToLowerCase();


                        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HHAideVisit || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PASVisit)
                        {
                            var pocNote = new PatientVisitNote { Id = pocEvent.Id };
                            SetQuestions(pocNote);
                            if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HHAideVisit)// (arguments.IsHHACarePlanNeeded)
                            {
                                noteViewData.Questions = pocNote.ToHHADefaults();
                            }
                            else
                            {
                                noteViewData.Questions = pocNote.ToPASDefaults();
                            }
                        }

                    }
                }


                if (disciplineTask.IsVitalSignMerged)
                {
                    var noteQuestionBeforeVital = noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                    MergeVitalSignWithNote(scheduleEvent.PatientId, scheduleEvent.EpisodeId, noteViewData.StartDate, noteViewData.EndDate, scheduleEvent.EventDate, ref noteQuestionBeforeVital);
                    noteViewData.Questions = noteQuestionBeforeVital;
                }
            }
            else
            {
                if (disciplineTask.IsNotOasisCarePlan) //(arguments.IsEvalNeeded)
                {
                    T pocEvent = this.GetNoteCarePlan(scheduleEvent, disciplineTask.CarePlanTaskId);
                    if (pocEvent != null)
                    {
                        noteViewData.CarePlanOrEvalId = pocEvent.Id;
                        noteViewData.CarePlanOrEvalGroup = DocumentType.Notes.ToString().ToLowerCase();
                    }
                }
            }
        }

        protected abstract void SetTaskSpecificInformationPrint(VisitNoteViewData noteViewData, T scheduleEvent, DisciplineTask disciplineTask);

        protected void SetEpisodeRange(VisitNoteViewData noteViewData, Guid episodeId, Guid patientId)
        {
            var episode = baseEpisodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                noteViewData.EndDate = episode.EndDate;
                noteViewData.StartDate = episode.StartDate;
            }
        }

        protected VisitNoteViewData SkilledNurseVisitContent(T scheduleEvent, PatientVisitNote previousNote, VisitNoteViewData viewData)
        {
            viewData.IsWoundCareExist = previousNote.IsWoundCare;
            viewData.IsSupplyExist = previousNote.IsSupplyExist;

            var noteItems = previousNote.ToDictionary();
            var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
            nameArray.ForEach(name => noteItems.Remove(name));
            viewData.Questions = noteItems;
            previousNote.Questions = noteItems.Values.ToList();
            previousNote.Modified = DateTime.Now;
            var currentNote = baseNoteRepository.GetVisitNote(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
            if (currentNote != null)
            {
                var oldCurrentNote = currentNote.DeepClone();
                CopyNoteValues(currentNote, previousNote);
                var oldStatus = scheduleEvent.Status;
                scheduleEvent.Status = (int)ScheduleStatus.NoteSaved;
                if (baseScheduleRepository.UpdateScheduleTask(scheduleEvent))
                {
                    if (baseNoteRepository.UpdateVisitNote(currentNote))
                    {
                        if (mongoRepository.UpdateNoteQuestion(new QuestionData(currentNote)))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.LoadPreviousNote, (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                        }
                        else
                        {
                            scheduleEvent.Status = oldStatus;
                            baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                            baseNoteRepository.UpdateVisitNote(oldCurrentNote);
                            return null;
                        }
                    }
                    else
                    {
                        scheduleEvent.Status = oldStatus;
                        baseScheduleRepository.UpdateScheduleTask(scheduleEvent);
                        return null;
                    }
                }
            }
            return viewData;
        }

        protected int SetNoteToCompletionStatus(int status, int version, int disciplineTask, bool sendAsOrder)
        {
            status = ((int)ScheduleStatus.NoteCompleted);
            if (DisciplineTaskFactory.AllNoteOrders().Contains(disciplineTask))
            {
                if (sendAsOrder || (version == 0 || version == 1))
                {
                    status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                }
            }
            return status;
        }

        protected Guid GetPhysicianFromNotes(int task, IDictionary<string, NotesQuestion> questions)
        {
            var physicianId = Guid.Empty;
            NotesQuestion physicianQuestion = null;
            if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => (int)d == task))
            {
                if (questions.TryGetValue("PhysicianId", out physicianQuestion))
                {
                    if (physicianQuestion != null && physicianQuestion.Answer.IsNotNullOrEmpty() && physicianQuestion.Answer.IsGuid())
                    {
                        physicianId = physicianQuestion.Answer.ToGuid();
                    }
                }
            }
            else
            {
                if (questions.TryGetValue("Physician", out physicianQuestion))
                {
                    if (physicianQuestion != null && physicianQuestion.Answer.IsNotNullOrEmpty() && physicianQuestion.Answer.IsGuid())
                    {
                        physicianId = physicianQuestion.Answer.ToGuid();
                    }
                }
            }
            return physicianId;
        }

        protected T GetNoteCarePlan(T evnt, int task)
        {
            T carePlan = null;
            var tasks = DisciplineTaskFactory.NoteCarePlanFromDisciplineTask(task);
            if (tasks != null && tasks.Count > 0)
            {
                carePlan = baseScheduleRepository.GetCarePlan(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.StartDate, evnt.EndDate, evnt.StartDate, evnt.EventDate.AddDays(-1), tasks);
            }
            return carePlan;
        }

        protected OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType = OrderType.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                }
            }
            return orderType;
        }

        protected DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        task = DisciplineTasks.MSWEvaluationAssessment;
                        break;
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        task = DisciplineTasks.SixtyDaySummary;
                        break;
                    case "PTReassessment":
                        task = DisciplineTasks.PTReassessment;
                        break;
                    case "OTReassessment":
                        task = DisciplineTasks.OTReassessment;
                        break;
                }
            }
            return task;
        }

        protected OrderType GetEvaluationOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        protected DisciplineTasks GetEvaluationDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        protected void SetQuestions(PatientVisitNote note)
        {
            try
            {
                var questionData = mongoRepository.GetNoteQuestion(Current.AgencyId, note.Id);
                if (questionData != null)
                {
                    note.Questions = questionData.Question ?? new List<NotesQuestion>();
                    if (note.IsWoundCare)
                    {
                        note.WoundQuestions = questionData.WoundNoteQuestion ?? new List<NotesQuestion>();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        protected PatientVisitNote CopyNoteValues(PatientVisitNote baseNote, PatientVisitNote noteToCopy)
        {
            PatientVisitNote note = baseNote ?? new PatientVisitNote();
            if (noteToCopy != null)
            {
                note.Modified = note.Modified;
                note.IsWoundCare = note.IsWoundCare;
                note.Questions = noteToCopy.Questions.IsNotNullOrEmpty() ? new List<NotesQuestion>(note.Questions) : new List<NotesQuestion>();
                note.WoundQuestions = noteToCopy.WoundQuestions.IsNotNullOrEmpty() ? new List<NotesQuestion>(note.WoundQuestions) : new List<NotesQuestion>();
                note.Supply = noteToCopy.Supply;
                note.IsSupplyExist = noteToCopy.IsSupplyExist;
            }
            return note;
        }

        protected abstract List<VitalSign> GetAppSpecificVitalSigns(Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime eventDate);

        protected void MergeVitalSignWithNote(Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime eventDate, ref IDictionary<string, NotesQuestion> noteQuestions)
        {
            noteQuestions = noteQuestions ?? new Dictionary<string, NotesQuestion>();
            var vitalSigns = GetAppSpecificVitalSigns(patientId, episodeId, startDate, endDate, eventDate);
            if (vitalSigns != null && vitalSigns.Count > 0)
            {
                #region BP

                var bp = new List<string>();
                var bpSysLow = int.MinValue;
                var bpSysHigh = int.MaxValue;
                var bpDiaLow = int.MinValue;
                var bpDiaHigh = int.MaxValue;

                var bpSitLeft = vitalSigns.Where(v => v.BPSittingLeft.IsNotNullOrEmpty()).Select(v => v.BPSittingLeft).ToList();
                var bpSitRight = vitalSigns.Where(v => v.BPSittingRight.IsNotNullOrEmpty()).Select(v => v.BPSittingRight).ToList();
                var bpStandLeft = vitalSigns.Where(v => v.BPStandingLeft.IsNotNullOrEmpty()).Select(v => v.BPStandingLeft).ToList();
                var bpStandRight = vitalSigns.Where(v => v.BPStandingRight.IsNotNullOrEmpty()).Select(v => v.BPStandingRight).ToList();
                var bpLyLeft = vitalSigns.Where(v => v.BPLyingLeft.IsNotNullOrEmpty()).Select(v => v.BPLyingLeft).ToList();
                var bpLyRight = vitalSigns.Where(v => v.BPLyingRight.IsNotNullOrEmpty()).Select(v => v.BPLyingRight).ToList();

                if (bpSitLeft.IsNotNullOrEmpty()) bp.AddRange(bpSitLeft.AsEnumerable());
                if (bpSitRight.IsNotNullOrEmpty()) bp.AddRange(bpSitRight.AsEnumerable());
                if (bpStandLeft.IsNotNullOrEmpty()) bp.AddRange(bpStandLeft.AsEnumerable());
                if (bpStandRight.IsNotNullOrEmpty()) bp.AddRange(bpStandRight.AsEnumerable());
                if (bpLyLeft.IsNotNullOrEmpty()) bp.AddRange(bpLyLeft.AsEnumerable());
                if (bpLyRight.IsNotNullOrEmpty()) bp.AddRange(bpLyRight.AsEnumerable());

                if (bp.IsNotNullOrEmpty())
                {
                    bpSysLow = bp.Select(v =>
                    {
                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (min != null && min.Length > 1)
                        {
                            if (min[0].IsInteger())
                            {
                                return min[0].ToInteger();
                            }
                        }
                        return int.MinValue;
                    }).Min();

                    bpSysHigh = bp.Select(v =>
                    {
                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (max != null && max.Length > 1)
                        {
                            if (max[0].IsInteger())
                            {
                                return max[0].ToInteger();
                            }
                        }
                        return int.MaxValue;
                    }).Max();

                    bpDiaLow = bp.Select(v =>
                    {
                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (min != null && min.Length > 1)
                        {
                            if (min[1].IsInteger())
                            {
                                return min[1].ToInteger();
                            }
                        }
                        return int.MinValue;
                    }).Min();

                    bpDiaHigh = bp.Select(v =>
                    {
                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (max != null && max.Length > 1)
                        {
                            if (max[1].IsInteger())
                            {
                                return max[1].ToInteger();
                            }
                        }
                        return int.MaxValue;
                    }).Max();
                }

                if (noteQuestions.ContainsKey("VitalSignBPMax"))
                {
                    noteQuestions["VitalSignBPMax"].Answer = bpSysHigh != int.MaxValue && bpSysHigh > 0 ? bpSysHigh.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPMax", new NotesQuestion { Name = "VitalSignBPMax", Answer = bpSysHigh != int.MaxValue && bpSysHigh > 0 ? bpSysHigh.ToString() : string.Empty });
                }

                if (noteQuestions.ContainsKey("VitalSignBPMin"))
                {
                    noteQuestions["VitalSignBPMin"].Answer = bpSysLow != int.MinValue && bpSysLow > 0 ? bpSysLow.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPMin", new NotesQuestion { Name = "VitalSignBPMin", Answer = bpSysLow != int.MinValue && bpSysLow > 0 ? bpSysLow.ToString() : string.Empty });
                }

                if (noteQuestions.ContainsKey("VitalSignBPDiaMax"))
                {
                    noteQuestions["VitalSignBPDiaMax"].Answer = bpDiaHigh != int.MaxValue && bpDiaHigh > 0 ? bpDiaHigh.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPDiaMax", new NotesQuestion { Name = "VitalSignBPDiaMax", Answer = bpDiaHigh != int.MaxValue && bpDiaHigh > 0 ? bpDiaHigh.ToString() : string.Empty });
                }

                if (noteQuestions.ContainsKey("VitalSignBPDiaMin"))
                {
                    noteQuestions["VitalSignBPDiaMin"].Answer = bpDiaLow != int.MinValue && bpDiaLow > 0 ? bpDiaLow.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignBPDiaMin", new NotesQuestion { Name = "VitalSignBPDiaMin", Answer = bpDiaLow != int.MinValue && bpDiaLow > 0 ? bpDiaLow.ToString() : string.Empty });
                }

                #endregion

                #region HR

                var apicalPulseMax = int.MinValue;
                var apicalPulseMin = int.MaxValue;
                var apicalPulse = vitalSigns.Where(v => v.ApicalPulse > 0).Select(v => v.ApicalPulse).ToList();
                if (apicalPulse != null && apicalPulse.Count > 0)
                {
                    apicalPulseMax = apicalPulse.Max();
                    apicalPulseMin = apicalPulse.Min();
                }

                var radialPulseMax = int.MinValue;
                var radialPulseMin = int.MaxValue;
                var radialPulse = vitalSigns.Where(v => v.RadialPulse > 0).Select(v => v.RadialPulse).ToList();
                if (radialPulse != null && radialPulse.Count > 0)
                {
                    radialPulseMax = radialPulse.Max();
                    radialPulseMin = radialPulse.Min();
                }

                var maxHR = Math.Max(apicalPulseMax, radialPulseMax);
                if (noteQuestions.ContainsKey("VitalSignHRMax"))
                {
                    noteQuestions["VitalSignHRMax"].Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignHRMax", new NotesQuestion { Name = "VitalSignHRMax", Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty });
                }

                var minHR = Math.Min(apicalPulseMin, radialPulseMin);

                if (noteQuestions.ContainsKey("VitalSignHRMin"))
                {
                    noteQuestions["VitalSignHRMin"].Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty;
                }
                else
                {
                    noteQuestions.Add("VitalSignHRMin", new NotesQuestion { Name = "VitalSignHRMin", Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty });
                }
                #endregion

                #region Resp

                var respMax = double.MaxValue;
                var respMin = double.MinValue;
                var resp = vitalSigns.Where(v => v.Resp > 0).Select(v => v.Resp).ToList();
                if (resp != null && resp.Count > 0)
                {
                    respMin = resp.Min();
                    respMax = resp.Max();
                }

                if (noteQuestions.ContainsKey("VitalSignRespMax"))
                {
                    noteQuestions["VitalSignRespMax"].Answer = respMax != double.MaxValue && respMax > 0 ? respMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignRespMax", new NotesQuestion { Name = "VitalSignRespMax", Answer = respMax != double.MaxValue && respMax > 0 ? respMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignRespMin"))
                {
                    noteQuestions["VitalSignRespMin"].Answer = respMin != double.MinValue && respMin > 0 ? respMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignRespMin", new NotesQuestion { Name = "VitalSignRespMin", Answer = respMin != double.MinValue && respMin > 0 ? respMin.ToString() : "" });
                }

                #endregion

                #region Temp

                var tempMax = double.MaxValue;
                var tempMin = double.MinValue;
                var temp = vitalSigns.Where(v => v.Temp > 0).Select(v => v.Temp).ToList();
                bool tempHasDegrees = false;//vitalSigns.Exists(v => v.Temp.IsNotNullOrEmpty() && v.Temp.Contains("F") || v.Temp.Contains("f"));
                if (temp != null && temp.Count > 0)
                {
                    tempMax = temp.Max();
                    tempMin = temp.Min();
                }

                if (noteQuestions.ContainsKey("VitalSignTempMax"))
                {
                    noteQuestions["VitalSignTempMax"].Answer = tempMax != double.MaxValue && tempMax > 0 ? tempMax.ToString() + (tempHasDegrees ? "F" : "") : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignTempMax", new NotesQuestion { Name = "VitalSignTempMax", Answer = tempMax != double.MaxValue && tempMax > 0 ? tempMax.ToString() + (tempHasDegrees ? "F" : "") : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignTempMin"))
                {
                    noteQuestions["VitalSignTempMin"].Answer = tempMin != double.MinValue && tempMin > 0 ? tempMin.ToString() + (tempHasDegrees ? "F" : "") : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignTempMin", new NotesQuestion { Name = "VitalSignTempMin", Answer = tempMin != double.MinValue && tempMin > 0 ? tempMin.ToString() + (tempHasDegrees ? "F" : "") : "" });
                }

                #endregion

                #region BS

                var bsMax = double.MaxValue;
                var bsMin = double.MinValue;
                var bsAllMax = vitalSigns.Where(v => v.BSMax > 0).Select(v => v.BSMax).ToList();
                var bsAllMin = vitalSigns.Where(v => v.BSMin > 0).Select(v => v.BSMin).ToList();
                if (bsAllMax != null && bsAllMax.Count > 0)
                {
                    bsMax = bsAllMax.Max();
                }
                if (bsAllMin != null && bsAllMin.Count > 0)
                {
                    bsMin = bsAllMin.Min();
                }

                if (noteQuestions.ContainsKey("VitalSignBGMax"))
                {
                    noteQuestions["VitalSignBGMax"].Answer = bsMax != double.MaxValue && bsMax > 0 ? bsMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignBGMax", new NotesQuestion { Name = "VitalSignBGMax", Answer = bsMax != double.MaxValue && bsMax > 0 ? bsMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignBGMin"))
                {
                    noteQuestions["VitalSignBGMin"].Answer = bsMin != double.MinValue && bsMin > 0 ? bsMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignBGMin", new NotesQuestion { Name = "VitalSignBGMin", Answer = bsMin != double.MinValue && bsMin > 0 ? bsMin.ToString() : "" });
                }
                #endregion

                #region Weight

                var weightMax = double.MaxValue;
                var weightMin = double.MinValue;
                var weight = vitalSigns.Where(v => v.Weight > 0).Select(v => v.Weight).ToList();
                if (weight != null && weight.Count > 0)
                {
                    weightMin = weight.Min();
                    weightMax = weight.Max();
                }

                if (noteQuestions.ContainsKey("VitalSignWeightMax"))
                {
                    noteQuestions["VitalSignWeightMax"].Answer = weightMax != double.MaxValue && weightMax > 0 ? weightMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignWeightMax", new NotesQuestion { Name = "VitalSignWeightMax", Answer = weightMax != double.MaxValue && weightMax > 0 ? weightMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignWeightMin"))
                {
                    noteQuestions["VitalSignWeightMin"].Answer = weightMin != double.MinValue && weightMin > 0 ? weightMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignWeightMin", new NotesQuestion { Name = "VitalSignWeightMin", Answer = weightMin != double.MinValue && weightMin > 0 ? weightMin.ToString() : "" });
                }

                #endregion

                #region Pain

                var painMax = int.MaxValue;
                var painMin = int.MinValue;
                var pain = vitalSigns.Where(v => v.PainLevel > 0).Select(v => v.PainLevel).ToList();
                if (pain != null && pain.Count > 0)
                {
                    painMin = pain.Min();
                    painMax = pain.Max();
                }

                if (noteQuestions.ContainsKey("VitalSignPainMax"))
                {
                    noteQuestions["VitalSignPainMax"].Answer = painMax != int.MaxValue && painMax > 0 ? painMax.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignPainMax", new NotesQuestion { Name = "VitalSignPainMax", Answer = painMax != int.MaxValue && painMax > 0 ? painMax.ToString() : "" });
                }

                if (noteQuestions.ContainsKey("VitalSignPainMin"))
                {
                    noteQuestions["VitalSignPainMin"].Answer = painMin != int.MinValue && painMin > 0 ? painMin.ToString() : "";
                }
                else
                {
                    noteQuestions.Add("VitalSignPainMin", new NotesQuestion { Name = "VitalSignPainMin", Answer = painMin != int.MinValue && painMin > 0 ? painMin.ToString() : "" });
                }

                #endregion
            }
        }

        protected List<VitalSignLogListItem> NewVitalSignFromList(Guid patientId, List<T> scheduleEvents)
        {
            var list = new List<VitalSignLogListItem>();
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var vitalSigns = vitalSignRepository.GetPatientVitalSignLogs(patientId, Current.AgencyId, scheduleEvents.Select(s => s.Id).ToList());
                if (vitalSigns.IsNotNullOrEmpty())
                {
                    vitalSigns.ForEach(v =>
                    {
                        var vitalSign = new VitalSignLogListItem();
                        var task = scheduleEvents.FirstOrDefault(t => t.Id == v.EntityId);
                        vitalSign.TempNum = v.Temp.HasValue ? v.Temp.Value : 0;
                        vitalSign.Temp = v.Temp.HasValue ? v.Temp + " °" + Enum.GetName(typeof(TemperatureUnit), v.TempUnit) : string.Empty;
                        vitalSign.Resp = v.Resp;
                        vitalSign.OxygenSaturation = v.OxygenSaturation;
                        vitalSign.Pulse = v.PulseToDisplay();
                        vitalSign.AddPulse(v.Pulse, v.PulseLocation, v.PulseStatus);
                        vitalSign.AddPulse(v.Pulse2, v.PulseLocation2, v.PulseStatus2);
                        vitalSign.BS = v.BloodGlucose;
                        vitalSign.Weight = v.Weight;
                        vitalSign.WeightUnit = Enum.GetName(typeof(WeightUnit), v.WeightUnit);
                        vitalSign.Pain = v.Pain;
                        vitalSign.Date = v.VisitDate;
                        vitalSign.AddBloodPressure(v.BloodPressure, v.BloodPressurePosition, v.BloodPressureSide);
                        vitalSign.AddBloodPressure(v.BloodPressure2, v.BloodPressurePosition2, v.BloodPressureSide2);
                        vitalSign.AddBloodPressure(v.BloodPressure3, v.BloodPressurePosition3, v.BloodPressureSide3);
                        vitalSign.AddBloodPressure(v.BloodPressure4, v.BloodPressurePosition4, v.BloodPressureSide4);
                        vitalSign.AddBloodPressure(v.BloodPressure5, v.BloodPressurePosition5, v.BloodPressureSide5);
                        vitalSign.AddBloodPressure(v.BloodPressure6, v.BloodPressurePosition6, v.BloodPressureSide6);
                        string maxBP, minBP;
                        v.MinMaxBloodPressures(out maxBP, out minBP);
                        vitalSign.MaxBP = maxBP;
                        vitalSign.MinBP = minBP;
                        if (task != null)
                        {
                            vitalSign.Date = vitalSign.Date.IsValid() ? vitalSign.Date : task.VisitDate;
                            vitalSign.Task = task.DisciplineTaskName;
                            vitalSign.EmployeeName = UserEngine.GetName(task.UserId, Current.AgencyId);
                        }
                        list.Add(vitalSign);
                    });
                }
            }
            return list;
        }
    }
}
