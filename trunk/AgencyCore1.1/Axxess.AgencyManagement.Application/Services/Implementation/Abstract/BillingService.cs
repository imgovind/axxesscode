﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using System.Net;
    using System.IO;

    using iTextSharp.text;

    using ViewData;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Logging;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;


    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Helpers;

    public abstract class BillingServiceAbstract<T> where T : ITask, new()
    {
        #region Private Members

        protected IPatientRepository patientRepository;
        private readonly PatientProfileAbstract basePatientProfileRepository;
        private readonly MongoAbstract baseMongoRepository;
        protected IAgencyRepository agencyRepository;
        protected IPhysicianRepository physicianRepository;
        protected ILookupRepository lookUpRepository;
        private readonly BillingAbstract baseBillingRepository;
        private readonly TaskScheduleAbstract<T> baseScheduleRepository;
        protected AgencyServices Service { get; set; }

        #endregion

        #region Constructor

        protected BillingServiceAbstract(TaskScheduleAbstract<T> baseScheduleRepository, BillingAbstract baseBillingRepository, PatientProfileAbstract basePatientProfileRepository, MongoAbstract baseMongoRepository)
        {
            this.baseBillingRepository = baseBillingRepository;
            this.baseScheduleRepository = baseScheduleRepository;
            this.basePatientProfileRepository = basePatientProfileRepository;
            this.baseMongoRepository = baseMongoRepository;
        }

        #endregion

        #region Public Methods

        public ManagedClaim CreateManagedClaim(Patient patient, Profile profile, DateTime startDate, DateTime endDate, int insuranceId)
        {
            var managedClaim = new ManagedClaim
            {
                Id = Guid.NewGuid(),
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeStartDate = startDate,
                EpisodeEndDate = endDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                InsuranceIdNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = profile.StartofCareDate,
                AreOrdersComplete = false,
                AdmissionSource = profile.AdmissionSource,
                PatientStatus = profile.Status,
                UB4PatientStatus = profile.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty),
                Status = (int)ManagedClaimStatus.ClaimCreated,
                PrimaryInsuranceId = insuranceId,
                DiagnosisCode = new DiagnosisCodes().ToXml(),
                ConditionCodes = new ConditionCodes().ToXml(),
                Created = DateTime.Now
            };
            return managedClaim;
        }

        public ManagedClaim GetManagedClaim(Guid Id)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                if (claim.ConditionCodes.IsNotNullOrEmpty())
                {
                    claim.ConditionCodesObject = claim.ConditionCodes.ToObject<ConditionCodes>();
                }
                if (claim.DiagnosisCode.IsNotNullOrEmpty())
                {
                    claim.DiagnosisCodesObject = claim.DiagnosisCode.ToObject<DiagnosisCodes>();
                }
            }
            return claim;
        }

        public ManagedClaim GetManagedClaim(Guid patientId, Guid Id)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                if (claim.ConditionCodes.IsNotNullOrEmpty())
                {
                    claim.ConditionCodesObject = claim.ConditionCodes.ToObject<ConditionCodes>();
                }
                if (claim.DiagnosisCode.IsNotNullOrEmpty())
                {
                    claim.DiagnosisCodesObject = claim.DiagnosisCode.ToObject<DiagnosisCodes>();
                }
            }
            return claim;
        }

        /// <summary>
        /// Gets the data used by the Managed Claim Verify page.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patientId"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetManagedClaimForVerify(Guid id, Guid patientId, AgencyServices service)
        {
            var data = baseBillingRepository.GetManagedClaimJsonByColumns(Current.AgencyId, patientId, id, "IsSupplyVerified", "IsInfoVerified", "IsVisitVerified", "IsInsuranceVerified");
            if (data != null)
            {
                data.Add("Id", id.ToString());
                data.Add("PatientId", patientId.ToString());
                data.Add("Service", ((int)service).ToString());
                string displayName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
                data.Add("DisplayName", displayName);
            }
            else
            {
                throw new Exception("A problem occured, while getting the managed claim.");
            }
            return data;
            //return new PatientNameViewData { EntityId = id, PatientId = patientId, Service = service, DisplayName =  };
        }

        public NewManagedClaimViewData GetNewMangedClaimViewData(Guid patientId)
        {
            var newClaimData = new NewManagedClaimViewData();
            newClaimData.Service = this.Service;
            if (!patientId.IsEmpty())
            {
                var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    //newClaimData.Insurances = AgencyInformationHelper.Insurances(profile.PrimaryInsurance.ToString(), false, false);
                    newClaimData.SelecetdInsurance = profile.PrimaryInsurance.ToString();
                    newClaimData.PatientId = patientId;
                }
            }
            return newClaimData;
        }

        public BillingJsonViewData AddManagedClaim(Guid patientId, DateTime startDate, DateTime endDate, int insuranceId)
        {
            var rules = new List<Validation>();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            viewData.Service = this.Service.ToString();
            viewData.Category = ClaimTypeSubCategory.ManagedCare;
            if (!patientId.IsEmpty())
            {
                rules.Add(new Validation(() => startDate.Date <= DateTime.MinValue.Date, "The Start Date is invalid. Type a valid date or use the date picker."));
                rules.Add(new Validation(() => endDate.Date <= DateTime.MinValue.Date, "The End Date is invalid. Type a valid date or use the date picker"));
                rules.Add(new Validation(() => startDate.Date > endDate.Date, "The Start Date must be less than the End Date."));
                rules.Add(new Validation(() => insuranceId <= 0, "An Insurance must be selected."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                        if (profile != null)
                        {
                            var managedClaim = CreateManagedClaim(patient, profile, startDate, endDate, insuranceId);
                            SetInsurancePlanInfo(managedClaim, profile);
                            SetClaimPhysicianInfo(managedClaim);
                            SetInvoiceNumberAppSpecific(managedClaim);
                            SetPayorTypeAppSpecific(managedClaim);
                            if (baseBillingRepository.AddManagedClaim(managedClaim))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedClaimAdded, string.Empty);
                                viewData.PatientId = patientId;
                                viewData.IsManagedCareRefresh = true;
                                viewData.IsManagedCareHistoryRefresh = true;
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The claim has been added successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The patient " + this.Service.GetDescription() + " does not exist. Close this window and try again.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The Patient does not exist. Close this window and try again.";
                    }
                }
                else
                {
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            else
            {
                viewData.errorMessage = "Please verify the information provided.";
            }
            return viewData;
        }

        #region Managed Claim Steps | Get

        public ManagedClaim GetManagedClaimInfoLatest(Guid patientId, Guid Id)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                if (claim.Status == (int)ManagedClaimStatus.ClaimCreated)
                {
                    if (!claim.IsInfoVerified)
                    {
                        var patientWithProfile = basePatientProfileRepository.GetPatientWithProfileForClaim(Current.AgencyId, patientId);
                        if (patientWithProfile != null)
                        {

                            claim.FirstName = patientWithProfile.FirstName;
                            claim.LastName = patientWithProfile.LastName;
                            claim.PatientIdNumber = patientWithProfile.PatientIdNumber;
                            claim.DOB = patientWithProfile.DOB;
                            claim.Gender = patientWithProfile.Gender;
                            claim.AddressLine1 = patientWithProfile.AddressLine1;
                            claim.AddressLine2 = patientWithProfile.AddressLine2;
                            claim.AddressCity = patientWithProfile.AddressCity;
                            claim.AddressStateCode = patientWithProfile.AddressStateCode;
                            claim.AddressZipCode = patientWithProfile.AddressZipCode;

                            SetClaimPhysicianInfo(claim);

                            //managedClaim.PatientStatus = patientWithProfile.Profile.Status;
                            //managedClaim.UB4PatientStatus = patientWithProfile.Profile.Status == (int)PatientStatus.Active ? "30" : (patientWithProfile.Profile.Status == (int)PatientStatus.Discharged ? "01" : string.Empty);
                            claim.AdmissionSource = patientWithProfile.Profile.AdmissionSource;
                            SetAdmissionInfoAppSpecific(claim, patientWithProfile);
                            SetManagedClaimWithAssessmentInfoAppSpecific(claim);

                            claim.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;

                            claim.InsuranceIdNumber = patientWithProfile.MedicareNumber;
                            claim.IsHomeHealthServiceIncluded = true;

                        }
                    }
                   
                }
                
                if (claim.IsMedicareHMO && claim.AuthorizationNumber.IsNullOrEmpty())
                {
                    claim.AuthorizationNumber = claim.ClaimKey;
                }
            }
            return claim;
        }

        public ManagedClaim ManagedClaimWithInsurance(Guid Id, Guid patientId)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                var IsSetLocatorFromLocationOrInsurance = false;
                var isTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId);
                if (claim.Status == (int)ManagedClaimStatus.ClaimCreated)
                {
                    if (!claim.IsInsuranceVerified)
                    {
                        var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                        if (profile != null)
                        {
                            claim.AgencyLocationId = profile.AgencyLocationId;
                            if (!isTraditionalMedicare)
                            {
                                SetInsurancePlanInfo(claim, profile);
                                if (claim.PrimaryInsuranceId >= 1000)
                                {
                                    SetClaimAuthorization(claim, claim.EpisodeStartDate, claim.EpisodeEndDate);
                                }

                            }
                        }
                        IsSetLocatorFromLocationOrInsurance = true;
                    }
                    else
                    {
                        if (isTraditionalMedicare && claim.PrimaryInsuranceId >= 1000)
                        {
                            claim.Authorizations = GetClaimAutorizationList(claim.PatientId, claim.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, this.Service);
                        }
                    }
                }
                else
                {
                    if (isTraditionalMedicare && claim.PrimaryInsuranceId >= 1000)
                    {
                        claim.Authorizations = GetClaimAutorizationList(claim.PatientId, claim.Authorization, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, AuthorizationStatusTypes.Active, this.Service);
                    }
                }
                ClaimWithInsuranceAppSpecific<ManagedClaimInsurance>(claim, IsSetLocatorFromLocationOrInsurance, claim.AgencyLocationId);
                //claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
                //claim.AgencyInsurance = ClaimToInsuranceNoLocationId<ManagedBill>(patientId, claim.PrimaryInsuranceId, claim.Insurance, false);
            }
            return claim;
        }

        public ManagedClaim GetManagedClaimVisit(Guid Id, Guid patientId)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                var visits = baseScheduleRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, claim.PrimaryInsuranceId, claim.EpisodeStartDate, claim.EpisodeEndDate, new int[] { }, new int[] { }, false);
                if (visits.IsNotNullOrEmpty())
                {
                    var chargeRates = ClaimInsuranceRatesAppSpecific<ManagedClaimInsurance>(claim.Id, claim.PayorType);
                    claim.BillVisitDatas = this.BillableVisitsData(visits, ClaimType.MAN, chargeRates, true);
                }
            }
            return claim;
        }

        public ClaimSupplyViewData GetManagedClaimSupply(Guid Id, Guid patientId)
        {
            var viewData = new ClaimSupplyViewData();
            viewData.Service = this.Service;
            viewData.Type = ClaimTypeSubCategory.ManagedCare;
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
                if (claim != null)
                {
                    viewData.IsInfoVerified = claim.IsInfoVerified;
                    viewData.IsInsuranceVerified = claim.IsInsuranceVerified;
                    viewData.IsVisitVerified = claim.IsVisitVerified;
                    viewData.IsSupplyVerified = claim.IsSupplyVerified;
                    var supplies = claim.Supply.ToObject<List<Supply>>();
                    List<Guid> ids = new List<Guid>();
                    if (supplies != null)
                    {
                        int id = 0;
                        supplies.ForEach(s =>
                        {
                            s.BillingId = id;
                            id++;
                            ids.Add(s.UniqueIdentifier);
                        });
                    }
                    else
                    {
                        supplies = new List<Supply>();
                    }
                    claim.Supply = supplies.ToXml();
                    baseBillingRepository.UpdateManagedClaimModel(claim);
                    viewData.Id = claim.Id;
                    viewData.PatientId = claim.PatientId;
                    viewData.EpisodeStartDate = claim.EpisodeStartDate;
                    viewData.EpisodeEndDate = claim.EpisodeEndDate;
                    viewData.BilledSupplies = supplies.Count > 0 ? supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                    viewData.UnbilledSupplies = supplies.Count > 0 ? supplies.Where(s => !s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                }
            }
            return viewData;
        }

        public ManagedClaim GetManagedClaimSummary(Guid Id, Guid patientId)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);// GetManagedClaimInfo(patientId, Id);
            if (claim != null)
            {
                claim.Service = this.Service;
                if (claim.DiagnosisCode.IsNotNullOrEmpty())
                {
                    claim.DiagnosisCodesObject = claim.DiagnosisCode.ToObject<DiagnosisCodes>();
                }
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits
                        .ToObject<List<T>>()
                        .Where(s => s.VisitDate.IsValid() && s.EventDate.IsValid())
                        .OrderBy(s => s.VisitDate.Date)
                        .ThenBy(s => s.EventDate.Date)
                        .ToList();

                    if (visits.IsNotNullOrEmpty())
                    {
                        if (claim.IsVisitVerified)
                        {
                            //var agencyInsurance = new AgencyInsurance();
                            var chargeRates = ClaimInsuranceRatesAppSpecific<ManagedClaimInsurance>(claim.Id, claim.PayorType);// ManagedToChargeRates(claim, out agencyInsurance, false)
                            claim.BillVisitSummaryDatas = BillableVisitSummary(visits, ClaimType.MAN, chargeRates, true);
                        }
                    }
                }
            }
            return claim;
        }

        #endregion

        #region Managed Claim Steps | Verify

        public BillingJsonViewData ManagedVerifyInfo(ManagedClaim claim)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Managed Claim Basic Info is not verified.", Category = ClaimTypeSubCategory.ManagedCare };
            viewData.Service = this.Service.ToString();
            try
            {
                if (claim != null)
                {
                    claim.Service = this.Service;
                    if (claim.IsValid())
                    {
                        var currentClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, claim.Id);
                        if (currentClaim != null)
                        {
                            if (this.VerifyInfoAppSpecific<ManagedClaimInsurance>(currentClaim, claim))
                            {
                                currentClaim.PayorType = claim.PayorType;
                                currentClaim.Type = claim.Type;
                                
                                currentClaim.FirstName = claim.FirstName;
                                currentClaim.LastName = claim.LastName;
                                currentClaim.PatientIdNumber = claim.PatientIdNumber;
                                currentClaim.DOB = claim.DOB;
                                currentClaim.Gender = claim.Gender;

                                currentClaim.AddressLine1 = claim.AddressLine1;
                                currentClaim.AddressLine2 = claim.AddressLine2;
                                currentClaim.AddressCity = claim.AddressCity;
                                currentClaim.AddressStateCode = claim.AddressStateCode;
                                currentClaim.AddressZipCode = claim.AddressZipCode;

                                currentClaim.PhysicianLastName = claim.PhysicianLastName;
                                currentClaim.PhysicianFirstName = claim.PhysicianFirstName;
                                currentClaim.PhysicianNPI = claim.PhysicianNPI;

                                if (currentClaim.PayorType != claim.PayorType)
                                {
                                    currentClaim.IsInsuranceVerified = false;
                                    currentClaim.IsVisitVerified = false;
                                    currentClaim.IsSupplyVerified = false;
                                }
                                else
                                {
                                    if (claim.PrimaryInsuranceId != currentClaim.PrimaryInsuranceId)
                                    {
                                        currentClaim.IsInsuranceVerified = false;
                                        currentClaim.IsVisitVerified = false;
                                        currentClaim.IsSupplyVerified = false;
                                    }
                                }

                                if (currentClaim.EpisodeStartDate.Date != claim.EpisodeStartDate.Date || currentClaim.EpisodeEndDate.Date != claim.EpisodeEndDate.Date)
                                {
                                    currentClaim.IsVisitVerified = false;
                                    currentClaim.IsSupplyVerified = false;
                                }

                                currentClaim.PrimaryInsuranceId = claim.PrimaryInsuranceId;
                                currentClaim.InsuranceIdNumber = claim.InsuranceIdNumber;
                                currentClaim.IsBillingAddressDifferent = claim.IsBillingAddressDifferent;
                                currentClaim.PrivatePayorId = claim.PrivatePayorId;

                                currentClaim.EpisodeStartDate = claim.EpisodeStartDate;
                                currentClaim.EpisodeEndDate = claim.EpisodeEndDate;
                                currentClaim.StartofCareDate = claim.StartofCareDate;
                                currentClaim.AdmissionSource = claim.AdmissionSource;
                                currentClaim.UB4PatientStatus = claim.UB4PatientStatus;
                                if (UB4PatientStatusFactory.Discharge().Contains(claim.UB4PatientStatus))
                                {
                                    currentClaim.DischargeDate = claim.DischargeDate;
                                }
                                currentClaim.FirstBillableVisitDate = claim.FirstBillableVisitDate;

                                currentClaim.HippsCode = claim.HippsCode;
                                currentClaim.ClaimKey = claim.ClaimKey;
                                currentClaim.ProspectivePay = claim.ProspectivePay;
                                currentClaim.IsHomeHealthServiceIncluded = claim.IsHomeHealthServiceIncluded;

                                currentClaim.DiagnosisCode = claim.DiagnosisCodesObject.ToXml();
                                currentClaim.ConditionCodes = claim.ConditionCodesObject.ToXml();
                                currentClaim.Remark = claim.Remark;
                                var oldVerification = currentClaim.IsInfoVerified;
                                currentClaim.IsInfoVerified = true;
                                currentClaim.Modified = DateTime.Now;

                                if (baseBillingRepository.UpdateManagedClaimModel(currentClaim))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedDemographicsVerified, string.Empty);
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Managed Claim Basic Info is successfully verified.";
                                    if (!oldVerification)
                                    {
                                        viewData.IsManagedCareRefresh = true;
                                    }
                                    viewData.PatientId = claim.PatientId;
                                }
                            }
                        }
                    }
                    else
                    {
                        viewData.errorMessage = claim.ValidationMessage;
                    }
                }
            }
            catch (Exception)
            {

            }
            return viewData;
        }

        public BillingJsonViewData ManagedVerifyInsurance(ManagedClaim claim)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified.", Service = this.Service.ToString(), Category = ClaimTypeSubCategory.ManagedCare };
            var claimToEdit = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, claim.PatientId, claim.Id);
            if (claimToEdit != null)
            {
                if (VerifyInsuranceAppSpecific(claimToEdit, claim))
                {
                    claimToEdit.IsInsuranceVerified = true;
                    if (baseBillingRepository.UpdateManagedClaimModel(claimToEdit))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, claimToEdit.PatientId, claimToEdit.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedInsuranceVerified, string.Empty);
                        viewData.errorMessage = "Managed Claim Insurance is successfully verified.";
                        viewData.isSuccessful = true;
                        viewData.PatientId = claimToEdit.PatientId;
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> visits)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Managed Claim visit is not verified.", Category = ClaimTypeSubCategory.ManagedCare };
            viewData.Service = this.Service.ToString();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var managedClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
                if (managedClaim != null)
                {
                    var oldVerification = managedClaim.IsVisitVerified;
                    if (visits.IsNotNullOrEmpty())
                    {
                        var scheduleEvents = baseScheduleRepository.GetScheduledEventsOnlyWithId(Current.AgencyId, patientId,managedClaim.PrimaryInsuranceId, visits.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate, new int[] { }, new int[] { }, false);
                        if (scheduleEvents.IsNotNullOrEmpty())
                        {
                            var visitList = new List<T>();
                            var managedClaimVisit = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>() : new List<T>();

                            visits.ForEach(v =>
                            {
                                var scheduleVisit = scheduleEvents.FirstOrDefault(s => s.Id == v);
                                if (scheduleVisit != null)
                                {
                                    scheduleVisit.IsBillable = true;
                                    visitList.Add(scheduleVisit);
                                }
                            });
                            if (managedClaimVisit != null && managedClaimVisit.Count > 0)
                            {
                                managedClaimVisit.ForEach(f =>
                                {
                                    var scheduleEvent = scheduleEvents.FirstOrDefault(e => e.Id == f.Id);
                                    if (scheduleEvent != null && !visits.Contains(f.Id))
                                    {
                                        scheduleEvent.IsBillable = false;
                                    }
                                });
                            }

                            managedClaim.IsVisitVerified = true;
                            managedClaim.VerifiedVisits = visitList.ToXml();
                            managedClaim.Modified = DateTime.Now;
                            if (visitList.IsNotNullOrEmpty())
                            {
                                var supplyList = this.GetSupplies(patientId, managedClaim.Supply, visitList, true);
                                if (supplyList.IsNotNullOrEmpty())
                                {
                                    managedClaim.Supply = supplyList.ToXml();
                                }
                            }

                            if (baseScheduleRepository.UpdateScheduleEventsForIsBillable(Current.AgencyId, patientId, scheduleEvents))
                            {
                                if (baseBillingRepository.UpdateManagedClaimModel(managedClaim))
                                {
                                    viewData.isSuccessful = true;
                                }
                            }
                        }
                        else
                        {
                            var managedClaimVisit = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>() : new List<T>();
                            managedClaimVisit.RemoveAll(m => !visits.Exists(v => v == m.Id));
                            managedClaim.IsVisitVerified = true;
                            managedClaim.VerifiedVisits = managedClaimVisit.ToXml();
                            managedClaim.Modified = DateTime.Now;
                            viewData.isSuccessful = baseBillingRepository.UpdateManagedClaimModel(managedClaim);
                        }
                    }
                    else
                    {
                        managedClaim.IsVisitVerified = true;
                        managedClaim.VerifiedVisits = new List<T>().ToXml();
                        managedClaim.Modified = DateTime.Now;
                        viewData.isSuccessful = baseBillingRepository.UpdateManagedClaimModel(managedClaim);
                    }
                    if (viewData.isSuccessful)
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedVisitVerified, string.Empty);
                        viewData.errorMessage = "Managed Claim Visit is successfully verified.";
                        if (!oldVerification)
                        {
                            viewData.IsManagedCareRefresh = true;
                        }
                        viewData.PatientId = patientId;
                    }
                }
            }
            
            return viewData;
        }

        public BillingJsonViewData ManagedVisitSupply(Guid Id, Guid patientId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Managed Claim supply is not verified.", Category = ClaimTypeSubCategory.ManagedCare };
            viewData.Service = this.Service.ToString();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
                if (claim != null)
                {
                    claim.Modified = DateTime.Now;
                    var oldVerification = claim.IsSupplyVerified;
                    claim.IsSupplyVerified = true;
                    if (baseBillingRepository.UpdateManagedClaimModel(claim))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSupplyVerified, string.Empty);
                        if (!oldVerification)
                        {
                            viewData.IsManagedCareRefresh = true;
                        }
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Managed Claim supply is successfully verified.";
                        viewData.PatientId = patientId;
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData ManagedComplete(Guid Id, Guid patientId, double total)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The Managed Claim could not be completed.", Category = ClaimTypeSubCategory.ManagedCare };
            viewData.Service = this.Service.ToString();
            viewData.PatientId = patientId;
            var managedClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (managedClaim != null)
            {
                managedClaim.ProspectivePay = total;
                if (baseBillingRepository.UpdateManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSummaryVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Managed Claim completed successfully.";
                }
            }
            return viewData;
        }

        #endregion

        //TODO: Need to add parameter for authorizations
        //public ManagedClaim GetManagedClaimInfo(Guid patientId, Guid Id)
        //{
        //    var managedClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
        //    if (managedClaim != null)
        //    {
        //        managedClaim.Service = this.Service;
        //        var patientWithProfile = basePatientProfileRepository.GetPatientWithProfileForClaim(Current.AgencyId, patientId);
        //        if (patientWithProfile != null)
        //        {
        //            if (managedClaim.CBSA.IsNullOrEmpty())
        //            {
        //                managedClaim.CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode);
        //            }
        //            if (managedClaim.Status == (int)ManagedClaimStatus.ClaimCreated)
        //            {
        //                if (!managedClaim.IsInfoVerified)
        //                {
        //                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
        //                    if (agencyLocation != null)
        //                    {
        //                        managedClaim.LocationZipCode = agencyLocation.AddressZipCode;
        //                        if (managedClaim.PrimaryInsuranceId > 0 && managedClaim.PrimaryInsuranceId < 1000)
        //                        {
        //                            managedClaim.Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
        //                        }
        //                        else if (managedClaim.PrimaryInsuranceId >= 1000)
        //                        {
        //                            var insurance = agencyRepository.GetInsurance(managedClaim.PrimaryInsuranceId, Current.AgencyId);
        //                            if (insurance != null)
        //                            {
        //                                managedClaim.Ub04Locator81cca = insurance.Ub04Locator81cca;
        //                            }
        //                            managedClaim.HealthPlanId = patientWithProfile.Profile.PrimaryHealthPlanId;
        //                            managedClaim.GroupName = patientWithProfile.Profile.PrimaryGroupName;
        //                            managedClaim.GroupId = patientWithProfile.Profile.PrimaryGroupId;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (managedClaim.PrimaryInsuranceId >= 1000)
        //                        {
        //                            var insurance = agencyRepository.GetInsurance(managedClaim.PrimaryInsuranceId, Current.AgencyId);
        //                            if (insurance != null)
        //                            {
        //                                managedClaim.Ub04Locator81cca = insurance.Ub04Locator81cca;
        //                            }
        //                            managedClaim.HealthPlanId = patientWithProfile.Profile.PrimaryHealthPlanId;
        //                            managedClaim.GroupName = patientWithProfile.Profile.PrimaryGroupName;
        //                            managedClaim.GroupId = patientWithProfile.Profile.PrimaryGroupId;

        //                        }
        //                    }

        //                    managedClaim.FirstName = patientWithProfile.FirstName;
        //                    managedClaim.LastName = patientWithProfile.LastName;
        //                    managedClaim.InsuranceIdNumber = patientWithProfile.MedicareNumber;
        //                    managedClaim.PatientIdNumber = patientWithProfile.PatientIdNumber;
        //                    managedClaim.Gender = patientWithProfile.Gender;
        //                    managedClaim.DOB = patientWithProfile.DOB;
        //                    managedClaim.PatientStatus = patientWithProfile.Profile.Status;
        //                    managedClaim.UB4PatientStatus = patientWithProfile.Profile.Status == 1 ? "30" : (patientWithProfile.Profile.Status == 2 ? "01" : string.Empty);
        //                    managedClaim.IsHomeHealthServiceIncluded = true;

        //                    SetAdmissionInfoAppSpecific(managedClaim, patientWithProfile);

        //                    managedClaim.AddressLine1 = patientWithProfile.AddressLine1;
        //                    managedClaim.AddressLine2 = patientWithProfile.AddressLine2;
        //                    managedClaim.AddressCity = patientWithProfile.AddressCity;
        //                    managedClaim.AddressStateCode = patientWithProfile.AddressStateCode;
        //                    managedClaim.AddressZipCode = patientWithProfile.AddressZipCode;
        //                    managedClaim.AdmissionSource = patientWithProfile.Profile.AdmissionSource;
        //                    var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
        //                    if (physician != null)
        //                    {
        //                        managedClaim.PhysicianLastName = physician.LastName;
        //                        managedClaim.PhysicianFirstName = physician.FirstName;
        //                        managedClaim.PhysicianNPI = physician.NPI;
        //                    }

        //                    SetManagedClaimWithAssessmentInfoAppSpecific(managedClaim);

        //                    var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patientWithProfile.Id, managedClaim.PrimaryInsuranceId, (AuthorizationStatusTypes.Active).ToString(), (int)this.Service, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
        //                    if (autorizations != null && autorizations.Count > 0)
        //                    {
        //                        var autorization = autorizations.FirstOrDefault();
        //                        var autoId = string.Empty;
        //                        if (autorization != null)
        //                        {
        //                            managedClaim.AuthorizationNumber = autorization.Number1;
        //                            managedClaim.AuthorizationNumber2 = autorization.Number2;
        //                            managedClaim.AuthorizationNumber3 = autorization.Number3;
        //                            autoId = autorization.Id.ToString();
        //                        }
        //                        managedClaim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
        //                    }
        //                    else
        //                    {
        //                        managedClaim.Authorizations = new List<SelectListItem>();
        //                    }
        //                }
        //                else
        //                {
        //                    var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patientWithProfile.Id, managedClaim.PrimaryInsuranceId, (AuthorizationStatusTypes.Active).ToString(), (int)this.Service, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
        //                    if (autorizations != null && autorizations.Count > 0)
        //                    {
        //                        managedClaim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == managedClaim.Authorization }).ToList();
        //                    }
        //                    else
        //                    {
        //                        managedClaim.Authorizations = new List<SelectListItem>();
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patientWithProfile.Id, managedClaim.PrimaryInsuranceId, (AuthorizationStatusTypes.Active).ToString(), (int)this.Service, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
        //                if (autorizations != null && autorizations.Count > 0)
        //                {
        //                    managedClaim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == managedClaim.Authorization }).ToList();
        //                }
        //                else
        //                {
        //                    managedClaim.Authorizations = new List<SelectListItem>();
        //                }
        //            }
        //            managedClaim.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;

        //        }
        //        if (managedClaim.IsMedicareHMO && managedClaim.AuthorizationNumber.IsNullOrEmpty())
        //        {
        //            managedClaim.AuthorizationNumber = managedClaim.ClaimKey;
        //        }
        //    }
        //    return managedClaim;
        //}

        protected void ClaimWithInsurance(BaseClaim claim)
        {
            if (claim != null)
            {
                claim.Locator39 = claim.Ub04Locator39.ToObject<List<Locator>>();
                claim.Locator81cca = claim.Ub04Locator81cca.ToObject<List<Locator>>();
                if (claim.ClaimSubCategory == ClaimTypeSubCategory.RAP)
                {
                }
                else
                {
                    claim.Locator31 = claim.Ub04Locator31.ToObject<List<Locator>>();
                    claim.Locator32 = claim.Ub04Locator32.ToObject<List<Locator>>();
                    claim.Locator33 = claim.Ub04Locator33.ToObject<List<Locator>>();
                    claim.Locator34 = claim.Ub04Locator34.ToObject<List<Locator>>();
                }
            }
        }

        protected List<ChargeRate> GetChargeRatesFromInsurance<C>(List<C> cliamInsurances, BaseClaim claim, bool isVisitExist) where C:ClaimInsurance, new()
        {
            List<ChargeRate> chargeRates = null;
            if (isVisitExist)
            {
                var claimInsurance = cliamInsurances.FirstOrDefault(i => i.Id == claim.Id);
                if (claimInsurance != null && claimInsurance.Insurance != null)
                {
                    chargeRates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                }
                else
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    if (agencyInsurance != null)
                    {
                        chargeRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                    }
                }
            }
            return chargeRates;
        }

        protected abstract void SetAdmissionInfoAppSpecific(ManagedClaim managedClaim, Patient patientWithProfile);

        protected void SetAdmissionInfo<E>(BaseClaim claim, Patient patientWithProfile) where E : CarePeriod, new()
        {
            var managedData = AdmissionHelperFactory<E>.GetPatientLatestAdmissionDate(patientWithProfile.Id, claim.EpisodeStartDate);
            if (managedData != null)
            {
                claim.PatientStatus = managedData.Status;
                claim.UB4PatientStatus = managedData.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (managedData.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty);

                if (UB4PatientStatusFactory.Discharge().Contains(claim.UB4PatientStatus))
                {
                    if (managedData.DischargedDate.Date > DateTime.MinValue.Date)
                    {
                        claim.DischargeDate = managedData.DischargedDate;
                    }
                    else
                    {
                        claim.DischargeDate = patientWithProfile.Profile.DischargeDate;
                    }
                }

                if (managedData.StartOfCareDate.Date > DateTime.MinValue.Date)
                {
                    claim.StartofCareDate = managedData.StartOfCareDate;
                }
                else
                {
                    claim.StartofCareDate = patientWithProfile.Profile.StartofCareDate;
                }
            }
            else
            {
                claim.PatientStatus = patientWithProfile.Profile.Status;
                claim.UB4PatientStatus = patientWithProfile.Profile.Status == (int)PatientStatus.Active ? ((int)UB4PatientStatus.StillPatient).ToString() : (patientWithProfile.Profile.Status == (int)PatientStatus.Discharged ? ((int)UB4PatientStatus.DischargeToHomeOrSelfCare).ToString() : string.Empty);

                if (UB4PatientStatusFactory.Discharge().Contains(claim.UB4PatientStatus))
                {
                    claim.DischargeDate = patientWithProfile.Profile.DischargeDate;
                }
                claim.StartofCareDate = patientWithProfile.Profile.StartofCareDate;
            }
        }

        protected abstract void SetManagedClaimWithAssessmentInfoAppSpecific(ManagedClaim managedClaim);

        protected abstract void ClaimWithInsuranceAppSpecific<C>(BaseClaim claim, bool IsSetLocatorFromLocationOrInsurance, Guid optionalLocatonId) where C : ClaimInsurance, new();

        protected void ClaimWithInsuranceAppSpecificHelper<C>(BaseClaim claim, bool IsSetLocatorFromLocationOrInsurance, Guid optionalLocatonId) where C : ClaimInsurance, new()
        {
            var insurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, claim.Id);
            if (insurance != null)
            {
                if (insurance.Insurance != null)
                {
                    if (insurance.Insurance.BillData.IsNotNullOrEmpty())
                    {
                        claim.VisitRates = insurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                    }
                    claim.InvoiceType = insurance.Insurance.InvoiceType;
                    if (IsSetLocatorFromLocationOrInsurance)
                    {
                        claim.Ub04Locator81cca = insurance.Insurance.Ub04Locator81cca;
                    }
                    SetUB04OrHCFALocators(claim);
                }
            }
            else
            {
                var agencyInsurance = new AgencyInsurance();
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                }
                else
                {
                    if (claim.PrimaryInsuranceId > 1000)
                    {
                        agencyInsurance = agencyRepository.GetInsurance(claim.PrimaryInsuranceId, Current.AgencyId);
                    }
                    else if (MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId))
                    {
                        if (!optionalLocatonId.IsEmpty())
                        {
                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(optionalLocatonId, claim.PrimaryInsuranceId, true);
                        }
                        else
                        {
                            var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                            if (profile != null)
                            {
                                agencyInsurance = this.CMSInsuranceToAgencyInsurance(profile.AgencyLocationId, claim.PrimaryInsuranceId, true);
                            }
                        }
                    }
                }
                if (agencyInsurance != null)
                {
                    claim.InvoiceType = agencyInsurance.InvoiceType;
                    if (baseMongoRepository.AddClaimInsurance<C>(new C { AgencyId = Current.AgencyId, Id = claim.Id, PatientId = claim.PatientId, Insurance = agencyInsurance }))
                    {
                        if (IsSetLocatorFromLocationOrInsurance)
                        {
                            claim.Ub04Locator81cca = insurance.Insurance.Ub04Locator81cca;
                        }
                        SetUB04OrHCFALocators(claim);
                        if (agencyInsurance.BillData.IsNotNullOrEmpty())
                        {
                            claim.VisitRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                        }
                    }
                }
            }
        }

        //protected void SetClaimInsuranceId(BaseClaim claim, Patient patientWithProfile)
        //{
        //    if (MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId))
        //    {
        //        if (claim.PrimaryInsuranceId == patientWithProfile.Profile.PrimaryInsurance)
        //        {

        //        }
        //    }
        //}

        private void SetUB04OrHCFALocators(BaseClaim claim)
        {
            if (claim.InvoiceType == (int)InvoiceType.UB)
            {
                ClaimWithInsurance(claim);
                //claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
            }
            else if (claim.InvoiceType == (int)InvoiceType.HCFA)
            {
                claim.LocatorHCFA = claim.HCFALocators.ToObject<List<Locator>>();
            }
        }

        protected void SetClaimPhysicianInfo(BaseClaim claim)
        {
            var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, claim.PatientId);
            if (physician != null)
            {
                claim.PhysicianLastName = physician.LastName;
                claim.PhysicianFirstName = physician.FirstName;
                claim.PhysicianNPI = physician.NPI;
            }
        }

        protected void SetClaimAuthorization(BaseClaim claim, DateTime startDate, DateTime endDate)
        {
            var autorizations = basePatientProfileRepository.GetAuthorizationsByStatus(Current.AgencyId, claim.PatientId, claim.PrimaryInsuranceId, (AuthorizationStatusTypes.Active).ToString(), (int)this.Service, startDate, endDate);
            if (autorizations.IsNotNullOrEmpty())
            {
                var autorization = autorizations.FirstOrDefault();
                var autoId = string.Empty;
                if (autorization != null)
                {
                    claim.AuthorizationNumber = autorization.Number1;
                    claim.AuthorizationNumber2 = autorization.Number2;
                    claim.AuthorizationNumber3 = autorization.Number3;
                    autoId = autorization.Id.ToString();
                }
                claim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
            }
            else
            {
                claim.Authorizations = new List<SelectListItem>();
            }
        }

        protected List<SelectListItem> GetClaimAutorizationList(Guid patientId, Guid authorization, int primaryInsuranceId, DateTime startDate, DateTime endDate, AuthorizationStatusTypes statusType, AgencyServices service)
        {
            var authorizationList = new List<SelectListItem>();
            if (primaryInsuranceId >= 1000)
            {
                var autorizations = basePatientProfileRepository.GetAuthorizationsByStatus(Current.AgencyId, patientId, primaryInsuranceId, (statusType).ToString(), (int)service, startDate, endDate);
                if (autorizations.IsNotNullOrEmpty())
                {
                    authorizationList = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == authorization }).ToList();
                    authorizationList.Insert(0, new SelectListItem { Text = "-- Select To Repopulate --", Value = Guid.Empty.ToString() });
                }
            }
            return authorizationList;
        }


        protected static void SetClaimWithAdmissionInfo(BaseClaim claim, Guid admissionId)
        {
            var managedDate = AdmissionHelperFactory<PatientEpisode>.GetPatientAdmissionDate(claim.PatientId, admissionId);
            if (managedDate != null)
            {
                if (UB4PatientStatusFactory.Discharge().Contains(claim.UB4PatientStatus))
                {
                    if (managedDate.DischargedDate.Date > DateTime.MinValue.Date)
                    {
                        claim.DischargeDate = managedDate.DischargedDate;
                    }
                }
                claim.StartofCareDate = managedDate.StartOfCareDate;
            }
        }

        protected void SetClaimWithAssessmentInfo(BaseClaim claim)
        {
            var assessment = EpisodeAssessmentHelperFactory<PatientEpisode, ScheduleEvent>.GetEpisodeAssessment(claim.EpisodeId, claim.PatientId);
            if (assessment != null)
            {
                //TODO: Need to pull assessment data from mongo instead of from assessment
                var assessmentQuestionsData = baseMongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id);
                if (assessmentQuestionsData != null)
                {
                    var assessmentQuestions = assessmentQuestionsData.ToDictionary();
                    claim.DiagnosisCode = assessmentQuestions.ToClaimDiagonasisCodesXml();
                }
                claim.HippsCode = assessment.HippsCode;
                claim.ClaimKey = assessment.ClaimKey;
                claim.AssessmentType = assessment.Type;
                claim.ProspectivePay = LookUpHelper.ProspectivePayAmount(assessment.Type, assessment.HippsCode, claim.EpisodeStartDate, claim.AddressZipCode, claim.LocationZipCode);
            }
        }

        protected void SetInsurancePlanInfo(BaseClaim claim, Profile profile)
        {
            if (claim.PrimaryInsuranceId >= 1000)
            {
                if (claim.PrimaryInsuranceId == profile.PrimaryInsurance)
                {
                    claim.HealthPlanId = profile.PrimaryHealthPlanId;
                    claim.GroupName = profile.PrimaryGroupName;
                    claim.GroupId = profile.PrimaryGroupId;
                    claim.Relationship = profile.PrimaryRelationship;
                }
                else if (claim.PrimaryInsuranceId == profile.SecondaryInsurance)
                {
                    claim.HealthPlanId = profile.SecondaryHealthPlanId;
                    claim.GroupName = profile.SecondaryGroupName;
                    claim.GroupId = profile.SecondaryGroupId;
                    claim.Relationship = profile.SecondaryRelationship;
                }
                else if (claim.PrimaryInsuranceId == profile.TertiaryInsurance)
                {
                    claim.HealthPlanId = profile.TertiaryHealthPlanId;
                    claim.GroupName = profile.TertiaryGroupName;
                    claim.GroupId = profile.TertiaryGroupId;
                    claim.Relationship = profile.TertiaryRelationship;
                }
            }
        }

        protected abstract void SetPayorTypeAppSpecific(BaseClaim claim);

        protected abstract void SetInvoiceNumberAppSpecific(ManagedClaim claim);

        protected abstract void GetInvoiceNumber(ManagedClaim managedClaim, ClaimViewData claimData);

        public List<ClaimLean> GetManagedClaims(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            return baseBillingRepository.GetManagedClaims(Current.AgencyId, branchId, status, startDate, endDate);
        }

        public IList<ManagedClaimLean> GetManagedClaimsActivity(Guid patientId, int insuranceId)
        {
            var claims = baseBillingRepository.GetManagedClaimsPerPatient(Current.AgencyId, patientId, insuranceId);
            if (claims.IsNotNullOrEmpty())
            {
                var isEditPermission = false;
                var isPrintPermission = false;
                var isDeletePermission = false;
                var isGeneratePermission = false;
                var allPermission = Current.CategoryService(this.Service, ParentPermission.ManagedCareClaim, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Print, (int)PermissionActions.Delete,(int)PermissionActions.GenerateElectronicClaim });
                if (allPermission.IsNotNullOrEmpty())
                {
                    isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                    isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                    isDeletePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Delete, AgencyServices.None) != AgencyServices.None;
                    isGeneratePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.GenerateElectronicClaim, AgencyServices.None) != AgencyServices.None;
                }

                var payments = baseBillingRepository.GetManagedClaimPaymentsByPatient(Current.AgencyId, patientId);
                var adjustments = baseBillingRepository.GetManagedClaimAdjustmentsByPatient(Current.AgencyId, patientId);
                foreach (var claim in claims)
                {
                    claim.Service = this.Service;
                    claim.IsUserCanEdit = isEditPermission;
                    claim.IsUserCanDelete = isDeletePermission;
                    claim.IsUserCanPrint = isPrintPermission;
                    claim.IsUserCanGenerate = isGeneratePermission;
                    if (payments != null && payments.Count > 0)
                    {
                        var claimPayments = payments.Where(s => s.ClaimId == claim.Id).ToList();
                        if (claimPayments != null && claimPayments.Count > 0)
                        {
                            claim.PaymentAmount = claimPayments.Sum(p => p.Payment);
                            var primaryPayments = payments.Where(p => p.Payor == claim.PrimaryInsuranceId).ToList();
                            if (primaryPayments != null && primaryPayments.Count > 0)
                            {
                                claim.PaymentDate = primaryPayments.Aggregate((agg, next) => next.PaymentDate > agg.PaymentDate ? next : agg).PaymentDate;
                            }
                        }
                    }
                    if (adjustments != null && adjustments.Count > 0)
                    {
                        var adjustmentSum = adjustments.Where(a => a.ClaimId == claim.Id).Sum(a => a.Adjustment);
                        claim.AdjustmentAmount = adjustmentSum;
                    }
                }
            }
            return claims;
        }
       
        protected abstract bool VerifyInfoAppSpecific<C>(BaseClaim currentClaimFomDB, BaseClaim claimFromClient) where C : ClaimInsurance, new();

        protected bool VerifyInfoInsuranceHelper<C>(BaseClaim currentClaimFomDB, BaseClaim claimFromClient) where C : ClaimInsurance, new()
        {
            var save = false;
            if (currentClaimFomDB.PrimaryInsuranceId != claimFromClient.PrimaryInsuranceId)
            {
                var agencyInsurance = new AgencyInsurance();
                if (claimFromClient.PrimaryInsuranceId >= 1000)
                {
                    agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claimFromClient.PrimaryInsuranceId);
                }
                else if (MedicareIntermediaryFactory.Intermediaries().Contains(claimFromClient.PrimaryInsuranceId))
                {
                    var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, claimFromClient.PatientId);
                    agencyInsurance = this.CMSInsuranceToAgencyInsurance(profile.AgencyLocationId, claimFromClient.PrimaryInsuranceId, true); //GetInsuranceHelper(profile.AgencyLocationId, claimFromClient.PrimaryInsuranceId, true);
                }
                ResetInsuranceAndLocatorsToDefault(claimFromClient, agencyInsurance);
                save = baseMongoRepository.UpdateClaimInsurance(new C { AgencyId = Current.AgencyId, Id = claimFromClient.Id, Insurance = agencyInsurance, PatientId = claimFromClient.PatientId });
            }
            else
            {
                save = true;
            }
            return save;
        }

        protected void ResetInsuranceAndLocatorsToDefault(BaseClaim claim, AgencyInsurance insurance)
        {
            claim.Insurance = insurance != null ? insurance.ToXml() : "";
            claim.Ub04Locator81cca = insurance != null ? insurance.Ub04Locator81cca : "";
            claim.Ub04Locator39 = "";
            claim.Ub04Locator31 = "";
            claim.Ub04Locator32 = "";
            claim.Ub04Locator33 = "";
            claim.Ub04Locator34 = "";
            claim.HCFALocators = "";
        }

        protected abstract bool VerifyInsuranceAppSpecific(BaseClaim currentClaimFomDB, BaseClaim claimFromClient);

        public BillingJsonViewData DeleteManagedClaim(Guid patientId, Guid Id)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Error in deleting the claim. Please try again." };
            viewData.Service = this.Service.ToString();
            viewData.Category = ClaimTypeSubCategory.ManagedCare;
            var rules = new List<Validation>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (baseBillingRepository.DeleteManagedClaim(Current.AgencyId, patientId, Id))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                        viewData.IsManagedCareHistoryRefresh = true;
                        viewData.PatientId = patientId;
                    }
                }
            }
            return viewData;
        }

        #region Managed Claim Charge rates

        public ManagedClaim UpdateManagedCareForInsuranceReload(Guid ClaimId, Guid patientId)
        {
            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified." };
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, ClaimId);
            if (claim != null)
            {
                if (claim.PayorType == (int)PayorTypeMainCategory.NonPrivatePayor)
                {
                }
                claim.Service = this.Service;
                var agencyInsurance = ClaimToInsuranceNoLocationId(claim.PatientId, claim.PrimaryInsuranceId, true);
                if (agencyInsurance != null)
                {
                    ResetInsuranceAndLocatorsToDefault(claim, agencyInsurance);
                    if (baseBillingRepository.UpdateManagedClaimModel(claim))
                    {
                        //viewData.isSuccessful = true;
                        //viewData.errorMessage = "Insurance was successfully reloaded.";
                    }
                    claim.AgencyInsurance = agencyInsurance;
                }
                claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
            }
            return claim;
        }

        public List<ChargeRate> UpdateManagedCareRatesForReload(Guid ClaimId, Guid patientId)
        {
            var rates = new List<ChargeRate>();
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, ClaimId);
            if (claim != null)
            {
                rates = UpdateRatesForReloadAppSpecific<ManagedClaimInsurance>(claim);
            }
            return rates;
        }

        public List<ChargeRate> ManagedClaimInsuranceRates(Guid Id)
        {
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, Id);
            var rates = new List<ChargeRate>();
            if (claim != null)
            {
                rates = ClaimInsuranceRatesAppSpecific<ManagedClaimInsurance>(claim.Id, claim.PayorType);
            }
            return rates;
        }

        #endregion

        #region Insurance

        protected abstract bool UpdateOrAddPayorForClaimAppSpecific<C>(BaseClaim claim) where C : ClaimInsurance, new();
        
        protected bool UpdateOrAddInsuranceForClaim<C>(BaseClaim claim) where C : ClaimInsurance, new()
        {
            var result = false;
            var agencyInsurance = ClaimToInsuranceNoLocationId(claim.PatientId, claim.PrimaryInsuranceId, true);
            if (agencyInsurance != null)
            {
                var claimInsurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, claim.Id);
                if (claimInsurance != null)
                {
                    claimInsurance.Insurance = agencyInsurance;
                    if (baseMongoRepository.UpdateClaimInsurance<C>(claimInsurance))
                    {
                        return true;
                    }
                }
                else
                {
                    if (baseMongoRepository.AddClaimInsurance(new C { AgencyId = Current.AgencyId, Id = claim.Id, PatientId = claim.PatientId, Insurance = agencyInsurance }))
                    {
                        return true;
                    }
                }
            }
            return result;
        }

        #endregion

        #region Insurance Charge rates

        protected abstract List<ChargeRate> UpdateRatesForReloadAppSpecific<C>(BaseClaim claim) where C:ClaimInsurance, new();

        public List<ChargeRate> UpdateRatesForReloadFromInsurance<C>(BaseClaim claim) where C:ClaimInsurance, new()
        {
            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified." };
            //var claim = baseBillingRepository.GetManagedClaim(Current.AgencyId, patientId, ClaimId);
            //if (claim != null)
            var rates = new List<ChargeRate>();
            var claimInsurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, claim.Id);
            if (claimInsurance != null)
            {
                if (claim.PrimaryInsuranceId >= 1000)
                {
                    var agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    if (agencyInsurance != null)
                    {
                        claimInsurance.Insurance.BillData = agencyInsurance.BillData;
                    }
                }
                else if (MedicareIntermediaryFactory.Intermediaries().Contains(claim.PrimaryInsuranceId))
                {
                    var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                    if (profile != null)
                    {
                        var location = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
                        if (location != null)
                        {
                            claimInsurance.Insurance.BillData = location.BillData;
                        }
                    }
                }
                if (baseMongoRepository.UpdateClaimInsurance<C>(claimInsurance))
                {
                    if (claimInsurance.Insurance.BillData.IsNotNullOrEmpty())
                    {
                        rates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                    }
                }
            }
            //claim.Service = this.Service;
            //var agencyInsurance = ClaimToInsuranceNoLocationId<ManagedBill>(claim.PatientId, claim.PrimaryInsuranceId, true);
            //if (agencyInsurance != null)
            //{
            //    ResetInsuranceAndLocatorsToDefault(claim, agencyInsurance);
            //    if (baseBillingRepository.UpdateManagedClaimModel(claim))
            //    {
            //        //viewData.isSuccessful = true;
            //        //viewData.errorMessage = "Insurance was successfully reloaded.";
            //    }
            //    claim.AgencyInsurance = agencyInsurance;
            //}
            //claim.CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode);
            return rates;
        }

        protected abstract List<ChargeRate> ClaimInsuranceRatesAppSpecific<C>(Guid claimId,int payorType) where C : ClaimInsurance, new();

        public NewBillDataViewData GetClaimNewBillData<C>(Guid ClaimId, int payorType, int primaryInsuranceId, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new()
        {
            var viewData = new NewBillDataViewData { ClaimId = ClaimId, Service = this.Service, IsTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(primaryInsuranceId), TypeOfClaim = claimSubCategory, IsMedicareHMO = false, PayorType = payorType };
            SetClaimNewBillDataAppSpecific<C>(viewData, ClaimId, payorType);
            //var claim = baseBillingRepository.GetManagedClaimInsurance(Current.AgencyId, ClaimId);
            //if (claim != null && claim.Insurance.IsNotNullOrEmpty())
            //{
            //    var insurance = ClaimToInsuranceNoLocationId<ManagedBill>(claim.PatientId, claim.PrimaryInsuranceId, claim.Insurance, false);
            //    if (insurance != null)
            //    {
            //        viewData.IsMedicareHMO = insurance.PayorType == 2;
            //    }
            //}
            return viewData;
        }

        protected abstract void SetClaimNewBillDataAppSpecific<C>(NewBillDataViewData data ,Guid ClaimId, int payorType) where C : ClaimInsurance, new();

        protected void SetClaimNewBillData<C>(NewBillDataViewData data, Guid claimId) where C : ClaimInsurance, new()
        {
            var insurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, claimId);
            if (insurance != null && insurance.Insurance != null)
            {
                data.IsMedicareHMO = insurance.Insurance.PayorType == (int)PayerTypes.MedicareHMO;
                if (insurance.Insurance.BillData.IsNotNullOrEmpty())
                {
                    data.ExistingNotes = insurance.Insurance.BillData.ToObject<List<ChargeRate>>().Select(r => r.Id).ToList();
                }
            }
        }

        public BillingJsonViewData ClaimDeleteBillData<C>(Guid ClaimId, int Id, int payorType, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new()
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be deleted." };
            viewData.Category = claimSubCategory;
            viewData.Service = this.Service.ToString();
            if (SetClaimNewBillDataAppSpecific<C>(ClaimId, Id, payorType))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Charge rate is successfuly deleted.";
            }
            //var claim = baseBillingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
            //if (claim != null && claim.Insurance.IsNotNullOrEmpty())
            //{
            //    var agencyInsurance = ClaimToInsuranceNoLocationId<ManagedBill>(claim.PatientId, claim.PrimaryInsuranceId, claim.Insurance, false);
            //    var rates = agencyInsurance.ToBillDataDictionary();
            //    if (rates.ContainsKey(Id.ToString()))
            //    {
            //        if (rates.Remove(Id.ToString()))
            //        {
            //            agencyInsurance.BillData = rates.Select(r => r.Value).ToList().ToXml();
            //            claim.Insurance = agencyInsurance.ToXml();
            //            if (baseBillingRepository.UpdateManagedClaimModel(claim))
            //            {
            //                viewData.isSuccessful = true;
            //                viewData.errorMessage = "Charge rate is successfuly deleted.";
            //            }
            //        }
            //    }
            //}
            return viewData;
        }

        protected abstract bool SetClaimNewBillDataAppSpecific<C>(Guid ClaimId, int Id, int payorType)where C : ClaimInsurance, new();

        public BillingJsonViewData ClaimAddBillData<C>(ChargeRate chargeRate, Guid ClaimId, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new()
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            viewData.Service = this.Service.ToString();
            viewData.Category = claimSubCategory;
            var payorType = 11;
            if (this.Service == AgencyServices.PrivateDuty)
            {
                var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, ClaimId);
                if (claim != null)
                {
                    payorType = claim.PayorType;
                }
            }
            if (ClaimAddBillDataAppSpecific<C>(chargeRate, ClaimId, payorType))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Charge rate is successfuly updated.";
            }
            //if (claim.Insurance.IsNotNullOrEmpty())
            //{
            //    var agencyInsurance = ClaimToInsuranceNoLocationId<ManagedBill>(claim.PatientId, claim.PrimaryInsuranceId, claim.Insurance, false);
            //    if (agencyInsurance != null)
            //    {
            //        var rates = agencyInsurance.BillData.IsNotNullOrEmpty() ? agencyInsurance.BillData.ToObject<List<ChargeRate>>() : new List<ChargeRate>();
            //        rates.Add(chargeRate);
            //        agencyInsurance.BillData = rates.ToXml();
            //        claim.Insurance = agencyInsurance.ToXml();
            //        if (baseBillingRepository.UpdateManagedClaimModel(claim))
            //        {
            //            viewData.isSuccessful = true;
            //            viewData.errorMessage = "Charge rate is successfuly updated.";
            //        }
            //    }
            //}
            return viewData;
        }

        protected abstract bool ClaimAddBillDataAppSpecific<C>(ChargeRate chargeRate, Guid ClaimId, int payorType) where C : ClaimInsurance, new();

        protected bool ClaimAddInsuranceBillData<C>(ChargeRate chargeRate, Guid ClaimId) where C : ClaimInsurance, new()
        {
            var result = false;
            var claimInsurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, ClaimId);
            if (claimInsurance != null)
            {
                if (claimInsurance.Insurance != null)
                {
                    var rates = new List<ChargeRate>();
                    if (claimInsurance.Insurance.BillData.IsNotNullOrEmpty())
                    {
                        rates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates.IsNotNullOrEmpty())
                        {
                            if (rates.Exists(r => r.Id == chargeRate.Id))
                            {
                                rates.RemoveAll(r => r.Id == chargeRate.Id);

                            }
                            rates.Add(chargeRate);
                        }
                        else
                        {
                            rates = new List<ChargeRate> { chargeRate };
                        }
                    }
                    else
                    {
                        rates = new List<ChargeRate> { chargeRate };
                    }
                    claimInsurance.Insurance.BillData = rates.ToXml();
                    result = baseMongoRepository.AddClaimInsurance<C>(claimInsurance);
                }
            }
            return result;

        }

        protected bool ClaimDeleteInsuranceBillData<C>(Guid ClaimId,int Id)where C : ClaimInsurance, new()
        {
            var result = false;
            var claimInsurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, ClaimId);
            if (claimInsurance != null)
            {
                if (claimInsurance.Insurance != null)
                {
                    if (claimInsurance.Insurance.BillData.IsNotNullOrEmpty())
                    {
                        var  rates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates.IsNotNullOrEmpty())
                        {
                            if (rates.Exists(r => r.Id == Id))
                            {
                                rates.RemoveAll(r => r.Id == Id);
                            }
                            claimInsurance.Insurance.BillData = rates.ToXml();
                        }
                    }
                    result = baseMongoRepository.UpdateClaimInsurance<C>(claimInsurance);
                }
            }
            return result;

        }

        public EditBillDataViewData GetClaimBillDataForEdit<C>(Guid ClaimId, int Id, int PayorType, int primaryInsuranceId, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new()
        {
            var viewData = new EditBillDataViewData { ClaimId = ClaimId, TypeOfClaim = claimSubCategory, Service = this.Service, IsTraditionalMedicare = MedicareIntermediaryFactory.Intermediaries().Contains(primaryInsuranceId), ChargeRate = new ChargeRate(), PayorType = PayorType };
            var rates = ClaimInsuranceRatesAppSpecific<C>(ClaimId, PayorType);
            if (rates.IsNotNullOrEmpty())
            {
                viewData.ChargeRate = rates.SingleOrDefault(r => r.Id == Id);
                if (viewData.ChargeRate != null)
                {
                    viewData.ChargeRate.InsuranceId = primaryInsuranceId;
                }
            }
            //if (PayorType == 11)
            //{
            //     var claimInsurance = baseMongoRepository.GetManagedClaimInsurance<ManagedClaimInsurance>(Current.AgencyId, ClaimId);
            //     if (claimInsurance != null && claimInsurance.Insurance!=null && claimInsurance.Insurance.BillData.IsNotNullOrEmpty())
            //     {
            //         var rates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
            //         if (rates.IsNotNullOrEmpty())
            //         {
            //             viewData.ChargeRate = rates.SingleOrDefault(r => r.Id == Id);
            //         }
            //     }
            //}
            //else if (PayorType == 10)
            //{
            //}
            //var claim = baseBillingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
            //if (claim != null)
            //{
            //    var agencyInsurance = ClaimToInsuranceNoLocationId<ManagedBill>(claim.PatientId, claim.PrimaryInsuranceId, claim.Insurance, false);
            //    var rates = agencyInsurance.ToBillDataDictionary();
            //    if (rates.ContainsKey(Id.ToString()))
            //    {
            //        viewData.ChargeRate = rates[Id.ToString()];
            //    }
            //}
            return viewData;
        }

        public BillingJsonViewData ClaimUpdateBillData<C>(ChargeRate chargeRate, Guid ClaimId, int PayorType, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new()
        {
            return ClaimUpdateBillDataAppSpecific<C>(chargeRate, ClaimId, PayorType,claimSubCategory);
        }

        protected abstract BillingJsonViewData ClaimUpdateBillDataAppSpecific<C>(ChargeRate chargeRate, Guid ClaimId, int PayorType, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new();

        protected BillingJsonViewData ClaimUpdateInsuranceBillData<C>(ChargeRate chargeRate, Guid claimId, ClaimTypeSubCategory claimSubCategory) where C : ClaimInsurance, new()
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            viewData.Service = this.Service.ToString();
            viewData.Category = claimSubCategory;
            var claimInsurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, claimId);
            if (claimInsurance != null)
            {
                if (claimInsurance.Insurance != null)
                {
                    if (claimInsurance.Insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var oldRate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (oldRate != null)
                            {
                                oldRate.PreferredDescription = chargeRate.PreferredDescription;
                                oldRate.Code = chargeRate.Code;
                                oldRate.RevenueCode = chargeRate.RevenueCode;
                                oldRate.Charge = chargeRate.Charge;
                                oldRate.Modifier = chargeRate.Modifier;
                                oldRate.Modifier2 = chargeRate.Modifier2;
                                oldRate.Modifier3 = chargeRate.Modifier3;
                                oldRate.Modifier4 = chargeRate.Modifier4;
                                if (!MedicareIntermediaryFactory.Intermediaries().Contains(chargeRate.InsuranceId))
                                {
                                    oldRate.ChargeType = chargeRate.ChargeType;
                                    oldRate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                                    if (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                                    {
                                        if (chargeRate.IsTimeLimit)
                                        {

                                            oldRate.TimeLimitHour = chargeRate.TimeLimitHour;
                                            oldRate.TimeLimitMin = chargeRate.TimeLimitMin;
                                            oldRate.SecondDescription = chargeRate.SecondDescription;
                                            oldRate.SecondCode = chargeRate.SecondCode;
                                            oldRate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                            oldRate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                            if (chargeRate.IsSecondChargeDifferent)
                                            {
                                                oldRate.SecondCharge = chargeRate.SecondCharge;
                                            }
                                            else
                                            {
                                                oldRate.SecondCharge = 0;
                                            }
                                            oldRate.SecondModifier = chargeRate.SecondModifier;
                                            oldRate.SecondModifier2 = chargeRate.SecondModifier2;
                                            oldRate.SecondModifier3 = chargeRate.SecondModifier3;
                                            oldRate.SecondModifier4 = chargeRate.SecondModifier4;
                                            oldRate.SecondChargeType = chargeRate.SecondChargeType;
                                            if (oldRate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                                            {
                                                oldRate.SecondUnit = chargeRate.SecondUnit;
                                            }
                                            else
                                            {
                                                oldRate.SecondUnit = 0;
                                            }
                                            oldRate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                                        }
                                        oldRate.IsTimeLimit = chargeRate.IsTimeLimit;
                                    }
                                    else if (oldRate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                                    {
                                        oldRate.Unit = chargeRate.Unit;
                                    }
                                    if (claimInsurance.Insurance.PayorType == (int)PayerTypes.MedicareHMO && (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
                                    {
                                        oldRate.MedicareHMORate = chargeRate.MedicareHMORate;
                                    }
                                }

                                claimInsurance.Insurance.BillData = rates.ToXml();
                                //claimInsurance.Insurance = agencyInsurance.ToXml();
                                if (baseMongoRepository.UpdateClaimInsurance<C>(claimInsurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Charge rate is successfuly updated.";
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        #endregion

        public Bill GetManagedCreateClaims()
        {
            var bill = new Bill();
            bill.Service = this.Service;
            var agencyMainBranch = agencyRepository.GetBranchForSelection(Current.AgencyId, (int)this.Service);
            if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
            {
                AgencyInsurance agencyMedicareInsurance = null;
                if (this.Service == AgencyServices.PrivateDuty)
                {
                    agencyMedicareInsurance = new AgencyInsurance { Id = 18, Name = "Private Pay", IsAxxessTheBiller = false };
                }
                else
                {
                    agencyMedicareInsurance = agencyRepository.GetFirstInsurance(Current.AgencyId, new int[] { (int)PayerTypes.MedicareTraditional });
                }

                var isAddPermission = false;
                var isViewPermission = false;
                var isEditPermission = false;
                var isPrintPermission = false;
                var isExportPermission = false;
                var isGeneratePermission = false;
                var allPermission = Current.CategoryService(this.Service, ParentPermission.ManagedCareClaim, new int[] { (int)PermissionActions.Add, (int)PermissionActions.Edit, (int)PermissionActions.Print, (int)PermissionActions.Export, (int)PermissionActions.GenerateElectronicClaim, (int)PermissionActions.ViewOutstandingClaim });
                if (allPermission.IsNotNullOrEmpty())
                {
                    isViewPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewOutstandingClaim, AgencyServices.None) != AgencyServices.None;
                    if (isViewPermission)
                    {
                        isAddPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None) != AgencyServices.None;
                        isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                        isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                        isExportPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None) != AgencyServices.None;
                        isGeneratePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.GenerateElectronicClaim, AgencyServices.None) != AgencyServices.None;
                    }
                }
                bill.IsUserCanAdd = isAddPermission;
                bill.IsUserCanView = isViewPermission;
                bill.IsUserCanEdit = isEditPermission;
                bill.IsUserCanExport = isExportPermission;
                bill.IsUserCanPrint = isPrintPermission;
                bill.IsUserCanGenerate = isGeneratePermission;
                bill.ClaimType = ClaimTypeSubCategory.ManagedCare;
                bill.BranchName = agencyMainBranch.Name;
                bill.BranchId = agencyMainBranch.Id;

                if (agencyMedicareInsurance != null)
                {
                    bill.IsElectronicSubmssion = agencyMedicareInsurance.IsAxxessTheBiller;
                    bill.Claims = baseBillingRepository.GetManagedClaims(Current.AgencyId, agencyMainBranch.Id, agencyMedicareInsurance.Id, (int)ManagedClaimStatus.ClaimCreated, false);
                    bill.InsuranceName = agencyMedicareInsurance.Name;
                    bill.Insurance = agencyMedicareInsurance.Id;
                }
            }
            return bill;
        }

        public BillingJsonViewData UpdateProccesedManagedClaimStatus(Guid patientId, Guid Id, string claimDate, int status, string comment)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            viewData.Service = this.Service.ToString();
            viewData.Category = ClaimTypeSubCategory.ManagedCare;
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                //var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                //if (profile != null)
                //{
                var managedClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
                if (managedClaim != null)
                {
                    managedClaim.PaymentDate = DateTime.MinValue;
                    managedClaim.Comment = comment;
                    var oldStatus = managedClaim.Status;
                    var isStatusChange = managedClaim.Status != status;
                    bool oldStatusOpened = ManagedClaimStatusFactory.UnProcessed().Contains(oldStatus);
                    bool newStatusOpened = ManagedClaimStatusFactory.UnProcessed().Contains(status);

                    if (newStatusOpened)
                    {
                        managedClaim.IsInfoVerified = false;
                        managedClaim.IsSupplyVerified = false;
                        managedClaim.IsVisitVerified = false;
                        managedClaim.IsGenerated = false;
                    }
                    if (claimDate.IsValidDate())
                    {
                        managedClaim.ClaimDate = claimDate.ToDateTime();
                    }
                    else
                    {
                        managedClaim.ClaimDate = DateTime.MinValue;
                    }

                    if (isStatusChange && oldStatusOpened && !newStatusOpened)
                    {
                        if (!managedClaim.IsInfoVerified || !managedClaim.IsInsuranceVerified)
                        {
                            this.UpdateOrAddPayorForClaimAppSpecific<ManagedClaimInsurance>(managedClaim);
                            //managedClaim.Insurance = SerializedInsuranceForClaim<ManagedClaim>(managedClaim, profile.AgencyLocationId, managedClaim.PrimaryInsuranceId, claim => ((managedClaim.IsInfoVerified && managedClaim.Insurance.IsNullOrEmpty()) || (!managedClaim.IsInfoVerified))) ?? managedClaim.Insurance;
                        }
                    }
                    managedClaim.Status = status;
                    if (baseBillingRepository.UpdateManagedClaimModel(managedClaim))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, isStatusChange ? LogAction.ManagedUpdatedWithStatus : LogAction.ManagedUpdated, isStatusChange ? ((Enum.IsDefined(typeof(ManagedClaimStatus), oldStatus) ? ("From " + ((ManagedClaimStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ManagedClaimStatus), status) ? (" To " + ((ManagedClaimStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        viewData.PatientId = patientId;
                        viewData.IsManagedCareHistoryRefresh = true;
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim updated successfully.";

                        if (isStatusChange && (oldStatusOpened || newStatusOpened))
                        {
                            viewData.IsManagedCareRefresh = true;
                        }
                    }
                }
                // }

            }
            return viewData;
        }

        public ManagedClaimSnapShotViewData GetManagedClaimSnapShotInfo(Guid patientId, Guid claimId)
        {
            var claimInfo = new ManagedClaimSnapShotViewData();
            claimInfo.Service = this.Service;
            var allPermission = Current.CategoryService(this.Service, ParentPermission.ManagedCareClaim, new int[] { (int)PermissionActions.Add, (int)PermissionActions.Edit, (int)PermissionActions.ViewLog, (int)PermissionActions.ViewList });
            if (allPermission.IsNotNullOrEmpty())
            {
                claimInfo.IsUserCanAdd = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Add, AgencyServices.None).Has(this.Service);
                claimInfo.IsUserCanEdit = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None).Has(this.Service);
                claimInfo.IsUserCanViewList = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewList, AgencyServices.None).Has(this.Service);
                claimInfo.IsUserCanViewLog = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.ViewLog, AgencyServices.None).Has(this.Service);
            }
            var managedClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, claimId);
            if (managedClaim != null)
            {
                claimInfo.Id = managedClaim.Id;
                claimInfo.PatientId = managedClaim.PatientId;
                claimInfo.ClaimDate = managedClaim.ClaimDate;
                claimInfo.PaymentDate = managedClaim.PaymentDate;
                var payments = baseBillingRepository.GetManagedClaimPaymentsByClaimAndPatient(Current.AgencyId, patientId, claimId);
                if (payments != null && payments.Count > 0)
                {
                    var primaryPayments = payments.Where(p => p.Payor == managedClaim.PrimaryInsuranceId).ToList();
                    if (primaryPayments.IsNotNullOrEmpty())
                    {
                        claimInfo.PaymentDate = primaryPayments.Aggregate((agg, next) => next.PaymentDate > agg.PaymentDate ? next : agg).PaymentDate;
                    }
                }
                if (managedClaim.IsInfoVerified)
                {
                    claimInfo.PatientName = string.Format("{0} {1}", managedClaim.FirstName, managedClaim.LastName);
                    claimInfo.PatientIdNumber = managedClaim.PatientIdNumber;
                    claimInfo.IsuranceIdNumber = managedClaim.InsuranceIdNumber;
                }
                else
                {
                    var patient = patientRepository.GetPatientJsonByColumns(Current.AgencyId, patientId, "FirstName", "LastName", "PatientIdNumber", "MedicareNumber");
                    if (patient != null)
                    {
                        claimInfo.PatientName = string.Format("{0} {1}", patient["FirstName"], patient["LastName"]);
                        claimInfo.PatientIdNumber = patient["PatientIdNumber"];
                        claimInfo.IsuranceIdNumber = patient["MedicareNumber"];
                    }
                }
                claimInfo.AuthorizationNumber = managedClaim.AuthorizationNumber;
                claimInfo.HealthPlainId = managedClaim.HealthPlanId;
                var hhrg = lookUpRepository.GetHHRGByHIPPSCODE(managedClaim.HippsCode);
                if (hhrg != null)
                {
                    claimInfo.HHRG = hhrg.HHRG;
                }
                claimInfo.HIPPS = managedClaim.HippsCode;
                claimInfo.ClaimKey = managedClaim.ClaimKey;

                if (managedClaim.HippsCode.IsNotNullOrEmpty())
                {
                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                    PPSStandard ppsStandard;
                    claimInfo.SupplyReimbursement = GetSupplyReimbursement(managedClaim.HippsCode[managedClaim.HippsCode.Length - 1], managedClaim.EpisodeStartDate.Year, out ppsStandard);
                    claimInfo.StandardEpisodeRate = Math.Round(lookUpRepository.GetProspectivePaymentAmount(managedClaim.HippsCode, managedClaim.EpisodeStartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                }
                claimInfo.ProspectivePay = claimInfo.StandardEpisodeRate;
                claimInfo.PayorName = InsuranceEngine.GetName(managedClaim.PrimaryInsuranceId, Current.AgencyId);
                claimInfo.Visible = true;
            }
            return claimInfo;
        }

        public List<ManagedClaimAdjustment> GetManagedClaimAdjustments(Guid claimId)
        {
            var adjustments = baseBillingRepository.GetManagedClaimAdjustmentsByClaim(Current.AgencyId, claimId).OrderBy(o => o.Created).ToList();
            var adjustmentCodes = agencyRepository.GetAdjustmentCodes(Current.AgencyId);
            adjustments.ForEach(a =>
            {
                var code = adjustmentCodes.FirstOrDefault(ac => ac.Id == a.TypeId);
                if (code != null)
                {
                    a.Type = code.Code;
                    a.Description = code.Description;
                }
            });
            return adjustments;
        }

        public List<Claim> AllUnProcessedManagedClaims(Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded)
        {
            var claims = baseBillingRepository.GetManagedClaims(Current.AgencyId, branchId, insuranceId, (int)ManagedClaimStatus.ClaimCreated, IsZeroInsuraceIdAll) ?? new List<Claim>();
            var users = new List<User>();
            if (isUsersNeeded)
            {
                var userIds = claims.Select(c => c.ClinicianId).ToList();
                userIds.AddRange(claims.Select(c => c.CaseManagerId));
                userIds = userIds.Distinct().ToList();
                users = UserEngine.GetUsers(Current.AgencyId, userIds);
                claims.ForEach((c) =>
                {
                    var clinician = users.FirstOrDefault(u => u.Id == c.ClinicianId);
                    var casemanager = users.FirstOrDefault(u => u.Id == c.CaseManagerId);
                    c.Clinician = clinician != null ? clinician.DisplayName : string.Empty;
                    c.CaseManager = casemanager != null ? casemanager.DisplayName : string.Empty;
                });
            }
            return claims;
        }

        public List<ManagedClaimPayment> GetManagedClaimPaymentsWithInsurance(Guid claimId)
        {
            var payments = baseBillingRepository.GetManagedClaimPaymentsByClaim(Current.AgencyId, claimId).OrderBy(o => o.Created).ToList();
            var insurances = agencyRepository.GetInsurances(Current.AgencyId);
            foreach (var payment in payments)
            {
                payment.PayorName = payment.Payor == 0 ? "" : insurances.FirstOrDefault(p => p.Id == payment.Payor).Name;
            }
            return payments;
        }

        public Bill ManagedBill(Guid branchId, int insuranceId, int status)
        {
            var manageBill = new Bill();
            manageBill.Claims = baseBillingRepository.GetManagedClaims(Current.AgencyId, branchId, insuranceId, status, false).ToList();
            manageBill.BranchId = branchId;
            manageBill.Insurance = insuranceId;
            manageBill.Service = this.Service;
            return manageBill;
        }

        private Bill ManagedClaimToGenerate(List<Guid> managedClaimToGenerate, Guid branchId, int primaryInsurance)
        {
            var bill = new Bill();
            bill.Service = this.Service;
            bill.BranchId = branchId;
            bill.Insurance = primaryInsurance;
            bill.ClaimType = ClaimTypeSubCategory.ManagedCare;
            var location = agencyRepository.FindLocationOrMain(Current.AgencyId, branchId);
            if (location != null)
            {
                bill.Claims = baseBillingRepository.GetManagedClaimByIds(Current.AgencyId, branchId, primaryInsurance, managedClaimToGenerate);
                bill.BranchName = location.Name;
                var isElectronicSubmssion = false;
                var insurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
                if (insurance != null)
                {
                    //isElectronicSubmssion = insurance.IsAxxessTheBiller;
                    bill.InsuranceName = insurance.Name;
                }
                bill.IsElectronicSubmssion = isElectronicSubmssion;
            }
            return bill;
        }
    
        public Bill ClaimToGenerate(List<Guid> claimSelected, Guid branchId, int primaryInsurance, string type)
        {
            var bill = new Bill();
            if (IsManagedClaim(type))
            {
                bill = this.ManagedClaimToGenerate(claimSelected, branchId, primaryInsurance);
            }
            else
            {
                bill = this.ClaimToGenerateAppSpecific(claimSelected, branchId, primaryInsurance, type);
            }

            return bill;
            
        }

        protected abstract Bill ClaimToGenerateAppSpecific(List<Guid> claimSelected, Guid branchId, int primaryInsurance, string type);

        public List<ChargeRate> MedicareBillRate(List<ChargeRate> disciplineRates)
        {
            var chargeRates = new List<ChargeRate>();
            var disciplineTaskArray = lookUpRepository.DisciplineTasks();
            disciplineRates = disciplineRates ?? new List<ChargeRate>();
            if (disciplineTaskArray.IsNotNullOrEmpty())
            {
                foreach (var task in disciplineTaskArray)
                {
                    var disciplineRate = disciplineRates.FirstOrDefault(d => d.Id == task.Id);
                    if (disciplineRate != null)
                    {
                        disciplineRate.ChargeType = ((int)BillUnitType.Per15Min).ToString();
                        chargeRates.Add(disciplineRate);
                    }
                    else
                    {
                        var rate = new ChargeRate();
                        rate.Unit = task.Unit;
                        rate.RevenueCode = task.RevenueCode;
                        rate.Code = task.GCode;
                        rate.Charge = task.Rate;
                        rate.Id = task.Id;
                        rate.ChargeType = ((int)BillUnitType.Per15Min).ToString();
                        chargeRates.Add(rate);
                    }
                }
            }
            return chargeRates;
        }

        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillableVisitsData(List<T> visits, ClaimType claimType, List<ChargeRate> chargeRates, bool isLimitApplied)
        {
            var visitDatas = new Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>>();
            if (visits.IsNotNullOrEmpty())
            {
                foreach (BillVisitCategory billVisitCategory in Enum.GetValues(typeof(BillVisitCategory)))
                {
                    var billCategoryVisits = this.BillCategoryVisits(billVisitCategory, visits);
                    if (billCategoryVisits != null && billCategoryVisits.Count > 0)
                    {
                        var categoryVisitsDictionary = new Dictionary<BillDiscipline, List<BillSchedule>>();
                        foreach (BillDiscipline discipline in Enum.GetValues(typeof(BillDiscipline)))
                        {
                            var disciplineVisits = billCategoryVisits.Where(v => v.Discipline == discipline.ToString()).ToList();
                            if (disciplineVisits != null && disciplineVisits.Count > 0)
                            {
                                var visitsDictionary = disciplineVisits.GroupBy(v => v.VisitDate.Date).ToDictionary(v => v.Key, v => v.ToList());
                                if (visitsDictionary != null && visitsDictionary.Count > 0)
                                {
                                    var billVisits = new List<BillSchedule>();
                                    visitsDictionary.ForEach((visitPerDayKey, visitPerDayValue) =>
                                    {
                                        var visitByDisciplneDictionary = visitPerDayValue.GroupBy(vd => vd.DisciplineTask).ToDictionary(vd => vd.Key, vd => vd.ToList());
                                        if (visitByDisciplneDictionary != null && visitByDisciplneDictionary.Count > 0)
                                        {
                                            visitByDisciplneDictionary.ForEach((vddk, vddv) =>
                                            {
                                                var rate = chargeRates.FirstOrDefault(c => c.Id == vddk);
                                                if (rate != null)
                                                {
                                                    var billSchedules = BillUnitTypeSchedules(rate, vddv, claimType, isLimitApplied);
                                                    if (billSchedules.IsNotNullOrEmpty())
                                                    {
                                                        billVisits.AddRange(billSchedules);
                                                    }
                                                }
                                                else
                                                {
                                                    var billSchedules = VisitsToBillSchedules(vddv);
                                                    if (billSchedules.IsNotNullOrEmpty())
                                                    {
                                                        billVisits.AddRange(billSchedules);
                                                    }
                                                }
                                            });
                                        }
                                    });

                                    if (billVisits.IsNotNullOrEmpty())
                                    {
                                        categoryVisitsDictionary.Add(discipline, billVisits);
                                    }
                                }
                            }
                        }
                        if (categoryVisitsDictionary.IsNotNullOrEmpty())
                        {
                            visitDatas.Add(billVisitCategory, categoryVisitsDictionary);
                        }
                    }
                }
            }
            return visitDatas;
        }
        /// <summary>
        /// this is used when there is no rate verified for the visit
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        protected BillSchedule VisitToBillSchedule(T visit)
        {
            var schedule = new BillSchedule();
            schedule.EventId = visit.Id;
            schedule.EventDate = visit.EventDate;
            schedule.VisitDate = visit.VisitDate;
            schedule.DisciplineTaskName = visit.DisciplineTaskName;
            schedule.StatusName = visit.StatusName;
            return schedule;
        }

        protected List<BillSchedule> VisitsToBillSchedules(List<T> visits)
        {
            var schedules = new List<BillSchedule>();
            if (visits.IsNotNullOrEmpty())
            {
                visits.ForEach(v =>
                {
                    schedules.Add(VisitToBillSchedule(v));
                });
            }
            return schedules;
        }

        protected BillSchedule GetBillSchedule(ChargeRate rate, T visit, ClaimType claimType, bool isLimitApplied)
        {
            var schedule = new BillSchedule();
            schedule.EventId = visit.Id;
            schedule.EventDate = visit.EventDate;
            schedule.VisitDate = visit.VisitDate;
            schedule.DisciplineTaskName = visit.DisciplineTaskName;
            schedule.PereferredName = rate.PreferredDescription.IsNotNullOrEmpty() ? rate.PreferredDescription : schedule.DisciplineTaskName;
            schedule.StatusName = visit.StatusName;
            schedule.HCPCSCode = rate.Code;
            schedule.RevenueCode = rate.RevenueCode;
            schedule.Modifier = rate.Modifier;
            schedule.Modifier2 = rate.Modifier2;
            schedule.Modifier3 = rate.Modifier3;
            schedule.Modifier4 = rate.Modifier4;
            int unit = 0;
            double charge = 0;
            CaluculateChargeAndUnit(rate, new List<T> { visit }, claimType, out unit, out charge);
            schedule.Unit = unit;
            schedule.Charge = charge;
            return schedule;
        }

        protected BillSchedule GetBillSchedule(ChargeRate rate, List<T> visits, ClaimType claimType, bool isLimitApplied)
        {
            var schedule = new BillSchedule();
            if (visits != null && visits.Count > 0)
            {
                var visit = visits.FirstOrDefault();
                if (visit != null)
                {
                    if (visits.Count == 1)
                    {
                        schedule.EventId = visit.Id;
                        schedule.StatusName = visit.StatusName;
                    }
                    else
                    {
                        if (visits.TrueForAll(v => v.StatusName == visit.StatusName))
                        {
                            schedule.StatusName = visit.StatusName;
                        }
                    }
                    schedule.EventDate = visit.EventDate;
                    schedule.VisitDate = visit.VisitDate;
                    schedule.DisciplineTaskName = visit.DisciplineTaskName;
                    schedule.PereferredName = rate.PreferredDescription.IsNotNullOrEmpty() ? rate.PreferredDescription : schedule.DisciplineTaskName;
                    schedule.HCPCSCode = rate.Code;
                    schedule.RevenueCode = rate.RevenueCode;
                    schedule.Modifier = rate.Modifier;
                    schedule.Modifier2 = rate.Modifier2;
                    schedule.Modifier3 = rate.Modifier3;
                    schedule.Modifier4 = rate.Modifier4;
                    int unit = 0;
                    double charge = 0;
                    CaluculateChargeAndUnit(rate, visits, claimType, out unit, out charge);
                    schedule.UnderlyingVisits = new List<BillSchedule>();
                    foreach (var v in visits)
                    {
                        schedule.UnderlyingVisits.Add(GetBillSchedule(rate, v, claimType, isLimitApplied));
                    }
                    schedule.Unit = unit;
                    schedule.Charge = charge;

                }
                else
                {
                    return null;

                }
            }
            else
            {
                return null;

            }
            return schedule;
        }

        protected static void CaluculateChargeAndUnit(ChargeRate rate, List<T> visits, ClaimType claimType, out int unitOut, out double chargeOut)
        {
            chargeOut = 0;
            unitOut = 0;
            if (rate.ChargeType.IsNotNullOrEmpty() && rate.ChargeType.IsInteger())
            {
                if (visits.IsNotNullOrEmpty())
                {
                    foreach (var visit in visits)
                    {
                        var visitUnit = 0;
                        switch (rate.ChargeType.ToInteger())
                        {
                            case (int)BillUnitType.PerVisit:
                                {
                                    if (claimType == ClaimType.CMS || claimType == ClaimType.HMO)
                                    {
                                        unitOut += (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        chargeOut += rate.Charge;
                                    }
                                    else
                                    {
                                        unitOut += rate.Unit;
                                        chargeOut += rate.Charge;
                                    }
                                }
                                break;
                            case (int)BillUnitType.Hourly:
                                {
                                    if (claimType == ClaimType.CMS)
                                    {
                                        unitOut += (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        chargeOut += rate.Charge;
                                    }
                                    else if (claimType == ClaimType.HMO)
                                    {
                                        var perUnitMin = rate.MedicareHMORate > 0 ? 15 : 60;
                                        visitUnit = (int)Math.Ceiling((double)visit.MinSpent / perUnitMin);
                                        unitOut += visitUnit;
                                        chargeOut += rate.MedicareHMORate > 0 ? rate.MedicareHMORate : visitUnit * rate.Charge;
                                    }
                                    else
                                    {
                                        visitUnit = (int)Math.Ceiling((double)visit.MinSpent / 60);
                                        unitOut += visitUnit;
                                        chargeOut += visitUnit * rate.Charge;
                                    }
                                }
                                break;

                            case (int)BillUnitType.Per15Min:
                                {
                                    if (claimType == ClaimType.CMS)
                                    {
                                        unitOut += (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        chargeOut += rate.Charge;
                                    }
                                    else if (claimType == ClaimType.HMO)
                                    {
                                        visitUnit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        unitOut += visitUnit;
                                        chargeOut += rate.MedicareHMORate > 0 ? rate.MedicareHMORate : visitUnit * rate.Charge;
                                    }
                                    else
                                    {
                                        visitUnit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        unitOut += visitUnit;
                                        chargeOut += visitUnit * rate.Charge;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }

        //protected List<object> BillableVisitForANSI(ChargeRate rate, List<T> visits, out double totalCharge, ClaimType claimType, bool isLimitApplied)
        //{
        //    totalCharge = 0;
        //    var schedules = new List<object>();
        //    if (visits != null && visits.Count > 0 && rate != null)
        //    {
        //        if (rate.IsUnitsPerDayOnSingleLineItem)
        //        {
        //            var oneDaySchedule = GetBillSchedule(rate, visits, claimType, isLimitApplied);
        //            if (oneDaySchedule != null)
        //            {
        //                var discipline = visits[0].GIdentify();
        //                schedules.Add(GenerateBillScheduleObject(oneDaySchedule, discipline, rate));
        //            }
        //        }
        //        else
        //        {
        //            foreach (var visit in visits)
        //            {
        //                int unitOut;
        //                double chargeOut;
        //                var discipline = visit.GIdentify();
        //                if (rate.ChargeType.IsNotNullOrEmpty() && rate.ChargeType.IsInteger())
        //                {
        //                    switch (rate.ChargeType.ToInteger())
        //                    {
        //                        case (int)BillUnitType.PerVisit:
        //                            {
        //                                CaluculateChargeAndUnit(rate, new List<T> { visit }, claimType, out unitOut, out chargeOut);
        //                                schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unitOut, chargeOut));
        //                                totalCharge += rate.Charge;
        //                            }
        //                            break;
        //                        case (int)BillUnitType.Hourly:
        //                            totalCharge += GenerateHourlyBillObject(rate, visit, claimType, isLimitApplied, ref schedules, discipline);
        //                            break;

        //                        case (int)BillUnitType.Per15Min:
        //                            totalCharge += GeneratePer15MinBillObject(rate, visit, claimType, isLimitApplied, ref schedules, discipline);
        //                            break;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return schedules;
        //}

        public AgencyInsurance CMSInsuranceToAgencyInsurance(Guid branchId, int insuranceId, bool IsAddressNeeded)
        {
            var agencyInsurance = new AgencyInsurance();
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    agencyInsurance = this.CMSInsuranceToAgencyInsurance(agency, location, insuranceId, IsAddressNeeded);
                }
            }
            return agencyInsurance;
        }

        public AgencyInsurance CMSInsuranceToAgencyInsurance(Agency agency, AgencyLocation location, int insuranceId, bool IsAddressNeeded)
        {
            var agencyInsurance = new AgencyInsurance();

            if (agency != null)
            {
                if (location != null)
                {
                    var insuranceName = Enum.IsDefined(typeof(MedicareIntermediary), insuranceId) ? ((MedicareIntermediary)insuranceId).GetDescription() : string.Empty;// lookUpRepository.GetInsurance(insuranceId);
                    var axxessData = lookUpRepository.SubmitterInfo(insuranceId);

                    var chargeRates = new List<ChargeRate>();
                    var data = new List<ChargeRate>();
                    if (location.BillData.IsNotNullOrEmpty())
                    {
                        data = location.BillData.ToObject<List<ChargeRate>>() ?? new List<ChargeRate>();
                    }
                    var medicareChargeRatesDictionary = this.MedicareBillRate(data);

                    if (medicareChargeRatesDictionary != null && medicareChargeRatesDictionary.Count > 0)
                    {
                        chargeRates = medicareChargeRatesDictionary;
                    }
                    if (!location.IsLocationStandAlone)
                    {
                        if (agency.IsAxxessTheBiller)
                        {
                            if (axxessData != null)
                            {
                                agency.SubmitterId = axxessData.SubmitterId;
                                agency.SubmitterName = axxessData.SubmitterName;
                                agency.SubmitterPhone = axxessData.Phone;
                                agency.SubmitterFax = axxessData.Fax;
                            }
                        }
                        agencyInsurance = new AgencyInsurance
                        {
                            Id = insuranceId,
                            AgencyId = Current.AgencyId,
                            PayorType = (int)PayerTypes.MedicareTraditional,
                            InvoiceType = (int)InvoiceType.UB,
                            BillType = "institutional",
                            Name = insuranceName,
                            PayorId = axxessData != null ? axxessData.Code : string.Empty,
                            SubmitterId = agency.SubmitterId,
                            SubmitterName = agency.SubmitterName,
                            SubmitterPhone = agency.SubmitterPhone,
                            FaxNumber = agency.SubmitterFax,
                            Ub04Locator81cca = location.Ub04Locator81cca,
                            IsAxxessTheBiller = agency.IsAxxessTheBiller,
                            BillData = chargeRates.ToXml()
                        };
                    }
                    else
                    {
                        if (location.IsAxxessTheBiller)
                        {
                            if (axxessData != null)
                            {
                                location.SubmitterId = axxessData.SubmitterId;
                                location.SubmitterName = axxessData.SubmitterName;
                                location.SubmitterPhone = axxessData.Phone;
                                location.SubmitterFax = axxessData.Fax;
                            }
                        }
                        agencyInsurance = new AgencyInsurance
                        {
                            Id = insuranceId,
                            AgencyId = Current.AgencyId,
                            PayorType = (int)PayerTypes.MedicareTraditional,
                            InvoiceType = (int)InvoiceType.UB,
                            BillType = "institutional",
                            Name = insuranceName,
                            PayorId = axxessData != null ? axxessData.Code : string.Empty,
                            SubmitterId = location.SubmitterId,
                            SubmitterName = location.SubmitterName,
                            SubmitterPhone = location.SubmitterPhone,
                            FaxNumber = location.SubmitterFax,
                            Ub04Locator81cca = location.Ub04Locator81cca,
                            IsAxxessTheBiller = axxessData != null ? axxessData.SubmitterId.IsEqual(location.SubmitterId) : false,
                            BillData = chargeRates.ToXml()
                        };
                    }
                    if (agencyInsurance != null && IsAddressNeeded)
                    {
                        var medicareLocation = agencyRepository.FindLocationMedicareInsurance(Current.AgencyId, insuranceId);
                        if (medicareLocation != null)
                        {
                            agencyInsurance.AddressLine1 = medicareLocation.AddressLine1;
                            agencyInsurance.AddressLine2 = medicareLocation.AddressLine2;
                            agencyInsurance.AddressCity = medicareLocation.AddressCity;
                            agencyInsurance.AddressStateCode = medicareLocation.AddressStateCode;
                            agencyInsurance.AddressZipCode = medicareLocation.AddressZipCode;

                        }
                    }
                }
            }
            return agencyInsurance;
        }


        protected AgencyInsurance ClaimToInsuranceNoLocationId(Guid patientId, int primaryInsuranceId, bool IsAddressNeeded)
        {
            AgencyInsurance agencyInsurance =null;
            if (primaryInsuranceId >= 1000)
            {
                agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, primaryInsuranceId);
            }
            else if (MedicareIntermediaryFactory.Intermediaries().Contains(primaryInsuranceId))
            {
                var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
                if (profile != null)
                {
                    agencyInsurance = CMSInsuranceToAgencyInsurance(profile.AgencyLocationId, primaryInsuranceId, IsAddressNeeded);
                }
            }
            return agencyInsurance;
        }


        protected AgencyInsurance GetInsuranceHelper(Guid agencyLocationId, int primaryInsuranceId, bool IsAddressNeeded)
        {
            var agencyInsurance = new AgencyInsurance();
            if (primaryInsuranceId > 0)
            {
                if (MedicareIntermediaryFactory.Intermediaries().Contains(primaryInsuranceId))
                {
                    agencyInsurance = this.CMSInsuranceToAgencyInsurance(agencyLocationId, primaryInsuranceId, IsAddressNeeded);
                }
                else if (primaryInsuranceId >= 1000)
                {
                    agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, primaryInsuranceId);
                }
            }
            return agencyInsurance;
        }


        protected Relationship ClaimRelationship(string relationshipId)
        {
            var relationship = new Relationship();
            if (relationshipId.IsNotNullOrEmpty())
            {
                if (relationshipId.IsInteger())
                {
                    relationship = lookUpRepository.GetRelationship(relationshipId.ToInteger());
                }
                else
                {
                    int relationshipValue = -1;
                    if (relationshipId == "Self")
                    {
                        relationshipValue = 2;
                    }
                    else if (relationshipId == "Spouse")
                    {
                        relationshipValue = 1;
                    }
                    else if (relationshipId == "Child")
                    {
                        relationshipValue = 3;
                    }
                    else if (relationshipId == "Other")
                    {
                        relationshipValue = 9;
                    }
                    if (relationshipValue != -1)
                    {
                        relationship = lookUpRepository.GetRelationship(relationshipValue);
                    }
                }
            }
            return relationship;
        }

        public BillingJsonViewData AddManagedClaimPayment(ManagedClaimPayment payment)
        {
            var rules = new List<Validation>();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The payment could not be added. Please try again." };
            viewData.Service = this.Service.ToString();
            if (payment != null)
            {
                rules.Add(new Validation(() => payment.PaymentAmount.IsNotNullOrEmpty() ? !payment.PaymentAmount.IsDouble() : false, "Payment Value is not a right format."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    payment.Payment = payment.PaymentAmount.ToDouble();
                    payment.Id = Guid.NewGuid();
                    payment.AgencyId = Current.AgencyId;
                    payment.Comments = payment.Comments != null ? payment.Comments : "";
                    if (baseBillingRepository.AddManagedClaimPayment(payment))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, payment.PatientId, payment.ClaimId.ToString(), LogType.ManagedClaim, LogAction.PaymentAdded, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.PatientId = payment.PatientId;
                        viewData.IsManagedCareRefresh = true;
                        viewData.errorMessage = "The payment has been added successfully.";
                    }
                }
            }
            return viewData;
        }

        public ManagedClaimPayment GetManagedClaimPayment(Guid id)
        {
            var payment = baseBillingRepository.GetManagedClaimPayment(Current.AgencyId, id);
            if (payment != null)
            {
                payment.Service = this.Service;
            }
            return payment ;
        }

        public BillingJsonViewData DeleteManagedClaimPayment(Guid patientId, Guid claimId, Guid id)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The payment could not be deleted. Please try again." };
            viewData.Service = this.Service.ToString();
            if (baseBillingRepository.DeleteManagedClaimPayment(id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, claimId.ToString(), LogType.ManagedClaim, LogAction.PaymentDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.PatientId = patientId;
                viewData.IsManagedCareRefresh = true;
                viewData.errorMessage = "The payment has been deleted successfully.";
            }
            return viewData;
        }

        public List<ManagedClaimPayment> GetManagedClaimPaymentsByPatient(Guid patientId)
        {
            return baseBillingRepository.GetManagedClaimPaymentsByPatient(Current.AgencyId, patientId);
        }

        public JsonViewData MarkManagedClaimSubmitted(List<Guid> managedClaimToGenerate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) were unsuccessful in updating their status." };
            if (managedClaimToGenerate.IsNotNullOrEmpty())
            {
                var claims = baseBillingRepository.GetManagedClaimsToGenerateByIdsLean(Current.AgencyId, managedClaimToGenerate);
                if (claims.IsNotNullOrEmpty())
                {
                    var count = baseBillingRepository.MarkManagedClaimsAsSubmitted(Current.AgencyId, claims);
                    if (count > 0)
                    {
                        Auditor.AddGeneralMulitLog(LogDomain.Patient, claims.GroupBy(c => c.PatientId).ToDictionary(g => g.Key, g => g.Select(r => r.Id.ToString()).ToList()), LogType.ManagedClaim, LogAction.ManagedMarkedAsSubmitted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = count == managedClaimToGenerate.Count ? "All of the claim(s) were successful in updating their status." : string.Format("{0} of {1} claim(s) were successful in updating their status.", count, managedClaimToGenerate.Count);
                    }
                }
                //int successCounter = 0;
                //managedClaimToGenerate.ForEach(id =>
                //{
                //    var managedClaim = baseBillingRepository.GetManagedClaim(Current.AgencyId, id);
                //    if (managedClaim != null)
                //    {
                //        var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, managedClaim.PatientId);
                //        if (profile != null)
                //        {
                //            var oldStatus = managedClaim.Status;
                //            if (statusType == "Submit")
                //            {
                //                if (managedClaim.Status != (int)ManagedClaimStatus.ClaimSubmitted)
                //                {
                //                    managedClaim.Status = (int)ManagedClaimStatus.ClaimSubmitted;
                //                    managedClaim.ClaimDate = DateTime.Now;
                //                }
                //                managedClaim.IsGenerated = true;
                //                managedClaim.IsInfoVerified = true;
                //                managedClaim.IsVisitVerified = true;
                //                managedClaim.IsSupplyVerified = true;
                //            }
                //            else if (statusType == "Cancelled")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimCancelledClaim;
                //            }
                //            else if (statusType == "Rejected")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimRejected;
                //            }
                //            else if (statusType == "Accepted")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimAccepted;
                //            }
                //            else if (statusType == "PaymentPending")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimPaymentPending;
                //            }
                //            else if (statusType == "Error")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimWithErrors;
                //            }
                //            else if (statusType == "Paid")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimPaidClaim;
                //            }
                //            else if (statusType == "ReOpen")
                //            {
                //                managedClaim.Status = (int)ManagedClaimStatus.ClaimReOpen;
                //                managedClaim.ClaimDate = DateTime.MinValue;
                //                managedClaim.IsInfoVerified = false;
                //                managedClaim.IsSupplyVerified = false;
                //                managedClaim.IsVisitVerified = false;
                //                managedClaim.IsGenerated = false;
                //            }
                //            var isStatusChange = oldStatus != managedClaim.Status;
                //            if (isStatusChange && (oldStatus == (int)ManagedClaimStatus.ClaimCreated || oldStatus == (int)ManagedClaimStatus.ClaimReOpen))
                //            {
                //                managedClaim.Insurance = SerializedInsuranceForClaim<ManagedClaim>(managedClaim, profile.AgencyLocationId, managedClaim.PrimaryInsuranceId, claim => ((managedClaim.IsInfoVerified && managedClaim.Insurance.IsNullOrEmpty()) || (!managedClaim.IsInfoVerified))) ?? managedClaim.Insurance;
                //            }
                //            if (baseBillingRepository.UpdateManagedClaim(managedClaim))
                //            {
                //                successCounter++;
                //                if (isStatusChange)
                //                {
                //                    Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedUpdatedWithStatus, ((Enum.IsDefined(typeof(ManagedClaimStatus), oldStatus) ? ("From " + ((ManagedClaimStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ManagedClaimStatus), managedClaim.Status) ? (" To " + ((ManagedClaimStatus)managedClaim.Status).GetDescription()) : string.Empty)));
                //                }
                //            }
                //        }
                //    }
                //});
                //if (successCounter > 0)
                //{
                //    viewData.isSuccessful = true;
                //    viewData.errorMessage = successCounter == managedClaimToGenerate.Count ? "All of the claim(s) were successful in updating their status." : string.Format("{0} of {1} claim(s) were successful in updating their status.", successCounter, managedClaimToGenerate.Count);
                //}
            }
            return viewData;
        }

        public BillingJsonViewData UpdateManagedClaimPayment(ManagedClaimPayment claimPayment)
        {
            var rules = new List<Validation>();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The payment could not be updated." };
            viewData.Service = this.Service.ToString();
            if (claimPayment.IsValid())
            {
                if (baseBillingRepository.UpdateManagedClaimPayment(claimPayment))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, claimPayment.PatientId, claimPayment.ClaimId.ToString(), LogType.ManagedClaim, LogAction.PaymentUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.IsManagedCareRefresh = true;
                    viewData.PatientId = claimPayment.PatientId;
                    viewData.errorMessage = "The payment updated successfully.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = claimPayment.ValidationMessage;
            }
            return viewData;
        }

        public ManagedClaimAdjustment GetManagedClaimAdjustment(Guid Id)
        {
            var adjustment = baseBillingRepository.GetManagedClaimAdjustment(Current.AgencyId, Id);
            if (adjustment != null)
            {
                adjustment.Service = this.Service;
            }
            return adjustment;
        }

        public BillingJsonViewData AddManagedClaimAdjustment(ManagedClaimAdjustment adjustment)
        {
            var rules = new List<Validation>();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The adjustment could not be added. Please try again." };
            viewData.Service = this.Service.ToString();
            if (adjustment != null)
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    adjustment.Id = Guid.NewGuid();
                    adjustment.AgencyId = Current.AgencyId;
                    adjustment.Comments = adjustment.Comments != null ? adjustment.Comments : "";
                    if (baseBillingRepository.AddManagedClaimAdjustment(adjustment))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, adjustment.PatientId, adjustment.ClaimId.ToString(), LogType.ManagedClaim, LogAction.AdjustmentAdded, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.IsManagedCareRefresh = true;
                        viewData.PatientId = adjustment.PatientId;
                        viewData.errorMessage = "The adjustment has been added successfully.";
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData UpdateManagedClaimAdjustment(ManagedClaimAdjustment managedClaimAdjustment)
        {
            var rules = new List<Validation>();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The adjustment could not be updated. Please try again." };
            if (managedClaimAdjustment != null)
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    managedClaimAdjustment.AgencyId = Current.AgencyId;
                    if (baseBillingRepository.UpdateManagedClaimAdjustment(managedClaimAdjustment))
                    {
                        viewData.isSuccessful = true;
                        viewData.PatientId = managedClaimAdjustment.PatientId;
                        viewData.IsManagedCareRefresh = true;
                        viewData.errorMessage = "The adjustment has been updated successfully.";
                    }
                }
            }
            return viewData;
        }

        public BillingJsonViewData DeleteManagedClaimAdjustment(Guid patientId, Guid claimId, Guid id)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The adjustment could not be deleted. Please try again." };
            if (baseBillingRepository.DeleteManagedClaimAdjustment(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.IsManagedCareRefresh = true;
                viewData.PatientId = patientId;
                viewData.errorMessage = "The adjustment has been deleted successfully.";
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, claimId.ToString(), LogType.ManagedClaim, LogAction.AdjustmentDeleted, string.Empty);
            }
            return viewData;
        }

        public List<ClaimLean> GetManagedClaimsWithPaymentData(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var claims = baseBillingRepository.GetManagedClaims(Current.AgencyId, branchId, status, startDate, endDate);
            if (claims != null && claims.Count > 0)
            {
                var payments = baseBillingRepository.GetManagedClaimPayments(Current.AgencyId);
                var adjustments = baseBillingRepository.GetManagedClaimAdjustments(Current.AgencyId);
                var isPayments = payments != null && payments.Count > 0;
                var isAdjustments = adjustments != null && adjustments.Count > 0;
                claims.ForEach(claim =>
                {
                    claim.PaymentAmount = 0;
                    claim.PaymentDate = DateTime.MinValue;
                    if (isPayments)
                    {
                        var claimPayments = payments.Where(p => p.ClaimId == claim.Id).ToList();
                        if (claimPayments.IsNotNullOrEmpty())
                        {
                            claim.PaymentAmount = claimPayments.Sum(cp => cp.Payment);
                            var primaryPayment = claimPayments.Where(p => p.Payor == claim.PrimaryInsuranceId).OrderByDescending(p => p.PaymentDate).FirstOrDefault();//.Aggregate((agg, next) => next.PaymentDate > agg.PaymentDate ? next : agg);
                            if (primaryPayment != null)
                            {
                                claim.PaymentDate = primaryPayment.PaymentDate;
                            }
                        }
                    }
                    if (isAdjustments)
                    {
                        var claimAdjustments = adjustments.Where(p => p.ClaimId == claim.Id).ToList();
                        if (claimAdjustments.IsNotNullOrEmpty())
                        {
                            claim.AdjustmentAmount = claimAdjustments.Sum(cp => cp.Adjustment);
                        }
                    }
                });
            }
            else if (claims == null)
            {
                claims = new List<ClaimLean>();
            }
            return claims;
        }

        protected double GetSupplyReimbursement(char type, int year, out PPSStandard ppsStandardOut)
        {
            var ppsStandard = PPSstandardEngine.Instance.Get(year);
            ppsStandardOut = ppsStandard;
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        return ppsStandard.S;
                    case 'T':
                        return ppsStandard.T;
                    case 'U':
                        return ppsStandard.U;
                    case 'V':
                        return ppsStandard.V;
                    case 'W':
                        return ppsStandard.W;
                    case 'X':
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        protected double GetSupplyReimbursement(char type, int year)
        {
            var ppsStandard = PPSstandardEngine.Instance.Get(year);
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        return ppsStandard.S;
                    case 'T':
                        return ppsStandard.T;
                    case 'U':
                        return ppsStandard.U;
                    case 'V':
                        return ppsStandard.V;
                    case 'W':
                        return ppsStandard.W;
                    case 'X':
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        //protected List<Supply> GetSupply<T>(T scheduleEvent) where T : ITask, new()
        //{
        //    var supplies = new List<Supply>();
        //    switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
        //    {
        //        case DisciplineTasks.NonOASISRecertification:
        //        case DisciplineTasks.NonOASISStartOfCare:
        //        case DisciplineTasks.OASISCStartOfCare:
        //        case DisciplineTasks.OASISCStartOfCarePT:
        //        case DisciplineTasks.OASISCStartOfCareOT:
        //        case DisciplineTasks.OASISCResumptionofCare:
        //        case DisciplineTasks.OASISCResumptionofCarePT:
        //        case DisciplineTasks.OASISCResumptionofCareOT:
        //        case DisciplineTasks.OASISCFollowUp:
        //        case DisciplineTasks.OASISCFollowupPT:
        //        case DisciplineTasks.OASISCFollowupOT:
        //        case DisciplineTasks.OASISCRecertification:
        //        case DisciplineTasks.OASISCRecertificationPT:
        //        case DisciplineTasks.OASISCRecertificationOT:
        //            var assessment = assessmentRepository.GetAssessmentOnly(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
        //            if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
        //            {
        //                supplies = assessment.Supply.ToObject<List<Supply>>();
        //            }
        //            break;
        //        case DisciplineTasks.SkilledNurseVisit:
        //        case DisciplineTasks.SNInsulinAM:
        //        case DisciplineTasks.SNInsulinPM:
        //        case DisciplineTasks.SNInsulinHS:
        //        case DisciplineTasks.SNInsulinNoon:
        //        case DisciplineTasks.FoleyCathChange:
        //        case DisciplineTasks.SNB12INJ:
        //        case DisciplineTasks.SNBMP:
        //        case DisciplineTasks.SNCBC:
        //        case DisciplineTasks.SNHaldolInj:
        //        case DisciplineTasks.PICCMidlinePlacement:
        //        case DisciplineTasks.PRNFoleyChange:
        //        case DisciplineTasks.PRNSNV:
        //        case DisciplineTasks.PRNVPforCMP:
        //        case DisciplineTasks.PTWithINR:
        //        case DisciplineTasks.PTWithINRPRNSNV:
        //        case DisciplineTasks.SkilledNurseHomeInfusionSD:
        //        case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
        //        case DisciplineTasks.SNAssessment:
        //        case DisciplineTasks.SNDC:
        //        case DisciplineTasks.SNEvaluation:
        //        case DisciplineTasks.SNFoleyLabs:
        //        case DisciplineTasks.SNFoleyChange:
        //        case DisciplineTasks.SNInjection:
        //        case DisciplineTasks.SNInjectionLabs:
        //        case DisciplineTasks.SNLabsSN:
        //        case DisciplineTasks.SNVPsychNurse:
        //        case DisciplineTasks.SNVwithAideSupervision:
        //        case DisciplineTasks.SNVDCPlanning:
        //        case DisciplineTasks.SNVManagementAndEvaluation:
        //        case DisciplineTasks.SNVObservationAndAssessment:
        //        case DisciplineTasks.SNVTeachingTraining:
        //        case DisciplineTasks.SNPsychAssessment:
        //            var note = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id);
        //            if (note != null && note.Supply.IsNotNullOrEmpty())
        //            {
        //                supplies = note.Supply.ToObject<List<Supply>>();
        //            }
        //            break;
        //    }
        //    return supplies;
        //}

        public List<BillSchedule> BillableVisitSummary(List<T> visits, ClaimType claimType, List<ChargeRate> chargeRates, bool isLimitApplied)
        {
            var billVisits = new List<BillSchedule>();
            if (visits.IsNotNullOrEmpty())
            {
                var visitsDictionary = visits.GroupBy(v => v.VisitDate.Date).ToDictionary(v => v.Key, v => v.ToList());
                if (visitsDictionary.IsNotNullOrEmpty())
                {
                    visitsDictionary.ForEach((visitPerDayKey, visitPerDayValue) =>
                    {
                        var visitByDisciplneDictionary = visitPerDayValue.GroupBy(vd => vd.DisciplineTask).ToDictionary(vd => vd.Key, vd => vd.ToList());
                        if (visitByDisciplneDictionary.IsNotNullOrEmpty())
                        {
                            visitByDisciplneDictionary.ForEach((vddk, vddv) =>
                            {
                                var rate = chargeRates.FirstOrDefault(c => c.Id == vddk);
                                if (rate != null)
                                {
                                    var billSchedules = BillUnitTypeSchedules(rate, vddv, claimType, isLimitApplied);
                                    if (billSchedules.IsNotNullOrEmpty())
                                    {
                                        billVisits.AddRange(billSchedules);
                                    }
                                }
                            });
                        }
                    });
                }
            }
            return billVisits;
        }
 
        public List<Bill> AllUnProcessedBillList(Guid branchId, int insuranceId, ClaimTypeSubCategory claimType, string parentSortType, bool isUsersNeeded)
        {
            var listOfBill = new List<Bill>();
            var claims = GetUnProcessed(claimType, branchId, insuranceId, true, isUsersNeeded);
            if (claims != null && claims.Count > 0)
            {
                var locations = new List<AgencyLocation>();
                var locationsIds = claims.Select(b => b.AgencyLocationId).Distinct().ToList();
                if (locationsIds != null && locationsIds.Count > 0)
                {
                    locations = agencyRepository.AgencyLocations(Current.AgencyId, locationsIds);
                }
                var listOfInsurance = new List<InsuranceCache>();
                var allInsurances = claims.Where(b => b.PrimaryInsuranceId > 0).Select(b => b.PrimaryInsuranceId).Distinct().ToList();
                if (allInsurances != null && allInsurances.Count > 0)
                {
                    var nonTradMedicare = allInsurances.Where(i => i > 1000).ToArray();
                    if (nonTradMedicare != null && nonTradMedicare.Length > 0)
                    {
                        var insurances = agencyRepository.GetInsurancesForBilling(Current.AgencyId, nonTradMedicare);
                        if (insurances != null && insurances.Count > 0)
                        {
                            listOfInsurance.AddRange(insurances);
                        }
                    }
                    var medicare = allInsurances.Where(i => i < 1000 && i > 0).ToArray();
                    if (medicare != null && medicare.Length > 0)
                    {
                        medicare.ForEach(id =>
                        {
                            if (Enum.IsDefined(typeof(MedicareIntermediary), id))
                            {
                                listOfInsurance.Add(new InsuranceCache { Id = id,Name=((MedicareIntermediary)id).ToString(), PayorType = (int)PayerTypes.MedicareTraditional });
                            }
                        });
                    }
                }
                if (parentSortType.IsEqual("branch"))
                {
                    var claimsPerLocationDictionary = claims.GroupBy(c => c.AgencyLocationId).ToDictionary(c => c.Key, c => c.ToList());
                    if (claimsPerLocationDictionary != null && claimsPerLocationDictionary.Count > 0)
                    {
                        claimsPerLocationDictionary.Keys.ForEach(locationKey =>
                        {
                            var location = locations.FirstOrDefault(loc => loc.Id == locationKey);
                            var claimsPerInsuranceDictionary = claimsPerLocationDictionary[locationKey].GroupBy(ins => ins.PrimaryInsuranceId).ToDictionary(ii => ii.Key, ii => ii.ToList());
                            if (claimsPerInsuranceDictionary != null && claimsPerInsuranceDictionary.Count > 0)
                            {
                                claimsPerInsuranceDictionary.Keys.ForEach(insuranceKey =>
                                {
                                    var eachBill = new Bill();
                                    eachBill.Service = this.Service;
                                    eachBill.ClaimType = claimType;
                                    eachBill.Insurance = insuranceKey;
                                    eachBill.BranchId = locationKey;
                                    eachBill.BranchName = location != null ? location.Name : string.Empty;
                                    var insurance = listOfInsurance.FirstOrDefault(ins => ins.Id == insuranceKey);
                                    eachBill.InsuranceName = insurance != null ? insurance.Name : string.Empty;
                                    eachBill.Claims = claimsPerInsuranceDictionary[insuranceKey];
                                    listOfBill.Add(eachBill);
                                });
                            }
                        });
                    }
                }
                else if (parentSortType.IsEqual("insurance"))
                {
                    var claimsPerInsuranceDictionary = claims.GroupBy(c => c.PrimaryInsuranceId).ToDictionary(c => c.Key, c => c.ToList());
                    if (claimsPerInsuranceDictionary != null && claimsPerInsuranceDictionary.Count > 0)
                    {
                        claimsPerInsuranceDictionary.Keys.ForEach(insuranceKey =>
                        {
                            var insurance = listOfInsurance.FirstOrDefault(ins => ins.Id == insuranceKey);
                            var claimsPerLocationDictionary = claimsPerInsuranceDictionary[insuranceKey].GroupBy(ins => ins.AgencyLocationId).ToDictionary(ii => ii.Key, ii => ii.ToList());
                            if (claimsPerLocationDictionary != null && claimsPerLocationDictionary.Count > 0)
                            {
                                claimsPerLocationDictionary.Keys.ForEach(locationKey =>
                                {
                                    var eachBill = new Bill();
                                    eachBill.Service = this.Service;
                                    eachBill.ClaimType = claimType;
                                    eachBill.Insurance = insuranceKey;
                                    eachBill.InsuranceName = insurance != null ? insurance.Name : string.Empty;
                                    var location = locations.FirstOrDefault(loc => loc.Id == locationKey);
                                    eachBill.BranchId = locationKey;
                                    eachBill.BranchName = location != null ? location.Name : string.Empty;
                                    eachBill.Claims = claimsPerLocationDictionary[locationKey];
                                    listOfBill.Add(eachBill);
                                });
                            }
                        });
                    }
                }
            }
            return listOfBill;
        }

        public List<Claim> GetUnProcessed(ClaimTypeSubCategory claimType, Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded)
        {
            var unProcessedClaims = new List<Claim>();
            switch (claimType)
            {
                case ClaimTypeSubCategory.ManagedCare:
                    unProcessedClaims = AllUnProcessedManagedClaims(branchId, insuranceId, IsZeroInsuraceIdAll, isUsersNeeded);
                    break;
                default:
                    unProcessedClaims = GetUnProcessedAppSpecific(claimType, branchId, insuranceId, IsZeroInsuraceIdAll, isUsersNeeded);
                    break;
            }
            return unProcessedClaims;
        }

        public Bill GetClaimsWithInsuranceAndInsuranceInfo(ClaimTypeSubCategory claimType, Guid branchId, int insuranceId, ParentPermission permissionCategory)
        {
            var bill = new Bill { Claims = GetUnProcessed(claimType, branchId, insuranceId, false, false) };

            var allPermission = new Dictionary<int, AgencyServices>();
            var isEditPermission = false;
            var isPrintPermission = false;
            var isExportPermission = false;
            var isGeneratePermission = false;
            allPermission = Current.CategoryService(this.Service, permissionCategory, new int[] { (int)PermissionActions.Edit, (int)PermissionActions.Print, (int)PermissionActions.Export, (int)PermissionActions.GenerateElectronicClaim });
            if (allPermission.IsNotNullOrEmpty())
            {
                isEditPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Edit, AgencyServices.None) != AgencyServices.None;
                isPrintPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Print, AgencyServices.None) != AgencyServices.None;
                isExportPermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None) != AgencyServices.None;
                isGeneratePermission = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.GenerateElectronicClaim, AgencyServices.None) != AgencyServices.None;
            }

            bill.IsUserCanEdit = isEditPermission;
            bill.IsUserCanExport = isExportPermission;
            bill.IsUserCanPrint = isPrintPermission;
            bill.IsUserCanGenerate = isGeneratePermission;

            bill.Service = this.Service;
            bill.ClaimType = claimType;
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    bill.BranchId = location.Id;
                    bill.BranchName = location.Name;
                }
                if (insuranceId > 0)
                {
                    if (MedicareIntermediaryFactory.Intermediaries().Contains(insuranceId))
                    {
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = ((MedicareIntermediary)insuranceId).GetDescription();

                    }
                }
            }
            else
            {
                bill.BranchName = "All Branches";
            }
            if (insuranceId >= 1000)
            {
                var insurance = agencyRepository.FindInsurance(Current.AgencyId, insuranceId);
                bill.Insurance = insuranceId;
                bill.InsuranceName = insurance != null ? insurance.Name : string.Empty;
            }
            return bill;
        }

        public BillingJsonViewData GenerateDirectly(List<Guid> ClaimToGenerate, Guid branchId, int insuranceId, string claimType)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim(s) are not processed. Try again." };
            if (this.IsManagedClaim(claimType))
            {
                viewData = this.GenerateManagedDirectly(ClaimToGenerate, branchId, insuranceId);
            }
            else
            {
                viewData = this.GenerateDirectlyAppSpecific(ClaimToGenerate, branchId, insuranceId, claimType);
            }
            return viewData;
        }

        protected abstract BillingJsonViewData GenerateDirectlyAppSpecific(List<Guid> claimToGenerate, Guid branchId, int insuranceId, string claimType);

        public object GenerateDownload(List<Guid> claimToGenerate, Guid branchId, int insuranceId, string claimType)
        {
            if (this.IsManagedClaim(claimType))
            {
                return this.GenerateManagedDownload(claimToGenerate, branchId, insuranceId);
            }
            else
            {
                return this.GenerateDownloadAppSpecific(claimToGenerate, branchId, insuranceId, claimType);
            }
        }

        protected abstract object GenerateDownloadAppSpecific(List<Guid> claimToGenerate, Guid branchId, int insuranceId, string claimType);

        public object GenerateManagedClaimSingle(Guid Id)
        {
            var result = false;
            ClaimData claimData = null;
            BillExchange billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, Id);
            if (claim != null)
            {
                var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, claim.PatientId);
                if (profile != null)
                {
                    var ids = (new List<Guid>());
                    ids.Add(Id);
                    if (GenerateManaged(ids, ClaimCommandType.download, out claimData, out billExchange, profile.AgencyLocationId, claim.PrimaryInsuranceId))
                    {
                        result = true;
                    }
                }
            }
            if (result)
            {
                return new { isSuccessful = true, errorMessage = "The claim was successfully created", Id = claimData != null ? claimData.Id : -1 };
            }
            else
            {
                return new { isSuccessful = false, errorMessage = billExchange != null ? billExchange.Message : string.Empty, Id = -1 };
            }
        }

        //public string GenerateJsonForManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AgencyInsurance payerInfo, out List<ManagedClaim> managedClaims, AgencyLocation branch)
        //{
        //    string requestArr = string.Empty;
        //    claimInfo = new List<ClaimInfo>();
        //    managedClaims = null;
        //    try
        //    {
        //        if (branch != null)
        //        {
        //            var managedClaimLists = baseBillingRepository.GetManagedClaimsToGenerateByIds(Current.AgencyId, managedClaimToGenerate);
        //            managedClaims = managedClaimLists;

        //            if (managedClaimLists.IsNotNullOrEmpty())
        //            {

        //                var cliamInsurances = baseMongoRepository.GetManyClaimInsurances<ManagedClaimInsurance>(Current.AgencyId, managedClaimToGenerate);
        //                if (cliamInsurances.IsNotNullOrEmpty())
        //                {
        //                    var patients = new List<AnsiPatient>();
        //                    var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
        //                    foreach (var managedClaim in managedClaimLists)
        //                    {
        //                        var claims = new List<AnsiClaim>();
        //                        var visitTotalAmount = 0.00;
        //                        var visits = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<T>();
        //                        var visitList = new List<AnsiVisit>();
        //                        if (visits != null && visits.Count > 0)
        //                        {

        //                            //new this.ManagedToChargeRates(managedClaim, out agencyInsurance, false) ?? new List<ChargeRate>();
        //                            List<ChargeRate> chargeRates = null;
        //                            var claimInsurance = cliamInsurances.FirstOrDefault(i => i.Id == managedClaim.Id);
        //                            if (claimInsurance != null && claimInsurance.Insurance != null)
        //                            {
        //                                chargeRates = claimInsurance.Insurance.BillData.ToObject<List<ChargeRate>>();
        //                            }
        //                            else
        //                            {
        //                                var agencyInsurance = managedClaim.Insurance.ToObject<AgencyInsurance>();
        //                                if (agencyInsurance != null)
        //                                {
        //                                    chargeRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
        //                                }
        //                            }
        //                            chargeRates = chargeRates ?? new List<ChargeRate>();

        //                            var visitsDictionary = visits.GroupBy(v => v.VisitDate.Date).ToDictionary(v => v.Key, v => v.ToList());
        //                            if (visitsDictionary != null && visitsDictionary.Count > 0)
        //                            {
        //                                visitsDictionary.ForEach((visitPerDayKey, visitPerDayValue) =>
        //                                {
        //                                    var visitByDisciplneDictionary = visitPerDayValue.GroupBy(vd => vd.DisciplineTask).ToDictionary(vd => vd.Key, vd => vd.ToList());
        //                                    if (visitByDisciplneDictionary != null && visitByDisciplneDictionary.Count > 0)
        //                                    {
        //                                        visitByDisciplneDictionary.ForEach((vddk, vddv) =>
        //                                        {
        //                                            var rate = chargeRates.FirstOrDefault(c => c.Id == vddk);
        //                                            if (rate != null)
        //                                            {
        //                                                double amount;
        //                                                var addedVisits = BillableVisitForANSIWithAdjustments(rate, vddv, out amount, ClaimType.MAN, true, null);
        //                                                if (addedVisits != null && addedVisits.Count > 0)
        //                                                {
        //                                                    visitList.AddRange(addedVisits);
        //                                                    visitTotalAmount += amount;
        //                                                }
        //                                            }
        //                                        });
        //                                    }
        //                                });
        //                            }
        //                        }

        //                        var supplies = managedClaim.Supply.IsNotNullOrEmpty() ? managedClaim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
        //                        var supplyList = new List<object>();
        //                        var supplyTotalAmount = 0.00;
        //                        if (supplies != null && supplies.Count > 0)
        //                        {
        //                            supplies.ForEach(v =>
        //                            {
        //                                supplyList.Add(new { date = v.Date, revenue = v.RevenueCode.IsNotNullOrEmpty() ? v.RevenueCode : string.Empty, hcpcs = v.Code.IsNotNullOrEmpty() ? v.Code : string.Empty, units = v.Quantity, amount = v.UnitCost * v.Quantity, modifier = v.Modifier.IsNotNullOrEmpty() ? v.Modifier : string.Empty });
        //                                supplyTotalAmount += v.UnitCost * v.Quantity;
        //                            });
        //                        }

        //                        var diagnosis = managedClaim.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(managedClaim.DiagnosisCode) : null;
        //                        var conditionCodes = managedClaim.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(managedClaim.ConditionCodes) : null;

        //                        var locator31 = ConvertStringLocatorToList(managedClaim.Ub04Locator31);
        //                        var locator32 = ConvertStringLocatorToList(managedClaim.Ub04Locator32);
        //                        var locator33 = ConvertStringLocatorToList(managedClaim.Ub04Locator33);
        //                        var locator34 = ConvertStringLocatorToList(managedClaim.Ub04Locator34);
        //                        var locator39 = ConvertStringLocatorToList(managedClaim.Ub04Locator39);
        //                        var locator81 = ConvertStringLocatorToList(managedClaim.Ub04Locator81cca);
        //                        var hcfaLocators = managedClaim.HCFALocators.ToLocatorDictionary();

        //                        claimInfo.Add(new ClaimInfo { ClaimId = managedClaim.Id, PatientId = managedClaim.PatientId, EpisodeId = managedClaim.EpisodeId, ClaimType = managedClaim.Type });
        //                        var manObj = new
        //                        {
        //                            claim_id = managedClaim.Id,
        //                            claim_type = managedClaim.Type,
        //                            claim_physician_upin = managedClaim.PhysicianNPI,
        //                            claim_physician_last_name = managedClaim.PhysicianLastName,
        //                            claim_physician_first_name = managedClaim.PhysicianFirstName,
        //                            claim_first_visit_date = managedClaim.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
        //                            claim_episode_start_date = managedClaim.EpisodeStartDate.ToString("MM/dd/yyyy"),
        //                            claim_episode_end_date = managedClaim.EpisodeEndDate.ToString("MM/dd/yyyy"),
        //                            claim_hipps_code = managedClaim.HippsCode,
        //                            claim_oasis_key = managedClaim.ClaimKey,
        //                            hmo_plan_id = managedClaim.HealthPlanId,
        //                            claim_group_name = managedClaim.GroupName,
        //                            claim_group_Id = managedClaim.GroupId,
        //                            claim_hmo_auth_key = managedClaim.AuthorizationNumber,
        //                            claim_hmo_auth_key2 = managedClaim.AuthorizationNumber2,
        //                            claim_hmo_auth_key3 = managedClaim.AuthorizationNumber3,
        //                            claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
        //                            claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
        //                            claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
        //                            claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
        //                            claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
        //                            claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
        //                            claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
        //                            claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
        //                            claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
        //                            claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
        //                            claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
        //                            claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
        //                            claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
        //                            claim_admission_source_code = managedClaim.AdmissionSource.IsNotNullOrEmpty() && managedClaim.AdmissionSource.IsInteger() ? managedClaim.AdmissionSource.ToInteger().GetSplitValue() : "9",
        //                            claim_patient_status_code = managedClaim.UB4PatientStatus,
        //                            claim_dob = ubo4DischargeStatus.Contains(managedClaim.UB4PatientStatus) ? managedClaim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            claim_ub04locator81 = locator81,
        //                            claim_ub04locator39 = locator39,
        //                            claim_ub04locator31 = locator31,
        //                            claim_ub04locator32 = locator32,
        //                            claim_ub04locator33 = locator33,
        //                            claim_ub04locator34 = locator34,
        //                            claim_hcfalocator33 = hcfaLocators.ContainsKey("33Locatorb") && hcfaLocators["33Locatorb"] != null ? ConvertLocatorToList(hcfaLocators["33Locatorb"]) : new List<object>(),
        //                            claim_supply_isBillable = true,
        //                            claim_supply_value = Math.Round(supplyTotalAmount, 2),
        //                            claim_supplies = supplyList,
        //                            claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
        //                            claim_visits = visitList
        //                        };
        //                        claims.Add(manObj);
        //                        var patient = new
        //                        {
        //                            patient_gender = managedClaim.Gender.Substring(0, 1),
        //                            patient_record_num = managedClaim.PatientIdNumber,
        //                            patient_dob = managedClaim.DOB.ToString("MM/dd/yyyy"),
        //                            patient_doa = managedClaim.StartofCareDate.ToString("MM/dd/yyyy"),
        //                            patient_dod = ubo4DischargeStatus.Contains(managedClaim.UB4PatientStatus) && managedClaim.DischargeDate.Date > DateTime.MinValue.Date ? managedClaim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
        //                            patient_address = managedClaim.AddressLine1,
        //                            patient_address2 = managedClaim.AddressLine2,
        //                            patient_city = managedClaim.AddressCity,
        //                            patient_state = managedClaim.AddressStateCode,
        //                            patient_zip = managedClaim.AddressZipCode,
        //                            patient_cbsa = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode),
        //                            patient_last_name = managedClaim.LastName,
        //                            patient_first_name = managedClaim.FirstName,
        //                            patient_middle_initial = "",
        //                            claims_arr = claims
        //                        };
        //                        patients.Add(patient);
        //                    }
        //                    var agencyClaim = new
        //                    {
        //                        format = "ansi837",
        //                        submit_type = commandType.ToString(),
        //                        user_login_name = Current.User.Name,
        //                        hmo_payer_id = payerInfo.PayorId,
        //                        hmo_payer_name = payerInfo.Name,
        //                        hmo_submitter_id = payerInfo.SubmitterId,
        //                        hmo_provider_id = payerInfo.ProviderId,
        //                        hmo_other_provider_id = payerInfo.OtherProviderId,
        //                        hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
        //                        // hmo_additional_codes = additionaCodes,
        //                        // payer_id = payerInfo.Code,
        //                        payer_name = payerInfo.Name,
        //                        insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
        //                        clearing_house_id = payerInfo.ClearingHouseSubmitterId,
        //                        provider_claim_type = payerInfo.BillType,
        //                        interchange_receiver_id = payerInfo.InterchangeReceiverId,
        //                        clearing_house = payerInfo.ClearingHouse,
        //                        claim_billtype = ClaimType.MAN.ToString(),
        //                        submitter_name = payerInfo.SubmitterName,
        //                        submitter_phone = payerInfo.SubmitterPhone,
        //                        // submitter_fax = payerInfo.Fax,
        //                        user_agency_name = branch.Name,
        //                        user_tax_id = branch.TaxId,
        //                        user_national_provider_id = branch.NationalProviderNumber,
        //                        user_address_1 = branch.AddressLine1,
        //                        user_address_2 = branch.AddressLine2,
        //                        user_city = branch.AddressCity,
        //                        user_state = branch.AddressStateCode,
        //                        user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
        //                        user_phone = branch.PhoneWork,
        //                        user_fax = branch.FaxNumber,
        //                        user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
        //                        ansi_837_id = claimId,
        //                        patients_arr = patients
        //                    };
        //                    var jss = new JavaScriptSerializer();
        //                    requestArr = jss.Serialize(agencyClaim);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return string.Empty;
        //    }
        //    return requestArr;
        //}

        public string GenerateJsonForManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AgencyInsurance payerInfo, out List<ManagedClaim> managedClaims, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            managedClaims = null;
            try
            {
                if (branch != null)
                {
                    var managedClaimLists = baseBillingRepository.GetManagedClaimsToGenerateByIds(Current.AgencyId, managedClaimToGenerate);
                    if (managedClaimLists.IsNotNullOrEmpty())
                    {
                        managedClaims = managedClaimLists;
                        var cliamInsurances = baseMongoRepository.GetManyClaimInsurances<ManagedClaimInsurance>(Current.AgencyId, managedClaimToGenerate);
                        if (cliamInsurances.IsNotNullOrEmpty())
                        {
                            var patients = new List<AnsiPatient>();
                            var ubo4DischargeStatus = UB4PatientStatusFactory.Discharge();
                            foreach (var managedClaim in managedClaimLists)
                            {
                                var claims = new List<AnsiClaim>();
                                claimInfo.Add(new ClaimInfo { ClaimId = managedClaim.Id, PatientId = managedClaim.PatientId, EpisodeId = managedClaim.EpisodeId, ClaimType = managedClaim.Type });

                                var visits = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<T>();
                                var isVisitExist = visits.IsNotNullOrEmpty();
                                var visitTotalAmount = 0.00;
                                var visitList = new List<AnsiVisit>();
                                var supplyTotalAmount = 0.0;
                                var supplyList = new List<AnsiSupply>();
                                if (isVisitExist)
                                {
                                    var chargeRates = GetChargeRatesFromInsurance(cliamInsurances, managedClaim, isVisitExist);
                                    visitList = Visits(visits, chargeRates, out visitTotalAmount, ClaimType.MAN, null);

                                    var supplies = managedClaim.Supply.IsNotNullOrEmpty() ? managedClaim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
                                    supplyList = Supplies(supplies, out supplyTotalAmount);
                                }
                                var manObj = ClaimsWithVisitsAndSupplies(managedClaim, visitList, visitTotalAmount, supplyList, supplyTotalAmount, false, ubo4DischargeStatus);
                                manObj.claim_type = managedClaim.Type;
                                manObj.claim_supply_isBillable = true;
                                
                                claims.Add(manObj);

                                patients.Add(PatientWithClaims(managedClaim, ubo4DischargeStatus, claims));
                            }
                            var agencyClaim = AgencyWithPatients(commandType, ClaimType.MAN, claimId, payerInfo, branch, patients);
                            var jss = new JavaScriptSerializer();
                            requestArr = jss.Serialize(agencyClaim);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return requestArr;
        }

        protected object GenerateManagedDownload(List<Guid> managedClaimToGenerate, Guid branchId, int insuranceId)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            GenerateManaged(managedClaimToGenerate, ClaimCommandType.download, out claimDataOut, out billExchage, branchId, insuranceId);
            return new { isSuccessful = claimDataOut != null, isDownload = true, errorMessage = billExchage != null ? billExchage.Message : "Error in processing of the claim(s).", Id = claimDataOut != null ? claimDataOut.Id : -1 };
        }

        protected BillingJsonViewData GenerateManagedDirectly(List<Guid> managedClaimToGenerate, Guid branchId, int insuranceId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim(s) are not processed. Try again." };
            viewData.Service = this.Service.ToString();
            viewData.Category = ClaimTypeSubCategory.ManagedCare;
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (GenerateManaged(managedClaimToGenerate, ClaimCommandType.direct, out claimDataOut, out billExchage, branchId, insuranceId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) are processed successfully.";
                viewData.IsManagedCareHistoryRefresh = true;
                viewData.IsManagedCareRefresh=true;
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return viewData;
        }

        public bool GenerateManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            bool result = false;
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (agencyLocation != null)
                {
                    if (!agencyLocation.IsLocationStandAlone)
                    {
                        //agencyLocation.Payor = agency.Payor;
                        //agencyLocation.SubmitterId = agency.SubmitterId;
                        //agencyLocation.SubmitterName = agency.SubmitterName;
                        //agencyLocation.SubmitterPhone = agency.SubmitterPhone;
                        //agencyLocation.SubmitterFax = agency.SubmitterFax;
                        agencyLocation.Name = agency.Name;
                        agencyLocation.TaxId = agency.TaxId;
                        agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;

                    }
                    if (insuranceId >= 1000)
                    {
                        var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                        if (insurance != null)
                        {
                            claimData.ClaimType = ClaimType.MAN.ToString();
                            var claimId = GetNextClaimId(claimData);
                            claimData.Id = claimId;
                            List<ManagedClaim> managedClaims = null;

                            var requestArr = GenerateJsonForManaged(managedClaimToGenerate, commandType, claimId, out claimInfo, insurance, out managedClaims, agencyLocation);
                            if (requestArr.IsNotNullOrEmpty())
                            {
                                requestArr = requestArr.Replace("&", "U+0026");
                                billExchange = GenerateANSI(requestArr);
                                if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                                {
                                    if (billExchange.Result.IsNotNullOrEmpty())
                                    {
                                        claimData.Data = billExchange.Result;
                                        claimData.ClaimType = ClaimType.MAN.ToString();
                                        claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                                        baseBillingRepository.UpdateClaimData(claimData);
                                        if (commandType == ClaimCommandType.direct)
                                        {
                                            if (managedClaims.IsNotNullOrEmpty())
                                            {
                                                baseBillingRepository.MarkManagedClaimsAsSubmitted(Current.AgencyId, managedClaims);
                                                managedClaims.ForEach(claim =>
                                                {
                                                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSubmittedElectronically, string.Empty);
                                                });
                                            }
                                        }
                                        else if (commandType == ClaimCommandType.download)
                                        {
                                            if (managedClaims.IsNotNullOrEmpty())
                                            {
                                                baseBillingRepository.MarkManagedClaimsAsGenerated(Current.AgencyId, managedClaims);
                                                managedClaims.ForEach(claim =>
                                                {
                                                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedGenerated, string.Empty);
                                                });
                                            }
                                        }
                                        claimDataOut = claimData;
                                        result = true;
                                    }
                                    else
                                    {
                                        baseBillingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                                        claimDataOut = null;
                                        result = false;
                                    }
                                }
                                else
                                {
                                    baseBillingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                                    result = false;
                                }
                            }
                            else
                            {
                                baseBillingRepository.RemoveClaimData(Current.AgencyId, claimData.Id);
                                billExchange.Message = "A system problem has occured. Try again.";
                                result = false;
                            }

                        }
                        else
                        {
                            billExchange.Message = "Insurance/Payer information is not right.";
                            return false;
                        }
                    }
                    else
                    {
                        billExchange.Message = "Insurance/Payer information is not right.";
                    }
                }
                else
                {
                    billExchange.Message = "The Branch information could not be found. Try again.";
                }
            }

            else
            {
                billExchange.Message = "Claim Information is not correct. Try again.";
            }
            return result;
        }

        public BillExchange GenerateANSI(string jsonData)
        {
            var billExchange = new BillExchange();
            try
            {
                var encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonData);
                byte[] data = encoding.GetBytes(postData);
                var request = (HttpWebRequest)WebRequest.Create(UrlSettings.ANSIGeneratorUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                var newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                var response = (HttpWebResponse)request.GetResponse();
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    var receiveStream = response.GetResponseStream();
                    var encode = System.Text.Encoding.GetEncoding("utf-8");
                    var readStream = new StreamReader(receiveStream, encode);
                    var strResult = readStream.ReadToEnd();
                    var jss = new JavaScriptSerializer();
                    billExchange = jss.Deserialize<BillExchange>(strResult);
                    if (billExchange.Status == "OK")
                    {
                        billExchange.isSuccessful = true;
                    }
                    else
                    {
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billExchange.Message = "The is a problem processing these claim. Try Again.";
                    billExchange.isSuccessful = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                billExchange.Message = "There is system problem. Try Again.";
                billExchange.isSuccessful = false;
                return billExchange;
            }
            return billExchange;
        }

        #endregion

        #region ANSI Helper

        protected List<AnsiVisit> BillableVisitForANSIWithAdjustments(ChargeRate rate, List<T> visits, out double totalCharge, ClaimType claimType, bool isLimitApplied, List<ServiceAdjustmentWrapper> claimAdjustments)
        {
            totalCharge = 0;
            var schedules = new List<AnsiVisit>();
            if (visits != null && visits.Count > 0 && rate != null)
            {
                if (rate.IsUnitsPerDayOnSingleLineItem)
                {
                    var oneDaySchedule = GetBillSchedule(rate, visits, claimType, isLimitApplied);
                    if (oneDaySchedule != null)
                    {
                        var discipline = visits[0].GIdentify();
                        var visitAdjustments = VisitAdjustments(visits.Select(v => v.Id).ToList(), claimAdjustments);
                        schedules.Add(GenerateBillScheduleObject(oneDaySchedule, discipline, rate, visitAdjustments));
                    }
                }
                else
                {
                    foreach (var visit in visits)
                    {
                        int unitOut;
                        double chargeOut;
                        var discipline = visit.GIdentify();
                        if (rate.ChargeType.IsNotNullOrEmpty() && rate.ChargeType.IsInteger())
                        {
                            switch (rate.ChargeType.ToInteger())
                            {
                                case (int)BillUnitType.PerVisit:
                                    {
                                        CaluculateChargeAndUnit(rate, new List<T> { visit }, claimType, out unitOut, out chargeOut);
                                        var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                                        schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unitOut, chargeOut, visitAdjustments));
                                        totalCharge += chargeOut;
                                    }
                                    break;
                                case (int)BillUnitType.Hourly:
                                    {
                                        chargeOut = GenerateHourlyBillObject(rate, visit, claimType, isLimitApplied, ref schedules, discipline, claimAdjustments);
                                        // chargeOut -= CalculateAdjAmount(visit.Id, claimAdjustments);
                                        totalCharge += chargeOut;
                                    }
                                    break;

                                case (int)BillUnitType.Per15Min:
                                    chargeOut = GeneratePer15MinBillObject(rate, visit, claimType, isLimitApplied, ref schedules, discipline, claimAdjustments);
                                    // chargeOut -= CalculateAdjAmount(visit.Id, claimAdjustments);
                                    totalCharge += chargeOut;
                                    break;
                            }
                        }
                    }
                }
            }
            return schedules;
        }

        protected double GeneratePer15MinBillObject(ChargeRate rate, T visit, ClaimType claimType, bool isLimitApplied, ref List<AnsiVisit> schedules, string discipline, List<ServiceAdjustmentWrapper> claimAdjustments)
        {
            double totalCharge = 0;
            if (claimType == ClaimType.CMS)
            {
                int units = (int)Math.Ceiling((double)visit.MinSpent / 15);
                var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, units, rate.Charge, visitAdjustments));
                totalCharge += rate.Charge;
            }
            else if (claimType == ClaimType.HMO)
            {
                var unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                var rateHMO = rate.MedicareHMORate > 0 ? rate.MedicareHMORate : unit * rate.Charge;
                var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, rateHMO, visitAdjustments));
                totalCharge += rateHMO;
            }
            else
            {
                if (isLimitApplied)
                {
                    if (rate.IsTimeLimit)
                    {
                        var totalMinLimit = rate.TimeLimitHour * 60 + rate.TimeLimitMin;
                        var totalTimeEventTake = visit.MinSpent;
                        if (totalTimeEventTake <= totalMinLimit)
                        {
                            var unit = (int)Math.Ceiling((double)totalTimeEventTake / 15);
                            var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                            schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                            totalCharge += unit * rate.Charge;

                        }
                        else if (totalTimeEventTake > totalMinLimit)
                        {
                            var unit = (int)Math.Ceiling((double)totalMinLimit / 15);
                            var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                            schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                            totalCharge += unit * rate.Charge;

                            if (rate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                            {
                                schedules.Add(GenerateSecondBillScheduleObject(visit, discipline, rate,
                                    rate.SecondUnit, rate.IsSecondChargeDifferent ? rate.SecondCharge : 0));
                                totalCharge += rate.IsSecondChargeDifferent ? rate.SecondCharge : 0;
                            }
                            else
                            {
                                var minute = rate.SecondChargeType == ((int)BillUnitType.Hourly).ToString() ? 60 : 15;
                                var unitAdded = (int)Math.Ceiling((double)(totalTimeEventTake - totalMinLimit) / minute);
                                var charge = rate.IsSecondChargeDifferent ? rate.SecondCharge : (rate.SecondChargeType == ((int)BillUnitType.Hourly).ToString() ? 0 : rate.Charge);
                                if (rate.IsUnitPerALineItem)
                                {
                                    for (int i = 0; i < unitAdded; i++)
                                    {
                                        schedules.Add(GenerateSecondBillScheduleObject(visit, discipline, rate, 1, charge));
                                        totalCharge += charge;
                                    }
                                }
                                else
                                {
                                    schedules.Add(GenerateSecondBillScheduleObject(visit, discipline, rate, unitAdded, unitAdded * charge));
                                    totalCharge += unitAdded * charge;
                                }
                            }
                        }
                    }
                    else
                    {
                        var unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                        var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                        schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                        totalCharge += unit * rate.Charge;
                    }
                }
                else
                {
                    var unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                    var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                    schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                    totalCharge += unit * rate.Charge;
                }
            }
            return totalCharge;
        }

        protected double GenerateHourlyBillObject(ChargeRate rate, T visit, ClaimType claimType, bool isLimitApplied, ref List<AnsiVisit> schedules, string discipline, List<ServiceAdjustmentWrapper> claimAdjustments)
        {
            double totalCharge = 0;
            if (claimType == ClaimType.CMS)
            {
                int units = (int)Math.Ceiling((double)visit.MinSpent / 15);
                var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, units, rate.Charge, visitAdjustments));
                totalCharge += rate.Charge;
            }
            else if (claimType == ClaimType.HMO)
            {
                var perUnitMin = rate.MedicareHMORate > 0 ? 15 : 60;
                var unit = (int)Math.Ceiling((double)visit.MinSpent / perUnitMin);
                var rateHMO = rate.MedicareHMORate > 0 ? rate.MedicareHMORate : unit * rate.Charge;
                var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, rateHMO, visitAdjustments));
                totalCharge += rateHMO;
            }
            else
            {
                if (isLimitApplied)
                {
                    if (rate.IsTimeLimit)
                    {
                        var totalMinLimit = rate.TimeLimitHour * 60 + rate.TimeLimitMin;
                        var totalTimeEventTake = visit.MinSpent;
                        if (totalTimeEventTake <= totalMinLimit)
                        {
                            var unit = (int)Math.Ceiling((double)totalTimeEventTake / 60);
                            var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                            schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                            totalCharge += unit * rate.Charge;
                        }
                        else if (totalTimeEventTake > totalMinLimit)
                        {
                            var unit = (int)Math.Ceiling((double)totalMinLimit / 60);
                            var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                            schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                            totalCharge += unit * rate.Charge;

                            if (rate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                            {
                                schedules.Add(GenerateSecondBillScheduleObject(visit, discipline, rate,
                                    rate.SecondUnit,
                                    rate.IsSecondChargeDifferent ? rate.SecondCharge : 0));
                                totalCharge += rate.IsSecondChargeDifferent ? rate.SecondCharge : 0;
                            }
                            else
                            {
                                var minute = rate.SecondChargeType == ((int)BillUnitType.Per15Min).ToString() ? 15 : 60;
                                var unitAdded = (int)Math.Ceiling((double)(totalTimeEventTake - totalMinLimit) / minute);
                                var charge = rate.IsSecondChargeDifferent ? rate.SecondCharge : (rate.SecondChargeType == ((int)BillUnitType.Per15Min).ToString() ? 0 : rate.Charge);
                                if (rate.IsUnitPerALineItem)
                                {
                                    for (int i = 0; i < unitAdded; i++)
                                    {
                                        schedules.Add(GenerateSecondBillScheduleObject(visit, discipline, rate, 1, charge));
                                        totalCharge += charge;
                                    }
                                }
                                else
                                {
                                    schedules.Add(GenerateSecondBillScheduleObject(visit, discipline, rate, unitAdded, unitAdded * charge));
                                    totalCharge += unitAdded * charge;
                                }
                            }
                        }
                    }
                    else
                    {
                        var unit = (int)Math.Ceiling((double)visit.MinSpent / 60);
                        var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                        schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                        totalCharge += unit * rate.Charge;
                    }
                }
                else
                {
                    var unit = (int)Math.Ceiling((double)visit.MinSpent / 60);
                    var visitAdjustments = VisitAdjustments(visit.Id, claimAdjustments);
                    schedules.Add(GenerateBillScheduleObject(visit, discipline, rate, unit, unit * rate.Charge, visitAdjustments));
                    totalCharge += unit * rate.Charge;
                }
            }
            return totalCharge;
        }

        protected AnsiVisit GenerateBillScheduleObject(T visit, string discipline, ChargeRate rate, int units, double amount, List<AnsiAdjustment> adjustments)
        {
            return new AnsiVisit
            {
                date = visit.VisitDate,
                type = discipline,
                revenue = rate.RevenueCode,
                hcpcs = rate.Code,
                units = units,
                amount = amount,
                modifier1 = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty,
                modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty,
                modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty,
                modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty,
                adjustments = adjustments
            };
        }

        protected AnsiVisit GenerateBillScheduleObject(BillSchedule visit, string discipline, ChargeRate rate, List<AnsiAdjustment> adjustments)
        {
            return new AnsiVisit
            {
                date = visit.VisitDate,
                type = discipline,
                revenue = rate.RevenueCode,
                hcpcs = rate.Code,
                units = visit.Unit,
                amount = visit.Charge,
                modifier1 = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty,
                modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty,
                modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty,
                modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty,
                adjustments = adjustments
            };
        }

        private AnsiVisit GenerateSecondBillScheduleObject(T visit, string discipline, ChargeRate rate, int units, double amount)
        {
            return new AnsiVisit
            {
                date = visit.VisitDate,
                type = discipline,
                revenue = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                hcpcs = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                units = units,
                amount = amount,
                modifier1 = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty
            };
        }

        protected List<AnsiAdjustment> VisitAdjustments(Guid visitId, List<ServiceAdjustmentWrapper> adjustments)
        {
            var listOfAdjustment = new List<AnsiAdjustment>();
            if (adjustments.IsNotNullOrEmpty())
            {
                var adjustment = adjustments.Where(f => f.EventId == visitId);
                if (adjustment.IsNotNullOrEmpty())
                {
                    adjustment.ForEach(a =>
                    {
                        if (a.AdjData != null)
                        {
                            listOfAdjustment.Add(new AnsiAdjustment { Group = a.AdjGroup, Reason = a.AdjData.AdjReason, Amount = a.AdjData.AdjAmount, Quantity = a.AdjData.AdjQuantity });
                        }

                    });
                }

            }
            return listOfAdjustment;
        }

        protected List<AnsiAdjustment> VisitAdjustments(List<Guid> visitIds, List<ServiceAdjustmentWrapper> adjustments)
        {
            var listOfAdjustment = new List<AnsiAdjustment>();
            if (adjustments.IsNotNullOrEmpty())
            {
                var adjustment = adjustments.Where(f => visitIds.Contains(f.EventId));
                if (adjustment.IsNotNullOrEmpty())
                {
                    adjustment.ForEach(a =>
                    {
                        if (a.AdjData != null)
                        {
                            listOfAdjustment.Add(new AnsiAdjustment { Group = a.AdjGroup, Reason = a.AdjData.AdjReason, Amount = a.AdjData.AdjAmount, Quantity = a.AdjData.AdjQuantity });
                        }
                    });
                }
            }
            return listOfAdjustment;
        }

        public AnsiClaim ClaimsWithVisitsAndSupplies(BaseClaim claim, List<AnsiVisit> visits, double visitTotalAmount, List<AnsiSupply> supplies, double supplyTotalAmount, bool IsSecondaryClaim, List<string> ubo4DischargeStatus)
        {

            var locator39 = ConvertStringLocatorToList(claim.Ub04Locator39);
            var locator81 = ConvertStringLocatorToList(claim.Ub04Locator81cca);
            var diagnosis = claim.DiagnosisCode.IsNotNullOrEmpty() ? claim.DiagnosisCode.ToObject<DiagnosisCodes>() : null;
            var conditionCodes = claim.ConditionCodes.IsNotNullOrEmpty() ? claim.ConditionCodes.ToObject<ConditionCodes>() : null;
            var ansiClaim = new
            AnsiClaim
            {
                claim_is_secondary_claim = IsSecondaryClaim,
                claim_id = claim.Id,
                //claim_type = claim.Type,
                claim_physician_upin = claim.PhysicianNPI,
                claim_physician_last_name = claim.PhysicianLastName,
                claim_physician_first_name = claim.PhysicianFirstName,
                claim_first_visit_date = claim.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                claim_episode_start_date = claim.EpisodeStartDate.ToString("MM/dd/yyyy"),
                claim_episode_end_date = claim.EpisodeEndDate.ToString("MM/dd/yyyy"),
                claim_hipps_code = claim.HippsCode,
                claim_oasis_key = claim.ClaimKey,
                hmo_plan_id = claim.HealthPlanId,
                claim_group_name = claim.GroupName,
                claim_group_Id = claim.GroupId,
                claim_hmo_auth_key = claim.AuthorizationNumber,
                claim_hmo_auth_key2 = claim.AuthorizationNumber2,
                claim_hmo_auth_key3 = claim.AuthorizationNumber3,
                claim_diagnosis_code1 = (diagnosis != null && diagnosis.Primary.IsNotNullOrEmpty() ? diagnosis.Primary.Replace(".", string.Empty) : string.Empty),
                claim_diagnosis_code2 = (diagnosis != null && diagnosis.Second.IsNotNullOrEmpty() ? diagnosis.Second.Replace(".", string.Empty) : string.Empty),
                claim_diagnosis_code3 = (diagnosis != null && diagnosis.Third.IsNotNullOrEmpty() ? diagnosis.Third.Replace(".", string.Empty) : string.Empty),
                claim_diagnosis_code4 = (diagnosis != null && diagnosis.Fourth.IsNotNullOrEmpty() ? diagnosis.Fourth.Replace(".", string.Empty) : string.Empty),
                claim_diagnosis_code5 = (diagnosis != null && diagnosis.Fifth.IsNotNullOrEmpty() ? diagnosis.Fifth.Replace(".", string.Empty) : string.Empty),
                claim_diagnosis_code6 = (diagnosis != null && diagnosis.Sixth.IsNotNullOrEmpty() ? diagnosis.Sixth.Replace(".", string.Empty) : string.Empty),
                claim_condition_code18 = (conditionCodes != null && conditionCodes.ConditionCode18.IsNotNullOrEmpty() ? conditionCodes.ConditionCode18 : string.Empty),
                claim_condition_code19 = (conditionCodes != null && conditionCodes.ConditionCode19.IsNotNullOrEmpty() ? conditionCodes.ConditionCode19 : string.Empty),
                claim_condition_code20 = (conditionCodes != null && conditionCodes.ConditionCode20.IsNotNullOrEmpty() ? conditionCodes.ConditionCode20 : string.Empty),
                claim_condition_code21 = (conditionCodes != null && conditionCodes.ConditionCode21.IsNotNullOrEmpty() ? conditionCodes.ConditionCode21 : string.Empty),
                claim_condition_code22 = (conditionCodes != null && conditionCodes.ConditionCode22.IsNotNullOrEmpty() ? conditionCodes.ConditionCode22 : string.Empty),
                claim_condition_code23 = (conditionCodes != null && conditionCodes.ConditionCode23.IsNotNullOrEmpty() ? conditionCodes.ConditionCode23 : string.Empty),
                claim_condition_code24 = (conditionCodes != null && conditionCodes.ConditionCode24.IsNotNullOrEmpty() ? conditionCodes.ConditionCode24 : string.Empty),
                claim_condition_code25 = (conditionCodes != null && conditionCodes.ConditionCode25.IsNotNullOrEmpty() ? conditionCodes.ConditionCode25 : string.Empty),
                claim_condition_code26 = (conditionCodes != null && conditionCodes.ConditionCode26.IsNotNullOrEmpty() ? conditionCodes.ConditionCode26 : string.Empty),
                claim_condition_code27 = (conditionCodes != null && conditionCodes.ConditionCode27.IsNotNullOrEmpty() ? conditionCodes.ConditionCode27 : string.Empty),
                claim_condition_code28 = (conditionCodes != null && conditionCodes.ConditionCode28.IsNotNullOrEmpty() ? conditionCodes.ConditionCode28 : string.Empty),
                claim_admission_source_code = claim.AdmissionSource.IsNotNullOrEmpty() && claim.AdmissionSource.IsInteger() ? claim.AdmissionSource.ToInteger().GetSplitValue() : "9",
                claim_ub04locator81 = locator81,
                claim_ub04locator39 = locator39,
                claim_patient_status_code = claim.UB4PatientStatus
            };


            if (claim.ClaimSubCategory == ClaimTypeSubCategory.RAP)
            {
                ansiClaim.claim_episode_end_date = claim.EpisodeStartDate.ToString("MM/dd/yyyy");
            }
            else
            {
                ansiClaim.claim_dob = ubo4DischargeStatus.Contains(claim.UB4PatientStatus) ? claim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty;
                ansiClaim.claim_supply_isBillable = true;
                ansiClaim.claim_supply_value = Math.Round(supplyTotalAmount, 2);
                ansiClaim.claim_supplies = supplies;
                ansiClaim.claim_total_charge_amount = Math.Round(visitTotalAmount, 2);
                ansiClaim.claim_visits = visits;

                var locator31 = ConvertStringLocatorToList(claim.Ub04Locator31);
                var locator32 = ConvertStringLocatorToList(claim.Ub04Locator32);
                var locator33 = ConvertStringLocatorToList(claim.Ub04Locator33);
                var locator34 = ConvertStringLocatorToList(claim.Ub04Locator34);
                ansiClaim.claim_ub04locator31 = locator31;
                ansiClaim.claim_ub04locator32 = locator32;
                ansiClaim.claim_ub04locator33 = locator33;
                ansiClaim.claim_ub04locator34 = locator34;

                if (claim.ClaimSubCategory == ClaimTypeSubCategory.Final)
                {
                }
                else
                {
                    var hcfaLocators = claim.HCFALocators.ToLocatorDictionary();
                    ansiClaim.claim_hcfalocator33 = hcfaLocators.ContainsKey("33Locatorb") && hcfaLocators["33Locatorb"] != null ? ConvertLocatorToList(hcfaLocators["33Locatorb"]) : new List<object>();
                }
            }
            return ansiClaim;
        }

        /// <summary>
        /// This is used for secodary claim that has remittance 
        /// </summary>
        /// <param name="claim"></param>
        /// <param name="visits"></param>
        /// <param name="visitTotalAmount"></param>
        /// <param name="supplies"></param>
        /// <param name="supplyTotalAmount"></param>
        /// <param name="IsSecondaryClaim"></param>
        /// <param name="ubo4DischargeStatus"></param>
        /// <returns></returns>
        public AnsiClaim ClaimsWithVisitsAndSuppliesForSecondary(SecondaryClaim claim, List<AnsiVisit> visits, double visitTotalAmount, List<AnsiSupply> supplies, double supplyTotalAmount, List<string> ubo4DischargeStatus)
        {
            var ansiClaim = ClaimsWithVisitsAndSupplies(claim, visits, visitTotalAmount, supplies, supplyTotalAmount, true, ubo4DischargeStatus);
            ansiClaim.claim_remit_date = claim.RemitDate.ToZeroFilled();
            ansiClaim.claim_remit_id = claim.RemitId;
            ansiClaim.claim_total_adjustment_amount = claim.TotalAdjustmentAmount;
            return ansiClaim;
        }
        
        public AnsiPatient PatientWithClaims(BaseClaim claim, List<string> ubo4DischargeStatus, List<AnsiClaim> claims)
        {

            var patient = new
           AnsiPatient
           {
               patient_gender = claim.Gender.Substring(0, 1),
               patient_record_num = claim.PatientIdNumber,
               patient_dob = claim.DOB.ToString("MM/dd/yyyy"),
               patient_doa = claim.StartofCareDate.ToString("MM/dd/yyyy"),
               patient_dod = ubo4DischargeStatus.Contains(claim.UB4PatientStatus) && claim.DischargeDate.Date > DateTime.MinValue.Date ? claim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
               patient_address = claim.AddressLine1,
               patient_address2 = claim.AddressLine2,
               patient_city = claim.AddressCity,
               patient_state = claim.AddressStateCode,
               patient_zip = claim.AddressZipCode,
               patient_cbsa = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode),
               patient_last_name = claim.LastName,
               patient_first_name = claim.FirstName,
               patient_middle_initial = "",
               claims_arr = claims
           };

            return patient;
        }

        public AnsiAgency AgencyWithPatients(ClaimCommandType commandType, ClaimType claimType, long claimId, AgencyInsurance payerInfo, AgencyLocation branch, List<AnsiPatient> patients)
        {
            var agencyWithClaims = CommonAgencyWithPatients(commandType, claimType, claimId, branch, patients);
            //HMO info
            agencyWithClaims.hmo_payer_id = payerInfo.PayorId;
            agencyWithClaims.hmo_payer_name = payerInfo.Name;
            agencyWithClaims.hmo_submitter_id = payerInfo.SubmitterId;
            agencyWithClaims.hmo_provider_id = payerInfo.ProviderId;
            agencyWithClaims.hmo_other_provider_id = payerInfo.OtherProviderId;
            agencyWithClaims.hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId;
            agencyWithClaims.payer_name = payerInfo.Name;
            agencyWithClaims.insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller;
            agencyWithClaims.clearing_house_id = payerInfo.ClearingHouseSubmitterId;
            agencyWithClaims.provider_claim_type = payerInfo.BillType;
            agencyWithClaims.interchange_receiver_id = payerInfo.InterchangeReceiverId;
            agencyWithClaims.clearing_house = payerInfo.ClearingHouse;
            agencyWithClaims.submitter_name = payerInfo.SubmitterName;
            agencyWithClaims.submitter_phone = payerInfo.SubmitterPhone;
            return agencyWithClaims;
        }

        public AnsiAgency MedicareOrMedicareHMOAgencyWithPatients(ClaimCommandType commandType, ClaimType claimType, long claimId, AxxessSubmitterInfo payerInfo, AgencyLocation branch, List<AnsiPatient> patients)
        {
            var agency = CommonAgencyWithPatients(commandType, claimType, claimId, branch, patients);
            agency.payer_id = payerInfo.Code;
            agency.payer_name = payerInfo.Name;
            agency.submitter_id = payerInfo.SubmitterId;
            agency.submitter_name = payerInfo.SubmitterName;
            agency.submitter_phone = payerInfo.Phone;
            agency.submitter_fax = payerInfo.Fax;
            if (ClaimType.HMO == claimType)
            {
                agency.hmo_payer_id = payerInfo.HMOPayerId;
                agency.hmo_payer_name = payerInfo.HMOPayerName;
                agency.hmo_submitter_id = payerInfo.HMOSubmitterId;
                agency.hmo_provider_id = payerInfo.ProviderId;
                agency.hmo_other_provider_id = payerInfo.OtherProviderId;
                agency.hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId;
                agency.insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller;
                agency.clearing_house_id = payerInfo.ClearingHouseSubmitterId;
                agency.provider_claim_type = payerInfo.BillType;
                agency.interchange_receiver_id = payerInfo.InterchangeReceiverId;
                agency.clearing_house = payerInfo.ClearingHouse;
            }
            return agency;
        }

        private AnsiAgency CommonAgencyWithPatients(ClaimCommandType commandType, ClaimType claimType, long claimId, AgencyLocation branch, List<AnsiPatient> patients)
        {
            var agencyClaim = new
            AnsiAgency
            {
                format = "ansi837",
                submit_type = commandType.ToString(),
                user_login_name = Current.User.Name,
                claim_billtype = claimType.ToString(),
                user_agency_name = branch.Name,
                user_tax_id = branch.TaxId,
                user_national_provider_id = branch.NationalProviderNumber,
                user_address_1 = branch.AddressLine1,
                user_address_2 = branch.AddressLine2,
                user_city = branch.AddressCity,
                user_state = branch.AddressStateCode,
                user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
                user_phone = branch.PhoneWork,
                user_fax = branch.FaxNumber,
                user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
                ansi_837_id = claimId,
                patients_arr = patients
            };
            return agencyClaim;
        }

        public List<AnsiVisit> Visits(List<T> visits, List<ChargeRate> chargeRates, out double totalAmout,ClaimType claimType , List<ServiceAdjustmentWrapper> adjustments)
        {
            var visitTotalAmount = 0.00;
            var visitList = new List<AnsiVisit>();
            if (visits.IsNotNullOrEmpty())
            {
                chargeRates = chargeRates ?? new List<ChargeRate>();
                var visitsDictionary = visits.GroupBy(v => v.VisitDate.Date).ToDictionary(v => v.Key, v => v.ToList());
                if (visitsDictionary != null && visitsDictionary.Count > 0)
                {
                    visitsDictionary.ForEach((visitPerDayKey, visitPerDayValue) =>
                    {
                        var visitByDisciplneDictionary = visitPerDayValue.GroupBy(vd => vd.DisciplineTask).ToDictionary(vd => vd.Key, vd => vd.ToList());
                        if (visitByDisciplneDictionary != null && visitByDisciplneDictionary.Count > 0)
                        {
                            visitByDisciplneDictionary.ForEach((vddk, vddv) =>
                            {
                                var rate = chargeRates.FirstOrDefault(c => c.Id == vddk);
                                if (rate != null)
                                {
                                    double amount;
                                    var addedVisits = BillableVisitForANSIWithAdjustments(rate, vddv, out amount, claimType, true, adjustments);
                                    if (addedVisits != null && addedVisits.Count > 0)
                                    {
                                        visitList.AddRange(addedVisits);
                                        visitTotalAmount += amount;
                                    }
                                }
                            });
                        }
                    });
                }
            }
            totalAmout = visitTotalAmount;
            return visitList;
        }

        public List<AnsiSupply> Supplies(List<Supply> supplies, out double totalAmout)
        {
            var supplyList = new List<AnsiSupply>();
            var supplyTotalAmount = 0.00;
            if (supplies.IsNotNullOrEmpty())
            {
                supplies.ForEach(v =>
                {
                    supplyList.Add(new
                   AnsiSupply
                   {
                        date = v.Date,
                        revenue = v.RevenueCode.IsNotNullOrEmpty() ? v.RevenueCode : string.Empty,
                        hcpcs = v.Code.IsNotNullOrEmpty() ? v.Code : string.Empty,
                        units = v.Quantity,
                        amount = v.UnitCost * v.Quantity,
                        modifier = v.Modifier.IsNotNullOrEmpty() ? v.Modifier : string.Empty
                    });
                    supplyTotalAmount += v.UnitCost * v.Quantity;
                });
            }
            totalAmout = supplyTotalAmount;
            return supplyList;
        }

        #endregion

        #region PDF print

        public BaseInvoiceFormViewData GetManagedClaimInvoiceFormViewData(Guid patientId, Guid claimId)
        {
            var formViewData = new BaseInvoiceFormViewData();
            formViewData.ClaimTypeSubCategory = ClaimTypeSubCategory.ManagedCare;
            var managedClaim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, claimId);
            if (managedClaim != null)
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    var patientWithProfile = basePatientProfileRepository.GetPatientWithProfileForClaimLean(Current.AgencyId, patientId);
                    if (patientWithProfile != null)
                    {
                        var claimData = EntityHelper.GetManagedClaimViewData(managedClaim);
                        claimData.CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode);
                        claimData.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;
                        formViewData.Agency = agency;
                        formViewData.AgencyLocation = agencyRepository.FindLocation(Current.AgencyId, patientWithProfile.Profile.AgencyLocationId);
                        formViewData.Claim = claimData;
                        SetInvoiceFormAppSpecific<ManagedClaimInsurance>(formViewData, managedClaim);
                        if (formViewData.InvoiceType == InvoiceType.HCFA)
                        {
                            claimData.HCFALocators = managedClaim.HCFALocators;
                            claimData.Relationship = ClaimRelationship(claimData.RelationshipId);
                            claimData.MaritalStatus = patientWithProfile.MaritalStatus;
                            claimData.TelephoneNum = patientWithProfile.PhoneHome;
                            
                        }
                        else if (formViewData.InvoiceType == InvoiceType.Invoice)
                        {
                            var paymentSum = baseBillingRepository.GetManagedClaimPaymentsByClaimAndPatient(Current.AgencyId, patientId, claimId).Sum(p => p.Payment);
                            var adjustmentSum = baseBillingRepository.GetManagedClaimAdjustmentsByClaimAndPatient(Current.AgencyId, patientId, claimId).Sum(a => a.Adjustment);
                            claimData.Id = managedClaim.Id;
                            claimData.PaymentAmount = paymentSum;
                            claimData.ClaimAmount = managedClaim.ProspectivePay;
                            claimData.AdjustmentAmount = adjustmentSum;
                            GetInvoiceNumber(managedClaim, claimData);
                        }
                        var schedules = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<T>();
                        if (schedules.IsNotNullOrEmpty())
                        {
                            formViewData.BillSchedules = BillableVisitSummary( schedules, ClaimType.MAN, formViewData.Claim.ChargeRates ?? new List<ChargeRate>(), true);
                        }
                    }
                }
            }
            return formViewData;
        }

   
        //public InvoiceViewData GetManagedInvoiceInfo(Guid patientId, Guid claimId, bool isForPatient)
        //{
        //    //var invoiceViewData = new InvoiceViewData();
        //    invoiceViewData.IsForPatient = isForPatient;
        //    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
        //    if (agency != null)
        //    {
        //        var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
        //        if (profile != null)
        //        {
        //            var managedClaim = baseBillingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
        //            if (managedClaim != null)
        //            {
        //                var paymentSum = baseBillingRepository.GetManagedClaimPaymentsByClaimAndPatient(Current.AgencyId, patientId, claimId).Sum(p => p.Payment);
        //                var adjustmentSum = baseBillingRepository.GetManagedClaimAdjustmentsByClaimAndPatient(Current.AgencyId, patientId, claimId).Sum(a => a.Adjustment);
        //                var claimData = EntityHelper.GetClaimViewData(managedClaim);
        //                claimData.Id = managedClaim.Id;
        //                claimData.PaymentAmount = paymentSum;
        //                claimData.ClaimAmount = managedClaim.ProspectivePay;
        //                claimData.AdjustmentAmount = adjustmentSum;
                        
                        
        //                claimData.CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode);
        //                claimData.Type = managedClaim.Type;
        //                claimData.MedicareNumber = managedClaim.InsuranceIdNumber;
        //                claimData.SupplyCode = managedClaim.SupplyCode;
        //                claimData.AgencyLocationId = profile.AgencyLocationId;

        //                claimData.IsHomeHealthServiceIncluded = managedClaim.IsHomeHealthServiceIncluded;
        //                claimData.Insurance = managedClaim.Insurance;

        //                invoiceViewData.Agency = agency;
        //                invoiceViewData.AgencyLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
        //                invoiceViewData.Claim = claimData;
        //                invoiceViewData.ClaimNumber = baseBillingRepository.GetClaimNumber(managedClaim.Id);

        //                SetInvoiceFormAppSpecific(invoiceViewData, managedClaim);
        //                var schedules = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<T>();
        //                if (schedules != null && schedules.Count > 0)
        //                {
        //                    invoiceViewData.BillSchedules = BillableVisitSummary(profile.AgencyLocationId, schedules, ClaimType.MAN, invoiceViewData.Claim.ChargeRates, true);

        //                }
        //            }
        //        }
        //    }
        //    return invoiceViewData;
        //}

        //public UBOFourViewData GetManagedUBOFourInfo(Guid patientId, Guid claimId)
        //{
        //    //var uboFourViewData = new UBOFourViewData();
        //    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
        //    if (agency != null)
        //    {
                
        //        var profile = basePatientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
        //        if (profile != null)
        //        {
                  
        //            var managedClaim = baseBillingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
        //            if (managedClaim != null)
        //            {
        //                var claimData = EntityHelper.GetClaimViewData(managedClaim);
                       
        //                claimData.CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode);
        //                claimData.Type = managedClaim.Type;
        //                claimData.MedicareNumber = managedClaim.InsuranceIdNumber;
        //                claimData.SupplyCode = managedClaim.SupplyCode;
        //                claimData.AgencyLocationId = profile.AgencyLocationId;

        //                claimData.IsHomeHealthServiceIncluded = managedClaim.IsHomeHealthServiceIncluded;

        //                uboFourViewData.Agency = agency;
        //                uboFourViewData.AgencyLocation = agencyRepository.FindLocation(Current.AgencyId, profile.AgencyLocationId);
        //                uboFourViewData.Claim = claimData;
        //                SetInvoiceFormAppSpecific(uboFourViewData, managedClaim);
        //                var schedules = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<T>();
        //                if (schedules.IsNotNullOrEmpty())
        //                {
        //                    uboFourViewData.BillSchedules = BillableVisitSummary(profile.AgencyLocationId, schedules, ClaimType.MAN, uboFourViewData.Claim.ChargeRates, true);
        //                }
        //            }
        //        }
        //    }
        //    return uboFourViewData;
        //}

        //public HCFA1500ViewData GetHCFA1500InfoForManagedClaim(Guid patientId, Guid claimId)
        //{
        //   // var hcfa1500ViewData = new HCFA1500ViewData();
        //    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
        //    if (agency != null)
        //    {
        //        var patientWithProfile = basePatientProfileRepository.GetPatientWithProfileForClaimLean(Current.AgencyId, patientId);
        //        if (patientWithProfile != null)
        //        {
        //            var managedClaim = baseBillingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
        //            if (managedClaim != null)
        //            {
        //                var claimData = EntityHelper.GetClaimViewData(managedClaim);

        //                claimData.CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode);
        //                claimData.Type = managedClaim.Type;
        //                claimData.MedicareNumber = managedClaim.InsuranceIdNumber;
        //                claimData.SupplyCode = managedClaim.SupplyCode;
        //                claimData.AgencyLocationId = patientWithProfile.Profile.AgencyLocationId;

        //                claimData.HCFALocators = managedClaim.HCFALocators;
        //                claimData.Relationship = ClaimRelationship(claimData.RelationshipId);

        //                hcfa1500ViewData.Agency = agency;
        //                hcfa1500ViewData.AgencyLocation = agencyRepository.FindLocation(Current.AgencyId, patientWithProfile.Profile.AgencyLocationId);
        //                hcfa1500ViewData.PatientMaritalStatus = patientWithProfile.MaritalStatus;
        //                hcfa1500ViewData.PatientTelephoneNum = patientWithProfile.PhoneHome;
        //                hcfa1500ViewData.Claim = claimData;
        //                SetInvoiceFormAppSpecific(hcfa1500ViewData, managedClaim);
        //                var schedules = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<T>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<T>();
        //                if (schedules.IsNotNullOrEmpty())
        //                {
        //                    hcfa1500ViewData.BillSchedules = BillableVisitSummary(patientWithProfile.Profile.AgencyLocationId, schedules, hcfa1500ViewData.Claim.ClaimType, hcfa1500ViewData.Claim.ChargeRates, true);
        //                }
        //            }
        //        }
        //    }
        //    return hcfa1500ViewData;
        //}



        //public void vv(BaseInvoiceFormViewData claimFormViewData)
        //{
        //    if (claimFormViewData.Claim.PrimaryInsuranceId >= 1000)
        //    {
        //        if (agencyInsurance != null)
        //        {
        //            if (agencyInsurance.PayorId.IsNotNullOrEmpty() && !(agencyInsurance.PayorId == "0"))
        //            {
        //                claimFormViewData.Claim.PayerId = agencyInsurance.PayorId;
        //            }
        //            if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO)
        //            {
        //                claimFormViewData.Claim.IsHMO = true;
        //            }
        //            claimFormViewData.Claim.PayorName = agencyInsurance.Name;
        //            claimFormViewData.Claim.PayorType = agencyInsurance.PayorType;
        //            claimFormViewData.PayorName = agencyInsurance.Name;
        //            claimFormViewData.PayorAddressLine1 = agencyInsurance.AddressFirstRow;
        //            claimFormViewData.PayorAddressLine2 = agencyInsurance.AddressSecondRow;
        //            claimFormViewData.Claim.OtherProviderId = agencyInsurance.OtherProviderId;
        //            claimFormViewData.Claim.ProviderId = agencyInsurance.ProviderId;
        //            claimFormViewData.Claim.ProviderSubscriberId = agencyInsurance.ProviderSubscriberId;
        //        }
        //        claimFormViewData.Claim.ClaimType = ClaimType.MAN;
        //    }
        //    else if (MedicareIntermediaryFactory.Intermediaries().Contains(claimFormViewData.Claim.PrimaryInsuranceId))
        //    {
        //        claimFormViewData.Claim.IsHMO = false;
        //        claimFormViewData.Claim.ClaimType = ClaimType.CMS;
        //        if (Enum.IsDefined(typeof(MedicareIntermediary), claimFormViewData.Claim.PrimaryInsuranceId))
        //        {
        //            claimFormViewData.Claim.PayorName = ((MedicareIntermediary)claimFormViewData.Claim.PrimaryInsuranceId).GetDescription();
        //        }
        //    }
        //}

        protected abstract void SetInvoiceFormAppSpecific<C>(BaseInvoiceFormViewData claimFormViewData, BaseClaim claim) where C : ClaimInsurance, new();

        protected void SetInvoiceFormInsuranceInfo<C>(BaseInvoiceFormViewData claimFormViewData) where C : ClaimInsurance, new()
        {
            var claimInsurance = baseMongoRepository.GetClaimInsurance<C>(Current.AgencyId, claimFormViewData.Claim.Id);
            if (claimInsurance != null)
            {
                if (claimInsurance.Insurance != null)
                {
                    SetInvoiceFormInsuranceInfoHelper<C>(claimFormViewData, claimInsurance.Insurance);
                }
                else
                {
                    SetInvoiceFormInsuranceInfoSaveOrUpdateHelper<C>(claimFormViewData, claimInsurance);
                }
            }
            else
            {
                claimInsurance = new C { AgencyId = Current.AgencyId, PatientId = claimFormViewData.Claim.PatientId, Id = claimFormViewData.Claim.Id };
                SetInvoiceFormInsuranceInfoSaveOrUpdateHelper<C>(claimFormViewData, claimInsurance);
            }

        }

        private void SetInvoiceFormInsuranceInfoSaveOrUpdateHelper<C>(BaseInvoiceFormViewData claimFormViewData, C claimInsurance) where C : ClaimInsurance,new()
        {
            AgencyInsurance insurance = null;
            if (claimFormViewData.Claim.Insurance.IsNotNullOrEmpty())
            {
                insurance = claimFormViewData.Claim.Insurance.ToObject<AgencyInsurance>();
                claimInsurance.Insurance = insurance;
            }
            else
            {
                if (claimFormViewData.Claim.PrimaryInsuranceId >= 1000)
                {
                    insurance = agencyRepository.GetInsurance(claimFormViewData.Claim.PrimaryInsuranceId, Current.AgencyId);
                    claimInsurance.Insurance = insurance;
                }
                else if (MedicareIntermediaryFactory.Intermediaries().Contains(claimFormViewData.Claim.PrimaryInsuranceId))
                {
                    insurance = this.CMSInsuranceToAgencyInsurance(claimFormViewData.Agency, claimFormViewData.AgencyLocation, claimFormViewData.Claim.PrimaryInsuranceId, true);
                    claimInsurance.Insurance = insurance;
                }
            }
            if (insurance != null)
            {
                if (baseMongoRepository.UpdateClaimInsurance(claimInsurance))
                {
                    SetInvoiceFormInsuranceInfoHelper<C>(claimFormViewData, insurance);
                }
            }
        }

        private static void SetInvoiceFormInsuranceInfoHelper<C>(BaseInvoiceFormViewData claimFormViewData, AgencyInsurance insurance) where C : ClaimInsurance,new()
        {
            if (insurance.PayorId.IsNotNullOrEmpty() && !(insurance.PayorId == "0"))
            {
                claimFormViewData.Claim.PayerId = insurance.PayorId;
            }
            if (MedicareIntermediaryFactory.Intermediaries().Contains(claimFormViewData.Claim.PrimaryInsuranceId))
            {
                claimFormViewData.Claim.IsHMO = false;
                claimFormViewData.Claim.ClaimType = ClaimType.CMS;
                claimFormViewData.InvoiceType = InvoiceType.UB;
            }
            else
            {
                if (insurance.PayorType == (int)PayerTypes.MedicareHMO)
                {
                    claimFormViewData.Claim.IsHMO = true;
                }
                claimFormViewData.Claim.ClaimType = ClaimType.MAN;
            }
            claimFormViewData.Claim.PayorName = insurance.Name;
            claimFormViewData.Claim.PayorType = insurance.PayorType;
            claimFormViewData.PayorName = insurance.Name;
            claimFormViewData.PayorAddressLine1 = insurance.AddressFirstRow;
            claimFormViewData.PayorAddressLine2 = insurance.AddressSecondRow;
            claimFormViewData.Claim.OtherProviderId = insurance.OtherProviderId;
            claimFormViewData.Claim.ProviderId = insurance.ProviderId;
            claimFormViewData.Claim.ProviderSubscriberId = insurance.ProviderSubscriberId;
            if (claimFormViewData.ClaimTypeSubCategory != ClaimTypeSubCategory.RAP)
            {
                if (insurance.BillData.IsNotNullOrEmpty())
                {
                    claimFormViewData.Claim.ChargeRates = insurance.BillData.ToObject<List<ChargeRate>>();
                }
                else
                {
                    claimFormViewData.Claim.ChargeRates = new List<ChargeRate>();
                }
            }
            if (insurance.InvoiceType == (int)InvoiceType.HCFA)
            {
                claimFormViewData.InvoiceType = InvoiceType.HCFA;
            }
            else if (insurance.InvoiceType == (int)InvoiceType.Invoice)
            {
                claimFormViewData.InvoiceType = InvoiceType.Invoice;
            }
        }


        #endregion

        #region Private Methods

        //private bool IsFirstBillableVisit(Claim claim)
        //{
        //    var isFirstBillableVisit = false;
        //    if (claim != null && claim.Schedule.IsNotNullOrEmpty())
        //    {
        //        var status = new string[] { 
        //            ((int)ScheduleStatus.NoteCompleted).ToString(),

        //            ((int)ScheduleStatus.OasisCompletedExportReady).ToString(),
        //            ((int)ScheduleStatus.OasisExported).ToString(),
        //            ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),

        //            ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString(),
        //            ((int)ScheduleStatus.EvalSentToPhysician).ToString(),
        //            ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
        //            ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString()

        //        };
        //        isFirstBillableVisit = claim.Schedule.ToObject<List<ScheduleEvent>>().Exists(s => !s.EventId.IsEmpty() && s.VisitDate.IsValidDate() && s.VisitDate.ToDateTime().Date >= claim.EpisodeStartDate.Date && s.VisitDate.ToDateTime().Date <= claim.EpisodeEndDate.Date && s.IsBillable && status.Contains(s.Status) && !s.IsDeprecated && !s.IsMissedVisit);
        //    }
        //    return isFirstBillableVisit;


        //}

        //private bool IsOasisComplete(Claim claim)
        //{
        //    var isOasisComplete = false;
        //    if (claim != null && claim.Schedule.IsNotNullOrEmpty())
        //    {
        //        var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date && !s.IsDeprecated && !s.IsMissedVisit);

        //        var socSchedule = scheduleEvents.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCStartOfCare || s.DisciplineTask == (int)DisciplineTasks.OASISCStartOfCarePT|| s.DisciplineTask == (int)DisciplineTasks.OASISCStartOfCareOT);
        //        if (socSchedule != null)
        //        {
        //            var status = socSchedule.Status;
        //            if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString() || status==((int)ScheduleStatus.OasisCompletedNotExported).ToString())
        //            {
        //                isOasisComplete = true;
        //            }
        //        }
        //        else
        //        {
        //            var previousEpisode = patientRepository.GetPreviousEpisode(Current.AgencyId, claim.PatientId, claim.EpisodeStartDate);
        //            if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
        //            {
        //                var previousSchedules = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date < claim.EpisodeStartDate.Date && !s.IsDeprecated && !s.IsMissedVisit).OrderByDescending(s => s.EventDate.ToDateTime().Date).ToList();
        //                if (previousSchedules != null)
        //                {
        //                    var recertSchedule = previousSchedules.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT);
        //                    if (recertSchedule != null)
        //                    {
        //                        var status = recertSchedule.Status;
        //                        if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString() || status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
        //                        {
        //                            isOasisComplete = true;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        var rocSchedule = previousSchedules.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
        //                        if (rocSchedule != null)
        //                        {
        //                            var status = rocSchedule.Status;
        //                            if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString() || status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
        //                            {
        //                                isOasisComplete = true;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return isOasisComplete;
        //}

        //private bool IsOrdersComplete(Claim claim)
        //{
        //    var result = false;
        //    if (claim != null && claim.Schedule.IsNotNullOrEmpty())
        //    {
        //        var currentScheduleList = new List<ScheduleEvent>();
        //        var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
        //            && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date
        //            && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date
        //            && !s.IsDeprecated && !s.IsMissedVisit
        //            && (s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)
        //            && !s.IsOrderForNextEpisode
        //            && s.Status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString()).ToList();

        //        if (scheduleEvents != null && scheduleEvents.Count > 0)
        //        {
        //            currentScheduleList.AddRange(scheduleEvents);
        //        }
        //        var previousScheduleList = new List<ScheduleEvent>();
        //        var previousEpisode = patientRepository.GetPreviousEpisode(Current.AgencyId, claim.PatientId, claim.EpisodeStartDate);
        //        if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
        //        {
        //            previousScheduleList = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= previousEpisode.StartDate.Date && s.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && !s.IsDeprecated && ((s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)) && s.IsOrderForNextEpisode == true && s.Status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString()).ToList();
        //            if (previousScheduleList != null && previousScheduleList.Count > 0)
        //            {
        //                currentScheduleList.AddRange(previousScheduleList);
        //            }
        //        }
        //        if (currentScheduleList != null && currentScheduleList.Count > 0)
        //        {
        //                result = false;
        //        }
        //        else
        //        {
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        //private bool IsVisitComplete(Claim claim)
        //{
        //    var result = false;
        //    if (claim != null && claim.Schedule.IsNotNullOrEmpty())
        //    {
        //        var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() 
        //            && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date 
        //            && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date 
        //            && !s.IsDeprecated 
        //            && !(s.Discipline == Disciplines.ReportsAndNotes.ToString()) 
        //            && !(s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter)
        //            && !(s.Discipline == Disciplines.Orders.ToString())
        //            && !(s.Discipline == Disciplines.Claim.ToString()) 
        //            && (!(s.Status == ((int)ScheduleStatus.NoteCompleted).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.OasisExported).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString()) 
        //            && !(s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())) 
        //            && !s.IsMissedVisit 
        //            && s.IsBillable).ToList();
        //        if (scheduleEvents != null && scheduleEvents.Count > 0)
        //        {
        //            result = false;
        //        }
        //        else
        //        {
        //            result = true;
        //        }
        //    }
        //    else
        //    {
        //        result = true;
        //    }

        //    return result;
        //}

        protected List<BillSchedule> BillUnitTypeSchedules(ChargeRate rate, List<T> visits, ClaimType claimType, bool isLimitApplied)
        {
            var schedules = new List<BillSchedule>();
            if (visits.IsNotNullOrEmpty())
            {
                if (rate.IsUnitsPerDayOnSingleLineItem)
                {
                    var oneDaySchedule = GetBillSchedule(rate, visits, claimType, isLimitApplied);
                    if (oneDaySchedule != null)
                    {
                        schedules.Add(oneDaySchedule);
                    }
                }
                else
                {
                    visits.ForEach(v =>
                    {
                        if (rate.ChargeType.IsNotNullOrEmpty() && rate.ChargeType.IsInteger())
                        {
                            if (rate.ChargeType.ToInteger() == (int)BillUnitType.PerVisit || !rate.IsTimeLimit)
                            {
                                var scheduleEvent = GetBillSchedule(rate, v, claimType, rate.IsTimeLimit);
                                if (scheduleEvent != null)
                                {
                                    schedules.Add(scheduleEvent);
                                }
                            }
                            else if (rate.IsTimeLimit && rate.ChargeType.ToInteger() == (int)BillUnitType.Per15Min)
                            {
                                var scheduleEvents = Per15MinTimeLimit(rate, v);
                                if (scheduleEvents != null && scheduleEvents.Count > 0)
                                {
                                    schedules.AddRange(scheduleEvents);
                                }
                            }
                            else if (rate.IsTimeLimit && rate.ChargeType.ToInteger() == (int)BillUnitType.Hourly)
                            {
                                var scheduleEvents = HourlyTimeLimit(rate, v);
                                if (scheduleEvents != null && scheduleEvents.Count > 0)
                                {
                                    schedules.AddRange(scheduleEvents);
                                }
                            }
                        }
                    });
                }
            }
            return schedules;
        }

        protected List<object> ConvertLocatorToList(Locator locator)
        {
            var objectList = new List<object>();
            {
                objectList.Add(new { code1 = locator.Code1 });
            };

            return objectList;
        }

        protected List<object> ConvertStringLocatorToList(string locator)
        {
            var locatorLists = locator.IsNotNullOrEmpty() ? locator.ToObject<List<Locator>>() : new List<Locator>();
            var objectList = new List<object>();
            if (locatorLists != null && locatorLists.Count > 0)
            {
                locatorLists.ForEach(l =>
                {
                    objectList.Add(new { code1 = l.Code1, code2 = l.Code2 });
                });
            }
            return objectList;
        }

        #endregion

        protected List<Supply> GetSupplies(Guid patientId, string existingSupply, List<T> visitList, bool isBillbaleSet)
        {
            var supplyList = existingSupply.IsNotNullOrEmpty() ? existingSupply.ToObject<List<Supply>>() : new List<Supply>();
            var newSupplyList = new List<Supply>();
            if (visitList.IsNotNullOrEmpty())
            {
                var assessmentSupplyIds = visitList.Where(v => DisciplineTaskFactory.EpisodeAllAssessments(true).Contains(v.DisciplineTask) || DisciplineTaskFactory.FollowUpDisciplineTasks().Contains(v.DisciplineTask)).Select(v => v.Id).Distinct().ToList();
                if (assessmentSupplyIds != null && assessmentSupplyIds.Count > 0)
                {
                    var assessmentSupplies = NoteAndAssessmentHelperFactory<T>.GetAssessmentSupplies(patientId, assessmentSupplyIds);
                    if (assessmentSupplies.IsNotNullOrEmpty())
                    {
                        assessmentSupplies.ForEach(a =>
                        {
                            var supplies = a.Supply.ToObject<List<Supply>>();
                            if (supplies != null && supplies.Count > 0)
                            {
                                newSupplyList.AddRange(supplies);
                            }
                        });
                    }
                }
                var noteSupplyIds = visitList.Where(v => DisciplineTaskFactory.SkilledNurseSharedFile().Contains(v.DisciplineTask) || DisciplineTaskFactory.AllTherapy(true).Contains(v.DisciplineTask)).Select(v => v.Id).Distinct().ToList();
                if (noteSupplyIds.IsNotNullOrEmpty())
                {
                    var noteSupplies = NoteAndAssessmentHelperFactory<T>.GetNoteSupplies(patientId, noteSupplyIds);
                    if (noteSupplies.IsNotNullOrEmpty())
                    {
                        noteSupplies.ForEach(a =>
                        {
                            var supplies = a.Supply.ToObject<List<Supply>>();
                            if (supplies.IsNotNullOrEmpty())
                            {
                                newSupplyList.AddRange(supplies);
                            }
                        });
                    }
                }
                if (newSupplyList.IsNotNullOrEmpty())
                {
                    newSupplyList.ForEach(s =>
                    {

                        if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                        {
                            if (isBillbaleSet)
                            {
                                s.IsBillable = true;
                            }
                            supplyList.Add(s);
                        }

                    });
                }
            }
            return supplyList;
        }

        public List<Supply> GetSuppliesByFlag(Guid Id, Guid patientId, string Type, bool isBillable)
        {
            var supplies = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                {
                    var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = GetSupplies(claim.Supply, isBillable, claim.IsSupplyVerified);
                    }
                }
                else
                {
                    supplies = GetSuppliesByFlagAppSpecific(Id, patientId, Type, isBillable);
                }
            }
            return supplies;
        }

        protected abstract List<Supply> GetSuppliesByFlagAppSpecific(Guid Id, Guid patientId, string Type, bool isBillable);

        public List<Supply> GetSupplies(string supplyXml, bool isBillable, bool isSupplyVerified)
        {
            return supplyXml
                .ToObject<List<Supply>>()
                .Where(s => s.IsBillable == isBillable && !s.IsDeprecated)
                .ToList() ?? new List<Supply>();
        }

        public bool AddSupplyManagedClaim(Guid patientId, Guid Id, Supply supply)
        {
            bool result = false;
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Supply = AddSupplyToXml(claim.Supply, supply);
                if (baseBillingRepository.UpdateManagedClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public Supply GetSupplyManagedClaim(Guid Id, int supplyBillingId)
        {
            var supply = new Supply();
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, Id);
            if (claim != null)
            {
                supply = GetSupplyFromXml(claim.Supply, supplyBillingId);
            }
            return supply;
        }

        public ClaimEditSupplyViewData GetSupplyManagedClaim(Guid patientId, Guid Id, int supplyBillingId)
        {
            return new ClaimEditSupplyViewData
            {
                Id = Id,
                PatientId = patientId,
                Type = ClaimTypeSubCategory.ManagedCare.ToString(),
                Service = this.Service,
                Supply = this.GetSupplyManagedClaim(Id, supplyBillingId)
            };
        }

        public bool EditSupplyManagedClaim(Guid patientId, Guid Id, Supply supply)
        {
            bool result = false;
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Supply = EditSupplyInXml(claim.Supply, supply);
                if (baseBillingRepository.UpdateManagedClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool ChangeStatusOfSuppliesManagedClaim(Guid patientId, Guid Id, List<int> ids, bool IsBillable)
        {
            bool result = false;
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                claim.Supply = ChangeStatusOfSuppliesInXml(claim.Supply, IsBillable, ids);
                if (baseBillingRepository.UpdateManagedClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteSupplyManagedClaim(Guid patientId, Guid Id, List<int> ids)
        {
            bool result = false;
            var claim = baseBillingRepository.GetManagedClaimOnly(Current.AgencyId, patientId, Id);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                claim.Supply = DeprecateSuppliesInXml(claim.Supply, ids);
                if (baseBillingRepository.UpdateManagedClaimModel(claim))
                {
                    result = true;
                }
            }
            return result;
        }

        protected string AddSupplyToXml(string supplyXml, Supply newSupply)
        {
            var suppliesToEdit = new List<Supply>();
            if (supplyXml.IsNotNullOrEmpty())
            {
                suppliesToEdit = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            }
            if (newSupply != null)
            {
                if (newSupply.UniqueIdentifier.IsEmpty())
                {
                    newSupply.UniqueIdentifier = Guid.NewGuid();
                }
                newSupply.IsBillable = true;
                newSupply.IsAddedFromBilling = true;
                newSupply.BillingId = suppliesToEdit.Count;
                suppliesToEdit.Add(newSupply);
            }
            return suppliesToEdit.ToXml();
        }

        protected Supply GetSupplyFromXml(string supplyXml, int billingId)
        {
            Supply supply = new Supply();
            var supplies = new List<Supply>();
            if (supplyXml.IsNotNullOrEmpty())
            {
                supplies = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            }
            if (supplies.IsNotNullOrEmpty())
            {
                supply = supplies.FirstOrDefault(s => s.BillingId == billingId);
            }
            return supply;
        }

        protected string EditSupplyInXml(string supplyXml, Supply edittedSupply)
        {
            Supply supply = new Supply();
            var supplies = new List<Supply>();
            if (supplyXml.IsNotNullOrEmpty())
            {
                supplies = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            }
            if (supplies.IsNotNullOrEmpty())
            {
                var supplyToEdit = supplies.FirstOrDefault(s => s.BillingId == edittedSupply.BillingId);
                if (supplyToEdit != null)
                {
                    supplyToEdit.Description = edittedSupply.Description;
                    supplyToEdit.UniqueIdentifier = edittedSupply.UniqueIdentifier;
                    supplyToEdit.RevenueCode = edittedSupply.RevenueCode;
                    supplyToEdit.Code = edittedSupply.Code;
                    supplyToEdit.Date = edittedSupply.Date;
                    supplyToEdit.Quantity = edittedSupply.Quantity;
                    supplyToEdit.UnitCost = edittedSupply.UnitCost;
                }
            }
            return supplies.ToXml();
        }

        protected string DeprecateSuppliesInXml(string supplyXml, List<int> ids)
        {
            var suppliesToEdit = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            suppliesToEdit.ForEach(s =>
            {
                if (ids.Contains(s.BillingId))
                {
                    s.IsDeprecated = true;
                }
            });
            return suppliesToEdit.ToXml();
        }

        protected string ChangeStatusOfSuppliesInXml(string supplyXml, bool isBillable, List<int> ids)
        {
            var suppliesToEdit = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            suppliesToEdit.ForEach(s =>
            {
                if (ids.Contains(s.BillingId))
                {
                    s.IsBillable = isBillable;
                }
            });
            return suppliesToEdit.ToXml();
        }

        //protected List<Locator> ConvertStringToLocatorList(FormCollection formCollection, string stringLocator)
        //{
        //    var locatorList = formCollection[stringLocator].ToArray();
        //    var locators = new List<Locator>();
        //    if (locatorList != null && locatorList.Length > 0)
        //    {
        //        var keys = formCollection.AllKeys;
        //        locatorList.ForEach(l =>
        //        {
        //            if (keys.Contains(l + "_Code1") || keys.Contains(l + "_Code2") || keys.Contains(l + "_Code3"))
        //            {
        //                Locator locator = new Locator(l);
        //                if (keys.Contains(l + "_Code1"))
        //                {
        //                    locator.Code1 = formCollection[l + "_Code1"];
        //                }
        //                if (keys.Contains(l + "_Code2"))
        //                {
        //                    locator.Code2 = formCollection[l + "_Code2"];
        //                }
        //                if (keys.Contains(l + "_Code3"))
        //                {
        //                    locator.Code3 = formCollection[l + "_Code3"];
        //                }
        //                locators.Add(locator);
        //            }
        //        });
        //    }
        //    return locators;
        //}

        protected abstract List<Claim> GetUnProcessedAppSpecific(ClaimTypeSubCategory claimType, Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll, bool isUsersNeeded);

        private List<BillSchedule> Per15MinTimeLimit(ChargeRate rate, T visit)
        {
            var schedules = new List<BillSchedule>();
            var schedule = new BillSchedule();
            schedule.EventId = visit.Id;
            schedule.EventDate = visit.EventDate;
            schedule.VisitDate = visit.VisitDate;
            schedule.DisciplineTaskName = visit.DisciplineTaskName;
            schedule.PereferredName = rate.PreferredDescription.IsNotNullOrEmpty() ? rate.PreferredDescription : schedule.DisciplineTaskName;
            schedule.StatusName = visit.StatusName;
            schedule.HCPCSCode = rate.Code;
            schedule.RevenueCode = rate.RevenueCode;
            schedule.Modifier = rate.Modifier;
            schedule.Modifier2 = rate.Modifier2;
            schedule.Modifier3 = rate.Modifier3;
            schedule.Modifier4 = rate.Modifier4;
            if (rate.IsTimeLimit)
            {
                var totalMinLimit = rate.TimeLimitHour * 60 + rate.TimeLimitMin;
                var totalTimeEventTake = visit.MinSpent;
                if (totalTimeEventTake <= totalMinLimit)
                {
                    schedule.Unit = (int)Math.Ceiling((double)totalTimeEventTake / 15);
                    schedule.Charge = schedule.Unit * rate.Charge;
                    schedules.Add(schedule);
                }
                else if (totalTimeEventTake > totalMinLimit)
                {
                    schedule.Unit = (int)Math.Ceiling((double)totalMinLimit / 15);
                    schedule.Charge = schedule.Unit * rate.Charge;
                    schedules.Add(schedule);

                    if (rate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                    {
                        schedules.Add(
                            new BillSchedule
                            {
                                EventId = visit.Id,
                                EventDate = visit.EventDate,
                                VisitDate = visit.VisitDate,
                                Unit = rate.SecondUnit,
                                Charge = rate.IsSecondChargeDifferent ? rate.SecondCharge : 0,
                                DisciplineTaskName = visit.DisciplineTaskName,
                                PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription,
                                StatusName = visit.StatusName,
                                HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                                RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                                Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                                Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                                Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                                Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty,
                                IsExtraTime = true
                            }
                            );
                    }
                    else
                    {
                        var minute = rate.SecondChargeType == ((int)BillUnitType.Hourly).ToString() ? 60 : 15;
                        var unit = (int)Math.Ceiling((double)(totalTimeEventTake - totalMinLimit) / minute);
                        var charge = rate.IsSecondChargeDifferent ? rate.SecondCharge : (rate.SecondChargeType == ((int)BillUnitType.Hourly).ToString() ? 0 : rate.Charge);
                        if (rate.IsUnitPerALineItem)
                        {
                            for (int i = 0; i < unit; i++)
                            {
                                var addedBillSchedule =
                                    new BillSchedule
                                    {
                                        EventId = visit.Id,
                                        EventDate = visit.EventDate,
                                        VisitDate = visit.VisitDate,
                                        Unit = 1,
                                        Charge = charge,
                                        DisciplineTaskName = visit.DisciplineTaskName,
                                        PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription,
                                        StatusName = visit.StatusName,
                                        HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                                        RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                                        Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                                        Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                                        Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                                        Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty,
                                        IsExtraTime = true
                                    };
                                schedules.Add(addedBillSchedule);
                            }
                        }
                        else
                        {
                            var addedBillSchedule = new
                            BillSchedule
                            {
                                EventId = visit.Id,
                                EventDate = visit.EventDate,
                                VisitDate = visit.VisitDate,
                                Unit = unit,
                                Charge = unit * charge,
                                DisciplineTaskName = visit.DisciplineTaskName,
                                PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription,
                                StatusName = visit.StatusName,
                                HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                                RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                                Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                                Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                                Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                                Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty,
                                IsExtraTime = true
                            };
                            schedules.Add(addedBillSchedule);
                        }
                    }
                }
            }
            else
            {
                schedule.Unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                schedule.Charge = schedule.Unit * rate.Charge;
                schedules.Add(schedule);
            }
            return schedules;
        }

        private List<BillSchedule> HourlyTimeLimit(ChargeRate rate, T visit)
        {
            var schedules = new List<BillSchedule>();
            var schedule = new BillSchedule();
            schedule.EventId = visit.Id;
            schedule.EventDate = visit.EventDate;
            schedule.VisitDate = visit.VisitDate;
            schedule.DisciplineTaskName = visit.DisciplineTaskName;
            schedule.PereferredName = rate.PreferredDescription.IsNotNullOrEmpty() ? rate.PreferredDescription : schedule.DisciplineTaskName;
            schedule.StatusName = visit.StatusName;
            schedule.HCPCSCode = rate.Code;
            schedule.RevenueCode = rate.RevenueCode;
            schedule.Modifier = rate.Modifier;
            schedule.Modifier2 = rate.Modifier2;
            schedule.Modifier3 = rate.Modifier3;
            schedule.Modifier4 = rate.Modifier4;
            if (rate.IsTimeLimit)
            {
                var totalMinLimit = rate.TimeLimitHour * 60 + rate.TimeLimitMin;
                var totalTimeEventTake = visit.MinSpent;
                if (totalTimeEventTake <= totalMinLimit)
                {
                    schedule.Unit = (int)Math.Ceiling((double)totalTimeEventTake / 60);
                    schedule.Charge = schedule.Unit * rate.Charge;
                    schedules.Add(schedule);
                }
                else if (totalTimeEventTake > totalMinLimit)
                {
                    schedule.Unit = (int)Math.Ceiling((double)totalMinLimit / 60);
                    schedule.Charge = schedule.Unit * rate.Charge;
                    schedules.Add(schedule);

                    if (rate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                    {
                        schedules.Add(
                            new BillSchedule
                            {
                                EventId = visit.Id,
                                EventDate = visit.EventDate,
                                VisitDate = visit.VisitDate,
                                Unit = rate.SecondUnit,
                                Charge = rate.IsSecondChargeDifferent ? rate.SecondCharge : 0,
                                DisciplineTaskName = visit.DisciplineTaskName,
                                PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription,
                                StatusName = visit.StatusName,
                                HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                                RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                                Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                                Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                                Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                                Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty,
                                IsExtraTime = true
                            }
                            );
                    }
                    else
                    {
                        var minute = rate.SecondChargeType == ((int)BillUnitType.Per15Min).ToString() ? 15 : 60;
                        var unit = (int)Math.Ceiling((double)(totalTimeEventTake - totalMinLimit) / minute);
                        var charge = rate.IsSecondChargeDifferent ? rate.SecondCharge : (rate.SecondChargeType == ((int)BillUnitType.Per15Min).ToString() ? 0 : rate.Charge);
                        if (rate.IsUnitPerALineItem)
                        {
                            for (int i = 0; i < unit; i++)
                            {
                                var addedBillSchedule =
                                    new BillSchedule
                                    {
                                        EventId = visit.Id,
                                        EventDate = visit.EventDate,
                                        VisitDate = visit.VisitDate,
                                        Unit = 1,
                                        Charge = charge,
                                        DisciplineTaskName = visit.DisciplineTaskName,
                                        PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription,
                                        StatusName = visit.StatusName,
                                        HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                                        RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                                        Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                                        Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                                        Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                                        Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty,
                                        IsExtraTime = true
                                    };
                                schedules.Add(addedBillSchedule);
                            }
                        }
                        else
                        {
                            var addedBillSchedule = new
                            BillSchedule
                            {
                                EventId = visit.Id,
                                EventDate = visit.EventDate,
                                VisitDate = visit.VisitDate,
                                Unit = unit,
                                Charge = unit * charge,
                                DisciplineTaskName = visit.DisciplineTaskName,
                                PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription,
                                StatusName = visit.StatusName,
                                HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code,
                                RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode,
                                Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty,
                                Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty,
                                Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty,
                                Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty,
                                IsExtraTime = true
                            };
                            schedules.Add(addedBillSchedule);
                        }
                    }
                }
            }
            else
            {
                schedule.Unit = (int)Math.Ceiling((double)visit.MinSpent / 60);
                schedule.Charge = schedule.Unit * rate.Charge;
                schedules.Add(schedule);
            }
            return schedules;
        }

        private List<T> BillCategoryVisits(BillVisitCategory category, List<T> visits)
        {
            var categoryVisits = new List<T>();
            if (visits.IsNotNullOrEmpty())
            {
                switch (category)
                {
                    case BillVisitCategory.Billable:
                        categoryVisits = visits.Where(v => v.IsBillable && (ScheduleStatusFactory.BillStatus().Contains(v.Status)) && !v.IsMissedVisit).ToList();
                        break;
                    case BillVisitCategory.NonBillable:
                        categoryVisits = visits.Where(v => !v.IsBillable && (ScheduleStatusFactory.BillStatus().Contains(v.Status)) && !v.IsMissedVisit).ToList();
                        break;
                    case BillVisitCategory.MissedVisit:
                        categoryVisits = visits.Where(v => v.IsMissedVisit).ToList();
                        break;
                }
            }
            return categoryVisits;
        }

        protected long GetNextClaimId(ClaimData claimData)
        {
            return baseBillingRepository.AddClaimData(claimData);
        }

        public bool IsManagedClaim(string type)
        {
            return type.IsNotNullOrEmpty() && (type.ToLowerCase().IsEqual("managedcare") || type == ((int)ClaimTypeSubCategory.ManagedCare).ToString());
        }


        public GridViewData GetPayorAndBranchFilterWithPermission(ParentPermission category, PermissionActions primaryAction, List<PermissionActions> actions)
        {
            var viewData = new GridViewData();
            viewData.Service = this.Service;
            var location = agencyRepository.GetBranchForSelection(Current.AgencyId, (int)this.Service);
            if (location != null)
            {
                viewData.Id = location.Id;
                var payorId = 0;
                if (AgencyServices.HomeHealth == this.Service)
                {
                    int.TryParse(location.IsLocationStandAlone ? location.Payor : Current.Payor, out payorId);
                    viewData.SubId = payorId;
                }
                else if (AgencyServices.PrivateDuty == this.Service)
                {
                    viewData.SubId = 18;
                }
            }
            actions = actions ?? new List<PermissionActions>();
            actions.Add(primaryAction);
            var allPermission = Current.CategoryService(Current.AcessibleServices, category, actions.Select(id => (int)id).ToArray());
            if (allPermission.IsNotNullOrEmpty())
            {
                actions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == primaryAction)
                    {
                        viewData.AvailableService = permission;
                    }
                    else if (a == PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }
                    else if (a == PermissionActions.Add)
                    {
                        viewData.NewPermissions = permission;
                    }
                    else if (a == PermissionActions.Edit)
                    {
                        viewData.EditPermissions = permission;
                    }
                    else if (a == PermissionActions.Delete)
                    {
                        viewData.DeletePermissions = permission;
                    }
                });
            }
            return viewData;
        }
    }
}
