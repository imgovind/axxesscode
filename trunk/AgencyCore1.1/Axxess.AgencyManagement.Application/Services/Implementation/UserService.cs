﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Web;

    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Enums;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Application;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.LookUp.Repositories;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Login = Axxess.Membership.Domain.Login;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web.Mvc;
    using SubSonic.Linq.Structure;

    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILoginRepository loginRepository;
        private readonly ILookupRepository lookupRepository;
       
        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider,ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
        }

        #region User



        public JsonViewData Add(User user, int maxAgencyUserAccount)
        {
            var viewData = new JsonViewData();
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= maxAgencyUserAccount)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User cannot be added because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
            }
            else if (!IsEmailAddressInUse(user.EmailAddress))
            {
                if (user.IsValid())
                {
                    user.AgencyId = Current.AgencyId;
                    user.AgencyName = Current.AgencyName;
                    viewData = CreateUser(user);
                    if (viewData.isSuccessful)
                    {
                        viewData.errorMessage = "User was saved successfully";
                    }
                    else
                    {
                        viewData.errorMessage = "Error in saving the new User.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = user.ValidationMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "This E-mail Address is already in use for your agency";
            }
            return viewData;
        }
        /// <summary>
        /// TODO:make user profile flat table
        /// </summary>
        /// <param name="user"></param>
        private void SetNewUserInfo(User user)
        {
            user.Id = Guid.NewGuid();
            user.Encode();
            user.Profile.Id = user.Id;
            user.Profile.AgencyId = user.AgencyId;
            if (user.AgencyRoleList.IsNotNullOrEmpty())
            {
                user.Roles = user.AgencyRoleList.ToArray().AddColons();
            }
            user.Status = (int)UserStatus.Active;
            //user.ProfileData = string.Empty;
            //user.Messages = new List<MessageState>().ToXml();
            //if (user.PermissionsArray.IsNotNullOrEmpty())
            //{
            //    user.Permissions = user.PermissionsArray.ToXml();
            //}

        }

        private JsonViewData CreateUser(User user)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            try
            {
                var isNewLogin = false;
                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                user.LoginId = login.Id;
                SetNewUserInfo(user);
                if (userRepository.AddModel(user) )
                {
                    user.Profile.EmailPersonal = user.EmailAddress;
                    if (userRepository.AddUserProfile(user.Profile))
                    {
                        if (loginRepository.AddLoginAgencyUser(Current.AgencyId, user.Id, login.Id))
                        {
                            if (isNewLogin)
                            {
                                ThreadPool.QueueUserWorkItem(state => SendNewUserNotifications(user));
                            }
                            else
                            {
                                ThreadPool.QueueUserWorkItem(state => SendExistingUserNotification(user));
                            }
                            // UserEngine.Refresh(Current.AgencyId);
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.Id = user.Id;
                        }
                        else
                        {
                            userRepository.Remove(Current.AgencyId, user.Id);
                            userRepository.RemoveProfile(Current.AgencyId, user.Id);
                            if (isNewLogin)
                            {
                                loginRepository.Remove(login.Id);
                            }
                            viewData.isSuccessful = false;
                        }
                    }
                    else
                    {
                        userRepository.Remove(Current.AgencyId, user.Id);
                        if (isNewLogin)
                        {
                            loginRepository.Remove(login.Id);
                        }
                        viewData.isSuccessful = false;
                    }
                }
                else
                {
                    if (isNewLogin)
                    {
                        loginRepository.Remove(login.Id);
                    }
                    viewData.isSuccessful = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                viewData.errorMessage = "Error in saving the new User.";
                return viewData;
            }
            return viewData;
        }

        private static void SendNewUserNotifications(User user)
        {
            string invitationSubject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
            const string WelcomeSubject = "Welcome to AgencyCore, rated the Most Recommended Home Health Software Solution";

            var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
            string invitationBodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, invitationSubject, invitationBodyText);

            Thread.Sleep(TimeSpan.FromMinutes(1));

            string welcomeBodyText = MessageBuilder.PrepareTextFrom("NewUserWelcome");
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, WelcomeSubject, welcomeBodyText);
        }

        private static void SendExistingUserNotification(User user)
        {
            string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
            string bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
        }

        public JsonViewData Activate(Guid userId, int maxAgencyUserAccount)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be activated. Try Again." };

            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= maxAgencyUserAccount)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User cannot be activated because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
            }
            else
            {
                if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Active))
                {
                    viewData = SetUserStatus(userId, (int)UserStatus.Active);
                    //Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                    //viewData.isSuccessful = true;
                    //viewData.errorMessage = "User has been activated successfully.";
                }
            }
            return viewData;
        }

        public JsonViewData SetUserStatus(Guid userId, int status)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            if (userRepository.SetUserStatus(Current.AgencyId, userId, status))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, status == (int)UserStatus.Inactive ? LogAction.UserDeactivated : LogAction.UserActivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deactivated successfully.";
            }
            return viewData;
        }

        public JsonViewData Update(User user)
        {
            var viewData = new JsonViewData();
            if (user.IsValid())
            {
                var editUser = userRepository.GetUserOnly(Current.AgencyId, user.Id);
                if (editUser != null)
                {
                    var editUserProfile = userRepository.GetUserProfileOnly(Current.AgencyId, user.Id);
                    if (editUserProfile != null)
                    {
                      
                        editUser.CustomId = user.CustomId;
                        editUser.AgencyLocationId = user.AgencyLocationId;
                        editUser.EmploymentType = user.EmploymentType;
                        if (user.AgencyRoleList != null && user.AgencyRoleList.Count > 0)
                        {
                            editUser.Roles = user.AgencyRoleList.ToArray().AddColons();
                        }
                        editUser.FirstName = user.FirstName;
                        editUser.LastName = user.LastName;
                        editUser.MiddleName = user.MiddleName;
                        editUser.Suffix = user.Suffix;
                        editUser.TitleType = user.TitleType;
                        editUser.TitleTypeOther = user.TitleTypeOther;
                        editUser.Credentials = user.Credentials;
                        editUser.CredentialsOther = user.CredentialsOther;
                        editUser.AccessibleServices = user.AccessibleServices;
                        editUser.PreferredService = user.PreferredService;
                        if (user.Profile != null)
                        {
                            editUserProfile.Gender = user.Profile.Gender;
                            editUserProfile.AddressLine1 = user.Profile.AddressLine1;
                            editUserProfile.AddressLine2 = user.Profile.AddressLine2;
                            editUserProfile.AddressCity = user.Profile.AddressCity;
                            editUserProfile.AddressZipCode = user.Profile.AddressZipCode;
                            editUserProfile.AddressStateCode = user.Profile.AddressStateCode;
                        }
                        if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
                        {
                            editUserProfile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                        }
                        if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
                        {
                            editUserProfile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                        }
                        if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0)
                        {
                            editUserProfile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                        }
                        editUser.AllowWeekendAccess = user.AllowWeekendAccess;
                        editUser.EarliestLoginTime = user.EarliestLoginTime;
                        editUser.AutomaticLogoutTime = user.AutomaticLogoutTime;
                        editUser.Comments = user.Comments;
                        editUser.DOB = user.DOB;
                        editUser.HireDate = user.HireDate;
                        editUser.SSN = user.SSN;

                        var userProfileDeepCopy = editUserProfile.DeepClone();
                        if (userRepository.UpdateUserProfile(editUserProfile) )
                        {
                            if (userRepository.UpdateModel(editUser, true))
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserEdited, string.Empty);
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User has been saved successfully";
                            }
                            else
                            {
                                userRepository.UpdateUserProfile(userProfileDeepCopy);
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "A problem has occured while saving the user.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "A problem has occured while saving the user.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "User could not be identified. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "User could not be identified. Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }
            return viewData;
        }

        public bool DeleteUser(Guid userId)
        {
            var user = userRepository.GetUserOnly(Current.AgencyId, userId);
            var result = false;
            if (user != null)
            {
                var accounts = userRepository.GetUsersByLoginId(user.LoginId);
                if (accounts != null)
                {
                    if (userRepository.Delete(Current.AgencyId, userId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserDeleted, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }

        public User GetUserOnly(Guid userId)
        {
            return userRepository.GetUserOnly(Current.AgencyId, userId);
        }

    
        public GeneralPermission GetUserPermissionsOnly(Guid userId)
        {
            var generalPermission = new GeneralPermission { UserId = userId };
            var user = userRepository.GetUserOnly(Current.AgencyId, userId);
            if (user != null)
            {
                generalPermission.AvailableServices = user.AccessibleServices;
                generalPermission.Permissions = FilterUserPermissions(user);
                bool isAllReportPermissionsSelected;
                generalPermission.ReportPermissions = FilterUserReportPermissions(user, out isAllReportPermissionsSelected);
                generalPermission.IsAllReportsSelected = isAllReportPermissionsSelected;
                generalPermission.ServiceLocations = userRepository.UserLocations(user.AgencyId, user.Id, (int)generalPermission.AvailableServices);

            }
            return generalPermission;
        }


        /// <summary>
        /// Filters the permissions of the site with what the user has access to and marks the ones the user has as checked
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<IGrouping<ParentPermissionCategory, IGrouping<ParentPermissionGroupKey, PermissionRow>>> FilterUserPermissions(User user)
        {
            //Deserialize the users permissions
            var userPermissions = user.NewPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>() ?? new Dictionary<int, Dictionary<int, List<int>>>();

            //Get the permission actions from the database
            var groupingOfPermissions = new List<IGrouping<ParentPermissionCategory, IGrouping<ParentPermissionGroupKey, PermissionRow>>>();
            var allPermissions = this.lookupRepository.GetPermissions();
            if (allPermissions != null)
            {
                //Group the permissions first by the category and then by the parent permission
                var groupedPermissons = from permission in allPermissions
                                        group permission by new ParentPermissionCategory(permission.Category, permission.IsSubCategory)
                                            into categoryGroup
                                            from parentGroup in
                                                (from permission in categoryGroup
                                                 //Uses AgencyServices.None as a representation of 8191
                                                 group permission by new ParentPermissionGroupKey(permission.Id, permission.HasServiceScope, user.AccessibleServices & (AgencyServices)permission.Service))
                                            group parentGroup by categoryGroup.Key;
                foreach (var categoryGroup in groupedPermissons)
                {
                    bool isAllSelected = true;

                    var permissionCategorizedList = new List<IGrouping<ParentPermissionGroupKey, PermissionRow>>();
                    foreach (var permissionGroup in categoryGroup)
                    {
                        var allOfServiceSelected = -1;
                        var permissionList = new List<PermissionRow>();
                        //The users permissions for the group
                        Dictionary<int, List<int>> userGroupPermissions;
                        userPermissions.TryGetValue(permissionGroup.Key.Id, out userGroupPermissions);
                       
                        foreach (var permission in permissionGroup)
                        {
                            if (permissionGroup.Key.Id == 1)
                            {
                                Console.WriteLine();
                            }
                            //If the permission has service scope but the user does not have access to any services belonging to the permission than the permission is not added
                            if (permission.HasServiceScope && (!user.AccessibleServices.Has((AgencyServices)permission.Service) && !((AgencyServices)permission.Service).Has(user.AccessibleServices)))
                            {
                                continue;
                            }
                            var checkedServices = -1;
                            //Check if the user has a permission value set for this permission
                            if (userGroupPermissions != null && userGroupPermissions.ContainsKey(permission.ChildId))
                            {
                                var serviceList = userGroupPermissions[permission.ChildId];
                                //If the user has services set for this permission and the value in the services is not 8191(represents all services) than combine them into a single value 
                                if (serviceList.IsNotNullOrEmpty())
                                {
                                    if (serviceList[0] != (int)AgencyServicesBoundary.All)
                                    {
                                        checkedServices = serviceList.CombineValues();
                                    }
                                    else
                                    {
                                        checkedServices = (int)AgencyServicesBoundary.All;
                                    }
                                }
                            }
                            //Uses bitwise operators to create a value that shows if all of the permissions for a certain service were selected
                            if (checkedServices != -1)
                            {
                                //If the permission has no service scope than the only value it will be using is AgencyServicesBoundary.All
                                if (!permission.HasServiceScope)
                                {
                                    //If the allOfServiceSelected has not yet been set 
                                    if (allOfServiceSelected == -1)
                                    {
                                        allOfServiceSelected = checkedServices;
                                    }
                                }
                                //Otherwise the permission has multiple all service checkboxes
                                else
                                {
                                    var possibleServices = ((int)user.AccessibleServices & permission.Service);
                                    //If the allOfServiceSelected has not yet been set, then it will be set to the first permission displayed for the group
                                    //and the checkbox for the main category is set to the value of the possible services for the permission equal to the services that are checked
                                    if (allOfServiceSelected == -1)
                                    {
                                        allOfServiceSelected = checkedServices;
                                        isAllSelected = possibleServices == checkedServices;
                                    }
                                    //If the possible services for this permission do not equal the services that are checked subtract
                                    //the unchecked services from all of the services that are checked
                                    else if(possibleServices != checkedServices)
                                    {
                                        allOfServiceSelected &= checkedServices;
                                        isAllSelected = false;
                                    }
                                }
                            }
                            else
                            {
                                allOfServiceSelected = 0;
                                isAllSelected = false;
                                checkedServices = 0;
                            }
                            permissionList.Add(new PermissionRow() { Description = permission.Name.IsNotNullOrEmpty() ? permission.Name : (Enum.IsDefined(typeof(PermissionActions), permission.ChildId) ? ((PermissionActions)permission.ChildId).GetDescription() : string.Empty), ToolTip = permission.Description, Checked =  checkedServices, ChildId = permission.ChildId, });
                        }


                        //Add the grouping of the permission actions under a Parent Permission into permissionCategorizedList
                        if (permissionList.IsNotNullOrEmpty())
                        {
                            permissionGroup.Key.IsAllSelected = permissionGroup.Key.AvailableForServices.All(s => (s & allOfServiceSelected) == s);

                            permissionGroup.Key.AllSelected = allOfServiceSelected != -1 ? allOfServiceSelected : (int)AgencyServices.None;
                            permissionCategorizedList.Add(new Grouping<ParentPermissionGroupKey, PermissionRow>(permissionGroup.Key, permissionList));
                        }
                    }
                    //Add the grouping of the parent permissions into the grouping of the permission category
                    if (permissionCategorizedList.IsNotNullOrEmpty())
                    {
                        categoryGroup.Key.IsAllSelected = isAllSelected;
                        groupingOfPermissions.Add(new Grouping<ParentPermissionCategory, IGrouping<ParentPermissionGroupKey, PermissionRow>>(categoryGroup.Key, permissionCategorizedList));
                    }
                }
                //Order the categories by name
                if (groupingOfPermissions.IsNotNullOrEmpty())
                {
                    groupingOfPermissions = groupingOfPermissions.OrderBy(o => o.Key.Name).ToList();
                }
            }
            return groupingOfPermissions;
        }


        /// <summary>
        /// Filters the report permissions of the site with what the user has access to and marks the ones the user has as checked
        /// </summary>
        /// <param name="user"></param>
        /// <param name="isAllReportPermissionsSelected"></param>
        /// <returns></returns>
        private List<IGrouping<ReportPermissionGroupKey, ReportPermissionRow>> FilterUserReportPermissions(User user, out bool isAllReportPermissionsSelected)
        {
            isAllReportPermissionsSelected = true;
            var reportPermissionsGroupedAndFiltered = new List<IGrouping<ReportPermissionGroupKey, ReportPermissionRow>>();
            var userReportPermissions = user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>() ?? new Dictionary<int, Dictionary<int, List<int>>>();
            //Gets the descriptions of the reports
            var reportPermissions = this.lookupRepository.GetReportDescriptions();
            if (reportPermissions.IsNotNullOrEmpty())
            {
                //Groups the reports by their category ids
                var reportPermissionsGrouped = reportPermissions.GroupBy(g => new ReportPermissionGroupKey(g.CategoryId, g.Category));
                foreach (var reportPermissionGroup in reportPermissionsGrouped)
                {
                    var reportPermissionsFiltered = new List<ReportPermissionRow>();
                    var servicesInCategory = AgencyServices.None;
                    var allViewPermissionsChecked = AgencyServices.None;
                    var allExportPermissionsChecked = AgencyServices.None;
                    foreach (var reportPermission in reportPermissionGroup)
                    {
                        //If the user does not have access to any services belonging to the report than the report is not added
                        if (!user.AccessibleServices.Has((AgencyServices)reportPermission.Service) && !((AgencyServices)reportPermission.Service).Has(user.AccessibleServices))
                        {
                            continue;
                        }
                        var servicesInPermission = (AgencyServices)reportPermission.Service & user.AccessibleServices;
                        //Records all of the services belonging to the current category of reports
                        servicesInCategory |= servicesInPermission;
                        List<int> viewPermissions = null;
                        List<int> exportPermissions = null;
                        //Gets the permissions of the user for the report
                        if (userReportPermissions.IsNotNullOrEmpty())
                        {
                            Dictionary<int, List<int>> generalCategoryPermissions;
                            userReportPermissions.TryGetValue(reportPermission.Id, out generalCategoryPermissions);
                            generalCategoryPermissions = generalCategoryPermissions ?? new Dictionary<int, List<int>>();
                            generalCategoryPermissions.TryGetValue((int)PermissionActions.ViewList, out viewPermissions);
                            generalCategoryPermissions.TryGetValue((int)PermissionActions.Export, out exportPermissions);
                        }
                        //Combines the users permission to view this report into a single agencyservice value
                        var viewChecked = (AgencyServices)viewPermissions.CombineValues();
                        //If the permission's service limited by the services of the user are equal to the checked services than add it to the allOfServiceSelected 
                        if (viewChecked == servicesInPermission)
                        {
                            allViewPermissionsChecked |= viewChecked;
                        }
                        //Otherwise the unchecked service is removed from allViewPermissionsChecked
                        else
                        {
                            allViewPermissionsChecked &= viewChecked;
                            isAllReportPermissionsSelected = false;
                        }
                        //Combines the users permission to export this report into a single agencyservice value
                        var exportChecked = (AgencyServices)exportPermissions.CombineValues();
                        //If the permission's service limited by the services of the user are equal to the checked services than add it to the allOfServiceSelected 
                        if (exportChecked == servicesInPermission)
                        {
                            allExportPermissionsChecked |= exportChecked;
                        }
                        //Otherwise the unchecked service is removed from allViewPermissionsChecked
                        else
                        {
                            allExportPermissionsChecked &= exportChecked;
                            isAllReportPermissionsSelected = false;
                        }
                        reportPermissionsFiltered.Add(new ReportPermissionRow() { Id = reportPermission.Id, Description = reportPermission.Title, AvailableForServices = (AgencyServices)reportPermission.Service & user.AccessibleServices, ToolTip = reportPermission.Description, ViewChecked = viewChecked, ExportChecked = exportChecked });
                    }
                    //If any reports for this category can be viewed by the user (reportPermissionsFiltered is not empty)
                    if (reportPermissionsFiltered.IsNotNullOrEmpty())
                    {
                        reportPermissionGroup.Key.ServicesInCategory = servicesInCategory;
                        reportPermissionGroup.Key.AllViewSelected = allViewPermissionsChecked;
                        reportPermissionGroup.Key.AllExportSelected = allExportPermissionsChecked;
                        reportPermissionsGroupedAndFiltered.Add(new Grouping<ReportPermissionGroupKey, ReportPermissionRow>(reportPermissionGroup.Key, reportPermissionsFiltered));
                    }
                }
            }
            return reportPermissionsGroupedAndFiltered;
        }


        //public ReportPermission GetUserReportPermissionsOnly(Guid userId)
        //{
            
        //    var permission = new ReportPermission { UserId = userId, Permissions = new Dictionary<int, Dictionary<int, List<int>>>() };
        //    var user = userRepository.GetUserOnly(Current.AgencyId, userId);
        //    if (user != null)
        //    {
        //        permission.AvailableServices = user.AccessibleServices;
        //        permission.Permissions = user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>();
        //    }

        //    return permission;
        //}


        //public User GetUserWithPermissions(Guid userId)
        //{
        //    var user = userRepository.GetUserOnly(userId, Current.AgencyId);
        //    if (user != null)
        //    {
        //        if (user.Permissions.IsNotNullOrEmpty())
        //        {
        //            user.PermissionsArray = user.Permissions.ToObject<List<string>>();
        //        }
        //    }
        //    else
        //    {
        //        user = new User();
        //    }
        //    return user;
        //}

        public User GetUserWithProfilePermissionsAndLicenses(Guid userId)
        {
            var user = userRepository.GetUserOnly(Current.AgencyId, userId);

            if (user != null)
            {
                var editUserProfile = userRepository.GetUserProfileOnly(Current.AgencyId, user.Id);
                if (editUserProfile != null)
                {
                    user.Profile = editUserProfile;
                }
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
                else
                {
                    user.PermissionsArray = new List<string>();
                }

                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>();
                }
                else
                {
                    user.LicensesArray = new List<License>();
                }
            }
            return user;
        }

        public User GetUserWithProfileOnly(Guid userId)
        {
            var user = userRepository.GetUserOnly(Current.AgencyId, userId);
            if (user != null)
            {
                var userProfile = userRepository.GetUserProfileOnly(Current.AgencyId, userId);
                if (userProfile != null)
                {
                    user.Profile = userProfile;
                }
                else
                {
                    user.Profile = new UserProfile();
                }
                user.ProfileData = string.Empty;
                user.Permissions = string.Empty;
                user.Licenses = string.Empty;
                //user.Rates = string.Empty;
                //SendNewUserNotifications(user);

                user.IsUserCanViewLog = Current.HasRight(Current.AcessibleServices, ParentPermission.User, PermissionActions.ViewLog);
            }
            return user;
        }

        public UserLicense GetUserLicensesOnly(Guid userId)
        {
            return new UserLicense { UserId = userId, Licenses = this.userRepository.GetLicenses(Current.AgencyId, userId).ToList() };
        }

        public List<License> GetUserLicenses(Guid userId)
        {
            return this.userRepository.GetLicenses(Current.AgencyId, userId).ToList();
        }

        //public IList<User> GetAgencyUsers()
        //{
        //    var users = userRepository.GetAgencyUsersOnly(Current.AgencyId);
        //    if (users != null && users.Count > 0)
        //    {
        //        users.ForEach(user =>
        //        {
        //            if (user.ProfileData.IsNotNullOrEmpty())
        //            {
        //                user.Profile = user.ProfileData.ToObject<UserProfile>();
        //            }

        //            if (user.Permissions.IsNotNullOrEmpty())
        //            {
        //                user.PermissionsArray = user.Permissions.ToObject<List<string>>();
        //            }
        //        });
        //        users = users.OrderBy(u => u.FirstName).ToList();
        //    }
        //    return users;
        //}

        public UserListViewData<User> GetUsersListViewData(Guid branchId, int status)
        {
            var viewData = new UserListViewData<User> { Users = userRepository.GetUsersByStatus(Current.AgencyId,branchId, status) };
            var userPermissions = Current.Permissions.GetCategoryPermission(ParentPermission.User);
            if (userPermissions.IsNotNullOrEmpty())
            {
                var IsFrozen = Current.IsAgencyFrozen;
                var acessibleServices = Current.AcessibleServices;
                viewData.IsUserCanEdit = !IsFrozen && userPermissions.IsInPermission(acessibleServices, PermissionActions.Edit);
                viewData.IsUserCanDelete = !IsFrozen && userPermissions.IsInPermission(acessibleServices, PermissionActions.Delete);
                viewData.IsUserCanActivate = !IsFrozen && userPermissions.IsInPermission(acessibleServices, PermissionActions.Reactivate) && status == (int)UserStatus.Inactive;
                viewData.IsUserCanDeactivate = !IsFrozen && userPermissions.IsInPermission(acessibleServices, PermissionActions.Deactivate) && status == (int)UserStatus.Active;
                viewData.IsUserCanAdd = !IsFrozen && userPermissions.IsInPermission(acessibleServices, PermissionActions.Add);
                viewData.IsUserCanExport = userPermissions.IsInPermission(acessibleServices, PermissionActions.Export);
                viewData.IsUserCanSeeStickyNote = userPermissions.IsInPermission(acessibleServices, PermissionActions.ViewStickyNotes);
            }
            return viewData;
        }
       
        public IList<User> GetUsersByStatus(Guid branchId, int status)
        {
            return userRepository.GetUsersByStatus(Current.AgencyId, branchId, status);;
        }

        public List<User> GetAllUsers(Guid branchId, int statusId)
        {
            return userRepository.GetAllUsers(Current.AgencyId, branchId, statusId);
        }

        public bool UpdateProfile(User user)
        {
            var result = false;
            if (user != null)
            {
                user.AgencyId = Current.AgencyId;
                user.Profile.Id = user.Id;
                user.Profile.AgencyId = user.AgencyId;
                user.Profile.HomePhoneArray = user.HomePhoneArray;
                user.Profile.MobilePhoneArray = user.MobilePhoneArray;
                if (userRepository.UpdateProfile(user.Profile))
                {
                   
                    if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                    {
                        User userInfo = userRepository.GetUserOnly(Current.AgencyId, user.Id);
                       
                        if (userInfo != null)
                        {
                            Login login = loginRepository.Find(userInfo.LoginId);
                            if (login != null)
                            {
                                if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                                {
                                    string passwordsalt;
                                    string passwordHash;

                                    var saltedHash = new SaltedHash();
                                    saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                                    login.PasswordSalt = passwordsalt;
                                    login.PasswordHash = passwordHash;
                                }
                                if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                                {
                                    string signaturesalt;
                                    string signatureHash;

                                    var saltedHash = new SaltedHash();
                                    saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                                    login.SignatureSalt = signaturesalt;
                                    login.SignatureHash = signatureHash;
                                }
                                if (loginRepository.Update(login))
                                {
                                    result = true;
                                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserProfileUpdated, string.Empty);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool IsAgencyHaveMoreAccount(int maxAgencyUserAccount)
        {
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            return userCount >= maxAgencyUserAccount;
        }

        public List<Recipient> GetMessageRecipientList(string searchTerm)
        {
            var recipients = new List<Recipient>();
            var query = userRepository.GetAgencyUsers(Current.AgencyId, searchTerm);
            query.ForEach(e => recipients.Add(new Recipient { id = e.Id.ToString(), name = string.Concat(e.LastName, ", ", e.FirstName) }));
            return recipients;
        }

        #endregion

        #region User Validate

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
                var users = userRepository.GetUsersByLoginId(Current.AgencyId, login.Id);
                if (users.Count > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.GetUserOnly(Current.AgencyId, userId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(Guid userId, string signature)
        {
            if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
            {
                var user = userRepository.GetUserOnly(Current.AgencyId, userId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        #endregion

        #region NonSoftware User

        public JsonViewData DeleteNonUser(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "nonUserId");
            var viewData = new JsonViewData(false, "Non-Software User could not be deleted. Try Again.");
            if (userRepository.DeleteNonUser(id, Current.AgencyId))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.NonUser, LogAction.NonUserDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Non-Software User has been deleted successfully.";
            }
            return viewData;
        }

        public JsonViewData AddNonUser(NonUser nonUser)
        {
            Check.Argument.IsNotNull(nonUser, "nonUser");
            var viewData = new JsonViewData(false, "Non-Software User could not be deleted. Try Again.");
            if (nonUser.IsValid())
            {
                nonUser.AgencyId = Current.AgencyId;
                if (userRepository.AddNonUser(nonUser))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, nonUser.Id.ToString(), LogType.NonUser, LogAction.NonUserAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Non-Software User has been added successfully.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = nonUser.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData UpdateNonUser(NonUser nonUser)
        {
            Check.Argument.IsNotNull(nonUser, "nonUser");
            var viewData = new JsonViewData(false, "Non-Software User could not be saved. Try Again.");
            if (nonUser.IsValid())
            {
                nonUser.AgencyId = Current.AgencyId;
                if (userRepository.UpdateNonUser(nonUser))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, nonUser.Id.ToString(), LogType.NonUser, LogAction.NonUserEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Non-Software User has been saved successfully";
                }
            }
            else
            {
                viewData.errorMessage = nonUser.ValidationMessage;
            }
            return viewData;
        }

        #endregion

        #region License

        public JsonViewData AddLicense(License license, HttpPostedFileBase httpFile)
        {
            var viewData = ValidateLicense(license);
            if (viewData.isSuccessful)
            {
                if (httpFile!= null)
                {
                    Guid assetId;
                    var isAssetSaved = AssetHelper.AddAsset(httpFile, out assetId);
                    if (isAssetSaved)
                    { license.AssetId = assetId;}
                }
                

                    license.AgencyId = Current.AgencyId;
                    license.Id = Guid.NewGuid();
                    license.Created = DateTime.Now;
                    license.Modified = license.Created;
                    if (license.OtherLicenseType.IsNotNullOrEmpty())
                    {
                        license.LicenseType = license.OtherLicenseType;
                    }
                    if (userRepository.AddLicense(license))
                    {
                        if (license.IsNonUser)
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, license.UserId.ToString(), LogType.NonUser, LogAction.NonUserLicenseAdded, string.Empty);
                        }
                        else
                        {
                            viewData.Id = license.UserId;
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, license.UserId.ToString(), LogType.User, LogAction.UserLicenseAdded, string.Empty);
                        }
                        viewData.errorMessage = "The License has been added successfully.";
                        viewData.isSuccessful = true;
                    }
                    else
                    {
                        viewData.errorMessage = "A problem occured while adding the license. Try again.";
                        viewData.isSuccessful = false;
                    } 
            }
            else
            {
                viewData.errorMessage = "A problem occured while uploading the asset. Try again.";
                viewData.isSuccessful = false;
            }
            return viewData;
        }

        public JsonViewData UpdateLicense(License licenseItem)
        {
            const string DefaultErrorMessage = "The License could not be updated. Try again.";
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = DefaultErrorMessage };
            if (licenseItem != null)
            {
                viewData = ValidateLicense(licenseItem);
                if (viewData.isSuccessful)
                {
                    licenseItem.AgencyId = Current.AgencyId;
                    if (licenseItem.OtherLicenseType.IsNotNullOrEmpty() && licenseItem.LicenseType == LicenseTypes.Other.ToString())
                    {
                        licenseItem.LicenseType = licenseItem.OtherLicenseType;
                    }
                    if (userRepository.UpdateLicense(licenseItem))
                    {
                        if (licenseItem.IsNonUser)
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.UserId.ToString(), LogType.NonUser, LogAction.NonUserLicenseUpdated, string.Empty);
                        }
                        else
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.UserId.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        }
                        viewData.Id = licenseItem.UserId;
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The License has been updated successfully.";
                    }
                    else
                    {
                        viewData.errorMessage = DefaultErrorMessage;
                        viewData.isSuccessful = false;
                    }
                }
            }
            return viewData;
        }

        public bool DeleteLicense(Guid id, Guid userId)
        {
            var license = userRepository.GetLicense(id, userId, Current.AgencyId);
            if (license != null)
            {
                if (!license.AssetId.IsEmpty())
                {
                    AssetHelper.Delete(license.AssetId);
                }
                if (userRepository.DeleteLicense(id, userId, Current.AgencyId))
                {
                    if (license.IsNonUser)
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.NonUser, LogAction.NonUserLicenseDeleted, string.Empty);
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserLicenseDeleted, string.Empty);
                    }
                    return true;
                }
                // Restoring the asset, if any exists, since the license failed to be deleted.
                if (!license.AssetId.IsEmpty())
                {
                    AssetHelper.Restore(license.AssetId);
                }
            }
            return false;
        }

        public IList<License> GetLicenses(Guid branchId, int status, bool appendCustomIdToDisplayName)
        {
            var licenses = userRepository.GetLicensesByBranchAndStatus(Current.AgencyId, branchId, status) ?? new List<License>();
            if (licenses.IsNotNullOrEmpty() && appendCustomIdToDisplayName)
            {
                licenses.ForEach(license => license.DisplayName += license.CustomId.IsNullOrEmpty() ? string.Empty : "_" + license.CustomId);
            }
            return licenses;
        }

        public IList<License> GetLicenses(Guid userId, bool isAssetNeeded)
        {
            return userRepository.GetLicenses(Current.AgencyId, userId) ?? new List<License>();
        }

        #endregion

        #region License Item

        public License EditLicenseItem(Guid licenseId, Guid userId)
        {
            return userRepository.GetLicense(licenseId, userId, Current.AgencyId);
        }

        public IList<License> GetLicenses()
        {
            var licenses = userRepository.GetLicenses(Current.AgencyId);
            return licenses.IsNotNullOrEmpty() ? licenses.OrderBy(l => l.DisplayName).ToList() : new List<License>();
        }

        protected JsonViewData ValidateLicense(License license)
        {
            var validations = new List<Validation>();
            validations.Add(new Validation(() => license.IsNonUser && license.UserId.IsEmpty(), "Non-Software User is required. "));
            validations.Add(new Validation(() => license.IssueDate.Date > license.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date."));
            var validator = new EntityValidator(validations.ToArray());
            validator.Validate();
            if (!validator.IsValid)
            {
                return new JsonViewData(false, validator.Message);
            }
            return new JsonViewData(true);
        }

        #endregion

        #region Permissions

        //public bool UpdatePermissions(FormCollection formCollection)
        //{
        //    var result = false;
        //    try
        //    {
        //        var userId = formCollection["UserId"] != null ? formCollection["UserId"].ToGuid() : Guid.Empty;
        //        var permissionArray = formCollection["PermissionsArray"] != null ? formCollection["PermissionsArray"].ToArray().ToList() : null;
        //        if (!userId.IsEmpty() && permissionArray != null && permissionArray.Count > 0)
        //        {
        //            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
        //            if (user != null)
        //            {
        //                user.PermissionsArray = permissionArray;
        //                if (user.PermissionsArray != null && user.PermissionsArray.Count > 0)
        //                {
        //                    user.Permissions = user.PermissionsArray.ToXml();
        //                }
        //                if (userRepository.UpdateModel(user, false))
        //                {
        //                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
        //                    result = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return result;
        //    }
        //    return result;
        //}

        //public bool UpdatePermissions(Guid userId, List<UserPermissionInput> userPermissions)
        //{
        //    var result = false;
        //    //try
        //    //{
        //    //    if (!userId.IsEmpty() )
        //    //    {
        //    //        var user = userRepository.GetUserOnly(userId, Current.AgencyId);
        //    //        if (user != null)
        //    //        {
        //    //            var homeHealthPermissions = userPermissions.FirstOrDefault(p => p.Service == (int)AgencyServices.HomeHealth);
        //    //            if (homeHealthPermissions != null)
        //    //            {
        //    //                long seralizedPermission = 0;
        //    //                homeHealthPermissions.Permissions.ForEach(hp =>
        //    //                {
        //    //                    seralizedPermission |= hp;
        //    //                });
        //    //                user.HomeHealthPermission =seralizedPermission;
        //    //            }
        //    //            var privateDutyPermissions = userPermissions.FirstOrDefault(p => p.Service == (int)AgencyServices.PrivateDuty);
        //    //            if (privateDutyPermissions != null)
        //    //            {
        //    //                long seralizedPermission = 0;
        //    //                privateDutyPermissions.Permissions.ForEach(hp =>
        //    //                {
        //    //                    seralizedPermission |= hp;
        //    //                });
        //    //                user.PrivateDutyPermission = seralizedPermission;
        //    //            }
        //    //            if (userRepository.UpdateModel(user, false))
        //    //            {
        //    //                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
        //    //                result = true;
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    //catch (Exception)
        //    //{
        //    //    return result;
        //    //}
        //    return result;
        //}

        //public bool UpdatePermissions(Guid userId, List<ServiceUserPermissionInput> servicePermissions)
        //{
        //    var result = false;
        //    try
        //    {
        //        if (servicePermissions.IsNotNullOrEmpty())
        //        {
        //            if (!userId.IsEmpty())
        //            {
        //                var user = userRepository.GetUserOnly(userId, Current.AgencyId);
        //                if (user != null)
        //                {
        //                    var homeHealthPermissions = servicePermissions.FirstOrDefault(p => p.Service == (int)AgencyServices.HomeHealth);
        //                    if (homeHealthPermissions != null)
        //                    {
        //                        user.HomeHealth = PermissionHelper(homeHealthPermissions).ToJson();
        //                    }

        //                    var privateDutyPermissions = servicePermissions.FirstOrDefault(p => p.Service == (int)AgencyServices.PrivateDuty);
        //                    if (privateDutyPermissions != null)
        //                    {
        //                        user.PrivateDuty = PermissionHelper(privateDutyPermissions).ToJson();
        //                    }
        //                    if (userRepository.UpdateModel(user, false))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
        //                        result = true;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return result;
        //    }
        //    return result;
        //}


        //public bool UpdatePermissions(Guid userId, IDictionary<int, IDictionary<int, List<int>>> permissions)
        //{
        //    var result = false;
        //    try
        //    {
        //        if (!userId.IsEmpty())
        //        {
        //            if (permissions.IsNotNullOrEmpty())
        //            {
        //                //var formattedPermissions = new Dictionary<int, Dictionary<int, int>>();
        //                //permissions.ForEach((key, value) =>
        //                //{
        //                //    bool IsAdded = false;
        //                //    if (value.IsNotNullOrEmpty())
        //                //    {
        //                //        var actionData = new Dictionary<int, int>();
        //                //        value.ForEach((iKey, iValue) =>
        //                //        {
        //                //            if (iValue.IsNotNullOrEmpty())
        //                //            {
        //                //                var service = 0;
        //                //                iValue.ForEach(v =>
        //                //                    {
        //                //                        service |= v;
        //                //                    });
        //                //                IsAdded = true;
        //                //                actionData.Add(iKey, service);
        //                //            }
        //                //        });
        //                //        if (IsAdded)
        //                //        {
        //                //            formattedPermissions.Add(key, actionData);
        //                //        }
        //                //    }

        //                //});
        //                var user = userRepository.GetUserOnly(userId, Current.AgencyId);
        //                if (user != null)
        //                {
        //                    user.NewPermissions = permissions.ToJson();
        //                    if (userRepository.UpdateModel(user, false))
        //                    {
        //                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
        //                        result = true;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return result;
        //    }
        //    return result;
        //}

        public bool UpdatePermissions(UserPermissionInput permissionInputs)
        {
            var result = false;
            try
            {
                if (!permissionInputs.UserId.IsEmpty())
                {

                    var user = userRepository.GetUserOnly(Current.AgencyId, permissionInputs.UserId);
                    if (user != null)
                    {
                        if (permissionInputs.Permissions.IsNotNullOrEmpty())
                        {
                            var updatedPermssions = UpdatePermissions(permissionInputs.Services, permissionInputs.Permissions, user.NewPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>() ?? new Dictionary<int, Dictionary<int, List<int>>>());
                            user.NewPermissions = updatedPermssions.ToJson();
                        }
                        if (permissionInputs.ReportPermissions.IsNotNullOrEmpty())
                        {
                            bool isReportCenterVisbile;
                            var updatedPermssions = UpdateReportPermissions(permissionInputs.Services, permissionInputs.ReportPermissions, user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>() ?? new Dictionary<int, Dictionary<int, List<int>>>(), out isReportCenterVisbile);
                            user.ReportPermissions = updatedPermssions.ToJson();
                            user.IsReportCenterVisbile = isReportCenterVisbile;
                        }

                        if (permissionInputs.AllLocationServices.IsNotNullOrEmpty())
                        {
                            user.AllLocationAccessServices = (AgencyServices)permissionInputs.AllLocationServices.Aggregate(0, (acc, curr) => acc | curr);
                        }
                        else
                        {
                            user.AllLocationAccessServices = AgencyServices.None;
                        }

                        var exisitingToUpdate = new List<UserLocation>();
                        var locationToBeAdded = new List<UserLocation>();
                        var userExisitingLocations = userRepository.UserLocationsOnly(Current.AgencyId, user.Id);
                        if (userExisitingLocations.IsNotNullOrEmpty())
                        {
                            userExisitingLocations.ForEach(el =>
                            {
                                var comingLocation = permissionInputs.ServiceLocations.FirstOrDefault(cl => cl.Location == el.LocationId);
                                if (comingLocation != null)
                                {
                                    var service = (AgencyServices)comingLocation.Services.Aggregate(0, (acc, curr) => acc | curr);
                                    if (service != el.Services)
                                    {
                                        el.Services = service;
                                        exisitingToUpdate.Add(el);
                                    }
                                    permissionInputs.ServiceLocations.RemoveAll(s => s.Location == el.LocationId);
                                }
                                else
                                {
                                    el.Services = AgencyServices.None;
                                    exisitingToUpdate.Add(el);
                                }
                            });
                        }
                        if (permissionInputs.ServiceLocations.IsNotNullOrEmpty())
                        {
                            locationToBeAdded = permissionInputs.ServiceLocations.Select(s => new UserLocation { AgencyId = user.AgencyId, UserId = user.Id, LocationId = s.Location, Services = (AgencyServices)s.Services.Aggregate(0, (acc, curr) => acc | curr) }).ToList();
                        }

                        if (exisitingToUpdate.Count == 0 || (exisitingToUpdate.Count > 0 && userRepository.UpdateMultipleUserLocations(exisitingToUpdate)))
                        {
                            if (locationToBeAdded.Count == 0 || (locationToBeAdded.Count > 0 && userRepository.AddMultipleUserLocations(locationToBeAdded)))
                            {
                                if (userRepository.UpdateModel(user, false))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    if (exisitingToUpdate.Count > 0)
                                    {
                                        userRepository.UpdateMultipleUserLocations(userExisitingLocations);
                                    }
                                    if (locationToBeAdded.Count > 0)
                                    {
                                        userRepository.RemoveMultipleUserLocations(user.AgencyId, user.Id, locationToBeAdded.Select(l => l.LocationId).ToList());
                                    }
                                }
                            }
                            else
                            {
                                if (exisitingToUpdate.Count > 0)
                                {
                                    userRepository.UpdateMultipleUserLocations(userExisitingLocations);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception Ex)
            {
                return result;
            }
            return result;
        }

       //public bool UpdatePermissions(UserPermissionInput permissionInputs)//(Guid userId, int[] editableServices, IDictionary<int, Dictionary<int, List<int>>> permissions, List<UserServiceLocation> serviceLocations)
       // {
       //     var result = false;
       //     try
       //     {
       //         var user = userRepository.GetUserOnly(Current.AgencyId, permissionInputs.UserId);
       //         if (user != null)
       //         {
       //             if (permissionInputs.Permissions.IsNotNullOrEmpty())
       //             {
       //                 var nonServiceScopeCategory = lookupRepository.GetPermissionCategoryId(false) ?? new List<int>();
       //                 var exsistingPermissions = user.NewPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>() ?? new Dictionary<int, Dictionary<int, List<int>>>();
       //                 var availableCategory = new List<int>();
       //                 var exisitingCategory = exsistingPermissions.Keys.ToArray();
       //                 if (exisitingCategory.IsNotNullOrEmpty())
       //                 {
       //                     availableCategory.AddRange(exisitingCategory);
       //                 }
       //                 var newCategory = permissionInputs.Permissions.Keys.ToArray();
       //                 if (newCategory.IsNotNullOrEmpty())
       //                 {
       //                     availableCategory.AddRange(newCategory);
       //                 }
       //                 availableCategory = availableCategory.Distinct().ToList();
       //                 var allService = (int)AgencyServicesBoundary.All;
       //                 if (availableCategory.IsNotNullOrEmpty())
       //                 {
       //                     availableCategory.ForEach(c =>
       //                     {
       //                         var isNonServiceScope = nonServiceScopeCategory.Contains(c);

       //                         Dictionary<int, List<int>> comingActions = null;
       //                         var newCategoryExist = permissionInputs.Permissions.TryGetValue(c, out comingActions);

       //                         Dictionary<int, List<int>> exsistingActions = null;
       //                         var exsistingCategoryExist = exsistingPermissions.TryGetValue(c, out exsistingActions);
       //                         exsistingActions = exsistingActions ?? new Dictionary<int, List<int>>();

       //                         if (comingActions.IsNotNullOrEmpty())
       //                         {
       //                             var mergedActions = new List<int>();
       //                             var exisitingActionsLean = exsistingActions.Keys.ToArray();
       //                             if (exisitingActionsLean.IsNotNullOrEmpty())
       //                             {
       //                                 mergedActions.AddRange(exisitingActionsLean);
       //                             }
       //                             var newActions = comingActions.Keys.ToArray();
       //                             if (newActions.IsNotNullOrEmpty())
       //                             {
       //                                 mergedActions.AddRange(newActions);
       //                             }

       //                             mergedActions = mergedActions.Distinct().ToList();

       //                             // var mergedActions = comingActions.Select(a => a.Key).Union(exsistingActions.Select(e => e.Key)).Distinct();
       //                             if (mergedActions.IsNotNullOrEmpty())
       //                             {
       //                                 mergedActions.ForEach(a =>
       //                                 {
       //                                     List<int> newServices;
       //                                     var newActionExist = comingActions.TryGetValue(a, out newServices);

       //                                     List<int> oldServices;
       //                                     var exsistingActionExist = exsistingActions.TryGetValue(a, out oldServices);
       //                                     oldServices = oldServices ?? new List<int>();

       //                                     if (newServices.IsNotNullOrEmpty())
       //                                     {
       //                                         if (isNonServiceScope)
       //                                         {
       //                                             if (newServices.Contains(allService))
       //                                             {
       //                                                 if (!oldServices.Contains(allService))
       //                                                 {
       //                                                     oldServices.Add(allService);
       //                                                 }
       //                                             }
       //                                             else
       //                                             {
       //                                                 oldServices.Remove(allService);
       //                                             }
       //                                         }
       //                                         else
       //                                         {
       //                                             permissionInputs.Services.ForEach(s =>
       //                                             {
       //                                                 if (newServices.Contains(s))
       //                                                 {
       //                                                     if (oldServices.Contains(s))
       //                                                     {
       //                                                     }
       //                                                     else
       //                                                     {
       //                                                         oldServices.Add(s);
       //                                                     }
       //                                                 }
       //                                                 else
       //                                                 {
       //                                                     oldServices.Remove(s);
       //                                                 }
       //                                                 //this helps to clear non service flag
       //                                                 oldServices.Remove(allService);
       //                                             });
       //                                         }


       //                                         if (exsistingActionExist)
       //                                         {
       //                                             exsistingActions[a] = oldServices;
       //                                         }
       //                                         else
       //                                         {
       //                                             exsistingActions.Add(a, oldServices);
       //                                         }
       //                                     }
       //                                     else
       //                                     {
       //                                         if (exsistingActionExist)
       //                                         {
       //                                             if (isNonServiceScope)
       //                                             {
       //                                                 oldServices.Remove(allService);
       //                                             }
       //                                             else
       //                                             {
       //                                                 permissionInputs.Services.ForEach(s =>
       //                                                 {
       //                                                     oldServices.Remove(s);
       //                                                 });
       //                                             }
       //                                             exsistingActions[a] = oldServices;
       //                                         }

       //                                     }

       //                                 });
       //                                 if (exsistingCategoryExist)
       //                                 {
       //                                     exsistingPermissions[c] = exsistingActions;
       //                                 }
       //                                 else
       //                                 {
       //                                     exsistingPermissions.Add(c, exsistingActions);
       //                                 }
       //                             }

       //                         }
       //                         else
       //                         {
       //                             if (exsistingCategoryExist)
       //                             {
       //                                 var actions = exsistingActions.Keys.Distinct();
       //                                 if (exsistingActions.IsNotNullOrEmpty())
       //                                 {
       //                                     exsistingActions.ForEach((key, value) =>
       //                                     {
       //                                         var oldServices = value;
       //                                         if (isNonServiceScope)
       //                                         {
       //                                             oldServices.Remove(allService);
       //                                         }
       //                                         else
       //                                         {
       //                                             permissionInputs.Services.ForEach(s =>
       //                                             {
       //                                                 oldServices.Remove(s);
       //                                             });
       //                                         }
       //                                         value = oldServices;

       //                                     });
       //                                 }
       //                             }
       //                         }

       //                     });
       //                 }
       //                 user.NewPermissions = exsistingPermissions.ToJson();
       //             }
       //             if (permissionInputs.AllLocationServices.IsNotNullOrEmpty())
       //             {
       //                 user.AllLocationAccessServices = (AgencyServices)permissionInputs.AllLocationServices.Aggregate(0, (acc, curr) => acc | curr);
       //             }
       //             else
       //             {
       //                 user.AllLocationAccessServices = AgencyServices.None;
       //             }

       //             var exisitingToUpdate = new List<UserLocation>();
       //             var locationToBeAdded = new List<UserLocation>();
       //             var userExisitingLocations = userRepository.UserLocationsOnly(Current.AgencyId, user.Id);
       //             if (userExisitingLocations.IsNotNullOrEmpty())
       //             {
       //                 userExisitingLocations.ForEach(el =>
       //                 {
       //                     var comingLocation = permissionInputs.ServiceLocations.FirstOrDefault(cl => cl.Location == el.LocationId);
       //                     if (comingLocation != null)
       //                     {
       //                         var service = (AgencyServices)comingLocation.Services.Aggregate(0, (acc, curr) => acc | curr);
       //                         if (service != el.Services)
       //                         {
       //                             el.Services = service;
       //                             exisitingToUpdate.Add(el);
       //                         }
       //                         permissionInputs.ServiceLocations.RemoveAll(s => s.Location == el.LocationId);
       //                     }
       //                     else
       //                     {
       //                         el.Services = AgencyServices.None;
       //                         exisitingToUpdate.Add(el);
       //                     }
       //                 });
       //             }
       //             if (permissionInputs.ServiceLocations.IsNotNullOrEmpty())
       //             {
       //                 locationToBeAdded = permissionInputs.ServiceLocations.Select(s => new UserLocation { AgencyId = user.AgencyId, UserId = user.Id, LocationId = s.Location, Services = (AgencyServices)s.Services.Aggregate(0, (acc, curr) => acc | curr) }).ToList();
       //             }

       //             if (exisitingToUpdate.Count == 0 || (exisitingToUpdate.Count > 0 && userRepository.UpdateMultipleUserLocations(exisitingToUpdate)))
       //             {
       //                 if (locationToBeAdded.Count == 0 || (locationToBeAdded.Count > 0 && userRepository.AddMultipleUserLocations(locationToBeAdded)))
       //                 {
       //                     if (userRepository.UpdateModel(user, false))
       //                     {
       //                         Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
       //                         result = true;
       //                     }
       //                     else
       //                     {
       //                         if (exisitingToUpdate.Count > 0)
       //                         {
       //                             userRepository.UpdateMultipleUserLocations(userExisitingLocations);
       //                         }
       //                         if (locationToBeAdded.Count > 0)
       //                         {
       //                             userRepository.RemoveMultipleUserLocations(user.AgencyId,user.Id, locationToBeAdded.Select(l=>l.LocationId).ToList());
       //                         }
       //                     }
       //                 }
       //                 else
       //                 {
       //                     if (exisitingToUpdate.Count > 0)
       //                     {
       //                         userRepository.UpdateMultipleUserLocations(userExisitingLocations);
       //                     }
       //                 }
       //             }
       //         }
       //     }
       //     catch (Exception Ex)
       //     {
       //         return result;
       //     }
       //     return result;
       // }
         
        private Dictionary<int, Dictionary<int, List<int>>> UpdatePermissions(List<int> editableServices, IDictionary<int, Dictionary<int, List<int>>> permissions, Dictionary<int, Dictionary<int, List<int>>> exsistingPermissions)
        {
            var nonServiceScopeCategory = this.lookupRepository.GetPermissionCategoryId(false) ?? new List<int>();
            var availableCategory = new List<int>();
            var exisitingCategory = exsistingPermissions.Keys.ToArray();
            if (exisitingCategory.IsNotNullOrEmpty())
            {
                availableCategory.AddRange(exisitingCategory);
            }
            var newCategory = permissions.Keys.ToArray();
            if (newCategory.IsNotNullOrEmpty())
            {
                availableCategory.AddRange(newCategory);
            }
            availableCategory = availableCategory.Distinct().ToList();
            var allService = (int)AgencyServicesBoundary.All;
            if (availableCategory.IsNotNullOrEmpty())
            {
                availableCategory.ForEach(category =>
                {
                    var isNonServiceScope = nonServiceScopeCategory.Contains(category);

                    Dictionary<int, List<int>> comingActions = null;
                    var newCategoryExist = permissions.TryGetValue(category, out comingActions);
                   
                    Dictionary<int, List<int>> exsistingActions = null;
                    var exsistingCategoryExist = exsistingPermissions.TryGetValue(category, out exsistingActions);
                    exsistingActions = exsistingActions ?? new Dictionary<int, List<int>>();

                    if (comingActions.IsNotNullOrEmpty())
                    {
                        //Debug.Assert(comingActions != null, "comingActions != null");
                        var mergedActions = new List<int>();
                        var exisitingActionsLean = exsistingActions.Keys.ToArray();
                        if (exisitingActionsLean.IsNotNullOrEmpty())
                        {
                            mergedActions.AddRange(exisitingActionsLean);
                        }
                        var newActions = comingActions.Keys.ToArray();
                        if (newActions.IsNotNullOrEmpty())
                        {
                            mergedActions.AddRange(newActions);
                        }

                        mergedActions = mergedActions.Distinct().ToList();
                        if (mergedActions.IsNotNullOrEmpty())
                        {
                            mergedActions.ForEach(action =>
                            {
                                List<int> newServices;
                                var newActionExist = comingActions.TryGetValue(action, out newServices);

                                List<int> oldServices;
                                var exsistingActionExist = exsistingActions.TryGetValue(action, out oldServices);
                                oldServices = oldServices ?? new List<int>();

                                if (newServices.IsNotNullOrEmpty())
                                {
                                   // Debug.Assert(newServices != null, "newServices != null");
                                    if (isNonServiceScope)
                                    {
                                        if (newServices.Contains(allService))
                                        {
                                            if (!oldServices.Contains(allService))
                                            {
                                                oldServices.Add(allService);
                                            }
                                        }
                                        else
                                        {
                                            oldServices.Remove(allService);
                                        }
                                    }
                                    else
                                    {
                                        editableServices.ForEach(service =>
                                        {
                                            if (newServices.Contains(service))
                                            {
                                                if (oldServices.Contains(service))
                                                {
                                                }
                                                else
                                                {
                                                    oldServices.Add(service);
                                                }
                                            }
                                            else
                                            {
                                                oldServices.Remove(service);
                                            }
                                            //this helps to clear non service flag
                                            oldServices.Remove(allService);
                                        });
                                    }

                                    if (exsistingActionExist)
                                    {
                                        exsistingActions[action] = oldServices;
                                    }
                                    else
                                    {
                                        exsistingActions.Add(action, oldServices);
                                    }
                                }
                                else
                                {
                                    if (exsistingActionExist)
                                    {
                                        if (isNonServiceScope)
                                        {
                                            oldServices.Remove(allService);
                                        }
                                        else
                                        {
                                            editableServices.ForEach(s => oldServices.Remove(s));
                                        }
                                        exsistingActions[action] = oldServices;
                                    }
                                }
                            });
                            exsistingActions.RemoveAll(ea => ea.Count <= 0);
                            if (exsistingCategoryExist)
                            {
                                exsistingPermissions[category] = exsistingActions;
                            }
                            else
                            {
                                exsistingPermissions.Add(category, exsistingActions);
                            }
                        }
                    }
                    else
                    {
                        if (exsistingCategoryExist)
                        {
                            var actions = exsistingActions.Keys.Distinct();
                            if (exsistingActions.IsNotNullOrEmpty())
                            {
                                exsistingActions.ForEach((key, value) =>
                                {
                                    var oldServices = value;
                                    if (isNonServiceScope)
                                    {
                                        oldServices.Remove(allService);
                                    }
                                    else
                                    {
                                        editableServices.ForEach(s => oldServices.Remove(s));
                                    }
                                    value = oldServices;
                                });
                                exsistingActions.RemoveAll(ea => ea.Count <= 0);
                            }
                        }
                    }
                });
            }
            return exsistingPermissions;
        }


        private Dictionary<int, Dictionary<int, List<int>>> UpdateReportPermissions(List<int> editableServices, IDictionary<int, Dictionary<int, List<int>>> permissions, Dictionary<int, Dictionary<int, List<int>>> exsistingPermissions, out bool IsReportVisible)
        {
            //var serviceExportList = new List<int>();
            var serviceViewList = new List<int>();
            IsReportVisible = false;
            var availableCategory = new List<int>();
            var exisitingCategory = exsistingPermissions.Keys.ToArray();
            if (exisitingCategory.IsNotNullOrEmpty())
            {
                availableCategory.AddRange(exisitingCategory);
            }

            var newCategory = permissions.Keys.ToArray();
            if (newCategory.IsNotNullOrEmpty())
            {
                availableCategory.AddRange(newCategory);
            }
            availableCategory = availableCategory.Distinct().ToList();
            if (availableCategory.IsNotNullOrEmpty())
            {
                availableCategory.ForEach(category =>
                {
                    Dictionary<int, List<int>> comingActions = null;
                    var newCategoryExist = permissions.TryGetValue(category, out comingActions);

                    Dictionary<int, List<int>> existingActions = null;
                    var existingCategoryExist = exsistingPermissions.TryGetValue(category, out existingActions);
                    existingActions = existingActions ?? new Dictionary<int, List<int>>();

                    if (comingActions.IsNotNullOrEmpty())
                    {
                        var mergedActions = new List<int>();
                        var exisitingActionsLean = existingActions.Keys.ToArray();
                        if (exisitingActionsLean.IsNotNullOrEmpty())
                        {
                            mergedActions.AddRange(exisitingActionsLean);
                        }
                        var newActions = comingActions.Keys.ToArray();
                        if (newActions.IsNotNullOrEmpty())
                        {
                            mergedActions.AddRange(newActions);
                        }
                        mergedActions = mergedActions.Distinct().ToList();
                        if (mergedActions.IsNotNullOrEmpty())
                        {
                            mergedActions.ForEach(action =>
                            {
                                List<int> newServices;
                                var newActionExist = comingActions.TryGetValue(action, out newServices);

                                List<int> oldServices;
                                var exsistingActionExist = existingActions.TryGetValue(action, out oldServices);
                                oldServices = oldServices ?? new List<int>();

                                if (newServices.IsNotNullOrEmpty())
                                {
                                    editableServices.ForEach(service =>
                                    {
                                        if (newServices.Contains(service))
                                        {
                                            if (oldServices.Contains(service))
                                            {
                                            }
                                            else
                                            {
                                                oldServices.Add(service);
                                            }
                                        }
                                        else
                                        {
                                            oldServices.Remove(service);
                                        }
                                    });

                                    if (exsistingActionExist)
                                    {
                                        existingActions[action] = oldServices;
                                    }
                                    else
                                    {
                                        existingActions.Add(action, oldServices);
                                    }
                                    //if ((int)PermissionActions.Export == action)
                                    //{
                                    //    serviceExportList.AddRange(oldServices);
                                    //}
                                    //else
                                    if ((int)PermissionActions.ViewList == action)
                                    {
                                        serviceViewList.AddRange(oldServices);
                                    }
                                }
                                else
                                {
                                    if (exsistingActionExist)
                                    {
                                        editableServices.ForEach(s => oldServices.Remove(s));
                                    }
                                    existingActions[action] = oldServices;

                                    //if ((int)PermissionActions.Export == action)
                                    //{
                                    //    serviceExportList.AddRange(oldServices);
                                    //}
                                    //else 
                                    if ((int)PermissionActions.ViewList == action)
                                    {
                                        serviceViewList.AddRange(oldServices);
                                    }
                                }
                            });
                            existingActions.RemoveAll(a => a.Count <= 0);
                            if (existingCategoryExist)
                            {
                                exsistingPermissions[category] = existingActions;
                            }
                            else
                            {
                                exsistingPermissions.Add(category, existingActions);
                            }
                        }
                    }
                    else
                    {
                        if (existingCategoryExist)
                        {
                            var actions = existingActions.Keys.Distinct();
                            if (existingActions.IsNotNullOrEmpty())
                            {
                                existingActions.ForEach(
                                    (key, value) =>
                                    {
                                        var oldServices = value;
                                        editableServices.ForEach(s => oldServices.Remove(s));
                                        value = oldServices;
                                        //if ((int)PermissionActions.Export == key)
                                        //{
                                        //    serviceExportList.AddRange(oldServices);
                                        //}
                                        //else 
                                        if ((int)PermissionActions.ViewList == key)
                                        {
                                            serviceViewList.AddRange(oldServices);
                                        }
                                    });
                                existingActions.RemoveAll(a => a.Count <= 0);
                            }
                        }
                    }
                });

                exsistingPermissions.RemoveAll(p => p.Values.Count <= 0);
                //var list = new Dictionary<int, List<int>>();
                if (serviceViewList.IsNotNullOrEmpty())
                {
                    IsReportVisible = true;
                    //list.Add((int)PermissionActions.ViewList, serviceViewList.Distinct().ToList());
                }
                //if (serviceExportList.IsNotNullOrEmpty())
                //{
                //    list.Add((int)PermissionActions.Export, serviceExportList.Distinct().ToList());
                //}
                //if (list.IsNotNullOrEmpty())
                //{
                //    IsReportVisible = true;
                //}

            }

            return exsistingPermissions;
        }


        #endregion

        #region User Rate

        //public bool LoadUserRate(Guid fromId, Guid toId)
        //{
        //    bool result = false;
        //    var fromUser = userRepository.GetUserOnly(fromId, Current.AgencyId);
        //    if (fromUser != null)
        //    {
        //        var toUser = userRepository.GetUserOnly(toId, Current.AgencyId);
        //        if (toUser != null)
        //        {
        //            toUser.Rates = fromUser.Rates;
        //            if (userRepository.UpdateModel(toUser, false))
        //            {
        //                result = true;
        //            }
        //        }
        //    }
        //    return result;
        //}

        public bool LoadUserRate(Guid fromId, Guid toId, List<PayorTaskInput> payors)
        {
            bool result = false;
            bool isSave=false;
            //var payorTaskDictonary=payors.GroupBy(pt=>pt.Id).ToDictionary(g=>g.Key,g=>g.ToList());
            var payorIds = payors.Where(p => p.Id > 0 && p.Tasks.IsNotNullOrEmpty()).Select(p => p.Id).Distinct().ToList();
            if (payorIds.IsNotNullOrEmpty())
            {
                var fromUserPayorRates = userRepository.GetUserPayorRates(Current.AgencyId, fromId, payorIds);
                if (fromUserPayorRates.IsNotNullOrEmpty())
                {
                    var toUserPayorRates = userRepository.GetUserPayorRates(Current.AgencyId, toId, payorIds);
                    var newToUserPayorRates = new List<UserPayorRates>();
                    var updateToUserPayorRates = new List<UserPayorRates>();
                    payors.ForEach(p =>
                    {
                        if (p.Tasks.IsNotNullOrEmpty())
                        {
                            var oldRates = fromUserPayorRates.FirstOrDefault(or => or.PayorId == p.Id);
                            if (oldRates != null && oldRates.PayorRates.IsNotNullOrEmpty())
                            {
                                var newTasks = oldRates.PayorRates.Where(nt => p.Tasks.Contains(nt.Id));
                                if (newTasks.IsNotNullOrEmpty())
                                {
                                    var toNewRates = toUserPayorRates.FirstOrDefault(nr => nr.PayorId == p.Id);
                                    if (toNewRates != null)
                                    {
                                        if (toNewRates.PayorRates.IsNotNullOrEmpty())
                                        {
                                            toNewRates.PayorRates.RemoveAll(r => p.Tasks.Contains(r.Id));
                                            toNewRates.PayorRates.AddRange(newTasks);
                                        }
                                        else
                                        {
                                            toNewRates.PayorRates = newTasks.ToList();
                                        }
                                        updateToUserPayorRates.Add(toNewRates);
                                        isSave=true;
                                    }
                                    else
                                    {
                                        newToUserPayorRates.Add(new UserPayorRates
                                                           {
                                                               AgencyId = Current.AgencyId,
                                                               UserId = toId,
                                                               PayorId = p.Id,
                                                               PayorRates = newTasks.ToList()
                                                           });
                                         isSave=true;
                                    }
                                }
                            }
                        }
                    });
                    if (isSave)
                    {
                        if (newToUserPayorRates.IsNotNullOrEmpty())
                        {
                            if (userRepository.AddUserPayorRates(newToUserPayorRates))
                            {
                                result = true;
                            }
                        }
                        if (updateToUserPayorRates.IsNotNullOrEmpty())
                        {
                            updateToUserPayorRates.ForEach(o =>
                            {
                                userRepository.UpdateUserPayorRates(Current.AgencyId, toId, o.PayorId, o);
                            });
                            result = true;
                        }
                    }
                    //fromUserPayorRates.ForEach(
                    //    r =>
                    //    {
                    //        var pa=payors.FirstOrDefault(p=>p.Id==r.PayorId);
                    //        if (pa!=null)
                    //        {
                    //            //var tasks=payorTaskDictonary[r.PayorId].
                    //        }
                    //    }
                    //    );
                    //if (toUser != null)
                    //{
                    //    toUser.Rates = fromUserPayorRates.Rates;
                    //    if (userRepository.UpdateModel(toUser, false))
                    //    {
                    //        result = true;
                    //    }
                    //}
                }
            }
            return result;
        }

        //TODO: needs improvement 
        //public List<UserRate> GetUserRates(Guid userId)
        //{
        //    var user = userRepository.GetUserOnly(userId, Current.AgencyId);
        //    return GetUserRatesHelper(user);
        //}

        //private static List<UserRate> GetUserRatesHelper(User user)
        //{
        //    var list = new List<UserRate>();
        //    if (user != null && user.Rates.IsNotNullOrEmpty())
        //    {
        //        list = user.Rates.ToObject<List<UserRate>>();
        //        if (list.IsNotNullOrEmpty())
        //        {
        //            var insuranceIds = list.Where(s =>s.Insurance > 0).Select(i => i.Insurance).Distinct().ToList();
        //            var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
        //            list.ForEach(r =>
        //            {
        //                var insurance = insurances.FirstOrDefault(i => i.Id == r.Insurance);
        //                if (insurance != null)
        //                {
        //                    r.InsuranceName = insurance.Name;
        //                }
        //            });
        //        }
        //    }
        //    return list;
        //}

        public List<UserPayorRates> GetUserRatesOnly(Guid userId)
        {
            var userPayorRates = userRepository.GetUserPayorRates(Current.AgencyId, userId);
            if (userPayorRates.IsNotNullOrEmpty())
            {

            }
            else
            {
                var newUserPayorRatesList = new List<UserPayorRates>();
                var rates = userRepository.GetUsersRates(Current.AgencyId, userId);
                if (rates.IsNotNullOrEmpty())
                {
                    var ratelist = rates.ToObject<List<UserRate>>();
                    if (ratelist.IsNotNullOrEmpty())
                    {
                        var ratesDictionary = ratelist.GroupBy(g => g.Insurance).ToDictionary(v => v.Key, v => v.ToList());
                        if (ratesDictionary.IsNotNullOrEmpty())
                        {
                           
                            ratesDictionary.ForEach((key, value) =>
                            {
                                newUserPayorRatesList.Add(new UserPayorRates
                                {
                                    AgencyId = Current.AgencyId,
                                    UserId = userId,
                                    PayorId = key,
                                    PayorRates = value
                                });
                            });
                            if (newUserPayorRatesList.IsNotNullOrEmpty())
                            {
                                userRepository.AddUserPayorRates(newUserPayorRatesList);
                                userPayorRates = newUserPayorRatesList;
                            }
                        }
                    }
                }
            }

            //var userRate = new UserPayorRates { UserId = userId };
            //var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            //if (user != null)
            //{
            //    userRate.LocationId = user.AgencyLocationId;
            //    //userRate.Rates = GetUserRatesHelper(user);
            //}
            if (userPayorRates.IsNotNullOrEmpty())
            {
                var insuranceIds = userPayorRates.Where(s => s.PayorId > 0).Select(i => i.PayorId).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds) ?? new List<InsuranceCache>();
                userPayorRates.ForEach(r =>
                {
                    var insurance = insurances.FirstOrDefault(i => i.Id == r.PayorId);
                    if (insurance != null)
                    {
                        r.InsuranceName = insurance.Name;
                    }
                });
            }

            return userPayorRates;
        }

        public JsonViewData AddUserRate(UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be added. Please try again." };
            if (userRate.IsValid())
            {
                  var userPayorRates = userRepository.GetUserPayorRate(Current.AgencyId, userRate.UserId, userRate.Insurance);
                  if (userPayorRates != null)
                  {
                      if (userPayorRates.PayorRates.IsNotNullOrEmpty())
                      {
                          var rate = userPayorRates.PayorRates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);
                          if (rate != null)
                          {
                              viewData.isSuccessful = false;
                              viewData.errorMessage = "User rate already exists.";
                          }
                          else
                          {
                              userPayorRates.PayorRates.Add(userRate);
                              if (userRepository.UpdateUserPayorRates(Current.AgencyId, userRate.UserId, userRate.Insurance, userPayorRates))
                              {
                                  viewData.isSuccessful = true;
                                  viewData.errorMessage = "User rate added successfully";
                              }
                          }
                      }
                      else
                      {
                          userPayorRates.PayorRates = new List<UserRate> { userRate };
                          if (userRepository.UpdateUserPayorRates(Current.AgencyId, userRate.UserId, userRate.Insurance, userPayorRates))
                          {
                              viewData.isSuccessful = true;
                              viewData.errorMessage = "User rate added successfully";
                          }
                      }
                  }
                  else
                  {

                      if (userRepository.AddUserPayorRates(new UserPayorRates { AgencyId = Current.AgencyId, UserId = userRate.UserId, PayorId = userRate.Insurance, PayorRates = new List<UserRate> { userRate } }))
                      {
                          viewData.isSuccessful = true;
                          viewData.errorMessage = "User rate added successfully";
                      }

                      //var user = userRepository.GetUserOnly(userRate.UserId, Current.AgencyId);
                      //if (user != null)
                      //{
                      //    if (user.Rates.IsNotNullOrEmpty())
                      //    {
                      //        var rates = user.Rates.ToObject<List<UserRate>>();
                      //        if (rates != null && rates.Count > 0)
                      //        {
                      //            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);
                      //            if (rate != null)
                      //            {
                      //                viewData.isSuccessful = false;
                      //                viewData.errorMessage = "User rate already exists.";
                      //            }
                      //            else
                      //            {
                      //                rates.Add(userRate);
                      //                user.Rates = rates.ToXml();
                      //                if (userRepository.UpdateModel(user, false))
                      //                {
                      //                    viewData.isSuccessful = true;
                      //                    viewData.errorMessage = "User rate added successfully";
                      //                }
                      //            }
                      //        }
                      //        else
                      //        {
                      //            rates = new List<UserRate>();
                      //            rates.Add(userRate);
                      //            user.Rates = rates.ToXml();
                      //            if (userRepository.UpdateModel(user, false))
                      //            {
                      //                viewData.isSuccessful = true;
                      //                viewData.errorMessage = "User rate added successfully";
                      //            }
                      //        }
                      //    }
                      //    else
                      //    {
                      //        var rates = new List<UserRate>();
                      //        rates.Add(userRate);
                      //        user.Rates = rates.ToXml();
                      //        if (userRepository.UpdateModel(user, false))
                      //        {
                      //            viewData.isSuccessful = true;
                      //            viewData.errorMessage = "User rate added successfully";
                      //        }
                      //    }
                      //}
                  }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData DeleteUserRate(Guid userId, int payorId,int rateId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };

            var userPayorRates = userRepository.GetUserPayorRate(Current.AgencyId, userId, payorId);
            if (userPayorRates != null)
            {
                if (userPayorRates.PayorRates.IsNotNullOrEmpty())
                {
                    userPayorRates.PayorRates.RemoveAll(r => r.Id == rateId);
                    if (userRepository.UpdateUserPayorRates(Current.AgencyId, userId, payorId, userPayorRates))
                    {
                        viewData.isSuccessful = true;
                    }
                }
            }
            //else
            //{

            //    var user = userRepository.GetUsersRates(Current.AgencyId, userId);
            //    if (user != null)
            //    {
            //        if (user.Rates.IsNotNullOrEmpty())
            //        {
            //            var rates = user.Rates.ToObject<List<UserRate>>();
            //            if (rates.IsNotNullOrEmpty())
            //            {
            //                var removed = rates.RemoveAll(r => r.Id == rateId && r.Insurance == payorId);
            //                if (removed > 0)
            //                {
            //                    user.Rates = rates.ToXml();
            //                    if (userRepository.UpdateModel(user, false))
            //                    {
            //                        viewData.isSuccessful = true;
            //                        viewData.errorMessage = "User rate deleted successfully";
            //                    }
            //                }
            //                else
            //                {
            //                    removed = rates.RemoveAll(r => r.Id == rateId && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
            //                    if (removed > 0)
            //                    {
            //                        user.Rates = rates.ToXml();
            //                        if (userRepository.UpdateModel(user, false))
            //                        {
            //                            viewData.isSuccessful = true;
            //                            viewData.errorMessage = "User rate deleted successfully";
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            return viewData;
        }

        public UserRate GetUserRateForEdit(Guid userId, int payorId, int rateId)
        {
            UserRate userRate = null;
            var payorUserRates = userRepository.GetUserPayorRate(Current.AgencyId, userId, payorId);
            if (payorUserRates != null)
            {
                if (payorUserRates.PayorRates.IsNotNullOrEmpty())
                {
                    userRate = payorUserRates.PayorRates.FirstOrDefault(r => r.Id == rateId);
                    if (userRate != null)
                    {
                        userRate.UserId = userId;
                        if (payorId > 0)
                        {
                            userRate.InsuranceName = InsuranceEngine.GetName(payorId, Current.AgencyId);
                        }
                    }
                }
            }
            
        
            //var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            //if (user != null && user.Rates.IsNotNullOrEmpty())
            //{
            //    var rates = user.Rates.ToObject<List<UserRate>>();
            //    userRate = rates.FirstOrDefault(r => r.Id == Id && r.Insurance == insurance);
            //    if (userRate != null)
            //    {
            //        userRate.UserId = user.Id;
            //        if (userRate.Insurance > 0)
            //        {
            //            userRate.InsuranceName = InsuranceEngine.GetName(userRate.Insurance, Current.AgencyId);
            //        }
            //    }
            //    else
            //    {
            //        userRate = rates.FirstOrDefault(r => r.Id == Id && r.Insurance<=0 && r.RateType == 0);
            //        if (userRate != null)
            //        {
            //            userRate.UserId = user.Id;
            //            if (userRate.Insurance > 0)
            //            {
            //                userRate.InsuranceName = InsuranceEngine.GetName(userRate.Insurance, Current.AgencyId);
            //            }
            //        }
            //        else
            //        {
            //            userRate = new UserRate();
            //            userRate.Id = Id;
            //            userRate.UserId = user.Id;
            //           // userRate.Insurance = string.Empty;
            //        }
            //    }
            //}
            return userRate;
        }

        public JsonViewData UpdateUserRate(UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be updated. Please try again." };
            if (userRate.IsValid())
            {
                var payorUserRates = userRepository.GetUserPayorRate(Current.AgencyId, userRate.UserId, userRate.Insurance);
                if (payorUserRates != null)
                {
                    if (payorUserRates.PayorRates.IsNotNullOrEmpty())
                    {
                       var userRateToEdit = payorUserRates.PayorRates.FirstOrDefault(r => r.Id == userRate.Id);
                       if (userRateToEdit != null)
                        {
                            userRateToEdit.Rate = userRate.Rate;
                            userRateToEdit.RateType = userRate.RateType;
                            userRateToEdit.MileageRate = userRate.MileageRate;
                            userRateToEdit.Insurance = userRate.Insurance;
                            if (userRepository.UpdateUserPayorRates(Current.AgencyId, userRate.UserId, userRate.Insurance, payorUserRates))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate updated successfully";
                            }
                        }
                        else
                        {
                           viewData.errorMessage = "Unable to find the  rate . Try again.";
                        }
                    }
                }

                //var user = userRepository.GetUserOnly(userRate.UserId, Current.AgencyId);
                //if (user != null)
                //{
                //    if (user.Rates.IsNotNullOrEmpty())
                //    {
                //        var rates = user.Rates.ToObject<List<UserRate>>();
                //        if (rates != null && rates.Count > 0)
                //        {
                //            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);

                //            if (rate == null)
                //            {
                //                rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance<=0 && r.RateType == 0);
                //                if (rate != null)
                //                {
                //                    rate.Rate = userRate.Rate;
                //                    rate.RateType = userRate.RateType;
                //                    rate.MileageRate = userRate.MileageRate;
                //                    rate.Insurance = userRate.Insurance;
                //                    user.Rates = rates.ToXml();
                //                    if (userRepository.UpdateModel(user, false))
                //                    {
                //                        viewData.isSuccessful = true;
                //                        viewData.errorMessage = "User rate updated successfully";
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                rate.Rate = userRate.Rate;
                //                rate.RateType = userRate.RateType;
                //                rate.MileageRate = userRate.MileageRate;
                //                rate.Insurance = userRate.Insurance;
                //                user.Rates = rates.ToXml();
                //                if (userRepository.UpdateModel(user, false))
                //                {
                //                    viewData.isSuccessful = true;
                //                    viewData.errorMessage = "User rate updated successfully";
                //                }
                //            }
                //        }
                //    }
                //}
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }
            return viewData;
        }

        #endregion
       
        public List<SelectedUser> GetUserAccess(Guid patientId)
        {
            var users = userRepository.GetUserAccessList(Current.AgencyId, patientId);
            foreach (var user in users)
            {
                if (user.PatientUserId != Guid.Empty)
                {
                    user.Selected = true;
                }
                else
                {
                    user.Selected = false;
                }
            }
            return users;
        }

        //public List<UserSelection> GetUserSelection()
        //{
        //    var users = userRepository.GetAgencyUsers(Current.AgencyId);

        //    var result = new List<UserSelection>();

        //    if (users != null && users.Count > 0)
        //    {
        //        users.ForEach(p => result.Add(new UserSelection(p)));
        //    }
        //    return result.OrderBy(u => u.DisplayName).ToList();
        //}

      

        public UserListViewData<NonUser> GetNonUsers()
        {
            var viewData = new UserListViewData<NonUser> { Users = userRepository.GetNonUsers(Current.AgencyId) };
            var userPermissions = Current.Permissions.GetCategoryPermission(ParentPermission.User);
            if (userPermissions.IsNotNullOrEmpty())
            {
                viewData.IsUserCanEdit = userPermissions.ContainsKey((int)PermissionActions.Edit);
                viewData.IsUserCanDelete = userPermissions.ContainsKey((int)PermissionActions.Delete);
                viewData.IsUserCanAdd = userPermissions.ContainsKey((int)PermissionActions.Add) && !Current.IsAgencyFrozen;
                viewData.IsUserCanExport = userPermissions.ContainsKey((int)PermissionActions.Export);
            }
            return viewData;
        }

        public NonUser GetNonUser(Guid id)
        {
            return userRepository.GetNonUser(Current.AgencyId, id);
        }

        public List<UserSelection> GetUserSelectionList(Guid branchId, int status, int service)
        {
            var users = userRepository.GetUsersByStatusLean(Current.AgencyId, branchId, status, service) ?? new List<User>();
            return users.Select(u => new UserSelection(u)).ToList();
        }

        //public List<SelectListItem> UserSelectList(Guid BranchId, int Status, int Service)
        //{
        //    var users = userRepository.GetUsersByStatusLean(Current.AgencyId, BranchId, Status, Service) ?? new List<User>();
        //    var list = new List<SelectListItem> { new SelectListItem() { Text = "", Value = "" } };
        //    list.AddRange(users.Select(item => new SelectListItem() { Text = item.DisplayName, Value = item.Id.ToString() }));
        //    return list;
        //}
    }
}
