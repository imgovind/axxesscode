﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;
    using Axxess.AgencyManagement.Entities;

    public interface IDateService
    {
        DateRange GetDateRange(string rangeIdentifier, Guid patientId);
    }
}
