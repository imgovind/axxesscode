﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;
    using ViewData;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.iTextExtension;

    public interface IPatientService
    {

        //JsonViewData AddNewPatient(Patient patient, List<Profile> profiles, int status);
        bool AddPatient(Patient patient);
        //JsonViewData EditPatient(Patient patient);
        //bool EditPatientSave(Patient patient);
        bool AdmitPatient(PendingPatient pending, out Patient patientOut);
        //JsonViewData NonAdmitPatient(PendingPatient pending);
        bool UpdateModal(Patient patient);
        //JsonViewData DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        //JsonViewData ActivatePatient(Guid patientId);
        //JsonViewData ActivatePatient(Guid patientId, DateTime startOfCareDate);
        bool RemovePatient(Guid id);
        bool SetStatus(Guid patientId, PatientStatus status, AgencyServices service);
        //JsonViewData SetPatientPending(Guid patientId);
        Patient GetPatientOnly(Guid patientId);
        //Patient GetAuthorizationPermission(Guid patientId);
        Patient GetPatientForPossibleNonAdmit(Guid patientId);
        Patient GetForDelete(Guid patientId);
        Patient GetForRestore(Guid patientId);
        //PatientProfile GetProfileWithOutEpisodeInfo(Guid id);
        //Patient GetPatientSnapshotInfo(Guid patientId, bool isPrimaryInsuranceSet, bool isEmergencyContactSet, bool isInsuranceSet);
        //void SetPatientSnapshotInfo(Patient patient, bool isPrimaryInsuranceSet, bool isEmergencyContactSet, bool isInsuranceSet);
        //PendingPatient GetPendingPatient(Guid id, NonAdmitTypes type);
        //List<PatientData> GetPatients(Guid agencyId, Guid branchId, int serviceId, int status);
        //List<PatientData> GetDeletedPatients(Guid branchId, int serviceId, bool IsInsuranceNameNeeded);
        //List<NonAdmit> GetNonAdmits();
        //List<PendingPatient> GetPendingPatients(Guid branchId, int serviceId);
        bool IsPatientExist(Guid patientId);
        bool IsPatientIdExist(string patientIdNumber);
        bool IsMedicareExist(string medicareNumber);
        bool IsMedicaidExist(string medicaidNumber);
        bool IsMedicareExistForEdit(Guid patientId, string medicareNumber);
        bool IsMedicaidExistForEdit(Guid patientId, string medicaidNumber);
        bool IsPatientIdExistForEdit(Guid patientId, string patientIdNumber);
        bool IsSSNExistForEdit(Guid patientId, string ssn);
        string LastPatientId();
        string GetPatientNameById(Guid patientId);
        PatientNameViewData GetPatientNameViewData(Guid patientId);
        void LinkPendingPatientToPhysician(PendingPatient pending, Patient patient);
        Patient GetPatientForPending(Guid Id);

        Dictionary<string, string> GetPatientJsonByColumns(Guid id, params string[] columns);
        bool AddPhoto(Guid patientId, HttpPostedFileBase httpFile);
        bool IsValidImage(HttpPostedFileBase httpFile);
        bool UpdatePatientForPhotoRemove(Guid patientId);

        //bool IsMedicareOrMedicareHMOInsurance(string insuranceStringId, out int insuranceIdOut);
        //void SetInsurance(Patient patient);
        //string GetInsurance(string insurance);
        //PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType);

        
        bool AdmitReferral(PendingPatient pending, out Patient patientOut);
        void LinkReferralPhysician(Referral referral);

        //List<PatientSelection> GetPatientSelection(Guid branchId, byte statusId, byte paymentSourceId, string name, bool checkAuditor, byte servicesId);
        //PatientSelectionViewData GetPatientCenterDataWithOutSchedules(Guid patientId, int status, ref  Patient patient);
        //PatientSelectionViewData GetScheduleCenterDataWithOutSchedules(Guid patientId, int status);

        //bool AddPatientAdmissionDate(PatientAdmissionDate managedDate);
        //JsonViewData AdmissionPatientNew([Bind] Patient patient);
        //JsonViewData AdmissionPatientEdit(Patient patient);
        //bool UpdatePatientAdmissionDate(PatientAdmissionDate admissionData);
        //bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id);
        //JsonViewData DeletePatientAdmission(Guid patientId, Guid Id);
        //bool RemovePatientAdmissionDate(Guid patientId, Guid Id);
        //bool RemovePatientAdmissionDates(Guid patientId);
        //PatientAdmissionDate GetIfExitOrCreate(Guid patientId);
        //Patient GetAdmissionPatientInfo(Guid patientId, Guid Id, string Type);
        //PatientAdmissionDate GetPatientAdmissionDate(Guid patientId, Guid Id);
        //IList<PatientAdmissionDate> GetPatientAdmissionPeriods(Guid patientId);
        //bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        //bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        //void SetMasterCalendarAdmissionInfo(CalendarViewData data);
        //IList<SelectListItem> GetPatientAdmissionDateSelectList(Guid patientId);

        bool AddPrimaryEmergencyContact(Patient patient);
        bool AddEmergencyContact(PatientEmergencyContact emergencyContact);
        JsonViewData AddNewEmergencyContact(Guid patientId, PatientEmergencyContact emergencyContact);
        JsonViewData EditEmergencyContact(PatientEmergencyContact emergencyContact);
        bool UpdateEmergencyContact(Guid patientId, PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContact(Guid id, Guid patientId);
        bool RemoveEmergencyContacts(Guid patientId);
        PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid Id);
        IList<PatientEmergencyContact> GetEmergencyContacts(Guid PatientId);


        bool CreateMedicationProfile(Patient patient, Guid medId);
        bool CreateMedicationProfile(MedicationProfile medicationProfile);
        JsonViewData SaveMedicationProfile(MedicationProfile medicationProfile);
        bool RemoveMedicationProfile(Guid patientId, Guid Id);
        MedicationProfile GetMedicationProfileByPatient(Guid patientId);
        MedicationProfile GetMedicationProfile(Guid medicationProfileId);
        MedicationProfile GetMedicationProfileWithPermission(Guid medicationProfileId);
        MedicationListViewData GetMedicationListViewData(Guid medicationProfileId);
        string GetMedicationProfileText(Guid patientId);
        bool IsMedicationProfileExist(Guid patientId);


        bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate);
        bool UpdateMedicationForDischarge(Guid medId, Guid Id, DateTime dischargeDate);
        bool DeleteMedication(Guid medicationProfileId, Guid medicationId);
        Medication GetMedication(Guid medProfileId, Guid medicationId);
        //List<Medication> GetMedications(Guid medId, string medicationCategory);
        //List<Medication> InsertThenReturnMedications(Guid medId, Medication medication, string medicationType, string medicationCategory);
        //List<Medication> UpdateThenReturnMedications(Guid medId, Medication medication, string medicationType, string medicationCategory);
        //List<Medication> DeleteThenReturnMedications(Guid medId, Medication medication, string medicationCategory);

        bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId);
        IList<MedicationProfileHistory> UpdateMedicationSnapshotHistoryThenReturn(Guid Id, Guid patientId, DateTime signedDate);
        IList<MedicationProfileHistory> DeleteMedicationSnapshotHistoryThenReturn(Guid Id, Guid patientId);

        JsonViewData AddAllergy(Allergy allergy);
        JsonViewData UpdateAllergy(Allergy allergy);
        JsonViewData UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted);
        Allergy GetAllergy(Guid allergyProfileId, Guid allergyId);
        string GetAllergiesText(Guid patientId);

        AllergyProfile GetAllergyProfile(Guid allergyProfileId, string prefix);
        AllergyProfileViewData GetPatientAllergyProfileViewData(Guid PatientId);


        bool LinkPhysicians(Patient patient);
        bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkAll(Guid patientId);
        bool UnlinkPhysician(Guid patientId, Guid physicianId);
        JsonViewData AddPatientPhysician(Guid physicianId, Guid patientId);
        AgencyPhysician GetPatientPrimaryPhysician(Guid patientId);
        IList<AgencyPhysician> GetPatientPhysicians(Guid patientId);
        bool SetPrimaryIfExistOrAddNew(Guid patientId, Guid physicianId);
        bool SetPrimary(Guid patientId, Guid physicianId);


        //PatientEpisode CreateEpisode(Guid patientId, DateTime startDate);
        //PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode);
        //bool AddEpisode(PatientEpisode patientEpisode);
        //JsonViewData UpdateEpisode(PatientEpisode episode);
        //bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate);
        //bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);


        //PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender);
        //PatientEligibility GetMedicareEligibility(Guid patientId, Guid medicareEligibilityId);
        //List<MedicareEligibility> GetMedicareEligibilities(Guid patientId);

       // JsonViewData AddAuthorization(Authorization authorization);
       // JsonViewData EditAuthorization(Authorization authorization);
       // JsonViewData DeleteAuthorization(Guid Id, Guid patientId);
       //// AuthorizationPdf GetAuthorizationPdf(Guid patientId, Guid id);
       // Authorization GetAuthorization(Guid authorizationId);
       // Authorization GetAuthorizationWithPatientName(Guid patientId, Guid Id);
       // IList<Authorization> GetAuthorizations(Guid patientId, int service);
       // IList<Authorization> GetAuthorizationsWithBranchName(Guid patientId, int service);


        bool AddHospitalizationLog(FormCollection formCollection);
        bool UpdateHospitalizationLog(FormCollection formCollection);
        JsonViewData UpdateHospitalizationLogStatus(Guid patientId, Guid hospitalizationLogId, bool isDeprecated);
        HospitalizationLog GetHospitalizationLogOnly(Guid patientId, Guid hospitalizationLogId);
        //HospitalizationLog GetHospitalizationLogPrintWithNoEpisodeInfo(Guid patientId, Guid hospitalizationLogId);
        HospitalizationViewData GetHospitalizationViewData(Guid patientId, bool isExport);
        //List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId);
        List<HospitalizationLog> GetHospitalizationLogsWithUserName(Guid agencyId, Guid patientId);
        List<PatientHospitalizationData> GetHospitalizationLogs(Guid agencyId);

        bool AddDocument(PatientDocument patientDocument, HttpPostedFileBase asset);
        bool DeleteDocument(Guid id, Guid patientId);
        bool EditDocument(PatientDocument patientDocument);
        PatientDocument GetPatientDocumentWithName(Guid patientId, Guid documentId);
        List<PatientDocument> GetDocuments(Guid patientId);

        JsonViewData AddUserAccess(Guid patientId, Guid userId);
        JsonViewData RemoveUserAccess(Guid patientUserId);
        List<SelectedPatient> GetPatientAccess(Guid userId);
        //object GetPatientSelectionList(Guid branchId, int status);

        AgencyServices GetAccessibleServices(Guid patientId);

        //List<ChargeRate> GetPatientVisitRates(Guid patientId);
        //ChargeRate GetPatientVisitRate(Guid patientId, int Id);
        //JsonViewData AddChargeRate(Guid patientId, ChargeRate chargeRate);
        //JsonViewData UpdateChargeRate(Guid patientId, ChargeRate chargeRate);
        //JsonViewData DeleteChargeRate(Guid patientId, int Id);
        //JsonViewData ReplaceVisitRates(Guid patientId, int replacedId);
    }
}
