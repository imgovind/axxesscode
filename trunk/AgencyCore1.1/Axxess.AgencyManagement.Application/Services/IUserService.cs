﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Domain;

    public interface IUserService
    {
        JsonViewData DeleteNonUser(Guid id);
        JsonViewData Add(User user, int maxAgencyUserAccount);
        JsonViewData AddNonUser(NonUser nonUser);
        JsonViewData Activate(Guid userId, int maxAgencyUserAccount);
        JsonViewData SetUserStatus(Guid userId, int status);
        JsonViewData Update(User user);
        bool DeleteUser(Guid userId);
        User GetUserOnly(Guid userId);
        GeneralPermission GetUserPermissionsOnly(Guid userId);
        User GetUserWithProfilePermissionsAndLicenses(Guid userId);
        User GetUserWithProfileOnly(Guid userId);
        UserLicense GetUserLicensesOnly(Guid userId);
        List<License> GetUserLicenses(Guid userId);
        bool IsEmailAddressInUse(string emailAddress);
        bool IsAgencyHaveMoreAccount(int maxAgencyUserAccount);
        List<Recipient> GetMessageRecipientList(string searchTerm);

        bool UpdateProfile(User user);
        bool IsPasswordCorrect(Guid userId, string password);
        bool IsSignatureCorrect(Guid userId, string signature);
        bool LoadUserRate(Guid fromId, Guid toId, List<PayorTaskInput> payors);
        //bool UpdateReportPermissions(Guid userId, int[] editableServices, IDictionary<int, Dictionary<int, List<int>>> permissions);
        bool UpdatePermissions(UserPermissionInput permissionInputs);//(Guid userId, int[] editableServices, IDictionary<int, Dictionary<int, List<int>>> permissions, List<UserServiceLocation> ServiceLocations);

        JsonViewData AddLicense(License license, System.Web.HttpPostedFileBase httpFiles);
        bool DeleteLicense(Guid Id, Guid userId);
        JsonViewData UpdateLicense(License licenseItem);
        License EditLicenseItem(Guid licenseId, Guid userId);

        IList<License> GetLicenses();
        IList<License> GetLicenses(Guid branchId, int status, bool appendCustomIdToDisplayName);
        IList<License> GetLicenses(Guid userId, bool isAssetNeeded);

        JsonViewData UpdateNonUser(NonUser nonUser);

        List<UserPayorRates> GetUserRatesOnly(Guid userId);
        JsonViewData AddUserRate(UserRate userRate);
        JsonViewData DeleteUserRate(Guid userId, int payorId, int rateId);
        JsonViewData UpdateUserRate(UserRate userRate);
        UserRate GetUserRateForEdit(Guid userId, int payorId, int rateId);
        IList<User> GetUsersByStatus(Guid branchId, int status);
        List<User> GetAllUsers(Guid branchId, int statusId);
        List<SelectedUser> GetUserAccess(Guid patientId);

       

        UserListViewData<User> GetUsersListViewData(Guid branchId, int status);

        UserListViewData<NonUser> GetNonUsers();

        NonUser GetNonUser(Guid id);

        List<UserSelection> GetUserSelectionList(Guid branchId, int status, int service);
    }
}
