﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;
    using ViewData;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.Core.Infrastructure;

    public interface INoteService
    {
        string GetScheduledEventUrl(ScheduleEvent evnt, DisciplineTasks task);

        VisitNoteViewData GetVisitNote(ITask scheduleEvent, NoteArguments arguments);
        VisitNoteViewData GetVisitNoteForContent(Guid patientId, Guid noteId, Guid previousNoteId, string type);
        VisitNoteViewData GetVisitNotePrint();
        VisitNoteViewData GetVisitNotePrint(int type);
        VisitNoteViewData GetVisitNotePrint(NoteArguments arguments);

        #region Note Supply
        bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply, out string errorMessage);
        bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply, out string errorMessage);
        bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Guid Id);
        List<Supply> GetNoteSupplies(Guid episodeId, Guid patientId, Guid eventId);
        Supply GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Guid id);
        #endregion

        #region Missed Visits
        bool AddMissedVisit(MissedVisit missedVisit);
        MissedVisit GetMissedVisit(Guid agencyId, Guid Id);
        bool ProcessMissedVisitNotes(string button, Guid Id);
        JsonViewData MissedVisitRestore(Guid patientId, Guid episodeId, Guid eventId);
        MissedVisit GetMissedVisitPrint();
        MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId);
        List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, bool isExcel);
        #endregion

        #region Return Comments
        string GetReturnComments(Guid eventId, Guid episodeId, Guid patientId);
        bool AddReturnComments(Guid eventId, Guid episodeId, string comment);
        bool EditReturnComments(int id, string comment);
        bool DeleteReturnComments(int id);
        #endregion

        #region Communication Notes
        JsonViewData AddCommunicationNote(CommunicationNote communicationNote, bool isSigned);
        JsonViewData UpdateCommunicationNote(CommunicationNote communicationNote, bool isSigned);
        CommunicationNote GetCommunicationNote(Guid patientId, Guid Id);
        CommunicationNote GetCommunicationNoteEdit(Guid patientId, Guid Id);
        JsonViewData ProcessCommunicationNotes(string button, Guid patientId, Guid eventId);
        List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate);
        List<CommunicationNote> GetCommunicationNotes(Guid patientId);
        bool DeleteCommunicationNote(Guid Id, Guid patientId);
        CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId);
        #endregion

        #region Physician Order
        JsonViewData ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType);
        List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate);
        List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId);
        bool DeletePhysicianOrder(Guid orderId, Guid patientId, Guid episodeId);
        PhysicianOrder GetOrderPrint();
        PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId);
        #endregion


        JsonViewData AddInfection(Infection infection, bool isSigned);
        JsonViewData UpdateInfection(Infection infection, bool isSigned);
        Infection GetInfection(Guid Id);
        List<Infection> GetInfections(Guid agencyId);
        JsonViewData ProcessInfections(string button, Guid patientId, Guid eventId);
        Infection GetInfectionReportPrint(Guid patientId, Guid eventId);

        JsonViewData AddIncident(Incident incident, bool isSigned);
        JsonViewData UpdateIncident(Incident incident, bool isSigned);
        Incident GetIncident(Guid Id);
        List<Incident> GetIncidents(Guid agencyId);
        JsonViewData ProcessIncidents(string button, Guid patientId, Guid eventId);
        Incident GetIncidentReportPrint(Guid patientId, Guid eventId);

        bool ToggleScheduleStatusNew(ITask task, bool isDelete);
        bool ToggleScheduleStatusNew(Guid patientId, Guid eventId, bool isDelete);
        bool ToggleScheduleStatusNew(Guid episodeId, Guid patientId, Guid eventId, bool isDelete);

        NoteJsonViewData SaveNotes<T, K>(SaveNoteArguments savingArguments, string button, FormCollection formCollection)
            where T : IVisitNote, new()
            where K : ITask, new();
        JsonViewData ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId);

        //bool RestoreTask(Guid episodeId, Guid patientId, Guid eventId);
        //bool ToggleScheduleStatusNew(Guid episodeId, Guid patientId, Guid eventId, bool isDelete);
        // bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid userId, int task);
        //JsonViewData ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);
        //JsonViewData Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId);
    }
}
