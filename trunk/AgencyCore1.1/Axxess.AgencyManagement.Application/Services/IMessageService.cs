﻿namespace Axxess.AgencyManagement.Application.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;

    public interface IMessageService
    {
        IList<MessageFolder> FolderList();
        bool AddSystemMessage(Message message);
        JsonViewData DeleteMany(List<Guid> messageList);
        int GetTotalMessagesCount(string inboxType);
        Message GetMessage(Guid messageId, int messageTypeId);
        DashboardMessage GetCurrentDashboardMessage();
        bool DeleteMessage(Guid messageId, int messageTypeId);
        IList<Message> GetMessages(string inboxType, int pageNumber);
        JsonViewData SendMessage(Message message, HttpPostedFileBase attachment1);
        JsonViewData AddMessageFolder(string newFolderName);
        JsonViewData MoveMessageFolder(Guid id, Guid folderId, string folderName, string messageType);
    }
}
