﻿//namespace Axxess.AgencyManagement.Application.Services
//{
//    using System;
//    using System.Collections.Generic;

//    using Domain;
//    using ViewData;
//    using Axxess.AgencyManagement.Entities;
//    using Axxess.AgencyManagement.Entities.Enums;

//    public interface IReportService
//    {
//        #region Patient Reports

//        List<BirthdayWidget> GetCurrentBirthdays();
//        List<PatientRoster> GetPatientRoster(Guid branchCode, int statusId, int InsuranceId, bool isExcel);
//        List<PatientRoster> GetPatientRosterByDateRange(Guid branchCode, int statusId, int InsuranceId, DateTime startDate, DateTime endDate, bool isExcel);
//        List<EmergencyContactInfo> GetPatientEmergencyContacts(int statusId, Guid addressBranchId);
//        List<PatientSocCertPeriod> GetPatientSocCertPeriod(Guid branchId, int statusId, DateTime startDate, DateTime endDate);
//        List<PatientRoster> GetPatientByPhysician(Guid physicianId, int statusId, AgencyServices service );
//        List<PatientRoster> GetPatientRosterByInsurance(Guid branchCode, int insurance, int statusId);
//        List<Authorization> GetExpiringAuthorizaton(Guid branchId, int status);
//        List<SurveyCensus> GetPatientSurveyCensus(Guid branchId, int statusId, int insuranceId, bool isExcel);
//        List<PatientRoster> GetPatientMonthlyAdmission(Guid branchCode, int statusId, int month, int year);
//        List<PatientRoster> GetPatientAnnualAdmission(Guid branchCode, int statusId, int year);
//        IList<Birthday> GetPatientBirthdays(Guid branchId, int month);
//        List<AddressBookEntry> GetPatientAddressListing(Guid branchId, int status);
//        List<DischargePatient> GetDischargePatients(Guid branchId, DateTime startDate, DateTime endDate);
//        List<PatientRoster> GetPatientByResponsiableEmployee(Guid branchId, Guid userId, int status);
//        List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid branchId, Guid caseManagerId);
       
//        #endregion

//        #region Clinician Reports

//        IList<OpenOasis> GetAllOpenOasis(Guid branchCode, DateTime startDate, DateTime endDate);
//        //IList<ClinicalOrder> GetOrders(int statusId);
//        List<MissedVisit> GetAllMissedVisit(Guid branchCode, DateTime startDate, DateTime endDate);
//        IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);
//        IList<Order> GetPlanOfCareHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);

//        #endregion

//        #region Schedule Reports

//        List<ScheduleEvent> GetPatientScheduleEventsByDateRange(Guid patientId, DateTime fromDate, DateTime toDate);
//        List<UserVisit> GetUserScheduleEventsByDateRange(Guid userId, Guid branchCode, DateTime fromDate, DateTime toDate);
//        List<ScheduleEvent> GetPastDueScheduleEvents(Guid BranchId, DateTime StartDate, DateTime EndDate);
//        List<ScheduleEvent> GetPastDueScheduleEventsByDiscipline(Guid BranchId, string discipline, DateTime StartDate, DateTime EndDate);
//        List<ScheduleEvent> GetScheduleEventsByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate);
//        List<ScheduleEvent> GetCaseManagerScheduleByBranch(Guid branchId, DateTime startDate, DateTime endDate);
//        List<ScheduleEvent> GetScheduleDeviation(Guid branchId, DateTime startDate, DateTime endDate);

//        #endregion

//        #region Employee Reports

//        List<User> GetEmployeeRoster(Guid branchCode, int status);
//        List<Birthday> GetEmployeeBirthdays(Guid branchCode, int status, int month);
//        List<License> GetEmployeeExpiringLicenses(Guid branchCode, int status);
//        List<UserVisit> GetEmployeeScheduleByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate);
//        List<UserPermission> GetEmployeePermissions(Guid branchId, int status);

//        #endregion

//        #region Billing Reports

//        List<ClaimLean> BillViewDataByStatus(Guid branchId, string type, int status, DateTime startDate, DateTime endDate);
//        List<ClaimLean> SubmittedBillViewDataByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
//        List<ClaimLean> SubmittedClaimsByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
//        List<ClaimLean> ExpectedSubmittedClaimsByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
//        List<TypeOfBill> UnProcessedBillViewData(Guid BranchId, string type);
//        //IList<Claim> GetPotentialCliamAutoCancels(Guid branchId);
//        #endregion

//        #region Statistical Reports

//        List<UserVisit> GetEmployeeVisistList(Guid userId, DateTime startDate, DateTime endDate);

//        List<PatientAdmission> GetPatientAdmissionsByInternalSource(Guid branchCode, DateTime startDate, DateTime endDate);

//        List<PatientRoster> GetPatientByAdmissionYear(Guid BranchId, int StatusId, int Year);

//        List<PatientRoster> GetPatientByAdmissionUnduplicatedByDateRange(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate);

//        #endregion

//        IList<ReportLite> GetRequestedReport(Guid agencyId);
//    }
//}
