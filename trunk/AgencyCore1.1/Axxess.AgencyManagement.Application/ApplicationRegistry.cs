﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Web.Mvc;

    using AutoMapper;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Services;
    using Security;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    //using Axxess.OasisC.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Api;
    using Axxess.Api.Contracts;
    using Axxess.TherapyManagement.Repositories;
    using Axxess.TherapyManagement.Repositories.Implementations;
    //using Axxess.AgencyManagement.Application.Areas.Therapy.Service;
    //using Axxess.AgencyManagement.Application.Areas.Therapy.Service.Implementation;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;

    public class ApplicationRegistry : Registry
    {
        public ApplicationRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.AddAllTypesOf<IController>();
                x.WithDefaultConventions();
            });
            For<ILog>().Use<DatabaseLog>();
            For<IMembershipDataProvider>().Use<MembershipDataProvider>();
            For<IAgencyManagementDataProvider>().Use<AgencyManagementDataProvider>();
            For<ITherapyManagementDataProvider>().Use<TherapyManagementDataProvider>();
            //For<IMongoDataProvider>().Use<MongoDataProvider>();
            For<HHDataProvider>().Use(new HHDataProvider());

            For<HtmlHelperAbstraction>().Use<HHHtmlHelper>().Named("HomeHealth");
            For<HtmlHelperAbstraction>().Use<PDHtmlHelper>().Named("PrivateDuty");

            For<TaskHelper<ScheduleEvent>>().Use<ScheduleTaskHelper>();
            For<TaskHelper<PrivateDutyScheduleTask>>().Use<PrivateDutyScheduleTaskHelper>();


           // For<UrlHelper<ScheduleEvent>>().Use<HHUrlHelper>();
            //For<UrlHelper<PrivateDutyScheduleTask>>().Use<PDUrlHelper>();


            For<DateService<PatientEpisode>>().Use<HHDateService>();
            For<DateService<PrivateDutyCarePeriod>>().Use<PrivateDutyDateService>();

            For<MediatorService<ScheduleEvent, PatientEpisode>>().Use<HHMediatorService>();
            For<MediatorService<PrivateDutyScheduleTask,PrivateDutyCarePeriod>>().Use<PrivateDutyMediatorService>();

            For<EpisodeAssessmentHelper<PatientEpisode, ScheduleEvent>>().Use<HHEpisodeAssessmentHelper>();
            For<EpisodeAssessmentHelper<PrivateDutyCarePeriod, PrivateDutyScheduleTask>>().Use<PrivateDutyEpisodeAssessmentHelper>();

            For<AdmissionHelper<PatientEpisode>>().Use<HHAdmissionHelper>();
            For<AdmissionHelper<PrivateDutyCarePeriod>>().Use<PDAdmissionHelper>();

            For<NoteAndAssessmentHelper<ScheduleEvent>>().Use<HHNoteAndAssessmentHelper>();
            For<NoteAndAssessmentHelper<PrivateDutyScheduleTask>>().Use<PrivateDutyNoteAndAssessmentHelper>();

            For<ILookUpDataProvider>().Use<LookUpDataProvider>();
            For<ILogDataProvider>().Use<LogDataProvider>();
            //For<IAssessmentService>().Use<AssessmentService>();
            For<IMembershipService>().Use<MembershipService>();
            For<IFormsAuthenticationService>().Use<FormsAuthenticationService>();
            //For<IAgencyManagementService>().Use<AgencyManagementService>();
            For<HHEpiosdeService>().Use<HHEpiosdeService>();
        }
    }
}
