﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Web;
    using System.Reflection;
    using System.Security.Principal;
    using System.Collections.Generic;
    using System.Linq;

    using OpenForum.Core.Models;

    using Enums;
    using Domain;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    public static class Current
    {
        public static Func<DateTime> Time = () => DateTime.UtcNow;
        public static Func<DateTime> EndofMonth = () => { return DateTime.Now.AddMonths(1).AddDays(-(DateTime.Now.Day)); };
        public static Func<DateTime> StartofMonth = () => { return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); };

        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        public static ForumUser ForumUser
        {
            get
            {
                return new ForumUser(Current.UserId, Current.UserFullName, null, null);
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            }
        }

        public static AxxessIdentity User
        {
            get
            {
                AxxessIdentity identity = null;
                if (HttpContext.Current.User is WindowsIdentity)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessPrincipal)
                {
                    AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
                    identity = (AxxessIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static bool IsImpersonated
        {
            get
            {
                var result = false;
                if (HttpContext.Current.User is AxxessPrincipal)
                {
                    AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
                    var identity = (AxxessIdentity)principal.Identity;
                    if (identity != null && identity.IsImpersonated == true)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }


        public static Guid UserId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.UserId;
                }

                return Guid.Empty;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static Guid AgencyId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AgencyId;
                }

                return Guid.Empty;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DisplayName;
                }

                return string.Empty;
            }
        }

        public static string AgencyName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AgencyName;
                }

                return string.Empty;
            }
        }

        public static string SessionId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.SessionId;
                }

                return string.Empty;
            }
        }
        public static string UserFullName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.FullName;
                }

                return string.Empty;
            }
        }

        public static string UserAddress
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return string.Empty;// User.Session.Address;
                }

                return string.Empty;
            }
        }

        public static string IpAddress
        {
            get
            {
                var ipAddress = string.Empty;

                if (HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR").IsNotNullOrEmpty())
                {
                    ipAddress = HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
                }
                else
                {
                    ipAddress = HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR");
                }
                return ipAddress;
            }
        }

        public static string ServerName
        {
            get
            {
                return Environment.MachineName;
            }
        }

        public static string ServerIp
        {
            get
            {
                return HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            }
        }

        public static bool IsIpAddressRestricted
        {
            get
            {
                var result = true;

                if (Current.IpAddress.StartsWith("10.0.1")
                    || Current.IpAddress.StartsWith("97.79.164.66")
                    || Current.IpAddress.StartsWith("10.0.5")
                    || Current.IpAddress.IsEqual("127.0.0.1"))
                {
                    result = false;
                }
                return result;
            }
        }

        public static bool CanContinue
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    if (IsTestingAgency)
                    {
                        var user = Container.Resolve<IAgencyManagementDataProvider>().UserRepository.GetUserOnly(AgencyId, UserId);
                        if (user.HasTrialAccountExpired())
                        {
                            return false;
                        }
                    }
                    if (User.Session.AutomaticLogoutTime.IsNotNullOrEmpty())
                    {
                        var autoLogoutTime = new Time(User.Session.AutomaticLogoutTime);
                        if (DateTime.Now.TimeOfDay >= autoLogoutTime.TimeOfDay)
                        {
                            if (User.Session.EarliestLoginTime.IsNotNullOrEmpty())
                            {
                                var earliestLoginTime = new Time(User.Session.EarliestLoginTime);
                                if (earliestLoginTime.TimeOfDay > autoLogoutTime.TimeOfDay)
                                {
                                    if (User.Session.LoginDay < DateTime.Today.Day)
                                    {
                                        return false;
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                        return true;
                    }
                    else
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        //public static bool HasRight(Permissions permission)
        //{
        //    bool result = false;
        //    if (HttpContext.Current.User is WindowsIdentity)
        //    {
        //        throw new InvalidOperationException("Windows authentication is not supported.");
        //    }

        //    if (HttpContext.Current.User is AxxessPrincipal)
        //    {
        //        AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
        //        result = principal.HasPermission(permission);
        //    }
        //    return result;
        //}

        public static bool HasRight(AgencyServices service, ParentPermission category, PermissionActions action)
        {
            return HasRight((int)service, category, action);
            //bool result = false;
            //if (HttpContext.Current.User is WindowsIdentity)
            //{
            //    throw new InvalidOperationException("Windows authentication is not supported.");
            //}
            //if (User != null && User.Session != null)
            //{ 
            //    if ((User.Session.AcessibleServices & service) == service || (User.Session.AcessibleServices & service) == User.Session.AcessibleServices)
            //    {
            //        if (User.Session.Permissions.IsNotNullOrEmpty())
            //        {
            //            var categoryList = new Dictionary<int, List<int>>();
            //            if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
            //            {
            //                List<int> exstingService;
            //                if (categoryList.TryGetValue((int)action, out exstingService))
            //                {
            //                    var comingSerive = (int)service;
            //                    return exstingService != null && exstingService.Exists(s => ((comingSerive & s) == s));
            //                }
            //            }
            //        }
            //    }
            //}
            //return result;
        }


        public static bool HasRight(int service, ParentPermission category, PermissionActions action)
        {
            bool result = false;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }
            if (User != null && User.Session != null)
            {
                if (((int)User.Session.AcessibleServices & service) == service || ((int)User.Session.AcessibleServices & service) == (int)User.Session.AcessibleServices)
                {
                    if (User.Session.Permissions.IsNotNullOrEmpty())
                    {
                        var categoryList = new Dictionary<int, List<int>>();
                        if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
                        {
                            List<int> exstingService;
                            if (categoryList.TryGetValue((int)action, out exstingService))
                            {
                                var comingSerive = (int)service;
                                return exstingService != null && exstingService.Exists(s => ((comingSerive & s) == s));
                            }
                        }
                    }
                }
            }
            return result;
        }


        public static bool HasRightAtLeastOne(AgencyServices service, ParentPermission category, PermissionActions[] actions)
        {
            bool result = false;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (User != null && User.Session != null)
            {
                if ((User.Session.AcessibleServices & service) == service)
                {
                    if (User.Session.Permissions.IsNotNullOrEmpty())
                    {
                        var categoryList = new Dictionary<int, List<int>>();
                        if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
                        {
                            foreach (var action in actions)
                            {
                                List<int> exstingService;
                                if (categoryList.TryGetValue((int)action, out exstingService))
                                {
                                    var comingSerive = (int)service;
                                    if (exstingService != null && exstingService.Exists(s => ((comingSerive & s) == s)))
                                    {
                                        result = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static AgencyServices AvailableService(AgencyServices service, ParentPermission category, PermissionActions action)
        {
            AgencyServices result = AgencyServices.None;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (User != null && User.Session != null)
            {
                if ((User.Session.AcessibleServices & service) == service)
                {
                    if (User.Session.Permissions.IsNotNullOrEmpty())
                    {
                        var categoryList = new Dictionary<int, List<int>>();
                        if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
                        {
                            List<int> exstingService;
                            if (categoryList.TryGetValue((int)action, out exstingService))
                            {
                                var comingSerive = (int)service;
                                if (exstingService != null)
                                {
                                    exstingService.ForEach(s =>
                                        {
                                            if (((comingSerive & s) == s) && Enum.IsDefined(typeof(AgencyServices), s))
                                            {
                                                result |= (AgencyServices)s;
                                            }
                                        });
                                }
                               // return exstingService != null && exstingService.Exists(s => ((comingSerive & s) == s));
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static Dictionary<int, AgencyServices> CategoryService(AgencyServices service, ParentPermission category, int[] actions)
        {
            var result = new Dictionary<int, AgencyServices>();
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (User != null && User.Session != null)
            {
                if ((User.Session.AcessibleServices & service) == service)
                {
                    if (User.Session.Permissions.IsNotNullOrEmpty())
                    {
                        var categoryList = new Dictionary<int, List<int>>();
                        if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
                        {
                            var comingSerive = (int)service;
                            categoryList.ForEach((key, value) =>
                            {
                                if (actions.Contains(key))
                                {
                                    AgencyServices actionService = AgencyServices.None;
                                    value.ForEach(s =>
                                    {
                                        if (((comingSerive & s) == s) && Enum.IsDefined(typeof(AgencyServices), s))
                                        {
                                            actionService |= (AgencyServices)s;
                                        }
                                    });
                                    if (actionService != AgencyServices.None)
                                    {
                                        result.Add(key, actionService);
                                    }
                                }
                            });

                        }
                    }
                }
            }
            return result;
        }

        public static IDictionary<int,Dictionary<int,List<int>>> Permissions
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.Permissions;
                }
                else
                {
                    return new Dictionary<int, Dictionary<int, List<int>>>();
                }
            }
        }

        //public static IDictionary<int, Dictionary<int, List<int>>> ReportPermissions
        //{
        //    get
        //    {
        //        if (User != null && User.Session != null)
        //        {
        //            return User.Session.ReportPermissions;
        //        }
        //        else
        //        {
        //            return new Dictionary<int, Dictionary<int, List<int>>>();
        //        }
        //    }
        //}

        public static bool IsInRole(AgencyRoles roleId)
        {
            var result = false;
            if (roleId > 0)
            {
                var role = ((int)roleId).ToString();
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    var agencyRoles = User.Session.AgencyRoles.Split(';');
                    if (agencyRoles.Length > 0)
                    {
                        foreach (string agencyRole in agencyRoles)
                        {
                            if (role.IsEqual(agencyRole))
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsAgencyAdmin
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsAgencyAdmin();
                }
                return false;
            }
        }

        public static bool IsDirectorOfNursing
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsDirectorOfNursing();
                }
                return false;
            }
        }

        public static bool IsClinician
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClinician();
                }
                return false;
            }
        }

        public static bool IsClinicianOrHHA
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClinicianOrHHA();
                }
                return false;
            }
        }

        public static bool IsCaseManager
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsCaseManager();
                }
                return false;
            }
        }

        public static bool IsBiller
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsBiller();
                }
                return false;
            }
        }

        public static bool IsClerk
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClerk();
                }
                return false;
            }
        }

        public static bool IsScheduler
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsScheduler();
                }
                return false;
            }
        }

        public static bool IsQA
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsQA();
                }
                return false;
            }
        }

        public static bool IsOfficeManager
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsOfficeManager();
                }
                return false;
            }
        }

        public static bool IsCommunityLiason
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsCommunityLiason();
                }
                return false;
            }
        }

        public static bool IfOnlyRole(AgencyRoles role)
        {
            var result = false;
            if (User != null && User.Session != null && User.Session.AgencyRoles != null)
            {
                var userRoles = User.Session.AgencyRoles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                if (userRoles.Length == 1 && userRoles[0].IsEqual(((int)role).ToString()))
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool IsAgencyFrozen
        {
            get
            {
                if (User != null && User.Session != null && User.Session.IsAgencyFrozen)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool IsUserPrime
        {
            get
            {
                if (User != null && User.Session != null && User.Session.IsPrimary)
                {
                    return true;
                }
                return false;
            }
        }

        public static List<SingleUser> ActiveUsers
        {
            get
            {
                return authenticationAgent.GetUsers(true);
            }
        }

        public static List<SingleUser> InActiveUsers
        {
            get
            {
                return authenticationAgent.GetUsers(false);
            }
        }

        public static bool OasisVendorExist
        {
            get
            {
                if (User != null && User.Session != null )
                {
                    return User.Session.OasisVendorExist;
                }
                return false;
            }
        }
        
        public static AgencyServices AcessibleServices
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AcessibleServices;
                }
                else
                {
                    return AgencyServices.None;
                }
            }
        }

        public static AgencyServices PreferredService
        {
            get
            {
                if (AcessibleServices.IsAlone())
                {
                    return AcessibleServices;
                }
                else
                {
                    return User.Session.PreferredService == AgencyServices.None ? AgencyServices.HomeHealth : User.Session.PreferredService;
                }
            }
        }

        public static bool IsTestingAgency
        {
            get
            {
                if (Current.AgencyId.ToSafeString().ToLowerCase().IsEqual("62b904fa-b38f-4686-b2e4-d748fa129c50"))
                {
                    return true;
                }
                return false;
            }
        }

        public static string DBServerIp
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DBServerIp;
                }

                return string.Empty;
            }
        }

        public static string Payor
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.Payor;
                }

                return string.Empty;
            }
        }

        public static AgencyServices Services
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.Services;
                }
                else
                {
                    return AgencyServices.None;
                }
            }
        }
    }
}
