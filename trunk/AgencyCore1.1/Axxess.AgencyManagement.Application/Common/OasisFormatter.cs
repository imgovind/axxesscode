﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;

    public static class OasisFormatter
    {
        public static string Format(IDictionary<string, Question> assessmentQuestions, int versionNumber, AgencyLocation patientLocation, Dictionary<string, SubmissionBodyFormat> submissionGuide)
        {
            if (submissionGuide != null && submissionGuide.Count > 0)
            {
                var submissionFormat = new StringBuilder();
                submissionFormat.Capacity = 1446;
                string type = assessmentQuestions.AnswerOrEmptyString("M0100AssessmentType");
                submissionFormat.Append("B1"); //REC_ID
                submissionFormat.Append(string.Empty.PadRight(2)); //REC_TYPE
                submissionFormat.Append(string.Empty.PadRight(8)); //LOCK_DATE
                submissionFormat.Append(versionNumber.ToString().PadLeft(2, '0')); //CORRECTION_NUM submissionFormat.Append("00"); //
                submissionFormat.Append(string.Empty.PadRight(8)); //ACY_DOC_CD
                submissionFormat.Append("C-072009".PadRight(12)); //VERSION_CD1
                submissionFormat.Append("02.00".PadRight(5)); //VERSION_CD2
                submissionFormat.Append("364649717".PadRight(9)); //SFTW_ID
                submissionFormat.Append("1.0".PadRight(5)); //SFT_VER
                if (patientLocation != null && patientLocation.HomeHealthAgencyId.IsNotNullOrEmpty()) //HHA_AGENCY_ID
                {
                    submissionFormat.Append(patientLocation.HomeHealthAgencyId.Trim().ToUpper().PartOfString(0, 16).PadRight(16));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(16));
                }
                submissionFormat.Append(string.Empty.PadRight(14)); //PAT_ID
                submissionFormat.Append(string.Empty.PadRight(2));  //ST_CODE
                submissionFormat.Append(string.Empty.PadRight(4));  //ST_ERR_CNT
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_PMT_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_KEY_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_DELETE
                submissionFormat.Append(string.Empty.PadRight(1));  //MC_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //MC_PMT_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //MC_KEY_COR
                submissionFormat.Append(string.Empty.PadRight(20)); //MASK_VERSION_CD
                submissionFormat.Append(string.Empty.PadRight(7));  //CNT_FILLER

                if (patientLocation != null && patientLocation.MedicareProviderNumber.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(patientLocation.MedicareProviderNumber.Trim().ToUpper().PartOfString(0, 6).PadRight(6));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(6));
                }

                submissionFormat.Append(string.Empty.PadRight(15)); //M0014_BRANCH_STATE

                if (submissionGuide.ContainsKey("M0014_BRANCH_STATE") && patientLocation != null && patientLocation.AddressStateCode.IsNotNullOrEmpty()) //M0014_BRANCH_STATE
                {
                    submissionFormat.Append(patientLocation.AddressStateCode.Trim().ToUpper().PartOfString(0, 2).PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M0016_BRANCH_ID") && patientLocation != null && patientLocation.BranchId.IsNotNullOrEmpty()) //M0016_BRANCH_ID
                {
                    if (patientLocation.BranchId.Trim().IsEqual("N") || patientLocation.BranchId.Trim().IsEqual("P"))
                    {
                        submissionFormat.Append(patientLocation.BranchId.Trim().ToUpperCase().PartOfString(0, 1).PadRight(1));
                        submissionFormat.Append(string.Empty.PadRight(9));
                    }
                    else if (patientLocation.BranchId.Trim().IsEqual("Other") && patientLocation.BranchIdOther.IsNotNullOrEmpty() && patientLocation.BranchIdOther.Length == 10)
                    {
                        submissionFormat.Append(patientLocation.BranchIdOther.Trim().ToUpper().PartOfString(0, 10).PadRight(10));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append("N");
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("M0020_PAT_ID") && assessmentQuestions.ContainsKey("M0020PatientIdNumber") && assessmentQuestions["M0020PatientIdNumber"].Answer.IsNotNullOrEmpty()) //M0020_PAT_ID
                {
                    submissionFormat.Append(assessmentQuestions["M0020PatientIdNumber"].Answer.Trim().ToUpper().PartOfString(0, 20).PadRight(20));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("M0030_START_CARE_DT") && assessmentQuestions.ContainsKey("M0030SocDate") && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0030SocDate"].Answer.IsValidDate()) //M0030_START_CARE_DT
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));

                }

                if ((submissionGuide.ContainsKey("M0032_ROC_DT_NA") && assessmentQuestions.ContainsKey("M0032ROCDateNotApplicable") && !assessmentQuestions["M0032ROCDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0032_ROC_DT_NA") && !assessmentQuestions.ContainsKey("M0032ROCDateNotApplicable"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0032_ROC_DT") && assessmentQuestions.ContainsKey("M0032ROCDate") && assessmentQuestions["M0032ROCDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0032ROCDate"].Answer.IsValidDate()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0032ROCDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                    submissionFormat.Append("1".PadRight(1));
                }
                if (submissionGuide.ContainsKey("M0040_PAT_FNAME") && assessmentQuestions.ContainsKey("M0040FirstName") && assessmentQuestions["M0040FirstName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_FNAME
                {

                    submissionFormat.Append(assessmentQuestions["M0040FirstName"].Answer.Trim().ToUpper().PartOfString(0, 12).PadRight(12));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(12));
                }

                if (submissionGuide.ContainsKey("M0040_PAT_MI") && assessmentQuestions.ContainsKey("M0040MI") && assessmentQuestions["M0040MI"].Answer.IsNotNullOrEmpty()) //M0040_PAT_MI
                {

                    submissionFormat.Append(assessmentQuestions["M0040MI"].Answer.Trim().ToUpper().PartOfString(0, 1).PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (submissionGuide.ContainsKey("M0040_PAT_LNAME") && assessmentQuestions.ContainsKey("M0040LastName") && assessmentQuestions["M0040LastName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_LNAME
                {

                    submissionFormat.Append(assessmentQuestions["M0040LastName"].Answer.Trim().ToUpper().PartOfString(0, 18).PadRight(18));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(18));
                }

                if (submissionGuide.ContainsKey("M0040_PAT_SUFFIX") && assessmentQuestions.ContainsKey("M0040Suffix") && assessmentQuestions["M0040Suffix"].Answer.IsNotNullOrEmpty()) //M0040_PAT_SUFFIX
                {

                    submissionFormat.Append(assessmentQuestions["M0040Suffix"].Answer.Trim().ToUpper().PartOfString(0, 3).PadRight(3));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(3));
                }

                if (submissionGuide.ContainsKey("M0050_PAT_ST") && assessmentQuestions.ContainsKey("M0050PatientState") && assessmentQuestions["M0050PatientState"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {

                    submissionFormat.Append(assessmentQuestions["M0050PatientState"].Answer.Trim().ToUpper().PartOfString(0, 2).PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M0060_PAT_ZIP") && assessmentQuestions.ContainsKey("M0060PatientZipCode") && assessmentQuestions["M0060PatientZipCode"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {

                    submissionFormat.Append(assessmentQuestions["M0060PatientZipCode"].Answer.Trim().ToUpper().PartOfString(0, 11).PadRight(11));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if ((submissionGuide.ContainsKey("M0063_MEDICARE_NA") && assessmentQuestions.ContainsKey("M0063PatientMedicareNumberUnknown") && !assessmentQuestions["M0063PatientMedicareNumberUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0032_ROC_DT_NA") && !assessmentQuestions.ContainsKey("M0063PatientMedicareNumberUnknown"))) // M0063_MEDICARE_NUM and M0063_MEDICARE_NA
                {

                    if (submissionGuide.ContainsKey("M0063_MEDICARE_NUM") && assessmentQuestions.ContainsKey("M0063PatientMedicareNumber") && assessmentQuestions["M0063PatientMedicareNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M0063PatientMedicareNumber"].Answer.Replace("-", "").Trim().ToUpper().PartOfString(0, 12).PadRight(12));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(12));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(12));
                    submissionFormat.Append("1".PadRight(1));
                }


                if ((submissionGuide.ContainsKey("M0064_SSN_UK") && assessmentQuestions.ContainsKey("M0064PatientSSNUnknown") && !assessmentQuestions["M0064PatientSSNUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0064_SSN_UK") && !assessmentQuestions.ContainsKey("M0064PatientSSNUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0064_SSN") && assessmentQuestions.ContainsKey("M0064PatientSSN") && assessmentQuestions["M0064PatientSSN"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M0064PatientSSN"].Answer.Trim().ToUpper().PartOfString(0, 9).PadRight(9));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(9));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                    submissionFormat.Append("1".PadRight(1));
                }

                if ((submissionGuide.ContainsKey("M0065_MEDICAID_NA") && assessmentQuestions.ContainsKey("M0065PatientMedicaidNumberUnknown") && !assessmentQuestions["M0065PatientMedicaidNumberUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0065_MEDICAID_NA") && !assessmentQuestions.ContainsKey("M0065PatientMedicaidNumberUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0065_MEDICAID_NUM") && assessmentQuestions.ContainsKey("M0065PatientMedicaidNumber") && assessmentQuestions["M0065PatientMedicaidNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M0065PatientMedicaidNumber"].Answer.Trim().ToUpper().PartOfString(0, 14).PadRight(14));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(14));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(14));
                    submissionFormat.Append("1".PadRight(1));
                }


                if (submissionGuide.ContainsKey("M0066_PAT_BIRTH_DT") && assessmentQuestions.ContainsKey("M0066PatientDoB") && assessmentQuestions["M0066PatientDoB"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0066PatientDoB"].Answer.IsValidDate()) //M0050_PAT_ST
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0066PatientDoB"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                }
                submissionFormat.Append(string.Empty.PadRight(1));

                if (submissionGuide.ContainsKey("M0069_PAT_GENDER") && assessmentQuestions.ContainsKey("M0069Gender") && assessmentQuestions["M0069Gender"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0069Gender"].Answer.Trim().ToUpper().PartOfString(0, 1).PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }



                if ((submissionGuide.ContainsKey("M0018_PHYSICIAN_UK") && assessmentQuestions.ContainsKey("M0018NationalProviderIdUnknown") && !assessmentQuestions["M0018NationalProviderIdUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0018_PHYSICIAN_UK") && !assessmentQuestions.ContainsKey("M0018NationalProviderIdUnknown")))
                {

                    if (submissionGuide.ContainsKey("M0018_PHYSICIAN_ID") && assessmentQuestions.ContainsKey("M0018NationalProviderId") && assessmentQuestions["M0018NationalProviderId"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(assessmentQuestions["M0018NationalProviderId"].Answer.Trim().ToUpper().PartOfString(0, 10).PadRight(10));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                    submissionFormat.Append("1".PadRight(1));
                }

                if (submissionGuide.ContainsKey("M0080_ASSESSOR_DISCIPLINE") && assessmentQuestions.ContainsKey("M0080DisciplinePerson") && assessmentQuestions["M0080DisciplinePerson"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0080DisciplinePerson"].Answer.Trim().ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M0090_INFO_COMPLETED_DT") && assessmentQuestions.ContainsKey("M0090AssessmentCompleted") && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsValidDate()) //M0050_PAT_ST
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0090AssessmentCompleted"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadLeft(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }

                if (submissionGuide.ContainsKey("M0100_ASSMT_REASON") && assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0100AssessmentType"].Answer.Trim().ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M0140_ETHNIC_AI_AN") && assessmentQuestions.ContainsKey("M0140RaceAMorAN") && assessmentQuestions["M0140RaceAMorAN"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceAMorAN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_ASIAN") && assessmentQuestions.ContainsKey("M0140RaceAsia") && assessmentQuestions["M0140RaceAsia"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceAsia"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_BLACK") && assessmentQuestions.ContainsKey("M0140RaceBalck") && assessmentQuestions["M0140RaceBalck"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceBalck"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_HISP") && assessmentQuestions.ContainsKey("M0140RaceHispanicOrLatino") && assessmentQuestions["M0140RaceHispanicOrLatino"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceHispanicOrLatino"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_NH_PI") && assessmentQuestions.ContainsKey("M0140RaceNHOrPI") && assessmentQuestions["M0140RaceNHOrPI"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceNHOrPI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_WHITE") && assessmentQuestions.ContainsKey("M0140RaceWhite") && assessmentQuestions["M0140RaceWhite"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceWhite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                submissionFormat.Append(string.Empty.PadLeft(1));


                if (submissionGuide.ContainsKey("M0150_CPAY_NONE") && assessmentQuestions.ContainsKey("M0150PaymentSourceNone") && assessmentQuestions["M0150PaymentSourceNone"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceNone"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCARE_FFS") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCREFFS") && assessmentQuestions["M0150PaymentSourceMCREFFS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCREFFS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCARE_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCREHMO") && assessmentQuestions["M0150PaymentSourceMCREHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCREHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCAID_FFS") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCAIDFFS") && assessmentQuestions["M0150PaymentSourceMCAIDFFS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCAIDFFS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCAID_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourceMACIDHMO") && assessmentQuestions["M0150PaymentSourceMACIDHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMACIDHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }


                if (submissionGuide.ContainsKey("M0150_CPAY_WRKCOMP") && assessmentQuestions.ContainsKey("M0150PaymentSourceWRKCOMP") && assessmentQuestions["M0150PaymentSourceWRKCOMP"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceWRKCOMP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }


                if (submissionGuide.ContainsKey("M0150_CPAY_TITLEPGMS") && assessmentQuestions.ContainsKey("M0150PaymentSourceTITLPRO") && assessmentQuestions["M0150PaymentSourceTITLPRO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceTITLPRO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_OTH_GOVT") && assessmentQuestions.ContainsKey("M0150PaymentSourceOTHGOVT") && assessmentQuestions["M0150PaymentSourceOTHGOVT"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceOTHGOVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_PRIV_INS") && assessmentQuestions.ContainsKey("M0150PaymentSourcePRVINS") && assessmentQuestions["M0150PaymentSourcePRVINS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourcePRVINS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_PRIV_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourcePRVHMO") && assessmentQuestions["M0150PaymentSourcePRVHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourcePRVHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_SELFPAY") && assessmentQuestions.ContainsKey("M0150PaymentSourceSelfPay") && assessmentQuestions["M0150PaymentSourceSelfPay"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceSelfPay"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }


                if (submissionGuide.ContainsKey("M0150_CPAY_OTHER") && assessmentQuestions.ContainsKey("M0150PaymentSourceOtherSRS") && assessmentQuestions["M0150PaymentSourceOtherSRS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceOtherSRS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }
                if (type.Contains("03") || type.Contains("01"))
                {

                    if (submissionGuide.ContainsKey("M0150_CPAY_UK") && assessmentQuestions.ContainsKey("M0150PaymentSourceUnknown") && assessmentQuestions["M0150PaymentSourceUnknown"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0150PaymentSourceUnknown"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadRight(6));
                submissionFormat.Append(string.Empty.PadRight(5));
                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }
                    else
                    {
                        if ((submissionGuide.ContainsKey("M1005_INP_DSCHG_UNKNOWN") && assessmentQuestions.ContainsKey("M1005InpatientDischargeDateUnknown") && !assessmentQuestions["M1005InpatientDischargeDateUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M1005_INP_DSCHG_UNKNOWN") && !assessmentQuestions.ContainsKey("M1005InpatientDischargeDateUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                        {

                            if (submissionGuide.ContainsKey("M1005_INP_DISCHARGE_DT") && assessmentQuestions.ContainsKey("M1005InpatientDischargeDate") && assessmentQuestions["M1005InpatientDischargeDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1005InpatientDischargeDate"].Answer.IsValidDate()) //M0010_CCN
                            {
                                submissionFormat.Append(DateTime.Parse(assessmentQuestions["M1005InpatientDischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(8));
                            }
                            submissionFormat.Append("0".PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                            submissionFormat.Append("1".PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                    {
                        submissionFormat.Append(string.Empty.PadLeft(14));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP1_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode1") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP2_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode2") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                submissionFormat.Append(string.Empty.PadLeft(1));

                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD_NA") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(28));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD1") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode1") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD2") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode2") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD3") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode3") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD4") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode4") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                    }

                    if (submissionGuide.ContainsKey("M1018_PRIOR_UNKNOWN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUK")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000001");
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NOCHG_14D") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNA"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000010");
                            }
                            else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append("000000100");
                                }
                                else
                                {
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    submissionFormat.Append("0");
                                    submissionFormat.Append("0");
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append(string.Empty.PadLeft(1));
                                submissionFormat.Append("0");
                                submissionFormat.Append("0");
                            }
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000100");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                                submissionFormat.Append(string.Empty.PadLeft(1));
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");

                        }
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NOCHG_14D") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNA"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000010");
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000100");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                submissionFormat.Append("0");
                                submissionFormat.Append("0");
                                submissionFormat.Append(string.Empty.PadLeft(1));

                            }

                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000100");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(28));
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1020ICD9M"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1020ICD9M"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        if (!assessmentQuestions["M1020ICD9M"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_SEVERITY") && assessmentQuestions.ContainsKey("M1020SymptomControlRating") && assessmentQuestions["M1020SymptomControlRating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1020SymptomControlRating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }


                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M1") && assessmentQuestions["M1022ICD9M1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose1Rating") && assessmentQuestions["M1022OtherDiagnose1Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose1Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }


                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M2") && assessmentQuestions["M1022ICD9M2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        if (!assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().Trim().StartsWith("V"))
                        {

                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose2Rating") && assessmentQuestions["M1022OtherDiagnose2Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose2Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M3") && assessmentQuestions["M1022ICD9M3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {

                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose3Rating") && assessmentQuestions["M1022OtherDiagnose3Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose3Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M4") && assessmentQuestions["M1022ICD9M4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose4Rating") && assessmentQuestions["M1022OtherDiagnose4Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose4Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M5") && assessmentQuestions["M1022ICD9M5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose5Rating") && assessmentQuestions["M1022OtherDiagnose5Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose5Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1030_THH_NONE_ABOVE") && assessmentQuestions.ContainsKey("M1030HomeTherapiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1030HomeTherapiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1030_THH_IV_INFUSION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesInfusion")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1030_THH_PAR_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesParNutrition")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1030_THH_ENT_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesEntNutrition")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1030_THH_IV_INFUSION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesInfusion")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1030_THH_PAR_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesParNutrition")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1030_THH_ENT_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesEntNutrition")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(54));
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }

                submissionFormat.Append(string.Empty.PadLeft(6));

                if (type.Contains("01") || type.Contains("03"))
                {

                    if (submissionGuide.ContainsKey("M1036_RSK_NONE") && assessmentQuestions.ContainsKey("M1036RiskFactorsNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1036RiskFactorsNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000010");
                        }
                        else if (submissionGuide.ContainsKey("M1036_RSK_UNKNOWN") && assessmentQuestions.ContainsKey("M1036RiskFactorsUnknown"))
                        {
                            if (assessmentQuestions["M1036RiskFactorsUnknown"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000001");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("00");
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                    }
                    else if (submissionGuide.ContainsKey("M1036_RSK_UNKNOWN") && assessmentQuestions.ContainsKey("M1036RiskFactorsUnknown"))
                    {
                        if (assessmentQuestions["M1036RiskFactorsUnknown"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                submissionFormat.Append(string.Empty.PadLeft(55));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M1200_VISION") && assessmentQuestions.ContainsKey("M1200Vision") && assessmentQuestions["M1200Vision"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1200Vision"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1230_SPEECH") && assessmentQuestions.ContainsKey("M1230SpeechAndOral") && assessmentQuestions["M1230SpeechAndOral"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1230SpeechAndOral"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1322_NBR_PRSULC_STG1") && assessmentQuestions.ContainsKey("M1322CurrentNumberStageIUlcer") && assessmentQuestions["M1322CurrentNumberStageIUlcer"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1322CurrentNumberStageIUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(7));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1324_STG_PRBLM_ULCER") && assessmentQuestions.ContainsKey("M1324MostProblematicUnhealedStage") && assessmentQuestions["M1324MostProblematicUnhealedStage"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1324MostProblematicUnhealedStage"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                submissionFormat.Append(string.Empty.PadLeft(14));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1400_WHEN_DYSPNEIC") && assessmentQuestions.ContainsKey("M1400PatientDyspneic") && assessmentQuestions["M1400PatientDyspneic"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1400PatientDyspneic"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1410_RESPTX_NONE") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1410HomeRespiratoryTreatmentsNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1410_RESPTX_OXYGEN") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1410_RESPTX_VENTILATOR") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1410_RESPTX_AIRPRESS") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1410_RESPTX_OXYGEN") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1410_RESPTX_VENTILATOR") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1410_RESPTX_AIRPRESS") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1600_UTI") && assessmentQuestions.ContainsKey("M1600UrinaryTractInfection") && assessmentQuestions["M1600UrinaryTractInfection"].Answer.IsNotNullOrEmpty()) //M1600_UTI
                    {
                        submissionFormat.Append(assessmentQuestions["M1600UrinaryTractInfection"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1610UrinaryIncontinence"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                submissionFormat.Append(string.Empty.PadLeft(2));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1620_BWL_INCONT") && assessmentQuestions.ContainsKey("M1620BowelIncontinenceFrequency") && assessmentQuestions["M1620BowelIncontinenceFrequency"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1620BowelIncontinenceFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M1630_OSTOMY") && assessmentQuestions.ContainsKey("M1630OstomyBowelElimination") && assessmentQuestions["M1630OstomyBowelElimination"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1630OstomyBowelElimination"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1700_COG_FUNCTION") && assessmentQuestions.ContainsKey("M1700CognitiveFunctioning") && assessmentQuestions["M1700CognitiveFunctioning"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1700CognitiveFunctioning"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1710_WHEN_CONFUSED") && assessmentQuestions.ContainsKey("M1710WhenConfused") && assessmentQuestions["M1710WhenConfused"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1710WhenConfused"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1720_WHEN_ANXIOUS") && assessmentQuestions.ContainsKey("M1720WhenAnxious") && assessmentQuestions["M1720WhenAnxious"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1720WhenAnxious"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(13));

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1740_BD_NONE") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0000001");
                        }

                        else
                        {
                            if (submissionGuide.ContainsKey("M1740_BD_MEM_DEFICIT") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_IMP_DECISN") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_VERBAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_PHYSICAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_SOC_INAPPRO") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_DELUSIONS") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1740_BD_MEM_DEFICIT") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_IMP_DECISN") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_VERBAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_PHYSICAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_SOC_INAPPRO") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_DELUSIONS") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }


                    if (submissionGuide.ContainsKey("M1745_BEH_PROB_FREQ") && assessmentQuestions.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && assessmentQuestions["M1745DisruptiveBehaviorSymptomsFrequency"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1745DisruptiveBehaviorSymptomsFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1750_REC_PSYCH_NURS") && assessmentQuestions.ContainsKey("M1750PsychiatricNursingServicing") && assessmentQuestions["M1750PsychiatricNursingServicing"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1750PsychiatricNursingServicing"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));


                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1800_CUR_GROOMING") && assessmentQuestions.ContainsKey("M1800Grooming") && assessmentQuestions["M1800Grooming"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1800Grooming"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1810_CUR_DRESS_UPPER") && assessmentQuestions.ContainsKey("M1810CurrentAbilityToDressUpper") && assessmentQuestions["M1810CurrentAbilityToDressUpper"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1810CurrentAbilityToDressUpper"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1820_CUR_DRESS_LOWER") && assessmentQuestions.ContainsKey("M1820CurrentAbilityToDressLower") && assessmentQuestions["M1820CurrentAbilityToDressLower"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1820CurrentAbilityToDressLower"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(18));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1870_CUR_FEEDING") && assessmentQuestions.ContainsKey("M1870FeedingOrEating") && assessmentQuestions["M1870FeedingOrEating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1870FeedingOrEating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1880_CUR_PREP_LT_MEALS") && assessmentQuestions.ContainsKey("M1880AbilityToPrepareLightMeal") && assessmentQuestions["M1880AbilityToPrepareLightMeal"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1880AbilityToPrepareLightMeal"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(18));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1890_CUR_PHONE_USE") && assessmentQuestions.ContainsKey("M1890AbilityToUseTelephone") && assessmentQuestions["M1890AbilityToUseTelephone"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1890AbilityToUseTelephone"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(21));
                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2310_ECR_MEDICATION") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMed")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_MEDICATION") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMed")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));
                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2310_ECR_HYPOGLYC") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHypo")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.IsNotNullOrEmpty())
                                    {

                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_HYPOGLYC") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHypo")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.IsNotNullOrEmpty())
                                {

                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    //if (type.Contains("09"))
                    //{
                    //    submissionFormat.Append("NA");
                    //}
                    //else
                    //{
                    if (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    // }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(7));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_MED") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMed")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_MED") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMed")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(3));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_HYPOGLYC") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHypo")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_HYPOGLYC") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHypo")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_UR_TRACT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUrinaryInf")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_UR_TRACT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUrinaryInf")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(1));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("00");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDVT")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDVT")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "01") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2440_NH_UNKNOWN") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnknown")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedUnknown"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0000001");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2440_NH_THERAPY") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedTherapy")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_RESPITE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedRespite")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_HOSPICE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedHospice")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_PERMANENT") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedPermanent")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_UNSAFE_HOME") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnsafe")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_OTHER") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedOther")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M2440_NH_THERAPY") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedTherapy")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_RESPITE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedRespite")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_HOSPICE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedHospice")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_PERMANENT") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedPermanent")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_UNSAFE_HOME") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnsafe")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_OTHER") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedOther")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("08") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M0903_LAST_HOME_VISIT") && assessmentQuestions.ContainsKey("M0903LastHomeVisitDate") && assessmentQuestions["M0903LastHomeVisitDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0903LastHomeVisitDate"].Answer.IsValidDate()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0903LastHomeVisitDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    if (submissionGuide.ContainsKey("M0906_DC_TRAN_DTH_DT") && assessmentQuestions.ContainsKey("M0906DischargeDate") && assessmentQuestions["M0906DischargeDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0906DischargeDate"].Answer.IsValidDate()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0906DischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }
                submissionFormat.Append(string.Empty.PadLeft(6));

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1000_DC_SNF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesSNF")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1000_DC_SNF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesSNF")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(14));
                if (submissionGuide.ContainsKey("NATL_PROV_ID") && patientLocation.NationalProviderNumber.IsNotNullOrEmpty() && patientLocation.NationalProviderNumber.Length == 10) //M0050_PAT_ST
                {
                    submissionFormat.Append(patientLocation.NationalProviderNumber.ToUpper().PartOfString(0, 10).PadLeft(10));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M0110_EPISODE_TIMING") && assessmentQuestions.ContainsKey("M0110EpisodeTiming") && assessmentQuestions["M0110EpisodeTiming"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0110EpisodeTiming"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_A3") && assessmentQuestions.ContainsKey("M1024ICD9MA3") && assessmentQuestions["M1024ICD9MA3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MA3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M1") && assessmentQuestions["M1022ICD9M1"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_B3") && assessmentQuestions.ContainsKey("M1024ICD9MB3") && assessmentQuestions["M1024ICD9MB3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MB3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M2") && assessmentQuestions["M1022ICD9M2"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_C3") && assessmentQuestions.ContainsKey("M1024ICD9MC3") && assessmentQuestions["M1024ICD9MC3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MC3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M3") && assessmentQuestions["M1022ICD9M3"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_D3") && assessmentQuestions.ContainsKey("M1024ICD9MD3") && assessmentQuestions["M1024ICD9MD3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MD3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M4") && assessmentQuestions["M1022ICD9M4"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_E3") && assessmentQuestions.ContainsKey("M1024ICD9ME3") && assessmentQuestions["M1024ICD9ME3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9ME3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M5") && assessmentQuestions["M1022ICD9M5"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_F3") && assessmentQuestions.ContainsKey("M1024ICD9MF3") && assessmentQuestions["M1024ICD9MF3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MF3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_A4") && assessmentQuestions.ContainsKey("M1024ICD9MA4") && assessmentQuestions["M1024ICD9MA4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MA4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M1") && assessmentQuestions["M1022ICD9M1"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_B4") && assessmentQuestions.ContainsKey("M1024ICD9MB4") && assessmentQuestions["M1024ICD9MB4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MB4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M2") && assessmentQuestions["M1022ICD9M2"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_C4") && assessmentQuestions.ContainsKey("M1024ICD9MC4") && assessmentQuestions["M1024ICD9MC4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MC4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M3") && assessmentQuestions["M1022ICD9M3"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_D4") && assessmentQuestions.ContainsKey("M1024ICD9MD4") && assessmentQuestions["M1024ICD9MD4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MD4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M4") && assessmentQuestions["M1022ICD9M4"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_E4") && assessmentQuestions.ContainsKey("M1024ICD9ME4") && assessmentQuestions["M1024ICD9ME4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9ME4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M5") && assessmentQuestions["M1022ICD9M5"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_F4") && assessmentQuestions.ContainsKey("M1024ICD9MF4") && assessmentQuestions["M1024ICD9MF4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MF4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M2200_THER_NEED_NA") && assessmentQuestions.ContainsKey("M2200TherapyNeedNA") && assessmentQuestions["M2200TherapyNeedNA"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2200TherapyNeedNA"].Answer == "1")
                    {
                        submissionFormat.Append("000");
                        submissionFormat.Append("0");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2200_THER_NEED_NUM") && assessmentQuestions.ContainsKey("M2200NumberOfTherapyNeed") && assessmentQuestions["M2200NumberOfTherapyNeed"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2200NumberOfTherapyNeed"].Answer.PartOfString(0, 3).PadLeft(3, '0'));
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(3));
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(90));
                }
                submissionFormat.Append(string.Empty.PadLeft(1));
                if (type.Contains("01") || type.Contains("03"))
                {
                    if ((submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && !assessmentQuestions["M0102PhysicianOrderedDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && !assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                    {

                        if (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDate") && assessmentQuestions["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0102PhysicianOrderedDate"].Answer.IsValidDate()) //M0010_CCN
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0102PhysicianOrderedDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                        submissionFormat.Append("0");
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                        submissionFormat.Append("1");
                    }


                    if ((submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && !assessmentQuestions["M0102PhysicianOrderedDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && !assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable"))) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M0104_PHYSN_RFRL_DT") && assessmentQuestions.ContainsKey("M0104ReferralDate") && assessmentQuestions["M0104ReferralDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0104ReferralDate"].Answer.IsValidDate()) //M0010_CCN
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0104ReferralDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                    }

                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1000_DC_LTC_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTC")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_IPPS_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIPPS")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_LTCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTCH")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_IRF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIRF")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_PSYCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesPhych")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_OTH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesOTHR")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {


                        if (submissionGuide.ContainsKey("M1000_DC_LTC_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTC")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_IPPS_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIPPS")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_LTCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTCH")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_IRF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIRF")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_PSYCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesPhych")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_OTH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesOTHR")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                    {
                        submissionFormat.Append(string.Empty.PadLeft(58));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP3_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode3") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP4_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode4") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP5_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode5") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP6_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode6") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode6"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode6"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1012_INP_NA_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCodeNotApplicable") && assessmentQuestions["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer == "1") //M0010_CCN
                        {
                            submissionFormat.Append(string.Empty.PadLeft(28));
                            submissionFormat.Append("10");
                        }
                        else if ((submissionGuide.ContainsKey("M1012_INP_UK_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCodeUnknown") && assessmentQuestions["M1012InpatientFacilityProcedureCodeUnknown"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1012InpatientFacilityProcedureCodeUnknown"].Answer == "1"))
                        {
                            submissionFormat.Append(string.Empty.PadLeft(28));
                            submissionFormat.Append("01");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR1_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode1") && assessmentQuestions["M1012InpatientFacilityProcedureCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode1"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR2_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode2") && assessmentQuestions["M1012InpatientFacilityProcedureCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode2"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR3_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode3") && assessmentQuestions["M1012InpatientFacilityProcedureCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode3"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR4_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode4") && assessmentQuestions["M1012InpatientFacilityProcedureCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode4"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            submissionFormat.Append("00");
                        }
                    }
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD_NA") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(14));
                        submissionFormat.Append("1");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD5") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode5") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD6") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode6") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode6"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode6"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_NONE_ABOVE") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskNone")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_RCNT_DCLN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskRecentDecline")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_MLTPL_HOSPZTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMultipleHosp")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_HSTRY_FALLS") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskHistoryOfFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_5PLUS_MDCTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMedications")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_FRAILTY") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskFrailty")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_OTHR") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_RCNT_DCLN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskRecentDecline")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_MLTPL_HOSPZTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMultipleHosp")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_HSTRY_FALLS") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskHistoryOfFall")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_5PLUS_MDCTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMedications")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_FRAILTY") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskFrailty")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_OTHR") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskOther")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskOther"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1034_PTNT_OVRAL_STUS") && assessmentQuestions.ContainsKey("M1034OverallStatus") && assessmentQuestions["M1034OverallStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1034OverallStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(6));
                    submissionFormat.Append(string.Empty.PadLeft(56));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(14));
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(2));

                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1040InfluenzaVaccine"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if ((submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1040InfluenzaVaccine"].Answer == "01") || (submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1040InfluenzaVaccine"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1045_INFLNZ_RSN_NOT_RCVD") && assessmentQuestions.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && assessmentQuestions["M1045InfluenzaVaccineNotReceivedReason"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1045InfluenzaVaccineNotReceivedReason"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1050_PPV_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1050PneumococcalVaccine") && assessmentQuestions["M1050PneumococcalVaccine"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1050PneumococcalVaccine"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1050_PPV_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1050PneumococcalVaccine") && assessmentQuestions["M1050PneumococcalVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1050PneumococcalVaccine"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1055_PPV_RSN_NOT_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1055PPVNotReceivedReason") && assessmentQuestions["M1055PPVNotReceivedReason"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1055PPVNotReceivedReason"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1100_PTNT_LVG_STUTN") && assessmentQuestions.ContainsKey("M1100LivingSituation") && assessmentQuestions["M1100LivingSituation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1100LivingSituation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1210_HEARG_ABLTY") && assessmentQuestions.ContainsKey("M1210Hearing") && assessmentQuestions["M1210Hearing"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1210Hearing"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1220_UNDRSTG_VERBAL_CNTNT") && assessmentQuestions.ContainsKey("M1220VerbalContent") && assessmentQuestions["M1220VerbalContent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1220VerbalContent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1240_FRML_PAIN_ASMT") && assessmentQuestions.ContainsKey("M1240FormalPainAssessment") && assessmentQuestions["M1240FormalPainAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1240FormalPainAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1242_PAIN_FREQ_ACTVTY_MVMT") && assessmentQuestions.ContainsKey("M1242PainInterferingFrequency") && assessmentQuestions["M1242PainInterferingFrequency"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1242PainInterferingFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1300_PRSR_ULCR_RISK_ASMT") && assessmentQuestions.ContainsKey("M1300PressureUlcerAssessment") && assessmentQuestions["M1300PressureUlcerAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1300PressureUlcerAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1300_PRSR_ULCR_RISK_ASMT") && assessmentQuestions.ContainsKey("M1300PressureUlcerAssessment") && assessmentQuestions["M1300PressureUlcerAssessment"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1300PressureUlcerAssessment"].Answer == "00") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1302_RISK_OF_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1302RiskDevelopingPressureUlcers") && assessmentQuestions["M1302RiskDevelopingPressureUlcers"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1302RiskDevelopingPressureUlcers"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                if (type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(10));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1307_OLDST_STG2_AT_DSCHRG") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcer") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer == "02") //M0010_CCN
                        {
                            if (submissionGuide.ContainsKey("M1307_OLDST_STG2_ONST_DT") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.IsValidDate()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.ToDateTime().ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(8));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                        if (submissionGuide.ContainsKey("M1307_OLDST_STG2_AT_DSCHRG") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcer") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG2") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_STG2_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG3") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_STG3_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG4") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_STG4_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DRSG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DRSG_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_CVRG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_CVRG_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DEEP_TISUE") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DEEP_TISUE_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(14));
                    }
                    else
                    {
                        if ((submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG3") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer == "0") && (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG4") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer == "0") && (submissionGuide.ContainsKey("M1308_NSTG_CVRG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer == "0"))
                        {
                            submissionFormat.Append(string.Empty.PadLeft(12));
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M1310_PRSR_ULCR_LNGTH") && assessmentQuestions.ContainsKey("M1310PressureUlcerLength") && assessmentQuestions["M1310PressureUlcerLength"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1310PressureUlcerLength"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                                submissionFormat.Append(".");
                                if (assessmentQuestions.ContainsKey("M1310PressureUlcerLengthDecimal"))
                                {
                                    submissionFormat.Append(assessmentQuestions["M1310PressureUlcerLengthDecimal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1, '0'));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(4));
                            }
                            if (submissionGuide.ContainsKey("M1312_PRSR_ULCR_WDTH") && assessmentQuestions.ContainsKey("M1312PressureUlcerWidth") && assessmentQuestions["M1312PressureUlcerWidth"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1312PressureUlcerWidth"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                                submissionFormat.Append(".");
                                if (assessmentQuestions.ContainsKey("M1312PressureUlcerWidthDecimal"))
                                {
                                    submissionFormat.Append(assessmentQuestions["M1312PressureUlcerWidthDecimal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1, '0'));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(4));
                            }
                            if (submissionGuide.ContainsKey("M1314_PRSR_ULCR_DEPTH") && assessmentQuestions.ContainsKey("M1314PressureUlcerDepth") && assessmentQuestions["M1314PressureUlcerDepth"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1314PressureUlcerDepth"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                                submissionFormat.Append(".");
                                if (assessmentQuestions.ContainsKey("M1314PressureUlcerDepthDecimal"))
                                {
                                    submissionFormat.Append(assessmentQuestions["M1314PressureUlcerDepthDecimal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1, '0'));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(4));
                            }

                        }
                        if (submissionGuide.ContainsKey("M1320_STUS_PRBLM_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1320MostProblematicPressureUlcerStatus") && assessmentQuestions["M1320MostProblematicPressureUlcerStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1320MostProblematicPressureUlcerStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1330StasisUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1330StasisUlcer"].Answer == "00") || (submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1330StasisUlcer"].Answer == "03")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(4));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1332_NUM_STAS_ULCR") && assessmentQuestions.ContainsKey("M1332CurrentNumberStasisUlcer") && assessmentQuestions["M1332CurrentNumberStasisUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1332CurrentNumberStasisUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                        if (submissionGuide.ContainsKey("M1334_STUS_PRBLM_STAS_ULCR") && assessmentQuestions.ContainsKey("M1334StasisUlcerStatus") && assessmentQuestions["M1334StasisUlcerStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1334StasisUlcerStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1340SurgicalWound"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1340SurgicalWound"].Answer == "00") || (submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1340SurgicalWound"].Answer == "02")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1342_STUS_PRBLM_SRGCL_WND") && assessmentQuestions.ContainsKey("M1342SurgicalWoundStatus") && assessmentQuestions["M1342SurgicalWoundStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1342SurgicalWoundStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1350_LESION_OPEN_WND") && assessmentQuestions.ContainsKey("M1350SkinLesionOpenWound") && assessmentQuestions["M1350SkinLesionOpenWound"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1350SkinLesionOpenWound"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1500HeartFailureSymptons"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if ((submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "00") || (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "02") || (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(6));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_NO_ACTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupNoAction")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupNoAction"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("100000");
                            }
                            else
                            {
                                submissionFormat.Append("0");
                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_CNTCT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupPhysicianCon")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_ER_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupAdvisedEmg")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupParameters")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CLNCL_INTRVTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupInterventions")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CARE_PLAN_CHG") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupChange")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_CNTCT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupPhysicianCon")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_ER_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupAdvisedEmg")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupParameters")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CLNCL_INTRVTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupInterventions")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CARE_PLAN_CHG") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupChange")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1610UrinaryIncontinence"].Answer == "00") || (submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1610UrinaryIncontinence"].Answer == "02")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1615_INCNTNT_TIMING") && assessmentQuestions.ContainsKey("M1615UrinaryIncontinenceOccur") && assessmentQuestions["M1615UrinaryIncontinenceOccur"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1615UrinaryIncontinenceOccur"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1730_STDZ_DPRSN_SCRNG") && assessmentQuestions.ContainsKey("M1730DepressionScreening") && assessmentQuestions["M1730DepressionScreening"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1730DepressionScreening"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1730_PHQ2_LACK_INTRST") && assessmentQuestions.ContainsKey("M1730DepressionScreeningInterest") && assessmentQuestions["M1730DepressionScreeningInterest"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1730DepressionScreeningInterest"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1730_PHQ2_DPRSN") && assessmentQuestions.ContainsKey("M1730DepressionScreeningHopeless") && assessmentQuestions["M1730DepressionScreeningHopeless"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1730DepressionScreeningHopeless"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1830_CRNT_BATHG") && assessmentQuestions.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && assessmentQuestions["M1830CurrentAbilityToBatheEntireBody"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1830CurrentAbilityToBatheEntireBody"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1840_CUR_TOILTG") && assessmentQuestions.ContainsKey("M1840ToiletTransferring") && assessmentQuestions["M1840ToiletTransferring"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1840ToiletTransferring"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1845_CUR_TOILTG_HYGN") && assessmentQuestions.ContainsKey("M1845ToiletingHygiene") && assessmentQuestions["M1845ToiletingHygiene"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1845ToiletingHygiene"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1850_CUR_TRNSFRNG") && assessmentQuestions.ContainsKey("M1850Transferring") && assessmentQuestions["M1850Transferring"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1850Transferring"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1860_CRNT_AMBLTN") && assessmentQuestions.ContainsKey("M1860AmbulationLocomotion") && assessmentQuestions["M1860AmbulationLocomotion"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1860AmbulationLocomotion"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    //if (submissionGuide.ContainsKey("SUBM_HIPPS_CODE") && assessmentQuestions.ContainsKey("HIIPSCODE") && assessmentQuestions["HIIPSCODE"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    //{
                    //    submissionFormat.Append(assessmentQuestions["HIIPSCODE"].Answer.ToUpper().PartOfString(0, 5).PadLeft(5));
                    //}
                    //else
                    if (submissionGuide.ContainsKey("M0110_EPISODE_TIMING") && assessmentQuestions.ContainsKey("M0110EpisodeTiming") && assessmentQuestions["M0110EpisodeTiming"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0110EpisodeTiming"].Answer == "NA") //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(5));
                    }
                    else
                    {
                        submissionFormat.Append("0".PadLeft(5, '0'));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(5));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    //if (submissionGuide.ContainsKey("SUBM_HIPPS_VERSION") && assessmentQuestions.ContainsKey("HIIPSVERSION") && assessmentQuestions["HIIPSVERSION"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    //{
                    //    submissionFormat.Append(assessmentQuestions["HIPPSVERSION"].Answer.ToUpper().PartOfString(0, 5).PadLeft(5));
                    //}
                    //else
                    {
                        submissionFormat.Append("0".PadLeft(5, '0'));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(5));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_SELF") && assessmentQuestions.ContainsKey("M1900SelfCareFunctioning") && assessmentQuestions["M1900SelfCareFunctioning"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900SelfCareFunctioning"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_AMBLTN") && assessmentQuestions.ContainsKey("M1900Ambulation") && assessmentQuestions["M1900Ambulation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900Ambulation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_TRNSFR") && assessmentQuestions.ContainsKey("M1900Transfer") && assessmentQuestions["M1900Transfer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900Transfer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_HSEHOLD") && assessmentQuestions.ContainsKey("M1900HouseHoldTasks") && assessmentQuestions["M1900HouseHoldTasks"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900HouseHoldTasks"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1910_MLT_FCTR_FALL_RISK_ASMT") && assessmentQuestions.ContainsKey("M1910FallRiskAssessment") && assessmentQuestions["M1910FallRiskAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1910FallRiskAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2000DrugRegimenReview"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "00") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "01") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2002_MDCTN_FLWP") && assessmentQuestions.ContainsKey("M2002MedicationFollowup") && assessmentQuestions["M2002MedicationFollowup"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2002MedicationFollowup"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(12));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    //if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "00") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "01") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    //{
                    //    submissionFormat.Append(string.Empty.PadLeft(2));
                    //}
                    //else
                    //{
                    if (submissionGuide.ContainsKey("M2004_MDCTN_INTRVTN") && assessmentQuestions.ContainsKey("M2004MedicationIntervention") && assessmentQuestions["M2004MedicationIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2004MedicationIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    // }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2010_HIGH_RISK_DRUG_EDCTN") && assessmentQuestions.ContainsKey("M2010PatientOrCaregiverHighRiskDrugEducation") && assessmentQuestions["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2015_DRUG_EDCTN_INTRVTN") && assessmentQuestions.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && assessmentQuestions["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2020_CRNT_MGMT_ORAL_MDCTN") && assessmentQuestions.ContainsKey("M2020ManagementOfOralMedications") && assessmentQuestions["M2020ManagementOfOralMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2020ManagementOfOralMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (type.Contains("01") || type.Contains("03"))
                    {
                        if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2030_CRNT_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2030ManagementOfInjectableMedications") && assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2030_CRNT_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2030ManagementOfInjectableMedications") && assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M2040_PRIOR_MGMT_ORAL_MDCTN") && assessmentQuestions.ContainsKey("M2040PriorMedicationInject") && assessmentQuestions["M2040PriorMedicationInject"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2040PriorMedicationInject"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2040_PRIOR_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2040PriorMedicationInject") && assessmentQuestions["M2040PriorMedicationInject"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2040PriorMedicationInject"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_ADL") && assessmentQuestions.ContainsKey("M2100ADLAssistance") && assessmentQuestions["M2100ADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100ADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_IADL") && assessmentQuestions.ContainsKey("M2100IADLAssistance") && assessmentQuestions["M2100IADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100IADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_MDCTN") && assessmentQuestions.ContainsKey("M2100MedicationAdministration") && assessmentQuestions["M2100MedicationAdministration"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100MedicationAdministration"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_PRCDR") && assessmentQuestions.ContainsKey("M2100MedicalProcedures") && assessmentQuestions["M2100MedicalProcedures"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100MedicalProcedures"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_EQUIP") && assessmentQuestions.ContainsKey("M2100ManagementOfEquipment") && assessmentQuestions["M2100ManagementOfEquipment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100ManagementOfEquipment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_SPRVSN") && assessmentQuestions.ContainsKey("M2100SupervisionAndSafety") && assessmentQuestions["M2100SupervisionAndSafety"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100SupervisionAndSafety"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_ADVCY") && assessmentQuestions.ContainsKey("M2100FacilitationPatientParticipation") && assessmentQuestions["M2100FacilitationPatientParticipation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100FacilitationPatientParticipation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2110_ADL_IADL_ASTNC_FREQ") && assessmentQuestions.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && assessmentQuestions["M2110FrequencyOfADLOrIADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2110FrequencyOfADLOrIADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PTNT_SPECF") && assessmentQuestions.ContainsKey("M2250PatientParameters") && assessmentQuestions["M2250PatientParameters"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250PatientParameters"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_DBTS_FT_CARE") && assessmentQuestions.ContainsKey("M2250DiabeticFoot") && assessmentQuestions["M2250DiabeticFoot"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250DiabeticFoot"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_FALL_PRVNT") && assessmentQuestions.ContainsKey("M2250FallsPrevention") && assessmentQuestions["M2250FallsPrevention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250FallsPrevention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_DPRSN_INTRVTN") && assessmentQuestions.ContainsKey("M2250DepressionPrevention") && assessmentQuestions["M2250DepressionPrevention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250DepressionPrevention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PAIN_INTRVTN") && assessmentQuestions.ContainsKey("M2250MonitorMitigatePainIntervention") && assessmentQuestions["M2250MonitorMitigatePainIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250MonitorMitigatePainIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PRSULC_PRVNT") && assessmentQuestions.ContainsKey("M2250PressureUlcerIntervention") && assessmentQuestions["M2250PressureUlcerIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250PressureUlcerIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PRSULC_TRTMT") && assessmentQuestions.ContainsKey("M2250PressureUlcerTreatment") && assessmentQuestions["M2250PressureUlcerTreatment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250PressureUlcerTreatment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2300EmergentCare"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(17));
                    }

                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("00000000000000000");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2310_ECR_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareFall")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareResInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOtherResInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_HRT_FAILR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartFail")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareCardiac")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMyocardial")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartDisease")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_STROKE_TIA") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareStroke")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_GI_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareGI")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDehMal")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_UTI") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUrinaryInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareIV")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_WND_INFCTN_DTRORTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareWoundInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_UNCNTLD_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMental")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDVT")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_OTHER") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOther")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M2310_ECR_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareResInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOtherResInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_HRT_FAILR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartFail")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareCardiac")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMyocardial")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartDisease")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_STROKE_TIA") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareStroke")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_GI_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareGI")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDehMal")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_UTI") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUrinaryInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareIV")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_WND_INFCTN_DTRORTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareWoundInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_UNCNTLD_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMental")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDVT")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_OTHER") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_DBTS_FT") && assessmentQuestions.ContainsKey("M2400DiabeticFootCare") && assessmentQuestions["M2400DiabeticFootCare"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400DiabeticFootCare"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_FALL_PRVNT") && assessmentQuestions.ContainsKey("M2400FallsPreventionInterventions") && assessmentQuestions["M2400FallsPreventionInterventions"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400FallsPreventionInterventions"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_DPRSN") && assessmentQuestions.ContainsKey("M2400DepressionIntervention") && assessmentQuestions["M2400DepressionIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400DepressionIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PAIN_MNTR") && assessmentQuestions.ContainsKey("M2400PainIntervention") && assessmentQuestions["M2400PainIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400PainIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PRSULC_PRVN") && assessmentQuestions.ContainsKey("M2400PressureUlcerIntervention") && assessmentQuestions["M2400PressureUlcerIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400PressureUlcerIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PRSULC_WET") && assessmentQuestions.ContainsKey("M2400PressureUlcerTreatment") && assessmentQuestions["M2400PressureUlcerTreatment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400PressureUlcerTreatment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(17));
                    submissionFormat.Append(string.Empty.PadLeft(12));
                }
                if (type.Contains("09"))
                {
                    //if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "01") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    //{
                    //    submissionFormat.Append(string.Empty.PadLeft(2));
                    //}
                    //else
                    //{
                    if (submissionGuide.ContainsKey("M2420_DSCHRG_DISP") && assessmentQuestions.ContainsKey("M2420DischargeDisposition") && assessmentQuestions["M2420DischargeDisposition"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2420DischargeDisposition"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    //}
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(16));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0000000000000001");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationFall")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationInfection")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOtherRP")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2430_HOSP_HRT_FAILR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartFail")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2430_HOSP_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationCardiac")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMyocardial")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }

                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartDisease")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }

                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_STROKE_TIA") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationStroke")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_GI_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationGI")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDehMal")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationIV")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_WND_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationWoundInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }

                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMental")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_SCHLD_TRTMT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationScheduled")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_OTHER") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOther")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M2430_HOSP_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationInfection")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOtherRP")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_HRT_FAILR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartFail")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationCardiac")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMyocardial")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartDisease")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_STROKE_TIA") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationStroke")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_GI_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationGI")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDehMal")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationIV")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_WND_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationWoundInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMental")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_SCHLD_TRTMT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationScheduled")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_OTHER") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }
                submissionFormat.Append(string.Empty.PadLeft(244));
                submissionFormat.Append("%");
                return submissionFormat.ToString().ToUpper();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
