﻿namespace Axxess.AgencyManagement.Application.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Net;
    using System.IO;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using System.Web.Mvc;
    using System.Web;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.ViewData;

    public static class FileGenerator
    {
        public static string RemoteDownload(string serverIP, string remoteDir, string file)
        {
            string result = string.Empty;
            try
            {
                if (serverIP.IsNotNullOrEmpty() && remoteDir.IsNotNullOrEmpty() && file.IsNotNullOrEmpty())
                {
                    string uri = string.Format("ftp://{0}/{1}/{2}", serverIP, remoteDir, file);
                    Uri serverUri = new Uri(uri);
                    if (serverUri.Scheme != Uri.UriSchemeFtp)
                    {
                        return result;
                    }
                    FtpWebRequest reqFTP;
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(serverUri);
                    reqFTP.Credentials = new NetworkCredential("AxxessAdmin", "29Eiyztygt");
                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.Proxy = null;
                    reqFTP.UsePassive = false;
                    FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    var readStream = new StreamReader(responseStream);
                    result = readStream.ReadToEnd();
                    response.Close();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, "Download Error");
            }
            return result;
        }

        public static FileResult PreviewPdf<T>(T inputDocument, string fileName, HttpResponseBase response) where T : AxxessPdf
        {
            var fileResult = Pdf(inputDocument, fileName);
            response.AddHeader("Content-Disposition", "inline; filename=" + fileResult.FileDownloadName);
            fileResult.FileDownloadName = null;
            return fileResult;
        }

        public static FileResult Pdf<T>(T inputDocument, string fileName) where T : AxxessPdf
        {
            var file = new FileStreamResult(new MemoryStream(), "application/pdf");
            try
            {
                if (inputDocument != null)
                {
                    var stream = inputDocument.GetStream();
                    if (stream != null)
                    {
                        stream.Position = 0;
                        file = new FileStreamResult(stream, "application/pdf");
                        file.FileDownloadName = string.Format("{0}_{1}.pdf", fileName.Replace(" ", "_"), DateTime.Now.Ticks.ToString());

                    }
                }
            }
            catch (Exception)
            {
                return file;
            }
            return file;
        }

        public static FileResult PlainText(string inputDocument, string fileName)
        {
            var fileStream = new MemoryStream();
            var file = new FileStreamResult(fileStream, "Text/Plain");
            try
            {
                if (inputDocument.IsNotNullOrEmpty() && inputDocument.Length > 0)
                {
                    var encoding = new UTF8Encoding();
                    byte[] buffer = encoding.GetBytes(inputDocument);
                    fileStream.Write(buffer, 0, inputDocument.Length);
                    fileStream.Position = 0;
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}{1}.txt", fileName, DateTime.Now.ToString("MMddyyyy")));
                    file = new FileStreamResult(fileStream, "Text/Plain");
                }
            }
            catch (Exception)
            {
                return file;
            }
            return file;
        }

        public static FileResult PreviewPdf(VisitNoteViewData note, HttpResponseBase response)
        {
            FileResult file = new FileStreamResult(new MemoryStream(), "application/pdf");
            var fileResult = GetPrintFile(note, file);
            response.AddHeader("Content-Disposition", "inline; filename=" + fileResult.FileDownloadName);
            fileResult.FileDownloadName = null;
            return fileResult;
        }

        public static FileResult GetPrintFile(VisitNoteViewData note, FileResult defaultValue)
        {
            FileResult file = defaultValue;
            switch (note.DisciplineTask)
            {
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                    file = Pdf(new SNVisitPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.Labs:
                    file = Pdf(new LabsPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.SNVPsychNurse:
                    file = Pdf(new PsychPdf(note, note.Version > 0 ? note.Version : 1), note.TypeName);
                    break;
                case (int)DisciplineTasks.SNPsychAssessment:
                    file = Pdf(new PsychAssessmentPdf(note, note.Version > 0 ? note.Version : 1), note.TypeName);
                    break;
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                    file = Pdf(new DiabeticDailyPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.SNPediatricAssessment:
                    file = Pdf(new PediatricPdf(note, PdfDocs.Get(PDFDOCSList.PediatricAssessment), note.Version > 0 ? note.Version : 1), note.TypeName);
                    break;
                case (int)DisciplineTasks.NutritionalAssessment:
                    file = Pdf(new DietPdf(note, PdfDocs.Get(PDFDOCSList.NutritionalAssessment)), note.TypeName);
                    break;
                case (int)DisciplineTasks.SNPediatricVisit:
                    file = Pdf(new PediatricPdf(note, PdfDocs.Get(PDFDOCSList.Pediatric), note.Version > 0 ? note.Version : 1), note.TypeName);
                    break;
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTReassessment:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTReassessment:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.STReassessment:
                    file = Pdf(new TherapyPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWProgressNote:
                    file = Pdf(new MSWPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.HomeMakerNote:
                    file = Pdf(new HomeMakerNotePdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.HHAideVisit:
                    file = Pdf(new HHAVisitPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.HHAideCarePlan:
                    file = Pdf(new HHACarePlanPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.DischargeSummary:
                    file = Pdf(new DischargeSummaryPdf(note, PdfDocs.Get(PDFDOCSList.DischargeSummary)), note.TypeName);
                    break;
                case (int)DisciplineTasks.PTDischargeSummary:
                    file = Pdf(new DischargeSummaryPdf(note, PdfDocs.Get(PDFDOCSList.PTDischargeSummary)), note.TypeName);
                    break;
                case (int)DisciplineTasks.OTDischargeSummary:
                    file = Pdf(new DischargeSummaryPdf(note, PdfDocs.Get(PDFDOCSList.OTDischargeSummary)), note.TypeName);
                    break;
                case (int)DisciplineTasks.STDischargeSummary:
                    file = Pdf(new DischargeSummaryPdf(note, PdfDocs.Get(PDFDOCSList.STDischargeSummary)), note.TypeName);
                    break;
                case (int)DisciplineTasks.PASTravel:
                    file = Pdf(new PASTravelPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.PASVisit:
                    file = Pdf(new PASVisitPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.PASCarePlan:
                    file = Pdf(new PASCarePlanPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.PTSupervisoryVisit:
                case (int)DisciplineTasks.OTSupervisoryVisit:
                    file = Pdf(new TherapySupervisoryPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                    file = Pdf(new LVNSVisitPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                    file = Pdf(new HHASVisitPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.UAPWoundCareVisit:
                    file = Pdf(new UAPWoundCarePdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                    file = Pdf(new UAPInsulinPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.SixtyDaySummary:
                    file = Pdf(new SixtyDaySummaryPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.InitialSummaryOfCare:
                    file = Pdf(new InitialSummaryPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                    file = Pdf(new TransferSummaryPdf(note), note.TypeName);
                    break;
                case (int)DisciplineTasks.DriverOrTransportationNote:
                    file = Pdf(new TransportationPdf(note), note.TypeName);
                    break;
            }
            return file;
        }

        public static FileResult GetClaimPDF(BaseInvoiceFormViewData viewData)
        {
            FileResult file = new FileStreamResult(new MemoryStream(), "application/pdf");
            switch (viewData.InvoiceType)
            {
                case InvoiceType.UB:
                    {
                        if (viewData.ClaimTypeSubCategory == ClaimTypeSubCategory.RAP || viewData.ClaimTypeSubCategory == ClaimTypeSubCategory.Final)
                        {
                            file = Pdf(new UB04Pdf(viewData), InvoiceType.UB.GetDescription());
                        }
                        else
                        {
                            file = Pdf(new ManagedUB04Pdf(viewData), InvoiceType.UB.GetDescription());
                        }
                    }
                    break;
                case InvoiceType.HCFA:
                    file = Pdf(new HCFA1500Pdf(viewData), InvoiceType.HCFA.GetDescription());
                    break;
                case InvoiceType.Invoice:
                    file = Pdf(new InvoicePdf(viewData), InvoiceType.Invoice.GetDescription());
                    break;
            }
            return file;
        }
    }
}
