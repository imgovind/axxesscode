﻿// -----------------------------------------------------------------------
// <copyright file="MessageFactory.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Axxess.AgencyManagement.Application.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    /// <summary>
    /// Generates messages to be sent to the client for success and failures of an action
    /// </summary>
    public class MessageFactory
    {
        public static string CreateMessage<T>(ClientAction action, bool isSuccessful)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(GetName<T>());
            sb.Append(isSuccessful ? " has been " : " could not be ");
            sb.Append(action.GetDescription()).Append(".");
            return sb.ToString();
        }

        private static string GetName<T>()
        {
            var type = typeof(T);
            var attributes = type.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            string name;
            if (attributes != null && attributes.Length > 0)
            {
                name = attributes[0].Description;
            }
            else
            {
                name = type.Name;
            }
            return name;
        }
    }

    public enum ClientAction
    {
        [Description("created")]
        Create,
        [Description("updated")]
        Update,
        [Description("returned")]
        Return,
        [Description("approved")]
        Approve,
        [Description("reopened")]
        ReOpen
    }
}
