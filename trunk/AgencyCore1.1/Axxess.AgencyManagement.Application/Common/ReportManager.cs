﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;

    using Exports;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Application.Domain;
   
    public static class ReportManager
    {
        private static readonly ILoginRepository loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
        private static readonly IUserRepository userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
        private static readonly IAssetRepository assetRepository = Container.Resolve<IAgencyManagementDataProvider>().AssetRepository;
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
        private static readonly IMessageRepository messageRepository = Container.Resolve<IAgencyManagementDataProvider>().MessageRepository;

        public static void AddPatientsAndVisitsByAgeReport(ReportParameters reportParameters)
        {
            var export = new PatientsAndVisitsByAgeExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddUnbilledVisitsReport(ReportParameters reportParameters)
        {
            var export = new UnbilledVisitsExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.InsuranceId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddTherapyManagementReport(ReportParameters reportParameters)
        {
            var export = new TherapyManagementExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddPPSVisitInformationReport(ReportParameters reportParameters)
        {
            var export = new PpsVisitExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddPPSEpisodeInformationReport(ReportParameters reportParameters)
        {
            var export = new PpsEpisodeExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddPPSChargeInformationReport(ReportParameters reportParameters)
        {
            var export = new PpsChargeExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddPPSPaymentInformationReport(ReportParameters reportParameters)
        {
            var export = new PpsPaymentExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddHHRGReport(ReportParameters reportParameters)
        {
            var export = new HHRGExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddDischargesByReasonReport(ReportParameters reportParameters)
        {
            var export = new DischargesByReasonExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddVisitsByPrimaryPaymentSourceReport(ReportParameters reportParameters)
        {
            var export = new VisitsByPrimaryPaymentSourceExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddVisitsByStaffTypeReport(ReportParameters reportParameters)
        {
            var export = new VisitsByStaffTypeExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddAdmissionsByReferralSourceReport(ReportParameters reportParameters)
        {
            var export = new AdmissionsByReferralSourceExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddPatientsVisitsByPrincipalDiagnosisReport(ReportParameters reportParameters)
        {
            var export = new PatientsVisitsByPrincipalDiagnosisExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        public static void AddCostReport(ReportParameters reportParameters)
        {
            var export = new CostReportExporter(reportParameters.AgencyId, reportParameters.BranchId, reportParameters.StartDate, reportParameters.EndDate, reportParameters.AgencyName);
            CompleteReport(reportParameters.AgencyId, reportParameters.Id, export);
        }

        private static void CompleteReport(Guid agencyId, Guid reportId, BaseExporter export)
        {
            var report = agencyRepository.GetReport(agencyId, reportId);
            if (report != null)
            {
                var bytes = export.Process().GetBuffer();
                var asset = new Asset
                    {
                        FileName = export.FileName,
                        AgencyId = agencyId,
                        ContentType = export.MimeType,
                        FileSize = bytes.Length.ToString(),
                        Bytes = bytes
                    };

                if (AssetHelper.AddAsset(asset))
                {
                    report.AssetId = asset.Id;
                    report.Completed = DateTime.Now;
                    report.Status = "Completed";

                    if (agencyRepository.UpdateReport(report))
                    {
                        var user = UserEngine.GetUser(report.UserId, agencyId);
                        if (user != null)
                        {
                            if (!user.LoginId.IsEmpty())
                            {
                                var messageId = Guid.NewGuid();
                                var message = new SystemMessage
                                {
                                    Id = messageId,
                                    CreatedBy = "Axxess",
                                    Created = DateTime.Now,
                                    Subject = "Your requested report is ready for download",
                                    Body = "<p>Dear User,</p><p>Go to the \"Report Menu\" and click on \"Completed Reports\" to view your requested report.</p><p>The Axxess Team</p>",
                                };
                                if (messageRepository.AddSystemMessage(message))
                                {
                                    var login = loginRepository.Find(user.LoginId);
                                    if (login != null && login.IsActive)
                                    {
                                        var userMessage = new UserMessage(agencyId, user.Id, messageId, MessageType.System);
                                        if (messageRepository.AddUserMessage(userMessage))
                                        {
                                            var parameters = new string[4];
                                            parameters[0] = "recipientfirstname";
                                            parameters[1] = user.DisplayName;
                                            parameters[2] = "senderfullname";
                                            parameters[3] = "Axxess";
                                            var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                                            Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Axxess Technology Solutions sent you a message.", bodyText);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
