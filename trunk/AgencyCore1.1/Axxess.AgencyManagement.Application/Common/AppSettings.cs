﻿namespace Axxess.AgencyManagement.Application {
    using System;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    public static class AppSettings {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();
        public static string IdentifyAgencyCookie { get { return configuration.AppSettings["IdentifyAgencyCookie"]; } }
        public static string RememberMeCookie { get { return configuration.AppSettings["RememberMeCookie"]; } }
        public static int RememberMeForTheseDays { get { return configuration.AppSettings["RememberMeForTheseDays"].ToInteger(); } }
        public static string AuthenticationType { get { return configuration.AppSettings["AuthenticationType"]; } }
        public static string GoogleAPIKey { get { return configuration.AppSettings["GoogleApiKey"]; } }
        public static double FormsAuthenticationTimeoutInMinutes { get { return configuration.AppSettings["FormsAuthenticationTimeoutInMinutes"].ToDouble(); } }
        public static bool UsePersistentCookies { get { return configuration.AppSettings["UsePersistentCookies"].ToBoolean(); } }
        public static bool UseMinifiedJs { get { return configuration.AppSettings["UseMinifiedJs"].ToBoolean(); } }
        public static string AllowedImageExtensions { get { return configuration.AppSettings["AllowedImageExtensions"]; } }
      
        public static string HomeHealthGoldLogo { get { return configuration.AppSettings["HomeHealthGoldLogo"]; } }
        public static string BrowserCompatibilityCheck { get { return configuration.AppSettings["BrowserCompatibilityCheck"]; } }
        public static string GetRemoteContent { get { return configuration.AppSettings["GetRemoteContent"]; } }
        public static bool IsSingleUserMode { get { return configuration.AppSettings["IsSingleUserMode"].ToBoolean(); } }
    }
}
