﻿namespace Axxess.AgencyManagement.Application.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Common;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Entities.Extensions;


    /// <summary>
    /// Contains methods for updating wound care questions
    /// </summary>
    public class WoundHelper<T> 
        where T : QuestionBase, new()
    {
        private static readonly string[] WoundKeys = { "GenericLocation", "GenericOnsetDate", "GenericWoundType", "GenericPressureUlcerStage", "GenericMeasurementLength", "GenericMeasurementWidth",
                                                       "GenericMeasurementDepth", "GenericWoundBedGranulation", "GenericWoundBedSlough", "GenericWoundBedEschar", "GenericSurroundingTissue", "GenericDrainage", "GenericDrainageAmount", "GenericOdor",
                                                       "GenericTunnelingLength", "GenericTunnelingTime", "GenericUnderminingLength", "GenericUnderminingTimeFrom", "GenericUnderminingTimeTo", "GenericDeviceType", "GenericDeviceSetting", "GenericUploadFile" };

        public static List<T> Update(WoundSaveArguments saveArguments, HttpFileCollectionBase httpFiles, FormCollection formCollection, IDictionary<string, T> questions, List<Guid> taskAssets, out List<Guid> savedAssetIds)
        {
            formCollection.Remove("Id");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("Action");
            formCollection.Remove("Number");
            formCollection.Remove("X");
            formCollection.Remove("Y");
            formCollection.Remove("PatientGuid");
            formCollection.Remove("Type");
            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray.Length == 3 && nameArray[2] == "text") continue;
                if (nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    questions[name] = QuestionFactory<T>.Create(name, formCollection.GetValues(key).Join(","));
                }
            }
            if (saveArguments.Action == "Add")
            {
                var question = questions.Get("GenericWoundPositions");
                var woundPosition = new WoundPosition(saveArguments.Number, saveArguments.X, saveArguments.Y);
                if (!SetPosition(question, woundPosition))
                {
                    questions["GenericWoundPositions"] = QuestionFactory<T>.Create("GenericWoundPositions", new List<WoundPosition> { woundPosition }.ToJson());
                }
            }
            savedAssetIds = AssetHelper.SaveAssetAndAddToXml(httpFiles, taskAssets, questions);
            return questions.Values.ToList();
        }

        public static bool CheckAndSetPosition(List<T> questions, WoundPosition woundToPositon)
        {
            bool isUpdated = false;
            if (questions != null)
            {
                foreach (var question in questions)
                {
                    if (question != null && question.Name == "WoundPositions")
                    {
                        isUpdated = SetPosition(question, woundToPositon);
                    }
                    if (isUpdated)
                    {
                        break;
                    }
                }
                if (!isUpdated)
                {
                    questions.Add(QuestionFactory<T>.Create("GenericWoundPositions", new List<WoundPosition> { woundToPositon }.ToJson()));
                }
            }
            return isUpdated;
        }

        private static bool SetPosition(T question, WoundPosition woundToPositon)
        {
            bool isUpdated = false;
            if (question != null)
            {
                var positions = question.Answer.FromJson<List<WoundPosition>>();
                if (positions == null)
                {
                    positions = new List<WoundPosition> { woundToPositon };
                }
                else
                {
                    var existingPosition = positions.FirstOrDefault(s => s.Number == woundToPositon.Number);
                    if (existingPosition != null)
                    {
                        existingPosition.x = woundToPositon.x;
                        existingPosition.y = woundToPositon.y;
                    }
                    else
                    {
                        positions.Add(woundToPositon);
                    }
                }
                isUpdated = true;
                question.Answer = positions.ToJson();
            }
            return isUpdated;
        }

        public static bool Delete(int woundNumber, int woundCount, IDictionary<string, T> questions, out Guid assetId)
        {
            assetId = Guid.Empty;
            try
            {
                var question = questions.GetOrDefault("GenericWoundPositions", QuestionFactory<T>.Create("GenericWoundPositions", ""));
                var positions = question.Answer.FromJson<List<WoundPosition>>();
                if (positions.IsNotNullOrEmpty())
                {
                    positions.Remove(new WoundPosition(woundNumber, 0, 0));
                    question.Answer = positions.ToJson();
                    questions["GenericWoundPositions"] = question;
                }
                var assetToDelete = questions.Get("GenericUploadFile" + woundNumber);
                if (assetToDelete != null && assetToDelete.Answer.IsNotNullOrEmpty())
                {
                    assetId = assetToDelete.Answer.ToGuid();
                }
                for (; woundNumber < woundCount; woundNumber++)
                {
                    foreach (string woundKey in WoundKeys)
                    {
                        string key = woundKey + woundNumber;
                        var documentQuestion = questions.Get(woundKey + (woundNumber + 1));
                        questions[woundKey + woundNumber] = QuestionFactory<T>.Create(key, documentQuestion != null ? documentQuestion.Answer : string.Empty);
                    }
                }
                foreach (string key in WoundKeys)
                {
                    questions.Remove(key + woundCount);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static Dictionary<string, T> GetWound(IDictionary<string, T> documentQuestions, int woundNumber)
        {
            var questions = new Dictionary<string, T>();
            if (documentQuestions.IsNotNullOrEmpty())
            {
                foreach (var woundKey in WoundKeys)
                {
                    string key = woundKey + woundNumber;
                    var documentQuestion = documentQuestions.Get(key);
                    questions[key] = QuestionFactory<T>.Create(key, documentQuestion != null ? documentQuestion.Answer : string.Empty);
                }
            }
            return questions;
        }

        public static string GetPositions(IDictionary<string, T> questions)
        {
            var question = questions.Get("GenericWoundPositions") ?? new T();
            var woundPositions = new List<WoundPosition>();
            if (question.Answer.IsNotNullOrEmpty())
            {
                woundPositions = question.Answer.FromJson<List<WoundPosition>>();
            }
            for (int i = 1; i < 5; i++)
            {
                if (questions.AnswerOrEmptyString("GenericLocation" + i).IsNotNullOrEmpty() && woundPositions.All(e => e.Number != i))
                {
                    woundPositions.Add(new WoundPosition(i, 5, i * 40));
                }
            }
            return woundPositions.ToJson();
        }
    }
}
