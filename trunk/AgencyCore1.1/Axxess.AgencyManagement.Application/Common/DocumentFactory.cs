﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Enums;
    using Services;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.Domain;

    public class DocumentFactory
    {
        public static DocumentType GetType(int disciplineTask, bool isMissedVisit)
        {
            var type = DocumentType.None;
            if (isMissedVisit)
            {
                type = DocumentType.MissedVisit;
            }
            else
            {
                if (DisciplineTaskFactory.AllPatientVisitNotes().Contains(disciplineTask))
                {
                    type = DocumentType.Notes;
                }
                else if (DisciplineTaskFactory.AllAssessments(true).Contains(disciplineTask))
                {
                    type = DocumentType.OASIS;
                }
                else if (DisciplineTaskFactory.POC().Contains(disciplineTask))
                {
                    type = DocumentType.PlanOfCare;
                }
                else
                {
                    switch (disciplineTask)
                    {
                        case (int)DisciplineTasks.PhysicianOrder:
                            type = DocumentType.PhysicianOrder;
                            break;
                        case (int)DisciplineTasks.CommunicationNote:
                            type = DocumentType.CommunicationNote;
                            break;
                        case (int)DisciplineTasks.FaceToFaceEncounter:
                            type = DocumentType.FaceToFaceEncounter;
                            break;
                        case (int)DisciplineTasks.InfectionReport:
                            type = DocumentType.Infection;
                            break;
                        case (int)DisciplineTasks.IncidentAccidentReport:
                            type = DocumentType.IncidentAccident;
                            break;
                        case (int)DisciplineTasks.MedicareEligibilityReport:
                            type = DocumentType.MedicareEligibility;
                            break;
                    }
                }
            }
            return type;
        }

        #region Delete

        //private static readonly UrlHelper<T> UrlHelper = Container.Resolve<UrlHelper<T>>();

        //public static void SetNew(T task, bool addReassignLink, bool isNotQA, bool isOasisProfile, bool isAssetUrl, PatientAccessInformation patientAccessInformation)
        //{
        //    if (task != null && task.DisciplineTask > 0 && !task.PatientId.IsEmpty() && !task.Id.IsEmpty())
        //    {
        //        string onclick = string.Empty;
        //        if (task.IsOrphaned && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //        {
        //            task.Url = task.DisciplineTaskName;
        //            SetAction(task, addReassignLink);
        //            task.PrintUrl = Print(task, isNotQA);
        //        }
        //        else
        //        {
        //            if (Current.IsAgencyFrozen)
        //            {
        //                task.Url = task.DisciplineTaskName;
        //                task.ActionUrl = string.Empty;
        //                task.Status = 0;
        //                task.EpisodeNotes = string.Empty;
        //                task.PrintUrl = Print(task, isNotQA);
        //            }
        //            else if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
        //            {
        //                SetEditUrl(task);
        //                SetAction(task, addReassignLink);
        //                task.PrintUrl = Print(task, isNotQA);
        //            }
        //            else if (Current.IsClinicianOrHHA && patientAccessInformation != null)
        //            {
        //                if (Current.UserId != task.UserId && (patientAccessInformation.PatientUserIds[task.PatientId] != Current.UserId))
        //                {
        //                    //task.PrintUrl  = string.Empty;
        //                    task.ActionUrl = string.Empty;
        //                    task.Status = 0;
        //                    task.Url = task.DisciplineTaskName;
        //                    if (patientAccessInformation.PatientUsers.Exists(p => p.UserId == Current.UserId) ||
        //                        patientAccessInformation.TeamPatients.Exists(p => p.PatientId == task.PatientId))
        //                    {
        //                        task.PrintUrl = Print(task, isNotQA);
        //                    }
        //                }
        //                else
        //                {
        //                    SetEditUrl(task);
        //                    SetAction(task, addReassignLink);
        //                    task.PrintUrl = Print(task, isNotQA);
        //                }
        //            }
        //            else if (Current.IfOnlyRole(AgencyRoles.Auditor))
        //            {
        //                task.PrintUrl = Print(task, isNotQA);
        //                task.Url = task.DisciplineTaskName;
        //                task.ActionUrl = string.Empty;
        //                task.Status = 0;

        //                task.MissedVisitComments = string.Empty;
        //                task.EpisodeNotes = string.Empty;
        //                task.ReturnReason = string.Empty;
        //                task.Comments = string.Empty;
        //            }
        //            else
        //            {
        //                SetEditUrl(task);
        //                task.PrintUrl = string.Empty;
        //                task.ActionUrl = string.Empty;
        //                task.Status = 0;
        //            }

        //        }
        //        if (isAssetUrl)
        //        {
        //            if (task.Asset.IsNotNullOrEmpty())
        //            {
        //                var assets = task.Asset.ToObject<List<Guid>>();
        //                if (assets != null && assets.Count > 0)
        //                {
        //                    task.Asset = string.Empty;
        //                    task.AttachmentUrl = UrlHelper.GetAttachmentAction(task);
        //                }
        //            }
        //        }
        //        if (isOasisProfile)
        //        {
        //            UrlHelper.SetOASISProfile(task);
        //        }
        //    }

        //}

        //protected static void SetAction(T task, bool addReassignLink)
        //{
        //    string reopenUrl = UrlHelper.GetReopenAction(task);
        //    string detailUrl = UrlHelper.GetDetailAction(task);
        //    string deleteUrl = UrlHelper.GetDeleteAction(task);
        //    string reassignUrl = UrlHelper.GetReassignAction(task);
        //    string restoreUrl = UrlHelper.GetRestoreAction(task);
        //    var listOfAction = new List<string>();
        //    if (task.IsMissedVisit)
        //    {
        //        if (Current.HasRight(Permissions.DeleteTasks))
        //        {
        //            listOfAction.Add(deleteUrl);
        //        }
        //        listOfAction.Add(restoreUrl);
        //    }
        //    else if (task is ScheduleEvent && task.IsOrphaned)
        //    {
        //        if (Current.HasRight(Permissions.EditTaskDetails) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //        {
        //            listOfAction.Add(detailUrl);
        //        }
        //        if (Current.HasRight(Permissions.DeleteTasks) && !Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //        {
        //            listOfAction.Add(deleteUrl);
        //        }
        //        if (task.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
        //        {
        //            task.ActionUrl = string.Empty;
        //        }
        //    }
        //    else
        //    {
        //        if (ScheduleStatusFactory.CaseManagerStatus().Contains(task.Status))
        //        {
        //            if (Current.HasRight(Permissions.EditTaskDetails))
        //            {
        //                listOfAction.Add(detailUrl);
        //                task.ActionUrl = detailUrl;
        //            }
        //        }
        //        else if (task.IsCompletelyFinished())
        //        {
        //            if (Current.HasRight(Permissions.EditTaskDetails))
        //            {
        //                listOfAction.Add(detailUrl);
        //            }
        //            if (Current.HasRight(Permissions.DeleteTasks))
        //            {
        //                listOfAction.Add(deleteUrl);
        //            }
        //            if (Current.HasRight(Permissions.ReopenDocuments))
        //            {
        //                listOfAction.Add(reopenUrl);
        //            }
        //            if (task.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
        //            {
        //                listOfAction.Clear();
        //            }
        //        }
        //        else
        //        {
        //            if (Current.HasRight(Permissions.EditTaskDetails))
        //            {
        //                listOfAction.Add(detailUrl);
        //            }
        //            if (Current.HasRight(Permissions.DeleteTasks))
        //            {
        //                listOfAction.Add(deleteUrl);
        //            }
        //            if (addReassignLink && Current.HasRight(Permissions.ScheduleVisits))
        //            {
        //                listOfAction.Add(reassignUrl);
        //            }
        //        }
        //    }
        //    if (listOfAction.IsNotNullOrEmpty())
        //    {
        //        task.ActionUrl = listOfAction.ToArray().Join(" | ");
        //    }
        //    else
        //    {
        //        task.ActionUrl = string.Empty;
        //    }
        //}

        /// <summary>
        /// Gets the url for creating new tasks
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        //public static string GetNewUrl(T task)
        //{
        //    string TaskUrl = string.Empty;
        //    string prefix = UrlHelper.Prefix;
        //    string disciplineTaskName = ((DisciplineTasks)task.DisciplineTask).ToString();
        //    switch (task.DisciplineTask)
        //    {
        //        case (int)DisciplineTasks.FaceToFaceEncounter:
        //            TaskUrl = string.Format("{0}", task.DisciplineTaskName);
        //            break;
        //        case (int)DisciplineTasks.PhysicianOrder:
        //            TaskUrl = string.Format("PhysicianOrder.{0}.New('{1}');", prefix, task.PatientId);
        //            break;
        //        case (int)DisciplineTasks.CommunicationNote:
        //            TaskUrl = string.Format("CommunicationNote.{0}.New('{1}');", prefix, task.PatientId);
        //            break;
        //        case (int)DisciplineTasks.IncidentAccidentReport:
        //            TaskUrl = string.Format("Incident.{0}.New('{1}');", prefix, task.PatientId);
        //            break;
        //        case (int)DisciplineTasks.InfectionReport:
        //            TaskUrl = string.Format("Infection.{0}.New('{1}');", prefix, task.PatientId);
        //            break;
        //    }
        //    return TaskUrl;
        //}

        //public static void SetEditUrl(T task)
        //{
        //    task.Url = GetEditUrl(task, true);
        //}

        /// <summary>
        /// Gets the urls for editing tasks
        /// </summary>
        /// <param name="task"></param>
        /// <param name="inDataCenter"></param>
        /// <returns></returns>
        //public static string GetEditUrl(T task, bool inDataCenter)
        //{
        //    string TaskUrl = string.Empty;
        //    if (((task.IsCompleted() || task.IsOrphaned) && inDataCenter) ||
        //        Current.IfOnlyRole(AgencyRoles.Auditor) || Current.IsAgencyFrozen)
        //    {
        //        TaskUrl = task.DisciplineTaskName;
        //    }
        //    else
        //    {
        //        string prefix = UrlHelper.Prefix;
        //        string area = UrlHelper.Area;
        //        if (task.IsMissedVisit)
        //        {
        //            if (ScheduleStatusFactory.NoteMissedVisitOnAndAfterQA(true).Contains(task.Status))
        //            {
        //                TaskUrl = string.Format("MissedVisit.{0}.Open($(this), '{1}');", prefix, task.Id);
        //            }
        //            else
        //            {
        //                TaskUrl = string.Format("MissedVisit.{0}.Edit('{1}');", prefix, task.Id);
        //            }
        //        }
        //        else
        //        {
        //            if (DisciplineTaskFactory.AllAssessments(true).Contains(task.DisciplineTask))
        //            {
        //                string oasisType = task.GetOasisType().ToString();
        //                TaskUrl = string.Format("Oasis.{0}.Load('{1}','{2}','{3}');", prefix, task.Id, task.PatientId, oasisType);
        //            }
        //            else if (task.IsNote())
        //            {
        //                TaskUrl = string.Format("Visit.Load({{Data: {{ PatientId: '{0}', EventId: '{1}' }}, Url:'{3}', Type:'{2}' }} );",  task.PatientId, task.Id, ((DisciplineTasks)task.DisciplineTask).ToString(), area + "/Note");
        //            }
        //            else
        //            {
        //                switch (task.DisciplineTask)
        //                {
        //                    case (int)DisciplineTasks.FaceToFaceEncounter:
        //                        TaskUrl = ((DisciplineTasks)task.DisciplineTask).ToString();
        //                        break;
        //                    case (int)DisciplineTasks.HCFA485:
        //                    case (int)DisciplineTasks.NonOasisHCFA485:
        //                    case (int)DisciplineTasks.HCFA485StandAlone:
        //                        TaskUrl = string.Format("PlanOfCare.{0}.Edit('{1}','{2}','{3}');", prefix, task.EpisodeId, task.PatientId, task.Id);
        //                        break;
        //                    case (int)DisciplineTasks.PhysicianOrder:
        //                        TaskUrl = string.Format("PhysicianOrder.{0}.Edit('{1}','{2}');", prefix, task.Id, task.PatientId);
        //                        break;
        //                    case (int)DisciplineTasks.CommunicationNote:
        //                        TaskUrl = string.Format("CommunicationNote.{0}.Edit('{1}','{2}');", prefix, task.PatientId, task.Id);
        //                        break;
        //                    case (int)DisciplineTasks.IncidentAccidentReport:
        //                        TaskUrl = string.Format("Incident.{0}.Edit('{1}');", prefix, task.Id);
        //                        break;
        //                    case (int)DisciplineTasks.InfectionReport:
        //                        TaskUrl = string.Format("Infection.{0}.Edit('{1}');", prefix, task.Id);
        //                        break;
        //                }
        //            }
        //        }
        //    }
        //    return TaskUrl;
        //}

        /// <summary>
        /// Gets the url for deleting tasks
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        //public static string GetDeleteUrl(T task)
        //{
        //    string TaskUrl = string.Empty;
        //    string prefix = string.Empty;
        //    if (!Current.IfOnlyRole(AgencyRoles.Auditor) && !Current.IsAgencyFrozen)
        //    {
        //        prefix = UrlHelper.Prefix;
        //        string disciplineTaskName = ((DisciplineTasks)task.DisciplineTask).ToString();
        //        switch (task.DisciplineTask)
        //        {
        //            case (int)DisciplineTasks.PhysicianOrder:
        //                TaskUrl = string.Format("Patient.Orders.{0}.DeleteOrder('{1}','{2}');", prefix, task.Id, task.PatientId);
        //                break;
        //            case (int)DisciplineTasks.CommunicationNote:
        //                TaskUrl = string.Format("CommunicationNote.{0}.Delete('{1}','{2}');", prefix, task.PatientId, task.Id);
        //                break;
        //            case (int)DisciplineTasks.IncidentAccidentReport:
        //                TaskUrl = string.Format("Incident.{0}.Delete('{1}', '{2}');", prefix, task.PatientId, task.Id);
        //                break;
        //            case (int)DisciplineTasks.InfectionReport:
        //                TaskUrl = string.Format("Infection.{0}.Delete('{1}', '{2}');", prefix, task.PatientId, task.Id);
        //                break;
        //        }
        //    }
        //    return TaskUrl;
        //}

        //public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool isNotQA)
        //{
        //    var scheduleTask = new T
        //    {
        //        EpisodeId = episodeId,
        //        PatientId = patientId,
        //        Id = eventId,
        //        DisciplineTask = (int)task,
        //        Discipline = task.GetCustomGroup(),
        //        Status = status
        //    };
        //    return Print(scheduleTask, isNotQA);
        //}

        /// <summary>
        /// Gets the url for printing tasks
        /// </summary>
        /// <param name="scheduleTask"></param>
        /// <param name="isNotQA"></param>
        /// <returns></returns>
        //public static string Print(T scheduleTask, bool isNotQA)
        //{
        //    Guid episodeId = scheduleTask.EpisodeId;
        //    UrlContainer urls = new UrlContainer();
        //    if (scheduleTask.IsMissedVisit)
        //    {
        //        UrlHelper.GetMissedVisitPrintUrls(scheduleTask, urls);
        //    }
        //    else if (scheduleTask.IsNote())
        //    {
        //        UrlHelper.GetVisitNotePrintUrls(scheduleTask, urls);
        //    }
        //    else if (DisciplineTaskFactory.AllAssessments(true).Contains(scheduleTask.DisciplineTask))
        //    {
        //        UrlHelper.GetAssessmentPrintUrls(scheduleTask, urls);
        //    }
        //    else if (scheduleTask.IsReport())
        //    {
        //        UrlHelper.GetReportPrintUrls(scheduleTask, urls);
        //    }
        //    else if (scheduleTask.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
        //    {
        //        UrlHelper.GetPhysicianOrderPrintUrls(scheduleTask, urls);
        //    }
        //    else if (scheduleTask.IsPlanOfCare())
        //    {
        //        UrlHelper.GetPlanOfCarePrintUrls(scheduleTask, urls);
        //    }
        //    else
        //    {
        //        UrlHelper.GetAppSpecificPrintUrls(scheduleTask, urls);
        //    }
        //    string printUrl = string.Empty;
        //    string linkText = scheduleTask.DisciplineTaskName;
        //    if (scheduleTask.IsMissedVisit)
        //    {
        //        printUrl = "Visit.Print({ " +
        //            "Area: '" + urls.Area + "', " +
        //            "Url: '" + urls.PreviewUrl + scheduleTask.PatientId + "/" + scheduleTask.Id + "', " +
        //            "EditUrl: '" + urls.EditUrl + "', " +
        //            "PdfUrl: '" + urls.PdfUrl + "'," +
        //            "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' }, " +
        //            "Load: " + urls.EditFunction + "," +
        //            "QA: " + (!isNotQA).ToString().ToLower() + "})";
        //    }
        //    else
        //    {
        //        if (DisciplineTaskFactory.AllAssessments(true).Contains(scheduleTask.DisciplineTask))
        //        {
        //            printUrl = "UserInterface.ShowPrint({ " +
        //                "Area: '" + urls.Area + "', " +
        //                "Url: '" + urls.PreviewUrl + scheduleTask.PatientId + "/" + scheduleTask.Id + "'," +
        //                "PdfUrl: '" + urls.PdfUrl + "'," +
        //                "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' }," +
        //                "Return: " + urls.ReturnFunction + "," +
        //                "Load: " + urls.EditFunction + "," +
        //                "QA: " + (!isNotQA).ToString().ToLower() + "," +
        //                "Approve: " + urls.ApproveFunction + " })";
        //        }
        //        else if (DisciplineTaskFactory.SkilledNurseSharedFile().Contains(scheduleTask.DisciplineTask))
        //        {
        //            printUrl = "Visit.Print({ " +
        //               "Area: '" + urls.Area + "', " +
        //               "Url: '" + urls.PreviewUrl + scheduleTask.PatientId + "/" + scheduleTask.Id + "', " +
        //               "EditUrl: '" + urls.EditUrl + "', " +
        //               "PdfUrl: '" + urls.PdfUrl + "'," +
        //               "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' }, " +
        //               "Type: '" + ((DisciplineTasks)scheduleTask.DisciplineTask).ToString() + "'" +
        //               (isNotQA ? string.Empty :
        //               ",ShowPlanOfCareLink: 'true'," +
        //               "QA: true ") + "})";
        //        }
        //        else
        //        {
        //            DisciplineTasks disciplineTask = (DisciplineTasks)scheduleTask.DisciplineTask;
        //            switch (disciplineTask)
        //            {
        //                case DisciplineTasks.FaceToFaceEncounter:
        //                    printUrl = "UserInterface.ShowPrint({ " +
        //                        "Url: '" + urls.PreviewUrl + scheduleTask.PatientId + "/" + scheduleTask.Id + "'," +
        //                        "PdfUrl: '" + urls.PdfUrl + "'," +
        //                        "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' }})";
        //                    break;
        //                case DisciplineTasks.HCFA485StandAlone:
        //                case DisciplineTasks.HCFA485:
        //                case DisciplineTasks.NonOasisHCFA485:
        //                case DisciplineTasks.PhysicianOrder:
        //                case DisciplineTasks.CommunicationNote:
        //                case DisciplineTasks.InfectionReport:
        //                case DisciplineTasks.IncidentAccidentReport:
        //                    printUrl = "UserInterface.ShowPrint({ " +
        //                        "Area: '" + urls.Area + "', " +
        //                        "Url: '" + urls.PreviewUrl + "'," +
        //                        "PdfUrl: '" + urls.PdfUrl + "'," +
        //                        "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' }, " +
        //                        "Return: " + urls.ReturnFunction + "," +
        //                        "Load: " + urls.EditFunction + "," +
        //                        "QA: " + (!isNotQA).ToString().ToLower() + "," +
        //                        "Approve: " + urls.ApproveFunction + " })";
        //                    break;
        //                case DisciplineTasks.HHAideVisit:
        //                    printUrl = "Visit.Print({ " +
        //                        "Area: '" + urls.Area + "', " +
        //                        "Url: '" + urls.PreviewUrl + scheduleTask.PatientId + "/" + scheduleTask.Id + "', " +
        //                        "EditUrl: '" + urls.EditUrl + "', " +
        //                        "PdfUrl: '" + urls.PdfUrl + "'," +
        //                        "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' } " +
        //                        (isNotQA ? string.Empty : ",QA: 'true', " +
        //                        "CarePlan: function() { Schedule.GetHHACarePlanUrlFromHHAVisit('" + episodeId + "','" + scheduleTask.PatientId + "','" + scheduleTask.Id + "'); }, " +
        //                        "ShowPlanOfCareLink: 'true'") + "})";
        //                    break;
        //                case DisciplineTasks.MedicareEligibilityReport:
        //                    printUrl = "U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleTask.PatientId + "', 'mcareEligibilityId': '" + scheduleTask.Id + "' });";
        //                    break;
        //                default:
        //                    printUrl = "Visit.Print({ " +
        //                        "Area: '" + urls.Area + "', " +
        //                        "Url: '" + urls.PreviewUrl + scheduleTask.PatientId + "/" + scheduleTask.Id + "', " +
        //                        "EditUrl: '" + urls.EditUrl + "', " +
        //                        "PdfUrl: '" + urls.PdfUrl + "'," +
        //                        "Data: { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' }, " +
        //                        "Type: '" + ((DisciplineTasks)scheduleTask.DisciplineTask).ToString() + "'," +
        //                        "QA: " + (!isNotQA).ToString().ToLower() + "})";
        //                    break;
        //            }
        //        }
        //    }
        //    return printUrl;
        //}

        /// <summary>
        /// Gets the url for downloading tasks
        /// </summary>
        /// <param name="scheduleTask"></param>
        /// <returns></returns>
        //public static string Download(T scheduleTask)
        //{
        //    UrlContainer urls = new UrlContainer();
        //    UrlHelper.GetDownloadUrl(scheduleTask, urls);
        //    string printUrl = string.Empty;
        //    string linkText = scheduleTask.DisciplineTaskName;
        //    if (scheduleTask.IsMissedVisit)
        //    {
        //        printUrl = "U.GetAttachment('MissedVisit/Pdf', { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' });";
        //    }
        //    else
        //    {
        //        if (DisciplineTaskFactory.AllAssessments(true).Contains(scheduleTask.DisciplineTask) || scheduleTask.IsPlanOfCare() || scheduleTask.IsNote())
        //        {
        //            printUrl = "U.GetAttachment('" + urls.PdfUrl + "', { 'episodeId': '" + scheduleTask.EpisodeId + "', 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' });";
        //        }
        //        else
        //        {
        //            DisciplineTasks task = (DisciplineTasks)scheduleTask.DisciplineTask;
        //            switch (task)
        //            {
        //                case DisciplineTasks.FaceToFaceEncounter:
        //                    printUrl = "U.GetAttachment('FaceToFaceEncounter/Pdf', { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' });";
        //                    break;
        //                case DisciplineTasks.PhysicianOrder:
        //                    printUrl = "U.GetAttachment('Order/Pdf', { 'patientId': '" + scheduleTask.PatientId + "', 'eventId':'" + scheduleTask.Id + "','episodeId':'" + scheduleTask.EpisodeId + "' });";
        //                    break;
        //                case DisciplineTasks.CommunicationNote:
        //                    printUrl = "U.GetAttachment('CommunicationNote/Pdf', { 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "', 'episodeId': '" + scheduleTask.EpisodeId + "' });";
        //                    break;
        //                case DisciplineTasks.InfectionReport:
        //                    printUrl = "U.GetAttachment('Infection/Pdf', { 'episodeId': '" + scheduleTask.EpisodeId + "', 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' });";
        //                    break;
        //                case DisciplineTasks.IncidentAccidentReport:
        //                    printUrl = "U.GetAttachment('Incident/Pdf', { 'episodeId': '" + scheduleTask.EpisodeId + "', 'patientId': '" + scheduleTask.PatientId + "', 'eventId': '" + scheduleTask.Id + "' });";
        //                    break;
        //                case DisciplineTasks.MedicareEligibilityReport:
        //                    printUrl = "U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleTask.PatientId + "', 'mcareEligibilityId': '" + scheduleTask.Id + "' });";
        //                    break;
        //            }
        //        }
        //    }
        //    return printUrl;
        //}

        //public static string GetType(DisciplineTasks task, int status)
        //{
        //    var scheduleTask = new T
        //    {
        //        DisciplineTask = (int)task,
        //        Discipline = task.GetCustomGroup(),
        //        Status = status
        //    };
        //    return GetType(scheduleTask);
        //}

        #endregion

    }
}