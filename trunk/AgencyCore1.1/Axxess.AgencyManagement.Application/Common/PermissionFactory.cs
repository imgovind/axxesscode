﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Entities.Enums;
    

    public static class PermissionFactory
    {
        public static bool IsInPermission(this IDictionary<int, Dictionary<int, List<int>>> instance, AgencyServices service, ParentPermission categorykey, PermissionActions actionKey)
        {
          return  instance.IsInPermission(service, (int)categorykey, actionKey);
        }

        public static bool IsInPermission(this IDictionary<int, Dictionary<int, List<int>>> instance, AgencyServices service, int categorykey, PermissionActions actionKey)
        {
            bool result = false;
            var categoryList = new Dictionary<int, List<int>>();
            if (instance.TryGetValue((int)categorykey, out categoryList))
            {
                List<int> exstingService;
                if (instance!=null && categoryList.TryGetValue((int)actionKey, out exstingService))
                {
                    var comingSerive = (int)service;
                    return exstingService != null && exstingService.Exists(s => ((comingSerive & s) == s));
                }
            }
            return result;
        }

        public static Dictionary<int, List<int>> GetCategoryPermission(this IDictionary<int, Dictionary<int, List<int>>> instance, ParentPermission categorykey)
        {
            var categoryList = new Dictionary<int, List<int>>();
            instance.TryGetValue((int)categorykey, out categoryList);
            return categoryList;
        }

        public static bool IsInPermission(this  Dictionary<int, List<int>> instance, AgencyServices service, PermissionActions actionKey)
        {
            bool result = false;
            List<int> exstingService;
            if (instance != null && instance.TryGetValue((int)actionKey, out exstingService))
            {
                var comingSerive = (int)service;
                return exstingService != null && exstingService.Exists(s => ((comingSerive & s) == s));
            }
            return result;
        }


    }
}
