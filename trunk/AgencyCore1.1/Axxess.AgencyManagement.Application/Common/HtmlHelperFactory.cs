﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;

    public static class HtmlHelperFactory
    {
        private static HtmlHelperAbstraction LoadFactory(int service)
        {
            switch (service)
            {
                case (int)AgencyServices.HomeHealth:
                    return Container.Resolve<HHHtmlHelper>();
                case (int)AgencyServices.PrivateDuty:
                    return Container.Resolve<PDHtmlHelper>();
                default:
                    return Container.Resolve<SharedHtmlHelper>();
            }
        }

        public static MvcHtmlString PatientsForSchedule(this HtmlHelper html, int service, string name, Guid branchId, string value, int status, string title, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).GenerateFilteredPatientsDropDownOnlyFromPatientList(branchId, value, status, true, title), htmlAttributes);
        }

        public static MvcHtmlString PatientsByStatus(this HtmlHelper html, int service, string name, Guid branchId, string value, int status, string title, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).GenerateFilteredPatientProfilesDropDown(branchId, value, status, true, title), htmlAttributes);
        }

        public static MvcHtmlString Patients(this HtmlHelper html, int service, string name, Guid branchId, string value, int status, string title, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).GenerateFilteredPatientProfilesDropDown(branchId, value, status, true, title), htmlAttributes);
        }

        public static MvcHtmlString AllPatients(this HtmlHelper html, int service, string name, Guid branchId, string value, int status, string title, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).GenerateFilteredPatientProfilesDropDown(branchId, value, status, true, title), htmlAttributes);
        }

        public static MvcHtmlString PatientsFilteredByService(this HtmlHelper html, int service, string name, string value, object htmlAttributes)
        {
            return html.SelectList(name, LoadFactory(service).GenerateFilteredPatientsDropDown(value, (AgencyServices)service, true, "-- Select Patient --"), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, int service, string name, string value, Guid patientId, string title, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).PatientEpisodes(patientId, value, true), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, int service, string name, string value, Guid patientId, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).PatientEpisodes(patientId, value, false), htmlAttributes);
        }

        public static MvcHtmlString PatientCarePeriods(this HtmlHelper html, int service, string name, Guid patientId, string value, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).PatientEpisodes(patientId, value, true), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodesExceptCurrent(this HtmlHelper html, int service, string name, string value, Guid patientId, object htmlAttributes)
        {
            return html.DropDownList(name, LoadFactory(service).PatientEpisodes(patientId, value, false), htmlAttributes);
        }

        public static MvcHtmlString PreviousAssessments(this HtmlHelper html, int service, Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate, object htmlAttributes)
        {
            return html.DropDownList("PreviousAssessments", LoadFactory(service).PreviousAssessments(patientId, assessmentId, assessmentType, scheduleDate), htmlAttributes);
        }

        public static MvcHtmlString PreviousCarePlans(this HtmlHelper html, int service, Guid patientId, Guid assessmentId, DisciplineTasks task, DateTime scheduleDate, object htmlAttributes)
        {
            return html.DropDownList("PreviousAssessments", LoadFactory(service).PreviousCarePlans(patientId, assessmentId, task, scheduleDate), htmlAttributes);
        }

        public static MvcHtmlString PatientSchedulesDateRangeFilter(this HtmlHelper html, int service, string name, string value, object htmlAttributes)
        {
            return MvcHtmlString.Create(LoadFactory(service).PatientSchedulesDateRangeFilter(name, value, htmlAttributes));
        }

        //public static MvcHtmlString ClaimDisciplineTask(this HtmlHelper html, string name, int service, int disciplineTask, Guid claimId, ClaimTypeSubCategory typeOfClaim,int payorType, bool isTaskIncluded, object htmlAttributes)
        //{
        //    var list = LoadFactory(service).ClaimDisciplineTask(name, disciplineTask, claimId, typeOfClaim,payorType, isTaskIncluded) ?? new List<SelectListItem>();
        //    return html.DropDownList(name, list.AsEnumerable(), htmlAttributes);
        //}

        public static MvcHtmlString CarePeriodLabel(this HtmlHelper html, int service, object htmlAttributes)
        {
            return MvcHtmlString.Create(LoadFactory(service).CarePeriodLabel(htmlAttributes));
        }

        public static MvcHtmlString PrimaryDiagnosisRow(this HtmlHelper html, int service, string diagnosis, string icd9, object htmlAttributes)
        {
            var htmlString = LoadFactory(service).DiagnosisRow("Primary Diagnosis", diagnosis, icd9, htmlAttributes);
            return htmlString.IsNotNullOrEmpty() ? MvcHtmlString.Create(htmlString) : MvcHtmlString.Empty;
        }

        public static MvcHtmlString SecondaryDiagnosisRow(this HtmlHelper html, int service, string diagnosis, string icd9, object htmlAttributes)
        {
            var htmlString = LoadFactory(service).DiagnosisRow("Secondary Diagnosis", diagnosis, icd9, htmlAttributes);
            return htmlString.IsNotNullOrEmpty() ? MvcHtmlString.Create(htmlString) : MvcHtmlString.Empty;
        }

        public static MvcHtmlString UsersFilteredByService(this HtmlHelper html, int service, string name, string value, object htmlAttributes)
        {
            return html.SelectList(name, LoadFactory(service).GenerateFilteredUsersDropDown(value), htmlAttributes);
        }

        //public static MvcHtmlString PatientChargeRateSelectList(this HtmlHelper html, int service, string name, int disciplineTask, Guid patientId, bool isTaskIncluded, object htmlAttributes)
        //{
        //    var list = LoadFactory(service).GetPatientChargeRateSelectList(patientId, disciplineTask, isTaskIncluded) ?? new List<SelectListItem>();
        //    return html.DropDownList(name, list.AsEnumerable(), htmlAttributes);
        //}

        public static MvcHtmlString PatientPrivatePayorList(this HtmlHelper html, int service, string name, Guid patientId, int value, object htmlAttributes)
        {
            var list = LoadFactory(service).GetPatientPrivatePayors(patientId) ?? new List<PrivatePayor>();
            var items = new List<SelectListItem>();
            if (list.IsNotNullOrEmpty())
            {
                items = list.Select(p => new SelectListItem { Text =  p.FirstName + ", " + p.LastName, Value = p.Id.ToString(), Selected = p.Id == value }).OrderBy(i => i.Text).ToList();
            }
            items.Insert(0, new SelectListItem { Text = "-- Select Payor --", Value = 0.ToString() });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
    }
}
