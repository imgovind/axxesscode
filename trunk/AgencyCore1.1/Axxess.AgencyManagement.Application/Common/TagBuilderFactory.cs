﻿namespace Axxess.AgencyManagement.Application
{
    using System.Web.Mvc;

    using Axxess.Core.Extension;

    public class TagBuilderFactory
    {
        public static readonly string ClearDiv;
        static TagBuilderFactory()
        {
            ClearDiv = BuildTag("div", "clr", string.Empty).ToString();
        }

        public static TagBuilder BuildTag(string tag, string innerHtml)
        {
            TagBuilder tb = new TagBuilder(tag);
            tb.InnerHtml = innerHtml;
            return tb;
        }

        public static TagBuilder BuildTag(string tag, string @class, string innerHtml)
        {
            TagBuilder tb = BuildTag(tag, innerHtml);
            tb.AddCssClass(@class);
            return tb;
        }

        public static TagBuilder BuildTag(string tag, string innerHtml, object htmlAttributes)
        {
            TagBuilder tb = BuildTag(tag, innerHtml);
            tb.AddCssClass(htmlAttributes.GetProperty<string>("class"));
            string toolTip = htmlAttributes.GetProperty<string>("tooltip");
            if (toolTip.IsNotNullOrEmpty())
            {
                tb.Attributes.Add("tooltip", toolTip);
            }
            return tb;
        }

        public static TagBuilder BuildTag(string tag, string @class, string id, string innerHtml)
        {
            TagBuilder tb = BuildTag(tag, @class, innerHtml);
            tb.Attributes.Add("id", id);
            return tb;
        }

        public static TagBuilder BuildTag(string tag, string @class, string id, string name, string innerHtml)
        {
            TagBuilder tb = BuildTag(tag, @class, id, innerHtml);
            tb.Attributes.Add("name", name);
            return tb;
        }

        public static TagBuilder BuildSelectOption(string textValueSelection, string valueToTest)
        {
            TagBuilder tb = BuildTag("option", textValueSelection);
            tb.Attributes.Add("value", textValueSelection);
            if (valueToTest.IsEqual(textValueSelection))
            {
                tb.Attributes.Add("selected", "selected");
            }
            return tb;
        }

        public static TagBuilder BuildSelectOption(string text, string value, bool isSelected)
        {
            TagBuilder tb = BuildTag("option", text);
            tb.Attributes.Add("value", value);
            if (isSelected)
            {
                tb.Attributes.Add("selected", "selected");
            }
            return tb;
        }

        public static TagBuilder BuildInput(string name, string type, string value)
        {
            return BuildInput(name, type, value, string.Empty, string.Empty);
        }

        public static TagBuilder BuildInput(string name, string type, string value, string @class, string id)
        {
            return BuildInput(name, type, value, @class, id, false);
        }

        public static TagBuilder BuildInput(string name, string type, string value, string @class, string id, bool isReadOnly)
        {
            TagBuilder tb = BuildTag("input", @class, id, name, string.Empty);
            tb.Attributes.Add("type", type);
            tb.Attributes.Add("value", value);
            if (isReadOnly)
            {
                tb.Attributes.Add("readonly", "readonly");
            }
            return tb;
        }

        public static TagBuilder BuildRadio(string @class, string id, string name, bool isChecked)
        {
            return BuildRadio(@class, id, name, string.Empty, isChecked);
        }

        public static TagBuilder BuildRadio(string @class, string id, string name, string value, bool isChecked)
        {
            TagBuilder tb = BuildInput(name, "radio", value, @class, id);
            if (isChecked)
            {
                tb.Attributes.Add("checked", "checked");
            }
            return tb;
        }

        public static TagBuilder BuildCheckBox(string @class, string id, string name, bool isChecked)
        {
            return BuildCheckBox(@class, id, name, string.Empty, isChecked);
        }

        public static TagBuilder BuildCheckBox(string @class, string id, string name, string value, bool isChecked)
        {
            TagBuilder tb = BuildInput(name, "checkbox", value, @class, id);
            if (isChecked)
            {
                tb.Attributes.Add("checked", "checked");
            }
            return tb;
        }

        public static TagBuilder BuildLabel(string @class, string @for, string innerHtml)
        {
            TagBuilder tb = BuildTag("label", @class, innerHtml);
            tb.Attributes.Add("for", @for);
            return tb;
        }

        public static TagBuilder BuildLink(string @class, string onclick, string innerHtml)
        {
            TagBuilder tb = BuildTag("a", @class, innerHtml);
            tb.Attributes.Add("href", "javascript:void(0)");
            tb.Attributes.Add("onclick", onclick);
            return tb;
        }

        public static TagBuilder BuildLink(string @class, string href, string target, string innerHtml)
        {
            TagBuilder tb = BuildTag("a", @class, innerHtml);
            tb.Attributes.Add("href", href);
            tb.Attributes.Add("target", target);
            return tb;
        }

        public static TagBuilder BuildRow(string id, string labelText, string innerHtml)
        {
            var innerDiv = BuildTag("div", "fr", innerHtml);
            var label = BuildLabel("fl strong", id, labelText);
            return BuildTag("div", "row", label.ToString() + innerDiv);
        }

        public static TagBuilder BuildRow(string id, string rowClass, string labelText, string innerHtml)
        {
            var innerDiv = BuildTag("div", "fr", innerHtml);
            var label = BuildLabel("fl strong", id, labelText);
            return BuildTag("div", "row " + rowClass, label.ToString() + innerDiv);
        }
    }
}
