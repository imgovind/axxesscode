﻿namespace Axxess.AgencyManagement.Application.Common
{
    using System.Web.Mvc;

    public class GroupedSelectListItem : SelectListItem
    {
        public string GroupKey { get; set; }
        public string GroupName { get; set; }
    }
}