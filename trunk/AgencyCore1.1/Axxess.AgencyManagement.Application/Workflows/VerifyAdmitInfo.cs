﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Entities.Extensions;
using Axxess.AgencyManagement.Application.Services;
using Axxess.Core;
using Axxess.Core.Enums;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.Workflows
{
    public class VerifyAdmitInfo //: IWorkflow
    {
        private Patient patient { get; set; }
        private readonly IPatientService patientService;
       
        public VerifyAdmitInfo(Patient patient)
        {
            this.patientService = Container.Resolve<IPatientService>();
            this.patient = patient;
            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            this.Process();
        }

        private JsonViewData viewData { get; set; }
        public JsonViewData ViewData { get { return viewData; } }


        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }

        public void Process()
        {
            var work = new WorkSequence();

            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
                this.viewData.isSuccessful = !this.IsErrorExist;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = item.Description;
                this.IsErrorExist = true;
            };
            var existingPatient = patientService.GetPatientOnly(patient.Id);
            if (existingPatient != null)
            {
                var serviceCount = 0;
                var serviceExist = 0;

                if (existingPatient.Services.Has(AgencyServices.HomeHealth))
                {
                    serviceExist++;
                    var hhWorkFlow = new HHVerifyAdmitInfo(patient);

                    var hhWorkItems = hhWorkFlow.Works;
                    if (hhWorkItems.IsNotNullOrEmpty())
                    {
                        serviceCount++;
                        work.AddRange(hhWorkItems);
                        viewData.IsNonAdmitPatientListRefresh = existingPatient.HomeHealthStatus == (int)PatientStatus.NonAdmission;
                        viewData.IsPendingPatientListRefresh = existingPatient.HomeHealthStatus == (int)PatientStatus.Pending;
                    }
                    else
                    {
                        this.isCommitted = false;
                        return;
                    }
                }

                if (existingPatient.Services.Has(AgencyServices.PrivateDuty))
                {
                    serviceExist++;
                    var pdWorkFlow = new PrivateDutyVerifyAdmitInfo(patient);

                    var pdWorkItems = pdWorkFlow.Works;
                    if (pdWorkItems.IsNotNullOrEmpty())
                    {
                        serviceCount++;
                        work.AddRange(pdWorkItems);
                    }
                    else
                    {
                        this.isCommitted = false;
                        return;
                    }
                }
               

                if (serviceCount > 0 && serviceCount == serviceExist)
                {
                }
                else
                {
                    this.isCommitted = false;
                    return;
                }


                work.Add(() => {
                   
                    existingPatient.FirstName = patient.FirstName;
                    existingPatient.MiddleInitial = patient.MiddleInitial;
                    existingPatient.LastName = patient.LastName;
                    existingPatient.Gender = patient.Gender;
                    existingPatient.DOB = patient.DOB;
                    existingPatient.MaritalStatus = patient.MaritalStatus;
                    existingPatient.PatientIdNumber = patient.PatientIdNumber;
                   // existingPatient.ReferralDate = patient.ReferralDate;
                    //existingPatient.PatientIdNumber = patient.PatientIdNumber;

                    return patientService.UpdateModal(existingPatient);
                }, () =>
                {

                    patientService.UpdateModal(existingPatient);
                },
                 "System could not admit Home Health patient.");
                if (patient.Physician != null)
                {
                    var physicianId = patient.Physician.Id;
                    if (!physicianId.IsEmpty())
                    {
                        work.Add(() => { return patientService.SetPrimaryIfExistOrAddNew(patient.Id, physicianId); }, null, "Unbale to add the physician.");
                    }
                }
                work.Perform();
                if (this.isCommitted)
                {
                    viewData.IsCenterRefresh = false;
                    viewData.IsPatientListRefresh = true;
                    viewData.IsReferralListRefresh = false;
                }
               
            }
        }
    }

    public class HHVerifyAdmitInfo
    {

        #region AdmitPatientWorkflow Members

        private Patient patient { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private string message { get; set; }
        public string Message { get { return message; } }

        private readonly HHPatientProfileService profileService;

        public HHVerifyAdmitInfo(Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profileService = Container.Resolve<HHPatientProfileService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        private void AddWorkItems()
        {
            var existingProfile = profileService.GetProfileOnly(patient.Id);
            if (existingProfile != null)
            {
                var oldPatientIdNumber = existingProfile.PatientIdNumber;
                var oldLastName = existingProfile.LastName;
                var oldFirstName = existingProfile.FirstName;
                var oldMiddleInitial = existingProfile.MiddleInitial;



                works.Add(new WorkItem(() =>
                {
                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
                    existingProfile.LastName = patient.LastName;
                    existingProfile.FirstName = patient.FirstName;
                    existingProfile.MiddleInitial = patient.MiddleInitial;
                    return profileService.UpdateModal(existingProfile);
                },
                 () =>
                 {
                     existingProfile.PatientIdNumber = oldPatientIdNumber;
                     existingProfile.LastName = oldLastName;
                     existingProfile.FirstName = oldFirstName;
                     existingProfile.MiddleInitial = oldMiddleInitial;

                     profileService.UpdateModal(existingProfile);
                 },
                 "System could not admit Home Health patient."));
            }
        }

    }

    public class PrivateDutyVerifyAdmitInfo
    {
        #region AdmitPatientWorkflow Members

        private Patient patient { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private string message { get; set; }
        public string Message { get { return message; } }
        private readonly PrivateDutyPatientProfileService profileService;
       
        public PrivateDutyVerifyAdmitInfo(Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        private void AddWorkItems()
        {
            var existingProfile = profileService.GetProfileOnly(patient.Id);
            if (existingProfile != null)
            {
                var oldPatientIdNumber = existingProfile.PatientIdNumber;
                var oldLastName = existingProfile.LastName;
                var oldFirstName = existingProfile.FirstName;
                var oldMiddleInitial = existingProfile.MiddleInitial;

                works.Add(new WorkItem(() =>
                {
                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
                    existingProfile.LastName = patient.LastName;
                    existingProfile.FirstName = patient.FirstName;
                    existingProfile.MiddleInitial = patient.MiddleInitial;

                    return profileService.UpdateModal(existingProfile);
                },
                 () =>
                 {
                     existingProfile.PatientIdNumber = oldPatientIdNumber;
                     existingProfile.LastName = oldLastName;
                     existingProfile.FirstName = oldFirstName;
                     existingProfile.MiddleInitial = oldMiddleInitial;

                     profileService.UpdateModal(existingProfile);
                 },
                 "System could not admit Private Duty patient."));
            }
        }

    }
}

