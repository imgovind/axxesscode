﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;
using Axxess.Core;
using Axxess.Core.Enums;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Application.Services;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Entities.Extensions;
using Axxess.AgencyManagement.Application.Helpers;

namespace Axxess.AgencyManagement.Application.Workflows
{
    public class EditWorkflow
    {

         #region AdmitPatientWorkflow Members
        private Patient Patient { get; set; }
        private List<Profile> profiles { get; set; }
        private List<int> servicesToEdit { get; set; }

        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }

        private readonly IPatientService patientService;
        public EditWorkflow(Patient patient, List<Profile> profiles, List<int> servicesToEdit)
        {
            Check.Argument.IsNotNull(profiles, "profile");
            this.Patient = patient;
            this.profiles = profiles;
            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be saved" };
            this.servicesToEdit = servicesToEdit;
            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private JsonViewData viewData { get; set; }
        public JsonViewData ViewData { get { return viewData; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }


        public void Process()
        {
            var work = new WorkSequence();

            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = item.Description;
                this.IsErrorExist = true;
            };
            if (Patient != null)
            {
                 this.Validate();
                 if (this.isValid)
                 {
                     var patientToEdit = patientService.GetPatientOnly(Patient.Id);
                     if (patientToEdit != null)
                     {
                         var patientCloned = patientToEdit.DeepClone();
                         this.Patient.Encode();
                         SetEditData(patientToEdit, this.Patient);
                        
                         var serviceCount = 0;
                         if (patientToEdit.Services.Has(AgencyServices.HomeHealth))
                         {
                             Profile homeHealthProfile = null;
                             var isFullEdit = false;
                             if (this.servicesToEdit.Contains((int)AgencyServices.HomeHealth))
                             {
                                 homeHealthProfile = this.profiles.Where(p => p.ServiceType == (int)AgencyServices.HomeHealth).FirstOrDefault();
                                 if (homeHealthProfile != null)
                                 {
                                     isFullEdit = true;
                                 }
                                 else
                                 {
                                     this.isCommitted = false;
                                     this.viewData.isSuccessful = false;
                                     this.viewData.errorMessage = "System could not edit the Patient. Home Health information not found.";
                                     return;
                                 }
                                 var hhWorkFlow = new HHEditWorkflow();
                                 var hhWorkItems = hhWorkFlow.GetWorkItemsForFullUpdate(patientToEdit, homeHealthProfile, isFullEdit);
                                 if (hhWorkFlow.IsValid)
                                 {
                                     if (hhWorkItems.IsNotNullOrEmpty())
                                     {
                                         if (isFullEdit)
                                         {
                                             serviceCount++;
                                         }
                                         work.AddRange(hhWorkItems);
                                     }
                                     else
                                     {
                                         this.isCommitted = false;
                                         return;
                                     }
                                 }
                                 else
                                 {
                                     this.isCommitted = false;
                                     this.viewData.isSuccessful = false;
                                     this.viewData.errorMessage = hhWorkFlow.Message;
                                     return;
                                 }
                             }

                         }
                         if (patientToEdit.Services.Has(AgencyServices.PrivateDuty))
                         {
                             Profile privateDutyProfile = null;
                             var isFullEdit = false;
                             if (this.servicesToEdit.Contains((int)AgencyServices.PrivateDuty))
                             {
                                 privateDutyProfile = this.profiles.Where(p => p.ServiceType == (int)AgencyServices.PrivateDuty).FirstOrDefault();
                                 if (privateDutyProfile != null)
                                 {
                                     isFullEdit = true;
                                 }
                                 else
                                 {
                                     this.isCommitted = false;
                                     this.viewData.isSuccessful = false;
                                     this.viewData.errorMessage = "System could not edit the Patient. Private Duty information not found.";
                                     return;
                                 }
                             }
                             else
                             {
                                 isFullEdit = false;
                             }
                             var pdWorkFlow = new PrivateDutyEditWorkflow();
                             var pdWorkItems = pdWorkFlow.GetWorkItemsForFullUpdate(patientToEdit, privateDutyProfile, isFullEdit);
                             if (pdWorkFlow.IsValid)
                             {
                                 if (pdWorkItems.IsNotNullOrEmpty())
                                 {
                                     if (isFullEdit)
                                     {
                                         serviceCount++;
                                     }
                                     work.AddRange(pdWorkItems);
                                 }
                                 else
                                 {
                                     this.isCommitted = false;
                                     return;
                                 }
                             }
                             else
                             {
                                 this.isCommitted = false;
                                 this.viewData.isSuccessful = false;
                                 this.viewData.errorMessage = pdWorkFlow.Message;
                                 return;
                             }

                         }

                         if (serviceCount > 0 && serviceCount == servicesToEdit.Count)
                         {
                         }
                         else
                         {
                             this.isCommitted = false;
                             return;
                         }
                         // Insert the patient on zero index
                         work.Insert(0, () => { return patientService.UpdateModal(patientToEdit); }, () =>
                         {
                             patientService.UpdateModal(patientCloned ?? patientToEdit);
                         }, "System could not edit the Patient.");
                         work.Perform();
                         if (this.isCommitted)
                         {
                             viewData.IsCenterRefresh = PatientStatusFactory.CenterStatus().Contains(patientToEdit.HomeHealthStatus) || PatientStatusFactory.CenterStatus().Contains(patientToEdit.PrivateDutyStatus) ? true : false;
                             viewData.IsPendingPatientListRefresh = (patientToEdit.HomeHealthStatus == (int)PatientStatus.Pending || patientToEdit.PrivateDutyStatus == (int)PatientStatus.Pending) ? true : false;
                             viewData.IsPatientListRefresh = true;
                            // viewData.PatientStatus = Status;
                         }
                     }
                     else
                     {
                         this.isCommitted = false;
                         this.viewData.isSuccessful = false;
                         this.viewData.errorMessage = "The patient could not be found. Try again.";
                         return;
                     }
                 }
                 else
                 {
                     this.isCommitted = false;
                 }
            }
        }

        public void SetEditData(Patient patientFromDB, Patient patientFromClient)
        {
            patientFromDB.FirstName = patientFromClient.FirstName;
            patientFromDB.LastName = patientFromClient.LastName;
            patientFromDB.MiddleInitial = patientFromClient.MiddleInitial;
            patientFromDB.Gender = patientFromClient.Gender;
            patientFromDB.DOB = patientFromClient.DOB;
            patientFromDB.MaritalStatus = patientFromClient.MaritalStatus;
            patientFromDB.Height = patientFromClient.Height;
            patientFromDB.HeightMetric = patientFromClient.HeightMetric;
            patientFromDB.Weight = patientFromClient.Weight;
            patientFromDB.WeightMetric = patientFromClient.WeightMetric;
            patientFromDB.PatientIdNumber = patientFromClient.PatientIdNumber;
            patientFromDB.MedicareNumber = patientFromClient.MedicareNumber;
            patientFromDB.MedicaidNumber = patientFromClient.MedicaidNumber;
            patientFromDB.SSN = patientFromClient.SSN;
            patientFromDB.IsDNR = patientFromClient.IsDNR;

            patientFromDB.Ethnicities = patientFromClient.Ethnicities;
            //patientFromDB.PaymentSource = patientFromClient.PaymentSource;
            //patientFromDB.OtherPaymentSource = patientFromClient.OtherPaymentSource;
            patientFromDB.AddressLine1 = patientFromClient.AddressLine1;
            patientFromDB.AddressLine2 = patientFromClient.AddressLine2;
            patientFromDB.AddressCity = patientFromClient.AddressCity;
            patientFromDB.AddressStateCode = patientFromClient.AddressStateCode;
            patientFromDB.AddressZipCode = patientFromClient.AddressZipCode;
            patientFromDB.PhoneHome = patientFromClient.PhoneHome;
            patientFromDB.PhoneMobile = patientFromClient.PhoneMobile;
            patientFromDB.EmailAddress = patientFromClient.EmailAddress;
            patientFromDB.PharmacyName = patientFromClient.PharmacyName;
            patientFromDB.PharmacyPhone = patientFromClient.PharmacyPhone;
            patientFromDB.ReferrerPhysician = patientFromClient.ReferrerPhysician;
            patientFromDB.ServicesRequired = patientFromClient.ServicesRequired;
            patientFromDB.DME = patientFromClient.DME;
            //patientToEdit.Payer = patient.Payer;
            patientFromDB.Triage = patientFromClient.Triage;
            patientFromDB.EvacuationZone = patientFromClient.EvacuationZone;
            patientFromDB.Comments = patientFromClient.Comments;
            patientFromDB.Modified = DateTime.Now;
        }


        #endregion

        public void Validate()
        {
            var rules = this.ValidationRules();
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules()
        {
            var rules = new List<Validation>();

            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.FirstName), "Patient first name is required. <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.LastName), "Patient last name is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
            rules.Add(new Validation(() => !Patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.Gender), "Patient gender has to be selected.  <br/>"));
            rules.Add(new Validation(() => (Patient.EmailAddress == null ? !string.IsNullOrEmpty(Patient.EmailAddress) : !Patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressLine1), "Patient address line is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressCity), "Patient city is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressStateCode), "Patient state is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressZipCode), "Patient zip is required.  <br/>"));
            rules.Add(new Validation(() => !string.IsNullOrEmpty(Patient.SSN) ? !Patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
            rules.Add(new Validation(() => Patient.Triage <= 0, "Emergency Triage is required."));
            if (Patient.SSN.IsNotNullOrEmpty())
            {
                bool ssnNumberCheck = patientService.IsSSNExistForEdit(Patient.Id, Patient.SSN);
                rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
            }
            if (Patient.PatientIdNumber.IsNotNullOrEmpty())
            {
                bool patientIdCheck = patientService.IsPatientIdExistForEdit(Patient.Id, Patient.PatientIdNumber);
                rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
            }
            return rules;
        }
    }

    public class HHEditWorkflow
    {

        #region AdmitPatientWorkflow Members

        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }


        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;
        public HHEditWorkflow()
        {
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<HHPatientProfileService>();
        }

        #endregion

        public List<WorkItem> GetWorkItemsForFullUpdate(Patient patient, Profile profile, bool isFullEdit)
        {
            var works = new List<WorkItem>();
            if (isFullEdit)
            {
                this.Validate(patient,profile);
                if (this.isValid)
                {
                    profile.Encode();
                }
                else
                {

                    return works;
                }
            }
            else
            {
                this.isValid = true;
            }
            //profile.Encode();
            //Profile.AdmissionId = Guid.NewGuid();

            var existingProfile = profileService.GetProfileOnly(patient.Id);
            if (existingProfile != null)
            {
                var profileCloned = existingProfile.DeepClone();

                SetEditData(existingProfile, profile, patient,isFullEdit);

                var admissionWorks = PatientAdmissionWorkItems(patient, existingProfile);
                if (admissionWorks.IsNotNullOrEmpty())
                {
                    works.AddRange(admissionWorks);
                }
                works.Insert(0, new WorkItem(() =>
                {
                    return profileService.UpdateModal(existingProfile);
                },
                () =>
                {
                    profileService.UpdateModal(profileCloned ?? existingProfile);
                }, "System could not edit Home Health patient."));
            }
           return works;
        }

        public List<WorkItem> GetWorkItemsPatientInfoOnly(Patient patient)
        {
            this.isValid = true;
            var works = new List<WorkItem>();
            var existingProfile = profileService.GetProfileOnly(patient.Id);
            if (existingProfile != null)
            {
                var profileCloned = existingProfile.DeepClone();

                SetEditDataPatientInfoOnly(existingProfile,patient);

                var admissionWorks = PatientAdmissionWorkItems(patient, existingProfile);
                if (admissionWorks.IsNotNullOrEmpty())
                {
                    works.AddRange(admissionWorks);
                }
                works.Insert(0, new WorkItem(() =>
                {
                    return profileService.UpdateModal(existingProfile);
                },
                () =>
                {
                    profileService.UpdateModal(profileCloned ?? existingProfile);
                }, "System could not edit Home Health patient."));
            }
            return works;
        }

        private List<WorkItem> PatientAdmissionWorkItems(Patient patient,Profile existingProfile)
        {
            var admissionWorks = new List<WorkItem>();
            var isAdmissionExist = false;
            if (!existingProfile.AdmissionId.IsEmpty())
            {
                var admissionData = AdmissionHelperFactory<PatientEpisode>.GetPatientAdmissionDate(existingProfile.Id, existingProfile.AdmissionId);
                if (admissionData != null)
                {
                    isAdmissionExist = true;
                    var admissionCloned = admissionData.DeepClone();
                    EntityHelper.SetPatientAdmissionDate(admissionData, existingProfile, patient);
                    admissionWorks.Add(new WorkItem(() => AdmissionHelperFactory<PatientEpisode>.UpdatePatientAdmissionDate(admissionData),
                        () =>
                        {
                            AdmissionHelperFactory<PatientEpisode>.UpdatePatientAdmissionDate(admissionCloned ?? admissionData);
                        },
                        "System could not edit the patient."));
                }
            }
            if (!isAdmissionExist)
            {
                existingProfile.AdmissionId = Guid.NewGuid();
                admissionWorks.Add(new WorkItem(() => AdmissionHelperFactory<PatientEpisode>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient, existingProfile, 0, string.Empty, existingProfile.AdmissionId)),
                    () => AdmissionHelperFactory<PatientEpisode>.RemovePatientAdmissionDate(patient.Id, existingProfile.AdmissionId),
                    "System could not edit the patient."));
            }
            return admissionWorks;
        }

        public void SetEditData(Profile profileFromDB, Profile profileClientInput, Patient patientClientInput, bool isFullEdit)
        {

            profileFromDB.PatientIdNumber = patientClientInput.PatientIdNumber;
            profileFromDB.FirstName = patientClientInput.FirstName;
            profileFromDB.LastName = patientClientInput.LastName;
            profileFromDB.MiddleInitial = patientClientInput.MiddleInitial;
            if (isFullEdit)
            {
                if (profileClientInput.PrimaryInsurance >= 1000)
                {
                    profileFromDB.PrimaryHealthPlanId = profileClientInput.PrimaryHealthPlanId;
                    profileFromDB.PrimaryGroupName = profileClientInput.PrimaryGroupName;
                    profileFromDB.PrimaryGroupId = profileClientInput.PrimaryGroupId;
                    profileFromDB.PrimaryRelationship = profileClientInput.PrimaryRelationship;
                }
                else profileFromDB.PrimaryHealthPlanId = profileFromDB.PrimaryGroupName = profileFromDB.PrimaryGroupId = string.Empty;

                if (profileClientInput.SecondaryInsurance >= 1000)
                {
                    profileFromDB.SecondaryHealthPlanId = profileClientInput.SecondaryHealthPlanId;
                    profileFromDB.SecondaryGroupName = profileClientInput.SecondaryGroupName;
                    profileFromDB.SecondaryGroupId = profileClientInput.SecondaryGroupId;
                    profileFromDB.SecondaryRelationship = profileClientInput.SecondaryRelationship;
                }
                else profileFromDB.SecondaryHealthPlanId = profileFromDB.SecondaryGroupName = profileFromDB.SecondaryGroupId = string.Empty;

                if (profileClientInput.TertiaryInsurance >= 1000)
                {
                    profileFromDB.TertiaryHealthPlanId = profileClientInput.TertiaryHealthPlanId;
                    profileFromDB.TertiaryGroupName = profileClientInput.TertiaryGroupName;
                    profileFromDB.TertiaryGroupId = profileClientInput.TertiaryGroupId;
                    profileFromDB.TertiaryRelationship = profileClientInput.TertiaryRelationship;
                }
                else profileFromDB.TertiaryHealthPlanId = profileFromDB.TertiaryGroupName = profileFromDB.TertiaryGroupId = string.Empty;

                profileFromDB.AgencyLocationId = profileClientInput.AgencyLocationId;
                profileFromDB.StartofCareDate = profileClientInput.StartofCareDate;
                if (profileClientInput.Status == (int)PatientStatus.Discharged)
                {
                    profileFromDB.DischargeDate = profileClientInput.DischargeDate;
                    profileFromDB.DischargeReason = profileClientInput.DischargeReason;
                    profileFromDB.DischargeReasonId = profileClientInput.DischargeReasonId;
                }
                profileFromDB.PrimaryInsurance = profileClientInput.PrimaryInsurance;
                profileFromDB.SecondaryInsurance = profileClientInput.SecondaryInsurance;
                profileFromDB.TertiaryInsurance = profileClientInput.TertiaryInsurance;
                profileFromDB.CaseManagerId = profileClientInput.CaseManagerId;
                profileFromDB.UserId = profileClientInput.UserId;
                profileFromDB.PaymentSource = profileClientInput.PaymentSource;
                profileFromDB.OtherPaymentSource = profileClientInput.OtherPaymentSource;

                profileFromDB.AdmissionSource = profileClientInput.AdmissionSource;
                profileFromDB.ReferralDate = profileClientInput.ReferralDate;
                profileFromDB.InternalReferral = profileClientInput.InternalReferral;
                profileFromDB.OtherReferralSource = profileClientInput.OtherReferralSource;
                profileFromDB.AuditorId = profileClientInput.AuditorId;
                profileFromDB.Modified = DateTime.Now;
            }
        }

        public void SetEditDataPatientInfoOnly(Profile profileFromDB, Patient patientClientInput)
        {
            profileFromDB.PatientIdNumber = patientClientInput.PatientIdNumber;
            profileFromDB.FirstName = patientClientInput.FirstName;
            profileFromDB.LastName = patientClientInput.LastName;
            profileFromDB.MiddleInitial = patientClientInput.MiddleInitial;

        }

        public void Validate(Patient patient,Profile profile)
        {
            var rules = this.ValidationRules(patient, profile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Patient patient,Profile profile)
        {
            var rules = new List<Validation>();
            rules.Add(new Validation(() => string.IsNullOrEmpty(profile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
            rules.Add(new Validation(() => !profile.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
            if (patient.MedicareNumber.IsNotNullOrEmpty())
            {
                bool medicareNumberCheck = patientService.IsMedicareExistForEdit(patient.Id, patient.MedicareNumber);
                rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
            }
            if (patient.MedicaidNumber.IsNotNullOrEmpty())
            {
                bool medicaidNumberCheck = patientService.IsMedicaidExistForEdit(patient.Id, patient.MedicaidNumber);
                rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
            }
            if (profile.ReferralDate.IsValid())
            {
                rules.Add(new Validation(() => profile.StartofCareDate < profile.ReferralDate, "Referral date must be less than or equal to the start of care."));
            }
            if (profile.Status == (int)PatientStatus.Discharged)
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(profile.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !profile.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
            }
            if (profile.PrimaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => profile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
            }
            if (profile.SecondaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => profile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
            }

            if (profile.TertiaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => profile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
            } return rules;
        }
    }

    public class PrivateDutyEditWorkflow
    {
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }
        private readonly IPatientService patientService;
        private readonly PrivateDutyPatientProfileService profileService;

        public PrivateDutyEditWorkflow()
        {
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
        }


        public List<WorkItem> GetWorkItemsForFullUpdate(Patient patient, Profile profile, bool isFullEdit)
        {
            var works = new List<WorkItem>();
            if (isFullEdit)
            {
                this.Validate(profile);
                if (this.IsValid)
                {
                    profile.Encode();
                }
                else
                {

                    return works;
                }
            }
            else
            {
                this.isValid = true;
            }
            var existingProfile = profileService.GetProfileOnly(patient.Id);
            if (existingProfile != null)
            {
                var profileCloned = existingProfile.DeepClone();

                SetEditData(existingProfile, profile, patient, isFullEdit);
                var admissionWorks = PatientAdmissionWorkItems(patient, existingProfile);
                if (admissionWorks.IsNotNullOrEmpty())
                {
                    works.AddRange(admissionWorks);
                }

                works.Insert(0, new WorkItem(() =>
                 {
                   return profileService.UpdateModal(existingProfile);
                 },
                () =>
                {
                    profileService.UpdateModal(profileCloned ?? existingProfile);
                }, "System could not edit Private Duty patient."));
            }
            return works;
        }

        public List<WorkItem> GetWorkItemsPatientInfoOnly(Patient patient)
        {
            this.isValid = true;
            var works = new List<WorkItem>();
            var existingProfile = profileService.GetProfileOnly(patient.Id);
            if (existingProfile != null)
            {
                var profileCloned = existingProfile.DeepClone();
                SetEditDataPatientInfoOnly(existingProfile, patient);
                var admissionWorks = PatientAdmissionWorkItems(patient, existingProfile);
                if (admissionWorks.IsNotNullOrEmpty())
                {
                    works.AddRange(admissionWorks);
                }

                works.Insert(0, new WorkItem(() =>
                {
                    return profileService.UpdateModal(existingProfile);
                },
                () =>
                {
                    profileService.UpdateModal(profileCloned ?? existingProfile);
                }, "System could not edit Private Duty patient."));
            }
            return works;
        }

        private List<WorkItem> PatientAdmissionWorkItems(Patient patient,Profile existingProfile)
        {
            var admissionWorks = new List<WorkItem>();
            var isAdmissionExist = false;
            if (!existingProfile.AdmissionId.IsEmpty())
            {
                var admissionData = AdmissionHelperFactory<PrivateDutyCarePeriod>.GetPatientAdmissionDate(existingProfile.Id, existingProfile.AdmissionId);
                if (admissionData != null)
                {
                    isAdmissionExist = true;
                    var admissionCloned = admissionData.DeepClone();
                    EntityHelper.SetPatientAdmissionDate(admissionData, existingProfile, patient);
                    admissionWorks.Add(new WorkItem(() => AdmissionHelperFactory<PrivateDutyCarePeriod>.UpdatePatientAdmissionDate(admissionData),
                        () =>
                        {
                            AdmissionHelperFactory<PrivateDutyCarePeriod>.UpdatePatientAdmissionDate(admissionCloned ?? admissionData);
                        },
                        "System could not edit the patient."));
                }
            }
            if (!isAdmissionExist)
            {
                existingProfile.AdmissionId = Guid.NewGuid();
                admissionWorks.Add(new WorkItem(() => AdmissionHelperFactory<PrivateDutyCarePeriod>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient, existingProfile, 0, string.Empty, existingProfile.AdmissionId)),
                    () => AdmissionHelperFactory<PrivateDutyCarePeriod>.RemovePatientAdmissionDate(patient.Id, existingProfile.AdmissionId),
                    "System could not edit the patient."));
            }
            return admissionWorks;
        }

        public void SetEditData(Profile profileFromDB, Profile profileClientInput, Patient patientClientInput, bool isFullEdit)
        {
            profileFromDB.PatientIdNumber = patientClientInput.PatientIdNumber;
            profileFromDB.FirstName = patientClientInput.FirstName;
            profileFromDB.LastName = patientClientInput.LastName;
            profileFromDB.MiddleInitial = patientClientInput.MiddleInitial;
            if (isFullEdit)
            {
                if (profileClientInput.PrimaryInsurance >= 1000)
                {
                    profileFromDB.PrimaryHealthPlanId = profileClientInput.PrimaryHealthPlanId;
                    profileFromDB.PrimaryGroupName = profileClientInput.PrimaryGroupName;
                    profileFromDB.PrimaryGroupId = profileClientInput.PrimaryGroupId;
                    profileFromDB.PrimaryRelationship = profileClientInput.PrimaryRelationship;
                }
                else profileFromDB.PrimaryHealthPlanId = profileFromDB.PrimaryGroupName = profileFromDB.PrimaryGroupId = string.Empty;

                if (profileClientInput.SecondaryInsurance >= 1000)
                {
                    profileFromDB.SecondaryHealthPlanId = profileClientInput.SecondaryHealthPlanId;
                    profileFromDB.SecondaryGroupName = profileClientInput.SecondaryGroupName;
                    profileFromDB.SecondaryGroupId = profileClientInput.SecondaryGroupId;
                    profileFromDB.SecondaryRelationship = profileClientInput.SecondaryRelationship;
                }
                else profileFromDB.SecondaryHealthPlanId = profileFromDB.SecondaryGroupName = profileFromDB.SecondaryGroupId = string.Empty;

                if (profileClientInput.TertiaryInsurance >= 1000)
                {
                    profileFromDB.TertiaryHealthPlanId = profileClientInput.TertiaryHealthPlanId;
                    profileFromDB.TertiaryGroupName = profileClientInput.TertiaryGroupName;
                    profileFromDB.TertiaryGroupId = profileClientInput.TertiaryGroupId;
                    profileFromDB.TertiaryRelationship = profileClientInput.TertiaryRelationship;
                }
                else profileFromDB.TertiaryHealthPlanId = profileFromDB.TertiaryGroupName = profileFromDB.TertiaryGroupId = string.Empty;

                profileFromDB.AgencyLocationId = profileClientInput.AgencyLocationId;
                profileFromDB.StartofCareDate = profileClientInput.StartofCareDate;
                if (profileClientInput.Status == (int)PatientStatus.Discharged)
                {
                    profileFromDB.DischargeDate = profileClientInput.DischargeDate;
                    profileFromDB.DischargeReason = profileClientInput.DischargeReason;
                    profileFromDB.DischargeReasonId = profileClientInput.DischargeReasonId;
                }
                profileFromDB.PrimaryInsurance = profileClientInput.PrimaryInsurance;
                profileFromDB.SecondaryInsurance = profileClientInput.SecondaryInsurance;
                profileFromDB.TertiaryInsurance = profileClientInput.TertiaryInsurance;
                profileFromDB.CaseManagerId = profileClientInput.CaseManagerId;
                profileFromDB.UserId = profileClientInput.UserId;
                profileFromDB.PaymentSource = profileClientInput.PaymentSource;
                profileFromDB.OtherPaymentSource = profileClientInput.OtherPaymentSource;

                profileFromDB.AdmissionSource = profileClientInput.AdmissionSource;
                profileFromDB.ReferralDate = profileClientInput.ReferralDate;
                profileFromDB.InternalReferral = profileClientInput.InternalReferral;
                profileFromDB.OtherReferralSource = profileClientInput.OtherReferralSource;
                profileFromDB.AuditorId = profileClientInput.AuditorId;
                profileFromDB.Modified = DateTime.Now;
            }
        }

        public void SetEditDataPatientInfoOnly(Profile profileFromDB, Patient patientClientInput)
        {
            profileFromDB.PatientIdNumber = patientClientInput.PatientIdNumber;
            profileFromDB.FirstName = patientClientInput.FirstName;
            profileFromDB.LastName = patientClientInput.LastName;
            profileFromDB.MiddleInitial = patientClientInput.MiddleInitial;
           
        }


        public void Validate(Profile profile)
        {
            var rules = this.ValidationRules(profile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile profile)
        {
            var rules = new List<Validation>();
            rules.Add(new Validation(() => string.IsNullOrEmpty(profile.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
            rules.Add(new Validation(() => !profile.StartofCareDate.IsValid(), "Patient Start of care date is not in valid format.  <br/>"));
            if (profile.Status == (int)PatientStatus.Discharged)
            {
                rules.Add(new Validation(() => string.IsNullOrEmpty(profile.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !profile.DischargeDate.IsValid(), "Patient Discharge date is not in valid format.  <br/>"));
            }
            if (profile.PrimaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => profile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
            }
            if (profile.SecondaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => profile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
            }
            if (profile.TertiaryInsurance >= 1000)
            {
                rules.Add(new Validation(() => profile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
            }
            return rules;
        }
    }


    public class EditPatientInfoWorkflow
    {

        #region AdmitPatientWorkflow Members
        private Patient Patient { get; set; }

        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }

        private readonly IPatientService patientService;
        public EditPatientInfoWorkflow(Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");
            this.Patient = patient;
            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be saved" };
            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private JsonViewData viewData { get; set; }
        public JsonViewData ViewData { get { return viewData; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }


        public void Process()
        {
            var work = new WorkSequence();

            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = item.Description;
                this.IsErrorExist = true;
            };
            if (Patient != null)
            {
                this.Validate();
                if (this.isValid)
                {
                    var patientToEdit = patientService.GetPatientOnly(Patient.Id);
                    if (patientToEdit != null)
                    {
                        var patientCloned = patientToEdit.DeepClone();
                        this.Patient.Encode();
                        var isServiceProfileInfoChanged = this.IsServiceProfileInfoChanged(patientToEdit, this.Patient);
                        SetEditData(patientToEdit, this.Patient);
                        if (isServiceProfileInfoChanged)
                        {
                            if (patientToEdit.Services.Has(AgencyServices.HomeHealth))
                            {
                                var hhWorkFlow = new HHEditWorkflow();
                                var hhWorkItems = hhWorkFlow.GetWorkItemsPatientInfoOnly(patientToEdit);
                                if (hhWorkFlow.IsValid)
                                {
                                    if (hhWorkItems.IsNotNullOrEmpty())
                                    {
                                        work.AddRange(hhWorkItems);
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        return;
                                    }
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.viewData.isSuccessful = false;
                                    this.viewData.errorMessage = hhWorkFlow.Message;
                                    return;
                                }

                            }
                            if (patientToEdit.Services.Has(AgencyServices.PrivateDuty))
                            {
                                var pdWorkFlow = new PrivateDutyEditWorkflow();
                                var pdWorkItems = pdWorkFlow.GetWorkItemsPatientInfoOnly(patientToEdit);
                                if (pdWorkFlow.IsValid)
                                {
                                    if (pdWorkItems.IsNotNullOrEmpty())
                                    {
                                        work.AddRange(pdWorkItems);
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        return;
                                    }
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.viewData.isSuccessful = false;
                                    this.viewData.errorMessage = pdWorkFlow.Message;
                                    return;
                                }

                            }

                        }
                        // Insert the patient on zero index
                        work.Insert(0, () => { return patientService.UpdateModal(patientToEdit); }, () =>
                        {
                            patientService.UpdateModal(patientCloned ?? patientToEdit);
                        }, "System could not edit the Patient.");
                        work.Perform();
                        if (this.isCommitted)
                        {
                            if (isServiceProfileInfoChanged)
                            {
                                viewData.IsCenterRefresh = PatientStatusFactory.CenterStatus().Contains(patientToEdit.HomeHealthStatus) || PatientStatusFactory.CenterStatus().Contains(patientToEdit.PrivateDutyStatus) ? true : false;
                                viewData.IsPendingPatientListRefresh = (patientToEdit.HomeHealthStatus == (int)PatientStatus.Pending || patientToEdit.PrivateDutyStatus == (int)PatientStatus.Pending) ? true : false;
                                viewData.IsPatientListRefresh = true;
                            }
                            // viewData.PatientStatus = Status;
                        }

                    }
                    else
                    {
                        this.isCommitted = false;
                        this.viewData.isSuccessful = false;
                        this.viewData.errorMessage = "The patient could not be found. Try again.";
                        return;
                    }
                }
                else
                {
                    this.isCommitted = false;
                }
            }
        }

        public void SetEditData(Patient patientFromDB, Patient patientFromClient)
        {
            patientFromDB.FirstName = patientFromClient.FirstName;
            patientFromDB.LastName = patientFromClient.LastName;
            patientFromDB.MiddleInitial = patientFromClient.MiddleInitial;
            patientFromDB.Gender = patientFromClient.Gender;
            patientFromDB.DOB = patientFromClient.DOB;
            patientFromDB.MaritalStatus = patientFromClient.MaritalStatus;
            patientFromDB.Height = patientFromClient.Height;
            patientFromDB.HeightMetric = patientFromClient.HeightMetric;
            patientFromDB.Weight = patientFromClient.Weight;
            patientFromDB.WeightMetric = patientFromClient.WeightMetric;
            patientFromDB.PatientIdNumber = patientFromClient.PatientIdNumber;
            patientFromDB.MedicareNumber = patientFromClient.MedicareNumber;
            patientFromDB.MedicaidNumber = patientFromClient.MedicaidNumber;
            patientFromDB.SSN = patientFromClient.SSN;
            patientFromDB.IsDNR = patientFromClient.IsDNR;

            patientFromDB.Ethnicities = patientFromClient.Ethnicities;
            //patientFromDB.PaymentSource = patientFromClient.PaymentSource;
            //patientFromDB.OtherPaymentSource = patientFromClient.OtherPaymentSource;
            patientFromDB.AddressLine1 = patientFromClient.AddressLine1;
            patientFromDB.AddressLine2 = patientFromClient.AddressLine2;
            patientFromDB.AddressCity = patientFromClient.AddressCity;
            patientFromDB.AddressStateCode = patientFromClient.AddressStateCode;
            patientFromDB.AddressZipCode = patientFromClient.AddressZipCode;
            patientFromDB.PhoneHome = patientFromClient.PhoneHome;
            patientFromDB.PhoneMobile = patientFromClient.PhoneMobile;
            patientFromDB.EmailAddress = patientFromClient.EmailAddress;
            patientFromDB.PharmacyName = patientFromClient.PharmacyName;
            patientFromDB.PharmacyPhone = patientFromClient.PharmacyPhone;
            patientFromDB.ReferrerPhysician = patientFromClient.ReferrerPhysician;
            patientFromDB.ServicesRequired = patientFromClient.ServicesRequired;
            patientFromDB.DME = patientFromClient.DME;
            //patientToEdit.Payer = patient.Payer;
            patientFromDB.Triage = patientFromClient.Triage;
            patientFromDB.EvacuationZone = patientFromClient.EvacuationZone;
            patientFromDB.Comments = patientFromClient.Comments;
            patientFromDB.Modified = DateTime.Now;
        }


        #endregion

        public void Validate()
        {
            var rules = this.ValidationRules();
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules()
        {
            var rules = new List<Validation>();

            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.FirstName), "Patient first name is required. <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.LastName), "Patient last name is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
            rules.Add(new Validation(() => !Patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.Gender), "Patient gender has to be selected.  <br/>"));
            rules.Add(new Validation(() => (Patient.EmailAddress == null ? !string.IsNullOrEmpty(Patient.EmailAddress) : !Patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressLine1), "Patient address line is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressCity), "Patient city is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressStateCode), "Patient state is required.  <br/>"));
            rules.Add(new Validation(() => string.IsNullOrEmpty(Patient.AddressZipCode), "Patient zip is required.  <br/>"));
            rules.Add(new Validation(() => !string.IsNullOrEmpty(Patient.SSN) ? !Patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
            rules.Add(new Validation(() => Patient.Triage <= 0, "Emergency Triage is required."));
            if (Patient.SSN.IsNotNullOrEmpty())
            {
                bool ssnNumberCheck = patientService.IsSSNExistForEdit(Patient.Id, Patient.SSN);
                rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
            }
            if (Patient.PatientIdNumber.IsNotNullOrEmpty())
            {
                bool patientIdCheck = patientService.IsPatientIdExistForEdit(Patient.Id, Patient.PatientIdNumber);
                rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
            }
            return rules;
        }

        private bool IsServiceProfileInfoChanged(Patient patientFromDB, Patient patientFromClient)
        {
            var result = false;
            if (!(patientFromDB.PatientIdNumber.ToLowerCase() == patientFromClient.PatientIdNumber.ToLowerCase()))
            {
                return true;
            }
            else if (!(patientFromDB.FirstName.ToLowerCase() == patientFromClient.FirstName.ToLowerCase()))
            {
                return true;
            }
            else if (!(patientFromDB.LastName.ToLowerCase() == patientFromClient.LastName.ToLowerCase()))
            {
                return true;
            }
            else if (!(patientFromDB.MiddleInitial.ToLowerCase() == patientFromClient.MiddleInitial.ToLowerCase()))
            {
                return true;
            }
            return result;

        }
    }
}
