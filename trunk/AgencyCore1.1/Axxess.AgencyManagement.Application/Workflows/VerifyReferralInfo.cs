﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Axxess.Core.Extension;
//using Axxess.Core.Infrastructure;
//using Axxess.AgencyManagement.Entities;
//using Axxess.AgencyManagement.Entities.Extensions;
//using Axxess.AgencyManagement.Application.Services;
//using Axxess.Core;
//using Axxess.AgencyManagement.Entities.Enums;

//namespace Axxess.AgencyManagement.Application.Workflows
//{
//    public class VerifyReferralInfo //: IWorkflow
//    {
//        private Referral referral { get; set; }
//        private readonly IReferralService referralService;
//        private readonly IPatientService patientService;

//        public VerifyReferralInfo(Referral referral)
//        {
//            this.referral = referral;
//            this.referralService = Container.Resolve<IReferralService>();
//            this.patientService = Container.Resolve<IPatientService>();
//            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
//            this.Process();
//        }

//        private JsonViewData viewData { get; set; }
//        public JsonViewData ViewData { get { return viewData; } }


//        private bool isCommitted { get; set; }
//        public bool IsCommitted { get { return isCommitted; } }

//        private bool IsErrorExist { get; set; }

//        public void Process()
//        {
//            var work = new WorkSequence();

//            work.Complete += (sequence) =>
//            {
//                this.isCommitted = !this.IsErrorExist;
//                this.viewData.isSuccessful = !this.IsErrorExist;
//            };

//            work.Error += (sequence, item, index) =>
//            {
//                this.isCommitted = false;
//                this.viewData.isSuccessful = false;
//                this.viewData.errorMessage = item.Description;
//                this.IsErrorExist = true;
//            };
//            var existingReferral = referralService.GetReferral(referral.Id);
//            if (existingReferral != null)
//            {
//                var patient = patientService.GetPatientOnly(referral.Id);
//                var IsPatientExist = false;
//                if (patient != null)
//                {
//                    IsPatientExist = true;
//                }
                
//                var serviceCount = 0;
//                var serviceExist = 0;
//                if (IsPatientExist)
//                {
//                    if (patient.Services.Has(AgencyServices.HomeHealth))
//                    {
//                        serviceExist++;
//                        var hhWorkFlow = new HHVerifyReferralInfo(patient);

//                        var hhWorkItems = hhWorkFlow.Works;
//                        if (hhWorkItems.IsNotNullOrEmpty())
//                        {
//                            serviceCount++;
//                            work.AddRange(hhWorkItems);
//                            viewData.IsNonAdmitPatientListRefresh = existingReferral.HomeHealthStatus == (int)PatientStatus.NonAdmission;
//                            viewData.IsPendingPatientListRefresh = existingReferral.HomeHealthStatus == (int)PatientStatus.Pending;
//                        }
//                        else
//                        {
//                            this.isCommitted = false;
//                            return;
//                        }
//                    }

//                    if (patient.Services.Has(AgencyServices.PrivateDuty))
//                    {
//                        serviceExist++;
//                        var pdWorkFlow = new PrivateDutyVerifyReferralInfo(patient);

//                        var pdWorkItems = pdWorkFlow.Works;
//                        if (pdWorkItems.IsNotNullOrEmpty())
//                        {
//                            serviceCount++;
//                            work.AddRange(pdWorkItems);
//                        }
//                        else
//                        {
//                            this.isCommitted = false;
//                            return;
//                        }
//                    }


//                    if (serviceCount > 0 && serviceCount == serviceExist)
//                    {
//                    }
//                    else
//                    {
//                        this.isCommitted = false;
//                        return;
//                    }
//                }
//                work.Add(() => {
                   
//                    existingReferral.FirstName = referral.FirstName;
//                    existingReferral.MiddleInitial = referral.MiddleInitial;
//                    existingReferral.LastName = referral.LastName;
//                    existingReferral.Gender = referral.Gender;
//                    existingReferral.DOB = referral.DOB;
//                    existingReferral.MaritalStatus = referral.MaritalStatus;
//                    existingReferral.PatientIdNumber = referral.PatientIdNumber;
//                   // existingPatient.ReferralDate = patient.ReferralDate;
//                    //existingPatient.PatientIdNumber = patient.PatientIdNumber;

//                    return referralService.UpdateModal(existingReferral);
//                }, () =>
//                {

//                    referralService.UpdateModal(existingReferral);
//                },
//                 "System could not admit Home Health patient.");
//                if (referral.Physician != null)
//                {
//                    var physicianId = referral.Physician.Id;
//                    if (!physicianId.IsEmpty())
//                    {
//                        work.Add(() => { return referralService.SetPrimaryIfExistOrAddNew(referral.Id, physicianId); }, null, "Unbale to add the physician.");
//                    }
//                }
//                work.Perform();
//                if (this.isCommitted)
//                {
//                    viewData.IsCenterRefresh = false;
//                    viewData.IsPatientListRefresh = true;
//                    viewData.IsReferralListRefresh = false;
//                }
               
//            }
//        }
//    }

//    public class HHVerifyReferralInfo
//    {

//        #region AdmitPatientWorkflow Members

//        private Patient patient { get; set; }
//        public List<WorkItem> works { get; set; }
//        public List<WorkItem> Works { get { return works; } }
//        private string message { get; set; }
//        public string Message { get { return message; } }

//        private readonly HHPatientProfileService profileService;

//        public HHVerifyReferralInfo(Patient patient)
//        {
//            Check.Argument.IsNotNull(patient, "patient");

//            this.patient = patient;
//            this.profileService = Container.Resolve<HHPatientProfileService>();
//            this.works = new List<WorkItem>();
//            this.AddWorkItems();
//        }

//        #endregion

//        private void AddWorkItems()
//        {
//            var existingProfile = profileService.GetProfileOnly(patient.Id);
//            if (existingProfile != null)
//            {
//                var oldPatientIdNumber = existingProfile.PatientIdNumber;
//                var oldLastName = existingProfile.LastName;
//                var oldFirstName = existingProfile.FirstName;
//                var oldMiddleInitial = existingProfile.MiddleInitial;



//                works.Add(new WorkItem(() =>
//                {
//                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
//                    existingProfile.LastName = patient.LastName;
//                    existingProfile.FirstName = patient.FirstName;
//                    existingProfile.MiddleInitial = patient.MiddleInitial;
//                    return profileService.UpdateModal(existingProfile);
//                },
//                 () =>
//                 {
//                     existingProfile.PatientIdNumber = oldPatientIdNumber;
//                     existingProfile.LastName = oldLastName;
//                     existingProfile.FirstName = oldFirstName;
//                     existingProfile.MiddleInitial = oldMiddleInitial;

//                     profileService.UpdateModal(existingProfile);
//                 },
//                 "System could not admit Home Health patient."));
//            }
//        }

//    }

//    public class PrivateDutyVerifyReferralInfo
//    {
//        #region AdmitPatientWorkflow Members

//        private Patient patient { get; set; }
//        public List<WorkItem> works { get; set; }
//        public List<WorkItem> Works { get { return works; } }
//        private string message { get; set; }
//        public string Message { get { return message; } }
//        private readonly PrivateDutyPatientProfileService profileService;
       
//        public PrivateDutyVerifyReferralInfo(Patient patient)
//        {
//            Check.Argument.IsNotNull(patient, "patient");

//            this.patient = patient;
//            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
//            this.works = new List<WorkItem>();
//            this.AddWorkItems();
//        }

//        #endregion

//        private void AddWorkItems()
//        {
//            var existingProfile = profileService.GetProfileOnly(patient.Id);
//            if (existingProfile != null)
//            {
//                var oldPatientIdNumber = existingProfile.PatientIdNumber;
//                var oldLastName = existingProfile.LastName;
//                var oldFirstName = existingProfile.FirstName;
//                var oldMiddleInitial = existingProfile.MiddleInitial;



//                works.Add(new WorkItem(() =>
//                {
//                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
//                    existingProfile.LastName = patient.LastName;
//                    existingProfile.FirstName = patient.FirstName;
//                    existingProfile.MiddleInitial = patient.MiddleInitial;

//                    return profileService.UpdateModal(existingProfile);
//                },
//                 () =>
//                 {
//                     existingProfile.PatientIdNumber = oldPatientIdNumber;
//                     existingProfile.LastName = oldLastName;
//                     existingProfile.FirstName = oldFirstName;
//                     existingProfile.MiddleInitial = oldMiddleInitial;

//                     profileService.UpdateModal(existingProfile);
//                 },
//                 "System could not admit Private Duty patient."));
//            }
//        }

//    }
//}

