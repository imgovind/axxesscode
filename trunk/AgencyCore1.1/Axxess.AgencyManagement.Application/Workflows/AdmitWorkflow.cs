﻿namespace Axxess.AgencyManagement.Application.Workflows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;

    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.Helpers;

    public class AdmitWorkflow
    {
        #region AdmitPatientWorkflow Members

        private Profile profile { get; set; }
        private AgencyServices service { get; set; }

        private readonly IPatientService patientService;
        public AdmitWorkflow(Profile profile, AgencyServices service)
        {
            Check.Argument.IsNotNull(profile, "profile");

            this.profile = profile;
            this.service = service;
            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }


        public void Process()
        {
            var work = new WorkSequence();

            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
                this.IsErrorExist = true;
            };
            var patient = patientService.GetPatientOnly(profile.Id);
            if (patient != null)
            {
                var oldHHStatus = patient.HomeHealthStatus;
                var oldPdStatus = patient.PrivateDutyStatus;

                var serviceCount = 0;
                if (patient.Services.Has(this.service))
                {
                    if (this.service.Has(AgencyServices.HomeHealth))
                    {
                        patient.HomeHealthStatus = (int)PatientStatus.Active;
                        var hhWorkFlow = new HHAdmitWorkflow(patient, this.profile);
                        if (hhWorkFlow.IsValid)
                        {
                            var hhWorkItems = hhWorkFlow.Works;
                            if (hhWorkItems.IsNotNullOrEmpty())
                            {
                                serviceCount++;
                                work.AddRange(hhWorkItems);
                            }
                            else
                            {
                                this.isCommitted = false;
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.message = hhWorkFlow.Message;
                            return;
                        }
                    }
                    else if (this.service.Has(AgencyServices.PrivateDuty))
                    {
                        patient.PrivateDutyStatus = (int)PatientStatus.Active;
                        var pdWorkFlow = new PrivateDutyAdmitWorkflow(patient, profile);
                        if (pdWorkFlow.IsValid)
                        {
                            var pdWorkItems = pdWorkFlow.Works;
                            if (pdWorkItems.IsNotNullOrEmpty())
                            {
                                serviceCount++;
                                work.AddRange(pdWorkItems);
                            }
                            else
                            {
                                this.isCommitted = false;
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.message = pdWorkFlow.Message;
                            return;
                        }
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.message = "The current service isn't set for this patient.";
                    return;
                }

                if (serviceCount > 0 && serviceCount == 1)
                {
                }
                else
                {
                    this.isCommitted = false;
                    return;
                }
                // Insert the patient on zero index
                work.Insert(0, () => this.patientService.UpdateModal(patient), () => {
                    patient.HomeHealthStatus = oldHHStatus;
                    patient.PrivateDutyStatus = oldPdStatus;
                    patientService.UpdateModal(patient);
                }, "System could not admit the Patient.");
                work.Perform();
            }
        }
        #endregion
    }

    public class HHAdmitWorkflow
    {

            #region AdmitPatientWorkflow Members

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }

        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;
        private readonly HHEpiosdeService epiosdeService;
        private readonly HHAssessmentService assessmentService;
        private readonly HHTaskService taskService;
        private readonly HHBillingService billingService;
        private readonly FaceToFaceEncounterService faceToFaceEncounterService;
        public HHAdmitWorkflow(Patient patient, Profile profile)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profile = profile;
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<HHPatientProfileService>();
            this.epiosdeService = Container.Resolve<HHEpiosdeService>();
            this.assessmentService = Container.Resolve<HHAssessmentService>();
            this.taskService = Container.Resolve<HHTaskService>();
            this.billingService = Container.Resolve<HHBillingService>();
            this.faceToFaceEncounterService = Container.Resolve<FaceToFaceEncounterService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        private void AddWorkItems()
        {
            this.Validate(this.profile);
            if (this.isValid)
            {
                //profile.Encode();

               
                profile.Modified = DateTime.Now;

                var existingProfile = profileService.GetProfileOnly(patient.Id);
                if (existingProfile != null)
                {
                    EntityHelper.SetProfileFromClientProfile(existingProfile, profile);
                    existingProfile.AdmissionId = Guid.NewGuid();
                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
                    existingProfile.LastName = patient.LastName;
                    existingProfile.FirstName = patient.FirstName;
                    existingProfile.MiddleInitial = patient.MiddleInitial;
                    existingProfile.Status = patient.HomeHealthStatus;
                    this.profile = existingProfile;

                    works.Add(new WorkItem(() => this.profileService.UpdateModal(existingProfile), null, "System could not admit Home Health patient."));
                }
                else
                {
                    profile.AdmissionId = Guid.NewGuid();
                    profile.Id = patient.Id;
                    profile.AgencyId = Current.AgencyId;
                    works.Add(new WorkItem(() =>
                    {
                        profile.PatientIdNumber = patient.PatientIdNumber;
                        profile.LastName = patient.LastName;
                        profile.FirstName = patient.FirstName;
                        profile.MiddleInitial = patient.MiddleInitial;
                        profile.Status = patient.HomeHealthStatus;
                        profile.Created = DateTime.Now;

                        return profileService.Add(profile);
                    },
                    () => profileService.Remove(profile.Id),
                    "System could not admit Home Health patient."));

                }
               

                works.Add(new WorkItem(() => AdmissionHelperFactory<PatientEpisode>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, string.Empty, profile.AdmissionId)), () => AdmissionHelperFactory<PatientEpisode>.RemovePatientAdmissionDate(patient.Id, profile.AdmissionId), "System could not admit Home Health patient."));

                if (profile.EpisodeStartDate.IsValid() && !epiosdeService.IsEpisodeExist(profile.Id, profile.EpisodeStartDate, profile.EpisodeStartDate.AddDays(59)))
                {
                    
                        var episode = EntityHelper.CreateEpisode<PatientEpisode>(profile.Id, profile.EpisodeStartDate, 59);
                        {
                            episode.StartOfCareDate = profile.StartofCareDate;
                            episode.AdmissionId = profile.AdmissionId;
                            episode.PrimaryInsurance = profile.PrimaryInsurance;
                            episode.SecondaryInsurance = profile.SecondaryInsurance;
                            episode.CaseManagerId = profile.CaseManagerId;
                            works.Add(new WorkItem(() => this.epiosdeService.AddEpisode(episode), () => this.epiosdeService.RemoveEpisode(this.patient.Id, episode.Id), "System could not admit Home Health patient."));
                        }


                        if (profile.StartofCareDate.Date == profile.EpisodeStartDate.Date)
                        {
                            var newEvent = EntityHelper.CreateTaskForSOC<ScheduleEvent, PatientEpisode>(patient, profile, episode);
                            var assessment = EntityHelper.CreateAssessment<ScheduleEvent>(newEvent, string.Empty);
                            works.Add(new WorkItem(() =>
                            {
                                assessment.Questions = assessmentService.CreateOASISPatientDemographics(patient, profile, newEvent.DisciplineTask);
                                return assessmentService.AddScheduleTaskAndAssessment(assessment, newEvent);
                            }, () =>
                            {
                                if (taskService.Remove(patient.Id, assessment.Id))
                                {
                                    assessmentService.Remove(assessment.Id);
                                }
                            }, "System could not admit Home Health patient."));
                        }

                        var physician = new AgencyPhysician();
                        var isClaimsAdded = AgencyInformationHelper.IsMedicareOrMedicareHMOInsurance(profile.PrimaryInsurance);
                        if (isClaimsAdded || ( (profile.StartofCareDate.Date == profile.EpisodeStartDate.Date) && profile.IsFaceToFaceEncounterCreated))
                        {
                            physician = patientService.GetPatientPrimaryPhysician(patient.Id);
                        }
                        if (isClaimsAdded)
                        {
                            patient.Profile = profile;
                            works.Add(new WorkItem(() =>
                            {
                                billingService.AddRap(patient, episode, profile.PrimaryInsurance, physician);
                                billingService.AddFinal(patient, episode, profile.PrimaryInsurance, physician);
                                return true;
                            }, () => this.billingService.RemoveMedicareClaims(this.patient.Id, episode.Id), "System could not admit Home Health patient."));
                        }


                        if ((profile.StartofCareDate.Date == profile.EpisodeStartDate.Date) && profile.IsFaceToFaceEncounterCreated)
                        {
                            var faceToFaceEncounter = EntityHelper.CreateFaceToFaceEncounter(profile, episode.Id, physician);
                            works.Add(new WorkItem(() => this.faceToFaceEncounterService.CreateFaceToFaceEncounterAfterPhysicianInfo(faceToFaceEncounter),
                            () =>
                            {
                                if (taskService.Remove(patient.Id, faceToFaceEncounter.Id))
                                {
                                    faceToFaceEncounterService.Remove(patient.Id, faceToFaceEncounter.Id);
                                }
                            }, "System could not admit Home Health patient."));
                        }
                    
                }
            }
        }

        public void Validate(Profile homeHealthProfile)
        {
            var rules = this.ValidationRules(homeHealthProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile homeHealthProfile)
        {
            var rules = new List<Validation>();
            if (homeHealthProfile != null)
            {
                //if (patient.MedicareNumber.IsNotNullOrEmpty())
                //{
                //    bool medicareNumberCheck = patientService.IsMedicareExistForEdit (patient.Id, patient.MedicareNumber.Trim());
                //    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                //}
                //if (patient.MedicaidNumber.IsNotNullOrEmpty())
                //{
                //    bool medicaidNumberCheck = patientService.IsMedicaidExistForEdit(patient.Id,patient.MedicaidNumber.Trim());
                //    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                //}
                rules.Add(new Validation(() => !homeHealthProfile.StartofCareDate.IsValid(), "Valid start of care is required."));

                if (homeHealthProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
                
            }
            return rules;
        }
    }

    public class PrivateDutyAdmitWorkflow
    {

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        private List<WorkItem> works { get;  set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }
        private readonly IPatientService patientService;
        private readonly PrivateDutyPatientProfileService profileService;
        private readonly PrivateDutyEpisodeService episodeService;
        public PrivateDutyAdmitWorkflow(Patient patient, Profile profile)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profile = profile;
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
            this.episodeService = Container.Resolve<PrivateDutyEpisodeService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        private void AddWorkItems()
        {
            this.Validate(this.profile);
            if (this.isValid)
            {
                //profile.Encode();
                //profile.PatientIdNumber = patient.PatientIdNumber;
                //profile.LastName = patient.LastName;
                //profile.FirstName = patient.FirstName;
                //profile.MiddleInitial = patient.MiddleInitial;
                //profile.Status = patient.HomeHealthStatus;
                //profile.Modified = DateTime.Now;
              
                profile.Modified = DateTime.Now;

                var existingProfile = profileService.GetProfileOnly(patient.Id);
                if (existingProfile != null)
                {
                    EntityHelper.SetProfileFromClientProfile(existingProfile, profile);
                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
                    existingProfile.LastName = patient.LastName;
                    existingProfile.FirstName = patient.FirstName;
                    existingProfile.MiddleInitial = patient.MiddleInitial;
                    var oldStatus = existingProfile.Status;
                    existingProfile.Status = patient.PrivateDutyStatus;
                    this.profile = existingProfile;
                    works.Add(new WorkItem(() => this.profileService.UpdateModal(existingProfile),
                        () =>
                        {
                            existingProfile.Status = oldStatus;
                            this.profileService.UpdateModal(existingProfile);
                        },
                     "System could not admit Private Duty patient."));
                }
                else
                {
                    profile.AdmissionId = Guid.NewGuid();
                    profile.Id = patient.Id;
                    works.Add(new WorkItem(() =>
                    {
                        profile.PatientIdNumber = patient.PatientIdNumber;
                        profile.LastName = patient.LastName;
                        profile.FirstName = patient.FirstName;
                        profile.MiddleInitial = patient.MiddleInitial;
                        profile.Status = patient.PrivateDutyStatus;
                        profile.Created = DateTime.Now;

                        return profileService.Add(profile);
                    },
                    () => profileService.Remove(profile.Id),
                    "System could not admit Private Duty patient."));
                }
                works.Add(new WorkItem(() => AdmissionHelperFactory<PrivateDutyCarePeriod>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient, profile, 0, string.Empty, profile.AdmissionId)), () => AdmissionHelperFactory<PrivateDutyCarePeriod>.RemovePatientAdmissionDate(patient.Id, profile.AdmissionId), "System could not add the new patient."));
                if (profile.EpisodeStartDate.IsValid() && profile.EpisodeEndDate > profile.EpisodeStartDate && !this.episodeService.IsEpisodeExist(profile.Id, profile.EpisodeStartDate, profile.EpisodeEndDate))
                {
                    var days = (profile.EpisodeEndDate - profile.EpisodeStartDate).Days;
                    var episode = EntityHelper.CreateEpisode<PrivateDutyCarePeriod>(profile.Id, profile.EpisodeStartDate, days);
                    {
                        episode.StartOfCareDate = profile.StartofCareDate;
                        episode.AdmissionId = profile.AdmissionId;
                        episode.PrimaryInsurance = profile.PrimaryInsurance;
                        episode.SecondaryInsurance = profile.SecondaryInsurance;
                        episode.CaseManagerId = profile.CaseManagerId;
                        works.Add(new WorkItem(() => this.episodeService.AddEpisode(episode), () => this.episodeService.RemoveEpisode(this.patient.Id, episode.Id), "System could not add the new patient."));
                    }
                }
            }
        }

        public void Validate(Profile privateDutyProfile)
        {
            var rules = this.ValidationRules(privateDutyProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile privateDutyProfile)
        {
            var rules = new List<Validation>();

            if (privateDutyProfile != null)
            {
                if (privateDutyProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
            }
            return rules;
        }
    }
}
