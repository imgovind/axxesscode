﻿namespace Axxess.AgencyManagement.Application.Workflows
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Services;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Application.Helpers;

    public class AdmitReferralWorkflow
    {
        #region CreateEpisodeWorkflow

        private Profile profile { get; set; }
        private AgencyServices service { get; set; }

        private readonly IReferralService referralService;
        private readonly IPatientService patientService;

        public AdmitReferralWorkflow(Profile profile, AgencyServices service)
        {
            Check.Argument.IsNotNull(profile, "profile");

            this.profile = profile;
            this.service = service;
            this.referralService = Container.Resolve<IReferralService>();
            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = this.message.IsNullOrEmpty();
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
            };
            var referral = referralService.GetReferral(profile.Id);
            if (referral != null)
            {
                var patient = patientService.GetPatientOnly(profile.Id);
                var IsPatientExist = false;
                if (patient != null)
                {
                    IsPatientExist = true;
                }
                else
                {
                    patient = new Patient
                    {
                        AgencyId=Current.AgencyId,
                        Id = referral.Id,
                        PatientIdNumber = profile.PatientIdNumber
                    };
                    SetData(patient, referral);
                }

                var oldReferralHHStatus = referral.HomeHealthStatus;
                var oldReferralPdStatus = referral.PrivateDutyStatus;

                var oldPatientHHStatus = patient.HomeHealthStatus;
                var oldPatientPdStatus = patient.PrivateDutyStatus;

                var serviceCount = 0;
                if (referral.Services.Has(this.service))
                {
                    if (this.service.Has(AgencyServices.HomeHealth))
                    {
                        var hhWorkFlow = new HHAdmitReferralWorkflow(patient, this.profile);
                        if (hhWorkFlow.IsValid)
                        {
                            var hhWorkItems = hhWorkFlow.Works;
                            if (hhWorkItems.IsNotNullOrEmpty())
                            {
                                referral.HomeHealthStatus = (int)ReferralStatus.Admitted;
                                patient.HomeHealthStatus = (int)PatientStatus.Active;
                                serviceCount++;
                                work.AddRange(hhWorkItems);
                                if (!IsPatientExist)
                                {
                                    if (referral.Services.Has(AgencyServices.PrivateDuty))
                                    {
                                        patient.PrivateDutyStatus = (int)PatientStatus.Referral;
                                    }
                                }
                                else
                                {
                                    if (!hhWorkFlow.IsProfileExist && patient.Services.Has(AgencyServices.PrivateDuty) && !Enum.IsDefined(typeof(PatientStatus), patient.PrivateDutyStatus))
                                    {
                                        patient.PrivateDutyStatus = (int)PatientStatus.Referral;
                                    }
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.message = hhWorkFlow.Message;
                            return;
                        }
                    }

                    if (this.service.Has(AgencyServices.PrivateDuty))
                    {
                        var pdWorkFlow = new PrivateDutyAdmitReferralWorkflow(patient, this.profile);
                        if (pdWorkFlow.IsValid)
                        {
                            var pdWorkItems = pdWorkFlow.Works;
                            if (pdWorkItems.IsNotNullOrEmpty())
                            {
                                referral.PrivateDutyStatus = (int)ReferralStatus.Admitted;
                                patient.PrivateDutyStatus = (int)PatientStatus.Active;
                                serviceCount++;
                                work.AddRange(pdWorkItems);
                                if (!IsPatientExist)
                                {
                                    if (referral.Services.Has(AgencyServices.HomeHealth))
                                    {
                                        patient.HomeHealthStatus = (int)PatientStatus.Referral;
                                    }
                                }
                                else
                                {
                                    if (!pdWorkFlow.IsProfileExist && patient.Services.Has(AgencyServices.HomeHealth) && !Enum.IsDefined(typeof(PatientStatus), patient.HomeHealthStatus))
                                    {
                                        patient.HomeHealthStatus = (int)PatientStatus.Referral;
                                    }
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.message = pdWorkFlow.Message;
                            return;
                        }
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.message = "The current service isn't set for this patient.";
                    return;
                }

                if (serviceCount > 0 && serviceCount == 1)
                {
                }
                else
                {
                    this.isCommitted = false;
                    return;
                }

                work.Add(() => { return referralService.UpdateReferralModal(referral);},
                 () => {
                     referral.HomeHealthStatus = oldReferralHHStatus;
                     referral.PrivateDutyStatus = oldReferralPdStatus;
                     referralService.UpdateReferralModal(referral);
                 },
                 "System could not admit the referral patient.");

                if (IsPatientExist)
                {
                    work.Insert(0, () => { return patientService.UpdateModal(patient); }, () =>
                    {
                        patient.HomeHealthStatus = oldPatientHHStatus;
                        patient.PrivateDutyStatus = oldPatientPdStatus;
                        patientService.UpdateModal(patient);
                    }, "System could not admit the referral patient.");
                }
                else
                {
                    patient.Created = DateTime.Now;

                    var medication = patientService.GetMedicationProfileByPatient(referral.Id);
                    if (medication != null)
                    {
                    }
                    else
                    {
                        medication = EntityHelper.CreateMedicationProfile(patient);
                        work.Add(() => { return patientService.CreateMedicationProfile(medication); }, () => { patientService.RemoveMedicationProfile(referral.Id, medication.Id); }, "System could not admit the referral patient.");
                    }

                    
                    work.Insert(0, () => {
                        if (patientService.AddPatient(patient))
                        {
                            patientService.LinkReferralPhysician(referral);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }, () => { patientService.RemovePatient(patient.Id); }, "System could not admit the referral patient.");

                }

            }

            work.Perform();
        }

        public void SetData(Patient patient, Referral referral)
        {

            patient.Id = referral.Id;
            patient.ReferrerPhysician = referral.ReferrerPhysician;
            //patient.PatientIdNumber = pending.PatientIdNumber;
            patient.FirstName = referral.FirstName;
            patient.MiddleInitial = referral.MiddleInitial;
            patient.LastName = referral.LastName;
            patient.Gender = referral.Gender;
            patient.DOB = referral.DOB;
            patient.MaritalStatus = referral.MaritalStatus;
            patient.SSN = referral.SSN;
            patient.IsDNR = referral.IsDNR;
            patient.Height = referral.Height;
            patient.HeightMetric = referral.HeightMetric;
            patient.Weight = referral.Weight;
            patient.WeightMetric = referral.WeightMetric;
           
            patient.MedicareNumber = referral.MedicareNumber;
            patient.MedicaidNumber = referral.MedicaidNumber;
           
            patient.AddressLine1 = referral.AddressLine1;
            patient.AddressLine2 = referral.AddressLine2;
            patient.AddressCity = referral.AddressCity;
            patient.AddressStateCode = referral.AddressStateCode;
            patient.AddressZipCode = referral.AddressZipCode;
            patient.PhoneHome = referral.PhoneHome;
            patient.EmailAddress = referral.EmailAddress;

            patient.ServicesRequired = referral.ServicesRequired;
            patient.Ethnicities = string.Empty;
            patient.DME = referral.DME;
            patient.OtherDME = referral.OtherDME;
            patient.Comments = referral.Comments;
            patient.Modified = DateTime.Now;
        }

        #endregion
    }

    public class HHAdmitReferralWorkflow
    {

         #region CreateEpisodeWorkflow

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isProfileExist { get; set; }
        public bool IsProfileExist { get { return this.isProfileExist; } }

        private readonly IReferralService referralService;
        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;
        private readonly HHMediatorService mediatorService;
        private readonly HHEpiosdeService epiosdeService;
        private readonly HHAssessmentService assessmentService;
        private readonly HHTaskService taskService;
        private readonly HHBillingService billingService;
        private readonly FaceToFaceEncounterService faceToFaceEncounterService;

        public HHAdmitReferralWorkflow(Patient patient, Profile profile)
        {
            Check.Argument.IsNotNull(profile, "profile");

            this.patient = patient;
            this.profile = profile;
            this.referralService = Container.Resolve<IReferralService>();
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<HHPatientProfileService>();
            this.mediatorService = Container.Resolve<HHMediatorService>();
            this.epiosdeService = Container.Resolve<HHEpiosdeService>();
            this.assessmentService = Container.Resolve<HHAssessmentService>();
            this.taskService = Container.Resolve<HHTaskService>();
            this.billingService = Container.Resolve<HHBillingService>();
            this.faceToFaceEncounterService = Container.Resolve<FaceToFaceEncounterService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        #region IWorkflow Members

        private void AddWorkItems()
        {
             this.Validate(this.profile);
             if (this.isValid)
             {
                 var existingProfile = profileService.GetProfileOnly(patient.Id);
                 if (existingProfile != null)
                 {
                     this.isProfileExist = true;
                     EntityHelper.SetProfileFromClientProfile(existingProfile, profile);

                     existingProfile.PatientIdNumber = patient.PatientIdNumber;
                     existingProfile.LastName = patient.LastName;
                     existingProfile.FirstName = patient.FirstName;
                     existingProfile.MiddleInitial = patient.MiddleInitial;
                     existingProfile.Status = patient.HomeHealthStatus;

                     this.profile = existingProfile;

                     works.Add(new WorkItem(() => { return profileService.UpdateModal(existingProfile); }, () => { }, "System could not admit the referral patient."));
                 }
                 else
                 {
                     profile.AdmissionId = Guid.NewGuid();
                     profile.Id = patient.Id;

                     works.Add(new WorkItem(() =>
                     {
                         profile.PatientIdNumber = patient.PatientIdNumber;
                         profile.LastName = patient.LastName;
                         profile.FirstName = patient.FirstName;
                         profile.MiddleInitial = patient.MiddleInitial;
                         profile.Status = patient.HomeHealthStatus;
                         profile.Created = DateTime.Now;

                         return profileService.Add(profile);

                     }, () => { profileService.Remove(profile.Id); }, "System could not admit the referral patient."));
                 }
               

                 works.Add(new WorkItem(
                     () =>
                     {
                         return AdmissionHelperFactory<PatientEpisode>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient,profile, 0, string.Empty, profile.AdmissionId));
                     },
                     () =>
                     {
                         AdmissionHelperFactory<PatientEpisode>.RemovePatientAdmissionDate(patient.Id, profile.AdmissionId);
                     },
                     "System could not admit the referral patient."));
                 if (profile.EpisodeStartDate.IsValid())
                 {

                     var episode = EntityHelper.CreateEpisode<PatientEpisode>(profile.Id, profile.EpisodeStartDate, 59);
                     {
                         episode.StartOfCareDate = profile.StartofCareDate;
                         episode.AdmissionId = profile.AdmissionId;
                         episode.PrimaryInsurance = profile.PrimaryInsurance;
                         episode.SecondaryInsurance = profile.SecondaryInsurance;
                         episode.CaseManagerId = profile.CaseManagerId;

                         works.Add(new WorkItem(() => this.epiosdeService.AddEpisode(episode), () => this.epiosdeService.RemoveEpisode(this.patient.Id, episode.Id), "System could not admit the referral patient."));
                     }


                     if (profile.StartofCareDate.Date == profile.EpisodeStartDate.Date)
                     {
                         var newEvent = EntityHelper.CreateTaskForSOC<ScheduleEvent, PatientEpisode>(patient, profile, episode);
                         var assessment = EntityHelper.CreateAssessment<ScheduleEvent>(newEvent, string.Empty);
                         works.Add(new WorkItem(() =>
                         {
                             assessment.Questions = assessmentService.CreateOASISPatientDemographics(patient, profile, newEvent.DisciplineTask);
                             return assessmentService.AddScheduleTaskAndAssessment(assessment, newEvent);
                         }, () =>
                         {
                             if (taskService.Remove(patient.Id, assessment.Id))
                             {
                                 assessmentService.Remove(assessment.Id);
                             }
                         }, "System could not admit Home Health patient."));

                     }

                     var physician = new AgencyPhysician();
                     var isClaimsAdded = AgencyInformationHelper.IsMedicareOrMedicareHMOInsurance(profile.PrimaryInsurance);
                     if (isClaimsAdded || profile.IsFaceToFaceEncounterCreated)
                     {
                         physician = patientService.GetPatientPrimaryPhysician(patient.Id);
                     }
                     if (isClaimsAdded)
                     {
                         works.Add(new WorkItem(() =>
                         {
                             patient.Profile = profile;
                             billingService.AddRap(patient, episode, profile.PrimaryInsurance, physician);
                             billingService.AddFinal(patient, episode, profile.PrimaryInsurance, physician);
                             return true;
                         }, () =>
                         {
                             billingService.RemoveMedicareClaims(patient.Id, episode.Id);
                         }, "System could not admit Home Health patient."));
                     }


                     if (profile.IsFaceToFaceEncounterCreated)
                     {
                         var faceToFaceEncounter = EntityHelper.CreateFaceToFaceEncounter(profile, episode.Id, physician);
                         works.Add(new WorkItem(() => { return faceToFaceEncounterService.CreateFaceToFaceEncounterAfterPhysicianInfo(faceToFaceEncounter); },
                         () =>
                         {
                             if (taskService.Remove(patient.Id, faceToFaceEncounter.Id))
                             {
                                 faceToFaceEncounterService.Remove(patient.Id, faceToFaceEncounter.Id);
                             }
                         }, "System could not admit Home Health patient."));
                     }
                 }
             }

        }

        public void Validate(Profile homeHealthProfile)
        {
            var rules = this.ValidationRules(homeHealthProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile homeHealthProfile)
        {
            var rules = new List<Validation>();
            if (homeHealthProfile != null)
            {
                //if (patient.MedicareNumber.IsNotNullOrEmpty())
                //{
                //    bool medicareNumberCheck = patientService.IsMedicareExistForEdit (patient.Id, patient.MedicareNumber.Trim());
                //    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                //}
                //if (patient.MedicaidNumber.IsNotNullOrEmpty())
                //{
                //    bool medicaidNumberCheck = patientService.IsMedicaidExistForEdit(patient.Id,patient.MedicaidNumber.Trim());
                //    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                //}
                rules.Add(new Validation(() => !homeHealthProfile.StartofCareDate.IsValid(), "Valid start of care is required."));

                if (homeHealthProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }

            }
            return rules;
        }

        #endregion

        public void SetData(Profile profile, Referral referral, PendingPatient pending)
        {
            profile.AdmissionId = pending.AdmissionId;
            profile.AgencyLocationId = pending.AgencyLocationId;
            profile.UserId = pending.UserId;
            profile.CaseManagerId = pending.CaseManagerId;
            profile.PrimaryInsurance = pending.PrimaryInsurance;
            profile.SecondaryInsurance = pending.SecondaryInsurance;
            profile.TertiaryInsurance = pending.TertiaryInsurance;
            profile.Payer = pending.Payer;
            profile.Status = (int)PatientStatus.Active;
            profile.StartofCareDate = pending.StartofCareDate;
            profile.AdmissionSource = referral.AdmissionSource.ToString();
            profile.OtherReferralSource = referral.OtherReferralSource;
            profile.InternalReferral = referral.InternalReferral;
            profile.ReferralDate = pending.ReferralDate;
        }

    }

    public class PrivateDutyAdmitReferralWorkflow
    {

        #region CreateEpisodeWorkflow

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isProfileExist { get; set; }
        public bool IsProfileExist { get { return this.isProfileExist; } }

        private readonly IPatientService patientService;
        private readonly PrivateDutyPatientProfileService profileService;
        private readonly PrivateDutyEpisodeService epiosdeService;
        public PrivateDutyAdmitReferralWorkflow(Patient patient, Profile profile)
        {
            Check.Argument.IsNotNull(profile, "profile");

            this.patient = patient;
            this.profile = profile;
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
            this.epiosdeService = Container.Resolve<PrivateDutyEpisodeService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        #region IWorkflow Members

        private void AddWorkItems()
        {
            this.Validate(this.profile);
            if (this.isValid)
            {
                var existingProfile = profileService.GetProfileOnly(patient.Id);
                if (existingProfile != null)
                {
                    this.isProfileExist = true;
                    EntityHelper.SetProfileFromClientProfile(existingProfile, profile);
                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
                    existingProfile.LastName = patient.LastName;
                    existingProfile.FirstName = patient.FirstName;
                    existingProfile.MiddleInitial = patient.MiddleInitial;
                    existingProfile.Status = patient.HomeHealthStatus;

                    this.profile = existingProfile;

                    works.Add(new WorkItem(() => { return profileService.UpdateModal(existingProfile); }, () => { }, "System could not admit the referral patient."));
                }
                else
                {
                    profile.AdmissionId =  Guid.NewGuid();
                    profile.Id = patient.Id;
                    works.Add(new WorkItem(() =>
                    {
                        profile.PatientIdNumber = patient.PatientIdNumber;
                        profile.LastName = patient.LastName;
                        profile.FirstName = patient.FirstName;
                        profile.MiddleInitial = patient.MiddleInitial;
                        profile.Status = patient.HomeHealthStatus;
                        profile.Created = DateTime.Now;
                        return profileService.Add(profile);
                    }, () => this.profileService.Remove(this.profile.Id), "System could not admit the referral patient."));
                }


                works.Add(new WorkItem(
                    () => AdmissionHelperFactory<PrivateDutyCarePeriod>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(this.patient, this.profile, 0, string.Empty, this.profile.AdmissionId)),
                    () => AdmissionHelperFactory<PrivateDutyCarePeriod>.RemovePatientAdmissionDate(this.patient.Id, this.profile.AdmissionId),
                    "System could not admit the referral patient."));
                if (profile.EpisodeStartDate.IsValid())
                {
                    var days = (profile.EpisodeEndDate - profile.EpisodeStartDate).Days;

                    var episode = EntityHelper.CreateEpisode<PrivateDutyCarePeriod>(profile.Id, profile.EpisodeStartDate, days);
                    {
                        episode.StartOfCareDate = profile.StartofCareDate;
                        episode.AdmissionId = profile.AdmissionId;
                        episode.PrimaryInsurance = profile.PrimaryInsurance;
                        episode.SecondaryInsurance = profile.SecondaryInsurance;
                        episode.CaseManagerId = profile.CaseManagerId;
                        works.Add(new WorkItem(() => this.epiosdeService.AddEpisode(episode), () => this.epiosdeService.RemoveEpisode(this.patient.Id, episode.Id), "System could not admit the referral patient."));
                    }
                }
            }

        }

        public void Validate(Profile privateDutyProfile)
        {
            var rules = this.ValidationRules(privateDutyProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile privateDutyProfile)
        {
            var rules = new List<Validation>();

            if (privateDutyProfile != null)
            {
                if (privateDutyProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
            }
            return rules;
        }

        #endregion

        public void SetData(Profile profile, Referral referral, PendingPatient pending)
        {
            profile.AdmissionId = pending.AdmissionId;
            profile.AgencyLocationId = pending.AgencyLocationId;
            profile.UserId = pending.UserId;
            profile.CaseManagerId = pending.CaseManagerId;
            profile.PrimaryInsurance = pending.PrimaryInsurance;
            profile.SecondaryInsurance = pending.SecondaryInsurance;
            profile.TertiaryInsurance = pending.TertiaryInsurance;
            profile.Payer = pending.Payer;
            profile.Status = (int)PatientStatus.Active;
            profile.StartofCareDate = pending.StartofCareDate;
            profile.AdmissionSource = referral.AdmissionSource.ToString();
            profile.OtherReferralSource = referral.OtherReferralSource;
            profile.InternalReferral = referral.InternalReferral;
            profile.ReferralDate = pending.ReferralDate;
        }

    }
}
