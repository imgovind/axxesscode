﻿namespace Axxess.AgencyManagement.Application.Workflows
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Services;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Application.Helpers;

    public class AdmitNewReferralWorkflow
    {
        #region CreateEpisodeWorkflow

        private Patient patient { get; set; }
        private List<Profile> profiles { get; set; }
        private List<int> servicesToAdmit { get; set; }

        private readonly IReferralService referralService;
        private readonly IPatientService patientService;

        public AdmitNewReferralWorkflow(Patient patient, List<Profile> profiles, List<int> servicesToAdmit)
        {
            Check.Argument.IsNotNull(profiles, "profile");
            this.patient = patient;
            this.profiles = profiles;
            this.servicesToAdmit = servicesToAdmit;
            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = "Referral could not be admitted." };
            this.referralService = Container.Resolve<IReferralService>();
            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private JsonViewData viewData { get; set; }
        public JsonViewData ViewData { get { return viewData; } }

        //private string message { get; set; }
        //public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }


        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
                this.viewData.isSuccessful = this.isCommitted;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = item.Description;
                this.IsErrorExist = true;
            };
            var referral = referralService.GetReferral(patient.Id);
            if (referral != null)
            {
                if (!patientService.IsPatientIdExist(patient.PatientIdNumber))
                {
                    patient.AgencyId = Current.AgencyId;
                    patient.Created = DateTime.Now;
                    patient.Modified = DateTime.Now;
                    patient.Encode();

                    var oldReferralHHStatus = referral.HomeHealthStatus;
                    var oldReferralPdStatus = referral.PrivateDutyStatus;

                    var serviceCount = 0;
                    if (this.servicesToAdmit.Contains((int)AgencyServices.HomeHealth))
                    {
                        if (referral.Services.Has(AgencyServices.HomeHealth))
                        {
                            var homeHealthProfile = this.profiles.Where(p => p.ServiceType == (int)AgencyServices.HomeHealth).FirstOrDefault();
                            if (homeHealthProfile != null)
                            {
                                patient.HomeHealthStatus = (int)PatientStatus.Active;
                                var hhWorkFlow = new HHAdmitNewReferralWorkflow(patient, homeHealthProfile);
                                if (hhWorkFlow.IsValid)
                                {
                                    var hhWorkItems = hhWorkFlow.Works;
                                    if (hhWorkItems.IsNotNullOrEmpty())
                                    {
                                        patient.Services |= AgencyServices.HomeHealth;

                                        referral.HomeHealthStatus = (int)ReferralStatus.Admitted;

                                        serviceCount++;
                                        work.AddRange(hhWorkItems);
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        this.viewData.isSuccessful = false;
                                        return;
                                    }
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.viewData.isSuccessful = false;
                                    this.viewData.errorMessage = hhWorkFlow.Message;
                                    return;
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                this.viewData.isSuccessful = false;
                                this.viewData.errorMessage = "Unable to find the home health service profile from referral information.";
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.viewData.isSuccessful = false;
                            this.viewData.errorMessage = "Unable to find the home health service profile from referral information.";
                            return;
                        }
                    }

                    if (this.servicesToAdmit.Contains((int)AgencyServices.PrivateDuty))
                    {
                        if (referral.Services.Has(AgencyServices.PrivateDuty))
                        {
                            var privateDutyProfile = this.profiles.Where(p => p.ServiceType == (int)AgencyServices.PrivateDuty).FirstOrDefault();
                            if (privateDutyProfile != null)
                            {
                                patient.PrivateDutyStatus = (int)PatientStatus.Active;
                                var pdWorkFlow = new PrivateDutyAdmitNewReferralWorkflow(patient, privateDutyProfile);
                                if (pdWorkFlow.IsValid)
                                {
                                    var pdWorkItems = pdWorkFlow.Works;
                                    if (pdWorkItems.IsNotNullOrEmpty())
                                    {
                                        patient.Services |= AgencyServices.PrivateDuty;
                                        referral.PrivateDutyStatus = (int)ReferralStatus.Admitted;


                                        serviceCount++;
                                        work.AddRange(pdWorkItems);
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        return;
                                    }
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.viewData.isSuccessful = false;
                                    this.viewData.errorMessage = pdWorkFlow.Message;
                                    return;
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                this.viewData.isSuccessful = false;
                                this.viewData.errorMessage = "Unable to find the home health service profile from referral information.";
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.viewData.isSuccessful = false;
                            this.viewData.errorMessage = "Unable to find the home health service profile from referral information.";
                            return;
                        }
                    }


                    if (serviceCount > 0 && serviceCount == servicesToAdmit.Count)
                    {
                       
                    }
                    else
                    {
                        this.isCommitted = false;
                        this.viewData.isSuccessful = false;
                        this.viewData.errorMessage = "Unable to admit this service profile.";
                        return;
                    }

                    EntityHelper.SetReferralDataFromPatient(referral, patient); 

                    work.Add(() => { return referralService.UpdateReferralModal(referral); },
                     () =>
                     {
                         referral.HomeHealthStatus = oldReferralHHStatus;
                         referral.PrivateDutyStatus = oldReferralPdStatus;

                         referralService.UpdateReferralModal(referral);
                     },
                     "System could not admit the referral patient.");
                    patient.EmergencyContact.Encode();
                    if (patient.EmergencyContact.FirstName.IsNotNullOrEmpty() && patient.EmergencyContact.LastName.IsNotNullOrEmpty() && patient.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty())
                    {
                        work.Add(
                            () =>
                            {
                                patient.EmergencyContact.PatientId = patient.Id;
                                patient.EmergencyContact.AgencyId = Current.AgencyId;
                                if (patient.EmergencyContact.Id.IsEmpty())
                                {
                                    patient.EmergencyContact.IsPrimary = true;
                                    return patientService.AddEmergencyContact(patient.EmergencyContact);
                                }
                                else
                                {
                                    patient.EmergencyContact.IsPrimary = true;
                                    if (!patientService.AddEmergencyContact(patient.EmergencyContact))
                                    {
                                        return patientService.UpdateEmergencyContact(this.patient.Id, patient.EmergencyContact);
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }

                            },
                            () => { patientService.RemoveEmergencyContacts(patient.Id); }, "System could not save the Emergency Contact information.");
                    }

                    var medication = patientService.GetMedicationProfileByPatient(referral.Id);
                    if (medication != null)
                    {
                    }
                    else
                    {
                        medication = EntityHelper.CreateMedicationProfile(patient);
                        work.Add(() => { return patientService.CreateMedicationProfile(medication); }, () => { patientService.RemoveMedicationProfile(referral.Id, medication.Id); }, "System could not admit the referral patient.");
                    }


                    work.Insert(0, () =>
                    {
                        if (patientService.AddPatient(patient))
                        {
                            patientService.LinkReferralPhysician(referral);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }, () => { patientService.RemovePatient(patient.Id); }, "System could not admit the referral patient.");

                    work.Perform();
                    if (this.isCommitted)
                    {
                        viewData.IsReferralListRefresh = true;
                        viewData.PatientStatus = (int)PatientStatus.Active;
                        viewData.IsNonAdmitPatientListRefresh = true;
                        viewData.IsCenterRefresh = true;
                        viewData.isSuccessful = true;
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.viewData.isSuccessful = false;
                    this.viewData.errorMessage = "Patient Id Number already exists.";
                }

            }
            else
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = "Referral do not exists. Try again.";
            }

           
        }


   

        #endregion
    }

    public class AdmitNewServiceReferralWorkflow
    {
        #region CreateEpisodeWorkflow

        private Guid patientId { get; set; }
        private List<Profile> profiles { get; set; }
        private List<int> servicesToAdmit { get; set; }

        private readonly IReferralService referralService;
        private readonly IPatientService patientService;

        public AdmitNewServiceReferralWorkflow(Guid patientId, List<Profile> profiles, List<int> servicesToAdmit)
        {
            Check.Argument.IsNotNull(profiles, "profile");
            this.patientId = patientId;
            this.profiles = profiles;
            this.servicesToAdmit = servicesToAdmit;
            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = "Referral could not be admitted." };
            this.referralService = Container.Resolve<IReferralService>();
            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private JsonViewData viewData { get; set; }
        public JsonViewData ViewData { get { return viewData; } }


        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }


        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
                this.viewData.isSuccessful = this.isCommitted;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = item.Description;
                this.IsErrorExist = true;
            };
            var referral = referralService.GetReferral(patientId);
            if (referral != null)
            {
                var patient = patientService.GetPatientOnly(patientId);
                if (patient != null)
                {
                    var oldReferralHHStatus = referral.HomeHealthStatus;
                    var oldReferralPdStatus = referral.PrivateDutyStatus;

                    var oldPatientHHStatus = patient.HomeHealthStatus;
                    var oldPatientPdStatus = patient.PrivateDutyStatus;

                    var serviceCount = 0;

                    if (this.servicesToAdmit.Contains((int)AgencyServices.HomeHealth))
                    {
                        if (!patient.Services.Has(AgencyServices.HomeHealth))
                        {
                            if (referral.Services.Has(AgencyServices.HomeHealth))
                            {
                                var homeHealthProfile = this.profiles.Where(p => p.ServiceType == (int)AgencyServices.HomeHealth).FirstOrDefault();
                                if (homeHealthProfile != null)
                                {
                                    patient.HomeHealthStatus = (int)PatientStatus.Active;
                                    var hhWorkFlow = new HHAdmitNewReferralWorkflow(patient, homeHealthProfile);
                                    if (hhWorkFlow.IsValid)
                                    {
                                        var hhWorkItems = hhWorkFlow.Works;
                                        if (hhWorkItems.IsNotNullOrEmpty())
                                        {
                                            patient.Services |= AgencyServices.HomeHealth;

                                            referral.HomeHealthStatus = (int)ReferralStatus.Admitted;

                                            serviceCount++;
                                            work.AddRange(hhWorkItems);
                                        }
                                        else
                                        {
                                            this.isCommitted = false;
                                            this.viewData.isSuccessful = false;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        this.viewData.isSuccessful = false;
                                        this.viewData.errorMessage = hhWorkFlow.Message;
                                        return;
                                    }
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.viewData.isSuccessful = false;
                                    this.viewData.errorMessage = "Unable to find the home health service profile from referral information.";
                                    return;
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                this.viewData.isSuccessful = false;
                                this.viewData.errorMessage = "Unable to find the home health service profile from referral information.";
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.viewData.isSuccessful = false;
                            this.viewData.errorMessage = "Home health service profile already exist.";
                            return;
                        }
                    }

                    if (this.servicesToAdmit.Contains((int)AgencyServices.PrivateDuty))
                    {
                        if (!patient.Services.Has(AgencyServices.PrivateDuty))
                        {
                            if (referral.Services.Has(AgencyServices.PrivateDuty))
                            {
                                var privateDutyProfile = this.profiles.Where(p => p.ServiceType == (int)AgencyServices.PrivateDuty).FirstOrDefault();
                                if (privateDutyProfile != null)
                                {
                                    patient.PrivateDutyStatus = (int)PatientStatus.Active;
                                    var pdWorkFlow = new PrivateDutyAdmitNewReferralWorkflow(patient, privateDutyProfile);
                                    if (pdWorkFlow.IsValid)
                                    {
                                        var pdWorkItems = pdWorkFlow.Works;
                                        if (pdWorkItems.IsNotNullOrEmpty())
                                        {
                                            patient.Services |= AgencyServices.PrivateDuty;
                                            referral.PrivateDutyStatus = (int)ReferralStatus.Admitted;


                                            serviceCount++;
                                            work.AddRange(pdWorkItems);
                                        }
                                        else
                                        {
                                            this.isCommitted = false;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        this.viewData.isSuccessful = false;
                                        this.viewData.errorMessage = pdWorkFlow.Message;
                                        return;
                                    }
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.viewData.isSuccessful = false;
                                    this.viewData.errorMessage = "Unable to find the private duty service profile from referral information.";
                                    return;
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                this.viewData.isSuccessful = false;
                                this.viewData.errorMessage = "Unable to find the private duty service profile from referral information.";
                                return;
                            }
                        }
                        else
                        {
                            this.isCommitted = false;
                            this.viewData.isSuccessful = false;
                            this.viewData.errorMessage = "Private duty service profile already exist.";
                            return;
                        }
                    }
                    


                    if (serviceCount > 0 && serviceCount == servicesToAdmit.Count)
                    {

                    }
                    else
                    {
                        this.isCommitted = false;
                        this.viewData.isSuccessful = false;
                        this.viewData.errorMessage = "Unable to admit this service profile.";
                        return;
                    }

                    EntityHelper.SetReferralDataFromPatient(referral, patient);

                    work.Add(() => { return referralService.UpdateReferralModal(referral); },
                     () =>
                     {
                         referral.HomeHealthStatus = oldReferralHHStatus;
                         referral.PrivateDutyStatus = oldReferralPdStatus;

                         referralService.UpdateReferralModal(referral);
                     },
                     "System could not admit the referral patient.");


                    work.Insert(0, () =>
                    {
                        return patientService.UpdateModal(patient);

                    }, () => {
                        patient.HomeHealthStatus = oldPatientHHStatus;
                        patient.PrivateDutyStatus = oldPatientPdStatus;

                        patientService.UpdateModal(patient); 
                    }, "System could not admit the referral patient.");

                    work.Perform();
                    if (this.isCommitted)
                    {
                        viewData.IsReferralListRefresh = true;
                        viewData.PatientStatus = (int)PatientStatus.Active;
                        viewData.IsNonAdmitPatientListRefresh = true;
                        viewData.IsCenterRefresh = true;
                        viewData.isSuccessful = true;
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.viewData.isSuccessful = false;
                    this.viewData.errorMessage = "Unable to find the patient information. Try again.";
                }

            }
            else
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = "Unable to find the Referral information. Try again.";
            }


        }




        #endregion
    }

    public class HHAdmitNewReferralWorkflow
    {

         #region CreateEpisodeWorkflow

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isProfileExist { get; set; }
        public bool IsProfileExist { get { return this.isProfileExist; } }

        private readonly IReferralService referralService;
        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;
        private readonly HHMediatorService mediatorService;
        private readonly HHEpiosdeService epiosdeService;
        private readonly HHAssessmentService assessmentService;
        private readonly HHTaskService taskService;
        private readonly HHBillingService billingService;
        private readonly FaceToFaceEncounterService faceToFaceEncounterService;

        public HHAdmitNewReferralWorkflow(Patient patient, Profile profile)
        {
            Check.Argument.IsNotNull(profile, "profile");

            this.patient = patient;
            this.profile = profile;
            this.referralService = Container.Resolve<IReferralService>();
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<HHPatientProfileService>();
            this.mediatorService = Container.Resolve<HHMediatorService>();
            this.epiosdeService = Container.Resolve<HHEpiosdeService>();
            this.assessmentService = Container.Resolve<HHAssessmentService>();
            this.taskService = Container.Resolve<HHTaskService>();
            this.billingService = Container.Resolve<HHBillingService>();
            this.faceToFaceEncounterService = Container.Resolve<FaceToFaceEncounterService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        #region IWorkflow Members

        private void AddWorkItems()
        {
             this.Validate(this.profile);
             if (this.isValid)
             {
                 var existingProfile = profileService.GetProfileOnly(patient.Id);
                 if (existingProfile != null)
                 {
                     this.isProfileExist = true;
                     EntityHelper.SetProfileFromClientProfile(existingProfile, profile);

                     existingProfile.PatientIdNumber = patient.PatientIdNumber;
                     existingProfile.LastName = patient.LastName;
                     existingProfile.FirstName = patient.FirstName;
                     existingProfile.MiddleInitial = patient.MiddleInitial;
                     existingProfile.Status = patient.HomeHealthStatus;

                     this.profile = existingProfile;

                     works.Add(new WorkItem(() => this.profileService.UpdateModal(existingProfile), () => { }, "System could not admit the referral patient."));
                 }
                 else
                 {
                     profile.AdmissionId = Guid.NewGuid();
                     profile.Id = patient.Id;
                     profile.AgencyId = Current.AgencyId;

                     works.Add(new WorkItem(() =>
                     {
                         profile.PatientIdNumber = patient.PatientIdNumber;
                         profile.LastName = patient.LastName;
                         profile.FirstName = patient.FirstName;
                         profile.MiddleInitial = patient.MiddleInitial;
                         profile.Status = patient.HomeHealthStatus;
                         profile.Created = DateTime.Now;

                         return profileService.Add(profile);

                     }, () => this.profileService.Remove(this.profile.Id), "System could not admit the referral patient."));
                 }
               

                 works.Add(new WorkItem(
                     () =>
                     {
                         return AdmissionHelperFactory<PatientEpisode>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient,profile, 0, string.Empty, profile.AdmissionId));
                     },
                     () => AdmissionHelperFactory<PatientEpisode>.RemovePatientAdmissionDate(this.patient.Id, this.profile.AdmissionId),
                     "System could not admit the referral patient."));
                 if (profile.EpisodeStartDate.IsValid())
                 {

                     var episode = EntityHelper.CreateEpisode<PatientEpisode>(profile.Id, profile.EpisodeStartDate, 59);
                     {
                         episode.StartOfCareDate = profile.StartofCareDate;
                         episode.AdmissionId = profile.AdmissionId;
                         episode.PrimaryInsurance = profile.PrimaryInsurance;
                         episode.SecondaryInsurance = profile.SecondaryInsurance;
                         episode.CaseManagerId = profile.CaseManagerId;
                         works.Add(new WorkItem(() => this.epiosdeService.AddEpisode(episode), () => this.epiosdeService.RemoveEpisode(this.patient.Id, episode.Id), "System could not admit the referral patient."));
                     }


                     if (profile.StartofCareDate.Date == profile.EpisodeStartDate.Date)
                     {
                         var newEvent = EntityHelper.CreateTaskForSOC<ScheduleEvent, PatientEpisode>(patient, profile, episode);
                         var assessment = EntityHelper.CreateAssessment<ScheduleEvent>(newEvent, string.Empty);
                         works.Add(new WorkItem(() =>
                         {
                             assessment.Questions = assessmentService.CreateOASISPatientDemographics(patient, profile, newEvent.DisciplineTask);
                             return assessmentService.AddScheduleTaskAndAssessment(assessment, newEvent);
                         }, () =>
                         {
                             if (taskService.Remove(patient.Id, assessment.Id))
                             {
                                 assessmentService.Remove(assessment.Id);
                             }
                         }, "System could not admit Home Health patient."));

                     }

                     var physician = new AgencyPhysician();
                     var isClaimsAdded = AgencyInformationHelper.IsMedicareOrMedicareHMOInsurance(profile.PrimaryInsurance);
                     if (isClaimsAdded || profile.IsFaceToFaceEncounterCreated)
                     {
                         physician = patientService.GetPatientPrimaryPhysician(patient.Id);
                     }
                     if (isClaimsAdded)
                     {
                         works.Add(new WorkItem(() =>
                         {
                             patient.Profile = profile;
                             billingService.AddRap(patient, episode, profile.PrimaryInsurance, physician);
                             billingService.AddFinal(patient, episode, profile.PrimaryInsurance, physician);
                             return true;
                         }, () => this.billingService.RemoveMedicareClaims(this.patient.Id, episode.Id), "System could not admit Home Health patient."));
                     }


                     if (profile.IsFaceToFaceEncounterCreated && physician!=null && !physician.IsDeprecated)
                     {
                         var faceToFaceEncounter = EntityHelper.CreateFaceToFaceEncounter(profile, episode.Id, physician);

                         works.Add(new WorkItem(() => this.faceToFaceEncounterService.CreateFaceToFaceEncounterAfterPhysicianInfo(faceToFaceEncounter),
                         () =>
                         {
                             if (taskService.Remove(patient.Id, faceToFaceEncounter.Id))
                             {
                                 faceToFaceEncounterService.Remove(patient.Id, faceToFaceEncounter.Id);
                             }
                         }, "System could not admit Home Health patient."));
                     }
                 }
             }

        }

        public void Validate(Profile homeHealthProfile)
        {
            var rules = this.ValidationRules(homeHealthProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile homeHealthProfile)
        {
            var rules = new List<Validation>();
            if (homeHealthProfile != null)
            {
                if (patient.MedicareNumber.IsNotNullOrEmpty())
                {
                    bool medicareNumberCheck = patientService.IsMedicareExistForEdit(patient.Id, patient.MedicareNumber.Trim());
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty())
                {
                    bool medicaidNumberCheck = patientService.IsMedicaidExistForEdit(patient.Id, patient.MedicaidNumber.Trim());
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                rules.Add(new Validation(() => !homeHealthProfile.StartofCareDate.IsValid(), "Valid start of care is required."));

                if (homeHealthProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }

            }
            return rules;
        }

        #endregion

    }

    public class PrivateDutyAdmitNewReferralWorkflow
    {

        #region CreateEpisodeWorkflow

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        public List<WorkItem> works { get; set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isProfileExist { get; set; }
        public bool IsProfileExist { get { return this.isProfileExist; } }

        private readonly IPatientService patientService;
        private readonly PrivateDutyPatientProfileService profileService;
        private readonly PrivateDutyEpisodeService epiosdeService;
        public PrivateDutyAdmitNewReferralWorkflow(Patient patient, Profile profile)
        {
            Check.Argument.IsNotNull(profile, "profile");

            this.patient = patient;
            this.profile = profile;
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
            this.epiosdeService = Container.Resolve<PrivateDutyEpisodeService>();
            this.works = new List<WorkItem>();
            this.AddWorkItems();
        }

        #endregion

        #region IWorkflow Members

        private void AddWorkItems()
        {
            this.Validate(this.profile);
            if (this.isValid)
            {
                var existingProfile = profileService.GetProfileOnly(patient.Id);
                if (existingProfile != null)
                {
                    this.isProfileExist = true;
                    EntityHelper.SetProfileFromClientProfile(existingProfile, profile);
                    existingProfile.PatientIdNumber = patient.PatientIdNumber;
                    existingProfile.LastName = patient.LastName;
                    existingProfile.FirstName = patient.FirstName;
                    existingProfile.MiddleInitial = patient.MiddleInitial;
                    existingProfile.Status = patient.PrivateDutyStatus;

                    this.profile = existingProfile;

                    works.Add(new WorkItem(() => this.profileService.UpdateModal(existingProfile), () => { }, "System could not admit the referral patient."));
                }
                else
                {
                    profile.AdmissionId =  Guid.NewGuid();
                    profile.Id = patient.Id;
                    profile.AgencyId = Current.AgencyId;
                    works.Add(new WorkItem(() =>
                    {
                        profile.PatientIdNumber = patient.PatientIdNumber;
                        profile.LastName = patient.LastName;
                        profile.FirstName = patient.FirstName;
                        profile.MiddleInitial = patient.MiddleInitial;
                        profile.Status = patient.PrivateDutyStatus;
                        profile.Created = DateTime.Now;

                        return profileService.Add(profile);

                    }, () => this.profileService.Remove(this.profile.Id), "System could not admit the referral patient."));
                }


                works.Add(new WorkItem(
                    () => AdmissionHelperFactory<PrivateDutyCarePeriod>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(this.patient, this.profile, 0, string.Empty, this.profile.AdmissionId)),
                    () => AdmissionHelperFactory<PrivateDutyCarePeriod>.RemovePatientAdmissionDate(this.patient.Id, this.profile.AdmissionId),
                    "System could not admit the referral patient."));
                if (profile.EpisodeStartDate.IsValid())
                {
                    var days = (profile.EpisodeEndDate - profile.EpisodeStartDate).Days;

                    var episode = EntityHelper.CreateEpisode<PrivateDutyCarePeriod>(profile.Id, profile.EpisodeStartDate, days);
                    {
                        episode.StartOfCareDate = profile.StartofCareDate;
                        episode.AdmissionId = profile.AdmissionId;
                        episode.PrimaryInsurance = profile.PrimaryInsurance;
                        episode.SecondaryInsurance = profile.SecondaryInsurance;
                        episode.CaseManagerId = profile.CaseManagerId;
                        works.Add(new WorkItem(() => this.epiosdeService.AddEpisode(episode), () => this.epiosdeService.RemoveEpisode(this.patient.Id, episode.Id), "System could not admit the referral patient."));
                    }
                }
            }

        }

        public void Validate(Profile privateDutyProfile)
        {
            var rules = this.ValidationRules(privateDutyProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        public List<Validation> ValidationRules(Profile privateDutyProfile)
        {
            var rules = new List<Validation>();

            if (privateDutyProfile != null)
            {
                if (privateDutyProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
            }
            return rules;
        }

        #endregion

    }
}
