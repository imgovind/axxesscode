﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core;
using Axxess.Core.Enums;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Application.Services;
using Axxess.AgencyManagement.Entities.Enums;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Application.Workflows
{
   public class TogglePatientWorkflow
    {

        private Guid Id { get; set; }
        private List<string> ServiceProvided { get; set; }
        private bool IsDeprecated { get; set; }

        private readonly IPatientService patientService;

        public TogglePatientWorkflow(Guid Id, List<string> ServiceProvided,bool IsDeprecated)
        {
            Check.Argument.IsNotNull(Id, "Id");

            this.Id = Id;
            this.ServiceProvided = ServiceProvided;
            this.IsDeprecated = IsDeprecated;
            this.viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("A problem occured while trying to {0} this Patient. Please try again.", this.IsDeprecated ? "delete" : "restore") };

            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }


        private JsonViewData viewData { get; set; }
        public JsonViewData ViewData { get { return viewData; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        private bool IsErrorExist { get; set; }

        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = !this.IsErrorExist;
                this.viewData.isSuccessful = this.isCommitted;
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.viewData.isSuccessful = false;
                this.viewData.errorMessage = item.Description;
                this.IsErrorExist = true;
            };
            if (ServiceProvided.IsNotNullOrEmpty())
            {
                var serviceCount = ServiceProvided.Count;
                if (!this.Id.IsEmpty())
                {
                    var patient = patientService.GetPatientOnly(this.Id);

                    if (patient != null)
                    {
                        var serviceChanged = 0;
                        var oldHHStatus = patient.HomeHealthStatus;
                        var oldPDStatus = patient.PrivateDutyStatus;

                        if (ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                        {
                            var hhWorkFlow = new HHTogglePatientWorkflow(this.Id, patient, IsDeprecated);
                            var hhWorkItems = hhWorkFlow.Works;
                            if (hhWorkItems.IsNotNullOrEmpty())
                            {
                                serviceChanged++;
                                patient.HomeHealthStatus = hhWorkFlow.ProfileStatus;
                                work.AddRange(hhWorkItems);
                            }
                            else
                            {
                                this.isCommitted = false;
                                return;
                            }
                        }

                        if (ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                        {
                                var pdWorkFlow = new PrivateDutyTogglePatientWorkflow(this.Id,patient,IsDeprecated);
                                var pdWorkItems = pdWorkFlow.Works;
                                if (pdWorkItems.IsNotNullOrEmpty())
                                {
                                    serviceChanged++;
                                    patient.PrivateDutyStatus = pdWorkFlow.ProfileStatus;
                                    work.AddRange(pdWorkItems);
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    return;
                                }
                           
                        }
                        if (serviceCount > 0 && serviceCount == serviceChanged)
                        {
                        }
                        else
                        {
                            this.isCommitted = false;
                            return;
                        }
                        // Insert the patient on zero index
                        work.Insert(0, () => { return patientService.UpdateModal(patient); },
                            () =>
                            {
                                patient.HomeHealthStatus = oldHHStatus;
                                patient.PrivateDutyStatus = oldPDStatus;

                                patientService.UpdateModal(patient);
                            },
                            string.Format("System could not {0} the patient.", this.IsDeprecated ? "delete" : "restore"));


                        work.Perform();
                        if (this.isCommitted)
                        {
                            viewData.isSuccessful = true;
                            viewData.PatientId = this.Id;
                            viewData.IsCenterRefresh = true;
                            viewData.IsDeletedPatientListRefresh = true;
                            viewData.IsPatientListRefresh = true;
                            viewData.IsPendingPatientListRefresh = patient.HomeHealthStatus == (int)PatientStatus.Pending || patient.PrivateDutyStatus == (int)PatientStatus.Pending || oldHHStatus == (int)PatientStatus.Pending || oldPDStatus == (int)PatientStatus.Pending;
                        }
                    }
                    else
                    {
                        this.isCommitted = false;
                        this.viewData.isSuccessful = false;
                        this.viewData.errorMessage = "Patient doesn't exists.";
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.viewData.isSuccessful = false;
                    this.viewData.errorMessage = "Patient doesn't exists.";
                }
            }
        }
    }

   internal class HHTogglePatientWorkflow
   {
       private Guid Id { get; set; }
       private Patient Patient { get; set; }
       private bool IsDeprecated { get; set; }
       private int profileStatus { get; set; }
       public int ProfileStatus { get { return this.profileStatus; } }
       public List<WorkItem> works { get; set; }
       public List<WorkItem> Works { get { return works; } }
       private string message { get; set; }
       public string Message { get { return message; } }
       private readonly HHPatientProfileService profileService;
       public HHTogglePatientWorkflow(Guid Id, Patient patient, bool IsDeprecated)
       {
           Check.Argument.IsNotNull(Id, "Id");

           this.Id = Id;
           this.Patient = patient;
           this.IsDeprecated = IsDeprecated;
           this.works = new List<WorkItem>();
           this.profileService = Container.Resolve<HHPatientProfileService>();
           this.AddWorkItems();
       }

       private void AddWorkItems()
       {
           var profile = profileService.GetProfileOnly(this.Id);
           if (profile != null)
           {
              
               var oldStaus = profile.Status;
               var previousOldStatus = profile.OldStatus;
               var oldIsDeprecated=profile.IsDeprecated;
               if (this.IsDeprecated)
               {
                   profile.OldStatus = profile.Status;
                   profile.Status = (int)PatientStatus.Deprecated;
                   profile.IsDeprecated = true;
               }
               else
               {
                   profile.Status = profile.OldStatus;
                   profile.IsDeprecated = false;
               }
               this.profileStatus = profile.Status;

               works.Add(new WorkItem(() =>   profileService.UpdateModal(profile),
                   () =>
                   {
                     
                       profile.Status = oldStaus;
                       profile.OldStatus = previousOldStatus;
                       profile.IsDeprecated=oldIsDeprecated;

                       profileService.UpdateModal(profile);
                   },
                   string.Format("System could not {0} Home Health patient.", this.IsDeprecated ? "delete" : "restore")));

           }
           else
           {
               works.Add(new WorkItem(() => { return false; }, null,"System could not find  Home Health profile."));
           }
       }
   }

   internal class PrivateDutyTogglePatientWorkflow
   {

       private Guid Id { get; set; }
       private Patient Patient { get; set; }
       private int profileStatus { get; set; }
       public int ProfileStatus { get { return this.profileStatus; } }
       private bool IsDeprecated { get; set; }
       public List<WorkItem> works { get; set; }
       public List<WorkItem> Works { get { return works; } }
       private string message { get; set; }
       public string Message { get { return message; } }
       private readonly PrivateDutyPatientProfileService profileService;
       public PrivateDutyTogglePatientWorkflow(Guid Id, Patient patient, bool IsDeprecated )
       {
           Check.Argument.IsNotNull(Id, "Id");

           this.Id = Id;
           this.Patient = patient;
           this.IsDeprecated = IsDeprecated;
           this.works = new List<WorkItem>();
           this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
           this.AddWorkItems();
       }
       private void AddWorkItems()
       {
           var profile = profileService.GetProfileOnly(this.Id);
           if (profile != null)
           {

               var oldStaus = profile.Status;
               var previousOldStatus = profile.OldStatus;
               var oldIsDeprecated = profile.IsDeprecated;
               if (this.IsDeprecated)
               {
                   profile.OldStatus = profile.Status;
                   profile.Status = (int)PatientStatus.Deprecated;
                   profile.IsDeprecated = true;
               }
               else
               {
                   profile.Status = profile.OldStatus;
                   profile.IsDeprecated = false;
               }
               this.profileStatus = profile.Status;
               works.Add(new WorkItem(() => profileService.UpdateModal(profile),
                   () =>
                   {

                       profile.Status = oldStaus;
                       profile.OldStatus = previousOldStatus;
                       profile.IsDeprecated = oldIsDeprecated;

                       profileService.UpdateModal(profile);
                   },
                   string.Format("System could not {0} Private Duty patient.", this.IsDeprecated ? "delete" : "restore")));

           }
           else
           {
               works.Add(new WorkItem(() => { return false; }, null, "System could not find  Private Duty profile."));
           }
       }

   }
}



