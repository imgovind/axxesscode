﻿namespace Axxess.AgencyManagement.Application.Workflows
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Services;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Application.Helpers;

    public class CreateEpisodeWorkflow
    {
        #region CreateEpisodeWorkflow Members

        private Patient patient { get; set; }
        private PatientEpisode episode { get; set; }

        private readonly IPatientService patientService;
        private readonly HHPatientProfileService patientProfileService;
        private readonly HHBillingService billingService;
        private readonly HHEpiosdeService episodeService;

        public CreateEpisodeWorkflow(PatientEpisode episode)
        {
            this.episode = episode;
            this.patientService = Container.Resolve<IPatientService>();
            this.patientProfileService = Container.Resolve<HHPatientProfileService>();
            this.billingService = Container.Resolve<HHBillingService>();
            this.episodeService = Container.Resolve<HHEpiosdeService>();

            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var patient = patientService.GetPatientOnly(episode.PatientId);

            if (patient != null)
            {
                var profile = patientProfileService.GetProfileOnly(patient.Id);
                if (profile != null)
                {
                    patient.Profile = profile;
                    this.patient = patient;
                    this.Validate();
                    if (this.isValid)
                    {
                        var work = new WorkSequence();
                        work.Complete += (sequence) =>
                        {
                            this.isCommitted = this.message.IsNullOrEmpty();
                        };

                        work.Error += (sequence, item, index) =>
                        {
                            this.isCommitted = false;
                            this.message = item.Description;
                        };
                        var patientEpisode = EntityHelper.CreateEpisode<PatientEpisode>(patient.Id, episode);

                        work.Add(
                            () =>
                            {
                                patient.AgencyId = Current.AgencyId;
                                patientEpisode.AdmissionId = episode.AdmissionId;
                                return episodeService.AddEpisode(patientEpisode);
                            },
                            () =>
                            {
                                episodeService.RemoveEpisode(patient.Id, patientEpisode.Id);
                            },
                            "System could not save the episode information.");

                        if ( patient.Profile.PrimaryInsurance>0)
                        {
                            if (AgencyInformationHelper.IsMedicareOrMedicareHMOInsurance(patient.Profile.PrimaryInsurance))
                            {
                                var physician = patientService.GetPatientPrimaryPhysician(patient.Id);
                                work.Add(
                                    () =>
                                    {
                                        return billingService.AddRap(patient, patientEpisode, patient.Profile.PrimaryInsurance, physician);
                                    },
                                    () =>
                                    {
                                        billingService.RemoveMedicareClaim(patient.Id, patientEpisode.Id, (int)ClaimTypeSubCategory.RAP);
                                    },
                                    "System could not save the episode information.");
                                work.Add(
                                    () =>
                                    {
                                        return billingService.AddFinal(patient, patientEpisode, patient.Profile.PrimaryInsurance, physician);
                                    },
                                    () =>
                                    {
                                        billingService.RemoveMedicareClaim(patient.Id, patientEpisode.Id, (int)ClaimTypeSubCategory.Final);
                                    },
                                    "System could not save the episode information.");
                            }
                        }

                        work.Perform();
                    }
                    else
                    {
                        this.isCommitted = false;
                        this.message = this.errorMessage;
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.message = "Patient profile is not available.";
                }
            }
            else
            {
                this.isCommitted = false;
                this.message = "Patient data is not available.";
            }
        }

        public void Validate()
        {
            var entityValidator = new EntityValidator(episodeService.EpisodeValidation(this.patient, this.episode).ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.errorMessage = entityValidator.Message;
            }
        }
        private bool isValid { get; set; }
        private string errorMessage { get; set; }


        #endregion
    }
}
