﻿namespace Axxess.AgencyManagement.Application.Workflows
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.Helpers;
    using System.Collections.Generic;

    public class CreatePatientWorkflow : IWorkflow
    {
        #region CreatePatientWorkflow Members

        private Patient patient { get; set; }
        private List<Profile> profiles { get; set; }
        private bool isPending { get; set; }
        private NewPatientReferralSource referralSource { get; set; }

        private readonly IPatientService patientService;
        private readonly IReferralService referralService;

        public CreatePatientWorkflow(Patient patient, List<Profile> profiles, bool isPending, NewPatientReferralSource referralSource)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profiles = profiles;
            this.isPending = isPending;
            this.referralSource = referralSource;

            this.patientService = Container.Resolve<IPatientService>();
            this.referralService = Container.Resolve<IReferralService>();
            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = this.message.IsNullOrEmpty();
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
            };
            if (patient != null)
            {
                if (patient.ServiceProvided.IsNotNullOrEmpty())
                {
                    if (profiles.IsNotNullOrEmpty())
                    {
                            if (patient.IsValid())
                            {
                                if (!patientService.IsPatientIdExist(patient.PatientIdNumber))
                                {
                                    patient.Encode();
                                    patient.Id = Guid.NewGuid();
                                    patient.AgencyId = Current.AgencyId;

                                    if (patient.EmergencyContact.FirstName.IsNotNullOrEmpty() && patient.EmergencyContact.LastName.IsNotNullOrEmpty() && patient.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty())
                                    {
                                        work.Add(
                                            () =>
                                            {
                                                patient.EmergencyContact.PatientId = patient.Id;
                                                patient.EmergencyContact.AgencyId = Current.AgencyId;
                                                return patientService.AddPrimaryEmergencyContact(patient);
                                            },
                                            () => this.patientService.RemoveEmergencyContacts(this.patient.Id), "System could not save the Emergency Contact information.");
                                    }

                                    work.Add(() => this.patientService.LinkPhysicians(this.patient), () => this.patientService.UnlinkAll(this.patient.Id), "System could not link the Patient to the Physician.");

                                    var medication = EntityHelper.CreateMedicationProfile(patient);
                                    work.Add(() => this.patientService.CreateMedicationProfile(medication), () => this.patientService.RemoveMedicationProfile(this.patient.Id, medication.Id), "System could not admit the patient.");

                                    var serviceCount = 0;

                                    if (patient.ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                                    {

                                        var homeHealthProfile = this.profiles.FirstOrDefault(p => p.ServiceType == (int)AgencyServices.HomeHealth);
                                        if (homeHealthProfile != null)
                                        {
                                            homeHealthProfile.Id = patient.Id;
                                            homeHealthProfile.AgencyId = patient.AgencyId;
                                            homeHealthProfile.AdmissionId = Guid.NewGuid();
                                            homeHealthProfile.AdmissionSource = referralSource.AdmissionSource.ToString();
                                            homeHealthProfile.OtherReferralSource = referralSource.OtherReferralSource;
                                            homeHealthProfile.ReferralDate = referralSource.ReferralDate;
                                            homeHealthProfile.InternalReferral = referralSource.InternalReferral;
                                            homeHealthProfile.Status = this.isPending || !homeHealthProfile.ShouldCreateEpisode ? (int)PatientStatus.Pending : (int)PatientStatus.Active;
                                            patient.HomeHealthStatus = homeHealthProfile.Status;

                                            var hhWorkFlow = new HHCreatePatientWorkflow(patient, homeHealthProfile, this.isPending);
                                            if (hhWorkFlow.IsValid)
                                            {
                                                var hhWorkItems = hhWorkFlow.Works;
                                                if (hhWorkItems.IsNotNullOrEmpty())
                                                {
                                                    serviceCount++;
                                                    patient.Services |= AgencyServices.HomeHealth;
                                                    work.AddRange(hhWorkItems);
                                                }
                                                else
                                                {
                                                    this.isCommitted = false;
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                this.isCommitted = false;
                                                this.message = hhWorkFlow.Message;
                                                return;
                                            }

                                        }
                                        else
                                        {
                                            this.isCommitted = false;
                                            return;
                                        }
                                    }
                                    if (patient.ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                                    {
                                        var privateDutyProfile = this.profiles.FirstOrDefault(p => p.ServiceType == (int)AgencyServices.PrivateDuty);
                                        if (privateDutyProfile != null)
                                        {
                                            privateDutyProfile.Id = patient.Id;
                                            privateDutyProfile.AgencyId = patient.AgencyId;
                                            privateDutyProfile.AdmissionId = Guid.NewGuid();
                                            privateDutyProfile.AdmissionSource = referralSource.AdmissionSource.ToString();
                                            privateDutyProfile.OtherReferralSource = referralSource.OtherReferralSource;
                                            privateDutyProfile.ReferralDate = referralSource.ReferralDate;
                                            privateDutyProfile.InternalReferral = referralSource.InternalReferral;
                                            privateDutyProfile.Status = this.isPending || !privateDutyProfile.ShouldCreateEpisode ? (int)PatientStatus.Pending : (int)PatientStatus.Active;
                                            patient.PrivateDutyStatus = privateDutyProfile.Status;
                                            var pdWorkFlow = new PrivateDutyCreatePatientWorkflow(patient, privateDutyProfile, this.isPending);
                                            if (pdWorkFlow.IsValid)
                                            {
                                                var pdWorkItems = pdWorkFlow.Works;
                                                if (pdWorkItems.IsNotNullOrEmpty())
                                                {
                                                    serviceCount++;
                                                    patient.Services |= AgencyServices.PrivateDuty;
                                                    work.AddRange(pdWorkItems);
                                                }
                                                else
                                                {
                                                    this.isCommitted = false;
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                this.isCommitted = false;
                                                this.message = pdWorkFlow.Message;
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            this.isCommitted = false;
                                            return;
                                        }
                                    }
                                    if (serviceCount > 0 && serviceCount == patient.ServiceProvided.Count)
                                    {
                                    }
                                    else
                                    {
                                        this.isCommitted = false;
                                        return;
                                    }
                                    // Insert the patient on zero index
                                    work.Insert(0, () => this.patientService.AddPatient(this.patient), () => this.patientService.RemovePatient(this.patient.Id), "System could not save the Patient information.");

                                    if (!patient.ReferralId.IsEmpty())
                                    {
                                        work.Add(() => this.referralService.SetStatus(this.patient.ReferralId, new Referral { HomeHealthStatus = (int)ReferralStatus.Admitted, PrivateDutyStatus = (int)ReferralStatus.Admitted, Services = this.patient.Services }));
                                    }

                                    work.Perform();
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    this.message = "Patient Id Number already exists.";
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                this.message = patient.ValidationMessage;
                            }
                    }
                }
            }
        }

        #endregion
    }

    internal class HHCreatePatientWorkflow
    {

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        private List<WorkItem> works { get;  set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }
        private bool isPending { get; set; }

        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;
        private readonly HHEpiosdeService episodeService;
        private readonly HHAssessmentService assessmentService;
        private readonly HHTaskService taskService;
        private readonly HHBillingService billingService;
        private readonly FaceToFaceEncounterService faceToFaceEncounterService;
        public HHCreatePatientWorkflow(Patient patient, Profile profile, bool isPending)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profile = profile;
            this.isPending = isPending;
            this.works = new List<WorkItem>();
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<HHPatientProfileService>();
            this.episodeService = Container.Resolve<HHEpiosdeService>();
            this.assessmentService = Container.Resolve<HHAssessmentService>();
            this.taskService = Container.Resolve<HHTaskService>();
            this.billingService = Container.Resolve<HHBillingService>();
            this.faceToFaceEncounterService = Container.Resolve<FaceToFaceEncounterService>();
            this.AddWorkItems();
        }

        private void AddWorkItems()
        {
            this.Validate(this.profile);
            if (this.isValid)
            {
                profile.Encode();
                profile.PatientIdNumber = patient.PatientIdNumber;
                profile.LastName = patient.LastName;
                profile.FirstName = patient.FirstName;
                profile.MiddleInitial = patient.MiddleInitial;
                profile.Modified = DateTime.Now;
                profile.Created = DateTime.Now;

                works.Add(new WorkItem(() => profileService.Add(profile), () => profileService.Remove(profile.Id), "System could not add the new patient."));

                works.Add(new WorkItem(() => AdmissionHelperFactory<PatientEpisode>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient, profile ?? new Profile(), 0, string.Empty, profile.AdmissionId)), () => AdmissionHelperFactory<PatientEpisode>.RemovePatientAdmissionDate(patient.Id, profile.AdmissionId), "System could not add the new patient."));
                if (!this.isPending)
                {
                    if (profile.ShouldCreateEpisode)
                    {
                        var episode = EntityHelper.CreateEpisode<PatientEpisode>(profile.Id, profile.EpisodeStartDate, 59);
                        {
                            episode.StartOfCareDate = profile.StartofCareDate;
                            episode.AdmissionId = profile.AdmissionId;
                            episode.PrimaryInsurance = profile.PrimaryInsurance;
                            episode.SecondaryInsurance = profile.SecondaryInsurance;
                            episode.CaseManagerId = profile.CaseManagerId;
                            works.Add(new WorkItem(() => this.episodeService.AddEpisode(episode),() => this.episodeService.RemoveEpisode(this.patient.Id, episode.Id),"System could not add the new patient."));
                        }

                        if (profile.StartofCareDate.Date == profile.EpisodeStartDate.Date)
                        {
                            var newEvent = EntityHelper.CreateTaskForSOC<ScheduleEvent, PatientEpisode>(patient, profile, episode);
                            var assessment = EntityHelper.CreateAssessment<ScheduleEvent>(newEvent, string.Empty);
                            works.Add(new WorkItem(() =>
                            {
                                assessment.Questions = assessmentService.CreateOASISPatientDemographics(patient,profile, newEvent.DisciplineTask);
                                return assessmentService.AddScheduleTaskAndAssessment(assessment, newEvent);
                            }, () =>
                            {
                                if (taskService.Remove(patient.Id, assessment.Id))
                                {
                                    assessmentService.Remove(assessment.Id);
                                }
                            }, "System could not add the new patient"));
                        }



                       // patient.IsFaceToFaceEncounterCreated = patient.IsFaceToFaceEncounterCreated;

                        var physician = new AgencyPhysician();
                        var isClaimsAdded = AgencyInformationHelper.IsMedicareOrMedicareHMOInsurance(profile.PrimaryInsurance);
                        if (isClaimsAdded || profile.IsFaceToFaceEncounterCreated)
                        {
                            physician = patientService.GetPatientPrimaryPhysician(patient.Id);
                        }
                        if (isClaimsAdded)
                        {
                            patient.Profile = profile;
                            works.Add(new WorkItem(() =>
                            {
                                billingService.AddRap(patient, episode, profile.PrimaryInsurance, physician);
                                billingService.AddFinal(patient, episode, profile.PrimaryInsurance, physician);
                                return true;
                            }, () => this.billingService.RemoveMedicareClaims(this.patient.Id, episode.Id), "System could not add the new patient"));
                        }


                        if (profile.IsFaceToFaceEncounterCreated)
                        {
                            var faceToFaceEncounter = EntityHelper.CreateFaceToFaceEncounter(profile, episode.Id, physician);
                            works.Add(new WorkItem(() => this.faceToFaceEncounterService.CreateFaceToFaceEncounterAfterPhysicianInfo(faceToFaceEncounter),
                            () =>
                            {
                                if (taskService.Remove(patient.Id, faceToFaceEncounter.Id))
                                {
                                    faceToFaceEncounterService.Remove(patient.Id, faceToFaceEncounter.Id);
                                }
                            }, "System could not add the new patient"));
                        }
                    }
                    else
                    {
                        works.Add((new WorkItem(() => this.patientService.SetStatus(this.patient.Id, PatientStatus.Pending, AgencyServices.HomeHealth), null, string.Empty)));
                    }
                }
            }
        }

        private void Validate(Profile homeHealthProfile)
        {
            var rules = this.ValidationRules(homeHealthProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        private List<Validation> ValidationRules(Profile homeHealthProfile)
        {
            var rules = new List<Validation>();


            if (homeHealthProfile != null)
            {
                if (patient.MedicareNumber.IsNotNullOrEmpty())
                {
                    bool medicareNumberCheck = patientService.IsMedicareExist(patient.MedicareNumber.Trim());
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty())
                {
                    bool medicaidNumberCheck = patientService.IsMedicaidExist(patient.MedicaidNumber.Trim());
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                if (homeHealthProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => homeHealthProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
                if (homeHealthProfile.PaymentSource.IsNullOrEmpty())
                {
                    rules.Add(new Validation(() => homeHealthProfile.PaymentSources.Count == 0, "Patient Payment Source is required."));
                }
            }
            return rules;
        }

    }

    internal class PrivateDutyCreatePatientWorkflow
    {

        private Patient patient { get; set; }
        private Profile profile { get; set; }
        private List<WorkItem> works { get;  set; }
        public List<WorkItem> Works { get { return works; } }
        private bool isValid { get; set; }
        public bool IsValid { get { return this.isValid; } }
        private string message { get; set; }
        public string Message { get { return message; } }
        private bool isPending { get; set; }
        private readonly IPatientService patientService;
        private readonly PrivateDutyPatientProfileService profileService;
        private readonly PrivateDutyEpisodeService epiosdeService;
        
        public PrivateDutyCreatePatientWorkflow(Patient patient, Profile profile, bool isPending)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.profile = profile;
            this.isPending = isPending;
            this.works = new List<WorkItem>();
            this.patientService = Container.Resolve<IPatientService>();
            this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
            this.epiosdeService = Container.Resolve<PrivateDutyEpisodeService>();
            this.AddWorkItems();
        }

        private void AddWorkItems()
        {
            this.Validate(this.profile);
            if (this.isValid)
            {
                profile.Encode();
                profile.PatientIdNumber = patient.PatientIdNumber;
                profile.LastName = patient.LastName;
                profile.FirstName = patient.FirstName;
                profile.MiddleInitial = patient.MiddleInitial;
                profile.Created = DateTime.Now;
                profile.Modified = DateTime.Now;

                works.Add(new WorkItem(() => profileService.Add(profile), () => profileService.Remove(profile.Id), "System could not add the new patient."));

                works.Add(new WorkItem(() => AdmissionHelperFactory<PrivateDutyCarePeriod>.AddPatientAdmissionDate(EntityHelper.CreatePatientAdmissionDate(patient, this.profile, 0, string.Empty, profile.AdmissionId)), () => AdmissionHelperFactory<PrivateDutyCarePeriod>.RemovePatientAdmissionDate(patient.Id, profile.AdmissionId), "System could not add the new patient."));
               
                if (!this.isPending)
                {
                    if (profile.ShouldCreateEpisode)
                    {
                        var days=(profile.EpisodeEndDate - profile.EpisodeStartDate).Days;
                        var episode = EntityHelper.CreateEpisode<PrivateDutyCarePeriod>(profile.Id, profile.EpisodeStartDate,days );
                        {
                            episode.StartOfCareDate = profile.StartofCareDate;
                            episode.AdmissionId = profile.AdmissionId;
                            episode.PrimaryInsurance = profile.PrimaryInsurance;
                            episode.SecondaryInsurance = profile.SecondaryInsurance;
                            episode.CaseManagerId = profile.CaseManagerId;
                            works.Add(new WorkItem(() => this.epiosdeService.AddEpisode(episode),() => this.epiosdeService.RemoveEpisode(this.patient.Id, episode.Id),"System could not add the new patient."));
                        }
                    }
                    else
                    {
                        works.Add((new WorkItem(() => this.patientService.SetStatus(this.patient.Id, PatientStatus.Pending, AgencyServices.PrivateDuty), null, string.Empty)));
                    }
                }

            }
        }

        private void Validate(Profile privateDutyProfile)
        {
            var rules = this.ValidationRules(privateDutyProfile);
            var entityValidator = new EntityValidator(rules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                this.isValid = true;
            }
            else
            {
                this.isValid = false;
                this.message = entityValidator.Message;
            }
        }

        private List<Validation> ValidationRules(Profile privateDutyProfile)
        {
            var rules = new List<Validation>();

            if (privateDutyProfile != null)
            {
                if (privateDutyProfile.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }
                if (privateDutyProfile.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => privateDutyProfile.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
            }
            return rules;
        }

    }
}
               
              
