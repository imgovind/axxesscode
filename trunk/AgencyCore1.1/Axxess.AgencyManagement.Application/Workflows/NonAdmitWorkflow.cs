﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core;
using Axxess.Core.Enums;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities;

using Axxess.AgencyManagement.Application.Services;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.Workflows
{
    public class NonAdmitWorkflow : IWorkflow
    {

        private Guid Id { get; set; }
        private Patient Patient { get; set; }
        private List<NonAdmit> Profiles { get; set; }
        private List<string> ServiceProvided { get; set; }

        private readonly IPatientService patientService;

        public NonAdmitWorkflow(Guid Id, List<string> ServiceProvided, List<NonAdmit> profiles)
        {
            Check.Argument.IsNotNull(Id, "Id");

            this.Id = Id;
            this.Profiles = profiles;
            this.ServiceProvided = ServiceProvided;

            this.patientService = Container.Resolve<IPatientService>();
            this.Process();
        }


        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = this.message.IsNullOrEmpty();
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
            };
            if (ServiceProvided.IsNotNullOrEmpty())
            {
                var serviceCount = ServiceProvided.Count;
                if (!this.Id.IsEmpty())
                {
                    var patient = patientService.GetPatientOnly(this.Id);

                    if (patient != null)
                    {
                        this.Patient = patient;
                        var serviceChanged = 0;
                        var oldHHStatus = patient.HomeHealthStatus;
                        var oldPDStatus = patient.PrivateDutyStatus;

                        if (ServiceProvided.Contains(AgencyServices.HomeHealth.ToString()))
                        {
                            var homeHealthProfile = Profiles.Where(p => p.ServiceType == (int)AgencyServices.HomeHealth).FirstOrDefault();
                            if (homeHealthProfile != null)
                            {
                                var hhWorkFlow = new HHNonAdmitWorkflow(this.Id, homeHealthProfile);
                                var hhWorkItems = hhWorkFlow.Works;
                                if (hhWorkItems.IsNotNullOrEmpty())
                                {
                                    serviceChanged++;
                                    patient.HomeHealthStatus = (int)ReferralStatus.NonAdmission;
                                    work.AddRange(hhWorkItems);
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    return;
                                }
                            }
                        }
                        if (ServiceProvided.Contains(AgencyServices.PrivateDuty.ToString()))
                        {
                            var privateDutyProfile = Profiles.Where(p => p.ServiceType == (int)AgencyServices.PrivateDuty).FirstOrDefault();
                            if (privateDutyProfile != null)
                            {
                                var pdWorkFlow = new PrivateDutyNonAdmitWorkflow(this.Id, privateDutyProfile);
                                var pdWorkItems = pdWorkFlow.Works;
                                if (pdWorkItems.IsNotNullOrEmpty())
                                {
                                    serviceChanged++;
                                    patient.PrivateDutyStatus = (int)ReferralStatus.NonAdmission;
                                    work.AddRange(pdWorkItems);
                                }
                                else
                                {
                                    this.isCommitted = false;
                                    return;
                                }
                            }
                            else
                            {
                                this.isCommitted = false;
                                return;
                            }
                        }
                        if (serviceCount > 0 && serviceCount == serviceChanged)
                        {
                        }
                        else
                        {
                            this.isCommitted = false;
                            return;
                        }
                        // Insert the patient on zero index
                        work.Insert(0, () => { return patientService.UpdateModal(patient); },
                            () => {
                                patient.HomeHealthStatus = oldHHStatus;
                                patient.PrivateDutyStatus = oldPDStatus;
                                patientService.UpdateModal(patient);
                            },
                            "System could not save the Non-admit information.");


                        work.Perform();
                    }
                    else
                    {
                        this.isCommitted = false;
                        this.message = "Patient doesn't exists.";
                    }
                }
                else
                {
                    this.isCommitted = false;
                    this.message = Patient.ValidationMessage;
                }
            }
        }
    }
    internal class HHNonAdmitWorkflow
    {
         private Guid Id { get; set; }
         private NonAdmit Profile { get; set; }
         public List<WorkItem> works { get; set; }
         public List<WorkItem> Works { get { return works; } }
         private string message { get; set; }
         public string Message { get { return message; } }
         private readonly HHPatientProfileService profileService;
         public HHNonAdmitWorkflow(Guid Id, NonAdmit profile)
         {
             Check.Argument.IsNotNull(Id, "Id");

             this.Id = Id;
             this.Profile = profile;
             this.works = new List<WorkItem>();
             this.profileService = Container.Resolve<HHPatientProfileService>();
             this.AddWorkItems();
         }

         private void AddWorkItems()
         {
             var profile = profileService.GetProfileOnly(this.Id);
             if (profile != null)
             {
                 var oldNonAdmissionDate = profile.NonAdmissionDate;
                 var oldNonAdmissionReason = profile.NonAdmissionReason;
                 var oldComments = profile.Comments;
                 var oldStaus = profile.Status;

                 profile.NonAdmissionDate = this.Profile.NonAdmitDate;
                 profile.NonAdmissionReason = this.Profile.Reason;
                 profile.Comments = this.Profile.Comments;
                 profile.Status = (int)PatientStatus.NonAdmission;

                 works.Add(new WorkItem(() => profileService.UpdateModal(profile),
                     () =>
                     {
                         profile.NonAdmissionDate = oldNonAdmissionDate;
                         profile.NonAdmissionReason = oldNonAdmissionReason;
                         profile.Comments = oldComments;
                         profile.Status = oldStaus;
                         profileService.UpdateModal(profile);
                     },
                     "System could not Non-admit Home Health patient."));

             }
             else
             {

             }
         }
    }

    internal class PrivateDutyNonAdmitWorkflow
    {

         private Guid Id { get; set; }
         private NonAdmit Profile { get; set; }
         public List<WorkItem> works { get; set; }
         public List<WorkItem> Works { get { return works; } }
         private string message { get; set; }
         public string Message { get { return message; } }
         private readonly PrivateDutyPatientProfileService profileService;
         public PrivateDutyNonAdmitWorkflow(Guid Id, NonAdmit profile)
         {
             Check.Argument.IsNotNull(Id, "Id");

             this.Id = Id;
             this.Profile = profile;
             this.works = new List<WorkItem>();
             this.profileService = Container.Resolve<PrivateDutyPatientProfileService>();
             this.AddWorkItems();
         }
         private void AddWorkItems()
         {
             var profile = profileService.GetProfileOnly(this.Id);
             if (profile != null)
             {
                 var oldNonAdmissionDate = profile.NonAdmissionDate;
                 var oldNonAdmissionReason = profile.NonAdmissionReason;
                 var oldComments = profile.Comments;
                 var oldStaus = profile.Status;

                 profile.NonAdmissionDate = this.Profile.NonAdmitDate;
                 profile.NonAdmissionReason = this.Profile.Reason;
                 profile.Comments = this.Profile.Comments;
                 profile.Status = (int)PatientStatus.NonAdmission;

                 works.Add(new WorkItem(() => profileService.UpdateModal(profile),
                     () =>
                     {
                         profile.NonAdmissionDate = oldNonAdmissionDate;
                         profile.NonAdmissionReason = oldNonAdmissionReason;
                         profile.Comments = oldComments;
                         profile.Status = oldStaus;
                         profileService.UpdateModal(profile);
                     },
                     "System could not Non-admit Private Duty patient."));

             }
             else
             {

             }
         }

    }
}
