﻿//using Axxess.Core;
//using Axxess.Core.Extension;
//using Axxess.Core.Infrastructure;
//using Axxess.AgencyManagement.Application.Services;
//using Axxess.AgencyManagement.Entities.Repositories;
//using Axxess.AgencyManagement.Entities;
//using Axxess.AgencyManagement.Application.Helpers;

//namespace Axxess.AgencyManagement.Application.Workflows
//{
//    public class PendingPatientWorkFlow : IWorkflow
//    {
//       #region PendingPatientWorkFlow Members

//        private Patient patient { get; set; }

//        private readonly IPatientService patientService;
//        public PendingPatientWorkFlow(Patient patient)
//        {
//            Check.Argument.IsNotNull(patient, "patient");

//            this.patient = patient;

//            this.patientService = Container.Resolve<IPatientService>();
//            this.Process();
//        }

//        #endregion

//        #region IWorkflow Members

//        private string message { get; set; }
//        public string Message { get { return message; } }

//        private bool isCommitted { get; set; }
//        public bool IsCommitted { get { return isCommitted; } }

//        public void Process()
//        {
//            var work = new WorkSequence();
//            work.Complete += (sequence) =>
//            {
//                this.isCommitted = this.message.IsNullOrEmpty();
//            };

//            work.Error += (sequence, item, index) =>
//            {
//                this.isCommitted = false;
//                this.message = item.Description;
//            };
//            work.Add(
//                () =>
//                {
//                    patient.AgencyId = Current.AgencyId;
//                    return patientService.AddPatient(patient);
//                },
//                () =>
//                {
//                    patientService.RemovePatient(patient.Id);
//                     AdmissionHelperFactory<PatientAdmissionDate>.RemovePatientAdmissionDates( patient.Id);
//                },
//                "System could not save the Patient information.");
//            if (patient.EmergencyContact != null && patient.EmergencyContact.PhonePrimaryArray != null && patient.EmergencyContact.PhonePrimaryArray.Count >= 2)
//            {
//                patient.EmergencyContact.PrimaryPhone = patient.EmergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
//            }
//            if (patient.EmergencyContact.PhoneAlternateArray != null && patient.EmergencyContact.PhoneAlternateArray.Count >= 2)
//            {
//                patient.EmergencyContact.AlternatePhone = patient.EmergencyContact.PhoneAlternateArray.ToArray().PhoneEncode();
//            }
//            if (patient.EmergencyContact.FirstName.IsNotNullOrEmpty() && patient.EmergencyContact.LastName.IsNotNullOrEmpty() && patient.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty())
//            {
//                work.Add(
//                    () =>
//                    {
//                        patient.EmergencyContact.PatientId = patient.Id;
//                        patient.EmergencyContact.AgencyId = Current.AgencyId;
//                        return patientService.AddPrimaryEmergencyContact(patient);
//                    },
//                    () =>
//                    {
//                        patientService.RemoveEmergencyContacts(patient.Id);
//                    },
//                    "System could not save the Emergency Contact information.");
//            }

//            work.Add(() => { return patientService.LinkPhysicians(patient); },
//                     () => { patientService.UnlinkAll(patient.Id); },
//                     "System could not link the Patient to the Physician.");

          
//            work.Perform();
//        }

//        #endregion
//    }
//}
