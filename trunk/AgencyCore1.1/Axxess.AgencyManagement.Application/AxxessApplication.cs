﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Security;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Application;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using System.Diagnostics;
    using System.Web.SessionState;
    using Axxess.AgencyManagement.Application.Controllers;

    public class AxxessApplication : HttpApplication
    {
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        public override void Init()
        {
            base.Init();
            this.AcquireRequestState += new EventHandler(AxxessApplication_AcquireRequestState);
        }

        protected void AxxessApplication_AcquireRequestState(object sender, EventArgs e)
        {
            if (Context.Handler is MvcHandler 
               && Context.User != null 
               && Context.User.Identity.IsAuthenticated)
            {
                IMembershipService membershipService = Container.Resolve<IMembershipService>();
                IFormsAuthenticationService formAuthService = Container.Resolve<IFormsAuthenticationService>();

                var userName = Context.User.Identity.Name;
                AxxessPrincipal principal = membershipService.Get(userName);

                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;

                    if (principal.IsImpersonated())
                    {
                        return;
                    }
                    else
                    {
                        if (AppSettings.IsSingleUserMode)
                        {
                            if (authenticationAgent.Verify(principal.ToSingleUser()))
                            {
                                if (Current.CanContinue)
                                {
                                    return;
                                }
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                membershipService.LogOff(userName);
                formAuthService.SignOut();
                formAuthService.RedirectToLogin();
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();

            ModelBinders.Binders.Add(typeof(DateTime), new DateAndTimeModelBinder() { Date = "Date", Time = "Time" });

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new MobileCapableWebFormViewEngine());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                Logger.Exception(exception);

                if (exception.Message == "FileUploadMoreThanMaxSize")
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.TrySkipIisCustomErrors = true;
                    Response.StatusCode = 200;
                    Response.ContentType = "application/json";
                    Response.StatusDescription = "Error in file size";
                    Response.AppendHeader("Content-Disposition", "alert");
                    JsonViewData viewData = new JsonViewData() { isSuccessful = false, errorMessage = "An attachment is larger than the maximum allowed file size. The maximum allowed file size is 10 MBs(Mega Bytes)." };
                    Response.Write("<SCRIPT type=\"text/javascript\"> U.Growl('" + viewData.errorMessage + "','error');</SCRIPT>");
                    Response.End();
                }
                else if (exception is HttpRequestValidationException)
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.TrySkipIisCustomErrors = true;
                    Response.StatusCode = 400;
                    Response.ContentType = "application/json";
                    Response.StatusDescription = "A potentially dangerous request was detected";
                    Response.AppendHeader("Content-Disposition", "alert");
                    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The form contains potentially dangerous values. Ensure that special characters such as #, $, <, &, etc. are not next to each other." };
                    Response.Write(viewData.ToJson());
                    Response.End();
                }
                else 
                {
                    Console.WriteLine();
#if !DEBUG
                    var routeData = new RouteData();
                    var httpException = exception as HttpException;
                    if (httpException != null)
                    {
                        routeData.Values["controller"] = "Error";
                        routeData.Values["error"] = exception.Message;

                        Response.StatusCode = httpException.GetHttpCode();
                        switch (Response.StatusCode)
                        {
                            case 401:
                                routeData.Values["action"] = "NotAuthorized";
                                break;
                            case 403:
                                routeData.Values["action"] = "Forbidden";
                                break;
                            case 404:
                                routeData.Values["action"] = "FileNotFound";
                                break;
                            default:
                                Logger.Exception(exception);
                                routeData.Values["action"] = "ApplicationError";
                                break;
                        }
                        Response.Clear();
                        Server.ClearError();
                        Response.TrySkipIisCustomErrors = false;
                        IController errorController = new ErrorController();
                        HttpContextWrapper wrapper = new HttpContextWrapper(Context);
                        var requestContext = new RequestContext(wrapper, routeData);
                        errorController.Execute(requestContext);
                    }
#endif
                }
            }
        }


        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (context.Request.IsAuthenticated && custom == "Agency")
            {
                return Current.AgencyId.ToString();
            }
            else
            {
                return base.GetVaryByCustomString(context, custom);
            }
        }
    }
}
