﻿namespace Axxess.AgencyManagement.Application.Security
{
    using System;
    using System.Security.Principal;
    using System.Collections.Generic;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    
    [Serializable]
    public class AxxessPrincipal : IPrincipal
    {
        #region Private Members

        private Roles roleId;
        private AxxessIdentity identity;
       // private Permissions permissions;

        private IDictionary<int, Dictionary<int, List<int>>> newPermissions;

        #endregion

        #region Constructor

        public AxxessPrincipal(AxxessIdentity identity)
            : this(identity, Roles.ApplicationUser,  new Dictionary<int, Dictionary<int, List<int>>>())
        {
        }

        public AxxessPrincipal(AxxessIdentity identity, Roles roleId)
            : this(identity, roleId,  new Dictionary<int, Dictionary<int, List<int>>>())
        {
        }

        public AxxessPrincipal(AxxessIdentity identity, IDictionary<int, Dictionary<int, List<int>>> newRights)
            : this(identity, Roles.ApplicationUser,newRights)
        {
        }

        public AxxessPrincipal(AxxessIdentity identity, Roles roleId,  IDictionary<int, Dictionary<int, List<int>>> newRights)
        {
            this.roleId = roleId;
            this.identity = identity;

            //rights.ForEach(i => {
            //    this.permissions |= (Permissions)Enum.Parse(typeof(Permissions), i);
            //});
            newPermissions = newRights;
        }

        #endregion

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return identity; }
        }

        public bool IsInRole(Roles roleId)
        {
            return this.roleId == roleId;
        }

        public bool IsInRole(string role)
        {
            return (Roles)Enum.Parse(typeof(Roles), role, true) == this.roleId;
        }

        //public bool HasPermission(Permissions permission)
        //{
        //    return this.permissions.Has(permission);
        //}

        public bool HasPermission(AgencyServices service, ParentPermission category, PermissionActions action)
        {
            var result = false;
            if ((this.identity.Session.AcessibleServices & service) == service)
            {
                var categoryList = new Dictionary<int, List<int>>();
                if (this.newPermissions.TryGetValue((int)category, out categoryList))
                {
                    List<int> exstingService ;
                    if (categoryList.TryGetValue((int)action, out exstingService))
                    {
                        var comingService = (int)service;
                        return exstingService != null && exstingService.Exists(s => (comingService & s) == s); 
                    }
                }
            }
            return result;
        }

        #endregion
    }
}
