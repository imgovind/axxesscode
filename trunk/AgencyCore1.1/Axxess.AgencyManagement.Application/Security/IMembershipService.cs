﻿namespace Axxess.AgencyManagement.Application.Security
{
    using System;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;

    public interface IMembershipService
    {
        void LogOff(string userName);

        //bool Deactivate(Guid userId);
        bool Activate(Account account);

        AxxessPrincipal Get(string userName);
        AxxessPrincipal Get(Guid userId, Guid agencyId, Login login,string bdIp);

        bool Login(Account account);
        LoginAttemptType Validate(Account account);
        ResetAttemptType Validate(string userName);

        bool Impersonate(Guid linkId);

        bool ResetPassword(string userName, string baseUrl);
        bool UpdatePassword(Account account);

        bool ResetSignature(Guid loginId);
        bool UpdateSignature(Account account);

        bool InitializeWith(Account account);

        bool Switch(Guid loginId);

        bool FacebookLogin(string accessToken);

    }
}
