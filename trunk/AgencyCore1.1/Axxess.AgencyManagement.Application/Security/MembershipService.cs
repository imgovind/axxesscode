﻿namespace Axxess.AgencyManagement.Application.Security
{
    using System;
    using System.Web;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Api;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Log.Repositories;

    public class MembershipService : IMembershipService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ISupportRepository supportRepository;
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        #endregion

        #region Constructor

        public MembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region IMembershipService Members

        public AxxessPrincipal Get(string userName)
        {
            ImpersonationLink link = null;
            AxxessPrincipal principal = null;
            if (userName.IsNotNullOrEmpty())
            {
                principal = Cacher.Get<AxxessPrincipal>(userName);
                if (principal == null)
                {
                    string[] userNameArray = userName.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    if (userNameArray != null && userNameArray.Length > 1)
                    {
                        userName = userNameArray[0];
                        link = supportRepository.GetImpersonationLink(userNameArray[1].ToGuid());
                    }

                    Login login = loginRepository.Find(userName);
                    if (login != null)
                    {
                        var accounts = loginRepository.GetAllLoginAgencyUsers(login.Id); 
                        if (accounts != null)
                        {
                            if (accounts.Count == 1)
                            {
                                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                                if (role == Roles.ApplicationUser)
                                {
                                    var user = accounts.FirstOrDefault();
                                    if (user != null)
                                    {
                                        var agencySnapShot = loginRepository.GetAgencySnapShot(user.AgencyId);
                                        if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                                        {
                                            if (link == null)
                                            {
                                                principal = Get(user.UserId, user.AgencyId, login, agencySnapShot.DBServerIp);
                                            }
                                            else
                                            {
                                                principal = Get(user.UserId, user.AgencyId, link.Id, login, agencySnapShot.DBServerIp);
                                            }
                                        }
                                    }
                                }
                            }
                            else if (accounts.Count > 1)
                            {
                                var request = HttpContext.Current.Request;
                                if ((request.Cookies.Count > 0) && (request.Cookies[AppSettings.IdentifyAgencyCookie] != null))
                                {
                                    var cookie = request.Cookies[AppSettings.IdentifyAgencyCookie];
                                    string info = Crypto.Decrypt(cookie.Value);
                                    if (info != null)
                                    {
                                        var infoArray = info.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (infoArray != null && infoArray.Length == 2)
                                        {
                                            var userId = infoArray[0].IsGuid() ? infoArray[0].ToGuid() : Guid.Empty;
                                            var agencyId = infoArray[1].IsGuid() ? infoArray[1].ToGuid() : Guid.Empty;
                                            if (!userId.IsEmpty() && !agencyId.IsEmpty())
                                            {
                                                var agencySnapShot = loginRepository.GetAgencySnapShot(agencyId);
                                                if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                                                {
                                                    if (link == null)
                                                    {
                                                        principal = Get(userId, agencyId, login, agencySnapShot.DBServerIp);
                                                    }
                                                    else
                                                    {
                                                        principal = Get(userId, agencyId, link.Id, login, agencySnapShot.DBServerIp);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return principal;
        }

        public AxxessPrincipal Get(Guid userId, Guid agencyId, Login login, string bdIp)
        {
            AxxessPrincipal principal = null;
            var user = userRepository.GetUserOnly(agencyId, userId);
            if (user != null)
            {
                if (login != null)
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                    if (role == Roles.ApplicationUser)
                    {
                        var agency = agencyRepository.GetAgencyOnly(user.AgencyId);
                        if (agency != null)
                        {
                            principal = this.GetPrincipalHelper(agency, user, login, role, false, null, bdIp);
                        }
                    }
                }
            }
            return principal;
        }

        private AxxessPrincipal GetPrincipalHelper(Agency agency, User user, Login login, Roles role, bool isLink, ImpersonationLink link, string dbIp)
        {
            AxxessPrincipal principal = null;
            if (user != null)
            {
                if (login != null)
                {
                    if (role == Roles.ApplicationUser)
                    {
                        if (agency != null)
                        {
                            AxxessIdentity identity = new AxxessIdentity(login.Id, isLink && link != null ? string.Format("{0}:{1}", login.EmailAddress, link.Id) : login.EmailAddress);
                            var acessibleServices = user.AccessibleServices & agency.Services;
                            //var formattedReportPermissions = UserSessionHelper.GetPermission(user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>(), acessibleServices);
                            var userSessionId = isLink && link != null ? string.Format("{0}:{1}", login.EmailAddress, link.Id) : login.EmailAddress;
                            var session = new UserSession
                                {
                                    UserId = user.Id,
                                    LoginId = login.Id,
                                    FullName = user.DisplayName,
                                    DisplayName = login.DisplayName,
                                    AgencyId = agency.Id,
                                    AgencyName = agency.Name,
                                    SessionId = userSessionId,
                                    IsPrimary = user.IsPrimary,
                                    IsAgencyFrozen = agency.IsFrozen,
                                    Services = agency.Services,
                                    AcessibleServices = acessibleServices,
                                    PreferredService = user.PreferredService,
                                    LoginDay = DateTime.Today.Day,
                                    AccountExpirationDate = user.AccountExpireDate,
                                    AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty,
                                    EarliestLoginTime = user.EarliestLoginTime.IsNotNullOrEmpty() ? user.EarliestLoginTime : string.Empty,
                                    AutomaticLogoutTime = user.AutomaticLogoutTime.IsNotNullOrEmpty() ? user.AutomaticLogoutTime : string.Empty,
                                    OasisVendorExist = agency.OasisAuditVendorApiKey.IsNotNullOrEmpty(),
                                    DBServerIp = dbIp,
                                    Payor = agency.Payor,
                                    Permissions = UserSessionHelper.GetPermission(user.NewPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>(), acessibleServices, user.IsReportCenterVisbile)
                                    //,ReportPermissions = formattedReportPermissions
                                };

                            identity.Session = session;
                            if (isLink && link != null)
                            {
                                identity.IsImpersonated = true;
                                identity.Session.ImpersonatorName = link.RepName;
                            }
                            principal = new AxxessPrincipal(identity, role, session.Permissions);
                            Cacher.Set<AxxessPrincipal>(userSessionId, principal);
                            UserSessionEngine.AddUser(user, acessibleServices, userSessionId);
                        }
                    }
                }
            }
            return principal;
        }

        private AxxessPrincipal Get(Guid userId, Guid agencyId, Guid linkId, Login login, string dbIp)
        {
            AxxessPrincipal principal = null;
            if (login != null)
            {
                var user = userRepository.GetUserOnly(agencyId, userId);
                if (user != null)
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                    if (role == Roles.ApplicationUser)
                    {
                        var agency = agencyRepository.GetAgencyOnly(user.AgencyId);
                        if (agency != null)
                        {
                            var link = supportRepository.GetImpersonationLink(linkId);
                            if (link != null)
                            {
                                principal = this.GetPrincipalHelper(agency, user, login, role, true, link, dbIp);
                            }
                        }
                    }
                }
            }
            return principal;
        }

        public bool Login(Account account)
        {
            var result = false;
            var login = loginRepository.Find(account.UserName);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(account.Password, login.PasswordHash, login.PasswordSalt))
                {
                    account.LoginId = login.Id;
                    account.EmailAddress = login.EmailAddress;

                    Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                    if (role == Roles.ApplicationUser)
                    {
                        var loginUserAgencies = loginRepository.GetAllLoginAgencyUsers(login.Id);
                        if (loginUserAgencies != null)
                        {
                            var loginUserAgencyCount = loginUserAgencies.Count;
                            if (loginUserAgencyCount == 1)
                            {
                                var agencyUser = loginUserAgencies.FirstOrDefault();
                                if (agencyUser != null)
                                {
                                    var agency = loginRepository.GetAgencySnapShot(agencyUser.AgencyId);
                                    if (agency != null && agency.DBServerIp.IsNotNullOrEmpty() && agency.DBServerIp.IsValidIp())
                                    {
                                        account.UserId = agencyUser.UserId;
                                        account.AgencyId = agencyUser.AgencyId;

                                        var principal = Get(agencyUser.UserId, agencyUser.AgencyId, login, agency.DBServerIp);
                                        if (principal != null)
                                        {
                                            Thread.CurrentPrincipal = principal;
                                            HttpContext.Current.User = principal;

                                            login.LastLoginDate = DateTime.Now;
                                            if (loginRepository.Update(login))
                                            {
                                                result = true;
                                            }
                                        }
                                    }
                                }
                            }
                            else if (loginUserAgencyCount > 1)
                            {
                                account.HasMultiple = true;
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public LoginAttemptType Validate(Account account)
        {
            var loginAttempt = LoginAttemptType.Failed;

            var login = loginRepository.Find(account.UserName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    loginAttempt = LoginAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var canContinue = true;
                        var principal = Get(login.EmailAddress);
                        if (AppSettings.IsSingleUserMode)
                        {
                            canContinue = authenticationAgent.Verify(principal.ToSingleUser());
                        }

                        if (canContinue)
                        {
                            var user = userRepository.GetUserOnly(account.AgencyId, account.UserId);
                            if (user != null)
                            {
                                if (user.Status == (int)UserStatus.Active)
                                {
                                    if (!user.HasTrialAccountExpired())
                                    {
                                        if (user.AllowWeekendAccess())
                                        {
                                            if (user.EarliestAccessible())
                                            {
                                                var agency = agencyRepository.GetAgencyOnly(user.AgencyId);
                                                if (agency != null)
                                                {
                                                    bool isTwoYearsAgo = DateTime.Now.AddYears(-2) >= agency.FrozenDate;
                                                    if (!agency.IsFrozen || (user.IsPrimary && !isTwoYearsAgo))
                                                    {
                                                        if (!agency.IsSuspended)
                                                        {
                                                            if (!agency.HasTrialPeriodExpired())
                                                            {
                                                                loginAttempt = LoginAttemptType.Success;
                                                            }
                                                            else
                                                            {
                                                                loginAttempt = LoginAttemptType.TrialPeriodOver;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            loginAttempt = LoginAttemptType.AccountSuspended;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        loginAttempt = LoginAttemptType.AgencyFrozen;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                loginAttempt = LoginAttemptType.TooEarly;
                                            }
                                        }
                                        else
                                        {
                                            loginAttempt = LoginAttemptType.WeekendAccessRestricted;
                                        }
                                    }
                                    else
                                    {
                                        loginAttempt = LoginAttemptType.TrialAccountExpired;
                                    }
                                }
                                else
                                {
                                    loginAttempt = LoginAttemptType.Deactivated;
                                }
                            }
                        }
                        else
                        {
                            loginAttempt = LoginAttemptType.AccountInUse;
                        }
                    }
                    else
                    {
                        loginAttempt = LoginAttemptType.Deactivated;
                    }
                }
            }

            return loginAttempt;
        }

        public ResetAttemptType Validate(string userName)
        {
            var resetAttempt = ResetAttemptType.Failed;

            var login = loginRepository.Find(userName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    resetAttempt = ResetAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var accounts = loginRepository.GetAllLoginAgencyUsers(login.Id); // var accounts = userRepository.GetUsersByLoginId(login.Id);
                        if (accounts != null && accounts.Count > 0)
                        {
                            var activeAccount = accounts.Where(a => a.Status == (int)UserStatus.Active).FirstOrDefault();
                            if (activeAccount != null)
                            {
                                resetAttempt = ResetAttemptType.Success;
                            }
                            else
                            {
                                resetAttempt = ResetAttemptType.Deactivated;
                            }
                        }
                    }
                    else
                    {
                        resetAttempt = ResetAttemptType.Deactivated;
                    }
                }
            }

            return resetAttempt;
        }

        public bool Impersonate(Guid linkId)
        {
            var result = false;

            var link = supportRepository.GetImpersonationLink(linkId);
            if (link != null && link.IsUsed == false && DateTime.Now < link.Created.AddMinutes(1))
            {
                var login = loginRepository.Find(link.LoginId);
                if (login != null)
                {
                    var agencySnapShot = loginRepository.GetAgencySnapShot(link.AgencyId);
                    if (agencySnapShot != null && agencySnapShot.DBServerIp.IsNotNullOrEmpty() && agencySnapShot.DBServerIp.IsValidIp())
                    {
                        var user = userRepository.GetUserOnly(link.AgencyId, link.UserId);
                        if (user != null)
                        {
                            var agency = agencyRepository.GetAgencyOnly(link.AgencyId);
                            if (agency != null)
                            {
                                var permissionsArray = new List<string>();
                                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                                if (role == Roles.ApplicationUser)
                                {
                                    var principal = this.GetPrincipalHelper(agency, user, login, role, true, link, agencySnapShot.DBServerIp);
                                    if (principal != null)
                                    {
                                        Thread.CurrentPrincipal = principal;
                                        HttpContext.Current.User = principal;
                                    }
                                }
                                link.IsUsed = true;
                                if (supportRepository.UpdateImpersonationLink(link))
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ResetPassword(string userName, string baseUrl)
        {
            var login = loginRepository.Find(userName);
            if (login != null && login.IsAxxessAdmin == false && login.IsAxxessSupport == false)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=password", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "PasswordResetInstructions",
                    "firstname", login.DisplayName,
                    "baseurl", baseUrl,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, userName, "Reset Password - Axxess Home Health Management Software", bodyText);
                return true;
            }
            return false;
        }

        public bool UpdatePassword(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newPasswordSalt = string.Empty;
                string newPasswordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out newPasswordHash, out newPasswordSalt);
                login.PasswordSalt = newPasswordSalt;
                login.PasswordHash = newPasswordHash;
                if (loginRepository.Update(login))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ResetSignature(Guid loginId)
        {
            var login = loginRepository.Find(loginId);
            if (login != null)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=signature", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "SignatureResetInstructions",
                    "firstname", login.DisplayName,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Reset Signature - Axxess Home Health Management Software", bodyText);
                return true;
            }

            return false;
        }

        public bool UpdateSignature(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newSignatureSalt = string.Empty;
                string newSignatureHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Signature, out newSignatureHash, out newSignatureSalt);
                login.SignatureSalt = newSignatureSalt;
                login.SignatureHash = newSignatureHash;

                if (loginRepository.Update(login))
                {
                    return true;
                }
            }
            return false;
        }

        //public bool Deactivate(Guid userId)
        //{
        //    var user = userRepository.Get(userId, Current.AgencyId);
        //    if (user != null)
        //    {
        //        var login = loginRepository.Find(user.LoginId);
        //        if (login != null)
        //        {
        //            login.IsActive = false;
        //            if (loginRepository.Update(login))
        //            {
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public bool Activate(Account account)
        {
            var agencyUser = loginRepository.GetLoginAgencyUser(account.AgencyId, account.UserId);//var user = userRepository.Get(account.UserId, account.AgencyId);
            if (agencyUser != null)
            {
                var login = loginRepository.Find(agencyUser.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    string passwordSalt = string.Empty;
                    string passwordHash = string.Empty;

                    saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                    login.PasswordSalt = passwordSalt;
                    login.SignatureSalt = passwordSalt;
                    login.PasswordHash = passwordHash;
                    login.SignatureHash = passwordHash;
                    login.LastLoginDate = DateTime.Now;

                    if (loginRepository.Update(login))
                    {
                        AxxessPrincipal principal = Get(login.EmailAddress);
                        if (principal != null)
                        {
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool InitializeWith(Account account)
        {
            var agencyUser = loginRepository.GetLoginAgencyUser(account.AgencyId, account.UserId); //  var user = userRepository.Get(account.UserId, account.AgencyId);
            if (agencyUser != null)
            {
                var login = loginRepository.Find(agencyUser.LoginId);
                if (login != null)
                {
                    var agency = loginRepository.GetAgencySnapShot(agencyUser.AgencyId);
                    if (agency != null && agency.DBServerIp.IsNotNullOrEmpty() && agency.DBServerIp.IsValidIp())
                    {
                        //Container.Inject<IAgencyManagementDataProvider>(new AgencyManagementDataProvider(agency.DBServerIp));
                        //Container.Inject<IOasisCDataProvider>(new OasisCDataProvider(agency.DBServerIp));
                        //Container.Inject<ILogDataProvider>(new LogDataProvider(agency.DBServerIp));

                        account.LoginId = login.Id;
                        account.UserName = login.EmailAddress;
                        account.EmailAddress = login.EmailAddress;

                        AxxessPrincipal principal = Get(account.UserId, account.AgencyId, login, agency.DBServerIp);
                        if (principal != null)
                        {
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                        }

                        var cookie = HttpContext.Current.Request.Cookies[AppSettings.IdentifyAgencyCookie];
                        if (cookie != null)
                        {
                            cookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                            cookie.Value = Crypto.Encrypt(string.Concat(account.UserId.ToString(), "_", agencyUser.AgencyId.ToString()));
                            HttpContext.Current.Response.Cookies.Set(cookie);
                        }
                        else
                        {
                            HttpCookie newCookie = new HttpCookie(AppSettings.IdentifyAgencyCookie);
                            newCookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                            newCookie.Value = Crypto.Encrypt(string.Concat(account.UserId.ToString(), "_", agencyUser.AgencyId.ToString()));
                            HttpContext.Current.Response.Cookies.Add(newCookie);
                        }

                        login.LastLoginDate = DateTime.Now;
                        if (loginRepository.Update(login))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public void LogOff(string emailAddress)
        {
            Cacher.Remove(emailAddress);
            //var principal = Get(emailAddress);
            if (!Current.IsImpersonated)
            {
                if (AppSettings.IsSingleUserMode)
                {
                    authenticationAgent.Logout(emailAddress);
                }
            }

            //var principal = Get(emailAddress);
            //if (principal != null && !principal.IsImpersonated())
            //{
            //    if (AppSettings.IsSingleUserMode)
            //    {
            //        authenticationAgent.Logout(emailAddress);
            //    }
            //}
        }

        public bool Switch(Guid loginId)
        {
            var result = false;
            var login = loginRepository.Find(loginId);
            if (login != null)
            {
                AxxessPrincipal principal = Get(login.EmailAddress);
                if (principal != null)
                {
                    authenticationAgent.Switch(loginId, principal.ToSingleUser());
                    result = true;
                }
            }

            return result;
        }

        public bool FacebookLogin(string accessToken)
        {
            return true;
        }

        #endregion
    }
}
