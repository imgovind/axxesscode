﻿namespace Axxess.AgencyManagement.Application.Security
{
    using System;
    using System.Web;
    using System.Web.Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        #region IFormsAuthenticationService Members

        public string LoginUrl
        {
            get
            {
                return FormsAuthentication.LoginUrl;
            }
        }

        public void SignIn(string userName, bool rememberMe)
        {
            FormsAuthentication.Initialize();

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, userName, DateTime.Now, 
                DateTime.Now.AddMinutes(AppSettings.FormsAuthenticationTimeoutInMinutes), 
                AppSettings.UsePersistentCookies, "");

            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

            if (rememberMe)
            {
                var cookie = HttpContext.Current.Request.Cookies[AppSettings.RememberMeCookie];
                if (cookie != null && userName.IsEqual(Crypto.Decrypt(cookie.Value)))
                {
                    cookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
                else
                {
                    HttpCookie newCookie = new HttpCookie(AppSettings.RememberMeCookie);
                    newCookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                    newCookie.Value = Crypto.Encrypt(userName);
                    HttpContext.Current.Response.Cookies.Add(newCookie);
                }
            }
            else
            {
                var cookie = HttpContext.Current.Request.Cookies[AppSettings.RememberMeCookie];
                if (cookie != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
            }
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        #endregion
    }
}
