﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class DeletedPatientExporter : BaseExporter
    {
        private IList<PatientData> patients;
        public DeletedPatientExporter(IList<PatientData> patients)
            : base()
        {
            this.patients = patients;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            ISheet sheet = base.workBook.CreateSheet("Patients");
           
            ICellStyle dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            IRow headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("DOB");
            headerRow.CreateCell(3).SetCellValue("Gender");
            headerRow.CreateCell(4).SetCellValue("Phone");
            headerRow.CreateCell(5).SetCellValue("Last Status");
            int count = 2;
            if (this.patients.Count > 0)
            {
                
                this.patients.ForEach(p =>
                {
                    IRow dataRow = sheet.CreateRow(count);
                    string patientDisplay = string.Format("{0}\nMRN: {1}", p.DisplayName, p.PatientIdNumber);
                    dataRow.CreateCell(0).SetCellValue(patientDisplay);
                    string insurnace = string.Format("{0}\n{1}", p.InsuranceName, p.PolicyNumber);
                    dataRow.CreateCell(1).SetCellValue(p.AddressFullFormatted);
                    if (p.DateOfBirth != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(p.DateOfBirth);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(3).SetCellValue(p.Gender);
                    string phone = p.Phone;
                    if (p.PhoneMobile.IsNotNullOrEmpty())
                    {
                        phone += (phone.IsNotNullOrEmpty() ? "\n" : string.Empty) + "Mobile:" + p.PhoneMobile;
                    }
                    dataRow.CreateCell(4).SetCellValue(phone);
                    dataRow.CreateCell(5).SetCellValue(p.Status);
                    count++;
                });
                var totalRow = sheet.CreateRow(count + 2);
                var totalCell = totalRow.CreateCell(0);
                totalCell.SetCellValue(string.Format("Total Number Of Patients: {0}", patients.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
