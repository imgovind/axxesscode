﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Api.Contracts;
    using System.Collections.Generic;

    public class OasisEntityValidator : IValidator
    {
        #region Constructor

        private OasisValidation[] validations;
        private List<ValidationError> validationErrors = new List<ValidationError>();

        public OasisEntityValidator(params OasisValidation[] validations)
        {
            this.validations = validations;
        }

        #endregion

        #region IValidator Members


        private bool isValid { get; set; }
        public bool IsValid { get { return isValid; } }

        public string Message
        {
            get { throw new NotImplementedException(); }
        }

        public List<ValidationError> ValidationError
        {
            get { return validationErrors; }
        }

        public void Validate()
        {
            var errors = new List<ValidationError>();

            foreach (OasisValidation validation in validations)
            {
                bool invalid = validation.Expression.Compile().Invoke();

                if (invalid)
                {
                    errors.Add(validation.ValidationError);
                }
            }

            if (errors.Count > 0)
            {
                this.isValid = false;
                this.validationErrors = errors;
            }
            else
            {
                this.isValid = true;
            }
        }

        #endregion
    }
}
