﻿namespace Axxess.AgencyManagement.Application.Exports {
    using System;
    using System.Collections.Generic;
    using Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PendingAdmissionsExporter : BaseExporter {
        private IList<PendingPatient> admissions;
        public PendingAdmissionsExporter(IList<PendingPatient> admissions) : base() {
            this.admissions = admissions;
        }

        protected override void InitializeExcel() {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Pending Admissions";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet() {
            var sheet = base.workBook.CreateSheet("Pending Admissions");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Pending Admissions");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Primary Insurance");
            headerRow.CreateCell(3).SetCellValue("Branch");
            headerRow.CreateCell(4).SetCellValue("Date Added");
            if (this.admissions.Count > 0) {
                int i = 2;
                this.admissions.ForEach(a => {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(a.DisplayName);
                    dataRow.CreateCell(2).SetCellValue(a.PrimaryInsuranceName);
                    dataRow.CreateCell(3).SetCellValue(a.Branch);
                    if (a.Created != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(a.Created);
                    }
                    else
                    {
                        dataRow.CreateCell(4).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Pending Admissions: {0}", admissions.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(5);
        }
    }
}
