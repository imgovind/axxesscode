﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
 
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;


    public class AdjustmentCodesExporter : BaseExporter
    {
        private IList<AgencyAdjustmentCode> codes;
        public AdjustmentCodesExporter(IList<AgencyAdjustmentCode> codes)
            : base()
        {
            this.codes = codes;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Agency Adjustment Codes";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("AgencyAdjustmentCodes");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Adjustment Codes");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Code");
            headerRow.CreateCell(1).SetCellValue("Description");
            headerRow.CreateCell(2).SetCellValue("Created Date");
            headerRow.CreateCell(3).SetCellValue("Modified Date");

            if (this.codes.Count > 0)
            {
                int i = 2;
                this.codes.ForEach(code =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(code.Code);
                    dataRow.CreateCell(1).SetCellValue(code.Description);
                    if (code.Created != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(code.Created);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    if (code.Modified != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(code.Modified);
                    }
                    else
                    {
                        dataRow.CreateCell(3).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agency Adjustment Codes: {0}", codes.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(4);
           
        }
    }
}
