﻿namespace Axxess.AgencyManagement.Application.Exports
{

    using System.Collections.Generic;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;


    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;

    public class UserScheduleExporter : BaseExporter
    {
        private IList<UserVisit> userVisit;
        public UserScheduleExporter(IList<UserVisit> userVisit)
            : base()
        {
            this.userVisit = userVisit;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - User Schedule Tasks";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("UserScheduleTasks");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("User Schedule Tasks");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Status");

            if (this.userVisit.Count > 0)
            {
                int i = 2;
                this.userVisit.ForEach(u =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.PatientName);
                    dataRow.CreateCell(1).SetCellValue(u.TaskName.ToTitleCase());
                    DateTime visitDate = u.VisitDate;
                    if (visitDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visitDate);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(3).SetCellValue(u.StatusName);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of User Schedule Tasks: {0}", userVisit.Count));
            }

            workBook.FinishWritingToExcelSpreadsheet(4);
        }
    }
}
