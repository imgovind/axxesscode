﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using Axxess.Api;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Application.Extensions;

namespace Axxess.AgencyManagement.Application.Exports
{
    public class HHRGExporter : BaseExporter
    {

        #region Private Members and Constructor

        private Guid AgencyId;
        private string AgencyName;
        private Guid BranchId;
        private DateTime StartDate;
        private DateTime EndDate;

        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public HHRGExporter(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, String agencyName)
            : base()
        {
            AgencyId = agencyId;
            AgencyName = agencyName;
            BranchId = branchId;
            StartDate = startDate;
            EndDate = endDate;

            FormatType = Axxess.AgencyManagement.Application.Enums.ExportFormatType.XLS;
            this.FileName = "HHRG.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - HHRG";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("HHRG");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = base.workBook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var titleRow = sheet.CreateRow(0);

            var tCell1 = titleRow.CreateCell(0);
            tCell1.SetCellValue(AgencyName);

            var tCell2 = titleRow.CreateCell(1);
            tCell2.SetCellValue("HHRG");

            var tCell3 = titleRow.CreateCell(2);
            tCell3.SetCellValue("Effective Date: " + DateTime.Today.ToZeroFilled());

            var tCell4 = titleRow.CreateCell(3);
            tCell4.SetCellValue("Date Range: " + StartDate.ToZeroFilled() + " - " + EndDate.ToZeroFilled());

           
            var rowIndex = 1;
            var headerRow = sheet.CreateRow(rowIndex);

            List<Dictionary<string, string>> data = reportAgent.HHRGReport(AgencyId, BranchId, StartDate, EndDate);

            if (data != null && data.Count > 0)
            {
                var columnSize = 0;
                var colIndex = 0;
               
                var dictionary = data.FirstOrDefault();

               
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    var hCell = headerRow.CreateCell(colIndex);
                    hCell.SetCellValue(kvp.Key);
                    colIndex++;
                }
                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            var cell = dataRow.CreateCell(colIndex);
                            if(value.IsNotNullOrEmpty())
                            {
                                if (colIndex == 3 || colIndex == 7)
                                {
                                    if (value.IsValidDateAndNotMin())
                                    {
                                        cell.CellStyle = dateStyle;
                                        cell.SetCellValue(value.ToDateTime());
                                    }
                                    else
                                    {
                                        value = "";
                                    }
                                }
                                else if (colIndex == 11 || colIndex == 16)
                                {
                                    cell.CellStyle = currencyStyle;
                                    cell.SetCellValue(value.ToCurrency());
                                }
                                else if (colIndex == 8 || colIndex == 12 || colIndex == 13)
                                {
                                    if (value.IsInteger())
                                    {
                                        cell.SetCellValue(value.ToInteger());
                                    }
                                    else
                                    {
                                        value = "";
                                    }
                                }
                                else
                                {
                                    cell.SetCellValue(value);
                                }
                            }
                            if (value == "")
                            {
                                cell.SetCellValue(string.Empty);
                            }
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Episode Information found!");
                workBook.FinishWritingToExcelSpreadsheet(1);
            }
        }
        #endregion

    }
}
