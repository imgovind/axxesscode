﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.SS.UserModel;
    using NPOI.SS.Util;

    public abstract class ReportExcelGenerator
    {
       protected AgencyServices Service { get; set; }
       
       protected ReportExcelGenerator(AgencyServices Service)
       {
           this.Service = Service;
       }

       public  FileContentResult ExportPatientRoster(List<PatientRoster> patientRosters,Guid branchId, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate)
       {
           var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DirectorySettings.ExcelDirectory);
           var file = new FileStream(Path.Combine(path, "ReportTemplate.xls"), FileMode.Open, FileAccess.Read);
           var workbook = new HSSFWorkbook(file);
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Roster";
           workbook.SummaryInformation = si;
           var sheet = workbook.GetSheetAt(0);
           workbook.SetSheetName(0, "PatientRoster");

           Dictionary<string, string> titleHeaders = new Dictionary<string, string>();
           string patientTitle = string.Format("Patient Status: {0}", Enum.GetName(typeof(PatientStatus), StatusId));
           string effectiveDate = string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy"));

           string insuranceName = "All";
           if (InsuranceId != 0)
           {
               var ins = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
               if (ins != null)
               {
                   insuranceName = ins.Name;
               }
           }
           titleHeaders.Add("Date", DateTime.Today.ToString("MM/dd/yyyy"));
           titleHeaders.Add("Patient Status", Enum.GetName(typeof(PatientStatus), StatusId));
           titleHeaders.Add("Insurance", insuranceName);
           if (StartDate.HasValue && EndDate.HasValue && StartDate.Value.IsValid() && EndDate.Value.IsValid())
           {
               titleHeaders.Add("Active Date Range", string.Format("{0} - {1}", StartDate.Value.ToString("MM/dd/yyyy"), EndDate.Value.ToString("MM/dd/yyyy")));
           }

           string agencyLocationName = branchId.IsEmpty() ? "" : LocationEngine.GetName(branchId, Current.AgencyId);
           int rowNumber = workbook.WriteHeaderOfExcelSpreadsheet(Current.AgencyName, agencyLocationName, "Patient Roster Report", titleHeaders);

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           //var headerRow = sheet.CreateRow(6);
           ////headerRow.CreateCell(0).SetCellValue("MRN");
           //headerRow.CreateCell(0).SetCellValue("Patient");
           //headerRow.CreateCell(1).SetCellValue("Insurance");
           ////headerRow.CreateCell(2).SetCellValue("Policy #");
           //headerRow.CreateCell(2).SetCellValue("Address");
           ////headerRow.CreateCell(4).SetCellValue("City");
           ////headerRow.CreateCell(5).SetCellValue("State");
           ////headerRow.CreateCell(6).SetCellValue("Zip Code");
           ////headerRow.CreateCell(3).SetCellValue("Home Phone");
           //headerRow.CreateCell(3).SetCellValue("Gender");
           //headerRow.CreateCell(4).SetCellValue("Triage");
           //headerRow.CreateCell(5).SetCellValue("DOB");
           //headerRow.CreateCell(6).SetCellValue("SOC");
           //headerRow.CreateCell(7).SetCellValue("D/C");
           //headerRow.CreateCell(8).SetCellValue("Physician");
           //headerRow.CreateCell(10).SetCellValue("Physician NPI");
           //headerRow.CreateCell(16).SetCellValue("Physician Phone");
           int specialHeaderRowCount = rowNumber - 1;
           sheet.CreateFreezePane(0, specialHeaderRowCount + 1, 0, specialHeaderRowCount + 1);

           foreach (var patient in patientRosters)
           {
               var row = sheet.CreateRow(rowNumber++);
               //row.CreateCell(0).SetCellValue(patient.PatientId);
               string mrn = patient.PatientId;
               row.CreateCell(0).SetCellValue(mrn);
               string patientName = patient.PatientDisplayName;
               row.CreateCell(1).SetCellValue(patientName);
               string insurance = string.Format("{0}\n{1}", patient.PatientInsuranceName, patient.PatientPolicyNumber);
               row.CreateCell(2).SetCellValue(insurance);

               //row.CreateCell(3).SetCellValue(patient.PatientPolicyNumber);

               string address = string.Format("{0}\n{1}, {2} {3}\n{4}", patient.PatientAddressLine1, patient.PatientAddressCity,
                   patient.PatientAddressStateCode, patient.PatientAddressZipCode, patient.PatientPhone);
               row.CreateCell(3).SetCellValue(address);
               //row.CreateCell(5).SetCellValue(patient.PatientAddressCity);
               //row.CreateCell(6).SetCellValue(patient.PatientAddressStateCode);
               //row.CreateCell(7).SetCellValue(patient.PatientAddressZipCode);
               //row.CreateCell(3).SetCellValue(patient.PatientPhone);
               row.CreateCell(4).SetCellValue(patient.PatientGender);
               row.CreateCell(5).SetCellValue(patient.Triage);
               if (patient.PatientDOB != DateTime.MinValue)
               {
                   var createdDateCell = row.CreateCell(6);
                   createdDateCell.CellStyle = dateStyle;
                   createdDateCell.SetCellValue(patient.PatientDOB);
               }
               else
               {
                   row.CreateCell(6).SetCellValue(string.Empty);
               }
               if (patient.PatientSoC != DateTime.MinValue)
               {
                   var createdDateCell = row.CreateCell(7);
                   createdDateCell.CellStyle = dateStyle;
                   createdDateCell.SetCellValue(patient.PatientSoC);
               }
               else
               {
                   row.CreateCell(7).SetCellValue(string.Empty);
               }
               if (patient.PatientDischargeDate != DateTime.MinValue)
               {
                   var createdDateCell = row.CreateCell(8);
                   createdDateCell.CellStyle = dateStyle;
                   createdDateCell.SetCellValue(patient.PatientDischargeDate);
               }
               else
               {
                   row.CreateCell(8).SetCellValue(string.Empty);
               }
               string physician = string.Format("{0}\n{1}\n{2}", patient.PhysicianName, patient.PhysicianNpi, patient.PhysicianPhone);
               row.CreateCell(9).SetCellValue(physician);
               //row.CreateCell(15).SetCellValue(patient.PhysicianNpi);
               //row.CreateCell(16).SetCellValue(patient.PhysicianPhone);
           }
           var totalRow = sheet.CreateRow(rowNumber + 2);
           totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Patients: {0}", patientRosters.Count));

           workbook.FinishWritingToExcelSpreadsheet(9, specialHeaderRowCount);
           //workbook.SetPrintArea(0, "A2:I"+rowNumber);


           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientRoster_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientEmergencyList(List<EmergencyContactInfo> emergencyContactInfos)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Emergency List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientEmergencyList");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Emergency List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Triage");
           headerRow.CreateCell(3).SetCellValue("Contact");
           headerRow.CreateCell(4).SetCellValue("Relationship");
           headerRow.CreateCell(5).SetCellValue("Contact Phone");
           headerRow.CreateCell(6).SetCellValue("Contact E-mail");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (emergencyContactInfos != null && emergencyContactInfos.Count > 0)
           {
               int rowNumber = 2;
               foreach (var emergencyContactInfo in emergencyContactInfos)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(emergencyContactInfo.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(emergencyContactInfo.PatientName);
                   row.CreateCell(2).SetCellValue(emergencyContactInfo.Triage);
                   row.CreateCell(3).SetCellValue(emergencyContactInfo.ContactName);
                   row.CreateCell(4).SetCellValue(emergencyContactInfo.ContactRelation);
                   row.CreateCell(5).SetCellValue(emergencyContactInfo.ContactPhoneHome);
                   row.CreateCell(6).SetCellValue(emergencyContactInfo.ContactEmailAddress);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Emergency List: {0}", emergencyContactInfos.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientEmergencyList_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientBirthdayList(IList<Birthday> birthDays, int Month)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Birthday List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientBirthdayList");
           var titleRow = sheet.CreateRow(0);

           ICellStyle birthDateStyle = workbook.CreateCellStyle();
           birthDateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MMMM d");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Birthday List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (new DateTime(DateTime.Now.Year, Month, 1).ToString("MMMM"))));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Name");
           headerRow.CreateCell(2).SetCellValue("Age");
           headerRow.CreateCell(3).SetCellValue("Birth Day");
           headerRow.CreateCell(4).SetCellValue("Address First Row");
           headerRow.CreateCell(5).SetCellValue("Address Second Row");
           headerRow.CreateCell(6).SetCellValue("Home Phone");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (birthDays != null && birthDays.Count > 0)
           {
               int rowNumber = 2;
               foreach (var birthDay in birthDays)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(birthDay.IdNumber);
                   row.CreateCell(1).SetCellValue(birthDay.Name);
                   row.CreateCell(2).SetCellValue(birthDay.Age);

                   var birthDayCell = row.CreateCell(3);
                   birthDayCell.SetCellValue(birthDay.SortableDate);
                   birthDayCell.CellStyle = birthDateStyle;
                   row.CreateCell(4).SetCellValue(birthDay.AddressFirstRow);
                   row.CreateCell(5).SetCellValue(birthDay.AddressSecondRow);
                   row.CreateCell(6).SetCellValue(birthDay.PhoneHome.ToPhone());
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Birthday List: {0}", birthDays.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientBirthdayList_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientAddressList(List<AddressBookEntry> addressBookEntries)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Address List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientAddressList");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Address List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Name");
           headerRow.CreateCell(2).SetCellValue("Address");
           headerRow.CreateCell(3).SetCellValue("City");
           headerRow.CreateCell(4).SetCellValue("State");
           headerRow.CreateCell(5).SetCellValue("Zip Code");
           headerRow.CreateCell(6).SetCellValue("Home Phone");
           headerRow.CreateCell(7).SetCellValue("Mobile Phone");
           headerRow.CreateCell(8).SetCellValue("Email Address");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (addressBookEntries != null && addressBookEntries.Count > 0)
           {
               int rowNumber = 2;
               foreach (var addressBook in addressBookEntries)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(addressBook.IdNumber);
                   row.CreateCell(1).SetCellValue(addressBook.Name);
                   row.CreateCell(2).SetCellValue(addressBook.AddressFirstRow);
                   row.CreateCell(3).SetCellValue(addressBook.AddressCity);
                   row.CreateCell(4).SetCellValue(addressBook.AddressStateCode);
                   row.CreateCell(5).SetCellValue(addressBook.AddressZipCode);
                   row.CreateCell(6).SetCellValue(addressBook.PhoneHome);
                   row.CreateCell(7).SetCellValue(addressBook.PhoneMobile);
                   row.CreateCell(8).SetCellValue(addressBook.EmailAddress);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Address List: {0}", addressBookEntries.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(9);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientAddressList_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientByPhysicians(List<PatientRoster> patientRosters, Guid PhysicianId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patients By Physician";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientByPhysicians");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patients By Physician");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           var physician = PhysicianEngine.Get(PhysicianId, Current.AgencyId);
           if (physician != null)
           {
               titleRow.CreateCell(3).SetCellValue(string.Format("Physician : {0}", physician.DisplayName));
           }

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Patient");
           headerRow.CreateCell(1).SetCellValue("Address");
           headerRow.CreateCell(2).SetCellValue("City");
           headerRow.CreateCell(3).SetCellValue("State");
           headerRow.CreateCell(4).SetCellValue("Zip Code");
           headerRow.CreateCell(5).SetCellValue("Home Phone");
           headerRow.CreateCell(6).SetCellValue("Gender");

           if (patientRosters != null && patientRosters.Count > 0)
           {
               int rowNumber = 2;
               foreach (var patient in patientRosters)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(patient.PatientDisplayName);
                   row.CreateCell(1).SetCellValue(patient.PatientAddressLine1);
                   row.CreateCell(2).SetCellValue(patient.PatientAddressCity);
                   row.CreateCell(3).SetCellValue(patient.PatientAddressStateCode);
                   row.CreateCell(4).SetCellValue(patient.PatientAddressZipCode);
                   row.CreateCell(5).SetCellValue(patient.PatientPhone);
                   row.CreateCell(6).SetCellValue(patient.PatientGender);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patients By Physician: {0}", patientRosters.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientByPhysicians_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientSocCertPeriodListing(List<PatientSocCertPeriod> patientSocCertPeriods)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient SOC Cert. Period Listing";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientSocCertPeriodListing");
           var titleRow = sheet.CreateRow(0);

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient SOC Cert. Period Listing");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Last Name");
           headerRow.CreateCell(2).SetCellValue("First Name");
           headerRow.CreateCell(3).SetCellValue("Middle Initial");
           headerRow.CreateCell(4).SetCellValue("SOC Date");
           headerRow.CreateCell(5).SetCellValue("SOC Cert. Period");
           headerRow.CreateCell(6).SetCellValue("Physician Name");
           headerRow.CreateCell(7).SetCellValue("Employee");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (patientSocCertPeriods != null && patientSocCertPeriods.Count > 0)
           {
               int rowNumber = 2;
               foreach (var patientSocCertPeriod in patientSocCertPeriods)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(patientSocCertPeriod.PatientPatientID);
                   row.CreateCell(1).SetCellValue(patientSocCertPeriod.PatientLastName);
                   row.CreateCell(2).SetCellValue(patientSocCertPeriod.PatientFirstName);
                   row.CreateCell(3).SetCellValue(patientSocCertPeriod.PatientMiddleInitial);
                   if (patientSocCertPeriod.PatientSoC != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(patientSocCertPeriod.PatientSoC);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   row.CreateCell(5).SetCellValue(patientSocCertPeriod.SocCertPeriod);
                   row.CreateCell(6).SetCellValue(patientSocCertPeriod.PhysicianName);
                   row.CreateCell(7).SetCellValue(patientSocCertPeriod.respEmp);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient SOC Cert. Period Listing: {0}", patientSocCertPeriods.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(8);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientSocCertPeriodListing_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientByResponsibleEmployeeListing(List<PatientRoster> patientRosters, Guid UserId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient By Responsible Employee Listing";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientByResponsibleEmployeeListing");
           var titleRow = sheet.CreateRow(0);

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient By Responsible Employee Listing");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Employee : {0}", UserEngine.GetName(UserId, Current.AgencyId)));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Last Name");
           headerRow.CreateCell(2).SetCellValue("First Name");
           headerRow.CreateCell(3).SetCellValue("Middle Initial");
           headerRow.CreateCell(4).SetCellValue("Address");
           headerRow.CreateCell(5).SetCellValue("SOC Date");
           sheet.CreateFreezePane(0, 2, 0, 2);
        
           if (UserId.IsEmpty())
           {
           }
           else
           {
               if (patientRosters != null && patientRosters.Count > 0)
               {
                   int rowNumber = 2;
                   foreach (var patient in patientRosters)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(0).SetCellValue(patient.PatientId);
                       row.CreateCell(1).SetCellValue(patient.PatientLastName);
                       row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                       row.CreateCell(3).SetCellValue(patient.PatientMiddleInitial);
                       row.CreateCell(4).SetCellValue(patient.AddressFull);
                       if (patient.PatientSoC != DateTime.MinValue)
                       {
                           var createdDateCell = row.CreateCell(5);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(patient.PatientSoC);
                       }
                       else
                       {
                           row.CreateCell(5).SetCellValue(string.Empty);
                       }
                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible Employee Listing: {0}", patientRosters.Count));

               }
               
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientByResponsibleCaseManager(List<PatientRoster> patientRosters, Guid UserId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient By Responsible CaseManager";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientByResponsibleCaseManager");
           var titleRow = sheet.CreateRow(0);

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient By Responsible CaseManager");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Case Manager : {0}", UserEngine.GetName(UserId, Current.AgencyId)));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Last Name");
           headerRow.CreateCell(2).SetCellValue("First Name");
           headerRow.CreateCell(3).SetCellValue("Middle Initial");
           headerRow.CreateCell(4).SetCellValue("Address");
           headerRow.CreateCell(5).SetCellValue("Status");
           headerRow.CreateCell(6).SetCellValue("SOC Date");
           if (UserId.IsEmpty())
           {
           }
           else
           {

               if (patientRosters != null && patientRosters.Count > 0)
               {
                   int rowNumber = 2;
                   foreach (var patient in patientRosters)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(0).SetCellValue(patient.PatientId);
                       row.CreateCell(1).SetCellValue(patient.PatientLastName);
                       row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                       row.CreateCell(3).SetCellValue(patient.PatientMiddleInitial);
                       row.CreateCell(4).SetCellValue(patient.AddressFull);
                       row.CreateCell(5).SetCellValue(patient.PatientStatusName);
                       if (patient.PatientSoC != DateTime.MinValue)
                       {
                           var createdDateCell = row.CreateCell(6);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(patient.PatientSoC);
                       }
                       else
                       {
                           row.CreateCell(5).SetCellValue(string.Empty);
                       }
                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible CaseManager: {0}", patientRosters.Count));

               }
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportExpiringAuthorizations(List<Authorization> autorizations)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Expiring Authorizations";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ExpiringAuthorizations");
           var titleRow = sheet.CreateRow(0);

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Expiring Authorizations");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Patient");
           headerRow.CreateCell(1).SetCellValue("Status");
           headerRow.CreateCell(2).SetCellValue("Number");
           headerRow.CreateCell(3).SetCellValue("Start Date");
           headerRow.CreateCell(4).SetCellValue("End Date");
           if (autorizations != null && autorizations.Count > 0)
           {
               int rowNumber = 2;
               foreach (var auto in autorizations)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(auto.DisplayName);
                   row.CreateCell(1).SetCellValue(auto.Status);
                   row.CreateCell(2).SetCellValue(auto.Number1);
                   if (auto.StartDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(auto.StartDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   if (auto.EndDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(auto.EndDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Expiring Authorizations: {0}", autorizations.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ExpiringAuthorizations_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientSurveyCensus(List<SurveyCensus> surveyCensuses)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Survey Census";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientSurveyCensus");
           var titleRow = sheet.CreateRow(0);

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Survey Census");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Medicare #");
           headerRow.CreateCell(3).SetCellValue("SSN");
           headerRow.CreateCell(4).SetCellValue("DOB");
           headerRow.CreateCell(5).SetCellValue("SOC Date");
           headerRow.CreateCell(6).SetCellValue("Cert. Period");
           headerRow.CreateCell(7).SetCellValue("Insurance");
           headerRow.CreateCell(8).SetCellValue("Primary Diagnosis");
           headerRow.CreateCell(9).SetCellValue("Secondary Diagnosis");
           headerRow.CreateCell(10).SetCellValue("Triage");
           headerRow.CreateCell(11).SetCellValue("Evacuation Zone");
           headerRow.CreateCell(12).SetCellValue("Disciplines");
           headerRow.CreateCell(13).SetCellValue("Phone");
           headerRow.CreateCell(14).SetCellValue("Address Line1");
           headerRow.CreateCell(15).SetCellValue("Address Line2");
           headerRow.CreateCell(16).SetCellValue("Physician");
           headerRow.CreateCell(17).SetCellValue("NPI");
           headerRow.CreateCell(18).SetCellValue("Physician Phone");
           headerRow.CreateCell(19).SetCellValue("Physician Fax");
           headerRow.CreateCell(20).SetCellValue("Case Manager");


           if (surveyCensuses != null && surveyCensuses.Count > 0)
           {
               int rowNumber = 2;
               foreach (var surveyCensus in surveyCensuses)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(surveyCensus.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(surveyCensus.PatientDisplayName);
                   row.CreateCell(2).SetCellValue(surveyCensus.MedicareNumber);
                   row.CreateCell(3).SetCellValue(surveyCensus.SSN);
                   if (surveyCensus.DOB != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(surveyCensus.DOB);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   if (surveyCensus.SOC != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(5);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(surveyCensus.SOC);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   row.CreateCell(6).SetCellValue(surveyCensus.CertPeriod);
                   row.CreateCell(7).SetCellValue(surveyCensus.InsuranceName);
                   row.CreateCell(8).SetCellValue(surveyCensus.PrimaryDiagnosis);
                   row.CreateCell(9).SetCellValue(surveyCensus.SecondaryDiagnosis);
                   row.CreateCell(10).SetCellValue(surveyCensus.Triage);
                   row.CreateCell(11).SetCellValue(surveyCensus.EvacuationZone);
                   row.CreateCell(12).SetCellValue(surveyCensus.Discipline);
                   row.CreateCell(13).SetCellValue(surveyCensus.Phone);
                   row.CreateCell(14).SetCellValue(surveyCensus.DisplayAddressLine1);
                   row.CreateCell(15).SetCellValue(surveyCensus.DisplayAddressLine2);
                   row.CreateCell(16).SetCellValue(surveyCensus.PhysicianDisplayName);
                   row.CreateCell(17).SetCellValue(surveyCensus.PhysicianNPI);
                   row.CreateCell(18).SetCellValue(surveyCensus.PhysicianPhone);
                   row.CreateCell(19).SetCellValue(surveyCensus.PhysicianFax.ToPhone());
                   row.CreateCell(20).SetCellValue(surveyCensus.CaseManagerDisplayName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Survey Census: {0}", surveyCensuses.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(21);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientSurveyCensus_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPatientSixtyDaySummary(List<VisitNoteViewData> sixtyDaySummaries, Guid PatientId,string patientName)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Sixty Day Summary";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("SixtyDaySummary");
           var titleRow = sheet.CreateRow(0);


           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Sixty Day Summary");

           titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Employee");
           headerRow.CreateCell(1).SetCellValue("Visit Date");
           headerRow.CreateCell(2).SetCellValue("Signature Date");
           headerRow.CreateCell(3).SetCellValue("Episode Range");
           headerRow.CreateCell(4).SetCellValue("Physician");
           if (PatientId.IsEmpty())
           {
           }
           else
           {
               titleRow.CreateCell(2).SetCellValue("Patient: " + patientName);
               if (sixtyDaySummaries != null && sixtyDaySummaries.Count > 0)
               {
                   int rowNumber = 2;
                   foreach (var sixtyDaySummary in sixtyDaySummaries)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(0).SetCellValue(sixtyDaySummary.UserDisplayName);
                       DateTime visitDate = sixtyDaySummary.VisitStartDate;
                       if (visitDate != DateTime.MinValue)
                       {
                           var createdDateCell = row.CreateCell(1);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(visitDate);
                       }
                       else
                       {
                           row.CreateCell(1).SetCellValue(string.Empty);
                       }
                       DateTime signatureDate = sixtyDaySummary.VisitStartDate;
                       if (signatureDate != DateTime.MinValue)
                       {
                           var createdDateCell = row.CreateCell(2);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(signatureDate);
                       }
                       else
                       {
                           row.CreateCell(2).SetCellValue(string.Empty);
                       }
                       row.CreateCell(3).SetCellValue(sixtyDaySummary.EpisodeRange);
                       row.CreateCell(4).SetCellValue(sixtyDaySummary.PhysicianDisplayName);
                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Sixty Day Summary: {0}", sixtyDaySummaries.Count));
               }
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientSixtyDaySummary_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportDischargePatients(List<DischargePatient> dischargePatients, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Discharge Patients";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("DischargePatients");
           var titleRow = sheet.CreateRow(0);

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Discharge Patients");
           titleRow.CreateCell(2).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);

           headerRow.CreateCell(0).SetCellValue("MR#");
           headerRow.CreateCell(1).SetCellValue("Last Name");
           headerRow.CreateCell(2).SetCellValue("First Name");
           headerRow.CreateCell(3).SetCellValue("Middile Initial");
           headerRow.CreateCell(4).SetCellValue("SOC Date");
           headerRow.CreateCell(5).SetCellValue("Discharge Date");
           headerRow.CreateCell(6).SetCellValue("Discharge Reason");

           if (dischargePatients != null && dischargePatients.Count > 0)
           {
               int rowNumber = 2;
               foreach (var patient in dischargePatients)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(patient.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(patient.LastName);
                   row.CreateCell(2).SetCellValue(patient.FirstName);
                   row.CreateCell(3).SetCellValue(patient.MiddleInitial);
                   if (patient.StartofCareDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(patient.StartofCareDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   if (patient.DischargeDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(5);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(patient.DischargeDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   row.CreateCell(6).SetCellValue(patient.DischargeReason);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Discharge Patients: {0}", dischargePatients.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("DischargePatients_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalOpenOasis( IList<OpenOasis> openOasis,DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Open OASIS";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OpenOASIS");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Open OASIS");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Assessment Type");
            headerRow.CreateCell(3).SetCellValue("Date");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Employee");


            if (openOasis != null && openOasis.Count > 0)
            {
                int rowNumber = 2;
                foreach (var oasis in openOasis)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(oasis.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(oasis.PatientName);
                    row.CreateCell(2).SetCellValue(oasis.AssessmentName);
                    if (oasis.Date != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(oasis.Date);
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    row.CreateCell(4).SetCellValue(oasis.Status);
                    row.CreateCell(5).SetCellValue(oasis.CurrentlyAssigned);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Open OASIS: {0}", openOasis.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ClinicalOpenOASIS_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalMissedVisit(List<MissedVisit> missedVisits, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Missed Visit";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("MissedVisit");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Missed Visit");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Date");
           headerRow.CreateCell(3).SetCellValue("Task");
           headerRow.CreateCell(4).SetCellValue("Employee");

           if (missedVisits != null && missedVisits.Count > 0)
           {
               int rowNumber = 2;
               foreach (var missedVisit in missedVisits)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(missedVisit.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(missedVisit.PatientName);
                   if (missedVisit.Date.IsValid())
                   {
                       var createdDateCell = row.CreateCell(2);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(missedVisit.Date);
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }
                   row.CreateCell(3).SetCellValue(missedVisit.DisciplineTaskName);
                   row.CreateCell(4).SetCellValue(missedVisit.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Missed Visit: {0}", missedVisits.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ClinicalMissedVisit_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalPhysicianOrderHistory(IList<PhysicianOrder> physicianOrders, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Physician Order History";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PhysicianOrderHistory");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Physician Order History");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Order Number");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Physician");
           headerRow.CreateCell(3).SetCellValue("Status");
           headerRow.CreateCell(4).SetCellValue("Order Date");
           headerRow.CreateCell(5).SetCellValue("Sent Date");
           headerRow.CreateCell(6).SetCellValue("Received Date");

           if (physicianOrders != null && physicianOrders.Count > 0)
           {
               int rowNumber = 2;
               foreach (var physicianOrder in physicianOrders)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(physicianOrder.OrderNumber);
                   row.CreateCell(1).SetCellValue(physicianOrder.DisplayName);
                   row.CreateCell(2).SetCellValue(physicianOrder.PhysicianName);
                   row.CreateCell(3).SetCellValue(physicianOrder.StatusName);
                   if (physicianOrder.OrderDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(physicianOrder.OrderDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   if (physicianOrder.SentDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(5);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(physicianOrder.SentDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   if (physicianOrder.ReceivedDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(6);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(physicianOrder.ReceivedDate);
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Physician Order History: {0}", physicianOrders.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ClinicalPhysicianOrderHistory_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalPlanOfCareHistory(IList<Order> planOfCareHistories,DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Plan Of Care History";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PlanOfCareHistory");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Plan Of Care History");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Order Number");
           headerRow.CreateCell(1).SetCellValue("Patient Name");
           headerRow.CreateCell(2).SetCellValue("Physician Name");
           headerRow.CreateCell(3).SetCellValue("Order Date");
           headerRow.CreateCell(4).SetCellValue("Sent Date");
           headerRow.CreateCell(5).SetCellValue("Received Date");

           if (planOfCareHistories != null && planOfCareHistories.Count > 0)
           {
               int rowNumber = 2;
               foreach (var planOfCareHistory in planOfCareHistories)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(planOfCareHistory.Number);
                   row.CreateCell(1).SetCellValue(planOfCareHistory.PatientName);
                   row.CreateCell(2).SetCellValue(planOfCareHistory.PhysicianName);

                   if (planOfCareHistory.CreatedDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(planOfCareHistory.CreatedDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   if (planOfCareHistory.SendDate != DateTime.MinValue)
                   {
                       var sendDateCell = row.CreateCell(4);

                       sendDateCell.CellStyle = dateStyle;
                       sendDateCell.SetCellValue(planOfCareHistory.SendDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   if (planOfCareHistory.ReceivedDate != DateTime.MinValue)
                   {
                       var receiveDateCell = row.CreateCell(5);
                       receiveDateCell.CellStyle = dateStyle;
                       receiveDateCell.SetCellValue(planOfCareHistory.ReceivedDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Plan Of Care History: {0}", planOfCareHistories.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ClinicalPlanOfCareHistory_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalThirteenAndNineteenVisitException(List<PatientEpisodeTherapyException> visitExceptions, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToZeroFilled(), EndDate.ToZeroFilled())));


           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode");
           headerRow.CreateCell(3).SetCellValue("Scheduled");
           headerRow.CreateCell(4).SetCellValue("Completed");
           headerRow.CreateCell(5).SetCellValue("Day");
           headerRow.CreateCell(6).SetCellValue("13th Visit");
           headerRow.CreateCell(7).SetCellValue("19th Visit");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (visitExceptions != null && visitExceptions.Count > 0)
           {
               int rowNumber = 2;
               foreach (var visitException in visitExceptions)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(visitException.PatientName);
                   row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                   row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                   row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                   row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                   row.CreateCell(6).SetCellValue(visitException.ThirteenVisit);
                   row.CreateCell(7).SetCellValue(visitException.NineteenVisit);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(8);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalThirteenTherapyReevaluationException(List<PatientEpisodeTherapyException> visitExceptions)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Thirteen Therapy Re-evaluation Exception";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ThirteenTherapy ReEvaluationException");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Thirteen Therapy Re-evaluation Exception");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode");
           headerRow.CreateCell(3).SetCellValue("Scheduled");
           headerRow.CreateCell(4).SetCellValue("Completed");
           headerRow.CreateCell(5).SetCellValue("Day");
           headerRow.CreateCell(6).SetCellValue("PT Re-eval");
           headerRow.CreateCell(7).SetCellValue("ST Re-eval");
           headerRow.CreateCell(8).SetCellValue("OT Re-eval");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (visitExceptions != null && visitExceptions.Count > 0)
           {
               int rowNumber = 2;
               foreach (var visitException in visitExceptions)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(visitException.PatientName);
                   row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                   row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                   row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                   row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                   row.CreateCell(6).SetCellValue(visitException.PTEval);
                   row.CreateCell(7).SetCellValue(visitException.STEval);
                   row.CreateCell(8).SetCellValue(visitException.OTEval);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen Therapy Re-evaluation Exception: {0}", visitExceptions.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(9);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ThirteenTherapyReEvaluationException_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClinicalNineteenTherapyReevaluationException(List<PatientEpisodeTherapyException> visitExceptions)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Nineteen Therapy Re-evaluation Exception";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("NineteenTherapy ReEvaluationException");

           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Nineteen Therapy Re-evaluation Exception");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode");
           headerRow.CreateCell(3).SetCellValue("Scheduled");
           headerRow.CreateCell(4).SetCellValue("Completed");
           headerRow.CreateCell(5).SetCellValue("Day");
           headerRow.CreateCell(6).SetCellValue("PT Re-eval");
           headerRow.CreateCell(7).SetCellValue("ST Re-eval");
           headerRow.CreateCell(8).SetCellValue("OT Re-eval");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (visitExceptions != null && visitExceptions.Count > 0)
           {
               int rowNumber = 2;
               foreach (var visitException in visitExceptions)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(visitException.PatientName);
                   row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                   row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                   row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                   row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                   row.CreateCell(6).SetCellValue(visitException.PTEval);
                   row.CreateCell(7).SetCellValue(visitException.STEval);
                   row.CreateCell(8).SetCellValue(visitException.OTEval);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Nineteen Therapy Re-evaluation Exception: {0}", visitExceptions.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(9);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("NineteenTherapyReEvaluationException_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportSchedulePatientWeeklySchedule(List<TaskLean> scheduleEvents, Guid PatientId, string patientName)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Schedule Patient Weekly";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("SchedulePatientWeekly");
           var titleRow = sheet.CreateRow(0);

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Schedule Patient Weekly");

           titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", DateTime.Today.ToString("MM/dd/yyyy"), DateTime.Today.AddDays(7).ToString("MM/dd/yyyy"))));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Task");
           headerRow.CreateCell(1).SetCellValue("Status");
           headerRow.CreateCell(2).SetCellValue("Schedule Date");
           headerRow.CreateCell(3).SetCellValue("Visit Date");
           headerRow.CreateCell(4).SetCellValue("Employee");
           if (PatientId.IsEmpty())
           {
           }
           else
           {
               titleRow.CreateCell(2).SetCellValue("Patient: " + patientName);

               if (scheduleEvents != null && scheduleEvents.Count > 0)
               {
                   int rowNumber = 2;
                   foreach (var evnt in scheduleEvents)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                       row.CreateCell(1).SetCellValue(evnt.StatusName);
                       if (evnt.EventDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(2);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.EventDate);
                       }
                       else
                       {
                           row.CreateCell(2).SetCellValue(string.Empty);
                       }
                       if (evnt.VisitDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(3);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.VisitDate);
                       }
                       else
                       {
                           row.CreateCell(3).SetCellValue(string.Empty);
                       }
                       row.CreateCell(4).SetCellValue(evnt.UserName);
                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Patient Weekly: {0}", scheduleEvents.Count));
               }
             
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportPatientMonthlySchedule(List<TaskLean> scheduleEvents, Guid PatientId, int Month, int Year, string patientName)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Monthly Schedule";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientMonthlySchedule");
           var titleRow = sheet.CreateRow(0);

           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Monthly Schedule");

           titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(4).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Task");
           headerRow.CreateCell(1).SetCellValue("Status");
           headerRow.CreateCell(2).SetCellValue("Schedule Date");
           headerRow.CreateCell(3).SetCellValue("Visit Date");
           headerRow.CreateCell(4).SetCellValue("Employee");
           if (PatientId.IsEmpty() || Month <= 0)
           {
           }
           else
           {
               titleRow.CreateCell(2).SetCellValue("Patient: " + patientName);

               if (scheduleEvents != null && scheduleEvents.Count > 0)
               {
                   int rowNumber = 2;
                   foreach (var evnt in scheduleEvents)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                       row.CreateCell(1).SetCellValue(evnt.StatusName);
                       if (evnt.EventDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(2);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.EventDate);
                       }
                       else
                       {
                           row.CreateCell(2).SetCellValue(string.Empty);
                       }
                       if (evnt.EventDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(3);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.VisitDate);
                       }
                       else
                       {
                           row.CreateCell(3).SetCellValue(string.Empty);
                       }
                       row.CreateCell(4).SetCellValue(evnt.UserName);
                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Schedule: {0}", scheduleEvents.Count));
               }
               workbook.FinishWritingToExcelSpreadsheet(5);

           }
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportScheduleEmployeeWeekly(List<UserVisit> userEvents, Guid branchId, Guid UserId, DateTime StartDate, DateTime EndDate)
       {
           var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DirectorySettings.ExcelDirectory);
           var file = new FileStream(Path.Combine(path, "ScheduledVisitsTemplate.xls"), FileMode.Open, FileAccess.Read);
           var workbook = new HSSFWorkbook(file);

           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Schedule Employee Weekly";
           workbook.SummaryInformation = si;
           var sheet = workbook.GetSheetAt(0);
           workbook.SetSheetName(0, "ScheduleEmployeeWeekly");

           Dictionary<string, string> headers = new Dictionary<string, string>();
           headers.Add("Employee:", UserEngine.GetName(UserId, Current.AgencyId));
           headers.Add("Effective Date:", DateTime.Today.ToString("MM/dd/yyyy"));
           headers.Add("Date Range:", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

           string agencyLocationName = "All";
           if (!branchId.IsEmpty())
           {
               agencyLocationName = LocationEngine.GetName(branchId, Current.AgencyId);
           }

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           if (UserId.IsEmpty())
           {

           }
           else
           {

               if (userEvents != null && userEvents.Count > 0)
               {
                   int rowNumber = workbook.WriteHeaderOfExcelSpreadsheet(Current.AgencyName, agencyLocationName, "Schedule Employee Weekly", headers);
                   sheet.CreateFreezePane(0, rowNumber, 0, rowNumber);
                   //workbook.CreateMergedRegionStyles();
                   foreach (var evnt in userEvents)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(0).SetCellValue(evnt.PatientName);
                       row.CreateCell(2).SetCellValue(evnt.TaskName);
                       row.CreateCell(4).SetCellValue(evnt.StatusName);

                       if (evnt.ScheduleDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(6);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.ScheduleDate);
                       }
                       else
                       {
                           row.CreateCell(6).SetCellValue(string.Empty);
                       }
                       if (evnt.VisitDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(8);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.VisitDate);
                       }
                       else
                       {
                           row.CreateCell(8).SetCellValue(string.Empty);
                       }
                       CellRangeAddress region = new CellRangeAddress(rowNumber, rowNumber, 0, 1);
                       CellRangeAddress region2 = new CellRangeAddress(rowNumber, rowNumber, 2, 3);
                       CellRangeAddress region3 = new CellRangeAddress(rowNumber, rowNumber, 4, 5);
                       CellRangeAddress region4 = new CellRangeAddress(rowNumber, rowNumber, 6, 7);
                       CellRangeAddress region5 = new CellRangeAddress(rowNumber, rowNumber, 8, 9);

                       sheet.AddMergedRegion(region);
                       sheet.AddMergedRegion(region2);
                       sheet.AddMergedRegion(region3);
                       sheet.AddMergedRegion(region4);
                       sheet.AddMergedRegion(region5);

                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region2);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region3);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region4);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region5);

                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Employee Weekly: {0}", userEvents.Count));
               }
           }

           workbook.FinishWritingToExcelSpreadsheet(9, 7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ScheduleEmployeeWeekly_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");

       }

       public  FileContentResult ExportScheduleMonthlyWork(List<UserVisit> userEvents, Guid branchId, Guid UserId, int Month, int Year)
       {
           var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DirectorySettings.ExcelDirectory);
           var file = new FileStream(Path.Combine(path, "ScheduledVisitsTemplate.xls"), FileMode.Open, FileAccess.Read);
           var workbook = new HSSFWorkbook(file);

           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Schedule Monthly Work";
           workbook.SummaryInformation = si;

           var sheet = workbook.GetSheetAt(0);
           workbook.SetSheetName(0, "ScheduleMonthlyWork");

           //var titleRow = sheet.CreateRow(0); 
           Dictionary<string, string> headers = new Dictionary<string, string>();
           headers.Add("Employee:", UserEngine.GetName(UserId, Current.AgencyId));
           headers.Add("Effective Date:", DateTime.Today.ToString("MM/dd/yyyy"));
           headers.Add("Month:", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty);
           headers.Add("Year:", (Year > 1999) ? (new DateTime(Year, Month, 1)).ToString("yyyy") : string.Empty);

           string agencyLocationName = "All";
           if (!branchId.IsEmpty())
           {
               agencyLocationName = LocationEngine.GetName(branchId, Current.AgencyId);
           }

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           //var headerRow = sheet.CreateRow(1);
           //headerRow.CreateCell(0).SetCellValue("Patient");
           //headerRow.CreateCell(1).SetCellValue("Task");
           //headerRow.CreateCell(2).SetCellValue("Status");
           //headerRow.CreateCell(3).SetCellValue("Schedule Date");
           //headerRow.CreateCell(4).SetCellValue("Visit Date");
           if (UserId.IsEmpty() || Month <= 0)
           {

           }
           else
           {
               var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
               var toDate = DateUtilities.GetEndOfMonth(Month, Year);

               if (userEvents != null && userEvents.Count > 0)
               {
                   int rowNumber = workbook.WriteHeaderOfExcelSpreadsheet(Current.AgencyName, agencyLocationName, "Schedule Monthly Work", headers);
                   sheet.CreateFreezePane(0, rowNumber, 0, rowNumber);
                   workbook.CreateMergedRegionStyles();
                   foreach (var evnt in userEvents)
                   {
                       var row = sheet.CreateRow(rowNumber);

                       row.CreateCell(0).SetCellValue(evnt.PatientName);
                       row.CreateCell(2).SetCellValue(evnt.TaskName);
                       row.CreateCell(4).SetCellValue(evnt.StatusName);
                       if (evnt.ScheduleDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(6);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.ScheduleDate);
                       }
                       else
                       {
                           row.CreateCell(6).SetCellValue(string.Empty);
                       }
                       if (evnt.VisitDate.IsValid())
                       {
                           var createdDateCell = row.CreateCell(8);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(evnt.VisitDate);
                       }
                       else
                       {
                           row.CreateCell(8).SetCellValue(string.Empty);
                       }
                       CellRangeAddress region = new CellRangeAddress(rowNumber, rowNumber, 0, 1);
                       CellRangeAddress region2 = new CellRangeAddress(rowNumber, rowNumber, 2, 3);
                       CellRangeAddress region3 = new CellRangeAddress(rowNumber, rowNumber, 4, 5);
                       CellRangeAddress region4 = new CellRangeAddress(rowNumber, rowNumber, 6, 7);
                       CellRangeAddress region5 = new CellRangeAddress(rowNumber, rowNumber, 8, 9);

                       sheet.AddMergedRegion(region);
                       sheet.AddMergedRegion(region2);
                       sheet.AddMergedRegion(region3);
                       sheet.AddMergedRegion(region4);
                       sheet.AddMergedRegion(region5);

                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region2);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region3);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region4);
                       workbook.SetMergedRegionBorder((HSSFSheet)sheet, region5);

                       rowNumber++;
                   }
                   var totalRow = sheet.CreateRow(rowNumber + 2);
                   totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Monthly Work: {0}", userEvents.Count));
               }
           }
           workbook.FinishWritingToExcelSpreadsheet(9, 7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ScheduleMonthlyWork_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportSchedulePastDueVisits(List<TaskLean> scheduleEvents, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Past Due Visits";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PastDueVisits");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Past Due Visits");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Task");
           headerRow.CreateCell(3).SetCellValue("Schedule Date");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Assigned To");


           if (scheduleEvents != null && scheduleEvents.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in scheduleEvents)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   row.CreateCell(4).SetCellValue(evnt.StatusName);
                   row.CreateCell(5).SetCellValue(evnt.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits: {0}", scheduleEvents.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("SchedulePastDueVisits_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportSchedulePastDueVisitsByDiscipline(List<TaskLean> scheduleEvents, string Discipline, DateTime StartDate, DateTime EndDate) 
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Past Due Visits By Discipline";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PastDueVisitsByDiscipline");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Past Due Visits By Discipline");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
           titleRow.CreateCell(4).SetCellValue(string.Format("Discipline: {0}", Enum.IsDefined(typeof(Disciplines), Discipline) ? ((Disciplines)Enum.Parse(typeof(Disciplines), Discipline)).GetDescription() : string.Empty));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Schedule Date");
           headerRow.CreateCell(3).SetCellValue("Task");
           headerRow.CreateCell(4).SetCellValue("Assigned To");


           if (scheduleEvents != null && scheduleEvents.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in scheduleEvents)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(2);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate);
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }
                   row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                   row.CreateCell(4).SetCellValue(evnt.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits By Discipline: {0}", scheduleEvents.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PastDueVisitsByDiscipline_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportScheduleDailyWork(List<TaskLean> scheduleEvents)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Daily Work Schedule";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("DailyWorkSchedule");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Daily Work Schedule");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Task");
           headerRow.CreateCell(3).SetCellValue("Status");
           headerRow.CreateCell(4).SetCellValue("Schedule Date");
           headerRow.CreateCell(5).SetCellValue("Employee");
           if (scheduleEvents != null && scheduleEvents.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in scheduleEvents)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                   row.CreateCell(3).SetCellValue(evnt.StatusName);
                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   row.CreateCell(5).SetCellValue(evnt.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Daily Work Schedule: {0}", scheduleEvents.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("DailyWorkSchedule_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportScheduleCaseManagerTask(List<TaskLean> scheduleEvents)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Case Manager Task";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("CaseManagerTask");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Case Manager Task");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Schedule Date");
           headerRow.CreateCell(3).SetCellValue("Task");
           headerRow.CreateCell(4).SetCellValue("Assigned To");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (scheduleEvents != null && scheduleEvents.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in scheduleEvents)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(2);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate);
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }
                   row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                   row.CreateCell(4).SetCellValue(evnt.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Case Manager Task: {0}", scheduleEvents.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("CaseManagerTask_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportScheduleUpcomingRecet(List<RecertEvent> recets)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Upcoming Recert.";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("UpcomingRecert");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Upcoming Recert.");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Employee Responsible");
           headerRow.CreateCell(3).SetCellValue("Status");
           headerRow.CreateCell(4).SetCellValue("Due Date");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (recets != null && recets.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in recets)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   row.CreateCell(2).SetCellValue(evnt.AssignedTo);
                   row.CreateCell(3).SetCellValue(evnt.StatusName);
                   if (evnt.TargetDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.TargetDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Upcoming Recert. : {0}", recets.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("UpcomingRecert_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportScheduleVisitsByStatus(List<TaskLean> events, Guid branchId, DateTime StartDate, DateTime EndDate, int status) 
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Visits By Status";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("VisitsByStatus");
           var titleRow = sheet.CreateRow(0);
           string agencyLocationName = "";
           if (!branchId.IsEmpty())
           {
               var name = LocationEngine.GetName(branchId, Current.AgencyId);
               if (name.IsNotNullOrEmpty())
               {
                   agencyLocationName = " - " + name;
               }
           }
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName + agencyLocationName);
           titleRow.CreateCell(1).SetCellValue("Visits By Status.");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
           titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", ((ScheduleStatus)status).GetDescription()));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Task");
           headerRow.CreateCell(3).SetCellValue("Scheduled Date");
           headerRow.CreateCell(4).SetCellValue("Assigned To");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (events != null && events.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in events)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   row.CreateCell(4).SetCellValue(evnt.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total: {0}", events.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("VisitsByStatus_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportScheduleVisitsByType(List<TaskLean> events, Guid branchId, DateTime StartDate, DateTime EndDate, int type)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Visits By Type";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("VisitsByType");
           var titleRow = sheet.CreateRow(0);
           string agencyLocationName = "";
           if (!branchId.IsEmpty())
           {
               var name = LocationEngine.GetName(branchId, Current.AgencyId);
               if (name.IsNotNullOrEmpty())
               {
                   agencyLocationName = " - " + name;
               }
           }
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName + agencyLocationName);
           titleRow.CreateCell(1).SetCellValue("Visits By Type");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
           titleRow.CreateCell(4).SetCellValue(string.Format("Type: {0}", ((DisciplineTasks)type).GetDescription()));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Status");
           headerRow.CreateCell(3).SetCellValue("Scheduled Date");
           headerRow.CreateCell(4).SetCellValue("Assigned To");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (events != null && events.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in events)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(evnt.PatientName);
                   row.CreateCell(2).SetCellValue(evnt.StatusName);
                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   row.CreateCell(4).SetCellValue(evnt.UserName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total: {0}", events.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("VisitsByType_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportOutstandingClaims(List<TypeOfBill> bills, string Type)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Outstanding Claims";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("OutstandingClaims");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Outstanding Claims");
           titleRow.CreateCell(2).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Type");
           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.PatientDisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   row.CreateCell(3).SetCellValue(bill.Type);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Outstanding Claims: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportClaimsByStatus(List<ClaimLean> bills, string Type, int Status)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Claims By Status";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ClaimsByStatus");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Claims By Status");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
           titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           ICellStyle currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Type");
           headerRow.CreateCell(4).SetCellValue("Claim Amount");
           headerRow.CreateCell(5).SetCellValue("Claim Date");
           headerRow.CreateCell(6).SetCellValue("Payment Amount");
           headerRow.CreateCell(7).SetCellValue("Payment Date");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   row.CreateCell(3).SetCellValue(bill.Type);
                   var claimAmountCell = row.CreateCell(4);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(bill.ClaimAmount);
                   if (bill.ClaimDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(5);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   var paymentAmountCell = row.CreateCell(6);
                   paymentAmountCell.CellStyle = currencyStyle;
                   paymentAmountCell.SetCellValue(bill.PaymentAmount);
                   if (bill.PaymentDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(7);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.PaymentDate);
                   }
                   else
                   {
                       row.CreateCell(7).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(8);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ClaimsByStatus_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportSubmittedClaims(List<ClaimLean> bills, string Type)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Claims By Status";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ClaimsByStatus");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Claims By Status");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           ICellStyle currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Type");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Claim Amount");
           headerRow.CreateCell(6).SetCellValue("Claim Date");
           headerRow.CreateCell(7).SetCellValue("Payment Amount");
           headerRow.CreateCell(8).SetCellValue("Payment Date");

           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   row.CreateCell(3).SetCellValue(bill.Type);
                   row.CreateCell(4).SetCellValue(bill.StatusName);
                   var claimAmountCell = row.CreateCell(5);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(bill.ClaimAmount);
                   if (bill.ClaimDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(6);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }
                   var paymentAmountCell = row.CreateCell(7);
                   paymentAmountCell.CellStyle = currencyStyle;
                   paymentAmountCell.SetCellValue(bill.PaymentAmount);
                   if (bill.ClaimDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(8);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(8).SetCellValue(string.Empty);
                   }

                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(9);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportExpectedSubmittedClaims(List<ClaimLean> bills, string Type)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Expected Submitted Claims";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ExpectedSubmittedClaims");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Expected Submitted Claims");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           ICellStyle currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Type");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Claim Amount");


           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   row.CreateCell(3).SetCellValue(bill.Type);
                   row.CreateCell(4).SetCellValue(bill.StatusName);
                   var claimAmountCell = row.CreateCell(5);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(bill.ClaimAmount);


                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Expected Submitted Claims {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ExpectedSubmittedClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportAccountsReceivable(List<ClaimLean> bills, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Accounts Receivable";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("AccountsReceivable");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Accounts Receivable");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
           titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Type");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Claim Date");
           headerRow.CreateCell(6).SetCellValue("Amount");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   row.CreateCell(3).SetCellValue(bill.Type);
                   row.CreateCell(4).SetCellValue(bill.StatusName);
                   if (bill.ClaimDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(5);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   var claimAmountCell = row.CreateCell(6);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(Math.Round(bill.ClaimAmount, 2));
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Accounts Receivable: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("AccountsReceivable_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportUnearnedRevenue(IList<Revenue> revenue, int InsuranceId, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Unearned Revenue";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("UnearnedRevenue");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Unearned Revenue Report");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Report Date: {0}", EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Assessment");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Episode Payment");
           headerRow.CreateCell(6).SetCellValue("Total Visits");
           headerRow.CreateCell(7).SetCellValue("Completed Visits");
           headerRow.CreateCell(8).SetCellValue("Unearned Visits");
           headerRow.CreateCell(9).SetCellValue("Unit Amount");
           headerRow.CreateCell(10).SetCellValue("Unearned Amount");
           headerRow.CreateCell(11).SetCellValue("Rap Amount");
           sheet.CreateFreezePane(0, 2, 0, 2);


           if (revenue != null && revenue.Count > 0)
           {
               int rowNumber = 2;
               foreach (var rap in revenue)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(rap.DisplayName);
                   row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                   row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                   row.CreateCell(4).SetCellValue(rap.StatusName);

                   if (rap.ProspectivePayment.IsNotNullOrEmpty())
                   {
                       var prospectivePaymentCell = row.CreateCell(5);
                       prospectivePaymentCell.CellStyle = currencyStyle;
                       prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }

                   row.CreateCell(6).SetCellValue(rap.BillableVisitCount);
                   row.CreateCell(7).SetCellValue(rap.CompletedVisitCount);
                   row.CreateCell(8).SetCellValue(rap.UnearnedVisitCount);

                   var unitAmountCell = row.CreateCell(9);
                   unitAmountCell.CellStyle = currencyStyle;
                   unitAmountCell.SetCellValue(rap.UnitAmount);

                   var unearnedRevenueAmountCell = row.CreateCell(10);
                   unearnedRevenueAmountCell.CellStyle = currencyStyle;
                   unearnedRevenueAmountCell.SetCellValue(rap.UnearnedRevenueAmount);

                   var rapAmountCell = row.CreateCell(11);
                   rapAmountCell.CellStyle = currencyStyle;
                   rapAmountCell.SetCellValue(rap.RapAmount);

                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unearned Revenue: {0}", revenue.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(12);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("UnearnedRevenue_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportUnbilledRevenue(IList<Revenue> revenue, int InsuranceId, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Unbilled Revenue";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("UnbilledRevenue");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Unbilled Revenue Report");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Report Date: {0}", EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Assessment");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Rap Amount");
           headerRow.CreateCell(6).SetCellValue("Episode Payment");
           headerRow.CreateCell(7).SetCellValue("Total Visits");
           headerRow.CreateCell(8).SetCellValue("Completed Visits");
           headerRow.CreateCell(9).SetCellValue("Unbilled Visits");
           headerRow.CreateCell(10).SetCellValue("Unit Amount");
           headerRow.CreateCell(11).SetCellValue("Unbilled Amount");

           if (revenue != null && revenue.Count > 0)
           {
               int rowNumber = 2;
               foreach (var rap in revenue)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(rap.DisplayName);
                   row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                   row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                   row.CreateCell(4).SetCellValue(rap.StatusName);

                   var rapAmountCell = row.CreateCell(5);
                   rapAmountCell.CellStyle = currencyStyle;
                   rapAmountCell.SetCellValue(rap.RapAmount);

                   if (rap.ProspectivePayment.IsNotNullOrEmpty())
                   {
                       var prospectivePaymentCell = row.CreateCell(6);
                       prospectivePaymentCell.CellStyle = currencyStyle;
                       prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }

                   row.CreateCell(7).SetCellValue(rap.BillableVisitCount);
                   row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);
                   row.CreateCell(9).SetCellValue(rap.UnbilledVisitCount);

                   var unitAmountCell = row.CreateCell(10);
                   unitAmountCell.CellStyle = currencyStyle;
                   unitAmountCell.SetCellValue(rap.UnitAmount);

                   var unbilledRevenueAmountCell = row.CreateCell(11);
                   unbilledRevenueAmountCell.CellStyle = currencyStyle;
                   unbilledRevenueAmountCell.SetCellValue(rap.UnbilledRevenueAmount);

                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unbilled Revenue: {0}", revenue.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(12);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("UnbilledRevenue_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEarnedRevenue(IList<Revenue> revenue, int InsuranceId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Earned Revenue";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EarnedRevenue");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Earned Revenue");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Assessment");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Total Visits");
           headerRow.CreateCell(6).SetCellValue("Episode Payment");
           headerRow.CreateCell(7).SetCellValue("Unit Amount");
           headerRow.CreateCell(8).SetCellValue("Completed Visits");
           headerRow.CreateCell(9).SetCellValue("Earned Amount");

           if (revenue != null && revenue.Count > 0)
           {
               int rowNumber = 2;
               foreach (var rap in revenue)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(rap.DisplayName);
                   row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                   row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                   row.CreateCell(4).SetCellValue(rap.StatusName);
                   row.CreateCell(5).SetCellValue(rap.BillableVisitCount);

                   if (rap.ProspectivePayment.IsNotNullOrEmpty())
                   {
                       var prospectivePaymentCell = row.CreateCell(6);
                       prospectivePaymentCell.CellStyle = currencyStyle;
                       prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }

                   var unitAmountCell = row.CreateCell(7);
                   unitAmountCell.CellStyle = currencyStyle;
                   unitAmountCell.SetCellValue(rap.UnitAmount);

                   row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);

                   var earnedRevenueAmountCell = row.CreateCell(9);
                   earnedRevenueAmountCell.CellStyle = currencyStyle;
                   earnedRevenueAmountCell.SetCellValue(rap.EarnedRevenueAmount);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Earned Revenue: {0}", revenue.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(10);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EarnedRevenue_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportAgedAccountsReceivable(List<ClaimLean> bills, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Aged Accounts Receivable";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("AgedAccountsReceivable");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Aged Accounts Receivable");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
           titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Type");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Claim Date");
           headerRow.CreateCell(6).SetCellValue("1-30");
           headerRow.CreateCell(7).SetCellValue("31-60");
           headerRow.CreateCell(8).SetCellValue("61-90");
           headerRow.CreateCell(9).SetCellValue("> 90");
           headerRow.CreateCell(10).SetCellValue("Total");

           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   row.CreateCell(3).SetCellValue(bill.Type);
                   row.CreateCell(4).SetCellValue(bill.StatusName);

                   if (bill.ClaimDate != DateTime.MinValue)
                   {
                       var claimDateCell = row.CreateCell(5);
                       claimDateCell.CellStyle = dateStyle;
                       claimDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   var amount30Cell = row.CreateCell(6);
                   amount30Cell.CellStyle = currencyStyle;
                   amount30Cell.SetCellValue(Math.Round(bill.Amount30, 2));

                   var amount60Cell = row.CreateCell(7);
                   amount60Cell.CellStyle = currencyStyle;
                   amount60Cell.SetCellValue(Math.Round(bill.Amount60, 2));

                   var amount90Cell = row.CreateCell(8);
                   amount90Cell.CellStyle = currencyStyle;
                   amount90Cell.SetCellValue(Math.Round(bill.Amount90, 2));

                   var amountOver90Cell = row.CreateCell(9);
                   amountOver90Cell.CellStyle = currencyStyle;
                   amountOver90Cell.SetCellValue(Math.Round(bill.AmountOver90, 2));

                   var claimAmountCell = row.CreateCell(10);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(Math.Round(bill.ClaimAmount, 2));
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Aged Accounts Receivable: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(11);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("AgedAccountsReceivable_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPPSRAPClaims(IList<Claim> raps)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - PPS RAP Claims";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PPSRAPClaims");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("PPS RAP Claims");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("HIPPS");
           headerRow.CreateCell(4).SetCellValue("Amount");

           if (raps != null && raps.Count > 0)
           {
               int rowNumber = 2;
               foreach (var rap in raps)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(rap.DisplayName);
                   row.CreateCell(2).SetCellValue(rap.DateRange);
                   row.CreateCell(3).SetCellValue(rap.HippsCode);

                   var prospectivePayCell = row.CreateCell(4);
                   prospectivePayCell.CellStyle = currencyStyle;
                   prospectivePayCell.SetCellValue(rap.ProspectivePay);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS RAP Claims: {0}", raps.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PPSRAPClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPPSFinalClaims(List<Claim> finals)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - PPS Final Claims";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PPSFinalClaims");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("PPS Final Claims");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("HIPPS");
           headerRow.CreateCell(4).SetCellValue("Amount");

           if (finals != null && finals.Count > 0)
           {
               int rowNumber = 2;
               foreach (var final in finals)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(final.DisplayName);
                   row.CreateCell(2).SetCellValue(final.DateRange);
                   row.CreateCell(3).SetCellValue(final.HippsCode);

                   var prospectivePayCell = row.CreateCell(4);
                   prospectivePayCell.CellStyle = currencyStyle;
                   prospectivePayCell.SetCellValue(final.ProspectivePay);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS Final Claims: {0}", finals.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPotentialClaimAutoCancel(List<Claim> finals)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Potential Claim AutoCancel";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PotentialClaimAutoCancel");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Potential Claim AutoCancel");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var headerRow = sheet.CreateRow(1);

           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");

           if (finals != null && finals.Count > 0)
           {
               int rowNumber = 2;
               foreach (var final in finals)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(final.DisplayName);
                   row.CreateCell(2).SetCellValue(final.DateRange);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Potential Claim AutoCancel: {0}", finals.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(4);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportBillingBatch(List<ClaimInfoDetail> bills, string ClaimType, DateTime BatchDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Billing Batch Report";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("BillingBatch");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Billing Batch");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Claim Type: {0}", ClaimType.ToUpperCase()));
           titleRow.CreateCell(4).SetCellValue(string.Format("Date: {0}", BatchDate.ToString("MM/dd/yyyy")));

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Medicare Number");
           headerRow.CreateCell(4).SetCellValue("Bill Type");
           headerRow.CreateCell(5).SetCellValue("Claim Amount");
           headerRow.CreateCell(6).SetCellValue("Prospective Amount");
           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.Range);
                   row.CreateCell(3).SetCellValue(bill.MedicareNumber);
                   row.CreateCell(4).SetCellValue(bill.BillType);

                   var claimAmountCell = row.CreateCell(5);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(Math.Round(bill.ClaimAmount, 2));

                   var prospectivePayCell = row.CreateCell(6);
                   prospectivePayCell.CellStyle = currencyStyle;
                   prospectivePayCell.SetCellValue(Math.Round(bill.ProspectivePay, 2));
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("BillingBatch_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEarnedRevenueByEpisodeDays(IList<Revenue> revenue, int InsuranceId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Earned Revenue By Episode Days";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EarnedRevenueEpisodeDays");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Earned Revenue By Episode Days");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Assessment");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Total Days");
           headerRow.CreateCell(6).SetCellValue("Episode Payment");
           headerRow.CreateCell(7).SetCellValue("Unit Amount");
           headerRow.CreateCell(8).SetCellValue("Completed Days");
           headerRow.CreateCell(9).SetCellValue("Earned Amount");

           if (revenue != null && revenue.Count > 0)
           {
               int rowNumber = 2;
               foreach (var rap in revenue)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(rap.DisplayName);
                   row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                   row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                   row.CreateCell(4).SetCellValue(rap.StatusName);
                   row.CreateCell(5).SetCellValue(rap.EpisodeDays);

                   if (rap.ProspectivePayment.IsNotNullOrEmpty())
                   {
                       var prospectivePaymentCell = row.CreateCell(6);
                       prospectivePaymentCell.CellStyle = currencyStyle;
                       prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }
                   var unitAmountCell = row.CreateCell(7);
                   unitAmountCell.CellStyle = currencyStyle;
                   unitAmountCell.SetCellValue(rap.UnitAmount);

                   row.CreateCell(8).SetCellValue(rap.CompletedDayCount);

                   var earnedRevenueAmountCell = row.CreateCell(9);
                   earnedRevenueAmountCell.CellStyle = currencyStyle;
                   earnedRevenueAmountCell.SetCellValue(rap.EarnedRevenueAmount);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Earned Revenue: {0}", revenue.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(10);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EarnedRevenueByEpisodeDays_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportBilledRevenue(IList<Revenue> revenue, int InsuranceId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Revenue Report";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("Revenue");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Revenue Report");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Assessment");
           headerRow.CreateCell(4).SetCellValue("Status");
           headerRow.CreateCell(5).SetCellValue("Episode Payment");
           headerRow.CreateCell(6).SetCellValue("Total Visits");
           headerRow.CreateCell(7).SetCellValue("Unit Amount");
           headerRow.CreateCell(8).SetCellValue("Earned Visits");
           headerRow.CreateCell(9).SetCellValue("Earned Amount");
           headerRow.CreateCell(10).SetCellValue("Unearned Visits");
           headerRow.CreateCell(11).SetCellValue("Unearned Amount");
           headerRow.CreateCell(12).SetCellValue("Unbilled Visits");
           headerRow.CreateCell(13).SetCellValue("Unbilled Amount");
           headerRow.CreateCell(14).SetCellValue("Billed Visits");
           headerRow.CreateCell(15).SetCellValue("Billed Amount");

           if (revenue != null && revenue.Count > 0)
           {
               int rowNumber = 2;
               Double totalEarned = 0.0;
               Double totalUnEarned = 0.0;
               Double totalUnbilled = 0.0;
               Double totalBilled = 0.0;
               foreach (var rap in revenue)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(rap.DisplayName);
                   row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                   row.CreateCell(3).SetCellValue(rap.AssessmentType);
                   row.CreateCell(4).SetCellValue(rap.StatusName);

                   if (rap.ProspectivePayment.IsNotNullOrEmpty())
                   {
                       var prospectivePaymentCell = row.CreateCell(5);
                       prospectivePaymentCell.CellStyle = currencyStyle;
                       prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }

                   row.CreateCell(6).SetCellValue(rap.BillableVisitCount);

                   var unitAmountCell = row.CreateCell(7);
                   unitAmountCell.CellStyle = currencyStyle;
                   unitAmountCell.SetCellValue(rap.UnitAmount);

                   row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);

                   var earnedRevenueAmountCell = row.CreateCell(9);
                   earnedRevenueAmountCell.CellStyle = currencyStyle;
                   earnedRevenueAmountCell.SetCellValue(rap.EarnedRevenueAmount);

                   row.CreateCell(10).SetCellValue(rap.UnearnedVisitCount);

                   var unearnedRevenueAmountCell = row.CreateCell(11);
                   unearnedRevenueAmountCell.CellStyle = currencyStyle;
                   unearnedRevenueAmountCell.SetCellValue(rap.UnearnedRevenueAmount);

                   row.CreateCell(12).SetCellValue(rap.UnbilledVisitCount);

                   var unbilledRevenueAmountCell = row.CreateCell(13);
                   unbilledRevenueAmountCell.CellStyle = currencyStyle;
                   unbilledRevenueAmountCell.SetCellValue(rap.UnearnedRevenueAmount);

                   row.CreateCell(14).SetCellValue(rap.RapVisitCount);

                   var rapAmountCell = row.CreateCell(15);
                   rapAmountCell.CellStyle = currencyStyle;
                   rapAmountCell.SetCellValue(rap.RapAmount);

                   rowNumber++;
                   totalEarned += rap.EarnedRevenueAmount;
                   totalUnEarned += rap.UnearnedRevenueAmount;
                   totalUnbilled += rap.UnbilledRevenueAmount;
                   totalBilled += rap.RapAmount;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Revenue Items: {0}", revenue.Count));

               totalRow.CreateCell(8).SetCellValue("Total Earned:");
               var totalEarnedCell = totalRow.CreateCell(9);
               totalEarnedCell.CellStyle = currencyStyle;
               totalEarnedCell.SetCellValue(totalEarned);

               totalRow.CreateCell(10).SetCellValue("Total Unearned:");
               var totalUnEarnedCell = totalRow.CreateCell(11);
               totalUnEarnedCell.CellStyle = currencyStyle;
               totalUnEarnedCell.SetCellValue(totalUnEarned);

               totalRow.CreateCell(12).SetCellValue("Total Unbilled:");
               var totalUnbilledCell = totalRow.CreateCell(13);
               totalUnbilledCell.CellStyle = currencyStyle;
               totalUnbilledCell.SetCellValue(totalUnbilled);

               totalRow.CreateCell(14).SetCellValue("Total Billed:");
               var totalBilledCell = totalRow.CreateCell(15);
               totalBilledCell.CellStyle = currencyStyle;
               totalBilledCell.SetCellValue(totalBilled);

               var sumRow = sheet.CreateRow(rowNumber + 4);

               sumRow.CreateCell(8).SetCellValue("Earned + Unearned:");
               var earnedAndUnearnedCell = sumRow.CreateCell(9);
               earnedAndUnearnedCell.CellStyle = currencyStyle;
               earnedAndUnearnedCell.SetCellValue(totalEarned + totalUnEarned);

               sumRow.CreateCell(13).SetCellValue("Billed + Unbilled:");
               var billedAndUnbilled = sumRow.CreateCell(14);
               totalBilledCell.CellStyle = currencyStyle;
               totalBilledCell.SetCellValue(totalBilled + totalUnbilled);
           }

           workbook.FinishWritingToExcelSpreadsheet(16);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("RevenueReport_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportManagedAccountsReceivable(List<ManagedBill> bills, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export -Managed Accounts Receivable";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ManagedAccountsReceivable");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Managed Accounts Receivable");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
           titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           var insurance = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insurance != null)
           {
               titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
           }

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Status");
           headerRow.CreateCell(4).SetCellValue("Claim Date");
           headerRow.CreateCell(5).SetCellValue("Amount");
           sheet.CreateFreezePane(0, 2, 0, 2);
           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.DateRange);
                   row.CreateCell(3).SetCellValue(bill.StatusName);
                   if (bill.ClaimDate != DateTime.MinValue)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   var claimAmountCell = row.CreateCell(5);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(Math.Round(bill.ProspectivePay, 2));
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Managed Accounts Receivable: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ManagedAccountsReceivable_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportManagedClaimsByStatus(List<ClaimLean> bills, int Status)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Managed Claims By Status";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("ManagedClaimsByStatus");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Managed Claims By Status");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Claim Amount");
           headerRow.CreateCell(4).SetCellValue("Claim Date");
           headerRow.CreateCell(5).SetCellValue("Payment Amount");
           headerRow.CreateCell(6).SetCellValue("Payment Date");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   var claimAmountCell = row.CreateCell(3);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(bill.ClaimAmount);
                   if (bill.ClaimDate.Date > DateTime.MinValue.Date)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   var paymentAmountCell = row.CreateCell(5);
                   paymentAmountCell.CellStyle = currencyStyle;
                   paymentAmountCell.SetCellValue(bill.PaymentAmount);
                   if (bill.PaymentDate.Date > DateTime.MinValue.Date)
                   {
                       var createdDateCell = row.CreateCell(6);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.PaymentDate);
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Managed Claims By Status: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("ManagedClaimsByStatus_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportUnbilledManagedClaims(List<ClaimLean> bills, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Unbilled Managed Claims";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("UnbilledManagedClaims");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Unbilled Managed Claims");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var currencyStyle = workbook.CreateCellStyle();
           currencyStyle.DataFormat = (short)8;

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Episode Range");
           headerRow.CreateCell(3).SetCellValue("Claim Amount");
           headerRow.CreateCell(4).SetCellValue("Claim Date");
           headerRow.CreateCell(5).SetCellValue("Payment Amount");
           headerRow.CreateCell(6).SetCellValue("Payment Date");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (bills != null && bills.Count > 0)
           {
               int rowNumber = 2;
               foreach (var bill in bills)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                   row.CreateCell(1).SetCellValue(bill.DisplayName);
                   row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                   var claimAmountCell = row.CreateCell(3);
                   claimAmountCell.CellStyle = currencyStyle;
                   claimAmountCell.SetCellValue(bill.ClaimAmount);
                   if (bill.ClaimDate.Date > DateTime.MinValue.Date)
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.ClaimDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   var paymentAmountCell = row.CreateCell(5);
                   paymentAmountCell.CellStyle = currencyStyle;
                   paymentAmountCell.SetCellValue(bill.PaymentAmount);
                   if (bill.PaymentDate.Date > DateTime.MinValue.Date)
                   {
                       var createdDateCell = row.CreateCell(6);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(bill.PaymentDate);
                   }
                   else
                   {
                       row.CreateCell(6).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unbilled Managed Claims: {0}", bills.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("UnbilledManagedClaims_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEmployeeRoster(List<User> employeeRosters, int StatusId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee Roster";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeeRoster");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee Roster");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));
           var headerRow = sheet.CreateRow(1);

           headerRow.CreateCell(0).SetCellValue("Name");
           headerRow.CreateCell(1).SetCellValue("Address");
           headerRow.CreateCell(2).SetCellValue("City");
           headerRow.CreateCell(3).SetCellValue("State");
           headerRow.CreateCell(4).SetCellValue("Zip Code");
           headerRow.CreateCell(5).SetCellValue("Home Phone");
           headerRow.CreateCell(6).SetCellValue("Gender");
           sheet.CreateFreezePane(0, 2, 0, 2);

           if (employeeRosters != null && employeeRosters.Count > 0)
           {
               int rowNumber = 2;
               foreach (var employee in employeeRosters)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(employee.DisplayName);
                   row.CreateCell(1).SetCellValue(employee.Profile.Address);
                   row.CreateCell(2).SetCellValue(employee.Profile.AddressCity);
                   row.CreateCell(3).SetCellValue(employee.Profile.AddressStateCode);
                   row.CreateCell(4).SetCellValue(employee.Profile.AddressZipCode);
                   row.CreateCell(5).SetCellValue(employee.Profile.PhoneHome.ToPhone());
                   row.CreateCell(6).SetCellValue(employee.Profile.Gender);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Roster: {0}", employeeRosters.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(7);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeeRoster_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEmployeeBirthdayList(List<Birthday> employeeBirthDays, int StatusId, int Month)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee Birthday List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeeBirthdayList");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee Birthday List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));
           titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));

           var birthDateStyle = workbook.CreateCellStyle();
           birthDateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MMMM d");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Name");
           headerRow.CreateCell(1).SetCellValue("Age");
           headerRow.CreateCell(2).SetCellValue("Birth Day");
           headerRow.CreateCell(3).SetCellValue("Address First Row");
           headerRow.CreateCell(4).SetCellValue("Address Second Row");
           headerRow.CreateCell(5).SetCellValue("Home Phone");

           if (employeeBirthDays != null && employeeBirthDays.Count > 0)
           {
               int rowNumber = 2;
               foreach (var birthDay in employeeBirthDays)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(birthDay.Name);
                   row.CreateCell(1).SetCellValue(birthDay.Age);
                   var birthDayCell = row.CreateCell(2);
                   birthDayCell.SetCellValue(birthDay.SortableDate);
                   birthDayCell.CellStyle = birthDateStyle;
                   row.CreateCell(3).SetCellValue(birthDay.AddressFirstRow);
                   row.CreateCell(4).SetCellValue(birthDay.AddressSecondRow);
                   row.CreateCell(5).SetCellValue(birthDay.PhoneHome.ToPhone());
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Birthday List: {0}", employeeBirthDays.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeeBirthdayList_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEmployeeLicenseListing(IList<License> employeeLicenses, Guid UserId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee License Listing";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeeLicenseListing");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee License Listing");
           titleRow.CreateCell(2).SetCellValue("Employee: " + UserEngine.GetName(UserId, Current.AgencyId));
           titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("License Name");
           headerRow.CreateCell(1).SetCellValue("License Number");
           headerRow.CreateCell(2).SetCellValue("Initiation Date");
           headerRow.CreateCell(3).SetCellValue("Expiration Date");


           if (employeeLicenses != null && employeeLicenses.Count > 0)
           {
               int rowNumber = 2;
               foreach (var employeeLicense in employeeLicenses)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(employeeLicense.LicenseType);
                   row.CreateCell(1).SetCellValue(employeeLicense.LicenseNumber);
                   if (employeeLicense.IssueDate != DateTime.MinValue)
                   {
                       var initiationDateCell = row.CreateCell(2);
                       initiationDateCell.CellStyle = dateStyle;
                       initiationDateCell.SetCellValue(employeeLicense.IssueDate);
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }
                   if (employeeLicense.ExpireDate != DateTime.MinValue)
                   {
                       var expirationDateCell = row.CreateCell(3);
                       expirationDateCell.CellStyle = dateStyle;
                       expirationDateCell.SetCellValue(employeeLicense.ExpireDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee License Listing: {0}", employeeLicenses.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(4);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportAllEmployeeLicenseListing(List<License> licenses)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - All Employee License Listing";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("AllEmployeeLicenseListing");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("All Employee License Listing");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Employee");
           headerRow.CreateCell(1).SetCellValue("License Name");
           headerRow.CreateCell(2).SetCellValue("License Number");
           headerRow.CreateCell(3).SetCellValue("Initiation Date");
           headerRow.CreateCell(4).SetCellValue("Expiration Date");

           if (licenses.IsNotNullOrEmpty())
           {
               int rowNumber = 2;
               licenses.ForEach(l =>
               {
                   var licenseRow = sheet.CreateRow(rowNumber);

                   var rowCellStyle = workbook.CreateCellStyle();
                   rowCellStyle.BorderTop = BorderStyle.THIN;
                   licenseRow.RowStyle = rowCellStyle;

                   var cell = licenseRow.CreateCell(0);
                   var cellStyle = workbook.CreateCellStyle();
                   cell.SetCellValue(l.DisplayName);
                          
                   //rowNumber++;
                   //var licenseRow = sheet.CreateRow(rowNumber);
                   //licenseRow.CreateCell(0).SetCellValue(string.Empty);
                   licenseRow.CreateCell(1).SetCellValue(l.LicenseType);
                   licenseRow.CreateCell(2).SetCellValue(l.LicenseNumber);
                   if (l.IssueDate != DateTime.MinValue)
                   {
                       var initiationDateCell = licenseRow.CreateCell(3);
                       initiationDateCell.CellStyle = dateStyle;
                       initiationDateCell.SetCellValue(l.IssueDate);
                   }
                   else
                   {
                       licenseRow.CreateCell(3).SetCellValue(string.Empty);
                   }
                   if (l.ExpireDate != DateTime.MinValue)
                   {
                       var expirationDateCell = licenseRow.CreateCell(4);
                       expirationDateCell.CellStyle = dateStyle;
                       expirationDateCell.SetCellValue(l.ExpireDate);
                   }
                   else
                   {
                       licenseRow.CreateCell(4).SetCellValue(string.Empty);
                   }
                   
                   rowNumber = rowNumber + 1;
               });
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of  Licenses : {0}", licenses.Count));
           }

           workbook.FinishWritingToExcelSpreadsheet(5);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("AllEmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEmployeeExpiringLicense(List<License> employeeLicenses, int StatusId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee Expiring License";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeeExpiringLicense");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee Expiring License");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Custom Id");
           headerRow.CreateCell(1).SetCellValue("Employee");
           headerRow.CreateCell(2).SetCellValue("License Name");
           headerRow.CreateCell(3).SetCellValue("License Number");
           headerRow.CreateCell(4).SetCellValue("Initiation Date");
           headerRow.CreateCell(5).SetCellValue("Expiration Date");


           if (employeeLicenses != null && employeeLicenses.Count > 0)
           {
               int rowNumber = 2;
               foreach (var employeeLicense in employeeLicenses)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(employeeLicense.CustomId);
                   row.CreateCell(1).SetCellValue(employeeLicense.DisplayName);
                   row.CreateCell(2).SetCellValue(employeeLicense.LicenseType);
                   row.CreateCell(3).SetCellValue(employeeLicense.LicenseNumber);

                   if (employeeLicense.IssueDate != DateTime.MinValue)
                   {
                       var initiationDateCell = row.CreateCell(4);
                       initiationDateCell.CellStyle = dateStyle;
                       initiationDateCell.SetCellValue(employeeLicense.IssueDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   if (employeeLicense.ExpireDate != DateTime.MinValue)
                   {
                       var expirationDateCell = row.CreateCell(5);
                       expirationDateCell.CellStyle = dateStyle;
                       expirationDateCell.SetCellValue(employeeLicense.ExpireDate);
                   }
                   else
                   {
                       row.CreateCell(5).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Expiring License: {0}", employeeLicenses.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeeExpiringLicenses_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportPayrollDetailSummary(List<PayrollDetailSummaryItem> summary, Guid employeeId, DateTime startDate, DateTime endDate)
       {
           const int TaskName = 0;
           const int TaskCount = 1;
           const int TaskTime = 2;
           const int Mileage = 3;
           const int Surcharges = 4;
           int rowNumber = 3;
           int totalDays = 0;
           int totalHours = 0;
           int totalMinutes = 0;
           int totalTasks = 0;
           double totalMileage = 0.0;
           double totalSurcharges = 0.0;

           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Author = "Axxess Consult";
           si.Subject = "Axxess Data Export - Payroll Detail Summary";
           workbook.SummaryInformation = si;

           var reportUser = UserEngine.GetUser(employeeId, Current.AgencyId);

           var sheet = workbook.CreateSheet("Payroll Summary");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue(string.Empty);
           titleRow.CreateCell(2).SetCellValue(
               "Payroll Detail Summary For Employee: " +
               (reportUser.LastName.IsNotNullOrEmpty() && reportUser.FirstName.IsNotNullOrEmpty() ? reportUser.LastName + ", " + reportUser.FirstName : "Unknown"));
           titleRow.CreateCell(3).SetCellValue("Period: " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString());

           var headerRow = sheet.CreateRow(2);
           headerRow.CreateCell(TaskName).SetCellValue("Task Name");
           headerRow.CreateCell(TaskCount).SetCellValue("Task Count");
           headerRow.CreateCell(TaskTime).SetCellValue("Task Time");
           headerRow.CreateCell(Mileage).SetCellValue("Total Mileage");
           headerRow.CreateCell(Surcharges).SetCellValue("Total Surcharges");

           if (summary != null && summary.Count > 0)
           {
               summary.ForEach<PayrollDetailSummaryItem>(summaryItem =>
               {
                   var row = sheet.CreateRow(rowNumber);
                   var col = 0;

                   ICell cell = row.CreateCell(col++);
                   cell.SetCellValue(summaryItem.TaskName);

                   if (summaryItem.TaskCount.IsInteger())
                   {
                       row.CreateCell(col++).SetCellValue(summaryItem.TaskCount.ToInteger());
                   }
                   else
                   {
                       row.CreateCell(col++).SetCellValue(string.Empty);
                   }
                   row.CreateCell(col++).SetCellValue(summaryItem.TotalTaskTimeFormatted);
                   row.CreateCell(col++).SetCellValue(summaryItem.TotalMileageFormatted);
                   row.CreateCell(col++).SetCellValue(summaryItem.TotalSurchargesFormatted);

                   rowNumber++;

                   totalTasks += summaryItem.TaskCount.ToInteger();

                   totalHours += (int)summaryItem.TotalTaskTime.TotalHours;
                   totalMinutes += summaryItem.TotalTaskTime.Minutes;
                   if (totalMinutes >= 60)
                   {
                       totalHours++;
                       totalMinutes -= 60;
                   }
                   if (totalHours >= 24)
                   {
                       totalDays++;
                       totalHours -= 24;
                   }

                   totalMileage += summaryItem.TotalMileage;
                   totalSurcharges += summaryItem.TotalSurcharges;
               });
           }

           sheet.CreateRow(rowNumber++);
           var summaryRow = sheet.CreateRow(rowNumber);
           summaryRow.CreateCell(TaskName).SetCellValue("Report Totals:");
           summaryRow.CreateCell(TaskCount).SetCellValue(totalTasks);

           string totalTimeStr = totalDays > 0 ? totalDays.ToString() + "bDays, " : string.Empty;
           totalTimeStr += totalHours.ToString() + " Hours, " + totalMinutes.ToString() + " Min.";
           summaryRow.CreateCell(TaskTime).SetCellValue(totalTimeStr);
           summaryRow.CreateCell(Mileage).SetCellValue(string.Format("{0:0.00}", totalMileage));
           ICell c = summaryRow.CreateCell(Surcharges);
           c.SetCellValue(string.Format("{0:C}", totalSurcharges));

           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PayrollDetailSummary_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEmployeeVisitByDateRange(List<UserVisit> employeeVisits, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee Visit By Date Range";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeeVisitByDateRange");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee Visit By Date Range");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Employee");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Task");
           headerRow.CreateCell(3).SetCellValue("Schedule Date");
           headerRow.CreateCell(4).SetCellValue("Visit Date");
           headerRow.CreateCell(5).SetCellValue("Status");


           if (employeeVisits != null && employeeVisits.Count > 0)
           {
               int rowNumber = 2;
               foreach (var visit in employeeVisits)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(visit.UserDisplayName);
                   row.CreateCell(1).SetCellValue(visit.PatientName);
                   row.CreateCell(2).SetCellValue(visit.TaskName);

                   if (visit.ScheduleDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(visit.ScheduleDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   if (visit.VisitDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(visit.VisitDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }

                   row.CreateCell(5).SetCellValue(visit.StatusName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit By Date Range: {0}", employeeVisits.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeeVisitByDateRange_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportEmployeePermissionsListing(List<User> users)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee Permissions Listing";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeePermissionListing");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee Permissions Listing");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Employee");
           headerRow.CreateCell(1).SetCellValue("Permission");


           if (users != null && users.Count > 0)
           {
               int userNumber = 0;
               int rowNumber = 2;
               Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
               users.ForEach(u =>
               {
                   if (u.Permissions.IsNotNullOrEmpty())
                   {
                       var row = sheet.CreateRow(rowNumber);

                       var rowCellStyle = workbook.CreateCellStyle();
                       rowCellStyle.BorderTop = BorderStyle.THIN;
                       row.RowStyle = rowCellStyle;

                       var cell = row.CreateCell(0);
                       var cellStyle = workbook.CreateCellStyle();
                       cell.SetCellValue(string.Format("{0}, {1} {2}", u.LastName, u.FirstName, u.CustomId.IsNotNullOrEmpty() ? " - " + u.CustomId : ""));

                       foreach (var permission in permissions)
                       {
                           ulong id = (ulong)permission;
                           u.PermissionsArray = u.Permissions.ToObject<List<string>>();
                           if (u.PermissionsArray.Contains(id.ToString()))
                           {
                               rowNumber++;
                               var permissionRow = sheet.CreateRow(rowNumber);
                               permissionRow.CreateCell(0);
                               permissionRow.CreateCell(1).SetCellValue(permission.GetDescription());
                           }
                       }
                       rowNumber = rowNumber + 2;
                       userNumber = userNumber + 1;
                   }
                  });
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of  Employee: {0}", userNumber));
           }

           workbook.FinishWritingToExcelSpreadsheet(2);

           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeePermissionsListing_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportStatisticalPatientAdmissionsByInternalReferralSource(List<PatientAdmission> admissions, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Admissions By Internal Referral Source";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientAdmissionsByInternalReferralSource");

           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Admissions By Internal Referral Source");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("MRN");
           headerRow.CreateCell(3).SetCellValue("Phone");
           headerRow.CreateCell(4).SetCellValue("Admit");
           headerRow.CreateCell(5).SetCellValue("SOC");
           headerRow.CreateCell(6).SetCellValue("D/C");
           headerRow.CreateCell(7).SetCellValue("Insurance");
           headerRow.CreateCell(8).SetCellValue("Primary Physician");
           List<int> headerIndexes = new List<int>();
           List<int> groupTotalIndexes = new List<int>();

           if (admissions != null && admissions.Count > 0)
           {
               int totalAdmitCount = 0;
               int totalNonAdmitCount = 0;
               int totalPendingCount = 0;
               headerIndexes.Add(1);
               int rowNumber = 2;
               var groupAdmissions = admissions.GroupBy(g => g.InternalReferral);
               //groupAdmissions
               foreach (var group in groupAdmissions)
               {
                   int admitCount = 0;
                   int nonAdmitCount = 0;
                   int pendingCount = 0;
                   var groupRow = sheet.CreateRow(rowNumber);
                   var element = group.ElementAtOrDefault(0);
                   groupRow.CreateCell(0).SetCellValue(element != null ? element.InternalReferralName : "");
                   for (int i = 1; i < 9; i++)
                   {
                       groupRow.CreateCell(i);
                   }
                   headerIndexes.Add(rowNumber);
                   rowNumber++;
                   foreach (var admission in group)
                   {
                       var row = sheet.CreateRow(rowNumber);
                       row.CreateCell(1).SetCellValue(admission.DisplayName);
                       row.CreateCell(2).SetCellValue(admission.PatientId);
                       row.CreateCell(3).SetCellValue(admission.PhoneHome);
                       row.CreateCell(4).SetCellValue(admission.Admit);

                       if (admission.StartOfCareDateFormatted.IsDate())
                       {
                           var createdDateCell = row.CreateCell(5);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(admission.StartOfCareDate);
                       }
                       else
                       {
                           row.CreateCell(5).SetCellValue(admission.StartOfCareDateFormatted);
                       }

                       if (admission.DischargedDateFormatted.IsDate())
                       {
                           var createdDateCell = row.CreateCell(6);
                           createdDateCell.CellStyle = dateStyle;
                           createdDateCell.SetCellValue(admission.DischargedDate);
                       }
                       else
                       {
                           row.CreateCell(6).SetCellValue(admission.DischargedDateFormatted);
                       }

                       row.CreateCell(7).SetCellValue(admission.InsuranceName);
                       row.CreateCell(8).SetCellValue(admission.PhysicianName);

                       if (admission.Status == (int)PatientStatus.Active)
                       {
                           admitCount++;
                       }
                       else if (admission.Status == (int)PatientStatus.NonAdmission)
                       {
                           nonAdmitCount++;
                       }
                       else if (admission.Status == (int)PatientStatus.Pending)
                       {
                           pendingCount++;
                       }

                       rowNumber++;
                   }
                   totalAdmitCount += admitCount;
                   totalNonAdmitCount += nonAdmitCount;
                   totalPendingCount += pendingCount;

                   groupTotalIndexes.Add(rowNumber);
                   var groupTotalRow = sheet.CreateRow(rowNumber);
                   groupTotalRow.CreateCell(0).SetCellValue("Patients: " + group.Count());
                   groupTotalRow.CreateCell(1).SetCellValue("Admits: " + admitCount);
                   groupTotalRow.CreateCell(2).SetCellValue("Non-Admits: " + nonAdmitCount);
                   groupTotalRow.CreateCell(3).SetCellValue("Pending: " + pendingCount);
                   rowNumber++;
                   sheet.CreateRow(rowNumber);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Patients: {0}", admissions.Count));
               totalRow.CreateCell(1).SetCellValue(string.Format("Total Admits: {0}", totalAdmitCount));
               totalRow.CreateCell(2).SetCellValue(string.Format("Total Non-Admits: {0}", totalNonAdmitCount));
               totalRow.CreateCell(3).SetCellValue(string.Format("Total Pending: {0}", totalPendingCount));
           }
           workbook.FinishWritingToGroupedExcelSpreadsheet(9, headerIndexes, groupTotalIndexes);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientAdmissionsByInternalReferralSource_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public FileContentResult ExportStatisticalPatientVisitHistory(List<TaskLean> scheduleEvents, Guid PatientId, DateTime StartDate, DateTime EndDate, string patientName)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Visit History";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientVisitHistory");

           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Visit History");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(4).SetCellValue(string.Format("Patient Name:{0}", patientName));

           var dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Task");
           headerRow.CreateCell(1).SetCellValue("Status");
           headerRow.CreateCell(2).SetCellValue("Schedule Date");
           headerRow.CreateCell(3).SetCellValue("Visit Date");
           headerRow.CreateCell(4).SetCellValue("Employee");


           if (scheduleEvents != null && scheduleEvents.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in scheduleEvents)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                   row.CreateCell(1).SetCellValue(evnt.StatusName);

                   if (evnt.EventDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(2);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.EventDate +" "+ evnt.EventDateTimeRange );
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }
                   if (evnt.VisitDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.VisitDate + " " + evnt.VisitDateTimeRange);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   row.CreateCell(4).SetCellValue(evnt.UserName);

                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Visit History: {0}", scheduleEvents.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientVisitHistory_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportStatisticalEmployeeVisitHistory(List<UserVisit> scheduleEvents, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Employee Visit History";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("EmployeeVisitHistory");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Employee Visit History");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Task");
           headerRow.CreateCell(1).SetCellValue("Status");
           headerRow.CreateCell(2).SetCellValue("Schedule Date");
           headerRow.CreateCell(3).SetCellValue("Visit Date");
           headerRow.CreateCell(4).SetCellValue("Patient");


           if (scheduleEvents != null && scheduleEvents.Count > 0)
           {
               int rowNumber = 2;
               foreach (var evnt in scheduleEvents)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(evnt.TaskName);
                   row.CreateCell(1).SetCellValue(evnt.StatusName);

                   if (evnt.ScheduleDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(2);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.ScheduleDate);
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }
                   if (evnt.VisitDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(evnt.VisitDate);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }

                   row.CreateCell(4).SetCellValue(evnt.PatientName);

                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit History: {0}", scheduleEvents.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(5);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportStatisticalMonthlyAdmission(List<PatientRoster> patients, int StatusId, int Month, int Year)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Monthly Admission List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientMonthlyAdmission");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Monthly Admission List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Month/Year : {0}/{1}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty, Year));
           titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("Patient");
           headerRow.CreateCell(1).SetCellValue("Admission Source");
           headerRow.CreateCell(2).SetCellValue("Admission Date");
           headerRow.CreateCell(3).SetCellValue("Status");
           headerRow.CreateCell(4).SetCellValue("Other Referral Source");
           headerRow.CreateCell(5).SetCellValue("Internal Referral");
           headerRow.CreateCell(6).SetCellValue("Referrer Physician");
           headerRow.CreateCell(7).SetCellValue("Referral Date");

           if (patients != null && patients.Count > 0)
           {
               int rowNumber = 2;
               foreach (var birthDay in patients)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(birthDay.PatientDisplayName);
                   row.CreateCell(1).SetCellValue(birthDay.AdmissionSourceName);
                   if (birthDay.PatientSoC.IsValid())
                   {
                       var createdDateCell = row.CreateCell(2);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(birthDay.PatientSoC);
                   }
                   else
                   {
                       row.CreateCell(2).SetCellValue(string.Empty);
                   }

                   row.CreateCell(3).SetCellValue(birthDay.PatientStatusName);
                   row.CreateCell(4).SetCellValue(birthDay.OtherReferralSource);
                   row.CreateCell(5).SetCellValue(birthDay.InternalReferral);
                   row.CreateCell(6).SetCellValue(birthDay.ReferrerPhysician);
                   if (birthDay.ReferralDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(7);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(birthDay.ReferralDate);
                   }
                   else
                   {
                       row.CreateCell(7).SetCellValue(string.Empty);
                   }

                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Admission List: {0}", patients.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(9);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientMonthlyAdmission_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportStatisticalAnnualAdmission(List<PatientRoster> patients, int StatusId, int Year)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Annual Admission List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("PatientAnnualAdmission");

           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Annual Admission List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Year : {0}", Year));
           titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Admission Source");
           headerRow.CreateCell(3).SetCellValue("Admission Date");

           if (patients != null && patients.Count > 0)
           {
               int rowNumber = 2;
               foreach (var birthDay in patients)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(birthDay.PatientId);
                   row.CreateCell(1).SetCellValue(birthDay.PatientDisplayName);
                   row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                   if (birthDay.PatientSoC.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(birthDay.PatientSoC);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Annual Admission List: {0}", patients.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(4);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("PatientAnnualAdmission_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportStatisticalUnduplicatedCensusReport(List<PatientRoster> patients, int StatusId, DateTime StartDate, DateTime EndDate)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Patient Unduplicated Census List";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("UnduplicatedCensusReport");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Patient Unduplicated Census List");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

           ICellStyle dateStyle = workbook.CreateCellStyle();
           dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Admission Source");
           headerRow.CreateCell(3).SetCellValue("Admission Date");
           headerRow.CreateCell(4).SetCellValue("Discharge Date");
           headerRow.CreateCell(5).SetCellValue("Status");
           if (patients != null && patients.Count > 0)
           {
               int rowNumber = 2;
               foreach (var birthDay in patients)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(birthDay.PatientId);
                   row.CreateCell(1).SetCellValue(birthDay.PatientDisplayName);
                   row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                   if (birthDay.PatientSoC.IsValid())
                   {
                       var createdDateCell = row.CreateCell(3);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(birthDay.PatientSoC);
                   }
                   else
                   {
                       row.CreateCell(3).SetCellValue(string.Empty);
                   }
                   if (birthDay.PatientDischargeDate.IsValid())
                   {
                       var createdDateCell = row.CreateCell(4);
                       createdDateCell.CellStyle = dateStyle;
                       createdDateCell.SetCellValue(birthDay.PatientDischargeDate);
                   }
                   else
                   {
                       row.CreateCell(4).SetCellValue(string.Empty);
                   }
                   row.CreateCell(5).SetCellValue(birthDay.PatientStatusName);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Unduplicated Census List: {0}", patients.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(6);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("UnduplicatedCensusReport_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }

       public  FileContentResult ExportCensusByPrimaryInsurance(List<PatientRoster> patientRosters, int InsuranceId, int StatusId)
       {
           var workbook = new HSSFWorkbook();
           var si = PropertySetFactory.CreateSummaryInformation();
           si.Subject = "Axxess Data Export - Census By Primary Insurance";
           workbook.SummaryInformation = si;
           var sheet = workbook.CreateSheet("CensusByPrimaryInsurance");
           var titleRow = sheet.CreateRow(0);
           titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
           titleRow.CreateCell(1).SetCellValue("Census By Primary Insurance");
           titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
           titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));
           var insuranceModel = InsuranceEngine.Get(InsuranceId, Current.AgencyId);
           if (insuranceModel != null)
           {
               titleRow.CreateCell(4).SetCellValue(string.Format("Insurance : {0}", insuranceModel.Name));
           }
           var headerRow = sheet.CreateRow(1);
           headerRow.CreateCell(0).SetCellValue("MRN");
           headerRow.CreateCell(1).SetCellValue("Patient");
           headerRow.CreateCell(2).SetCellValue("Address");
           headerRow.CreateCell(3).SetCellValue("City");
           headerRow.CreateCell(4).SetCellValue("State");
           headerRow.CreateCell(5).SetCellValue("Zip Code");
           headerRow.CreateCell(6).SetCellValue("Home Phone");
           headerRow.CreateCell(7).SetCellValue("Gender");
           if (patientRosters != null && patientRosters.Count > 0)
           {
               int rowNumber = 2;
               foreach (var patient in patientRosters)
               {
                   var row = sheet.CreateRow(rowNumber);
                   row.CreateCell(0).SetCellValue(patient.PatientId);
                   row.CreateCell(1).SetCellValue(patient.PatientDisplayName);
                   row.CreateCell(2).SetCellValue(patient.PatientAddressLine1);
                   row.CreateCell(3).SetCellValue(patient.PatientAddressCity);
                   row.CreateCell(4).SetCellValue(patient.PatientAddressStateCode);
                   row.CreateCell(5).SetCellValue(patient.PatientAddressZipCode);
                   row.CreateCell(6).SetCellValue(patient.PatientPhone);
                   row.CreateCell(7).SetCellValue(patient.PatientGender);
                   rowNumber++;
               }
               var totalRow = sheet.CreateRow(rowNumber + 2);
               totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Census By Primary Insurance: {0}", patientRosters.Count));
           }
           workbook.FinishWritingToExcelSpreadsheet(8);
           MemoryStream output = new MemoryStream();
           workbook.Write(output);
           HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + string.Format("CensusByPrimaryInsurance_{0}.xls", DateTime.Now.Ticks));
           return new FileContentResult(output.ToArray(), "application/vnd.ms-excel");
       }


    }

   public class HHReportExcelGenerator : ReportExcelGenerator
   {
       public HHReportExcelGenerator()
           :base(AgencyServices.HomeHealth)
       {
       }
   }

   public class PDReportExcelGenerator : ReportExcelGenerator
   {
       public PDReportExcelGenerator()
           : base(AgencyServices.PrivateDuty)
       {
       }
   }
}
