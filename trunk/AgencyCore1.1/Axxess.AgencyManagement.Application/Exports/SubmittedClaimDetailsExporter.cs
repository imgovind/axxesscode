﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Enums;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Entities;
    public class SubmittedClaimDetailsExporter : BaseExporter
    {
        private IList<ClaimInfoDetail> batchClaims;
        private int batchId;
        public SubmittedClaimDetailsExporter(IList<ClaimInfoDetail> batchClaims, int batchId)
            : base()
        {
            this.batchClaims = batchClaims;
            this.batchId = batchId;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Submitted Claims Detail";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("SubmittedClaimsDetail");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Submitted Claims Detail");
            titleRow.CreateCell(2).SetCellValue("Batch Id : "+ this.batchId);

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Bill Type");

            if (this.batchClaims.Count > 0)
            {
                int i = 2;
                this.batchClaims.ForEach(claim =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(claim.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(claim.DisplayName);
                    row.CreateCell(2).SetCellValue(claim.Range);
                    row.CreateCell(3).SetCellValue(claim.BillType);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Details: {0}", batchClaims.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(4);
        }
    }
}
