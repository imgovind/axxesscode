﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class OasisExporter : BaseExporter
    {
         private IList<AssessmentExport> assessments;
         public OasisExporter(IList<AssessmentExport> assessments)
            : base()
        {
            this.assessments = assessments;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Export Ready OASIS";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("ExportReadyOASIS");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Export Ready OASIS");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Assessment Type");
            headerRow.CreateCell(2).SetCellValue("Assessment Date");
            headerRow.CreateCell(3).SetCellValue("Episode");
            headerRow.CreateCell(4).SetCellValue("Insurance");
            headerRow.CreateCell(5).SetCellValue("Correction Number");

            if (this.assessments.Count > 0)
            {
                int i = 2;
                this.assessments.ForEach(a =>
                {
                    var dataRow = sheet.CreateRow(i);
                    var insurance = InsuranceEngine.Get(a.InsuranceId, Current.AgencyId);
                    if (insurance != null) { a.Insurance = insurance.Name; }
                    dataRow.CreateCell(0).SetCellValue(a.PatientName);
                    dataRow.CreateCell(1).SetCellValue(a.AssessmentName);
                    if (a.AssessmentDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(a.AssessmentDate);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(3).SetCellValue(a.EpisodeRange);
                    dataRow.CreateCell(4).SetCellValue(a.Insurance);
                    dataRow.CreateCell(5).SetCellValue(a.CorrectionNumber);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Export Ready OASIS: {0}", assessments.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
