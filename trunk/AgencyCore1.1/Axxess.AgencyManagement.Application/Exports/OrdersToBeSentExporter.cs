﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Extensions;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class OrdersToBeSentExporter :BaseExporter
    {

        private IList<Order> orders;
        private DateTime StartDate;
        private DateTime EndDate;
        private bool sendAutomatically;
        public OrdersToBeSentExporter(IList<Order> orders, bool sendAutomatically, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.orders = orders;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.sendAutomatically = sendAutomatically;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Orders To Be Sent";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("OrdersToBeSent");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Orders To Be Sent");
            titleRow.CreateCell(2).SetCellValue(string.Format("Type :  {0}", sendAutomatically ? "Electronic Orders" : "Manual Orders (Fax, Mail, etc)"));
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Physician");
            headerRow.CreateCell(4).SetCellValue("Order Date");

            if (this.orders.Count > 0)
            {
                int i = 2;
                this.orders.ForEach(order =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(order.Number);
                    row.CreateCell(1).SetCellValue(order.PatientName);
                    row.CreateCell(2).SetCellValue(order.Text);
                    row.CreateCell(3).SetCellValue(order.PhysicianName);
                    if (order.CreatedDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(order.CreatedDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of OrdersToBeSent: {0}", orders.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(5);

        }

    }
}
