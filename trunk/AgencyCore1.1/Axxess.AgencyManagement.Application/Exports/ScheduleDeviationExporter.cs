﻿namespace Axxess.AgencyManagement.Application.Exports
{
     using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class ScheduleDeviationExporter : BaseExporter
    {
       private List<TaskLean> scheduleEvents;
        private DateTime StartDate;
        private DateTime EndDate;
        public ScheduleDeviationExporter(List<TaskLean> scheduleEvents, DateTime startDate, DateTime endDate)
            : base()
        {
            this.scheduleEvents = scheduleEvents;
            StartDate = startDate;
            EndDate = endDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Schedule Deviations";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("ScheduleDeviations");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Deviations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("MRN");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Assigned To");
            headerRow.CreateCell(5).SetCellValue("Schedule Date");
            headerRow.CreateCell(6).SetCellValue("Visit Date");

            if (this.scheduleEvents.Count > 0)
            {
                int i = 2;
                this.scheduleEvents.ForEach(evnt =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    dataRow.CreateCell(1).SetCellValue(evnt.PatientName);
                    dataRow.CreateCell(2).SetCellValue(evnt.PatientIdNumber);
                    dataRow.CreateCell(3).SetCellValue(evnt.StatusName);
                    dataRow.CreateCell(4).SetCellValue(evnt.UserName);
                    DateTime eventDate = evnt.EventDate;
                    if (eventDate.IsValid())
                    {
                        var createdDateCell = dataRow.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(eventDate);
                    }
                    else
                    {
                        dataRow.CreateCell(5).SetCellValue(string.Empty);
                    }
                    DateTime visitDate = evnt.VisitDate;
                    if (visitDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visitDate);
                    }
                    else
                    {
                        dataRow.CreateCell(6).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviations: {0}", scheduleEvents.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(7);
        }
    }
}
