﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Extensions;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class PpsVisitExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid agencyId;
        private string agencyName;
        private Guid branchId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public PpsVisitExporter(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, string agencyName)
            : base()
        {
            this.agencyId = agencyId;
            this.agencyName = agencyName;
            this.branchId = branchId;
            this.endDate = endDate;
            this.startDate = startDate;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "PPSLogVisitInformation.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Log Visit Information";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("VisitInformation");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            List<Dictionary<string, string>> data = reportAgent.PPSVisitInformation(agencyId, branchId, startDate, endDate);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                var titleRow = sheet.CreateRow(0);
                titleRow.CreateCell(0).SetCellValue(agencyName);
                titleRow.CreateCell(1).SetCellValue("PPS Visit Information Report");
                titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
                titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.startDate.ToString("MM/dd/yyyy"), this.endDate.ToString("MM/dd/yyyy")));

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }

                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            bool isCreated = false;
                            if (value.IsNotNullOrEmpty())
                            {
                                if (colIndex == 4 || colIndex == 5 || colIndex == 6)
                                {
                                    if (value.IsDate())
                                    {
                                        if (value.IsValidDateAndNotMin())
                                        {
                                            var createdDateCell = dataRow.CreateCell(colIndex);
                                            createdDateCell.CellStyle = dateStyle;
                                            createdDateCell.SetCellValue(value.ToDateTime());
                                            isCreated = true;
                                        }
                                    }
                                }
                                else if (colIndex >= 7)
                                {
                                    if (value.IsNotNullOrEmpty() && value.IsDouble())
                                    {
                                        dataRow.CreateCell(colIndex).SetCellValue(value.ToDouble());
                                        isCreated = true;
                                    }
                                }
                            }

                            if (!isCreated)
                            {
                                dataRow.CreateCell(colIndex).SetCellValue(value);
                            }
                        }
                        else
                        {
                            dataRow.CreateCell(colIndex).SetCellValue(string.Empty);
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Visit Information found!");
                workBook.FinishWritingToExcelSpreadsheet(1);
            }
        }

        #endregion

    }
}
