﻿namespace Axxess.AgencyManagement.Application.Exports
{

    using System.Collections.Generic;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;


    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;

    public class SupplyExporter : BaseExporter
    {
        private IList<AgencySupply> supplies;
        public SupplyExporter(IList<AgencySupply> supplies)
            : base()
        {
            this.supplies = supplies;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Supplies";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("AgencySupplies");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = base.workBook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var wrapStyle = base.workBook.CreateCellStyle();
            wrapStyle.WrapText = true;

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Supplies");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Description");
            headerRow.CreateCell(1).SetCellValue("HCPCS");
            headerRow.CreateCell(2).SetCellValue("Rev Code");
            headerRow.CreateCell(3).SetCellValue("Unit Cost");
            headerRow.CreateCell(4).SetCellValue("Created Date");

            if (this.supplies.Count > 0)
            {
                int i = 2;
                this.supplies.ForEach(s =>
                {
                    var dataRow = sheet.CreateRow(i);
                    var descriptionCell = dataRow.CreateCell(0);
                    descriptionCell.CellStyle = wrapStyle;
                    descriptionCell.SetCellValue(s.Description);

                    dataRow.CreateCell(1).SetCellValue(s.Code);
                    dataRow.CreateCell(2).SetCellValue(s.RevenueCode);

                    var unitCostCell = dataRow.CreateCell(3);
                    unitCostCell.CellStyle = currencyStyle;
                    unitCostCell.SetCellValue(s.UnitCost);

                    if (s.Created != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(s.Created);
                    }
                    else
                    {
                        dataRow.CreateCell(4).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var emptyRow = sheet.CreateRow(i + 1);
                emptyRow.HeightInPoints = 15;
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Supplies: {0}", supplies.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(5);
        }
    }
}
