﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class InfectionExporter: BaseExporter
    {
       private IList<Infection> infections;
       public InfectionExporter(IList<Infection> infections)
            : base()
        {
            this.infections = infections;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Infections";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Infections");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Infections");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Physician");
            headerRow.CreateCell(2).SetCellValue("Treatment/Antibiotic");
            headerRow.CreateCell(3).SetCellValue("Type Of Incident");
            headerRow.CreateCell(4).SetCellValue("MD Notified");
            headerRow.CreateCell(5).SetCellValue("Incident Date");
            headerRow.CreateCell(6).SetCellValue("Status");

            if (this.infections.Count > 0)
            {
                int i = 2;
                this.infections.ForEach(incident =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(incident.PatientName);
                    dataRow.CreateCell(1).SetCellValue(incident.PhysicianName);
                    dataRow.CreateCell(2).SetCellValue(incident.Treatment);
                    dataRow.CreateCell(3).SetCellValue(incident.InfectionType);
                    dataRow.CreateCell(4).SetCellValue(incident.MDNotified);
                    if (incident.InfectionDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(incident.InfectionDate);
                    }
                    else
                    {
                        dataRow.CreateCell(5).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(6).SetCellValue(incident.StatusName);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Infections: {0}", infections.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(6);
           
        }
    }
}
