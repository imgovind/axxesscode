﻿
namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class OrderExport : BaseExporter
    {
         private IList<Order> orders;
         private DateTime StartDate;
         private DateTime EndDate;
         public OrderExport(IList<Order> orders, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.orders = orders;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Orders";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Orders");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Orders");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Physician");
            headerRow.CreateCell(4).SetCellValue("Order Date");
            headerRow.CreateCell(5).SetCellValue("Sent Date");
            headerRow.CreateCell(6).SetCellValue("Received Date");
            headerRow.CreateCell(7).SetCellValue("MD Sign Date");

            if (this.orders.Count > 0)
            {
                int i = 2;
                this.orders.ForEach(order =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(order.Number);
                    row.CreateCell(1).SetCellValue(order.PatientName);
                    row.CreateCell(2).SetCellValue(order.Text);
                    row.CreateCell(3).SetCellValue(order.PhysicianName);
                    if (order.CreatedDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(order.CreatedDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (order.SendDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(order.SendDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    if (order.ReceivedDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(order.ReceivedDate);
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }
                    if (order.PhysicianSignatureDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(7);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(order.PhysicianSignatureDate);
                    }
                    else
                    {
                        row.CreateCell(7).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Orders: {0}", orders.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(7);
        }
    }
}
