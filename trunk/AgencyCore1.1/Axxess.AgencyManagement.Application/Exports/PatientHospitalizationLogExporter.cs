﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Extensions;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;
using Axxess.AgencyManagement.Application.ViewData;

    class PatientHospitalizationLogExporter:BaseExporter
    {
        private HospitalizationViewData ViewData;

        public PatientHospitalizationLogExporter(HospitalizationViewData viewData)
            : base()
        {
            this.ViewData = viewData ?? new HospitalizationViewData(); ;
        }
        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - " + this.ViewData.PatientName + " Hospitalization Log";
            base.workBook.SummaryInformation = si;
        }
        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("PatientHospitalizationLog");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue(this.ViewData.PatientName+" Hospitalization Log");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Source");
            headerRow.CreateCell(1).SetCellValue("User");
            headerRow.CreateCell(2).SetCellValue("Date");

            if (this.ViewData.Logs.Count > 0)
            {
                int i = 2;
                this.ViewData.Logs.ForEach(h =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(h.Source);
                    dataRow.CreateCell(1).SetCellValue(h.UserName);
                    if (h.HospitalizationDate!=null&&h.HospitalizationDate!= DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(h.HospitalizationDate);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Hospitalization Log: {0}", ViewData.Logs.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(3);
        }
    }
}
