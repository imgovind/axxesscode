﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Entities.Extensions;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class PpsPaymentExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid agencyId;
        private string agencyName;
        private Guid branchId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public PpsPaymentExporter(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, string agencyName)
            : base()
        {
            this.agencyId = agencyId;
            this.agencyName = agencyName;
            this.branchId = branchId;
            this.endDate = endDate;
            this.startDate = startDate;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "PPSLogPaymentInformation.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Log Payment Information";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("PaymentInformation");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = base.workBook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            List<Dictionary<string, string>> data = reportAgent.PPSPaymentInformation(agencyId, branchId, startDate, endDate);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                var titleRow = sheet.CreateRow(0);
                titleRow.CreateCell(0).SetCellValue(agencyName);
                titleRow.CreateCell(1).SetCellValue("PPS Payment Information Report");
                titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
                titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.startDate.ToString("MM/dd/yyyy"), this.endDate.ToString("MM/dd/yyyy")));

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }

                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            bool isCreated = false;
                            if (colIndex == 4 || colIndex == 5 || colIndex == 6 || colIndex == 7 || colIndex == 9 || colIndex == 12)
                            {
                                DateTime parsedDate = DateTime.MinValue;
                                bool successfullyParsedDate = DateTime.TryParse(value, out parsedDate);
                                if (successfullyParsedDate)
                                {
                                    if (parsedDate != DateTime.MinValue)
                                    {
                                        var createdDateCell = dataRow.CreateCell(colIndex);
                                        createdDateCell.CellStyle = dateStyle;
                                        createdDateCell.SetCellValue(parsedDate);
                                        isCreated = true;
                                    }
                                }
                                if (!isCreated)
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue(string.Empty);
                                }
                            }
                            if (!isCreated)
                            {
                                if (value.IsNotNullOrEmpty() && value.StartsWith("$"))
                                {
                                    string currencyValue = value.RemoveAt(0);
                                    var createdCurrencyCell = dataRow.CreateCell(colIndex);
                                    createdCurrencyCell.CellStyle = currencyStyle;
                                    createdCurrencyCell.SetCellValue(double.Parse(currencyValue));
                                }
                                else
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue(value);
                                }
                            }
                        }
                        else
                        {
                            dataRow.CreateCell(colIndex).SetCellValue(string.Empty);
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });

                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Payment Information found!");
                workBook.FinishWritingToExcelSpreadsheet(1);
            }
        }

        #endregion

    }
}
