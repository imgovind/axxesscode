﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Api.Contracts;
    using System.Linq.Expressions;

    public class OasisValidation : Validation
    {
        public ValidationError ValidationError { get; set; }
        public OasisValidation(Expression<Func<bool>> expression, ValidationError validationError) : base(expression, "")
        {
            ValidationError = validationError;
        }
    }
}
