﻿namespace Axxess.AgencyManagement.Application.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Extensions;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class ContactExporter : BaseExporter
    {
        private IList<AgencyContact> contacts;
        public ContactExporter(IList<AgencyContact> contacts)
            : base()
        {
            this.contacts = contacts;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Contacts";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Contacts");
           
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);

            titleRow.CreateCell(1).SetCellValue("Agency Contacts");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Last Name");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Company");
            headerRow.CreateCell(4).SetCellValue("Address");
            headerRow.CreateCell(5).SetCellValue("City");
            headerRow.CreateCell(6).SetCellValue("State");
            headerRow.CreateCell(7).SetCellValue("Zip Code");
            headerRow.CreateCell(8).SetCellValue("Office Phone");
            headerRow.CreateCell(9).SetCellValue("Mobile Phone");
            headerRow.CreateCell(10).SetCellValue("Fax Number");
            headerRow.CreateCell(11).SetCellValue("Email Address");
            sheet.CreateFreezePane(0, 2, 0, 2);
            int i = 2;
            if (this.contacts.Count > 0)
            {
                this.contacts.ForEach(c =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(c.LastName);
                    dataRow.CreateCell(1).SetCellValue(c.FirstName);
                    dataRow.CreateCell(2).SetCellValue(c.ContactType);
                    dataRow.CreateCell(3).SetCellValue(c.CompanyName);
                    dataRow.CreateCell(4).SetCellValue(c.AddressFirstRow);
                    dataRow.CreateCell(5).SetCellValue(c.AddressCity);
                    dataRow.CreateCell(6).SetCellValue(c.AddressStateCode);
                    dataRow.CreateCell(7).SetCellValue(c.AddressZipCode);
                    dataRow.CreateCell(8).SetCellValue(c.PhonePrimaryExtended);
                    dataRow.CreateCell(9).SetCellValue(c.PhoneAlternate.ToPhone());
                    dataRow.CreateCell(10).SetCellValue(c.FaxNumberFormatted);
                    dataRow.CreateCell(11).SetCellValue(c.EmailAddress);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                var totalCell = totalRow.CreateCell(0);
                totalCell.SetCellValue(string.Format("Total Number Of Agency Contacts: {0}", contacts.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(12);
        }
    }
}
