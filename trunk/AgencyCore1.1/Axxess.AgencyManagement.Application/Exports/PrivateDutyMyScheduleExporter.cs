﻿namespace Axxess.AgencyManagement.Application.Exports
{

    using System.Collections.Generic;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;


    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;

    public class PrivateDutyMyScheduleExporter : BaseExporter
    {
        private IList<PrivateDutyScheduleTask> userVisits;
        public PrivateDutyMyScheduleExporter(IList<PrivateDutyScheduleTask> userVisits)
            : base()
        {
            this.userVisits = userVisits;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - My Schedule Tasks";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("MyPrivateDutyScheduleTasks");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Private Duty - My Schedule Tasks");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Time");
            headerRow.CreateCell(4).SetCellValue("Status");

            if (this.userVisits.Count > 0)
            {
                int i = 2;
                this.userVisits.ForEach(u =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.PatientName);
                    dataRow.CreateCell(1).SetCellValue(u.DisciplineTaskName.ToTitleCase());
                    DateTime visitDate = u.VisitStartTime;
                    if (visitDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visitDate);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }

                    if (u.VisitStartTime.IsValid() && u.VisitEndTime.IsValid())
                    {
                        var createdDateCell = dataRow.CreateCell(3);
                        createdDateCell.SetCellValue(!u.IsAllDay ? u.VisitStartTime.ToShortTimeString() + " - " + u.VisitEndTime.ToShortTimeString() : "All Day");
                    }
                    else
                    {
                        dataRow.CreateCell(3).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(4).SetCellValue(u.StatusName);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of My Schedule Tasks: {0}", userVisits.Count));
            }

            workBook.FinishWritingToExcelSpreadsheet(5);
        }
    }
}
