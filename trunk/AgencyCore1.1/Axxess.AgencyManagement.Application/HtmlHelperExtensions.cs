﻿#define DEBUG
// ReSharper disable SpecifyACultureInStringConversionExplicitly
namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using StructureMap.Diagnostics;

    using Container = Axxess.Core.Infrastructure.Container;

    public static class HtmlHelperExtensions
    {
        #region Data Repositories

        private static IPatientRepository patientRepository
        {
            get
            {
                var dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PatientRepository;
            }
        }

        private static IUserRepository userRepository
        {
            get
            {
                var dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.UserRepository;
            }
        }

        private static IAgencyRepository agencyRepository
        {
            get
            {
                var dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                var dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        #endregion

        #region HtmlHelper Extensions

        [Obsolete("Not being used anywhere. Remove this attribute if used.")]
        public static MvcHtmlString AssemblyVersion(this HtmlHelper html)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            var versionText = string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            return MvcHtmlString.Create(versionText);
        }

        public static MvcHtmlString NotAuthorized(this HtmlHelper html, string permission)
        {
            return html.ErrorMessage("Not Authorized", "You do not have permissions to " + permission + ". Please contact your administrator to get the necessary permissions.");
        }

        public static MvcHtmlString ErrorMessage(this HtmlHelper html, string title, string message)
        {
            return MvcHtmlString.Create("<div class='error-box error inline-error'><div class='logo'><span class='img icon48 fl'></span><h1>" + title + "</h1><p>" + message + "</p><ul class='buttons ac'><li><a class='close'>Close</a></li></ul></div></div>");
        }

        public static MvcHtmlString ZipCode(this HtmlHelper html)
        {
            var zipCode = "75243";
            var mainLocation = agencyRepository.GetMainLocation(Current.AgencyId);
            if (mainLocation != null)
            {
                zipCode = mainLocation.AddressZipCode;
            }
            return MvcHtmlString.Create(zipCode);
        }

        public static MvcHtmlString Asset(this HtmlHelper html, Guid assetId)
        {
            var sb = new StringBuilder();
            if (!assetId.IsEmpty())
            {
                var asset = AssetHelper.Get(Current.AgencyId, assetId);
                if (asset != null)
                {
                    sb.AppendFormat("<a class='link' href='/Asset/{0}'>{1}</a>", asset.Id, asset.FileName);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"recipient-list\" id=\"recipient-list\">");
            sb.AppendLine("<div class=\"compose-header\">Recipient List</div>");
            sb.AppendLine("<div class=\"recipient-panel\">");
            sb.AppendLine("<div class=\"recipient\">");
            sb.AppendLine("<input type=\"checkbox\" class=\"contact\" id=\"NewMessage_SelectAllRecipients\" />");
            sb.AppendLine("<label for=\"NewMessage_SelectAllRecipients\" class=\"strong\">Select All</label>");
            sb.AppendLine("</div>"); // recipient
            //TODO: Discuss with Sonya or John later on as to whether or not recipients should be filterd by service
            var recipients = userRepository.GetUsersByStatusLean(Current.AgencyId, Guid.Empty, (int)UserStatus.Active, 0);
            if (recipients.Count > 0)
            {
                int counter = 1;
                recipients.ForEach(r =>
                {
                    sb.AppendFormat("<div class=\"recipient{0}{1}\">", counter % 2 == 1 ? " t-alt" : string.Empty, r.Id == Current.UserId ? " self" : string.Empty);
                    sb.AppendFormat("<input name=\"Recipients\" type=\"checkbox\" id=\"NewMessage_Recipient_{0}\" value=\"{1}\" title=\"{2}\" />", counter.ToString(), r.Id.ToString(), r.DisplayName);
                    sb.AppendFormat("<label for=\"NewMessage_Recipient_{0}\">{1}</label>", counter.ToString(), r.DisplayName);
                    sb.AppendLine("</div>");
                    sb.AppendLine();
                    counter++;
                });
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html, string prefix, List<Guid> selectedRecipients)
        {
            var sb = new StringBuilder();
            if (selectedRecipients == null)
            {
                selectedRecipients = new List<Guid>();
            }
            sb.AppendLine("<ul class=\"checkgroup four-wide\">");
            //TODO: Discuss with Sonya or John later on as to whether or not recipients should be filterd by service
            var recipients = userRepository.GetUsersByStatusLean(Current.AgencyId, Guid.Empty, (int)UserStatus.Active, 0);
            int count = 0;
            int totalItems = recipients.Count;
            foreach (User u in recipients)
            {
                sb.Append(string.Format("<li class=\"option\"><div class=\"wrapper\"><input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" {4} /><label for=\"{0}_Recipient{1}\">{3}</label></div></li>", prefix, count.ToString(), u.Id.ToString(), u.DisplayName, selectedRecipients.Contains(u.Id) ? " checked" : string.Empty));
                count++;
            }
            sb.AppendLine("</ul>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Templates(this HtmlHelper html, string name, object htmlAttributes)
        {
            var items = new List<SelectListItem> { new SelectListItem { Text = "-- Select Template --", Value = string.Empty }, new SelectListItem { Text = "-- Erase --", Value = "empty" } };
            var templates = TemplateEngine.GetTemplates(Current.AgencyId);
            if (templates != null && templates.Count > 0)
            {
                items.Insert(items.Count, new SelectListItem { Text = "------------------------------", Value = "spacer" });
                templates.ForEach(t => items.Add(new SelectListItem { Text = t.Title, Value = t.Id.ToString() }));
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString ToggleTemplates(this HtmlHelper html, string name)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<span class=\"button\"><a>Load Template</a></span>");
            sb.AppendFormat("<select class=\"hidden\" id=\"{0}\"></select>", name);
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Status(this HtmlHelper html, string name, int status, int disciplineTask, DateTime date, object htmlAttributes)
        {
            var items = new List<SelectListItem>();

            if (disciplineTask > 0)
            {
                var task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var taskGroup = task.GetCustomGroup();

                var scheduleStatusValues = Enum.GetValues(typeof(ScheduleStatus));
                bool isNotStartedStatus = (status == (int)ScheduleStatus.OasisNotYetDue || status == (int)ScheduleStatus.NoteNotYetDue || status == (int)ScheduleStatus.OrderNotYetDue) && date.Date <= DateTime.Now.Date;
                if (isNotStartedStatus)
                {
                    items.Add(new SelectListItem
                    {
                        Text = ScheduleStatus.CommonNotStarted.GetDescription(),
                        Value = status.ToString(),
                        Selected = true
                    });
                }
                else
                {
                    foreach (ScheduleStatus scheduleStatus in scheduleStatusValues)
                    {
                        if (scheduleStatus.GetGroup().IsEqual(taskGroup))
                        {
                            int statusId = (int)scheduleStatus;
                            if (status == statusId)
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = scheduleStatus.GetDescription(),
                                    Value = statusId.ToString(),
                                    Selected = true
                                });
                                break;
                            }
                        }
                    }
                }


                string discipline = task.GetCustomCategory<CustomDescriptionAttribute>();
                switch (discipline.ToLower())
                {
                    case "sn":
                    case "pt":
                    case "ot":
                    case "st":
                    case "hha":
                    case "notes":
                    case "msw":
                        if (DisciplineTaskFactory.AllNoteOrders().Contains(disciplineTask))
                        {
                            if (status != ((int)ScheduleStatus.NoteCompleted) && status != ((int)ScheduleStatus.EvalReturnedWPhysicianSignature))
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("Completed - {0}", ScheduleStatus.EvalReturnedWPhysicianSignature.GetDescription()),
                                    Value = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                                    Selected = false
                                });
                                items.Add(new SelectListItem
                                {
                                    Text = ScheduleStatus.NoteCompleted.GetDescription(),
                                    Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                    Selected = false
                                });
                            }
                            else
                            {
                                if (status != ((int)ScheduleStatus.NoteCompleted) && status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature))
                                {
                                    items.Add(new SelectListItem
                                    {
                                        Text = ScheduleStatus.NoteCompleted.GetDescription(),
                                        Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                        Selected = false
                                    });
                                }
                                if (status == ((int)ScheduleStatus.NoteCompleted) && status != ((int)ScheduleStatus.EvalReturnedWPhysicianSignature))
                                {
                                    items.Add(new SelectListItem
                                    {
                                        Text = string.Format("Completed - {0}", ScheduleStatus.EvalReturnedWPhysicianSignature.GetDescription()),
                                        Value = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                                        Selected = false
                                    });
                                }
                            }
                        }
                        else
                        {
                            if (status != ((int)ScheduleStatus.NoteCompleted))
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                    Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                    Selected = false
                                });
                            }
                        }
                        break;
                    case "485":
                    case "486":
                    case "order":
                    case "nonoasis485":
                        if (status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature))
                        {
                            items.Add(new SelectListItem
                            {
                                Text = string.Format("Completed - {0}", ScheduleStatus.OrderReturnedWPhysicianSignature.GetDescription()),
                                Value = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString(),
                                Selected = false
                            });
                        }
                        break;
                    case "reportsandnotes":
                        if (status != ((int)ScheduleStatus.ReportAndNotesCompleted))
                        {
                            items.Add(new SelectListItem
                            {
                                Text = ScheduleStatus.ReportAndNotesCompleted.GetDescription(),
                                Value = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString(),
                                Selected = false
                            });
                        }
                        break;
                    case "oasis":
                    case "nonoasis":
                        if (status != ((int)ScheduleStatus.OasisExported) && status != ((int)ScheduleStatus.OasisCompletedNotExported))
                        {
                            items.Add(new SelectListItem
                            {
                                Text = string.Format("Completed - {0}", ScheduleStatus.OasisExported.GetDescription()),
                                Value = ((int)ScheduleStatus.OasisExported).ToString(),
                                Selected = false
                            });
                            items.Add(new SelectListItem
                            {
                                Text = ScheduleStatus.OasisCompletedNotExported.GetDescription(),
                                Value = ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                                Selected = false
                            });
                        }
                        else
                        {
                            if (status == ((int)ScheduleStatus.OasisExported) && status != ((int)ScheduleStatus.OasisCompletedNotExported))
                            {
                                items.Add(new SelectListItem
                                   {
                                       Text = (ScheduleStatus.OasisCompletedNotExported).GetDescription(),
                                       Value = ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                                       Selected = false
                                   });
                            }
                            else if (status != ((int)ScheduleStatus.OasisExported) && status == ((int)ScheduleStatus.OasisCompletedNotExported))
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("Completed - {0}", (ScheduleStatus.OasisExported).GetDescription()),
                                    Value = ((int)ScheduleStatus.OasisExported).ToString(),
                                    Selected = false
                                });
                            }
                        }
                        break;
                }
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString VisitStatus(this HtmlHelper html, string name, string status, int disciplineTask, bool isSelectTitle, string title, string defaultValue, object htmlAttributes)
        {
            IEnumerable<SelectListItem> items = null;
            if (disciplineTask > 0)
            {
                var task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var taskGroup = task.GetCustomGroup();

                var list = EnumExtensions.GetValues<ScheduleStatus>();
                items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), args => args.GetGroup().IsEqual(taskGroup), status, isSelectTitle, title, defaultValue);
            }
            return html.SelectList(name, items, htmlAttributes);
        }

        [Obsolete("Not being used anywhere. Remove this attribute if used.")]
        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var agencies = agencyRepository.All();
            var items = GenerateSelectListItems(agencies.ToList(), args => args.Name, args => args.Id.ToString(), value, true, "-- Select Agency --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString PreviousNotes(this HtmlHelper html, List<PreviousNote> previousNotes, object htmlAttributes)
        {
            var items = GenerateSelectListItems(previousNotes ?? new List<PreviousNote>(), args => string.Format("{0} {1}", args.Name, args.Date), args => args.Id.ToString(), null, true, "-- Select Previous Notes --", Guid.Empty.ToString());
            return html.DropDownList("PreviousNotes", items, htmlAttributes);
        }

        public static MvcHtmlString TogglePreviousNotes(this HtmlHelper html, string name, string id)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<span class=\"button\"><a class=\"load-previousnotes\">View Previous Notes</a></span>");
            sb.AppendFormat("<select name=\"{0}\" class=\"hidden previousnotes-list\" id=\"{1}\"> </select>", name,id);
            return MvcHtmlString.Create(sb.ToString());
        }
      
        [Obsolete("Not being used anywhere. Remove this attribute if used.")]
        public static MvcHtmlString MedicationTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem> { new SelectListItem { Text = @"New", Value = "N", Selected = value.IsEqual("N") }, new SelectListItem { Text = @"Changed", Value = "C", Selected = value.IsEqual("C") }, new SelectListItem { Text = @"Unchanged", Value = "U", Selected = value.IsEqual("U") } };
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        [Obsolete("Not being used anywhere. Remove this attribute if used.")]
        public static MvcHtmlString DisciplineTaskList(this HtmlHelper html, string discipline, string name, string value, object htmlAttributes)
        {
            var tasks = lookupRepository.DisciplineTasks(discipline);
            var items = GenerateSelectListItems(tasks, args => string.Format("{0} ({1})", args.Task, args.GCode), args => args.Id.ToString(), value, true, "-- Select Task --", string.Empty);
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString MultipleDisciplineTasks(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var tasks = lookupRepository.DisciplineTasks()
                .Where(d => d.Discipline == Disciplines.Nursing.ToString() || d.Discipline == Disciplines.PT.ToString() || d.Discipline == Disciplines.ST.ToString() || d.Discipline == Disciplines.OT.ToString() || d.Discipline == Disciplines.HHA.ToString() || d.Discipline == Disciplines.MSW.ToString() || d.Discipline == Disciplines.Orders.ToString() || d.Discipline == Disciplines.ReportsAndNotes.ToString())
                .OrderBy(d => d.Task)
                .ToList();

            var selectList = new StringBuilder();
            var attributes = htmlAttributes.GetStyle();
            selectList.AppendFormat("<select name={0} {1}><option value='0'>-- Select Discipline --</option>", name, attributes);
            if (tasks.IsNotNullOrEmpty())
            {
                tasks.ForEach(task => selectList.AppendFormat("<option value='{0}' IsBillable='{1}' data='{2}'{3}>{4}</option>", task.Id.ToString(), task.IsBillable, task.Discipline, task.Id.ToString().IsEqual(value) ? " selected" : string.Empty, task.Task));
            }
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString StatusPatients(this HtmlHelper html, string name, PatientStatus excludedValue, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<PatientStatus>();
            var notIncluded = new List<PatientStatus> { PatientStatus.Deprecated, PatientStatus.Referral, excludedValue };
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), args => !notIncluded.Contains(args), null, false, null, null);
            return html.SelectList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientStatusList(this HtmlHelper html, string name, string value, bool isOnlyAciveAndDischarge, bool isTitle, string title, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<PatientStatus>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), args => (int)args != (int)PatientStatus.Deprecated && (isOnlyAciveAndDischarge ? PatientStatusFactory.CenterStatus().Contains((int)args) : PatientStatusFactory.AllStatus(false).Contains((int)args)), value, isTitle, title, "0").ToList();
            return html.SelectList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DischargeReasons(this HtmlHelper html, string name, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<DischargeReasons>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), args => (int)args != (int)Entities.Enums.DischargeReasons.Other, null, false, "-- Select Reason --", string.Empty).ToList();
            items.Add(new SelectListItem { Text = Entities.Enums.DischargeReasons.Other.GetDescription(), Value = ((int)Entities.Enums.DischargeReasons.Other).ToString() });
            return html.SelectList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AuthorizationStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<AuthorizationStatusTypes>().Select(s => s.GetDescription()).ToList();
            var items = GenerateSelectListItems(list, args => args, args => args, value, false, null, null);
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString CahpsVendors(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<CahpsVendors>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, false, null, null);
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString ContactTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var contactTypes = EnumExtensions.GetValues<ContactTypes>().Select(s => s.GetDescription()).ToList();
            var items = GenerateSelectListItems(contactTypes, args => args, args => args, value, true, "-- Select Contact Type --", "0");
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString LicenseTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var licenseTypeValues = EnumExtensions.GetValues<LicenseTypes>().Select(s => s.GetDescription()).ToList();
            var selectedValue = value.IsNullOrEmpty() || licenseTypeValues.Contains(value) ? value : Entities.Enums.LicenseTypes.Other.GetDescription();
            var items = GenerateSelectListItems(licenseTypeValues, args => args, args => args, selectedValue, true, "-- Select License Type --", string.Empty);
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString CredentialTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var titleValues = EnumExtensions.GetValues<CredentialTypes>();
            var items = GenerateSelectListItems(titleValues, args => args.GetDescription(), args => args.GetDescription(), value, true, "-- Unknown --", string.Empty);
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString TitleTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var titleValues = EnumExtensions.GetValues<TitleTypes>();
            var items = GenerateSelectListItems(titleValues, args => args.GetDescription(), args => args.GetDescription(), value, true, "-- Select Title Type --", "0");
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString AdmissionSources(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var adminSources = lookupRepository.AdmissionSources();
            var items = GenerateSelectListItems(adminSources, args => string.Format("({0}) {1}", args.Code, args.Description), args => args.Id.ToString(), value, true, "-- Select Admission Source --", "0");
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString MapAddress(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var addresses = agencyRepository.GetBranches(Current.AgencyId);
            var items = GenerateSelectListItems(addresses, args => args.Name, args => string.Format("{0} {1}, {2}, {3} {4}", args.AddressLine1, args.AddressLine2, args.AddressCity, args.AddressStateCode, args.AddressZipCode), value, Current.UserAddress.IsNotNullOrEmpty(), "Your Address", Current.UserAddress).ToList();
            items.Add(new SelectListItem { Text = @"Specify Address", Value = "specify" });
            return html.SelectList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString States(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var states = lookupRepository.States();
            var items = GenerateSelectListItems(states, args => args.Name, args => args.Code, value, true, "-- Select State --", "0").ToList();
            return html.SelectList(name, items, htmlAttributes);
        }

        public static MvcHtmlString PaymentSourceWithOutMedicareTradition(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var paymentSources = lookupRepository.PaymentSources();
            var items = GenerateSelectListItems(paymentSources, args => args.Name, args => args.Id.ToString(), args => args.Id != 3, value, true, "-- Select Payment Source --", "0").ToList();
            items.Insert((int)PayorTypeMainCategory.NonPrivatePayor, new SelectListItem { Text = @"Contract", Value = "13" });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        //public static MvcHtmlString PermissionList4(this HtmlHelper html, string namePrefix, Dictionary<int, Dictionary<int, List<int>>> userPermissions, AgencyServices userServices)
        //{
        //    const int AllService = (int)AgencyServicesBoundary.All;
        //    var sb = new StringBuilder();
        //    var list = lookupRepository.GetPermissions();
        //    if (list.IsNotNullOrEmpty())
        //    {
        //        var count = 0;
        //        //var privateDutyPermissions = new Dictionary<int, List<int>>();
        //        if (userServices.Has(AgencyServices.PrivateDuty))
        //        {
        //            count++;
        //            sb.AppendFormat("<input type=\"hidden\" name=\"Services\" value=\"{0}\" />", (int)AgencyServices.PrivateDuty);
        //        }

        //        // var homeHealthPermissions = new Dictionary<int, List<int>>();
        //        if (userServices.Has(AgencyServices.HomeHealth))
        //        {
        //            count++;
        //            sb.AppendFormat("<input type=\"hidden\" name=\"Services\" value=\"{0}\" />", (int)AgencyServices.HomeHealth);
        //        }
        //        sb.AppendFormat("<input type=\"hidden\" name=\"Services\" value=\"{0}\" />", AllService);

        //        var descriptionWidth = 100 - 10 * (count + 1);
        //        var groupDictionary = list.GroupBy(l => l.GroupId).ToDictionary(g => g.Key, g => g.GroupBy(l => l.Id).ToDictionary(ig => ig.Key, ig => ig.ToList()));
        //        userPermissions = userPermissions ?? new Dictionary<int, Dictionary<int, List<int>>>();
        //        if (groupDictionary.IsNotNullOrEmpty())
        //        {
        //            //var listDictionary = list.GroupBy(l => l.Id).ToDictionary(g => g.Key, g => g.ToList());
        //            if (groupDictionary.ContainsKey(1))
        //            {
        //                var listDictionary = groupDictionary[1];
        //                groupDictionary.Remove(1);
        //                listDictionary.ForEach((key, value) =>
        //                {
        //                    if (value.IsNotNullOrEmpty())
        //                    {
        //                        if (Enum.IsDefined(typeof(ParentPermission), key))
        //                        {
        //                            //if the permission is MedicareClaims or Remittance related and the user does not have home health then it will be skipped
        //                            if ((key == (int)ParentPermission.MedicareClaim || key == (int)ParentPermission.Remittance) && !userServices.Has(AgencyServices.HomeHealth))
        //                            {
        //                                return;
        //                            }
        //                            var ceiling = Math.Ceiling((value.Count / (decimal)2));
        //                            var filteredCategory = ((ParentPermission)key);
        //                            sb.AppendFormat("<fieldset class=\"\">");
        //                            sb.AppendFormat("<legend>{0}</legend>", filteredCategory.GetDescription());
        //                            sb.AppendFormat("<input type=\"hidden\" name=\"Permissions.Index\" value=\"{0}\" />", key);
        //                            sb.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Key\" value=\"{0}\" />", key);
        //                            Dictionary<int, List<int>> generalCategoryPermissions;
        //                            userPermissions.TryGetValue(key, out generalCategoryPermissions);
        //                            generalCategoryPermissions = generalCategoryPermissions ?? new Dictionary<int, List<int>>();
        //                            var firstCategory = value.FirstOrDefault();

        //                            var firstColumn = new StringBuilder();
        //                            var secondColumn = new StringBuilder();


        //                            var header = new StringBuilder();

        //                            header.Append("<div class=\"row\">");
        //                            header.Append("<ol>");
        //                            header.Append("<div class=\"wrapper\">");
        //                            if (firstCategory.HasServiceScope && !userServices.IsAlone())
        //                            {
        //                                if (userServices.Has(AgencyServices.PrivateDuty) && ((firstCategory.Service & (int)AgencyServices.PrivateDuty) == (int)AgencyServices.PrivateDuty))
        //                                {
        //                                    header.Append("<div class='fr center width10'>Private Duty</div>");
        //                                }

        //                                if (userServices.Has(AgencyServices.HomeHealth) && ((firstCategory.Service & (int)AgencyServices.HomeHealth) == (int)AgencyServices.HomeHealth))
        //                                {
        //                                    header.Append("<div class='fr center width10'>Home Health</div>");
        //                                }
        //                            }
        //                            header.AppendFormat("<div class='fl width{0}'>Description</div>", descriptionWidth);
        //                            header.Append("</ol>");
        //                            header.Append("</div>");

        //                            firstColumn.Append("<div class=\"column\">");
        //                            secondColumn.Append("<div class=\"column\">");

        //                            firstColumn.Append(header);
        //                            secondColumn.Append(header);

        //                            firstColumn.Append("<ol class=\"content\">");
        //                            secondColumn.Append("<ol class=\"content\">");

        //                            //sb.Append("<ol class=\"content\">");
        //                            var loopCounter = 1;
        //                            value.ForEach(per =>
        //                            {
        //                                if (loopCounter <= ceiling)
        //                                {
        //                                    PermissionHelper(namePrefix, userServices, firstColumn, generalCategoryPermissions, key, filteredCategory, firstCategory.HasServiceScope, per);
        //                                }
        //                                else
        //                                {
        //                                    PermissionHelper(namePrefix, userServices, secondColumn, generalCategoryPermissions, key, filteredCategory, firstCategory.HasServiceScope, per);
        //                                }
        //                                loopCounter++;
        //                            });
        //                            //sb.Append("</ol>");

        //                            firstColumn.Append("</ol>");
        //                            secondColumn.Append("</ol>");

        //                            firstColumn.Append("</div>");
        //                            secondColumn.Append("</div>");

        //                            sb.Append(firstColumn);
        //                            sb.Append(secondColumn);

        //                            sb.AppendFormat("</fieldset>");
        //                        }
        //                    }
        //                });
        //            }
        //            #region Group 3 and 4
        //            var filteredDictionary = groupDictionary.Where(g => g.Key == 3 || g.Key == 4);
        //            if (filteredDictionary.IsNotNullOrEmpty())
        //            {
        //                filteredDictionary.ForEach((value) =>
        //                {
        //                    //if (groupDictionary.ContainsKey(4))
        //                    //{
        //                    var listDictionary = value.Value;
        //                    if (listDictionary.IsNotNullOrEmpty())
        //                    {
        //                        sb.AppendFormat("<fieldset class=\"\">");
        //                        sb.AppendFormat("<legend> Permissions</legend>");


        //                        var viewValue = (int)PermissionActions.ViewList;
        //                        var logValue = (int)PermissionActions.ViewLog;
        //                        var editValue = (int)PermissionActions.Edit;
        //                        var exportValue = (int)PermissionActions.Export;
        //                        var addValue = (int)PermissionActions.Add;
        //                        var deleteValue = (int)PermissionActions.Delete;
        //                        const int printValue = (int)PermissionActions.Print;
        //                        var generalPermissionActions = new Dictionary<PermissionActions, bool>(){
        //                                        {PermissionActions.Add, true},
        //                                        {PermissionActions.Edit, true},
        //                                        {PermissionActions.Delete, true},
        //                                        {PermissionActions.ViewList, true},
        //                                        {PermissionActions.Export, true},
        //                                        {PermissionActions.ViewLog, false},
        //                                        {PermissionActions.Print, false},
        //                                   };
        //                        var isAtLeastOnePrintExist = false;
        //                        var isAtLeastOneViewLogExist = false;
        //                        var body = new StringBuilder();
        //                        body.Append("<ol class=\"content\">");
        //                        listDictionary.ForEach((keyi, valuei) =>
        //                        {
        //                            if (valuei.IsNotNullOrEmpty())
        //                            {
        //                                if (Enum.IsDefined(typeof(ParentPermission), keyi))
        //                                {
        //                                    Dictionary<int, List<int>> generalCategoryPermissions;
        //                                    userPermissions.TryGetValue(keyi, out generalCategoryPermissions);
        //                                    generalCategoryPermissions = generalCategoryPermissions ?? new Dictionary<int, List<int>>();

        //                                    var filteredCategory = ((ParentPermission)keyi);

        //                                        body.Append("<li>");
        //                                        body.Append("<div class=\"wide-column wrapper clear\">");
        //                                        body.AppendFormat("<div class=\"center width20 fl\">{0}</div>", filteredCategory.GetDescription());
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions.Index\" value=\"{0}\" />", keyi);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Key\" value=\"{0}\" />", keyi);
        //                                        body.Append("<div class=\"width70 fr\">");

        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", keyi, addValue);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, addValue);

        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", keyi, editValue);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, editValue);

        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", keyi, exportValue);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, exportValue);

        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", keyi, viewValue);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, viewValue);

        //                                    if (valuei.Exists(v => v.ChildId == printValue))
        //                                    {
        //                                        generalPermissionActions[PermissionActions.Print] = true;
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Value.Index\" value=\"{1}\" />", keyi, printValue);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, printValue);
        //                                        if (!isAtLeastOnePrintExist)
        //                                        {
        //                                            generalPermissionActions[PermissionActions.Print] = true;
        //                                            body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", keyi, printValue);
        //                                            body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, printValue);
        //                                            if (!isAtLeastOnePrintExist)
        //                                            {
        //                                                isAtLeastOnePrintExist = true;
        //                                            }
        //                                        }
        //                                    }

        //                                    if (valuei.Exists(v => v.ChildId == logValue))
        //                                    {
        //                                        generalPermissionActions[PermissionActions.ViewLog] = true;
        //                                        body.Append("<input type=\"hidden\" name=\"ServicePermissions[").Append(keyi).Append("].Value.Index\" value=\"").Append(logValue).Append("\" />");
        //                                        body.Append("<input type=\"hidden\" name=\"ServicePermissions[").Append(keyi).Append("].Value[").Append(logValue).Append("].Key\" value=\"").Append(logValue).Append("\" />");
        //                                        if (!isAtLeastOneViewLogExist)
        //                                        {
        //                                            generalPermissionActions[PermissionActions.ViewLog] = true;
        //                                            body.Append("<input type=\"hidden\" name=\"Permissions[").Append(keyi).Append("].Value.Index\" value=\"").Append(logValue).Append("\" />");
        //                                            body.Append("<input type=\"hidden\" name=\"Permissions[").Append(keyi).Append("].Value[").Append(logValue).Append("].Key\" value=\"").Append(logValue).Append("\" />"); 
        //                                            if (!isAtLeastOneViewLogExist)
        //                                            {
        //                                                isAtLeastOneViewLogExist = true;
        //                                            }
        //                                        }
        //                                    }

        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", keyi, deleteValue);
        //                                        body.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", keyi, deleteValue);


        //                                    var firstElement = valuei.FirstOrDefault();
        //                                    if (firstElement.HasServiceScope)
        //                                    {
        //                                        var hhService = (int)AgencyServices.HomeHealth;
        //                                        if (userServices.Has(AgencyServices.HomeHealth) && ((firstElement.Service & hhService) == hhService))
        //                                        {
        //                                            foreach (var generalPermissionAction in generalPermissionActions)
        //                                            {
        //                                                var actionName = generalPermissionAction.Key.ToString();
        //                                                if (generalPermissionAction.Value)
        //                                                {
        //                                                    var actionName = generalPermissionAction.Key.ToString();
        //                                                    if (generalPermissionAction.Value)
        //                                                    {
        //                                                        body.Append(
        //                                                            TagBuilderFactory.BuildTag(
        //                                                                "div",
        //                                                                "fr center width10",
        //                                                                TagBuilderFactory.BuildCheckBox(
        //                                                                    "hh" + filteredCategory.ToString(),
        //                                                                    namePrefix + "_HHPermission_" + actionName + filteredCategory.ToString(),
        //                                                                    "Permissions[" + keyi + "].Value[" + (int)generalPermissionAction.Key + "].Value",
        //                                                                    hhService.ToString(),
        //                                                                    generalCategoryPermissions.GetOrDefault((int)generalPermissionAction.Key, new List<int>()).Contains(hhService)).ToString()).ToString());
        //                                                    }
        //                                                }
        //                                            }
        //                                            if (!userServices.IsAlone())
        //                                            {
        //                                                body.Append("<div class=\"clear\"/>");
        //                                                foreach (var generalPermissionAction in generalPermissionActions)
        //                                                {
        //                                                    var actionName = generalPermissionAction.Key.ToString();
        //                                                    if (generalPermissionAction.Value)
        //                                                    {
        //                                                        body.Append(
        //                                                            TagBuilderFactory.BuildTag(
        //                                                                "div",
        //                                                                "fr center width10",
        //                                                                TagBuilderFactory.BuildCheckBox(
        //                                                                    "hh" + filteredCategory.ToString(),
        //                                                                    namePrefix + "_PDPermission_" + actionName + filteredCategory.ToString(),
        //                                                                    "Permissions[" + keyi + "].Value[" + (int)generalPermissionAction.Key + "].Value",
        //                                                                    pdService.ToString(),
        //                                                                    generalCategoryPermissions.GetOrDefault((int)generalPermissionAction.Key, new List<int>()).Contains(pdService)).ToString()).ToString());
        //                                                    }
        //                                                }
        //                                                body.Append("<div class='fl width10'>Private Duty</div>");
        //                                            }
        //                                        }
        //                                        var pdService = (int)AgencyServices.PrivateDuty;
        //                                        if (userServices.Has(AgencyServices.PrivateDuty) && ((firstElement.Service & pdService) == pdService))
        //                                        {
        //                                            body.Append("<div class=\"clear\"/>");
        //                                            foreach (var generalPermissionAction in generalPermissionActions)
        //                                            {
        //                                                var actionName = generalPermissionAction.Key.ToString();
        //                                                if (generalPermissionAction.Value)
        //                                                {
        //                                                    body.Append(
        //                                                        TagBuilderFactory.BuildTag(
        //                                                            "div",
        //                                                            "fr center width10",
        //                                                            TagBuilderFactory.BuildCheckBox(
        //                                                                "hh" + filteredCategory.ToString(),
        //                                                                namePrefix + "_PDPermission_" + actionName + filteredCategory.ToString(),
        //                                                                "Permissions[" + keyi + "].Value[" + (int)generalPermissionAction.Key + "].Value",
        //                                                                pdService.ToString(),
        //                                                                generalCategoryPermissions.GetOrDefault((int)generalPermissionAction.Key, new List<int>()).Contains(pdService)).ToString()).ToString());
        //                                                }
        //                                            }
        //                                            if (!userServices.IsAlone())
        //                                            {
        //                                                body.Append("<div class='fl width10'>Private Duty</div>");
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        foreach (var generalPermissionAction in generalPermissionActions)
        //                                        {
        //                                            var actionName = generalPermissionAction.Key.ToString();
        //                                            if (generalPermissionAction.Value)
        //                                            {
        //                                                body.Append(
        //                                                    TagBuilderFactory.BuildTag(
        //                                                        "div",
        //                                                        "fr center width10",
        //                                                        TagBuilderFactory.BuildCheckBox(
        //                                                            filteredCategory.ToString(),
        //                                                            namePrefix + "_Permission_" + actionName + filteredCategory.ToString(),
        //                                                            "ServicePermissions[" + keyi + "].Value[" + (int)generalPermissionAction.Key + "].Value",
        //                                                            AllService.ToString(),
        //                                                            generalCategoryPermissions.GetOrDefault((int)generalPermissionAction.Key, new List<int>()).Contains(AllService)).ToString()).ToString());
        //                                            }
        //                                        }
        //                                        //body.Append("<div class='fl width10'></div>");
        //                                    }
        //                                    body.Append("</div>");
        //                                    body.Append("</div>");
        //                                    body.Append("</li>");
        //                                }
        //                            }
        //                        });
        //                        body.Append("</ol>");


        //                        var header = new StringBuilder();
        //                        header.Append("<ol>");
        //                        header.Append("<li>");
        //                        header.Append("<div class=\"wide-column wrapper\">");
        //                        header.Append("<div class=\"width20 fl\">Description</div>");
        //                        header.Append("<div class=\"width70 fr\">");
        //                        header.Append("<div class='fr center width10'>Add</div>");
        //                        header.Append("<div class='fr center width10'>Edit</div>");
        //                        header.Append("<div class='fr center width10'>Delete</div>");
        //                        header.Append("<div class='fr center width10'>View List</div>");
        //                        header.Append("<div class='fr center width10'>Export</div>");
        //                        if (isAtLeastOnePrintExist)
        //                        {
        //                            header.Append("<div class='fr center width10'>Print</div>");
        //                        }
        //                        if (isAtLeastOneViewLogExist)
        //                        {
        //                            header.Append("<div class='fr center width10'>View Log</div>");
        //                        }
        //                        if (!userServices.IsAlone())
        //                        {
        //                            header.Append("<div class='fl width10'>Service</div>");
        //                        }
        //                        header.Append("</div>");
        //                        header.Append("</div>");
        //                        header.Append("</li>");
        //                        header.Append("</ol>");
        //                        header.Append("<br/><br/>");

        //                        sb.Append(header);
        //                        sb.Append(body);

        //                        sb.AppendFormat("</fieldset>");
        //                    }
        //                    //}
        //                });
        //            }
        //            #endregion


        //        }
        //    }
        //    return MvcHtmlString.Create(sb.ToString());
        //}

        private static void PermissionHelper(string namePrefix, AgencyServices userServices, StringBuilder sb, Dictionary<int, List<int>> generalCategoryPermissions, int key, ParentPermission filteredCategory, bool hasServiceScope, PermissionLink per)
        {
            List<int> permissions;
            generalCategoryPermissions.TryGetValue(per.ChildId, out permissions);
            permissions = permissions ?? new List<int>();
            sb.Append("<li>");
            sb.Append("<div class=\"wrapper clear\">");
            sb.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value.Index\" value=\"{1}\" />", key, per.ChildId);
            sb.AppendFormat("<input type=\"hidden\" name=\"Permissions[{0}].Value[{1}].Key\" value=\"{1}\" />", key, per.ChildId);

            if (hasServiceScope)
            {
                if (userServices.Has(AgencyServices.PrivateDuty) && ((per.Service & (int)AgencyServices.PrivateDuty) == (int)AgencyServices.PrivateDuty))
                {
                    var input = TagBuilderFactory.BuildCheckBox("pd" + filteredCategory.ToString(), namePrefix + "_PDPermission_" + filteredCategory.ToString(), "Permissions[" + key + "].Value[" + per.ChildId + "].Value", ((int)AgencyServices.PrivateDuty).ToString(), permissions.Contains((int)AgencyServices.PrivateDuty)).ToString();

                    sb.AppendFormat("<div class='fr center width10'>{0}</div>", input);
                }

                if (userServices.Has(AgencyServices.HomeHealth) && ((per.Service & (int)AgencyServices.HomeHealth) == (int)AgencyServices.HomeHealth))
                {
                    var input = TagBuilderFactory.BuildCheckBox("hh" + filteredCategory.ToString(), namePrefix + "_HHPermission_" + filteredCategory.ToString(), "Permissions[" + key + "].Value[" + per.ChildId + "].Value", ((int)AgencyServices.HomeHealth).ToString(), permissions.Contains((int)AgencyServices.HomeHealth)).ToString();
                    sb.AppendFormat("<div class='fr center width10'>{0}</div>", input);
                }
            }
            else
            {
                var input = TagBuilderFactory.BuildCheckBox(filteredCategory.ToString(), namePrefix + "_Permission_" + filteredCategory.ToString(), "Permissions[" + key + "].Value[" + per.ChildId + "].Value", ((int)AgencyServicesBoundary.All).ToString(), permissions.Contains((int)AgencyServicesBoundary.All)).ToString();
                sb.AppendFormat("<div class='fr center width10'>{0}</div>", input);
            }
            string name = per.Name.IsNotNullOrEmpty() ? per.Name : (Enum.IsDefined(typeof(PermissionActions), per.ChildId) ? ((PermissionActions)per.ChildId).GetDescription() : string.Empty);
            sb.AppendFormat("<div  class='fl  width{1} option' tooltip=\"{2}\"><label><strong>{0}</strong></label></div>", name, 70, per.Description ?? string.Empty);
            sb.Append("</div>");
            sb.Append("</li>");
        }

        public static MvcHtmlString ReportPermissions(this HtmlHelper html, string namePrefix, Dictionary<int, Dictionary<int, List<int>>> userPermissions, AgencyServices userServices)
        {

            var sb = new StringBuilder();
            var list = lookupRepository.GetReportDescriptions();
            if (list.IsNotNullOrEmpty())
            {
                var viewValue = (int)PermissionActions.ViewList;
                var exportValue = (int)PermissionActions.Export;

                var body = new StringBuilder();

                var count = 0;
                if (userServices.Has(AgencyServices.PrivateDuty))
                {
                    count++;
                }
                if (userServices.Has(AgencyServices.HomeHealth))
                {
                    count++;
                }

                body.Append("<ol>");
                body.Append("<li>");
                body.Append("<div class=\"wrapper\">");
                var descriptionWidth = 100 - 10 * (count + 1);
                body.AppendFormat("<div class='fl width{0}'>Description</div>", descriptionWidth);
                if (userServices.Has(AgencyServices.PrivateDuty))
                {
                    count++;
                    body.Append("<div class='fl center width10'>");
                    if (!userServices.IsAlone())
                    {
                        body.Append("Private Duty<br>");
                    }
                    body.Append("View | Export</div>");
                    body.AppendFormat("<input type=\"hidden\" name=\"ServicesToEdit\" value=\"{0}\" />", (int)AgencyServices.PrivateDuty);
                }

                if (userServices.Has(AgencyServices.HomeHealth))
                {
                    count++;
                    body.Append("<div class='fl center width10'>");
                    if (!userServices.IsAlone())
                    {
                        body.Append("Home Health<br>");
                    }
                    body.Append("View | Export</div>");
                    body.AppendFormat("<input type=\"hidden\" name=\"ServicesToEdit\" value=\"{0}\" />", (int)AgencyServices.HomeHealth);
                }


                body.Append("</div>");
                body.Append("</li>");
                body.Append("</ol>");
                body.Append("<br/>");
                userPermissions = userPermissions ?? new Dictionary<int, Dictionary<int, List<int>>>();
                var listDictionary = list.GroupBy(l => l.CategoryId).ToDictionary(g => g.Key, g => g.ToList());
                listDictionary.ForEach((key, value) =>
                {
                    if (value.IsNotNullOrEmpty())
                    {
                        if (Enum.IsDefined(typeof(ReportCategory), key))
                        {
                            var filteredCategory = ((ReportCategory)key);
                            body.AppendFormat("<div class=\"wrapper clear\"><label><strong>{0}</strong></label><br/></div>", filteredCategory.GetDescription());
                            body.Append("<ol class=\"content indent\">");
                            value.ForEach(per =>
                            {
                                var service = (AgencyServices)per.Service;
                                if (!userServices.Has(service) && !service.Has(userServices))
                                {
                                    return;
                                }
                                Dictionary<int, List<int>> generalCategoryPermissions;
                                userPermissions.TryGetValue(per.Id, out generalCategoryPermissions);
                                generalCategoryPermissions = generalCategoryPermissions ?? new Dictionary<int, List<int>>();

                                List<int> viewPermissions;
                                generalCategoryPermissions.TryGetValue(viewValue, out viewPermissions);
                                viewPermissions = viewPermissions ?? new List<int>();

                                List<int> exportPermissions;
                                generalCategoryPermissions.TryGetValue(exportValue, out exportPermissions);
                                exportPermissions = exportPermissions ?? new List<int>();

                                body.Append("<li>");
                                body.Append("<div class=\"wrapper clear\">");

                                body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions.Index\" value=\"{0}\" />", per.Id);
                                body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Key\" value=\"{0}\" />", per.Id);

                                body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Value.Index\" value=\"{1}\" />", per.Id, viewValue);
                                body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Value[{1}].Key\" value=\"{1}\" />", per.Id, viewValue);

                                body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Value.Index\" value=\"{1}\" />", per.Id, exportValue);
                                body.AppendFormat("<input type=\"hidden\" name=\"ServicePermissions[{0}].Value[{1}].Key\" value=\"{1}\" />", per.Id, exportValue);

                                body.AppendFormat("<div  class='fl  width{1} option' tooltip=\"{2}\"><label><strong>{0}</strong></label></div>", per.Title, 70, string.Empty);

                                if (userServices.Has(AgencyServices.PrivateDuty) && service.Has(AgencyServices.PrivateDuty))
                                {
                                    var input1 = TagBuilderFactory.BuildCheckBox("pd" + filteredCategory.ToString(), namePrefix + "_PDPermission_View" + filteredCategory.ToString(), "ServicePermissions[" + per.Id + "].Value[" + viewValue + "].Value", ((int)AgencyServices.PrivateDuty).ToString(), viewPermissions.Contains((int)AgencyServices.PrivateDuty)).ToString();
                                    var input2 = TagBuilderFactory.BuildCheckBox("pd" + filteredCategory.ToString(), namePrefix + "_PDPermission_Export" + filteredCategory.ToString(), "ServicePermissions[" + per.Id + "].Value[" + exportValue + "].Value", ((int)AgencyServices.PrivateDuty).ToString(), exportPermissions.Contains((int)AgencyServices.PrivateDuty)).ToString();
                                    body.AppendFormat("<div class='fr center width10'>{0} | {1}</div>", input1, input2);
                                }

                                if (userServices.Has(AgencyServices.HomeHealth) && service.Has(AgencyServices.HomeHealth))
                                {
                                    var input1 = TagBuilderFactory.BuildCheckBox("hh" + filteredCategory.ToString(), namePrefix + "_HHPermission_View" + filteredCategory.ToString(), "ServicePermissions[" + per.Id + "].Value[" + viewValue + "].Value", ((int)AgencyServices.HomeHealth).ToString(), viewPermissions.Contains((int)AgencyServices.HomeHealth)).ToString();
                                    var input2 = TagBuilderFactory.BuildCheckBox("hh" + filteredCategory.ToString(), namePrefix + "_HHPermission_Export" + filteredCategory.ToString(), "ServicePermissions[" + per.Id + "].Value[" + exportValue + "].Value", ((int)AgencyServices.HomeHealth).ToString(), exportPermissions.Contains((int)AgencyServices.HomeHealth)).ToString();
                                    body.AppendFormat("<div class='fl center width10'>{0} | {1} </div>", input1, input2);
                                }

                                body.Append("</div>");
                                body.Append("</li>");
                            });
                            body.Append("</ol>");

                        }
                    }
                });

                //var header = new StringBuilder();
                //if (count > 1)
                //{
                //    header.AppendFormat("<fieldset>");
                //    header.AppendFormat("<legend>Services</legend>");
                //    header.AppendFormat("<div class=\"wide-column\">"); 

                //    if (userServices.Has(AgencyServices.PrivateDuty))
                //    {
                //        count++;
                //        header.AppendFormat("<div class=\"column\"><input type=\"checkbox\" name=\"ServicesToEdit\" value=\"{0}\" />Home Health</div>", (int)AgencyServices.PrivateDuty);
                //    }

                //    if (userServices.Has(AgencyServices.HomeHealth))
                //    {
                //        count++;
                //        header.AppendFormat("<div class=\"column\"><input type=\"checkbox\" name=\"ServicesToEdit\" value=\"{0}\" />Private Duty</div>", (int)AgencyServices.HomeHealth);
                //    }
                //    header.Append("</div>");
                //    header.AppendFormat("</fieldset>");
                //}
                //else
                //{
                //    header.AppendFormat("<input type=\"hidden\" name=\"ServicesToEdit\" value=\"{0}\" />", (int)userServices);
                //}
                //sb.Append(header);
                sb.Append(body);

            }
            return MvcHtmlString.Create(sb.ToString());
        }


        public static MvcHtmlString Months(this HtmlHelper html, string name, string value, int start, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            int currentYear = DateTime.Now.Year;
            if (start < currentYear)
            {
                for (int val = start; val <= currentYear; val++)
                {
                    items.Add(new SelectListItem
                    {
                        Text = val.ToString(),
                        Value = val.ToString(),
                        Selected = val.ToString().IsEqual(value)
                    });
                }
            }
            items.Reverse();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString CalendarMonths(this HtmlHelper html, string name, int value, bool isTitleIncluded, object htmlAttributes)
        {
            var items = DateTimeFormatInfo.InvariantInfo.MonthNames.Select((monthname, index) => new SelectListItem { Text = monthname, Value = (index + 1).ToString(), Selected = (index + 1) == value }).ToList();
            if (isTitleIncluded)
            {
                items.Insert(0, new SelectListItem { Text = @"Select Month", Value = "0", Selected = 0 == value });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString ClaimTypes(this HtmlHelper html, string name, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<ClaimType>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), "All", true, "All", "All");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString UB4PatientStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<UB4PatientStatus>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString().PadLeft(2, '0'), value, true, "-- Select Patient Status --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString BillType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<BillType>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Bill Type --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString ClaimTypeSubCategories(this HtmlHelper html, string name, string value, bool isTitleIncluded, string title, object htmlAttributes)
        {
            var attributes = htmlAttributes.GetStyle();
            var selectList = new StringBuilder();
            selectList.AppendFormat("<select name={0} {1}>", name, attributes);
            if (isTitleIncluded)
            {
                selectList.AppendFormat("<option value='0'>{0}</option>", title);
            }
            selectList.AppendFormat("<option value='{0}' billtype='medicare' {1}>{2}</option>", (int)ClaimTypeSubCategory.RAP, ((int)ClaimTypeSubCategory.RAP).ToString().IsEqual(value).ToSelected(), ClaimTypeSubCategory.RAP.GetDescription());
            selectList.AppendFormat("<option value='{0}' billtype='medicare' {1}>{2}</option>", (int)ClaimTypeSubCategory.Final, ((int)ClaimTypeSubCategory.Final).ToString().IsEqual(value).ToSelected(), ClaimTypeSubCategory.Final.GetDescription());
            selectList.AppendFormat("<option value='{0}' billtype='managedcare' {1}>{2}</option>", (int)ClaimTypeSubCategory.ManagedCare, ((int)ClaimTypeSubCategory.ManagedCare).ToString().IsEqual(value).ToSelected(), ClaimTypeSubCategory.ManagedCare.GetDescription());
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString UnitType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<BillUnitType>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Unit Type --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString InsuranceDisciplineTask(this HtmlHelper html, string name, int disciplineTask, List<int> existingRates, bool isTaskIncluded, object htmlAttributes)
        {
            //var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            //var disciplines = new List<int>();
            //if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            //{
            //    disciplines = insurance.BillData.ToObject<List<ChargeRate>>().Select(i => i.Id).ToList();
            //}
            return html.DropDownList(name, InsuranceDisciplineTaskHelper(disciplineTask, isTaskIncluded, existingRates), htmlAttributes);
        }

        public static MvcHtmlString LocationCostDisciplineTask(this HtmlHelper html, string name, int disciplineTask, Guid locationId, bool isTaskIncluded, object htmlAttributes)
        {
            var location = agencyRepository.FindLocation(Current.AgencyId, locationId);
            var disciplines = new List<int>();
            if (location != null && location.BillData.IsNotNullOrEmpty())
            {
                disciplines = location.BillData.ToObject<List<ChargeRate>>().Select(i => i.Id).ToList();
            }
            return html.DropDownList(name, InsuranceDisciplineTaskHelper(disciplineTask, isTaskIncluded, disciplines), htmlAttributes);
        }

        public static MvcHtmlString UserDisciplineTask(this HtmlHelper html, string name, int disciplineTask, Guid userId, int insurance, bool isTaskIncluded, object htmlAttributes)
        {
            var disciplines = new List<int>();
            if (!userId.IsEmpty() && insurance > 0)
            {
                var userRates = userRepository.GetPayorUserRates(Current.AgencyId, userId, insurance);
                if (userRates.IsNotNullOrEmpty())
                {
                    disciplines = userRates.Select(i => i.Id).ToList();
                }
            }
            return html.DropDownList(name, InsuranceDisciplineTaskHelper(disciplineTask, isTaskIncluded, disciplines), htmlAttributes);
        }

        public static MvcHtmlString BillStatus(this HtmlHelper html, string name, string value, bool isSelectIncluded, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<BillingStatus>();
            var ignoredValues = new[] { (int)BillingStatus.ProcessedAsPrimaryFTAP, (int)BillingStatus.ProcessedAsSecondaryFTAP, (int)BillingStatus.ProcessedAsTertiaryFTAP };
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), args => !ignoredValues.Contains((int)args), value, isSelectIncluded, "-- Select Bill Status --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString ManagedBillStatus(this HtmlHelper html, string name, string value, bool isSelectIncluded, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<ManagedClaimStatus>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, isSelectIncluded, "-- Select Bill Status --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString TherapyAssistance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<TherapyAssistance>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Assistance --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString Sensory(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<Sensory>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString StaticBalance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<StaticBalance>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Balance --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString DynamicBalance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<DynamicBalance>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Balance --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString UserRateTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<UserRateTypes>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), args => ((int)args) != 0, value, true, "-- Select Rate Type --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString TherapyAssistiveDevice(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<TherapyAssistiveDevices>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Assistive Device --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString WeightBearingStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<WeightBearingStatus>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Status --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString InsuranceType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<InsuranceType>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => ((int)args).ToString(), value, true, "-- Select Insurance Type --", "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString InsuranceRelationships(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            //Since this feature was originally implemented using strings instead of an id, this will allow the program to convert the old values to their new ones
            if (value.IsNullOrEmpty())
            {
                value = "2";
            }
            else
            {
                switch (value)
                {
                    case "Self":
                        value = "2";
                        break;
                    case "Spouse":
                        value = "1";
                        break;
                    case "Child":
                        value = "3";
                        break;
                    case "Other":
                        value = "9";
                        break;
                }
            }
            IList<Relationship> list = lookupRepository.Relationships();
            var items = list.Select(item => new SelectListItem { Text = item.Description, Value = item.Id.ToString(), Selected = value == item.Id.ToString() }).ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Relationships(this HtmlHelper html, string name, string value, bool isTitle, string title, object htmlAttributes)
        {
            Array list = Enum.GetValues(typeof(RelationshipTypes));
            var items = (from RelationshipTypes item in list let itemArray = item.ToString().Split('_') where itemArray != null && itemArray.Length >= 2 let itemValue = itemArray[1] where itemValue.IsNotNullOrEmpty() select new SelectListItem { Text = item.GetDescription(), Value = itemValue, Selected = value == itemValue }).ToList();
            if (isTitle)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }
            return html.SelectList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Gender(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new SelectList(new[] {
                            new SelectListItem { Text = "-- Select Gender --", Value = "" },
                            new SelectListItem { Text = "Male", Value = "Male" },
                            new SelectListItem { Text = "Female", Value = "Female" }
                        }, "Value", "Text");
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DNR(this HtmlHelper html, string name, string value, bool isValueInt, object htmlAttributes)
        {
            var items = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = isValueInt?"1": "true" },
                            new SelectListItem { Text = "No", Value = isValueInt?"0":  "false"}
                        }, "Value", "Text", value);
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString HeightMetric(this HtmlHelper html, string name, int value, object htmlAttributes)
        {
            var items = new SelectList(new[] {
                            new SelectListItem { Text = "in", Value = "0" ,Selected=value==0 },
                            new SelectListItem { Text = "cm", Value = "1" ,Selected=value==1}
                        }, "Value", "Text");
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString WeightMetric(this HtmlHelper html, string name, int value, object htmlAttributes)
        {
            var items = new SelectList(new[] {
                            new SelectListItem { Text = "lb", Value = "0" ,Selected=value==0},
                            new SelectListItem { Text = "kg", Value = "1" ,Selected=value==1}
                        }, "Value", "Text");
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }


        public static MvcHtmlString AuthorizationType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new SelectList(new[] {
                                new SelectListItem { Text = "Visits", Value = "1" },
                                new SelectListItem { Text = "Hours", Value = "2" }
                            }, "Value", "Text", value);
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }


        public static MvcHtmlString EvacuationZone(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var evacuationZone = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "" },
                                    new SelectListItem { Text = "Zone A", Value = "A" },
                                    new SelectListItem { Text = "Zone B", Value = "B" },
                                    new SelectListItem { Text = "Zone C", Value = "C"},
                                    new SelectListItem { Text = "Zone D", Value = "D" },
                                    new SelectListItem { Text = "Zone E", Value = "E" },
                                    new SelectListItem { Text = "Zone NE", Value = "NE"},
                                    new SelectListItem { Text = "Zone SN", Value = "SN" }
                                }, "Value", "Text", value);
            return html.DropDownList(name, evacuationZone.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PhoneOrFax(this HtmlHelper html, string name, string value, string id, string classValues)
        {
            return PhoneOrFax(html, name,
                value.IsNotNullOrEmpty() && value.Length >= 3 ? value.Substring(0, 3) : string.Empty,
                value.IsNotNullOrEmpty() && value.Length >= 6 ? value.Substring(3, 3) : string.Empty,
                value.IsNotNullOrEmpty() && value.Length >= 10 ? value.Substring(6, 4) : string.Empty,
                id, classValues);
        }

        public static MvcHtmlString PhoneOrFax(this HtmlHelper html, string name, string phone1, string phone2, string phone3, string id, string classValues)
        {
            var phoneString = string.Empty;
            phoneString += string.Format("<input type='text' class=\"digits phone-short {3} \" name='{0}' value='{1}' id='{2}1' maxlength='3' size='3'/>", name, phone1, id, classValues);
            phoneString += " - ";
            phoneString += string.Format("<input type='text' class=\"digits phone-short {3} \" name='{0}' value='{1}' id='{2}2' maxlength='3'  size='3'/>", name, phone2, id, classValues);
            phoneString += " - ";
            phoneString += string.Format("<input type='text' class=\"digits phone-long {3} \" name='{0}' value='{1}' id='{2}3' maxlength='4'  size='5'/>", name, phone3, id, classValues);
            return MvcHtmlString.Create(phoneString);
        }

        //For the medication link. Do not delete.
        //public static MvcHtmlString MedicationInformationLink(this HtmlHelper html, string drugId)
        //{
        //    if (drugId.IsNotNullOrEmpty())
        //    {
        //        return html.ActionLink("Teaching Guide", "MedicationEducation", "Patient", new { DrugId = drugId }, new { @class = "teachingguide", target = "_blank" });
        //        //return MvcHtmlString.Create(string.Format("<a class='teachingguide' target='_blank' href='{0}'>Teaching Guide</a>", url));
        //    }
        //    else
        //    {
        //        return MvcHtmlString.Empty;
        //    }
        //}
        //public static MvcHtmlString RenderHtml(this HtmlHelper html, string serverPath)
        //{

        //var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", string.Empty), serverPath);
        //var stream = File.OpenText(filePath);
        //if (serverPath.IsNotNullOrEmpty())
        //{
        //    var htmlResult = FileGenerator.RemoteDownload(AppSettings.LexiFileServerIP, AppSettings.LexiFileMainDirectory, serverPath);
        //    if (htmlResult.IsNotNullOrEmpty())
        //    {
        //        htmlResult = htmlResult.Replace("../styles", "../HTML/styles").Replace("../javascript", "../HTML/javascript")
        //            .Replace("../images", "../HTML/images").Replace("../symbols", "../HTML/symbols");
        //        return MvcHtmlString.Create(htmlResult);
        //    }
        //}
        //    return MvcHtmlString.Create(string.Empty);
        //}
        public static MvcHtmlString GeneralStatusDropDown(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem> { new SelectListItem { Text = @"-- Select Level -- ", Value = "0" }, new SelectListItem { Text = "Mild", Value = "1", Selected = "1".Equals(value) }, new SelectListItem { Text = "Moderate", Value = "2", Selected = "2".Equals(value) }, new SelectListItem { Text = "Severe", Value = "3", Selected = "3".Equals(value) } };
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AllDisciplineTaskList(this HtmlHelper html, string name, string value, string title, object htmlAttributes)
        {
            var tasks = lookupRepository.DisciplineTasks()
                .Where(d => d.Discipline == Disciplines.Nursing.ToString()
                         || d.Discipline == Disciplines.PT.ToString()
                         || d.Discipline == Disciplines.ST.ToString()
                         || d.Discipline == Disciplines.OT.ToString()
                         || d.Discipline == Disciplines.Dietician.ToString()
                         || d.Discipline == Disciplines.HHA.ToString()
                         || d.Discipline == Disciplines.MSW.ToString()
                         || d.Discipline == Disciplines.Orders.ToString()
                         || d.Discipline == Disciplines.ReportsAndNotes.ToString())
                .OrderBy(d => d.Task)
                .ToList();

            var items = GenerateSelectListItems(tasks, args => args.Task, args => args.Id.ToString(), value, true, title, "0");
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString AdjustmentCodes(this HtmlHelper helper, string name, string value, string title, object htmlAttributes)
        {
            var codes = agencyRepository.GetAdjustmentCodes(Current.AgencyId);
            var items = GenerateSelectListItems(codes, args => args.Code + " - " + args.Description, args => args.Id.ToString(), value, true, title, "0");
            return helper.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString UploadTypes(this HtmlHelper helper, string name, string value, string title, object htmlAttributes)
        {
            var types = agencyRepository.GetUploadTypes(Current.AgencyId);
            var items = GenerateSelectListItems(types, args => args.Type, args => args.Id.ToString(), value, true, title, "0");
            return helper.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString MaritalStatus(this HtmlHelper html, string name, string value, bool isTitle, string title, string defaultValue, object htmlAttributes)
        {
            var list = EnumExtensions.GetValues<MaritalStatus>();
            var items = GenerateSelectListItems(list, args => args.GetDescription(), args => args.ToString(), value, isTitle, title, defaultValue);
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString InterchangeReceiver(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            Array list = Enum.GetValues(typeof(InterchangeReceiver));
            var items = (from InterchangeReceiver item in list let itemArray = item.ToString().Split('_') where itemArray != null && itemArray.Length >= 2 let itemValue = itemArray[1] where itemValue.IsNotNullOrEmpty() select new SelectListItem { Text = item.GetDescription(), Value = itemValue, Selected = value == itemValue }).ToList();
            return html.SelectList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientSchedulesDisciplines(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var attributes = htmlAttributes.GetStyle();
            var selectList = new StringBuilder();
            selectList.AppendFormat("<select  name='{0}' {1}>", name, attributes);
            selectList.Append(TagBuilderFactory.BuildSelectOption("All", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("Orders", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("Nursing", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("PT", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("OT", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("HHA", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("MSW", value));
            selectList.Append(TagBuilderFactory.BuildSelectOption("Dietician", value));
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString EnumDropDown<TEnum>(this HtmlHelper html, string name, bool includeBlank, object htmlAttributes) where TEnum : IComparable, IFormattable
        {
            var values = EnumExtensions.GetValues<TEnum>();
            var selectedValue = htmlAttributes.GetProperty<string>("value") ?? string.Empty;
            var list = values.ToList().Select(s => new SelectListItem() { Text = s.GetDescription<TEnum>(), Value = Convert.ToInt32(s).ToString(), Selected = Convert.ToInt32(s).ToString() == selectedValue }).ToList();
            if (includeBlank)
            {
                list.Insert(0, new SelectListItem() { Text = "", Value = "" });
            }
            return html.DropDownList(name, list, htmlAttributes);
        }

        public static MvcHtmlString StringNumberBasedDropDown(this HtmlHelper html, string name, object htmlAttributes, params string[] items)
        {
            var list = new List<SelectListItem> { new SelectListItem() { Text = "", Value = "" } };
            list.AddRange(items.Select((t, i) => new SelectListItem() { Text = t, Value = (i + 1).ToString() }));
            return html.DropDownList(name, list, htmlAttributes);
        }

        public static MvcHtmlString StringNumberBasedDropDownNoEmptyItem(this HtmlHelper html, string name, object htmlAttributes, params string[] items)
        {
            var list = new List<SelectListItem>();
            list.AddRange(items.Select((t, i) => new SelectListItem() { Text = t, Value = (i + 1).ToString() }));
            return html.DropDownList(name, list, htmlAttributes);
        }

        public static MvcHtmlString StringNumberBasedDropDownZeroStart(this HtmlHelper html, string name, object htmlAttributes, params string[] items)
        {
            var list = new List<SelectListItem> { new SelectListItem() { Text = "", Value = "" } };
            list.AddRange(items.Select((t, i) => new SelectListItem() { Text = t, Value = i.ToString() }));
            return html.DropDownList(name, list, htmlAttributes);
        }

        #region User

        public static MvcHtmlString RatedUser(this HtmlHelper html, string name, Guid locationId, Guid excludedUserId, bool isSelectAll, string title, object htmlAttributes)
        {
            var users = userRepository.GetRatedUserByBranch(Current.AgencyId, locationId, excludedUserId);
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), string.Empty, isSelectAll, title, Guid.Empty.ToString());
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Users(this HtmlHelper html, string name, string value, string title, Guid branchId, int status, int service, object htmlAttributes)
        {
            var users = userRepository.GetUsersByStatusLean(Current.AgencyId, branchId, status, service) ?? new List<User>();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, title, Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString ToggleUsers(this HtmlHelper html, string name, string value, string userName, string id, string classValues)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"user-selector\">");
            sb.AppendFormat("<span class=\"user\"><input type=\"hidden\" id=\"{0}\" class=\"{1}\" value=\"{2}\" name=\"{3}\"/>{4}</span>",id, classValues,value,name, userName);
            sb.AppendLine("<span class=\"button\"><a class=\"load-users\">Load User</a></span>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CaseManagers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetCaseManagerUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select Case Manager --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString Auditors(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetAuditors(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select Auditor --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString HHAides(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetHHAUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select Home Health Aide --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString PTTherapists(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetPTUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select PTA --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString OTTherapists(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetOTUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select COTA --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString LVNurses(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetLVNUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select LVN/LPN --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString Clinicians(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var users = userRepository.GetClinicalUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            var items = GenerateSelectListItems(users, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select Clinician --", Guid.Empty.ToString());
            return html.DropDownList(name, items, htmlAttributes);
        }

        public static MvcHtmlString NonSoftwareUsers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var nonUsers = userRepository.GetNonUsers(Current.AgencyId).OrderBy(o => o.DisplayName).ToList();
            var items = GenerateSelectListItems(nonUsers, item => item.DisplayName, item => item.Id.ToString(), value, true, "-- Select Non-Software User --", "0");
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        #endregion

        #region Branch

        public static MvcHtmlString AllServiceBranchList(this HtmlHelper html, string name, string value, bool isSelectTitle, string title, object htmlAttributes)
        {
            var selectList = UserBranchHelperMainContent(name, value, isSelectTitle, title, htmlAttributes, UserSessionEngine.Locations(Current.AgencyId, Current.UserId, Current.AcessibleServices, Current.SessionId));
            if (selectList.IsNotNullOrEmpty())
            {
                return MvcHtmlString.Create(selectList);
            }
            var items = new List<SelectListItem>();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BranchOnlyList(this HtmlHelper html, string name, string value, int service, object htmlAttributes)
        {
            var selectList = BranchHelper(name, value, service, false, string.Empty, false, htmlAttributes);
            if (selectList.IsNotNullOrEmpty())
            {
                return MvcHtmlString.Create(selectList);
            }
            var items = new List<SelectListItem>();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BranchList(this HtmlHelper html, string name, string value, int service, object htmlAttributes)
        {
            return html.BranchList(name, value, service, false, htmlAttributes);
        }

        public static MvcHtmlString BranchList(this HtmlHelper html, string name, string value, int service, bool useTextBox, object htmlAttributes)
        {
            var selectList = BranchHelper(name, value, service, true, "-- All Branches --", useTextBox, htmlAttributes);
            if (selectList.IsNotNullOrEmpty())
            {
                return MvcHtmlString.Create(selectList);
            }
            var items = new List<SelectListItem>();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BranchList(this HtmlHelper html, string name, string value, int service, bool IsTitleSelect, string titile, object htmlAttributes)
        {
            var locations = UserSessionEngine.Locations(Current.AgencyId, Current.UserId, Current.AcessibleServices, Current.SessionId);
            if (locations.IsNotNullOrEmpty())
            {
                locations.RemoveAll(l => ((int)l.Services & service) != service);
            }
            var selectList = UserBranchHelperMainContent(name, value, IsTitleSelect, titile, htmlAttributes, locations);
            if (selectList.IsNotNullOrEmpty())
            {
                return MvcHtmlString.Create(selectList);
            }
            var items = new List<SelectListItem>();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
      
        #endregion

        #region Insurance

        /// <summary>
        /// Creates a drop down with select insurance option
        /// </summary>
        /// <param name="html"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="branchId">The Id reperesenting the Agency's branch is only used if the service is HomeHealth</param>
        /// <param name="service"></param>
        /// <param name="addNew"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString InsurancesByBranch(this HtmlHelper html, string name, string value, Guid branchId, int service, bool addNew, object htmlAttributes)
        {
            var selectList = new StringBuilder();
            var attributes = htmlAttributes.GetStyle();
            selectList.AppendFormat("<select name={0} {1}><option value='{2}' IsHmo='{3}'>-- Select Insurance --</option>", name, attributes, "0", 0);
            if (!Current.IsAgencyFrozen && Current.HasRight(service, ParentPermission.Insurance, PermissionActions.Add))
            {
                if (addNew)
                {
                    selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                    selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                    selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                }
            }
            var agencyInsurances = agencyRepository.GetAgencyInsuranceSelection(Current.AgencyId, null, null);
            var isIsuranceExist = agencyInsurances.IsNotNullOrEmpty();
            var isGroupExist = false;
            if (service == (int)AgencyServices.HomeHealth)
            {
                var insuranceId = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
                if (Enum.IsDefined(typeof(MedicareIntermediary), insuranceId))
                {
                    selectList.AppendFormat("<option value='{0}' Itype='medicaretraditional' IsHmo='{1}'{2}>{3}</option>", insuranceId, 0, ((insuranceId.ToString().IsEqual(value)) ? " selected" : ""), ((MedicareIntermediary)insuranceId).GetDescription());
                }
            }
            else if (service == (int)AgencyServices.PrivateDuty)
            {
                //TODO: replace 18 with enum value representing private duty self pay insurance
                selectList.AppendFormat("<option value='{0}' Itype='genericprivateduty' IsHmo='{3}' {1}>{2}</option>", 18, value == "18" ? " selected" : "", " Patient Self Pay", 0);
                if (isIsuranceExist)
                {
                    var selfPayInsurances = agencyInsurances.Where(i => i.PayorType == (int)PayerTypes.Selfpay).ToList();
                    if (selfPayInsurances.IsNotNullOrEmpty())
                    {
                        isGroupExist = true;
                        selectList.AppendFormat("<optgroup label='Self Pay Insurances' >");
                        selfPayInsurances.ForEach(insurance => selectList.AppendFormat("<option value='{0}' Itype='{1}' IsHmo='{4}' {2}>{3}</option>", insurance.Id.ToString(), insurance.PayorType == (int)PayerTypes.MedicareHMO ? "medicarehmo" : (insurance.PayorType == (int)PayerTypes.Selfpay && service == (int)AgencyServices.PrivateDuty ? "selfpay" : "other"), ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name, 1));
                        selectList.AppendFormat("</optgroup>");
                        agencyInsurances.RemoveAll(i => i.PayorType == (int)PayerTypes.Selfpay);
                    }
                }
            }

            if (agencyInsurances.IsNotNullOrEmpty())
            {
                agencyInsurances = agencyInsurances.OrderBy(a => a.Name).ToList();
                if (isGroupExist)
                {
                    selectList.AppendFormat("<optgroup label='Other Insurances' >");
                }
                agencyInsurances.ForEach(insurance => selectList.AppendFormat("<option value='{0}' Itype='{1}' IsHmo='{4}' {2}>{3}</option>", insurance.Id.ToString(), insurance.PayorType == (int)PayerTypes.MedicareHMO ? "medicarehmo" : (insurance.PayorType == (int)PayerTypes.Selfpay && service == (int)AgencyServices.PrivateDuty ? "selfpay" : "other"), ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name, 1));
                if (isGroupExist)
                {
                    selectList.AppendFormat("</optgroup>");
                }
            }
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString InsurancesMedicareByBranch(this HtmlHelper html, string name, string value, Guid branchId, bool isMedicareHMOOnly, bool isSelectAll, string title, object htmlAttributes)
        {
            var insuranceId = AgencyInformationHelper.Payor(Current.AgencyId, branchId);
            var includedPayorTypes = new List<int>();
            if (isMedicareHMOOnly)
            {
                includedPayorTypes.Add((int)PayerTypes.MedicareHMO);
            }
            var items = InsuranceHelper(value, isSelectAll, title, insuranceId, includedPayorTypes);
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, int service, bool isPatientPrivatePayIncluded, bool isTitleNeeded, string title, object htmlAttributes)
        {
            return MvcHtmlString.Create(InsuranceHelper(name, value, service, isTitleNeeded, title, htmlAttributes, null, true, isPatientPrivatePayIncluded));
        }

        public static MvcHtmlString InsurancesNoneMedicareTraditional(this HtmlHelper html, string name, string value, int service, bool isPatientPrivatePayIncluded, bool isSelectAll, string title, object htmlAttributes)
        {
            return MvcHtmlString.Create(InsuranceHelper(name, value, service, isSelectAll, title, htmlAttributes, null, false, isPatientPrivatePayIncluded));
        }

        public static MvcHtmlString InsurancesMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            return MvcHtmlString.Create(InsuranceHelper(name, value, (int)AgencyServices.HomeHealth, isSelectAll, title, htmlAttributes, new List<int> { (int)PayerTypes.MedicareHMO }, true, false));
        }

        #endregion

        #region DatePicker

        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, DateTime value, bool isRequired)
        {
            return DatePicker(html, name, value != DateTime.MinValue ? value.ToZeroFilled() : string.Empty, isRequired, new { });
        }

        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, DateTime value, bool isRequired, object htmlAttributes)
        {
            return DatePicker(html, name, value != DateTime.MinValue ? value.ToZeroFilled() : string.Empty, isRequired, htmlAttributes);
        }

        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, string value, bool isRequired, object htmlAttributes)
        {
            return DatePicker(html, name, value, string.Empty, string.Empty, isRequired, htmlAttributes);
        }

        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, DateTime value, DateTime maxDate, bool isRequired, object htmlAttributes)
        {
            string maxDateString = maxDate != DateTime.MinValue ? maxDate.ToZeroFilled() : string.Empty;
            return DatePicker(html, name, value != DateTime.MinValue ? value.ToZeroFilled() : string.Empty, string.Empty, maxDateString, isRequired, htmlAttributes);
        }

        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, DateTime value, DateTime minDate, DateTime maxDate, bool isRequired, object htmlAttributes)
        {
            string minDateString = minDate != DateTime.MinValue ? minDate.ToZeroFilled() : string.Empty;
            string maxDateString = maxDate != DateTime.MinValue ? maxDate.ToZeroFilled() : string.Empty;
            return DatePicker(html, name, value != DateTime.MinValue ? value.ToZeroFilled() : string.Empty, minDateString, maxDateString, isRequired, htmlAttributes);
        }

        public static MvcHtmlString DatePicker(this HtmlHelper html, string name, string value, string minDate, string maxDate, bool isRequired, object htmlAttributes)
        {
            string classString = htmlAttributes.GetProperty<string>("class");
            string idString = htmlAttributes.GetProperty<string>("id");
            string styleString = htmlAttributes.GetProperty<string>("style");
            string defaultString = htmlAttributes.GetProperty<string>("default");
            styleString = styleString.IsNotNullOrEmpty() ? string.Format("style='{0}'", styleString) : string.Empty;
            string minDateString = minDate.IsNotNullOrEmpty() ? string.Format("mindate='{0}'", minDate) : string.Empty;
            string maxDateString = maxDate.IsNotNullOrEmpty() ? string.Format("maxdate='{0}'", maxDate) : string.Empty;
            string input = string.Format("<input name='{0}' type='text' value='{1}' class='date-picker {2} {3}' id='{4}' {5} {6} {7} default='{8}'/>", name, value, isRequired ? "required" : string.Empty, classString, idString, styleString, minDateString, maxDateString, defaultString);
            return MvcHtmlString.Create(input);
        }

        #endregion

        #region TimePicker

        public static MvcHtmlString TimePicker(this HtmlHelper html, string name, DateTime value, bool isRequired)
        {
            return TimePicker(html, name, value != DateTime.MinValue ? value.ToShortTimeString() : string.Empty, isRequired, new { });
        }

        public static MvcHtmlString TimePicker(this HtmlHelper html, string name, bool isRequired, object htmlAttributes)
        {
            return TimePicker(html, name, string.Empty, isRequired, htmlAttributes);
        }

        public static MvcHtmlString TimePicker(this HtmlHelper html, string name, DateTime value, bool isRequired, object htmlAttributes)
        {
            return TimePicker(html, name, value != DateTime.MinValue ? value.ToShortTimeString() : string.Empty, isRequired, htmlAttributes);
        }

        public static MvcHtmlString TimePicker(this HtmlHelper html, string name, string value, bool isRequired, object htmlAttributes)
        {
            var classString = htmlAttributes.GetProperty<string>("class");
            var idString = htmlAttributes.GetProperty<string>("id");
            var styleString = htmlAttributes.GetProperty<string>("style");
            var defaultString = htmlAttributes.GetProperty<string>("default");
            styleString = styleString.IsNotNullOrEmpty() ? string.Format("style='{0}'", styleString) : string.Empty;
            string input = string.Format("<input name='{0}' type='text' value='{1}' class='time-picker {2} {3}' id='{4}' {5} default='{6}'/>", name, value, isRequired ? "required" : string.Empty, classString, idString, styleString, defaultString);
            return MvcHtmlString.Create(input);
        }

        public static MvcHtmlString YesNoNaSelectList(this HtmlHelper html, string name, string selectedValue, object htmlAttributes)
        {
            var list = new List<SelectListItem>()
             {
                 new SelectListItem() { Value = "2", Text = "N/A", Selected = selectedValue.Equals("2")},
                 new SelectListItem() { Value = "0", Text = "No", Selected = selectedValue.Equals("0") },
				 new SelectListItem() { Value = "1", Text = "Yes", Selected = selectedValue.Equals("1") }
             };
            return SelectList(html, name, list, htmlAttributes);
        }

        #endregion

        private static string SelectListHelper(string name, IEnumerable<SelectListItem> items, object htmlAttributes)
        {
            var builder = new StringBuilder();
            var classValue = htmlAttributes.GetProperty<string>("class");
            var idValue = htmlAttributes.GetProperty<string>("id");
            var styleValue = htmlAttributes.GetProperty<string>("style");
            var multipleValue = htmlAttributes.GetProperty<string>("multiple");
            var multipleValueString = multipleValue.IsNotNullOrEmpty() ? string.Format("multiple='{0}'", multipleValue) : string.Empty;
            builder.AppendFormat("<select class='{0}' id='{1}' name='{2}' style='{3}' {4}>\r\n", classValue, idValue, name, styleValue, multipleValueString);
            var list = items.ToList();
            if (list.IsNotNullOrEmpty())
            {
                list.ForEach(item => builder.Append(TagBuilderFactory.BuildSelectOption(item.Text, item.Value, item.Selected).ToString()));
            }
            builder.AppendFormat("</select>");
            return builder.ToString();
        }

        public static MvcHtmlString SelectList(this HtmlHelper html, string name, IEnumerable<SelectListItem> items, object htmlAttributes)
        {
            return MvcHtmlString.Create(SelectListHelper(name, items, htmlAttributes));
        }

        public static MvcHtmlString SelectYear(this HtmlHelper html, string name, int value, int startYear, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            for (int i = DateTime.Now.Year; i >= startYear; i--)
            {
                items.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = i == value });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString SampleYear(this HtmlHelper html, string name, object htmlAttributes)
        {
            return html.SelectYear(name, DateTime.Now.Year, 2010, htmlAttributes);
        }

        public static MvcHtmlString LimitedAgencyServicesRow(this HtmlHelper html, string name, string hiddenName, string label, AgencyServices value, object htmlAttributes)
        {
            AgencyServices agencyServices = agencyRepository.GetServicesAccessibleForAgency(Current.AgencyId);
            if (agencyServices.IsAlone())
            {
                return html.Hidden(hiddenName, agencyServices);
            }
            var list = GenerateServicesSelectList(value, agencyServices, true, true);
            var id = htmlAttributes.GetProperty<string>("id");
            var builder = TagBuilderFactory.BuildRow(id, label, html.SelectList(name, list.AsEnumerable(), htmlAttributes).ToString());
            return MvcHtmlString.Create(builder.ToString());
        }

        public static MvcHtmlString LimitedAgencyServicesRow(this HtmlHelper html, string name, string hiddenName, string label, string rowClass, AgencyServices value, object htmlAttributes)
        {
            AgencyServices agencyServices = agencyRepository.GetServicesAccessibleForAgency(Current.AgencyId);
            if (agencyServices.IsAlone())
            {
                return html.Hidden(hiddenName, agencyServices);
            }
            var list = GenerateServicesSelectList(value, agencyServices, true, true);
            var id = htmlAttributes.GetProperty<string>("id");
            var builder = TagBuilderFactory.BuildRow(id, rowClass, label, html.SelectList(name, list.AsEnumerable(), htmlAttributes).ToString());
            return MvcHtmlString.Create(builder.ToString());
        }

        public static MvcHtmlString LimitedServicesRow(this HtmlHelper html, string name, string label, AgencyServices value, AgencyServices limiterService, object htmlAttributes)
        {
            var list = GenerateServicesSelectList(value, limiterService, true, true);
            var id = htmlAttributes.GetProperty<string>("id");
            var builder = TagBuilderFactory.BuildRow(id, label, html.SelectList(name, list.AsEnumerable(), htmlAttributes).ToString());
            return MvcHtmlString.Create(builder.ToString());
        }

        public static MvcHtmlString LimitedAgencyServicesByCurrentUser(this HtmlHelper html, string name, bool useIntegersForValues, object htmlAttributes)
        {
            var list = GenerateServicesSelectList(Current.PreferredService, Current.AcessibleServices, true, useIntegersForValues);
            return html.SelectList(name, list, htmlAttributes);
        }

        public static MvcHtmlString LimitedAgencyServicesByCurrentUser(this HtmlHelper html, string name, AgencyServices value, AgencyServices limiterService, bool useIntegersForValues, object htmlAttributes)
        {
            var list = GenerateServicesSelectList(value, Current.AcessibleServices & limiterService, true, useIntegersForValues);
            return html.SelectList(name, list, htmlAttributes);
        }

        public static MvcHtmlString LimitedAgencyServicesByCurrentUser(this HtmlHelper html, string name, AgencyServices value, AgencyServices limiterService, bool useIntegersForValues, string pagePrefix, object htmlAttributes)
        {
            var builder = new StringBuilder();
            if (!limiterService.IsAlone())
            {
                builder.AppendFormat("<label for=\"{0}\">Service</label>", pagePrefix + "_" + name);
                var list = GenerateServicesSelectList(value, Current.AcessibleServices & limiterService, true, useIntegersForValues);
                builder.Append(SelectListHelper(name, list, htmlAttributes));
            }
            else
            {
                if (!Current.Services.IsAlone() && Current.AcessibleServices != limiterService)
                {
                    builder.AppendFormat("<label for=\"{0}\">Service:</label><label>{1}</label>", pagePrefix + "_" + name, value.GetDescription());
                }
                var attrString = htmlAttributes.ToHTMLAttributeString();
                builder.AppendFormat("<input type=\"hidden\" name=\"{0}\" value=\"{1}\" {2} />", name, (int)value, attrString);
            }
            return MvcHtmlString.Create(builder.ToString());
        }


        private static string ToHTMLAttributeString(this Object attributes)
        {
            var props = attributes.GetType().GetProperties();
            var pairs = props.Select(x => string.Format(@"{0}=""{1}""", x.Name, x.GetValue(attributes, null))).ToArray();
            return string.Join(" ", pairs);
        }


        [Obsolete("Not being used anywhere. Remove this attribute if used.")]
        public static MvcHtmlString AgencyServicesDropDown(this HtmlHelper html, string name, AgencyServices value, object htmlAttributes)
        {
            var list = GenerateServicesSelectList(value, AgencyServices.None, false, true);
            return html.SelectList(name, list.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PrintLink(this HtmlHelper html, string onClick)
        {
            return Link(html, "<span class='img icon16 print'></span>", onClick);
        }

        public static MvcHtmlString Link(this HtmlHelper html, string linkText, string onClick)
        {
            return linkText != onClick ? MvcHtmlString.Create("<a href=\"javascript:void(0);\" onclick=\"" + onClick + "\">" + linkText + "</a>") : MvcHtmlString.Create(linkText);
        }

        public static MvcHtmlString ReturnCommentsLink(this HtmlHelper html, AgencyServices service, Guid id, Guid episodeId, Guid patientId, bool isToolTip)
        {
            string classString = isToolTip ? "img icon16 red note fr" : string.Empty;
            string innerHtml = isToolTip ? string.Empty : "View Comments";
            var link = TagBuilderFactory.BuildLink(classString, string.Format("Acore.ReturnReason({{EventId:'{0}',PatientId:'{1}',UrlPrefix:'{2}'}}); return false", id, patientId, service.ToArea()), innerHtml);
            return MvcHtmlString.Create(link.ToString());
        }

        public static MvcHtmlString CreateTestNotesButton(this HtmlHelper html, Guid patientId, Guid episodeId)
        {
#if DEBUG
            var link = TagBuilderFactory.BuildLink(string.Empty, "U.PostUrl('Schedule/CreateTestNotes', { patientId: '" + patientId + "', episodeId: '" + episodeId + "'});", "Create Test Visit Notes");
            return MvcHtmlString.Create("<li>" + link + "</li>");
#endif
#if !DEBUG
            return MvcHtmlString.Create(string.Empty); 
#endif
        }

        public static MvcHtmlString CheckgroupOptions(this HtmlHelper html, string name, string id, string[] names, string[] values, object optionHtmlAttributes)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < names.Length; i++)
            {
                var value = (i + 1).ToString();
                sb.Append(html.CheckgroupOption(name, id + value, value, values.Contains(value), names[i], optionHtmlAttributes));
            }
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("ul", "checkgroup", sb.ToString()).ToString());
        }

        public static MvcHtmlString ServiceCheckgroupOptions(this HtmlHelper html, string name, string id, AgencyServices availableServices, AgencyServices values, bool isRadio, object optionHtmlAttributes)
        {
            var list = Enum.GetValues(typeof(AgencyServices))
                 .Cast<AgencyServices>()
                 .Where(x => ((availableServices & x) == x) && x != AgencyServices.None).ToList();
            var sb = new StringBuilder();
            if (list.IsNotNullOrEmpty())
            {
                list.ForEach(value => sb.Append(html.CheckgroupOption(name, id + "_" + value.ToString(), ((int)value).ToString(), (values & value) == value, isRadio, value.GetDescription(), optionHtmlAttributes, null)));
            }
            string wide = "three-wide";
            if (list.Count == 2) wide = "two-wide";
            else if (list.Count == 4) wide = "four-wide";
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("ul", "checkgroup " + wide, sb.ToString()).ToString());
        }


        //public static MvcHtmlString Locations(this HtmlHelper html, List<UserServiceLocation> ServiceLocations, AgencyServices availableServices, string @class)
        //{
        //    var branches = agencyRepository.GetBranchesForUserControl(Current.AgencyId, (int)availableServices);
        //    var list = Enum.GetValues(typeof(AgencyServices)).Cast<AgencyServices>().Where(x => ((availableServices & x) == x) && x != AgencyServices.None).ToList();
        //    var sb = new StringBuilder();
        //    if (list.IsNotNullOrEmpty())
        //    {
        //        list.ForEach(value =>
        //        {
        //            sb.Append("<div class=\"column\">");
        //            sb.AppendFormat("<input type=\"hidden\" name=\"ServiceLocations.Index\" value=\"{0}\" />", (int)value);
        //            sb.AppendFormat("<input type=\"hidden\" name=\"ServiceLocations[{0}].Service\" value=\"{0}\" />", (int)value);
        //            //sb.Append("<div class=\"row\">");
        //            //sb.Append(TagBuilderFactory.BuildTag("div", "checkgroup", html.CheckgroupOption("ServiceLocations[" + (int)value + "].Service", id + "_" + value.ToString(), ((int)value).ToString(), (values & value) == value, isRadio, value.GetDescription(), optionHtmlAttributes, null).ToString()));
        //            //sb.Append("</div>");
        //            sb.Append("<div class=\"row\">");
        //            var serviceBranches = branches.Where(b => (b.Services & value) == value);
        //            var serviceBranchValue = ServiceLocations.Where(b => (b.Service & (int)value) == (int)value).FirstOrDefault();
        //            if (serviceBranches.IsNotNullOrEmpty())
        //            {
        //                var selectList = new StringBuilder();
        //                selectList.Append("<div class=\"multiselect-checkbox\">");
        //                selectList.AppendFormat("<label><input type=\"checkbox\" name=\"ServiceLocations[{0}].IsAllLocation\" value=\"1\" />All</label>", (int)value , serviceBranchValue.IsAllLocation ? " selected" : string.Empty);
        //                serviceBranches.ForEach(branch =>
        //                {
        //                    selectList.AppendFormat("<label><input type=\"checkbox\" name=\"{0}\" value=\"{1}\" {2} class=\"{3}\"/>{4}</label>", "ServiceLocations[" + (int)value + "].Locations", branch.Id, serviceBranchValue.IsAllLocation || serviceBranchValue.Locations.Contains(branch.Id) ? " selected" : string.Empty, @class, branch.Name);
        //                    //selectList.AppendFormat("<option value='{0}' {1}  > {2} </option>", branch.Id, true ? " selected" : string.Empty, branch.Name);
        //                });
        //                selectList.Append("</div>");
        //                //selectList.AppendFormat("<select name={0} {1} class=\"multiple required\">", "Service[" + (int)value + "].Locations", string.Empty);// attributes);
        //                //serviceBranches.ForEach(branch => selectList.AppendFormat("<option value='{0}' {1}  > {2} </option>", branch.Id, true ? " selected" : string.Empty, branch.Name));
        //                //selectList.Append("</select>");
        //                sb.Append(selectList);
        //            }
        //            sb.Append("</div>");
        //            sb.Append("</div>");
        //        }
        //        );
        //    }


        //    //string wide = "three-wide";
        //    //if (list.Count == 2) wide = "two-wide";
        //    //else if (list.Count == 4) wide = "four-wide";
        //    return MvcHtmlString.Create(sb.ToString());
        //}
        public static MvcHtmlString Locations2(this HtmlHelper html, List<UserLocation> ServiceLocations, AgencyServices availableServices, string @class)
        {
            var branches = agencyRepository.GetBranchesForUserControl(Current.AgencyId, (int)availableServices).OrderByDescending(b => b.IsMainOffice).ThenBy(b => b.Name);
            var list = Enum.GetValues(typeof(AgencyServices)).Cast<AgencyServices>().Where(x => ((availableServices & x) == x) && x != AgencyServices.None).ToList();
            var sb = new StringBuilder();
            var locationSb = new StringBuilder();
            var listOfServiceControl = new Dictionary<AgencyServices, StringBuilder>();
            list.ForEach(l =>
            {
                listOfServiceControl.Add(l, new StringBuilder(string.Format("<label><input type=\"checkbox\" name=\"AllLocationServices\" value=\"{0}\" />All</label>", (int)l)));
            });
            var index = 0;
            if (branches.IsNotNullOrEmpty())
            {
                branches.ForEach(branch =>
                    {
                        var location = ServiceLocations.FirstOrDefault(sl => sl.LocationId == branch.Id);
                        var addLocation = false;
                        list.ForEach(s =>
                        {
                            if ((branch.Services & s) == s)
                            {
                                addLocation = true;
                                listOfServiceControl[s].AppendFormat("<label><input type=\"checkbox\" name=\"{0}\" value=\"{1}\" {2} class=\"{3}\"/>{4}</label>", "ServiceLocations[" + index + "].Services", (int)s, location != null && ((location.Services & s) == s) ? " checked='checked'" : string.Empty, @class, branch.Name);
                            }
                        });
                        if (addLocation)
                        {
                            locationSb.AppendFormat("<input type=\"hidden\" name=\"ServiceLocations[{0}].Location\" value=\"{1}\" />", index, branch.Id);
                            locationSb.AppendFormat("<input type=\"hidden\" name=\"ServiceLocations.Index\" value=\"{0}\" />", index);
                            index++;
                        }
                    });
            }

            sb.Append(locationSb);
            list.ForEach(value =>
                {
                    sb.Append("<div class=\"column\">");
                    sb.Append("<div class=\"row\">");
                    sb.Append("<div class=\"multiselect-checkbox\">");

                    sb.Append(listOfServiceControl[value]);
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                });

            return MvcHtmlString.Create(sb.ToString());
        }



        public static MvcHtmlString CheckgroupOption(this HtmlHelper html, string name, string id, string value, bool isChecked, string labelText, object htmlAttributes)
        {
            return html.CheckgroupOption(name, id, value, isChecked, labelText, htmlAttributes, null);
        }

        public static MvcHtmlString CheckgroupOption(this HtmlHelper html, string name, string value, bool isChecked, string labelText)
        {
            return CheckgroupOption(html, name, name + value, value, isChecked, labelText, null, null);
        }

        public static MvcHtmlString CheckgroupOption(this HtmlHelper html, string name, string value, bool isChecked, string labelPrefix, string labelText)
        {
            string label = string.Format("<span class=\"fl\">{0} &#8211;</span><span class=\"margin\">{1}</span>", labelPrefix, labelText);
            return CheckgroupOption(html, name, name + value, value, isChecked, label, null, null);
        }

        public static MvcHtmlString CheckgroupOption(this HtmlHelper html, string name, string value, bool isChecked, string labelPrefix, string labelText, object optionHtmlAttributes)
        {
            string label = string.Format("<span class=\"fl\">{0} &#8211;</span><span class=\"margin\">{1}</span>", labelPrefix, labelText);
            return CheckgroupOption(html, name, name + value, value, isChecked, label, optionHtmlAttributes, null);
        }

        public static MvcHtmlString CheckgroupRadioOption(this HtmlHelper html, string name, string value, bool isChecked, string labelText)
        {
            return CheckgroupOption(html, name, name + value, value, isChecked, true, labelText, null, null);
        }

        public static MvcHtmlString CheckgroupRadioOption(this HtmlHelper html, string name, string value, bool isChecked, string labelPrefix, string labelText)
        {
            string label = string.Format("<span class=\"fl\">{0} &#8211;</span><span class=\"margin\">{1}</span>", labelPrefix, labelText);
            return CheckgroupOption(html, name, name + value, value, isChecked, true, label, null, null);
        }

        public static MvcHtmlString CheckgroupRadioOption(this HtmlHelper html, string name, string value, bool isChecked, string labelPrefix, string labelText, object optionHtmlAttributes)
        {
            string label = string.Format("<span class=\"fl\">{0} &#8211;</span><span class=\"margin\">{1}</span>", labelPrefix, labelText);
            return CheckgroupOption(html, name, name + value, value, isChecked, true, label, optionHtmlAttributes, null);
        }

        public static MvcHtmlString CheckgroupOption(this HtmlHelper html, string name, string id, string value, bool isChecked, string labelText, object optionHtmlAttributes, object labelHtmlAttributes)
        {
            return html.CheckgroupOption(name, id, value, isChecked, false, labelText, optionHtmlAttributes, labelHtmlAttributes);
        }

        public static MvcHtmlString CheckgroupOption(this HtmlHelper html, string name, string id, string value, bool isChecked, bool isRadio, string labelText, object optionHtmlAttributes, object labelHtmlAttributes)
        {
            var option = isRadio ? TagBuilderFactory.BuildRadio(optionHtmlAttributes.GetProperty<string>("class"), id, name, value, isChecked)
                : TagBuilderFactory.BuildCheckBox(optionHtmlAttributes.GetProperty<string>("class"), id, name, value, isChecked);
            var label = TagBuilderFactory.BuildLabel(labelHtmlAttributes.GetProperty<string>("class"), id, labelText);
            var wrapper = TagBuilderFactory.BuildTag("div", "wrapper", option.ToString() + label);
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("li", wrapper.ToString(), new { @class = "option", tooltip = optionHtmlAttributes.GetProperty<string>("tooltip") }).ToString());
        }

        public static MvcHtmlString CheckgroupOptionWithOther(this HtmlHelper html, string name, string value, bool isChecked, string labelText, string otherName, string otherValue)
        {
            return html.CheckgroupOptionWithOther(name, value, isChecked, false, labelText, otherName, otherValue);
        }

        public static MvcHtmlString CheckgroupOptionWithOther(this HtmlHelper html, string name, string value, bool isChecked, bool isRadio, string labelText, string otherName, string otherValue)
        {
            var option = isRadio ? TagBuilderFactory.BuildRadio(string.Empty, name + value, name, value, isChecked)
                : TagBuilderFactory.BuildCheckBox(string.Empty, name + value, name, value, isChecked);
            var label = TagBuilderFactory.BuildLabel(string.Empty, name + value, labelText);
            var wrapper = TagBuilderFactory.BuildTag("div", "wrapper", option.ToString() + label);
            var moreInnerDiv = TagBuilderFactory.BuildTag("div", "fr", TagBuilderFactory.BuildInput(otherName, "text", otherValue, string.Empty, otherName).ToString());
            var moreLabel = TagBuilderFactory.BuildLabel("fl", otherName, "Specify");
            var more = TagBuilderFactory.BuildTag("div", "more ac", moreLabel.ToString() + moreInnerDiv.ToString() + TagBuilderFactory.ClearDiv);
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("li", wrapper.ToString() + more.ToString(), new { @class = "option" }).ToString());
        }

        public static MvcHtmlString OasisCheckgroupRadioOptions(this HtmlHelper html, string name, string prefix, IDictionary<string, Question> data, string[] values, string[] labels, string optionFormatting)
        {
            string answer = data.AnswerOrEmptyString(name);
            var sb = new StringBuilder();
            for (int i = 0; i < labels.Length; i++)
            {
                var value = values[i];
                sb.Append(html.OasisCheckgroupRadioOption(name, prefix + "_" + name + (i + 1), value, answer.IsEqual(value), (i + 1).ToString(), labels[i], false));
            }
            var hidden = TagBuilderFactory.BuildInput(name, "hidden", " ");
            var div = TagBuilderFactory.BuildTag("ul", "checkgroup " + optionFormatting, sb.ToString());
            return MvcHtmlString.Create(hidden.ToString() + div);
        }

        public static MvcHtmlString OasisCheckgroupRadioOption(this HtmlHelper html, string name, string id, string value, bool isChecked, string labelPrefix, string labelText, bool createHidden)
        {
            var option = TagBuilderFactory.BuildRadio(string.Empty, id, name, value, isChecked);
            return OasisCheckgroupBase(name, id, labelPrefix, labelText, createHidden, option);
        }

        //public static MvcHtmlString OasisCheckgroupOptionShared(this HtmlHelper html, string name, string prefix, string value, IDictionary<string, Question> data, string labelText, object optionHtmlAttributes)
        //{
        //    var labelNumber = value.TrimStart('0');
        //    return html.OasisCheckgroupOption(name, prefix + "_" + name + labelNumber, value, data, labelNumber, labelText, false, optionHtmlAttributes);
        //}
        public static MvcHtmlString OasisCheckgroupOption(this HtmlHelper html, string name, string prefix, IDictionary<string, Question> data, string labelPrefix, string labelText, bool createHidden)
        {
            return html.OasisCheckgroupOption(name, prefix + "_" + name, "1", data.AnswerOrEmptyString(name).IsEqual("1"), labelPrefix, labelText, createHidden);
        }

        public static MvcHtmlString OasisCheckgroupOption(this HtmlHelper html, string name, string id, string value, IDictionary<string, Question> data, string labelPrefix, string labelText, bool createHidden)
        {
            return html.OasisCheckgroupOption(name, id, value, data.AnswerOrEmptyString(name).IsEqual(value), labelPrefix, labelText, createHidden);
        }

        public static MvcHtmlString OasisCheckgroupOption(this HtmlHelper html, string name, string id, string value, bool isChecked, string labelPrefix, string labelText, bool createHidden)
        {
            var option = TagBuilderFactory.BuildCheckBox(string.Empty, id, name, value, isChecked);
            return OasisCheckgroupBase(name, id, labelPrefix, labelText, createHidden, option);
        }

        public static MvcHtmlString GroupedUsersAndNonUsers(this HtmlHelper html, string name, Guid value, string title, object htmlAttributes)
        {
            var list = new List<GroupedSelectListItem>();
            var usersAndNonUsers = userRepository.GetUsersAndNonUsers(Current.AgencyId).OrderBy(o => o.IsNonUser).ThenBy(o => o.DisplayName);
            usersAndNonUsers.ForEach(u => list.Add(new GroupedSelectListItem() { Selected = u.Id == value, Value = u.Id.ToString(), GroupKey = u.IsNonUser.ToInteger().ToString(), GroupName = u.IsNonUser ? "Non-Software User" : "User", Text = u.DisplayName }));
            list.Insert(0, new GroupedSelectListItem { Text = title, Value = Guid.Empty.ToString(), Selected = value == Guid.Empty });
            return html.DropDownGropList(name, list, htmlAttributes);
        }

        public static MvcHtmlString YesNoCheckGroup(this HtmlHelper html, string name, string value)
        {
            var yes = html.CheckgroupOption(name, name + "Y", "1", value.Equals("1"), true, "Yes", null, null);
            var no = html.CheckgroupOption(name, name + "N", "0", value.Equals("0"), true, "No", null, null);
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("ul", "checkgroup two-wide", yes.ToString() + no.ToString()).ToString());
        }

        public static MvcHtmlString Filter(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("div", "filter", html.Hidden(name, value, htmlAttributes).ToString()).ToString());
        }

        public static MvcHtmlString CarePlanStatusSelect(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var list = new List<SelectListItem>()
                       {
                           new SelectListItem(){ Value = " ", Selected = value.IsNullOrEmpty(), Text = "-- Select Status --"},
                           new SelectListItem(){ Value = "0", Selected = "0" == value, Text = "Not Applicable"},
                           new SelectListItem(){ Value = "1", Selected = "1" == value, Text = "Patient Request"},
                           new SelectListItem(){ Value = "2", Selected = "2" == value, Text = "Every Week"},
                           new SelectListItem(){ Value = "3", Selected = "3" == value, Text = "Every Visit"},
                       };
            return html.DropDownList(name, list, htmlAttributes);
        }

        #endregion

        private static IEnumerable<SelectListItem> GenerateSelectListItems<T>(IList<T> items, Func<T, string> textFunc, Func<T, string> valueFunc, string value, bool isDefaultSelectIncluded, string title, string defaultValue)
        {
            return GenerateSelectListItems(items, textFunc, valueFunc, arg => true, value, isDefaultSelectIncluded, title, defaultValue);
        }

        private static IEnumerable<SelectListItem> GenerateSelectListItems<T>(IList<T> items, Func<T, string> textFunc, Func<T, string> valueFunc, Func<T, bool> whereFunc, string value, bool isDefaultSelectIncluded, string title, string defaultValue)
        {
            return items.GenerateSelectListItems(textFunc, valueFunc, whereFunc, value, isDefaultSelectIncluded, title, defaultValue);
        }

        private static MvcHtmlString OasisCheckgroupBase(string name, string id, string labelPrefix, string labelText, bool createHidden, TagBuilder option)
        {
            var hidden = string.Empty;
            if (createHidden)
            {
                hidden = TagBuilderFactory.BuildInput(name, "hidden", " ").ToString();
            }
            var firstLabelSpan = TagBuilderFactory.BuildTag("span", "fl", labelPrefix + " &#8211;");
            var secondLabelSpan = TagBuilderFactory.BuildTag("span", "normal margin", labelText);
            var label = TagBuilderFactory.BuildLabel(string.Empty, id, firstLabelSpan.ToString() + secondLabelSpan);
            var wrapper = TagBuilderFactory.BuildTag("div", "wrapper", hidden + option + label);
            return MvcHtmlString.Create(TagBuilderFactory.BuildTag("li", "option", wrapper.ToString()).ToString());
        }

        private static IEnumerable<SelectListItem> GenerateServicesSelectList(AgencyServices selectedService, AgencyServices limiterService, bool isLimited, bool useIntegersForValues)
        {
            var enums = Enum.GetValues(typeof(AgencyServices));
            return (from AgencyServices service in enums where service != AgencyServices.None where !isLimited || limiterService.Has(service) let value = useIntegersForValues ? ((int)service).ToString() : service.ToString() select new SelectListItem { Text = service.GetDescription(), Value = value, Selected = selectedService.Has(service) }).ToList();
        }

        public static string ProperCheckBox(this HtmlHelper html, string name, string key, IDictionary<string, NotesQuestion> data)
        {
            return string.Format("<input name='{0}_" + name + "' value='true' type='checkbox' {1} />", key, (data.ContainsKey(name) && data[name].Answer == "true").ToChecked());
        }

        private static string BranchHelper(string name, string value, int service, bool isSelectTitle, string title, bool useTextBoxInsteadOfLabel, object htmlAttributes)
        {
            return BranchHelperMainContent(name, value, isSelectTitle, title, htmlAttributes,agencyRepository.GetBranchesForUserControl(Current.AgencyId, service).OrderByDescending(b => b.IsMainOffice).ThenBy(b => b.Name));
        }

        private static string BranchHelperMainContent(string name, string value, bool isSelectTitle, string title, object htmlAttributes, IOrderedEnumerable<AgencyLocation> branches)
        {
            var payor = Current.Payor;
            var attributes = htmlAttributes.GetStyle();
            var selectList = new StringBuilder();
            selectList.AppendFormat("<select name={0} {1}>", name, attributes);
            if (isSelectTitle)
            {
                selectList.AppendFormat("<option value='{0}' Payor='{1}'>{2}</option>", Guid.Empty, payor, title);
            }
            if (branches.IsNotNullOrEmpty())
            {
                branches.ForEach(branch => selectList.AppendFormat("<option value='{0}' Payor='{1}' {2} Service='{4}' > {3} </option>", branch.Id.ToString(), branch.IsLocationStandAlone ? branch.Payor : payor, branch.Id.ToString() == value ? " selected" : string.Empty, branch.Name, (int)branch.Services));
            }
            selectList.Append("</select>");
            return selectList.ToString();
        }

        private static string UserBranchHelperMainContent(string name, string value, bool isSelectTitle, string title, object htmlAttributes, List<UserLocation> branches)
        {
            var payor = Current.Payor;
            var attributes = htmlAttributes.GetStyle();
            var selectList = new StringBuilder();
            selectList.AppendFormat("<select name={0} {1}>", name, attributes);
            if (isSelectTitle)
            {
                selectList.AppendFormat("<option value='{0}' Payor='{1}'>{2}</option>", Guid.Empty, payor, title);
            }
            if (branches.IsNotNullOrEmpty())
            {
                branches.ForEach(branch => selectList.AppendFormat("<option value='{0}' Payor='{1}' {2} Service='{4}' > {3} </option>", branch.LocationId.ToString(), branch.IsLocationStandAlone ? branch.Payor : payor, branch.LocationId.ToString() == value ? " selected" : string.Empty, branch.LocationName, (int)branch.Services));
            }
            selectList.Append("</select>");
            return selectList.ToString();
        }

        private static string InsuranceHelper(string name, string value, int service, bool isTitleNeeded, string title, object htmlAttributes, List<int> onlyIncludedPayorTypes, bool isTraditionalMedicareIncluded, bool isPatientPrivatePayIncluded)
        {
            var attributes = htmlAttributes.GetStyle();
            var selectList = new StringBuilder();
            selectList.AppendFormat("<select name={0} {1}>", name, attributes);
            if (isTitleNeeded)
            {
                selectList.AppendFormat("<option value='{0}' Itype='default'>{1}</option>", 0, title);
            }
            var agencyInsurances = agencyRepository.GetAgencyInsuranceSelection(Current.AgencyId, onlyIncludedPayorTypes, null);
            var isIsuranceExist = agencyInsurances.IsNotNullOrEmpty();
            var isGroupExist = false;
            if (service == (int)AgencyServices.HomeHealth)
            {
                if (isTraditionalMedicareIncluded)
                {
                    Array list = Enum.GetValues(typeof(MedicareIntermediary));
                    foreach (MedicareIntermediary item in list)
                    {
                        selectList.AppendFormat("<option value='{0}' Itype='medicaretraditional' {1}>{2}</option>", (int)item, ((int)item).ToString().IsEqual(value).ToSelected(), item.GetDescription());
                    }
                }
            }
            else if (service == (int)AgencyServices.PrivateDuty)
            {
                if (isPatientPrivatePayIncluded)
                {
                    selectList.AppendFormat("<option value='{0}' Itype='genericprivateduty' {1} {3}>{2}</option>", 18, "", " Patient Self Pay", ((18.ToString().IsEqual(value)) ? " selected" : ""));
                }
                if (isIsuranceExist)
                {
                    var selfPayInsurances = agencyInsurances.Where(i => i.PayorType == (int)PayerTypes.Selfpay).ToList();
                    if (selfPayInsurances.IsNotNullOrEmpty())
                    {
                        isGroupExist = true;
                        selectList.AppendFormat("<optgroup label='Self Pay Insurances' >");
                        selfPayInsurances.ForEach(insurance => selectList.AppendFormat("<option value='{0}' Itype='{1}' {2}>{3}</option>", insurance.Id.ToString(), insurance.PayorType == (int)PayerTypes.MedicareHMO ? "medicarehmo" : (insurance.PayorType == (int)PayerTypes.Selfpay && service == (int)AgencyServices.PrivateDuty ? "selfpay" : "other"), ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name));
                        selectList.AppendFormat("</optgroup>");
                        agencyInsurances.RemoveAll(i => i.PayorType == (int)PayerTypes.Selfpay);
                    }
                }
            }
            if (agencyInsurances.IsNotNullOrEmpty())
            {
                if (isGroupExist)
                {
                    selectList.AppendFormat("<optgroup label='Other Insurances' >");
                }
                agencyInsurances.ForEach(insurance => selectList.AppendFormat("<option value='{0}' Itype='{1}' {2}>{3}</option>", insurance.Id.ToString(), insurance.PayorType == (int)PayerTypes.MedicareHMO ? "medicarehmo" : (insurance.PayorType == (int)PayerTypes.Selfpay && service == (int)AgencyServices.PrivateDuty ? "selfpay" : "other"), ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name));
                if (isGroupExist)
                {
                    selectList.AppendFormat("</optgroup>");
                }
            }
            selectList.Append("</select>");
            return selectList.ToString();
        }

        private static List<SelectListItem> InsuranceHelper(string value, bool isSelectAll, string title, int insuranceId, List<int> onlyIncludedPayorTypes)
        {
            var items = new List<SelectListItem>();
            if (Enum.IsDefined(typeof(MedicareIntermediary), insuranceId))
            {
                items.Add(new SelectListItem { Text = ((MedicareIntermediary)insuranceId).GetDescription(), Value = insuranceId.ToString(), Selected = insuranceId.ToString().IsEqual(value) });
            }
            var agencyInsurances = agencyRepository.GetAgencyInsuranceSelection(Current.AgencyId, onlyIncludedPayorTypes, null);
            if (agencyInsurances.IsNotNullOrEmpty())
            {
                agencyInsurances.ForEach(i => items.Add(new SelectListItem { Text = i.Name, Value = i.Id.ToString(), Selected = i.Id.ToString().IsEqual(value) }));
            }
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }
            return items;
        }

        private static List<SelectListItem> InsuranceDisciplineTaskHelper(int disciplineTask, bool isTaskIncluded, List<int> disciplines)
        {
            var tasks = lookupRepository.DisciplineTasksForRates(DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToList(), disciplines, new List<int> { (int)DisciplineTasks.CommunicationNote }, disciplineTask, isTaskIncluded);
            return tasks.GenerateSelectListItems(args => args.Task, args => args.Id.ToString(), args => true, disciplineTask.ToString(), true, "-- Select Task --", "0");
        }
    }
}
// ReSharper restore SpecifyACultureInStringConversionExplicitly