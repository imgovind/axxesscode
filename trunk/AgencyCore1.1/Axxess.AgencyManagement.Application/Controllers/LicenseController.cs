﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class LicenseController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;

        public LicenseController(IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            this.userService = userService;
        }

        #endregion

        #region License Manager

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Manager()
        {
            return PartialView(userService.GetLicenses());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagerList()
        {
            return PartialView(userService.GetLicenses());
        }

        #endregion

        #region Licenses

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(Guid userId)
        {
            return PartialView(userService.GetUserLicenses(userId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid? userId)
        {
            return PartialView(userId.HasValue ? userId.Value : Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(License license, HttpPostedFileBase Attachment)
        {
            Check.Argument.IsNotNull(license, "License");
            var viewData = userService.AddLicense(license, Attachment);
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid licenseId, Guid userId)
        {
            return PartialView(userService.EditLicenseItem(licenseId, userId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(License licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "licenseItem");
            return Json(userService.UpdateLicense(licenseItem));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            string messageStart = userId.IsEmpty() ? "Non-Software User License" : "User License";
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = string.Format("{0} cannot be deleted. Try Again.", messageStart) };
            if (userService.DeleteLicense(Id, userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = string.Format("{0} has been successfully deleted.", messageStart);
                return Json(viewData);
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteFromGrid(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.DeleteLicense(Id, userId))
            {
                return View(new GridModel(userService.GetLicenses(userId, true)));
            }
            return View(new GridModel(new List<License>()));
        }

        #endregion
    }
}
