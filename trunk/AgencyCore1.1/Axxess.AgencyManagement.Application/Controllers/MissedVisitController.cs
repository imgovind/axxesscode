﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;

    using iTextExtension;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MissedVisitController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly HHTaskService scheduleService;

        public MissedVisitController(IPatientService patientService, HHTaskService scheduleService)
        {
            this.scheduleService = scheduleService;
            this.patientService = patientService;
        }

        #endregion

        #region MissedVisit

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId, Guid eventId)
        {
            var task = scheduleService.GetScheduleTask(patientId, eventId);
            if (task != null)
            {
                task.PatientName = patientService.GetPatientNameById(patientId);
            }
            return PartialView(task);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add([Bind] MissedVisit missedVisit)
        {
            Check.Argument.IsNotNull(missedVisit, "missedVisit");
            return Json(scheduleService.AddOrUpdateMissedVisit(missedVisit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView(scheduleService.GetMissedVisit(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid patientId, Guid eventId)
        {
            var errorMessage = "Visit could not be restored";
            var viewData = scheduleService.MissedVisitRestore(patientId, eventId);
            if (viewData.isSuccessful)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Visit has been restored.";
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, new ParentPermission[] { ParentPermission.Schedule, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<MissedVisitPdf>(new MissedVisitPdf(scheduleService.GetMissedVisitPrint(patientId, eventId)), "MissedVisit", Response);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<MissedVisitPdf>(new MissedVisitPdf(scheduleService.GetMissedVisitPrint(patientId, eventId)), "MissedVisit", Response);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<MissedVisitPdf>(new MissedVisitPdf(scheduleService.GetMissedVisitPrint(patientId, eventId)), "MissedVisit");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Blank()
        {
            return FileGenerator.Pdf<MissedVisitPdf>(new MissedVisitPdf(scheduleService.GetMissedVisitPrint()), "MissedVisit");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Popup(Guid id)
        {
            return PartialView(scheduleService.GetMissedVisit(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = scheduleService.GetMissedVisitViewData(Current.AgencyId, Guid.Empty, true, DateTime.Now.AddDays(-89), DateTime.Today, false);
            viewData.SortColumn = "PatientName";
            viewData.SortDirection = "ASC";
            viewData.GroupName = "EventDate";
            viewData.ShowTime = false;
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid branchId, DateTime startDate, DateTime endDate, string GroupName, string SortParams)
        {
            var viewData = scheduleService.GetMissedVisitViewData(Current.AgencyId, branchId, false, startDate, endDate, false);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            viewData.GroupName= GroupName.IsNotNullOrEmpty() ? GroupName : "EventDate";
            viewData.ShowTime = false; 
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(Guid patientId, Guid eventId, string reason)
        {
            return Json(scheduleService.ProcessMissedVisitNotes(patientId, eventId, "Return", reason));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(Guid patientId, Guid eventId)
        {
            return Json(scheduleService.ProcessMissedVisitNotes(patientId, eventId, "Approve", null));
        }

        #endregion
    }
}
