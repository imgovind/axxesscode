﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Services;
    using iTextExtension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    using Telerik.Web.Mvc;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MessageController : BaseController
    {
        #region Private Members

        private readonly IMessageService messageService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;

        #endregion

        #region Constructor

        public MessageController(IAgencyService agencyService,IUserService userService,IMessageService messageService)
        {
            Check.Argument.IsNotNull(messageService, "messageService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(userService, "userService");

            this.messageService = messageService;
            this.userService = userService;
            this.agencyService = agencyService;
        }

        #endregion

        #region MessageController Actions

        #region CRUD
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public JsonResult Create([Bind] Message message, HttpPostedFileBase attachment1)
        {
            Check.Argument.IsNotNull(message, "message");
            var viewData = new JsonViewData(false);
            if (message.IsValid())
            {
                viewData = messageService.SendMessage(message, attachment1);
            }
            else
            {
                viewData.errorMessage = message.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Inbox()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid id, int messageType)
        {
            return Json(messageService.GetMessage(id, messageType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult View(Guid id, int messageType, string inboxType)
        {
            var message = messageService.GetMessage(id, messageType);
            if (message != null)
            {
                if (inboxType.IsNotNullOrEmpty() && inboxType == "inbox")
                {
                    message.IsInInbox = true;
                }
            }
            return PartialView(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Grid(string inboxType)
        {
            var pageNumber = HttpContext.Request.Params["page"] != null ? int.Parse(HttpContext.Request.Params["page"]) : 1;
            var messages = messageService.GetMessages(inboxType, pageNumber).Select(s => new MessageItem() { Id = s.Id, Subject = s.Subject, FromName = s.FromName, MarkAsRead = s.MarkAsRead, Created = s.Created, Type = s.Type, FolderId = s.FolderId }).ToList();
            var messagesCount = messageService.GetTotalMessagesCount(inboxType);
            if (messages != null && messages.Count > 0)
            {
                var gridModel = new GridModel(messages);
                gridModel.Data = messages;
                gridModel.Total = messagesCount;
                return Json(gridModel);
            }
            return Json(new GridModel(new List<MessageItem>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GridWidget()
        {
            var messages = messageService.GetMessages("inbox", 1).Take(5).Select(s => new MessageItem() { Id = s.Id, Subject = s.Subject, FromName = s.FromName, MarkAsRead = s.MarkAsRead, Created = s.Created, Type = s.Type }).ToList();
            return Json(new GridModel(messages));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid id, int messageType)
        {
            var doc = new MessagePdf(messageService.GetMessage(id, messageType), agencyService.AgencyNameWithMainLocationAddress());
            var stream = doc.GetFormattedStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Message_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, int messageType, string inboxType)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Message could not be deleted." };
            if (messageType == (int)MessageType.System || messageType == (int)MessageType.User)
            {
                if (messageService.DeleteMessage(id, messageType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Message has been deleted successfully";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteMany(List<Guid> deleteMessage)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Messages could not be deleted." };
            if (deleteMessage != null && deleteMessage.Count > 0)
            {
                 viewData = messageService.DeleteMany(deleteMessage);
            }
            else
            {
                viewData.errorMessage = "You must select at least one message to delete.";
            }
            return Json(viewData);
        }

     

        #endregion

        #region Helpers
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CustomWidget()
        {
            return Json(messageService.GetCurrentDashboardMessage() ?? new DashboardMessage());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RecipientList(string searchTerm)
        {
            return Json(userService.GetMessageRecipientList(searchTerm));
        }

     
        #endregion

        #region Folder Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FolderNew()
        {
            return PartialView("Folder/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FolderCreate(string newFolderName)
        {
            var jsonData = new JsonViewData() { isSuccessful = false, errorMessage = "Error adding folder." };
            if (newFolderName.IsNullOrEmpty())
            {
                jsonData.isSuccessful = false;
                jsonData.errorMessage = "The folder name is empty.";
            }
            else
            {
                jsonData = messageService.AddMessageFolder(newFolderName);
            }
            return Json(jsonData);
        }

      

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FolderList()
        {
            return Json(messageService.FolderList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FolderDelete(Guid folderId)
        {
            JsonViewData jsonData = new JsonViewData();
            jsonData.isSuccessful = true;
            jsonData.errorMessage = (jsonData.isSuccessful ? "Folder deleted." : "Error Deleting Folder");
            return Json(jsonData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Move(Guid id, Guid folderId, string folderName, string messageType)
        {
            return Json(messageService.MoveMessageFolder(id, folderId, folderName, messageType));
        }

       

        #endregion

        #endregion
    }
}
