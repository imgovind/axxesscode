﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.Membership.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.ViewData;
   
    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : BaseController
    {
        #region Private Members/Constructor

        private readonly IErrorRepository errorRepository;
        private readonly ILookupRepository lookupRepository;

        public HomeController(IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.errorRepository = membershipDataProvider.ErrorRepository;
            this.lookupRepository = lookUpProvider.LookUpRepository;
        }

        #endregion

        #region HomeController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            var menus = MenuHelper();
            return View("Index", menus);
        }

        private List<MenuElement> MenuHelper()
        {
            var menus = new List<MenuElement>();
            var service = (int)Current.AcessibleServices;
            if (service > 0)
            {
                var menuList = lookupRepository.GetMenuElements();
                var datas = Current.Permissions ?? new Dictionary<int, Dictionary<int, List<int>>>();
                var preferredService = Current.PreferredService;
                foreach (var category in datas)
                {
                    var actions = category.Value;
                    if (actions.IsNotNullOrEmpty())
                    {
                        var menuCategoryList = menuList.Where(m => m.Category == category.Key || m.OtherCategory.Contains(category.Key.ToString())).ToList();
                        if (menuCategoryList.IsNotNullOrEmpty())
                        {
                            menuCategoryList.ForEach(m =>
                            {
                                if (((service & m.Service) == m.Service) || ((m.Service & service) == service))
                                {
                                        List<int> services;
                                        if (actions.TryGetValue(m.Action, out services))
                                        {
                                            if (m.Id.IsEqual("PDmanagedclaimedit"))
                                            {
                                                Console.WriteLine();
                                            }
                                            if (services.Exists(s => ((service & s) == s) && ((m.Service & s) == s)))
                                            {
                                                bool isMenuToBeAdded = true;
                                                if (m.IsAreaPrefix)
                                                {
                                                    if (((service & (int)preferredService) == (int)preferredService) && services.Contains((int)preferredService))
                                                    {
                                                        m.Url = string.Format("{0}/{1}", preferredService.ToArea(), m.Url);
                                                    }
                                                    else
                                                    {
                                                        var firstService = services.FirstOrDefault(s => (service & s) == s);
                                                        if (firstService > 0 && Enum.IsDefined(typeof(AgencyServices), firstService))
                                                        {
                                                            m.Url = string.Format("{0}/{1}", ((AgencyServices)firstService).ToArea(), m.Url);
                                                        }
                                                        else
                                                        {
                                                            isMenuToBeAdded = false;
                                                        }
                                                    }
                                                }
                                                if (isMenuToBeAdded)
                                                {
                                                    //Checks if the service is alone, if it is it will remove any service initials attached to parent menus
                                                    if (m.ParentMenus.IsNotNullOrEmpty() && service.IsPowerOfTwo() && m.Service.IsPowerOfTwo())
                                                    {
                                                        var servicePrefix = m.Service.ToEnum(AgencyServices.None).GetCustomShortDescription();
                                                        m.ParentMenus = m.ParentMenus.Replace(servicePrefix, "");
                                                    }
                                                    menus.Add(new MenuElement
                                                    {
                                                        Id = m.Id,
                                                        Name = m.Name,
                                                        MenuName = m.MenuName,
                                                        Url = m.Url,
                                                        OnLoad = m.OnLoad,
                                                        StyleOptions = m.StyleOptions,
                                                        ParentMenus = m.ParentMenus
                                                    });
                                                }
                                            }
                                        }
                                    }
                            });
                        }
                    }
                }
                menus.AddRange(menuList
                    .Where(w => w.Category == (int)ParentPermission.General)
                    .Select(s => new MenuElement
                    {
                        Id = s.Id,
                        Name = s.Name,
                        MenuName = s.MenuName,
                        Url = s.Url,
                        OnLoad = s.OnLoad,
                        StyleOptions = s.StyleOptions,
                        ParentMenus = s.ParentMenus
                    }));
            }
            return menus;
        }



        //private List<MenuElement> MenuHelper2()
        //{
        //    var menus = new List<MenuElement>();
        //    var service = (int)Current.AcessibleServices;
        //    if (service > 0)
        //    {
        //        var menuList = lookupRepository.GetMenuElements();
        //         var menuDictionary = menuList.GroupBy(m => m.Category).ToDictionary(g => g.Key, g => g.ToList());
        //        var datas = Current.Permissions ?? new Dictionary<int, Dictionary<int, List<int>>>();
        //        var prefredService = Current.PreferredService;
        //        var filteredActions = new int[] 
        //                                        { 
        //                                          (int)PermissionActions.Add,
        //                                          (int)PermissionActions.ViewList,
        //                                          (int)PermissionActions.Edit,
        //                                          (int)PermissionActions.Print,
        //                                          (int)PermissionActions.Reassign,
        //                                          (int)PermissionActions.ViewToExport,
        //                                          (int)PermissionActions.ViewExported,
        //                                          (int)PermissionActions.ViewPastDue,
        //                                          (int)PermissionActions.ViewUpComing,
        //                                          (int)PermissionActions.ViewOutstandingClaim,
        //                                          (int)PermissionActions.ViewPendingClaim,
        //                                          (int)PermissionActions.ViewClaimHistory
        //                                        };

        //        foreach (var mn in menuDictionary)
        //        {
        //            Dictionary<int, List<int>> categoryPermission = null;
        //            if (datas.TryGetValue(mn.Key, out categoryPermission))
        //            {
                         
        //            }
        //        }


        //        foreach (var category in datas)
        //        {

        //            //if (menuDictionary.TryGetValue(category.Key, out menuCategoryList))
        //            //{
        //            var actions = category.Value;
        //            if (actions.IsNotNullOrEmpty())
        //            {
        //                var menuCategoryList = menuList.Where(m => m.Category == category.Key);
        //                if (menuCategoryList.IsNotNullOrEmpty())
        //                {
        //                    menuCategoryList.ForEach(m =>
        //                    {
        //                        if (((service & m.Service) == m.Service) || ((m.Service & service) == service))
        //                        {
        //                            if (filteredActions.Contains(m.Action))
        //                            {
        //                                List<int> services;
        //                                if (actions.TryGetValue(m.Action, out services))
        //                                {
        //                                    if (m.ParentMenus.IsNotNullOrEmpty())
        //                                    {
        //                                        m.Menu = m.ParentMenus.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
        //                                    }
        //                                    else
        //                                    {
        //                                        m.Menu = new string[0];
        //                                    }

        //                                    if (services.Exists(s => ((service & s) == s) && ((m.Service & s) == s)))
        //                                    {
        //                                        bool isMenuToBeAdded = true;
        //                                        if (m.IsAreaPrefix)
        //                                        {
        //                                            if (((service & (int)prefredService) == (int)prefredService) && services.Contains((int)prefredService))
        //                                            {
        //                                                m.Url = string.Format("{0}/{1}", prefredService.ToArea(), m.Url);
        //                                            }
        //                                            else
        //                                            {
        //                                                var firstService = services.FirstOrDefault(s => (service & s) == s);
        //                                                if (firstService > 0 && Enum.IsDefined(typeof(AgencyServices), firstService))
        //                                                {
        //                                                    m.Url = string.Format("{0}/{1}", ((AgencyServices)firstService).ToArea(), m.Url);
        //                                                }
        //                                                else
        //                                                {
        //                                                    isMenuToBeAdded = false;
        //                                                }
        //                                            }
        //                                        }
        //                                        if (isMenuToBeAdded)
        //                                        {
        //                                            menus.Add(new MenuElement
        //                                            {
        //                                                Id = m.Id,
        //                                                Name = m.Name,
        //                                                MenuName = m.MenuName,
        //                                                Url = m.Url,
        //                                                OnLoad = m.OnLoad,
        //                                                Resize = m.Resize,
        //                                                StatusBar = m.StatusBar,
        //                                                Center = m.Center,
        //                                                IgnoreMinSize = m.IgnoreMinSize,
        //                                                Height = m.Height,
        //                                                Width = m.Width,
        //                                                VersitileHeight = m.VersitileHeight,
        //                                                MaxOnMobile = m.MaxOnMobile,
        //                                                Overflow = m.Overflow,
        //                                                Menu = m.Menu,
        //                                                IsPost = m.IsPost,
        //                                                Modal = m.Modal,
        //                                                WindowFrame = m.WindowFrame,
        //                                            });
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    });
        //                }
        //            }
        //            // }
        //        }
        //    }
        //    return menus;
        //}


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Error()
        {
            return View("~/Views/Error/Application.aspx", new ErrorPageViewData());
        }
       
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Logs()
        {
            return View("~/Views/Shared/Logs.aspx", errorRepository.GetSome());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {

            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Updates()
        {
            return View();
        }

        #endregion
    }
}
