﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;

    using Telerik.Web.Mvc;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Filter;
    
    
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Domain;
    using Axxess.Log.Enums;
    using Axxess.LookUp.Domain;

   
   

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly HHBillingService billingService;
        private readonly IAgencyService agencyService;
        private readonly HHEpiosdeService episodeService;
        private readonly HHMediatorService mediatorService;
        private readonly HHPatientProfileService profileService;


        public BillingController(IPatientService patientService, HHBillingService billingService, IAgencyService agencyService, HHEpiosdeService epiosdeService, HHMediatorService mediatorService,HHPatientProfileService profileService)
        {
            this.patientService = patientService;
            this.billingService = billingService;
            this.agencyService = agencyService;
            this.episodeService = epiosdeService;
            this.mediatorService = mediatorService;
            this.profileService = profileService;
        }

        #endregion

        #region Actions

        [OutputCache(NoStore = false, Duration = 600, VaryByParam = "*")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EligibilityReport()
        {
            return PartialView("Medicare/Eligibility/Report");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EligibilityList(Guid? patientId)
        {
            if (patientId.HasValue)
            {
                return View(new GridModel(profileService.GetMedicareEligibilityLists(patientId.Value)));
            }
            return View(new GridModel(new List<MedicareEligibility>()));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicareClaim, PermissionActions.ViewOutstandingClaim)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RAPCenter()
        {
            return PartialView("Medicare/Claims/CreateClaims", billingService.GetMedicareClaimCenterData(ClaimTypeSubCategory.RAP) ?? new Bill { Service = AgencyServices.HomeHealth, ClaimType = ClaimTypeSubCategory.RAP });
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicareClaim, PermissionActions.ViewOutstandingClaim)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FinalCenter()
        {
            return PartialView("Medicare/Claims/CreateClaims", billingService.GetMedicareClaimCenterData(ClaimTypeSubCategory.Final) ?? new Bill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimsPdf(Guid BranchId, int InsuranceId, string ParentSortType,  int ClaimType)
        {
            if (ParentSortType.IsNullOrEmpty())
            {
                ParentSortType = "branch";
            }
            var location = agencyService.AgencyNameWithLocationAddress(BranchId) ?? new LocationPrintProfile();
            var bill = new List<Bill>();
            if (Enum.IsDefined(typeof(ClaimTypeSubCategory),ClaimType))
            {
                bill = billingService.AllUnProcessedBillList(BranchId, InsuranceId, (ClaimTypeSubCategory)ClaimType, ParentSortType, true);
            }
            return FileGenerator.Pdf<BillingClaimsPdf>(new BillingClaimsPdf(bill ?? new List<Bill>(), location), "Claims");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimsXls(Guid BranchId, int InsuranceId, string ParentSortType, int ClaimType)
        {
            if (ParentSortType.IsNullOrEmpty())
            {
                ParentSortType = "branch";
            }
            var bill = new List<Bill>();
            if (Enum.IsDefined(typeof(ClaimTypeSubCategory), ClaimType))
            {
                bill = billingService.AllUnProcessedBillList(BranchId, InsuranceId, (ClaimTypeSubCategory)ClaimType, ParentSortType, true);
            }
            var export = new ClaimsExport(bill ?? new List<Bill>());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ClaimSummary.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalGrid(Guid BranchId, int InsuranceId)
        {
            return PartialView("Medicare/FinalGrid", billingService.GetClaimsWithInsuranceAndInsuranceInfo(ClaimTypeSubCategory.Final, BranchId, InsuranceId, ParentPermission.MedicareClaim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapGrid(Guid BranchId, int InsuranceId)
        {
            return PartialView("Medicare/RapGrid", billingService.GetClaimsWithInsuranceAndInsuranceInfo(ClaimTypeSubCategory.RAP, BranchId, InsuranceId, ParentPermission.MedicareClaim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Unprocessed()
        {
            return Json(billingService.GetAllUnProcessedBill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RAPClaim(Guid Id, Guid patientId)
        {
            return PartialView("Medicare/Claim/Rap", billingService.GetRap(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult RapPdf(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<BillingRapPdf>(new BillingRapPdf((billingService.GetRapPrint(episodeId, patientId))), "RAP");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaim(Guid Id, Guid patientId)
        {
            if (Id.IsEmpty() || patientId.IsEmpty())
            {
                return PartialView("Medicare/Claim/Final/Verify", new Final());
            }
            return PartialView("Medicare/Claim/Final/Verify", billingService.GetFinalInfoLatest(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult FinalPdf(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<BillingFinalPdf>(new BillingFinalPdf(billingService.GetFinalPrint(episodeId, patientId)), "Final");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult SuppliesPdf(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<BillingSuppliesPdf>(new BillingSuppliesPdf(billingService.GetFinalWithSupplies(episodeId, patientId)), "FinalSupplies");
        }
     
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Info(Guid Id, Guid PatientId)
        {
            return PartialView("Medicare/Claim/Final/Info", billingService.GetFinalInfoLatest(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Insurance(Guid Id, Guid PatientId)
        {
            return PartialView("Medicare/Claim/Final/Insurance", billingService.GetFinalWithInsurance(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Visit(Guid Id, Guid PatientId)
        {
            return PartialView("Medicare/Claim/Final/Visit", billingService.GetFinalVisit(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supply(Guid Id, Guid PatientId)
        {
            return PartialView("Medicare/Claim/Final/Supply", billingService.GetFinalSupply(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Summary(Guid Id, Guid PatientId)
        {
            return PartialView("Medicare/Claim/Final/Summary", billingService.GetFinalSummary(PatientId, Id));
        }
     
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoVerify(Final claim, DateTime FirstBillableVisitDateFormatInput)
        {
            var viewData = new BillingJsonViewData
            {
                isSuccessful = false,
                errorMessage = "Final Basic Info is not verified.",
                PatientId = claim.PatientId,
                Category = ClaimTypeSubCategory.Final
            };
            if (claim != null)
            {
                claim.FirstBillableVisitDateFormat = FirstBillableVisitDateFormatInput.ToString("MM/dd/yyyy");
                if (claim.IsValid())
                {
                    if (billingService.VerifyInfo(claim))
                    {
                        viewData.Service = AgencyServices.HomeHealth.ToString();
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final Basic Info is successfully verified.";
                        viewData.PatientId = claim.PatientId;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = claim.ValidationMessage;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceVerify(Final claim)
        {
            return Json(billingService.VerifyInsurance(claim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && Visit != null && Visit.Count > 0)
            {
                return Json(billingService.VisitVerify(Id, episodeId, patientId, Visit));
            }
            else
            {
                return Json(new BillingJsonViewData { isSuccessful = false, errorMessage = "Final is not verified.", PatientId = patientId, Category = ClaimTypeSubCategory.Final });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyVerify(Guid Id, Guid EpisodeId, Guid PatientId, bool IsSupplyNotBillable)
        {
            if (!Id.IsEmpty() && !EpisodeId.IsEmpty() && !PatientId.IsEmpty())
            {
                return Json(billingService.VisitSupply(Id, EpisodeId, PatientId, IsSupplyNotBillable));
            }
            else
            {
                return Json(new BillingJsonViewData { PatientId = PatientId, isSuccessful = false, errorMessage = "Final supply is not verified.", Category = ClaimTypeSubCategory.Final });
            }
        }

        #region Final Insurance Charge rates

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult FinalClaimReloadInsurance(Guid Id, Guid PatientId)
        //{
        //    return PartialView("Managed/Claim/Insurance", billingService.UpdateManagedCareForInsuranceReload(Id, PatientId));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaimReloadRates(Guid Id, Guid PatientId)
        {
            return Json(billingService.UpdateFinalRatesForReload(Id, PatientId));
        }

        [GridAction]
        public ActionResult FinalClaimInsuranceRates(Guid Id)
        {
            return View(new GridModel(billingService.FinalClaimInsuranceRates(Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaimNewBillData(Guid Id, int PrimaryInsuranceId)
        {
            return PartialView("BillData/New", billingService.GetClaimNewBillData<FinalClaimInsurance>(Id,(int)PayorTypeMainCategory.NonPrivatePayor, PrimaryInsuranceId, ClaimTypeSubCategory.Final));
        }

        /// <summary>
        /// Id is a claim Id
        /// RateId is the rate id 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="RateId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaimEditBillData(Guid Id, int PrimaryInsuranceId, int RateId)
        {
            return PartialView("BillData/Edit", billingService.GetClaimBillDataForEdit<FinalClaimInsurance>(Id, RateId, (int)PayorTypeMainCategory.NonPrivatePayor, PrimaryInsuranceId, ClaimTypeSubCategory.Final));
        }

        /// <summary>
        /// Id is a claim Id
        /// RateId is the rate id 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="RateId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaimDeleteBillData(Guid Id,  int RateId)
        {
            return Json(billingService.ClaimDeleteBillData<FinalClaimInsurance>(Id, RateId, (int)PayorTypeMainCategory.NonPrivatePayor, ClaimTypeSubCategory.Final));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaimAddBillData(Guid ClaimId,ChargeRate chargeRate)
        {
            return Json(billingService.ClaimAddBillData<FinalClaimInsurance>(chargeRate, ClaimId, ClaimTypeSubCategory.Final));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalClaimUpdateBillData(Guid ClaimId,ChargeRate chargeRate)
        {
            return Json(billingService.ClaimUpdateBillData<FinalClaimInsurance>(chargeRate, ClaimId, (int)PayorTypeMainCategory.NonPrivatePayor, ClaimTypeSubCategory.Final));
        }

        #endregion
      
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RapVerify(Rap claim, FormCollection formCollection)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The RAP could not be verified.", Category = ClaimTypeSubCategory.Final,Service=AgencyServices.HomeHealth.ToString() };
            if (claim != null)
            {
                if (formCollection != null)
                {
                    var keys = formCollection.AllKeys;
                    if (keys != null && keys.Length > 0)
                    {
                        claim.FirstBillableVisitDateFormat = keys.Contains("FirstBillableVisitDateFormatInput") ? formCollection["FirstBillableVisitDateFormatInput"] : string.Empty;
                        if (keys.Contains("Ub04Locator81"))
                        {
                            var locatorList = formCollection["Ub04Locator81"].ToArray();
                            var locators = new List<Locator>();
                            if (locatorList != null && locatorList.Length > 0)
                            {
                                locatorList.ForEach(l =>
                                {
                                    if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                                    {
                                        locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                    }
                                });
                            }
                            claim.Ub04Locator81cca = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator39"))
                        {
                            var locatorList = formCollection["Ub04Locator39"].ToArray();
                            var locators = new List<Locator>();
                            if (locatorList != null && locatorList.Length > 0)
                            {
                                locatorList.ForEach(l =>
                                {
                                    if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2"))
                                    {
                                        locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"] });
                                    }
                                });
                            }
                            claim.Ub04Locator39 = locators.ToXml();
                        }
                    }
                }

                if (claim.IsValid())
                {
                    if (billingService.VerifyRap(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The RAP was verified successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = claim.ValidationMessage;
                }
            }

            return Json(viewData);
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Generate(int ansiId)
        {
            var claimData = billingService.GetClaimData(ansiId);
            string generateJsonClaim = claimData != null ? claimData.Data : string.Empty;
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(generateJsonClaim);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, generateJsonClaim.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=billing{0}.txt", DateTime.Now.ToString("yyyyMMddHH:mm:ss:ff")));
            return new FileStreamResult(fileStream, "Text/Plain");

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MedicareClaimPdf(Guid patientId, Guid Id, string type)
        {
            return FileGenerator.GetClaimPDF(billingService.GetMedicareClaimInvoiceFormViewData(patientId, Id,type));
        }
     
        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult UB04Pdf(Guid patientId, Guid Id, string type)
        //{
        //    return FileGenerator.Pdf<UB04Pdf>(new UB04Pdf(billingService.GetUBOFourInfo(patientId, Id, type)), "UB04");
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult HCFA1500Pdf(Guid patientId, Guid Id, string type)
        //{
        //    return FileGenerator.Pdf<HCFA1500Pdf>(new HCFA1500Pdf(billingService.GetHCFA1500Info(patientId, Id, type)), "HCFA1500");
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalComplete(Guid Id, Guid PatientId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The Final (EOE) could not be completed.", Service = AgencyServices.HomeHealth.ToString(), PatientId = PatientId, Category = ClaimTypeSubCategory.Final };
            if (!Id.IsEmpty())
            {
                if (billingService.FinalComplete(Id, PatientId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Final(EOE) completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult History()
        {
            var viewData = billingService.GetPayorAndBranchFilterWithPermission(ParentPermission.MedicareClaim, PermissionActions.ViewClaimHistory,null);
            viewData.Status = (int)PatientStatus.Active;
            viewData.ViewListPermissions = viewData.AvailableService;
            return PartialView("Medicare/Center/Layout", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingClaims()
        {
            var viewData = billingService.GetPayorAndBranchFilterWithPermission(ParentPermission.MedicareClaim, PermissionActions.ViewPendingClaim, new List<PermissionActions> { PermissionActions.Edit });
            viewData.ViewListPermissions = viewData.AvailableService;
            viewData.Status = (int)PatientStatus.Active;
            return PartialView("Medicare/Pending/Claims", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimRaps(Guid BranchId, string InsuranceId)
        {
            return View(new GridModel(billingService.PendingClaimRaps(BranchId, InsuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePedingRapClaim(Guid Id, Guid branchId, string insuranceId, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateRapClaimStatus(Id, PaymentDate, PaymentAmount, Status);
            return View(new GridModel(billingService.PendingClaimRaps(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapSnapShots(Guid Id)
        {
            return View(new GridModel(billingService.GetRapSnapShotsByClaimId(Id) ?? new List<RapSnapShot>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateRapSnapShotsClaim(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "RAP");
            return View(new GridModel(billingService.GetRapSnapShotsByClaimId(Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePedingFinalClaim(Guid Id, Guid branchId, string insuranceId, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateFinalClaimStatus(Id, PaymentDate, PaymentAmount, Status);
            return View(new GridModel(billingService.PendingClaimFinals(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimFinals(Guid BranchId, string InsuranceId)
        {
            return View(new GridModel(billingService.PendingClaimFinals(BranchId, InsuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalSnapShots(Guid Id)
        {
            return View(new GridModel(billingService.GetFinalSnapShotsByClaimId(Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateFinalSnapShotsClaim(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "Final");
            return View(new GridModel(billingService.GetFinalSnapShotsByClaimId(Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SnapShotClaims(Guid Id, string Type)
        {
            return View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSnapShotClaim(Guid Id, long BatchId, string Type, DateTime? PaymentDate, double PaymentAmountValue, int Status)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The batch claim could not be updated." };
            DateTime paymentDate = DateTime.MinValue;
            if (PaymentDate != null)
            {
                paymentDate = PaymentDate.Value;
            }
            if (billingService.UpdateSnapShot(Id, BatchId, PaymentAmountValue, paymentDate, Status, Type))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The batch claim has been updated successfully.";
            }
            return Json(viewData);// View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryActivity(Guid PatientId, int InsuranceId)
        {
            return View(new GridModel(billingService.Activity(PatientId, InsuranceId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateRAPClaim(Guid patientId, Guid id)
        {
            return PartialView("Medicare/Claim/Update", billingService.GetClaimViewData(patientId, id, ClaimTypeSubCategory.RAP));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateFinalClaim(Guid patientId, Guid id)
        {
            return PartialView("Medicare/Claim/Update", billingService.GetClaimViewData(patientId, id, ClaimTypeSubCategory.Final));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveUpdate(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                // var primaryInsuranceId = keys.Contains("PrimaryInsuranceId") && formCollection["PrimaryInsuranceId"].IsNotNullOrEmpty() && formCollection["PrimaryInsuranceId"].IsInteger() ? formCollection["PrimaryInsuranceId"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;

                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() && !formCollection["PaymentAmount"].IsDouble(), "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() && !formCollection["PaymentDateValue"].IsValidDate(), "Payment date is not a right format."));

                if (!Id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                        var paymentDate = formCollection["PaymentDateValue"].IsDate() ? formCollection["PaymentDateValue"].ToDateTime() : DateTime.MinValue;
                        viewData = billingService.UpdateProccesedClaimStatus(patientId, Id, type, paymentDate, paymentAmount, paymentDate, claimStatus, comment);
                        if (viewData.isSuccessful)
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }

                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapSnapShotClaimInfo(Guid PatientId, Guid ClaimId)
        {
            return PartialView("Medicare/Center/Info", billingService.GetClaimSnapShotInfo(PatientId, ClaimId, ClaimTypeSubCategory.RAP));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalSnapShotClaimInfo(Guid PatientId, Guid ClaimId)
        {
            return PartialView("Medicare/Center/Info", billingService.GetClaimSnapShotInfo(PatientId, ClaimId, ClaimTypeSubCategory.Final));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateRAPClaim(Guid PatientId, Guid EpisodeId, int InsuranceId)
        {
            return Json(billingService.CreateClaim(PatientId, EpisodeId, InsuranceId, ClaimTypeSubCategory.RAP));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateFinalClaim(Guid PatientId, Guid EpisodeId, int InsuranceId)
        {
            return Json(billingService.CreateClaim(PatientId, EpisodeId, InsuranceId, ClaimTypeSubCategory.Final));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewRAPClaim(Guid patientId)
        {
            return PartialView("Medicare/Claim/New", billingService.GetEpisodeNeedsClaimViewData(patientId, (int)ClaimTypeSubCategory.RAP));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewFinalClaim(Guid patientId)
        {
            return PartialView("Medicare/Claim/New", billingService.GetEpisodeNeedsClaimViewData(patientId,(int)ClaimTypeSubCategory.Final));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteRAPClaim(Guid patientId, Guid id)
        {
            return Json(billingService.RemoveMedicareClaimAfterValidate(patientId, id,ClaimTypeSubCategory.RAP));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteFinalClaim(Guid patientId, Guid id)
        {
            return Json(billingService.RemoveMedicareClaimAfterValidate(patientId, id, ClaimTypeSubCategory.Final));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult UpdatePending(FormCollection formCollection)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim update was not successful." };
        //    var rules = new List<Validation>();
        //    var keys = formCollection.AllKeys;
        //    if (keys != null && keys.Length > 0)
        //    {
        //        var rapIds = keys.Contains("RapId") && formCollection["RapId"].IsNotNullOrEmpty() ? formCollection["RapId"].ToArray() : null;
        //        var finalIds = keys.Contains("FinalId") && formCollection["FinalId"].IsNotNullOrEmpty() ? formCollection["FinalId"].ToArray() : null;
        //        if ((rapIds != null && rapIds.Length > 0) || (finalIds != null && finalIds.Length > 0))
        //        {
        //            if (rapIds != null && rapIds.Length > 0)
        //            {
        //                foreach (var id in rapIds)
        //                {
        //                    rules.Add(new Validation(() => keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
        //                    rules.Add(new Validation(() => keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
        //                }
        //            }
        //            if (finalIds != null && finalIds.Length > 0)
        //            {
        //                foreach (var id in finalIds)
        //                {
        //                    rules.Add(new Validation(() => keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
        //                    rules.Add(new Validation(() => keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
        //                }
        //            }
        //            var entityValidator = new EntityValidator(rules.ToArray());
        //            entityValidator.Validate();
        //            if (entityValidator.IsValid)
        //            {
        //                if (billingService.UpdatePendingClaimStatus(formCollection, rapIds, finalIds))
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "The claim updated successfully.";
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = "The claim update was not successful.";
        //                }
        //            }
        //            else
        //            {
        //                viewData.isSuccessful = false;
        //                viewData.errorMessage = entityValidator.Message;
        //            }
        //        }
        //    }
        //    return Json(viewData);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimLogs(string type, Guid claimId, Guid patientId)
        {
            if (type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() || type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() || type.ToUpperCase() == LogType.ManagedClaim.ToString().ToUpperCase())
            {
                return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Patient, type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() ? LogType.Rap : (type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() ? LogType.Final : LogType.ManagedClaim), patientId, claimId.ToString()));
            }
            return PartialView("ActivityLogs", new List<AppAudit>());
        }

        [OutputCache(NoStore = false, Duration = 600, VaryByParam = "*")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedList()
        {
            return PartialView();
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSubmittedList(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            return View(new GridModel(billingService.GetClaimDatas(StartDate, EndDate, ClaimType)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimDetail(int id)
        {
            return View(new GridModel(billingService.GetSubmittedBatchClaims(id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimResponse(int Id)
        {
            var claimData = billingService.GetClaimData(Id);
            return PartialView("Medicare/ClaimResponse", claimData != null ? claimData.Response.Replace("\r\n", "<br />") : string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationContent(Guid AuthorizationId)
        {
            return PartialView(profileService.GetAuthorization(AuthorizationId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceInfoContent(Guid PatientId, int InsuranceId, DateTime StartDate, DateTime EndDate, string ClaimTypeIdentifier)
        {
            var viewData = billingService.InsuranceWithAuthorization(PatientId, InsuranceId, StartDate, EndDate);
            if (viewData != null) { viewData.ClaimTypeIdentifier = ClaimTypeIdentifier; }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllClaims()
        {
            var bills = new List<Bill>();
            ViewData["ClaimType"] = ClaimTypeSubCategory.RAP.ToString();
            var filter = billingService.GetPayorAndBranchFilterWithPermission(ParentPermission.MedicareClaim, PermissionActions.ViewOutstandingClaim, new List<PermissionActions> { PermissionActions.ViewOutstandingClaim });
            if (filter != null)
            {
                bills = billingService.AllUnProcessedBillList(filter.Id, filter.SubId, ClaimTypeSubCategory.RAP, "branch", false);
                ViewData["BranchId"] = filter.Id;
                ViewData["InsuranceId"] = filter.SubId;
                ViewData["Service"] = (int)filter.Service;
            }
            else
            {
                ViewData["Service"] = (int)AgencyServices.HomeHealth;
            }
            return PartialView("AllClaim", bills);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllClaimGrid(Guid BranchId, int InsuranceId, int ClaimType)
        {
            ViewData["BillUIIdentifier"] = "All";
            List<Bill> bill = null;
            if (Enum.IsDefined(typeof(ClaimTypeSubCategory),ClaimType))
            {
                bill = billingService.AllUnProcessedBillList(BranchId, InsuranceId,(ClaimTypeSubCategory) ClaimType, "branch",  false);
            }
            if (bill.IsNullOrEmpty())
            {
                return JavaScript("");
            }
            return PartialView("AllClaimContent", bill ?? new List<Bill>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditClaimSnapShot(Guid id, long batchId, string type)
        {
            return PartialView("Medicare/Claim/BatchUpdate", billingService.GetClaimSnapShotForEdit(id, batchId, type));
        }

        #region Managed Claim

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleManagedANSI(Guid Id)
        {
            return Json(billingService.GenerateManagedClaimSingle(Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedHistory()
        {
            var viewData = billingService.GetPayorAndBranchFilterWithPermission(ParentPermission.ManagedCareClaim, PermissionActions.ViewClaimHistory, null);
            viewData.Status = (int)PatientStatus.Active;
            viewData.ViewListPermissions = viewData.AvailableService;
            return PartialView("Managed/Center/Layout", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsActivity(Guid PatientId, int InsuranceId)
        {
            return View(PatientId.IsEmpty() ? new GridModel(new List<ManagedClaimLean>()) : new GridModel(billingService.GetManagedClaimsActivity(PatientId, InsuranceId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedCreateClaims()
        {
            return PartialView("Managed/Claims/CreateClaims", billingService.GetManagedCreateClaims() ?? new Bill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedGrid(Guid branchId, int insuranceId)
        {
            return PartialView("Managed/ManagedGrid", billingService.GetClaimsWithInsuranceAndInsuranceInfo(ClaimTypeSubCategory.ManagedCare, branchId, insuranceId, ParentPermission.ManagedCareClaim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaim(Guid patientId)
        {
            return PartialView("Managed/Claim/New", billingService.GetNewMangedClaimViewData(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateManagedClaim(Guid PatientId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            return Json(billingService.AddManagedClaim(PatientId, StartDate, EndDate, InsuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaim(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Verify", billingService.GetManagedClaimForVerify(Id, patientId, AgencyServices.HomeHealth));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimInfo(Guid Id, Guid PatientId)
        {
            return PartialView("Managed/Claim/Info", billingService.GetManagedClaimInfoLatest(PatientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimInsurance(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Claim/Insurance", billingService.ManagedClaimWithInsurance(Id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimVisit(Guid Id, Guid PatientId)
        {
            return PartialView("Managed/Claim/Visit", billingService.GetManagedClaimVisit(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSupply(Guid Id, Guid PatientId)
        {
            return PartialView("Managed/Claim/Supply", billingService.GetManagedClaimSupply(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSummary(Guid Id, Guid PatientId)
        {
            return PartialView("Managed/Claim/Summary", billingService.GetManagedClaimSummary(Id, PatientId));
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInfoVerify(ManagedClaim claim)
        {
            return Json(billingService.ManagedVerifyInfo(claim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInsuranceVerify(ManagedClaim claim)
        {
            return Json(billingService.ManagedVerifyInsurance(claim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> Visit)
        {
            return Json(billingService.ManagedVisitVerify(Id, patientId, Visit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSupplyVerify(Guid Id, Guid PatientId)
        {
            return Json(billingService.ManagedVisitSupply(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedComplete(Guid Id, Guid PatientId, string Total)
        {
            return Json(billingService.ManagedComplete(Id, PatientId, Total.IsNotNullOrEmpty() ? Total.ToCurrency() : 0));
        }

        //Not used in private duty
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEpisodes(Guid Id, Guid PatientId)
        {
            var viewData = new MultipleEpisodeViewData { PatientId = PatientId, EpisodeItems = this.billingService.GetManagedClaimEpisodes(PatientId, Id) };
            return PartialView("Managed/MultipleEpisodes", viewData);
        }

        #region Managed Insurance Charge rates

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimReloadInsurance(Guid Id, Guid PatientId)
        {
            return PartialView("Managed/Claim/Insurance", billingService.UpdateManagedCareForInsuranceReload(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimReloadRates(Guid Id, Guid PatientId)
        {
            return Json( billingService.UpdateManagedCareRatesForReload(Id, PatientId));
        }

        [GridAction]
        public ActionResult ManagedClaimInsuranceRates(Guid Id)
        {
            return View(new GridModel(billingService.ManagedClaimInsuranceRates(Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimNewBillData(Guid Id, int PrimaryInsuranceId)
        {
            return PartialView("BillData/New", billingService.GetClaimNewBillData<ManagedClaimInsurance>(Id, (int)PayorTypeMainCategory.NonPrivatePayor, PrimaryInsuranceId, ClaimTypeSubCategory.ManagedCare));
        }

        /// <summary>
        /// Id is a claim Id
        /// RateId is the rate id 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="RateId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEditBillData(Guid Id, int PrimaryInsuranceId, int RateId)
        {
            return PartialView("BillData/Edit", billingService.GetClaimBillDataForEdit<ManagedClaimInsurance>(Id, RateId, (int)PayorTypeMainCategory.NonPrivatePayor, PrimaryInsuranceId, ClaimTypeSubCategory.ManagedCare));
        }

        /// <summary>
        /// Id is a claim Id
        /// RateId is the rate id 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="RateId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimDeleteBillData(Guid Id, int RateId)
        {
            return Json(billingService.ClaimDeleteBillData<ManagedClaimInsurance>(Id, RateId, (int)PayorTypeMainCategory.NonPrivatePayor, ClaimTypeSubCategory.ManagedCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAddBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            return Json(billingService.ClaimAddBillData<ManagedClaimInsurance>(chargeRate, ClaimId,ClaimTypeSubCategory.ManagedCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimUpdateBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            return Json(billingService.ClaimUpdateBillData<ManagedClaimInsurance>(chargeRate, ClaimId, (int)PayorTypeMainCategory.NonPrivatePayor, ClaimTypeSubCategory.ManagedCare));
        }

        #endregion

        //Not used in private duty
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ManagedClaimAssessmentData(Guid episode, Guid patientId)
        {
            var managedClaimEpisodeData = new ManagedClaimEpisodeData(){ isSuccessful =  false, errorMessage = "A problem occured while getting the associated episode information."};
            if (!episode.IsEmpty() && !patientId.IsEmpty())
            {
                managedClaimEpisodeData = mediatorService.GetEpisodeAssessmentData(episode, patientId);
            }
            return Json(managedClaimEpisodeData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult ManagedClaimsToGenerate(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        //{
        //    return PartialView("Managed/Claims/ClaimsToGenerate", billingService.ManagedClaimToGenerate(ManagedClaimSelected, BranchId, PrimaryInsurance));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ManagedClaimPdf(Guid patientId, Guid Id)
        {
            return FileGenerator.GetClaimPDF(billingService.GetManagedClaimInvoiceFormViewData(patientId, Id));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult ManagedUB04Pdf(Guid patientId, Guid Id)
        //{
        //    var viewData = billingService.GetInvoiceFormViewData(patientId, Id);
        //    return FileGenerator.Pdf<ManagedUB04Pdf>(new ManagedUB04Pdf(viewData), viewData.InvoiceType.GetDescription());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult ManagedHCFA1500Pdf(Guid patientId, Guid Id)
        //{
        //    var viewData = billingService.GetInvoiceFormViewData(patientId, Id);
        //    return FileGenerator.Pdf<HCFA1500Pdf>(new HCFA1500Pdf(viewData), viewData.InvoiceType.GetDescription());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult InvoicePdf(Guid patientId, Guid Id, bool isForPatient)
        //{
        //    var viewData = billingService.GetInvoiceFormViewData(patientId, Id);
        //    return FileGenerator.Pdf(new InvoicePdf(viewData), viewData.InvoiceType.GetDescription());
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaim(Guid patientId, Guid id)
        {
            return Json(billingService.DeleteManagedClaim(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaim(Guid patientId, Guid id)
        {
            return PartialView("Managed/Claim/Update", billingService.GetManagedClaim(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimStatus(Guid Id, Guid PatientId, int Status, string Comment, string ClaimDateValue)
        {
            return Json(billingService.UpdateProccesedManagedClaimStatus(PatientId, Id, ClaimDateValue, Status, Comment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSnapShotClaimInfo(Guid PatientId, Guid ClaimId)
        {
            return PartialView("Managed/Center/Info", billingService.GetManagedClaimSnapShotInfo(PatientId, ClaimId));
        }

        #region Managed Claim Payments

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPayments(Guid claimId, Guid patientId)
        {
            var allPermission = Current.CategoryService(AgencyServices.HomeHealth, ParentPermission.ManagedCareClaim, new int[]{ (int)PermissionActions.Edit });
            return PartialView("Managed/Payment/List", new GridViewData() { Id = claimId, Service = AgencyServices.HomeHealth, EditPermissions = allPermission.GetOrDefault((int)PermissionActions.Edit) });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllManagedClaimPaymentsGrid(Guid patientId)
        {
            return View(new GridModel(billingService.GetManagedClaimPaymentsByPatient(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaimPayment(Guid Id)
        {
            return PartialView("Managed/Payment/New", billingService.GetManagedClaim(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddManagedClaimPayment(ManagedClaimPayment payment)
        {
            return Json(billingService.AddManagedClaimPayment(payment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimPayment(Guid id)
        {
            return PartialView("Managed/Payment/Edit", billingService.GetManagedClaimPayment(id));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPaymentsGrid(Guid claimId)
        {
            return View(new GridModel(billingService.GetManagedClaimPaymentsWithInsurance(claimId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaimPayment(Guid patientId, Guid claimId, Guid id)
        {
            return Json(billingService.DeleteManagedClaimPayment(patientId, claimId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimPaymentDetails(ManagedClaimPayment claimPayment)
        {
            return Json(billingService.UpdateManagedClaimPayment(claimPayment));
        }

        #endregion

        #region Managed Claim Adjustments

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAdjustments(Guid claimId, Guid patientId)
        {
            var allPermission = Current.CategoryService(AgencyServices.HomeHealth, ParentPermission.ManagedCareClaim, new int[] { (int)PermissionActions.Edit });
            return PartialView("Managed/Adjustment/List", new GridViewData() { Id = claimId, Service = AgencyServices.HomeHealth, EditPermissions = allPermission.GetOrDefault((int)PermissionActions.Edit) });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAdjustmentsGrid(Guid claimId)
        {
            return View(new GridModel(billingService.GetManagedClaimAdjustments(claimId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaimAdjustment(Guid Id)
        {
            return PartialView("Managed/Adjustment/New", billingService.GetManagedClaim(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddManagedClaimAdjustment(ManagedClaimAdjustment managedClaimAdjustment)
        {
            return Json(billingService.AddManagedClaimAdjustment(managedClaimAdjustment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimAdjustment(Guid Id)
        {
            return PartialView("Managed/Adjustment/Edit", billingService.GetManagedClaimAdjustment(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimAdjustmentDetails(ManagedClaimAdjustment managedClaimAdjustment)
        {
            return Json(billingService.UpdateManagedClaimAdjustment(managedClaimAdjustment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaimAdjustment(Guid patientId, Guid claimId, Guid id)
        {
            return Json(billingService.DeleteManagedClaimAdjustment(patientId, claimId, id));
        }

        #endregion

        #endregion

        #region ClaimSupplies

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillable(Guid Id, Guid PatientId, string Type)
        {
            return View(new GridModel(billingService.GetSuppliesByFlag(Id, PatientId, Type, true)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyUnBillable(Guid Id, Guid PatientId, string Type)
        {
            return View(new GridModel(billingService.GetSuppliesByFlag(Id, PatientId, Type, false)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeSupplyBillStatus(Guid Id, Guid PatientId, List<int> BillingId, bool IsBillable, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply bill status change is unsuccessful." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (BillingId != null && BillingId.Count > 0)
                {
                    if (ClaimTypeSubCategory.Final.ToString().Equals(Type))
                    {
                        if (billingService.ChangeStatusOfSuppliesFinal(PatientId, Id, BillingId, IsBillable))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = string.Format("Supplies were successfuly {0}.",IsBillable ? "Marked As Billable" : "Marked As Non-Billable");
                        }
                    }
                    else if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                    {
                        if (billingService.ChangeStatusOfSuppliesManagedClaim(PatientId, Id, BillingId, IsBillable))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = string.Format("Supplies were successfuly {0}.", IsBillable ? "Marked As Billable" : "Marked As Non-Billable");
                        }
                    }
                    else if (ClaimTypeSubCategory.Secondary.ToString().Equals(Type))
                    {
                        if (billingService.ChangeStatusOfSuppliesSecondary(PatientId, Id, BillingId, IsBillable))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = string.Format("Supplies were successfuly {0}.", IsBillable ? "Marked As Billable" : "Marked As Non-Billable");
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupplyBillable(Guid ClaimId, Guid PatientId, int Id, string Type)
        {
            var viewData = new ClaimEditSupplyViewData(ClaimId, PatientId, Type, new Supply(), AgencyServices.HomeHealth);
            if (ClaimTypeSubCategory.Final.ToString().Equals(Type))
            {
               viewData.Supply= billingService.GetSupplyFinal(ClaimId, Id);
            }
            else if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
            {
                viewData = billingService.GetSupplyManagedClaim(PatientId, ClaimId,Id);
            }
            else if (ClaimTypeSubCategory.Secondary.ToString().Equals(Type))
            {
                viewData.Supply = billingService.GetSupplySecondary(ClaimId, Id);
            }
            return View("Supply/Edit",viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillableUpdate(Guid ClaimId, Guid patientId, Supply supply, string Type)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to be updated." };
            var suppliesEdited = new List<Supply>();
            if (!ClaimId.IsEmpty() && !patientId.IsEmpty())
            {
                if (ClaimTypeSubCategory.Final.ToString().Equals(Type))
                {
                    if (billingService.EditSupplyFinal(ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully updated.";
                    }
                }
                else if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                {
                    if (billingService.EditSupplyManagedClaim(patientId, ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully updated.";
                    }
                }
                else if (ClaimTypeSubCategory.Secondary.ToString().Equals(Type))
                {

                    if (billingService.EditSupplySecondary(patientId, ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully updated.";
                    }
                }
            }
            return Json(viewData);
        }
        //Id is a claim Id
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSupplyBillable(Guid Id, Guid PatientId, string Type)
        {
            return View("Supply/New", new ClaimNewSupplyViewData(Id, PatientId, Type,AgencyServices.HomeHealth));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillableAdd(Guid ClaimId, Guid patientId, string Type, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to be added." };
            if (!ClaimId.IsEmpty() && !patientId.IsEmpty())
            {
                if (ClaimTypeSubCategory.Final.ToString().Equals(Type))
                {
                    if (billingService.AddSupplyFinal(ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully added.";
                    }
                }
                else if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                {
                    if (billingService.AddSupplyManagedClaim(patientId, ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully added.";
                    }
                }
                else if (ClaimTypeSubCategory.Secondary.ToString().Equals(Type))
                {
                    if (billingService.AddSupplySecondary(patientId, ClaimId, supply))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The supply was successfully added.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuppliesDelete(Guid Id, Guid PatientId, List<int> BillingId, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supplies were not deleted." };

            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (BillingId != null && BillingId.Count > 0)
                {
                    if (ClaimTypeSubCategory.Final.ToString().Equals(Type))
                    {
                        if (billingService.DeleteSupplyFinal(PatientId, Id, BillingId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supplies were successfuly deleted.";
                        }
                    }
                    else if (ClaimTypeSubCategory.ManagedCare.ToString().Equals(Type))
                    {
                        if (billingService.DeleteSupplyManagedClaim(PatientId, Id, BillingId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supplies were successfuly deleted.";
                        }
                    }
                    else if (ClaimTypeSubCategory.Secondary.ToString().Equals(Type))
                    {
                        if (billingService.DeleteSupplySecondary(PatientId, Id, BillingId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supplies were successfuly deleted.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Remittance

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Remittance()
        {
            return PartialView("Medicare/Remittance/Remittances", billingService.GetRemittanceViewData(DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceContent(DateTime StartDate, DateTime EndDate)
        {
            var remits = billingService.GetRemittanceViewData(StartDate, EndDate);
            if (remits.List.IsNotNullOrEmpty())
            {
                return PartialView("Medicare/Remittance/RemittanceContent", remits);
            }
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult RemittancesPdf(DateTime StartDate, DateTime EndDate)
        {
            return FileGenerator.Pdf<RemittancesPdf>(billingService.GetRemittanceListPdf(StartDate, EndDate), "Remittances");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult RemittancePdf(Guid Id)
        {
            return FileGenerator.Pdf<RemittancePdf>(billingService.GetRemittancePdf(Id), "RemittanceDetail");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetail(Guid Id)
        {
            try
            {
                return PartialView("Medicare/Remittance/RemittanceDetail", billingService.GetRemittanceWithClaims(Id));
            }
            catch (Exception)
            {
                return PartialView("Medicare/Remittance/RemittanceDetail", new Remittance());
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetailContent(Guid Id)
        {
            try
            {
                return PartialView("Medicare/Remittance/RemittanceDetailContent", billingService.GetRemittanceWithClaims(Id));
            }
            catch (Exception)
            {
                return PartialView("Medicare/Remittance/RemittanceDetailContent", new Remittance());
            }
        }

        [OutputCache(NoStore = false, Duration = 600, VaryByParam = "*")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RemittanceUpload()
        {
            return PartialView("Medicare/Remittance/Upload");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RemittanceUpload(HttpPostedFileBase Upload)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "There is no file to upload." };
            if (Upload != null)
            {
                if (Upload.ContentType == "text/plain")
                {
                    if (Upload.FileName.IsNotNullOrEmpty() && Upload.ContentLength > 0)
                    {
                        if (billingService.AddRemittanceUpload(Upload))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The remittance file has been uploaded.";
                        }
                        else
                        {
                            viewData.errorMessage = "The remittance file failed to upload. Please try again.";
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "The remittance file is empty.";
                    }
                }
                else
                {
                    viewData.errorMessage = "Invalid file type. It must be a text file.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteRemittance(Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance could not be deleted. Please try again." };
            if (billingService.DeleteRemittance(Id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The remittance has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PostRemittance(Guid Id, List<string> Episodes)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "A problem occured while posting the remittance." };
            if (!Id.IsEmpty())
            {
                if (Episodes != null && Episodes.Count > 0)
                {
                    if (billingService.PostRemittance(Id, Episodes))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The remittance posted successfully.";
                    }
                }
                else
                {
                    viewData.errorMessage = "At least one must be selected.";
                }
            }
            else
            {
                viewData.errorMessage = "The remittance Id could not be found. Close the Remittance Detail window and try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRemittance(Guid Id, string Type)
        {
            return PartialView("Medicare/Remittance/ClaimRemittance", billingService.GetClaimRemittance(Id, Type));
        }

        #endregion

        #region Secondary Claims

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimCenter(Guid patientId, Guid primaryClaimId)
        {
            var viewData = new SecondaryClaimViewData() { PatientId = patientId, PrimaryClaimId = primaryClaimId };
            if (!patientId.IsEmpty() && !primaryClaimId.IsEmpty())
            {
                viewData.PrimaryClaimData = billingService.GetClaimSnapShotInfo(patientId, primaryClaimId, ClaimTypeSubCategory.Final) ?? new ClaimInfoSnapShotViewData();
            }
            else
            {
                viewData.PrimaryClaimData = new ClaimInfoSnapShotViewData();
            }
            return View("Secondary/Center/Index", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaims(Guid PatientId, Guid PrimaryClaimId)
        {
            return View(new GridModel(billingService.GetSecondaryClaimLeansOfPrimaryClaim(PatientId, PrimaryClaimId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondarySnapShotClaimInfo(Guid patientId, Guid claimId)
        {
            return View("Secondary/Center/ClaimInfo", billingService.GetSecondaryClaimSnapShotInfo(patientId, claimId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateSecondaryClaim(Guid patientId, Guid primaryClaimId, DateTime startDate, DateTime endDate)
        {
            return Json(billingService.AddSecondaryClaim(patientId, primaryClaimId, startDate, endDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSecondaryClaim(Guid episodeId, Guid patientId, Guid primaryClaimId)
        {
            var newClaimData = new SecondaryClaimViewData();
            newClaimData.PatientId = patientId;
            newClaimData.PrimaryClaimId = primaryClaimId;
            var episodeRange = episodeService.GetEpisodeDateRange(patientId, episodeId);
            if (episodeRange != null)
            {
                newClaimData.EpisodeStartDate = episodeRange.StartDate;
                newClaimData.EpisodeEndDate = episodeRange.EndDate;
            }
            return PartialView("Secondary/Claim/New", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSecondaryClaim(Guid Id)
        {
            return PartialView("Secondary/Claim/Update", billingService.GetSecondaryClaim(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSecondaryClaimStatus(SecondaryClaim claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Secondary Claim was not updated." };
            var rules = new List<Validation>();
            if (claim != null)
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingService.UpdateSecondaryClaimStatus(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Secondary Claim was updated.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSecondaryClaim(Guid patientId, Guid Id)
        {
            return Json(billingService.DeleteSecondaryClaim(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaim(Guid Id)
        {
            return PartialView("Secondary/Claim/Verify", Id.IsEmpty() ? new SecondaryClaim() : billingService.GetSecondaryClaim(Id));
        }
      
        [GridAction]
        public ActionResult SecondaryClaimInsuranceRates(Guid Id)
        {
            return View(new GridModel(billingService.GetSecondaryClaimInsuranceRates(Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimReloadInsurance(Guid Id, Guid PatientId)
        {
            return PartialView("Secondary/Claim/Insurance", billingService.UpdateSecondaryClaimReloadInsurance(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimNewBillData(Guid Id)
        {
            return PartialView("BillData/New", new NewBillDataViewData() { ClaimId = Id, Service = AgencyServices.HomeHealth, IsMedicareHMO = false, TypeOfClaim = ClaimTypeSubCategory.Secondary, PayorType = (int)PayorTypeMainCategory.NonPrivatePayor });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimEditBillData(Guid Id, int RateId)
        {
            var viewData = new EditBillDataViewData() { ClaimId = Id, TypeOfClaim = ClaimTypeSubCategory.Secondary, Service = AgencyServices.HomeHealth, ChargeRate = new ChargeRate() };
            var rates = billingService.GetSecondaryClaimInsuranceRates(Id);
            if (rates != null && rates.Count > 0)
            {
                var rate = rates.FirstOrDefault(r => r.Id == RateId);
                viewData.ChargeRate = rate;
            }
            return PartialView("BillData/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimDeleteBillData(Guid Id, int RateId)
        {
            return Json(billingService.ClaimDeleteBillData<SecondaryClaimInsurance>(Id, RateId, (int)PayorTypeMainCategory.NonPrivatePayor, ClaimTypeSubCategory.Secondary));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimAddBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            return Json(billingService.ClaimAddBillData<SecondaryClaimInsurance>(chargeRate, ClaimId, ClaimTypeSubCategory.Secondary));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimUpdateBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            return Json(billingService.ClaimUpdateBillData<SecondaryClaimInsurance>(chargeRate, ClaimId,(int)PayorTypeMainCategory.NonPrivatePayor, ClaimTypeSubCategory.Secondary));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimInfo(Guid Id)
        {
            return PartialView("Secondary/Claim/Info", billingService.GetSecondaryClaim(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimInsurance(Guid Id, Guid PatientId)
        {
            return PartialView("Secondary/Claim/Insurance", billingService.SecondaryClaimWithInsurance(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimVisit(Guid Id, Guid patientId)
        {
            return PartialView("Secondary/Claim/Visit", billingService.GetSecondaryClaimVisit(Id, patientId));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimRemittance(Guid Id, Guid PatientId)
        {
            return PartialView("Secondary/Claim/Remittance", billingService.GetSecondaryClaimRemittance(Id, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimSupply(Guid Id, Guid PatientId)
        {
            var viewData = new ClaimSupplyViewData();
            if (!Id.IsEmpty())
            {
                viewData = billingService.GetSecondaryClaimSupply(Id, PatientId);
            }
            else
            {
                viewData.Service = AgencyServices.HomeHealth;
                viewData.Type = ClaimTypeSubCategory.Secondary;
            }
            return PartialView("Secondary/Claim/Supply", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimSummary(Guid Id, Guid patientId)
        {
            return PartialView("Secondary/Claim/Summary", billingService.GetSecondaryClaimSummary(Id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimInfoVerify(SecondaryClaim claim)
        {
            var viewData = new BillingJsonViewData
            {
                isSuccessful = false,
                errorMessage = "Secondary Claim Info was not verified.",
                PatientId = claim.PatientId,
                Service = AgencyServices.HomeHealth.ToString(),
                Category = ClaimTypeSubCategory.Secondary
            };
            if (claim.IsValid())
            {
                viewData = billingService.SecondaryVerifyInfo(claim);
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = claim.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimInsuranceVerify(SecondaryClaim claim)
        {
            return Json(billingService.SecondaryClaimInsuranceVerify(claim));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimVisitVerify(Guid Id, Guid PatientId, List<Guid> Visit)
        {
            return Json(billingService.SecondaryVerifyVisit(Id, PatientId, Visit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimRemittanceVerify(Guid Id, Guid PatientId, DateTime RemitDate, string RemitId, Dictionary<Guid, List<ServiceAdjustment>> adjustments)
        {
            return Json(billingService.SecondaryRemittanceVerify(Id, PatientId, RemitDate, RemitId, adjustments));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimSupplyVerify(Guid Id, Guid PatientId)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "Secondary Claim supplies were not verified.", Service = AgencyServices.HomeHealth.ToString(), Category = ClaimTypeSubCategory.Secondary };
            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                viewData = billingService.SecondarySupplyVerify(Id, PatientId);
            }
            return Json(viewData);
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimComplete(Guid id, Guid patientId, string total)
        {
            var viewData = new BillingJsonViewData { isSuccessful = false, errorMessage = "The Secondary Claim could not be completed.", Service = AgencyServices.HomeHealth.ToString(), Category = ClaimTypeSubCategory.Secondary };
            if (!id.IsEmpty())
            {
                if (billingService.SecondaryClaimComplete(id, patientId, total.IsNotNullOrEmpty() ? total.ToCurrency() : 0))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Secondary Claim completed successfully.";
                    viewData.PatientId = patientId;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleSecondaryANSI(Guid Id)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateSecondarySingle(Id, out claimDataOut, out billExchage))
            {
                return Json(new { isSuccessful = true, Id = claimDataOut != null ? claimDataOut.Id : -1 });
            }
            else
            {
                return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage != null ? billExchage.Message : string.Empty });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult SecondaryClaimPdf(Guid patientId, Guid Id)
        {
            return FileGenerator.GetClaimPDF(billingService.GetSecondaryClaimInvoiceFormViewData(patientId, Id));
        }


        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult SecondaryUB04Pdf(Guid patientId, Guid Id, string type)
        //{
        //    return FileGenerator.Pdf<UB04Pdf>(new UB04Pdf(billingService.GetSecondaryUBOFourInfo(patientId, Id)), "UB04");
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult SecondaryHCFA1500Pdf(Guid patientId, Guid Id)
        //{
        //    return FileGenerator.Pdf<HCFA1500Pdf>(new HCFA1500Pdf(billingService.GetHCFA1500InfoForSecondaryClaim(patientId, Id)), "HCFA1500");
        //}

        #endregion

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkSubmitted(List<Guid> ClaimSelected, string ClaimType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not update the status of the claim(s)." };
            if (ClaimType.IsNotNullOrEmpty())
            {
                if (billingService.IsRap(ClaimType))
                {
                    viewData = billingService.MarkRapSubmitted(ClaimSelected);
                }
                else if (billingService.IsFinal(ClaimType))
                {
                    viewData = billingService.MarkFinalSubmitted(ClaimSelected);
                }
                else if (billingService.IsManagedClaim(ClaimType))
                {
                    viewData = billingService.MarkManagedClaimSubmitted(ClaimSelected);
                }
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult SubmitManagedClaims(List<Guid> ManagedClaimSelected, string StatusType)
        //{
        //    return Json();
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSummary(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId, string ClaimType)
        {
            return PartialView("ClaimSummary", billingService.ClaimToGenerate(ClaimSelected, BranchId, InsuranceId, ClaimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateANSI(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId, string ClaimType)
        {
            return Json(billingService.GenerateDownload(ClaimSelected, BranchId, InsuranceId, ClaimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitClaimDirectly(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId, string ClaimType)
        {
            return Json(billingService.GenerateDirectly(ClaimSelected,  BranchId, InsuranceId,ClaimType));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult CreateManagedANSI(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId)
        //{
        //    return Json(billingService.GenerateDownload(ClaimSelected, BranchId, InsuranceId));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult SubmitManagedClaimDirectly(List<Guid> ClaimSelected, Guid BranchId, int InsuranceId)
        //{
        //    return Json(billingService.GenerateDirectly(ClaimSelected, BranchId, InsuranceId));
        //}
    }
}
