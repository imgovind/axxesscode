﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using iTextExtension;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PayrollController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IPayrollService payrollService;
        private readonly IAgencyService agencyService;

        public PayrollController( IAgencyService agencyService, IPayrollService payrollService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(payrollService, "payrollService");
            Check.Argument.IsNotNull(agencyService, "agencyService");

            this.userService = userService;
            this.payrollService = payrollService;
            this.agencyService = agencyService;
        }

        #endregion

        #region PayrollController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Search()
        {
            return View(DateTime.Today.AddDays(-15));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return View("Results", payrollService.GetPayrollSummaryViewData(payrollStartDate,payrollEndDate,payrollStatus));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SummaryPdf(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            PayrollSummaryPdf doc = new PayrollSummaryPdf(payrollService.GetSummary(payrollStartDate, payrollEndDate, payrollStatus), agencyService.AgencyNameWithMainLocationAddress(), payrollStartDate, payrollEndDate);
            var Stream = doc.GetStream();
            Stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PayrollSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(Stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SummaryDetailPdf(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus);
            var doc = new PayrollSummaryDetailsPdf(detail, Current.AgencyName, payrollStartDate, payrollEndDate);
            var Stream = doc.GetStream();
            Stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PayrollSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(Stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SummaryDetailsPdf(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var doc = new PayrollSummaryDetailsPdf(payrollService.GetDetails(payrollStartDate, payrollEndDate, payrollStatus), Current.AgencyName, payrollStartDate, payrollEndDate);
            var Stream = doc.GetStream();
            Stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PayrollSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(Stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Detail(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus);
            if (detail != null)
            {
                detail.IsUserCanExport = Current.HasRight(AgencyServices.HomeHealth, ParentPermission.Payroll, PermissionActions.Export);
            }
            return PartialView(detail);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult DetailSummary(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        //{
        //    var detail = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus);
        //    return PartialView(detail);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return PartialView( payrollService.GetPayrollDetailsViewData(payrollStartDate, payrollEndDate, payrollStatus));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayVisit(List<Guid> visitSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The visits selected could not be processed. Please try again." };

            if (payrollService.MarkAsPaid(visitSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The visits have been marked as paid.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnpayVisit(List<Guid> visitSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The visits selected could not be unpaid. Please try again." };

            if (payrollService.MarkAsUnpaid(visitSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The visits have been marked as unpaid.";
            }

            return Json(viewData);
        }
        
        #endregion

    }
}
