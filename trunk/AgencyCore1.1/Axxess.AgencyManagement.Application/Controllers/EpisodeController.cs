﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Workflows;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class EpisodeController : BaseController
    {
        #region Constructor

        private readonly HHEpiosdeService episodeService;

        public EpisodeController(HHEpiosdeService episodeService)
        {
            Check.Argument.IsNotNull(episodeService, "episodeService");
            this.episodeService = episodeService;
        }

        #endregion

        #region Episode

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            return PartialView(episodeService.GetNewEpisodeData(patientId, true));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewPatientEpisode()
        {
            var viewData = new NewEpisodeData();
            viewData.Service = AgencyServices.HomeHealth;
            return PartialView("NewEpisode", viewData);
        }

        public ActionResult NewPatientEpisodeContent(Guid patientId)
        {
            return PartialView("NewEpisodeContent", episodeService.GetNewEpisodeData(patientId, false));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid episodeId, Guid patientId)
        {
            if (patientId.IsEmpty() || episodeId.IsEmpty())
            {
                var episodeViewData = new PatientEpisode();
                var selection = new List<SelectListItem>();
                episodeViewData.AdmissionDates = selection;
                return PartialView(episodeViewData);
            }
            else
            {
                return PartialView(episodeService.GetEpisodeForEdit(episodeId, patientId));
            }
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update([Bind] PatientEpisode patientEpisode)
        {
            return Json(episodeService.UpdateEpisode(patientEpisode));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add(PatientEpisode patientEpisode)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Episode could not be saved", Service = AgencyServices.HomeHealth.ToString(), ServiceId = (int)AgencyServices.HomeHealth };
            if (patientEpisode != null)
            {
                var workflow = new CreateEpisodeWorkflow(patientEpisode);
                if (workflow.IsCommitted)
                {
                    viewData.PatientId = patientEpisode.PatientId;
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Episode was created successfully.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = workflow.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RangeList(Guid patientId)
        {
            return Json(episodeService.EpisodeRangeList(patientId));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Inactive(Guid patientId)
        {
            return PartialView(episodeService.GetNonActiveEpisodeViewData(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InactiveGrid(Guid patientId)
        {
            return PartialView(episodeService.GetNonActiveEpisodeViewData(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid patientId, Guid episodeId)
        {
            return Json(episodeService.ActivateEpisode(patientId, episodeId));
        }


        #endregion

        #region Logs

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Episode, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeLogs(Guid episodeId, Guid patientId)
        {
            return PartialView("Logs", Auditor.GetGeneralLogs(LogDomain.Patient, LogType.Episode, patientId, episodeId.ToString()));
        }

        #endregion
    }
}
