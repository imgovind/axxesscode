﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Application.Domain;

    using Telerik.Web.Mvc;

    using Services;
    using ViewData;
    using Workflows;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.Log.Enums;

    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Helpers;
    using System.Web;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PatientController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IPatientService patientService;
        private readonly IReferralService referralService;
        private readonly IAgencyService agencyService;

        public PatientController(IAgencyService agencyService, IPatientService patientService, IUserService userService, IReferralService referralService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.userService = userService;
            this.patientService = patientService;
            this.referralService = referralService;
            this.agencyService = agencyService;

        }

        #endregion

        #region PatientController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var availableService = Current.AcessibleServices;
            var viewData = new Referral { LastUsedPatientId = patientService.LastPatientId(), IsOnlyOneService = availableService.IsAlone(), CurrentServices = availableService };
            viewData.IsUserCanAddPhysicain = Current.HasRight(availableService, ParentPermission.Physician, PermissionActions.Add);
            return PartialView("New/New", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid referralId)
        {
            return PartialView("New/New", referralService.GetReferralForNewOrExisitngPatient(referralId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add([Bind] Patient patient, [Bind]List<Profile> profile, int Status, NewPatientReferralSource referralSource)
        {
            Check.Argument.IsNotNull(patient, "patient");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be saved." };
            if (Status == (int)PatientStatus.Active || Status == (int)PatientStatus.Pending)
            {
                var workflow = new CreatePatientWorkflow(patient, profile, Status == (int)PatientStatus.Pending, referralSource);
                if (workflow.IsCommitted)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = Status == (int)PatientStatus.Active ? "Patient was created successfully." : "Patient was saved successfully.";
                    viewData.IsCenterRefresh = Status == (int)PatientStatus.Active;
                    viewData.IsPendingPatientListRefresh = Status == (int)PatientStatus.Pending;
                    viewData.IsPatientListRefresh = true;
                    viewData.PatientStatus = Status;
                    viewData.PatientId = Status == (int)PatientStatus.Pending ? Guid.Empty : patient.Id;
                    if (!patient.ReferralId.IsEmpty())
                    {
                        viewData.IsReferralListRefresh = true;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = workflow.Message;
                }
            }
            else
            {
                viewData.errorMessage = "Patient could not be saved. Try again.";
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPatientContent(Guid patientId)
        {
            return PartialView("Edit/Information", patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Charts(Guid patientId, int status, int service)
        {
            var viewData = new PatientChartsViewData();
            var serviceToShow = service != 0 ? service.ToEnum(Current.PreferredService) : Current.PreferredService;
            if (AgencyServices.HomeHealth == serviceToShow)// && Current.AcessibleServices.IsAlone()
            {
                var mediatorService = Container.Resolve<MediatorService<ScheduleEvent, PatientEpisode>>();
                if (mediatorService != null)
                {
                    viewData = mediatorService.GetPatientChartsDataWithSchedules(patientId, status);
                    viewData.ApplicationName = AgencyServices.HomeHealth.ToString();
                }
            }
            else if (AgencyServices.PrivateDuty == serviceToShow)
            {
                var mediatorService = Container.Resolve<MediatorService<PrivateDutyScheduleTask, PrivateDutyCarePeriod>>();
                if (mediatorService != null)
                {
                    viewData = mediatorService.GetPatientChartsDataWithSchedules(patientId, status);
                    viewData.ApplicationName = AgencyServices.PrivateDuty.ToString();
                }
            }
            viewData.Service = serviceToShow;
            return PartialView("Charts/Layout", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(patientService.GetPatientOnly(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteView(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return PartialView("DeleteOrRestore", patientService.GetForDelete(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid Id, List<string> ServiceProvided)
        {
            Check.Argument.IsNotEmpty(Id, "patientId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be deleted. Try Again." };
            var workFlow = new TogglePatientWorkflow(Id, ServiceProvided, true);
            if (workFlow.IsCommitted)
            {
                viewData = workFlow.ViewData;
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Patient has been successfully deleted.";
                    viewData.isSuccessful = true;
                }

            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RestoreView(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return PartialView("DeleteOrRestore", patientService.GetForRestore(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid Id, List<string> ServiceProvided)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Patient could not be restored." };
            if (!Id.IsEmpty())
            {
                var workFlow = new TogglePatientWorkflow(Id, ServiceProvided, false);
                if (workFlow.IsCommitted)
                {
                    viewData = workFlow.ViewData;
                    if (viewData.isSuccessful)
                    {
                        viewData.errorMessage = "The patient has been restored successfully.";
                        viewData.isSuccessful = true;
                    }
                }
            }
            else
            {
                viewData.errorMessage = "The patient identifier provided is not valid. Please try again.";
            }
            return Json(viewData);
        }

        #region Hospitalization Logs

        [AllServicePermissionFilter(ParentPermission.HospitalizationLog, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HospitalizationList()
        {
            var viewData = new SingleServiceGridViewData();
            var actions = new int[] { (int)PermissionActions.Add, (int)PermissionActions.Export, (int)PermissionActions.ViewList };
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.HospitalizationLog, actions);
            var isFrozen = Current.IsAgencyFrozen;
            if (allPermission.IsNotNullOrEmpty())
            {
                actions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == (int)PermissionActions.ViewList)
                    {
                        viewData.AvailableService = permission;
                        viewData.IsUserCanViewList = permission.HasValidValue();
                    }
                    else if (a == (int)PermissionActions.Export)
                    {
                        viewData.IsUserCanExport = permission.HasValidValue();
                    }
                    else if (a == (int)PermissionActions.Add)
                    {
                        viewData.IsUserCanAdd = !isFrozen && permission.HasValidValue();
                    }

                });
            }
            return View(viewData);
        }

        [GridAction]
        public ActionResult HospitalizationGrid()
        {
            return View(new GridModel(patientService.GetHospitalizationLogs(Current.AgencyId) ?? new List<PatientHospitalizationData>()));
        }

        [AllServicePermissionFilter(ParentPermission.HospitalizationLog, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLogs(Guid patientId)
        {
            return View("Hospitalization/List", patientService.GetHospitalizationViewData(patientId, false));
        }

        [AllServicePermissionFilter(ParentPermission.HospitalizationLog, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewHospitalizationLog(Guid patientId)
        {
            return PartialView("Hospitalization/New", patientId);
        }

        [AllServicePermissionFilter(ParentPermission.HospitalizationLog, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertHospitalizationLog(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new hospitalization log could not be added." };
            if (patientService.AddHospitalizationLog(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The new hospitalization log was added successfully.";
            }
            return Json(viewData);
        }

        [AllServicePermissionFilter(ParentPermission.HospitalizationLog, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditHospitalizationLog(Guid patientId, Guid hospitalizationLogId)
        {
            return PartialView("Hospitalization/Edit", patientService.GetHospitalizationLogOnly(patientId, hospitalizationLogId));
        }

        [AllServicePermissionFilter(ParentPermission.HospitalizationLog,PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateHospitalizationLog(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Hospitalization Log could not be updated. Please try again." };
            if (patientService.UpdateHospitalizationLog(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Hospitalization Log was updated successfully.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateHospitalizationLogStatus(Guid patientId, Guid hospitalizationLogId, bool isDeprecated)
        {
            return Json(patientService.UpdateHospitalizationLogStatus(patientId, hospitalizationLogId, isDeprecated));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Hospitalizations(Guid patientId)
        {
            return PartialView("Hospitalization/Logs", patientService.GetHospitalizationLogsWithUserName(Current.AgencyId, patientId));
        }

        #endregion

        #region Photo

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewPhoto(Guid patientId)
        {
            return PartialView("Photo", patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddPhoto(Guid patientId, HttpPostedFileBase photo)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The patient's photo could not be saved." };

            if (patientService.IsValidImage(photo))
            {
                if (patientService.AddPhoto(patientId, photo))
                {
                    viewData.PatientId = patientId;
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The patient's photo has been uploaded successfully.";
                }
            }
            else
            {
                viewData.errorMessage = "The photo uploaded is not a valid image.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RemovePhoto(Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The patient's photo could not be removed." };
            if (patientService.UpdatePatientForPhotoRemove(patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The patient's photo has been successfully removed.";
            }

            return Json(viewData);
        }

        #endregion

        #region Patient Documents

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewDocument(Guid patientId)
        {
            return PartialView("Document/New", patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddDocument([Bind] PatientDocument patientDocument, HttpPostedFileBase asset)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient document could not be saved. " };
            if (patientDocument.IsValid())
            {
                patientDocument.AgencyId = Current.AgencyId;
                patientDocument.IsDeprecated = false;
                if (patientService.AddDocument(patientDocument, asset))
                {
                    viewData.PatientId = patientDocument.PatientId;
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Patient document uploaded successfully.";
                }
            }
            else
            {
                viewData.errorMessage = patientDocument.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Documents(Guid patientId)
        {
            return PartialView("Document/List", patientService.GetPatientOnly(patientId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetDocuments(Guid patientId)
        {
            return View(new GridModel(patientService.GetDocuments(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteDocument(Guid patientId, Guid documentId)
        {

            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(documentId, "documentId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Document as not been deleted." };
            if (patientService.DeleteDocument(documentId, patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Document has been deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditDocument(Guid patientId, Guid documentId)
        {
            return PartialView("Document/Edit", patientService.GetPatientDocumentWithName(patientId, documentId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDocument([Bind] PatientDocument patientDocument)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Document did not update successfully." };
            if (patientDocument != null && patientDocument.IsValid())
            {
                if (patientService.EditDocument(patientDocument))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Document updated successfully.";
                }
            }
            return Json(viewData);
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserAccess(Guid patientId)
        {
            return View(patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult GetUserAccess(Guid patientId)
        {
            return View(new GridModel(userService.GetUserAccess(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddUserAccess(Guid patientId, Guid userId)
        {
            return Json(patientService.AddUserAccess(patientId, userId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemoveUserAccess(Guid patientUserId)
        {
            return Json(patientService.RemoveUserAccess(patientUserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult GetPatientAccess(Guid userId)
        {
            return View(new GridModel(patientService.GetPatientAccess(userId)));
        }

        #region Emergency Contacts

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetEmergencyContact(Guid patientId, Guid EmergencyContactId)
        {
            Check.Argument.IsNotEmpty(EmergencyContactId, "EmergencyContactId");
            return Json(patientService.GetEmergencyContact(patientId, EmergencyContactId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewEmergencyContact(Guid PatientId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(patientService.AddNewEmergencyContact(PatientId, emergencyContact));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditEmergencyContact(Guid Id, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            return Json(patientService.EditEmergencyContact(emergencyContact));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContacts(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return PartialView("Edit/EmergencyContacts", PatientId);
        }

        [GridAction]
        public ActionResult GetEmergencyContacts(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(new GridModel { Data = patientService.GetEmergencyContacts(PatientId).Select(s => new PatientEmergencyContactGridRow(s)) });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteEmergencyContact(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeleteEmergencyContact(id, patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewEmergencyContactContent(Guid patientId)
        {
            return PartialView("~/Views/Patient/EmergencyContact/New.ascx", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditEmergencyContactContent(Guid patientId, Guid Id)
        {
            return PartialView("~/Views/Patient/EmergencyContact/Edit.ascx", patientService.GetEmergencyContact(patientId, Id));
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPatientPhysician(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return Json(patientService.AddPatientPhysician(id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePhysicianContact(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientID");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be removed. Try Again." };
            if (patientService.UnlinkPhysician(patientId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been successfully removed from this patient.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "A problem occured while removing the physician from this patient.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Physicians(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            var viewData = new SingleServiceGridViewData { Id = PatientId, AvailableService = Current.AcessibleServices };
            viewData.IsUserCanAdd = Current.HasRight(viewData.AvailableService, ParentPermission.Physician, PermissionActions.Add);
            return PartialView("Edit/Physicians", viewData);
        }

        [GridAction]
        public ActionResult GetPhysicians(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(new GridModel(patientService.GetPatientPhysicians(PatientId).Select(s => new AgencyPhysicianLeanGridRow(s)).ToList()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DateRange(string dateRangeId, Guid patientId)
        {
            return Json(DateServiceFactory<PatientEpisode>.GetDateRange(dateRangeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAdmit(Guid id)
        {
            Check.Argument.IsNotNull(id, "id");
            return PartialView("Admit/Main", patientService.GetPatientForPending(id));
        }

        public ActionResult Pending(Guid Id)
        {
            Check.Argument.IsNotNull(Id, "Id");
            return PartialView("Admit/Info", patientService.GetPatientForPending(Id));
        }

        public ActionResult VerifyPending(Patient patient)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            var workflow = new VerifyAdmitInfo(patient);
            if (workflow.IsCommitted)
            {
                viewData = workflow.ViewData;
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient information has been verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNonAdmit(Guid patientId)
        {
            return PartialView("NonAdmit/New", patientService.GetPatientForPossibleNonAdmit(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNonAdmit(Guid Id, List<string> ServiceProvided, [Bind(Prefix = "profile")] List<NonAdmit> profile)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Non-Admission of patient could not be saved." };
            var workflow = new NonAdmitWorkflow(Id, ServiceProvided, profile);
            if (workflow.IsCommitted)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient non-admission successful.";
                viewData.IsPendingPatientListRefresh = true;
                viewData.IsNonAdmitPatientListRefresh = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = workflow.Message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmitReferral([Bind] Patient patient, [Bind(Prefix = "profile")]List<Profile> profiles)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Referral could not be admitted." };
            if (profiles.IsNotNullOrEmpty())
            {
                var services = profiles.Where(p => Enum.IsDefined(typeof(AgencyServices), p.ServiceType)).Select(p => p.ServiceType).ToList();
                if (services.Count > 0)
                {
                    var workflow = new AdmitNewReferralWorkflow(patient, profiles, services);
                    if (workflow.IsCommitted)
                    {
                        viewData.IsReferralListRefresh = true;
                        viewData.IsPatientListRefresh = true;
                        viewData.IsCenterRefresh = true;
                        viewData = workflow.ViewData;
                        viewData.errorMessage = "The Referral successfully admitted";
                    }
                    else
                    {
                        viewData = workflow.ViewData;
                        viewData.isSuccessful = false;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmitService([Bind] Guid Id, [Bind(Prefix = "profile")]List<Profile> profiles)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Referral could not be admitted." };
            if (profiles.IsNotNullOrEmpty())
            {
                var services = profiles.Where(p => Enum.IsDefined(typeof(AgencyServices), p.ServiceType)).Select(p => p.ServiceType).ToList();
                if (services.Count > 0)
                {
                    var workflow = new AdmitNewServiceReferralWorkflow(Id, profiles, services);
                    if (workflow.IsCommitted)
                    {
                        viewData = workflow.ViewData;
                        viewData.errorMessage = "The Referral has been successfully admitted.";
                    }
                    else
                    {
                        viewData = workflow.ViewData;
                        viewData.isSuccessful = false;
                    }
                }
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid patientId)
        {
            return PartialView("Edit", patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Information(Guid patientId)
        {
            return PartialView("Edit/Information", patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Patient patient)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be saved." };
            var workflow = new EditPatientInfoWorkflow(patient);
            if (workflow.IsCommitted)
            {
                viewData = workflow.ViewData;
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient has been successfully saved.";
            }
            else
            {
                viewData = workflow.ViewData;
            }

            return Json(viewData);
        }

        #region Medications

        [HomeHealthViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.ViewList)]
        public ActionResult MedicationProfileSnapShotHistory(Guid patientId)
        {
            var viewData = new SingleServiceGridViewData();
            viewData.Id = patientId;
            viewData.DisplayName = patientService.GetPatientNameById(patientId);
            var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.MedicationProfile);
            if (permissions.IsNotNullOrEmpty())
            {
                var isForzen = Current.IsAgencyFrozen;
                viewData.IsUserCanEdit = !isForzen && permissions.IsInPermission(Current.AcessibleServices, PermissionActions.Edit);
                viewData.IsUserCanDelete = !isForzen && permissions.IsInPermission(Current.AcessibleServices, PermissionActions.Delete);
                viewData.IsUserCanPrint = permissions.IsInPermission(Current.AcessibleServices,PermissionActions.Print);
            }
            return PartialView("MedicationProfile/SnapShotHistory", viewData);
        }

        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotHistory(Guid patientId)
        {
            return View(new GridModel(patientService.GetMedicationHistoryForPatient(patientId)));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.ViewLog)]
        public ActionResult MedicationLogs(Guid patientId)
        {
            return PartialView("ActivityLogs", Auditor.GetMedicationLogs(LogDomain.Patient, LogType.Patient, patientId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Deactivate)]
        public ActionResult MedicationDischarge()
        {
            return PartialView("MedicationProfile/Discharge");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LastestMedications(Guid patientId)
        {
            return Json(patientService.GetMedicationProfileText(patientId));
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Medication(Guid medId, string medicationCategory, string assessmentType)
        //{
        //    return View(new GridModel(patientService.GetMedications(medId, medicationCategory)));
        //}

        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Medications(Guid medicationProfileId)
        {
            return View("MedicationProfile/Medication/List", patientService.GetMedicationListViewData(medicationProfileId));
        }

        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DrugDrugInteractions(Guid medicationProfileId)
        {
            return View("MedicationProfile/Medication/DrugDrug", patientService.GetMedicationProfile(medicationProfileId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationsForOasis(Guid medicationProfileId, string assessmentType)
        {
            return View("MedicationProfile/ProfileGrid", new OasisMedicationProfileViewData { Id = medicationProfileId, AssessmentType = assessmentType, Profile = patientService.GetMedicationProfileWithPermission(medicationProfileId) });
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult MedicationSnapshot(Guid medId)
        //{
        //    return View(new GridModel(patientService.GetMedications(medId, "Active")));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Add)]
        public JsonResult InsertNewMedication(Guid medicationProfileId, [Bind] Medication medication, string medicationType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new medication could not be added to the medication profile." };
            if (medication != null)
            {
                if (patientService.AddMedication(medicationProfileId, medication, medicationType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The new medication was added to the medication profile successfully.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Edit)]
        public ActionResult UpdateMedicationSnapshotHistory(Guid Id, Guid patientId, DateTime signedDate)
        {
            return View(new GridModel(patientService.UpdateMedicationSnapshotHistoryThenReturn(Id, patientId, signedDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Edit)]
        public JsonResult UpdatePatientMedication([Bind] Medication medication, string medicationType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The medication could not be updated. Please try again." };
            if (medication != null)
            {
                if (patientService.UpdateMedication(medication.ProfileId, medication, medicationType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The medication was updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Edit)]
        public JsonResult UpdateMedicationStatus(Guid medProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The medication could not be updated. Please try again." };
            if (patientService.UpdateMedicationStatus(medProfileId, medicationId, medicationCategory, dischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The medication was updated successfully.";
            }
            return Json(viewData);
        }
       
        // Id (mspecific medication id) was named as patientId 
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Deactivate)]
        public JsonResult UpdateMedicationForDischarge(Guid medId, Guid Id, DateTime dischargeDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your medication could not be discharged." };
            if (patientService.UpdateMedicationForDischarge(medId, Id, dischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your medication has been discharged.";
            }
            return Json(viewData);
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //[AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Delete)]
        //public ActionResult DeleteMedication(Guid medId, Medication medication, string medicationCategory, string assessmentType)
        //{
        //    return View(new GridModel(patientService.DeleteThenReturnMedications(medId, medication, medicationCategory)));
        //}

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Delete)]
        public ActionResult DeleteMedicationSnapshotHistory(Guid Id, Guid patientId)
        {
            return View(new GridModel(patientService.DeleteMedicationSnapshotHistoryThenReturn(Id, patientId)));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult SaveMedicationProfile(MedicationProfile medicationProfile)
        //{
        //    Check.Argument.IsNotNull(medicationProfile, "medicationProfile");
        //    return Json(patientService.SaveMedicationProfile(medicationProfile));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Sign)]
        public ActionResult SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            Check.Argument.IsNotNull(medicationProfileHistory, "medicationProfileHistory");

            var viewData = Validate<JsonViewData>(
                   new Validation(() => string.IsNullOrEmpty(medicationProfileHistory.Signature), "The signature field is empty."),
                   new Validation(() => !ServiceHelper.IsSignatureCorrect(Current.UserId, medicationProfileHistory.Signature), "The signature provided is not correct."),
                   new Validation(() => !medicationProfileHistory.SignedDate.IsValid(), "The signature date is not valid."));
            if (viewData.isSuccessful)
            {
                if (patientService.SignMedicationHistory(medicationProfileHistory))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Medication Profile Snapshot was created successfully.";
                }
                else
                {
                    viewData.isSuccessful = false;
                }
            }
            return Json(viewData);
        }

       
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Add)]
        public ActionResult NewMedication(Guid medProfileId)
        {
            return PartialView("MedicationProfile/Medication/New", medProfileId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Edit)]
        public ActionResult EditMedication(Guid medProfileId, Guid medicationId)
        {
            return PartialView("MedicationProfile/Medication/Edit", patientService.GetMedication(medProfileId, medicationId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Deactivate)]
        public ActionResult DischargeMedication(Guid medProfileId, Guid medicationId)
        {
            return PartialView("MedicationProfile/Medication/Discharge", patientService.GetMedication(medProfileId, medicationId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.MedicationProfile, PermissionActions.Delete)]
        public JsonResult DeletePatientMedication(Guid medProfileId, Guid medicationId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Medication could not be deleted from the Medication Profile." };
            if (patientService.DeleteMedication(medProfileId, medicationId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Medication was deleted successfully.";
            }
            return Json(viewData);
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AccessibleServices(Guid patientId)
        {
            return PartialView("ServiceRow", new ServiceViewData(patientService.GetAccessibleServices(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientLogs(Guid patientId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Patient, LogType.Patient, patientId, patientId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NextStep()
        {
            return PartialView("NextStep");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceInfoContent(string Action, string identifiers)
        {
            var viewData = new PatientInsuranceInfoViewData();
            viewData.ActionType = Action;
            var list = identifiers.Split('|');
            if (list != null && list.Length > 2)
            {
                viewData.InsuranceType = list[0];
                viewData.IdPrefix = list[1];
                viewData.NamePrefix = list[2];
            }
            return PartialView("InsuranceInfoContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Map(Guid id)
        {
            return PartialView("~/Views/Patient/Map.ascx", patientService.GetPatientOnly(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MapIframe(Guid id)
        {
            return View("~/Views/Patient/Map.aspx", patientService.GetPatientOnly(id));
        }

        #endregion
    }
}