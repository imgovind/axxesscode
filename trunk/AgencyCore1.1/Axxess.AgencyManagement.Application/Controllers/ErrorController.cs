﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System.Web.Mvc;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.ViewData;

    [Compress]
    public class ErrorController : Controller
    {
        public ErrorController() { }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get), ValidateInput(false)]
        public ActionResult ApplicationError(string error)
        {
            var viewData = new ErrorPageViewData { message = error };
            return View("Application", viewData);
        }

        public ActionResult FileNotFound(string error)
        {
            var viewData = new ErrorPageViewData { message = error };
            return View(viewData);
        }

        public ActionResult NotAuthorized(string error)
        {
            return View();
        }

        public ActionResult Forbidden(string error)
        {
            return View();
        }

        //public ActionResult NotAuthorizedTest(string error)
        //{
        //    //Response.StatusCode = 403;
        //    //return Json(new JsonViewData { errorMessage="not authorized"});
        //    return JavaScript("alert('not authorized')");
        //}
    }
}
