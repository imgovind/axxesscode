﻿#define DEBUG

namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using Telerik.Web.Mvc;
    using iTextExtension;

    using ViewData;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly IAgencyService agencyService;
        private readonly HHTaskService taskService;
        private readonly HHMediatorService mediatorService;
        private readonly HHEpiosdeService episodeService;
        private readonly HHPatientProfileService patientProfileService;

        public ScheduleController(IPatientService patientService,
            IAgencyService agencyService,
            HHTaskService taskService,
            HHMediatorService mediatorService,
            HHEpiosdeService episodeService,
            HHPatientProfileService patientProfileService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");

            this.taskService = taskService;
            this.mediatorService = mediatorService;
            this.episodeService = episodeService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.patientProfileService = patientProfileService;
        }

        #endregion

        #region ScheduleController Actions

        #region Schedule Center

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult All(Guid BranchId, byte StatusId, byte PaymentSourceId)
        {
            var patientList = patientProfileService.GetPatientSelection(BranchId, StatusId, PaymentSourceId, "", false) ?? new List<PatientSelection>();
            return Json(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllSort(Guid BranchId, byte StatusId, byte PaymentSourceId)
        {
            var patientList = patientProfileService.GetPatientSelection(BranchId, StatusId, PaymentSourceId, "", false) ?? new List<PatientSelection>();
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        //TODO: Needs fix to work based on the service

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Center(Guid patientId, int status)
        {
            var viewData = new ScheduleViewData();
            var selectedViewData = patientProfileService.GetScheduleCenterDataWithOutSchedules(patientId, status);
            if (selectedViewData != null)
            {
                if (!selectedViewData.CurrentPatientId.IsEmpty())
                {
                    viewData.CalendarData = taskService.GetScheduleWithPreviousAfterEpisodeInfo(selectedViewData.CurrentPatientId, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();
                    viewData.CalendarData.IsDischarged = selectedViewData.IsDischarged;
                    viewData.CalendarData.DisplayName = selectedViewData.DisplayName;
                    var permissions = Current.Permissions.GetCategoryPermission(ParentPermission.Episode);
                    if (permissions.IsNotNullOrEmpty())
                    {
                        var isFrozen = Current.IsAgencyFrozen;
                        viewData.CalendarData.IsUserCanAddEpisode = !isFrozen && permissions.IsInPermission(AgencyServices.HomeHealth, PermissionActions.Add);
                        viewData.CalendarData.IsUserCanViewEpisodeList = permissions.IsInPermission(AgencyServices.HomeHealth, PermissionActions.ViewList);
                        viewData.CalendarData.IsUserCanEditEpisode = !isFrozen && permissions.IsInPermission(AgencyServices.HomeHealth, PermissionActions.Edit);
                    }
                   
                }
                viewData.SelectionViewData = selectedViewData;
            }
            // ReSharper disable Mvc.InvalidModelType
            return PartialView("Center/Layout", viewData);
            // ReSharper restore Mvc.InvalidModelType
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid patientId, Guid? episodeId, string discipline)
        {
            return PartialView("Center/Content", episodeId.HasValue ? taskService.GetPatientScheduleDataForCenter(patientId, (Guid)episodeId, discipline) : taskService.GetPatientScheduleDataForCenter(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid patientId, Guid episodeId, string discipline)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            return Json(taskService.GetScheduleWithPreviousAfterEpisodeInfo(patientId, episodeId, discipline, false, false, true, false) ?? new CalendarViewData());
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivitySort(Guid episodeId, Guid patientId, string discipline)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            var viewData = taskService.GetScheduleWithPreviousAfterEpisodeInfo(patientId, episodeId, discipline, false, false, true, false) ?? new CalendarViewData();
            return new LargeJsonResult
            {
                Data = new GridModel<GridTask>
                {
                    Data = viewData.ScheduleEvents
                },
                MaxJsonLength = int.MaxValue
            };
            // return View(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Calendar(Guid patientId, string discipline)
        {
            return PartialView("Center/Calendar", taskService.GetScheduleDataForCalendar(patientId, discipline) ?? new CalendarViewData());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CalendarNav(Guid patientId, Guid episodeId, string discipline)
        {
            return PartialView("Center/Calendar", taskService.GetPatientScheduleDataForCenter(patientId, episodeId, discipline) ?? new CalendarViewData());
        }

        #endregion

#if DEBUG

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateTestNotes(Guid episodeId, Guid patientId)
        {
            return Json(mediatorService.GenerateTestVisits(patientId, episodeId));
        }

#endif

        #region Add Schedule

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(Guid PatientId, Guid EpisodeId, [Bind]List<ScheduleEvent> Tasks)//string schedule)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task(s) could not be saved. Please try again." };
            if (!PatientId.IsEmpty() && !EpisodeId.IsEmpty())
            {
                if (Tasks.IsNotNullOrEmpty())
                {
                    viewData = mediatorService.AddMultipleSchedule(PatientId, EpisodeId, Tasks);
                    viewData.errorMessage = "Task(s) successfully added to the episode";
                }
                else
                {
                    viewData.errorMessage = "Unable to schedule this task. Pick at least one task";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiple(Guid EpisodeId, Guid PatientId, int DisciplineTask, Guid UserId, string StartDate, string EndDate)
        {
            Check.Argument.IsNotNull(UserId, "userId");
            Check.Argument.IsNotNull(EpisodeId, "episodeId");
            Check.Argument.IsNotNull(PatientId, "patientId");
            return Json(mediatorService.AddMultiDateRangeScheduleTask(EpisodeId, PatientId, DisciplineTask, UserId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiDay(Guid episodeId, Guid patientId)
        {
            return PartialView("MultiDayScheduler", episodeService.GetCalendarViewData(episodeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiDaySchedule(Guid EpisodeId, Guid PatientId, Guid UserId, int DisciplineTaskId, string EventDates)
        {
            Check.Argument.IsNotNull(UserId, "userId");
            Check.Argument.IsNotNull(EpisodeId, "episodeId");
            Check.Argument.IsNotNull(PatientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Pick the proper discipline task." };

            if (EventDates.IsNotNullOrEmpty())
            {
                var visitDateArray = EventDates.Split(',').Where(s => s.IsNotNullOrEmpty() && s.IsDate()).Select(s => s.ToDateTime()).ToList();
                if (visitDateArray != null && visitDateArray.Count > 0)
                {
                    viewData = mediatorService.AddMultiDaySchedule(EpisodeId, PatientId, UserId, DisciplineTaskId, visitDateArray);
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Select at least one valid date from the calendar .";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Select at least one valid date from the calendar .";
            }
            return Json(viewData);
        }


        #endregion

        #region Schedule Actions

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Reopen)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "eventId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData(false);

            //TODO: needs recode 
            viewData = mediatorService.Reopen(patientId, id);
            if (viewData.isSuccessful)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Task has been reopened sucessfully.";
            }
            else
            {
                viewData.errorMessage = "Task cannot be reopened.";
            }

            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Restore)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore( Guid patientId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to restore this task. Please try again." };
            if (!patientId.IsEmpty() && !id.IsEmpty())
            {
                viewData = mediatorService.ToggleTaskStatus(patientId, id, false);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Task has been successfully restored.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid patientId, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to delete this task. Please try again." };
            if (!patientId.IsEmpty() && !Id.IsEmpty())
            {
                viewData = mediatorService.ToggleTaskStatus(patientId, Id, true);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Task has been successfully deleted.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteMultiple(Guid patientId, Guid episodeId, List<Guid> eventId)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "The selected task(s) could not be deleted." };
            if (!patientId.IsEmpty())
            {
                viewData=taskService.DeleteSchedules(patientId, eventId);
                if (viewData.isSuccessful)
                {
                    viewData.EpisodeId = episodeId;
                    viewData.errorMessage = "The selected task(s) sucessfully deleted.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSchedules(Guid episodeId, Guid patientId)
        {
            return PartialView("Delete", episodeService.GetReassignViewData(episodeId, patientId, ""));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Restore)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteList(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            return View(new GridModel(taskService.GetScheduledEvents(episodeId, patientId, "all")));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteAsset(Guid patientId, Guid Id, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your asset was not deleted." };
            if (mediatorService.DeleteScheduleTaskAsset(patientId, Id, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your asset was successfully deleted.";
            }
            return Json(viewData);
        }

        #endregion

        #region Re-Assign Schedules

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignModal(Guid id, Guid patientId)
        {
            var viewData = taskService.GetReassignViewData(id, patientId);
            return PartialView("Task/Reassign", viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssign(Guid PatientId, Guid Id, Guid UserId)
        {
            Check.Argument.IsNotEmpty(PatientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "eventId");
            Check.Argument.IsNotEmpty(UserId, "userId");
            var viewData = taskService.Reassign(PatientId, Id, UserId);
            var errorMessage = "This task could not be reassigned to another user.";
            if (viewData.isSuccessful)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "This task has been reassigned sucessfully.";
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReAssignSchedules()
        {
            return PartialView("Reassign", new ReassignViewData { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Type = "All" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignMultipleModal(Guid episodeId, Guid patientId)
        {
            return PartialView("Reassign", episodeService.GetReassignViewData(episodeId, patientId, ""));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssignPatientSchedules(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "Events could not be reassigned.";
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    viewData = taskService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId, StartDate, EndDate);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee has to be different from the previous one.";
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignEpisodeSchedules(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId, DateTime StartDate, DateTime EndDate)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "Events is not reassigned.";
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    viewData = taskService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId, StartDate, EndDate);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee has to be different from the previous one.";
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignAllSchedules(Guid EmployeeOldId, Guid EmployeeId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "Events is not reassigned.";
            if (!EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    viewData = taskService.ReassignSchedules(Guid.Empty, EmployeeOldId, EmployeeId, StartDate, EndDate);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        #endregion

        #region Schedule Deviation

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Deviation()
        {
            var viewData = taskService.GetScheduleDeviationViewData(Guid.Empty, true, DateTime.Now.AddDays(-59), DateTime.Now);
            viewData.SortColumn = "PatientName";
            viewData.SortColumn = "ASC";
            return PartialView("Deviation",viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeviationContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            var viewData = taskService.GetScheduleDeviationViewData(BranchId, false, StartDate, EndDate);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortColumn = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("DeviationContent", viewData);
        }

        #endregion

        #region Schedule Detail

        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditDetails(Guid patientId, Guid Id)
        {
            ViewData["Service"] = AgencyServices.HomeHealth;
            if (patientId.IsEmpty() || Id.IsEmpty())
            {
                return PartialView("Detail/Edit", new ScheduleEvent());
            }
            return PartialView("Detail/Edit", mediatorService.GetScheduledEventForDetail(patientId, Id));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.ViewDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDetails(ScheduleEvent scheduleEvent)
        {
            var viewData = mediatorService.UpdateScheduleEventDetails(scheduleEvent, Request.Files);
            return PartialView("JsonResult", viewData.ToJson());
        }

        #endregion

        #region Master Calendar

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendarMain(Guid patientId, Guid episodeId)
        {
            var data = taskService.GetMasterCalendarViewData(patientId, episodeId, "All", true, true, false, true) ?? new CalendarViewData();
            return PartialView(data);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendar(Guid patientId, Guid episodeId)
        {
            var data = taskService.GetMasterCalendarViewData(patientId, episodeId, "All", true, true, false, true) ?? new CalendarViewData();
            return PartialView(data);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MasterCalendarPdf(Guid patientId, Guid episodeId, bool showMissedVisits)
        {
            var data = taskService.GetMasterCalendarViewData(patientId, episodeId, "All", true, true, false, true) ?? new CalendarViewData();
            return FileGenerator.Pdf<MasterCalendarPdf>(new MasterCalendarPdf(data, showMissedVisits), "MasterCalendar");
        }

        #endregion

        #region Return Reasons
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReturnReason(Guid eventId,  Guid patientId)
        {
            var viewData = new JsonViewData();
            var comments = taskService.GetReturnComments(eventId,  patientId);
            if (comments.IsNotNullOrEmpty())
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = comments;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddReturnReason(Guid eventId, Guid patientId, string comment)
        {
            return Json(taskService.AddReturnComments(patientId, eventId, comment));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditReturnReason(int id, string comment)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to save." };
            if (taskService.EditReturnComments(id, comment))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteReturnReason(int id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to delete." };
            if (taskService.DeleteReturnComments(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment deleted successfully";
            }
            return Json(viewData);
        }

        #endregion

        #region Logs

        public ActionResult ScheduleLogs(Guid eventId, Guid patientId, int task)
        {
            return PartialView("Logs", taskService.GetTaskLogs(patientId, eventId, task));
        }

        #endregion

        public ActionResult Attachments( Guid patientId, Guid eventId)
        {
            if (patientId.IsEmpty() || eventId.IsEmpty())
            {
                return PartialView("Attachments", new AttachmentViewData());
            }
            else
            {
                return PartialView("Attachments", taskService.GetAttachments( patientId, eventId) ?? new AttachmentViewData());
            }
        }

        public ActionResult Pdf(Guid patientId, Guid eventId)
        {
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                return PartialView("Attachments", new AttachmentViewData());
            }
            else
            {
                return PartialView("Attachments", taskService.GetAttachments(patientId, eventId) ?? new AttachmentViewData());
            }
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTaskHistory(Guid patientId)
        {
            var viewData = patientService.GetPatientNameViewData(patientId);
            if (viewData != null)
            {
                viewData.Service = AgencyServices.HomeHealth;
            }
            return PartialView("Task/DeletedTaskHistory", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTaskHistoryList(Guid patientId)
        {
            var tasks = taskService.GetDeletedTasks(patientId);
            return View(new GridModel(tasks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertsPastDueGrid()
        {
            var viewData = taskService.GetPastDueAndUpcomingRecertViewData(ParentPermission.OASIS, PermissionActions.ViewPastDue, new List<PermissionActions>() { PermissionActions.Export }, true);
            return PartialView("~/Views/Agency/Oasis/RecertsPastDue.ascx", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertsUpcomingGrid()
        {
            var viewData = taskService.GetPastDueAndUpcomingRecertViewData(ParentPermission.OASIS, PermissionActions.ViewUpComing, new List<PermissionActions>() { PermissionActions.Export }, true);
            return PartialView("~/Views/Agency/Oasis/RecertsUpcoming.ascx",viewData);
        }

        [GridAction]
        public ActionResult RecertsPastDue(Guid BranchId, int InsuranceId, DateTime StartDate)
        {
            return View(new GridModel(taskService.GetRecertsPastDue(BranchId, InsuranceId, StartDate, DateTime.Now, false, 0)));
        }

        public JsonResult RecertsPastDueWidget()
        {
            return Json(taskService.GetRecertsPastDueWidget());
        }

        [GridAction]
        public ActionResult RecertsUpcoming(Guid BranchId, int InsuranceId)
        {
            return View(new GridModel(taskService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24), false, 0)));
        }

        public JsonResult RecertsUpcomingWidget()
        {
            return Json(taskService.GetRecertsUpcomingWidget());
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserSchedule()
        {
            var viewData = taskService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14),false);
            viewData.GroupName = "VisitDate";
            viewData.SortColumn = "PatientName";
            viewData.SortDirection = "ASC";
            return PartialView("User/List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserScheduleListContent(string groupName, string sortParams)
        {
            var viewData = taskService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14), false);
            if (sortParams.IsNotNullOrEmpty())
            {
                var paramArray = sortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            viewData.GroupName = groupName;
            return PartialView("User/ListContent", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleList()
        {
            var pageNumber = this.HttpContext.Request.Params["page"];
            return View(new GridModel(taskService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14),false).UserEvents));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ScheduleWidget()
        {
            return Json(taskService.GetScheduleWidget(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserCalendar()
        {
            var fromDate = DateUtilities.GetStartOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            var viewData = taskService.GetScheduleLeanViewData(Current.UserId, fromDate, toDate, false);
            if (viewData != null)
            {
                viewData.SortColumn = "PatientName";
                viewData.SortDirection = "ASC";
            }
            return PartialView("~/Views/User/Calendar.ascx", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UserCalendarPdf(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return FileGenerator.Pdf<MonthCalendarPdf>(new MonthCalendarPdf(taskService.GetScheduleLeanViewData(Current.UserId, fromDate, toDate, true)), "MonthlyCalendar");

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserCalendarNavigate(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            var viewData = taskService.GetScheduleLeanViewData(Current.UserId, fromDate, toDate, false);
            if (viewData != null)
            {
                viewData.SortColumn = "PatientName";
                viewData.SortDirection = "ASC";
            }
            return PartialView("~/Views/User/Calendar.ascx", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserVisits(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            var viewData = taskService.GetScheduleLeanViewData(Current.UserId, fromDate, toDate, false);
            return View(new GridModel(viewData.UserEvents));
        }

    }
}