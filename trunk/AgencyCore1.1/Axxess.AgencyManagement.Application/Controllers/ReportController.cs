﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Exports;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Application.Filter;

    // ReSharper disable InconsistentNaming
    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReportController : BaseController
    {
        #region Constructor / Member

        private readonly IAgencyService agencyService;
        private readonly IUserService userService;
        private readonly IPatientService patientService;
        private readonly IPayrollService payrollService;
        private readonly HHReportService reportService;
        private readonly HHTaskService scheduleService;
        private readonly HHNoteService noteService;
        private readonly HHBillingService billingService;

        private readonly HHReportExcelGenerator reportExcelGenerator;

        public ReportController(IAgencyService agencyService, IUserService userService, IPatientService patientService, IPayrollService payrollService, HHTaskService scheduleService, HHReportService reportService, HHBillingService billingService, HHNoteService noteService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.patientService = patientService;
            this.billingService = billingService;
            this.userService = userService;
            this.scheduleService = scheduleService;
            this.payrollService = payrollService;
            this.noteService = noteService;
            this.reportExcelGenerator = Container.Resolve<HHReportExcelGenerator>();
        }

        #endregion

        #region ReportController Actions

        #region General Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var dictionary = reportService.GetAllReport();
            return PartialView("Center", dictionary);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Patient()
        //{
        //    return PartialView("Patient/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Clinical()
        //{
        //    return PartialView("Clinical/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Schedule()
        //{
        //    return PartialView("Schedule/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Billing()
        //{
        //    return PartialView("Billing/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Employee()
        //{
        //    return PartialView("Employee/Home");
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Statistical()
        //{
        //    return PartialView("Statistical/Home");
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Completed()
        {
            return PartialView("Completed");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Error()
        {
            return PartialView("Error");
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult CompletedList(GridCommand command)
        {
            return View(new GridModel(reportService.GetRequestedReports(Current.AgencyId, command.PageSize, command.Page)));
        }

        #endregion

        #region Patient Reports

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientRoster()
        {
            SetExportPermission(ReportPermissions.PatientRoster);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Patient/Roster", reportService.GetPatientRoster(branchId, (int)PatientStatus.Active, 0, false));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientRosterContent(Guid BranchId, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate, string SortParams)
        {
            SetSortParams(SortParams);
            if (StartDate.HasValue && EndDate.HasValue)
            {
                AreDatesValid(StartDate.Value, EndDate.Value);
                return PartialView("Patient/Content/RosterContent", reportService.GetPatientRosterByDateRange(BranchId, StatusId, InsuranceId, StartDate.Value, EndDate.Value, false));
            }
            return PartialView("Patient/Content/RosterContent", reportService.GetPatientRoster(BranchId, StatusId, InsuranceId, false));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth,ReportPermissions.PatientRoster,PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPatientRoster(Guid BranchId, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate, bool? IsRangeApplicable)
        {
            List<PatientRoster> patientRosters;
            if (IsRangeApplicable.HasValue && (bool)IsRangeApplicable && StartDate.HasValue && EndDate.HasValue)
            {
                AreDatesValid(StartDate.Value, EndDate.Value);
                patientRosters = reportService.GetPatientRosterByDateRange(BranchId, StatusId, InsuranceId, StartDate.Value, EndDate.Value, true);
            }
            else
            {
                patientRosters = reportService.GetPatientRoster(BranchId, StatusId, InsuranceId, true);
            }

            return reportExcelGenerator.ExportPatientRoster(patientRosters, BranchId, StatusId, InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CAHPSReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Cahps()
        {
            SetBranchWithService();
            return PartialView("Patient/Cahps");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CAHPSReport, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ExportCahps(Guid branchId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var export = new CahpsExporter(branchId, sampleMonth, sampleYear, paymentSources);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmergencyContactListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Emergencies()
        {
            SetExportPermission(ReportPermissions.EmergencyContactListing);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Patient/EmergencyList", reportService.GetPatientEmergencyContacts(branchId, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmergencyContactListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergenciesContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/EmergencyListContent", reportService.GetPatientEmergencyContacts(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmergencyContactListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmergencies(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientEmergencyList(reportService.GetPatientEmergencyContacts(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Birthdays()
        {
            SetExportPermission(ReportPermissions.PatientBirthdayListing);
            var branchId = SetSortParamsWithServiceAndBranch("Name", "ASC", true);
            return PartialView("Patient/BirthdayList", reportService.GetPatientBirthdays(branchId, DateTime.Now.Month));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BirthdaysContent(Guid BranchId, int Month, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/BirthdayListContent", reportService.GetPatientBirthdays(BranchId, Month));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientBirthdayListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportBirthdays(Guid BranchId, int Month)
        {
            return reportExcelGenerator.ExportPatientBirthdayList(reportService.GetPatientBirthdays(BranchId, Month), Month);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientAddressListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Addresses()
        {
            SetExportPermission(ReportPermissions.PatientAddressListing);
            var branchId = SetSortParamsWithServiceAndBranch("Name", "ASC", true);
            return PartialView("Patient/AddressList", reportService.GetPatientAddressListing(branchId, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientAddressListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddressesContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/AddressListContent", reportService.GetPatientAddressListing(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientAddressListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportAddresses(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientAddressList(reportService.GetPatientAddressListing(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByPhysicianListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianPatients()
        {
            SetExportPermission(ReportPermissions.PatientByPhysicianListing);
            SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Patient/Physician", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByPhysicianListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianPatientsContent(Guid PhysicianId, string SortParams, int StatusId)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/Physician", PhysicianId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByPhysician(PhysicianId, StatusId, AgencyServices.HomeHealth));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByPhysicianListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPhysicianPatients(Guid PhysicianId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientByPhysicians(PhysicianId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByPhysician(PhysicianId, StatusId, AgencyServices.HomeHealth), PhysicianId);
        }

        [GridAction]
        public JsonResult PatientBirthdayWidget()
        {
            return Json(new GridModel(reportService.GetCurrentBirthdays()));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientStartOfCareCertificationPeriodListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SocCertPeriods()
        {
            SetExportPermission(ReportPermissions.PatientStartOfCareCertificationPeriodListing);
            var branchId = SetSortParamsWithServiceAndBranch("PatientLastName", "ASC", true);
            return PartialView("Patient/PatientSocCertPeriodListing", reportService.GetPatientSocCertPeriod(branchId, (int)PatientStatus.Active, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientStartOfCareCertificationPeriodListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SocCertPeriodsContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Patient/Content/PatientSocCertPeriodListingContent", reportService.GetPatientSocCertPeriod(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientStartOfCareCertificationPeriodListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportSocCertPeriods(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportPatientSocCertPeriodListing(reportService.GetPatientSocCertPeriod(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByResponsibleEmployeeListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult EmployeePatients()
        {
            SetExportPermission(ReportPermissions.PatientByResponsibleEmployeeListing);
            SetSortParamsWithServiceAndBranch("PatientLastName", "ASC", true);
            return PartialView("Patient/PatientByResponsibleEmployeeListing", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByResponsibleEmployeeListing, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult EmployeePatientsContent(Guid BranchId, Guid UserId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/PatientByResponsibleEmployeeListingContent", UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableEmployee(BranchId, UserId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByResponsibleEmployeeListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeePatients(Guid BranchId, Guid UserId, int StatusId)
        {
            return reportExcelGenerator.ExportPatientByResponsibleEmployeeListing(UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableEmployee(BranchId, UserId, StatusId), UserId);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByResponsibleCaseManagerListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult CaseManagerPatients()
        {
            SetExportPermission(ReportPermissions.PatientByResponsibleCaseManagerListing);
            SetSortParamsWithServiceAndBranch("PatientLastName", "ASC", true);
            return PartialView("Patient/PatientByResponsibleCaseManager", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByResponsibleCaseManagerListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagerPatientsContent(Guid BranchId, Guid UserId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/PatientByResponsibleCaseManagerContent", UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableByCaseManager(BranchId, UserId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientByResponsibleCaseManagerListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportCaseManagerPatients(Guid BranchId, Guid UserId)
        {
            return reportExcelGenerator.ExportPatientByResponsibleCaseManager(UserId.IsEmpty() ? new List<PatientRoster>() : reportService.GetPatientByResponsiableByCaseManager(BranchId, UserId), UserId);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpiringAuthorizations, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExpiringAuthorizations()
        {
            SetExportPermission(ReportPermissions.ExpiringAuthorizations);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Patient/ExpiringAuthorizations", reportService.GetExpiringAuthorizaton(branchId, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpiringAuthorizations, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringAuthorizationsContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/ExpiringAuthorizationsContent", reportService.GetExpiringAuthorizaton(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpiringAuthorizations, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportExpiringAuthorizations(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportExpiringAuthorizations(reportService.GetExpiringAuthorizaton(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.SurveyCensus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SurveyCensus()
        {
            SetExportPermission(ReportPermissions.SurveyCensus);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Patient/SurveyCensus", reportService.GetPatientSurveyCensus(branchId, (int)PatientStatus.Active, 0, false));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.SurveyCensus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SurveyCensusContent(Guid BranchId, int StatusId, int InsuranceId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/SurveyCensusContent", reportService.GetPatientSurveyCensus(BranchId, StatusId, InsuranceId, false));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.SurveyCensus, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportSurveyCensus(Guid BranchId, int StatusId, int InsuranceId)
        {
            return reportExcelGenerator.ExportPatientSurveyCensus(reportService.GetPatientSurveyCensus(BranchId, StatusId, InsuranceId, true));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientVitalSignReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult VitalSigns()
        {
            SetExportPermission(ReportPermissions.PatientVitalSignReport);
            SetSortParamsWithServiceAndBranch("VisitDate", "ASC", true);
            return PartialView("Patient/VitalSigns", new List<VitalSign>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientVitalSignReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VitalSignsContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Patient/Content/VitalSignsContent", PatientId.IsEmpty() ? new List<VitalSign>() : noteService.GetPatientVitalSigns(PatientId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientVitalSignReport, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ExportVitalSigns(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            var vitalSigns = PatientId.IsEmpty() ? new List<VitalSign>() : noteService.GetPatientVitalSigns(PatientId, StartDate, EndDate);
            var patientName = PatientId.IsEmpty() ? string.Empty : patientService.GetPatientNameById(PatientId);
            var export = new VitalSignExporter(vitalSigns.ToList(), StartDate, EndDate, patientName);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PatientVitalSigns_{0}.xls", DateTime.Now.Ticks));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.SixityDaySummaryByPatient, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SixtyDaySummaries()
        {
            SetExportPermission(ReportPermissions.SixityDaySummaryByPatient);
            SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Patient/SixtyDaySummary", new List<VisitNoteViewData>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.SixityDaySummaryByPatient, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummariesContent(Guid PatientId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Patient/Content/SixtyDaySummaryContent", PatientId.IsEmpty() ? new List<VisitNoteViewData>() : noteService.GetSixtyDaySummary(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.SixityDaySummaryByPatient, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportSixtyDaySummaries(Guid PatientId)
        {
            return reportExcelGenerator.ExportPatientSixtyDaySummary(PatientId.IsEmpty() ? new List<VisitNoteViewData>() : noteService.GetSixtyDaySummary(PatientId), PatientId, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.DischargePatients, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargePatients()
        {
            SetExportPermission(ReportPermissions.DischargePatients);
            var branchId = SetSortParamsWithServiceAndBranch("LastName", "ASC", true);
            return PartialView("Patient/DischargePatients", reportService.GetDischargePatients(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.DischargePatients, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargePatientsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Patient/Content/DischargePatientsContent", reportService.GetDischargePatients(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.DischargePatients, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportDischargePatients(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportDischargePatients(reportService.GetDischargePatients(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        #endregion

        #region Clinical Reports

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.TherapyManagement, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TherapyManagement()
        {
            SetBranchWithService();
            return PartialView("Clinical/TherapyManagement");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.OpenOASIS, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OpenOASIS()
        {
            SetExportPermission(ReportPermissions.OpenOASIS);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Clinical/OpenOasis", reportService.GetAllOpenOasis(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.OpenOASIS, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OpenOASISContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/OpenOasis", reportService.GetAllOpenOasis(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.OpenOASIS, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportOpenOASIS(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalOpenOasis(reportService.GetAllOpenOasis(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult ClinicalOrders()
        //{
        //    return PartialView("Clinical/Orders");
        //}

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult ClinicalOrders([Bind] ReportParameters parameters)
        //{
        //    return View(new GridModel(reportService.GetOrders(parameters.StatusId)));
        //}

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MissedVisitReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisits()
        {
            SetExportPermission(ReportPermissions.MissedVisitReport);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Clinical/MissedVisit", reportService.GetAllMissedVisit(branchId, DateTime.Now.AddDays(-60), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MissedVisitReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/MissedVisit", reportService.GetAllMissedVisit(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MissedVisitReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportMissedVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalMissedVisit(reportService.GetAllMissedVisit(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PhysicianOrderHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderHistory()
        {
            SetExportPermission(ReportPermissions.PhysicianOrderHistory);
            var branchId = SetSortParamsWithServiceAndBranch("OrderNumber", "ASC", true);
            return PartialView("Clinical/OrderHistory", reportService.GetPhysicianOrderHistory(branchId, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PhysicianOrderHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianOrderHistoryContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/OrderHistory", reportService.GetPhysicianOrderHistory(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PhysicianOrderHistory, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPhysicianOrderHistory(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalPhysicianOrderHistory(reportService.GetPhysicianOrderHistory(BranchId, StatusId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PlanofCareHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanOfCareHistory()
        {
            SetExportPermission(ReportPermissions.PlanofCareHistory);
            SetSortParamsWithServiceAndBranch("Number", "ASC", true);
            return PartialView("Clinical/PlanOfCareHistory", reportService.GetPlanOfCareHistory(Guid.Empty, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PlanofCareHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanOfCareHistoryContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/PlanOfCareHistory", reportService.GetPlanOfCareHistory(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PlanofCareHistory, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPlanOfCareHistory(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalPlanOfCareHistory(reportService.GetPlanOfCareHistory(BranchId, StatusId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ThirteenthAnd19thTherapyVisitException, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ThirteenAndNineteenVisitException()
        {
            SetExportPermission(ReportPermissions.ThirteenthAnd19thTherapyVisitException);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Clinical/ThirteenAndNineteenVisitException", scheduleService.GetTherapyException(branchId, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ThirteenthAnd19thTherapyVisitException, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ThirteenAndNineteenVisitExceptionContent(Guid BranchId, Guid PatientId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/ThirteenAndNineteenVisitException", scheduleService.GetTherapyException(BranchId, PatientId, StartDate, EndDate, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ThirteenthAnd19thTherapyVisitException, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportThirteenAndNineteenVisitException(Guid BranchId, Guid PatientId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalThirteenAndNineteenVisitException(scheduleService.GetTherapyException(BranchId, PatientId, StartDate, EndDate, StatusId), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ThirteenthTherapyReevaluationExceptionReports, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ThirteenTherapyReevaluationException()
        {
            SetExportPermission(ReportPermissions.ThirteenthTherapyReevaluationExceptionReports);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Clinical/ThirteenTherapyReevaluationException", scheduleService.GetTherapyReevaluationException(branchId, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now, 13, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ThirteenthTherapyReevaluationExceptionReports, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ThirteenTherapyReevaluationExceptionContent(Guid BranchId, Guid PatientId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/ThirteenTherapyReevaluationException", scheduleService.GetTherapyReevaluationException(BranchId, PatientId, StartDate, EndDate, 13, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ThirteenthTherapyReevaluationExceptionReports, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportThirteenTherapyReevaluationException(Guid BranchId, Guid PatientId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalThirteenTherapyReevaluationException(scheduleService.GetTherapyReevaluationException(BranchId, PatientId, StartDate, EndDate, 13, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.NineteenthTherapyReevaluationExceptionReports, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NineteenTherapyReevaluationException()
        {
            SetExportPermission(ReportPermissions.NineteenthTherapyReevaluationExceptionReports);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Clinical/NineteenTherapyReevaluationException", scheduleService.GetTherapyReevaluationException(branchId, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now, 19, (int)PatientStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.NineteenthTherapyReevaluationExceptionReports, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NineteenTherapyReevaluationExceptionContent(Guid BranchId, Guid PatientId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Clinical/Content/NineteenTherapyReevaluationException", scheduleService.GetTherapyReevaluationException(BranchId, PatientId, StartDate, EndDate, 19, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.NineteenthTherapyReevaluationExceptionReports, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportNineteenTherapyReevaluationException(Guid BranchId, Guid PatientId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClinicalNineteenTherapyReevaluationException(scheduleService.GetTherapyReevaluationException(BranchId, PatientId, StartDate, EndDate, 19, StatusId));
        }

        #endregion

        #region Schedule Reports

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientWeekly()
        {
            SetExportPermission(ReportPermissions.PatientWeeklySchedule);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Schedule/PatientWeeklySchedule", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientWeeklyContent(Guid PatientId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PatientWeeklySchedule", PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7)));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientWeeklySchedule, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPatientWeekly(Guid PatientId)
        {
            return reportExcelGenerator.ExportSchedulePatientWeeklySchedule(PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7)), PatientId, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientMonthlySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientMonthly()
        {
            SetExportPermission(ReportPermissions.PatientMonthlySchedule);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Schedule/PatientMonthlySchedule", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientMonthlySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientMonthlyContent(Guid PatientId, int Month, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            if (PatientId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/PatientMonthlySchedule", new List<TaskLean>());
            }

            return PartialView("Schedule/Content/PatientMonthlySchedule", reportService.GetPatientScheduleEventsByDateRange(PatientId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientMonthlySchedule, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPatientMonthly(Guid PatientId, int Month, int Year)
        {
            return reportExcelGenerator.ExportPatientMonthlySchedule(PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)), PatientId, Month, Year, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeDailyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DailyWork()
        {
            SetExportPermission(ReportPermissions.EmployeeDailyWorkSchedule);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/DailyWork", reportService.GetScheduleEventsByDateRange(branchId, DateTime.Now, DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeDailyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DailyWorkContent(Guid BranchId, DateTime Date, string SortParams)
        {
            IsDateValid(Date);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/DailyWork", reportService.GetScheduleEventsByDateRange(BranchId, Date, Date));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeDailyWorkSchedule, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportDailyWork(DateTime Date, Guid BranchId)
        {
            IsDateValid(Date);
            return reportExcelGenerator.ExportScheduleDailyWork(reportService.GetScheduleEventsByDateRange(BranchId, Date, Date));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeWeekly()
        {
            SetExportPermission(ReportPermissions.EmployeeWeeklySchedule);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/EmployeeWeekly", new List<UserVisit>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeWeeklySchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeWeeklyContent(Guid BranchId, Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/EmployeeWeekly", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeWeeklySchedule, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeWeekly(Guid BranchId, Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleEmployeeWeekly(UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, StartDate, EndDate), BranchId, UserId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeMonthlyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeMonthly()
        {
            SetExportPermission(ReportPermissions.EmployeeMonthlyWorkSchedule);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/MonthlyWork", new List<UserVisit>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeMonthlyWorkSchedule, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeMonthlyContent(Guid BranchId, Guid UserId, int Month, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            if (UserId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/MonthlyWork", new List<UserVisit>());
            }

            return PartialView("Schedule/Content/MonthlyWork", reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeMonthlyWorkSchedule, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeMonthly(Guid BranchId, Guid UserId, int Month, int Year)
        {
            return reportExcelGenerator.ExportScheduleMonthlyWork((UserId.IsEmpty() || Month <= 0) ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchId, DateUtilities.GetStartOfMonth(Month, Year), DateUtilities.GetEndOfMonth(Month, Year)), BranchId, UserId, Month, Year);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueVisits, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueVisits()
        {
            SetExportPermission(ReportPermissions.PastDueVisits);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/PastDueVisits", reportService.GetPastDueScheduleEvents(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueVisits, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PastDueVisits", reportService.GetPastDueScheduleEvents(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueVisits, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPastDueVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportSchedulePastDueVisits(reportService.GetPastDueScheduleEvents(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueVisitsByDiscipline, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueVisitsPerDiscipline()
        {
            SetExportPermission(ReportPermissions.PastDueVisitsByDiscipline);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(branchId, "Nursing", DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueVisitsByDiscipline, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsPerDisciplineContent(Guid BranchId, string Discipline, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(BranchId, Discipline, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueVisitsByDiscipline, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPastDueVisitsPerDiscipline(Guid BranchId, string Discipline, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportSchedulePastDueVisitsByDiscipline(reportService.GetPastDueScheduleEventsByDiscipline(BranchId, Discipline, StartDate, EndDate), Discipline, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CaseManagerTaskList, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagerTasks()
        {
            SetExportPermission(ReportPermissions.CaseManagerTaskList);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CaseManagerTaskList, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagerTasksContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CaseManagerTaskList, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportCaseManagerTasks(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleCaseManagerTask(reportService.GetCaseManagerScheduleByBranch(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ScheduleDeviation, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleDeviations()
        {
            SetExportPermission(ReportPermissions.ScheduleDeviation);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/Deviation", scheduleService.GetScheduleDeviations(branchId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ScheduleDeviation, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleDeviationsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/Deviation", scheduleService.GetScheduleDeviations(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ScheduleDeviation, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ExportScheduleDeviations(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            var scheduleEvents = scheduleService.GetScheduleDeviations(BranchId, StartDate, EndDate);
            var export = new ScheduleDeviationExporter(scheduleEvents.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ScheduleDeviations_{0}.xls", DateTime.Now.Ticks));
        }

        //public ActionResult ExportScheduleDeviation(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var scheduleEvents = reportService.GetScheduleDeviation(BranchId, StartDate, EndDate);
        //    var workbook = new HSSFWorkbook();
        //    var si = PropertySetFactory.CreateSummaryInformation();
        //    si.Subject = "Axxess Data Export - Schedule Deviation";
        //    workbook.SummaryInformation = si;
        //    var sheet = workbook.CreateSheet("ScheduleDeviation");
        //    var titleRow = sheet.CreateRow(0);
        //    titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
        //    titleRow.CreateCell(1).SetCellValue("Schedule Deviation");
        //    titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
        //    var headerRow = sheet.CreateRow(1);
        //    headerRow.CreateCell(0).SetCellValue("Task");
        //    headerRow.CreateCell(1).SetCellValue("Patient Name");
        //    headerRow.CreateCell(2).SetCellValue("MR #");
        //    headerRow.CreateCell(3).SetCellValue("Status");
        //    headerRow.CreateCell(4).SetCellValue("Employee");
        //    headerRow.CreateCell(5).SetCellValue("Schedule Date");
        //    headerRow.CreateCell(6).SetCellValue("Visit Date");
        //    sheet.CreateFreezePane(0, 2, 0, 2);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        int rowNumber = 2;
        //        foreach (var evnt in scheduleEvents)
        //        {
        //            var row = sheet.CreateRow(rowNumber);
        //            row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
        //            row.CreateCell(1).SetCellValue(evnt.PatientName);
        //            row.CreateCell(2).SetCellValue(evnt.PatientIdNumber);
        //            row.CreateCell(3).SetCellValue(evnt.StatusName);
        //            row.CreateCell(4).SetCellValue(evnt.UserName);
        //            row.CreateCell(5).SetCellValue(evnt.EventDate);
        //            row.CreateCell(6).SetCellValue(evnt.VisitDate);
        //            rowNumber++;
        //        }
        //        var totalRow = sheet.CreateRow(rowNumber + 2);
        //        totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviation: {0}", scheduleEvents.Count));
        //    }
        //    sheet.AutoSizeColumn(0);
        //    sheet.AutoSizeColumn(1);
        //    sheet.AutoSizeColumn(2);
        //    sheet.AutoSizeColumn(3);
        //    sheet.AutoSizeColumn(4);
        //    sheet.AutoSizeColumn(5);
        //    sheet.AutoSizeColumn(6);
        //    MemoryStream output = new MemoryStream();
        //    workbook.Write(output);
        //    return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleDeviation_{0}.xls", DateTime.Now.Ticks));
        //}

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueRecertification, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueRecet()
        {
            SetExportPermission(ReportPermissions.PastDueRecertification);
            var branchId = SetSortParamsWithServiceAndBranch("OrderNumber", "ASC", true);
            var payorId = agencyService.AgencyPayor();
            ViewData["Payor"] = payorId;
            return PartialView("Schedule/PastDueRecet", scheduleService.GetRecertsPastDue(branchId, payorId, DateTime.Now.AddDays(-59), DateTime.Now, false, 0));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueRecertification, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueRecetContent(Guid BranchId, int InsuranceId, DateTime StartDate, string SortParams)
        {
            IsDateValid(StartDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/PastDueRecet", scheduleService.GetRecertsPastDue(BranchId, InsuranceId, StartDate, DateTime.Now, false, 0));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PastDueRecertification, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPastDueRecet(Guid BranchId, int InsuranceId, DateTime StartDate)
        {
            IsDateValid(StartDate);
            var export = new PastDueRecertExporter(scheduleService.GetRecertsPastDue(BranchId, InsuranceId, StartDate, DateTime.Now, false, 0), StartDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PastDueRecerts_{0}.xls", DateTime.Now.Ticks));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UpcomingRecertification, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UpcomingRecet()
        {
            SetExportPermission(ReportPermissions.UpcomingRecertification);
            var branchId = SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            var payorId = agencyService.AgencyPayor();
            ViewData["Payor"] = payorId;
            return PartialView("Schedule/UpcomingRecet", scheduleService.GetRecertsUpcoming(branchId, payorId, DateTime.Now, DateTime.Now.AddDays(24), false, 0));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UpcomingRecertification, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpcomingRecetContent(Guid BranchId, int InsuranceId, string SortParams, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/UpcomingRecet", scheduleService.GetRecertsUpcoming(BranchId, InsuranceId, StartDate, EndDate, false, 0));
        }

        //TODO Replace exporter
        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UpcomingRecertification, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportUpcomingRecet(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleUpcomingRecet(scheduleService.GetRecertsUpcoming(BranchId, InsuranceId, StartDate, EndDate, false, 0));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.VisitsByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByStatus()
        {
            SetExportPermission(ReportPermissions.VisitsByStatus);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/VisitsByStatus", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.VisitsByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitsByStatusContent(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int status, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            var isValidStatus = status > 0;
            SetSortParams(SortParams);
            var scheduleEvents = isValidStatus ? scheduleService.GetScheduledEventsByStatus(BranchId, PatientId, ClinicianId, StartDate, EndDate, status) : new List<TaskLean>();
            return PartialView("Schedule/Content/VisitsByStatus", scheduleEvents);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.VisitsByStatus, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportVisitsByStatus(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int status)
        {
            AreDatesValid(StartDate, EndDate);
            var isValidStatus = status > 0;
            var events = isValidStatus ? scheduleService.GetScheduledEventsByStatus(BranchId, PatientId, ClinicianId, StartDate, EndDate, status) : new List<TaskLean>();
            return reportExcelGenerator.ExportScheduleVisitsByStatus(events, BranchId, StartDate, EndDate, status);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.VisitsByType, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByType()
        {
            SetExportPermission(ReportPermissions.VisitsByType);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Schedule/VisitsByType", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.VisitsByType, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitsByTypeContent(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int type, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Schedule/Content/VisitsByType", scheduleService.GetScheduledEventsByType(BranchId, PatientId, ClinicianId, StartDate, EndDate, type));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.VisitsByType, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportVisitsByType(Guid BranchId, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int type)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportScheduleVisitsByType(scheduleService.GetScheduledEventsByType(BranchId, PatientId, ClinicianId, StartDate, EndDate, type), BranchId, StartDate, EndDate, type);
        }

        #endregion

        #region Billing Reports

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.HHRGReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHRG()
        {
            SetBranchWithService();
            return PartialView("Billing/HHRG");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnbilledVisitsforManagedClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledVisitsForManagedClaims()
        {
            SetBranchWithService();
            return PartialView("Billing/UnbilledVisits");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.OutstandingClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OutstandingClaims()
        {
            SetExportPermission(ReportPermissions.OutstandingClaims);
            var branchId = SetSortParamsWithServiceAndBranch("LastName", "ASC", true);
            return PartialView("Billing/OutstandingClaims", reportService.UnProcessedBillViewData(branchId, "All"));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.OutstandingClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OutstandingClaimsContent(Guid BranchId, string Type, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Billing/Content/OutstandingClaims", reportService.UnProcessedBillViewData(BranchId, Type));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.OutstandingClaims, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportOutstandingClaims(Guid BranchId, string Type)
        {
            return reportExcelGenerator.ExportOutstandingClaims(reportService.UnProcessedBillViewData(BranchId, Type), Type);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ClaimsHistoryByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimsByStatus()
        {
            SetExportPermission(ReportPermissions.ClaimsHistoryByStatus);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/ClaimByStatus", reportService.BillViewDataByStatus(branchId, "All", 300, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ClaimsHistoryByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimsByStatusContent(Guid BranchId, string Type, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/ClaimByStatus", reportService.BillViewDataByStatus(BranchId, Type, Status, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ClaimsHistoryByStatus, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportClaimsByStatus(Guid BranchId, string Type, int Status, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportClaimsByStatus(reportService.BillViewDataByStatus(BranchId, Type, Status, StartDate, EndDate), Type, Status);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ActualSubmittedClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedClaims()
        {
            SetExportPermission(ReportPermissions.ActualSubmittedClaims);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/SubmittedClaims", reportService.SubmittedBillViewDataByDateRange(branchId, "All", DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ActualSubmittedClaims, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimsContent(Guid BranchId, string Type, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/SubmittedClaims", reportService.SubmittedBillViewDataByDateRange(BranchId, Type, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ActualSubmittedClaims, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportSubmittedClaims(Guid BranchId, string Type, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportSubmittedClaims(reportService.SubmittedBillViewDataByDateRange(BranchId, Type, StartDate, EndDate), Type);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpectedSubmittedClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExpectedSubmittedClaims()
        {
            SetExportPermission(ReportPermissions.ExpectedSubmittedClaims);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/ExpectedSubmittedClaims", reportService.ExpectedSubmittedClaimsByDateRange(branchId, "All", DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpectedSubmittedClaims, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpectedSubmittedClaimsContent(Guid BranchId, string Type, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/ExpectedSubmittedClaims", reportService.ExpectedSubmittedClaimsByDateRange(BranchId, Type, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpectedSubmittedClaims, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportExpectedSubmittedClaims(Guid BranchId, string Type, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportExpectedSubmittedClaims(reportService.ExpectedSubmittedClaimsByDateRange(BranchId, Type, StartDate, EndDate), Type);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AccountsReceivableReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AccountsReceivable()
        {
            SetExportPermission(ReportPermissions.AccountsReceivableReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/AccountsReceivable", billingService.AccountsReceivables(branchId, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AccountsReceivableReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AccountsReceivableContent(Guid BranchId, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            var viewData = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    viewData = billingService.AccountsReceivableRaps(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    viewData = billingService.AccountsReceivableFinals(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    viewData = billingService.AccountsReceivables(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
            }

            return PartialView("Billing/Content/AccountsReceivable", viewData);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AccountsReceivableReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportAccountsReceivable(Guid BranchId, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            var bills = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    bills = billingService.AccountsReceivableRaps(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    bills = billingService.AccountsReceivableFinals(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    bills = billingService.AccountsReceivables(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
            }

            return reportExcelGenerator.ExportAccountsReceivable(bills, Type, InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedandUnearnedRevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnearnedRevenue()
        {
            SetExportPermission(ReportPermissions.EarnedandUnearnedRevenueReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/UnearnedRevenue", billingService.GetUnearnedRevenue(branchId, 0, DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedandUnearnedRevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnearnedRevenueContent(Guid BranchId, int InsuranceId, DateTime EndDate, string SortParams)
        {
            IsDateValid(EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/UnearnedRevenue", billingService.GetUnearnedRevenue(BranchId, InsuranceId, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedandUnearnedRevenueReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportUnearnedRevenue(Guid BranchId, int InsuranceId, DateTime EndDate)
        {
            IsDateValid(EndDate);
            return reportExcelGenerator.ExportUnearnedRevenue(billingService.GetUnearnedRevenue(BranchId, InsuranceId, EndDate), InsuranceId, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.BilledandUnbilledRevenueReport , PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledRevenue()
        {
            SetExportPermission(ReportPermissions.BilledandUnbilledRevenueReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/UnbilledRevenue", billingService.GetUnbilledRevenue(branchId, 0, DateTime.Now.AddDays(-29), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.BilledandUnbilledRevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnbilledRevenueContent(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/UnbilledRevenue", billingService.GetUnbilledRevenue(BranchId, InsuranceId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.BilledandUnbilledRevenueReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportUnbilledRevenue(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportUnbilledRevenue(billingService.GetUnbilledRevenue(BranchId, InsuranceId, StartDate, EndDate), InsuranceId, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedRevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EarnedRevenue()
        {
            SetExportPermission(ReportPermissions.EarnedRevenueReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/EarnedRevenue", billingService.GetEarnedRevenue(branchId, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedRevenueReport, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EarnedRevenueContent(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/EarnedRevenue", billingService.GetEarnedRevenue(BranchId, InsuranceId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedRevenueReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEarnedRevenue(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportEarnedRevenue(billingService.GetEarnedRevenue(BranchId, InsuranceId, StartDate, EndDate), InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AgedAccountsReceivableReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AgedAccountsReceivable()
        {
            SetExportPermission(ReportPermissions.AgedAccountsReceivableReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/AgedAccountsReceivable", billingService.AccountsReceivables(branchId, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AgedAccountsReceivableReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgedAccountsReceivableContent(Guid BranchId, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            var viewData = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    viewData = billingService.AccountsReceivableRaps(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    viewData = billingService.AccountsReceivableFinals(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    viewData = billingService.AccountsReceivables(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
            }

            return PartialView("Billing/Content/AgedAccountsReceivable", viewData);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AgedAccountsReceivableReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportAgedAccountsReceivable(Guid BranchId, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            var bills = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    bills = billingService.AccountsReceivableRaps(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    bills = billingService.AccountsReceivableFinals(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    bills = billingService.AccountsReceivables(BranchId, InsuranceId, StartDate, EndDate).ToList();
                }
            }

            return reportExcelGenerator.ExportAgedAccountsReceivable(bills, Type, InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSRAPClaimsNeeded, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSRAPClaims()
        {
            SetExportPermission(ReportPermissions.PPSRAPClaimsNeeded);
            var branchId = SetSortParamsWithServiceAndBranch("LastName", "ASC", true);
            return PartialView("Billing/PPSRAPClaims", billingService.GetPPSRapClaims(branchId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSRAPClaimsNeeded, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSRAPClaimsContent(Guid BranchId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Billing/Content/PPSRAPClaims", billingService.GetPPSRapClaims(BranchId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSRAPClaimsNeeded, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPPSRAPClaims(Guid BranchId)
        {
            return reportExcelGenerator.ExportPPSRAPClaims(billingService.GetPPSRapClaims(BranchId).OrderBy(b => b.DisplayName).ToList());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSFinalClaimsNeeded, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSFinalClaims()
        {
            SetExportPermission(ReportPermissions.PPSFinalClaimsNeeded);
            var branchId = SetSortParamsWithServiceAndBranch("LastName", "ASC", true);
            return PartialView("Billing/PPSFinalClaims", billingService.GetPPSFinalClaims(branchId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSFinalClaimsNeeded, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSFinalClaimsContent(Guid BranchId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Billing/Content/PPSFinalClaims", billingService.GetPPSFinalClaims(BranchId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSFinalClaimsNeeded, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPPSFinalClaims(Guid BranchId)
        {
            return reportExcelGenerator.ExportPPSFinalClaims(billingService.GetPPSFinalClaims(BranchId).OrderBy(b => b.DisplayName).ToList());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PotentialClaimAutoCancel, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PotentialClaimAutoCancel()
        {
            SetExportPermission(ReportPermissions.PotentialClaimAutoCancel);
            var branchId = SetSortParamsWithServiceAndBranch("LastName", "ASC", true);
            return PartialView("Billing/PotentialClaimAutoCancel", billingService.GetPotentialCliamAutoCancels(branchId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PotentialClaimAutoCancel, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PotentialClaimAutoCancelContent(Guid BranchId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Billing/Content/PotentialClaimAutoCancel", billingService.GetPotentialCliamAutoCancels(BranchId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PotentialClaimAutoCancel, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPotentialClaimAutoCancel(Guid BranchId)
        {
            return reportExcelGenerator.ExportPotentialClaimAutoCancel(billingService.GetPotentialCliamAutoCancels(BranchId).OrderBy(b => b.DisplayName).ToList());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.BillingBatchReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BillingBatch()
        {
            SetExportPermission(ReportPermissions.BillingBatchReport);
            SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/BillingBatch", billingService.BillingBatch("ALL", DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.BillingBatchReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BillingBatchContent(string ClaimType, DateTime BatchDate, string SortParams)
        {
            IsDateValid(BatchDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/BillingBatch", billingService.BillingBatch(ClaimType, BatchDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.BillingBatchReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportBillingBatch(string ClaimType, DateTime BatchDate)
        {
            IsDateValid(BatchDate);
            return reportExcelGenerator.ExportBillingBatch(billingService.BillingBatch(ClaimType, BatchDate).ToList(), ClaimType, BatchDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedRevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EarnedRevenueByEpisodeDays()
        {
            SetExportPermission(ReportPermissions.EarnedRevenueReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/EarnedRevenueByEpisodeDays", billingService.GetEarnedRevenueByEpisodeDays(branchId, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedRevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EarnedRevenueByEpisodeDaysContent(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/EarnedRevenueByEpisodeDays", billingService.GetEarnedRevenueByEpisodeDays(BranchId, InsuranceId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EarnedRevenueReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEarnedRevenueByEpisodeDays(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportEarnedRevenueByEpisodeDays(billingService.GetEarnedRevenueByEpisodeDays(BranchId, InsuranceId, StartDate, EndDate), InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.RevenueReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BilledRevenue()
        {
            SetExportPermission(ReportPermissions.RevenueReport);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/BilledRevenue", billingService.GetRevenueReport(branchId, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.RevenueReport, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BilledRevenueContent(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/BilledRevenue", billingService.GetRevenueReport(BranchId, InsuranceId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.RevenueReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportBilledRevenue(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportBilledRevenue(billingService.GetRevenueReport(BranchId, InsuranceId, StartDate, EndDate), InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ManagedCareClaimsAccountsReceivable, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedAccountsReceivable()
        {
            SetExportPermission(ReportPermissions.ManagedCareClaimsAccountsReceivable);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/ManagedAccountsReceivable", billingService.GetAccountsReceivableManagedClaims(branchId, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ManagedCareClaimsAccountsReceivable, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedAccountsReceivableContent(Guid BranchId, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/ManagedAccountsReceivable", billingService.GetAccountsReceivableManagedClaims(BranchId, InsuranceId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ManagedCareClaimsAccountsReceivable, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportManagedAccountsReceivable(Guid BranchId, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportManagedAccountsReceivable(billingService.GetAccountsReceivableManagedClaims(BranchId, InsuranceId, StartDate, EndDate), Type, InsuranceId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ManagedCareClaimsHistoryByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimsByStatus()
        {
            SetExportPermission(ReportPermissions.ManagedCareClaimsHistoryByStatus);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/ManagedClaimByStatus", billingService.GetManagedClaimsWithPaymentData(branchId, (int)ManagedClaimStatus.ClaimCreated, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ManagedCareClaimsHistoryByStatus, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsByStatusContent(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/ManagedClaimByStatus", billingService.GetManagedClaimsWithPaymentData(BranchId, Status, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ManagedCareClaimsHistoryByStatus, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportManagedClaimsByStatus(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportManagedClaimsByStatus(billingService.GetManagedClaimsWithPaymentData(BranchId, Status, StartDate, EndDate), Status);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnbilledManagedCareClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledManagedClaims()
        {
            SetExportPermission(ReportPermissions.UnbilledManagedCareClaims);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Billing/UnbilledManagedClaim", billingService.GetManagedClaimsWithPaymentData(branchId, (int)ManagedClaimStatus.ClaimCreated, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnbilledManagedCareClaims, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnbilledManagedClaimsContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Billing/Content/UnbilledManagedClaim", billingService.GetManagedClaimsWithPaymentData(BranchId, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnbilledManagedCareClaims, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportUnbilledManagedClaims(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportUnbilledManagedClaims(billingService.GetManagedClaimsWithPaymentData(BranchId, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate), StartDate, EndDate);
        }

        #endregion

        #region Employee Reports

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeRoster()
        {
            SetExportPermission(ReportPermissions.EmployeeRoster);
            var branchId = SetSortParamsWithServiceAndBranch("DisplayName", "ASC", true);
            return PartialView("Employee/Roster", reportService.GetEmployeeRoster(branchId, (int)UserStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeRoster, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeRosterContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/Roster", reportService.GetEmployeeRoster(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeRoster, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeRoster(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportEmployeeRoster(reportService.GetEmployeeRoster(BranchId, StatusId), StatusId);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeBirthdays()
        {
            SetExportPermission(ReportPermissions.EmployeeBirthdayListing);
            var branchId = SetSortParamsWithServiceAndBranch("Name", "ASC", true);
            return PartialView("Employee/BirthdayList", reportService.GetEmployeeBirthdays(branchId, (int)UserStatus.Active, DateTime.Now.Month));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeBirthdayListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeBirthdaysContent(Guid BranchId, int StatusId, int Month, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/BirthdayList", reportService.GetEmployeeBirthdays(BranchId, StatusId, Month));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeBirthdayListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeBirthdays(Guid BranchId, int StatusId, int Month)
        {
            return reportExcelGenerator.ExportEmployeeBirthdayList(reportService.GetEmployeeBirthdays(BranchId, StatusId, Month), StatusId, Month);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeLicense()
        {
            SetExportPermission(ReportPermissions.EmployeeLicenseListing);
            SetSortParamsWithServiceAndBranch("InitiationDate", "ASC", true);
            return PartialView("Employee/License", new List<License>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeLicenseContent(Guid UserId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/License", UserId.IsEmpty() ? new List<License>() : userService.GetLicenses(UserId, false));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeLicenseListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeLicense(Guid UserId)
        {
            return reportExcelGenerator.ExportEmployeeLicenseListing(userService.GetLicenses(UserId, false), UserId);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AllEmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllEmployeeLicense()
        {
            SetExportPermission(ReportPermissions.AllEmployeeLicenseListing);
            var branchId = SetSortParamsWithServiceAndBranch("InitiationDate", "ASC", true);
            return PartialView("Employee/AllEmployeeLicense", userService.GetLicenses(branchId, (int)UserStatus.Active, true));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AllEmployeeLicenseListing, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllEmployeeLicenseContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/AllEmployeeLicense", userService.GetLicenses(BranchId, StatusId, true));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AllEmployeeLicenseListing, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportAllEmployeeLicense(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportAllEmployeeLicenseListing(userService.GetLicenses(BranchId, StatusId, true).ToList());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpiringLicenses, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExpiringLicense()
        {
            SetExportPermission(ReportPermissions.ExpiringLicenses);
            var branchId = SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Employee/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(branchId, (int)UserStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpiringLicenses, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringLicenseContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.ExpiringLicenses, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportExpiringLicense(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportEmployeeExpiringLicense(reportService.GetEmployeeExpiringLicenses(BranchId, StatusId), StatusId);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeVisitByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisitByDateRange()
        {
            SetExportPermission(ReportPermissions.EmployeeVisitByDateRange);
            var branchId = SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Employee/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(branchId, DateTime.Now.AddDays(-15), DateTime.Now.AddDays(15)));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeVisitByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeVisitByDateRangeContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Employee/Content/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeVisitByDateRange, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeVisitByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportEmployeeVisitByDateRange(reportService.GetEmployeeScheduleByDateRange(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeePermissionsReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeePermissions()
        {
            SetExportPermission(ReportPermissions.EmployeePermissionsReport);
            var branchId = SetSortParamsWithServiceAndBranch("UserDisplayName", "ASC", true);
            return PartialView("Employee/Permissions", reportService.GetEmployeePermissions(branchId, (int)UserStatus.Active));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeePermissionsReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeePermissionsContent(Guid BranchId, int StatusId, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Employee/Content/Permissions", reportService.GetEmployeePermissions(BranchId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeePermissionsReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeePermissions(Guid BranchId, int StatusId)
        {
            return reportExcelGenerator.ExportEmployeePermissionsListing(userService.GetAllUsers(BranchId, StatusId));
        }

        // BOOKMARK

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PayrollDetailSummary, PermissionActions.ViewList)]
        public ActionResult PayrollDetailSummary()
        {
            SetExportPermission(ReportPermissions.PayrollDetailSummary);
            SetSortParamsWithServiceAndBranch(string.Empty, string.Empty, true);
            return PartialView("Employee/PayrollDetailSummary");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PayrollDetailSummary, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayrollDetailSummaryContent(Guid UserId, DateTime StartDate, DateTime EndDate, string PayrollStatus)
        {
            AreDatesValid(StartDate, EndDate);
            return PartialView("Employee/Content/PayrollDetailSummary", UserId.IsEmpty() ? new List<PayrollDetailSummaryItem>() : payrollService.GetVisitsSummary(UserId, StartDate, EndDate, PayrollStatus));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PayrollDetailSummary, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPayrollDetailSummary(Guid UserId, DateTime StartDate, DateTime EndDate, string PayrollStatus)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportPayrollDetailSummary(payrollService.GetVisitsSummary(UserId, StartDate, EndDate, PayrollStatus), UserId, StartDate, EndDate);
        }

        #endregion

        #region Statistical Reports

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientVisits()
        {
            SetExportPermission(ReportPermissions.PatientVisitHistory);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Statistical/PatientVisitHistory", new List<TaskLean>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientVisitsContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/PatientVisitHistory", PatientId.IsEmpty() ? new List<TaskLean>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientVisitHistory, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportPatientVisits(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalPatientVisitHistory(reportService.GetPatientScheduleEventsByDateRange(PatientId, StartDate, EndDate), PatientId, StartDate, EndDate, patientService.GetPatientNameById(PatientId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientAdmissionsByInternalReferralSource, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdmissionsByInternalReferralSource()
        {
            SetExportPermission(ReportPermissions.PatientAdmissionsByInternalReferralSource);
            SetSortParamsWithServiceAndBranch("UserName", "ASC", true);
            return PartialView("Statistical/PatientAdmissionsByInternalReferralSource", new List<PatientAdmission>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientAdmissionsByInternalReferralSource, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionsByInternalReferralSourceContent(Guid BranchId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/PatientAdmissionsByInternalReferralSource", reportService.GetPatientAdmissionsByInternalSource(BranchId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientAdmissionsByInternalReferralSource, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportAdmissionsByInternalReferralSource(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalPatientAdmissionsByInternalReferralSource(reportService.GetPatientAdmissionsByInternalSource(BranchId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisits()
        {
            SetExportPermission(ReportPermissions.EmployeeVisitHistory);
            SetSortParamsWithServiceAndBranch("PatientName", "ASC", true);
            return PartialView("Statistical/EmployeeVisitHistory", new List<UserVisit>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeVisitHistory, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeVisitsContent(Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/EmployeeVisitHistory", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetEmployeeVisistList(UserId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.EmployeeVisitHistory, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportEmployeeVisits(Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalEmployeeVisitHistory(reportService.GetEmployeeVisistList(UserId, StartDate, EndDate), StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MonthlyAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MonthlyAdmissions()
        {
            SetExportPermission(ReportPermissions.MonthlyAdmissionReport);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Statistical/MonthlyAdmission", reportService.GetPatientMonthlyAdmission(branchId, (int)PatientStatus.Active, DateTime.Now.Month, DateTime.Now.Year));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MonthlyAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MonthlyAdmissionsContent(Guid BranchId, int StatusId, int Month, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/MonthlyAdmission", reportService.GetPatientMonthlyAdmission(BranchId, StatusId, Month, Year));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MonthlyAdmissionReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportMonthlyAdmissions(Guid BranchId, int StatusId, int Month, int Year)
        {
            return reportExcelGenerator.ExportStatisticalMonthlyAdmission(reportService.GetPatientMonthlyAdmission(BranchId, StatusId, Month, Year), StatusId, Month, Year);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AnnualAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AnnualAdmissions()
        {
            SetExportPermission(ReportPermissions.AnnualAdmissionReport);
            var branchId = SetSortParamsWithServiceAndBranch("PatientFirstName", "ASC", true);
            return PartialView("Statistical/AnnualAdmission", reportService.GetPatientByAdmissionYear(branchId, (int)PatientStatus.Active, DateTime.Now.Year));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AnnualAdmissionReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualAdmissionsContent(Guid BranchId, int StatusId, int Year, string SortParams)
        {
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/AnnualAdmission", reportService.GetPatientByAdmissionYear(BranchId, StatusId, Year));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AnnualAdmissionReport, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportAnnualAdmissions(Guid BranchId, int StatusId, int Year)
        {
            return reportExcelGenerator.ExportStatisticalAnnualAdmission(reportService.GetPatientByAdmissionYear(BranchId, StatusId, Year), StatusId, Year);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnduplicatedCensusReportByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnduplicatedCensus()
        {
            SetExportPermission(ReportPermissions.UnduplicatedCensusReportByDateRange);
            var branchId = SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Statistical/UnduplicatedCensusReport", reportService.GetPatientByAdmissionUnduplicatedByDateRange(branchId, (int)PatientStatus.Active, DateTime.Now.AddDays(-59), DateTime.Now.Date));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnduplicatedCensusReportByDateRange, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnduplicatedCensusContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            AreDatesValid(StartDate, EndDate);
            SetSortParams(SortParams);
            return PartialView("Statistical/Content/UnduplicatedCensusReport", reportService.GetPatientByAdmissionUnduplicatedByDateRange(BranchId, StatusId, StartDate, EndDate));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.UnduplicatedCensusReportByDateRange, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportUnduplicatedCensus(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            AreDatesValid(StartDate, EndDate);
            return reportExcelGenerator.ExportStatisticalUnduplicatedCensusReport(reportService.GetPatientByAdmissionUnduplicatedByDateRange(BranchId, StatusId, StartDate, EndDate), StatusId, StartDate, EndDate);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CensusByPrimaryInsurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CensusByPrimaryInsurance()
        {
            SetExportPermission(ReportPermissions.CensusByPrimaryInsurance);
            SetSortParamsWithServiceAndBranch("PatientDisplayName", "ASC", true);
            return PartialView("Statistical/CensusByPrimaryInsurance", new List<PatientRoster>());
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CensusByPrimaryInsurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CensusByPrimaryInsuranceContent(Guid BranchId, int InsuranceId, int StatusId)
        {
            return PartialView("Statistical/Content/CensusByPrimaryInsurance", InsuranceId <= 0 ? new List<PatientRoster>() : reportService.GetPatientRosterByInsurance(BranchId, InsuranceId, StatusId));
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.CensusByPrimaryInsurance, PermissionActions.Export)]
        [FileDownload]
        public ActionResult ExportCensusByPrimaryInsurance(Guid BranchId, int InsuranceId, int StatusId)
        {
            return reportExcelGenerator.ExportCensusByPrimaryInsurance(InsuranceId > 0 ? reportService.GetPatientRosterByInsurance(BranchId, InsuranceId, StatusId) : new List<PatientRoster>(), InsuranceId, StatusId);
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSEpisodeInformationReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSEpisodeInformation()
        {
            SetBranchWithService();
            return PartialView("Statistical/PPSEpisodeInformation");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSVisitInformationReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSVisitInformation()
        {
            SetBranchWithService();
            return PartialView("Statistical/PPSVisitInformation");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSPaymentInformationReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSPaymentInformation()
        {
            SetBranchWithService();
            return PartialView("Statistical/PPSPaymentInformation");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PPSChargeInformationReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSChargeInformation()
        {
            SetBranchWithService();
            return PartialView("Statistical/PPSChargeInformation");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.DischargesByReasonReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargesByReason()
        {
            SetBranchWithService();
            return PartialView("Statistical/DischargesByReason");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PrimaryPaymentSourceReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByPrimaryPaymentSource()
        {
            SetBranchWithService();
            return PartialView("Statistical/VisitsByPrimaryPaymentSource");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.TypeofStaffReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByStaffType()
        {
            SetBranchWithService();
            return PartialView("Statistical/VisitsByStaffType");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.AdmissionsByReferralSourceReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdmissionsByReferralSource()
        {
            SetBranchWithService();
            return PartialView("Statistical/AdmissionsByReferralSource");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PrincipalDiagnosisReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientsVisitsByPrincipalDiagnosis()
        {
            SetBranchWithService();
            return PartialView("Statistical/PatientsVisitsByPrincipalDiagnosis");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.PatientsAndVisitsByAgeReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientsAndVisitsByAge()
        {
            SetBranchWithService();
            return PartialView("Statistical/PatientsAndVisitsByAge");
        }

        [ReportPermissionFilter(AgencyServices.HomeHealth, ReportPermissions.MedicareCostReport, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CostReport()
        {
            SetBranchWithService();
            return PartialView("Statistical/CostReport");
        }

        #endregion

        #endregion

        private void SetSortParams(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
        }

        private Guid SetSortParamsWithServiceAndBranch(string sortColumn, string sortDirection, bool isSetBranchNeeded)
        {
            ViewData["SortColumn"] = sortColumn;
            ViewData["SortDirection"] = sortDirection;

            var branchId = Guid.Empty;
            if (isSetBranchNeeded)
            {
                branchId = SetBranchWithService();
            }
            else
            {
                ViewData["Service"] = (int)AgencyServices.HomeHealth;
            }

            return branchId;
        }

        private void SetExportPermission(ReportPermissions report)
        {
            var permission = UserSessionEngine.ReportPermissions(Current.AgencyId, Current.UserId, Current.AcessibleServices, Current.SessionId);// Current.ReportPermissions;
            if (permission.IsInPermission(AgencyServices.HomeHealth, (int)report, PermissionActions.Export))
            {
                ViewData["IsUserCanExport"] = 1;
            }
            else
            {
                ViewData["IsUserCanExport"] = 0;
            }
        }

        /// <summary>
        /// Set the service and branch and return branch Id
        /// </summary>
        /// <returns></returns>
        private Guid SetBranchWithService()
        {
            var branchId = agencyService.GetBranchForSelectionId((int)AgencyServices.HomeHealth);
            ViewData["BranchId"] = branchId;
            ViewData["Service"] = (int)AgencyServices.HomeHealth;
            return branchId;
        }

        private void IsDateValid(DateTime date)
        {
            if (!date.IsValid())
            {
                throw new ArgumentException("The date used is invalid.");
            }
        }

        private void AreDatesValid(DateTime date, DateTime otherDate)
        {
            if (!date.IsValid() || !otherDate.IsValid())
            {
                throw new ArgumentException("The dates used in the date range are invalid.");
            }
            if (otherDate < date)
            {
                throw new ArgumentException("The End Date must be larger than the Start Date.");
            }
        }
    }
    // ReSharper restore InconsistentNaming
}