﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.Log.Enums;
    using Telerik.Web.Mvc;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PhysicianController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly IPatientService patientService;

        public PhysicianController(IPhysicianService physicianService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.physicianService = physicianService;
            this.patientService = patientService;
        }

        #endregion

        #region PhysicianController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPhysician(Guid physicianId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            return Json(physicianService.GetAgencyPhysician(physicianId, false));
        }

        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = physicianService.GetPhysicianListViewData();
            viewData.SortColumn ="DisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(string SortParams)
        {
            var viewData = physicianService.GetPhysicianListViewData();
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 1800, VaryByParam = "*")]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Add)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Add)]
        public ActionResult Add([Bind] AgencyPhysician agencyPhysician)
        {
            //AgencyPhysician agencyPhysician = AgencyPhysician;
            Check.Argument.IsNotNull(agencyPhysician, "agencyPhysician");
            return Json(physicianService.AddPhysician(agencyPhysician));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(physicianService.GetAgencyPhysicianForEdit(Id, true));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult Update([Bind] AgencyPhysician physician)
        {
            Check.Argument.IsNotNull(physician, "physician");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician, " + physician.DisplayName + ", could not be updated." };

            if (physician.PhysicianAccess && physician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "An E-mail Address is required for Physician Access";
                return Json(viewData);
            }

            if (physician.IsValid())
            {
                if (physicianService.UpdatePhysician(physician))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician, " + physician.DisplayName + ", has been successfully updated.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = physician.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Delete)]
        public JsonResult Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            return Json(physicianService.DeletePhysician(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetPrimary(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "PatientId");
            var physicianName = PhysicianEngine.GetName(id, Current.AgencyId);
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician, " + physicianName + ", could not be set as the primary." };
            if (patientService.SetPrimary(patientId, id))
            {
                viewData.PatientId = patientId; 
                viewData.isSuccessful = true;
                viewData.errorMessage = "The physician, " + physicianName + ", is now the primary physician.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.ViewLog)]
        public ActionResult PhysicianLogs(Guid physicianId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyPhysician, Current.AgencyId, physicianId.ToString()));
        }

        [GridAction]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult LicenseList(Guid physicianId)
        {
            return View(new GridModel(physicianService.GeAgencyPhysicianLicenses(physicianId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult NewLicense(Guid physicianId)
        {
            return PartialView("License/New", physicianId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult AddLicense(PhysicainLicense license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (license.IsValid())
            {
                if (physicianService.AddLicense(license))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "License has been successfully saved.";
                }
            }
            else
            {
                viewData.errorMessage = license.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult EditLicense(Guid physicianId, Guid licenseId)
        {
            return PartialView("License/Edit", physicianService.GetLicense(licenseId, physicianId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Physician, PermissionActions.Edit)]
        public ActionResult UpdateLicense(PhysicainLicense license)
        {
            Check.Argument.IsNotEmpty(license.Id, "Id");
            Check.Argument.IsNotEmpty(license.PhysicianId, "PhysicianId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician license could not be updated. Please try again." };
            if (license.IsValid())
            {
                if (physicianService.UpdateLicense(license))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician license has been updated.";
                }
            }
            else
            {
                viewData.errorMessage = license.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicense(Guid Id, Guid physicianId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician license could not be deleted. Please try again." };
            if (physicianService.DeleteLicense(Id, physicianId))
            {
                viewData.isSuccessful = true;
            }
            return Json(viewData);
        }

        #endregion

    }
}
