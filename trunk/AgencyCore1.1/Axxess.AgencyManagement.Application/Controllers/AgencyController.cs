﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Extensions;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Enums;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Entities.Enums;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;

        public AgencyController(IAgencyService agencyService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");

            this.userService = userService;
            this.agencyService = agencyService;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Info()
        {
            return PartialView(agencyService.GetAgencyWithMainLocation());
        }

        [OutputCache(NoStore = false, Duration = 300, VaryByParam = "*")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Signature()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoContent(Guid branchId)
        {
            return PartialView("InfoContent", agencyService.GetLocationOrDefault(branchId));
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateAgency([Bind] Agency agency, [Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(agency, "agency");
            Check.Argument.IsNotNull(location, "location");

            return Json(agencyService.UpdateAgency(agency, location));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckSignature(string signature)
        {
            Check.Argument.IsNotEmpty(signature, "signature");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your signature does not match the one on file." };

            if (ServiceHelper.IsSignatureCorrect(Current.UserId, signature))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Signature verified.";
            }
            return Json(viewData);
        }

        //[GridAction]
        //public ActionResult Users()
        //{
        //    return View(new GridModel(userService.GetAgencyUsers()));
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Blankforms()
        {
            return PartialView();
        }

        #endregion

        #region Medicare Eligibility Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicareEligibilitySummary()
        {
            ViewData["SortColumn"] = "CreatedFormatted";
            ViewData["SortDirection"] = "ASC";
            return PartialView("MedicareEligibility/Summary", agencyService.GetMedicareEligibilitySummariesBetweenDates(DateTime.Now.AddDays(-25), DateTime.Now.AddDays(25)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityContent(string SortParams, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("MedicareEligibility/Content", agencyService.GetMedicareEligibilitySummariesBetweenDates(StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicareEligibilityPrint(Guid id)
        {
            return View("MedicareEligibility/Print", agencyService.GetMedicareEligibilitySummary(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityPdf(Guid id)
        {
            var doc = new MedicareEligibilitySummaryPdf(agencyService.GetMedicareEligibilitySummary(id));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MedicareEligibilitySummary_{0}.pdf", DateTime.Now.Ticks));
            return new FileStreamResult(stream, "application/pdf");
        }
      
        #endregion

        #region Contact Actions

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Contacts()
        {
            var viewData = agencyService.GetGridViewData(ParentPermission.Contact);
            return PartialView("Contact/List", viewData);
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult ContactList()
        {
            return View(new GridModel(agencyService.GetContacts().Select(s => new AgencyContactGridRow(s)).ToList()));
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 1800, VaryByParam = "*")]
        public ActionResult NewContact()
        {
            return PartialView("Contact/New");
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");
            return Json(agencyService.AddContact(contact));
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditContact(Guid Id)
        {
            return PartialView("Contact/Edit", agencyService.FindContact(Id));
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");
            return Json(agencyService.UpdateContact(contact));
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteContact(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.DeleteContact(id));
        }

        [AllServicePermissionFilter(ParentPermission.Contact, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactLogs(Guid contactId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyContact, Current.AgencyId, contactId.ToString()));
        }

        #endregion

        #region Hospital Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 1800, VaryByParam = "*")]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.Add)]
        public ActionResult NewHospital()
        {
            return PartialView("Hospital/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.Add)]
        public ActionResult AddHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");
            return Json(agencyService.AddHospital(hospital));
        }
      
        [AcceptVerbs(HttpVerbs.Get)]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.ViewList)]
        public ActionResult Hospitals()
        {
            var viewData = agencyService.GetGridViewData(ParentPermission.Hospital);
            return PartialView("Hospital/List", viewData);
        }

        [GridAction]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.ViewList)]
        public ActionResult HospitalList()
        {
            return View(new GridModel(agencyService.GetHospitals().Select(s => new AgencyHospitalGridRow(s))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.Edit)]
        public ActionResult EditHospital(Guid Id)
        {
            return PartialView("Hospital/Edit", agencyService.FindHospital( Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.Edit)]
        public ActionResult UpdateHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");
            return Json(agencyService.UpdateHospital(hospital));
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.Delete)]
        public JsonResult DeleteHospital(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(agencyService.DeleteHospital(Id));
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.Hospital, PermissionActions.ViewLog)]
        public ActionResult HospitalLogs(Guid hospitalId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyHospital, Current.AgencyId, hospitalId.ToString()));
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewLocation()
        {
            return PartialView("Location/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid())
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Locations()
        {
            return PartialView("Location/List");
        }

        [GridAction]
        public ActionResult LocationList()
        {
            return View(new GridModel(agencyService.GetBranches()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid Id)
        {
            return PartialView("Location/Edit", agencyService.FindLocation(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");
            return Json(agencyService.UpdateLocation(location));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationLogs(Guid locationId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyLocation, Current.AgencyId, locationId.ToString()));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult LocationBillDatas(Guid locationId)
        {
            return View(new GridModel(agencyService.GetLocationBillDataChargeRates(locationId)));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocationBillData(Guid LocationId, int Id)
        {
            return PartialView("EditBillData", agencyService.GetLocationBillDataChargeRate(LocationId, Id));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocationBillData(ChargeRate chargeRate)
        {
            return Json(agencyService.UpdateLocationBillData(chargeRate));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLocationBillData(Guid locationId)
        {
            return PartialView("NewBillData", locationId);
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveLocationBillData(Guid locationId, ChargeRate chargeRate)
        {
            return Json(agencyService.SaveLocationBillData(locationId, chargeRate));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLocationBillData(Guid LocationId, int Id)
        {
            return Json(agencyService.DeleteLocationBillData(LocationId, Id));
        }

        public JsonResult DisciplineTypes(Guid LocationId, int DisciplineTask)
        {
            return Json(agencyService.DisciplineTypes(LocationId, DisciplineTask));
        }

        #endregion

        #region Insurance Actions

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 1800, VaryByParam = "*")]
        public ActionResult NewInsurance()
        {
            return PartialView("Insurance/New");
        }

        [AllServicePermissionFilter(ParentPermission.Insurance,PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            var rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                if (disciplineList != null && disciplineList.Length > 0)
                {
                    disciplineList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Charge") && formCollection[l + "_Charge"].IsDouble())
                        {
                            visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"].ToDouble(), ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                            rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) && !formCollection[l + "_Charge"].IsDouble(), "Wrong entry"));
                        }
                    });
                }

                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }

            var ub04Locator81ccaXml = formCollection.ToUb04Locator81Xml(keys, "Ub04Locator81cca");
            if (ub04Locator81ccaXml.IsNotNullOrEmpty())
            {
                insurance.Ub04Locator81cca = ub04Locator81ccaXml;
            }

            if (insurance.IsValid())
            {
                insurance.AgencyId = Current.AgencyId;
                viewData = agencyService.AddInsurance(insurance);
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }
            return Json(viewData);
        }
       

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetInsurances()
        {
            return Json(AgencyInformationHelper.GetInsurances());
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Insurances()
        {
            var viewData = agencyService.GetGridViewData(ParentPermission.Insurance);
            return PartialView("Insurance/List", viewData);
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult InsuranceList()
        {
            return View(new GridModel(agencyService.InsuranceList()));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditInsurance(int Id)
        {
            return PartialView("Insurance/Edit", agencyService.FindInsurance(Id));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            var rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                disciplineList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Charge"))
                    {
                        visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"].ToDouble(), ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) && !formCollection[l + "_Charge"].IsDouble(), "Wrong entry"));
                    }
                });

                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            var ub04Locator81ccaXml = formCollection.ToUb04Locator81ccaXml(keys);
            if (ub04Locator81ccaXml.IsNotNullOrEmpty())
            {
                insurance.Ub04Locator81cca = ub04Locator81ccaXml;
            }

            if (insurance.IsValid())
            {
                viewData = agencyService.EditInsurance(insurance);
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }

            return Json(viewData);
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteInsurance(int Id)
        {
            Check.Argument.IsNotNegativeOrZero(Id, "Id");
            return Json(agencyService.DeleteInsurance(Id));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceLogs(int insuranceId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyInsurance, Current.AgencyId, insuranceId.ToString()));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewBillData(int insuranceId)
        {
            return PartialView("Insurance/NewBillData", agencyService.GetInsuranceChargeRateForNew(insuranceId));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveBillData(int InsuranceId, ChargeRate chargeRate)
        {
            return Json(agencyService.SaveBillData(InsuranceId, chargeRate));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult BillDatas(int InsuranceId)
        {
            return View(new GridModel(agencyService.GetInsuranceBillDatas(InsuranceId)));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditBillData(int InsuranceId , int Id)
        {
            return PartialView("Insurance/EditBillData", agencyService.GetInsuranceChargeRateForEdit(InsuranceId, Id));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateBillData(ChargeRate chargeRate)
        {
            return Json(agencyService.UpdateBillData(chargeRate));
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteBillData(int InsuranceId, int Id)
        {
            return Json(agencyService.DeleteBillData(InsuranceId, Id));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult InsuranceSelectList(Guid BranchId, int Service, bool IsMediHMO)
        //{
        //    return Json(AgencyInformationHelper.InsuranceSelectList(BranchId,Service,IsMediHMO));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult InsuranceSelectList(Guid BranchId, int Service, int ClaimType)
        //{
        //    return Json(AgencyInformationHelper.InsuranceSelectList(BranchId, Service, ClaimType, ClaimType == (int)ClaimTypeSubCategory.ManagedCare ? false : true));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult MedicareInsuranceSelectList(Guid branchId)
        //{
        //    return Json(AgencyInformationHelper.MedicareInsuranceSelectList(branchId));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult MedicareWithHMOInsuranceSelectList(Guid BranchId)
        //{
        //    return Json(AgencyInformationHelper.MedicareWithHMOInsuranceSelectList(BranchId));
        //}

        public JsonResult PatientInsurances(Guid branchId, int service)
        {
            var selectList = AgencyInformationHelper.PatientInsurances(branchId,service);
            return Json(selectList.ToString());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReplaceInsuranceVisit(int Id, int replacedId)
        {
            return Json(agencyService.ReplaceInsuranceVisit(Id, replacedId));
        }

        #endregion

        #region Visit Rate Actions

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitRates()
        {
            return PartialView("VisitRate", agencyService.GetMainLocation());
        }

        [AllServicePermissionFilter(ParentPermission.Insurance, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitRateContent(Guid branchId)
        {
            return PartialView("VisitRateContent", agencyService.VisitRateContent(branchId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditCost(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.IsNotNullOrEmpty())
            {
                if (keys.Contains("AgencyLocationId"))
                {
                    var locationIdString = formCollection["AgencyLocationId"];
                    if (locationIdString.IsNotNullOrEmpty() && locationIdString.IsGuid())
                    {
                        var locationId = locationIdString.ToGuid();

                        if (!locationId.IsEmpty())
                        {
                            var ub04Locator81 = formCollection.ToUb04Locator81Xml(keys, "Ub04Locator81");
                            var agencyLocationMedicare = new AgencyMedicareInsurance();
                            agencyLocationMedicare.AddressLine1 = keys.Contains("MedicareAddressLine1") ? formCollection["MedicareAddressLine1"] : string.Empty;
                            agencyLocationMedicare.AddressLine2 = keys.Contains("MedicareAddressLine2") ? formCollection["MedicareAddressLine2"] : string.Empty;
                            agencyLocationMedicare.AddressCity = keys.Contains("MedicareAddressCity") ? formCollection["MedicareAddressCity"] : string.Empty;
                            agencyLocationMedicare.AddressStateCode = keys.Contains("MedicareAddressStateCode") ? formCollection["MedicareAddressStateCode"] : string.Empty;
                            agencyLocationMedicare.AddressZipCode = keys.Contains("MedicareAddressZipCode") ? formCollection["MedicareAddressZipCode"] : string.Empty;
                            agencyLocationMedicare.AgencyId = Current.AgencyId;
                            viewData = agencyService.EditBranchLocatorAndMedicareInsuranceInfo(locationId, ub04Locator81, agencyLocationMedicare);
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Selected branch information is not correct. Try again";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Selected branch information is not correct. Try again";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Select branch to edit.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected branch don't exist.";
            }
            return Json(viewData);
        }

        #endregion

        #region Template Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Templates()
        {
            var viewData = agencyService.GetGridViewData(ParentPermission.Template);
            return PartialView("Template/List", viewData);
        }

       
        [GridAction]
        public ActionResult TemplateList()
        {
            return View(new GridModel(TemplateEngine.GetTemplates(Current.AgencyId)));
        }

        public JsonResult TemplateSelect()
        {
            var templates = TemplateEngine.GetTemplates(Current.AgencyId);
            List<SelectListItem> items = new List<SelectListItem>();
            items.Insert(0, new SelectListItem { Text = "-- Select Template --", Value = "" });
            items.Insert(items.Count, new SelectListItem { Text = "-- Erase --", Value = "empty" });
            if (templates != null && templates.Count > 0)
            {
                items.Insert(items.Count, new SelectListItem { Text = "------------------------------", Value = "spacer" });
                templates.ForEach(t => items.Add(new SelectListItem { Text = t.Title, Value = t.Id.ToString() }));
            }
            return Json(items);
        }

        [AllServicePermissionFilter(ParentPermission.Template, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewTemplate()
        {
            return PartialView("Template/New");
        }

        [AllServicePermissionFilter(ParentPermission.Template, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddTemplate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");
            return Json(agencyService.AddTemplate(template));
        }

        [AllServicePermissionFilter(ParentPermission.Template, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditTemplate(Guid id)
        {
            return PartialView("Template/Edit", agencyService.GetTemplate(id));
        }

        [AllServicePermissionFilter(ParentPermission.Template, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateTemplate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");
            return Json(agencyService.UpdateTemplate(template));
        }

        [AllServicePermissionFilter(ParentPermission.Template, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteTemplate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.DeleteTemplate(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetTemplate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.GetTemplateOrDefault(id));
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateLogs(Guid templateId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyTemplate, Current.AgencyId, templateId.ToString()));
        }

        #endregion

        #region Supply Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SuppliesSearch(string term, int limit)
        {
            return Json(agencyService.SuppliesSearch(term, limit));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Supplies()
        {
            return PartialView("Supply/List",agencyService.GetGridViewData(ParentPermission.Supply));
        }

        [GridAction]
        public ActionResult SupplyList()
        {
            return View(new GridModel(agencyService.GetSupplies()));
        }

        [AllServicePermissionFilter(ParentPermission.Supply,PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 1800, VaryByParam = "*")]
        public ActionResult NewSupply()
        {
            return PartialView("Supply/New");
        }

        [AllServicePermissionFilter(ParentPermission.Supply, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddSupply([Bind] AgencySupply supply)
        {
            Check.Argument.IsNotNull(supply, "supply");
            return Json(agencyService.AddSupply(supply));
        }

        [AllServicePermissionFilter(ParentPermission.Supply, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid id)
        {
            return PartialView("Supply/Edit", agencyService.GetSupply(id));
        }

        [AllServicePermissionFilter(ParentPermission.Supply, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSupply([Bind] AgencySupply supply)
        {
            Check.Argument.IsNotNull(supply, "supply");
            return Json(agencyService.UpdateSupply(supply));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteSupply(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.DeleteSupply(id));
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetSupply(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.GetSupplyOrDefault(id));
        }
      
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyLogs(Guid supplyId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencySupply, Current.AgencyId, supplyId.ToString()));
        }

        #endregion

        #region Adjustment Code Actions

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdjustmentCodes()
        {
            var viewData = new GridViewData();
            var permissions = Current.CategoryService(Current.AcessibleServices, ParentPermission.AdjustmentCode, new []{ (int)PermissionActions.Export, (int)PermissionActions.Add, (int)PermissionActions.Delete,(int)PermissionActions.Edit });
            viewData.NewPermissions = permissions.GetOrDefault((int)PermissionActions.Add, AgencyServices.None);
            viewData.EditPermissions = permissions.GetOrDefault((int)PermissionActions.Edit, AgencyServices.None);
            viewData.ExportPermissions = permissions.GetOrDefault((int)PermissionActions.Export, AgencyServices.None);
            viewData.DeletePermissions = permissions.GetOrDefault((int)PermissionActions.Delete, AgencyServices.None);
            return PartialView("AdjustmentCode/List", viewData);
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.ViewList)]
        [GridAction]
        public ActionResult AdjustmentCodeList()
        {
            return View(new GridModel(agencyService.GetAdjustmentCodes()));
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewAdjustmentCode()
        {
            return PartialView("AdjustmentCode/New");
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddAdjustmentCode(AgencyAdjustmentCode adjustmentCode)
        {
            Check.Argument.IsNotNull(adjustmentCode, "code");
            return Json(agencyService.AddAdjustmentCode(adjustmentCode));
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditAdjustmentCode(Guid id)
        {
            return PartialView("AdjustmentCode/Edit", agencyService.FindAdjustmentCode(id));
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.Edit)]
        public JsonResult UpdateAdjustmentCode(AgencyAdjustmentCode adjustmentCode)
        {
            Check.Argument.IsNotNull(adjustmentCode, "code");
            return Json(agencyService.UpdateAdjustmentCode(adjustmentCode));
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.Delete)]
        public JsonResult DeleteAdjustmentCode(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.DeleteAdjustmentCode(id));
        }

        [AllServicePermissionFilter(ParentPermission.AdjustmentCode, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdjustmentCodeLogs(Guid adjustmentCodeId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyAdjustmentCode, Current.AgencyId, adjustmentCodeId.ToString()));
        }

        #endregion

        #region UploadTypes

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UploadTypes()
        {
            var viewData = new GridViewData();
            var permissions = Current.CategoryService(Current.AcessibleServices, ParentPermission.UploadType, new[] { (int)PermissionActions.Export, (int)PermissionActions.Add, (int)PermissionActions.Delete, (int)PermissionActions.Edit });
            viewData.NewPermissions = permissions.GetOrDefault((int)PermissionActions.Add, AgencyServices.None);
            viewData.EditPermissions = permissions.GetOrDefault((int)PermissionActions.Edit, AgencyServices.None);
            viewData.ExportPermissions = permissions.GetOrDefault((int)PermissionActions.Export, AgencyServices.None);
            viewData.DeletePermissions = permissions.GetOrDefault((int)PermissionActions.Delete, AgencyServices.None);
            return PartialView("UploadType/List", viewData);
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadTypeGrid()
        {
            return View(new GridModel(agencyService.GetUploadTypes()));
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewUploadType()
        {
            return PartialView("UploadType/New");
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddUploadType(UploadType uploadType)
        {
            Check.Argument.IsNotNull(uploadType, "uploadType");
            return Json(agencyService.AddUploadType(uploadType));
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUploadType(Guid id)
        {
            return PartialView("UploadType/Edit", agencyService.FindUploadType(id));
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.Edit)]
        public JsonResult UpdateUploadType(UploadType uploadType)
        {
            Check.Argument.IsNotNull(uploadType, "uploadType");
            return Json(agencyService.UpdateUploadType(uploadType));
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.Delete)]
        public JsonResult DeleteUploadType(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(agencyService.DeleteUploadType(id));
        }

        [AllServicePermissionFilter(ParentPermission.UploadType, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UploadTypeLogs(Guid uploadTypeId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.AgencyUploadType, Current.AgencyId, uploadTypeId.ToString()));
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AfterHoursSupport()
        {
            return PartialView();
        }
    }
}
