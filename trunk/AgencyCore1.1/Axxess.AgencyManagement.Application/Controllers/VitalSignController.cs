﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class VitalSignController : BaseController
    {
        private readonly HHVitalSignService vitalSignService;
        private readonly PatientService patientService;

        #region Constructor

        public VitalSignController(HHVitalSignService vitalSignService, PatientService patientService)
        {
            this.vitalSignService = vitalSignService;
            this.patientService = patientService;
        }

        #endregion

        #region Actions

        public ActionResult New(Guid patientId, Guid entityId)
        {
            var viewData = new PatientNameViewData { DisplayName = this.patientService.GetPatientNameById(patientId), PatientId = patientId, EntityId = entityId };
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(VitalSignLog log)
        {
            return Json(vitalSignService.Add(log));
        }

        public ActionResult Edit(Guid patientId, Guid entityId, Guid id)
        {
            return PartialView(vitalSignService.Get(id, entityId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(VitalSignLog log)
        {
            return Json(vitalSignService.Update(log));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid patientId, Guid id)
        {
            return Json(vitalSignService.ToggleDeprecation(patientId, id, true));
        }

        public ActionResult List(Guid patientId, Guid entityId, int disciplineTaskId)
        {
            string disciplineTask = disciplineTaskId.ToEnum(DisciplineTasks.NoDiscipline).GetDescription();
            var viewData = new EntityVitalSignViewData { PatientName = this.patientService.GetPatientNameById(patientId), PatientId = patientId, EntityId = entityId, EntityName = disciplineTask };
            return PartialView(viewData);
        }

        [GridAction]
        public ActionResult EntityListContent(Guid patientId, Guid entityId)
        {
            return View(new GridModel(vitalSignService.GetEntitiesVitalSignLogsLean(entityId, patientId)));
        }

        #endregion
    }
}
