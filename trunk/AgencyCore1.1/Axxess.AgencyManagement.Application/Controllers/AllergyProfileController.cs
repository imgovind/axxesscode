﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;

    using iTextExtension;

    using Services;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AllergyProfileController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;

        public AllergyProfileController(IPatientService patientService, HHPatientProfileService profileService)
        {
            this.profileService = profileService;
            this.patientService = patientService;
        }

        #endregion

        #region Actions

        [HomeHealthViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.AllergyProfile, PermissionActions.ViewList)]
        public ActionResult Index(Guid PatientId)
        {
            return PartialView(patientService.GetPatientAllergyProfileViewData(PatientId));
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.AllergyProfile, PermissionActions.Print)]
        public ActionResult PrintPreview(Guid id)
        {
            return FileGenerator.PreviewPdf<AllergyProfilePdf>(new AllergyProfilePdf(profileService.GetAllergyProfilePrint(id)), "AllergyProfile", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.AllergyProfile, PermissionActions.Print)]
        public FileResult Pdf(Guid id)
        {
            return FileGenerator.Pdf<AllergyProfilePdf>(new AllergyProfilePdf(profileService.GetAllergyProfilePrint(id)), "AllergyProfile");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LastestAllergies(Guid patientId)
        {
            return Json(patientService.GetAllergiesText(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.AllergyProfile, PermissionActions.ViewList)]
        public ActionResult List(Guid allergyProfileId, string prefix)
        {
            return PartialView(patientService.GetAllergyProfile(allergyProfileId, prefix));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.AllergyProfile, PermissionActions.Add)]
        public ActionResult NewAllergy(Guid allergyProfileId)
        {
            return PartialView("New", allergyProfileId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.AllergyProfile, PermissionActions.Add)]
        public JsonResult AddAllergy(Allergy allergy)
        {
            return Json(patientService.AddAllergy(allergy));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.AllergyProfile, PermissionActions.Edit)]
        public ActionResult EditAllergy(Guid allergyProfileId, Guid allergyId)
        {
            return PartialView("Edit", patientService.GetAllergy(allergyProfileId, allergyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.AllergyProfile, PermissionActions.Edit)]
        public JsonResult UpdateAllergy(Allergy allergy)
        {
            return Json(patientService.UpdateAllergy(allergy));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllServicePermissionFilter(ParentPermission.AllergyProfile, PermissionActions.Edit)]
        public JsonResult UpdateAllergyStatus(Guid allergyProfileId, Guid allergyId, bool isDeprecated)
        {
            return Json(patientService.UpdateAllergy(allergyProfileId, allergyId, isDeprecated));
        }

        #endregion
    }
}
