﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    #region

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Infrastructure.Transaction;
    using Axxess.LookUp.Domain;
    using Telerik.Web.Mvc;

    #endregion

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OasisController : BaseController
    {
        #region Fields

        private readonly IAgencyService agencyService;
        private readonly HHAssessmentService assessmentService;
        private readonly HHEpiosdeService episodeService;
        private readonly HHMediatorService mediatorService;
        private readonly HHTaskService scheduleService;
        private readonly EfficiencyEvaluator efficiencyEvaluator;

        #endregion

        #region Constructors and Destructors

        public OasisController(HHTaskService scheduleService, HHAssessmentService assessmentService, IPatientService patientService, HHEpiosdeService episodeService, HHMediatorService mediatorService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");

            this.assessmentService = assessmentService;
            this.scheduleService = scheduleService;
            this.episodeService = episodeService;
            this.mediatorService = mediatorService;
            this.agencyService = agencyService;
            this.efficiencyEvaluator = new EfficiencyEvaluator();
        }

        #endregion

        #region Public Methods and Operators

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult AddSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        //{
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(eventId, "eventId");
        //    this.assessmentService.AddSupply(patientId, eventId, supply);
        //    return View(new GridModel(this.assessmentService.GetAssessmentSupply(patientId, eventId)));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult Assessment(FormCollection formCollection)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false, errorMessage = "There is an error sending the data. Try again." };
            if (formCollection != null)
            {
                var keys = formCollection.AllKeys;
                if (keys.IsNotNullOrEmpty())
                {
                    if (keys.Contains("assessment"))
                    {
                        var assessmentType = formCollection["assessment"];
                        if (assessmentType.IsNotNullOrEmpty())
                        {
                            oasisViewData = this.assessmentService.SaveAssessment(formCollection, Request.Files, assessmentType);
                        }
                        else
                        {
                            oasisViewData.errorMessage = "Assessment type is not identfied. Try again.";
                        }
                    }
                    else
                    {
                        oasisViewData.errorMessage = "Assessment type is not identfied. Try again.";
                    }
                }
            }
            return Json(oasisViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AuditPdf(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return FileGenerator.Pdf(new OasisAuditPdf(this.assessmentService.Audit(id, patientId)), "OASISLogicalCheck");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BlankMasterCalendar(Guid episodeId, Guid patientId, string assessmentType)
        {
            return PartialView("BlankMasterCalendar", this.episodeService.GetMasterCalendarEpiosdePeriod(episodeId, patientId, assessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult BlankPdf(int AssessmentType, string Discipline)
        {
            return FileGenerator.Pdf(new OasisPdf(this.assessmentService.GetAssessmentPrint(Enum.IsDefined(typeof(AssessmentType), AssessmentType) ? (AssessmentType)AssessmentType : Entities.Enums.AssessmentType.None), Discipline), "OASIS");
        }

        [HomeHealthViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Category(Guid Id, Guid PatientId, string Category, string Version)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Assessment assessment = this.assessmentService.LoadCategory(Id, PatientId, Category) ?? new Assessment();
            return PartialView(string.Format("Assessments/Tabs/{0}{1}", Category, Version), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WoundSetPostion(Guid Id, Guid PatientId, WoundPosition position)
        {
            return Json(this.assessmentService.WoundSetPostion(Id, PatientId, position));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundDelete(Guid Id, Guid PatientId, int WoundNumber, int WoundCount)
        {
            return Json(this.assessmentService.WoundDelete(Id, PatientId, WoundNumber, WoundCount));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundAdd(Guid Id, Guid PatientId, string Type, WoundPosition position)
        {
            return PartialView("Assessments/Wound/Add", new WoundViewData<Question>() { EventId = Id, PatientId = PatientId, Type = Type, Position = position });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundEdit(Guid Id, Guid PatientId, int WoundNumber)
        {
            return PartialView("Assessments/Wound/Edit", assessmentService.GetWound(Id, PatientId, WoundNumber));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WoundUpdate(WoundSaveArguments saveArguments, FormCollection formCollection)
        {
            return Json(assessmentService.UpdateWound(saveArguments, Request.Files, formCollection));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Correction(Guid Id, Guid PatientId, int CorrectionNumber)
        {
            var export = new OasisExport();
            export.AssessmentId = Id;
            export.PatientId = PatientId;
            export.CorrectionNumber = CorrectionNumber;
            return PartialView(export);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CorrectionChange(Guid Id, Guid PatientId, int CorrectionNumber)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Correction Number could not be updated." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty() )
            {
                viewData = this.assessmentService.UpdateAssessmentCorrectionNumber(Id, PatientId, CorrectionNumber);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your Correction Number is updated.";
                }
            }
            return Json(viewData);
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        //{
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(eventId, "eventId");
        //    Check.Argument.IsNotNull(supply, "supply");
        //    this.assessmentService.DeleteSupply(patientId, eventId, supply);
        //    return View(new GridModel(this.assessmentService.GetAssessmentSupply(patientId, eventId)));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The asset could not be deleted." };
            if (this.assessmentService.DeleteWoundCareAsset(patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The asset has been successfully deleted.";
            }
            return Json(viewData);
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult EditSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        //{
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(eventId, "eventId");
        //    Check.Argument.IsNotNull(supply, "supply");
        //    this.assessmentService.UpdateSupply(patientId, eventId,  supply);
        //    return View(new GridModel(this.assessmentService.GetAssessmentSupply(patientId, eventId)));
        //}
       
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExportView()
        {
            var viewData = assessmentService.GetAssessmentByStatusViewData(Guid.Empty, true, ScheduleStatus.OasisCompletedExportReady, new List<int> { 1 });
            viewData.SortColumn = "Index";
            viewData.SortDirection = "ASC";
            return PartialView("OasisExport", viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Export(Guid BranchId, [ModelBinder(typeof(CommaSeparatedModelBinder))] List<int> PaymentSources, string SortParams)
        {

            var viewData = PaymentSources.IsNotNullOrEmpty() ? assessmentService.GetAssessmentByStatusViewData(BranchId, false, ScheduleStatus.OasisCompletedExportReady, PaymentSources) : new AssessmentExportViewData();
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Content/OasisExport", viewData);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExportedView()
        {
            return PartialView("Exported", assessmentService.ExporteAndNonExportedGridViewData(ParentPermission.OASIS, PermissionActions.ViewExported, new List<PermissionActions> { PermissionActions.Export }, true));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExportedGrid(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(this.assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisExported, StatusId, StartDate, EndDate,true)));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Frequencies(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            return PartialView("~/Views/Schedule/Frequencies.ascx", this.assessmentService.GetPatientEpisodeFrequencyData(episodeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Generate(Guid BranchId, List<Guid> OasisSelected)
        {
            return FileGenerator.PlainText(this.assessmentService.GenerateOASIS1448ForSelected(BranchId, OasisSelected),"OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult GenerateExportFile(Guid assessmentId)
        {
            var exportString = "Export File could not be created!";
            if (!assessmentId.IsEmpty())
            {
                exportString = this.assessmentService.GenerateExportFile(assessmentId);
            }
            return FileGenerator.PlainText(exportString, "OASIS Export");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult GenerateForCancel(Guid Id)
        {
            return FileGenerator.PlainText(this.assessmentService.GetOASIS1448ForCancel(Id), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(this.assessmentService.GetQuestions(Id).ToDictionary());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetDiagnosisData(Guid patientId, Guid episodeId)
        {
            return Json(this.assessmentService.GetDiagnosisData(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Inactivate(Guid Id)
        {
            return Json(this.assessmentService.ValidateInactivate(Id));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.LoadPrevious)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadPrevious(Guid episodeId, Guid patientId, Guid assessmentId, Guid previousAssessmentId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(assessmentId, "eventId");
            Check.Argument.IsNotEmpty(previousAssessmentId, "previousAssessmentId");
            return Json(this.assessmentService.LoadPreviousAssessment(episodeId, patientId, assessmentId, previousAssessmentId));
        }


        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OASIS, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkExported(List<Guid> OasisSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "The selected OASIS Assessments status could not be changed.";
            if (OasisSelected.IsNotNullOrEmpty())
            {
                if (StatusType.IsNotNullOrEmpty() && (StatusType.IsEqual("Exported") || StatusType.IsEqual("CompletedNotExported")))
                {
                    viewData = this.assessmentService.MarkAsExportedOrCompleted(OasisSelected, StatusType.IsEqual("Exported") ? (int)ScheduleStatus.OasisExported : (int)ScheduleStatus.OasisCompletedNotExported);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The selected OASIS Assessments have been  marked as " + (StatusType.IsEqual("Exported") ? "Exported." : "Completed ( Not Exported ).");
                    }
                    else
                    {
                        viewData.errorMessage = errorMessage;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected OASIS status change is not successfully . Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Select OASIS you want to change the status.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisSignature(Guid Id, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return PartialView("NonOasisSignature", assessmentService.GetAssessmentWithScheduleType(PatientId, Id, false));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NotExportedGrid(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(this.assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedNotExported, StatusId, StartDate, EndDate,true)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NotExportedView()
        {
            return PartialView("NotExported", assessmentService.ExporteAndNonExportedGridViewData(ParentPermission.OASIS, PermissionActions.ViewExported, new List<PermissionActions> { PermissionActions.Export }, true));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId), true), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PdfLimited(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf(new OasisPdf(this.assessmentService.GetAssessmentPrint(patientId, eventId), false), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PpsExport(Guid assessmentId)
        {
            return Json(this.assessmentService.GeneratePpsExport(assessmentId));
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.HomeHealth, new ParentPermission[] { ParentPermission.Schedule, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId)), "OASIS", Response);
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new OasisPdf(assessmentService.GetAssessmentPrint(patientId, eventId)), "OASIS", Response);
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ProfilePdf(Guid id, Guid patientId)
        {
            return FileGenerator.Pdf(new OasisProfilePdf(assessmentService.OASISProfileData(patientId, id)), "OASISProfile");
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ProfilePrint(Guid id, Guid patientId)
        {
            return FileGenerator.PreviewPdf(new OasisProfilePdf(assessmentService.OASISProfileData(patientId, id)), "OASISProfile", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Regenerate(Guid Id, Guid patientId)
        {
            return Json(assessmentService.Regenerate(Id, patientId));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult Submit(Guid Id, Guid patientId,   string actionType, string reason)
        //{
        //    return Json(this.mediatorService.OASISSubmit(Id, patientId,  actionType, reason));
        //}

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OASIS, PermissionActions.Reopen)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid Id, Guid patientId, string reason)
        {
            return Json(mediatorService.OASISSubmit(Id, patientId, "ReOpen", reason));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Return(Guid Id, Guid patientId,string reason)
        {
            return Json(mediatorService.OASISSubmit(Id, patientId, "Return", reason));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Approve(Guid Id, Guid patientId, string reason)
        {
            return Json(mediatorService.OASISSubmit(Id, patientId, "Approve", reason));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SubmitOnly(AssessmentSubmitArguments arguments)
        {
            return Json(this.mediatorService.OASISSubmitOnly(arguments));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supply(Guid patientId, Guid eventId)
        {
            return View(new GridModel(this.assessmentService.GetAssessmentSupply(patientId, eventId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Validate(Guid id, Guid patientId, Guid episodeId, string type, bool pps)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(type, "assessmentType");
            var viewData = this.assessmentService.Validate(id, patientId, episodeId);
            viewData.Service = AgencyServices.HomeHealth;
            ViewData["pps"] = pps;
            return PartialView("Validation", viewData);
        }

        //[HomeHealthViewDataFilter]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult View(Guid Id, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            var assessment = assessmentService.GetAssessmentWithComment(PatientId, Id);
            return PartialView(string.Format("Assessments/{0}{1}", assessment != null ? assessment.Type : "NoData", assessment != null && assessment.Version > 0 ? assessment.Version.ToString() : string.Empty), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSupply(Guid patientId, Guid eventId)
        {
            return PartialView("OasisSupply/New", new NoteSupplyNewViewData { Service = AgencyServices.PrivateDuty, PatientId = patientId, EventId = eventId });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to be added." };
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            if (assessmentService.AddSupply(patientId, eventId, supply))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully added.";
            }
            else
            {
                viewData.errorMessage = "The supply failed to be added.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid patientId, Guid eventId, Guid supplyId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(eventId, "eventId");
            Check.Argument.IsNotEmpty(supplyId, "supplyId");

            var viewData = new NoteSupplyEditViewData { Service = AgencyServices.PrivateDuty, PatientId = patientId, EventId = eventId };
            viewData.Supply = assessmentService.GetSupply(patientId, eventId, supplyId);
            return PartialView("OasisSupply/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to save." };
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            if (assessmentService.UpdateSupply(patientId, eventId, supply))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully saved.";
            }
            else
            {
                viewData.errorMessage = "The supply failed to save.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSupply(Guid patientId, Guid eventId, Guid supplyId)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply failed to delete." };
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supplyId, "supplyId");

            if (assessmentService.DeleteSupply(patientId, eventId, supplyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully deleted.";
            }
            return Json(viewData);
        }
        #endregion
    }
}