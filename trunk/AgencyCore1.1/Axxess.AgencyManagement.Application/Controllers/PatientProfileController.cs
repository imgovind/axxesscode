﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.ViewData;

    using Telerik.Web.Mvc;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Workflows;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PatientProfileController : BaseController
    {
        #region Constructor
       
        private readonly IPatientService patientService;
        private readonly HHTaskService scheduleService;
        private readonly HHMediatorService mediatorService;
        private readonly HHPatientProfileService profileService;

        public PatientProfileController(IPatientService patientService, HHPatientProfileService profileService, HHTaskService scheduleService, HHMediatorService mediatorService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.profileService = profileService;
            this.scheduleService = scheduleService;
            this.mediatorService = mediatorService;

        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult All(Guid BranchId, byte StatusId, byte PaymentSourceId, byte servicesId)
        {
            var patientList = profileService.GetPatientSelection(BranchId, StatusId, PaymentSourceId, "", true) ?? new List<PatientSelection>();
            return Json(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllSelectList(Guid BranchId, byte StatusId, byte servicesId)
        {
            var patientList = profileService.GetPatientSelection(BranchId, StatusId, 0, "", true) ?? new List<PatientSelection>();
            var patients = patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName);
            var selectList =
                from patient in patients
                select new SelectListItem
                {
                    Text = patient.DisplayNameWithMi.Trim().ToTitleCase(),
                    Value = patient.Id.ToString()
                };
            return Json(selectList);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllSort(Guid BranchId, byte StatusId, byte PaymentSourceId, byte servicesId)
        {
            var patientList = profileService.GetPatientSelection(BranchId, StatusId, PaymentSourceId, "", true) ?? new List<PatientSelection>();
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllMedicare(Guid BranchId, byte StatusId, string InsuranceId, string Name)
        {
            var patientList = new List<PatientSelection>();
            byte insurance;
            if (byte.TryParse(InsuranceId, out insurance))
            {
                patientList = profileService.GetPatientSelectionByInsurance(BranchId, StatusId, insurance, Name, false) ?? new List<PatientSelection>(); ;
            }
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid patientId)
        {
            return PartialView("Charts/Data", mediatorService.GetScheduleCenterData(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MoreInfo(Guid id)
        {
            return PartialView("PatientPopup", mediatorService.GetScheduleCenterData(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid PatientId, string Discipline, string DateRangeId, DateTime? RangeStartDate, DateTime? RangeEndDate)
        {
            Check.Argument.IsNotNull(PatientId, "PatientId");
            Check.Argument.IsNotNull(Discipline, "discipline");
            Check.Argument.IsNotNull(DateRangeId, "dateRangeId");

            var dateRange = new DateRange { Id = DateRangeId };

            if (DateRangeId == "DateRange")
            {
                dateRange.EndDate = RangeEndDate.HasValue ? RangeEndDate.Value : DateTime.MinValue;
                dateRange.StartDate = RangeStartDate.HasValue ? RangeStartDate.Value : DateTime.MinValue;
            }
            else if (DateRangeId == "ThisEpisode" || DateRangeId == "LastEpisode" || DateRangeId == "NextEpisode")
            {
            }
            else
            {
                dateRange = DateServiceFactory<PatientEpisode>.GetDateRange(DateRangeId, PatientId);
            }
            return Json(scheduleService.GetPatientScheduleEventViewData(PatientId, Discipline, dateRange));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivitySort(Guid PatientId, string Discipline, string DateRangeId, DateTime? RangeStartDate, DateTime? RangeEndDate)
        {
            Check.Argument.IsNotNull(PatientId, "patientId");
            Check.Argument.IsNotNull(Discipline, "discipline");
            Check.Argument.IsNotNull(DateRangeId, "dateRangeId");

            var dateRange = new DateRange { Id = DateRangeId };

            if (DateRangeId == "DateRange")
            {
                dateRange.EndDate = RangeEndDate.HasValue ? RangeEndDate.Value : DateTime.MinValue;
                dateRange.StartDate = RangeStartDate.HasValue ? RangeStartDate.Value : DateTime.MinValue;
            }
            else if (DateRangeId == "ThisEpisode" || DateRangeId == "LastEpisode" || DateRangeId == "NextEpisode")
            {
            }
            else
            {
                dateRange = DateServiceFactory<PatientEpisode>.GetDateRange(DateRangeId, PatientId);
            }
            var viewData = scheduleService.GetPatientScheduleEventViewData(PatientId, Discipline, dateRange);
            return View(new GridModel(viewData.ScheduleEvents ?? new List<GridTask>()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Profile(Guid id)
        {
            return FileGenerator.PreviewPdf(new PatientProfilePdf(mediatorService.GetProfileWithEpisodeInfo(id)), "PatientProfile", Response);
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ProfilePdf(Guid id)
        {
            return FileGenerator.Pdf(new PatientProfilePdf(mediatorService.GetProfileWithEpisodeInfo(id)), "PatientProfile");
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Triage(Guid id)
        {
            return FileGenerator.PreviewPdf(new TriageClassificationPdf(mediatorService.GetProfileWithEpisodeInfo(id)), "TriageClassification", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TriagePdf(Guid id)
        {
            return FileGenerator.Pdf(new TriageClassificationPdf(mediatorService.GetProfileWithEpisodeInfo(id)), "TriageClassification");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Info(Guid patientId)
        {
            if (patientId.IsEmpty())
            {
                return PartialView("Charts/Info", new Patient());
            }
            return PartialView("Charts/Info", profileService.GetPatientSnapshotInfo(patientId, true, true, false) ?? new Patient());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Verify(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            return PartialView("~/Views/Patient/Eligibility.ascx", profileService.VerifyEligibility(medicareNumber, lastName, firstName, dob, gender));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityReports(Guid patientId)
        {
            return View(new GridModel(profileService.GetMedicareEligibilityLists(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityList(Guid patientId)
        {
            return PartialView("~/Views/Patient/EligibilityList.ascx", patientService.GetPatientOnly(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibility(Guid medicareEligibilityId, Guid patientId)
        {
            return PartialView("~/Views/Patient/Eligibility.ascx", profileService.GetMedicareEligibility(patientId, medicareEligibilityId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MedicareEligibilityPdf([Bind]PatientEligibility Eligibility)
        {
            return FileGenerator.Pdf<MedicareEligibilityPdf>(profileService.MedicareEligibilityReportPdf(Eligibility), "MedicareEligibility");
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MedicareEligibilityReportPdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<MedicareEligibilityPdf>(profileService.MedicareEligibilityReportPdf(patientId, eventId), "MedicareEligibility");
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicareEligibilityReportPrint(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(profileService.MedicareEligibilityReportPdf(patientId, eventId), "MedicareEligibility",Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Pending(Guid Id)
        {
            return PartialView("Admit/HomeHealthAdmitContent", profileService.GetProfileOnly(Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Grid()
        {
            var viewData = profileService.GetPatientsViewData(Guid.Empty,true, (int)PatientStatus.Active);
            if (viewData != null)
            {
                viewData.SortColumn = "DisplayName";
                viewData.SortDirection = "ASC";
            }
            return PartialView("List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid BranchId, int Status, string SortParams)
        {
            var viewData = profileService.GetPatientsViewData(BranchId,false, Status);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("ListContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingGrid()
        {
            return PartialView("Pending", profileService.GetServiceAndLocationViewData(ParentPermission.Patient, PermissionActions.ViewList, new List<PermissionActions> { PermissionActions.Add, PermissionActions.Export },true));
        }

        [GridAction]
        public ActionResult PendingList(Guid BranchId)
        {
            return View(new GridModel(profileService.GetPendingPatients(BranchId,false)));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonAdmitGrid()
        {
            return PartialView("NonAdmit/List", profileService.GetServiceAndLocationViewData(ParentPermission.Patient, PermissionActions.ViewList, new List<PermissionActions> { PermissionActions.Add, PermissionActions.Export }, false));
        }

        [GridAction]
        public ActionResult NonAdmitList()
        {
            return PartialView(new GridModel(profileService.GetNonAdmits(false) ?? new List<NonAdmit>()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DeletedPatientGrid()
        {
            var viewData = profileService.GetDeletedPatientViewData(Guid.Empty,true, false);
            if (viewData != null)
            {
                viewData.SortColumn = "DisplayName";
                viewData.SortDirection = "ASC";
            }
            return View("Deleted/List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedPatientContent(Guid BranchId, string SortParams)
        {
            var viewData = profileService.GetDeletedPatientViewData(BranchId,false, false);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return View("Deleted/ListContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Admit([Bind(Prefix = "profile[1]")]Profile profile)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            var workflow = new AdmitWorkflow(profile, AgencyServices.HomeHealth);
            if (workflow.IsCommitted)
            {
                viewData.isSuccessful = true;
                viewData.IsCenterRefresh = true;
                viewData.IsPatientListRefresh = true;
                viewData.PatientStatus = (int)PatientStatus.Active;
                viewData.IsReferralListRefresh = false;
                viewData.IsNonAdmitPatientListRefresh = false;
                viewData.IsPendingPatientListRefresh = true;
                viewData.errorMessage = "Patient successfully admitted.";
            }
            else
            {
                viewData.errorMessage = workflow.Message;
            }
            return Json(viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateStatus([Bind] PendingPatient patient)
        {
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "The patient status could not be changed. Please try again.";
            if (!patient.Id.IsEmpty())
            {
                patient.AgencyId = Current.AgencyId;
                if (patient.Status == (int)PatientStatus.Active)
                {
                    viewData = profileService.ActivatePatient(patient.Id);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully activated.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Discharged)
                {
                    //TODO:fix the patient discahrge
                    viewData = profileService.DischargePatient(patient.Id, patient.DateOfDischarge, patient.ReasonId, patient.Comments, Guid.Empty);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully discharged.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Pending)
                {
                    viewData = profileService.SetPatientPending(patient.Id);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to pending successfully.";
                    }
                }
                if (patient.Status == (int)PatientStatus.NonAdmission)
                {
                    viewData = profileService.NonAdmitPatient(patient);
                    if (viewData.isSuccessful)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to non-admit successfully.";
                    }
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Readmit(Guid patientId)
        {
            return PartialView(new PatientNameViewData(){ PatientId = patientId, Service = AgencyServices.HomeHealth });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientReadmit(Guid PatientId, DateTime ReadmissionDate)
        {
            var viewData = profileService.ActivatePatient(PatientId, ReadmissionDate);
            if (viewData.isSuccessful)
            {
                viewData.errorMessage = "Patient Re-admission is successful.";
            }
            else
            {
                viewData.errorMessage = "Patient Re-admission is unsuccessful.";
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Edit(Guid patientId)
        //{
        //    return PartialView("~/Views/Patient/Edit.ascx", profileService.GetPatientWithProfile(patientId));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Information(Guid patientId)
        //{
        //    return PartialView("~/Views/Patient/Edit/Information.ascx", profileService.GetPatientWithProfile(patientId));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid PatientId)
        {
            return PartialView("~/Views/Patient/Edit/HomeHealth.ascx", profileService.GetProfileOnly(PatientId));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult EditPatientProfile(Guid patientId)
        //{
        //    var profile = profileService.GetProfileOnly(patientId);
        //    if (profile != null)
        //    {
        //        profile.IsUnloadButtonVisible = true;
        //    }
        //    return PartialView("~/Views/Patient/Edit/HomeHealth.ascx", profile);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind(Prefix = "profile[1]")]Profile profile)
        {
            return Json(profileService.EditProfile(profile));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AuthorizationPdf(Guid patientId, Guid id)
        {
            return FileGenerator.Pdf<AuthorizationPdf>(profileService.GetAuthorizationPdf(patientId, id), "Auth");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult HospitalizationLogPdf(Guid patientId, Guid hospitalizationLogId)
        {
            return FileGenerator.Pdf<HospitalizationLogPdf>(new HospitalizationLogPdf(profileService.GetHospitalizationLogPrint(patientId, hospitalizationLogId)), "HospitalizationLog");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Status(Guid patientId)
        {
            return PartialView(profileService.GetProfileOnly(patientId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllInsurancePatient(Guid BranchId, byte StatusId, string Name, int InsuranceId)
        {
            var patientList = profileService.GetPatientSelectionByInsurance(BranchId, StatusId, InsuranceId, Name, false) ?? new List<PatientSelection>();
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(Guid branchId, int statusId)
        {
            return Json(profileService.GetPatientSelectionList(branchId, statusId));
        }

        #region Medication Profile

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult DrugDrugInteractionsPdf(Guid patientId, List<string> drugsSelected)
        {
            return FileGenerator.Pdf<DrugDrugInteractionsPdf>(new DrugDrugInteractionsPdf(profileService.GetDrugDrugInteractionsPrint(patientId, drugsSelected)), "DrugDrugInteractions");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicationProfile, PermissionActions.ViewList)]
        public ActionResult MedicationProfile(Guid patientId)
        {
            return PartialView("~/Views/Patient/MedicationProfile/Profile.ascx", mediatorService.GetMedicationProfileViewDataWithDiagnosis(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicationProfile, PermissionActions.Sign)]
        public ActionResult MedicationProfileSnapShot(Guid patientId)
        {
            return PartialView("~/Views/Patient/MedicationProfile/Sign.ascx", mediatorService.GetMedicationProfileViewDataWithDiagnosisForSign(patientId));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicationProfile, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationProfilePrint(Guid id)
        {
            return FileGenerator.PreviewPdf<MedProfilePdf>(new MedProfilePdf(mediatorService.GetMedicationProfilePrint(id)), "MedProfile", Response);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicationProfile, PermissionActions.Print)]
        public FileResult MedicationProfilePdf(Guid id)
        {
            return FileGenerator.Pdf<MedProfilePdf>(new MedProfilePdf(mediatorService.GetMedicationProfilePrint(id)), "MedProfile");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicationProfile, PermissionActions.Print)]
        public FileResult MedicationProfileSnapshotPdf(Guid id)
        {
            return FileGenerator.Pdf<MedProfilePdf>(new MedProfilePdf(profileService.GetMedicationSnapshotPrint(id)), "MedProfile");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.MedicationProfile, PermissionActions.Print)]
        public ActionResult MedicationProfileSnapshotPrint(Guid id)
        {
            return FileGenerator.PreviewPdf<MedProfilePdf>(new MedProfilePdf(profileService.GetMedicationSnapshotPrint(id)), "MedProfile", Response);
        }

        #endregion
    }
}
