﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Helpers;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PlanOfCareController : BaseController
    {

        #region Constructor / Member

        private readonly IPatientService patientService;
        private readonly IUserService userService;
        private readonly HHPlanOfCareService planOfCareService;
        private readonly HHMediatorService mediatorService;


        public PlanOfCareController(IPatientService patientService,
            IUserService userService,
            HHPlanOfCareService planOfCareService,
            HHMediatorService mediatorService)
        {

            this.userService = userService;
            this.patientService = patientService;
            this.planOfCareService = planOfCareService;
            this.mediatorService = mediatorService;
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid patientId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return View("Edit", planOfCareService.GetPlanOfCareWithPatientAndAgencyInfo(patientId, id) ?? new PlanofCareViewData ());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Content(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            ViewData["Service"] = AgencyServices.HomeHealth;
            return PartialView("LocatorQuestions", mediatorService.GetPlanOfCareWithAssessment(patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public JsonResult Save(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false };
            var errorMessage = "The Plan of Care could not be saved.";
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();
                if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !ServiceHelper.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                        return Json(viewData);
                    }
                    if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                        return Json(viewData);
                    }
                }
                viewData = planOfCareService.UpdatePlanofCare(formCollection);
                if (!viewData.isSuccessful)
                {
                    viewData.errorMessage = errorMessage;
                }
            }
            else
            {
                viewData.errorMessage = errorMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, new ParentPermission[] { ParentPermission.Schedule, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new PlanOfCarePdf(planOfCareService.GetPlanOfCarePrint(patientId, eventId)), "PlanOfCare", Response);
        }
       
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview( Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new PlanOfCarePdf(planOfCareService.GetPlanOfCarePrint(patientId, eventId)), "PlanOfCare", Response);
        }
        
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf(new PlanOfCarePdf(planOfCareService.GetPlanOfCarePrint(patientId, eventId)), "PlanOfCare");
        }

       
    }
}
