﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Telerik.Web.Mvc;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.ViewData;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReferralController : BaseController
    {
        #region Private Members/Constructor

        private readonly IReferralService referralService;
        private readonly HHPatientProfileService profileService;

        public ReferralController( IReferralService referralService, HHPatientProfileService profileService)
        {
            this.referralService = referralService;
            this.profileService = profileService;
        }

        #endregion

        #region ReferralController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Get(Guid id)
        {
            return Json(referralService.Get(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = new GridViewData();
            var actions = new int[] { (int)PermissionActions.ViewList, (int)PermissionActions.Add, (int)PermissionActions.Export };
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Referral, actions);
            if (allPermission.IsNotNullOrEmpty())
            {
                actions.ForEach(a =>
                {
                    var permission = allPermission.GetOrDefault<int, AgencyServices>((int)a, AgencyServices.None);
                    if (a == (int)PermissionActions.Add)
                    {
                        viewData.NewPermissions = permission;
                    }
                    if (a == (int)PermissionActions.Export)
                    {
                        viewData.ExportPermissions = permission;
                    }
                    if (a == (int)PermissionActions.ViewList)
                    {
                        viewData.ViewListPermissions = permission;
                        viewData.AvailableService = permission;
                    }
                });
                viewData.Service = viewData.AvailableService.GetTheFirstIfThePreferredNotIncluded(Current.PreferredService);
            }
            return PartialView("List", viewData);
        }

        [GridAction]
        public ActionResult Grid(int ServiceId)
        {
            if (ServiceId > 0 && Enum.IsDefined(typeof(AgencyServices), ServiceId))
            {
                return View(new GridModel(referralService.GetPending(Current.AgencyId, (AgencyServices)ServiceId)));
            }
            else
            {
                return View(new GridModel(new List<Referral>()));
            }
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var viewData = new ServiceAndGuidViewData(Current.AcessibleServices);
            viewData.IsUserCanAddPhysicain = Current.HasRight(viewData.Service, ParentPermission.Physician, PermissionActions.Add);
            return PartialView(viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add([Bind]Referral referral)
        {
            Check.Argument.IsNotNull(referral, "referral");
            return Json(referralService.AddReferral(referral));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAdmit(Guid id)
        {
            Check.Argument.IsNotNull(id, "id");
            return PartialView("Admit/Main", referralService.GetReferralForNewOrExisitngPatient(id));//patientService.GetPatientForPending(id)
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNonAdmit(Guid referralId)
        {
            return PartialView("NonAdmit", referralService.GetForNonAdmit(referralId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNonAdmit(Guid Id, List<string> ServiceProvided, [Bind(Prefix = "profile")] List<NonAdmit> profile)
        {
            return Json(referralService.NonAdmitReferral(Id, ServiceProvided, profile));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView("Edit", referralService.GetReferralWithEmergencyContact(id));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ReferralPdf(Guid id)
        {
            return FileGenerator.Pdf<ReferralPdf>(referralService.GetReferralPdf(id), "Referral");
        }

        [GridAction]
        public ActionResult GetPhysicians(Guid ReferralId)
        {
            Check.Argument.IsNotEmpty(ReferralId, "ReferralId");
            return Json(new GridModel(referralService.GetReferalPhysicians(ReferralId)));
        }
    
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddPhysician(Guid id, Guid referralId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(referralId, "referralId");
            return Json(referralService.AddReferralPhysician(id, referralId));
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePhysician(Guid id, Guid referralId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(referralId, "referralId");
            return Json(referralService.DeleteReferralPhysician(id, referralId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetPrimaryPhysician(Guid id, Guid referralId) {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(referralId, "referralId");
            return Json(referralService.SetPrimaryPhysician(id, referralId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Referral referral) {
            Check.Argument.IsNotNull(referral, "referral");
            return Json(referralService.Update(referral));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteView(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return PartialView("DeleteContent", referralService.GetForDelete(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid Id, List<string> ServiceProvided)
        {
            Check.Argument.IsNotEmpty(Id, "id");
            return Json(referralService.Delete(Id, ServiceProvided));
        }

      
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReferralLogs(Guid referralId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.Referral, Current.AgencyId, referralId.ToString()));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Verify(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        //{
        //    return PartialView("Eligibility", patientService.VerifyEligibility(medicareNumber, lastName, firstName, dob, gender));
        //}

        #endregion
    }
}
