﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;
    
    using iTextExtension;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class FaceToFaceEncounterController: BaseController
    {
        #region Constructor

        private readonly FaceToFaceEncounterService faceToFaceEncounterService;

        public FaceToFaceEncounterController(FaceToFaceEncounterService faceToFaceEncounterService)
        {
            Check.Argument.IsNotNull(faceToFaceEncounterService, "faceToFaceEncounterService");
            this.faceToFaceEncounterService = faceToFaceEncounterService;
        }

        #endregion

        #region Face To Face Encounter

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.HomeHealth);
            viewData.IsUserCanAdd = Current.HasRight(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.Add);
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind]FaceToFaceEncounter faceToFaceEncounter)
        {
            Check.Argument.IsNotNull(faceToFaceEncounter, "faceToFaceEncounter");
            return Json(faceToFaceEncounterService.CreateFaceToFaceEncounterBeforePhysicianInfoSet(faceToFaceEncounter));
        }

        [PermissionFilter(AgencyServices.HomeHealth, new ParentPermission[] { ParentPermission.Orders, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new PhysFaceToFacePdf(faceToFaceEncounterService.GetFaceToFacePrint(patientId, eventId)), "FaceToFace", Response);
        }


        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(new PhysFaceToFacePdf(faceToFaceEncounterService.GetFaceToFacePrint(patientId, eventId)), "FaceToFace", Response);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf(new PhysFaceToFacePdf(faceToFaceEncounterService.GetFaceToFacePrint(patientId, eventId)), "FaceToFace");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Blank()
        {
            return FileGenerator.Pdf(new PhysFaceToFacePdf(faceToFaceEncounterService.GetFaceToFacePrint()), "FaceToFace");
        }

        #endregion

    }
}
