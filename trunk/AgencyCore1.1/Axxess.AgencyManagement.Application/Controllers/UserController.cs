﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Security;
    using Services;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    using Telerik.Web.Mvc;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Application.Helpers;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IMembershipService membershipService;

        public UserController(IUserService userService, IMembershipService membershipService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(membershipService, "membershipService");

            this.userService = userService;
            this.membershipService = membershipService;
            this.agencyService = agencyService;
        }

        #endregion

        #region UserController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView("NewNew", userService.IsAgencyHaveMoreAccount(agencyService.MaxAgencyUserCount()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] User user, [ModelBinder(typeof(CommaSeparatedModelBinder))]List<int> ServiceArray)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error saving the user." };
            if (ServiceArray.IsNotNullOrEmpty())
            {
                var service = 0;
                ServiceArray.ForEach(s =>
                {
                    service |= s;
                });
                user.AccessibleServices = (AgencyServices)service;
                viewData = userService.Add(user, agencyService.MaxAgencyUserCount());
            }
            else
            {
                viewData.errorMessage = "User service is not identified.";
            }

            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Add([Bind] User user, [ModelBinder(typeof(CommaSeparatedModelBinder))]List<int> ServiceArray)
        //{
        //    Check.Argument.IsNotNull(user, "user");
        //    user.AccessibleServices = (AgencyServices)ServiceArray.Sum();
        //    return Json(userService.Add(user, agencyService.MaxAgencyUserCount()));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(userService.GetUserWithProfileOnly(Id));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult EditSection(Guid id, String category)
        //{
        //    var cat = category.IsNotNullOrEmpty() ? category.Split('_').Last() : string.Empty;
        //    return PartialView("Edit/" + cat, userService.GetUserWithProfilePermissionsAndLicenses(id));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Information(Guid UserId)
        {
            return PartialView("Edit/Information", userService.GetUserWithProfileOnly(UserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Permissions(Guid UserId)
        {
            return PartialView("Edit/Permissions", userService.GetUserPermissionsOnly(UserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Licenses(Guid UserId)
        {
            return PartialView("Edit/Licenses", userService.GetUserLicensesOnly(UserId));
        }

        public ActionResult Rates(Guid UserId)
        {
            var viewData = new UserPayRatesViewData() { UserId = UserId, PayorRates = userService.GetUserRatesOnly(UserId) };
            return View("Edit/PayRates", viewData);
        }

        public ActionResult RateList(Guid userId)
        {
            return View("Rate/List", userService.GetUserRatesOnly(userId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] User user, [ModelBinder(typeof(CommaSeparatedModelBinder))]List<int> ServiceArray)
        {
            Check.Argument.IsNotNull(user, "user");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error saving the user." };
            if (ServiceArray.IsNotNullOrEmpty())
            {
                var service = 0;
                ServiceArray.ForEach(s =>
                {
                    service |= s;
                });
                user.AccessibleServices = (AgencyServices)service;
                viewData = userService.Update(user);
            }
            else
            {
                viewData.errorMessage = "User service is not identified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePermissions(UserPermissionInput permissionInputs)//( Guid UserId, int[] Services, [Bind(Prefix = "Permissions")]IDictionary<int, Dictionary<int, List<int>>> Permissions, [Bind(Prefix = "ServiceLocations")]List<UserLocationService> ServiceLocations)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Permissions could not be updated." };
            if (permissionInputs != null)
            {
                if (!permissionInputs.UserId.IsEmpty())
                {
                    if (userService.UpdatePermissions(permissionInputs))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Permissions updated successfully";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deactivate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            return Json(userService.SetUserStatus(userId, (int)UserStatus.Inactive));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            return Json(userService.Activate(userId, agencyService.MaxAgencyUserCount()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deleted. Try Again." };
            if (userService.DeleteUser(userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Profile()
        {
            return PartialView("Profile/Edit", userService.GetUserWithProfilePermissionsAndLicenses(Current.UserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Profile([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your profile could not be saved." };

            if (user.IsValid())
            {
                if (user.PasswordChanger.NewPassword.IsNotNullOrEmpty() && !userService.IsPasswordCorrect(user.Id, user.PasswordChanger.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The password provided does not match the one on file.";
                }
                else
                {
                    if (user.SignatureChanger.NewSignature.IsNotNullOrEmpty() && !ServiceHelper.IsSignatureCorrect(user.Id, user.SignatureChanger.CurrentSignature))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature provided does not match the one on file.";
                    }
                    else
                    {
                        if (userService.UpdateProfile(user))
                        {

                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your profile has been updated successfully.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult All()
        //{
        //    return Json(userService.GetUserSelection());
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Grid()
        {
            var viewData = userService.GetUsersListViewData(Guid.Empty, (int)UserStatus.Active);
            if (viewData.Users.IsNotNullOrEmpty())
            {
                viewData.SortColumn = "PatientName";
                viewData.SortDirection = "ASC";
            }

            return PartialView("List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(int status, string sortParams)
        {
            var viewData = userService.GetUsersListViewData(Guid.Empty, status);
            if (sortParams.IsNotNullOrEmpty())
            {
                var paramArray = sortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(viewData);
        }

       

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotSignature()
        {
            return PartialView("Signature/Forgot", Current.User.Name);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult NewLicenseItem()
        //{
        //    return PartialView("License/New");
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GoToMeeting()
        {
            return PartialView("Help/GoToMeeting", string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Webinars()
        {
            return PartialView("Help/Webinars", userService.GetUserWithProfilePermissionsAndLicenses(Current.UserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmailSignature()
        {
            var viewData = new JsonViewData();

            if (membershipService.ResetSignature(Current.LoginId))
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Signature could not be reset. Please try again later.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserLogs(Guid userId)
        {
            return PartialView("ActivityLogs", Auditor.GetGeneralLogs(LogDomain.Agency, LogType.User, Current.AgencyId, userId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateNew(Guid userId, int? InsuranceId)
        {
            return PartialView("Rate/New", new UserRate { UserId = userId, Insurance = InsuranceId.HasValue ? InsuranceId.Value : 0 });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RateAdd([Bind]UserRate userRate)
        {
            return Json(userService.AddUserRate(userRate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateEdit(Guid userId, int Id, int insurance)
        {
            return PartialView("Rate/Edit", userService.GetUserRateForEdit(userId, insurance, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RateUpdate([Bind] UserRate userRate)
        {
            return Json(userService.UpdateUserRate(userRate));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult LoadUserRate(Guid fromId, Guid toId)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to load rate. Please try again." };
        //    if (userService.LoadUserRate(fromId, toId))
        //    {
        //        viewData.isSuccessful = true;
        //        viewData.errorMessage = "User rate loaded successfully";
        //    }
        //    return Json(viewData);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateLoad(Guid userId)
        {
            return PartialView("Rate/Load/Main", userId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateLoadContent(Guid userId)
        {
            return PartialView("Rate/Load/Content", userService.GetUserRatesOnly(userId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLoadUserRate(Guid ToUserId, Guid FromUserId, [Bind(Prefix = "Payors")] List<PayorTaskInput> payors)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to load rate. Please try again." };
            if (userService.LoadUserRate(FromUserId, ToUserId, payors))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User rate loaded successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateDelete(Guid userId, int insurance, int id)
        {
            return Json(userService.DeleteUserRate(userId, insurance, id));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult SelectList(Guid BranchId, int Status, int Service)
        //{
        //    return Json(userService.UserSelectList(BranchId,Status,Service));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(Guid branchId, int statusId, int service)
        {
            return Json(userService.GetUserSelectionList(branchId, statusId, service).Select(u => new { u.Id, Name = u.DisplayName }));
        }

        #endregion
    }
}
