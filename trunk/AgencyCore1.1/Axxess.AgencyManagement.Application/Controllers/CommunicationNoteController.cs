﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class CommunicationNoteController : BaseController
    {
        #region Constructor

        private readonly HHCommunicationNoteService communicationNoteService;
        private readonly HHMediatorService mediatorService;

        public CommunicationNoteController(
            HHCommunicationNoteService communicationNoteService,
            HHMediatorService mediatorService)
        {
            Check.Argument.IsNotNull(communicationNoteService, "communicationNoteService");
            Check.Argument.IsNotNull(mediatorService, "mediatorService");


            this.communicationNoteService = communicationNoteService;
            this.mediatorService = mediatorService;
        }

        #endregion

        #region Communication Note

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.HomeHealth);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.HomeHealth, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.HomeHealth, ParentPermission.CommunicationNote, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId)
        {
            var viewData = new ServiceAndGuidViewData(AgencyServices.HomeHealth, patientId);
            var permission = Current.Permissions;
            viewData.IsUserCanAddPhysicain = permission.IsInPermission(AgencyServices.HomeHealth, ParentPermission.Physician, PermissionActions.Add);
            viewData.IsUserCanAdd = permission.IsInPermission(AgencyServices.HomeHealth, ParentPermission.CommunicationNote, PermissionActions.Add);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            return Json(mediatorService.AddCommunicationNote(communicationNote));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(communicationNoteService.GetCommunicationNote(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return PartialView(communicationNoteService.GetCommunicationNoteEdit(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            return Json(mediatorService.UpdateCommunicationNote(communicationNote));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = communicationNoteService.GetCommunicationNotesViewData(Guid.Empty, true, Guid.Empty, Guid.Empty, (int)PatientStatus.Active, DateTime.Now.AddDays(-60), DateTime.Now);
            viewData.SortColumn = "DisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(Guid PatientId)
        {
            var viewData = communicationNoteService.GetCommunicationNotesViewData(Guid.Empty, true, PatientId, null, 0, DateTime.Now.AddDays(-60), DateTime.Now);
            viewData.SortColumn = "UserDisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid PatientId, Guid? EpisodeId, Guid BranchId, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {

            var viewData = communicationNoteService.GetCommunicationNotesViewData(BranchId, false, PatientId, EpisodeId, Status, StartDate, EndDate);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid patientId, Guid Id)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(communicationNoteService.ToggleCommunicationNote(patientId, Id, true));
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult CommunicationNotes(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        //{
        //    return View(new GridModel(communicationNoteService.GetCommunicationNotes(BranchId, Status, StartDate, EndDate)));
        //}

        [PermissionFilter(AgencyServices.HomeHealth, new ParentPermission[] { ParentPermission.CommunicationNote, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<ComNotePdf>(new ComNotePdf(communicationNoteService.GetCommunicationNotePrint(eventId, patientId)), "CommunicationNote", Response);
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.CommunicationNote, PermissionActions.Print)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf<ComNotePdf>(new ComNotePdf(communicationNoteService.GetCommunicationNotePrint(eventId, patientId)), "CommunicationNote", Response);
        }

        [FileDownload]
        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.CommunicationNote, PermissionActions.Print)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<ComNotePdf>(new ComNotePdf(communicationNoteService.GetCommunicationNotePrint(eventId, patientId)), "CommunicationNote");
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your communication note could not be approved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = communicationNoteService.ProcessCommunicationNotes(patientId, eventId, "Approve", null);
                if (viewData.isSuccessful)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your communication note has been successfully approved.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(Guid patientId, Guid eventId, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your communication note could not be returned." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = communicationNoteService.ProcessCommunicationNotes(patientId, eventId, "Return", reason);
                if (viewData.isSuccessful)
                {
                    viewData.errorMessage = "Your communication note has been successfully returned.";
                }
            }
            return Json(viewData);
        }


        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult PatientCommunicationNotesView(Guid patientId)
        //{
        //    return PartialView("CommunicationList", patientService.GetPatientOnly(patientId));
        //}

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]

        //public ActionResult PatientCommunicationNotes(Guid patientId)
        //{
        //    return View(new GridModel(communicationNoteService.GetCommunicationNotes(patientId)));
        //}

        #endregion
    }
}
