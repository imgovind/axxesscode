﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;
    using System.Collections.Generic;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Exports;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PrintQueueController : BaseController
    {
        #region Constructor

        private readonly HHTaskService taskService;
        private readonly IAgencyService agencyService;


        public PrintQueueController(IAgencyService agencyService, HHTaskService taskService)
        {
            Check.Argument.IsNotNull(taskService, "taskService");
            Check.Argument.IsNotNull(agencyService, "agencyService");

            this.taskService = taskService;
            this.agencyService = agencyService;
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var viewData = taskService.GetPrintQueue(Guid.Empty, true, DateTime.Now.AddDays(-7), DateTime.Now, true);
            viewData.GroupName = "EventDate";
            viewData.SortColumn = "DisplayName";
            viewData.SortDirection = "ASC";
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Content(Guid branchId, DateTime startDate, DateTime endDate, string groupName, string SortParams)
        {
            var viewData = taskService.GetPrintQueue(branchId, false, startDate, endDate, true);
            if (groupName.IsNullOrEmpty())
            {
                groupName = "EventDate";
            }
            viewData.GroupName = groupName;
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    viewData.SortColumn = paramArray[0];
                    viewData.SortDirection = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Export(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            string branchName = "All branches";
            if (BranchId != Guid.Empty)
            {
                branchName = LocationEngine.GetName(BranchId, Current.AgencyId);
            }
            var export = new PrintQueueExporter(branchName, taskService.GetPrintQueue(BranchId, StartDate, EndDate,false).OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PrintQueue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkAsPrinted(List<Guid> Id)
        {
            return Json(taskService.MarkScheduleTasksAsPrinted(Id));
        }
    }
}
