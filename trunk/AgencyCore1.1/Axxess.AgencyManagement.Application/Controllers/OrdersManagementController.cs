﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using Telerik.Web.Mvc;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Enums;
    using Axxess.AgencyManagement.Application.Filter;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.AgencyManagement.Application.ViewData;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OrdersManagementController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly HHEpiosdeService episodeService;
        private readonly HHOrderManagmentService orderManagmentService;

        public OrdersManagementController(IPatientService patientService, HHEpiosdeService episodeService, HHOrderManagmentService orderManagmentService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(episodeService, "episodeService");
            Check.Argument.IsNotNull(orderManagmentService, "orderManagmentService");
            this.patientService = patientService;
            this.episodeService = episodeService;
            this.orderManagmentService = orderManagmentService;
           
        }

        #endregion

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ViewOrderToBeSent)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ToBeSent()
        {
            var viewData = orderManagmentService.GetOrdersGridViewData(PermissionActions.ViewOrderToBeSent, true);
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ViewOrderToBeSent)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ToBeSentContent(Guid branchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(orderManagmentService.GetOrdersToBeSent(branchId, sendAutomatically, startDate, endDate).Select(s => new { s.Id, s.PatientId, s.EpisodeId, s.PatientName, s.Text, s.PhysicianName, s.CanUserPrint, s.DocumentType, s.Service, s.CreatedDate, s.Number, s.Type })));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ViewOrderHistory)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult History()
        {
            var viewData = orderManagmentService.GetOrdersGridViewData(PermissionActions.ViewOrderHistory, true);
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ViewOrderHistory)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryList(Guid branchId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(orderManagmentService.GetOrdersByStatus(branchId, startDate, endDate, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature }, false, true).Select(s => new { s.Id, s.PatientId, s.EpisodeId, s.PatientName, s.Text, s.PhysicianName, s.SendDate, s.ReceivedDate, s.PhysicianSignatureDate, s.PhysicianAccess, s.Service, s.CreatedDate, s.Number, s.Type, s.CanUserEdit })));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.SendToPhysician)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkAsSent(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            return Json(orderManagmentService.MarkOrdersAsSent(formCollection));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ReceiveFromPhysician)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReceiveOrder(Guid id, Guid patientId, string type)
        {
            return PartialView(orderManagmentService.GetOrderForReceiving(patientId, id, type));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ReceiveFromPhysician)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkAsReturned(Guid id, Guid patientId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            return Json(orderManagmentService.MarkOrderAsReturned(patientId, id, type, receivedDate, physicianSignatureDate));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryEdit(Guid id, Guid patientId, string type)
        {
            return PartialView(orderManagmentService.GetOrderForHistoryEdit(patientId, id, type));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Order order, OrderType type)
        {
            var viewData = Validate<JsonViewData>(
                    new Validation(() => order.Id.IsEmpty() || order.PatientId.IsEmpty() || order.EpisodeId.IsEmpty(), "Order could not be updated. Please Try again."),
                    new Validation(() => !order.ReceivedDate.IsValid(), "Received date must be a valid date."),
                    new Validation(() => !order.SendDate.IsValid(), "Send date must be a valid date."),
                    new Validation(() => !order.PhysicianSignatureDate.IsValid(), "Physician Signature date must be a valid date.")
                );
            if (viewData.isSuccessful)
            {
                return Json(orderManagmentService.UpdateOrderDates(order.Id, order.PatientId, type, order.ReceivedDate, order.SendDate, order.PhysicianSignatureDate));
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ViewOrderSent)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingSignature()
        {
            var viewData = orderManagmentService.GetOrdersGridViewData(PermissionActions.ViewOrderSent, true);
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.OrderManagement, PermissionActions.ViewOrderSent)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingSignatureContent(Guid branchId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(orderManagmentService.GetOrdersByStatus(branchId, startDate, endDate, ScheduleStatusFactory.OrdersPendingPhysicianSignature(), true, false).Select(s => new { s.Id, s.PatientId, s.PatientName, s.Text, s.PhysicianName, s.SendDate, s.Service, s.CreatedDate, s.Number, s.Type, s.CanUserEdit })));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Approve(Guid eventId, Guid patientId, string orderType)
        {
            return Json(orderManagmentService.UpdatePhysicianOrderAndPOCStatus(eventId, patientId, orderType, "Approve", null));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Return(Guid eventId, Guid patientId, string orderType, string reason)
        {
            return Json(orderManagmentService.UpdatePhysicianOrderAndPOCStatus(eventId, patientId, orderType, "Return", reason));
        }

        #region Patient Order History

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHistory(Guid patientId)
        {
            var viewData = new GridViewData { Service = AgencyServices.HomeHealth, Id = patientId, AvailableService = Current.AcessibleServices };
            viewData.DisplayName = patientService.GetPatientNameById(patientId);
            var actions = new int[] { (int)PermissionActions.Export };
            var allPermission = Current.CategoryService(Current.AcessibleServices, ParentPermission.Orders, actions);
            if (allPermission.IsNotNullOrEmpty())
            {
                viewData.EditPermissions = allPermission.GetOrDefault<int, AgencyServices>((int)PermissionActions.Export, AgencyServices.None);
            }
            return PartialView(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Orders, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHistoryGrid(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(orderManagmentService.GetPatientOrders(patientId, startDate, endDate)));
        }

        #endregion

        #region Patient Episode Order History

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeOrdersView(Guid episodeId, Guid patientId)
        {
            return PartialView("EpisodeOrders", episodeService.GetPatientEpisodeFluent(episodeId, patientId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeOrdersContent(Guid episodeId, Guid patientId)
        {
            return View(new GridModel(orderManagmentService.GetEpisodeOrders(episodeId, patientId)));
        }

        #endregion

    }
}
