﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Application.Services;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class NonUserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;

        public NonUserController(IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            this.userService = userService;
        }

        #endregion

        #region UserController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(NonUser nonUser)
        {
            return Json(userService.AddNonUser(nonUser));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView(userService.GetNonUser(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(NonUser nonUser)
        {
            return Json(userService.UpdateNonUser(nonUser));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid id)
        {
            return Json(userService.DeleteNonUser(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var nonUsers = userService.GetNonUsers();
            if (nonUsers.Users.IsNotNullOrEmpty())
            {
                ViewData["SortColumn"] = "DisplayName";
                ViewData["SortDirection"] = "ASC";
            }

            return PartialView("List", nonUsers);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(string sortParams)
        {
            if (sortParams.IsNotNullOrEmpty())
            {
                var paramArray = sortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(userService.GetNonUsers());
        }

        #endregion
    }
}
