﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Helpers;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AssetController : BaseController
    {
        #region Private Members/Constructor


        public AssetController()
        {
        }

        #endregion

        #region AssetController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Serve(Guid assetId)
        {
            var asset = AssetHelper.Get(Current.AgencyId, assetId);
            return File(asset.Bytes, asset.ContentType, asset.FileName);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Delete(Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "This asset could not be deleted." };
            if (AssetHelper.Delete(assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The asset was successfully deleted.";
            }
            return Json(viewData);
        }
        
        #endregion
    }
}
