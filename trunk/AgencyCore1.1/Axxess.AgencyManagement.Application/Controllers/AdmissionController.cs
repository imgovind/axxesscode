﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;

    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Application.Services;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AdmissionController : BaseController
    {
          #region Constructor


        private readonly IPatientService patientService;
        private readonly HHPatientProfileService profileService;
        private readonly HHAdmissionService admissionService;
        private readonly HHEpiosdeService episodeService;

        public AdmissionController(IPatientService patientService, HHPatientProfileService profileService, HHAdmissionService admissionService, HHEpiosdeService episodeService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(profileService, "profileService");
            Check.Argument.IsNotNull(admissionService, "admissionService");
            Check.Argument.IsNotNull(episodeService, "epiosdeService");

            this.patientService = patientService;
            this.profileService = profileService;
            this.admissionService = admissionService;
            this.episodeService = episodeService;
        }


        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(Guid patientId)
        {
            var admissionListViewData = new AdmissionListViewData();
            var profile = profileService.GetProfileJsonByColumns(patientId, "Status", "AdmissionId", "FirstName", "LastName");
            if (profile != null)
            {

                if (!Current.AcessibleServices.IsAlone())
                {
                    admissionListViewData.PatientsServices = patientService.GetAccessibleServices(patientId);
                }
                else 
                {
                    admissionListViewData.PatientsServices = AgencyServices.HomeHealth;
                }
                admissionListViewData.DisplayName = profile.Get("LastName").ToUpperCase() + ", " + profile.Get("FirstName").ToUpperCase();
                admissionListViewData.AdmissionId = profile.Get("AdmissionId").ToGuid();
                admissionListViewData.IsPatientActive = profile.Get("Status").ToInteger() == (int)PatientStatus.Active;
            }
            admissionListViewData.PatientId = patientId;
            admissionListViewData.Service = AgencyServices.HomeHealth;
            return PartialView(admissionListViewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContent(Guid patientId)
        {
            return View(new GridModel(episodeService.GetPatientAdmissonPeriods(patientId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListContentDetails(Guid patientId, Guid admissionId)
        {
            return View(new GridModel(episodeService.GetPatientAdmissonPeriodEpisodes(patientId, admissionId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientInfo(Guid patientId, Guid id, string type)
        {
            var admissionPatient = new Patient();
            ViewData["IsDischarge"] = false;
            if (!patientId.IsEmpty() && type.IsNotNullOrEmpty())
            {
                admissionPatient = admissionService.GetAdmissionPatientInfo(patientId, id, type);
                if (admissionPatient != null)
                {
                    ViewData["IsDischarge"] = admissionPatient.Profile.IsDischarged;
                    admissionPatient.Profile.ServiceType = (int)AgencyServices.HomeHealth;
                }
            }
            ViewData["Type"] = type;
          
            return PartialView("AdmissionPatientInfo", admissionPatient);
        }

        public ActionResult New([Bind] Patient patient,[Bind] Profile profile)
        {
            return Json(admissionService.AdmissionPatientNew(patient,profile));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit([Bind] Patient patient, [Bind(Prefix = "profile")] Profile profile)
        {
            return Json(admissionService.AdmissionPatientEdit(patient,profile));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkAsCurrent(Guid patientId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient's admission could not be set active." };
            if (!patientId.IsEmpty() && !id.IsEmpty())
            {
                if (admissionService.MarkPatientAdmissionCurrent(patientId, id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The patient's admission period is set active. ";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Patient's admission could not be set active. ";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionDates(Guid patientId)
        {
            return Json(admissionService.GetPatientAdmissionDateSelectList(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid patientId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient's admission period could not be deleted." };
            if (!patientId.IsEmpty() && !id.IsEmpty())
            {
                viewData = admissionService.DeletePatientAdmission(patientId, id);
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Could not delete the patient's admission period. Try again.";
            }
            return Json(viewData);
        }
    }
}
