﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.IO;

    using Axxess.AgencyManagement.Application.Domain;

    using Telerik.Web.Mvc;
    using iTextExtension;

    using ViewData;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.Filter;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class NoteController : BaseController
    {
        private readonly IPatientService patientService;
        private readonly HHNoteService noteService;
        private readonly HHMediatorService mediatorService;

        public NoteController(IPatientService patientService, HHNoteService noteService, HHMediatorService mediatorService)
        {

            Check.Argument.IsNotNull(noteService, "scheduleService");
            Check.Argument.IsNotNull(mediatorService, "mediatorService");
            Check.Argument.IsNotNull(patientService, "patientService");
            this.patientService = patientService;
            this.noteService = noteService;
            this.mediatorService = mediatorService;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(Guid patientId, Guid Id)
        {
            var noteViewData = noteService.GetVisitNoteView(patientId, Id);
            if (noteViewData != null)
            {
                var contentPath = "~/Views/Error/FileNotFound.aspx";
                if (noteViewData.ContentPath.IsNotNullOrEmpty())
                {
                    contentPath = noteViewData.ContentPath;
                }
                noteViewData.ContentPath = "";
                return PartialView(contentPath, noteViewData);
            }
            return PartialView("~/Views/Error/FileNotFound.aspx", new ErrorPageViewData("The Note you are looking for is not Found. Close this window and try again."));
        }


        #region Note View, Content, Print

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Content(Guid patientId, Guid eventId, Guid previousNoteId, string type)
        {
            var noteViewData = noteService.GetVisitNoteForContent(patientId, eventId, previousNoteId, type);
            if (noteViewData != null)
            {
                var contentPath = "~/Views/Error/FileNotFound.aspx";
                if (noteViewData.ContentPath.IsNotNullOrEmpty())
                {
                    contentPath = noteViewData.ContentPath;
                }
                noteViewData.ContentPath = "";
                return PartialView(contentPath, noteViewData);
            }
            return PartialView("~/Views/Error/FileNotFound.aspx", new ErrorPageViewData("The Previous Note you are looking for to load is not found. Close this window and try again."));
        }


        [PermissionFilter(AgencyServices.HomeHealth, new ParentPermission[] { ParentPermission.Schedule, ParentPermission.QA }, new PermissionActions[] { PermissionActions.Print }, new PermissionActions[] { PermissionActions.Approve, PermissionActions.Return, PermissionActions.EditApproved })]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(noteService.GetVisitNotePrint(patientId, eventId), Response);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintPreview(Guid patientId, Guid eventId)
        {
            return FileGenerator.PreviewPdf(noteService.GetVisitNotePrint(patientId, eventId), Response);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print)]
        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid? patientId, Guid? eventId)
        {
            FileResult file = new FileStreamResult(new MemoryStream(), "application/pdf");
            var noteViewData = new VisitNoteViewData();
            if (patientId.HasValue && eventId.HasValue)
            {
                noteViewData = noteService.GetVisitNotePrint((Guid)patientId, (Guid)eventId) ?? new VisitNoteViewData();
                return FileGenerator.GetPrintFile(noteViewData, file);
            }
            return FileGenerator.GetPrintFile(noteViewData, file);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Blank(int type)
        {
            var viewData = noteService.GetVisitNotePrint(type) ?? new VisitNoteViewData();
            viewData.DisciplineTask = type;
            return FileGenerator.GetPrintFile(viewData, new FileStreamResult(new MemoryStream(), "application/pdf"));
        }

        #endregion

        #region Process Notes

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Notes(string button, FormCollection formCollection, SaveNoteArguments arguments, VitalSignLog vitalSignLog)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            return Json(noteService.ProcessNoteAction(button, formCollection, arguments, vitalSignLog));
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Return)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Return(Guid patientId, Guid eventId, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your note could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = noteService.ProcessNotes(patientId, eventId, "Return", reason);
                viewData.errorMessage = viewData.isSuccessful ? "Your note has been successfully returned." : "Your note could not be returned.";
            }
            return Json(viewData);
        }

        [PermissionFilter(AgencyServices.HomeHealth, ParentPermission.QA, PermissionActions.Approve)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Approve(Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your note could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                viewData = noteService.ProcessNotes(patientId, eventId, "Approve", null);
                viewData.errorMessage = viewData.isSuccessful ? "Your note has been successfully approved." : "Your note could not be approved.";
            }
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult BulkUpdate(List<string> CustomValue, string CommandType)
        //{
        //    return Json(mediatorService.BulkUpdate(CustomValue, CommandType));
        //}

        #endregion

        #region Note Supply

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyWorksheet(Guid patientId, Guid Id)
        {
            return PartialView("NotesSupply/WorkSheet", new NoteSupplyNewViewData { Service = AgencyServices.HomeHealth, PatientId = patientId, EventId = Id });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoteSupply(Guid patientId, Guid Id)
        {
            return View(new GridModel(noteService.GetNoteSupplies(patientId, Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNoteSupply(Guid patientId, Guid eventId)
        {
            return PartialView("NotesSupply/New", new NoteSupplyNewViewData { Service = AgencyServices.HomeHealth, PatientId = patientId, EventId = eventId });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddNoteSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var viewData = new JsonViewData(false, "The supply failed to be added.");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            string message;
            if (noteService.AddNoteSupply(patientId, eventId, supply, out message))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully added.";
            }
            else if (message.IsNotNullOrEmpty())
            {
                viewData.errorMessage = message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditNoteSupply(Guid patientId, Guid eventId, Guid id)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(id, "id");
            var viewData = new NoteSupplyEditViewData { Service = AgencyServices.PrivateDuty, PatientId = patientId, EventId = id };
            viewData.Supply = noteService.GetNoteSupply(patientId, eventId, id);
            Supply supply = this.noteService.GetNoteSupply(patientId, eventId, id);
            return PartialView("NotesSupply/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateNoteSupply(Guid patientId, Guid eventId, Supply supply)
        {
            var viewData = new JsonViewData(false, "The supply failed to save.");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");
            string message;
            if (noteService.UpdateNoteSupply(patientId, eventId, supply, out message))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully saved.";
            }
            else if (message.IsNotNullOrEmpty())
            {
                viewData.errorMessage = message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteNoteSupply(Guid patientId, Guid eventId, Guid id)
        {
            var viewData = new JsonViewData(false, "The supply failed to delete.");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(id, "id");

            if (noteService.DeleteNoteSupply(patientId, eventId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The supply was successfully deleted.";
            }
            return Json(viewData);
        }

        #endregion

        #region WoundCare

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WoundSetPostion(Guid Id, Guid PatientId, WoundPosition position)
        {
            return Json(noteService.WoundSetPostion(Id, PatientId, position));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundDelete(Guid Id, Guid PatientId, int WoundNumber, int WoundCount)
        {
            return Json(noteService.WoundDelete(Id, PatientId, WoundNumber, WoundCount));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundAdd(Guid Id, Guid PatientId, string Type, WoundPosition position)
        {
            return PartialView("Nursing/Wound/Add", new WoundViewData<NotesQuestion>() { EventId = Id, PatientId = PatientId, Type = Type, Position = position });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundEdit(Guid Id, Guid PatientId, int WoundNumber)
        {
            return PartialView("Nursing/Wound/Edit", noteService.GetWound(Id, PatientId, WoundNumber));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCare( Guid patientId, Guid Id)
        {
            return PartialView("Nursing/WoundCare", noteService.GetWoundCareNoteViewData( patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult WoundCareSave(WoundSaveArguments saveArguments, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            return Json(noteService.SaveWoundCare(saveArguments, Request.Files, formCollection));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult WoundCareBlank()
        {
            return FileGenerator.Pdf(new WoundCarePdf(noteService.GetVisitNotePrint()), "WoundCare");
        }

        #endregion

        #region Note Asset Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care asset could not be deleted." };
            if (noteService.DeleteWoundCareAsset(patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Wound care asset successfully deleted.";
            }
            return Json(viewData);
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummary(Guid patientId)
        {
            var viewData = new SingleServiceGridViewData { Service = AgencyServices.HomeHealth, Id = patientId, AvailableService = Current.AcessibleServices };
            viewData.DisplayName = patientService.GetPatientNameById(patientId);
            viewData.IsUserCanPrint = Current.HasRight(AgencyServices.HomeHealth, ParentPermission.Schedule, PermissionActions.Print);
            return PartialView("/Views/Patient/SixtyDaySummary.ascx", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummaryList(Guid patientId)
        {
            return View(new GridModel(noteService.GetSixtyDaySummary(patientId)));
        }

        [FileDownload]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetCarePlan(Guid patientId, Guid eventId)
        {
            var viewData = mediatorService.GetCarePlan(patientId, eventId);
            if (viewData.isSuccessful)
            {
                return viewData.File;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult VitalSignsCharts(Guid PatientId)
        {
            ViewData["PatientId"] = PatientId;
            ViewData["DisplayName"] = patientService.GetPatientNameById(PatientId);
            return PartialView("VitalSigns/Main", PatientId.IsEmpty() ? new List<VitalSignLogListItem>() : noteService.GetPatientNewVitalSigns(PatientId, 7));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult VitalSignsChartsContent(Guid PatientId, string CurrentView, DateTime? StartDate, DateTime? EndDate)
        {
            if (!StartDate.HasValue) StartDate = DateTime.Now.AddDays(-60);
            if (!EndDate.HasValue) EndDate = DateTime.Now;
            ViewData["PatientId"] = PatientId;
            ViewData["DisplayName"] = patientService.GetPatientNameById(PatientId);
            var viewUrl = CurrentView == "vitals-report" ? "VitalSigns/Logs" : "VitalSigns/Graphs";
            return PartialView(viewUrl, PatientId.IsEmpty() ? new List<VitalSignLogListItem>() : (CurrentView == "vitals-report" ? noteService.GetPatientNewVitalSigns(PatientId, StartDate.Value, EndDate.Value) : noteService.GetPatientNewVitalSigns(PatientId, 7)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Previous(Guid PatientId, Guid EventId, int DisciplineTask,DateTime EventDate)
        {
            var previousNotes = noteService.GetPreviousNotes(PatientId, new ScheduleEvent { Id = EventId, PatientId = PatientId, DisciplineTask = DisciplineTask, EventDate = EventDate });
             var  items = new List<SelectListItem>();
            items.Insert(0, new SelectListItem { Text =  "-- Select Previous Notes --", Value = Guid.Empty.ToString() });
            if (previousNotes.IsNotNullOrEmpty())
            {
                previousNotes.ForEach(v => items.Add(new SelectListItem { Text =string.Format("{0} {1}", v.Name, v.Date) , Value = v.Id.ToString() }));
            }
            return Json(items);
        }
    }
}
