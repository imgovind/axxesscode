﻿namespace Axxess.AgencyManagement.Application.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Threading;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;

    [Compress]
    [AxxessAuthorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class RequestController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAgencyRepository agencyRepository;

        public RequestController(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region RequestController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TherapyManagementReport(Guid BranchId, DateTime startDate, DateTime endDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Therapy Management Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, startDate, endDate, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddTherapyManagementReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HHRGReport(Guid BranchId, DateTime startDate, DateTime endDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "HHRG Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, startDate, endDate, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddHHRGReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UnbilledVisitsForManagedClaimsReport(Guid BranchId, int InsuranceId, DateTime startDate, DateTime endDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Unbilled Visits for Managed Claims Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, startDate, endDate, InsuranceId, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddUnbilledVisitsReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientsAndVisitsByAgeReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Patients & Visits By Age Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddPatientsAndVisitsByAgeReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DischargesByReasonReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Discharges By Reason Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddDischargesByReasonReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VisitsByPrimaryPaymentSourceReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Visits By Primary Payment Source Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddVisitsByPrimaryPaymentSourceReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VisitsByStaffTypeReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Visits By Staff Type Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddVisitsByStaffTypeReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AdmissionsByReferralSourceReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Admissions By Referral Source Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddAdmissionsByReferralSourceReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientsVisitsByPrincipalDiagnosisReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Patients & Visits By Principal Diagnosis");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddPatientsVisitsByPrincipalDiagnosisReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CostReport(Guid BranchId, int Year)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "Cost Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, Year, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddCostReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PPSEpisodeInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "PPS Episode Information Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, StartDate, EndDate, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddPPSEpisodeInformationReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PPSVisitInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "PPS Visit Information Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, StartDate, EndDate, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddPPSVisitInformationReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PPSPaymentInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "PPS Payment Information Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, StartDate, EndDate, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddPPSPaymentInformationReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PPSChargeInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var report = Report.CreateExcelReport(Current.AgencyId, Current.UserId, "PPS Charge Information Report");
            var reportParameters = new ReportParameters(report.Id, Current.AgencyId, BranchId, StartDate, EndDate, Current.AgencyName);
            return Json(AddRequestedReport(report, reportParameters, new Action<ReportParameters>(ReportManager.AddPPSChargeInformationReport)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteReport(Guid completedReportId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Report could not be deleted." };
            var report = agencyRepository.GetReport(Current.AgencyId, completedReportId);
            if (report != null)
            {
                report.IsDeprecated = true;
                if (agencyRepository.UpdateReport(report))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "This report has been successfully deleted.";
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Private Methods

        private string GetAgencyName(Guid BranchId)
        {
            var agencyAndLocation = agencyRepository.AgencyNameWithLocationName(Current.AgencyId, BranchId);
            return agencyAndLocation.Name + " - " +  agencyAndLocation.OfficeName;
        }

        private JsonViewData AddRequestedReport(Report report, ReportParameters parameters, Action<ReportParameters> action)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The " + report.Type + " could not be requested." };
            parameters.AgencyName = GetAgencyName(parameters.BranchId);
            if (report.IsValid())
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => action.Invoke(parameters)).Start();
                    viewData.errorMessage = "You will be notified when the " + report.Type + " is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return viewData;
        }

        #endregion
    }
}
