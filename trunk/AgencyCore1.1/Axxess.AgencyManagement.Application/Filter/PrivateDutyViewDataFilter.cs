﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Application.Filter
{
    public class PrivateDutyViewDataFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewData["Service"] = AgencyServices.PrivateDuty;
        }
    }
}
