﻿namespace Axxess.AgencyManagement.Application.Filter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;
    using System.Net;
    using System.Web.Routing;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Extensions;

    
    public class PermissionFilter : ActionFilterAttribute
    {
        private AgencyServices Service { get; set; }
        private ParentPermission Category { get; set; }
        private PermissionActions Action { get; set; }
        private ParentPermission[] MultiCategory { get; set; }
        private List<PermissionActions []> MultiAction { get; set; }
        private bool IsMultipleAction { get; set; }
        private bool IsMultipleCategory { get; set; }

        public PermissionFilter(AgencyServices Service, ParentPermission Category, PermissionActions Action)
        {
            this.Service = Service;
            this.Category = Category;
            this.Action = Action;
            this.IsMultipleAction = false;
            this.IsMultipleCategory = false;
        }

        public PermissionFilter(AgencyServices Service, ParentPermission Category, PermissionActions [] Actions)
        {
            this.Service = Service;
            this.Category = Category;
            this.MultiAction = new List<PermissionActions[]>();
            this.MultiAction.Add(Actions);
            this.IsMultipleAction = true;
            this.IsMultipleCategory = false;
        }
        
       /// <summary>
        /// the the index of the category will corresponde with the index of the action list
        /// It is used to check if the user has the permission in the list of category and corresponding actions
       /// </summary>
       /// <param name="Service"></param>
       /// <param name="Categories"></param>
       /// <param name="Actions"></param>
        public PermissionFilter(AgencyServices Service, ParentPermission[] Categories, params object[] Actions)
        {
            this.Service = Service;
            this.MultiAction = new List<PermissionActions[]>();
            foreach (PermissionActions[] a in Actions)
            {
                this.MultiAction.Add(a);
            }
            this.MultiCategory = Categories;
            this.IsMultipleAction = true;
            this.IsMultipleCategory = true;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Current.Services.Has(this.Service) && Current.AcessibleServices.Has(this.Service))
            {
                if (!this.IsMultipleCategory)
                {
                    if (!this.IsMultipleAction)
                    {
                        if (Current.HasRight(this.Service, this.Category, this.Action))
                        {
                            return;
                        }
                        else
                        {
                            FilterHelper(filterContext);
                            return;
                        }
                    }
                    else
                    {
                        if (Current.HasRightAtLeastOne(this.Service, this.Category, this.MultiAction.FirstOrDefault()))
                        {
                            return;
                        }
                        else
                        {
                            FilterHelper(filterContext);
                            return;
                        }
                    }
                }
                else
                {
                    var hasPermission = false;
                    var count = 1;
                    var actionCount = this.MultiAction.Count;
                    foreach (var catagory in this.MultiCategory)
                    {
                        if (count <= actionCount)
                        {
                            if (Current.HasRightAtLeastOne(this.Service, catagory, this.MultiAction[count - 1]))
                            {
                                hasPermission = true;
                                break;
                            }
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (hasPermission)
                    {
                        return;
                    }
                    else
                    {
                        FilterHelper(filterContext);
                        return;
                    }
                }
                //if (!this.IsMultipleAction && Current.HasRight(this.Service, this.Category, this.Action))
                //{
                //    return;
                //}
                //else if (this.IsMultipleAction)
                //{
                //    var hasPermission = false;
                //    var count = 1;
                //    var actionCount = this.MultiAction.Count;
                //    foreach (var catagory in this.MultiCategory)
                //    {
                //        if (count <= actionCount)
                //        {
                //            if (Current.HasRightAtLeastOne(this.Service, catagory, this.MultiAction[count - 1]))
                //            {
                //                hasPermission = true;
                //                break;
                //            }
                //            count++;
                //        }
                //        else
                //        {
                //            break;
                //        }
                //    }
                //    if (hasPermission)
                //    {
                //        return;
                //    }
                //    else
                //    {
                //        FilterHelper(filterContext);
                //        return;
                //    }
                //}
                //else
                //{
                //    FilterHelper(filterContext);
                //    return;
                //}
            }
            else
            {
                FilterHelper(filterContext);
                return;
            }
        }

        private static void FilterHelper(ActionExecutingContext filterContext)
        {
            var result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            //filterContext.
            result.Data = new JsonViewData { isSuccessful = false, errorMessage = "You don't have enough permission to perform this action." };
            filterContext.HttpContext.Response.StatusDescription = "Unauthorized action.";
            filterContext.HttpContext.Response.StatusCode = 403;

            filterContext.Result = result;

           
            return;
        }
    }


    public class AllServicePermissionFilter : ActionFilterAttribute
    {
        #region IAuthorizationFilter Members

       // public AgencyServices Service { get; set; }
        public ParentPermission Category { get; set; }
        public PermissionActions Action { get; set; }

        public AllServicePermissionFilter(ParentPermission Category, PermissionActions Action)
        {
            //this.Service = Service;
            this.Category = Category;
            this.Action = Action;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (Current.HasRight((int)AgencyServicesBoundary.All, this.Category, this.Action))
            {
                return;
            }
            else
            {
                FilterHelper(filterContext);
                return;
            }
        }

        private static void FilterHelper(ActionExecutingContext filterContext)
        {
            var result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = new JsonViewData { isSuccessful = false, errorMessage = "You don't have enough permission to perform this action." };
            filterContext.HttpContext.Response.StatusDescription = "Unauthorized action.";
            filterContext.Result = result;
            return;
        }
        #endregion
    }
}
