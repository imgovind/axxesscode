﻿namespace Axxess.AgencyManagement.Application.Filter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.Core.Enums;
    using System.Net;
    using System.Web.Routing;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities;

    
    public class ReportPermissionFilter : ActionFilterAttribute
    {
        private AgencyServices Service { get; set; }
        private ReportPermissions Category { get; set; }
        private PermissionActions Action { get; set; }

        public ReportPermissionFilter(AgencyServices Service, ReportPermissions Category, PermissionActions Action)
        {
            this.Service = Service;
            this.Category = Category;
            this.Action = Action;
        }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Current.Services.Has(this.Service) && Current.AcessibleServices.Has(this.Service))
            {
                var permission = UserSessionEngine.ReportPermissions(Current.AgencyId, Current.UserId, Current.AcessibleServices, Current.SessionId);// Current.ReportPermissions;
                if (permission.IsInPermission(this.Service,(int) this.Category, this.Action))
                {
                    return;
                }
                else
                {
                    FilterHelper(filterContext);
                    return;
                }
            }
            else
            {
                FilterHelper(filterContext);
                return;
            }
        }

        private static void FilterHelper(ActionExecutingContext filterContext)
        {
            var result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = new JsonViewData { isSuccessful = false, errorMessage = "You don't have enough permission to perform this action." };
            filterContext.HttpContext.Response.StatusDescription = "Unauthorized action.";
            filterContext.Result = result;
            return;
        }
    }


   
}
