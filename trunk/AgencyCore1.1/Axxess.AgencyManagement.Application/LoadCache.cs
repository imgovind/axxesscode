﻿namespace Axxess.AgencyManagement.App
{
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.OasisC.Repositories;

    public class LoadCache : IStartupTask
    {
        #region Constructor/Members

        private readonly ICachedDataRepository cachedDataRepository;

        public LoadCache(IOasisCDataProvider oasicDataProvider)
        {
            Check.Argument.IsNotNull(oasicDataProvider, "oasicDataProvider");

            this.cachedDataRepository = oasicDataProvider.CachedDataRepository;
        }

        #endregion

        #region IStartupTask Members

        public void Execute()
        {
            this.cachedDataRepository.GetOasisGuides();
            this.cachedDataRepository.GetSubmissionFormatInstructions();
        }

        #endregion
    }
}
