﻿namespace Axxess.AgencyManagement.App.Areas.PrivateDuty.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using ViewData;
    using Services;
    using Extensions;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.Workflows;

    using Telerik.Web.Mvc;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.LookUp.Domain;

    using Axxess.Log.Enums;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IPatientService patientService;
        private readonly IPrivateDutyScheduleService scheduleService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPrivateDutyScheduleRepository scheduleRepository;

        public ScheduleController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService, IReportService reportService, IPrivateDutyScheduleService scheduleService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = dataProvider.UserRepository;
            this.assetRepository = dataProvider.AssetRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.scheduleRepository = dataProvider.PrivateDutyScheduleRepository;
            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.scheduleService = scheduleService;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSelector()
        {
            return PartialView("Center/PatientSelector");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserSelector()
        {
            return PartialView("Center/UserSelector");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            return PartialView("Center/Layout");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId, "");
            else if (Current.IsClinicianOrHHA)
                patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterUserGrid(Guid branchId, int statusId)
        {
            var userList = userService.GetUsersByStatus(branchId, statusId);
            return View(new GridModel(userList.OrderBy(u => u.LastName).ThenBy(u => u.FirstName)));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult List(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            List<PrivateDutyScheduleTask> tasks = null;
            if(!patientId.IsEmpty() && userId.IsEmpty())
            {
                tasks = scheduleService.GetScheduleTasksBetweenDates(patientId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            }
            else if(patientId.IsEmpty() && !userId.IsEmpty())
            {
                tasks = scheduleService.GetUsersScheduleTasksBetweenDates(userId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            }
            else if(!patientId.IsEmpty() && !userId.IsEmpty())
            {
                tasks = scheduleService.GetUsersScheduleTasksBetweenDates(userId, patientId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            }
            else
            {
                tasks = new List<PrivateDutyScheduleTask>();
            }
            if (tasks.IsNotNullOrEmpty())
            {
                var jsonEvents = tasks.Select(s => new
                {
                    Id = s.Id,
                    s.IsAllDay,
                    EventStartTime = s.EventStartTime,
                    EventEndTime = s.EventEndTime,
                    DisciplineTaskName = s.DisciplineTaskName,
                    UserName = s.UserName,
                    Status = s.Status
                });
                return Json(jsonEvents);
            }
            else
            {
                return Json(tasks);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            List<PrivateDutyScheduleTask> tasks = null;
            if (!patientId.IsEmpty() && userId.IsEmpty())
            {
                tasks = scheduleService.GetScheduleTasksBetweenDates(patientId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            }
            else if (patientId.IsEmpty() && !userId.IsEmpty())
            {
                tasks = scheduleService.GetUsersScheduleTasksBetweenDates(userId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            }
            else if (!patientId.IsEmpty() && !userId.IsEmpty())
            {
                tasks = scheduleService.GetUsersScheduleTasksBetweenDates(userId, patientId, startDate, endDate) ?? new List<PrivateDutyScheduleTask>();
            }
            else
            {
                tasks = new List<PrivateDutyScheduleTask>();
            }
            return PartialView("Center/Grid", tasks);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid patientId, Guid userId)
        {
            ViewData["patientId"] = patientId.ToSafeString();
            ViewData["userId"] = userId.ToSafeString();
            return PartialView("Task/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Create(PrivateDutyScheduleTask newTask)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved." };
            if (newTask != null)
            {
                if (newTask.IsValid)
                {
                    if (scheduleService.AddTask(newTask))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Task was saved successfully.";
                    }
                }
                else
                {
                    viewData.errorMessage = newTask.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id, Guid patientId)
        {
            var task = new PrivateDutyScheduleTask();
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                task = scheduleService.GetTaskWithPatientAndUserName(patientId, id);
            }
            return PartialView("Task/Edit", task);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update([Bind] PrivateDutyScheduleTask editTask)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved." };
            if (editTask != null)
            {
                if (editTask.IsValid)
                {
                    if (scheduleService.UpdateTaskDetails(editTask, Request.Files))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The task was updated successfully.";
                    }
                }
                else
                {
                    viewData.errorMessage = editTask.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be deleted." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (scheduleService.DeleteTask(patientId, id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was deleted successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be restored." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (scheduleService.RestoreTask(patientId, id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The task was restored successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reassign(Guid id, Guid patientId, Guid newUserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The task could not be reassigned." };
            if (!id.IsEmpty() && !patientId.IsEmpty() && !newUserId.IsEmpty())
            {
                if (scheduleService.ReassignTask(patientId, id, newUserId))
                {
                    string userName = UserEngine.GetName(newUserId, Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = string.Format("The task was reassigned to {0} successfully.", userName);
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReassignMultiple(Guid PatientId, Guid OldUserId, Guid NewUserId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The tasks could not be reassigned." };
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignSchedules(Guid patientId, Guid userId, DateTime startDate, DateTime endDate)
        {
            ViewData["PatientId"] = patientId.ToSafeString();
            ViewData["UserId"] = userId.ToSafeString();
            ViewData["StartDate"] = startDate.ToShortDateString();
            ViewData["EndDate"] = endDate.ToShortDateString();
            return PartialView("Task/ReassignMutliple");
        }

        #endregion
    }
}
