﻿namespace Axxess.AgencyManagement.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.Core.Extension;
using Axxess.AgencyManagement.Application.ViewData;

    public class EntityHelper
    {
        public static Profile CreateProfile(PendingPatient pending, Referral referral)
        {
            var profile = new Profile();
            SetProfileFromPending(profile, pending, referral);
            profile.Id = referral.Id;
            profile.AgencyId = referral.AgencyId;
            profile.Created = DateTime.Now;
            profile.IsDeprecated = false;
            return profile;
        }

        public static Profile CreateProfile(PendingPatient pending, Patient patient)
        {
            var profile = new Profile();
            SetProfileFromPending(profile, pending, patient);
            profile.Id = patient.Id;
            profile.AgencyId = patient.AgencyId;
            profile.Created = DateTime.Now;
            profile.IsDeprecated = false;
            return profile;
        }

        public static void SetProfileFromPending(Profile profile, PendingPatient pending, Referral referral)
        {
            SetProfileFromPending(profile, pending);
            profile.PatientIdNumber = pending.PatientIdNumber;
            profile.FirstName = referral.FirstName;
            profile.MiddleInitial = referral.MiddleInitial;
            profile.LastName = referral.LastName;
        }
    
        public static void SetProfileFromPending(Profile profile, PendingPatient pending, Patient patient)
        {
            SetProfileFromPending(profile, pending);
            profile.PatientIdNumber = patient.PatientIdNumber;
            profile.FirstName = patient.FirstName;
            profile.MiddleInitial = patient.MiddleInitial;
            profile.LastName = patient.LastName;
        }
      
        private static void SetProfileFromPending(Profile profile, PendingPatient pending)
        {
            profile.AdmissionId = pending.AdmissionId;
            profile.AgencyLocationId = pending.AgencyLocationId;
            profile.UserId = pending.UserId;
            profile.CaseManagerId = pending.CaseManagerId;
            profile.PrimaryInsurance = pending.PrimaryInsurance;
            profile.SecondaryInsurance = pending.SecondaryInsurance;
            profile.TertiaryInsurance = pending.TertiaryInsurance;
            profile.Payer = pending.Payer;
            profile.Status = (int)PatientStatus.Active;
            profile.StartofCareDate = pending.StartofCareDate;
            profile.Modified = DateTime.Now;
        }

        public static void SetProfileFromClientProfile(Profile existingProfile, Profile clientInputProfile)
        {
           // exsitingProfile.AdmissionId = clientInputProfile.AdmissionId;
            existingProfile.StartofCareDate = clientInputProfile.StartofCareDate;
            existingProfile.EpisodeStartDate = clientInputProfile.EpisodeStartDate;
            existingProfile.EpisodeEndDate = clientInputProfile.EpisodeEndDate;
            existingProfile.CaseManagerId = clientInputProfile.CaseManagerId;
            existingProfile.UserId = clientInputProfile.UserId;
            existingProfile.AgencyLocationId = clientInputProfile.AgencyLocationId;

            existingProfile.ReferralDate = clientInputProfile.ReferralDate;
            existingProfile.AdmissionSource = clientInputProfile.AdmissionSource;
            existingProfile.OtherReferralSource = clientInputProfile.OtherReferralSource;
            existingProfile.InternalReferral = clientInputProfile.InternalReferral;

            existingProfile.PrimaryInsurance = clientInputProfile.PrimaryInsurance;
            existingProfile.SecondaryInsurance = clientInputProfile.SecondaryInsurance;
            existingProfile.TertiaryInsurance = clientInputProfile.TertiaryInsurance;

            if (clientInputProfile.PrimaryInsurance >= 1000)
            {
                existingProfile.PrimaryHealthPlanId = clientInputProfile.PrimaryHealthPlanId;
                existingProfile.PrimaryGroupName = clientInputProfile.PrimaryGroupName;
                existingProfile.PrimaryGroupId = clientInputProfile.PrimaryGroupId;
                existingProfile.PrimaryRelationship = clientInputProfile.PrimaryRelationship;
            }
            else existingProfile.PrimaryHealthPlanId = existingProfile.PrimaryGroupName = existingProfile.PrimaryGroupId = string.Empty;

            if (clientInputProfile.SecondaryInsurance >= 1000)
            {
                existingProfile.SecondaryHealthPlanId = clientInputProfile.SecondaryHealthPlanId;
                existingProfile.SecondaryGroupName = clientInputProfile.SecondaryGroupName;
                existingProfile.SecondaryGroupId = clientInputProfile.SecondaryGroupId;
                existingProfile.SecondaryRelationship = clientInputProfile.SecondaryRelationship;
            }
            else existingProfile.SecondaryHealthPlanId = existingProfile.SecondaryGroupName = existingProfile.SecondaryGroupId = string.Empty;

            if (clientInputProfile.TertiaryInsurance >= 1000)
            {
                existingProfile.TertiaryHealthPlanId = clientInputProfile.TertiaryHealthPlanId;
                existingProfile.TertiaryGroupName = clientInputProfile.TertiaryGroupName;
                existingProfile.TertiaryGroupId = clientInputProfile.TertiaryGroupId;
                existingProfile.TertiaryRelationship = clientInputProfile.TertiaryRelationship;
            }
            else existingProfile.TertiaryHealthPlanId = existingProfile.TertiaryGroupName = existingProfile.TertiaryGroupId = string.Empty;
           
            //exsitingProfile.Payer = clientInputProfile.Payer;
            //exsitingProfile.Status = (int)PatientStatus.Active;
            //exsitingProfile.Modified = DateTime.Now;
        }

        public static E CreateEpisode<E>(Guid patientId, DateTime startDate, int addedDay) where E : CarePeriod, new()
        {
            var patientEpisode = new E();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = startDate;
            patientEpisode.EndDate = startDate.AddDays(addedDay);
            patientEpisode.IsActive = true;
            return patientEpisode;
        }

        public static E CreateEpisode<E>(Guid patientId, E episode) where E: CarePeriod, new()
        {
            var patientEpisode = new E();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = episode.StartDate;
            patientEpisode.EndDate = episode.EndDate;
            patientEpisode.IsActive = true;
            patientEpisode.PrimaryInsurance = episode.PrimaryInsurance;
            patientEpisode.SecondaryInsurance = episode.SecondaryInsurance;
            patientEpisode.Comments = episode.Comments;
            patientEpisode.CaseManagerId = episode.CaseManagerId;
            patientEpisode.PhysicianId = episode.PhysicianId;
            patientEpisode.StartOfCareDate = episode.StartOfCareDate;
            return patientEpisode;
        }

        public static T CreateTaskForSOC<T, E>(Patient patient, Profile profile, E episode)
            where E : CarePeriod, new()
            where T : ITask, new()
        {
            var newEvent = new T
            {
                AgencyId = Current.AgencyId,
                Id = Guid.NewGuid(),
                PatientId = patient.Id,
                EpisodeId = episode.Id,
                UserId = profile.UserId,
                Discipline = Disciplines.Nursing.ToString(),
                Status = ((int)ScheduleStatus.OasisNotYetDue),
                DisciplineTask = (int)DisciplineTasks.OASISCStartOfCare,
                EventDate = profile.EpisodeStartDate,
                VisitDate = profile.EpisodeStartDate,
                IsBillable = true
            };
            return newEvent;
        }

        public static FaceToFaceEncounter CreateFaceToFaceEncounter(Profile profile, Guid episodeId, AgencyPhysician physician)
        {
            var faceToFaceEncounter = new FaceToFaceEncounter
            {
                PatientId = profile.Id,
                EpisodeId = episodeId,
                UserId = profile.UserId,
                Id = Guid.NewGuid(),
                RequestDate = DateTime.Now
            };
            if (physician != null)
            {
                faceToFaceEncounter.PhysicianId = physician.Id;
                faceToFaceEncounter.PhysicianData = physician.ToXml();
            }
            return faceToFaceEncounter;
        }

        public static PatientAdmissionDate CreatePatientAdmissionDate(Patient patient, Profile profile, int dischargeReasonId, string dischargeReason, Guid admissionDateId)
        {
            return new PatientAdmissionDate
            {
                Id = admissionDateId,
                AgencyId = Current.AgencyId,
                PatientId = profile.Id,
                StartOfCareDate = profile.StartofCareDate,
                DischargedDate = profile.Status == (int)PatientStatus.Discharged ? profile.DischargeDate : DateTime.MinValue,
                PatientData = patient.ToXml(),
                ProfileData = profile.ToXml(),
                Status = profile.Status,
                DischargeReasonId = dischargeReasonId,
                Reason = dischargeReason,
                IsDeprecated = false,
                IsActive = true
            };
        }

        public static Assessment CreateAssessment<T>(T scheduleEvent, string medicationProfile) where T : ITask, new()
        {
            var assessment = new Assessment
            {
                AgencyId = Current.AgencyId,
                Id = scheduleEvent.Id,
                EpisodeId = scheduleEvent.EpisodeId,
                PatientId = scheduleEvent.PatientId,
                Status = scheduleEvent.Status,
                UserId = scheduleEvent.UserId,
                AssessmentDate = scheduleEvent.EventDate,
                Version = scheduleEvent.Version,
                MedicationProfile = medicationProfile,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Type = scheduleEvent.GetOasisType().ToString()
            };
            return assessment;
        }

        public static MedicationProfile CreateMedicationProfile(Patient patient)
        {
            return new MedicationProfile
            {
                AgencyId = Current.AgencyId,
                Id = Guid.NewGuid(),
                Medication = new List<Medication>().ToXml(),
                PharmacyName = patient.PharmacyName,
                PharmacyPhone = patient.PharmacyPhone,
                PatientId = patient.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now
            };
        }

        public static void SetPatientPrintProfile(PatientProfileLean profile, PatientAdmissionDate admission)
        {
            profile = profile ?? new PatientProfileLean();
            if (admission != null)
            {
                if (admission.PatientData.IsNotNullOrEmpty())
                {
                    var patient = admission.PatientData.ToObject<Patient>();
                    if (patient != null)
                    {
                        profile.LastName = patient.LastName;
                        profile.FirstName = patient.FirstName;
                        profile.MiddleInitial = patient.MiddleInitial;
                        profile.PatientIdNumber = patient.PatientIdNumber;
                        profile.DOB = patient.DOB;
                        profile.Phone = patient.PhoneHomeFormatted;

                        profile.Address = new Address();

                        profile.Address.Line1 = patient.AddressLine1;
                        profile.Address.Line2 = patient.AddressLine2;
                        profile.Address.City = patient.AddressCity;
                        profile.Address.ZipCode = patient.AddressZipCode;
                        profile.Address.StateCode = patient.AddressStateCode;

                    }
                    profile.StartofCareDate = admission.StartOfCareDate;
                }
            }
        }

        public static void SetPatientAdmissionDate(PatientAdmissionDate admission, Profile profile, Patient patient)
        {
            admission.ProfileData = profile.ToXml();
            admission.PatientData = patient.ToXml();
            admission.StartOfCareDate = profile.StartofCareDate;
            if (profile.Status == (int)PatientStatus.Discharged)
            {
                admission.DischargedDate = profile.DischargeDate;
                admission.DischargeReasonId = profile.DischargeReasonId;
                admission.Reason = profile.DischargeReason;
            }

        }

        public static void SetReferralDataFromPatient(Referral referral, Patient patient)
        {
            referral.ReferrerPhysician = patient.ReferrerPhysician;
            referral.FirstName = patient.FirstName;
            referral.MiddleInitial = patient.MiddleInitial;
            referral.LastName = patient.LastName;
            referral.Gender = patient.Gender;
            referral.DOB = patient.DOB;
            referral.MaritalStatus = patient.MaritalStatus;
            referral.SSN = patient.SSN;
            referral.IsDNR = patient.IsDNR;
            referral.Height = patient.Height;
            referral.HeightMetric = patient.HeightMetric;
            referral.Weight = patient.Weight;
            referral.WeightMetric = patient.WeightMetric;

            referral.MedicareNumber = patient.MedicareNumber;
            referral.MedicaidNumber = patient.MedicaidNumber;

            referral.AddressLine1 = patient.AddressLine1;
            referral.AddressLine2 = patient.AddressLine2;
            referral.AddressCity = patient.AddressCity;
            referral.AddressStateCode = patient.AddressStateCode;
            referral.AddressZipCode = patient.AddressZipCode;
            referral.PhoneHome = patient.PhoneHome;
            referral.EmailAddress = patient.EmailAddress;

            referral.ServicesRequired = patient.ServicesRequired;
            referral.DME = patient.DME;
            referral.OtherDME = patient.OtherDME;
            referral.Comments = patient.Comments;
            referral.Modified = DateTime.Now;
        }

        public static void SetProfile(Profile profileFromDB, Profile profileClientInput)
        {
            if (profileClientInput.PrimaryInsurance >= 1000)
            {
                profileFromDB.PrimaryHealthPlanId = profileClientInput.PrimaryHealthPlanId;
                profileFromDB.PrimaryGroupName = profileClientInput.PrimaryGroupName;
                profileFromDB.PrimaryGroupId = profileClientInput.PrimaryGroupId;
                profileFromDB.PrimaryRelationship = profileClientInput.PrimaryRelationship;
            }
            else profileFromDB.PrimaryHealthPlanId = profileFromDB.PrimaryGroupName = profileFromDB.PrimaryGroupId = string.Empty;

            if (profileClientInput.SecondaryInsurance >= 1000)
            {
                profileFromDB.SecondaryHealthPlanId = profileClientInput.SecondaryHealthPlanId;
                profileFromDB.SecondaryGroupName = profileClientInput.SecondaryGroupName;
                profileFromDB.SecondaryGroupId = profileClientInput.SecondaryGroupId;
                profileFromDB.SecondaryRelationship = profileClientInput.SecondaryRelationship;
            }
            else profileFromDB.SecondaryHealthPlanId = profileFromDB.SecondaryGroupName = profileFromDB.SecondaryGroupId = string.Empty;

            if (profileClientInput.TertiaryInsurance >= 1000)
            {
                profileFromDB.TertiaryHealthPlanId = profileClientInput.TertiaryHealthPlanId;
                profileFromDB.TertiaryGroupName = profileClientInput.TertiaryGroupName;
                profileFromDB.TertiaryGroupId = profileClientInput.TertiaryGroupId;
                profileFromDB.TertiaryRelationship = profileClientInput.TertiaryRelationship;
            }
            else profileFromDB.TertiaryHealthPlanId = profileFromDB.TertiaryGroupName = profileFromDB.TertiaryGroupId = string.Empty;

            profileFromDB.AgencyLocationId = profileClientInput.AgencyLocationId;
            profileFromDB.StartofCareDate = profileClientInput.StartofCareDate;
            if (profileClientInput.Status == (int)PatientStatus.Discharged)
            {
                profileFromDB.DischargeDate = profileClientInput.DischargeDate;
                profileFromDB.DischargeReason = profileClientInput.DischargeReason;
            }
            profileFromDB.PrimaryInsurance = profileClientInput.PrimaryInsurance;
            profileFromDB.SecondaryInsurance = profileClientInput.SecondaryInsurance;
            profileFromDB.TertiaryInsurance = profileClientInput.TertiaryInsurance;
           
            profileFromDB.CaseManagerId = profileClientInput.CaseManagerId;
            profileFromDB.UserId = profileClientInput.UserId;
            profileFromDB.AdmissionId = profileClientInput.AdmissionId;
            profileFromDB.PaymentSource = profileClientInput.PaymentSource;
            profileFromDB.OtherPaymentSource = profileClientInput.OtherPaymentSource;

            //profileFromDB.FirstName = profileClientInput.FirstName;
            //profileFromDB.LastName = profileClientInput.LastName;
            //profileFromDB.MiddleInitial = profileClientInput.MiddleInitial;

            profileFromDB.AdmissionSource = profileClientInput.AdmissionSource;
            profileFromDB.ReferralDate = profileClientInput.ReferralDate;
            profileFromDB.InternalReferral = profileClientInput.InternalReferral;
            profileFromDB.OtherReferralSource = profileClientInput.OtherReferralSource;
            profileFromDB.AuditorId = profileClientInput.AuditorId;
        }

        public static void SetPatientPrivatePayor(PrivatePayor privatePayorFromDB, PrivatePayor privatePayorClientInput)
        {
            privatePayorFromDB.FirstName = privatePayorClientInput.FirstName;
            privatePayorFromDB.LastName = privatePayorClientInput.LastName;
            privatePayorFromDB.MI = privatePayorClientInput.MI;
            privatePayorFromDB.AddressLine1 = privatePayorClientInput.AddressLine1;
            privatePayorFromDB.AddressLine2 = privatePayorClientInput.AddressLine2;
            privatePayorFromDB.AddressCity = privatePayorClientInput.AddressCity;
            privatePayorFromDB.AddressStateCode = privatePayorClientInput.AddressStateCode;
            privatePayorFromDB.AddressZipCode = privatePayorClientInput.AddressZipCode;
            privatePayorFromDB.PhoneHome = privatePayorClientInput.PhoneHome;
            privatePayorFromDB.PhoneMobile = privatePayorClientInput.PhoneMobile;
            privatePayorFromDB.FaxNumber = privatePayorClientInput.FaxNumber;
            privatePayorFromDB.EmailAddress = privatePayorClientInput.EmailAddress;
            privatePayorFromDB.Relationship = privatePayorClientInput.Relationship;
            if (string.Format("R_{0}",privatePayorClientInput.Relationship).IsEqual(RelationshipTypes.R_G8.ToString()))
            {
                privatePayorFromDB.OtherRelationship = privatePayorClientInput.OtherRelationship;
            }
        }

        public static void SetVitalSignLog(VitalSignLog vitalSignLog, Guid agencyId, Guid patientId, Guid entityId, DateTime visitDate, string timeIn)
        {
            vitalSignLog.AgencyId = agencyId;
            vitalSignLog.PatientId = patientId;
            vitalSignLog.EntityId = entityId;
            //Converts the visit date and time in into a single datetime struct
            var stringDate = (visitDate.ToString("MM/dd/yyyy") + (timeIn.IsNotNullOrEmpty() ? " " + timeIn : " 12:00 AM")).Trim();
            vitalSignLog.VisitDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm tt", CultureInfo.CurrentCulture);
        }

        private static ClaimViewData GetClaimViewData(BaseClaim claim)
        {
            var claimData = new ClaimViewData
                       {
                           Id = claim.Id,
                           PatientId=claim.PatientId,
                           UB4PatientStatus = claim.UB4PatientStatus,
                           PatientIdNumber = claim.PatientIdNumber,
                           EpisodeStartDate = claim.EpisodeStartDate,
                           EpisodeEndDate = claim.EpisodeEndDate,
                           FirstName = claim.FirstName,
                           LastName = claim.LastName,
                           AddressLine1 = claim.AddressLine1,
                           AddressLine2 = claim.AddressLine2,
                           AddressCity = claim.AddressCity,
                           AddressStateCode = claim.AddressStateCode,
                           AddressZipCode = claim.AddressZipCode,
                           DOB = claim.DOB,
                           Gender = claim.Gender,
                           AdmissionSource = claim.AdmissionSource.IsNotNullOrEmpty() && claim.AdmissionSource.IsInteger() ? claim.AdmissionSource.ToInteger().GetSplitValue() : "9",
                           HippsCode = claim.HippsCode,
                           PrimaryInsuranceId = claim.PrimaryInsuranceId,
                           ClaimKey = claim.ClaimKey,
                           DiagnosisCode = claim.DiagnosisCode.IsNotNullOrEmpty() ? claim.DiagnosisCode.ToObject<DiagnosisCodes>() : null,
                           PhysicianFirstName = claim.PhysicianFirstName,
                           PhysicianLastName = claim.PhysicianLastName,
                           PhysicianNPI = claim.PhysicianNPI,
                           StartofCareDate = claim.StartofCareDate,
                           FirstBillableVisitDate = claim.FirstBillableVisitDate,
                           HealthPlanId = claim.HealthPlanId,
                           GroupName = claim.GroupName,
                           GroupId = claim.GroupId,
                           AuthorizationNumber = claim.AuthorizationNumber,
                           AuthorizationNumber2 = claim.AuthorizationNumber2,
                           AuthorizationNumber3 = claim.AuthorizationNumber3,
                           ConditionCodes = claim.ConditionCodes.IsNotNullOrEmpty() ? claim.ConditionCodes.ToObject<ConditionCodes>() : null,
                           Ub04Locator81cca = claim.Ub04Locator81cca,
                           Ub04Locator39 = claim.Ub04Locator39,
                           Ub04Locator31 = claim.Ub04Locator31,
                           Ub04Locator32 = claim.Ub04Locator32,
                           Ub04Locator33 = claim.Ub04Locator33,
                           Ub04Locator34 = claim.Ub04Locator34,
                           Status = claim.Status,
                           RelationshipId = claim.Relationship,
                           Remark = claim.Remark,
                           IsBillingAddressDifferent = claim.IsBillingAddressDifferent,
                           Insurance = claim.Insurance
                       };
            return claimData;
        }

        public static ClaimViewData GetManagedClaimViewData(ManagedClaim claim)
        {
            var viewData = GetClaimViewData(claim);
            viewData.Type = claim.Type;
            viewData.SupplyCode = claim.SupplyCode;
            viewData.IsHomeHealthServiceIncluded = claim.IsHomeHealthServiceIncluded;
            viewData.MedicareNumber = claim.InsuranceIdNumber;
            viewData.VerifiedVisit = claim.VerifiedVisits;
            viewData.Supply = claim.Supply;
            viewData.SupplyTotal = claim.SupplyTotal;
            return viewData;
        }

        public static ClaimViewData GetSecondaryClaimViewData(SecondaryClaim claim)
        {
            var viewData = GetClaimViewData(claim);
            viewData.SupplyCode = claim.SupplyCode;
            viewData.MedicareNumber = claim.InsuranceIdNumber;
            viewData.PrimaryInsuranceId = claim.SecondaryInsuranceId;
            viewData.Type = claim.Type.ToString();
            viewData.VerifiedVisit = claim.VerifiedVisits;
            viewData.Supply = claim.Supply;
            viewData.SupplyTotal = claim.SupplyTotal;
            return viewData;
        }

        public static ClaimViewData GetFinalClaimViewData(Final claim)
        {
            var viewData = GetClaimViewData(claim);
            viewData.VerifiedVisit = claim.VerifiedVisits;
            viewData.Supply = claim.Supply;
            viewData.MedicareNumber = claim.MedicareNumber;
            viewData.SupplyTotal = claim.SupplyTotal;
            viewData.IsSupplyNotBillable = claim.IsSupplyNotBillable;
            viewData.Type = claim.Type == 1 ? "328" : ((int)BillType.HHPPSFinal).ToString();
            return viewData;
                          
                           
        }

        public static ClaimViewData GetRAPClaimViewData(Rap claim)
        {
            var viewData = GetClaimViewData(claim);
            viewData.MedicareNumber = claim.MedicareNumber;
            viewData.Type = claim.Type == 1 ? "328" : ((int)BillType.HHPPSRAP).ToString();
            return viewData;

          
        }

        //public static ClaimViewData xxx(Final claim)
        //{
        //    var claimData = new ClaimViewData
        //    {
        //            UB4PatientStatus = claim.UB4PatientStatus,
        //        PatientIdNumber = claim.PatientIdNumber,
        //        Type = claim.Type == 1 ? "328" : ((int)BillType.HHPPSFinal).ToString(),
        //        EpisodeStartDate = claim.EpisodeStartDate,
        //        EpisodeEndDate = claim.EpisodeEndDate,
        //        FirstName = claim.FirstName,
        //        LastName = claim.LastName,
        //        AddressLine1 = claim.AddressLine1,
        //        AddressLine2 = claim.AddressLine2,
        //        AddressCity = claim.AddressCity,
        //        AddressStateCode = claim.AddressStateCode,
        //        AddressZipCode = claim.AddressZipCode,
        //        DOB = claim.DOB,
        //        Gender = claim.Gender,
        //        AdmissionSource = claim.AdmissionSource.IsNotNullOrEmpty() && claim.AdmissionSource.IsInteger() ? claim.AdmissionSource.ToInteger().GetSplitValue() : "9",
        //        CBSA = lookUpRepository.CbsaCodeByZip(claim.AddressZipCode),
        //        HippsCode = claim.HippsCode,
        //        PrimaryInsuranceId = claim.PrimaryInsuranceId,
        //        ClaimKey = claim.ClaimKey,
        //        DiagnosisCode = claim.DiagnosisCode,
        //        PhysicianFirstName = claim.PhysicianFirstName,
        //        PhysicianLastName = claim.PhysicianLastName,
        //        PhysicianNPI = claim.PhysicianNPI,
        //        VerifiedVisit = claim.VerifiedVisits,
        //        StartofCareDate = claim.StartofCareDate,
        //        MedicareNumber = claim.MedicareNumber,
        //        SupplyTotal = claim.SupplyTotal,
        //        FirstBillableVisitDate = claim.FirstBillableVisitDate,
        //        ConditionCodes = claim.ConditionCodes,
        //        Ub04Locator81cca = claim.Ub04Locator81cca,
        //        Ub04Locator39 = claim.Ub04Locator39,
        //        Ub04Locator31 = claim.Ub04Locator31,
        //        Ub04Locator32 = claim.Ub04Locator32,
        //        Ub04Locator33 = claim.Ub04Locator33,
        //        Ub04Locator34 = claim.Ub04Locator34,
        //        Status = claim.Status,
        //        AgencyLocationId = profile.AgencyLocationId,
        //        Remark = claim.Remark,
        //        Supply = claim.Supply,
        //        Insurance = claim.Insurance,
        //        IsSupplyNotBillable = claim.IsSupplyNotBillable,
        //        RelationshipId = claim.Relationship,
                
        //                   HealthPlanId = claim.HealthPlanId,
        //                   GroupName = claim.GroupName,
        //                   GroupId = claim.GroupId,
        //                   AuthorizationNumber = claim.AuthorizationNumber,
        //                   AuthorizationNumber2 = claim.AuthorizationNumber2,
        //                   AuthorizationNumber3 = claim.AuthorizationNumber3,
        //                   IsBillingAddressDifferent=claim.IsBillingAddressDifferent


            




        //        //Type = final.Type == 1 ? "328" : ((int)BillType.HHPPSFinal).ToString(),
        //        //CBSA = lookUpRepository.CbsaCodeByZip(final.AddressZipCode),
        //        //PrimaryInsuranceId = final.PrimaryInsuranceId,
        //        //MedicareNumber = final.MedicareNumber,
        //        //AgencyLocationId = profile.AgencyLocationId,
        //        //Insurance = final.Insurance,
        //        //IsSupplyNotBillable = final.IsSupplyNotBillable,
        //        //RelationshipId = final.Relationship
        //    };
        //    return claimData;
        //}
    }
}
