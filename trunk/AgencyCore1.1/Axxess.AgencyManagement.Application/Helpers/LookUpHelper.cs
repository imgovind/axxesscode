﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.LookUp.Repositories;
using Axxess.Core.Infrastructure;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.Helpers
{
   public class LookUpHelper
    {
       private static ILookupRepository lookUpRepository
       {
           get
           {
               ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
               return dataProvider.LookUpRepository;
           }
       }

       public static double ProspectivePayAmount(string assessmentType, string hippsCode, DateTime startDate, string claimZipCode, string locationZipCode)
       {
           var value = 0.0;
           if (assessmentType.IsNotNullOrEmpty())
           {
               if (assessmentType == DisciplineTasks.OASISCStartOfCare.ToString() || assessmentType == DisciplineTasks.OASISCStartOfCarePT.ToString() || assessmentType == DisciplineTasks.OASISCStartOfCareOT.ToString())
               {
                   value = Math.Round(lookUpRepository.ProspectivePayAmount(hippsCode, startDate, claimZipCode, locationZipCode), 2);
               }
               else
               {
                   value = Math.Round(lookUpRepository.ProspectivePayAmount(hippsCode, startDate, claimZipCode, locationZipCode), 2);
               }
           }
           return value;
       }

    }
}
