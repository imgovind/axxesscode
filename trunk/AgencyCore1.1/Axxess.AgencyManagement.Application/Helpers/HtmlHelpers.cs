﻿namespace Axxess.AgencyManagement.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Web.Mvc;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.LookUp.Repositories;

    public abstract class HtmlHelperAbstraction
    {
        #region Fields

        protected ILookupRepository LookupRepository { get; set; }
        
        protected IPatientRepository PatientRepository { get; set; }
        
        protected PatientProfileAbstract ProfileRepository { get; set; }
        
        protected IUserRepository UserRepository { get; set; }

        #endregion

        #region Public Properties

        protected abstract AgencyServices Service { get; }

        #endregion

        #region Constructors and Destructors

        public HtmlHelperAbstraction(ILookUpDataProvider lookUpDataProvider)
        {
            LookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Public Methods and Operators

        public abstract string CarePeriodLabel(object htmlAttributes);

        //public abstract List<SelectListItem> ClaimDisciplineTask(string name, int disciplineTask, Guid claimId, ClaimTypeSubCategory typeOfClaim,int payorType, bool isTaskIncluded);

        public abstract string DiagnosisRow(string diagnosisName, string diagnosis, string icd9, object htmlAttributes);

        public IEnumerable<SelectListItem> GenerateFilteredPatientProfilesDropDown(Guid branchId, string value, int patientStatus, bool isTitleIncluded, string title)
        {
            var items = new List<SelectListItem>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                var patients = ProfileRepository.Find(Current.AgencyId, branchId, patientStatus);
                if (patients.IsNotNullOrEmpty())
                {
                    items = patients.Where(patient => !patient.Id.IsEmpty()).Select(patient => new SelectListItem { Text = patient.DisplayNameWithMi.Trim().ToUpperCase(), Value = patient.Id.ToString(), Selected = patient.Id.ToString().IsEqual(value) }).OrderBy(o => o.Text).ToList();
                }
            }
            else if (Current.IsClinicianOrHHA)
            {
                var patients = GetUserPatientSelectionFromEpisodeInfo(branchId, Current.UserId, (byte)patientStatus);
                if (patients.IsNotNullOrEmpty())
                {
                    items = patients.Where(patient => !patient.Id.IsEmpty()).Select(patient => new SelectListItem { Text = patient.DisplayName.Trim().ToUpperCase(), Value = patient.Id.ToString(), Selected = patient.Id.ToString().IsEqual(value) }).OrderBy(o => o.Text).ToList();
                }
            }
            if (isTitleIncluded)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = Guid.Empty.ToString() });
            }
            return items.AsEnumerable();
        }

        public IEnumerable<SelectListItem> GenerateFilteredPatientsDropDown(string value, AgencyServices service, bool isTitleIncluded, string title)
        {
            var items = new List<SelectListItem>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                var patients = PatientRepository.Find(Current.AgencyId, 0, service);
                if (patients.IsNotNullOrEmpty())
                {
                    items = patients.Where(patient => !patient.Id.IsEmpty()).Select(patient => new SelectListItem { Text = patient.DisplayNameWithMi.Trim().ToTitleCase(), Value = patient.Id.ToString(), Selected = patient.Id.ToString().IsEqual(value) }).OrderBy(o => o.Text).ToList();
                }
            }
            else if (Current.IsClinicianOrHHA)
            {
                var patients = PatientRepository.GetPatientsWithUserAccess(Current.UserId, Current.AgencyId, Guid.Empty, 0, 0, string.Empty);
                if (patients.IsNotNullOrEmpty())
                {
                    items = patients.Where(patient => !patient.Id.IsEmpty()).Select(patient => new SelectListItem { Text = patient.DisplayName.Trim().ToTitleCase(), Value = patient.Id.ToString(), Selected = patient.Id.ToString().IsEqual(value) }).OrderBy(o => o.Text).ToList();
                }
            }
            if (isTitleIncluded)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = Guid.Empty.ToString() });
            }
            return items.AsEnumerable();
        }

        public IEnumerable<SelectListItem> GenerateFilteredPatientsDropDownOnlyFromPatientList(Guid branchId, string value, int patientStatus, bool isTitleIncluded, string title)
        {
            var items = new List<SelectListItem>();
            var patients = ProfileRepository.Find(Current.AgencyId, branchId, patientStatus);
            if (patients.IsNotNullOrEmpty())
            {
                items = patients
                    .Where(patient => !patient.Id.IsEmpty())
                    .Select(patient => new SelectListItem { Text = patient.DisplayNameWithMi.Trim().ToTitleCase(), Value = patient.Id.ToString(), Selected = patient.Id.ToString().IsEqual(value) }).OrderBy(i => i.Text).ToList();
            }
            if (isTitleIncluded)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = Guid.Empty.ToString() });
            }
            return items.AsEnumerable();
        }

        public IEnumerable<SelectListItem> GenerateFilteredUsersDropDown(string value)
        {
            var users = UserRepository.GetUsersByStatusAndServicesLean(Current.AgencyId, Guid.Empty, (int)UserStatus.Active, (int)Service) ?? new List<User>();
            var items = users.Select(user => new SelectListItem { Text = user.DisplayName, Value = user.Id.ToString(), Selected = user.Id.ToString().IsEqual(value) }).ToList();
            items.Insert(0, new SelectListItem { Text = "-- Select User --", Value = Guid.Empty.ToString() });
            return items;
        }

        public abstract IEnumerable<SelectListItem> PatientEpisodes(Guid patientId, string value, bool isTitleIncluded);

        public abstract string PatientSchedulesDateRangeFilter(string name, string value, object htmlAttributes);

        public abstract IEnumerable<SelectListItem> PreviousAssessments(Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate);

        public abstract IEnumerable<SelectListItem> PreviousCarePlans(Guid patientId, Guid assessmentId, DisciplineTasks task, DateTime scheduleDate);

        //public abstract List<SelectListItem> GetPatientChargeRateSelectList(Guid patientId, int disciplineTask, bool isTaskIncluded);

        public abstract List<PrivatePayor> GetPatientPrivatePayors(Guid patientId); 
        
        #endregion

        #region Methods

        protected string GenerateBegginningPatientSchedulesDateRangeFilter(string name, string value, object htmlAttributes)
        {
            var attributes = htmlAttributes.GetStyle();
            var selectList = new StringBuilder();
            selectList.AppendFormat("<select  name='{0}' {1}>", name, attributes);
            selectList.AppendFormat("<option value='All' {0} >All</option>", value.IsEqual("All").ToSelected());
            selectList.AppendFormat("<option  disabled='disabled'>---------------------------------------------</option>");
            selectList.AppendFormat("<option value='DateRange' {0} >Date Range</option>", value.IsEqual("DateRange").ToSelected());
            selectList.AppendFormat("<option  disabled='disabled'>---------------------------------------------</option>");
            return selectList.ToString();
        }

        protected List<SelectListItem> GenerateDropDownFromEpisodes(List<EpisodeLean> episodes, string value, bool isTitleIncluded, string title)
        {
            var items = new List<SelectListItem>();
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(e => items.Add(new SelectListItem { Text = string.Format("{0} - {1}", e.StartDate.ToZeroFilled(), e.EndDate.ToZeroFilled()), Value = e.Id.ToString(), Selected = e.Id.ToString().IsEqual(value) }));
            }
            if (isTitleIncluded)
            {
                if (title.IsNotNullOrEmpty())
                {
                    items.Insert(0, new SelectListItem { Text = title, Value = Guid.Empty.ToString() });
                }
            }
            return items;
        }

        protected List<SelectListItem> GenerateDropDownFromTasks<T>(List<T> scheduleTasks) where T : ITask, new()
        {
            var items = new List<SelectListItem>();
            if (scheduleTasks != null && scheduleTasks.Count > 0)
            {
                var userIds = scheduleTasks.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                scheduleTasks.ForEach(
                    v =>
                        {
                            if (Enum.IsDefined(typeof(DisciplineTasks), v.DisciplineTask))
                            {
                                var user = users.SingleOrDefault(u => u.Id == v.UserId);
                                items.Add(new SelectListItem { Text = string.Format("{0} {1} by {2}", ((DisciplineTasks)v.DisciplineTask).GetDescription(), v.EventDate.ToString("MM/dd/yyyy"), user != null ? user.DisplayName : string.Empty), Value = string.Format("{0}_{1}", v.Id, ((DisciplineTasks)v.DisciplineTask).ToString()) });
                            }
                        });
            }
            items.Insert(0, new SelectListItem { Text = "-- Select --", Value = string.Empty });
            return items;
        }

        protected string GenerateEndingPatientSchedulesDateRangeFilter(string value)
        {
            var selectList = new StringBuilder();
            selectList.AppendFormat("<option  disabled='disabled'>---------------------------------------------</option>");
            selectList.AppendFormat("<option value='Today' {0} >Today</option>", value.IsEqual("Today").ToSelected());
            selectList.AppendFormat("<option value='ThisWeek' {0} >This Week</option>", value.IsEqual("ThisWeek").ToSelected());
            selectList.AppendFormat("<option value='ThisWeekToDate' {0} >This Week-to-date</option>", value.IsEqual("ThisWeekToDate").ToSelected());
            selectList.AppendFormat("<option value='LastMonth' {0} >Last Month</option>", value.IsEqual("LastMonth").ToSelected());
            selectList.AppendFormat("<option value='ThisMonth' {0} >This Month</option>", value.IsEqual("ThisMonth").ToSelected());
            selectList.AppendFormat("<option value='ThisMonthToDate' {0} >This Month-to-date</option>", value.IsEqual("ThisMonthToDate").ToSelected());
            selectList.AppendFormat("<option value='ThisFiscalQuarter' {0} >This Fiscal Quarter</option>", value.IsEqual("ThisFiscalQuarter").ToSelected());
            selectList.AppendFormat("<option value='ThisFiscalQuarterToDate' {0} >This Fiscal Quarter-to-date</option>", value.IsEqual("ThisFiscalQuarterToDate").ToSelected());
            selectList.AppendFormat("<option value='ThisFiscalYear' {0} >This Fiscal Year</option>", value.IsEqual("ThisFiscalYear").ToSelected());
            selectList.AppendFormat("<option value='ThisFiscalYearToDate' {0} >This Fiscal Year-to-date</option>", value.IsEqual("ThisFiscalYearToDate").ToSelected());
            selectList.AppendFormat("<option value='Yesterday' {0} >Yesterday</option>", value.IsEqual("Yesterday").ToSelected());
            selectList.AppendFormat("<option value='LastWeek' {0} >Last Week</option>", value.IsEqual("LastWeek").ToSelected());
            selectList.AppendFormat("<option value='LastWeekToDate' {0} >Last Week-to-date</option>", value.IsEqual("LastWeekToDate").ToSelected());
            selectList.AppendFormat("<option value='LastMonthToDate' {0} >Last Month-to-date</option>", value.IsEqual("LastMonthToDate").ToSelected());
            selectList.AppendFormat("<option value='LastFiscalQuarter' {0} >Last Fiscal Quarter</option>", value.IsEqual("LastFiscalQuarter").ToSelected());
            selectList.AppendFormat("<option value='LastFiscalQuarterToDate' {0} >Last Fiscal Quarter-to-date</option>", value.IsEqual("LastFiscalQuarterToDate").ToSelected());
            selectList.AppendFormat("<option value='LastFiscalYear' {0} >Last Fiscal Year</option>", value.IsEqual("LastFiscalYear").ToSelected());
            selectList.AppendFormat("<option value='LastFiscalYearToDate' {0} >Last Fiscal Year-to-date</option>", value.IsEqual("LastFiscalYearToDate").ToSelected());
            selectList.AppendFormat("<option value='NextWeek' {0} >Next Week</option>", value.IsEqual("NextWeek").ToSelected());
            selectList.AppendFormat("<option value='Next4Weeks' {0} >Next 4 Weeks</option>", value.IsEqual("Next4Weeks").ToSelected());
            selectList.AppendFormat("<option value='NextMonth' {0} >Next Month</option>", value.IsEqual("NextMonth").ToSelected());
            selectList.AppendFormat("<option value='NextFiscalQuarter' {0} >Next Fiscal Quarter</option>", value.IsEqual("NextFiscalQuarter").ToSelected());
            selectList.AppendFormat("<option value='NextFiscalYear' {0} >Next Fiscal Year</option>", value.IsEqual("NextFiscalYear").ToSelected());
            selectList.Append("</select>");
            return selectList.ToString();
        }

        protected abstract List<EpisodeLean> GetPatientEpisodes(Guid patientId);

        //protected abstract List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, byte patientStatus);

        protected abstract List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, Guid userId, byte patientStatus);

        protected List<SelectListItem> GetPayorDisciplineTasks(int disciplineTask, bool isTaskIncluded, List<ChargeRate> rates)
        {
            var disciplines = new List<int>();
            if (rates.IsNotNullOrEmpty())
            {
                disciplines = rates.Select(i => i.Id).Distinct().ToList();
            }
            var items = new List<SelectListItem>();
            var isDisciplines = disciplines.IsNotNullOrEmpty();
            var disciplineTaskArray = LookupRepository.DisciplineTasks();
            var mainDisciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString());
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                items.AddRange(from disciplineTaskItem in disciplineTaskArray where (mainDisciplines.Contains(disciplineTaskItem.Discipline)) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote where !isTaskIncluded || !isDisciplines || !disciplines.Contains(disciplineTaskItem.Id) || disciplineTaskItem.Id == disciplineTask select new SelectListItem { Text = disciplineTaskItem.Task, Value = disciplineTaskItem.Id.ToString(), Selected = disciplineTaskItem.Id == disciplineTask });
            }
            items.Insert(0, new SelectListItem { Text = " -- Select Task --", Value = "0" });
            return items;
        }

        #endregion
    }

    /// <summary>
    /// Used for accessing the html helper abstraction without loading a helper built for a specific service.
    /// </summary>
    public class SharedHtmlHelper : HtmlHelperAbstraction
    {
        #region Constructors and Destructors

        public SharedHtmlHelper(ILookUpDataProvider lookUpDataProvider, IAgencyManagementDataProvider dataProvider)
            : base(lookUpDataProvider)
        {
            PatientRepository = dataProvider.PatientRepository;
        }

        #endregion

        #region Public Properties

        protected override AgencyServices Service
        {
            get
            {
                return AgencyServices.None;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override string CarePeriodLabel(object htmlAttributes)
        {
            throw new NotImplementedException();
        }

        //public override List<SelectListItem> ClaimDisciplineTask(string name, int disciplineTask, Guid claimId, ClaimTypeSubCategory typeOfClaim, int payorType, bool isTaskIncluded)
        //{
        //    throw new NotImplementedException();
        //}

        public override string DiagnosisRow(string diagnosisName, string diagnosis, string icd9, object htmlAttributes)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<SelectListItem> PatientEpisodes(Guid patientId, string value, bool isTitleIncluded)
        {
            throw new NotImplementedException();
        }

        public override string PatientSchedulesDateRangeFilter(string name, string value, object htmlAttributes)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<SelectListItem> PreviousAssessments(Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<SelectListItem> PreviousCarePlans(Guid patientId, Guid assessmentId, DisciplineTasks task, DateTime scheduleDate)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Methods

        protected override List<EpisodeLean> GetPatientEpisodes(Guid patientId)
        {
            throw new NotImplementedException();
        }

        //protected override List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, byte patientStatus)
        //{
        //    throw new NotImplementedException();
        //}

        protected override List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, Guid userId, byte patientStatus)
        {
            throw new NotImplementedException();
        }

        //public override List<SelectListItem> GetPatientChargeRateSelectList(Guid patientId, int disciplineTask, bool isTaskIncluded)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        public override List<PrivatePayor> GetPatientPrivatePayors(Guid patientId)
        {
            return null;
        }
    }

    public class HHHtmlHelper : HtmlHelperAbstraction
    {
        #region Fields

        private readonly HHBillingRepository billingRepository;
        private readonly HHEpisodeRepository episodeRepository;
        private readonly HHTaskRepository scheduleRepository;
        private readonly HHMongoRepository mongoRepository;

        #endregion

        #region Constructors and Destructors

        public HHHtmlHelper(ILookUpDataProvider lookUpDataProvider, HHDataProvider hhDataProvider)
            : base(lookUpDataProvider)
        {
            this.episodeRepository = hhDataProvider.EpisodeRepository;
            this.scheduleRepository = hhDataProvider.TaskRepository;
            this.billingRepository = hhDataProvider.BillingRepository;
            this.mongoRepository = hhDataProvider.MongoRepository;
            ProfileRepository = hhDataProvider.PatientProfileRepository;
            PatientRepository = hhDataProvider.PatientRepository;
            UserRepository = hhDataProvider.UserRepository;
        }

        #endregion

        #region Public Properties

        protected override AgencyServices Service
        {
            get
            {
                return AgencyServices.HomeHealth;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override string CarePeriodLabel(object htmlAttributes)
        {
            return TagBuilderFactory.BuildLabel(htmlAttributes.GetProperty<string>("class"), htmlAttributes.GetProperty<string>("for"), htmlAttributes.GetProperty<string>("value") + "Episode").ToString();
        }

        //public override List<SelectListItem> ClaimDisciplineTask(string name, int disciplineTask, Guid claimId, ClaimTypeSubCategory typeOfClaim, int payorType, bool isTaskIncluded)
        //{
        //    var rates = this.mongoRepository.GetClaimInsuranceRates(Current.AgencyId, claimId, typeOfClaim);
        //    return GetPayorDisciplineTasks(disciplineTask, isTaskIncluded, rates);
        //}

        public override string DiagnosisRow(string diagnosisName, string diagnosis, string icd9, object htmlAttributes)
        {
            string diagnosisNameWOSpace = diagnosisName.Replace(" ", string.Empty);
            var link = string.Empty;
            if (icd9.IsNotNullOrEmpty())
            {
                link = TagBuilderFactory.BuildLink("teachingguide", string.Format("http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c=en", icd9), "_blank", "Teaching Guide").ToString();
            }
            string span = TagBuilderFactory.BuildTag("span", htmlAttributes.GetProperty<string>("class"), diagnosisNameWOSpace, diagnosis).ToString();
            var innerDiv = TagBuilderFactory.BuildTag("div", "fr", span + link).ToString();
            var label = TagBuilderFactory.BuildLabel("fl strong", diagnosisNameWOSpace, diagnosisName + ":").ToString();
            var rowDiv = TagBuilderFactory.BuildTag("div", "row", label + innerDiv).ToString();
            return rowDiv;
        }

        public override IEnumerable<SelectListItem> PatientEpisodes(Guid patientId, string value, bool isTitleIncluded)
        {
            var episodes = new List<EpisodeLean>();
            if (!patientId.IsEmpty())
            {
                episodes = this.episodeRepository.GetPatientActiveEpisodesLeanWithNoDetail(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate).ToList();
            }
            return GenerateDropDownFromEpisodes(episodes, value, isTitleIncluded, "-- Select Episode --");
        }

        public override string PatientSchedulesDateRangeFilter(string name, string value, object htmlAttributes)
        {
            var selectList = new StringBuilder();
            selectList.Append(GenerateBegginningPatientSchedulesDateRangeFilter(name, value, htmlAttributes));
            selectList.AppendFormat("<option value='ThisEpisode' {0} >This Episode</option>", value.IsEqual("ThisEpisode").ToSelected());
            selectList.AppendFormat("<option value='LastEpisode' {0} >Last Episode</option>", value.IsEqual("LastEpisode").ToSelected());
            selectList.AppendFormat("<option value='NextEpisode' {0} >Next Episode</option>", value.IsEqual("NextEpisode").ToSelected());
            selectList.Append(GenerateEndingPatientSchedulesDateRangeFilter(value));
            return selectList.ToString();
        }

        public override IEnumerable<SelectListItem> PreviousAssessments(Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate)
        {
            var scheduleTasks = new List<ScheduleEvent>();
            if (assessmentType > 0)
            {
                var statusArray = ScheduleStatusFactory.OASISCompleted(true).ToArray();
                var disciplineTasks = assessmentType <= 9 ? DisciplineTaskFactory.AllAssessments(false).ToArray() : DisciplineTaskFactory.AllNonOASIS(false).ToArray();
                    // new int[] { (int)DisciplineTasks.OASISCStartOfCare, (int)DisciplineTasks.OASISCStartOfCarePT, (int)DisciplineTasks.OASISCStartOfCareOT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT } : new int[] { (int)DisciplineTasks.NonOASISStartOfCare, (int)DisciplineTasks.NonOASISRecertification };
                scheduleTasks = this.scheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, statusArray, new string[] { }, disciplineTasks, true, scheduleDate.AddMonths(-12), scheduleDate.AddDays(-1), false, false, 5);
            }
            return GenerateDropDownFromTasks(scheduleTasks);
        }

        public override IEnumerable<SelectListItem> PreviousCarePlans(Guid patientId, Guid assessmentId, DisciplineTasks task, DateTime scheduleDate)
        {
            var statusArray = ScheduleStatusFactory.OrdersCompleted(true).ToArray();
            var disciplineTasks = new[] { (int)task };
            var scheduleTasks = this.scheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, statusArray, new string[] { }, disciplineTasks, true, scheduleDate.AddMonths(-6), scheduleDate.AddDays(-1), false, false, 5);
            return GenerateDropDownFromTasks(scheduleTasks);
        }

        #endregion

        #region Methods

        protected override List<EpisodeLean> GetPatientEpisodes(Guid patientId)
        {
            return this.episodeRepository.GetPatientActiveEpisodesLeanWithNoDetail(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
        }

        //protected override List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, byte patientStatus)
        //{
        //    return this.episodeRepository.GetUserPatientsForDropDown(Current.AgencyId, branchId, Current.UserId, patientStatus);
        //}
        // TODO: need rewrite
        protected override List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, Guid userId, byte patientStatus)
        {
            var patientList = new List<PatientSelection>();
           var locations = new List<Guid>();
            if (branchId.IsEmpty())
            {
                locations = UserSessionEngine.LocationIds(Current.AgencyId, Current.UserId, Current.AcessibleServices, this.Service, Current.SessionId);
            }
            else
            {
                locations.Add(branchId);
            }
            if (locations.Count > 0)
            {
                patientList = this.episodeRepository.GetPatientEpisodeDataForList(Current.AgencyId, locations, userId, patientStatus);
            }
            return patientList;
        }

        //public override List<SelectListItem> GetPatientChargeRateSelectList(Guid patientId, int disciplineTask, bool isTaskIncluded)
        //{
        //    return new List<SelectListItem>();
        //}

        #endregion

        public override List<PrivatePayor> GetPatientPrivatePayors(Guid patientId)
        {
            return null;
        }
    }

    public class PDHtmlHelper : HtmlHelperAbstraction
    {
        #region Fields

        private readonly PrivateDutyEpisodeRepository episodeRepository;
        private readonly PrivateDutyTaskRepository scheduleRepository;
        private readonly PrivateDutyMongoRepository mongoRepository;
        private readonly PrivateDutyPatientProfileRepository profileRepository;

        #endregion

        #region Constructors and Destructors

        public PDHtmlHelper(ILookUpDataProvider lookUpDataProvider, PrivateDutyDataProvider pdDataProvider)
            : base(lookUpDataProvider)
        {
            this.episodeRepository = pdDataProvider.EpisodeRepository;
            this.scheduleRepository = pdDataProvider.TaskRepository;
            this.mongoRepository = pdDataProvider.MongoRepository;
            this.profileRepository = pdDataProvider.PatientProfileRepository;
            ProfileRepository = pdDataProvider.PatientProfileRepository;
            PatientRepository = pdDataProvider.PatientRepository;
            UserRepository = pdDataProvider.UserRepository;
        }

        #endregion

        #region Public Properties

        protected override AgencyServices Service
        {
            get
            {
                return AgencyServices.PrivateDuty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override string CarePeriodLabel(object htmlAttributes)
        {
            return TagBuilderFactory.BuildLabel(htmlAttributes.GetProperty<string>("class"), htmlAttributes.GetProperty<string>("for"), htmlAttributes.GetProperty<string>("value") + "Care Period").ToString();
        }

        //public override List<SelectListItem> ClaimDisciplineTask(string name, int disciplineTask, Guid claimId, ClaimTypeSubCategory typeOfClaim, int payorType, bool isTaskIncluded)
        //{
        //    var rates = new List<ChargeRate>();
        //    if (payorType == 10)
        //    {
        //        var billingAddress = this.mongoRepository.GetManagedClaimBillingAddress(Current.AgencyId, claimId);
        //        if (billingAddress != null)
        //        {
        //            rates = billingAddress.Rates;
        //        }
        //    }
        //    else if (payorType == 11)
        //    {
        //        rates = this.mongoRepository.GetClaimInsuranceRates(Current.AgencyId, claimId, typeOfClaim);
        //    }
        //    return GetPayorDisciplineTasks(disciplineTask, isTaskIncluded, rates);
        //}

      

        public override string DiagnosisRow(string diagnosisName, string diagnosis, string icd9, object htmlAttributes)
        {
            return string.Empty;
        }

        public override IEnumerable<SelectListItem> PatientEpisodes(Guid patientId, string value, bool isTitleIncluded)
        {
            List<EpisodeLean> episodes = null;
            if (!patientId.IsEmpty())
            {
                episodes = this.episodeRepository.GetPatientActiveEpisodesLeanWithNoDetail(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate).ToList();
            }
            return GenerateDropDownFromEpisodes(episodes, value, isTitleIncluded, "-- Select Care Period --");
        }

        public override string PatientSchedulesDateRangeFilter(string name, string value, object htmlAttributes)
        {
            var selectList = new StringBuilder();
            selectList.Append(GenerateBegginningPatientSchedulesDateRangeFilter(name, value, htmlAttributes));
            selectList.AppendFormat("<option value='ThisEpisode' {0} >This Care Period</option>", value.IsEqual("ThisEpisode").ToSelected());
            selectList.AppendFormat("<option value='LastEpisode' {0} >Last Care Period</option>", value.IsEqual("LastEpisode").ToSelected());
            selectList.AppendFormat("<option value='NextEpisode' {0} >Next Care Period</option>", value.IsEqual("NextEpisode").ToSelected());
            selectList.Append(GenerateEndingPatientSchedulesDateRangeFilter(value));
            return selectList.ToString();
        }

        public override IEnumerable<SelectListItem> PreviousAssessments(Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate)
        {
            var scheduleTasks = new List<PrivateDutyScheduleTask>();
            if (assessmentType > 0)
            {
                var statusArray = ScheduleStatusFactory.OASISCompleted(true).ToArray();
                var disciplineTasks = assessmentType <= 9 ? DisciplineTaskFactory.AllAssessments(false).ToArray() : DisciplineTaskFactory.AllNonOASIS(false).ToArray();
                    // new int[] { (int)DisciplineTasks.OASISCStartOfCare, (int)DisciplineTasks.OASISCStartOfCarePT, (int)DisciplineTasks.OASISCStartOfCareOT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT } : new int[] { (int)DisciplineTasks.NonOASISStartOfCare, (int)DisciplineTasks.NonOASISRecertification };
                scheduleTasks = this.scheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, statusArray, new string[] { }, disciplineTasks, true, scheduleDate.AddMonths(-12), scheduleDate.AddDays(-1), false, false, 5);
            }
            return GenerateDropDownFromTasks(scheduleTasks);
        }

        public override IEnumerable<SelectListItem> PreviousCarePlans(Guid patientId, Guid assessmentId, DisciplineTasks task, DateTime scheduleDate)
        {
            var statusArray = ScheduleStatusFactory.OrdersCompleted(true).ToArray();
            var disciplineTasks = new[] { (int)task };
            var scheduleTasks = this.scheduleRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, statusArray, new string[] { }, disciplineTasks, true, scheduleDate.AddMonths(-6), scheduleDate.AddDays(-1), false, false, 5);
            return GenerateDropDownFromTasks(scheduleTasks);
        }

        #endregion

        #region Methods

        protected override List<EpisodeLean> GetPatientEpisodes(Guid patientId)
        {
            return this.episodeRepository.GetPatientActiveEpisodesLeanWithNoDetail(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
        }

        //protected override List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, byte patientStatus)
        //{
        //    return null; // episodeRepository.GetUserPatients(Current.AgencyId, Current.UserId, patientStatus, (int)service);
        //}

        protected override List<PatientSelection> GetUserPatientSelectionFromEpisodeInfo(Guid branchId, Guid userId, byte patientStatus)
        {
            return null; // episodeRepository.GetUserPatients(Current.AgencyId, branchId, userId, patientStatus);
        }

        //public override List<SelectListItem> GetPatientChargeRateSelectList(Guid patientId, int disciplineTask, bool isTaskIncluded)
        //{
        //    var rates = mongoRepository.GetPatientVisitRates(Current.AgencyId, patientId);
        //    var disciplines =rates.IsNotNullOrEmpty()? rates.Select(i => i.Id).ToList():new List<int>();
        //    var tasks = LookupRepository.DisciplineTasksForRates(DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToList(), disciplines, new List<int> { (int)DisciplineTasks.CommunicationNote }, disciplineTask, isTaskIncluded);
        //    return tasks.GenerateSelectListItems(args => args.Task, args => args.Id.ToString(), args => true, disciplineTask.ToString(), true, "-- Select Task --", "0");
        //}

        #endregion

        public override List<PrivatePayor> GetPatientPrivatePayors(Guid patientId)
        {
            return  profileRepository.GetPatientPrivatePayorLean(Current.AgencyId, patientId);
        }
    }
}