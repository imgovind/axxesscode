﻿
namespace Axxess.AgencyManagement.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Entities;
    using Axxess.LookUp.Repositories;

    public static class AgencyInformationHelper
    {

        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        //private static ILookupRepository lookupRepository
        //{
        //    get
        //    {
        //        ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
        //        return dataProvider.LookUpRepository;
        //    }
        //}

        public static List<InsuranceViewData> GetInsurances()
        {
            var insuranceList = new List<InsuranceViewData>();
            Array list = Enum.GetValues(typeof(MedicareIntermediary));
            foreach (MedicareIntermediary item in list)
            {
                insuranceList.Add(new InsuranceViewData
                {
                    Name = item.GetDescription(),
                    Id = (int)item
                });
            }
            var agencyNonMedicareInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyNonMedicareInsurances != null && agencyNonMedicareInsurances.Count > 0)
            {
                agencyNonMedicareInsurances.ForEach(i =>
                {
                    insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
                });
            }
            return insuranceList;
        }

        //public static List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded)
        //{
        //    var items = new List<SelectListItem>();
        //    if (IsMedicareTradIncluded)
        //    {
        //        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
        //        if (agency != null)
        //        {
        //            var payorId = 0;
        //            if (int.TryParse(agency.Payor, out payorId))
        //            {

        //                // var standardInsurance = lookupRepository.GetInsurance(payorId);
        //                var name = Enum.IsDefined(typeof(MedicareIntermediary), payorId) ? ((MedicareIntermediary)payorId).GetDescription() : string.Empty;
        //                if (name.IsNotNullOrEmpty())
        //                {
        //                    items.Add(new SelectListItem
        //                    {
        //                        Text = name,
        //                        Value = payorId.ToString(),
        //                        Selected = (payorId.ToString().IsEqual(value))
        //                    });
        //                }
        //            }
        //        }
        //    }
        //    if (IsAll)
        //    {
        //        items.Insert(0, new SelectListItem
        //        {
        //            Text = "All",
        //            Value = "0"
        //        });
        //    }
        //    var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
        //    if (agencyInsurances != null && agencyInsurances.Count > 0)
        //    {
        //        agencyInsurances.ForEach(i =>
        //        {
        //            items.Add(new SelectListItem()
        //            {
        //                Text = i.Name,
        //                Value = i.Id.ToString(),
        //                Selected = (i.Id.ToString().IsEqual(value))
        //            });
        //        });
        //    }
        //    return items;
        //}

        //public static List<SelectListItem> Branches(string value, bool IsAll)
        //{
        //    var items = new List<SelectListItem>();
        //    var branches = agencyRepository.GetBranches(Current.AgencyId);
        //    if (branches.IsNotNullOrEmpty())
        //    {
        //       var tempItems = from branch in branches
        //                    select new SelectListItem
        //                    {
        //                        Text = branch.Name,
        //                        Value = branch.Id.ToString(),
        //                        Selected = (branch.Id.ToString().IsEqual(value))
        //                    };
        //        items.AddRange(tempItems);
        //    }
        //    if (IsAll)
        //    {
        //        items.Insert(0, new SelectListItem
        //        {
        //            Text = "-- All Branches --",
        //            Value = Guid.Empty.ToString(),
        //        });
        //    }
        //    return items;
        //}

        //public static FilterViewData GetInsuranceAndBranchFilter()
        //{
        //    var filter = GetInsuranceAndBranchFilterWithOutLists();
        //    if (filter != null)
        //    {
        //        filter.Branches = Branches(filter.SelecetdBranch.ToString(), false);
        //        filter.Insurances = Insurances(filter.SelecetdInsurance.ToString(), false, true);
        //    }
        //    return filter;
        //}

        //public static FilterViewData GetInsuranceAndBranchFilterWithOutLists()
        //{
        //    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
        //    var filterViewData = new FilterViewData();
        //    if (agency != null)
        //    {
        //        var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
        //        if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
        //        {
        //            int payorType;
        //            if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
        //            {
        //                filterViewData.SelecetdBranch = agencyMainBranch.Id;
        //                filterViewData.SelecetdInsurance = payorType;
        //            }
        //            else
        //            {
        //                var agencyMedicareInsurance = agencyRepository.GetFirstAgencyInsuranceByPayer(Current.AgencyId, new int[] { (int)PayerTypes.MedicareTraditional, (int)PayerTypes.MedicareHMO });//Where(i => i.PayorType == (int)PayerTypes.MedicareTraditional || i.PayorType == (int)PayerTypes.MedicareHMO).OrderBy(i => i.Id).ToList().FirstOrDefault();
        //                if (agencyMedicareInsurance != null)
        //                {
        //                    filterViewData.SelecetdBranch = agencyMainBranch.Id;
        //                    filterViewData.SelecetdInsurance = agencyMedicareInsurance.Id;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            var agencyBranch = agencyRepository.GetFirstLoction(Current.AgencyId);
        //            if (agencyBranch != null)
        //            {
        //                var agencyMedicareInsurance = agencyRepository.GetFirstAgencyInsuranceByPayer(Current.AgencyId, new int[] { (int)PayerTypes.MedicareTraditional, (int)PayerTypes.MedicareHMO });//.Where(i => i.PayorType == (int)PayerTypes.MedicareTraditional || i.PayorType == (int)PayerTypes.MedicareHMO).OrderBy(i => i.Id).ToList().FirstOrDefault();
        //                if (agencyMedicareInsurance != null)
        //                {
        //                    filterViewData.SelecetdBranch = agencyBranch.Id;
        //                    filterViewData.SelecetdInsurance = agencyMedicareInsurance.Id;
        //                }
        //            }
        //        }
        //    }
        //    return filterViewData;
        //}

      

        //public static FilterViewData GetInsuranceFilter()
        //{
        //    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
        //    var selectionLists = new FilterViewData();
        //    if (agency != null)
        //    {
        //        selectionLists = GetInsuranceFilterHelper(agency.Payor);
        //    }
        //    return selectionLists;
        //}

        //private static FilterViewData GetInsuranceFilterHelper(string payor)
        //{
        //    var selectionLists = new FilterViewData();
        //    int payorType;
        //    if (payor.IsNotNullOrEmpty() && int.TryParse(payor, out payorType))
        //    {
        //        selectionLists.Insurances = Insurances(payor.ToString(), false, true);
        //        selectionLists.SelecetdInsurance = payorType;
        //    }
        //    else
        //    {
        //        var agencyMedicareInsurance = agencyRepository.GetFirstAgencyInsuranceByPayer(Current.AgencyId, new int[] { (int)PayerTypes.MedicareTraditional, (int)PayerTypes.MedicareHMO });// var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
        //        if (agencyMedicareInsurance != null)
        //        {
        //            selectionLists.Insurances = Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
        //            selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id;
        //        }
        //    }
        //    return selectionLists;
        //}

        //public static List<object> MedicareInsuranceSelectList(Guid branchId)
        //{
        //    var list = new List<object>();
        //    var payor = Payor(Current.AgencyId, branchId);
        //    if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
        //    {
        //        var name = ((MedicareIntermediary)payor).GetDescription();
        //        if (name.IsNotNullOrEmpty())
        //        {
        //            list.Add(new { Name = name, Id = payor });
        //        }
        //    }
        //    return list;
        //}

        //public static List<object> MedicareWithHMOInsuranceSelectList(Guid branchId)
        //{
        //    var list = new List<object>();
        //    var payor = Payor(Current.AgencyId, branchId);
        //    if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
        //    {
        //        var name = ((MedicareIntermediary)payor).GetDescription();
        //        if (name.IsNotNullOrEmpty())
        //        {
        //            list.Add(new { Name = name, Id = payor, Selected = true });
        //        }
        //    }
            
        //    var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
        //    if (agencyInsurances != null && agencyInsurances.Count > 0)
        //    {
        //        agencyInsurances.ForEach(i =>
        //        {
        //            if (i.PayorType == (int)PayerTypes.MedicareHMO)
        //            {
        //                list.Add(new { Name = i.Name, Id = i.Id, Selected = false });
        //            }
        //        });
        //    }
        //    return list;
        //}

        //public static List<object> InsuranceSelectList(Guid branchId, int service,int claimType, bool isMedicareANDMedicareHMOOnly)
        //{
        //    var list = new List<object>();
        //    var excludedPayorTypes = new List<int>();
        //    var count = 0;
        //    if (service == (int)AgencyServices.HomeHealth)
        //    {
        //        if (claimType != (int)ClaimTypeSubCategory.ManagedCare)
        //        {
        //            var payor = Payor(Current.AgencyId, branchId);
        //            if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
        //            {
        //                var name = ((MedicareIntermediary)payor).GetDescription();
        //                if (name.IsNotNullOrEmpty())
        //                {
        //                    list.Add(new { Name = name, Id = payor, Selected = count == 0 ? true : false });
        //                    count++;
        //                }
        //            }
        //        }
        //    }
        //    else if (service == (int)AgencyServices.PrivateDuty)
        //    {
        //        excludedPayorTypes.Add((int)PayerTypes.Selfpay);
        //    }
        //    var agencyInsurances = agencyRepository.GetAgencyInsuranceSelection(Current.AgencyId, isMedicareANDMedicareHMOOnly, excludedPayorTypes);
        //    if (agencyInsurances != null && agencyInsurances.Count > 0)
        //    {
        //        agencyInsurances.ForEach(i =>
        //        {
        //            list.Add(new { Name = i.Name, Id = i.Id, Selected = count == 0 ? true : false });
        //            count++;
        //        });
        //    }
        //    return list;
        //}

        public static StringBuilder PatientInsurances(Guid branchId, int service)
        {
            var selectList = new StringBuilder();
            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>-- Select Insurance --</option>", "0", 0);
            if (!Current.IsAgencyFrozen && Current.HasRight(service,ParentPermission.Insurance, PermissionActions.Add))
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            }
            var payor = Payor(Current.AgencyId, branchId);
            if (Enum.IsDefined(typeof(MedicareIntermediary), payor))
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", payor.ToString(), 0, ((MedicareIntermediary)payor).GetDescription());
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances.ForEach(insurance =>
                {
                    selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", insurance.Id.ToString(), 1, insurance.Name);
                });
            }
            return selectList;
        }

      
        public static int Payor(Guid agencyId, Guid branchId)
        {
            var payor = 0;
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(agencyId, branchId);
                int.TryParse(location != null && location.IsLocationStandAlone ? location.Payor : Current.Payor, out payor);
                //if (location != null && location.IsLocationStandAlone)
                //{
                //    int.TryParse(location.Payor, out payor);
                //}
                //else
                //{
                //    //var agency = agencyRepository.GetAgencyOnly(agencyId);
                //    int.TryParse(Current.Payor, out payor);
                //}
            }
            else
            {
                //var agency = agencyRepository.GetAgencyOnly(agencyId);
                int.TryParse(Current.Payor, out payor);
            }
            return payor;
        }


        public static bool IsMedicareOrMedicareHMOInsurance(int insuranceId)
        {
            bool IsMedicareOrMedicareHMOInsurance = false;
           // int insuranceId = 0;

            //if (int.TryParse(insuranceStringId, out insuranceId) && insuranceId > 0)
            //{
            if (Enum.IsDefined(typeof(MedicareIntermediary), insuranceId))
            {
                IsMedicareOrMedicareHMOInsurance = true;
            }
            else
            {
                var insurance = agencyRepository.FindInsurance(Current.AgencyId, insuranceId);
                if (insurance != null && insurance.PayorType == (int)PayerTypes.MedicareHMO)
                {
                    IsMedicareOrMedicareHMOInsurance = true;
                }
            }
            //}
            //insuranceIdOut = insuranceId;
            return IsMedicareOrMedicareHMOInsurance;
        }
    }
}
