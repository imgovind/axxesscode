﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Infrastructure;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.AgencyManagement.Entities;
using Axxess.AgencyManagement.Application.ViewData;
using System.Web.Mvc;
using Axxess.AgencyManagement.Repositories;

namespace Axxess.AgencyManagement.Application.Helpers
{
    public abstract class AdmissionHelper<A> where A : CarePeriod,new()
    {
        protected readonly IPatientRepository patientRepository;
        protected readonly PatientProfileAbstract profileRepository;
        protected readonly PatientAdmissionAbstract admissionRepository;
        public AdmissionHelper(IPatientRepository patientRepository, PatientProfileAbstract profileRepository, PatientAdmissionAbstract admissionRepository)
        {
            this.patientRepository = patientRepository;
            this.profileRepository = profileRepository;
            this.admissionRepository = admissionRepository;
        }

        public bool AddPatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            return admissionRepository.AddPatientAdmissionDate(managedDate);
        }

        public PatientAdmissionDate AddPatientAdmissionDate(Patient patient, Profile profile)
        {
            var admission = new PatientAdmissionDate
            {
                Id = Guid.NewGuid(),
                AgencyId = Current.AgencyId,
                PatientId = profile.Id,
                StartOfCareDate = profile.StartofCareDate,
                DischargedDate = profile.DischargeDate,
                Status = profile.Status,
                IsActive = true,
                IsDeprecated = false,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                PatientData = patient.ToXml(),
                ProfileData = profile.ToXml()
            };
            if (admissionRepository.AddPatientAdmissionDate(admission))
            {
                if (profileRepository.UpdatePatientForAdmissionDateId(Current.AgencyId, profile.Id, admission.Id))
                {
                    return admission;
                }
                else
                {
                    admissionRepository.RemovePatientAdmissionDate(Current.AgencyId, profile.Id, admission.Id);
                }
            }
            return null;
        }

        public PatientAdmissionDate AddPatientAdmissionDate(Profile profile)
        {
            PatientAdmissionDate admission = null;
            var patient = patientRepository.GetPatientOnly(profile.Id, Current.AgencyId);
            if (patient != null)
            {
                admission = AddPatientAdmissionDate(patient, profile);
            }
            return admission;
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid patientId, Guid Id)
        {
            return admissionRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
        }

        public PatientAdmissionDate GetPatientLatestAdmissionDate(Guid patientId, DateTime date)
        {
            return admissionRepository.GetPatientLatestAdmissionDate(Current.AgencyId, patientId, date);
        }

        public bool RemovePatientAdmissionDate(Guid patientId, Guid Id)
        {
            return admissionRepository.RemovePatientAdmissionDate(Current.AgencyId, patientId, Id);
        }

        public bool RemovePatientAdmissionDates(Guid patientId)
        {
            return admissionRepository.RemovePatientAdmissionDates(Current.AgencyId, patientId);
        }
      
        public bool UpdatePatientAdmissionDate(PatientAdmissionDate admissionData)
        {
            return admissionRepository.UpdatePatientAdmissionDate(admissionData);
        }

        public List<SelectListItem> GetAdmissionSelectList(Profile profile, bool createNewAdmissionIfNotExist)
        {
            var selection = new List<SelectListItem>();
            if (!profile.AdmissionId.IsEmpty())
            {
                selection = admissionRepository.GetPatientAdmissionDateSelectList(Current.AgencyId, profile.Id).ToList();
            }
            if (createNewAdmissionIfNotExist && !selection.IsNotNullOrEmpty())
            {
                var admission = AddPatientAdmissionDate(profile);
                if (admission != null)
                {
                    selection.Add(new SelectListItem { Text = admission.StartOfCareDate.ToString("MM/dd/yyy"), Value = admission.Id.ToString(), Selected = true });
                }
            }
            return selection;
        }

    }

   public class HHAdmissionHelper :AdmissionHelper<PatientEpisode>
   {
       public HHAdmissionHelper(HHDataProvider dataProvider)
           : base(dataProvider.PatientRepository,dataProvider.PatientProfileRepository,dataProvider.PatientAdmissionRepository)
        {
        }
   }

   public class PDAdmissionHelper : AdmissionHelper<PrivateDutyCarePeriod>
   {
       public PDAdmissionHelper(PrivateDutyDataProvider dataProvider)
           : base(dataProvider.PatientRepository,dataProvider.PatientProfileRepository,dataProvider.PatientAdmissionRepository)
        {
        }
   }

   public class AdmissionHelperFactory<A> where A : CarePeriod, new()
   {
       private static readonly AdmissionHelper<A> admissionHelper = Container.Resolve<AdmissionHelper<A>>();

       public static bool AddPatientAdmissionDate(PatientAdmissionDate managedDate)
       {
         return  admissionHelper.AddPatientAdmissionDate(managedDate);
       }

       public static PatientAdmissionDate GetPatientAdmissionDate(Guid patientId, Guid Id)
       {
           return admissionHelper.GetPatientAdmissionDate(patientId, Id);
       }

       public static PatientAdmissionDate GetPatientLatestAdmissionDate(Guid patientId, DateTime date)
       {
           return admissionHelper.GetPatientLatestAdmissionDate( patientId, date);
       }

       public static bool UpdatePatientAdmissionDate(PatientAdmissionDate admissionData)
       {
           return admissionHelper.UpdatePatientAdmissionDate(admissionData);
       }

       public static bool RemovePatientAdmissionDate(Guid patientId, Guid Id)
       {
           return admissionHelper.RemovePatientAdmissionDate( patientId, Id);
       }

       public static bool RemovePatientAdmissionDates(Guid patientId)
       {
           return admissionHelper.RemovePatientAdmissionDates(patientId);
       }

       public static List<SelectListItem> GetAdmissionSelectList(Profile profile, bool createNewAdmissionIfNotExist)
       {
           return admissionHelper.GetAdmissionSelectList(profile, createNewAdmissionIfNotExist);
       }
       

   }

}
