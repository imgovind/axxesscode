﻿//namespace Axxess.AgencyManagement.Application.Helpers
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;
//    using Axxess.Core.Extension;
//    using Axxess.AgencyManagement.Entities;
//    using Axxess.AgencyManagement.Repositories;
//    using Axxess.AgencyManagement.Application.ViewData;
//    using Axxess.AgencyManagement.Entities.Repositories;
//    using Axxess.AgencyManagement.Entities.Enums;
//    using Axxess.Core.Infrastructure;

//    internal abstract class PatientSelectionHelper<E> where E : CarePeriod, new()
//    {
//        protected IPatientRepository patientRepository;
//        protected PatientProfileAbstract patientProfileRepository;
//        protected AgencyServices Service { get; set; }


//        //public List<PatientSelection> GetPatientSelection(Guid branchId, byte statusId, byte paymentSourceId, string name, bool checkAuditor)
//        //{
//        //    var patientList = new List<PatientSelection>();
//        //    if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
//        //    {
//        //        patientList = patientProfileRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId, name);
//        //    }
//        //    else if (Current.IsClinicianOrHHA)
//        //    {
//        //        GetPatientSelectionAppSpecific(branchId, statusId);
//        //    }
//        //    else if (checkAuditor && Current.IfOnlyRole(AgencyRoles.Auditor))
//        //    {
//        //        patientList = patientProfileRepository.GetAuditorPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId, name, Current.UserId);
//        //        if (patientList == null)
//        //        {
//        //            patientList = new List<PatientSelection>();
//        //        }
//        //    }
//        //    else
//        //    {
//        //    }

//        //    return patientList;
//        //}

//        //public PatientSelectionViewData GetPatientChartsDataWithOutSchedules(Guid patientId, int status, ref  Profile profile)
//        //{
//        //    var viewData = new PatientSelectionViewData();
//        //    var patients = GetPatientSelection(patientId, status, ref profile);
//        //    if (patients != null && patients.Count > 0)
//        //    {
//        //        viewData.Count = patients.Count;
//        //        viewData.Patients = patients;
//        //        var currentPatientId = profile != null ? profile.Id : Guid.Empty;
//        //        if (profile != null && !currentPatientId.IsEmpty() && patients.Exists(p => p.Id == currentPatientId))
//        //        {
//        //            viewData.CurrentPatientId = profile.Id;
//        //            viewData.PatientListStatus = profile.Status;
//        //            viewData.IsDischarged = profile.IsDischarged;
//        //        }
//        //        else
//        //        {
//        //            var patientForID = patients.FirstOrDefault();
//        //            if (patientForID != null && !patientForID.Id.IsEmpty())
//        //            {
//        //                profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientForID.Id);
//        //                if (profile != null)
//        //                {
//        //                    viewData.CurrentPatientId = patientForID.Id;
//        //                    viewData.IsDischarged = profile.IsDischarged;

//        //                }
//        //            }
//        //            viewData.PatientListStatus = status;
//        //        }
//        //    }
//        //    else
//        //    {
//        //        viewData.PatientListStatus = status;
//        //        viewData.Count = 0;
//        //    }
//        //    return viewData;
//        //}

//        //public PatientSelectionViewData GetScheduleCenterDataWithOutSchedules(Guid patientId, int status)
//        //{
//        //    var viewData = new PatientSelectionViewData();
//        //    Profile profile = null;
//        //    var patients = GetPatientSelection(patientId, status, ref profile);
//        //    if (patients != null && patients.Count > 0)
//        //    {
//        //        viewData.Count = patients.Count;
//        //        viewData.Patients = patients;
//        //        if (profile != null && patients.Exists(p => p.Id == profile.Id))
//        //        {
//        //            // viewData.CalendarData = scheduleService.GetScheduleWithPreviousAfterEpisodeInfo(patient.Id, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();

//        //            viewData.CurrentPatientId = profile.Id;
//        //            viewData.IsDischarged = profile.IsDischarged;
//        //            viewData.PatientListStatus = profile.Status;
//        //            viewData.DisplayName = string.Format("{0}, {1}", profile.LastName.ToUpperCase(), profile.FirstName.ToUpperCase());
//        //        }
//        //        else
//        //        {
//        //            var patientForID = patients.FirstOrDefault();
//        //            if (patientForID != null && !patientForID.Id.IsEmpty())
//        //            {
//        //                // viewData.CalendarData = scheduleService.GetScheduleWithPreviousAfterEpisodeInfo(patientForID.Id, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();
//        //                viewData.CurrentPatientId = patientForID.Id;
//        //                viewData.IsDischarged = patientForID.IsDischarged;
//        //                viewData.DisplayName = string.Format("{0}, {1}", patientForID.LastName.ToUpperCase(), patientForID.FirstName.ToUpperCase());

//        //            }
//        //            viewData.PatientListStatus = status;
//        //        }
//        //    }
//        //    else
//        //    {
//        //        viewData.PatientListStatus = status;
//        //        viewData.Count = 0;
//        //    }
//        //    return viewData;
//        //}

//        //private List<PatientSelection> GetPatientSelection(Guid patientId, int status, ref Profile profile)
//        //{
//        //    var patients = new List<PatientSelection>();
//        //    if (!patientId.IsEmpty())
//        //    {
//        //        profile = patientProfileRepository.GetProfileOnly(Current.AgencyId, patientId);
//        //        if (profile != null && (profile.Status == (int)PatientStatus.Active || profile.Status == (int)PatientStatus.Discharged))
//        //        {
//        //            status = profile.Status;
//        //        }
//        //    }
//        //    patients = this.GetPatientSelection(Guid.Empty, (byte)status, 0, "", false) ?? new List<PatientSelection>();

//        //    var patientsAccess = patientProfileRepository.GetPatientsWithUserAccess(Current.UserId, Current.AgencyId, Guid.Empty, status, 0, "");
//        //    if (patientsAccess != null)
//        //    {
//        //        foreach (var p in patientsAccess)
//        //        {
//        //            var search = patients.Find(pa => pa.DisplayName == p.DisplayName);
//        //            if (search == null)
//        //            {
//        //                patients.Add(p);
//        //            }
//        //            search = null;
//        //        }
//        //    }

//        //    return patients;
//        //}



//        //protected abstract List<PatientSelection> GetPatientSelectionAppSpecific(Guid branchId, byte statusId);

        

//    }

//    internal class HHPatientSelectionHelper : PatientSelectionHelper<PatientEpisode>
//    {
//        private HHEpisodeRepository episodeRepository;
//        public HHPatientSelectionHelper(HHDataProvider dataProvider)
//        {
//            this.episodeRepository = dataProvider.EpisodeRepository;
//            base.patientRepository = dataProvider.PatientRepository;
//            base.patientProfileRepository = dataProvider.PatientProfileRepository;
//            base.Service = AgencyServices.HomeHealth;
//        }

//        //protected override List<PatientSelection> GetPatientSelectionAppSpecific(Guid branchId, byte statusId)
//        //{
//        //    return episodeRepository.GetPatientEpisodeDataForList(Current.AgencyId, branchId, Current.UserId, statusId);
//        //}
//    }

//    internal class PrivateDutyPatientSelectionHelper : PatientSelectionHelper<PrivateDutyCarePeriod>
//    {
//        public PrivateDutyPatientSelectionHelper(PrivateDutyDataProvider dataProvider)
//        {
//            base.patientRepository = dataProvider.PatientRepository;
//            base.patientProfileRepository = dataProvider.PatientProfileRepository;
//            base.Service = AgencyServices.PrivateDuty;
//        }

//        //protected override List<PatientSelection> GetPatientSelectionAppSpecific(Guid branchId, byte statusId)
//        //{
//        //    return null;
//        //}
//    }

//    public class PatientSelectionFactory<E> where E : CarePeriod, new()
//    {
//        private static readonly PatientSelectionHelper<E> patientSelectionHelper = Container.Resolve<PatientSelectionHelper<E>>();

//        //public static List<PatientSelection> GetPatientSelection(Guid branchId, byte statusId, byte paymentSourceId, string name, bool checkAuditor)
//        //{
//        //    return patientSelectionHelper.GetPatientSelection(branchId, statusId, paymentSourceId, name, checkAuditor);
//        //}
       
//        //public static PatientSelectionViewData GetPatientChartsDataWithOutSchedules(Guid patientId, int status, ref  Profile profile)
//        //{
//        //    return patientSelectionHelper.GetPatientChartsDataWithOutSchedules(patientId, status, ref profile);
//        //}
       
//        //public static PatientSelectionViewData GetScheduleCenterDataWithOutSchedules(Guid patientId, int status)
//        //{
//        //    return patientSelectionHelper.GetScheduleCenterDataWithOutSchedules(patientId, status);
//        //}

//    } 
//}
