﻿namespace Axxess.AgencyManagement.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Entities.Common;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using System.Web;
    using Axxess.AgencyManagement.Entities;
    using System.IO;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;

    public static class AssetHelper
    {
        private static IAssetRepository assetRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AssetRepository;
            }
        }

        public static Asset Get(Guid agencyId, Guid id)
        {
            return assetRepository.Get(agencyId, id);
        }

        public static bool AddAssetAndSetToSchedule<T>(T scheduleEventToEdit, HttpFileCollectionBase httpFiles) where T : ITask, new()
        {
            var assetsSaved = false;
            var assets = CreateAssetHelper(httpFiles);
            if (assets.IsNotNullOrEmpty())
            {
                if (assetRepository.AddMany(assets))
                {
                    assetsSaved = true;
                    scheduleEventToEdit.Assets = (scheduleEventToEdit.Asset.IsNotNullOrEmpty() ? scheduleEventToEdit.Asset.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();
                    scheduleEventToEdit.Assets.AddRange(assets.Select(a => a.Id));
                    scheduleEventToEdit.Asset = scheduleEventToEdit.Assets.ToXml();
                }
            }
            return assetsSaved;
        }

        /// <summary>
        /// Saves assets to the database. Uses the ids that are generated to add them to the schedule event and questions
        /// </summary>
        /// <typeparam name="T">The type of questions used</typeparam>
        /// <param name="httpFiles">The files to be added</param>
        /// <param name="scheduleTaskAssets">The schedule task's assets</param>
        /// <param name="questions">The questions that will have the asset ids added to.</param>
        /// <returns></returns>
        public static List<Guid> SaveAssetAndAddToXml<T>(HttpFileCollectionBase httpFiles, List<Guid> scheduleTaskAssets, IDictionary<string, T> questions)
            where T : QuestionBase, new()
        {
            var assetsAdded = new List<Guid>();
            if (httpFiles != null && httpFiles.Count > 0)
            {
                List<Asset> assets = null;
                var allKeys = httpFiles.AllKeys;
                if (AddAssets(httpFiles, allKeys, out assets) && assets.IsNotNullOrEmpty())
                {
                    foreach (var asset in assets)
                    {
                        if (allKeys.Contains(asset.FieldId))
                        {
                            var key = allKeys.FirstOrDefault(k => k == asset.FieldId);
                            if (key.IsNotNullOrEmpty())
                            {
                                var keyArray = key.Split('_');
                                if (keyArray.Length >= 2)
                                {
                                    if (!scheduleTaskAssets.Contains(asset.Id))
                                    {
                                        scheduleTaskAssets.Add(asset.Id);
                                    }
                                    questions[keyArray[1]] = QuestionFactory<T>.Create(keyArray[1], asset.Id.ToString());
                                    assetsAdded.Add(asset.Id);
                                }
                            }
                        }
                    }
                }
            }
            return assetsAdded;
        }

        public static bool Delete(Guid assetId, List<Guid> scheduleTaskAssets)
        {
            bool result = false;
            if (!assetId.IsEmpty())
            {
                if (scheduleTaskAssets.Remove(assetId))
                {
                    if (Delete(assetId))
                    {
                        result = true;
                    }
                    else
                    {
                        scheduleTaskAssets.Add(assetId);
                    }
                }
            }
            return result;
        }

        public static bool AddAssets(HttpFileCollectionBase httpFiles, out List<Asset> assetsOut)
        {
            return AddAssets(httpFiles, httpFiles.AllKeys, false, out assetsOut);
        }

        public static bool AddAssets(HttpFileCollectionBase httpFiles, string[] allkeys, out List<Asset> assetsOut)
        {
            return AddAssets(httpFiles, allkeys, false, out assetsOut);
        }

        /// <summary>
        /// Adds the files passed to it to the asset repository
        /// </summary>
        /// <param name="httpFiles">The files sent to the controller</param>
        /// <param name="allkeys">The names of the inputs used in the submission.</param>
        /// <param name="isResized">Used for images that need to be resized</param>
        /// <param name="assetsOut">Returns the assets that were created, so that they may be deleted if the request fails further on.</param>
        /// <returns>The result of attempting to add assets.</returns>
        public static bool AddAssets(HttpFileCollectionBase httpFiles, string[] allkeys, bool isResized, out List<Asset> assetsOut)
        {
            var assetsSaved = false;
            assetsOut = null;
            var assets = CreateAssetHelper(httpFiles, allkeys, isResized);
            if (assets.IsNotNullOrEmpty())
            {
                if (assetRepository.AddMany(assets))
                {
                    assetsOut = assets;
                    assetsSaved = true;
                }
            }
            return assetsSaved;
        }

        public static bool AddAsset(HttpPostedFileBase file, out Guid assetId)
        {
            return AddAsset(file, out assetId, false);
        }

        public static bool AddAsset(HttpPostedFileBase file, out Guid assetId, bool isResize)
        {
            assetId = Guid.Empty;
            var asset = CreateAssetFromFile(file, isResize);
            var isAssetSaved = false;
            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                assetId = asset.Id;
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;
                isAssetSaved = assetRepository.Add(asset);
            }
            return isAssetSaved;
        }


        public static bool AddAsset(Asset asset)
        {
            var isAssetSaved = false;
            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;
                isAssetSaved = assetRepository.Add(asset);
            }
            return isAssetSaved;
        }

        public static bool AddAssetByIndex(HttpFileCollectionBase httpFiles, int index, out Guid assetId, bool isResized)
        {
            assetId = Guid.Empty;
            var result = false;
            if (httpFiles != null && httpFiles.Count > 0)
            {
                var asset = CreateAssetHelper(httpFiles, index, isResized);
                if (asset != null)
                {
                    if (assetRepository.Add(asset))
                    {
                        assetId = asset.Id;
                        result = true;
                    }
                }
            }
            return result;
        }

        public static bool AddAssetByName(HttpFileCollectionBase httpFiles, string name, out Guid assetId, bool isResized)
        {
            assetId = Guid.Empty;
            var result = false;
            if (httpFiles != null && httpFiles.Count > 0)
            {
                var asset = CreateAssetHelper(httpFiles, name, isResized);
                if (asset != null)
                {
                    if (assetRepository.Add(asset))
                    {
                        assetId = asset.Id;
                        result = true;
                    }
                }
            }
            return result;
        }

        private static List<Asset> CreateAssetHelper(HttpFileCollectionBase httpFiles)
        {
            var assets = new List<Asset>();
            if (httpFiles != null && httpFiles.Count > 0)
            {
                assets = CreateAssetHelper(httpFiles, httpFiles.AllKeys, false);
            }
            return assets;
        }

        private static List<Asset> CreateAssetHelper(HttpFileCollectionBase httpFiles, string[] allkeys, bool isResized)
        {
            var assets = new List<Asset>();
            if (allkeys != null && allkeys.Length > 0)
            {
                foreach (string key in allkeys)
                {
                    var asset = CreateAssetHelper(httpFiles, key, isResized);
                    if (asset != null)
                    {
                        asset.FieldId = key;
                        assets.Add(asset);
                    }
                }
            }
            return assets;
        }

        public static bool Restore(Guid assetId)
        {
            return assetRepository.UpdateStatus(Current.AgencyId, assetId, false);
        }

        public static bool Remove(Guid assetId)
        {
            return assetRepository.Remove(assetId);
        }

        public static bool Delete(Guid assetId)
        {
            return assetRepository.UpdateStatus(Current.AgencyId, assetId, true);
        }

        public static bool DeleteMany(List<Guid> assetIds)
        {
            return assetRepository.DeleteMany(Current.AgencyId, assetIds);
        }

        public static List<Asset> GetAssetsNameAndId(Guid agencyId, List<Guid> assetIds)
        {
            return assetRepository.GetAssetsNameAndId(Current.AgencyId, assetIds);
        }

        public static bool ContainsAssets(HttpFileCollectionBase httpFiles)
        {
            return httpFiles.AllKeys.Select(httpFiles.Get).Any(asset => asset != null && asset.FileName.IsNotNullOrEmpty() && asset.ContentLength > 0);
        }

        private static Asset CreateAssetHelper(HttpFileCollectionBase httpFiles, string name, bool isResized)
        {
            Asset asset = null;
            HttpPostedFileBase file = httpFiles.Get(name);
            if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
            {
                asset = CreateAssetFromFile(file, isResized);
                asset.FieldId = name;
            }
            return asset;
        }

        private static Asset CreateAssetHelper(HttpFileCollectionBase httpFiles, int index, bool isResized)
        {
            Asset asset = null;
            HttpPostedFileBase file = httpFiles[index];
            if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
            {
                asset = CreateAssetFromFile(file, isResized);
            }
            return asset;
        }

        private static Asset CreateAssetFromFile(HttpPostedFileBase file, bool isResized)
        {
            var binaryReader = default(BinaryReader);
            if (isResized)
            {
                var photo = Image.FromStream(file.InputStream);
                var resizedImageStream = ResizeAndEncodePhoto(photo, file.ContentType, false);
                if (resizedImageStream != null)
                {
                    binaryReader = new BinaryReader(resizedImageStream);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                binaryReader = new BinaryReader(file.InputStream);
            }
            var asset = new Asset
            {
                AgencyId = Current.AgencyId,
                Id = Guid.NewGuid(),
                FileName = file.FileName,
                ContentType = file.ContentType,
                FileSize = file.ContentLength.ToString(),
                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length)),
                Created = DateTime.Now,
                Modified = DateTime.Now
            };
            return asset;
        }

        private static MemoryStream ResizeAndEncodePhoto(Image original, string contentType, bool resizeIfWider)
        {
            int imageWidth = 140;
            int imageHeight = 140;
            MemoryStream memoryStream = null;

            if (original != null)
            {
                int destX = 0;
                int destY = 0;
                int sourceX = 0;
                int sourceY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                int sourceWidth = original.Width;
                int sourceHeight = original.Height;

                nPercentW = ((float)imageWidth / (float)sourceWidth);
                nPercentH = ((float)imageHeight / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                var newImage = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                newImage.SetResolution(original.HorizontalResolution, original.VerticalResolution);

                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.Clear(Color.White);
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(original,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);
                }
                memoryStream = new MemoryStream();

                ImageCodecInfo imageCodec = null;
                ImageCodecInfo[] imageCodecs = ImageCodecInfo.GetImageEncoders();

                foreach (ImageCodecInfo imageCodeInfo in imageCodecs)
                {
                    if (imageCodeInfo.MimeType == contentType)
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }

                    if (imageCodeInfo.MimeType == "image/jpeg" && contentType == "image/pjpeg")
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }
                }
                if (imageCodec != null)
                {
                    EncoderParameters encoderParameters = new EncoderParameters();
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                    newImage.Save(memoryStream, imageCodec, encoderParameters);
                    newImage.Dispose();
                    original.Dispose();
                }
            }

            memoryStream.Position = 0;
            return memoryStream;
        }
    }
}
