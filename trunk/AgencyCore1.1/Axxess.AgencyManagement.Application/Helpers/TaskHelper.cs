﻿namespace Axxess.AgencyManagement.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.Common;
    using Axxess.AgencyManagement.Application.ViewData;
    using Axxess.AgencyManagement.Application.Domain;

    public abstract class TaskHelper<T> where T : ITask, new()
    {
        public abstract T CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask);

        public abstract T CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode);

        public abstract T CreatePlanOfCareTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode);

        public abstract PreviousNote CreatePreviousNote(T task);

        public abstract T SetNoteProperties(T task, DateTime date, int status);

        public abstract bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, T scheduleTask);

        public abstract bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, List<T> scheduleTasks);

        public abstract bool ShouldMyScheduleTaskRefresh(Guid oldEmployeeId, Guid newEmployeeId, List<T> scheduleTasks);

        public abstract T CopyTaskValues(T baseTask, T taskToCopy);

        public abstract T CopyTaskValues(T taskToCopy);

        //public string GetPlanOfCareForNote(T planOfcareScheduleEvent)
        //{
        //    string url = string.Empty;
        //    if (planOfcareScheduleEvent != null)
        //    {
        //        string onClick = UrlFactory<T>.Print(planOfcareScheduleEvent, true);
        //        switch (planOfcareScheduleEvent.DisciplineTask)
        //        {
        //            case (int)DisciplineTasks.HHAideCarePlan:
        //                url = new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"")
        //                .Append(onClick)
        //                .Append("\">View Care Plan</a>").ToString();
        //                break;
        //            case (int)DisciplineTasks.PTEvaluation:
        //                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">View Eval</a>", onClick);
        //                break;
        //            case (int)DisciplineTasks.PTReEvaluation:
        //                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">View ReEval</a>", onClick);
        //                break;
        //            case (int)DisciplineTasks.STEvaluation:
        //                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">View Eval</a>", onClick);
        //                break;
        //            case (int)DisciplineTasks.STReEvaluation:
        //                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">View ReEval</a>", onClick);
        //                break;
        //            case (int)DisciplineTasks.OTEvaluation:
        //                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">View Eval</a>", onClick);
        //                break;
        //            case (int)DisciplineTasks.OTReEvaluation:
        //                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">View ReEval</a>", onClick);
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //    return url;
        //}

        public JsonViewData Validate(List<T> oldEvents, List<T> newEvents, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved. Please try again." };
            try
            {
                if ((oldEvents != null && oldEvents.Count > 0))
                {
                    var recertTasks = DisciplineTaskFactory.RecertDisciplineTasks(false);
                    var transferTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks();
                    var rocTasks = DisciplineTaskFactory.ROCDisciplineTasks();
                    var socTasks = DisciplineTaskFactory.SOCDisciplineTasks(false);
                    var dischargeTasks = DisciplineTaskFactory.DischargeOASISDisciplineTasks(false);

                    foreach (var evnt in newEvents)
                    {
                        if (recertTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date < evnt.EventDate.Date));
                            T roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return (viewData);
                            }
                            else if (transfer != null && roc != null && roc.EventDate.Date <= transfer.EventDate.Date)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return (viewData);
                            }
                            else if (oldEvents.Exists(oe => recertTasks.Contains(oe.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return (viewData);
                            }
                            else if (evnt.EventDate.Date < endDate.AddDays(-5).Date || evnt.EventDate.Date > endDate.Date)
                            {
                                viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                                return (viewData);
                            }
                        }
                        else if (socTasks.Contains(evnt.DisciplineTask))
                        {
                            if (oldEvents.Exists(oe => socTasks.Contains(evnt.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return (viewData);
                            }
                            else if (oldEvents.Exists(oe => dischargeTasks.Contains(oe.DisciplineTask)))
                            {
                            }
                        }
                        else if (rocTasks.Contains(evnt.DisciplineTask))
                        {
                            var roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask));
                            var transfers = oldEvents.Where(oe => transferTasks.Contains(oe.DisciplineTask)).ToList();
                            if (roc == null)
                            {
                                if (transfers == null || (transfers != null && transfers.Count == 0))
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return (viewData);
                                }
                                else if (transfers.IsNotNullOrEmpty())
                                {
                                    if (transfers.Exists(t => t.EventDate.Date > evnt.EventDate.Date))
                                    {
                                        viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                        return (viewData);
                                    }
                                }
                            }
                            else if (roc != null)
                            {
                                if (transfers != null)
                                {
                                    var transfersAfterEvent = transfers.Where(t => t.EventDate.Date > evnt.EventDate.Date).ToList();
                                    var transfersBeforeEvent = transfers.Where(t => t.EventDate.Date < evnt.EventDate.Date);
                                    if ((transfersAfterEvent.IsNotNullOrEmpty() && transfersAfterEvent.Exists(t => roc.EventDate.Date > t.EventDate.Date)) && transfersBeforeEvent == null)
                                    {
                                        viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                        return (viewData);
                                    }
                                }
                            }
                        }
                        else if (transferTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask));
                            T roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                                return (viewData);
                            }
                        }
                    }
                    viewData.isSuccessful = true;
                }
                else
                {
                    viewData.isSuccessful = true;
                }
            }
            catch (Exception)
            {
                return viewData;
            }
            return viewData;
        }

        public abstract JsonViewData ValidateScheduleTaskDate(T scheduleEvent, DateTime startDate, DateTime endDate);

        // public abstract MissedVisitTaskViewData CreateMissedTaskViewDataFromTask(T task);

        public abstract void ReassignViewDataEventDate(ReassignViewData viewData, T task);

        public abstract bool ShouldDataCentersRefresh(T uneditedTask, T editedTask);
    }

    public class ScheduleTaskHelper : TaskHelper<ScheduleEvent>
    {
        public override ScheduleEvent CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask)
        {
            return CreateTask(id, userId, episodeId, patientId, status, discipline, date, disciplineTask, false);
        }

        public override ScheduleEvent CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode)
        {
            return new ScheduleEvent
            {
                AgencyId = Current.AgencyId,
                Id = id,
                UserId = userId,
                EpisodeId = episodeId,
                PatientId = patientId,
                Status = status,
                Discipline = discipline.ToString(),
                EventDate = date,
                VisitDate = date,
                DisciplineTask = disciplineTask,
                IsOrderForNextEpisode = isOrderForNextEpisode
            };
        }

        public override ScheduleEvent CreatePlanOfCareTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode)
        {
            return CreateTask(id, userId, episodeId, patientId, status, discipline, date, disciplineTask, isOrderForNextEpisode);
        }

        public override ScheduleEvent SetNoteProperties(ScheduleEvent task, DateTime date, int status)
        {
            task.EventDate = date;
            task.VisitDate = date;
            task.Status = status;
            return task;
        }

        public override bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, ScheduleEvent scheduleTask)
        {
            bool result = false;
            if (isDataCentersRefresh && scheduleTask.UserId == Current.UserId)
            {
                result = scheduleTask.EventDate.Date >= DateTime.Now.AddDays(-89) && scheduleTask.EventDate.Date <= DateTime.Now.AddDays(14);
            }
            return result;
        }

        public override bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, List<ScheduleEvent> scheduleTasks)
        {
            bool result = false;
            if (isDataCentersRefresh && scheduleTasks.Exists(s => s.UserId == Current.UserId))
            {
                result = scheduleTasks.Exists(s => s.EventDate.IsBetween(DateTime.Now.AddDays(-89), DateTime.Now.AddDays(14)));
            }
            return result;
        }

        public override bool ShouldMyScheduleTaskRefresh(Guid oldEmployeeId, Guid newEmployeeId, List<ScheduleEvent> scheduleTasks)
        {
            bool result = false;
            if (Current.UserId == oldEmployeeId || Current.UserId == newEmployeeId)
            {
                result = scheduleTasks.Exists(s => s.EventDate.IsBetween(DateTime.Now.AddDays(-89), DateTime.Now.AddDays(14)));
            }
            return result;
        }

        public override ScheduleEvent CopyTaskValues(ScheduleEvent baseTask, ScheduleEvent taskToCopy)
        {
            var copyTask = baseTask ?? new ScheduleEvent();
            if (taskToCopy != null)
            {
                copyTask.TimeIn = taskToCopy.TimeIn;
                copyTask.TimeOut = taskToCopy.TimeOut;
                copyTask.VisitDate = taskToCopy.VisitDate;
                copyTask.EventDate = taskToCopy.EventDate;
                copyTask.Status = taskToCopy.Status;
                copyTask.InPrintQueue = taskToCopy.InPrintQueue;
                copyTask.DisciplineTask = taskToCopy.DisciplineTask;
                copyTask.Surcharge = taskToCopy.Surcharge;
                copyTask.AssociatedMileage = taskToCopy.AssociatedMileage;
                copyTask.SendAsOrder = taskToCopy.SendAsOrder;
                copyTask.EpisodeId = taskToCopy.EpisodeId;
                copyTask.IsMissedVisit = taskToCopy.IsMissedVisit;
                copyTask.PhysicianId = taskToCopy.PhysicianId;
                copyTask.IsBillable = taskToCopy.IsBillable;
                copyTask.UserId = taskToCopy.UserId;
                copyTask.Comments = taskToCopy.Comments;
                copyTask.Asset = taskToCopy.Asset;
                copyTask.StartDate = taskToCopy.StartDate;
                copyTask.EndDate = taskToCopy.EndDate;

            }
            return copyTask;
        }

        public override ScheduleEvent CopyTaskValues(ScheduleEvent taskToCopy)
        {
            return CopyTaskValues(null, taskToCopy);
        }


        public override bool ShouldDataCentersRefresh(ScheduleEvent uneditedTask, ScheduleEvent editedTask)
        {
            return uneditedTask.Status != editedTask.Status || uneditedTask.UserId != editedTask.UserId ||  uneditedTask.EventDate != editedTask.EventDate;
        }

        public override JsonViewData ValidateScheduleTaskDate(ScheduleEvent scheduleEvent, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            var validationRules = new List<Validation>();
            if (!DisciplineTaskFactory.OutOfEpisodeDisciplineTask().Contains(scheduleEvent.DisciplineTask))
            {
                validationRules.Add(new Validation(() => scheduleEvent.EventDate.Date <= DateTime.MinValue.Date, "Schedule date is required."));
                validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValid() || !(scheduleEvent.EventDate.Date >= startDate && scheduleEvent.EventDate.Date <= endDate), "Schedule date is not in the episode range."));
                validationRules.Add(new Validation(() => scheduleEvent.VisitDate.Date <= DateTime.MinValue.Date, "Visit date is required."));
                validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValid() || !(scheduleEvent.VisitDate.Date >= startDate && scheduleEvent.VisitDate.Date <= endDate), "Visit date is not in the episode range."));
            }
            var entityValidator = new EntityValidator(validationRules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return viewData;
        }

        public override PreviousNote CreatePreviousNote(ScheduleEvent task)
        {
            return new PreviousNote { Id = task.Id, Name = task.DisciplineTaskName, Date = task.VisitDate.ToZeroFilled() };
        }

        public override void ReassignViewDataEventDate(ReassignViewData viewData, ScheduleEvent task)
        {
            viewData.EventDate = task.EventDate.ToZeroFilled();
        }

    }

    public class PrivateDutyScheduleTaskHelper : TaskHelper<PrivateDutyScheduleTask>
    {
        public override PrivateDutyScheduleTask CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask)
        {
            return CreateTask(id, userId, episodeId, patientId, status, discipline, date, disciplineTask, false);
        }

        public override PrivateDutyScheduleTask CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode)
        {
            PrivateDutyScheduleTask newTask = new PrivateDutyScheduleTask
            {
                AgencyId = Current.AgencyId,
                Id = id,
                UserId = userId,
                PatientId = patientId,
                EpisodeId = episodeId,
                Status = status,
                Discipline = discipline.ToString(),
                EventStartTime = date,
                VisitStartTime = date,
                IsHidden = DisciplineTaskFactory.HiddenDocuments().Contains(disciplineTask),
                DisciplineTask = disciplineTask,
                IsOrderForNextEpisode = isOrderForNextEpisode
            };
            return newTask;
        }

        public override PrivateDutyScheduleTask CreatePlanOfCareTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode)
        {
            PrivateDutyScheduleTask newTask = new PrivateDutyScheduleTask
            {
                AgencyId = Current.AgencyId,
                Id = id,
                UserId = userId,
                PatientId = patientId,
                EpisodeId = episodeId,
                Status = status,
                Discipline = discipline.ToString(),
                EventStartTime = date,
                VisitStartTime = date,
                EventEndTime = date,
                VisitEndTime = date,
                IsAllDay = true,
                DisciplineTask = disciplineTask,
                IsOrderForNextEpisode = isOrderForNextEpisode
            };
            return newTask;
        }

        public override PrivateDutyScheduleTask SetNoteProperties(PrivateDutyScheduleTask task, DateTime date, int status)
        {
            task.EventStartTime = date;
            task.VisitStartTime = date;
            task.Status = status;
            return task;
        }

        public override bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, PrivateDutyScheduleTask scheduleTask)
        {
            return isDataCentersRefresh && scheduleTask.UserId == Current.UserId;
        }

        public override bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, List<PrivateDutyScheduleTask> scheduleTasks)
        {
            return isDataCentersRefresh && scheduleTasks.Exists(s => s.UserId == Current.UserId);
        }

        public override bool ShouldMyScheduleTaskRefresh(Guid oldEmployeeId, Guid newEmployeeId, List<PrivateDutyScheduleTask> scheduleTasks)
        {
            return Current.UserId == oldEmployeeId || Current.UserId == newEmployeeId;
        }

        public override PrivateDutyScheduleTask CopyTaskValues(PrivateDutyScheduleTask baseTask, PrivateDutyScheduleTask taskToCopy)
        {
            var copyTask = baseTask ?? new PrivateDutyScheduleTask();
            if (taskToCopy != null)
            {
                copyTask.EventStartTime = taskToCopy.EventStartTime;
                copyTask.EventEndTime = taskToCopy.EventEndTime;
                copyTask.VisitStartTime = taskToCopy.VisitStartTime;
                copyTask.VisitEndTime = taskToCopy.VisitEndTime;
                copyTask.Status = taskToCopy.Status;
                copyTask.InPrintQueue = taskToCopy.InPrintQueue;
                copyTask.DisciplineTask = taskToCopy.DisciplineTask;
                copyTask.Surcharge = taskToCopy.Surcharge;
                copyTask.AssociatedMileage = taskToCopy.AssociatedMileage;
                copyTask.SendAsOrder = taskToCopy.SendAsOrder;
                copyTask.EpisodeId = taskToCopy.EpisodeId;
                copyTask.IsMissedVisit = taskToCopy.IsMissedVisit;
                copyTask.PhysicianId = taskToCopy.PhysicianId;
                copyTask.IsBillable = taskToCopy.IsBillable;
                copyTask.UserId = taskToCopy.UserId;
                copyTask.Comments = taskToCopy.Comments;
                copyTask.Asset = taskToCopy.Asset;
                copyTask.IsAllDay = taskToCopy.IsAllDay;
                copyTask.StartDate = taskToCopy.StartDate;
                copyTask.EndDate = taskToCopy.EndDate;
            }
            return copyTask;
        }

        public override PrivateDutyScheduleTask CopyTaskValues(PrivateDutyScheduleTask taskToCopy)
        {
            return CopyTaskValues(null, taskToCopy);
        }


        public override bool ShouldDataCentersRefresh(PrivateDutyScheduleTask uneditedTask, PrivateDutyScheduleTask editedTask)
        {
            return uneditedTask.Status != editedTask.Status || (uneditedTask.EventStartTime != editedTask.EventStartTime || uneditedTask.EventEndTime != editedTask.EventEndTime) || uneditedTask.UserId != editedTask.UserId || uneditedTask.IsAllDay != editedTask.IsAllDay;
        }

        public override JsonViewData ValidateScheduleTaskDate(PrivateDutyScheduleTask scheduleEvent, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            var validationRules = new List<Validation>();

            validationRules.Add(new Validation(() => scheduleEvent.EventStartTime.Date <= DateTime.MinValue.Date, "Scheduled start date is required."));
            validationRules.Add(new Validation(() => scheduleEvent.EventEndTime.Date <= DateTime.MinValue.Date, "Scheduled end date is required."));
            validationRules.Add(new Validation(() => scheduleEvent.VisitStartTime.Date <= DateTime.MinValue.Date, "Visit start date is required."));
            validationRules.Add(new Validation(() => scheduleEvent.VisitEndTime.Date <= DateTime.MinValue.Date, "Visit end date is required."));
            if (!scheduleEvent.IsAllDay)
            {
                validationRules.Add(new Validation(() => scheduleEvent.EventStartTime.TimeOfDay <= DateTime.MinValue.TimeOfDay, "Scheduled start time is required."));
                validationRules.Add(new Validation(() => scheduleEvent.EventEndTime.TimeOfDay <= DateTime.MinValue.TimeOfDay, "Scheduled end time is required."));
                validationRules.Add(new Validation(() => scheduleEvent.VisitStartTime.TimeOfDay <= DateTime.MinValue.TimeOfDay, "Visit start time is required."));
                validationRules.Add(new Validation(() => scheduleEvent.VisitEndTime.TimeOfDay <= DateTime.MinValue.TimeOfDay, "Visit end time is required."));
            }
            var entityValidator = new EntityValidator(validationRules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return viewData;
        }

        public override PreviousNote CreatePreviousNote(PrivateDutyScheduleTask task)
        {
            return new PreviousNote() { Id = task.Id, Name = task.DisciplineTaskName, Date = task.VisitStartTime.ToDateAndTime() };
        }
       
        public override void ReassignViewDataEventDate(ReassignViewData viewData, PrivateDutyScheduleTask task)
        {
            viewData.EventDate = task.IsAllDay ? task.EventStartTime.ToZeroFilled() : task.EventStartTime.ToDateAndTime();
        }

    }

    public class TaskHelperFactory<T> where T : ITask, new()
    {
        private static readonly TaskHelper<T> taskHelper = Container.Resolve<TaskHelper<T>>();

        public static T CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask)
        {
            return taskHelper.CreateTask(id, userId, episodeId, patientId, status, discipline, date, disciplineTask);
        }

        public static T CreateTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode)
        {
            return taskHelper.CreateTask(id, userId, episodeId, patientId, status, discipline, date, disciplineTask, isOrderForNextEpisode);
        }

        public static T CreatePlanOfCareTask(Guid id, Guid userId, Guid episodeId, Guid patientId, int status, Disciplines discipline, DateTime date, int disciplineTask, bool isOrderForNextEpisode)
        {
            return taskHelper.CreatePlanOfCareTask(id, userId, episodeId, patientId, status, discipline, date, disciplineTask, isOrderForNextEpisode);
        }

        public static T SetNoteProperties(T task, DateTime date, int status)
        {
            return taskHelper.SetNoteProperties(task, date, status);
        }

        public static bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, T scheduleTask)
        {
            return taskHelper.ShouldMyScheduleTaskRefresh(isDataCentersRefresh, scheduleTask);
        }

        public static bool ShouldMyScheduleTaskRefresh(bool isDataCentersRefresh, List<T> scheduleTasks)
        {
            return taskHelper.ShouldMyScheduleTaskRefresh(isDataCentersRefresh, scheduleTasks);
        }

        public static bool ShouldMyScheduleTaskRefresh(Guid oldEmployeeId, Guid newEmployeeId, List<T> scheduleTasks)
        {
            return taskHelper.ShouldMyScheduleTaskRefresh(oldEmployeeId, newEmployeeId, scheduleTasks);
        }

        public static T CopyTaskValues(T baseTask, T taskToCopy)
        {
            return taskHelper.CopyTaskValues(baseTask, taskToCopy);
        }

        public static T CopyTaskValues(T taskToCopy)
        {
            return taskHelper.CopyTaskValues(taskToCopy);
        }


        public static JsonViewData Validate(List<T> oldEvents, List<T> newEvents, DateTime endDate)
        {
            return taskHelper.Validate(oldEvents, newEvents, endDate);
        }

        public static JsonViewData ValidateScheduleTaskDate(T scheduleEvent, DateTime startDate, DateTime endDate)
        {
            return taskHelper.ValidateScheduleTaskDate(scheduleEvent, startDate, endDate);
        }


        public static PreviousNote CreatePreviousNote(T task)
        {
            return taskHelper.CreatePreviousNote(task);
        }

        public static string FormatReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        public static void ReassignViewDataEventDate(ReassignViewData viewData, T task)
        {
            taskHelper.ReassignViewDataEventDate(viewData, task);
        }

        public static bool ShouldDataCentersRefresh(T uneditedTask, T editedTask)
        {
            return taskHelper.ShouldDataCentersRefresh(uneditedTask, editedTask);
        }
    }
}
