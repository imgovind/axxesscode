﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities.Repositories;
using Axxess.Core.Infrastructure;
using Axxess.Core.Extension;
using Axxess.Membership.Repositories;

namespace Axxess.AgencyManagement.Application.Helpers
{
   public static class ServiceHelper
    {
       #region Private Members

       private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
       private static IMembershipDataProvider membershipDataProvider = Container.Resolve<IMembershipDataProvider>();

       #endregion

       public static bool IsSignatureCorrect(Guid userId, string signature)
       {
           if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
           {
               var user = dataProvider.UserRepository.GetUserOnly(Current.AgencyId, userId);
               if (user != null)
               {
                   var login = membershipDataProvider.LoginRepository.Find(user.LoginId);
                   if (login != null)
                   {
                       var saltedHash = new SaltedHash();
                       if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                       {
                           return true;
                       }
                   }
               }
           }
           return false;
       }
    }
}
