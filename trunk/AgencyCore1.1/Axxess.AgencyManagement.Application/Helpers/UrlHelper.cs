﻿//namespace Axxess.AgencyManagement.Application.Helpers
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;

//    using Enums;
//    using Services;

//    using Axxess.Core.Extension;
//    using Axxess.Core.Infrastructure;

//    using Axxess.AgencyManagement.Entities;
//    using Axxess.AgencyManagement.Entities.Repositories;
//    using Axxess.AgencyManagement.Entities.Enums;
//    using Axxess.AgencyManagement.Entities.Extensions;

//    using Axxess.AgencyManagement.Repositories;
//    using Axxess.AgencyManagement.Application.Domain;

//    public abstract class UrlHelper<T> where T : ITask, new()
//    {
//        //public void GetMissedVisitPrintUrls(T task, UrlContainer urls)
//        //{
//        //    string prefix = this.Prefix;
//        //    string area = this.Area;
//        //    urls.Area = area;
//        //    urls.PreviewUrl = area + "/MissedVisit/View/";
//        //    urls.PdfUrl = area + "/MissedVisit/Pdf";
//        //    urls.EditFunction = "function() { MissedVisit." + prefix + ".Edit('" + task.Id + "');  UserInterface.CloseModal(); }";
//        //}

//        //public void GetAssessmentPrintUrls(T task, UrlContainer urls)
//        //{
//        //    string prefix = this.Prefix;
//        //    string area = this.Area;
//        //    string oasisType = task.GetOasisType().ToString();
//        //    urls.Area = area;
//        //    urls.PreviewUrl = area + "/Oasis/PrintPreview/";
//        //    urls.PdfUrl = area + "/Oasis/Pdf";
//        //    urls.EditFunction = "function() { Oasis." + prefix + ".Load('" + task.Id + "','" + task.PatientId + "','" + oasisType + "'); UserInterface.CloseModal(); }";
//        //    urls.ApproveFunction = "function() { Oasis." + prefix + ".OasisStatusAction('" + task.Id + "','" + task.PatientId + "','Approve','" + oasisType.ToLowerCase() + "'); }";
//        //    urls.ReturnFunction = "function() { Oasis." + prefix + ".OasisStatusAction('" + task.Id + "','" + task.PatientId + "','Return','" + oasisType.ToLowerCase() + "'); }";
//        //}

//        //public void GetPlanOfCarePrintUrls(T task, UrlContainer urls)
//        //{
//        //    string type = task.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone ? "PlanofCareStandAlone" : "PlanofCare";
//        //    string prefix = this.Prefix;
//        //    string area = this.Area;
//        //    urls.Area = area;
//        //    urls.PreviewUrl = string.Format("{0}/PlanOfCare/PrintPreview/{1}/{2}", area, task.PatientId, task.Id);
//        //    urls.PdfUrl = area + "/PlanOfCare/Pdf";
//        //    urls.EditFunction = string.Format("function() {{ PlanofCare.{0}.Edit('{1}','{2}','{3}'); UserInterface.CloseModal(); }}", prefix, task.EpisodeId, task.PatientId, task.Id);
//        //    urls.ApproveFunction = string.Format("function() {{ Agency.OrdersManagement.{0}.UpdateStatus('{1}','{2}','{3}','{4}','Approve'); }}", prefix, task.Id, task.PatientId, task.EpisodeId, type);
//        //    urls.ReturnFunction = string.Format("function() {{ Agency.OrdersManagement.{0}.UpdateStatus('{1}','{2}','{3}','{4}','Return'); }}", prefix, task.Id, task.PatientId, task.EpisodeId, type);
//        //}

//        //public void GetReportPrintUrls(T task, UrlContainer urls)
//        //{
//        //    string prefix = this.Prefix;
//        //    string area = this.Area;
//        //    string type = task.TypeOfEvent();
//        //    urls.Area = area;
//        //    urls.PreviewUrl = string.Format("{0}/{1}/PrintPreview/{2}/{3}", area, type, task.PatientId, task.Id);
//        //    urls.PdfUrl = string.Format("{0}/{1}/Pdf", area, type);
//        //    urls.EditFunction = string.Format("function() {{ {0}.{1}.Edit('{2}', '{3}'); UserInterface.CloseModal(); }}", type, prefix, task.PatientId, task.Id);
//        //    urls.ApproveFunction = string.Format("function() {{ {0}.{1}.Process('Approve','{2}', '{3}'); }}", type, prefix, task.PatientId, task.Id);
//        //    urls.ReturnFunction = string.Format("function() {{ {0}.{1}.Process('Return','{2}', '{3}'); }}", type, prefix, task.PatientId, task.Id);
//        //}

//        //public void GetPhysicianOrderPrintUrls(T task, UrlContainer urls)
//        //{
//        //    string prefix = this.Prefix;
//        //    string area = this.Area;
//        //    urls.Area = area;
//        //    urls.PreviewUrl = string.Format("{0}/Order/PrintPreview/{1}/{2}", area, task.PatientId, task.Id);
//        //    urls.PdfUrl = string.Format("{0}/Order/Pdf", area);
//        //    urls.EditFunction = string.Format("function() {{ PhysicianOrder.{0}.Edit('{1}','{2}'); UserInterface.CloseModal(); }}", prefix, task.Id, task.PatientId);
//        //    urls.ApproveFunction = string.Format("function() {{ Agency.OrdersManagement.{0}.UpdateStatus('{1}','{2}','{3}','PhysicianOrder','Approve'); }}", prefix, task.Id, task.PatientId, task.EpisodeId);
//        //    urls.ReturnFunction = string.Format("function() {{ Agency.OrdersManagement.{0}.UpdateStatus('{1}','{2}','{3}','PhysicianOrder','Return'); }}", prefix, task.Id, task.PatientId, task.EpisodeId);
//        //}

//        //public virtual void GetDownloadUrl(T task, UrlContainer urls)
//        //{
//        //    string area = this.Area;
//        //    if (DisciplineTaskFactory.AllAssessments(true).Contains(task.DisciplineTask))
//        //    {
//        //        urls.PdfUrl = area + "/Oasis/Pdf";
//        //    }
//        //    else if (task.IsPlanOfCare())
//        //    {
//        //        urls.PdfUrl = area + "/PlanOfCare/Pdf";
//        //    }
//        //}

//        //public abstract string Prefix { get;}
//        //public abstract string Area { get; }

//        //public string GetDetailAction(T task)
//        //{
//        //    return string.Format("<a class=\"link\" onclick=\"Schedule.Task.{2}.Edit('{0}','{1}')\">Details</a>", task.PatientId, task.Id, this.Prefix);
//        //}
//        //public string GetDeleteAction(T task)
//        //{
//        //    return string.Format("<a class=\"link\" onclick=\"Schedule.Task.{2}.Delete('{0}','{1}')\" >Delete</a>", task.PatientId, task.Id, this.Prefix);
//        //}
//        //public  string GetReassignAction(T task)
//        //{
//        //    return string.Format("<a onclick=\"Schedule.Task.{2}.Reassign('{0}','{1}')\" class=\"reassign link\">Reassign</a>", task.PatientId, task.Id, this.Prefix);
//        //}
//        //public abstract string GetRestoreAction(T task);
//        //public string GetReopenAction(T task)
//        //{
//        //    return string.Format("<a onclick=\"Schedule.Task.{2}.Reopen('{0}','{1}')\" class=\"reopen link\">Reopen Task</a>", task.PatientId, task.Id,this.Prefix);
//        //}

//        //public  string GetAttachmentAction(T task)
//        //{
//        //    return string.Format("<a onclick=\"Schedule.Task.{3}.GetAttachments('{0}', '{1}', '{2}')\" class=\"img icon16 paperclip\"></a>", task.EpisodeId, task.PatientId, task.Id,this.Prefix);
//        //}

//        //public abstract void GetVisitNotePrintUrls(T task, UrlContainer urls);
//        //public abstract void GetAppSpecificPrintUrls(T task, UrlContainer urls);
//       // public abstract void SetOASISProfile(T task);

//    }

//    /// <summary>
//    /// Helps generate urls for Home Health
//    /// </summary>
//    public class HHUrlHelper : UrlHelper<ScheduleEvent>
//    {
//        //public override string Prefix
//        //{
//        //    get { return "HomeHealth"; }
//        //}

//        //public override string Area
//        //{
//        //    get { return string.Empty; }
//        //}

//        //public override string GetRestoreAction(ScheduleEvent task)
//        //{
//        //    return string.Format("<a class=\"link\" href=\"javascript:void(0);\" onclick=\"MissedVisit.HomeHealth.Restore('{0}','{1}','{2}');\">Restore</a>", task.EpisodeId, task.PatientId, task.Id);
//        //}

//        //public override void GetVisitNotePrintUrls(ScheduleEvent task, UrlContainer urls)
//        //{
//        //    urls.Area = this.Area;
//        //    urls.PreviewUrl = "/Note/Print/";
//        //    urls.PdfUrl = "Note/Pdf";
//        //}

//        //public override void GetAppSpecificPrintUrls(ScheduleEvent task, UrlContainer urls)
//        //{
//        //    urls.Area = this.Area;
//        //    if (task.DisciplineTask ==(int)DisciplineTasks.FaceToFaceEncounter)
//        //    {
//        //        urls.PreviewUrl = "/FaceToFaceEncounter/Print/";
//        //        urls.PdfUrl = "/FaceToFaceEncounter/Pdf";
//        //    }
//        //}

//        //public override void GetDownloadUrl(ScheduleEvent task, UrlContainer urls)
//        //{
//        //    if (task.IsNote())
//        //    {
//        //        urls.PdfUrl = "Note/Pdf";
//        //    }
//        //    else
//        //    {
//        //        base.GetDownloadUrl(task, urls);
//        //    }
//        //}

//        //public override void SetOASISProfile(ScheduleEvent task)
//        //{
//        //    if (Current.HasRight(Permissions.ViewHHRGCalculations) && !Current.IfOnlyRole(AgencyRoles.Auditor))
//        //    {
//        //        if (DisciplineTaskFactory.EpisodeAllAssessments(false).Contains(task.DisciplineTask) && ScheduleStatusFactory.OASISCompleted(true).Contains(task.Status))
//        //        {
//        //            task.OasisProfileUrl = string.Format("<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({{ Url: 'Oasis/Profile/{0}/{1}', PdfUrl: 'Oasis/ProfilePdf',PdfData: {{ 'patientId':'{0}','Id': '{2}' }} }})\"><span class=\"img icon16 money\"></span></a>", task.PatientId, task.EpisodeId, task.Id);
//        //        }
//        //    }
//        //}
//    }

//    /// <summary>
//    /// Helps generate urls for Private Duty
//    /// </summary>
//    public class PDUrlHelper : UrlHelper<PrivateDutyScheduleTask>
//    {
//        //public override string Prefix
//        //{
//        //    get { return "PrivateDuty"; }
//        //}

//        //public override string Area
//        //{
//        //    get { return "PrivateDuty"; }
//        //}

//        //public override string GetRestoreAction(PrivateDutyScheduleTask task)
//        //{
//        //    return string.Format("<a class=\"link\" href=\"javascript:void(0);\" onclick=\"MissedVisit.PrivateDuty.Restore('{0}','{1}');\">Restore</a>", task.PatientId, task.Id);
//        //}

//        //public override void GetVisitNotePrintUrls(PrivateDutyScheduleTask task, UrlContainer urls)
//        //{
//        //    urls.Area = this.Area;
//        //    urls.PreviewUrl = "/PrivateDuty/Note/Print/";
//        //    urls.PdfUrl = "PrivateDuty/Note/Pdf";
//        //    urls.EditUrl = "/PrivateDuty/Note/" + task.EpisodeId + "/" + task.PatientId + "/" + task.Id;
//        //}

//        //public override void GetAppSpecificPrintUrls(PrivateDutyScheduleTask task, UrlContainer urls)
//        //{
//        //    urls.Area = this.Area;
//        //}

//        //public override void GetDownloadUrl(PrivateDutyScheduleTask task, UrlContainer urls)
//        //{
//        //    if (task.IsNote())
//        //    {
//        //        urls.PdfUrl = "PrivateDuty/Note/Print";
//        //    }
//        //    else
//        //    {
//        //        base.GetDownloadUrl(task, urls);
//        //    }
//        //}

//        //public override void SetOASISProfile(PrivateDutyScheduleTask task)
//        //{
//        //}
//    }
//}
