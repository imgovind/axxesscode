﻿namespace Axxess.AgencyManagement.Application.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
using Axxess.AgencyManagement.Entities.Repositories;

    internal abstract class EpisodeAssessmentHelper<E, T>
        where E : CarePeriod, new()
        where T : ITask, new()
    {
        private readonly EpisodeAbstract<E> episodeRepository;
        private readonly TaskScheduleAbstract<T> scheduleRepository;
        private readonly AssessmentAbstract assessmentRepository;
        private readonly MongoAbstract mongoRepository;

        public EpisodeAssessmentHelper(EpisodeAbstract<E> episodeRepository, TaskScheduleAbstract<T> scheduleRepository, AssessmentAbstract assessmentRepository, MongoAbstract mongoRepository)
        {
            this.episodeRepository = episodeRepository;
            this.scheduleRepository = scheduleRepository;
            this.assessmentRepository = assessmentRepository;
            this.mongoRepository = mongoRepository;
        }

        public abstract T GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId);

        public abstract T GetEpisodeAssessmentEvent(E episode, bool isAssessmentCompleted);

        public abstract T GetSOCEpisodeAssessmentEvent(E episode, bool isAssessmentCompleted, out bool isSOCExist);

        public abstract T GetRecetOrROCEpisodeAssessmentEvent(E previousEpisode, bool isNeedProcessed);

        public abstract T GetSOCORROCEpisodeAssessmentEvent(E episode, DateTime currentEpisodeMaxCheckDate, bool isNeedProcessed, out bool isEventExist);

        public abstract Assessment GetEpisodeAssessment(E episode);

        public abstract Assessment GetEpisodeAssessment(E episode, bool isAssessmentCompleted);

        public abstract Assessment GetEpisodeAssessment(E episode, E previousEpisode);

        public abstract Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId);

        public Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate)
        {
            Assessment assessment = null;
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var isEventExist = false;
                var assessmentEvent = GetSOCORROCEpisodeAssessmentEvent(episode, eventDate, false, out isEventExist);// scheduleRepository.GetLastScheduledEvents(Current.AgencyId, patientId, episodeId, episode.StartDate, episode.EndDate, episode.StartDate, eventDate, oasis);
                if (assessmentEvent != null)
                {
                    assessment = assessmentRepository.Get(Current.AgencyId, assessmentEvent.Id);
                }
                else
                {
                    if (!isEventExist)
                    {
                        var previousEpisode = episodeRepository.GetPreviousEpisodeByEndDate(Current.AgencyId, patientId, episode.StartDate.AddDays(-1));
                        if (previousEpisode != null)
                        {
                            var previousAssessmentEvent = GetRecetOrROCEpisodeAssessmentEvent(previousEpisode, false);// scheduleRepository.GetLastScheduledEvents(Current.AgencyId, patientId, episodeId, previousEpisode.StartDate, previousEpisode.EndDate, previousEpisode.EndDate.AddDays(-5), previousEpisode.EndDate, previousOasis);
                            if (previousAssessmentEvent != null)
                            {
                                assessment = assessmentRepository.Get(Current.AgencyId, previousAssessmentEvent.Id);
                            }
                        }
                    }
                }
            }
            return assessment;
        }

        public abstract T GetEpisodeAssessmentEvent(E episode, E previousEpisode);

        public DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId)
        {
            var dateRange = new DateRange();
            dateRange.IsLinkedToAssessment = false;
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, assessmentId);
                if (scheduleEvent != null)
                {
                    if (DisciplineTaskFactory.LastFiveDayAssessments(true).Contains(scheduleEvent.DisciplineTask))
                    {
                        var nextEpisode = episodeRepository.GetNextEpisodeByStartDate(Current.AgencyId, patientId, episode.EndDate.AddDays(1));
                        if (nextEpisode != null)
                        {
                            dateRange.StartOfCareDate = nextEpisode.StartOfCareDate;
                            dateRange.StartDate = nextEpisode.StartDate;
                            dateRange.EndDate = nextEpisode.EndDate;
                        }
                        else
                        {
                            dateRange.StartOfCareDate = episode.StartOfCareDate;
                            dateRange.StartDate = episode.EndDate.AddDays(1);
                            dateRange.EndDate = episode.EndDate.AddDays(60);
                        }
                    }
                    else
                    {
                        dateRange.StartDate = episode.StartDate;
                        dateRange.EndDate = episode.EndDate;
                        dateRange.StartOfCareDate = episode.StartOfCareDate;
                    }
                    dateRange.IsLinkedToAssessment = true;
                }
                else
                {
                    dateRange.StartOfCareDate = episode.StartOfCareDate;
                    dateRange.StartDate = episode.StartDate;
                    dateRange.EndDate = episode.EndDate;
                }
            }
            return dateRange;
        }

        public abstract string GetFrequencyForCalendar(E episode, E previousEpisode);

        public  Assessment GetEpisodeAssessmentAndOasisData(E episode)
        {
            var assessment = this.GetEpisodeAssessment(episode);
            if (assessment != null)
            {
                var questionData = mongoRepository.GetAssessmentQuestion(Current.AgencyId, assessment.Id);
                if (questionData != null)
                {
                    assessment.Questions = questionData.Question ?? new List<Question>();
                }
            }
            return assessment;
        }

    }

    internal class HHEpisodeAssessmentHelper : EpisodeAssessmentHelper<PatientEpisode, ScheduleEvent>
    {
        private readonly HHEpisodeRepository episodeRepository;
        private readonly HHTaskRepository scheduleRepository;
        private readonly HHAssessmentRepository assessmentRepository;
        private readonly HHMongoRepository mongoRepository;

        public HHEpisodeAssessmentHelper(HHDataProvider dataProvider)
            : base(dataProvider.EpisodeRepository, dataProvider.TaskRepository, dataProvider.AssessmentRepository, dataProvider.MongoRepository)
        {
            this.scheduleRepository = dataProvider.TaskRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.assessmentRepository = dataProvider.AssessmentRepository;
            this.mongoRepository = dataProvider.MongoRepository;
        }

        public override ScheduleEvent GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId)
        {
            ScheduleEvent scheduleEvent = null;
            var episode = episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                scheduleEvent = GetEpisodeAssessmentEvent(episode, true);
            }
            return scheduleEvent;
        }

        public override ScheduleEvent GetEpisodeAssessmentEvent(PatientEpisode episode, bool isAssessmentCompleted)
        {
            ScheduleEvent scheduleEvent = null;
            if (episode != null)
            {
                var isSOCExist = false;
                var socSchedule = GetSOCEpisodeAssessmentEvent(episode, isAssessmentCompleted, out isSOCExist);
                if (socSchedule != null)
                {
                    scheduleEvent = socSchedule;
                }
                else
                {
                    if (!isSOCExist)
                    {
                        var previousEpisode = episodeRepository.GetPreviousEpisodeByEndDate(Current.AgencyId, episode.PatientId, episode.StartDate.AddDays(-1));
                        if (previousEpisode != null)
                        {
                            scheduleEvent = GetRecetOrROCEpisodeAssessmentEvent(previousEpisode, isAssessmentCompleted);
                        }
                    }
                }
            }
            return scheduleEvent;
        }

        public override ScheduleEvent GetSOCEpisodeAssessmentEvent(PatientEpisode episode, bool isAssessmentCompleted, out bool isSOCExist)
        {
            ScheduleEvent result = null;
            isSOCExist = false;
            if (episode != null)
            {
                var socSchedule = scheduleRepository.GetFirstScheduledEvent(Current.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, episode.StartDate, episode.EndDate.AddDays(-6), DisciplineTaskFactory.SOCDisciplineTasks(true).ToArray());
                if (socSchedule != null)
                {
                    isSOCExist = true;
                    if (isAssessmentCompleted)
                    {
                        if (ScheduleStatusFactory.OASISAfterQA().Exists(s => s == socSchedule.Status))
                        {
                            result = socSchedule;
                        }
                    }
                    else
                    {
                        result = socSchedule;
                    }
                }
            }
            return result;
        }

        public override ScheduleEvent GetRecetOrROCEpisodeAssessmentEvent(PatientEpisode previousEpisode, bool isNeedProcessed)
        {
            ScheduleEvent scheduleEvent = null;
            if (previousEpisode != null)
            {
                var recetOrROCSchedule = scheduleRepository.GetLastScheduledEvent(Current.AgencyId, previousEpisode.Id, previousEpisode.PatientId, previousEpisode.StartDate, previousEpisode.EndDate, previousEpisode.EndDate.AddDays(-5), previousEpisode.EndDate, DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray());
                if (recetOrROCSchedule != null)
                {
                    if (isNeedProcessed)
                    {
                        if (ScheduleStatusFactory.OASISAfterQA().Exists(s => s == recetOrROCSchedule.Status))
                        {
                            scheduleEvent = recetOrROCSchedule;
                        }
                    }
                    else
                    {
                        scheduleEvent = recetOrROCSchedule;
                    }
                }
            }
            return scheduleEvent;
        }

        public override ScheduleEvent GetSOCORROCEpisodeAssessmentEvent(PatientEpisode episode, DateTime currentEpisodeMaxCheckDate, bool isNeedProcessed, out bool isEventExist)
        {
            ScheduleEvent scheduleEvent = null;
            isEventExist = false;
            if (episode != null)
            {
                var assessmentEvent = scheduleRepository.GetLastScheduledEvent(Current.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, episode.StartDate, currentEpisodeMaxCheckDate, DisciplineTaskFactory.SOCAndROCDisciplineTasks(true).ToArray());
                if (assessmentEvent != null)
                {
                    isEventExist = true;
                    if (isNeedProcessed)
                    {
                        if (ScheduleStatusFactory.OASISAfterQA().Exists(s => s == assessmentEvent.Status))
                        {
                            scheduleEvent = assessmentEvent;
                        }
                    }
                    else
                    {
                        scheduleEvent = assessmentEvent;
                    }
                }
            }
            return scheduleEvent;
        }

        public override Assessment GetEpisodeAssessment(PatientEpisode episode)
        {
            return GetEpisodeAssessment(episode, true);
        }

        public override Assessment GetEpisodeAssessment(PatientEpisode episode, bool isAssessmentCompleted)
        {
            Assessment assessment = null;
            var assessmentEvent = GetEpisodeAssessmentEvent(episode, isAssessmentCompleted);
            if (assessmentEvent != null)
            {
                assessment = assessmentRepository.Get(Current.AgencyId,assessmentEvent.Id);
            }
            return assessment;
        }

        public override Assessment GetEpisodeAssessment(PatientEpisode episode, PatientEpisode previousEpisode)
        {
            Assessment assessment = null;
            var evnt = this.GetEpisodeAssessmentEvent(episode, previousEpisode);
            if (evnt != null)
            {
                assessment = assessmentRepository.Get( Current.AgencyId,evnt.Id);
            }
            return assessment;
        }

        public override ScheduleEvent GetEpisodeAssessmentEvent(PatientEpisode episode, PatientEpisode previousEpisode)
        {
            ScheduleEvent evnt = null;
            if (episode != null)
            {
                var isEventExist = false;
                evnt = GetSOCORROCEpisodeAssessmentEvent(episode, episode.EndDate.AddDays(-6), false, out isEventExist);
                if (evnt != null)
                {
                }
                else
                {
                    if (!isEventExist)
                    {
                        if (previousEpisode != null)
                        {
                            evnt = GetRecetOrROCEpisodeAssessmentEvent(previousEpisode, false);

                        }
                    }
                }
            }
            return evnt;
        }

        public override Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId)
        {
            return GetEpisodeAssessment(episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId));
        }

        public override string GetFrequencyForCalendar(PatientEpisode episode, PatientEpisode previousEpisode)
        {
            var scheduleEvent = GetEpisodeAssessmentEvent(episode, previousEpisode);
            if (scheduleEvent != null)
            {
                var questionData = mongoRepository.GetAssessmentQuestion(Current.AgencyId, scheduleEvent.Id);
                if (questionData != null)
                {
                    var questions = questionData.Question.ToOASISDictionary();
                    if (questions != null)
                    {
                        return questions.ToFrequencyString();
                    }
                }
            }
            return string.Empty;
        }
    }

    internal class PrivateDutyEpisodeAssessmentHelper : EpisodeAssessmentHelper<PrivateDutyCarePeriod, PrivateDutyScheduleTask>
    {
        private readonly PrivateDutyEpisodeRepository episodeRepository;
        private readonly PrivateDutyTaskRepository scheduleRepository;
        private readonly PrivateDutyMongoRepository mongoRepository;
        public PrivateDutyEpisodeAssessmentHelper(PrivateDutyDataProvider dataProvider)
            : base(dataProvider.EpisodeRepository, dataProvider.TaskRepository, dataProvider.AssessmentRepository, dataProvider.MongoRepository)
        {
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.scheduleRepository = dataProvider.TaskRepository;
            this.mongoRepository = dataProvider.MongoRepository;
        }

        public override PrivateDutyScheduleTask GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId)
        {
            return null;
        }

        public override PrivateDutyScheduleTask GetEpisodeAssessmentEvent(PrivateDutyCarePeriod episode, bool isAssessmentCompleted)
        {
            return null;
        }

        public override PrivateDutyScheduleTask GetSOCEpisodeAssessmentEvent(PrivateDutyCarePeriod episode, bool isAssessmentCompleted, out bool isSOCExist)
        {
            isSOCExist = false;
            return null;
        }

        public override PrivateDutyScheduleTask GetRecetOrROCEpisodeAssessmentEvent(PrivateDutyCarePeriod previousEpisode, bool isNeedProcessed)
        {
            return null;
        }

        public override PrivateDutyScheduleTask GetSOCORROCEpisodeAssessmentEvent(PrivateDutyCarePeriod episode, DateTime currentEpisodeMaxCheckDate, bool isNeedProcessed, out bool isEventExist)
        {
            isEventExist = false;
            return null;
        }

        public override Assessment GetEpisodeAssessment(PrivateDutyCarePeriod episode)
        {
            return null;
        }

        public override Assessment GetEpisodeAssessment(PrivateDutyCarePeriod episode, bool isAssessmentCompleted)
        {
            return null;
        }

        public override Assessment GetEpisodeAssessment(PrivateDutyCarePeriod episode, PrivateDutyCarePeriod previousEpisode)
        {
            return null;
        }

        public override PrivateDutyScheduleTask GetEpisodeAssessmentEvent(PrivateDutyCarePeriod episode, PrivateDutyCarePeriod previousEpisode)
        {
            return null;
        }

        public override Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId)
        {
            return GetEpisodeAssessment(episodeRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId));
        }

        public override string GetFrequencyForCalendar(PrivateDutyCarePeriod episode, PrivateDutyCarePeriod previousEpisode)
        {
            return null;
        }
    }

    public class EpisodeAssessmentHelperFactory<E, T>
        where E : CarePeriod, new()
        where T : ITask, new()
    {
        private static readonly EpisodeAssessmentHelper<E, T> episodeAssessmentHelper = Container.Resolve<EpisodeAssessmentHelper<E, T>>();

        public static T GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId)
        {
            return episodeAssessmentHelper.GetEpisodeAssessmentEvent(episodeId, patientId);
        }

        public static T GetEpisodeAssessmentEvent(E episode, bool isAssessmentCompleted)
        {
            return episodeAssessmentHelper.GetEpisodeAssessmentEvent(episode, isAssessmentCompleted);
        }

        public static T GetSOCEpisodeAssessmentEvent(E episode, bool isAssessmentCompleted, out bool isSOCExist)
        {
            return episodeAssessmentHelper.GetSOCEpisodeAssessmentEvent(episode, isAssessmentCompleted, out isSOCExist);
        }

        public static T GetRecetOrROCEpisodeAssessmentEvent(E previousEpisode, bool isNeedProcessed)
        {
            return episodeAssessmentHelper.GetRecetOrROCEpisodeAssessmentEvent(previousEpisode, isNeedProcessed);
        }

        public static T GetSOCORROCEpisodeAssessmentEvent(E episode, DateTime currentEpisodeMaxCheckDate, bool isNeedProcessed, out bool isEventExist)
        {
            return episodeAssessmentHelper.GetSOCORROCEpisodeAssessmentEvent(episode, currentEpisodeMaxCheckDate, isNeedProcessed, out isEventExist);
        }

        public static Assessment GetEpisodeAssessmentAndOasisData(E episode)
        {
            return episodeAssessmentHelper.GetEpisodeAssessmentAndOasisData(episode);
        }

        public static Assessment GetEpisodeAssessment(E episode, bool isAssessmentCompleted)
        {
            return episodeAssessmentHelper.GetEpisodeAssessment(episode, isAssessmentCompleted);
        }

        //public static Assessment GetEpisodeAssessment(E episode, E previousEpisode)
        //{
        //    return episodeAssessmentHelper.GetEpisodeAssessment(episode, previousEpisode);
        //}

        public static Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId)
        {
            return episodeAssessmentHelper.GetEpisodeAssessment(episodeId, patientId);
        }

        /// <summary>
        /// Home Health Only
        /// </summary>
        /// <param name="episodeId"></param>
        /// <param name="patientId"></param>
        /// <param name="eventDate"></param>
        /// <returns></returns>
        public static Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate)
        {
            return episodeAssessmentHelper.GetEpisodeAssessment(episodeId, patientId, eventDate);
        }

        public static DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId)
        {
            return episodeAssessmentHelper.GetPlanofCareCertPeriod(episodeId, patientId, assessmentId);
        }

        public static string GetFrequencyForCalendar(E episode, E previousEpisode)
        {
            return episodeAssessmentHelper.GetFrequencyForCalendar(episode, previousEpisode);
        }
    }

}
