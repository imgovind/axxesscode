﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Repositories;
using Axxess.AgencyManagement.Entities;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Entities.Enums;

namespace Axxess.AgencyManagement.Application.Helpers
{
    internal abstract class NoteAndAssessmentHelper<T> where T : ITask, new()
    {
        private AssessmentAbstract assessmentRepository;
        private VisitNoteAbstract noteRepository;
        public NoteAndAssessmentHelper(AssessmentAbstract assessmentRepository, VisitNoteAbstract noteRepository)
        {
            this.assessmentRepository = assessmentRepository;
            this.noteRepository = noteRepository;
        }

        public List<Assessment> GetAssessmentSupplies(Guid patientId, List<Guid> assessmentIds)
        {
            return assessmentRepository.GetAssessmentSupplies(Current.AgencyId, patientId, assessmentIds);
        }

        public List<PatientVisitNote> GetNoteSupplies(Guid patientId, List<Guid> noteIds)
        {
            return noteRepository.GetNoteSupplies(Current.AgencyId, patientId, noteIds);
        }
    }

    internal class HHNoteAndAssessmentHelper : NoteAndAssessmentHelper<ScheduleEvent>
    {
        private HHAssessmentRepository assessmentRepository;
        private HHNoteRepository noteRepository;
        public HHNoteAndAssessmentHelper(HHDataProvider dataProvider)
            : base(dataProvider.AssessmentRepository, dataProvider.NoteRepository)
        {
            this.assessmentRepository = dataProvider.AssessmentRepository;
            this.noteRepository = dataProvider.NoteRepository;
        }
    }

    internal class PrivateDutyNoteAndAssessmentHelper : NoteAndAssessmentHelper<PrivateDutyScheduleTask>
    {
        private PrivateDutyAssessmentRepository assessmentRepository;
        private PrivateDutyNoteRepository noteRepository;
        public PrivateDutyNoteAndAssessmentHelper(PrivateDutyDataProvider dataProvider)
            : base(dataProvider.AssessmentRepository, dataProvider.NoteRepository)
        {
            this.assessmentRepository = dataProvider.AssessmentRepository;
            this.noteRepository = dataProvider.NoteRepository;
        }
    }

    public class NoteAndAssessmentHelperFactory<T> where T : ITask, new()
    {
        private static readonly NoteAndAssessmentHelper<T> taskHelper = Container.Resolve<NoteAndAssessmentHelper<T>>();

        public static List<Assessment> GetAssessmentSupplies(Guid patientId, List<Guid> assessmentIds)
        {
            return taskHelper.GetAssessmentSupplies(patientId, assessmentIds);
        }

        public static List<PatientVisitNote> GetNoteSupplies(Guid patientId, List<Guid> noteIds)
        {
            return taskHelper.GetNoteSupplies(patientId, noteIds);
        }
    }
}
