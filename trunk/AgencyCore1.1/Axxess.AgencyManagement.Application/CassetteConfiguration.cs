//using Cassette;
//using Cassette.Scripts;
//using Cassette.Stylesheets;

//namespace Axxess.AgencyManagement.Application
//{
//    using System;
//    using System.Collections.Generic;
//    using System.IO;
//    using System.Text.RegularExpressions;

//    using Axxess.Core.Infrastructure;

//    using Cassette.BundleProcessing;
//    using Cassette.TinyIoC;
//    using Cassette.Utilities;

//    using iTextSharp.text;

//    using Microsoft.Ajax.Utilities;

//    /// <summary>
//    /// Configures the Cassette asset bundles for the web application.
//    /// </summary>
//    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
//    {
//        public void Configure(BundleCollection bundles)
//        {
//            // TODO: Configure your bundles here...
//            // Please read http://getcassette.net/documentation/configuration

//            // This default configuration treats each file as a separate 'bundle'.
//            // In production the content will be minified, but the files are not combined.
//            // So you probably want to tweak these defaults!
//            bundles.Add<StylesheetBundle>("Content");
//            bundles.Add<ScriptBundle>("Scripts/jQuery");
//            bundles.Add<ScriptBundle>("Scripts/Plugins", new FileSearch
//            {
//                SearchOption = SearchOption.AllDirectories,
//                Exclude = new Regex("/min/.*\\.js$"),
//                Pattern = "*.js",
//            });
//            bundles.Add<ScriptBundle>("Scripts", new FileSearch
//            {
//                SearchOption = SearchOption.AllDirectories,
//                Exclude = new Regex("/min/.*\\.js$"),
//                Pattern = "*.js",
//            });
//        }
//    }

//    public class CustomCassetteServices : IConfiguration<TinyIoCContainer>
//    {
//        public void Configure(TinyIoCContainer container)
//        {
          
//            container.Register<IJavaScriptMinifier>(new MyCustomJavaScriptMinifier());
//        }
//    }

//    public class MyCustomJavaScriptMinifier : IJavaScriptMinifier
//    {
//        public Func<Stream> Transform(Func<Stream> openSourceStream, IAsset asset)
//        {
//            //if (asset.Path != "~/Scripts/jQuery")
//            //{
//            //    var settings = new CodeSettings();
//            //    settings.LocalRenaming = LocalRenaming.KeepLocalizationVars;
//            //    settings.PreserveFunctionNames = true;
//            //    settings.LineBreakThreshold = 180;
//            //    var minifyer = new MicrosoftJavaScriptMinifier(settings);
//            //    return minifyer.Transform(openSourceStream, asset);
//            //}
//            //else
//            //{
//                return delegate
//                {
//                    using (var reader = new StreamReader(openSourceStream()))
//                    {
//                        var output = reader.ReadToEnd();
//                        return output.AsStream();
//                    }
//                };
//            //}
//            //return delegate
//            //{
//            //    using (var reader = new StreamReader(openSourceStream()))
//            //    {
//            //        var output = string.Empty;
//            //        if (asset.Path != "~/Scripts/jQuery")
//            //        {
//            //            output = MinifyJavaScript(reader.ReadToEnd());
//            //        }
//            //        else
//            //        {
//            //            output = reader.ReadToEnd();
//            //        }
//            //        return output.AsStream();
//            //    }
//            //};
//        }

//        string MinifyJavaScript(string input)
//        {
//            GoogleClosure gc = new GoogleClosure();
//            return gc.Compress(input);
//        }
//    }

//}