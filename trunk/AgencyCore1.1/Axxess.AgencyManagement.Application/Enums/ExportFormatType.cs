﻿namespace Axxess.AgencyManagement.Application.Enums
{
    using System.ComponentModel;

    public enum ExportFormatType : int
    {
        [Description("application/vnd.ms-excel")]
        XLS = 1,
        [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
        XLSX = 2,
        [Description("text/csv")]
        CSV = 3
    }
}
