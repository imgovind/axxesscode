﻿namespace Axxess.AgencyManagement.Application.Enums
{
    public enum SelectListTypes
    {
        Users,
        Tasks,
        States,
        Branches,
        Patients,
        Insurance,
        Physicians,
        TitleTypes,
        ContactTypes,
        AdmissionSources,
        PaymentSource,
        CredentialTypes,
        LicenseTypes,
        AuthorizationStatus,
        MapAddress,
        CahpsVendors,
        BranchesReport
    }
}
