﻿namespace Axxess.AgencyManagement.Application.Enums
{
    using Axxess.Core.Infrastructure;
    public enum ClaimType
    {
        [CustomDescription("Medicare Claims", "CMS")]
        CMS,
        [CustomDescription("Medicare HMO claims", "HMO")]
        HMO,
        [CustomDescription("Managed Care Claims", "MAN")]
        MAN
    }
}
