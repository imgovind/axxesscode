﻿namespace Axxess.AgencyManagement.Application.Enums
{
    public enum OrderType : int
    {
        PhysicianOrder = 1,
        HCFA485 = 2,
        HCFA486 = 3,
        HCFA485StandAlone = 4,
        FaceToFaceEncounter = 5,
        PtEvaluation = 6,
        PtReEvaluation = 7,
        OtEvaluation = 8,
        OtReEvaluation = 9,
        StEvaluation = 10,
        StReEvaluation = 11,
        MSWEvaluation = 12,
        PTDischarge = 13,
        SixtyDaySummary = 14,
        PTReassessment = 15,
        OTReassessment = 16,
        NonOasisHCFA485=17
    }
}
