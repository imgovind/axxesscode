﻿namespace Axxess.AgencyManagement.Application.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum ClientClickActionType
    {
        [Description("Saved")]
        Save,
        [Description("Approved")]
        Approve,
        [Description("Returned")]
        Return
    }
}
