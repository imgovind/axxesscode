﻿namespace Axxess.AgencyManagement.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [Flags]
    public enum SectionQuestionType
    {
        None = 0,
        Diagnoses = 1,
        HomeBoundStatus = 2,
        Skin = 4,
        PainProfile = 8,
    }
}
