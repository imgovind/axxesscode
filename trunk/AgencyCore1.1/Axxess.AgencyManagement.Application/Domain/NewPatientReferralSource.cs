﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.Domain
{
    /// <summary>
    /// Used to bind data from Referral Source on the New Patient view
    /// </summary>
    public class NewPatientReferralSource
    {
        public int AdmissionSource { get; set; }
        public string OtherReferralSource { get; set; }
        public DateTime ReferralDate { get; set; }
        public Guid InternalReferral { get; set; }
    }
}
