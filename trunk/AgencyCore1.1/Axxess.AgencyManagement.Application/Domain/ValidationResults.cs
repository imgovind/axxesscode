﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class ValidationResults
    {
        public string ValidationMessage { get; set; }
        public bool IsSuccessful { get; set; }
        public bool IsSigned { get; set; }
    }
}
