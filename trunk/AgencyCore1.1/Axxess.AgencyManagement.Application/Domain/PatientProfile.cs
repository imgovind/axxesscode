﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;

    using Axxess.AgencyManagement.Entities;

    public class PatientProfile
    {
        public LocationPrintProfile LocationProfile { get; set; }
        public Patient Patient { get; set; }
        public CarePeriod CurrentEpisode { get; set; }
        public Assessment CurrentAssessment { get; set; }
        public AgencyPhysician Physician { get; set; }
        public PatientEmergencyContact EmergencyContact { get; set; }
        public string Clinician { get; set; }
        public string Frequencies { get; set; }
        public string Allergies { get; set; }
    }
}
