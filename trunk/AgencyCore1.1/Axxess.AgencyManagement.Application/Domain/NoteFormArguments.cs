﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class NoteFormArguments
    {
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EventId { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string SendAsOrder { get; set; }
        public string Surcharge { get; set; }
        public string AssociatedMileage { get; set; }
        public DateTime VisitDate { get; set; }
        public DateTime EventDate { get; set; }
        public int DisciplineTask { get; set; }
        public string Type { get; set; }
        public string Clinician { get; set; }
        public DateTime SignatureDate { get; set; }
        public Guid PhysicianId { get; set; }
        public string ReturnForSignature { get; set; }
    }
}
