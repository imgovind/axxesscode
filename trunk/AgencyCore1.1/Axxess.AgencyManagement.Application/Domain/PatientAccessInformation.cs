﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Entities;

namespace Axxess.AgencyManagement.Application.Domain
{
    public class PatientAccessInformation
    {
        //Contains the UserId assigned to a patient that is assigned to a task
        public Dictionary<Guid, Guid> PatientUserIds { get; set; }
        public List<PatientUser> PatientUsers { get; set; }
        public List<SelectedPatient> TeamPatients { get; set; }

        public PatientAccessInformation()
        {
            PatientUserIds = new Dictionary<Guid, Guid>();
        }
    }
}
