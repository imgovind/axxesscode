﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    using Enums;
    using Axxess.AgencyManagement.Entities.Enums;

    [KnownType(typeof(Order))]
    public class Order
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientName { get; set; }
        public string PhysicianName { get; set; }
        public string Text { get; set; }
        public OrderType Type { get; set; }
        public string DocumentType { get; set; }
        public long Number { get; set; }
        [DataType(DataType.Date)]
        [UIHint("HiddenMinDate")]
        public DateTime SendDate { get; set; }
        [DataType(DataType.Date)]
        [UIHint("HiddenMinDate")]
        public DateTime ReceivedDate { get; set; }
        [UIHint("HiddenMinDate")]
        public DateTime CreatedDate { get; set; }
        public bool PhysicianAccess { get; set; }
        [DataType(DataType.Date)]
        public DateTime PhysicianSignatureDate { get; set; }
        public int Status { get; set; }
        public string StatusName
        {
            get
            {
                ScheduleStatus status = this.Status > 0 && Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status.ToString()) : ScheduleStatus.NoStatus;
                if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.CreatedDate < DateTime.Today)
                {
                    return ScheduleStatus.CommonNotStarted.GetDescription();
                }
                else
                {
                    return status.GetDescription();
                }
            }
        }
        public AgencyServices Service { get; set; }
        public bool CanUserEdit { get; set; }
        public bool CanUserPrint { get; set; }
        public bool CanUserDelete { get; set; }
    }
}
