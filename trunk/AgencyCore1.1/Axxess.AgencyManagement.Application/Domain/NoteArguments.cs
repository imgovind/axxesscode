﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Application.iTextExtension;
    using Axxess.Core;
    using Axxess.Core.Enums;

    public class NoteArgumentsBase
    {
    }

    [Flags]
    public enum NoteArgumentsType
    {
        Print = 1,
        View = 2,
        Save = 4
    }

   

    public class NoteArguments : NoteArgumentsBase
    {
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EventId { get; set; }
        public DateTime EventDate { get; set; }
        public int DisciplineTask { get; set; }
        public bool IsDNRNeeded { get; set; }
        
        public bool IsPlanOfCareNeeded { get; set; }
        public bool IsHHACarePlanNeeded { get; set; }
        public bool IsPASCarePlanNeeded { get; set; }
       
        public bool IsVitalSignMerged { get; set; }
        public bool IsPreviousNoteNeeded { get; set; }
        public bool IsPhysicianNeeded { get; set; }
        public bool IsAllergyNeeded { get; set; }
        public bool IsDiagnosisNeeded { get; set; }
        public bool IsEvalNeeded { get; set; }
        public bool IsReturnCommentNeeded { get; set; }
        public bool IsEpisodeNeeded { get; set; }
        public AgencyServices ServiceUsed { get; set; }
        public DisciplineTasks EvalTask { get; set; }
        public string ContentPath { get; set; }
        public int Version { get; set; }

        // used for print
        public bool IsAssessmentNeeded { get; set; }
        public bool IsJsonSerialize { get; set; }
        public PdfDoc PdfDoc { get; set; }
    }





}
