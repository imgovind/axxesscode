﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application
{
    public class AssessmentSubmitArguments
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string OasisValidationType { get; set; }
        public string oasisPageName { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string ValidationClinician { get; set; }
        public DateTime ValidationSignatureDate { get; set; }

    }
}
