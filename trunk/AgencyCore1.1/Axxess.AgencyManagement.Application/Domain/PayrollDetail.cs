﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Application.Domain;
    using Axxess.Core.Enums;

    public class PayrollDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PayrollStatus { get; set; }
        public List<UserVisit> Visits { get; set; }
        public AgencyServices Service { get; set; }
        public bool IsUserCanExport { get; set; }
    }

    public class PayrollDetailSummaryItem
    {
        public string TaskName { get; set; }

        public string TaskCount { get; set; }

        public TimeSpan TotalTaskTime { get; set; }

        public int TotalTaskTimeInGrid
        {
            get
            {
                return (int)TotalTaskTime.TotalMinutes;
            }
        }

        public string TotalTaskTimeFormatted
        {
            get
            {
                return string.Format("{0} Hours, {1} Min.", (int)TotalTaskTime.TotalHours, TotalTaskTime.Minutes);
            }
        }
        
        public double TotalMileage { get; set; }

        public string TotalMileageFormatted
        {
            get
            {
                return string.Format("{0:0.00}", TotalMileage);
            }
        }

        public double TotalSurcharges { get; set; }

        public string TotalSurchargesFormatted 
        { 
            get
            {
                return string.Format("{0:C}", TotalSurcharges);
            }
        }
    }

}
