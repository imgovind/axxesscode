﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;

    public class ReportParameters
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid BranchId { get; set; }
        public int InsuranceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string AgencyName { get; set; }

        public ReportParameters(Guid id, Guid agencyId, Guid branchId)
        {
            this.Id = id;
            this.AgencyId = agencyId;
            this.BranchId = branchId;
        }

        public ReportParameters(Guid id, Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, string agencyName)
            : this(id, agencyId, branchId)
        {
            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        public ReportParameters(Guid id, Guid agencyId, Guid branchId, int year, string agencyName)
            : this(id, agencyId, branchId, new DateTime(year, 1, 1), new DateTime(year, 12, 31), agencyName) { }

        public ReportParameters(Guid id, Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int insuranceId, string agencyName)
            : this(id, agencyId, branchId, startDate, endDate, agencyName)
        {
            this.InsuranceId = insuranceId;
        }
    }
}
