﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;

    public class Recipient
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
