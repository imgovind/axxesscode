﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class UrlContainer
    {
        public string Area { get; set; }
        public string PreviewUrl { get; set; }
        public string PdfUrl { get; set; }
        public string EditUrl { get; set; }
        public string ReturnFunction { get; set; }
        public string EditFunction { get; set; }
        public string ApproveFunction { get; set; }
    }
}
