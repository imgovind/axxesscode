﻿namespace Axxess.AgencyManagement.Application.Domain
{
    using System;

    public class SaveNoteArguments
    {
        public Guid EventId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid SelectedEpisodeId { get; set; }
        
        public DateTime VisitDate { get; set; }
        public DateTime EventDate { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }

      
        public DateTime VisitStartTime { get; set; }
        public DateTime VisitEndTime { get; set; }
       
        public string AssociatedMileage { get; set; }
        public string Surcharge { get; set; }
        public bool SendAsOrder { get; set; }
       
        public int DisciplineTask { get; set; }

        public string Type { get; set; }
        public string Clinician { get; set; }
        public DateTime SignatureDate { get; set; }
        public Guid PhysicianId { get; set; }
        public string ReturnForSignature { get; set; }
        public string ReturnReason { get; set; }

        //public SaveNoteArguments()
        //{
        //}

        //public SaveNoteArguments(Guid id, Guid patientId, Guid episodeId, DateTime date, string mileage,
        //    string surcharge, bool sendAsOrder, string timeIn, string timeOut, int disciplineTask)
        //{
        //    this.EventId = id;
        //    this.PatientId = patientId;
        //    this.EpisodeId = episodeId;
        //    this.VisitDate = date;
        //    this.AssociatedMileage = mileage;
        //    this.Surcharge = surcharge;
        //    this.SendAsOrder = sendAsOrder;
        //    this.TimeIn = timeIn;
        //    this.TimeOut = timeOut;
        //    this.DisciplineTask = disciplineTask;
        //}

        //public SaveNoteArguments(Guid id, Guid patientId, DateTime date, string mileage,
        //    string surcharge, bool sendAsOrder, string timeIn, string timeOut, int disciplineTask)
        //{
        //    this.EventId = id;
        //    this.PatientId = patientId;
        //    this.VisitDate = date;
        //    this.AssociatedMileage = mileage;
        //    this.Surcharge = surcharge;
        //    this.SendAsOrder = sendAsOrder;
        //    this.TimeIn = timeIn;
        //    this.TimeOut = timeOut;
        //    this.DisciplineTask = disciplineTask;
        //}

    }
}
