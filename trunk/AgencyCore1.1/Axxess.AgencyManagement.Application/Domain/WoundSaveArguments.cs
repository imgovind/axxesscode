﻿// -----------------------------------------------------------------------
// <copyright file="WoundSaveArguments.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Axxess.AgencyManagement.Application.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WoundSaveArguments
    {
        public Guid Id { get; set; }
        public Guid PatientGuid { get; set; }
        public Guid EpisodeId { get; set; }
        public string Action { get; set; }
        public int Number { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
