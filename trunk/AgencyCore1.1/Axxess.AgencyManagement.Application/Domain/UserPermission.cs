﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Application.Domain
{
    public class UserPermissionViewData
    {
        public string Employee { get; set; }
        public string Permission { get; set; }
    }
}
