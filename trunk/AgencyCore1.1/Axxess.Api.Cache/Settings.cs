﻿

namespace Axxess.Api.Cache
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.Specialized;
    using System.Configuration;

    public static class Settings
    {
        private static NameValueCollection AppSettings = ConfigurationManager.AppSettings;

        public static string[] MemcacheServerUriArray
        {
            get
            {
                return AppSettings["MemcacheServerUris"].Split(';');
            }
        }
        public static double CachingIntervalInMinutes
        {
            get
            {
                var interval = AppSettings["CachingIntervalInMinutes"];
                double value;
                if (double.TryParse(interval, out value))
                {
                    return value;
                }
                return value;
            }
        }
    }
}
