﻿namespace OpenForum.Core
{
    using System;
    using System.Web.Mvc;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using ViewModels;
    using DataAccess;

    public class ForumRegistry : Registry
    {
        public ForumRegistry()
        {
            For<IIndexViewModel>().Use<IndexViewModel>();
            For<IPostViewModel>().Use<PostViewModel>();
            For<IReplyViewModel>().Use<ReplyViewModel>();
            For<IViewModelFactory>().Use<ViewModelFactory>();
            For<IViewViewModel>().Use<ViewViewModel>();

            For<IPostRepository>().Use<DefaultPostRepository>();
        }
    }
}
