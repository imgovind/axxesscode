﻿
namespace Axxess.Log.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.Log.Domain;
    using Axxess.Log.Enums;
using Axxess.Core.Infrastructure;

    public class LogRepository : ILogRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LogRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Audit Methods

        public bool AddTaskAudit(TaskAudit audit)
        {
            var result = false;
            if (audit != null)
            {
                audit.Created = DateTime.Now;
                audit.Modified = DateTime.Now;
                database.Add<TaskAudit>(audit);
                result = true;
            }
            return result;
        }

        public bool UpdateTaskAudit(TaskAudit audit)
        {
            var result = false;
            if (audit != null)
            {
                audit.Modified = DateTime.Now;
                database.Update<TaskAudit>(audit);
                result = true;
            }
            return result;
        }

        public TaskAudit GetTaskAudit(Guid agencyId, Guid patientId, Guid entityId, int disciplineTaskId)
        {
            return database.Single<TaskAudit>(a => a.PatientId == patientId && a.AgencyId == agencyId && a.EntityId == entityId && a.DisciplineTaskId == disciplineTaskId);
        }

        public bool AddGeneralAudit(AppAudit appAudit)
        {
            var result = false;
            try
            {
                if (appAudit != null)
                {
                    appAudit.Created = DateTime.Now;
                    appAudit.Modified = DateTime.Now;
                    database.Add<AppAudit>(appAudit);
                    result = true;
                }
            }
            catch (Exception )
            {
                return false;
            }
            return result;
        }

        public bool AddGeneralMulitAudits(List<AppAudit> appAudits)
        {
            var result = false;
            try
            {
                if (appAudits != null && appAudits.Count > 0)
                {
                    database.AddMany<AppAudit>(appAudits);
                    result = true;
                }
            }
            catch (Exception )
            {
                return false;
            }
            return result;
        }


        public bool UpdateGeneralAudit(AppAudit appAudit)
        {
            var result = false;
            if (appAudit != null)
            {
                appAudit.Modified = DateTime.Now;
                database.Update<AppAudit>(appAudit);
                result = true;
            }
            return result;
        }

        public AppAudit GetGeneralAudit(Guid agencyId, string logDomain, Guid domainId, string entityId, int Id)
        {
            return database.Single<AppAudit>(a => a.AgencyId == agencyId && a.LogDomain == logDomain && a.DomainId == domainId && a.EntityId == entityId && a.Id == Id);
        }

        public List<AppAudit> GetGeneralAudits(Guid agencyId, string logDomain,string logType ,Guid domainId, string entityId)
        {
            return database.Find<AppAudit>(a => a.AgencyId == agencyId && a.LogDomain == logDomain && a.LogType == logType && a.DomainId == domainId && a.EntityId == entityId).ToList();
        }

        public List<AppAudit> GetMedicationAudits(Guid agencyId, string logDomain, Guid domainId)
        {
            var medPro = LogType.MedicationProfile.ToString();
            var medHis = LogType.MedicationProfileHistory.ToString();
            return database.Find<AppAudit>(a => a.AgencyId == agencyId && a.LogDomain == logDomain &&  a.DomainId == domainId).Where(a=>(a.LogType == medPro || a.LogType == medHis)).ToList();
        }

        //public List<UserAudit> ScheduleUserAudits(Guid agencyId, Guid userId , DateTime startTime, DateTime endTime)
        //{
        //    var script = @"SELECT taskaudits.FirstName, patients.LastName, patients.Status, patients.DischargeDate, patientepisodes.Id, " +
        //       "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details, patientepisodes.IsDischarged " +
        //       "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
        //       "WHERE patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 " +
        //       "AND patientepisodes.IsActive = 1 AND patients.Id = @patientid " +
        //       "AND (patientepisodes.StartDate between @startdate and @enddate OR patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))";

        //}

        //public List<UserAudit> UserAppAudits(Guid agencyId, Guid userId, DateTime startTime, DateTime endTime)
        //{
        //    var script = @"SELECT appaudits.LogType, appaudits.Action, appaudits.Description, appaudits.Date " +
        //       "FROM appaudits " +
        //       "WHERE appaudits.AgencyId = @agencyid AND appaudits.UserId = @agencyid " +
        //       "AND (patientepisodes.Date between @starttime and @endtime )";
        //     var list = new List<UserAudit>();
        //    using (var cmd = new FluentCommand<UserAudit>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddDateTime("startdate", startTime)
        //        .AddDateTime("enddate", endTime)
        //        .SetMap(reader => new UserAudit
        //        {
        //            Id = reader.GetGuid("Id"),
        //            Status = reader.GetInt("Status"),
        //            Details = reader.GetStringNullable("Details"),
        //            Schedule = reader.GetStringNullable("Schedule"),
        //            IsEpisodeDischarged = reader.GetBoolean("IsDischarged"),
        //            PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
        //            EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
        //            StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
        //            PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
        //        })
        //        .AsList();

        //        return list;
        //}


        public List<TaskAudit> GetTaskAudits(Guid agencyId, string entityIds)
        {
            var script = string.Format(@"SELECT
                            Id,
                            EpisodeId,
                            PatientId,
                            EntityId,
                            Log, 
                            DisciplineTaskId,
                            Created,
                            Modified
                                FROM 
                                    taskaudits 
                                        WHERE
                                            AgencyId = @agencyid AND 
                                            EntityId IN ({0})", entityIds);

            var list = new List<TaskAudit>();
            using (var cmd = new FluentCommand<TaskAudit>(script))
            {
                list = cmd.SetConnection("LogsConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new TaskAudit
                {
                    Id = reader.GetInt("Id"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EntityId = reader.GetGuid("EntityId"),
                    Log = reader.GetStringNullable("Log"),
                    DisciplineTaskId = reader.GetInt("DisciplineTaskId"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified")
                })
                .AsList();

                return list;
            }
        }

        public bool UpdateOrAddTaskAudits(Guid agencyId, string script)
        {
            bool result = false;
            if (script.IsNotNullOrEmpty())
            {
                try
                {
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("LogsConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }


        #endregion
    }
}
