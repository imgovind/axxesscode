﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.Core.Infrastructure;
using Axxess.Core.Enums;
using Axxess.Core.Extension;

namespace Axxess.Log.Repositories
{
   public class LogDataProvider : ILogDataProvider
    {
       #region Members and Properties

       private readonly SimpleRepository database;
      
        public LogDataProvider()
        {
            this.database = new SimpleRepository("LogsConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        #region AgencyCoreDataProvider Members

        private ILogRepository logRepository;
        public ILogRepository LogRepository
        {
            get
            {
                if (logRepository == null)
                {
                    logRepository = new LogRepository(this.database);
                }
                return logRepository;
            }
        }
#endregion 
    }
}
