﻿namespace Axxess.Log.Enums
{
    using System.ComponentModel;

    public enum LogAction : int
    {
        [Description("Patient Added.")]
        PatientAdded,
        [Description("Patient Updated.")]
        PatientEdited,
        [Description("Patient Activated.")]
        PatientActivated,
        [Description("Patient Readmitted.")]
        PatientReadmitted,
        [Description("Patient Discharged.")]
        PatientDischarged,
        [Description("Patient Deleted.")]
        PatientDeleted,
        [Description("Patient Admitted.")]
        PatientAdmitted,
        [Description("Patient Non-Admitted.")]
        PatientNonAdmitted,
        [Description("Patient Set as Pending.")]
        PatientSetPending,
        [Description("Patient Set as Non-Admit.")]
        PatientSetNonAdmit,
        [Description("Patient Start-Of-Care Date Updated.")]
        PatientStartOfCareUpdated,
        [Description("Patient Discarged Date Updated.")]
        PatientDiscargedDateUpdated,
        [Description("Patient Start-Of-Care Date Deleted.")]
        PatientStartOfCareDeleted,
        [Description("Patient Discarged Date Deleted.")]
        PatientDiscargedDateDeleted,
        [Description("Patient Admission Period updated.")]
        PatientAdmissionPeriodEdited,
        [Description("Patient restored from the deleted list.")]
        PatientRestored,
        [Description("Referral Admitted As Patient.")]
        ReferralAdmitted,
        [Description("Referral Updated.")]
        ReferralUpdated,
        [Description("Referral Deleted.")]
        ReferralDeleted,
        [Description("Referral Added.")]
        ReferralAdded,
        [Description("Referral Non Admitted.")]
        ReferralNonAdmitted,
        [Description("Emergency Contact Added.")]
        EmergencyContactAdded,
        [Description("Emergency Contact Deleted.")]
        EmergencyContactDeleted,
        [Description("Emergency Contact Updated.")]
        EmergencyContactEdited,
        [Description("Episode Added")]
        EpisodeAdded,
        [Description("Episode Updated.")]
        EpisodeEdited,
        [Description("Episode Deactivated when patient discharged.")]
        EpisodeDeactivatedForDischarge,
        [Description("Episode Activated.")]
        EpisodeActivated,
        [Description("Episode Deactivated.")]
        EpisodeDeactivated,
        [Description("Physician Added to Patient.")]
        PhysicianLinked,
        [Description("Physician Removed from Patient.")]
        PhysicianUnLinked,
        [Description("Medication Profile Added for Patient.")]
        MedicationProfileAdded,
        [Description("Medication Added for Patient.")]
        MedicationAdded,
        [Description("Medication Deleted for Patient.")]
        MedicationDeleted,
        [Description("Medication Activated for Patient.")]
        MedicationActivated,
        [Description("Medication Discharged for Patient.")]
        MedicationDischarged,
        [Description("Medication Updated(By discharging) for Patient.")]
        MedicationUpdatedWithDischarge,
        [Description("Medication Updated for Patient.")]
        MedicationUpdated,
        [Description("Medication Profile Signed.")]
        MedicationProfileSigned,
        [Description("Medication History Deleted.")]
        MedicationHistoryDeleted,
        [Description("Medication History Updated.")]
        MedicationHistoryUpdated,
        [Description("RAP Added.")]
        RAPAdded,
        [Description("RAP Updated.")]
        RAPUpdated,
        [Description("RAP Claim Info Verified.")]
        RAPVerified,
        [Description("RAP Claim Submitted Electronically.")]
        RAPSubmittedElectronically,
        [Description("RAP Claim Generated.")]
        RAPGenerated,
        [Description("RAP Marked As Submitted.")]
        RAPMarkedAsSubmitted,
        [Description("RAP Updated With Status.")]
        RAPUpdatedWithStatus,
        [Description("RAP Deleted.")]
        RAPDeleted,
        [Description("RAP Remittance Posted.")]
        RAPRemittancePosted,
        [Description("Final Added.")]
        FinalAdded,
        [Description("Final Claim Demographics Verified.")]
        FinalDemographicsVerified,
        [Description("Final Claim Insurance Verified.")]
        FinalInsuranceVerified,
        [Description("Final Claim Visit Verified.")]
        FinalVisitVerified,
        [Description("Final Claim Supply Verified.")]
        FinalSupplyVerified,
        [Description("Final Marked As Submitted.")]
        FinalMarkedAsSubmitted,
        [Description("Final Updated.")]
        FinalUpdated,
        [Description("Final Updated With Status.")]
        FinalUpdatedWithStatus,
        [Description("Final Claim Summary Verified.")]
        FinalSummaryVerified,
        [Description("Final Claim Submitted Electronically.")]
        FinalSubmittedElectronically,
        [Description("Final Claim Generated.")]
        FinalGenerated,
        [Description("Final Deleted.")]
        FinalDeleted,
        [Description("Final Remittance Posted.")]
        FinalRemittancePosted,
        [Description("Managed Claim Added.")]
        ManagedClaimAdded,
        [Description("Managed Claim Demographics Verified.")]
        ManagedDemographicsVerified,
        [Description("Managed Claim Visit Verified.")]
        ManagedVisitVerified,
        [Description("Managed Claim Supply Verified.")]
        ManagedSupplyVerified,
        [Description("Managed Claim Updated.")]
        ManagedUpdated,
        [Description("Managed Claim Updated With Status.")]
        ManagedUpdatedWithStatus,
        [Description("Managed Claim Summary Verified.")]
        ManagedSummaryVerified,
        [Description("Managed Claim Submitted Electronically.")]
        ManagedSubmittedElectronically,
        [Description("Managed Claim Generated.")]
        ManagedGenerated,
        [Description("Managed Claim Deleted.")]
        ManagedDeleted,
        [Description("Managed Claim Deleted.")]
        ManagedClaimDeleted,
        [Description("Managed Claim Marked As Submitted.")]
        ManagedMarkedAsSubmitted,
        [Description("Patient Authorization Added.")]
        AuthorizationAdded,
        [Description("Patient Authorization Updated.")]
        AuthorizationEdited,
        [Description("Patient Authorization Deleted.")]
        AuthorizationDeleted,
        [Description("Agency Contact Added.")]
        AgencyContactAdded,
        [Description("Agency Contact Updated.")]
        AgencyContactUpdated,
        [Description("Agency Contact Deleted.")]
        AgencyContactDeleted,
        [Description("Agency Hospital Added.")]
        AgencyHospitalAdded,
        [Description("Agency Hospital Updated.")]
        AgencyHospitalUpdated,
        [Description("Agency Hospital Deleted.")]
        AgencyHospitalDeleted,
        [Description("Agency Physician Added.")]
        AgencyPhysicianAdded,
        [Description("Agency Physician Updated.")]
        AgencyPhysicianUpdated,
        [Description("Agency Physician Deleted.")]
        AgencyPhysicianDeleted,
        [Description("Agency Physician License Added.")]
        AgencyPhysicianLicenseAdded,
        [Description("Agency Physician License Updated.")]
        AgencyPhysicianLicenseUpdated,
        [Description("Agency Physician License Deleted.")]
        AgencyPhysicianLicenseDeleted,
        [Description("Agency Location Added.")]
        AgencyLocationAdded,
        [Description("Agency Location Updated.")]
        AgencyLocationUpdated,
        [Description("Agency Location Deleted.")]
        AgencyLocationDeleted,
        [Description("Agency Insurance Added.")]
        AgencyInsuranceAdded,
        [Description("Agency Insurance Updated.")]
        AgencyInsuranceUpdated,
        [Description("Agency Insurance Deleted.")]
        AgencyInsuranceDeleted,
        [Description("Agency Insurance Visit Information Overwritten.")]
        AgencyInsuranceVisitInfoOverWrite,
        [Description("User Added.")]
        UserAdded,
        [Description("User Deleted.")]
        UserDeleted,
        [Description("User Deactivated.")]
        UserDeactivated,
        [Description("User Activated.")]
        UserActivated,
        [Description("User Updated.")]
        UserEdited,
        [Description("User Logged In.")]
        UserLoggedIn,
        [Description("User Logged Out.")]
        UserLoggedOut,
        [Description("User License Added.")]
        UserLicenseAdded,
        [Description("User License Updated.")]
        UserLicenseUpdated,
        [Description("User License Deleted.")]
        UserLicenseDeleted,
        [Description("User Permissions Updated.")]
        UserPermissionsUpdated,
        [Description("User Profile Updated.")]
        UserProfileUpdated,
        [Description("Agency Template Added.")]
        AgencyTemplateAdded,
        [Description("Agency Template Updated.")]
        AgencyTemplateUpdated,
        [Description("Agency Template Deleted.")]
        AgencyTemplateDeleted,
        [Description("Allergy Added for Patient.")]
        AllergyAdded,
        [Description("Allergy Profile Added for Patient.")]
        AllergyProfileAdded,
        [Description("Allergy Deleted for Patient.")]
        AllergyDeleted,
        [Description("Allergy Updated  for Patient.")]
        AllergyUpdated,
        [Description("Agency Supply Added.")]
        AgencySupplyAdded,
        [Description("Agency Supply Updated.")]
        AgencySupplyUpdated,
        [Description("Agency Supply Deleted.")]
        AgencySupplyDeleted,
        [Description("Admission Period Deleted.")]
        AdmissionPeriodDeleted,
        [Description("Admission Period set active period.")]
        AdmissionPeriodSetCurrent,
        [Description("Incident Deleted.")]
        IncidentDeleted,
        [Description("Infection Deleted.")]
        InfectionDeleted,
        [Description("Agency Information Updated.")]
        AgencyInformationUpdated,
        [Description("Managed Claim Insurance Verified.")]
        ManagedInsuranceVerified,
        [Description("Payment Added.")]
        PaymentAdded,
        [Description("Payment Deleted.")]
        PaymentDeleted,
        [Description("Payment Updated.")]
        PaymentUpdated,
        [Description("Adjustment Added.")]
        AdjustmentAdded,
        [Description("Adjustment Deleted.")]
        AdjustmentDeleted,
        [Description("Adjustment Updated.")]
        AdjustmentUpdated,
        [Description("Adjustment Code Added.")]
        AgencyAdjustmentCodeAdded,
        [Description("Adjustment Code Deleted.")]
        AgencyAdjustmentCodeDeleted,
        [Description("Adjustment Code Updated.")]
        AgencyAdjustmentCodeUpdated,
        [Description("Upload Type Added.")]
        AgencyUploadTypeAdded,
        [Description("Upload Type Deleted.")]
        AgencyUploadTypeDeleted,
        [Description("Upload Type Updated.")]
        AgencyUploadTypeUpdated,
        [Description("Non-Software User Added.")]
        NonUserAdded,
        [Description("Non-Software User Deleted.")]
        NonUserDeleted,
        [Description("Non-Software User Updated.")]
        NonUserEdited,
        [Description("Non-Software User License Added.")]
        NonUserLicenseAdded,
        [Description("Non-Software User License Updated.")]
        NonUserLicenseUpdated,
        [Description("Non-Software User License Deleted.")]
        NonUserLicenseDeleted,
        [Description("Claim Demographics Verified.")]
        DemographicsVerified,
        [Description("Claim Insurance Verified.")]
        InsuranceVerified,
        [Description("Claim Visit Verified.")]
        VisitVerified,
        [Description("Claim Remittance Verified.")]
        RemittanceVerified,
        [Description("Claim Supply Verified.")]
        SupplyVerified
    }
}
