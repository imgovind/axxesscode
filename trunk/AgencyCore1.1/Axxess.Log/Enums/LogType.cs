﻿namespace Axxess.Log.Enums
{
    using System;

    public enum LogType : int
    {
        Patient,
        Episode,
        [Obsolete("Use AgencyPhysician instead.")]
        Physician,
        Referral,
        User,
        MedicationProfile,
        MedicationProfileHistory,
        Rap,
        Final,
        ManagedClaim,
        Authorization,
        AgencyContact,
        AgencyHospital,
        AgencyPhysician,
        AgencyLocation,
        AgencyInsurance,
        AgencyTemplate,
        AllergyProfile,
        ManagedDate,
        AgencySupply,
        AgencyInformation,
        AgencyIncident,
        AgencyInfection,
        AgencyAdjustmentCode,
        AgencyUploadType,
        NonUser,
        Secondary
    }
}