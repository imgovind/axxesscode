﻿namespace Axxess.LookUp
{
    internal enum CacheKey
    {
        Npi,
        Races,
        States,
        Supplies,
        Insurances,
        MedicareRates,
        PaymentSources,
        DiagnosisCodes,
        ProcedureCodes,
        DisciplineTasks,
        ReferralSources,
        SupplyCategories,
        AdmissionSources,
        DrugClassifications,
        PPSStandards,
        CBSACode,
        HippsAndHhrg,
        Relationships,
        ZipCodes,
        PrivateDutyDisciplineTask,
        OasisGuide,
        SubmissionBodyFormat,
        SubmissionHeaderFormat,
        SubmissionFooterFormat,
        SubmissionInactiveBodyFormat,
        //used as a prefix for user report permission
        ReportPermission
    }
}
