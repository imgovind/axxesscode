﻿namespace Axxess.LookUp.Repositories
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Globalization;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;

    public class LookupRepository : ILookupRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LookupRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ILookupRepository Members

        public IList<AmericanState> States()
        {
            IList<AmericanState> states = null;
            if (!InProcCacher.TryGet(CacheKey.States.ToString(), out states))
            {
                states = database.All<AmericanState>().ToList();
                InProcCacher.Set(CacheKey.States.ToString(), states);
            }
            return states.OrderBy(s => s.Name).ToList();
        }

        //public IList<Insurance> Insurances()
        //{
        //    IList<Insurance> insurances = null;
        //    if (!Cacher.TryGet(CacheKey.Insurances.ToString(), out insurances))
        //    {
        //        insurances = database.All<Insurance>().ToList();
        //        Cacher.Set(CacheKey.Insurances.ToString(), insurances);
        //    }
        //    return insurances;
        //}

        //public Insurance GetInsurance(int insuranceId)
        //{
        //    return Insurances().SingleOrDefault(i => i.Id == insuranceId);
        //}

        public IList<EthnicRace> Races()
        {
            IList<EthnicRace> races = null;
            if (!Cacher.TryGet(CacheKey.Races.ToString(), out races))
            {
                races = database.All<EthnicRace>().ToList();
                Cacher.Set(CacheKey.Races.ToString(), races);
            }
            return races;
        }

        public IList<DrugClassification> DrugClassifications()
        {
            IList<DrugClassification> classifcations = null;
            if (!Cacher.TryGet(CacheKey.DrugClassifications.ToString(), out classifcations))
            {
                classifcations = database.All<DrugClassification>().ToList();
                Cacher.Set(CacheKey.DrugClassifications.ToString(), classifcations);
            }
            return classifcations;
        }

        public IList<PaymentSource> PaymentSources()
        {
            IList<PaymentSource> paymentSources = null;
            if (!Cacher.TryGet(CacheKey.PaymentSources.ToString(), out paymentSources))
            {
                paymentSources = database.All<PaymentSource>().ToList();
                Cacher.Set(CacheKey.PaymentSources.ToString(), paymentSources);
            }
            return paymentSources;
        }

        public IList<ReferralSource> ReferralSources()
        {
            IList<ReferralSource> referralSources = null;
            if (!Cacher.TryGet(CacheKey.ReferralSources.ToString(), out referralSources))
            {
                referralSources = database.All<ReferralSource>().ToList();
                Cacher.Set(CacheKey.ReferralSources.ToString(), referralSources);
            }
            return referralSources;
        }

        public IList<DiagnosisCode> DiagnosisCodes()
        {
            IList<DiagnosisCode> diagnosisCodes = null;
            if (!InProcCacher.TryGet(CacheKey.DiagnosisCodes.ToString(), out diagnosisCodes))
            {
                diagnosisCodes = database.All<DiagnosisCode>().ToList();
                InProcCacher.Set(CacheKey.DiagnosisCodes.ToString(), diagnosisCodes);
            }
            return diagnosisCodes;
        }

        public IList<ProcedureCode> ProcedureCodes()
        {
            IList<ProcedureCode> procedureCodes = null;
            if (!InProcCacher.TryGet(CacheKey.ProcedureCodes.ToString(), out procedureCodes))
            {
                procedureCodes = database.All<ProcedureCode>().ToList();
                InProcCacher.Set(CacheKey.ProcedureCodes.ToString(), procedureCodes);
            }
            return procedureCodes;
        }

        public IList<SupplyCategory> SupplyCategories()
        {
            IList<SupplyCategory> supplyCategories = null;
            if (!Cacher.TryGet(CacheKey.SupplyCategories.ToString(), out supplyCategories))
            {
                supplyCategories = database.All<SupplyCategory>().ToList();
                Cacher.Set(CacheKey.SupplyCategories.ToString(), supplyCategories);
            }
            return supplyCategories;
        }

        public IList<AdmissionSource> AdmissionSources()
        {
            IList<AdmissionSource> admissionSources = null;
            if (!Cacher.TryGet(CacheKey.AdmissionSources.ToString(), out admissionSources))
            {
                admissionSources = database.All<AdmissionSource>().ToList();
                Cacher.Set(CacheKey.AdmissionSources.ToString(), admissionSources);
            }
            return admissionSources;
        }

        public IList<Relationship> Relationships()
        {
            IList<Relationship> relationships = null;
            if (!Cacher.TryGet(CacheKey.Relationships.ToString(), out relationships))
            {
                relationships = database.All<Relationship>().ToList();
                Cacher.Set(CacheKey.Relationships.ToString(), relationships);
            }
            return relationships;
        }

        public Relationship GetRelationship(int Id)
        {
            return Relationships().SingleOrDefault(a => a.Id == Id);
        }

        public AdmissionSource GetAdmissionSource(int sourceId)
        {
            return AdmissionSources().SingleOrDefault(a => a.Id == sourceId);
        }

        public string GetAdmissionSourceCode(int sourceId)
        {
            var addmissionCode = AdmissionSources().SingleOrDefault(a => a.Id == sourceId);
            if (addmissionCode!=null)
            {
                return addmissionCode.Code;
            }
            return string.Empty;
        }

        public IList<Supply> Supplies()
        {
            IList<Supply> supplies = null;
            if (!Cacher.TryGet(CacheKey.Supplies.ToString(), out supplies))
            {
                supplies = database.All<Supply>().ToList();
                Cacher.Set(CacheKey.Supplies.ToString(), supplies);
            }
            return supplies;
        }

        public Supply GetSupply(int Id)
        {
            return database.Single<Supply>(s => s.Id == Id);
        }

        public Npi GetNpiData(string npiId)
        {
            return database.Single<Npi>(n => n.Id == npiId);
        }

        public IList<ZipCode> ZipCode()
        {
            IList<ZipCode> zipCodes = null;
            if (!InProcCacher.TryGet(CacheKey.ZipCodes.ToString(), out zipCodes))
            {
                zipCodes = database.All<ZipCode>().ToList();
                InProcCacher.Set(CacheKey.ZipCodes.ToString(), zipCodes);
            }
            return zipCodes;
        }

        public ZipCode GetZipCode(string zipCode)
        {
            return ZipCode().FirstOrDefault(z => z.Code == zipCode);
        }

        public Axxess.LookUp.Domain.IPAddress GetIPAddress(int ipAddress)
        {
            return database.Single<Axxess.LookUp.Domain.IPAddress>(i => i.Ip == ipAddress);
        }

        public string GetZipCodeFromIpAddress(int ipAddress)
        {
            var query = database.GetPaged<Axxess.LookUp.Domain.IPAddress>(i => i.Ip < ipAddress, "Ip desc", 0, 1);
            if (query.Count > 0)
            {
                return query[0].ZipCode;
            }
            return string.Empty;
        }

        public IList<Npi> GetNpis(string q, int limit)
        {
            return database.GetPaged<Npi>(n => n.Id.StartsWith(q) && n.EntityTypeCode == "1", 0, limit);
        }

        public bool VerifyPecos(string npi)
        {
            return database.Exists<PecosPhysician>(p => p.Id == npi);
        }

        public List<StringContainer> GetPecoVerified(List<string> npis)
        {
            var pecosNPIs = new List<StringContainer>();
            if (npis.IsNotNullOrEmpty())
            {

                var script = string.Format(@"SELECT 
                                pp.Id
                                    FROM 
                                        pecosphysicians pp
                                            WHERE
                                                pp.Id in ({0})", npis.ToCommaSeperatedList());


                using (var cmd = new FluentCommand<StringContainer>(script))
                {
                    pecosNPIs = cmd.SetConnection("AxxessLookupConnectionString").SetMap(reader => reader.GetStringNullable("Id")).AsList();
                }

            }
            return pecosNPIs;
        }

        public IList<DisciplineTask> DisciplineTasks(string Discipline)
        {
            IList<DisciplineTask> disciplineTasks = null;
            if (!Cacher.TryGet(CacheKey.DisciplineTasks.ToString(), out disciplineTasks))
            {
                disciplineTasks = database.All<DisciplineTask>().ToList();
                Cacher.Set(CacheKey.DisciplineTasks.ToString(), disciplineTasks);
            }
           
            if (Discipline.IsNotNullOrEmpty())
            {
                if (Discipline.ToLowerCase() == "nursing")
                {
                    disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.IsUsedInDropDown && (e.Discipline == "Nursing" || e.Discipline == "ReportsAndNotes" && e.Task != "Medicare Eligibility Report")).ToList();
                }
                else if (Discipline.ToLowerCase() == "therapy")
                {
                    disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.IsUsedInDropDown && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).ToList();
                }
                else
                {
                    disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.IsUsedInDropDown && e.Discipline == Discipline).ToList();
                }
            }
            else
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.IsUsedInDropDown).ToList();
            }
            return disciplineTasks.OrderBy(t => t.Task).ToList();
        }

        public IList<DisciplineTask> DisciplineTasks()
        {
            IList<DisciplineTask> disciplineTasks = null;
            if (!Cacher.TryGet(CacheKey.DisciplineTasks.ToString(), out disciplineTasks))
            {
                disciplineTasks = database.All<DisciplineTask>().ToList();
                Cacher.Set(CacheKey.DisciplineTasks.ToString(), disciplineTasks);
            }
            return disciplineTasks.Cast<DisciplineTask>().OrderBy(d => d.Task).ToList();
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return DisciplineTasks().SingleOrDefault(t => t.Id == disciplineTaskId);
        }

        public IList<DisciplineTask> DisciplineTasksForRates(List<string> allowedDisciplines, IList<int> disciplines, List<int> notAllowedDisciplineTasks, int disciplineTask, bool isTaskIncluded)
        {
            var isDisciplines = disciplines.IsNotNullOrEmpty();
            var disciplineTaskArray = DisciplineTasks();
            Func<DisciplineTask, bool> whereFunc = args => allowedDisciplines.Contains(args.Discipline) && !notAllowedDisciplineTasks.Contains(args.Id) && !(isTaskIncluded && isDisciplines && disciplines.Contains(args.Id) && args.Id != disciplineTask);
            return disciplineTaskArray.Where(whereFunc).ToList();
        }

        public IList<MedicationRoute> MedicationRoute(string q, int limit)
        {
            return database.Find<MedicationRoute>(m => m.LongName.Contains(q));
        }

        public IList<MedicationClassfication> MedicationClassification(string q, int limit)
        {
            return database.Find<MedicationClassfication>(m => m.Name.Contains(q));
        }

        public IList<MedicationDosage> MedicationDosage(string q, int limit)
        {
            return database.Find<MedicationDosage>(m => m.CommonName.StartsWith(q) || m.CommonName.Contains(q));
        }

        public IList<PPSStandard> PPSStandards()
        {
            IList<PPSStandard> ppsStandards = null;
            if (!Cacher.TryGet(CacheKey.PPSStandards.ToString(), out ppsStandards))
            {
                ppsStandards = database.All<PPSStandard>().ToList();
                Cacher.Set(CacheKey.PPSStandards.ToString(), ppsStandards);
            }
            return ppsStandards;
        }

        public IList<HippsAndHhrg> HippsAndHhrgs()
        {
            IList<HippsAndHhrg> hippsAndHhrgs = null;
            if (!Cacher.TryGet(CacheKey.HippsAndHhrg.ToString(), out hippsAndHhrgs))
            {
                hippsAndHhrgs = database.All<HippsAndHhrg>().ToList();
                Cacher.Set(CacheKey.HippsAndHhrg.ToString(), hippsAndHhrgs);
            }
            return hippsAndHhrgs;
        }

        public IList<CBSACode> CbsaCodes()
        {
            IList<CBSACode> cbsaCodes = null;
            if (!InProcCacher.TryGet(CacheKey.CBSACode.ToString(), out cbsaCodes))
            {
                cbsaCodes = database.All<CBSACode>().ToList();
                InProcCacher.Set(CacheKey.CBSACode.ToString(), cbsaCodes);
            }
            return cbsaCodes;
        }

        public string CbsaCodeByZip(string zipCode)
        {
            var cbsa = string.Empty;
            if (zipCode.IsNotNullOrEmpty())
            {
                var cbsaCode = CbsaCodes().Where(c => c.Zip == zipCode).FirstOrDefault();
                if (cbsaCode != null)
                {
                    cbsa = cbsaCode.CBSA;
                }
            }
            return cbsa;
        }

        public List<CBSACode> CbsaCodes(string[] zipCodes)
        {
            var cbsa = string.Empty;
            if (!zipCodes.IsNotNullOrEmpty())
            {
                return null;
            }
            return CbsaCodes().Where(c => zipCodes.Contains(c.Zip)).ToList();
        }

        public CBSACode CbsaCode(string zipCode)
        {
            if (zipCode.IsNullOrEmpty())
            {
                return null;
            }
            return CbsaCodes().Where(c => c.Zip == zipCode).FirstOrDefault();
        }

        public HippsAndHhrg GetHHRGByHIPPSCODE(string hippsCode)
        {
            return HippsAndHhrgs().Where(h => h.HIPPS == hippsCode).FirstOrDefault();
        }

        public HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year)
        {
            return HippsAndHhrgs().Where(h => h.HIPPS == hippsCode && h.Time.Year == year).FirstOrDefault();
        }

        public PPSStandard GetPPSStandardByYear(int year)
        {
            return PPSStandards().Where(s => s.Time.Year == year).FirstOrDefault();
        }

        public List<PPSStandard> GetPPSStandardsByYears(int[] years)
        {
            return PPSStandards().Where(s => years.Contains(s.Time.Year)).ToList();
        }

        public double ProspectivePayAmount(string hippsCode, DateTime time, string zipCode, string agencyZipCode)
        {
            double amount = 0;
            CBSACode cbsaCode = null;
            if (hippsCode.IsNotNullOrEmpty() && time.IsValid())
            {
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();
                var hhrg = GetHHRGByHIPPSCODE(hippsCode);
                if (zipCode.IsNotNullOrEmpty())
                {
                    cbsaCode = CbsaCode(zipCode);
                }
                if (cbsaCode == null || (cbsaCode != null && cbsaCode.CBSA.IsNullOrEmpty()))
                {
                    cbsaCode = CbsaCode(agencyZipCode);
                }
                var ppsStandard = GetPPSStandardByYear(time.Year);
                if (hhrg != null && cbsaCode != null && ppsStandard != null)
                {
                    var ppsRate = ppsStandard.UrbanRate;
                    var hhrgWeight = hhrg.HHRGWeight;
                    var labor = ppsStandard.Labor;
                    var nonLabor = ppsStandard.NonLabor;
                    double wageIndex = 0;
                    if (time.Year == 2012)
                    {
                        wageIndex = cbsaCode.WITwoTwelve;
                    }
                    else if (time.Year == 2011)
                    {
                        wageIndex = cbsaCode.WITwoEleven;
                    }
                    else if (time.Year == 2010)
                    {
                        wageIndex = cbsaCode.WITwoTen;
                    }
                    else if (time.Year == 2009)
                    {
                        wageIndex = cbsaCode.WITwoNine;
                    }
                    else if (time.Year == 2008)
                    {
                        wageIndex = cbsaCode.WITwoEight;
                    }
                    else if (time.Year == 2007)
                    {
                        wageIndex = cbsaCode.WITwoSeven;
                    }
                    amount = (ppsRate * hhrgWeight) * ((labor * wageIndex) + nonLabor);
                }
            }
            return amount;
        }

        public double GetProspectivePaymentAmount(string hippsCode, DateTime time, string zipCode)
        {
            Double prospectivePayment = 0;

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && zipCode.IsNotNullOrEmpty() && zipCode.Length == 5)
            {
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();
                var cbsaCode = CbsaCode(zipCode);
                var hhrg = GetHhrgByHippsCodeAndYear(hippsCode, time.Year);
                var ppsStandard = GetPPSStandardByYear(time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = false;
                    var wageIndex = this.WageIndex(cbsaCode, time);

                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        if (cbsaCode.CBSA.StartsWith("9"))
                        {
                            isRural = true;
                            rate = ppsStandard.RuralRate;
                        }
                        else
                        {
                            isRural = false;
                            rate = ppsStandard.UrbanRate;
                        }
                    }

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                    var totalAmount = supplyAmount + totalAmountWithoutSupplies;

                    prospectivePayment = totalAmount;

                }
            }
            return prospectivePayment;
        }

        public ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode)
        {
            var prospectivePayment = new ProspectivePayment();

            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && zipCode.IsNotNullOrEmpty() && zipCode.Length == 5)
            {
                hippsCode = hippsCode.GetReverseNrsSeverityLevel().ToUpperCase();
                var cbsaCode = CbsaCode(zipCode);
                var hhrg = GetHhrgByHippsCodeAndYear(hippsCode, time.Year);
                var ppsStandard = GetPPSStandardByYear(time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = false;
                    var wageIndex = this.WageIndex(cbsaCode, time);

                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        if (cbsaCode.CBSA.StartsWith("9"))
                        {
                            isRural = true;
                            rate = ppsStandard.RuralRate;
                        }
                        else
                        {
                            isRural = false;
                            rate = ppsStandard.UrbanRate;
                        }
                    }

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                    prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;

                    prospectivePayment.Hhrg = hhrg.HHRG;
                    prospectivePayment.CbsaCode = cbsaCode.CBSA;
                    prospectivePayment.WageIndex = wageIndex.ToString();
                    prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                    prospectivePayment.LaborAmount = string.Format("${0:#0.00}", laborAmount);
                    prospectivePayment.NonLaborAmount = string.Format("${0:#0.00}", nonLaborAmount);
                    prospectivePayment.NonRoutineSuppliesAmount = string.Format("${0:#0.00}", supplyAmount);
                    prospectivePayment.TotalAmountWithoutSupplies = string.Format("${0:#0.00}", totalAmountWithoutSupplies);
                    prospectivePayment.TotalProspectiveAmount = string.Format("${0:#0.00}", prospectivePayment.TotalAmount);
                }
            }
            return prospectivePayment;
        }

        public IList<MedicareRate> DefaultMedicareRates()
        {
            IList<MedicareRate> medicareRates = null;
            if (!Cacher.TryGet(CacheKey.MedicareRates.ToString(), out medicareRates))
            {
                medicareRates = database.All<MedicareRate>().ToList();
                Cacher.Set(CacheKey.MedicareRates.ToString(), medicareRates);
            }
            return medicareRates;
        }

    

        public DisciplineTask GetDisciplineTasksWithTableName(int disciplinetaskId)
        {
            var script = string.Format(@"SELECT 
                        disciplinetasks.Id as Id ,
                        disciplinetasks.Discipline as Discipline ,
                        disciplinetasks.IsBillable as IsBillable ,
                        disciplinetasks.IsMultiple as IsMultiple ,
                        disciplinetasks.Version as Version , 
                        disciplinetasks.DefaultStatus as DefaultStatus ,
                        disciplinetasks.`Table` as `Table`,
                        dbtables.Name as TableName 
                                 FROM disciplinetasks INNER JOIN dbtables ON disciplinetasks.`Table` = dbtables.Id 
                                      WHERE 
                                            disciplinetasks.Id = @disciplinetaskid limit 0,1; ");

            var disciplineTask = new DisciplineTask();
            using (var cmd = new FluentCommand<DisciplineTask>(script))
            {
                disciplineTask = cmd.SetConnection("AxxessLookupConnectionString")
                .AddInt("disciplinetaskid", disciplinetaskId)
                .SetMap(reader => new DisciplineTask
                {
                    Id = reader.GetInt("Id"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMultiple = reader.GetBoolean("IsMultiple"),
                    Version = reader.GetInt("Version"),
                    DefaultStatus = reader.GetInt("DefaultStatus"),
                    Table = reader.GetInt("Table"),
                    TableName = reader.GetStringNullable("TableName")
                })
                .AsSingle();
            }
            return disciplineTask;
        }

        public List<DisciplineTask> GetDisciplineTasksWithTableName(int [] disciplinetaskIds)
        {
            var script = string.Format(@"SELECT 
                        disciplinetasks.Id as Id ,
                        disciplinetasks.Discipline as Discipline ,
                        disciplinetasks.IsBillable as IsBillable ,
                        disciplinetasks.IsMultiple as IsMultiple ,
                        disciplinetasks.Version as Version , 
                        disciplinetasks.DefaultStatus as DefaultStatus ,
                        disciplinetasks.`Table` as `Table` ,
                        dbtables.Name as TableName 
                                 FROM disciplinetasks  INNER JOIN dbtables ON  disciplinetasks.`Table` = dbtables.Id 
                                      WHERE 
                                            disciplinetasks.Id IN ({0}); ", disciplinetaskIds.Select(d => d.ToString()).ToArray().Join(","));

            var disciplineTasks = new List<DisciplineTask>();
            using (var cmd = new FluentCommand<DisciplineTask>(script))
            {
                disciplineTasks = cmd.SetConnection("AxxessLookupConnectionString")
                .SetMap(reader => new DisciplineTask
                {
                    Id = reader.GetInt("Id"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMultiple = reader.GetBoolean("IsMultiple"),
                    Version = reader.GetInt("Version"),
                    DefaultStatus = reader.GetInt("DefaultStatus"),
                    Table = reader.GetInt("Table"),
                    TableName = reader.GetStringNullable("TableName")
                })
                .AsList();
            }
            return disciplineTasks;
        }


        #endregion

        #region Private Methods

        private double WageIndex(CBSACode cbsaCode, DateTime time)
        {
            double wageIndex = 0;
            if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }

        private double GetSupplyReimbursement(PPSStandard ppsStandard, bool isRural, char type)
        {
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        if (isRural)
                        {
                            return ppsStandard.RuralS;
                        }
                        return ppsStandard.S;
                    case 'T':
                        if (isRural)
                        {
                            return ppsStandard.RuralT;
                        }
                        return ppsStandard.T;
                    case 'U':
                        if (isRural)
                        {
                            return ppsStandard.RuralU;
                        }
                        return ppsStandard.U;
                    case 'V':
                        if (isRural)
                        {
                            return ppsStandard.RuralV;
                        }
                        return ppsStandard.V;
                    case 'W':
                        if (isRural)
                        {
                            return ppsStandard.RuralW;
                        }
                        return ppsStandard.W;
                    case 'X':
                        if (isRural)
                        {
                            return ppsStandard.RuralX;
                        }
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        #endregion

        #region Billing

        public AxxessSubmitterInfo SubmitterInfo(int payerId)
        {
            return database.Single<AxxessSubmitterInfo>(s => s.Id == payerId);
        }

        #endregion

        #region OASIS 

        public List<OasisGuide> GetOasisGuides()
        {
            List<OasisGuide> oasisGuides = null;
            if (!Cacher.TryGet(CacheKey.OasisGuide.ToString(), out oasisGuides))
            {
                oasisGuides = database.All<OasisGuide>().ToList();
                Cacher.Set(CacheKey.OasisGuide.ToString(), oasisGuides);
            }
            return oasisGuides;
        }

        public OasisGuide GetOasisGuide(string mooCode)
        {
            List<OasisGuide> oasisGuides = GetOasisGuides();
            return oasisGuides.Where(o => o.Id.IsEqual(mooCode)).SingleOrDefault();
        }

        public List<SubmissionBodyFormat> GetSubmissionFormatInstructions()
        {
            List<SubmissionBodyFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionBodyFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionBodyFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionBodyFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }

        public List<SubmissionHeaderFormat> GetSubmissionHeaderFormatInstructions()
        {
            List<SubmissionHeaderFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionHeaderFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionHeaderFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionHeaderFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }

        public List<SubmissionFooterFormat> GetSubmissionFooterFormatInstructions()
        {
            List<SubmissionFooterFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionFooterFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionFooterFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionFooterFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }

        public List<SubmissionInactiveBodyFormat> GetSubmissionInactiveBodyFormatInstructions()
        {
            List<SubmissionInactiveBodyFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionInactiveBodyFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionInactiveBodyFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionInactiveBodyFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }


        #endregion 

        #region Persmissions

        //public List<Permission> GetPersmissions()
        //{
        //   return database.Find<Permission>(p => p.IsDeprecated == false).ToList();
        //}

        public List<PermissionLink> GetPermissions()
        {
            var script = string.Format(@"SELECT 
                                        pal.ParentId as ParentId ,
                                        pal.ActionId as ActionId ,
                                        pal.Name as Name, 
                                        pal.Description as Description ,
                                        pr.HasServiceScope as HasServiceScope ,
                                        pal.GroupId as GroupId,
                                        pr.Service as Service,
                                        pc.Name as Category,
                                        pr.IsSubCategory as IsSubCategory
                                                 FROM
                                                    permissionactivitylinks as pal
                                                        INNER JOIN permissions as pr ON pr.`Id` = pal.ParentId 
                                                        INNER JOIN permissioncategories as pc ON pc.`Id` = pr.CategoryId 
                                                        INNER JOIN permissionactivities as pa ON pa.`Id` = pal.ActionId ");

            var permissionLinks = new List<PermissionLink>();
            using (var cmd = new FluentCommand<PermissionLink>(script))
            {
                permissionLinks = cmd.SetConnection("AxxessLookupConnectionString")
                .SetMap(reader => new PermissionLink
                {
                    Id = reader.GetInt("ParentId"),
                    ChildId = reader.GetInt("ActionId"),
                    Name = reader.GetStringNullable("Name"),
                    Description = reader.GetStringNullable("Description"),
                    HasServiceScope=reader.GetBoolean("HasServiceScope"),
                    GroupId = reader.GetInt("GroupId"),
                    Service = reader.GetInt("Service"),
                    Category = reader.GetString("Category"),
                    IsSubCategory = reader.GetBoolean("IsSubCategory")
                })
                .AsList();
            }
            return permissionLinks;
        }

        public List<int> GetPermissionCategoryId(bool IsServiceScope)
        {
            var script = string.Format(@"SELECT 
                                             Id 
                                                 FROM
                                                        permissions 
                                                              Where HasServiceScope = @hasserviceccope ");

            var categories = new List<int>();
            using (var cmd = new FluentCommand<int>(script))
            {
                categories = cmd.SetConnection("AxxessLookupConnectionString")
                    .AddInt("hasserviceccope", IsServiceScope ? 1 : 0)
                .SetMap(reader =>
                {
                    return reader.GetInt("Id");

                }).AsList();
            }
            return categories;
        }

        public List<ReportDescription> GetReportDescriptions()
        {
            return database.All<ReportDescription>().ToList();
        }

        //public List<ReportDescription> GetDescriptions(bool IsProduction)
        //{
        //    if (IsProduction)
        //    {

        //        return database.Find<ReportDescription>(r => r.IsProductionReady == true).ToList();

        //    }
        //    else
        //        return database.All<ReportDescription>().ToList();
        //}

        public List<MenuElement> GetMenuElements()
        {
            var script = string.Format(@"SELECT 
                                        mn.Id,
                                        mn.Name,
                                        mn.MenuName,
                                        mn.Url,
                                        mn.OnLoad,
                                        mn.IsAreaPrefix,
                                        mn.ParentMenus,
                                        mn.Category,
                                        mn.OtherCategory,
                                        mn.Action,
                                        mn.Service,
                                        mn.StyleOptions
                                                 FROM
                                                    menus as mn");

            var menuElements = new List<MenuElement>();
            using (var cmd = new FluentCommand<MenuElement>(script))
            {
                menuElements = cmd.SetConnection("AxxessLookupConnectionString")
                .SetMap(reader => new MenuElement
                {
                    Id = reader.GetString("Id"),
                    Name = reader.GetStringNullable("Name"),
                    MenuName = reader.GetStringNullable("MenuName"),
                    Url = reader.GetStringNullable("Url"),
                    OnLoad = reader.GetStringNullable("OnLoad"),
                    //Resize = reader.GetBoolean("Resize"),
                    //StatusBar = reader.GetBoolean("StatusBar"),
                    //Center = reader.GetBoolean("Center"),
                    //IgnoreMinSize = reader.GetBoolean("IgnoreMinSize"),
                    //Height = reader.GetStringNullable("Height"),
                    //Width = reader.GetStringNullable("Width"),
                    IsAreaPrefix = reader.GetBoolean("IsAreaPrefix"),
                    ParentMenus = reader.GetStringNullable("ParentMenus"),
                    Category = reader.GetInt("Category"),
                    OtherCategory = reader.GetStringNullable("OtherCategory"),
                    Action = reader.GetInt("Action"),
                    //Overflow = reader.GetBoolean("Overflow"),
                    Service = reader.GetInt("Service"),
                    StyleOptions = reader.GetStringNullable("StyleOptions")

                    //IsPost = reader.GetBoolean("IsPost"),
                    //Modal = reader.GetBoolean("Modal"),
                    //WindowFrame = reader.GetBoolean("WindowFrame"),
                    //MaxOnMobile = reader.GetBoolean("MaxOnMobile"),
                    //VersitileHeight = reader.GetBoolean("VersitileHeight")
                   
                })
                .AsList();
            }
            return menuElements;
        }

        #endregion
    }
}
