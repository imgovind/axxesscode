﻿namespace Axxess.LookUp.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.Core;

    public interface ILookupRepository
    {
        IList<Supply> Supplies();
        Supply GetSupply(int Id);
        IList<EthnicRace> Races();
        IList<AmericanState> States();
        IList<PaymentSource> PaymentSources();
        IList<DiagnosisCode> DiagnosisCodes();
        IList<ProcedureCode> ProcedureCodes();
        IList<MedicareRate> DefaultMedicareRates();
        //IList<Insurance> Insurances();
        IList<SupplyCategory> SupplyCategories();
        IList<ReferralSource> ReferralSources();
        IList<AdmissionSource> AdmissionSources();
        IList<Relationship> Relationships();
        IList<DrugClassification> DrugClassifications();
        IList<DisciplineTask> DisciplineTasks(string Discipline);
        IList<DisciplineTask> DisciplineTasks();
        DisciplineTask GetDisciplineTask(int disciplineTaskId);
        IList<DisciplineTask> DisciplineTasksForRates(List<string> allowedDisciplines, IList<int> disciplines, List<int> notAllowedDisciplineTasks, int disciplineTask, bool isTaskIncluded);

        Relationship GetRelationship(int Id);
        Npi GetNpiData(string npi);
        AdmissionSource GetAdmissionSource(int sourceId);
        string GetAdmissionSourceCode(int sourceId);
        IList<Npi> GetNpis(string q, int limit);
        ZipCode GetZipCode(string zipCode);
        bool VerifyPecos(string npi);
        List<StringContainer> GetPecoVerified(List<string> npis);
        IPAddress GetIPAddress(int ipAddress);
        string GetZipCodeFromIpAddress(int ipAddress);
        IList<MedicationRoute> MedicationRoute(string q, int limit);
        IList<MedicationClassfication> MedicationClassification(string q, int limit);
        IList<MedicationDosage> MedicationDosage(string q, int limit);

        //Insurance GetInsurance(int insuranceId);
        string CbsaCodeByZip(string zipCode);
        CBSACode CbsaCode(string zipCode);
        List<CBSACode> CbsaCodes(string[] zipCodes);
        HippsAndHhrg GetHHRGByHIPPSCODE(string hippsCode);
        HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year);
        PPSStandard GetPPSStandardByYear(int year);
        List<PPSStandard> GetPPSStandardsByYears(int[] years);
        double ProspectivePayAmount(string hppisCode, DateTime time, string zipCode, string agencyZipCode);
        IList<CBSACode> CbsaCodes();
        IList<PPSStandard> PPSStandards();
        IList<HippsAndHhrg> HippsAndHhrgs();
        double GetProspectivePaymentAmount(string hippsCode, DateTime time, string zipCode);
        ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode);
        AxxessSubmitterInfo SubmitterInfo(int payerId);

        List<OasisGuide> GetOasisGuides();
        OasisGuide GetOasisGuide(string mooCode);
        List<SubmissionBodyFormat> GetSubmissionFormatInstructions();
        List<SubmissionHeaderFormat> GetSubmissionHeaderFormatInstructions();
        List<SubmissionFooterFormat> GetSubmissionFooterFormatInstructions();
        List<SubmissionInactiveBodyFormat> GetSubmissionInactiveBodyFormatInstructions();

       

        DisciplineTask GetDisciplineTasksWithTableName(int disciplinetaskId);
        List<DisciplineTask> GetDisciplineTasksWithTableName(int[] disciplinetaskIds);

        List<PermissionLink> GetPermissions();
        List<ReportDescription> GetReportDescriptions();
        List<int> GetPermissionCategoryId(bool IsServiceScope);
        List<MenuElement> GetMenuElements();
    }
}
