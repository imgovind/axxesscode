﻿namespace Axxess.LookUp.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    [Serializable]
    public class DisciplineTask
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public string Discipline { get; set; }
        public bool IsBillable { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsTypeChangeable { get; set; }
        public string RevenueCode { get; set; }
        public string GCode { get; set; }
        public int Unit { get; set; }
        public double Rate { get; set; }
        public int Version { get; set; }
        public int DefaultStatus { get; set; }
        public int Table { get; set; }
        public bool IsUsedInDropDown { get; set; }
        public int? NewVitalSignsVersion { get; set; }
        public string FooterSettings { get; set; }

        public int Physician { get; set; }
        public int Diagnosis { get; set; }
        public int CarePlan { get; set; }
        public int PreviousNote { get; set; }
        public int Allergy { get; set; }

        public bool IsJsonSerialize { get; set; }
        public bool IsVersionAppended { get; set; }
        public bool IsOasisCarePlan { get; set; }
        public bool IsNotOasisCarePlan { get; set; }
        public bool IsDNRVisible { get; set; }
        public bool IsVitalSignMerged { get; set; }
        public bool IsVisitParameterVisible { get; set; }
        public bool IsTravelParameterVisible { get; set; }
        public bool IsTimeIntervalVisible { get; set; }
        public bool HasPhysicianDate { get; set; }

        public string ParentPath { get; set; }
        public string ContentPath { get; set; }
        public int CarePlanTaskId { get; set; }


        [SubSonicIgnore]
        public string TableName { get; set; }
    }
}
