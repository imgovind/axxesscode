﻿namespace Axxess.LookUp.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class DBTable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public int DatabaseId { get; set; }


        [SubSonicIgnore]
        public string DatabaseName { get; set; }
       
    }
}
