﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.LookUp.Domain
{
    public class FooterSettings
    {
        public int NumberOfSignatures { get; set; }
        public bool CanAddSupplies { get; set; }
        public bool CanScheduleSupVisit { get; set; }
        public bool CanAddWoundCare { get; set; }

        public FooterSettings()
        {
            NumberOfSignatures = 1;
            CanAddSupplies = false;
            CanScheduleSupVisit = false;
            CanAddWoundCare = false;
        }
    }
}
