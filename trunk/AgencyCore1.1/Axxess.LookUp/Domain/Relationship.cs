﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.LookUp.Domain
{
    [Serializable]
    public class Relationship
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
