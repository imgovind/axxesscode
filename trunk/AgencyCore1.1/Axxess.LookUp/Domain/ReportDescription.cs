﻿namespace Axxess.LookUp.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class ReportDescription
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public int CategoryId { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string Parameters { get; set; }
        public string InformationInclude { get; set; }
        public string Comments { get; set; }
        public bool IsOnclick { get; set; }
        public bool HasServiceScope { get; set; }
        public int Service { get; set; }
        [SubSonicIgnore]
        public int ExportService { get; set; }
        [SubSonicIgnore]
        public int ViewService { get; set; }
    }
}
