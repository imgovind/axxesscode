﻿namespace Axxess.LookUp.Domain
{
    using System;
    [Serializable]
    public class SupplyCategory
    {
        public byte Id { get; set; }
        public string Description { get; set; }
    }
}
