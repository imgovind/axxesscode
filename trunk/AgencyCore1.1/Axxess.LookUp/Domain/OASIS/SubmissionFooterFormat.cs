﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
   public class SubmissionFooterFormat
    {
        public short Id { get; set; }
        public string Item { get; set; }
        public double Length { get; set; }
        public double Start { get; set; }
        public double End { get; set; }
    }
}
