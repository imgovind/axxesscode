﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class OasisGuide
    {
        public string Id { get; set; }
        public string ItemIntent { get; set; }
        public string Response { get; set; }
        public string DataSources { get; set; }
    }
}
