﻿namespace Axxess.Api.Membership
{
    using System;
    using System.Timers;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Contracts;

    public sealed class SupportMonitor
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly SupportMonitor instance = new SupportMonitor();
        }

        #endregion

        #region Private Members

        private Timer timer;
        private SafeList<SupportUser> users;

        #endregion

        #region Public Instance

        public static SupportMonitor Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private SupportMonitor()
        {
            users = new SafeList<SupportUser>();
        }

        private void StartTimer()
        {
            this.timer = new System.Timers.Timer(TimeSpan.FromMinutes(2).TotalMilliseconds);
            this.timer.Enabled = true;
            this.timer.Elapsed += (sender, e) =>
            {
                var inactive = this.users.Find(user =>
                {
                    if (user != null) 
                    {
                        return DateTime.Now > user.LastSecureActivity.AddMinutes(90);
                    }
                    return false;
                });

                inactive.ForEach(user =>
                {
                    if (user != null)
                    {
                        user.IsAuthenticated = false;
                    }
                });
            };
            this.timer.Start();
        }

        #endregion

        #region Public Methods

        public SupportUser Get(Guid loginId)
        {
            return this.users.Single(u => u.LoginId.ToString().IsEqual(loginId.ToString()));
        }

        public SupportUser Get(string emailAddress)
        {
            return this.users.Single(u => u.EmailAddress.IsEqual(emailAddress));
        }

        public bool IsAuthenticated(string sessionId)
        {
            this.users.ForEach(u => Windows.EventLog.WriteEntry(string.Format("{0} - {1} - {2}", u.DisplayName, u.EmailAddress, u.SessionId), System.Diagnostics.EventLogEntryType.Information));
            var result = false;
            var user = this.users.Single(u => u.SessionId.IsEqual(sessionId));
            if (user != null)
            {
                result = user.IsAuthenticated;
            }
            return result;
        }

        public SupportUser Get(Predicate<SupportUser> predicate)
        {
            return this.users.Single(predicate);
        }

        public void Log(SupportUser user)
        {
            var existingUser = this.users.Single(u => u.LoginId.ToString().IsEqual(user.LoginId.ToString()));
            if (existingUser != null)
            {
                if (existingUser.SessionId.IsEqual(user.SessionId) && existingUser.IpAddress.IsEqual(user.IpAddress))
                {
                    existingUser.DisplayName = user.DisplayName;
                    existingUser.SessionId = user.SessionId;
                    existingUser.IpAddress = user.IpAddress;
                    existingUser.ServerName = user.ServerName;
                    existingUser.EmailAddress = user.EmailAddress;
                    existingUser.LastSecureActivity = DateTime.Now;
                }
            }
        }

        public void Add(SupportUser user)
        {
            var existingUser = this.users.Single(u => u.LoginId.ToString().IsEqual(user.LoginId.ToString()));
            if (existingUser == null)
            {
                this.users.Add(user);
            }
            else
            {
                if (existingUser.SessionId.IsEqual(user.SessionId) && existingUser.IpAddress.IsEqual(user.IpAddress))
                {
                    existingUser.IsAuthenticated = true;
                    existingUser.LastSecureActivity = DateTime.Now;
                }
            }
        }

        public void Expire(string emailAddress)
        {
            var user = this.users.Single(u => u.EmailAddress.IsEqual(emailAddress));
            if (user != null)
            {
                user.IsAuthenticated = false;
            }
        }

        public int Count
        {
            get
            {
                return users.Count;
            }
        }

        public List<SupportUser> AsList()
        {
            var items = new List<SupportUser>();
            this.users.ForEach(user => items.Add(user));
            return items;
        }

        #endregion
    }
}
