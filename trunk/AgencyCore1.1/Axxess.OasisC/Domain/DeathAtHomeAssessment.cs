﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class DeathAtHomeAssessment : Assessment
    {
        public DeathAtHomeAssessment()
        {
            this.Type = AssessmentType.DischargeFromAgencyDeath.ToString();
        }
    }
}
