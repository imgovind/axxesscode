﻿namespace Axxess.OasisC.Domain
{
    using System;

    using Enums;

    [Serializable]
    public class TransferNotDischargedAssessment : Assessment
    {
        public TransferNotDischargedAssessment()
        {
            this.Type = AssessmentType.TransferInPatientNotDischarged.ToString();
        }
    }
}
