﻿using System;
namespace Axxess.OasisC.Domain
{
    [Serializable]
    public class SubmissionInactiveBodyFormat
    {
        public short Id { get; set; }
        public string Item { get; set; }
        public double Length { get; set; }
        public double Start { get; set; }
        public double End { get; set; }
        public string PadType { get; set; }
        public string DataType { get; set; }
        public string DefaultValue { get; set; }
    }
}
