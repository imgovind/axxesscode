﻿namespace Axxess.OasisC.Domain
{
    using System;
    
    using Enums;

    [Serializable]
    public class FollowUpAssessment : Assessment
    {
        public FollowUpAssessment()
        {
            this.Type = AssessmentType.FollowUp.ToString();
        }
    }
}
