﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    public class OasisCDataProvider : IOasisCDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;
        private readonly string databaseIp;

        public OasisCDataProvider(string ip)
        {
            this.database = new SimpleRepository(string.Format(CoreSettings.OasisCConnectionString,ip), CoreSettings.OasisCProviderName, SimpleRepositoryOptions.None);
            this.databaseIp = ip;
        }

        #endregion

        #region IOasisCDataProvider Members

        private IAssessmentRepository oasisAssessmentRepository;
        public IAssessmentRepository OasisAssessmentRepository
        {
            get
            {
                if (oasisAssessmentRepository == null)
                {
                    oasisAssessmentRepository = new AssessmentRepository(this.database,this.databaseIp);
                }
                return oasisAssessmentRepository;
            }
        }

        private IPlanofCareRepository planofCareRepository;
        public IPlanofCareRepository PlanofCareRepository
        {
            get
            {
                if (planofCareRepository == null)
                {
                    planofCareRepository = new PlanofCareRepository(this.database,this.databaseIp);
                }
                return planofCareRepository;
            }
        }

        private ICachedDataRepository cachedDataRepository;
        public ICachedDataRepository CachedDataRepository
        {
            get
            {
                if (cachedDataRepository == null)
                {
                    cachedDataRepository = new CachedDataRepository(this.database);
                }
                return cachedDataRepository;
            }
        }

        #endregion
    }
}

