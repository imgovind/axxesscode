﻿namespace Axxess.Scheduled.Eligibility.Service
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Configuration;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Data;
    using Model;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    public static class MedicareService
    {
        public static string EligibilityVerificationUrl = ConfigurationManager.AppSettings["PatientEligibilityUrl"];

        public static string Verify(PatientEligibilityData patient)
        {
            var jsonResult = string.Empty;
            try
            {
                var jsonData = new
                {
                    input_last_name = patient.LastName.IsNotNullOrEmpty() ? patient.LastName.Trim() : "",
                    input_medicare_number = patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber.Trim() : "",
                    input_first_name = patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.Trim().Substring(0, 1) : "",
                    input_date_of_birth = patient.DateofBirth.IsValid() && patient.DateofBirth != DateTime.MinValue ? patient.DateofBirth.ToString("MM/dd/yyyy") : "",
                    input_gender_id = patient.Gender.IsNotNullOrEmpty() && patient.Gender.Trim().Length > 0 ? patient.Gender.Trim().Substring(0, 1) : ""
                };

                var javaScriptSerializer = new JavaScriptSerializer();
                var jsonRequest = javaScriptSerializer.Serialize(jsonData);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonRequest);
                byte[] requestData = encoding.GetBytes(postData);

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(EligibilityVerificationUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jsonResult = streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return jsonResult;
        }

        public static PatientEligibilityResult ToEligibilityResult(this string jsonResult)
        {
            var javaScriptSerializer = new JavaScriptSerializer();
            return javaScriptSerializer.Deserialize<PatientEligibilityResult>(jsonResult);
        }

        public static bool Insert(PatientEligibilityData patientData, string jsonResult)
        {
            var result = false;
            if (patientData != null)
            {
                var episodeData = Database.GetCurrentEpisode(patientData);
                patientData.EligibilityId = Guid.NewGuid();
                patientData.LastEligibilityCheckDate = DateTime.Now;
                if (episodeData != null)
                {
                    patientData.EpisodeId = episodeData.Id;
                    var scheduleEvent = CreateScheduleEvent(patientData, episodeData);
                    //var scheduleEvents = episodeData.Schedule.ToObject<List<ScheduleEvent>>();
                    //scheduleEvents.Add(CreateScheduleEvent(patientData, episodeData));
                    //episodeData.Schedule = scheduleEvents.ToXml().Replace("'", "\\'");
                    if (scheduleEvent != null)
                    {
                        if (Database.InsertScheduleEvent(scheduleEvent))
                        {
                            if (Database.InsertMedicareEligibility(patientData, jsonResult))
                            {
                                if (Database.UpdatePatient(patientData))
                                {
                                    result = true;
                                }
                                else
                                {
                                    if (Database.DeleteScheduleEvent(scheduleEvent))
                                    {
                                        Database.DeleteMedicareEligibility(patientData);
                                    }
                                }
                            }
                            else
                            {
                                Database.DeleteScheduleEvent(scheduleEvent);
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static ScheduleEvent CreateScheduleEvent(PatientEligibilityData patientData, EpisodeData episodeData)
        {
            return new ScheduleEvent
            {
                AgencyId = patientData.AgencyId,
                Id = patientData.EligibilityId,
                EpisodeId = episodeData.Id,
                PatientId = patientData.Id,
                UserId = Guid.Empty,
                UserName = "Axxess",
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                DisciplineTask = (int)DisciplineTasks.MedicareEligibilityReport,
                Status = (int)ScheduleStatus.ReportAndNotesCompleted,
                EventDate = patientData.LastEligibilityCheckDate,
                VisitDate = patientData.LastEligibilityCheckDate,
                StartDate = episodeData.StartDate,
                EndDate = episodeData.EndDate,
                IsBillable = false
            };
        }
    }
}
