﻿namespace Axxess.Scheduled.Eligibility.Data
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Model;
    using Axxess.AgencyManagement.Entities;

    public static class Database
    {
        private const string SELECT_PATIENT_WITH_AGENCY_NPI_BY_PRIMARY_INSURANCE = "SELECT distinct p.Id, p.AgencyId, p.PrimaryInsurance, p.SecondaryInsurance, p.TertiaryInsurance, p.MedicareNumber, p.PatientIdNumber, p.FirstName, p.LastName, p.MiddleInitial, p.DOB, p.Gender, p.LastEligibilityCheck, a.NationalProviderNumber, m.Result as PreviousResult FROM patients p INNER JOIN agencies a on a.Id = p.AgencyId LEFT JOIN medicareeligibilities m on p.Id = m.PatientId and p.LastEligibilityCheck = m.Created WHERE (p.PrimaryInsurance = 1 OR p.PrimaryInsurance = 2 OR p.PrimaryInsurance = 3 OR p.PrimaryInsurance = 4) AND p.IsDeprecated = 0 AND p.Status = 1";
        private const string SELECT_PATIENT_BY_PRIMARY_INSURANCE = @"SELECT `Id`,`AgencyId`,`MedicareNumber`,`FirstName`,`LastName`,`MiddleInitial`,`DOB`,`Gender`, `LastEligibilityCheck` FROM `patients` WHERE (`PrimaryInsurance` = 1 || `PrimaryInsurance` = 2 || `PrimaryInsurance` = 3 || `PrimaryInsurance` = 4) AND `IsDeprecated` = 0 AND `Status` = 1;";
       
        private const string SELECT_USER_MESSAGES = @"SELECT u.Id, u.AgencyId, u.FirstName, u.LastName, u.Messages FROM users u WHERE u.permissions LIKE '%<string>8</string>%'";
        private const string SELECT_USERS_TO_SEND_MESSAGES_TO = @"SELECT u.AgencyId, GROUP_CONCAT(l.EmailAddress SEPARATOR ';') as EmailRecipients, GROUP_CONCAT(u.FirstName SEPARATOR ';') as RecipientFirstNames FROM users u JOIN axxessmembership.logins l ON u.LoginId = l.Id WHERE u.permissions LIKE '%<string>8</string>%' GROUP BY u.AgencyId";
        private const string INSERT_SYSTEM_MESSAGE = @"INSERT INTO `systemmessages` (`Id`,`Subject`,`Body`,`Created`,`CreatedBy`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')";
       
        private const string UPDATE_PATIENT_EPISODE = @"Update `patientepisodes` SET `Schedule` = '{0}' WHERE `Id` = '{1}' AND `AgencyId` = '{2}' AND `PatientId` = '{3}';";
        private const string UPDATE_USER_MESSAGES = @"Update `users` SET `Messages` = '{0}' WHERE `Id` = '{1}' AND `AgencyId` = '{2}'";
        private const string INSERT_MEDICARE_ELIGIBILITY_REPORT = @"INSERT INTO `medicareeligibilitysummaries` (`Id`,`AgencyId`,`Report`,`Created`) VALUES ('{0}', '{1}', '{2}', '{3}')";

        public static List<UserData> GetNotificationRecipients()
        {
            var usersList = new List<UserData>();
            using (var cmd = new FluentCommand<UserData>(SELECT_USERS_TO_SEND_MESSAGES_TO))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                usersList = cmd.SetMap(reader => new UserData()
                {
                    AgencyId = reader.GetGuid("AgencyId"),
                    Emails = reader.GetStringNullable("EmailRecipients"),
                    FirstNames = reader.GetStringNullable("RecipientFirstNames")
                }).AsList();
            }
            return usersList;
        }

        public static List<User> GetUserMessages()
        {
            var userMessages = new List<User>();
            using (var cmd = new FluentCommand<User>(SELECT_USER_MESSAGES))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                userMessages = cmd.SetMap(reader => new User()
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    MessageList = reader.GetStringNullable("Messages").ToObject<List<MessageState>>()
                }).AsList();
            }
            return userMessages;
        }

        public static bool InsertSystemMessage(Message message)
        {
            var result = false;
            var sql = string.Format(INSERT_SYSTEM_MESSAGE, message.Id, message.Subject, message.Body, message.Created.ToString("yyyy-MM-dd HH:mm:ss"), "Axxess");
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool AddMessage(User user, Guid messageId)
        {
            var result = false;
            MessageState state = new MessageState()
            {
                Id = messageId,
                IsRead = false,
                IsDeprecated = false
            };
            if (user.MessageList != null)
            {
                user.MessageList.Add(state);
            }
            else
            {
                user.MessageList = new List<MessageState>() { state };
            }
            user.Messages = user.MessageList.ToXml();
            var sql = string.Format(UPDATE_USER_MESSAGES, user.Messages, user.Id, user.AgencyId);
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

       

        //public static bool UpdateEpisode(EpisodeData episodeData)
        //{
        //    var result = false;
        //    var sql = string.Format(UPDATE_PATIENT_EPISODE, episodeData.Schedule, episodeData.Id, episodeData.AgencyId, episodeData.PatientId);
        //    using (var cmd = new FluentCommand<int>(sql))
        //    {
        //        cmd.SetConnection("AgencyManagementConnectionString");
        //        if (cmd.AsNonQuery() > 0)
        //        {
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        public static bool UpdatePatient(PatientEligibilityData patientData)
        {
            var result = false;
            var sql = string.Format(@"Update `patients` SET `LastEligibilityCheck` = '{0}' WHERE `Id` = '{1}' AND `AgencyId` = '{2}';", patientData.LastEligibilityCheckDate.ToString("yyyy-M-d"), patientData.Id, patientData.AgencyId);
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static Queue<PatientEligibilityData> GetPatients()
        {
            var queue = new Queue<PatientEligibilityData>();
            var patientList = new List<PatientEligibilityData>();

            using (var cmd = new FluentCommand<PatientEligibilityData>(SELECT_PATIENT_WITH_AGENCY_NPI_BY_PRIMARY_INSURANCE))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                patientList = cmd.SetMap(reader => new PatientEligibilityData
                {
                    Id = reader.GetGuid("Id"),
                    Gender = reader.GetString("Gender"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetString("LastName"),
                    DateofBirth = reader.GetDateTime("DOB"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                    SecondaryInsurance = reader.GetStringNullable("SecondaryInsurance"),
                    TertiaryInsurance = reader.GetStringNullable("TertiaryInsurance"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    LastEligibilityCheckDate = reader.GetDateTime("LastEligibilityCheck"),
                    NPI = reader.GetStringNullable("NationalProviderNumber"),
                    PreviousResult = reader.GetStringNullable("PreviousResult")
                }).AsList();
            }

            patientList.ForEach(patient =>
            {
                queue.Enqueue(patient);
            });

            return queue;
        }

        public static User GetTestUser()
        {
            var user = new User();
            using (var cmd = new FluentCommand<User>("SELECT * FROM users WHERE id = '81f4514e-10e0-4d54-812f-3d1faeedeafe'"))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                user = cmd.SetMap(reader => new User()
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    MessageList = reader.GetStringNullable("Messages").ToObject<List<MessageState>>()
                }).AsSingle();
            }
            return user;
        }


        public static bool InsertMedicareEligibility(PatientEligibilityData patientData, string rawResult)
        {
            var result = false;
            var sql = string.Format(@"INSERT INTO `medicareeligibilities` (`Id`,`AgencyId`,`PatientId`,`EpisodeId`,`Result`,`Status`,`Created`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', {5}, '{6}')", patientData.EligibilityId, patientData.AgencyId, patientData.Id, patientData.EpisodeId, rawResult.Replace("'", "\\'"), 525, patientData.LastEligibilityCheckDate.ToString("yyyy-M-d"));
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool DeleteMedicareEligibility(PatientEligibilityData patientData)
        {
            var result = false;
            var sql = @"DELETE FROM medicareeligibilities WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND Id = @id ;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(sql))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", patientData.AgencyId)
                    .AddGuid("patientid", patientData.Id)
                    .AddGuid("episodeid", patientData.EpisodeId)
                    .AddGuid("id", patientData.EligibilityId)
                    .AsNonQuery();
                if (count > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool InsertMedicareEligibilityReport(Guid agencyId, string report)
        {
            var result = false;
            var sql = string.Format(INSERT_MEDICARE_ELIGIBILITY_REPORT, Guid.NewGuid(), agencyId, report, DateTime.Now.ToString("yyyy-M-d"));
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static EpisodeData GetCurrentEpisode(PatientEligibilityData patientData)
        {
            var result = new EpisodeData();
            var sql = @"SELECT 
                            patientepisodes.Id,
                            patientepisodes.AgencyId,
                            patientepisodes.PatientId,
                            patientepisodes.EndDate,
                            patientepisodes.StartDate
                                FROM 
                                    patientepisodes 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND
                                            patientepisodes.PatientId = @patientid AND
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0 AND 
                                            CURDATE() BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate LIMIT 0,1";
            using (var cmd = new FluentCommand<EpisodeData>(sql))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", patientData.AgencyId)
                 .AddGuid("patientid", patientData.Id)
                  .SetMap(reader => new EpisodeData
                  {
                      Id = reader.GetGuid("Id"),
                      AgencyId = reader.GetGuid("AgencyId"),
                      PatientId = reader.GetGuid("PatientId"),
                      StartDate = reader.GetDateTime("StartDate"),
                      EndDate = reader.GetDateTime("EndDate")
                  }).AsSingle();
            }

            return result;
        }

        public static bool InsertScheduleEvent(ScheduleEvent schedule)
        {
            var result = false;
            var INSERT_SCHEDULEEVENTS = @"INSERT INTO 
                                        `scheduleevents` 
                                                    (
                                                        `AgencyId`,
                                                        `Id`, 
                                                        `EpisodeId`, 
                                                        `PatientId`, 
                                                        `UserId`,
                                                        `UserName`, 
                                                        `Discipline`, 
                                                        `DisciplineTask`,
                                                        `Status`,
                                                        `EventDate`,
                                                        `VisitDate`,
                                                        `StartDate`, 
                                                        `EndDate`, 
                                                        `IsBillable`,
                                                        `Asset`, 
                                                        `Version`
                                                    )
                                                    VALUES
                                                    (
                                                        '{0}', 
                                                        '{1}',
                                                        '{2}',
                                                        '{3}',
                                                        '{4}',
                                                        '{5}', 
                                                        '{6}',
                                                        '{7}',
                                                        '{8}', 
                                                        '{9}', 
                                                        '{10}',
                                                        '{11}',
                                                        '{12}', 
                                                        '{13}', 
                                                        '<ArrayOfGuid />', 
                                                        1
                                                    );";
               
            var sql = string.Format(INSERT_SCHEDULEEVENTS,
                schedule.AgencyId,
                schedule.Id,
                schedule.EpisodeId,
                schedule.PatientId,
                schedule.UserId,
                schedule.UserName,
                schedule.Discipline,
                schedule.DisciplineTask,
                schedule.Status,
                schedule.EventDate,
                schedule.VisitDate,
                schedule.StartDate,
                schedule.EndDate,
                schedule.IsBillable);
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool DeleteScheduleEvent(ScheduleEvent schedule)
        {
            var result = false;
            var sql = @"DELETE FROM scheduleevents WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND Id = @id ;";
            var count = 0;
            using (var cmd = new FluentCommand<int>(sql))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", schedule.AgencyId)
                    .AddGuid("patientid", schedule.PatientId)
                    .AddGuid("episodeid", schedule.EpisodeId)
                    .AddGuid("id", schedule.Id)
                    .AsNonQuery();
                if (count > 0)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
