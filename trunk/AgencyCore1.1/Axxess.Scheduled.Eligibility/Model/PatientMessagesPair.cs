﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Scheduled.Eligibility.Model
{
    public class PatientMessagesPair
    {
        public PatientMessageData Patient { get; set; }
        public List<string> Messages { get; set; }

        public PatientMessagesPair() { }
        public PatientMessagesPair(PatientMessageData p, List<string> m)
        {
            Patient = p;
            Messages = m;
        }
    }
}
