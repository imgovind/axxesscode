﻿//namespace Axxess.Api
//{
//    using System;
//    using System.Linq;
//    using System.Collections.Generic;

//    using Axxess.Api.Contracts;

//    using Axxess.Core.Extension;

//    public class CacheAgent : BaseAgent<ICacheService>
//    {
//        #region Overrides

//        public override string ToString()
//        {
//            return "CacheService";
//        }

//        #endregion

//        #region Base Service Methods

//        public bool Ping()
//        {
//            return Service.Ping();
//        }

//        #endregion

//        #region Cache Methods

//        public void RefreshAgency(Guid agencyId)
//        {
//            BaseAgent<ICacheService>.Call(c => c.RefreshAgency(agencyId), this.ToString());
//        }

//        public string GetAgencyXml(Guid agencyId)
//        {
//            string agencyData = string.Empty;
//            BaseAgent<ICacheService>.Call(c => agencyData = c.GetAgencyXml(agencyId), this.ToString());
//            return agencyData;
//        }

//        public void RefreshPhysicians(Guid agencyId)
//        {
//            BaseAgent<ICacheService>.Call(c => c.RefreshPhysicians(agencyId), this.ToString());
//        }

//        public string GetPhysicianXml(Guid physicianId, Guid agencyId)
//        {
//            string physicianData = string.Empty;
//            BaseAgent<ICacheService>.Call(c => physicianData = c.GetPhysicianXml(physicianId, agencyId), this.ToString());
//            return physicianData;
//        }

//        public List<string> GetPhysicians(Guid agencyId)
//        {
//            var physicians = new List<string>();
//            BaseAgent<ICacheService>.Call(c => physicians = c.GetPhysicians(agencyId), this.ToString());
//            return physicians;
//        }

//        public void RefreshUsers(Guid agencyId)
//        {
//            BaseAgent<ICacheService>.Call(c => c.RefreshUsers(agencyId), this.ToString());
//        }
       
//        public string GetUserDisplayName(Guid userId, Guid agencyId)
//        {
//            string userData = string.Empty;
//            BaseAgent<ICacheService>.Call(c => userData = c.GetUserDisplayName(userId, agencyId), this.ToString());
//            return userData;
//        }

//        public List<UserData> GetUsers()
//        {
//            var users = new List<UserData>();
//            BaseAgent<ICacheService>.Call(c => users = c.GetUsers(), this.ToString());
//            return users;
//        }

//        #endregion
//    }
//}
