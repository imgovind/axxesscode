﻿namespace Axxess.Api
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    public class AuthenticationAgent : BaseAgent<IAuthenticationService>
    {
        #region Overrides

        public override string ToString()
        {
            return "AuthenticationService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region AgencyCore Authentication Methods

        public void Login(SingleUser user)
        {
            BaseAgent<IAuthenticationService>.Call(a => a.SignIn(user), this.ToString());
        }

        public SingleUser Get(Guid loginId)
        {
            SingleUser user = null;
            BaseAgent<IAuthenticationService>.Call(a => user = a.Get(loginId), this.ToString());
            return user;
        }

        public bool Verify(SingleUser user)
        {
            var result = false;
            BaseAgent<IAuthenticationService>.Call(a => result = a.Verify(user), this.ToString());
            return result;
        }

        //public void Log(SingleUser user)
        //{
        //    BaseAgent<IAuthenticationService>.Call(a => a.Log(user), this.ToString());
        //}

        public void Logout(string emailAddress)
        {
            BaseAgent<IAuthenticationService>.Call(a => a.SignOut(emailAddress), this.ToString());
        }

        public List<SingleUser> GetUsers(bool isActive)
        {
            List<SingleUser> users = null;
            BaseAgent<IAuthenticationService>.Call(a => users = a.ToList(), this.ToString());
            if (users != null && users.Count > 0)
            {
                users = users.Where(u => u.IsAuthenticated == isActive).ToList();
            }
            return users;
        }

        public void Switch(Guid loginId, SingleUser user)
        {
            var activeUser = Get(loginId);
            if (activeUser != null)
            {
                BaseAgent<IAuthenticationService>.Call(a => a.SignOut(user.EmailAddress), this.ToString());
            }

            Login(user);
        }

        #endregion

        #region Support Authentication Methods

        public void LoginSupport(SupportUser user)
        {
            BaseAgent<IAuthenticationService>.Call(a => a.SignInSupport(user), this.ToString());
        }

        public SupportUser GetSupportUser(Guid loginId)
        {
            SupportUser user = null;
            BaseAgent<IAuthenticationService>.Call(a => user = a.GetSupportUser(loginId), this.ToString());
            return user;
        }

        public bool VerifySupportuser(SupportUser user)
        {
            var result = false;
            BaseAgent<IAuthenticationService>.Call(a => result = a.VerifySupportuser(user), this.ToString());
            return result;
        }

        public void LogSupportUser(SupportUser user)
        {
            BaseAgent<IAuthenticationService>.Call(a => a.LogSupportUser(user), this.ToString());
        }

        public void LogoutSupportUser(string emailAddress)
        {
            BaseAgent<IAuthenticationService>.Call(a => a.SignOutSupport(emailAddress), this.ToString());
        }

        public List<SupportUser> GetSupportUsers(bool isActive)
        {
            List<SupportUser> users = null;
            BaseAgent<IAuthenticationService>.Call(a => users = a.ToSupportList(), this.ToString());
            if (users != null && users.Count > 0)
            {
                users = users.Where(u => u.IsAuthenticated == isActive).ToList();
            }
            return users;
        }

        #endregion
    }
}
