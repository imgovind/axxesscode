﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
string[] homeboundStatus = data.ContainsKey("HomeboundStatus") && data["HomeboundStatus"].Answer != "" ? data["HomeboundStatus"].Answer.Split(',') : null;
string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
string[] recommendedService = data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>60 Day Summary/Case Conference<%= Model != null && Model.Patient != null ? (" | " + (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/Nursing/sixtyday.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : "") + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "patientname": "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() : "") : "").Clean() %>",
            "mr": "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : "" %>",
            "dob": "<%= Model.Patient != null ? Model.Patient.DOBFormatted : "" %>",
            "episode": "<%= data != null && Model.StartDate.IsValid() && Model.EndDate.IsValid() ? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()).Clean() : "" %>",
            "physician": "<%= Model.PhysicianDisplayName.Clean().ToTitleCase() %>",
            <%string notificationDate = ""; if (data.ContainsKey("DNR") && data["DNR"].Answer == "1" && data.ContainsKey("NotificationDate")) { if (data["NotificationDate"].Answer == "1") { notificationDate = "5 days"; } else if (data["NotificationDate"].Answer == "2") { notificationDate = "2 days"; } else if (data["NotificationDate"].Answer == "3") { notificationDate = data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "Other"; } }   %>
        
            "dnr": "<%= data != null && data != null ? (data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? "Yes" + notificationDate : "No") : ""%>",
            "sign": "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>",
            "signdate": "<%= Model != null && Model.SignatureDate != null && Model.SignatureDate.ToDateTime().IsValid() ? Model.SignatureDate : string.Empty %>"
        };
        PdfPrint.BuildSections(<%= Model.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>