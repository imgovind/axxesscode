﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="buttons">
    <ul>
        <li><a class="groupLink" onclick="CarePlanOversight.Load('AgencyName');">Group By Agency</a></li>
        <li><a class="groupLink" onclick="CarePlanOversight.Load('PatientName');">Group By Patient</a></li>
        <li><a class="groupLink" onclick="CarePlanOversight.RebindList();">Refresh</a></li>
        <li><a class="groupLink" onclick="CarePlanOversight.SubmitCpoBill($('#CPO_BillList').val());">Generate Claim</a></li>
    </ul>
</div>
<div id="cpoListContent"><% Html.RenderPartial("~/Views/Order/CPO/ListView.ascx"); %></div>    
