﻿namespace Axxess.DataLoader
{
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    using Axxess.LookUp.Domain;

    public static class LoadAgencyLocationChargeRates
    {
        public static void Run()
        {
            var locations = Database.GetAgencyLocations();
            if (locations != null && locations.Count > 0)
            {
                locations.ForEach(location =>
                {
                    if (location.BillData.IsNotNullOrEmpty())
                    {
                        var billDatas = location.BillData.ToObject<List<ChargeRate>>();
                        if (billDatas != null && billDatas.Count > 0)
                        {
                            var socRate = billDatas.SingleOrDefault(r => r.Id == (int)DisciplineTasks.OASISCStartofCare);
                            if (socRate == null)
                            {
                                socRate = new ChargeRate { };
                                socRate.Code = "G0162";
                                socRate.ChargeType = "3";
                                socRate.Charge = 200.00;
                                socRate.RevenueCode = "0551";
                                socRate.Id = (int)DisciplineTasks.OASISCStartofCare;
                                billDatas.Add(socRate);
                            }

                            var recertRate = billDatas.SingleOrDefault(r => r.Id == (int)DisciplineTasks.OASISCRecertification);
                            if (recertRate == null)
                            {
                                recertRate = new ChargeRate { };
                                recertRate.Code = "G0162";
                                recertRate.ChargeType = "3";
                                recertRate.Charge = 200.00;
                                recertRate.RevenueCode = "0551";
                                recertRate.Id = (int)DisciplineTasks.OASISCRecertification;
                                billDatas.Add(recertRate);
                            }
                            var billDataXml = billDatas.ToXml();
                            if (!location.BillData.IsEqual(billDataXml))
                            {
                                location.BillData = billDataXml;
                                Database.Update<AgencyLocation>(location);
                            }
                        }
                    }
                });
            }
        }

        public static void RunToUpdateTheDisciplineTasks()
        {
            var disciplineTasks = Database.GetDisciplineTasks();
            if (disciplineTasks != null && disciplineTasks.Count > 0)
            {
                disciplineTasks.ForEach(task =>
                {
                    SetBillRateValue(task);
                    Database.UpdateForLookup<DisciplineTask>(task);
                });
            }
        }

        public static List<DisciplineTasks> SetBillRateValue(ChargeRate rate, string discipline)
        {
            var tasks = new List<DisciplineTasks>();
            switch (discipline)
            {
                case "SkilledNurse":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0154";
                        rate.Charge = 200.00;
                        tasks = new List<DisciplineTasks> {
                            DisciplineTasks.OASISCDeath,
                            DisciplineTasks.OASISCDischarge,
                            DisciplineTasks.NonOASISDischarge,
                            DisciplineTasks.OASISCFollowUp,
                            DisciplineTasks.NonOASISRecertification,
                            DisciplineTasks.OASISCResumptionofCare,
                            DisciplineTasks.NonOASISStartofCare,
                            DisciplineTasks.OASISCTransfer,
                            DisciplineTasks.OASISCTransferDischarge,
                            DisciplineTasks.SNAssessment,
                            DisciplineTasks.SNAssessmentRecert,
                            DisciplineTasks.SkilledNurseVisit,
                            DisciplineTasks.SNInsulinAM,
                            DisciplineTasks.SNInsulinPM,
                            DisciplineTasks.FoleyCathChange,
                            DisciplineTasks.SNB12INJ,
                            DisciplineTasks.SNBMP,
                            DisciplineTasks.SNCBC,
                            DisciplineTasks.SNHaldolInj,
                            DisciplineTasks.PICCMidlinePlacement,
                            DisciplineTasks.PRNFoleyChange,
                            DisciplineTasks.PRNSNV,
                            DisciplineTasks.PRNVPforCMP,
                            DisciplineTasks.PTWithINR,
                            DisciplineTasks.PTWithINRPRNSNV,
                            DisciplineTasks.SkilledNurseHomeInfusionSD,
                            DisciplineTasks.SkilledNurseHomeInfusionSDAdditional,
                            DisciplineTasks.SNDC,
                            DisciplineTasks.SNEvaluation,
                            DisciplineTasks.SNFoleyLabs,
                            DisciplineTasks.SNFoleyChange,
                            DisciplineTasks.SNInjection,
                            DisciplineTasks.SNInjectionLabs,
                            DisciplineTasks.SNLabsSN,
                            DisciplineTasks.SNVPsychNurse,
                            DisciplineTasks.SNVwithAideSupervision,
                            DisciplineTasks.SNVDCPlanning,
                            DisciplineTasks.LVNSupervisoryVisit,
                            DisciplineTasks.DieticianVisit,
                            DisciplineTasks.DischargeSummary,
                            DisciplineTasks.SixtyDaySummary,
                            DisciplineTasks.TransferSummary,
                            DisciplineTasks.CoordinationOfCare,
                            DisciplineTasks.SNDiabeticDailyVisit,
                            DisciplineTasks.SNPediatricVisit,
                            DisciplineTasks.SNPsychAssessment,
                            DisciplineTasks.SNInsulinHS,
                            DisciplineTasks.SNInsulinNoon
                       };
                    }
                    break;
                case "SkilledNurseManagement":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0162";
                        rate.Charge = 200.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.SNVManagementAndEvaluation };
                    }
                    break;
                case "SkilledNurseObservation":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0163";
                        rate.Charge = 200.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.SNVObservationAndAssessment };
                    }
                    break;
                case "SkilledNurseTeaching":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0164";
                        rate.Charge = 200.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.SNVTeachingTraining };
                    }
                    break;
                case "HomeHealthAide":
                    {
                        rate.Unit = 4;
                        rate.RevenueCode = "0571";
                        rate.Code = "G0156";
                        rate.Charge = 120.00;
                        tasks = new List<DisciplineTasks>{
                                       DisciplineTasks.HHAideSupervisoryVisit,
                                       DisciplineTasks.HHAideVisit,
                                       DisciplineTasks.HomeMakerNote,
                                       DisciplineTasks.PASVisit};
                    }
                    break;
                case "PhysicalTherapy":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0421";
                        rate.Code = "G0151";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks>{
                             DisciplineTasks.OASISCStartofCarePT,
                             DisciplineTasks.OASISCResumptionofCarePT,
                             DisciplineTasks.OASISCFollowupPT,
                             DisciplineTasks.OASISCTransferPT, 
                             DisciplineTasks.OASISCDischargePT,
                             DisciplineTasks.OASISCTransferDischargePT,
                             DisciplineTasks.OASISCDeathPT,
                             DisciplineTasks.PTVisit,
                             DisciplineTasks.PTEvaluation,
                             DisciplineTasks.PTReEvaluation,
                             DisciplineTasks.PTDischarge,
                             DisciplineTasks.PTDischargeSummary,
                             DisciplineTasks.PTSupervisoryVisit
                         };
                    }
                    break;
                case "PhysicalTherapyAssistance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0421";
                        rate.Code = "G0157";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.PTAVisit };
                    }
                    break;
                case "PhysicalTherapyMaintenance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0421";
                        rate.Code = "G0159";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.PTMaintenance };
                    }
                    break;
                case "OccupationalTherapy":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0431";
                        rate.Code = "G0152";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks>{  
                             DisciplineTasks.OASISCResumptionofCareOT,
                             DisciplineTasks.OASISCFollowupOT,
                             DisciplineTasks.OASISCTransferOT,
                             DisciplineTasks.OASISCDischargeOT,
                             DisciplineTasks.OASISCDeathOT,
                             DisciplineTasks.OTDischarge,
                             DisciplineTasks.OTVisit,
                             DisciplineTasks.OTEvaluation,
                             DisciplineTasks.OTReEvaluation,
                             DisciplineTasks.OTDischargeSummary,
                            DisciplineTasks.OTSupervisoryVisit};
                    }
                    break;
                case "OccupationalTherapyAssistance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0431";
                        rate.Code = "G0158";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.OTMaintenance };
                    }
                    break;
                case "OccupationalTherapyMaintenance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0431";
                        rate.Code = "G0160";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.COTAVisit };
                    }
                    break;
                case "SpeechTherapy":
                    {
                        rate.Unit = 4;
                        rate.RevenueCode = "0440";
                        rate.Code = "G0153";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks>{
                           DisciplineTasks.STDischarge,
                           DisciplineTasks.STVisit,
                           DisciplineTasks.STEvaluation,
                           DisciplineTasks.STReEvaluation};
                    }
                    break;
                case "SpeechTherapyMaintenance":
                    {
                        rate.Unit = 4;
                        rate.RevenueCode = "0440";
                        rate.Code = "G0161";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks> { DisciplineTasks.STMaintenance };
                    }
                    break;
                case "MedicareSocialWorker":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0561";
                        rate.Code = "G0155";
                        rate.Charge = 250.00;
                        tasks = new List<DisciplineTasks>{
                   DisciplineTasks.MSWEvaluationAssessment,
                   DisciplineTasks.MSWVisit,
                   DisciplineTasks.MSWDischarge,
                   DisciplineTasks.MSWAssessment, 
                   DisciplineTasks.MSWProgressNote};
                    }
                    break;
                case "SkilledNurseSOC":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0162";
                        rate.Charge = 200.00;
                        tasks = new List<DisciplineTasks> { 
                               DisciplineTasks.OASISCStartofCare,
                               DisciplineTasks.OASISCStartofCareOT
                              };
                    }
                    break;
                case "SkilledNurseRecet":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0162";
                        rate.Charge = 200.00;
                        tasks = new List<DisciplineTasks> {
                           DisciplineTasks.OASISCRecertification,
                           DisciplineTasks.OASISCRecertificationPT,
                           DisciplineTasks.OASISCRecertificationOT
                       };
                    }
                    break;
            }
            return tasks;
        }

        public static void SetBillRateValue(DisciplineTask task)
        {
            switch (task.Id)
            {
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0551";
                        task.GCode = "G0162";
                        task.Rate = 200.00;
                    }
                    break;
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0551";
                        task.GCode = "G0162";
                        task.Rate = 200.00;
                    }
                    break;
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.SNPediatricVisit:
                case (int)DisciplineTasks.SNPsychAssessment:
                case (int)DisciplineTasks.SNInsulinHS:
                case (int)DisciplineTasks.SNInsulinNoon:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0551";
                        task.GCode = "G0154";
                        task.Rate = 200.00;
                    }
                    break;

                case (int)DisciplineTasks.SNVTeachingTraining:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0551";
                        task.GCode = "G0164";
                        task.Rate = 200.00;
                    }
                    break;

                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0551";
                        task.GCode = "G0162";
                        task.Rate = 200.00;
                    }
                    break;

                case (int)DisciplineTasks.SNVObservationAndAssessment:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0551";
                        task.GCode = "G0163";
                        task.Rate = 200.00;
                    }
                    break;

                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.OASISCTransferDischargePT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.PTDischargeSummary:
                case (int)DisciplineTasks.PTSupervisoryVisit:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0421";
                        task.GCode = "G0151";
                        task.Rate = 250.00;
                    }
                    break;

                case (int)DisciplineTasks.PTAVisit:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0421";
                        task.GCode = "G0157";
                        task.Rate = 250.00;
                    }
                    break;

                case (int)DisciplineTasks.PTMaintenance:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0421";
                        task.GCode = "G0159";
                        task.Rate = 250.00;
                    }
                    break;

                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTDischargeSummary:
                case (int)DisciplineTasks.OTSupervisoryVisit:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0431";
                        task.GCode = "G0152";
                        task.Rate = 250.00;
                    }
                    break;
                case (int)DisciplineTasks.OTMaintenance:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0431";
                        task.GCode = "G0160";
                        task.Rate = 250.00;
                    }
                    break;

                case (int)DisciplineTasks.STMaintenance:
                    {
                        task.Unit = 4;
                        task.RevenueCode = "0440";
                        task.GCode = "G0161";
                        task.Rate = 250.00;
                    }
                    break;

                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                    {
                        task.Unit = 4;
                        task.RevenueCode = "0440";
                        task.GCode = "G0153";
                        task.Rate = 250.00;
                    }
                    break;

                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0561";
                        task.GCode = "G0155";
                        task.Rate = 250.00;
                    }
                    break;
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HomeMakerNote:
                case (int)DisciplineTasks.PASVisit:
                    {
                        task.Unit = 4;
                        task.RevenueCode = "0571";
                        task.GCode = "G0156";
                        task.Rate = 120.00;
                    }
                    break;

                case (int)DisciplineTasks.COTAVisit:
                    {
                        task.Unit = 3;
                        task.RevenueCode = "0431";
                        task.GCode = "G0158";
                        task.Rate = 250.00;
                    }
                    break;
            }
        }

        public static Dictionary<string, CostRate> ToCostRateDictionary(this AgencyLocation agencyLocation)
        {
            var questions = new Dictionary<string, CostRate>();
            if (agencyLocation != null && agencyLocation.Cost.IsNotNullOrEmpty())
            {
                var costRateQuestions = agencyLocation.Cost.ToObject<List<CostRate>>();
                if (costRateQuestions != null && costRateQuestions.Count > 0)
                {
                    costRateQuestions.ForEach(n =>
                    {
                        questions.Add(n.RateDiscipline, n);
                    });
                }
            }
            return questions;
        }
    }
}
