﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public static class PermissionScript
    {
        public static void Run()
        {
            var users = Database.GetUsers();
            if (users != null && users.Count > 0)
            {
                Console.WriteLine("users List Count: {0}", users.Count);
                users.ForEach(u =>
                {
                    var roles = u.Roles.IsNotNullOrEmpty() ? u.Roles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };
                    if (roles.Contains(((int)AgencyRoles.Administrator).ToString()))
                    {
                        var permissions = u.Permissions.IsNotNullOrEmpty() ? u.Permissions.ToObject<List<string>>() : new List<string>();
                        if (permissions != null && permissions.Count > 0)
                        {
                                permissions.Add(((ulong)Permissions.ManageAgencyInformation).ToString());
                                u.PermissionsArray = permissions;
                                if (u.PermissionsArray != null && u.PermissionsArray.Count > 0)
                                {
                                    u.Permissions = u.PermissionsArray.ToXml();
                                }
                                if (Database.Update<User>(u))
                                {
                                    Console.WriteLine("{0}'s Permission updated", u.DisplayName);
                                }
                        }
                    }
                });
            }
        }
    }
}
