﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;

    public static class OasisSubmissionIdScript
    {
        public static void Run(Guid agencyId, List<string> wrongFacilityIds, string newFacilityId)
        {
            var oasisAssessments = Database.GetAssessmentsNewWithSubmissionFormat<Assessment>(agencyId);
            if (oasisAssessments != null && oasisAssessments.Count > 0)
            {
                ReplaceFacilityId<Assessment>(agencyId,oasisAssessments, wrongFacilityIds, newFacilityId);
            }
            //var socList = Database.GetAssessments<StartOfCareAssessment>(agencyId);
            //if (socList != null && socList.Count > 0)
            //{
            //    ReplaceFacilityId<StartOfCareAssessment>(socList, wrongFacilityIds, newFacilityId);
            //}

            //var rocList = Database.GetAssessments<ResumptionofCareAssessment>(agencyId);
            //if (rocList != null && rocList.Count > 0)
            //{
            //    ReplaceFacilityId<ResumptionofCareAssessment>(rocList, wrongFacilityIds, newFacilityId);
            //}

            //var recertList = Database.GetAssessments<RecertificationAssessment>(agencyId);
            //if (recertList != null && recertList.Count > 0)
            //{
            //    ReplaceFacilityId<RecertificationAssessment>(recertList, wrongFacilityIds, newFacilityId);
            //}

            //var followupList = Database.GetAssessments<FollowUpAssessment>(agencyId);
            //if (followupList != null && followupList.Count > 0)
            //{
            //    ReplaceFacilityId<FollowUpAssessment>(followupList, wrongFacilityIds, newFacilityId);
            //}

            //var deathList = Database.GetAssessments<DeathAtHomeAssessment>(agencyId);
            //if (deathList != null && deathList.Count > 0)
            //{
            //    ReplaceFacilityId<DeathAtHomeAssessment>(deathList, wrongFacilityIds, newFacilityId);
            //}

            //var dischargeList = Database.GetAssessments<DischargeFromAgencyAssessment>(agencyId);
            //if (dischargeList != null && dischargeList.Count > 0)
            //{
            //    ReplaceFacilityId<DischargeFromAgencyAssessment>(dischargeList, wrongFacilityIds, newFacilityId);
            //}

            //var transferList = Database.GetAssessments<TransferDischargeAssessment>(agencyId);
            //if (transferList != null && transferList.Count > 0)
            //{
            //    ReplaceFacilityId<TransferDischargeAssessment>(transferList, wrongFacilityIds, newFacilityId);
            //}

            //var transferNotList = Database.GetAssessments<TransferNotDischargedAssessment>(agencyId);
            //if (transferNotList != null && transferNotList.Count > 0)
            //{
            //    ReplaceFacilityId<TransferNotDischargedAssessment>(transferNotList, wrongFacilityIds, newFacilityId);
            //}
        }

        private static void ReplaceFacilityId<T>(Guid agencyId, List<T> list, List<string> wrongFacilityIds, string newFacilityId) where T : Assessment, new()
        {
            Console.WriteLine("Item Count: {0}", list.Count);
            list.ForEach(a =>
            {
                //if (a.SubmissionFormat.IsNotNullOrEmpty() && !a.SubmissionFormat.ToLower().Contains(newFacilityId.ToLower()))
                //{
                //    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);
                //    var assessment = Database.GetAssessment<T>(a.AgencyId, a.EpisodeId, a.PatientId, a.Id);
                //    if (assessment != null)
                //    {
                //        assessment.SubmissionFormat = result;
                //        Database.UpdateForOasisC<T>((T)assessment);
                //    }
                //    else
                //    {
                //        Console.WriteLine("Nothing to change");
                //    }
                //}
                if (a.SubmissionFormat.IsNotNullOrEmpty() && !a.SubmissionFormat.ToLower().Contains(newFacilityId.ToLower()))
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);
                    //var assessment = Database.GetAssessment<T>(a.AgencyId, a.EpisodeId, a.PatientId, a.Id);
                    if (result.IsNotNullOrEmpty())
                    {
                        Database.UpdateForOasisCSubmissionFormatNew<T>(agencyId, a.PatientId, a.EpisodeId, a.Id, result);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                }

            });
        }


        private static string ReplaceWrongFacilityId(string submissionFormat, List<string> wrongFacilityIds, string newFacilityId)
        {
            var result = submissionFormat;
            if (submissionFormat.IsNotNullOrEmpty() && submissionFormat.Length == 1446)
            {
                wrongFacilityIds.ForEach(wrongId =>
                {
                    if (submissionFormat.Contains(wrongId))
                    {
                        Console.WriteLine("Wrong Id: {0}", wrongId);
                        if (wrongId.Length == newFacilityId.Length)
                        {
                            result = submissionFormat.Replace(wrongId, newFacilityId);
                        }
                        else if (wrongId.Length > newFacilityId.Length)
                        {
                            var diff = wrongId.Length - newFacilityId.Length;
                            var total = newFacilityId.Length + diff;
                            result = submissionFormat.Replace(wrongId, newFacilityId.PadRight(total, ' '));
                        }
                        else if (wrongId.Length < newFacilityId.Length)
                        {
                            var diff = newFacilityId.Length - wrongId.Length;
                            var total = wrongId.Length + diff;
                            result = submissionFormat.Replace(wrongId.PadRight(total, ' '), newFacilityId);
                        }

                        Console.WriteLine("Result Length == SubmissionFormat : {0}", result.Length == submissionFormat.Length);
                        Console.WriteLine("Result Lenght: {0}", result.Length);
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("This Wrong Id not Found.");
                        Console.WriteLine();
                    }
                });
            }
            return result;
        }
    }
}
