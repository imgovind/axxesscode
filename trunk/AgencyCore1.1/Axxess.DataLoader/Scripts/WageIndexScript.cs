﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Excel;
    using HtmlAgilityPack;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class WageIndexScript
    {
        private static TextWriter textWriter;
        private static List<string> completedZips = new List<string>();
        private static List<string> uniqueCbsas = new List<string>();
        private static List<string> uniqueZips = new List<string>();
        private static List<ZipEntry> zipEntries = new List<ZipEntry>();
        private static List<CbsaEntry> cbsaEntries = new List<CbsaEntry>();
        private static List<ZipCbsaEntry> zipCbsaEntries = new List<ZipCbsaEntry>();
        private static List<StateWageIndex> stateWageIndexes = new List<StateWageIndex>();

        private static Dictionary<string, string> stateNames = new Dictionary<string, string>
        {
            { "N Carolina", "North Carolina" },
            { "S Carolina", "South Carolina" },
            { "N Dakota", "North Dakota" },
            { "S Dakota", "South Dakota" },
            { "W Virginia", "West Virginia" },
            { "Dist Of Col", "District of Columbia" }
        };

        private static string zipcodeinput = Path.Combine(App.Root, "Files\\zipcodes.csv");
        private static string wageindexinput = Path.Combine(App.Root, "Files\\WageIndex.xlsx");
        private static string statewageindex2011input = Path.Combine(App.Root, "Files\\wageindex2011.xls");
        private static string statewageindex2012input = Path.Combine(App.Root, "Files\\wageindex2012.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\zipcbsaoutput_{0}.txt", DateTime.Now.Ticks));

        public static void Run()
        {
            using (textWriter = new StreamWriter(output, true))
            {
                try
                {
                    // Load 2011 Wage Index Table
                    //LoadZipCodes();
                    //LoadWageIndex2011();
                    //LoadStateWageIndex2011();
                    //MergeZipCodesWithCbsaEntries();
                    //RevisitStateWageIndex2011Entries(5);
                    //PrintZipCbsaCodesInsert();

                    // Load 2012 Wage Index into Existing CbsaCode table
                    LoadWageIndex2012();
                    LoadStateWageIndex2012();
                    LoadZipCbsaCodesFromDatabase();
                    RevisitStateWageIndex2012Entries(3);
                    PrintZipCbsaCodesUpdate();
                }
                catch (WebException webEx)
                {
                    Console.Write(webEx.ToString());
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
            }
        }

        private static void LoadZipCbsaCodesFromDatabase()
        {
            var i = 0;
            var list = Database.GetCbsaCodes();
            list.ForEach(cbsaCode =>
            {
                if (cbsaCode.StateName.IsNotNullOrEmpty())
                {
                    var zipCbsaEntry = new ZipCbsaEntry
                    {
                        Id = cbsaCode.Id,
                        Zip = cbsaCode.Zip,
                        StateName = cbsaCode.StateName
                    };
                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        zipCbsaEntry.CBSA = cbsaCode.CBSA;

                        var cbsaEntry = cbsaEntries.Find(c => c.CBSA.IsEqual(cbsaCode.CBSA) && c.State.StartsWith(cbsaCode.StateName));
                        if (cbsaEntry != null)
                        {
                            zipCbsaEntry.IsFound = true;
                            zipCbsaEntry.WageIndex2012 = cbsaEntry.WageIndex;
                        }
                    }

                    if (zipCbsaEntry.WageIndex2012 == 0 || zipCbsaEntry.IsFound == false)
                    {
                        var siteId = (i + 1) % 4;
                        var county = string.Empty;
                        switch (siteId)
                        {
                            case 0:
                                try
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 1:
                                try
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 2:
                                try
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 3:
                                try
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                break;
                        }


                        if (county.IsNotNullOrEmpty())
                        {
                            if (county.StartsWith("St ") || county.StartsWith("st ") || county.StartsWith("St. ") || county.StartsWith("st. "))
                            {
                                county = county.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                            }
                            var matchingState = cbsaEntries.Find(c =>
                            {
                                if (c.State.StartsWith(zipCbsaEntry.StateName) && c.Counties.Exists(e => e.Title.IsEqual(county)))
                                {
                                    return true;
                                }
                                return false;
                            });
                            if (matchingState != null)
                            {
                                zipCbsaEntry.IsFound = true;
                                zipCbsaEntry.WageIndex2012 = matchingState.WageIndex;
                            }
                        }
                    }

                    if (zipCbsaEntry.WageIndex2012 == 0 || zipCbsaEntry.IsFound == false)
                    {
                        var stateWageIndex = stateWageIndexes.Find(w => w.State.StartsWith(cbsaCode.StateName));
                        if (stateWageIndex != null)
                        {
                            zipCbsaEntry.IsFound = true;
                            zipCbsaEntry.WageIndex2012 = stateWageIndex.WageIndex;
                        }
                    }

                    zipCbsaEntries.Add(zipCbsaEntry);
                    i++;
                    Console.WriteLine("{2} Row: {0} of {1}", i, list.Count, zipCbsaEntry.IsFound ? "Found" : "Didn't Find");
                }
            });
            Console.WriteLine("Load Complete");
        }

        private static void PrintZipCbsaCodesInsert()
        {
            zipCbsaEntries.ForEach(zipCbsaEntry =>
            {
                Console.WriteLine(zipCbsaEntry.ToString());
                Console.WriteLine();
                textWriter.WriteLine(zipCbsaEntry.ToSQL());
            });
        }

        private static void PrintZipCbsaCodesUpdate()
        {
            zipCbsaEntries.ForEach(zipCbsaEntry =>
            {
                Console.WriteLine(zipCbsaEntry.ToUpdateSQL());
                Console.WriteLine();
                textWriter.WriteLine(zipCbsaEntry.ToUpdateSQL());
            });
        }

        private static void RevisitStateWageIndex2011Entries(short attempts)
        {
            short attempt = 0;
            do
            {
                var corrections = 0;
                zipCbsaEntries.ForEach(zipCbsaEntry =>
                {
                    if (zipCbsaEntry.CBSA.IsNotNullOrEmpty() && zipCbsaEntry.CBSA.StartsWith("9") && zipCbsaEntry.CountyTitle.IsNullOrEmpty())
                    {
                        var siteId = (corrections + 1) % 4;
                        var county = string.Empty;
                        switch (siteId)
                        {
                            case 0:
                                try
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 1:
                                try
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 2:
                                try
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 3:
                                try
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                break;
                        }

                        if (county.IsNotNullOrEmpty())
                        {
                            if (county.StartsWith("St ") || county.StartsWith("st ") || county.StartsWith("St. ") || county.StartsWith("st. "))
                            {
                                county = county.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                            }
                            var matchingState = cbsaEntries.Find(c =>
                            {
                                if (c.State.StartsWith(zipCbsaEntry.StateName) && c.Counties.Exists(e => e.Title.IsEqual(county)))
                                {
                                    return true;
                                }
                                return false;
                            });
                            if (matchingState != null)
                            {
                                zipCbsaEntry.CBSA = matchingState.CBSA;
                                zipCbsaEntry.WageIndex2011 = matchingState.WageIndex;
                                var countyInfo = matchingState.Counties.Find(e => e.Title.IsEqual(county));
                                if (countyInfo != null)
                                {
                                    zipCbsaEntry.CountyTitle = countyInfo.Title;
                                    zipCbsaEntry.CountyCode = countyInfo.Code;
                                }
                            }
                        }
                        corrections++;
                    }
                });
                attempt++;
                Console.WriteLine("Attempt {0} Complete with {1} Corrections", attempt, corrections);
            }
            while (attempt <= attempts);
        }

        private static void RevisitStateWageIndex2012Entries(short attempts)
        {
            short attempt = 0;
            do
            {
                var corrections = 0;
                zipCbsaEntries.ForEach(zipCbsaEntry =>
                {
                    if (zipCbsaEntry.IsFound == false)
                    {
                        var siteId = (corrections + 1) % 4;
                        var county = string.Empty;
                        switch (siteId)
                        {
                            case 0:
                                try
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 1:
                                try
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 2:
                                try
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 3:
                                try
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                break;
                        }

                        if (county.IsNotNullOrEmpty())
                        {
                            if (county.StartsWith("St ") || county.StartsWith("st ") || county.StartsWith("St. ") || county.StartsWith("st. "))
                            {
                                county = county.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                            }
                            var matchingState = cbsaEntries.Find(c =>
                            {
                                if (c.State.StartsWith(zipCbsaEntry.StateName) && c.Counties.Exists(e => e.Title.IsEqual(county)))
                                {
                                    return true;
                                }
                                return false;
                            });
                            if (matchingState != null)
                            {
                                zipCbsaEntry.IsFound = true;
                                zipCbsaEntry.WageIndex2012 = matchingState.WageIndex;
                            }
                        }
                        corrections++;
                    }
                });
                attempt++;
                Console.WriteLine("Attempt {0} Complete with {1} Corrections", attempt, corrections);
            }
            while (attempt <= attempts);
        }

        private static void MergeZipCodesWithCbsaEntries()
        {
            var i = 0;
            zipEntries.ForEach(zip =>
            {
                if (!completedZips.Contains(zip.Zip))
                {
                    var zipCbsaEntry = new ZipCbsaEntry
                    {
                        Zip = zip.Zip,
                        CityName = zip.City,
                        StateCode = zip.State,
                        StateName = zip.StateFull
                    };

                    if (zip.CBSA.IsNotNullOrEmpty())
                    {
                        zipCbsaEntry.CBSA = zip.CBSA;

                        var cbsaEntry = cbsaEntries.Find(c => c.CBSA.IsEqual(zip.CBSA) && c.State.StartsWith(zip.StateFull));
                        if (cbsaEntry != null)
                        {
                            zipCbsaEntry.WageIndex2011 = cbsaEntry.WageIndex;
                        }
                    }

                    if (zipCbsaEntry.CBSA.IsNullOrEmpty() || zipCbsaEntry.WageIndex2011 == 0)
                    {
                        var siteId = (i + 1) % 4;
                        var county = string.Empty;
                        switch (siteId)
                        {
                            case 0:
                                try
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 1:
                                try
                                {
                                    county = GetCountyFromPublicRecordsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 2:
                                try
                                {
                                    county = GetCountyFromCensusBureauUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                break;
                            case 3:
                                try
                                {
                                    county = GetCountyFromGetZipsUrl(zipCbsaEntry.Zip);
                                }
                                catch (Exception)
                                {
                                    county = GetCountyFromZipInfoUrl(zipCbsaEntry.Zip);
                                }
                                break;
                        }


                        if (county.IsNotNullOrEmpty())
                        {
                            if (county.StartsWith("St ") || county.StartsWith("st ") || county.StartsWith("St. ") || county.StartsWith("st. "))
                            {
                                county = county.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                            }
                            var matchingState = cbsaEntries.Find(c =>
                            {
                                if (c.State.StartsWith(zipCbsaEntry.StateName) && c.Counties.Exists(e => e.Title.IsEqual(county)))
                                {
                                    return true;
                                }
                                return false;
                            });
                            if (matchingState != null)
                            {
                                zipCbsaEntry.CBSA = matchingState.CBSA;
                                zipCbsaEntry.WageIndex2011 = matchingState.WageIndex;
                                var countyInfo = matchingState.Counties.Find(e => e.Title.IsEqual(county));
                                if (countyInfo != null)
                                {
                                    zipCbsaEntry.CountyTitle = countyInfo.Title;
                                    zipCbsaEntry.CountyCode = countyInfo.Code;
                                }
                            }
                        }
                    }

                    if (zipCbsaEntry.WageIndex2011 == 0)
                    {
                        var stateWageIndex = stateWageIndexes.Find(w => w.State.StartsWith(zip.StateFull));
                        if (stateWageIndex != null)
                        {
                            zipCbsaEntry.CBSA = stateWageIndex.CBSA;
                            zipCbsaEntry.WageIndex2011 = stateWageIndex.WageIndex;
                        }
                    }
                    zipCbsaEntries.Add(zipCbsaEntry);
                    i++;
                    Console.WriteLine("Row: {0} of {1}", i, zipEntries.Count);
                }
            });
            Console.WriteLine("Merge Complete");
        }

        private static string GetCountyFromCensusBureauUrl(string zipCode)
        {
            string county = string.Empty;
            string rawHtml = string.Empty;
            using (WebClient client = new WebClient())
            {
                var form = new NameValueCollection();
                form.Add("Go.x", "9");
                form.Add("Go.y", "5");
                form.Add("place", zipCode);
                form.Add("state", "00000");
                Byte[] responseData = client.UploadValues("http://quickfacts.census.gov/cgi-bin/qfd/lookup", form);
                rawHtml = Encoding.ASCII.GetString(responseData);
            }
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(rawHtml);
            foreach (HtmlNode imageNode in htmlDocument.DocumentNode.SelectNodes("//img"))
            {
                imageNode.Remove();
            }
            var nodes = htmlDocument.DocumentNode.SelectNodes("//span");
            var countyText = nodes.ToList().Find(d => d.HasAttributes && d.Attributes["CLASS"]  != null && d.Attributes["CLASS"].Value.IsEqual("TEN"));
            var countyArray = countyText != null && countyText.InnerText.IsNotNullOrEmpty() ? countyText.InnerText.Replace("\r", "").Replace("\t", "").Replace("\n", "").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };
            if (countyArray != null && countyArray.Length > 2)
            {
                county = countyArray[1].Replace("County", "").Trim();
            }
            return county;
        }

        private static string GetCountyFromPublicRecordsUrl(string zipCode)
        {
            bool isFound = false;
            string county = string.Empty;
            string rawHtml = string.Empty;
            using (WebClient client = new WebClient())
            {
                rawHtml = client.DownloadString(string.Format("http://publicrecords.onlinesearches.com/zip-ac.php?m=1&ZC={0}", zipCode));
            }
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(rawHtml);
            foreach (HtmlNode imageNode in htmlDocument.DocumentNode.SelectNodes("//img"))
            {
                imageNode.Remove();
            }
            var nodes = htmlDocument.DocumentNode.SelectNodes("//table");
            if (nodes != null && nodes.Count > 0)
            {
                var tableNode = nodes.Last();
                if (tableNode != null)
                {
                    var rowNodes = tableNode.SelectNodes("//tr");
                    if (rowNodes != null && rowNodes.Count > 0)
                    {
                        var rowNode = rowNodes.Last();
                        if (rowNode != null && rowNode.HasChildNodes)
                        {
                            rowNode.ChildNodes.ToList().ForEach(dataNode =>
                            {
                                if (isFound) return;
                                if (dataNode.Name.IsEqual("td") && dataNode.InnerHtml.IsNotNullOrEmpty() && dataNode.InnerHtml.StartsWith("<a href"))
                                {
                                    var countyArray = dataNode.InnerText.Trim().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                    if (countyArray != null && countyArray.Length > 1)
                                    {
                                        county = countyArray[0].Replace("County", "").Replace("county", "").Trim();
                                    }
                                    isFound = true;
                                }
                            });
                        }
                    }
                }
            }
            return county;
        }

        private static string GetCountyFromZipInfoUrl(string zipCode)
        {
            string county = string.Empty;
            string rawHtml = string.Empty;
            using (WebClient client = new WebClient())
            {
                rawHtml = client.DownloadString(string.Format("http://zipinfo.com/cgi-local/zipsrch.exe?cnty=cnty&zip={0}&Go=Go", zipCode));
            }
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(rawHtml);
            foreach (HtmlNode imageNode in htmlDocument.DocumentNode.SelectNodes("//img"))
            {
                imageNode.Remove();
            }
            var node = htmlDocument.DocumentNode.SelectNodes("//table");
            if (node != null && node.Count > 3)
            {
                var table = node[3];
                if (table != null && table.HasChildNodes)
                {
                    if (table.LastChild != null)
                    {
                        var cells = table.LastChild.SelectNodes("//td");
                        if (cells != null && cells.Count > 7)
                        {
                            county = cells[7].InnerText;
                        }
                    }
                }
            }
            return county;
        }

        private static string GetCountyFromGetZipsUrl(string zipCode)
        {
            bool isFound = false;
            string county = string.Empty;
            string rawHtml = string.Empty;
            using (var client = new WebClient())
            {
                rawHtml = client.DownloadString(string.Format("http://www.getzips.com/cgi-bin/ziplook.exe?What=1&Zip={0}&Submit=Look+It+Up", zipCode));
            }

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(rawHtml);
            foreach (HtmlNode imageNode in htmlDocument.DocumentNode.SelectNodes("//img"))
            {
                imageNode.Remove();
            }
            var nodes = htmlDocument.DocumentNode.SelectNodes("//table");
            if (nodes != null && nodes.Count > 0)
            {
                var tableNode = nodes.Last();
                if (tableNode != null)
                {
                    var rowNodes = tableNode.SelectNodes("//tr");
                    if (rowNodes != null && rowNodes.Count > 0)
                    {
                        var rowNode = rowNodes.Last();
                        if (rowNode != null && rowNode.HasChildNodes)
                        {
                            rowNode.ChildNodes.ToList().ForEach(dataNode =>
                            {
                                if (isFound) return;
                                if (dataNode.Name.IsEqual("td") && dataNode.Attributes["WIDTH"].Value.IsEqual("25%"))
                                {
                                    if (dataNode.InnerText.IsNotNullOrEmpty())
                                    {
                                        county = dataNode.InnerText.Trim();
                                        isFound = true;
                                    }
                                }
                            });
                        }
                    }
                }
            }
        
            return county;
        }

        private static void LoadZipCodes()
        {
            using (FileStream zipCodeStream = new FileStream(zipcodeinput, FileMode.Open, FileAccess.Read))
            {
                using (var csvReader = new CsvReader(zipCodeStream))
                {
                    if (csvReader != null)
                    {
                        var zipCounter = 1;
                        csvReader.ReadHeaderRecord();
                        foreach (var dataRow in csvReader.DataRecords)
                        {
                            if (dataRow.GetValue(0).IsNotNullOrEmpty() && dataRow.GetValue(1).IsNotNullOrEmpty())
                            {
                                var zipCode = dataRow.GetValue(0);

                                if (!uniqueZips.Contains(zipCode))
                                {
                                    uniqueZips.Add(zipCode);
                                    zipEntries.Add(new ZipEntry
                                    {
                                        Zip = zipCode.PadLeft(5, '0'),
                                        State = dataRow.GetValue(1),
                                        StateFull = dataRow.GetValue(2),
                                        City = dataRow.GetValue(3),
                                        CBSA = dataRow.GetValue(4)
                                    });
                                    zipCounter++;
                                }
                            }
                        }
                        Console.WriteLine("Zip Code Count: {0}", zipEntries.Count);
                        Console.WriteLine("Unique Zips == Zip Entries: {0}", zipEntries.Count == uniqueZips.Count);
                        Console.WriteLine();
                    }
                }
            }
        }

        private static void LoadWageIndex2011()
        {
            using (FileStream wageIndexStream = new FileStream(wageindexinput, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(wageIndexStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[1];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            var cbsaCounter = 1;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                var state = string.Empty;
                                var cbsaCode = dataRow.GetValue(3);
                                if (!stateNames.TryGetValue(dataRow.GetValue(2), out state))
                                {
                                    state = dataRow.GetValue(2);
                                }
                                if (state.IsNotNullOrEmpty() && cbsaCode.IsNotNullOrEmpty())
                                {
                                    var key = string.Format("{0}_{1}", state, cbsaCode);
                                    if (!uniqueCbsas.Contains(key))
                                    {
                                        uniqueCbsas.Add(key);

                                        var cbsaEntry = new CbsaEntry
                                        {
                                            State = state,
                                            CBSA = dataRow.GetValue(3),
                                            WageIndex = dataRow.GetValue(4).IsNotNullOrEmpty() && dataRow.GetValue(4).IsDouble() ? dataRow.GetValue(4).ToDouble() : 0
                                        };
                                        var title = dataRow.GetValue(1).ToLower().Replace("county", "").Trim();
                                        if (title.StartsWith("St ") || title.StartsWith("st ") || title.StartsWith("St. ") || title.StartsWith("st. "))
                                        {
                                            title = title.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                                        }
                                        cbsaEntry.Counties.Add(new County
                                        {
                                            Code = dataRow.GetValue(0),
                                            Title = title
                                        });
                                        cbsaEntries.Add(cbsaEntry);
                                        cbsaCounter++;
                                    }
                                    else
                                    {
                                        var cbsaEntry = cbsaEntries.Find(c => c.CBSA.IsEqual(cbsaCode) && c.State.StartsWith(state));
                                        if (cbsaEntry != null)
                                        {
                                            var title = dataRow.GetValue(1).ToLower().Replace("county", "").Trim();
                                            if (title.StartsWith("St ") || title.StartsWith("st ") || title.StartsWith("St. ") || title.StartsWith("st. "))
                                            {
                                                title = title.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                                            }
                                            cbsaEntry.Counties.Add(new County
                                            {
                                                Code = dataRow.GetValue(0),
                                                Title = title
                                            });
                                        }
                                    }
                                }
                            }
                            Console.WriteLine("Wage Index 2012 Count: {0}", cbsaEntries.Count);
                            Console.WriteLine();
                        }
                    }
                }
            }
        }

        private static void LoadWageIndex2012()
        {
            using (FileStream wageIndexStream = new FileStream(wageindexinput, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(wageIndexStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            var cbsaCounter = 1;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                var state = string.Empty;
                                var cbsaCode = dataRow.GetValue(3);
                                if (!stateNames.TryGetValue(dataRow.GetValue(2), out state))
                                {
                                    state = dataRow.GetValue(2);
                                }
                                if (state.IsNotNullOrEmpty() && cbsaCode.IsNotNullOrEmpty())
                                {
                                    var key = string.Format("{0}_{1}", state, cbsaCode);
                                    if (!uniqueCbsas.Contains(key))
                                    {
                                        uniqueCbsas.Add(key);

                                        var cbsaEntry = new CbsaEntry
                                        {
                                            State = state,
                                            CBSA = dataRow.GetValue(3),
                                            WageIndex = dataRow.GetValue(4).IsNotNullOrEmpty() && dataRow.GetValue(4).IsDouble() ? dataRow.GetValue(4).ToDouble() : 0
                                        };
                                        var title = dataRow.GetValue(1).ToLower().Replace("county", "").Trim();
                                        if (title.StartsWith("St ") || title.StartsWith("st ") || title.StartsWith("St. ") || title.StartsWith("st. "))
                                        {
                                            title = title.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                                        }
                                        cbsaEntry.Counties.Add(new County
                                        {
                                            Code = dataRow.GetValue(0),
                                            Title = title
                                        });
                                        cbsaEntries.Add(cbsaEntry);
                                        cbsaCounter++;
                                    }
                                    else
                                    {
                                        var cbsaEntry = cbsaEntries.Find(c => c.CBSA.IsEqual(cbsaCode) && c.State.StartsWith(state));
                                        if (cbsaEntry != null)
                                        {
                                            var title = dataRow.GetValue(1).ToLower().Replace("county", "").Trim();
                                            if (title.StartsWith("St ") || title.StartsWith("st ") || title.StartsWith("St. ") || title.StartsWith("st. "))
                                            {
                                                title = title.Replace("St ", "Saint ").Replace("st ", "Saint ").Replace("St. ", "Saint ").Replace("st. ", "Saint ");
                                            }
                                            cbsaEntry.Counties.Add(new County
                                            {
                                                Code = dataRow.GetValue(0),
                                                Title = title
                                            });
                                        }
                                    }
                                }
                            }
                            Console.WriteLine("Wage Index 2012 Count: {0}", cbsaEntries.Count);
                            Console.WriteLine();
                        }
                    }
                }
            }
        }

        private static void LoadStateWageIndex2011()
        {
            using (FileStream wageIndexStream = new FileStream(statewageindex2011input, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(wageIndexStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[1];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            var zipCbsaCounter = 1;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty() && dataRow.GetValue(0).IsNotNullOrEmpty() && dataRow.GetValue(1).IsNotNullOrEmpty())
                                {
                                    var stageIndexEntry = new StateWageIndex
                                    {
                                        CBSA = dataRow.GetValue(0).PadLeft(5, '9'),
                                        State = dataRow.GetValue(1),
                                        WageIndex = dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(2).IsDouble() ? dataRow.GetValue(2).ToDouble() : 0
                                    };
                                    stateWageIndexes.Add(stageIndexEntry);
                                    zipCbsaCounter++;
                                }
                            }
                            Console.WriteLine("State Wage Index Count: {0}", stateWageIndexes.Count);
                            Console.WriteLine();
                        }
                    }
                }
            }
        }

        private static void LoadStateWageIndex2012()
        {
            using (FileStream wageIndexStream = new FileStream(statewageindex2012input, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(wageIndexStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[1];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            var zipCbsaCounter = 1;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty() && dataRow.GetValue(0).IsNotNullOrEmpty() && dataRow.GetValue(1).IsNotNullOrEmpty())
                                {
                                    var stageIndexEntry = new StateWageIndex
                                    {
                                        CBSA = dataRow.GetValue(0).PadLeft(5, '9'),
                                        State = dataRow.GetValue(1),
                                        WageIndex = dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(2).IsDouble() ? dataRow.GetValue(2).ToDouble() : 0
                                    };
                                    stateWageIndexes.Add(stageIndexEntry);
                                    zipCbsaCounter++;
                                }
                            }
                            Console.WriteLine("State Wage Index 2012 Count: {0}", stateWageIndexes.Count);
                            Console.WriteLine();
                        }
                    }
                }
            }
        }
    }
}
