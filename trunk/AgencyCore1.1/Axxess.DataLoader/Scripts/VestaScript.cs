﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class VestaScript
    {
        private static string input = Path.Combine(App.Root, "Files\\Bestmed.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Bestmed_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PrimaryInsurance = "1";
                                            patientData.Gender = "";
                                            var nameArray = dataRow.GetValue(1);
                                            if (!nameArray.Contains("-"))
                                            {
                                                patientData.LastName = nameArray.Split(',')[0].Trim();
                                                var firstNameandMI = nameArray.Split(',')[1].Trim();
                                                if (firstNameandMI.Split(' ').Length > 1)
                                                {
                                                    patientData.FirstName = firstNameandMI.Split(' ')[0].Trim();
                                                    patientData.MiddleInitial = firstNameandMI.Split(' ')[1].Trim().Replace(".", "");
                                                }
                                                else
                                                {
                                                    patientData.FirstName = nameArray.Split(',')[1].Trim();
                                                }
                                            }
                                            else
                                            {
                                                patientData.FirstName = nameArray.Split('-')[0].Trim();
                                                patientData.LastName = nameArray.Split('-')[1].Trim().Replace(",", "");
                                            }
                                            
                                            patientData.PatientIdNumber = dataRow.GetValue(2);
                                            patientData.StartofCareDate = dataRow.GetValue(3).ToDateTime();
                                            
                                            patientData.AddressLine1 = dataRow.GetValue(4);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(5);
                                            patientData.AddressStateCode = dataRow.GetValue(6);
                                            patientData.AddressZipCode = dataRow.GetValue(7);
                                            patientData.PhoneHome = dataRow.GetValue(9).ToPhoneDB();
                                            patientData.Comments += string.Format("CBSA: {0}. ", dataRow.GetValue(8));
                                            //if (dataRow.GetValue(10).IsNotNullOrEmpty() && dataRow.GetValue(10).IsDate())
                                            //{
                                            //    patientData.DOB = dataRow.GetValue(10).ToDateTime();
                                            //}
                                            //patientData.Comments += string.Format("RN Name: {0}. ", dataRow.GetValue(11));
                                            //patientData.Comments += string.Format("RN Phone: {0}. ", dataRow.GetValue(12));

                                            //patientData.Comments += string.Format("Physician Num: {0}. ", dataRow.GetValue(13));
                                            //patientData.Comments += string.Format("Physician Name: {0}. ", dataRow.GetValue(14));
                                            //if (dataRow.GetValue(16).IsNotNullOrEmpty() && dataRow.GetValue(16).IsDate())
                                            //{
                                            //    patientData.StartofCareDate = dataRow.GetValue(16).ToDateTime();
                                            //    //patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                            //}
                                            //patientData.MedicareNumber = dataRow.GetValue(17);
                                            //patientData.PatientIdNumber = dataRow.GetValue(18);
                                            //patientData.Comments += string.Format("CLI Class: {0}. ", dataRow.GetValue(19));

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
