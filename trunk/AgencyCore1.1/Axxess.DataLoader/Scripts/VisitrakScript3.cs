﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using HtmlAgilityPack;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class VisiTrakScript3
    {
        private static string input = Path.Combine(App.Root, "Files\\PatientRoster.HTM");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PatientRoster_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.Load(input);

                    int i = 1;
                    int counter = 1;
                    Patient patientData = null;
                    htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                    {
                        if (htmlNode.Name.IsEqual("html"))
                        {
                            htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                            {
                                if (bodyNode.Name.IsEqual("body"))
                                {
                                    bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                    {
                                        if (tableNode.DoesHtmlRowHaveData())
                                        {

                                            int columnNumber = 1;
                                                
                                            patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Gender = "";
                                            patientData.PrimaryInsurance = "1";
                                            patientData.PrimaryRelationship = "";
                                            patientData.SecondaryRelationship = "";
                                            patientData.TertiaryRelationship = "";
                                                    tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                                    {
                                                        if (tRowNode.Name.IsEqual("tr"))
                                                        {
                                                            tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                            {
                                                                if (dataNode.Name.IsEqual("td"))
                                                                {
                                                                    switch (columnNumber) { 
                                                                        case 2:
                                                                            patientData.LastName = dataNode.InnerText.Split(',')[0].Trim();

                                                                            var firstName = dataNode.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                                            if (firstName.Length > 1)
                                                                            {
                                                                                patientData.FirstName = firstName[0].Trim();
                                                                                patientData.MiddleInitial = firstName[1].Trim().Replace(".", "");
                                                                            }
                                                                            else
                                                                            {
                                                                                patientData.FirstName = dataNode.InnerText.Split(',')[1].Trim();
                                                                            }
                                                                            break;
                                                                        case 3:
                                                                            patientData.PatientIdNumber = dataNode.InnerText;
                                                                            break;
                                                                        case 4:
                                                                            if (dataNode.InnerText.IsNotNullOrEmpty())
                                                                            {
                                                                                patientData.StartofCareDate = dataNode.InnerText.ToMySqlDate();
                                                                            }
                                                                            break;
                                                                        case 5:
                                                                            patientData.AddressLine1 = dataNode.InnerText;
                                                                            break;
                                                                        case 6:
                                                                            patientData.AddressCity = dataNode.InnerText;
                                                                            break;
                                                                        case 7:
                                                                            patientData.AddressStateCode = dataNode.InnerText;
                                                                            break;
                                                                        case 8:
                                                                            patientData.AddressZipCode = dataNode.InnerText;
                                                                            break;
                                                                        case 9:
                                                                            patientData.Comments += string.Format("CBSA:{0}.", dataNode.InnerText);
                                                                            break;
                                                                        case 10:
                                                                            patientData.PhoneHome = dataNode.InnerText.ToPhoneDB();
                                                                            break;
                                                                    }
                                                                    columnNumber++;
                                                                }
                                                            });
                                                        }
                                                    });


                                                    patientData.Created = DateTime.Now;
                                                    patientData.Modified = DateTime.Now;

                                                    var medicationProfile = new MedicationProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Medication = "<ArrayOfMedication />"
                                                    };

                                                    var allergyProfile = new AllergyProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Allergies = "<ArrayOfAllergy />"
                                                    };

                                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                    {
                                                        var admissionPeriod = new PatientAdmissionDate
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            Created = DateTime.Now,
                                                            DischargedDate = DateTime.MinValue,
                                                            IsActive = true,
                                                            IsDeprecated = false,
                                                            Modified = DateTime.Now,
                                                            PatientData = patientData.ToXml().Replace("'", ""),
                                                            PatientId = patientData.Id,
                                                            Reason = string.Empty,
                                                            StartOfCareDate = patientData.StartofCareDate,
                                                            Status = patientData.Status
                                                        };
                                                        if (Database.Add(admissionPeriod))
                                                        {
                                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                                            if (patient != null)
                                                            {
                                                                patient.AdmissionId = admissionPeriod.Id;
                                                                if (Database.Update(patient))
                                                                {
                                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    i++;
                                                
                                                
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
