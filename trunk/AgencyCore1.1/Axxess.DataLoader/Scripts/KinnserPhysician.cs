﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class KinnserPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\MD_LISIT.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\MD_LISIT{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;
                                            physicianData.IsDeprecated = false;


                                            physicianData.FirstName = dataRow.GetValue(0).Trim();
                                            physicianData.LastName = dataRow.GetValue(1).Trim();
                                            physicianData.AddressLine1 = dataRow.GetValue(2);
                                            physicianData.AddressCity = dataRow.GetValue(3);
                                            physicianData.AddressStateCode = dataRow.GetValue(4);
                                            physicianData.AddressZipCode = dataRow.GetValue(5).Trim();
                                            physicianData.PhoneWork = dataRow.GetValue(6).ToPhoneDB();
                                            physicianData.FaxNumber = dataRow.GetValue(7).ToPhoneDB();
                                            if(dataRow.GetValue(8).IsNotNullOrEmpty())
                                                physicianData.UPIN = dataRow.GetValue(8);
                                            physicianData.NPI = dataRow.GetValue(9);
                                            physicianData.LicenseNumber = dataRow.GetValue(10);
                                            physicianData.PhysicianAccess = (dataRow.GetValue(16).ToLower() == "no") ? false : true;


                                            
                                                
                                          
                                                
                                            
                                            
                                            var stringBuilder = new StringBuilder();
                                            //if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            //{
                                            //    stringBuilder.AppendFormat("License: {0}. ", dataRow.GetValue(11));
                                            //}
                                            //if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            //{
                                            //    stringBuilder.AppendFormat("Specialty: {0}. ", dataRow.GetValue(12));
                                            //}
                                            if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            {
                                                stringBuilder.AppendFormat("License Expiration: {0}.", dataRow.GetValue(11));
                                            }
                                            physicianData.Comments = stringBuilder.ToString().Trim();

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
