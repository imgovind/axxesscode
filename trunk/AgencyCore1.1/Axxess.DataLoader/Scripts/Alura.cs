﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class Alura
    {
        private static string input = Path.Combine(App.Root, "Files\\PAtientsData.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PAtientsData_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PrimaryRelationship = "";
                                            patientData.SecondaryRelationship = "";
                                            patientData.TertiaryRelationship = "";


                                            if (dataRow.GetValue(0).Split(',').Length > 1)
                                            {
                                                patientData.LastName = dataRow.GetValue(0).Split(',')[0];
                                                var nameArray = dataRow.GetValue(0).Split(',')[1].Trim();
                                                if (nameArray.Split(' ').Length > 1)
                                                {
                                                    patientData.FirstName = nameArray.Split(' ')[0];
                                                    patientData.MiddleInitial = nameArray.Split(' ')[1];
                                                }
                                                else
                                                {
                                                    patientData.FirstName = dataRow.GetValue(0).Split(',')[1].Trim();
                                                }
                                            }
                                            else
                                            {
                                                patientData.FirstName = dataRow.GetValue(0);
                                            }
                                            patientData.Comments += string.Format("Admission Date:{0}.", DateTime.FromOADate(double.Parse(dataRow.GetValue(1))).ToShortDateString());
                                            if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                            {
                                                patientData.Status = 2;
                                            }
                                            patientData.PatientIdNumber = dataRow.GetValue(3);
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(4)));
                                            }
                                            patientData.Gender = dataRow.GetValue(5).IsEqual("F") ? "Female" : "Male";
                                            patientData.CaseManagerName = dataRow.GetValue(6);
                                            patientData.Comments += string.Format("Case Manager:{0}.", dataRow.GetValue(6));
                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Cert period:{0}.", dataRow.GetValue(7));
                                            }
                                            patientData.Comments += string.Format("Physician:{0}.", dataRow.GetValue(8));
                                            if (dataRow.GetValue(9).Trim().IsNotNullOrEmpty())
                                                patientData.PhoneHome = dataRow.GetValue(9).ToPhoneDB();
                                            patientData.Comments += string.Format("Primary DX:{0}.", dataRow.GetValue(10));
                                            if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Emer Code:{0}.", dataRow.GetValue(11));
                                            }
                                            patientData.AddressLine1 = dataRow.GetValue(12).Split(',')[0];
                                            var length = dataRow.GetValue(12).Split(',').Length;
                                            if (length > 2)
                                            {
                                                patientData.AddressCity = dataRow.GetValue(12).Split(',')[length - 2];
                                                var addressArray = dataRow.GetValue(12).Split(',')[length - 1].Trim();
                                                patientData.AddressStateCode = addressArray.Split(' ')[0];
                                                patientData.AddressZipCode = addressArray.Split(' ')[1];
                                            }
                                            if (dataRow.GetValue(13).Trim().StartsWith("MEDICARE"))
                                            {
                                                patientData.PrimaryInsurance = "1";
                                                string ss = dataRow.GetValue(13).Trim().Split('#')[1].Trim();
                                                patientData.MedicareNumber = ss.Substring(0, ss.Length - 1);
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("{0}.", dataRow.GetValue(13));
                                            }

                                            if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                patientData.Comments += string.Format("Referral scource:{0}.", dataRow.GetValue(14));
                                            patientData.MaritalStatus = "Unknown";
                                            


                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            //var exists = true;
                                                            //var npi = dataRow.GetValue(24);
                                                            //var physician = Database.GetPhysician(npi, agencyId.ToGuid());
                                                            //if (physician == null)
                                                            //{
                                                            //    exists = false;
                                                            //    var info = Database.GetNpiData(npi);
                                                            //    physician = new AgencyPhysician
                                                            //    {
                                                            //        Id = Guid.NewGuid(),
                                                            //        AgencyId = agencyId.ToGuid(),
                                                            //        NPI = npi,
                                                            //        LoginId = Guid.Empty,
                                                            //        AddressLine1 = dataRow.GetValue(18),
                                                            //        AddressCity = dataRow.GetValue(19),
                                                            //        AddressStateCode = dataRow.GetValue(20),
                                                            //        AddressZipCode = dataRow.GetValue(21),
                                                            //        PhoneWork = dataRow.GetValue(22).ToPhoneDB(),
                                                            //        FaxNumber = dataRow.GetValue(23).ToPhoneDB(),
                                                            //        FirstName = info != null ? info.ProviderFirstName : string.Empty,
                                                            //        LastName = info != null ? info.ProviderLastName : string.Empty,
                                                            //        Credentials = dataRow.GetValue(17)
                                                            //    };
                                                            //    var physicianNameArray = dataRow.GetValue(16).Replace("Dr.", "").Replace("Dr", "").Replace("DR.", "").Replace("DR", "").Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                            //    //var physicianNameArray = dataRow.GetValue(15).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                            //    if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                            //    {
                                                            //        physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : physician.LastName;
                                                            //        physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : physician.FirstName;
                                                            //    }
                                                            //    //else if (physicianNameArray != null && physicianNameArray.Length == 2)
                                                            //    //{
                                                            //    //    physician.FirstName = info.ProviderFirstName;
                                                            //    //    physician.LastName = physicianNameArray[1].Trim();
                                                            //    //}

                                                            //    Database.Add(physician);
                                                            //}

                                                            //if (physician != null)
                                                            //{
                                                            //    var patientPhysician = new PatientPhysician
                                                            //    {
                                                            //        IsPrimary = true,
                                                            //        PatientId = patientData.Id,
                                                            //        PhysicianId = physician.Id
                                                            //    };

                                                            //    if (Database.Add(patientPhysician))
                                                            //    {
                                                            //        Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                            //    }
                                                            //}
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
