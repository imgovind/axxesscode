﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class SynergyPhysicianScript
    {
        private static string input = Path.Combine(App.Root, "Files\\americanangelsphysicians.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\americanangelsphysicians_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                var i = 1;
                                csvReader.ReadHeaderRecord();
                                foreach (var dataRow in csvReader.DataRecords)
                                {
                                    if (dataRow.GetValue(7).IsNotNullOrEmpty() && dataRow.GetValue(7).IsEqual("Physician"))
                                    {
                                        var physicianData = new AgencyPhysician();
                                        physicianData.Id = Guid.NewGuid();
                                        physicianData.AgencyId = agencyId;
                                        physicianData.IsDeprecated = false;

                                        if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                        {
                                            var nameArray = dataRow.GetValue(1).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                physicianData.LastName = nameArray[0].Trim();
                                                physicianData.FirstName = nameArray[1].Trim();
                                                if (nameArray.Length > 2)
                                                {
                                                    physicianData.MiddleName = nameArray[2].Trim();
                                                }
                                            }
                                        }
                                        physicianData.AddressLine1 = dataRow.GetValue(2);
                                        physicianData.PhoneWork = dataRow.GetValue(3);
                                        physicianData.FaxNumber = dataRow.GetValue(4);
                                        physicianData.Credentials = dataRow.GetValue(6);
                                        physicianData.AddressLine2 = dataRow.GetValue(10);
                                        physicianData.AddressCity = dataRow.GetValue(11);
                                        physicianData.AddressStateCode = dataRow.GetValue(12);
                                        physicianData.AddressZipCode = dataRow.GetValue(13);
                                        physicianData.NPI = dataRow.GetValue(18);

                                        var stringBuilder = new StringBuilder();
                                        if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                        {
                                            stringBuilder.AppendFormat("Pecos Enrollment Status: {0}. ", dataRow.GetValue(8));
                                        }
                                        if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                        {
                                            stringBuilder.AppendFormat("Primary Language: {0}. ", dataRow.GetValue(16));
                                        }
                                        if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                        {
                                            stringBuilder.AppendFormat("Provider ID: {0}. ", dataRow.GetValue(17));
                                        }
                                        physicianData.Comments = stringBuilder.ToString().Trim();

                                        physicianData.Created = DateTime.Now;
                                        physicianData.Modified = DateTime.Now;

                                        var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                        if (physician == null)
                                        {
                                            if (Database.Add(physicianData))
                                            {
                                                Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
