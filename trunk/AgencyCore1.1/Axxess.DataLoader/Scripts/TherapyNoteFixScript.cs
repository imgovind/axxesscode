﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Extensions;

    public static class TherapyNoteFixScript
    {
        public static void Run()
        {
            var agency = Database.GetAgency(new Guid("76961263-e928-4df4-8752-613ff49bd002"));
            if (!agency.IsDeprecated)
            {
                var notes = Database.GetPatientVisitNotes(agency.Id, DateTime.Now.AddDays(-60), DateTime.Now);
                Console.WriteLine("{0} Count: {1}", agency.Name, notes.Count);
                notes.ForEach(note =>
                {
                    if (note.IsPTNote() && !note.IsDeprecated && note.Status >= (int)ScheduleStatus.NoteNotYetDue)
                    {
                        note.Questions = note.Note.ToObject<List<NotesQuestion>>();
                        Console.WriteLine("{0} last modified: {1}, status: {2}", note.NoteType, note.Modified.ToShortDateString(), note.Status.ToName<ScheduleStatus>(ScheduleStatus.NoStatus));
                        IDictionary<string, NotesQuestion> data = note.ToDictionary();
                        if (data != null)
                        {
                            if (data.AnswerOrEmptyString("GenericPriorBP").IsNotNullOrEmpty())
                            {
                                Console.WriteLine("GenericPriorBP: {0}", data.AnswerOrEmptyString("GenericPriorBP"));
                            }

                            if (data.AnswerOrEmptyString("GenericPostBP").IsNotNullOrEmpty())
                            {
                                Console.WriteLine("GenericPostBP: {0}", data.AnswerOrEmptyString("GenericPostBP"));
                            }

                            if (data.AnswerOrEmptyString("GenericPriorPulse").IsNotNullOrEmpty())
                            {
                                Console.WriteLine("GenericPriorPulse: {0}", data.AnswerOrEmptyString("GenericPriorPulse"));
                            }

                            if (data.AnswerOrEmptyString("GenericPostPulse").IsNotNullOrEmpty())
                            {
                                Console.WriteLine("GenericPostPulse: {0}", data.AnswerOrEmptyString("GenericPostPulse"));
                            }

                            if (data.AnswerOrEmptyString("GenericBloodPressure").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericBloodPressurePer").IsNotNullOrEmpty())
                            {
                                var priorBP = string.Empty;
                                var postBP = string.Empty;
                                
                                Console.WriteLine("GenericBloodPressure: {0}", data.AnswerOrEmptyString("GenericBloodPressure"));
                                Console.WriteLine("GenericBloodPressurePer: {0}", data.AnswerOrEmptyString("GenericBloodPressurePer"));
                                if (data.AnswerOrEmptyString("GenericBloodPressure").Contains("/") && data.AnswerOrEmptyString("GenericBloodPressurePer").Contains("/"))
                                {
                                    priorBP = data.AnswerOrEmptyString("GenericBloodPressure");
                                    postBP = data.AnswerOrEmptyString("GenericBloodPressurePer");
                                    Console.WriteLine("GenericPriorBP: {0}", data.AnswerOrEmptyString("GenericBloodPressure"));
                                    Console.WriteLine("GenericPostBP: {0}", data.AnswerOrEmptyString("GenericBloodPressurePer"));
                                }
                                else
                                {
                                    var diastolic = data.AnswerOrEmptyString("GenericBloodPressure").Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                    var systolic = data.AnswerOrEmptyString("GenericBloodPressurePer").Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                                    if (diastolic != null && systolic != null)
                                    {
                                        if (diastolic.Length == 1 && systolic.Length == 1)
                                        {
                                            priorBP = diastolic[0] + "/" + systolic[0];
                                            Console.WriteLine("GenericPriorBP: {0}{1}", diastolic[0] + "/", systolic[0]);
                                        }
                                        else if (diastolic.Length == 2 && systolic.Length == 2)
                                        {
                                            priorBP = diastolic[0] + "/" + systolic[0];
                                            postBP = diastolic[1] + "/" + systolic[1];
                                            Console.WriteLine("GenericPriorBP: {0}{1}", diastolic[0] + "/", systolic[0]);
                                            Console.WriteLine("GenericPostBP: {0}{1}", diastolic[1] + "/", systolic[1]);
                                        }
                                    }
                                }

                                if (!data.ContainsKey("GenericPriorBP"))
                                {
                                    note.Questions.Add(new NotesQuestion { Name = "GenericPriorBP", Answer = priorBP, Type = note.NoteType.IsNotNullOrEmpty() ? note.NoteType.Trim() : string.Empty });
                                }
                                if (!data.ContainsKey("GenericPostBP"))
                                {
                                    note.Questions.Add(new NotesQuestion { Name = "GenericPostBP", Answer = postBP, Type = note.NoteType.IsNotNullOrEmpty() ? note.NoteType.Trim() : string.Empty });
                                }
                            }

                            if (data.AnswerOrEmptyString("GenericPulse").IsNotNullOrEmpty())
                            {
                                var priorPulse = string.Empty;
                                var postPulse = string.Empty;

                                Console.WriteLine("GenericPulse: {0}", data.AnswerOrEmptyString("GenericPulse"));
                                var pulse = data.AnswerOrEmptyString("GenericPulse").Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                                if (pulse != null)
                                {
                                    if (pulse.Length == 1)
                                    {
                                        priorPulse = pulse[0];
                                        Console.WriteLine("GenericPriorPulse: {0}", pulse[0]);
                                    }
                                    else if (pulse.Length == 2)
                                    {
                                        priorPulse = pulse[0];
                                        postPulse = pulse[1];
                                        Console.WriteLine("GenericPriorPulse: {0}", pulse[0]);
                                        Console.WriteLine("GenericPostPulse: {0}", pulse[1]);
                                    }

                                    if (!data.ContainsKey("GenericPriorPulse"))
                                    {
                                        note.Questions.Add(new NotesQuestion { Name = "GenericPriorPulse", Answer = priorPulse, Type = note.NoteType.IsNotNullOrEmpty() ? note.NoteType.Trim() : string.Empty });
                                    }
                                    if (!data.ContainsKey("GenericPostPulse"))
                                    {
                                        note.Questions.Add(new NotesQuestion { Name = "GenericPostPulse", Answer = postPulse, Type = note.NoteType.IsNotNullOrEmpty() ? note.NoteType.Trim() : string.Empty });
                                    }
                                }
                            }

                            note.Modified = DateTime.Now;
                            note.Note = note.Questions.ToXml();
                            if (Database.Update<PatientVisitNote>(note))
                            {
                                Console.WriteLine("Converted Patient Visit Note");
                            }
                        }

                        
                    }
                });
            }
        }

    }
}