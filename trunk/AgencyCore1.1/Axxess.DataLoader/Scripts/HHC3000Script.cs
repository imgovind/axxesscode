﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class HHC3000Script
    {
        private static string input = Path.Combine(App.Root, "Files\\vcp.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\vcp_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Gender = "";

                                            patientData.LastName = dataRow.GetValue(0);
                                            patientData.FirstName = dataRow.GetValue(1);
                                            patientData.MiddleInitial = dataRow.GetValue(2);
                                            patientData.PatientIdNumber = dataRow.GetValue(3);
                                            patientData.AddressLine1 = dataRow.GetValue(4);
                                            patientData.AddressLine2 = string.Empty;
                                            patientData.AddressCity = dataRow.GetValue(5);
                                            patientData.AddressStateCode = dataRow.GetValue(6);
                                            patientData.AddressZipCode = dataRow.GetValue(7);

                                            if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            {
                                                if (dataRow.GetValue(8).ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "1";
                                                    patientData.MedicareNumber = dataRow.GetValue(9);
                                                }
                                                patientData.Comments += string.Format("Primary Insurance: {0}. ", dataRow.GetValue(8));
                                                patientData.Comments += string.Format("HIN: {0}. ", dataRow.GetValue(9));
                                            }

                                            if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Services Required: {0}. ", dataRow.GetValue(11));
                                            }

                                            if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            {
                                                patientData.PhoneHome = dataRow.GetValue(12).ToPhoneDB();
                                            }

                                            if (dataRow.GetValue(13).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(13)));
                                            }

                                            if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                            {
                                                patientData.Gender = dataRow.GetValue(15);
                                            }

                                            if (dataRow.GetValue(16).ToLower().Contains("m"))
                                            {
                                                patientData.MaritalStatus = "Married";
                                            }
                                            else if (dataRow.GetValue(16).ToLower().Contains("s"))
                                            {
                                                patientData.MaritalStatus = "Single";
                                            }
                                            else if (dataRow.GetValue(16).ToLower().Contains("w"))
                                            {
                                                patientData.MaritalStatus = "Widowed";
                                            }
                                            else if (dataRow.GetValue(16).ToLower().Contains("d"))
                                            {
                                                patientData.MaritalStatus = "Divorced";
                                            }
                                            else if (dataRow.GetValue(16).ToLower().Contains("u"))
                                            {
                                                patientData.MaritalStatus = "Unknown";
                                            }
                                            else
                                            {
                                                patientData.MaritalStatus = "";
                                            }

                                            patientData.SSN = dataRow.GetValue(17).IsNotNullOrEmpty() ? dataRow.GetValue(17).ToPhoneDB() : "";

                                            if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(18)));
                                            }

                                            if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician: {0}. ", dataRow.GetValue(19));
                                            }

                                            if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician Phone: {0}. ", dataRow.GetValue(19));
                                            }

                                            if (dataRow.GetValue(21).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(21));
                                            }

                                            if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Second Diagnosis: {0}. ", dataRow.GetValue(25));
                                            }

                                            if (dataRow.GetValue(24).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Third Diagnosis: {0}. ", dataRow.GetValue(24));
                                            }

                                            if (dataRow.GetValue(23).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Fourth Diagnosis: {0}. ", dataRow.GetValue(23));
                                            }

                                            if (dataRow.GetValue(22).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Fifth Diagnosis: {0}. ", dataRow.GetValue(22));
                                            }

                                            if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Responsible Employee: {0}. ", dataRow.GetValue(26));
                                            }

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
