﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using System.Text;

    public static class AgencySuppliesLoad
    {
        private static string input = Path.Combine(App.Root, "Files\\communityHHLafayetteSupp.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\communityHHLafayetteSupp_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var supply = new AgencySupply();
                                            supply.Id = Guid.NewGuid();
                                            supply.AgencyId = agencyId;
                                            supply.Created = DateTime.Now;
                                            supply.Modified = DateTime.Now;
                                            supply.IsDeprecated = false;
                                            if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                            {
                                                supply.Description = dataRow.GetValue(0);
                                            }
                                            supply.UnitCost = Convert.ToDouble(dataRow.GetValue(1));
                                            if (Database.Add(supply))
                                            {
                                                Console.WriteLine("{0}){1}", i, supply.Description);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
            }
        }
    }
}
