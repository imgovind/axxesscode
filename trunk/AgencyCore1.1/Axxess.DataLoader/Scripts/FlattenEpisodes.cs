﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.DataLoader
{
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class FlattenEpisodes
    {
        public static void Run()
        {
            var script = @"SELECT 
                        Id,
                        PatientId,
                        AgencyId,
                        Details
                            FROM 
                                patientepisodes";
                                        

            var list = new List<PrivateDutyCarePeriod>();
            using (var cmd = new FluentCommand<PrivateDutyCarePeriod>(script))
            {
                list = cmd.SetConnection("PrivateDutyConnectionString")
                .SetMap(reader => new PrivateDutyCarePeriod
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Details = reader.GetString("Details")
                })
                .AsList();
            }
            StringBuilder sb = new StringBuilder();
            foreach (var episode in list)
            {
                if (episode.Details.IsNotNullOrEmpty())
                {
                    var detail = episode.Details.ToObject<EpisodeDetail>();
                    if (detail != null)
                    {
                        episode.CaseManagerId = detail.CaseManager.ToGuid();
                        episode.PhysicianId = detail.PrimaryPhysician.ToGuid();
                        episode.PrimaryInsurance = detail.PrimaryInsurance.ToInteger();
                        episode.SecondaryInsurance = detail.SecondaryInsurance.ToInteger();
                        episode.Comments = detail.Comments;
                    }
                }
                sb.Append("UPDATE patientepisodes SET CaseManagerId = '").Append(episode.CaseManagerId).
                       Append("', Comments = '").Append(episode.Comments).Append("', PhysicianId = '").Append(episode.PhysicianId)
                       .Append("', PrimaryInsurance = ").Append(episode.PrimaryInsurance).Append(", SecondaryInsurance = ")
                       .Append(episode.SecondaryInsurance).Append(" WHERE Id = '").Append(episode.Id).Append("';");
            }
            using (var cmd = new FluentCommand<PrivateDutyCarePeriod>(sb.ToString()))
            {
                cmd.SetConnection("PrivateDutyConnectionString").AsNonQuery();
            }
        }
    }
}
