﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class Wizard
    {
        private static string input = Path.Combine(App.Root, "Files\\ActivePatients.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\ActivePatients_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = false;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    int count = 0;
                                    Patient patientData = null;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            count++;
                                            if (count % 3 == 1)
                                            {
                                                //first row
                                                patientData = new Patient();
                                                patientData.Id = Guid.NewGuid();
                                                patientData.AgencyId = agencyId;
                                                patientData.AgencyLocationId = locationId;
                                                patientData.Ethnicities = string.Empty;
                                                patientData.MaritalStatus = string.Empty;
                                                patientData.IsDeprecated = false;
                                                patientData.IsHospitalized = false;
                                                patientData.Status = 1;
                                                var Name = dataRow.GetValue(0);
                                                if (Name.IsNotNullOrEmpty())
                                                {
                                                    patientData.LastName = Name.Split(',')[0].Trim();
                                                    var FirstName = Name.Split(',')[1].Trim().Split(' ');
                                                    if (FirstName.Length > 1)
                                                    {
                                                        patientData.FirstName = FirstName[0].Trim();
                                                        patientData.MiddleInitial = FirstName[1].Trim().Replace(".", "");
                                                    }
                                                    else
                                                        patientData.FirstName = Name.Split(',')[1].Trim();
                                                }
                                                patientData.PatientIdNumber = dataRow.GetValue(1);

                                                if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                                {
                                                    var dob = dataRow.GetValue(2).Trim().Split(' ');
                                                    if (dob.Length > 1)
                                                    {
                                                        var dobDate = dob[0].Substring(0, 6) + "19" + dob[0].Substring(6);
                                                        patientData.DOB = Convert.ToDateTime(dobDate);
                                                    }
                                                    else
                                                    {
                                                        var dobDate = dataRow.GetValue(2).Substring(0, 6) + "19" + dataRow.GetValue(2).Substring(6);
                                                        patientData.DOB = Convert.ToDateTime(dobDate);
                                                    }
                                                }

                                                if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                                {
                                                    patientData.PhoneHome = dataRow.GetValue(3).Trim().ToPhoneDB();
                                                }

                                                if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Clinician Phone:{0}.", dataRow.GetValue(4));
                                                }
                                            }
                                            else if (count % 3 == 2)
                                            {
                                                patientData.AddressLine1 = dataRow.GetValue(0).Trim();
                                                if (dataRow.GetValue(1).IsNotNullOrEmpty() && dataRow.GetValue(1).Length >= 1)
                                                {
                                                    patientData.Gender = dataRow.GetValue(1).ToUpper().StartsWith("F") ? "Female" : dataRow.GetValue(1).ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                                }
                                                if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("{0}.", dataRow.GetValue(4).Trim());
                                                }
                                            }
                                            else if (count % 3 == 0)
                                            {
                                                patientData.AddressCity = dataRow.GetValue(0).Trim().Split(',')[0];
                                                patientData.AddressStateCode = dataRow.GetValue(0).Trim().Split(',')[1].Trim().Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries)[0];
                                                patientData.AddressZipCode = dataRow.GetValue(0).Trim().Split(',')[1].Trim().Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries)[1];
                                                if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                                {
                                                    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(2)));
                                                }
                                                if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("{0}.", dataRow.GetValue(3));
                                                }
                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;

                                                var medicationProfile = new MedicationProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Medication = "<ArrayOfMedication />"
                                                };

                                                var allergyProfile = new AllergyProfile
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    PatientId = patientData.Id,
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    Allergies = "<ArrayOfAllergy />"
                                                };

                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    var admissionPeriod = new PatientAdmissionDate
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        Created = DateTime.Now,
                                                        DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                        IsActive = true,
                                                        IsDeprecated = false,
                                                        Modified = DateTime.Now,
                                                        PatientData = patientData.ToXml().Replace("'", ""),
                                                        PatientId = patientData.Id,
                                                        Reason = string.Empty,
                                                        StartOfCareDate = patientData.StartofCareDate,
                                                        Status = patientData.Status
                                                    };
                                                    if (Database.Add(admissionPeriod))
                                                    {
                                                        var patient = Database.GetPatient(patientData.Id, agencyId);
                                                        if (patient != null)
                                                        {
                                                            patient.AdmissionId = admissionPeriod.Id;
                                                            if (Database.Update(patient))
                                                            {
                                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);

                                                            }
                                                        }
                                                    }
                                                }
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}