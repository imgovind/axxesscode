﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;
    using System.Text;

    public static class CradleMedPointScript
    {
        private static string input = Path.Combine(App.Root, "Files\\royalstar.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\royalstar_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.Gender = "";

                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            patientData.MedicareNumber = dataRow.GetValue(1);

                                            var nameArray = dataRow.GetValue(2).Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length == 2)
                                            {
                                                patientData.LastName = nameArray[0].Trim();
                                                patientData.FirstName = nameArray[1].Trim();
                                            }

                                            if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = dataRow.GetValue(3).ToDateTime();
                                            }

                                            if (dataRow.GetValue(4).IsNotNullOrEmpty() && dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Certification Period: {0} - {1}. ", dataRow.GetValue(4), dataRow.GetValue(5));
                                            }

                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                            {
                                                var addressArray = dataRow.GetValue(6).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                if (addressArray != null)
                                                {
                                                    if (addressArray.Length == 2)
                                                    {
                                                        var twoWordCityArray = addressArray[0].Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (twoWordCityArray != null && twoWordCityArray.Length == 2)
                                                        {
                                                            patientData.AddressLine1 = twoWordCityArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = "";
                                                            patientData.AddressCity = twoWordCityArray[1].Replace("'", "");
                                                        }
                                                        else
                                                        {
                                                            var oneWordCityArray = addressArray[0].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (oneWordCityArray != null && oneWordCityArray.Length > 1)
                                                            {
                                                                patientData.AddressCity = oneWordCityArray.Reverse()[0].Replace("'", "");

                                                                oneWordCityArray = oneWordCityArray.Reverse();
                                                                int addressLine1Count = 0;
                                                                var addressLine1Array = new StringBuilder();
                                                                do
                                                                {
                                                                    addressLine1Array.AppendFormat("{0} ", oneWordCityArray[addressLine1Count]);
                                                                    addressLine1Count++;
                                                                } while (addressLine1Count < oneWordCityArray.Length - 1);

                                                                patientData.AddressLine1 = addressLine1Array.ToString().TrimEnd();
                                                                patientData.AddressLine2 = "";
                                                            }
                                                        }
                                                        var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (locationArray != null && locationArray.Length == 2)
                                                        {
                                                            patientData.AddressStateCode = locationArray[0];
                                                            patientData.AddressZipCode = locationArray[1].Substring(0, 5);
                                                        }
                                                    }
                                                }
                                            }

                                            patientData.AddressZipCode = dataRow.GetValue(7);
                                            patientData.PhoneHome = dataRow.GetValue(8).ToPhoneDB();

                                            if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = dataRow.GetValue(9).ToDateTime();
                                            }

                                            if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Skill Frequency: {0}. ", dataRow.GetValue(10));
                                            }

                                            if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(11));
                                            }

                                            if (dataRow.GetValue(13).IsNotNullOrEmpty() && dataRow.GetValue(13).Length >= 5)
                                            {
                                                patientData.ReferralDate = dataRow.GetValue(13).ToDateTime();
                                            }

                                            if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("RFA Description: {0}. ", dataRow.GetValue(19));
                                            }

                                            if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Assessor Discipline: {0}. ", dataRow.GetValue(20));
                                            }

                                            if (dataRow.GetValue(22).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Emergency Class: {0}. ", dataRow.GetValue(22));
                                            }

                                            if (dataRow.GetValue(23).IsNotNullOrEmpty() && dataRow.GetValue(23).IsEqual("inactive"))
                                            {
                                                patientData.Status = (int)PatientStatus.Discharged;
                                            }
                                           
                                            patientData.PrimaryInsurance = "1";
                                            patientData.Comments = patientData.Comments.Replace("'", "");

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            var npi = dataRow.GetValue(16);
                                            var physician = Database.GetPhysician(npi, agencyId);
                                            if (physician == null)
                                            {
                                                physician = new AgencyPhysician
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    NPI = npi,
                                                    LoginId = Guid.Empty,
                                                    PhoneWork = dataRow.GetValue(17).ToPhoneDB()
                                                };

                                                var physicianArray = dataRow.GetValue(15).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                if (physicianArray != null && physicianArray.Length == 2)
                                                {
                                                    physician.FirstName = physicianArray[0];
                                                    physician.LastName = physicianArray[1];
                                                }
                                                else if (physicianArray != null && physicianArray.Length > 2)
                                                {
                                                    physician.FirstName = physicianArray[0];

                                                    physicianArray = physicianArray.Reverse();
                                                    int physicianLastNameCount = 0;
                                                    var physicianLastName = new StringBuilder();
                                                    do
                                                    {
                                                        physicianLastName.AppendFormat("{0} ", physicianArray[physicianLastNameCount]);
                                                        physicianLastNameCount++;
                                                    } while (physicianLastNameCount < physicianArray.Length - 1);

                                                    physician.LastName = physicianLastName.ToString().TrimEnd();
                                                }
                                                if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                                {
                                                    var physicianAddressArray = dataRow.GetValue(18).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (physicianAddressArray != null)
                                                    {
                                                        if (physicianAddressArray.Length == 2)
                                                        {
                                                            var twoWordCityArray = physicianAddressArray[0].Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (twoWordCityArray != null && twoWordCityArray.Length == 2)
                                                            {
                                                                physician.AddressLine1 = twoWordCityArray[0].Replace("'", "");
                                                                physician.AddressLine2 = "";
                                                                physician.AddressCity = twoWordCityArray[1].Replace("'", "");
                                                            }
                                                            else
                                                            {
                                                                var oneWordCityArray = physicianAddressArray[0].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                                if (oneWordCityArray != null && oneWordCityArray.Length > 1)
                                                                {
                                                                    physician.AddressCity = oneWordCityArray.Reverse()[0].Replace("'", "");

                                                                    oneWordCityArray = oneWordCityArray.Reverse();
                                                                    int addressLine1Count = 0;
                                                                    var addressLine1Array = new StringBuilder();
                                                                    do
                                                                    {
                                                                        addressLine1Array.AppendFormat("{0} ", oneWordCityArray[addressLine1Count]);
                                                                        addressLine1Count++;
                                                                    } while (addressLine1Count < oneWordCityArray.Length - 1);

                                                                    physician.AddressLine1 = addressLine1Array.ToString().TrimEnd();
                                                                    physician.AddressLine2 = "";
                                                                }
                                                            }
                                                            var locationArray = physicianAddressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (locationArray != null && locationArray.Length == 2)
                                                            {
                                                                physician.AddressStateCode = locationArray[0];
                                                                physician.AddressZipCode = locationArray[1].Substring(0, 5);
                                                            }
                                                        }
                                                    }
                                                }
                                                Database.Add(physician);
                                            }

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                var patientPhysician = new PatientPhysician
                                                {
                                                    IsPrimary = true,
                                                    PatientId = patientData.Id,
                                                    PhysicianId = physician.Id
                                                };
                                                if (Database.Add(admissionPeriod) && Database.Add(patientPhysician))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
            }
        }
    }
}
