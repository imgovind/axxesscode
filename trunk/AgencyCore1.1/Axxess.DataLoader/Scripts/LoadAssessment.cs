﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Repositories;
    using SubSonic.Repository;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using System.Configuration;
    using Axxess.AgencyManagement.Entities;
    public class LoadAssessment
    {
        public static void Run()
        {
            var script = @"SELECT 
                        Id,
                        PatientId,
                        EpisodeId,
                        AgencyId,
                        Type,
                        OasisData
                            FROM 
                                assessments 
                                        WHERE 
                                            AgencyId = '62b904fa-b38f-4686-b2e4-d748fa129c50' AND
                                            IsDeprecated = 0 ";

            var list = new List<Assessment>();
            using (var cmd = new FluentCommand<Assessment>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .SetMap(reader => new Assessment
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Type = reader.GetString("Type"),
                    OasisData = reader.GetStringNullable("OasisData")
                })
                .AsList();
            }
            Console.WriteLine(string.Format("Totally {0} items", list.Count));
            List<AssessmentQuestionData> noteQuestions = new List<AssessmentQuestionData>();
            var urlStr = ConfigurationManager.AppSettings["MongoDB"];
            var url = MongoUrl.Create(urlStr);
            var mongodb = MongoDatabase.Create(url);
            var collection = mongodb.GetCollection<AssessmentQuestionData>("assessment");
            foreach (Assessment p in list)
            {
                p.Questions = p.OasisData.ToObject<List<Question>>();
                var noteQuestion = new AssessmentQuestionData(p);
                collection.Insert(noteQuestion);
                Console.WriteLine(string.Format("Processed assessment id: {0}", p.Id.ToString()));
            }
        }
    }
}
