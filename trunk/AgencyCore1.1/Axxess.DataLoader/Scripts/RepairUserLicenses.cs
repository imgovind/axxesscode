﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Repositories;
    using SubSonic.Repository;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using System.Configuration;
    using Axxess.AgencyManagement.Entities;
    public class RepairUserLicenses
    {
        public static void Run()
        {
            var script = @"SELECT 
                        Id,
                        AgencyId,
                        Licenses
                            FROM 
                                users 
                                    WHERE 
                                        AgencyId = '62b904fa-b38f-4686-b2e4-d748fa129c50' AND
                                        IsDeprecated = 0 ";

            var list = new List<User>();
            using (var cmd = new FluentCommand<User>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new User
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Licenses = reader.GetStringNullable("Licenses")
                })
                .AsList();
            }
            DateTime now = DateTime.Now;
            var simpleRepo = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
            var addedItems = new List<License>();
            try
            {
                foreach (var user in list)
                {
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        var licenses = user.Licenses.ToObject<List<License>>();
                        if (licenses.IsNotNullOrEmpty())
                        {
                            var licenseItems = licenses.Select(s => new License() { Id = s.Id, AgencyId = user.AgencyId, UserId = user.Id, AssetId = s.AssetId, LicenseNumber = s.LicenseNumber, IssueDate = s.IssueDate, ExpireDate = s.ExpireDate, LicenseType = s.LicenseType.IsNotNullOrEmpty() ? s.LicenseType : "Other", Created = s.Created, Modified = now, IsDeprecated = s.IsDeprecated }).ToList();
                            simpleRepo.AddMany(licenseItems);
                            addedItems.AddRange(licenseItems);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                simpleRepo.DeleteMany(addedItems);
            }
        }
    }
}
