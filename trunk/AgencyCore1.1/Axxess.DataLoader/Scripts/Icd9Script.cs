﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class Icd9Script
    {
        private static string input; 
        private static string output = Path.Combine(App.Root, string.Format("Files\\DXSG_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(bool isDiagnosis)
        {
            if (isDiagnosis)
            {
                input = Path.Combine(App.Root, "Files\\CMS29_DESC_LONG_SHORT_DX 101111.xls");
            }
            else
            {
                input = Path.Combine(App.Root, "Files\\CMS29_DESC_LONG_SHORT_SG.xls");
            }
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            if (isDiagnosis)
                                            {
                                                var dxCode = new DiagnosisCode_Copy
                                                {
                                                    Code = dataRow.GetValue(0),
                                                    LongDescription = dataRow.GetValue(1),
                                                    ShortDescription = dataRow.GetValue(2)
                                                };
                                                if (Database.AddForLookup<DiagnosisCode_Copy>(dxCode))
                                                {
                                                    Console.WriteLine("{0}) {1}", dxCode.Id, dxCode.FormatCode);
                                                }
                                                else
                                                {
                                                    Console.WriteLine("DX Code could not be added");
                                                }
                                            }
                                            else
                                            {
                                                var sgCode = new ProcedureCode_Copy
                                                {
                                                    Code = dataRow.GetValue(0),
                                                    LongDescription = dataRow.GetValue(1),
                                                    ShortDescription = dataRow.GetValue(2)
                                                };
                                                if (Database.AddForLookup<ProcedureCode_Copy>(sgCode))
                                                {
                                                    Console.WriteLine("{0}) {1}", sgCode.Id, sgCode.FormatCode);
                                                }
                                                else
                                                {
                                                    Console.WriteLine("SG Code could not be added");
                                                }
                                            }
                                            Console.WriteLine();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
