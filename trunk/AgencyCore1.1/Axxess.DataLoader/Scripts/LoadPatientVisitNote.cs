﻿
namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Repositories;
    using SubSonic.Repository;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using System.Configuration;
    using Axxess.AgencyManagement.Entities;
    public class LoadPatientVisitNote
    {
        public static void Run()
        {
            Console.WriteLine("Begginning moving patient visit note data to mongo.");
            var script = @"SELECT 
                        Id ,
                        PatientId ,
                        EpisodeId ,
                        AgencyId ,
                        NoteType,
                        Note,
                        WoundNote
                            FROM 
                                patientvisitnotes 
                                        WHERE 
                                            AgencyId = '62b904fa-b38f-4686-b2e4-d748fa129c50' AND
                                            IsDeprecated = 0 ";
            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    NoteType = reader.GetString("NoteType"),
                    Note = reader.GetStringNullable("Note"),
                    WoundNote = reader.GetStringNullable("WoundNote")
                }).SetTimeOut(int.MaxValue)
                .AsList();
            }
            Console.WriteLine(string.Format("Totally {0} items",list.Count));
            List<QuestionData> noteQuestions = new List<QuestionData>();
            var urlStr = ConfigurationManager.AppSettings["LocalMongoDB"];
            var url = MongoUrl.Create(urlStr);
            var mongodb = MongoDatabase.Create(url);
            var collection = mongodb.GetCollection<QuestionData>("patientvisitnote");
            foreach (PatientVisitNote p in list)
            {
                p.Questions = p.Note.ToObject<List<NotesQuestion>>();
                p.WoundQuestions = p.WoundNote.ToObject<List<NotesQuestion>>();
                var noteQuestion = new QuestionData(p);
                collection.Insert(noteQuestion);
                Console.WriteLine(string.Format("Processed visitnote id: {0}", p.Id.ToString()));
            }
            //var urlStr = ConfigurationManager.AppSettings["LocalMongoDB"];
            //var url = MongoUrl.Create(urlStr);
            //var mongodb = MongoDatabase.Create(url);
            //var collection = mongodb.GetCollection<QuestionData>("patientvisitnote");
            //collection.InsertBatch(noteQuestions);
        }
    }
}
