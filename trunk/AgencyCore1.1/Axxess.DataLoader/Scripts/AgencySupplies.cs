﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public static class AgencySupplies
    {
        private static string input = Path.Combine(App.Root, "Files\\Supplies.txt");
        public static void Run()
        {
            var suppliesSQL = File.ReadAllText(input);
            if (suppliesSQL.IsNotNullOrEmpty())
            {
                var agencies = Database.GetAgencies();
                if (agencies != null && agencies.Count > 0)
                {
                    Console.WriteLine("Agency List Count: {0}", agencies.Count);
                    agencies.ForEach(agency =>
                    {
                        using (var cmd = new FluentCommand<int>(suppliesSQL.Replace("'{0}'", string.Format("'{0}'", agency.Id.ToString()))))
                        {
                            cmd.SetConnection("AgencyManagementConnectionString")
                            .AsNonQuery();
                        }
                    });
                }
            }
        }
    }
}
