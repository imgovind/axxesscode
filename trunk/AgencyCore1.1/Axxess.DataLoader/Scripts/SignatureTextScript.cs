﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public static class SignatureTextScript
    {
        public static void Run()
        {
            var agency = Database.GetAgency(new Guid("e843ffbc-6865-41e0-8b7e-dc39e1ae224f"));
            if (agency != null)
            {
            //    var notes = Database.GetPatientVisitNotes(agency.Id);
            //    notes.ForEach(note =>
            //    {
            //        if (note.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && note.Status >= (int)ScheduleStatus.NoteSaved)
            //        {
            //            if (note.SignatureText.IsNotNullOrEmpty() && note.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", note.SignatureText);
            //                note.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.Update<PatientVisitNote>(note))
            //                {
            //                    Console.WriteLine("new signature: {0}", note.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //var pocorders = Database.GetPlanofCareByAgencyId(agency.Id);
            //pocorders.ForEach(order =>
            //{
            //    if (order.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && order.Status >= (int)ScheduleStatus.OrderSaved)
            //    {
            //        if (order.SignatureText.IsNotNullOrEmpty() && order.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //        {
            //            Console.WriteLine("old signature: {0}", order.SignatureText);
            //            order.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //            if (Database.UpdateForOasisC<PlanofCare>(order))
            //            {
            //                Console.WriteLine("new signature: {0}", order.SignatureText);
            //            }
            //        }
            //    }
            //});

            //    var orders = Database.GetPhysicianOrders(agency.Id);
            //    orders.ForEach(order =>
            //    {
            //        if (order.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && order.Status >= (int)ScheduleStatus.OrderSaved)
            //        {
            //            if (order.SignatureText.IsNotNullOrEmpty() && order.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", order.SignatureText);
            //                order.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.Update<PhysicianOrder>(order))
            //                {
            //                    Console.WriteLine("new signature: {0}", order.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var socAssessments = Database.GetSocByAgencyId(agency.Id);
            //    socAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<StartOfCareAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var rocAssessments = Database.GetRocByAgencyId(agency.Id);
            //    rocAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<ResumptionofCareAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var recertAssessments = Database.GetRecertByAgencyId(agency.Id);
            //    recertAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<RecertificationAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var followupAssessments = Database.GetFollowupByAgencyId(agency.Id);
            //    followupAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<FollowUpAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var deathAssessments = Database.GetDeathatHomeByAgencyId(agency.Id);
            //    deathAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<DeathAtHomeAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var dischargeAssessments = Database.GetDischargeByAgencyId(agency.Id);
            //    dischargeAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<DischargeFromAgencyAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var transferAssessments = Database.GetTransferByAgencyId(agency.Id);
            //    transferAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<TransferDischargeAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });

            //    var transfernotAssessments = Database.GetTransferNotByAgencyId(agency.Id);
            //    transfernotAssessments.ForEach(assessment =>
            //    {
            //        if (assessment.UserId == new Guid("0c13e051-e766-43c4-862c-624e9ce71072") && assessment.Status >= (int)ScheduleStatus.OasisSaved)
            //        {
            //            if (assessment.SignatureText.IsNotNullOrEmpty() && assessment.SignatureText.ToLower().Contains("chinwem nwonumah"))
            //            {
            //                Console.WriteLine("old signature: {0}", assessment.SignatureText);
            //                assessment.SignatureText = "Electronically Signed by: Chinwem Nwonumah RN";
            //                if (Database.UpdateForOasisC<TransferNotDischargedAssessment>(assessment))
            //                {
            //                    Console.WriteLine("new signature: {0}", assessment.SignatureText);
            //                }
            //            }
            //        }
            //    });
            }
        }
    }
}
