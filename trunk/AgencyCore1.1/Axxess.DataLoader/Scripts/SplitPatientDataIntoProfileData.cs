﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Repositories;
    using SubSonic.Repository;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using System.Configuration;
    using Axxess.AgencyManagement.Entities;
    public class SplitPatientDataIntoProfileData
    {
        public static void Run()
        {
            var script = @"SELECT 
                        Id,
                        PatientId,
                        PatientData
                            FROM 
                                patientadmissiondates 
                                        WHERE 
                                            PatientId = '4c87486e-02bc-43b5-a4da-1e14421ebe7e' AND
                                            AgencyId = '62b904fa-b38f-4686-b2e4-d748fa129c50' AND
                                            IsDeprecated = 0 ";

            var list = new List<PatientAdmissionDate>();
            using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
            {
                list = cmd.SetConnection("HomeHealthConnectionString")
                .SetMap(reader => new PatientAdmissionDate
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    PatientData = reader.GetStringNullable("PatientData")
                })
                .AsList();
            }

            foreach (var admission in list)
            {
                var oldPatient = admission.PatientData.ToObject<OldPatient>();
                Patient patient = new Patient(oldPatient);
                Profile profile = new Profile(oldPatient);
                if (!UpdatePatientAdmissionDate(admission, patient, profile))
                {
                    throw new Exception("You are a FAILURE!!!!!");
                }
            }
        }

        private static bool UpdatePatientAdmissionDate(PatientAdmissionDate data, Patient patient, Profile profile)
        {
            var script = @"UPDATE patientadmissiondates
                            SET PatientData = @patientdata, ProfileData = @profiledata
                            WHERE 
                                Id = @id AND
                                PatientId = @patientid AND
                                IsDeprecated = 0 ";

            using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
            {
                return cmd.SetConnection("HomeHealthConnectionString")
                    .AddGuid("id", data.Id)
                    .AddGuid("patientid", data.PatientId)
                    .AddString("patientdata", patient.ToXml())
                    .AddString("profiledata", profile.ToXml())
                    .AsNonQuery() > 0;
            }
        }
    }
}
