﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    
    using Axxess.AgencyManagement.Entities.Enums;

    public static class AgencyMergeScript
    {
        public static void Run(string agencyId, List<string> agencyIdList)
        {
            var mainAgency = Database.GetAgency(agencyId.ToGuid());

            if (mainAgency != null)
            {
                agencyIdList.ForEach(otherAgencyId =>
                {
                    //ConvertLocationUsersPatientsAndOASIS(mainAgency, otherAgencyId);
                    //ConvertPhysicians(mainAgency.Id, otherAgencyId);
                    //ConvertPatientEpisodes(mainAgency.Id, otherAgencyId);
                    //ConvertPatientAdmissionPeriodAndNotes(mainAgency.Id, otherAgencyId);
                    //ConvertAllergyMedicationProfileAndOrders(mainAgency.Id, otherAgencyId);
                    ConvertRapsAndFinals(mainAgency.Id, otherAgencyId);
                });
            }
        }

        private static void ConvertRapsAndFinals(Guid mainAgencyId, string otherAgencyId)
        {
            var raps = Database.GetRaps(otherAgencyId.ToGuid());
            raps.ForEach(item =>
            {
                item.AgencyId = mainAgencyId;
                item.Modified = DateTime.Now;

                if (Database.Update<Rap>(item))
                {
                    Console.WriteLine("Converted Rap");
                }
            });

            var finals = Database.GetFinals(otherAgencyId.ToGuid());
            finals.ForEach(item =>
            {
                item.AgencyId = mainAgencyId;
                item.Modified = DateTime.Now;

                if (Database.Update<Final>(item))
                {
                    Console.WriteLine("Converted Final");
                }
            });
        }

        private static void ConvertAllergyMedicationProfileAndOrders(Guid mainAgencyId, string otherAgencyId)
        {
            var allergies = Database.GetAllergyProfiles(otherAgencyId.ToGuid());
            allergies.ForEach(item =>
            {
                item.AgencyId = mainAgencyId;
                item.Modified = DateTime.Now;

                if (Database.Update<AllergyProfile>(item))
                {
                    Console.WriteLine("Converted Allergy Profile");
                }
            });

            var medications = Database.GetMedicationProfiles(otherAgencyId.ToGuid());
            medications.ForEach(item =>
            {
                item.AgencyId = mainAgencyId;
                item.Modified = DateTime.Now;

                if (Database.Update<MedicationProfile>(item))
                {
                    Console.WriteLine("Converted Patient Medications");
                }
            });

            var orders = Database.GetPhysicianOrders(otherAgencyId.ToGuid());
            orders.ForEach(item =>
            {
                item.AgencyId = mainAgencyId;
                item.Modified = DateTime.Now;

                if (Database.Update<PhysicianOrder>(item))
                {
                    Console.WriteLine("Converted Patient Physician Orders");
                }
            });

            var f2fOrders = Database.GetFaceToFaceEncounters(otherAgencyId.ToGuid());
            f2fOrders.ForEach(item =>
            {
                item.AgencyId = mainAgencyId;
                item.Modified = DateTime.Now;

                if (Database.Update<FaceToFaceEncounter>(item))
                {
                    Console.WriteLine("Converted Face 2 Face Orders");
                }
            });
        }

        private static void ConvertPatientAdmissionPeriodAndNotes(Guid mainAgencyId, string otherAgencyId)
        {
            var periods = Database.GetPatientAdmissionDates(otherAgencyId.ToGuid());
            periods.ForEach(period =>
            {
                period.AgencyId = mainAgencyId;
                period.Modified = DateTime.Now;

                if (Database.Update<PatientAdmissionDate>(period))
                {
                    Console.WriteLine("Converted Admin Period");
                }
            });

            var notes = Database.GetPatientVisitNotes(otherAgencyId.ToGuid());
            notes.ForEach(note =>
            {
                note.AgencyId = mainAgencyId;
                note.Modified = DateTime.Now;

                if (Database.Update<PatientVisitNote>(note))
                {
                    Console.WriteLine("Converted Patient Visit Note");
                }
            });
        }


        private static void ConvertPatientEpisodes(Guid mainAgencyId, string otherAgencyId)
        {
            var episodes = Database.GetEpisodes(otherAgencyId.ToGuid());
            episodes.ForEach(episode =>
            {
                episode.AgencyId = mainAgencyId;
                episode.Modified = DateTime.Now;

                if (Database.Update<PatientEpisode>(episode))
                {
                    Console.WriteLine("Converted {0} - {1}", episode.StartOfCareDateFormatted, episode.EndDateFormatted);
                }
            });
        }

        private static void ConvertPhysicians(Guid mainAgencyId, string otherAgencyId)
        {
            var physicians = Database.GetPhysicians(otherAgencyId.ToGuid());
            physicians.ForEach(physician =>
            {
                physician.AgencyId = mainAgencyId;
                physician.Modified = DateTime.Now;

                if (Database.Update<AgencyPhysician>(physician))
                {
                    Console.WriteLine("Converted {0}", physician.DisplayName);
                }
            });
        }

        private static void ConvertLocationUsersPatientsAndOASIS(Agency mainAgency, string otherAgencyId)
        {
            var oldAgency = Database.GetAgency(otherAgencyId.ToGuid());
            var oldAgencyLocation = Database.GetAgencyLocation(otherAgencyId.ToGuid());
            if (oldAgency != null && oldAgencyLocation != null)
            {
                var newLocation = new AgencyLocation
                {
                    Id = Guid.NewGuid(),
                    AgencyId = mainAgency.Id,
                    Name = oldAgency.Name,
                    CBSA = oldAgencyLocation.CBSA,
                    MedicareProviderNumber = oldAgency.MedicareProviderNumber,
                    AddressLine1 = oldAgencyLocation.AddressLine1,
                    AddressLine2 = oldAgencyLocation.AddressLine2,
                    AddressCity = oldAgencyLocation.AddressCity,
                    AddressStateCode = oldAgencyLocation.AddressStateCode,
                    AddressZipCode = oldAgencyLocation.AddressZipCode,
                    AddressZipCodeFour = oldAgencyLocation.AddressZipCodeFour,
                    PhoneWork = oldAgencyLocation.PhoneWork,
                    FaxNumber = oldAgencyLocation.FaxNumber,
                    IsSubmitterInfoTheSame = false,
                    SubmitterId = oldAgency.SubmitterId,
                    SubmitterName = oldAgency.SubmitterName,
                    SubmitterFax = oldAgency.SubmitterFax,
                    SubmitterPhone = oldAgency.SubmitterPhone,
                    Payor = oldAgency.Payor,
                    BranchId = oldAgencyLocation.BranchId,
                    BranchIdOther = oldAgencyLocation.BranchIdOther,
                    Ub04Locator81cca = oldAgencyLocation.Ub04Locator81cca,
                    TaxId = oldAgency.TaxId,
                    TaxIdType = oldAgency.TaxIdType,
                    MedicaidProviderNumber = oldAgency.MedicaidProviderNumber,
                    NationalProviderNumber = oldAgency.NationalProviderNumber,
                    HomeHealthAgencyId = oldAgency.HomeHealthAgencyId,
                    CahpsVendor = oldAgency.CahpsVendor,
                    CahpsSurveyDesignator = oldAgency.CahpsSurveyDesignator,
                    ContactPersonEmail = oldAgency.ContactPersonEmail,
                    ContactPersonFirstName = oldAgency.ContactPersonFirstName,
                    ContactPersonLastName = oldAgency.ContactPersonLastName,
                    ContactPersonPhone = oldAgency.ContactPersonPhone,
                    CahpsVendorId = oldAgency.CahpsVendorId,
                    IsAxxessTheBiller = oldAgency.IsAxxessTheBiller,
                    OasisAuditVendor = oldAgency.OasisAuditVendor,
                    IsLocationStandAlone = true,
                    Created = oldAgency.Created,
                    Modified = DateTime.Now
                };

                if (Database.Add<AgencyLocation>(newLocation))
                {
                    var mainAgencyUsers = Database.GetUsers(mainAgency.Id);
                    var oldAgencyUsers = Database.GetUsers(oldAgency.Id);
                    oldAgencyUsers.ForEach(user =>
                    {
                        user.AgencyId = mainAgency.Id;
                        user.AgencyLocationId = newLocation.Id;
                        user.Modified = DateTime.Now;
                        if (Database.Update<User>(user))
                        {
                            Console.WriteLine("Converted {0} from {1}", user.DisplayName, oldAgency.Name);
                        }
                    });

                    var mainAgencyPatients = Database.GetPatients(mainAgency.Id);
                    var oldAgencyPatients = Database.GetPatients(oldAgency.Id);
                    oldAgencyPatients.ForEach(patient =>
                    {
                        patient.AgencyId = mainAgency.Id;
                        patient.AgencyLocationId = newLocation.Id;
                        patient.Modified = DateTime.Now;

                        if (Database.Update<Patient>(patient))
                        {
                            var patientAdminPeriod = Database.GetPatientAdminPeriod(patient.Id, oldAgency.Id);
                            if (patientAdminPeriod != null)
                            {
                                patientAdminPeriod.AgencyId = mainAgency.Id;
                                patientAdminPeriod.PatientData = patient.ToXml();
                                patientAdminPeriod.Modified = DateTime.Now;
                                Database.Update<PatientAdmissionDate>(patientAdminPeriod);
                            }
                            Console.WriteLine("Converted {0} from {1}", patient.DisplayName, oldAgency.Name);
                        }
                    });

                    var oasisAssessments = Database.GetAssessmentsNew<Assessment>(oldAgency.Id);
                    if (oasisAssessments != null && oasisAssessments.Count > 0)
                    {
                        Console.WriteLine("Assessment List Count: {0}", oasisAssessments.Count);
                        bool result=Database.UpdateForOasisCNew<Assessment>(oldAgency.Id, mainAgency.Id, oasisAssessments.Select(a => a.Id).Distinct().ToList());
                      
                    }

                    //var socList = Database.GetAssessments<StartOfCareAssessment>(oldAgency.Id);
                    //if (socList != null && socList.Count > 0)
                    //{
                    //    Console.WriteLine("SOC List Count: {0}", socList.Count);
                    //    socList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<StartOfCareAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}

                    //var rocList = Database.GetAssessments<ResumptionofCareAssessment>(oldAgency.Id);
                    //if (rocList != null && rocList.Count > 0)
                    //{
                    //    Console.WriteLine("ROC List Count: {0}", rocList.Count);
                    //    rocList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<ResumptionofCareAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}

                    //var recertList = Database.GetAssessments<RecertificationAssessment>(oldAgency.Id);
                    //if (recertList != null && recertList.Count > 0)
                    //{
                    //    Console.WriteLine("Recert List Count: {0}", recertList.Count);
                    //    recertList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<RecertificationAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}


                    //var followupList = Database.GetAssessments<FollowUpAssessment>(oldAgency.Id);
                    //if (followupList != null && followupList.Count > 0)
                    //{
                    //    Console.WriteLine("FollowUp List Count: {0}", followupList.Count);
                    //    followupList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<FollowUpAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}


                    //var deathList = Database.GetAssessments<DeathAtHomeAssessment>(oldAgency.Id);
                    //if (deathList != null && deathList.Count > 0)
                    //{
                    //    Console.WriteLine("Death List Count: {0}", deathList.Count);
                    //    deathList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<DeathAtHomeAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}

                    //var dischargeList = Database.GetAssessments<DischargeFromAgencyAssessment>(oldAgency.Id);
                    //if (dischargeList != null && dischargeList.Count > 0)
                    //{
                    //    Console.WriteLine("Discharge List Count: {0}", dischargeList.Count);
                    //    dischargeList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<DischargeFromAgencyAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}

                    //var transferList = Database.GetAssessments<TransferDischargeAssessment>(oldAgency.Id);
                    //if (transferList != null && transferList.Count > 0)
                    //{
                    //    Console.WriteLine("Transfer List Count: {0}", transferList.Count);
                    //    transferList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<TransferDischargeAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}

                    //var transferNotList = Database.GetAssessments<TransferNotDischargedAssessment>(oldAgency.Id);
                    //if (transferNotList != null && transferNotList.Count > 0)
                    //{
                    //    Console.WriteLine("TransferNot List Count: {0}", transferNotList.Count);
                    //    transferNotList.ForEach(a =>
                    //    {
                    //        a.AgencyId = mainAgency.Id;
                    //        a.Modified = DateTime.Now;
                    //        if (Database.UpdateForOasisC<TransferNotDischargedAssessment>(a))
                    //        {
                    //            Console.WriteLine("Converted {0}", a.TypeDescription);
                    //        }
                    //    });
                    //}

                    var planofCareList = Database.GetPlanofCareByAgencyId(oldAgency.Id);
                    if (planofCareList != null && planofCareList.Count > 0)
                    {
                        Console.WriteLine("Plan of Care List Count: {0}", planofCareList.Count);
                        planofCareList.ForEach(a =>
                        {
                            a.AgencyId = mainAgency.Id;
                            a.Modified = DateTime.Now;
                            if (Database.UpdateForOasisC<PlanofCare>(a))
                            {
                                Console.WriteLine("Converted Plan of Care");
                            }
                        });
                    }
                }
            }
        }
    }
}
