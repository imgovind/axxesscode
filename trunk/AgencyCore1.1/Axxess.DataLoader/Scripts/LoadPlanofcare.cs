﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Entities.Repositories;
    using SubSonic.Repository;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using System.Configuration;
    using Axxess.AgencyManagement.Entities;
    public class LoadPlanofcare
    {
        private static SimpleRepository database;
        public static void Run()
        {
            database = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
            //agencyRepository = new AgencyRepository(database);
            var script = @"SELECT 
                        planofcares.Id as Id ,
                        planofcares.PatientId as PatientId ,
                        planofcares.EpisodeId as EpisodeId ,
                        planofcares.AgencyId as AgencyId ,
                        planofcares.AssessmentType as AssessmentType,
                        planofcares.Data as Data
                            FROM 
                                planofcares 
                                        WHERE 
                                            planofcares.IsDeprecated = 0 ";

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    AssessmentType = reader.GetString("AssessmentType"),
                    Data = reader.GetString("Data")
                })
                .AsList();
            }
            Console.WriteLine(string.Format("Totally {0} items", list.Count));
            List<AssessmentQuestionData> noteQuestions = new List<AssessmentQuestionData>();
            var urlStr = ConfigurationManager.AppSettings["MongoDB"];
            var url = MongoUrl.Create(urlStr);
            var mongodb = MongoDatabase.Create(url);
            var collection = mongodb.GetCollection<AssessmentQuestionData>("planofcares");
            foreach (PlanofCare p in list)
            {
                p.Questions = p.Data.ToObject<List<Question>>();
                var noteQuestion = new AssessmentQuestionData(p);
                //noteQuestions.Add(noteQuestion);
                collection.Insert(noteQuestion);
                Console.WriteLine(string.Format("Processed planofcare id: {0}", p.Id.ToString()));
            }


        }
    }
}
