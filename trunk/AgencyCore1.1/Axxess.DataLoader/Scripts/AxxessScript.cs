﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class AxxessScript
    {
        private static string input = Path.Combine(App.Root, "Files\\PatientDataTemplate.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PatientDataTemplate_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PrimaryRelationship = "";
                                            patientData.SecondaryRelationship = "";
                                            patientData.TertiaryRelationship = "";
                                            

                                            if (dataRow.GetValue(0).Split(' ').Length > 1)
                                            {
                                                patientData.FirstName = dataRow.GetValue(0).Split(' ')[0];
                                                patientData.MiddleInitial = dataRow.GetValue(0).Split(' ')[1];
                                            }
                                            else
                                            {
                                                patientData.FirstName = dataRow.GetValue(0);
                                            }
                                            patientData.LastName = dataRow.GetValue(1);
                                            patientData.Gender = dataRow.GetValue(2).IsEqual("F") ? "Female" : "Male";
                                            if(dataRow.GetValue(3).IsNotNullOrEmpty())
                                                patientData.PatientIdNumber = dataRow.GetValue(3);
                                            if(dataRow.GetValue(4).IsNotNullOrEmpty())
                                                patientData.MedicareNumber = dataRow.GetValue(4);

                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            }

                                            //if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            //{
                                            //    patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            //}
                                            if(dataRow.GetValue(6).IsNotNullOrEmpty())
                                                patientData.SSN = dataRow.GetValue(6).Replace(" ", "");
                                            //if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            //{
                                            //    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                            //}
                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                            }
                                            if (dataRow.GetValue(8).IsNotNullOrEmpty() && dataRow.GetValue(8).Split('-').Length > 1)
                                            {
                                                patientData.Comments += string.Format("Episode Start Date:{0}", dataRow.GetValue(8));
                                            }
                                            else if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Episode Start Date:{0}.", DateTime.FromOADate(double.Parse(dataRow.GetValue(8))).ToShortDateString());
                                            }
                                            if (!dataRow.GetValue(9).Trim().Equals("N/A"))
                                            {
                                                patientData.DischargeDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(9)));
                                                patientData.Status = 2;
                                            }
                                            if (dataRow.GetValue(10).ToLower().Contains("married"))
                                            {
                                                patientData.MaritalStatus = "Married";
                                            }
                                            else if (dataRow.GetValue(10).ToLower().Contains("single"))
                                            {
                                                patientData.MaritalStatus = "Single";
                                            }
                                            else if (dataRow.GetValue(10).ToLower().Contains("widowed"))
                                            {
                                                patientData.MaritalStatus = "Widowed";
                                            }
                                            else if (dataRow.GetValue(10).ToLower().Contains("divorce"))
                                            {
                                                patientData.MaritalStatus = "Divorced";
                                            }
                                            else
                                            {
                                                patientData.MaritalStatus = "Unknown";
                                            }

                                            patientData.AddressLine1 = dataRow.GetValue(11);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(12);
                                            patientData.AddressStateCode = dataRow.GetValue(13);
                                            patientData.AddressZipCode = dataRow.GetValue(14);
                                            if(dataRow.GetValue(15).Trim().IsNotNullOrEmpty())
                                                patientData.PhoneHome = dataRow.GetValue(15).ToPhoneDB();
                                            patientData.PrimaryInsurance = "1";
                                            

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(24);
                                                            var physician = Database.GetPhysician(npi, agencyId.ToGuid());
                                                            if (physician == null)
                                                            {
                                                                exists = false;
                                                                var info = Database.GetNpiData(npi);
                                                                physician = new AgencyPhysician
                                                                {
                                                                    Id = Guid.NewGuid(),
                                                                    AgencyId = agencyId.ToGuid(),
                                                                    NPI = npi,
                                                                    LoginId = Guid.Empty,
                                                                    AddressLine1 = dataRow.GetValue(18),
                                                                    AddressCity = dataRow.GetValue(19),
                                                                    AddressStateCode = dataRow.GetValue(20),
                                                                    AddressZipCode = dataRow.GetValue(21),
                                                                    PhoneWork = dataRow.GetValue(22).ToPhoneDB(),
                                                                    FaxNumber = dataRow.GetValue(23).ToPhoneDB(),
                                                                    FirstName = info != null ? info.ProviderFirstName : string.Empty,
                                                                    LastName = info != null ? info.ProviderLastName : string.Empty,
                                                                    Credentials = dataRow.GetValue(17)
                                                                };
                                                                var physicianNameArray = dataRow.GetValue(16).Replace("Dr.", "").Replace("Dr", "").Replace("DR.", "").Replace("DR", "").Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                //var physicianNameArray = dataRow.GetValue(15).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                                {
                                                                    physician.LastName =physicianNameArray[1].IsNotNullOrEmpty()? physicianNameArray[1].Trim():physician.LastName;
                                                                    physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : physician.FirstName;
                                                                }
                                                                //else if (physicianNameArray != null && physicianNameArray.Length == 2)
                                                                //{
                                                                //    physician.FirstName = info.ProviderFirstName;
                                                                //    physician.LastName = physicianNameArray[1].Trim();
                                                                //}

                                                                Database.Add(physician);
                                                            }

                                                            if (physician != null)
                                                            {
                                                                var patientPhysician = new PatientPhysician
                                                                {
                                                                    IsPrimary = true,
                                                                    PatientId = patientData.Id,
                                                                    PhysicianId = physician.Id
                                                                };

                                                                if (Database.Add(patientPhysician))
                                                                {
                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
