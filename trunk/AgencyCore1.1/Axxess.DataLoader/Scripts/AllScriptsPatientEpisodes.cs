﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Linq;


    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class AllScriptsPatientEpisodes
    {
        private static string input = Path.Combine(App.Root, "Files\\ActivePatientDiagnosesEpisodesAxxessImplementation.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\ActivePatientDiagnosesEpisodesAxxessImplementation{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    var patients = Database.GetPatients(agencyId);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            if(dataRow.GetValue(0).ToLower().Contains("patient id"))
                                                continue;
                                            var id = dataRow.GetValue(0);
                                            var patientsData = patients.Where(p => p.PatientIdNumber == id);
                                            Patient patientData = null;
                                            if (patients != null && patients.Count > 0)
                                            {
                                                patientData = patientsData.First();
                                            }
                                            if (patientData == null)
                                                continue;
                                            patientData.StartofCareDate = dataRow.GetValue(4).IsValidDate() ? dataRow.GetValue(4).ToDateTime() : DateTime.MinValue;
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty() && dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Diagnosis Code & Description: {0} - {1}",dataRow.GetValue(6), dataRow.GetValue(7));
                                            }
                                            if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Acuit: {0}", dataRow.GetValue(10));
                                            }
                                            if (dataRow.GetValue(11).IsNotNullOrEmpty() && dataRow.GetValue(11).IsValidDate() && dataRow.GetValue(12).IsNotNullOrEmpty() && dataRow.GetValue(12).IsValidDate())
                                            {
                                                patientData.Comments += string.Format("Episode Range: {0} - {1} .", dataRow.GetValue(11).ToDateTime().ToString("MM/dd/yyyy"), dataRow.GetValue(12).ToDateTime().ToString("MM/dd/yyyy"));
                                            }
                                            var a = 5;
                                            if (Database.Update(patientData))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };

                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
