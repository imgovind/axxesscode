﻿

namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Entities;
    public static class AllegenyIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\bogata.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\bogata_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int count = 0;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    var patientData = new Patient();
                                    patientData.Id = Guid.NewGuid();
                                    patientData.AgencyId = agencyId;
                                    patientData.AgencyLocationId = locationId;
                                    patientData.Ethnicities = string.Empty;
                                    patientData.MaritalStatus = string.Empty;
                                    patientData.IsDeprecated = false;
                                    patientData.IsHospitalized = false;
                                    patientData.PrimaryRelationship = "";
                                    patientData.SecondaryRelationship = "";
                                    patientData.TertiaryRelationship = "";
                                    patientData.LastName = dataRow.GetValue(0);
                                    patientData.FirstName = dataRow.GetValue(1);
                                    patientData.MiddleInitial = dataRow.GetValue(2);
                                    patientData.PatientIdNumber = dataRow.GetValue(3);
                                    patientData.AddressLine1 = dataRow.GetValue(4);
                                    patientData.AddressLine2 = dataRow.GetValue(5);
                                    patientData.AddressCity = dataRow.GetValue(6);
                                    patientData.AddressStateCode = dataRow.GetValue(7);
                                    patientData.AddressZipCode = dataRow.GetValue(8);
                                    if (dataRow.GetValue(9).IsNotNullOrEmpty() && dataRow.GetValue(9).ToLower().Contains("medicare"))
                                    {
                                        patientData.PrimaryInsurance = "1";
                                        if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                            patientData.MedicareNumber = dataRow.GetValue(10);
                                    }
                                    else
                                    {
                                        patientData.MedicaidNumber = dataRow.GetValue(10);
                                    }
                                    if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Priority:{0}.", dataRow.GetValue(11));
                                    }
                                    if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Office:{0}.", dataRow.GetValue(12));
                                    }
                                    patientData.Comments += string.Format("Therapystring:{0}.", dataRow.GetValue(14));
                                    patientData.PhoneHome = dataRow.GetValue(15).IsNotNullOrEmpty() ? dataRow.GetValue(15).ToPhoneDB() : "";
                                    if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                    {
                                        patientData.DOB = dataRow.GetValue(16).ToDateTime();
                                    }
                                    if (dataRow.GetValue(17).IsNotNullOrEmpty() && dataRow.GetValue(17).ToLower().Contains("active"))
                                    {
                                        patientData.Status = 1;
                                    }
                                    else if (dataRow.GetValue(17).IsNotNullOrEmpty() && dataRow.GetValue(17).ToLower().Contains("terminated/discharged"))
                                    {
                                        patientData.Status = 2;
                                    }
                                    else
                                    {
                                        patientData.Status = 3;
                                    }
                                    patientData.Gender = dataRow.GetValue(18);

                                    if (dataRow.GetValue(19).IsNotNullOrEmpty() && dataRow.GetValue(19).ToLower().Contains("m"))
                                    {
                                        patientData.MaritalStatus = "Married";
                                    }
                                    if (dataRow.GetValue(19).IsNotNullOrEmpty() && dataRow.GetValue(19).ToLower().Contains("s"))
                                    {
                                        patientData.MaritalStatus = "Single";
                                    }
                                    if (dataRow.GetValue(19).IsNotNullOrEmpty() && dataRow.GetValue(19).ToLower().Contains("w"))
                                    {
                                        patientData.MaritalStatus = "Widowed";
                                    }
                                    if (dataRow.GetValue(19).IsNotNullOrEmpty() && dataRow.GetValue(19).ToLower().Contains("d"))
                                    {
                                        patientData.MaritalStatus = "Divorce";
                                    }
                                    if (dataRow.GetValue(19).IsNotNullOrEmpty() && dataRow.GetValue(19).ToLower().Contains("u"))
                                    {
                                        patientData.MaritalStatus = "Unknown";
                                    }

                                    patientData.SSN = dataRow.GetValue(20).Replace("-", "");
                                    if (dataRow.GetValue(21).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Caseworker:{0}.", dataRow.GetValue(20));
                                    }
                                    
                                    if (dataRow.GetValue(22).IsNotNullOrEmpty())
                                    {
                                        patientData.StartofCareDate = dataRow.GetValue(22).ToDateTime();
                                    }

                                    patientData.Comments += string.Format("Doc name:{0}.", dataRow.GetValue(23));
                                    patientData.Comments += string.Format("{0}.", dataRow.GetValue(24));
                                    if(dataRow.GetValue(27).IsNotNullOrEmpty())
                                        patientData.Comments += string.Format("Primary diag:{0}.", dataRow.GetValue(27));
                                    if(dataRow.GetValue(31).IsNotNullOrEmpty())
                                        patientData.Comments += string.Format("Second diag:{0}.", dataRow.GetValue(31));
                                    if(dataRow.GetValue(30).IsNotNullOrEmpty())
                                        patientData.Comments += string.Format("Third diag:{0}.", dataRow.GetValue(30));
                                    if(dataRow.GetValue(29).IsNotNullOrEmpty())
                                        patientData.Comments += string.Format("Fourth diag:{0}.", dataRow.GetValue(29));
                                    if(dataRow.GetValue(28).IsNotNullOrEmpty())
                                        patientData.Comments += string.Format("Fifth diag:{0}.", dataRow.GetValue(28));
                                    if (dataRow.GetValue(32).IsNotNullOrEmpty())
                                        patientData.Comments += string.Format("Employee Responsible:{0}.", dataRow.GetValue(32));
                                    if (dataRow.GetValue(33).IsNotNullOrEmpty() && dataRow.GetValue(34).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Cert Period: {0} - {1}. ", DateTime.Parse(dataRow.GetValue(33)).ToShortDateString(), DateTime.Parse(dataRow.GetValue(34)).ToShortDateString());
                                    }

                                    patientData.Created = DateTime.Now;
                                    patientData.Modified = DateTime.Now;

                                    var medicationProfile = new MedicationProfile
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Medication = "<ArrayOfMedication />"
                                    };

                                    var allergyProfile = new AllergyProfile
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Allergies = "<ArrayOfAllergy />"
                                    };

                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                    {
                                        var admissionPeriod = new PatientAdmissionDate
                                        {
                                            Id = Guid.NewGuid(),
                                            AgencyId = agencyId,
                                            Created = DateTime.Now,
                                            DischargedDate = DateTime.MinValue,
                                            IsActive = true,
                                            IsDeprecated = false,
                                            Modified = DateTime.Now,
                                            PatientData = patientData.ToXml().Replace("'", ""),
                                            PatientId = patientData.Id,
                                            Reason = string.Empty,
                                            StartOfCareDate = patientData.StartofCareDate,
                                            Status = patientData.Status
                                        };
                                        if (Database.Add(admissionPeriod))
                                        {
                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                            if (patient != null)
                                            {
                                                patient.AdmissionId = admissionPeriod.Id;
                                                if (Database.Update(patient))
                                                {
                                                    count++;
                                                    Console.WriteLine("{0}) {1}", count, patientData.DisplayName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
