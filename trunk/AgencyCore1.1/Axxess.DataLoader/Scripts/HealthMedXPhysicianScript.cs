﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Entities.Enums;
    using Axxess.AgencyManagement.Entities;

    public static class HealthMedXPhysicianScript
    {
        private static string input = Path.Combine(App.Root, "Files\\algonquinphysicians.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\algonquinphysicians_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty() && dataRow.GetValue(3).IsNotNullOrEmpty())
                                        {
                                            var physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;
                                            physicianData.IsDeprecated = false;
                                            physicianData.UPIN = dataRow.GetValue(0);
                                            physicianData.NPI = dataRow.GetValue(2);

                                            var nameArray = dataRow.GetValue(3).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                physicianData.LastName = nameArray[0].Trim();
                                                physicianData.FirstName = nameArray[1].Trim();
                                            }

                                            physicianData.Credentials = dataRow.GetValue(6);
                                            if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            {
                                                physicianData.AddressLine1 = dataRow.GetValue(8);
                                                physicianData.AddressLine2 = string.Empty;
                                                var addressArray = dataRow.GetValue(8).Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (addressArray != null && addressArray.Length > 1)
                                                {
                                                    physicianData.AddressLine1 = addressArray[0].Trim();
                                                    physicianData.AddressLine2 = addressArray[1].Trim();
                                                }
                                            }
                                            physicianData.AddressCity = dataRow.GetValue(9);
                                            physicianData.AddressStateCode = dataRow.GetValue(10);
                                            physicianData.AddressZipCode = dataRow.GetValue(11);
                                            if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            {
                                                var phone = dataRow.GetValue(12).ToPhoneDB();
                                                if (phone.Length >= 10)
                                                {
                                                    physicianData.PhoneWork = phone.Substring(0, 10);
                                                }
                                            }
                                            physicianData.FaxNumber = dataRow.GetValue(14).ToPhoneDB();

                                            var stringBuilder = new StringBuilder();
                                            if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                            {
                                                stringBuilder.AppendFormat("UPIN: {0}. ", dataRow.GetValue(0));
                                            }
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                stringBuilder.AppendFormat("Internal ID: {0}. ", dataRow.GetValue(5));
                                            }
                                            if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            {
                                                stringBuilder.AppendFormat("Phone Number(s): {0}. ", dataRow.GetValue(12));
                                            }
                                            physicianData.Comments = stringBuilder.ToString().Trim();

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
