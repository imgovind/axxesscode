﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;

    public static class PhysicianOrders
    {
        public static void Run()
        {
            var i = 1;
            var orders = Database.GetFaceToFaceEncounters();
            if (orders != null && orders.Count > 0)
            {
                Console.WriteLine("Order List Count: {0}", orders.Count);
                orders.ForEach(order =>
                {
                    if (order.SignatureText.IsNullOrEmpty() && order.SignatureDate.IsValid() && order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                    {
                        Console.WriteLine("{0}) {1}", i, order.SignatureDate.ToShortDateString());
                        var physician = Database.GetPhysician(order.PhysicianId, order.AgencyId);
                        if (physician != null && physician.DisplayName.IsNotNullOrEmpty())
                        {
                            var signature = string.Format("Signed by: {0}", physician.DisplayName);
                            if (physician.PhysicianAccess)
                            {
                                signature = string.Format("Electronically Signed by: {0}", physician.DisplayName);
                            }
                            order.SignatureText = signature;
                            if (Database.Update<FaceToFaceEncounter>(order))
                            {
                                Console.WriteLine("{0}) {1}", i, signature);
                                i++;
                            }
                        }
                        else
                        {
                            Thread.Sleep(1000);
                        }
                    }
                });
            }
        }
    }
}
