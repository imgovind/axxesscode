﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Entities;
    using Axxess.AgencyManagement.Entities.Enums;


    using SubSonic.Repository;
   

    public static class Database
    {
       
        private static readonly SimpleRepository agencyManagementDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository oasisDatabase = new SimpleRepository("OasisCConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository lookupDatabase = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);

        internal static List<Agency> GetAgencies()
        {
            return agencyManagementDatabase.All<Agency>().ToList();
        }

        internal static Agency GetAgency(Guid agencyId)
        {
            return agencyManagementDatabase.Single<Agency>(a => a.Id == agencyId);
        }

        internal static AgencyLocation GetAgencyLocation(Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyLocation>(a => a.AgencyId == agencyId);
        }

        internal static List<AgencyLocation> GetAgencyLocations(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyLocation>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<AgencyLocation> GetAgencyLocations()
        {
            return agencyManagementDatabase.All<AgencyLocation>().ToList();
        }

        internal static List<PhysicianOrder> GetOrders()
        {
            return agencyManagementDatabase.All<PhysicianOrder>().ToList();
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters()
        {
            return agencyManagementDatabase.All<FaceToFaceEncounter>().ToList();
        }

        internal static Patient GetPatient(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static PatientAdmissionDate GetPatientAdminPeriod(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<PatientAdmissionDate>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static User GetUser(Guid userId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<User>(p => p.Id == userId && p.AgencyId == agencyId);
        }

        internal static List<User> GetUsers(Guid agencyId)
        {
            return agencyManagementDatabase.Find<User>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Rap> GetRaps(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Rap>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Final> GetFinals(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Final>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Patient> GetPatients(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Patient>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<AllergyProfile> GetAllergyProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AllergyProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<MedicationProfile> GetMedicationProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<MedicationProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PhysicianOrder> GetPhysicianOrders(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PhysicianOrder>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId)
        {
            return agencyManagementDatabase.Find<FaceToFaceEncounter>(p => p.AgencyId == agencyId).ToList();
        }

        internal static bool UpdatePatient(Patient patient)
        {
            if (patient != null)
            {
                agencyManagementDatabase.Update<Patient>(patient);
                return true;
            }
            return false;
        }

        internal static AgencyPhysician GetPhysician(string npi, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.NPI == npi && p.AgencyId == agencyId);
        }

        internal static AgencyPhysician GetPhysician(Guid physicianId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.Id == physicianId && p.AgencyId == agencyId);
        }

        internal static List<AgencyPhysician> GetPhysicians(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyPhysician>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientEpisode> GetEpisodes(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientEpisode>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientAdmissionDate>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId)
        {
            var list = new List<PatientVisitNote>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `WoundNote` from patientvisitnotes where agencyid = '{0}' and IsWoundCare = 1 and IsDeprecated = 0;", agencyId);
            using (var cmd = new FluentCommand<PatientVisitNote>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    WoundNote = reader.GetStringNullable("WoundNote")
                })
                .AsList();
            }
            return list;
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId, DateTime startTime, DateTime endTime)
        {
            return agencyManagementDatabase.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.Modified >= startTime && p.Modified <= endTime).OrderByDescending(p => p.Modified).ToList();
        }

        internal static Npi GetNpiData(string npi)
        {
            return lookupDatabase.Single<Npi>(p => p.Id == npi);
        }

        internal static List<PlanofCare> GetPlanofCareByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<PlanofCare>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<ScheduleEvent> GetScheduleEventsWithAsset(Guid agencyId, List<Guid> eventIds)
        {
             var list = new List<ScheduleEvent>();
             if (eventIds != null && eventIds.Count > 0)
             {
                 var script = string.Format(@"SELECT 
                        scheduleevents.Id as Id ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.Asset as Asset 
                            FROM
                                scheduleevents
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.Id  IN ( {0})", eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));


                 using (var cmd = new FluentCommand<ScheduleEvent>(script))
                 {
                     list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ScheduleEvent
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         Asset = reader.GetStringNullable("Asset")
                     })
                     .AsList();
                 }
             }
            return list;
        }

        internal static bool UpdateScheduleEventsWithAsset<T>(Guid agencyId, Guid patientId, Guid episodeId, Guid Id, string asset) where T : class, new()
        {
            bool result = false;

            try
            {
                var script = string.Format(@"UPDATE scheduleevents set Asset = @asset WHERE AgencyId = @agencyid AND  PatientId = @patientid AND  EpisodeId = @episodeid AND  Id = @id ");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("id", Id)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("episodeid", episodeId)
                    .AddString("asset", asset)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }



        internal static List<T> GetAssessments<T>(Guid agencyId) where T : Assessment, new()
        {
            var list = new List<T>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `OasisData` from `{0}s` where agencyid = '{1}' and IsDeprecated = 0;", typeof(T).Name.ToLower(), agencyId);
            using (var cmd = new FluentCommand<T>(sql))
            {
                list = cmd.SetConnection("OasisCConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    //SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                    OasisData = reader.GetStringNullable("OasisData")
                })
                .AsList();
            }
            return list;
        }

        internal static List<T> GetAssessmentsNew<T>(Guid agencyId) where T : Assessment, new()
        {
            var list = new List<T>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `OasisData` from `assessments` where agencyid = '{0}' and IsDeprecated = 0;",  agencyId);
            using (var cmd = new FluentCommand<T>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    //SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
                    OasisData = reader.GetStringNullable("OasisData")
                })
                .AsList();
            }
            return list;
        }

        internal static List<T> GetAssessmentsNewWithSubmissionFormat<T>(Guid agencyId) where T : Assessment, new()
        {
            var list = new List<T>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `OasisData` from `assessments` where agencyid = '{0}' and IsDeprecated = 0;", agencyId);
            using (var cmd = new FluentCommand<T>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat")
                })
                .AsList();
            }
            return list;
        }


        internal static List<T> GetAssessmentsNewLean<T>(Guid agencyId) where T : Assessment, new()
        {
            var list = new List<T>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId` from `assessments` where agencyid = '{0}' and IsDeprecated = 0;", agencyId);
            using (var cmd = new FluentCommand<T>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId")
                })
                .AsList();
            }
            return list;
        }


        internal static Assessment GetAssessment<T>(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId) where T : Assessment, new()
        {
            return oasisDatabase.Single<T>(a => a.EpisodeId == episodeId && a.PatientId == patientId && a.Id == assessmentId && a.AgencyId == agencyId);
            //return oasisDatabase.Find<RecertificationAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool AddForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateMany<T>(IEnumerable<T> items) where T : class, new()
        {
            if (items != null)
            {
                agencyManagementDatabase.UpdateMany<T>(items);
                return true;
            }
            return false;
        }

        internal static bool UpdateForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForOasisC<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                oasisDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForOasisCNew<T>(Guid agencyId,Guid newAgencyId, List<Guid> oasisIds) where T : class, new()
        {
            bool result = false;
            if (oasisIds != null && oasisIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE assessments set AgencyId = @newagencyid , Modified = @modified WHERE AgencyId = @agencyid AND  Id IN ( {0})", oasisIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("newagencyid", newAgencyId)
                        .AddDateTime("modified", DateTime.Now)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        internal static bool UpdateForOasisCSubmissionFormatNew<T>(Guid agencyId, Guid patientId, Guid episodeId, Guid Id, string submissionFormat) where T : class, new()
        {
            bool result = false;

            try
            {
                var script = string.Format(@"UPDATE assessments set SubmissionFormat = @submissionformat  WHERE AgencyId = @agencyid AND  PatientId = @patientid AND  EpisodeId = @episodeid AND  Id = @id ");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("id", Id)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("episodeid", episodeId)
                    .AddString("submissionformat", submissionFormat)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }


        internal static List<CBSACode> GetCbsaCodes()
        {
            return lookupDatabase.All<CBSACode>().ToList();
        }

        internal static List<DisciplineTask> GetDisciplineTasks()
        {
            return lookupDatabase.All<DisciplineTask>().ToList();
        }

        internal static List<User> GetUsers()
        {
            return agencyManagementDatabase.All<User>().ToList();
        }

        internal static List<PatientEpisode> GetPatientEpisodes()
        {
            return agencyManagementDatabase.All<PatientEpisode>().ToList();
        }

        internal static void AddMany<T>(IEnumerable<T> items) where T : class, new()
        {
                agencyManagementDatabase.AddMany<T>(items);
        }

        internal static List<ManagedClaim> GetPaidManagedClaims(Guid agencyId)
        {
            var list = new List<ManagedClaim>();
            var sql = @"SELECT `Id`, `AgencyId`, `PatientId`, `Payment`, `PrimaryInsuranceId`, `PaymentDate` " +
                       "FROM managedclaims WHERE agencyid = @agencyid AND `Status` = 3030;";
            using (var cmd = new FluentCommand<ManagedClaim>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new ManagedClaim
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Payment = reader.GetDouble("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
                })
                .AsList();
            }
            return list;
        }

        internal static List<ManagedClaimPayment> GetManagedClaimPayments()
        {
            return agencyManagementDatabase.Find<ManagedClaimPayment>(payment => payment.IsDeprecated == false).ToList();
        }
    }
}
