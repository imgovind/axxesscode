﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    public class ZipEntry
    {
        public string Zip { get; set; }
        public string CBSA { get; set; }
        public string State { get; set; }
        public string StateFull { get; set; }
        public string City { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
            .AppendFormat("Zip: {0}{1}", this.Zip, Environment.NewLine)
            .AppendFormat("City: {0}{1}", this.City, Environment.NewLine)
            .AppendFormat("State: {0}{1}", this.State, Environment.NewLine)
            .AppendFormat("CBSA: {0}{1}", this.CBSA, Environment.NewLine)
            .AppendFormat("{0}", Environment.NewLine).ToString();
        }
    }
}
