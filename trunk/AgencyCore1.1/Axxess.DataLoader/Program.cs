﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using StructureMap;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;
    using Axxess.LookUp.Domain;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting");
            FlattenEpisodes.Run();
            //LoadPatientVisitNote.Run();
            //Console.WriteLine("Args 1: " + args[0]);
            //Console.WriteLine("Args 2: " + args[1]);
            //Console.WriteLine("Args 3: " + args[2]);
            //HeightVitalSignData.Run(args[0], args[1], args[2]);
            //HeightVitalSignData.Run("BloodPressure", "0", "50");
            //RepairUserLicenses.Run();
            //AgencyAddAdjustmentsScript.Run();
            //UpdatePaymentsScript.Run();

            //AllegenyIntake.Run(new Guid("f3c587a1-6303-4ebf-ac16-403ee70b9aa7"), new Guid("efe80c4b-cd75-45d7-98ef-8be524263bd7"));

            //UsingSimpleRepo();
            //UsingFluentAdoNet();

            //UsingSimpleRepo();
            //UsingFluentAdoNet();

            //UsingSimpleRepo();
            //UsingFluentAdoNet();

            //UsingSimpleRepo();
            //UsingFluentAdoNet();

            //UsingSimpleRepo();
            //UsingFluentAdoNet();

            //UsingSimpleRepo();
            //UsingFluentAdoNet();
            //Alura.Run("fc7c6c62-eee6-4186-84ba-25105ebca2f2", "20882903-4608-4c3f-993c-82d2d5132fbf");
            //AluraPhysician.Run(new Guid("fc7c6c62-eee6-4186-84ba-25105ebca2f2"));
            //IDatabaseAdministration databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=agencymanagement;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);
            ////AgencySuppliesLoad.Run(new Guid("173c62df-061b-434c-8bb8-bd2db4e8c98f"));
            //databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=oasisc;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);

            //databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=axxessmembership;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);

            //AssetScript.Run(new Guid("153e863d-669a-4edd-851a-4088890fd9a1"));
            //Wizard.Run(new Guid("d544542f-75da-4ade-9c84-5e4aa4d2fb59"), new Guid("411ac04a-3868-40b4-99a2-bb4d18021f03"));
            //WizardPhysician.Run(new Guid("2698b74b-7b41-4b29-b05c-4669c4863616"));
            //AssetScript.Run(new Guid("153e863d-669a-4edd-851a-4088890fd9a1"));
            //VestaScript.Run("565e9603-d887-407b-8404-ffe9a8b7b92f", "145bd87c-9e3e-47d9-8bb0-1130620d9422");
            //ColoradoScript.Run();
            //ArizonaScript.Run();
            //MinnesotaScript.Run();
            //CaliforniaHHAScript.Run();
            //SponsorScript.Run();
            //var stopWatch = new Stopwatch();
            //stopWatch.Start();
            //CradleMedPointScript.Run(new Guid("5d50ae56-5e62-46cf-9b7f-40f5ca8202b3"), new Guid("4bab3fe9-51dc-4e90-9262-32575e505349"));
            //KinnserScript.Run(new Guid("d6e8ef02-4ea3-4a19-8414-ef77a6481b63"), new Guid("87dc0418-74c5-4f8f-9333-e9e8fd4792ba"));
            //KinnserCsvScript.Run(new Guid("37d54330-f914-451a-937d-ebe3b1d19bf4"), new Guid("12c407d0-b0e5-4c9c-aab0-386c36e4057d"));
            //AxxessScript.Run("e102b6eb-c80f-4a34-a097-d7d48ebf91df", "1fb7e110-76f1-4bf7-ac2c-1f37c3a4b1f3");
            //MedicationProfileScript.Run(new Guid("6d80ae1f-410d-408f-b342-051e956b037f"));
            //GenericExcelScript.Run(new Guid("8bf20512-359a-4fe5-9438-966dfaa5a34c"), new Guid("8885e4f3-36ce-4a26-a08a-0b691e89ccbe"));
            //SynergyScript.Run(new Guid("855d3109-d693-4458-9ed7-ca44612502c2"), new Guid("cce3cbb8-2ddd-47a0-8828-5ee0852bfada"));
            //VisiTrakScript.Run(new Guid("68eb9075-5248-4573-a3de-e5f05f831643"), new Guid("68c960d1-67bc-40b7-b602-e81db51d9c48"));
            //VisiTrakScript2.Run(new Guid("3a5b8373-9e84-4247-8b7f-a317dd470ad6"), new Guid("0c146d98-8457-4948-adb8-8fe8bdd42581"));
            //VisiTrakPhysicianScript.Run(new Guid("a11e9aeb-74d6-4130-9976-d2587192f932"));
            //SynergyTwoScript.Run(new Guid("ce218bb3-7b0e-41e3-9842-59e9947d1000"), new Guid("2dd72a29-5b1c-4bb5-97fd-05eb99fc66c0"));
            //SynergyPhysicianScript.Run(new Guid("ce218bb3-7b0e-41e3-9842-59e9947d1000"));
            //SynergyTwoPhysicianScript.Run(new Guid("ce218bb3-7b0e-41e3-9842-59e9947d1000"));
            //HealthMedXPhysicianScript.Run(new Guid("abc2fbb2-807e-4dd0-987f-608e5d75558a"));
            //HealthCareFirstScript.Run(new Guid("93a811f1-ffda-43f9-a844-b2a0f9db81f4"), new Guid("9a41ef5a-f55c-4f98-b1de-84a094b91354"));
            //Icd9Script.Run(true);
            //Icd9Script.Run(false);
            //GenericExcelTwoScript.Run(new Guid("4a318889-50b7-4ba2-90d2-bb259a663411"), new Guid("e4556fd3-e9e9-42e3-a801-06ddd9da77ee"));
            //WageIndexScript.Run();
            //CradleCsvScript.Run("00e33641-c341-4d41-8741-dc3a5b3cfc50", "7e330511-4536-45b9-993b-3fc7c219d7f1");
            //OasisSubmissionIdScript.Run(new Guid("d2d2f784-b0dc-4399-82e8-2cfcebc77665"), new List<string> { "4000083" }, "TX4000083");
            //OasisSubmissionBranchIdScript.Run(new Guid("ea9c9ab1-d667-4c1d-8d74-1436344666a8"));
            //HHCenterScript.Run(new Guid("9d5afae5-6127-4c58-b457-c5185273ad4a"), new Guid("044ec64d-118e-43df-85f1-9c772a004c86"));
            //MjsOneScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"), new Guid("e8460caf-8a63-42b2-89e7-058bf9b8be15"));
            //MsjOnePhysicianScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"));
            //IgeaHomeHealthIntake.Run("5ccbf159-dc60-4b5a-a6aa-ade0ca60a1be", "80404746-3140-46c9-9ea8-4c68ee2bdb47");
            //PermissionScript.Run();
            //AgencySupplies.Run();
            //PhysicianOrders.Run();
            //AgencyMergeScript.Run("4da8bb1c-0b75-415c-862d-c36ff673619d", new List<string> { "949dfac1-8bdb-4097-868b-a7729cc991b4", "c65edc26-12be-43d1-b71c-5074d16213e8" });
            //AxxessScript4.Run(new Guid("91ddde23-b364-4c96-bb4b-84a25b8fa1de"), new Guid("77df1f10-3e78-4c94-a490-a1bbc91ad977"));
            //int i = PatinetAddressScript.Run(new Guid("0bd5ffc9-61c1-495a-9df5-7b93885ab30d"));
            //AxxessScript3.Run(new Guid("ef439ad6-2eff-4562-9ae3-a50449f64154"), new Guid("af4cc0bb-2f84-4f25-80ab-726022279243"));
            //AxxessScript2.Run(new Guid("2698b74b-7b41-4b29-b05c-4669c4863616"), new Guid("9f5be673-138f-408c-a17f-5415d8864ba8"));
           // SchduleLoadScript.Run(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //SchduleLoadScript.Run(new Guid("d0307ff6-9d69-435a-a411-f737b1980bfb"));
            //SchduleLoadScript.Run(new Guid("86493a0e-0fc0-4260-998b-442dd17f1a6e"));


           

            //LoadAgencyLocationChargeRates.Run();
           // LoadAgencyLocationChargeRates.RunToUpdateTheDisciplineTasks();
            Console.WriteLine("Script Complete");
            Console.ReadLine();
        }

        private static void UsingFluentAdoNet()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            using (var fluentCommand = new FluentCommand<Npi>("select * from npis limit 0, 50000"))
            {
                var npi = fluentCommand
                    .SetConnection("AxxessLookupConnectionString")
                    .AsList();
            }

            stopWatch.Stop();
            //Console.WriteLine("Finished @: {0}", DateTime.Now.ToString("MM/dd/yyyy @ hh:mm:ss tt"));
            Console.WriteLine("Fluent: {0} milliseconds", stopWatch.ElapsedMilliseconds);
        }

        private static void UsingSimpleRepo()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var simpleRepo = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);
            var npi = simpleRepo.GetPaged<Npi>(0, 50000);

            stopWatch.Stop();
            //Console.WriteLine("Finished @: {0}", DateTime.Now.ToString("MM/dd/yyyy @ hh:mm:ss tt"));
            Console.WriteLine("Simple Repo: {0} milliseconds", stopWatch.ElapsedMilliseconds);
        }
    }
}
