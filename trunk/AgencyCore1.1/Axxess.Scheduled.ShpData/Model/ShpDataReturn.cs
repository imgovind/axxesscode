﻿namespace Axxess.Scheduled.ShpData
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SHP_DATA_RETURN
    {
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public int RETURN_NUMBER { get; set; }

        [XmlElementAttribute("RETURN_MESSAGE")]
        public string[] RETURN_MESSAGE { get; set; }
    }
}
