﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Scheduled.ShpData.Model
{
    public class SubmissionHeaderFormat
    {
        public string Item { get; set; }
        public double Length { get; set; }
        public double Start { get; set; }
        public double End { get; set; }
        public string PadType { get; set; }
        public string DataType { get; set; }
        public string DefaultValue { get; set; }
    }
}
