﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Add", "Login", FormMethod.Post, new { @id = "newLoginForm" }))%>
<%  { %>
<div class="form-wrapper">
    <fieldset>
        <legend>Login Information</legend>
        <div class="column">
            <div class="row"><label for="New_Login_EmailAddress">E-mail Address:</label><div class="float-right"><%=Html.TextBox("EmailAddress", "", new { @id = "New_Login_EmailAddress", @class = "text required", @maxlength = "50" })%></div></div>
            <div class="row"><label for="New_Login_DisplayName">Display Name:</label><div class="float-right"><%=Html.TextBox("DisplayName", "", new { @id = "New_Login_DisplayName", @class = "text required names input_wrapper", @maxlength = "30" })%></div></div>
            <div class="row"><label for="New_Login_IsAxxessAdmin">Is Axxess Admin?</label><div class="float-right"><%= Html.CheckBox("IsAxxessAdmin", false, new { @id = "New_Login_IsAxxessAdmin" })%></div></div>
            <div class="row"><label for="New_Login_IsAxxessSupport">Is Axxess Support?</label><div class="float-right"><%= Html.CheckBox("IsAxxessSupport", false, new { @id = "New_Login_IsAxxessSupport" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('newlogin');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
