﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySnapShot>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Locations | ",
        Model.Name, "','listagencylocations');</script>")%>
<% var editLink = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditLocation('{0}','<#=Id#>');\">Edit</a>", Model.Id.ToString()); %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyLocation>().Name("List_Agency_Locations").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(l => l.Name).Title("Location Name").Width(150).Sortable(true);
    columns.Bound(l => l.MedicareProviderNumber).Title("Provider Number").Sortable(false).Width(110);
    columns.Bound(l => l.AddressFull).Title("Address").Sortable(true);
    columns.Bound(l => l.PhoneWorkFormatted).Title("Phone Number").Width(120);
    columns.Bound(l => l.FaxNumberFormatted).Title("Fax Number").Width(120);
    columns.Bound(l => l.Id).ClientTemplate(editLink).Title("Action").Width(60);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("LocationList", "Agency", new { agencyId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Agency_Locations .t-grid-toolbar").html("");
    $("#List_Agency_Locations .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewLocation('<%=Model.Id %>');%22%3ENew Location%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
