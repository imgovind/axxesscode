﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  var type = Model != null && Model.Id != Guid.Empty ? "Edit" : "New"; %>
<div class="column">
    <div class="row">
        <label for="<%=type %>_Location_TaxId">Tax Id:</label>
        <div class="float-right"><%=Html.TextBox("TaxId", Model.TaxId, new { @id = type + "_Location_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div>
    </div>
    <div class="row"><label for="<%=type %>_Location_TaxIdType">Tax Id Type:</label><div class="float-right"><%var taxIdTypes = new SelectList(new[] { new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" }, new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" }, new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" } }, "Value", "Text", Model.TaxIdType); %><%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = type + "_Location_TaxIdType", @class = "input_wrapper" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_Payor">Payor:</label><div class="float-right"><%var payors = new SelectList(new[] { new SelectListItem { Text = "** Select Payor **", Value = "0" }, new SelectListItem { Text = "Palmetto GBA", Value = "1" }, new SelectListItem { Text = "National Government Services (formerly known as United Government Services)", Value = "2" }, new SelectListItem { Text = "Blue Cross Blue Shield of Alabama (AKA Cahaba GBA)", Value = "3" }, new SelectListItem { Text = "Anthem Health Plans of Maine", Value = "4" } }, "Value", "Text", Model.Payor); %><%= Html.DropDownList("Payor", payors, new { @id = type + "_Location_Payor", @class = "input_wrapper" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_NationalProviderNo">National Provider Number:</label><div class="float-right"><%=Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = type + "_Location_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_MedicareProviderNo">Medicare Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = type + "_Location_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = type + "_Location_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>

   </div>
 <div class="column">
    <div class="row"><label for="<%=type %>_Location_HomeHealthAgencyId">Unique Agency OASIS ID Code:</label><div class="float-right"><%=Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = type + "_Location_HomeHealthAgencyId", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_CahpsVendor">CAHPS Vendor:</label><div class="float-right"><%= Html.CahpsVendors( "CahpsVendor", Model.CahpsVendor.ToString(), new { @id = type + "_Location_CahpsVendor", @class = "valid" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_ContactPersonFirstName">Contact Person First Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = type + "_Location_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_ContactPersonLastName">Contact Person Last Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = type + "_Location_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "30" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_ContactPersonEmail">Contact Person E-mail:</label><div class="float-right"><%=Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = type + "_Location_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "40" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_ContactPhone1">Contact Person Phone:</label><div class="float-right"><%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(0, 3) : "", new { @id = type + "_Location_ContactPhone1", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(3, 3) : "", new { @id = type + "_Location_ContactPhone2", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(6, 4) : "", new { @id = type + "_Location_ContactPhone3", @class = "input_wrappermultible autotext required numeric phone_long", @maxlength = "4", @size = "5" })%></div></div>
 </div>
<div class="float-left"><%=Html.CheckBox("IsAxxessTheBiller", Model.IsAxxessTheBiller, new { @id = type + "_Location_IsAxxessTheBiller", @class = "radio" })%>&nbsp;<label for="<%=type %>_Location_IsAxxessTheBiller">Check here if Axxess&trade; HealthCare Consult handles billing for this Agency.</label></div>
<div class="clear"></div>
<div id="<%=type %>_Location_SubmiterInfoContent">
<div class="column">
    <div class="row"><label for="<%=type %>_Location_SubmitterId">Submitter Id:</label><div class="float-right"><%=Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = type + "_Location_SubmitterId", @maxlength = "15", @class = "text input_wrapper required" })%></div></div>
    <div class="row"><label for="<%=type %>_Location_SubmitterName">Submitter Name:</label><div class="float-right"><%=Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = type + "_Location_SubmitterName", @maxlength = "50", @class = "text input_wrapper required" })%></div></div>
</div>
<div class="column">
    <div class="row"><label for="<%=type %>_Location_SubmitterPhone1">Submitter Phone Number:</label>
        <div class="float-right">
            <span class="input_wrappermultible">
                <%=Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 3 ? Model.SubmitterPhone.Substring(0, 3) : "", new { @id = type + "_Location_SubmitterPhone1", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
            </span>- <span class="input_wrappermultible">
                <%=Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 6 != null && Model.SubmitterPhone != "" ? Model.SubmitterPhone.Substring(3, 3) : "", new { @id = type + "_Location_SubmitterPhone2", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
            </span>- <span class="input_wrappermultible">
                <%=Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 10 != null && Model.SubmitterPhone != "" ? Model.SubmitterPhone.Substring(6, 4) : "", new { @id = type + "_Location_SubmitterPhone3", @class = "autotext required digits phone_long", @maxlength = "4" })%>
            </span>
        </div>
    </div>
    <div class="row"><label for="<%=type %>_Location_SubmitterFax1">Submitter Fax Number:</label>
        <div class="float-right">
            <span class="input_wrappermultible">
                <%=Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 3 ? Model.SubmitterFax.Substring(0, 3) : "", new { @id = type + "_Location_SubmitterFax1", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
            </span>- <span class="input_wrappermultible">
                <%=Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 6 != null && Model.SubmitterFax != "" ? Model.SubmitterFax.Substring(3, 3) : "", new { @id = type + "_Location_SubmitterFax2", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
            </span>- <span class="input_wrappermultible">
                <%=Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 10 != null && Model.SubmitterFax != "" ? Model.SubmitterFax.Substring(6, 4) : "", new { @id = type + "_Location_SubmitterFax3", @class = "autotext required digits phone_long", @maxlength = "4" })%>
            </span>
         </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $("#<%=type %>_Location_IsAxxessTheBiller").click(function() {
        if ($(this).is(':checked')) {
            $("#<%=type %>_Location_SubmiterInfoContent").hide();
            $("#<%=type %>_Location_SubmiterInfoContent input[type=text]").removeClass("required");
            $("#<%=type %>_Location_SubmiterInfoContent select").removeClass("requireddropdown");
        } else {
            $("#<%=type %>_Location_SubmiterInfoContent").show();
            $("#<%=type %>_Location_SubmiterInfoContent input[type=text]").addClass("required");
            $("#<%=type %>_Location_SubmiterInfoContent select").addClass("requireddropdown");
        }
    })
</script>