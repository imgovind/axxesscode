﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  using (Html.BeginForm("Update", "Location", FormMethod.Post, new { @id = "editLocationForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Location_Id" })%>
<%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Edit_Location_AgencyId" })%>
<%= string.Format("{0}{1}{2}", "<script type='text/javascript'>acore.renamewindow('Edit Location | ", Model != null ? Model.Name.ToTitleCase() : "", "','editlocation');</script>")%>
<div class="wrapper main">
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_Name" class="float-left">Name:</label><div class="float-right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Location_Name", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
        </div>
         <div class="column">               
             <div class="row"><label for="Edit_Location_CustomId" class="float-left">Custom ID:</label><div class="float-right"><%=Html.TextBox("CustomId", Model.CustomId, new { @id = "Edit_Location_CustomId", @class = "text input_wrapper" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
       <legend>Stand Alone branch Information </legend>
       <div class="float-left"><%=Html.CheckBox("IsLocationStandAlone", Model.IsLocationStandAlone, new { @id = "Edit_Location_IsLocationStandAlone", @class = "radio" })%>&nbsp;<label for="Edit_Location_IsLocationStandAlone">Check here if this branch office Stand Alone.</label></div>
       <div class="clear"></div>
       <div id="Edit_Location_StandAloneInfoContent"><% if (Model.IsLocationStandAlone) { Html.RenderPartial("/Views/Agency/Location/StandAloneInfo.ascx", Model); }%></div>
    </fieldset>
    <fieldset>
       <legend>Submitter Information (Medicare)</legend>
      <div class="wide-column"><div class="row"><label for="Edit_Location_BranchID" class="float-left">(M0016)Branch ID Number(For OASIS Submission):</label><div class="float-left"><%var branchId = new SelectList(new[] { new SelectListItem { Text = "-- Select Branch Id --", Value = "0" }, new SelectListItem { Text = "N", Value = "N" }, new SelectListItem { Text = "P", Value = "P" }, new SelectListItem { Text = "Other", Value = "Other" }}, "Value", "Text", Model.BranchId.IsNotNullOrEmpty()? Model.BranchId: "0"); %><%= Html.DropDownList("BranchId", branchId, new { @id = "Edit_Location_BranchId", @class = "requireddropdown" })%></div><%=Html.TextBox("BranchIdOther", Model.BranchIdOther, new { @id = "Edit_Location_BranchIdOther", @class = string.Format("text input_wrapper {0}",Model.BranchId=="Other"? "":"hidden") })%> </div></div>
    </fieldset> 
    <fieldset>
        <legend>Location Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Location_AddressLine1", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Location_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressCity" class="float-left">City:</label><div class="float-right"><%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Location_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressStateCode" class="float-left">State:</label><div class="float-right"><%= Html.States("AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Location_AddressStateCode", @class = "AddressStateCode required valid" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressZipCode" class="float-left">Zip:</label><div class="float-right"><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%>&nbsp;-&nbsp;<%= Html.TextBox("AddressZipCodeFour", Model.AddressZipCodeFour, new { @id = "Edit_Location_AddressZipCodeFour", @class = "text required digits isValidUSZip zip-small", @maxlength = "4" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Location_PhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"><%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty()&& Model.PhoneWork.Length>=3 ? Model.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Location_PhoneArray1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length >= 6 ? Model.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Location_PhoneArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length >= 10 ? Model.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Location_PhoneArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Location_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 3 ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Location_FaxNumberArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 6 ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Location_FaxNumberArray2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 10 ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Location_FaxNumberArray3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        <table class="form"><tbody>
            <tr class="line-seperated vert">
                <td><label for="Edit_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", Model.Comments, new { @id = "Edit_Location_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editlocation');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $("#Edit_Location_IsLocationStandAlone").change(function() {
        if ($(this).is(':checked')) Agency.LoadLocationStandAloneContent("#Edit_Location_StandAloneInfoContent", "<%=Model.AgencyId%>", "<%= Model.Id %>");
        else $("#Edit_Location_StandAloneInfoContent").empty();
    });
    $("#Edit_Location_BranchId").change(function() {
        if ($(this).val() == 'Other') $("#Edit_Location_BranchIdOther").removeClass("hidden");
        else $("#Edit_Location_BranchIdOther").removeClass("hidden").addClass("hidden");
    });
</script>
