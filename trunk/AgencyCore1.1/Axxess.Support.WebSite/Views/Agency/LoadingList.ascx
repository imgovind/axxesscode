﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencySnapShot>>" %>
<% string pagename = "LoadingList"; %>
<div class="wrapper">
  <% using (Html.BeginForm("Load", "Agency", FormMethod.Post, new { @id = "dataLoaderForm" }))
     { %>

   <fieldset >
      <div class="column">
            <table style="padding-top:0px;">
                   <thead>
                     <tr>
                        <th><label class="float-left">Select Database Move Type</label></th>
                        <th></th>
                     </tr>
                   </thead>
                 <tbody>
                 <tr>
                    <td> <%= Html.RadioButton("ActionType", "1", false, new { @id = pagename + "_MoveType1", @class = "required radio" })%>
                      <label for="<%= pagename%>_MoveType" class="inline-radio">Schedule Flat And OASIS Agg.</label>
                      </td>
                    <td>
                     <%= Html.TablesToMove("TablesToMove", true, "** Select Tables **", "", new { @id = pagename + "_TablesToMove", @class = "requireddropdown " })%>
                    </td>
                 </tr>
                 <tr>
                     <td> <%= Html.RadioButton("ActionType", "2", false, new { @id = pagename + "_MoveType2", @class = "required radio" })%>
                      <label for="<%= pagename%>_MoveType" class="inline-radio">Data Table Move</label>
                      </td>
                    <td> 
                    <%= Html.Tables("Tables", true, "** Select Tables **", "", new { @id = pagename + "_Tables", @class = "requireddropdown " })%>
                     </td>
                  </tr>
                  <tr>
                      <td>
                       <div class="float-left"> <%= Html.CheckBox("IsAll", false, new { @id = pagename +"_SelectAll", @class = "radio float-left" })%><label for="<%=pagename %>_SelectAll" >Check/Uncheck All</label></div>
                      </td>
                      <td>
                    
                      </td>
                  </tr>
                </tbody>
           </table>
      </div>
      <div class="column">
          <label class="strong">From :</label><%= Html.DbServers("FromServerIp", "", new { @id = pagename + "_FromServerIp", @class = "requireddropdown " })%>
          <label class="strong">To :</label><%= Html.DbServers("ToServerIp", "", new { @id = pagename + "_ToServerIp", @class = "requireddropdown" })%><div class="clear"></div>
          <div >
            <div id="<%= pagename %>_Search" class="float-left"></div>
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Submit</a></li>
                </ul>
                </div>
           </div>
      </div>
      
   </fieldset>
  

  
<div id="<%=pagename %>_GridContainer">
    
        <% Html.Telerik()
            .Grid<AgencySnapShot>(Model)
            .Name(pagename+"_Grid").HtmlAttributes(new { @style = "top:120px;" })
             .Columns(columns => 
                {
                     columns.Bound(o => o.Id).Template(t =>
                { %><%= string.Format("<input name=\"AgencyIds\" type=\"checkbox\" value=\"{0}\" />", t.Id)%><% }).Title("").Width(30).HtmlAttributes(new { style = "text-align:center" }).Sortable(false);
                 columns.Bound(a => a.Name).Title("Name").Sortable(true);
                 })
                                .Sortable()
                                                                                                                                                         .Scrollable(scrolling => scrolling.Enabled(true)).Footer(false).Render();
        %>
        </div>
 
  <%} %>
    </div>

<script type="text/javascript">
    $("#<%=pagename %>_Tables").multiSelect({
        selectAll: false,
        noneSelected: '- Select Table(s) -',
        oneOrMoreSelected: '*'
    });
    $("#<%=pagename %>_TablesToMove").multiSelect({
        selectAll: false,
        noneSelected: '- Select Table(s) -',
        oneOrMoreSelected: '*'
    });
    $("#<%=pagename %>_SelectAll").click(function() { $("#<%=pagename %>_GridContainer input[name='AgencyIds']").attr('checked', this.checked); });
    $("#<%=pagename %>_Grid .t-grid-content").css({ 'height': 'auto' });
    $("input:radio[name=ActionType]").click(function() {
        if ($(this).val() == 1) {
            $("#<%=pagename %>_TablesToMove").addClass("requireddropdown"); $("#<%=pagename %>_Tables").removeClass("requireddropdown");
        }
        else if ($(this).val() == 2) {
            $("#<%=pagename %>_TablesToMove").removeClass("requireddropdown"); $("#<%=pagename %>_Tables").addClass("requireddropdown");
        }
    });
    $("#<%= pagename %>_Search").append($("<div/>").addClass("no-left-margin").GridSearchById("#<%= pagename %>_Grid"));
  
</script>