﻿
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%  using (Html.BeginForm("Update", "Agency", FormMethod.Post, new { @id = "editAgencyForm" })) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Agency | ",
        Model != null ? Model.Name.ToTitleCase() : "",
        "','editagency');</script>")%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Agency_Id" }) %>
<div class="form-wrapper">
    <fieldset>
        <legend>Trial Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Agency_IsAgreementSignedYes">Agreement Signed?</label>
                <div class="float-right">
                    <%=Html.RadioButton("IsAgreementSigned", "true", Model.IsAgreementSigned ? true : false, new { @id = "Edit_Agency_IsAgreementSignedYes", @class = "required radio" })%><label for="" class="inline-radio">Yes</label><%=Html.RadioButton("IsAgreementSigned", "false", Model.IsAgreementSigned == false ? true : false, new { @id = "Edit_Agency_IsAgreementSignedNo", @class = "required radio" })%><label for="Edit_Agency_IsAgreementSignedNo" class="inline-radio">No</label>
                </div>
            </div><div class="row">
                <label for="Edit_Agency_TrialPeriod">Trial Period:</label>
                <div class="float-right">
                    <% var trialPeriod = new SelectList(new[] {
                       new SelectListItem { Text = "-- Select Trial Period --", Value = "" },
                       new SelectListItem { Text = "30 Days", Value = "30" },
                       new SelectListItem { Text = "60 Days", Value = "60" },
                       new SelectListItem { Text = "90 Days", Value = "90" }
                       }, "Value", "Text", Model.TrialPeriod); %>
                    <%= Html.DropDownList("TrialPeriod", trialPeriod, new { @id = "Edit_Agency_TrialPeriod", @class = "input_wrapper" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row"><label for="Edit_Agency_Package">Package:</label>
                <div class="float-right">
                    <% var package = new SelectList(new[] {
                       new SelectListItem { Text = "Bronze (1 to 5 users)", Value = "5" },
                       new SelectListItem { Text = "Silver (6 to 10 users)", Value = "10" },
                       new SelectListItem { Text = "Gold (11 to 20 users)", Value = "20" },
                       new SelectListItem { Text = "Platinum (21 to 40 users)", Value = "40" },
                       new SelectListItem { Text = "Titanium (41 to unlimited users)", Value = "0" }
                       }, "Value", "Text", Model.Package); %>
                    <%= Html.DropDownList("Package", package, new { @id = "Edit_Agency_Package", @class = "input_wrapper", @style="width: 235px;" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Audit Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Agency_SalesPerson">Sales Person:</label>
                <div class="float-right"><%= Html.Users("SalesPerson", Model.SalesPerson.ToString(), new { @id = "Edit_Agency_SalesPerson", @class = "" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Agency_SalesPerson">Trainer:</label>
                <div class="float-right"><%= Html.Users("Trainer", Model.Trainer.ToString(), new { @id = "Edit_Agency_Trainer", @class = "" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Agency_CompanyName">Company Name:</label><div class="float-right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Agency_CompanyName", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_TaxId">Tax Id:</label><div class="float-right"><%=Html.TextBox("TaxId", Model.TaxId, new { @id = "Edit_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_TaxIdType">Tax Id Type:</label><div class="float-right"><%var taxIdTypes = new SelectList(new[] { new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" }, new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" }, new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" } }, "Value", "Text", Model.TaxIdType); %><%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = "Edit_Agency_TaxIdType", @class = "input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_Payor">Payor:</label><div class="float-right"><%var payors = new SelectList(new[] { new SelectListItem { Text = "** Select Payor **", Value = "0" }, new SelectListItem { Text = "Palmetto GBA", Value = "1" }, new SelectListItem { Text = "National Government Services", Value = "2" }, new SelectListItem { Text = "Cigna Government Services", Value = "3" }, new SelectListItem { Text = "Anthem Health Plans of Maine", Value = "4" } }, "Value", "Text", Model.Payor); %><%= Html.DropDownList("Payor", payors, new { @id = "Edit_Agency_Payor", @class = "input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonEmail">Contact Person E-mail:</label><div class="float-right"><%=Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = "Edit_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPhone1">Contact Person Phone:</label><div class="float-right"><%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(0, 3) : "", new { @id = "Edit_Agency_ContactPhone1", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(3, 3) : "", new { @id = "Edit_Agency_ContactPhone2", @class = "input_wrappermultible autotext required numeric phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(6, 4) : "", new { @id = "Edit_Agency_ContactPhone3", @class = "input_wrappermultible autotext required numeric phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Agency_CahpsVendor">CAHPS Vendor:</label><div class="float-right"><%= Html.CahpsVendors("CahpsVendor", "", new { @id = "Edit_Agency_CahpsVendor", @class = "valid" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Agency_NationalProviderNo">National Provider Number:</label><div class="float-right"><%=Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = "Edit_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_MedicareProviderNo">Medicare Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = "Edit_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = "Edit_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_HomeHealthAgencyId">Unique Agency OASIS ID Code:</label><div class="float-right"><%=Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = "Edit_Agency_HomeHealthAgencyId", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
          
            <div class="row"><label for="Edit_Agency_ContactPersonFirstName">Contact Person First Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_ContactPersonLastName">Contact Person Last Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "30" })%></div></div>
        </div>
    </fieldset>
     <fieldset>
        <legend>Submitter Information (Medicare)</legend>
        <div class="float-left"><%=Html.CheckBox("IsAxxessTheBiller", Model.IsAxxessTheBiller, new { @id = "Edit_Agency_AxxessBiller", @class = "radio" })%>&nbsp;<label for="Edit_Agency_AxxessBiller">Check here if Axxess&trade; HealthCare Consult handles billing for this Agency.</label></div>
        <div class="clear"></div>
        <div class="column">
            <div class="row"><label for="Edit_Agency_SubmitterId">Submitter Id:</label><div class="float-right"><%=Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "Edit_Agency_SubmitterId", @maxlength = "15", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Agency_SubmitterName">Submitter Name:</label><div class="float-right"><%=Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "Edit_Agency_SubmitterName", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Agency_SubmitterPhone1">Submitter Phone Number:</label>
                <div class="float-right">
                    <span class="input_wrappermultible">
                        <%=Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 3 ? Model.SubmitterPhone.Substring(0, 3) : "", new { @id = "Edit_Agency_SubmitterPhone1", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
                    </span>- <span class="input_wrappermultible">
                        <%=Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 6 != null && Model.SubmitterPhone != "" ? Model.SubmitterPhone.Substring(3, 3) : "", new { @id = "Edit_Agency_SubmitterPhone2", @class = "autotext numeric phone_short required", @maxlength = "3"})%>
                    </span>- <span class="input_wrappermultible">
                        <%=Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 10 != null && Model.SubmitterPhone != "" ? Model.SubmitterPhone.Substring(6, 4) : "", new { @id = "Edit_Agency_SubmitterPhone3", @class = "autotext required digits phone_long", @maxlength = "4" })%>
                    </span>
                </div>
            </div>
            <div class="row"><label for="Edit_Agency_SubmitterFax1">Submitter Fax Number:</label>
                <div class="float-right">
                        <span class="input_wrappermultible">
                            <%=Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 3 ? Model.SubmitterFax.Substring(0, 3) : "", new { @id = "Edit_Agency_SubmitterFax1", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
                        </span>- <span class="input_wrappermultible">
                            <%=Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 6 != null && Model.SubmitterFax != "" ? Model.SubmitterFax.Substring(3, 3) : "", new { @id = "Edit_Agency_SubmitterFax2", @class = "autotext numeric phone_short required", @maxlength = "3" })%>
                        </span>- <span class="input_wrappermultible">
                            <%=Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 10 != null && Model.SubmitterFax != "" ? Model.SubmitterFax.Substring(6, 4) : "", new { @id = "Edit_Agency_SubmitterFax3", @class = "autotext required digits phone_long", @maxlength = "4" })%>
                        </span>
                 </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('editagency');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    if ($("#Edit_Agency_IsAgreementSignedYes").attr('checked') == true) $("#Edit_Agency_TrialPeriod").val("").attr("disabled", "disabled");
    else $("#Edit_Agency_TrialPeriod").removeAttr("disabled");
</script>
<%} %>
