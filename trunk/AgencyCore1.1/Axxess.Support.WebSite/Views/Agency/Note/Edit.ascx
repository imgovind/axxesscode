﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CustomerNote>" %>
<% using (Html.BeginForm("UpdateNote", "Agency", FormMethod.Post, new { @id = "editNoteForm" })) {%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Note_Id" }) %>
<%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Edit_Note_AgencyId" }) %>
<div class="wrapper main">
    <fieldset>
        <legend>Details</legend>
        <div class="column"><div class="row"><label class="float-left">Customer Support Representative:</label><div class="float-right"><span><%= Model.RepName %></span></div></div></div>   
        <div class="column"><div class="row"><label class="float-left">Date & Time of Call:</label><div class="float-right"><%= Model.Created.ToString("MM/dd/yyy hh:mm:ss tt") %></div></div></div>
    </fieldset> 
    <fieldset>
        <legend>Comments</legend>
        <table class="form"><tbody><tr><td><div><%= Html.TextArea("Comments", Model.Comments, new { @id = "Edit_Note_Comments", @class="required", @style="height: 300px;" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editnote');">Close</a></li>
    </ul></div>
</div>
<% } %>
