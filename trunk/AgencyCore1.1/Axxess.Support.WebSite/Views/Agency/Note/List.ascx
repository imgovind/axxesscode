﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySnapShot>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Notes | ",
        Model.Name, "','listagencynotes');</script>")%>
        <% var template = Current.IsAxxessAdmin ? "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditNote('<#=AgencyId#>','<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Note.View('<#=AgencyId#>','<#=Id#>');\">View</a> | <a href=\"javascript:void(0);\" onclick=\"Note.Delete('<#=AgencyId#>','<#=Id#>');\">Delete</a>" : "<a href=\"javascript:void(0);\" onclick=\"Note.View('<#=AgencyId#>','<#=Id#>');\">View</a>"; %>
<div class="wrapper">
    <%= Html.Telerik().Grid<CustomerNote>().Name("List_CustomerNotes").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.RepName).Title("Rep Name").Width(180).Sortable(true);
    columns.Bound(c => c.Preview).Title("Preview").Sortable(true);
    columns.Bound(c => c.Created).Width(150).Format("{0:MM/dd/yyyy hh:mm tt}").Title("Time").Sortable(false);
    columns.Bound(c => c.Id).Title("Action").Sortable(false).Width(130).ClientTemplate(template);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("NoteList", "Agency", new { agencyId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_CustomerNotes .t-grid-toolbar").html("");
    $("#List_CustomerNotes .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float-left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewNote('<%=Model.Id %>'); return false;%22%3ENew Note%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
</script>