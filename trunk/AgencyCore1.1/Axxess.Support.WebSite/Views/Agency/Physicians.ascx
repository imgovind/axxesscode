﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySnapShot>" %>
<%= string.Format("<script type='text/javascript'>acore.renamewindow('Agency Physicians | {0}','listagencyphysicians');</script>", Model.Name)%>
<div class="wrapper">
    <%= Html.Telerik().Grid<PhysicianSelection>().Name("List_Agencies_Physicians").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.DisplayName).Title("Name");
    columns.Bound(u => u.EmailAddress).ClientTemplate("<a href=\"mailto:<#=EmailAddress#>\" \"><#=EmailAddress#></a>");
    columns.Bound(u => u.IsLoginActive).Title("Active").Sortable(false).Width(80);
    columns.Bound(u => u.LoginId).Sortable(false).ClientTemplate(string.Format("<a href='/Agency/ImpersonatePhysician/<#=LoginId#>' target='_blank'>Login As</a> | <a href='javascript:void(0);' onclick=\"UserInterface.ResendActivationLink('<#=LoginId#>','{0}');\">Activate Link</a>", Model.Id)).Title("Action").Width(170);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("PhysicianList", "Agency", new { agencyId = Model.Id })).Pageable(paging => paging.PageSize(500)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Agencies_Physicians .t-grid-toolbar").html(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3E%3Clabel for=%22Physician_FilterBox%22%3ESearch:&nbsp;%3C/label%3E%3Cinput type=%22text%22 id=%22Physician_FilterBox%22 /%3E%3C/div%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
    $("#Physician_FilterBox").keyup(function() {
        if ($(this).val()) {
            FilterTGrid($(this).val())
            if ($("tr.match:even", "#List_Agencies_Physicians .t-grid-content").length) $("tr.match:first", "#List_Agencies_Physicians .t-grid-content").click();
        } else {
            $("tr", "#List_Agencies_Physicians .t-grid-content").removeClass("match t-alt").show();
            $("tr:even", "#List_Agencies_Physicians .t-grid-content").addClass("t-alt");
            $("tr:first", "#List_Agencies_Physicians .t-grid-content").click();
        }
    });
    function FilterTGrid(text) {
        search = text.split(" ");
        $("tr", "#List_Agencies_Physicians .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#List_Agencies_Physicians .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#List_Agencies_Physicians .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#List_Agencies_Physicians .t-grid-content").addClass("t-alt");
    }
</script>