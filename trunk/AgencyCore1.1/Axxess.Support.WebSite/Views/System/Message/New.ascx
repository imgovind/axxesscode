﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="messaging-container" class="wrapper">
<%  using (Html.BeginForm("AddMessage", "System", FormMethod.Post, new { @id = "new-system-message-form" })) { %>
    <div class="new-message-content" id="new-message-content">
        <div class="inbox-subheader">
            <span>New System Message</span>
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newsystemmessage');">Cancel</a></li>
                </ul>
            </div>
        </div>
        <div class="buttons float-left">
            <ul>
                <li class="send"><a href="javascript:void(0);" onclick="$(this).closest('form').submit();"><span class="img icon send"></span><br />Send</a></li>
            </ul>
        </div>
        <div class="new-message-content-panel clear">
            <div class="new-message-row">
                <label for="New_SystemMessage_Subject">Subject:</label>
                <input type="text" id="New_SystemMessage_Subject" name="Subject" maxlength="150" />
            </div>
            <div class="new-message-row">
                <label for="New_SystemMessage_CreatedBy">Created By:</label>
                <span id="New_SystemMessage_CreatedBy"><%= Current.DisplayName %></span>
            </div>
            <div class="new-message-row">
                <label for="New_SystemMessage_CreatedDate">Created By:</label>
                <span id="New_SystemMessage_CreatedDate"><%= DateTime.Now.ToShortDateString() %></span>
            </div>
        </div>
        <div class="new-message-row" id="new-message-body-div">
            <div>
                <textarea id="NewSystemMessageBody" name="Body"></textarea>
            </div>
        </div>
    </div>
<%  } %>
</div>
