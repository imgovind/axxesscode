﻿namespace Axxess.Membership
{
    public enum SessionKey
    {
        UserSession,
        AdministratorSession
    }
}
