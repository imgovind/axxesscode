﻿namespace Axxess.Membership.Enums
{
    public enum Categories
    {
        Caching,
        Logging,
        Security,
        Session,
        MetaData
    }
}
