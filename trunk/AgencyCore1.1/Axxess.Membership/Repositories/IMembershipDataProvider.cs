﻿namespace Axxess.Membership.Repositories
{
    public interface IMembershipDataProvider
    {
        IHostRepository HostRepository { get; }
        //IAuditRepository AuditRepository { get; }
        IErrorRepository ErrorRepository { get; }
        ILoginRepository LoginRepository { get; }
        ISupportRepository SupportRepository { get; }
        IApplicationRepository ApplicationRepository { get; }

        //IApplicationRepository AppRepository { get; }
    }
}
