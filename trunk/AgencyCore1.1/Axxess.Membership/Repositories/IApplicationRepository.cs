﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IApplicationRepository
    {
        //Guid Add(string name);
        //bool Delete(Guid applicationId);
        //bool Update(Guid id, string name);

        //Application Find(Guid id);
        //Application Find(string applicationName);
        //ICollection<Application> GetAll();
        //ICollection<Application> GetUserApps(Guid userId);
        //ICollection<Application> GetApplicationEntryOnTimeInterval(DateTime startDate, DateTime endDate);
        //ICollection<Application> GetAppliactionsModifiedInTimeInterval(DateTime startDate, DateTime endDate);
        bool AddTherapyEmployors(TherapyEmployors therapyEmployors);
        bool UpdateTherapyEmployors(TherapyEmployors therpyEmployors);
        List<TherapyEmployors> GetTherapyEmployorsByTherapyId(Guid therapyId);
        List<TherapyEmployors> GetTherapyEmployorsByStateAndStatus(string state, bool status, Guid therapyId);
    }
}
