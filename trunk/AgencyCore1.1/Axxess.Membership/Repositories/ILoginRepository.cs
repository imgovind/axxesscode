﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Infrastructure;

    public interface ILoginRepository
    {
        Login Find(Guid id);
        bool Add(Login login);
        bool Delete(Guid loginId);
        ICollection<Login> GetAll();
        List<Login> GetLogins(List<Guid> Ids);
        Login Find(string emailAddress);
        ICollection<Login> AutoComplete(string searchString);
        bool Update(Login login);
        bool Remove(Guid loginId);

        bool AddLoginAgencyUser(Guid agencyId, Guid userId, Guid loginId);
        string GetLoginDisplayName(Guid loginId);
        string GetLoginEmailAddress(Guid loginId);

        IList<Login> GetAllByRole(Roles role);
        UserLoginAgency GetLoginAgencyUser(Guid agencyId, Guid userId);
        IList<UserLoginAgency> GetAllLoginAgencyUsers(Guid loginId);
        AgencySnapShot GetAgencySnapShot(Guid agencyId);
        IList<AgencySnapShot> GetAgencySnapShots();
        IList<AgencySnapShot> GetAgencySnapShots(List<Guid> agencyIds);

    }
}
