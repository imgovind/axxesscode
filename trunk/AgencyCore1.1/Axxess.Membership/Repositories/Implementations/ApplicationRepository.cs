﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;

    public class ApplicationRepository : IApplicationRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        //private readonly string databaseIp;

        public ApplicationRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IApplicationRepository Members

        //public Guid Add(string name)
        //{
        //    Check.Argument.IsNotEmpty(name, "name");

        //    Application newApp = new Application()
        //    {
        //        Id = Guid.NewGuid(),
        //        Name = name,
        //        Created = DateTime.Now,
        //        Modified = DateTime.Now
        //    };
        //    db.Insert<Application>(newApp);
        //    db.SubmitChanges();
        //    return newApp.Id;
        //}
        
        //public bool Update(Guid id, string name)
        //{
        //    Check.Argument.IsNotEmpty(id, "id");
        //    Check.Argument.IsNotEmpty(name, "name");

        //    bool result = false;

        //    Application app = db.ApplicationsDataSource.SingleOrDefault(a => a.Id == id);

        //    if (app != null)
        //    {
        //        app.Name = name;
        //        app.Modified = DateTime.Now;
        //        db.SubmitChanges();
        //        result = true;
        //    }

        //    return result;
        //}

        //public bool Delete(Guid applicationId)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");

        //    bool result = false;
        //    Application app = db.ApplicationsDataSource.SingleOrDefault(a => a.Id == applicationId);
        //    if (app != null)
        //    {
        //        db.DeleteAll(db.UserApplicationsDataSource.Where(ua => ua.ApplicationId == app.Id));
        //        db.DeleteAll(db.SettingsDataSource.Where(s => s.ApplicationId == app.Id));
        //        db.DeleteAll(db.AuditsDataSource.Where(a => a.ApplicationId == app.Id));
        //        db.DeleteAll(db.ErrorsDataSource.Where(e => e.Host.ApplicationId == app.Id));
        //        db.DeleteAll(db.HostsDataSource.Where(h => h.ApplicationId == app.Id));
        //        db.Delete(app);
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public IApplication Find(Guid id)
        //{
        //    Check.Argument.IsNotEmpty(id, "id");

        //    return db.ApplicationsDataSource
        //        .SingleOrDefault(a => a.Id == id);
        //}

        //public IApplication Find(string applicationName)
        //{
        //    Check.Argument.IsNotNull(applicationName, "applicationName");

        //    return db.ApplicationsDataSource
        //        .SingleOrDefault(a => a.Name == applicationName);
        //}

        //public ICollection<IApplication> GetApplicationEntryOnTimeInterval(DateTime startDate, DateTime endDate)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");

        //    return db.ApplicationsDataSource
        //        .Where(a => (a.Created >= startDate && a.Created <= endDate))
        //        .OrderBy(a => a.Created)
        //        .Cast<IApplication>()
        //        .ToList();
        //}

        //public ICollection<IApplication> GetAppliactionsModifiedInTimeInterval(DateTime startDate, DateTime endDate)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");
        //    return db.ApplicationsDataSource
        //        .Where(a => (a.Created >= startDate && a.Created <= endDate))
        //        .OrderBy(a => a.Created)
        //        .Cast<IApplication>()
        //        .ToList();
        //}

        //public ICollection<IApplication> GetAll()
        //{
        //    return db.ApplicationsDataSource
        //        .Cast<IApplication>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<IApplication> GetUserApps(Guid userId)
        //{
        //    Check.Argument.IsNotEmpty(userId, "userId");

        //    var query = from app in db.ApplicationsDataSource
        //                join userApps in db.UserApplicationsDataSource
        //                on app.Id equals userApps.ApplicationId
        //                where userApps.UserId == userId
        //                select app;

        //    return query.Cast<IApplication>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        #endregion

        #region Methods
        public bool AddTherapyEmployors(TherapyEmployors therapyEmployors)
        {
            if (therapyEmployors != null)
            {
                therapyEmployors.Id = new Guid();
                therapyEmployors.Created = DateTime.Now;
                database.Add<TherapyEmployors>(therapyEmployors);
                return true;
            }
            return false;
        }

        public bool UpdateTherapyEmployors(TherapyEmployors therpyEmployors)
        {
            if (therpyEmployors != null)
            {
                database.Update<TherapyEmployors>(therpyEmployors);
                return true;
            }
            return false;
        }

        public List<TherapyEmployors> GetTherapyEmployorsByTherapyId(Guid therapyId)
        {
            
            //return database.Find<TherapyEmployors>(t => t.TherapyId == therapyId && t.IsDeprecated == false).ToList();
            var query = new QueryBuilder("select therapyemployors.TherapyId, therapyemployors.AgencyId, therapyemployors.AgencyName, therapyemployors.Comments from therapyemployors");
            query.Where("therapyemployors.TherapyId=@therapyId").And("therapyemployors.IsDeprecated=0");
            return new FluentCommand<TherapyEmployors>(query.Build())
            .SetConnection("AxxessMembershipConnectionString")
            .AddGuid("therapyId", therapyId)
            .SetMap(reader => new TherapyEmployors
            {
                TherapyId=reader.GetGuid("TherapyId"),
                AgencyId=reader.GetGuid("AgencyId"),
                AgencyName=reader.GetString("AgencyName"),
                Comments=reader.GetString("Comments")
            })
            .AsList();
        }

        public List<TherapyEmployors> GetTherapyEmployorsByStateAndStatus(string state, bool status, Guid therapyId)
        {
            var query = @"select therapyemployors.TherapyId, therapyemployors.AgencyId, therapyemployors.AgencyName from therapyemployors"+
            " Where therapyemployors.IsDeprecated=@status and therapyemployors.therapyId=@therapyId";
            if (state.Trim() != "")
            {
                query += " AND therapyemployors.state=@state";
            }
            var list = new List<TherapyEmployors>();
            using (var cmd = new FluentCommand<TherapyEmployors>(query))
            {
                list = cmd.SetConnection("AxxessMembershipConnectionString")
                .AddString("state", state)
                .AddBoolean("status", status)
                .AddGuid("therapyId",therapyId)
                .SetMap(reader => new TherapyEmployors
                {
                    TherapyId = reader.GetGuid("TherapyId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    AgencyName = reader.GetString("AgencyName")
                })
                .AsList();
            }
            return list;
        }
        #endregion
    }
}
