﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class SupportRepository : ISupportRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public SupportRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ISupportRepository Members

        public bool AddImpersonationLink(ImpersonationLink link)
        {
            if (link != null)
            {
                link.Id = Guid.NewGuid();
                link.Created = DateTime.Now;
                database.Add<ImpersonationLink>(link);
                return true;
            }
            return false;
        }

        public ImpersonationLink GetImpersonationLink(Guid linkId)
        {
            return database.Single<ImpersonationLink>(l => l.Id == linkId);
        }

        public bool UpdateImpersonationLink(ImpersonationLink link)
        {
            if (link != null)
            {
                database.Update<ImpersonationLink>(link);
                return true;
            }
            return false;
        }

        #endregion
    }
}
