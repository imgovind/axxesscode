﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using SubSonic.Repository;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

   

    public class LoginRepository : ILoginRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LoginRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ILoginRepository Members

        public bool Add(Login login)
        {
            if (login != null)
            {
                login.Id = Guid.NewGuid();
                login.Created = DateTime.Now;
                database.Add<Login>(login);
                return true;
            }
            return false;
        }

        public Login Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<Login>(l => l.Id == id);
        }

        public Login Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<Login>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(Login login)
        {
            if (login != null)
            {
                if (database.Update<Login>(login) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Delete(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);
            if (login != null)
            {
                login.IsActive = false;
                database.Update<Login>(login);
                result = true;
            }
            return result;
        }

        public bool Remove(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            try
            {
                var login = database.Single<Login>(l => l.Id == loginId);

                if (login != null)
                {
                    if (database.Delete<Login>(login.Id) > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }


        public ICollection<Login> GetAll()
        {
            return database.All<Login>().ToList().AsReadOnly();
        }

        public List<Login> GetLogins(List<Guid> Ids)
        {
            var list = new List<Login>();

            var script = string.Format(@"SELECT 
                                Id as Id ,
                                EmailAddress as EmailAddress ,
                                DisplayName as DisplayName ,
                                IsAxxessAdmin as IsAxxessAdmin,
                                IsAxxessSupport as IsAxxessSupport ,
                                IsActive as  IsActive ,
                                IsLocked as IsLocked , 
                                LastLoginDate as  LastLoginDate,
                                Created as Created 
                                    FROM 
                                        logins 
                                            WHERE 
                                                Id IN ({0}) ", Ids.Select(Id => string.Format("'{0}'", Id)).ToArray().Join(", "));
            using (var cmd = new FluentCommand<Login>(script))
            {
                list = cmd.SetConnection("AxxessMembershipConnectionString")
                .SetMap(reader => new Login
                {
                    Id = reader.GetGuid("Id"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    DisplayName = reader.GetStringNullable("DisplayName"),
                    IsAxxessAdmin = reader.GetBoolean("IsAxxessAdmin"),
                    IsAxxessSupport = reader.GetBoolean("IsAxxessSupport"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsLocked = reader.GetBoolean("IsLocked"),
                    LastLoginDate = reader.GetDateTime("LastLoginDate"),
                    Created = reader.GetDateTime("Created")
                }).AsList();
            }
            return list;
        }

        public IList<Login> GetAllByRole(Roles role)
        {
            return database.Find<Login>(l => l.Role == role.ToString() && l.IsActive == true).ToList();
        }

        public ICollection<Login> AutoComplete(string searchString)
        {
            return database.Find<Login>(u => u.EmailAddress.StartsWith(searchString)).ToList();
        }

        public string GetLoginDisplayName(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.DisplayName;
                }
            }

            return name;
        }

        public string GetLoginEmailAddress(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.EmailAddress;
                }
            }

            return name;
        }

        public bool AddLoginAgencyUser(Guid agencyId, Guid userId, Guid loginId)
        {
            var userLoginAgency = new UserLoginAgency { AgencyId = agencyId, UserId = userId, LoginId = loginId, IsDeprecated = false, Status = 1 };
            try
            {
                database.Add(userLoginAgency);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public UserLoginAgency GetLoginAgencyUser(Guid agencyId, Guid userId)
        {
            return database.Single<UserLoginAgency>(u => u.AgencyId == agencyId && u.UserId == userId && u.IsDeprecated == false);
        }

        public IList<UserLoginAgency> GetAllLoginAgencyUsers(Guid loginId)
        {
            return database.Find<UserLoginAgency>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == 1);
        }

        public AgencySnapShot GetAgencySnapShot(Guid agencyId)
        {
            return database.Single<AgencySnapShot>(a => a.Id == agencyId);
        }

        public IList<AgencySnapShot> GetAgencySnapShots()
        {
            return database.All<AgencySnapShot>().ToList();
        }

        public IList<AgencySnapShot> GetAgencySnapShots(List<Guid> agencyIds)
        {
             var list = new List<AgencySnapShot>();
             if (agencyIds != null && agencyIds.Count > 0)
             {
                 var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name ,
                        DBServerIp as DBServerIp 
                            FROM
                                agencysnapshots
                                        WHERE 
                                            Id  IN ({0})", agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));


                 using (var cmd = new FluentCommand<AgencySnapShot>(script))
                 {
                     list = cmd.SetConnection("AxxessMembershipConnectionString")
                     .SetMap(reader => new AgencySnapShot
                     {
                         Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name"),
                         DBServerIp = reader.GetStringNullable("DBServerIp")
                     })
                     .AsList();
                 }
             }
            return list;
        }

        #endregion
    }
}
