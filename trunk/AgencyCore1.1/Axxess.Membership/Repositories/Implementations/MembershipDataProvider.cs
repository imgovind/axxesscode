﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    
    using SubSonic.Repository;

    public class MembershipDataProvider : IMembershipDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public MembershipDataProvider()
        {
            this.database = new SimpleRepository("AxxessMembershipConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        #region IMembershipDataProvider Members

        private ISupportRepository supportRepository;
        public ISupportRepository SupportRepository
        {
            get
            {
                if (supportRepository == null)
                {
                    supportRepository = new SupportRepository(this.database);
                }
                return supportRepository;
            }
        }

        private IErrorRepository errorRepository;
        public IErrorRepository ErrorRepository
        {
            get
            {
                if (errorRepository == null)
                    errorRepository = new ErrorRepository(this.database);
                return errorRepository;
            }
        }

        //private IApplicationRepository appRepository;
        //public virtual IApplicationRepository AppRepository
        //{
        //    get
        //    {
        //        if (appRepository == null)
        //            appRepository = new ApplicationRepository(this.database);

        //        return appRepository;
        //    }
        //}

        private IHostRepository hostRepository;
        public IHostRepository HostRepository
        {
            get
            {
                if (hostRepository == null)
                    hostRepository = new HostRepository(this.database);

                return hostRepository;
            }
        }

        //private ISettingRepository settingRepository;
        //public ISettingRepository SettingRepository
        //{
        //    get
        //    {
        //        if (settingRepository == null)
        //            settingRepository = new SettingRepository(this.database);

        //        return settingRepository;
        //    }
        //}

        private ILoginRepository loginRepository;
        public ILoginRepository LoginRepository
        {
            get
            {
                if (loginRepository == null)
                    loginRepository = new LoginRepository(this.database);

                return loginRepository;
            }
        }

        private IApplicationRepository applicationRepository;
        public IApplicationRepository ApplicationRepository
        {
            get
            {
                if (applicationRepository == null)
                {
                    applicationRepository = new ApplicationRepository(this.database);
                }
                return applicationRepository;
            }
        }

        #endregion
    }
}
