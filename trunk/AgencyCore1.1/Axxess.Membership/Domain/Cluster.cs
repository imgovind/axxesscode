﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class Cluster
    {
       public string Id { get; set; }
       public string DBServerIp { get; set; }
       public string WebServerIp { get; set; }
       public bool IsActive { get; set; }
    }
}
