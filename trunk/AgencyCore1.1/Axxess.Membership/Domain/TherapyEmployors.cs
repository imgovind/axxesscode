﻿

namespace Axxess.Membership.Domain
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    public class TherapyEmployors
    {
        public Guid Id { get; set; }
        public Guid TherapyId { get; set; }
        public Guid AgencyId { get; set; }
        public string state { get; set; }
        public string AgencyName { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
    }
}
