﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    using Enums;

    public class Account
    {
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string UserName { get; set; }
        public Change ResetType { get; set; }
        public bool HasMultiple { get; set; }


        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Agency Name")]
        public string AgencyName { get; set; }

        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("New Password:")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password:")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("New Signature:")]
        public string Signature { get; set; }

        [Required]
        [DisplayName("Security Check")]
        public bool captchaValid { get; set; }

    }
}
