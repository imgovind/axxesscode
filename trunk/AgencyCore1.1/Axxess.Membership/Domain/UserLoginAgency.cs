﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class UserLoginAgency
    {
        public int Id { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public bool IsDeprecated { get; set; }
        public int Status { get; set; }
    }
}
