﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class AgencySnapShot
    {
        #region Members

        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid SalesPerson { get; set; }
        public Guid Trainer { get; set; }
        public Guid BackupTrainer { get; set; }
        public bool IsAgreementSigned { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime FrozenDate { get; set; }
        public bool IsFrozen { get; set; }
        public string DBServerIp { get; set; }

        #endregion
    }
}
