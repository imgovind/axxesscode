﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core
{
    public class StringContainer : object
    {
        public string Value { get; set; }
        public StringContainer()
        {
            
        }

        public StringContainer(string s)
        {
            Value = s;
        }

        public static bool operator ==(StringContainer a, string b)
        {
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }
            return a.Value == b;
        }

        public static bool operator !=(StringContainer a, string b)
        {
            if (a == null)
            {
                return false;
            }
            //Cannot simplfy because that would cause an infinite loop
            return !(a.Value == b);
        }

        public static implicit operator StringContainer(string s)
        {
            return new StringContainer(s);
        }

        public static implicit operator string(StringContainer x)
        {
            return x.Value;
        }

    }
}
