﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using System.Web.Script.Serialization;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.Core
{
    [SubSonicTableNameOverride("menus")]
    public class MenuElement
    {

        public MenuElement()
        {
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string MenuName { get; set; }
        public string Url { get; set; }
        public string OnLoad { get; set; }
        public int Service { get; set; }

        public string StyleOptions { get; set; }
        public string Json
        {
            get
            {
                bool isStyleNotEmpty = StyleOptions.IsNotNullOrEmpty();
                var json = StyleOptions.IsNullOrEmpty() ? "{}" : StyleOptions;
                string jsonToAppend = "Id" + ":'" + Id + "'";
                jsonToAppend += "," + "Name" + ":'" + Name + "'";
                if (ParentMenus.IsNotNullOrEmpty())
                {
                    var menu = ParentMenus.Contains(",") ? "["+ParentMenus.Split(',').Select(s => "'" + s.Trim() + "'").ToArray().Join(",")+"]" : "'" + ParentMenus + "'";
                    jsonToAppend += "," + "Menu" + ":" + menu + "";
                }
                if (MenuName.IsNotNullOrEmpty())
                {
                    jsonToAppend += "," + "MenuName" + ":'" + MenuName + "'";
                }
                if (Url.IsNotNullOrEmpty())
                {
                    jsonToAppend += "," + "Url" + ":'" + Url + "'";
                }
                if (OnLoad.IsNotNullOrEmpty())
                {
                    jsonToAppend += "," + "OnLoad" + ":" + OnLoad;
                }
                if (isStyleNotEmpty)
                {
                    jsonToAppend += ",";
                }
                return json.Insert(json.IndexOf("{", System.StringComparison.Ordinal) + 1, jsonToAppend);
            }
        }

        [ScriptIgnore]
        public bool IsAreaPrefix { get; set; }
        [ScriptIgnore]
        public string ParentMenus { get; set; }
        [ScriptIgnore]
        public int Category { get; set; }
        [ScriptIgnore]
        public string OtherCategory { get; set; }
        [ScriptIgnore]
        public int Action { get; set; }
        //[ScriptIgnore]
        //[SubSonicIgnore]
        //public string[] Menu { get; set; }
    }
}
