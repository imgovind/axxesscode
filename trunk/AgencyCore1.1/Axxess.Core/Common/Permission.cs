﻿namespace Axxess.Core
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using System.Collections.Generic;

    public class Permission
    {
        //public int Id { get; set; }
        //public string Description { get; set; }
        //public string LongDescription { get; set; }
        //public string Category { get; set; }
        //public string Tip { get; set; }

        public int Id { get; set; }
        public bool HasChild { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Description { get; set; }
        public string ChildPermissions { get; set; }
        public int Category { get; set; }
        public int ChildGroup { get; set; }
        public bool IsDeprecated { get; set; }

        [SubSonicIgnore]
        public string CategoryName { get; set; }
    }

    public class PermissionLink
    {
        public int Id { get; set; }
        public int ChildId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool IsSubCategory { get; set; }
        public int GroupId { get; set; }
        [SubSonicIgnore]
        public bool HasServiceScope { get; set; }
        [SubSonicIgnore]
        public int Service { get; set; }
    }
}
