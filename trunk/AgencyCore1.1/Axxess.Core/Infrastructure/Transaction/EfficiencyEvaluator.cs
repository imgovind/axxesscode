﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure.Transaction
{
    using System.Security.AccessControl;

    public class EfficiencyEvaluator
    {
        public static List<double> Efficiencies { get; set; }

        static EfficiencyEvaluator()
        {
            Efficiencies = new List<double>();
        }

        public double GetMedian()
        {
            double median = 0;
            var sorted = Efficiencies.OrderBy(d => d).ToList();
            if (sorted.Count % 2 == 0)
            {
                var med1 = sorted[(sorted.Count - 1) / 2];
                var med2 = sorted[sorted.Count / 2];
                median = (med1 + med2) / 2.0;
            }
            else
            {
                median = sorted[sorted.Count / 2];
            }
            return median;
        }

        public double GetAverage()
        {
            return Efficiencies.Average();
        }

        //public int Median(List<double> list, int left, int right)
        //{
        //    var numMedians = (right - left) / 5;
        //    for (int i = 0; i < numMedians; i++)
        //    {
        //        var subLeft = left + i * 5;
        //        var subRight = subLeft + 4;
        //        if (subRight > right) subRight = right;
        //        var medianIdx = SelectIndex(list, subLeft, subRight, (subRight - subLeft) / 2);
        //        Swap(list, left + i, medianIdx);
        //    }
        //    return SelectIndex(list, left, left + numMedians, numMedians + 2);
        //}

        //public int Partition(List<double> list, int left, int right, int pivotIndex)
        //{
        //    var pivotValue = list[pivotIndex];
        //    Swap(list, pivotIndex, right);
        //    var storeIndex = left;
        //    for (int i = left; i < right - 1; i++)
        //    {
        //        if (list[i] < pivotValue)
        //        {
        //            Swap(list, storeIndex, i);
        //            storeIndex++;
        //        }
        //    }
        //    Swap(list, right, storeIndex);
        //    return storeIndex;
        //}

        //public void Swap(List<double> list, int swapIndex, int otherSwapIndex)
        //{
        //    var tempValue = list[otherSwapIndex];
        //    list[otherSwapIndex] = list[swapIndex];
        //    list[swapIndex] = tempValue;
        //}

        //public int SelectIndex(List<double> list, int left, int right, int k)
        //{
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        var pivotIndex = (right - left) / 2;
        //        var newPivotIndex = Partition(list, left, right, pivotIndex);
        //        var pivotDist = newPivotIndex - left + 1;
        //        if (pivotDist == k)
        //        {
        //            return newPivotIndex;
        //        }
        //        else if (k < pivotDist)
        //        {
        //            right = newPivotIndex - 1;
        //        }
        //        else
        //        {
        //            k = k - pivotDist;
        //            left = newPivotIndex + 1;
        //        }
        //    }
        //    return 0;
        //}

        public T EvaluateFunc<T>(Func<T> func)
        {
            DateTime start = DateTime.Now;
            T obj = func.Invoke();
            DateTime end = DateTime.Now;
            var diff = end - start;
            Efficiencies.Add(diff.TotalMilliseconds);
            return obj;
        }
    }
}
