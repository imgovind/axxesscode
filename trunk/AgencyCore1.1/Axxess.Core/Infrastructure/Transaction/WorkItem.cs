﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Reflection;

    public class WorkItem
    {
        #region Members

        private Func<bool> perform;
        private Action rollback;
        private string description;

        #endregion

        #region Constructors

        public WorkItem(Func<bool> perform, Action rollback, string description)
        {
            this.perform = perform;
            this.rollback = rollback;
            this.description = description;
        }

        #endregion

        #region Public Methods & Properties

        public string Description { get { return description; } }

        public bool Perform()
        {
            if (perform == null) { return false; }

            return perform();
        }

        public void Rollback()
        {
           // if (perform == null) { return; }
            if (this.rollback != null)
            {
                this.rollback();
            }
        }

        #endregion
       
    }
}
