﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Collections.Generic;

    public class WorkSequence
    {
        #region Constructors

        public WorkSequence()
        {
            this.works = new List<WorkItem>();
            this.IgnoreRollbackErrors = false;
        }

        #endregion

        #region Properties

        public bool IgnoreRollbackErrors { get; set; }

        private List<WorkItem> works;
        public IEnumerable<WorkItem> Items
        {
            get { return this.works.AsEnumerable(); }
        }

        #endregion

        #region Events

        public event Action<WorkSequence> Complete;

        public event Action<WorkSequence, WorkItem, int> Error;

        #endregion

        #region Public Methods

        public void Add(Func<bool> perform)
        {
            this.Add(perform, null);
        }

        public void Add(Func<bool> perform, Action rollback)
        {
            this.Add(perform, rollback, string.Empty);
        }

        public void Add(Func<bool> perform, Action rollback, string description)
        {
            WorkItem item = new WorkItem(perform, rollback, description);
            this.works.Add(item);
        }

        public void Insert(int index,Func<bool> perform, Action rollback, string description)
        {
            WorkItem item = new WorkItem(perform, rollback, description);
            this.works.Insert(index, item);
        }

        public void AddRange(List<WorkItem> items)
        {
            this.works.AddRange(items);
        }

        public void Perform()
        {
            WorkItem[] workItems = this.works.ToArray();

            int index;
            for (index = 0; index < workItems.Length; index++)
            {
                try
                {
                    if (!workItems[index].Perform())
                    {
                        break;
                    }
                }
                catch (Exception)
                {
                    break;
                }
            }

            if (index < workItems.Length)
            {
                if (this.Error is Action<WorkSequence, WorkItem, int>)
                {
                    this.Error(this, workItems[index], index);
                }
                
                for (int j = index; j >= 0; j--)
                {
                    try
                    {
                        workItems[j].Rollback();
                    }
                    catch
                    {
                        if (!this.IgnoreRollbackErrors)
                        {
                            throw;
                        }
                    }
                }
            }

            if (this.Complete is Action<WorkSequence>)
            {
                this.Complete(this);
            }
        }

        public void PerformAsync(AsyncCallback callback)
        {
            Action perform = this.Perform;
            perform.BeginInvoke(callback, this);
        }

        #endregion

    }
}
