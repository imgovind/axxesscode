﻿namespace Axxess.Core.Infrastructure
{
    public static class Crypto
    {
        private static ICryptoProvider InternalCryptoProvider
        {
            get
            {
                return Container.Resolve<ICryptoProvider>();
            }
        }

        public static string Encrypt(string plainText)
        {
            Check.Argument.IsNotEmpty(plainText, "plainText");
            var croptoSetting = CryptoSettings.Get(1) ?? new CryptoElement();
            return InternalCryptoProvider.Encrypt(plainText,
                croptoSetting.PassPhrase,
                croptoSetting.SaltValue,
                croptoSetting.HashAlgorithm,
                croptoSetting.PasswordIterations,
                croptoSetting.InitVector,
                croptoSetting.KeySize);
        }

        public static string Decrypt(string cypherText)
        {
            Check.Argument.IsNotEmpty(cypherText, "cypherText");
            var croptoSetting = CryptoSettings.Get(1) ?? new CryptoElement();
            return InternalCryptoProvider.Decrypt(cypherText,
                croptoSetting.PassPhrase,
                croptoSetting.SaltValue,
                croptoSetting.HashAlgorithm,
                croptoSetting.PasswordIterations,
                croptoSetting.InitVector,
                croptoSetting.KeySize);
        }
    }
}
