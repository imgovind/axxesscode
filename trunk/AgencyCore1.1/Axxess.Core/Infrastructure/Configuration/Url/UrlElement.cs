﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Configuration;

    public class UrlElement : AxxessElement 
    {
        [ConfigurationProperty("url", IsRequired = true)]
        public string URL
        {
            get { return (string)this["url"]; }
            set { this["url"] = value; }
        }
        
    }
}
