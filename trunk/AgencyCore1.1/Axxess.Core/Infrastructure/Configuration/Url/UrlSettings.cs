﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure
{
    public static class UrlSettings
    {
        private static string GetUlrHelper(string urlName)
        {
            var element = UrlConfig.Instances(urlName);
            return element!=null ? element.URL : string.Empty;
        }

        public static string ANSIGeneratorUrl { get { return GetUlrHelper("ANSIGeneratorUrl"); } }
        public static string NewsFeedUrl { get { return GetUlrHelper("NewsFeedUrl"); } }
        public static string PatientEligibilityUrl { get { return GetUlrHelper("PatientEligibilityUrl"); } }
        public static string WeatherImageUriFormat { get { return GetUlrHelper("WeatherImageUriFormat"); } }
        public static string WeatherForecastImageUriFormat { get { return GetUlrHelper("WeatherForecastImageUriFormat"); } }
        public static string WeatherImageThumbnailUriFormat { get { return GetUlrHelper("WeatherImageThumbnailUriFormat"); } }
        public static string YahooWeatherUri { get { return GetUlrHelper("YahooWeatherUri"); } }
        public static string[] MemcacheServerUriArray
        {
            get
            {
                return GetUlrHelper("MemcacheServerUris").Split(';');
            }
        }
        public static string RemittanceSchedulerUrl { get { return GetUlrHelper("RemittanceSchedulerUrl"); } }
    }
}
