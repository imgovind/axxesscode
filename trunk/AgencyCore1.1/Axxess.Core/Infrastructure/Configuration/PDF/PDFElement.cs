﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Configuration;

    public class PDFElement : AxxessElement 
    {
        [ConfigurationProperty("pdf", IsRequired = true)]
        public string PDF
        {
            get { return (string)this["pdf"]; }
            set { this["pdf"] = value; }
        }

        [ConfigurationProperty("xml", IsRequired = true)]
        public string XML
        {
            get { return (string)this["xml"]; }
            set { this["xml"] = value; }
        }

        [ConfigurationProperty("isXmlExist", IsRequired = true, DefaultValue = false)]
        public bool IsXmlExist
        {
            get { return (bool)this["isXmlExist"]; }
            set { this["isXmlExist"] = value; }
        }
        
    }
}
