﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Configuration;

    public class CryptoElement : AxxessElement
    {
        [ConfigurationProperty("passPhrase", IsRequired = true)]
        public string PassPhrase
        {
            get { return (string)this["passPhrase"]; }
            set { this["passPhrase"] = value; }
        }
        [ConfigurationProperty("saltValue", IsRequired = true)]
        public string SaltValue
        {
            get { return (string)this["saltValue"]; }
            set { this["saltValue"] = value; }
        }
        [ConfigurationProperty("hashAlgorithm", IsRequired = true)]
        public string HashAlgorithm
        {
            get { return (string)this["hashAlgorithm"]; }
            set { this["hashAlgorithm"] = value; }
        }
        [ConfigurationProperty("passwordIterations", IsRequired = true)]
        public int PasswordIterations
        {
            get { return (int)this["passwordIterations"]; }
            set { this["passwordIterations"] = value; }
        }

        [ConfigurationProperty("initVector", IsRequired = true)]
        public string InitVector
        {
            get { return (string)this["initVector"]; }
            set { this["initVector"] = value; }
        }

        [ConfigurationProperty("keySize", IsRequired = true)]
        public int KeySize
        {
            get { return (int)this["keySize"]; }
            set { this["keySize"] = value; }
        }
    }
}
