﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure
{
    public class CryptoSettings
    {
        public static CryptoElement Get(int index)
        {
            return CryptoConfig.Instances(index.ToString());
        }
    }
}
