﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Configuration;

    [ConfigurationCollection(typeof(CryptoElement))]
    public class CryptoElementCollection : AxxessElementCollection<CryptoElement>
    {
    }
}
