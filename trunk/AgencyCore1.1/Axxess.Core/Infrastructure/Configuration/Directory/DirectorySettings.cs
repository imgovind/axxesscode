﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure
{
    public class DirectorySettings
    {
        public static string GetPath(string name)
        {
            var element = DirectoryConfig.Instances(name);
            return element != null ? element.Path : string.Empty;
        }

        public static string PdfBasePath { get { return GetPath("PdfBasePath"); } }
        public static string XmlBasePath { get { return GetPath("XmlBasePath"); } }
        public static string MailTemplate
        {
            get
            {
                return GetPath("TemplateDirectory");
            }
        }
        public static string ExcelDirectory
        {
            get
            {
                return GetPath("ExcelDirectory");
            }
        }
    }
}
