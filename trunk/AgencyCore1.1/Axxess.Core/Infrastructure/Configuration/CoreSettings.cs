﻿namespace Axxess.Core.Infrastructure
{
    using System;

    using Axxess.Core.Extension;

    public static class CoreSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static bool UseSSL
        {
            get
            {
                return configuration.AppSettings["UseSSL"].ToBoolean();
            }
        }

        public static bool CompressHtmlResponse
        {
            get
            {
                return configuration.AppSettings["CompressHtmlResponse"].ToBoolean();
            }
        }

        public static double CachingIntervalInMinutes
        {
            get
            {
                return configuration.AppSettings["CachingIntervalInMinutes"].ToDouble();
            }
        }

        public static double MembaseSessionTimeoutInMinutes
        {
            get
            {
                return configuration.AppSettings["MembaseSessionTimeoutInMinutes"].ToDouble();
            }
        }

        public static string NoReplyEmail
        {
            get
            {
                return configuration.AppSettings["NoReplyEmail"];
            }
        }

        public static string NoReplyDisplayName
        {
            get
            {
                return configuration.AppSettings["NoReplyDisplayName"];
            }
        }

        public static bool EnableSSLMail
        {
            get
            {
                return configuration.AppSettings["EnableSSLMail"].ToBoolean();
            }
        }

       

        public static string[] NoEncryptPaths
        {
            get
            {
                return configuration.AppSettings["NoEncryptPaths"].Split(new string[] {";"}, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static string[] NonSecurePaths
        {
            get
            {
                return configuration.AppSettings["NonSecurePaths"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static string[] NoEncryptFiles
        {
            get
            {
                return configuration.AppSettings["NoEncryptFiles"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static bool MobileTestEnvironment
        {
            get
            {
                return configuration.AppSettings["MobileTestEnvironment"] == "true" ? true : false;
            }
        }

        public static bool MobileRedirect
        {
            get
            {
                return configuration.AppSettings["MobileRedirect"] == "true" ? true : false;
            }
        }

        public static int MaxFileSizeInBytes
        {
            get
            {
                return configuration.AppSettings["MaxFileSizeInBytes"].ToInteger();
            }
        }

        public static string AgencyManagementConnectionString
        {
            get
            {
                return configuration.AppSettings["AgencyManagementConnectionString"];
            }
        }

        public static string AgencyManagementProviderName
        {
            get
            {
                return configuration.AppSettings["AgencyManagementProviderName"];
            }
        }

        public static string ApplicationManagementConnectionString
        {
            get
            {
                return configuration.AppSettings["ApplicationManagementConnectionString"];
            }
        }

        public static string OasisCConnectionString
        {
            get
            {
                return configuration.AppSettings["OasisCConnectionString"];
            }
        }

        public static string OasisCProviderName
        {
            get
            {
                return configuration.AppSettings["OasisCProviderName"];
            }
        }

        public static string LogsConnectionString
        {
            get
            {
                return configuration.AppSettings["LogsConnectionString"];
            }
        }

        public static string LogsProviderName
        {
            get
            {
                return configuration.AppSettings["LogsProviderName"];
            }
        }
    
    }
}
