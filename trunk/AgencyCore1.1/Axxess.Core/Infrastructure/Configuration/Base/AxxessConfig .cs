﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public abstract class AxxessConfig<E,S,C>
        where E: AxxessElement, new()
        where C:AxxessElementCollection<E>
        where S:AxxessConfigSection<E,C>
    {
        protected static Dictionary<string, E> _instances;
        static AxxessConfig()
        {
            _instances = new Dictionary<string, E>();
            S sec = (S)System.Configuration.ConfigurationManager.GetSection(typeof(S).Name);
            if (sec != null)
            {
                foreach (E i in sec.Instances)
                {
                    _instances.Add(i.Name, i);
                }
            }
        }
       
        public static E Instances(string instanceName)
        {
            return _instances[instanceName];
        }

        protected AxxessConfig()
        {
        }
    }
}
