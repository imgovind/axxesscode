﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Axxess.Core.Infrastructure
{
    public abstract class AxxessConfigSection <E,C> :  ConfigurationSection
        where E : AxxessElement, new()
        where C: AxxessElementCollection<E> 
    {
        [ConfigurationProperty("section", IsRequired = true, IsDefaultCollection = true)]
        public C Instances
        {
            get { return (C)this["section"]; }
            set { this["section"] = value; }
        }
    }
}
