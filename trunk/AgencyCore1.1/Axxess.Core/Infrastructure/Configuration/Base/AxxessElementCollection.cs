﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Axxess.Core.Infrastructure
{
    public class AxxessElementCollection<E> : ConfigurationElementCollection where E :AxxessElement, new()
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new E();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((E)element).Name;
        }  
    }
}
