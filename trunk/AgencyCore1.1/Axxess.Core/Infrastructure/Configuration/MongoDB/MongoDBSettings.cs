﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure
{
    public class MongoDBSettings
    {
        public static string GetConnectionString(string name)
        {
            var element = MongoDBConfig.Instances(name);
            return element != null ? element.ConnectionString : string.Empty;
        }
    }
}
