﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.ComponentModel;

    public class PermissionAttribute : BaseAttribute
    {
        public PermissionAttribute(string description, string longDescription, string category, string tip) :
            base(description,category)
        {
            this.LongDescription = longDescription;
            this.Tip = tip;
        }

        public string LongDescription { get; private set; }
        public string Tip { get; private set; }
    }
}
