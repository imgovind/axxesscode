﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.ComponentModel;

    public class CustomDescriptionAttribute : BaseAttribute
    {
        public CustomDescriptionAttribute(string description, string shortDescription )
            : base(description)
        {
            this.ShortDescription = shortDescription;
        }

        public CustomDescriptionAttribute(string description, string shortDescription, string category)
            : base(description, category)
        {
            this.ShortDescription = shortDescription;
        }

        public CustomDescriptionAttribute(string description, string shortDescription, string category, string statusGroup)
            : base(description,category)
        {
            this.StatusGroup = statusGroup;
            this.ShortDescription = shortDescription;
        }

        public CustomDescriptionAttribute(string description, string shortDescription, string category, string statusGroup, string formGroup)
            : base(description,category)
        {
            this.StatusGroup = statusGroup;
            this.ShortDescription = shortDescription;
            this.FormGroup = formGroup;
        }

        public CustomDescriptionAttribute(string description, string shortDescription, string category, string statusGroup, string formGroup, string actualDescription)
            : base(description,category)
        {
            this.StatusGroup = statusGroup;
            this.ShortDescription = shortDescription;
            this.FormGroup = formGroup;
            this.ActualDescription = actualDescription;
        }


        public string StatusGroup { get; private set; }
        public string ShortDescription { get; private set; }
        public string FormGroup { get; private set; }
        public string ActualDescription { get; private set; }
    }
}
