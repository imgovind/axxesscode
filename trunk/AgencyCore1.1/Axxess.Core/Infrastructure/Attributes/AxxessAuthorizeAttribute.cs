﻿namespace Axxess.Core.Infrastructure
{
    using System.Web.Mvc;

    public class AxxessAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.End();
            }
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
