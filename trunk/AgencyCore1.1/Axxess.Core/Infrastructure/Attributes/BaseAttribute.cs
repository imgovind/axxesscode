﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.ComponentModel;

    public class BaseAttribute : DescriptionAttribute
    {
        public BaseAttribute(string description)
            : base(description)
        {
        }
        public BaseAttribute(string description,string category)
            : base(description)
        {
            this.Category = category;
        }

        public string Category { get; private set; }
    }
}
