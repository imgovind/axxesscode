﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Net;

    using Couchbase;
    using Couchbase.Configuration;

    using Enyim.Caching.Memcached;

    public static class CouchbaseClientActivator
    {
        #region Members

        private static CouchbaseClient couchbaseClient;

        public static CouchbaseClient Cache
        {
            get
            {
                if (couchbaseClient == null)
                {
                    LoadConfig();
                }
                return couchbaseClient;
            }
        }

        private static void LoadConfig()
        {
            var couchbaseClientConfiguration = new CouchbaseClientConfiguration();
            couchbaseClientConfiguration.SocketPool.DeadTimeout = new TimeSpan(0, 0, 10);
            couchbaseClientConfiguration.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, 2);
            couchbaseClientConfiguration.Bucket = "dev";
            couchbaseClientConfiguration.BucketPassword = "";

            //Quick test of Store/Get operations
            if (UrlSettings.MemcacheServerUriArray != null && UrlSettings.MemcacheServerUriArray.Length > 0)
            {
                foreach (var serverUri in UrlSettings.MemcacheServerUriArray)
                {
                    couchbaseClientConfiguration.Urls.Add(new Uri(serverUri));
                }
                couchbaseClient = new CouchbaseClient(couchbaseClientConfiguration);
            }
        }

        #endregion
    }
}
