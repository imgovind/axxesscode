﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class MessageBuilder
    {
        #region Static Constructor

        private static readonly string templateDirectory;
        static MessageBuilder()
        {
            templateDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DirectorySettings.MailTemplate);
        }

        #endregion

        #region Public Methods

        public static string PrepareTextFrom(string templateName, params string[] pairs)
        {
            string body = ReadTextFrom(templateName);

            for (var i = 0; i < pairs.Length; i += 2)
            {
                body = body.Replace("<%={0}%>".FormatWith(pairs[i]), pairs[i + 1]);
            }

            return body;
        }

        public static string PrepareTextFromTemplateBody(string templateBody, params string[] pairs)
        {
            for (var i = 0; i < pairs.Length; i += 2)
            {
                templateBody = templateBody.Replace("<%={0}%>".FormatWith(pairs[i]), pairs[i + 1]);
            }
            return templateBody;
        }

        public static string ReadTextFrom(string templateName)
        {
            string filePath = string.Concat(Path.Combine(templateDirectory, templateName), ".txt");
            return File.ReadAllText(filePath);
        }

       

        #endregion
    }
}
