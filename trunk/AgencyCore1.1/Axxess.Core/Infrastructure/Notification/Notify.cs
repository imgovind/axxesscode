﻿namespace Axxess.Core.Infrastructure
{
    using System;
   
    public static class Notify
    {
        #region Static Methods

        private static INotification Notification
        {
            get
            {
                return Container.Resolve<INotification>();
            }
        }

        public static void User(string fromAddress, string toAddress, string subject, string body)
        {
            //INotification notification = Container.Resolve<INotification>();
            Notification.Send(fromAddress, toAddress, subject, body);
        }

        public static void User(string fromAddress, string toAddress, string ccAddress, string subject, string body)
        {
            //INotification notification = Container.Resolve<INotification>();
            Notification.Send(fromAddress, toAddress, ccAddress, subject, body);
        }

        public static void Webmaster(string subject, string body)
        {
            //INotification notification = Container.Resolve<INotification>();
            Notification.Send(CoreSettings.NoReplyEmail, CoreSettings.NoReplyEmail, subject, body);
        }

        #endregion
    }
}
