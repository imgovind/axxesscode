﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Text;

    using MySql.Data;
    using MySql.Data.MySqlClient;

    using Extension;

    public class MySqlRestore : IDatabaseOperation
    {
        #region Constructor and Private Members

        private string inputFilePath;
        private bool enableEncryption;
        private string connectionString;

        public MySqlRestore(string connectionString, string inputFilePath, bool enableEncryption)
        {
            this.inputFilePath = inputFilePath;
            this.enableEncryption = enableEncryption;
            this.connectionString = connectionString;
        }

        #endregion

        #region IDatabaseOperation Members

        public void Execute()
        {
            var stringBuilder = new StringBuilder();
            using (var textReader = new StreamReader(this.inputFilePath, true))
            {
                var line = string.Empty;

                while (line != null)
                {
                    line = textReader.ReadLine();
                    if (line == null)
                    {
                        continue;
                    }
                    if (line.Trim().Length == 0)
                        continue;

                    line = ReadLine(line);

                    if (line.Trim() + "" == "")
                        continue;
                    if (line == "\r\n")
                        continue;
                    if (line.StartsWith("--"))
                        continue;

                    stringBuilder.Append(line);

                    if (line.EndsWith(";"))
                    {
                        if (stringBuilder.ToString().IsNotNullOrEmpty())
                        {
                            using (var fluentCommand = new FluentCommand<int>(stringBuilder.ToString()))
                            {
                                fluentCommand
                                    .UseConnectionString(connectionString, int.MaxValue)
                                    .AsNonQuery();
                            }
                            stringBuilder = new StringBuilder();
                        }
                    }
                }
            }
        }

        #endregion

        #region Private Members

        private string ReadLine(string input)
        {
            var result = input;
            if (input.IsNotNullOrEmpty())
            {
                if (this.enableEncryption)
                {
                    result = Crypto.Decrypt(input);
                }
            }
            return result;
        }

        #endregion
    }
}
