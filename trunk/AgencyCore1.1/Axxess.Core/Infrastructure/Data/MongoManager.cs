﻿namespace Axxess.AgencyManagement.Entities
{
    using System.Configuration;
    using MongoDB.Driver;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class MongoManager
    {
        public static MongoDatabase GetDataBase(string dataBaseName)
        {
            var urlStr = MongoDBSettings.GetConnectionString(dataBaseName);
            var url = MongoUrl.Create(urlStr);
            var settings = MongoClientSettings.FromUrl(url);
            var client = new MongoClient(settings);
            MongoServer server = client.GetServer();
            return server.GetDatabase(dataBaseName.ToLowerCase());

        }
    }
}
