﻿// -----------------------------------------------------------------------
// <copyright file="SimpleRepositoryExtended.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Axxess.Core.Infrastructure.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;

    using SubSonic.DataProviders;
    using SubSonic.Extensions;
    using SubSonic.Query;
    using SubSonic.Repository;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SimpleRepositoryExtended : SimpleRepository
    {
        private readonly SimpleRepositoryOptions _options = SimpleRepositoryOptions.None;
        private readonly IDataProvider _provider;
        private readonly List<Type> migrated;

        public SimpleRepositoryExtended()
            : this(ProviderFactory.GetProvider(), SimpleRepositoryOptions.None)
        {
            
        }

        public SimpleRepositoryExtended(string connectionStringName)
            : this(connectionStringName, SimpleRepositoryOptions.None)
        {
        }

        public SimpleRepositoryExtended(string connectionStringName, SimpleRepositoryOptions options)
            : this(ProviderFactory.GetProvider(connectionStringName), options)
        {
        }

        public SimpleRepositoryExtended(string connectionString, string providerName, SimpleRepositoryOptions options)
            : this(ProviderFactory.GetProvider(connectionString, providerName), options)
        {
        }

        public SimpleRepositoryExtended(SimpleRepositoryOptions options)
            : this(ProviderFactory.GetProvider(), options)
        {
        }

        public SimpleRepositoryExtended(IDataProvider provider)
            : this(provider, SimpleRepositoryOptions.None)
        {
        }

        public SimpleRepositoryExtended(IDataProvider provider, SimpleRepositoryOptions options)
            : base(provider, options)
        {
              this._provider = provider;
              this._options = options;
        }

        public new T Single<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            T obj = default(T);
            SqlQuery sqlQuery = new SubSonic.Query.Select(this._provider).From(this._provider.FindOrCreateTable<T>()).Paged(1, 1);
            List<SubSonic.Query.Constraint> list1 = ((IEnumerable<SubSonic.Query.Constraint>)((Expression)expression).ParseConstraints()).ToList();
            sqlQuery.Constraints = list1;
            return sqlQuery.ExecuteSingle<T>();
        }
    }
}
