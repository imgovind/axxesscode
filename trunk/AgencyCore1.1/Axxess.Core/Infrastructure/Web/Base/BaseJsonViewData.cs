﻿
namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class BaseJsonViewData
    {
        public string url { get; set; }
        public bool isSuccessful { get; set; }
        public string errorMessage { get; set; }
        public bool isWarning { get; set; }

        public List<Guid> UserIds { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid Id { get; set; }
        public string Service { get; set; }
        public int ServiceId { get; set; }

        public BaseJsonViewData(bool success)
        {
            this.isSuccessful = success;
        }

        public BaseJsonViewData(bool success, string errorMessage)
        {
            this.isSuccessful = success;
            this.errorMessage = errorMessage;
        }

        public BaseJsonViewData()
        {
        }
    }
}
