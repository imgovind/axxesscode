﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Axxess.Core.Infrastructure.Web.Base;

    public abstract class Module : IModule
    {
        #region Constructor

        public Module() { }

        #endregion

        #region Properties

        public abstract string Name { get; }

        #endregion

        #region Methods

        public static void Register(Module module)
        {
            if (module == null)
            {
                throw new ArgumentNullException("Null module was passed.");
            }

            module.Register();
        }

        public virtual void Register()
        {
            RegisterRoutes(RouteTable.Routes);
        }

        public abstract void RegisterRoutes(RouteCollection routes);

        #endregion
    }
}
