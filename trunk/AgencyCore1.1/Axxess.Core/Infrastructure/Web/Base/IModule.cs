﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure.Web.Base
{
    public interface IModule
    {
        string Name { get; }
        void Register();

    }
}
