﻿namespace Axxess.Core.Infrastructure
{
    using System;
    public class JsonViewData : BaseJsonViewData
    {
        public int PatientStatus { get; set; }

        public bool IsPatientListRefresh { get; set; }
        public bool IsDeletedPatientListRefresh { get; set; }
        public bool IsPendingPatientListRefresh { get; set; }
        public bool IsNonAdmitPatientListRefresh { get; set; }
        public bool IsReferralListRefresh { get; set; }

        public bool IsCenterRefresh { get; set; }
        public bool IsDataCentersRefresh { get; set; }
        public bool IsCaseManagementRefresh { get; set; }
        public bool IsMyScheduleTaskRefresh { get; set; }
        
        public bool IsExportOASISRefresh { get; set; }
        public bool IsExportedOASISRefresh { get; set; }
        public bool IsNotExportedOASISRefresh { get; set; }

        public bool IsOrdersToBeSentRefresh { get; set; }
        public bool IsOrdersHistoryRefresh { get; set; }
        public bool IsOrdersPendingRefresh { get; set; }

        public bool IsPhysicianOrderPOCRefresh { get; set; }
       
        public bool IsFaceToFaceEncounterRefresh { get; set; }

        public bool IsCommunicationNoteRefresh { get; set; }
       
        public bool IsIncidentAccidentRefresh { get; set; }
       
        public bool IsInfectionRefresh { get; set; }

        public JsonViewData() { }

        public JsonViewData(bool success) 
            : base(success)
        {
        }

        public JsonViewData(bool success, string errorMessage) 
            : base (success, errorMessage)
        {
        }
    }
}
