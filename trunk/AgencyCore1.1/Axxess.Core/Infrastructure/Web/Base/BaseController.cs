﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    public abstract class BaseController : Controller
    {
        public static T Validate<T>(params Validation[] validations) where T : JsonViewData, new()
        {
            var entityValidator = new EntityValidator(validations);
            entityValidator.Validate();
            
            if (!entityValidator.IsValid)
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }
            else
            {
                return new T { isSuccessful = true };
            }
        }

        protected ActionResult PartialViewOrJson(object model)
        {
            if (Request.AcceptTypes.Contains("text/html"))
            {
                return PartialView(model);
            }
            if (Request.AcceptTypes.Contains("application/json"))
            {
                return Json(model);
            }
            return View(model);
        }

        protected ActionResult PartialViewOrJson(string view, object model)
        {
            if (Request.AcceptTypes.Contains("text/html"))
            {
                return PartialView(view, model);
            }
            if (Request.AcceptTypes.Contains("application/json"))
            {
                return Json(model);
            }
            return View(model);
        }
    }
}
