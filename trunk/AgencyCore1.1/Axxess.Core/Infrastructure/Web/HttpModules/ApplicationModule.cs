﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Configuration;

    public class ApplicationModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        public void Init(HttpApplication httpApp)
        {
            httpApp.BeginRequest += new EventHandler(this.BeginRequest);
        }

        #endregion

        #region Private Methods

        void BeginRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            HttpRequest currentRequest = currentContext.Request;
            if (currentRequest.ContentLength > CoreSettings.MaxFileSizeInBytes)
            {
                currentContext.Response.StatusCode = 999;
                throw new ConfigurationErrorsException("FileUploadMoreThanMaxSize");
            }
        }

        #endregion
    }
}
