﻿
namespace Axxess.Core.Enums
{
    using System.ComponentModel;

    public enum DataProviderType
    {
        [Description("System.Data.SqlClient")]
        SQL,
        [Description("MySql.Data.MySqlClient")]
        MYSQL,
        [Description("System.Data.SQLite")]
        SQLite
    }
}
