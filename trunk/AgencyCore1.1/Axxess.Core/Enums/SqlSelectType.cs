﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Enums
{
    public enum SqlSelectType : int
    {
        None = 0,
        All = 1,
        EventDate = 2,
        VisitDate = 3
    }
}
