﻿namespace Axxess.Core.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    [Flags]
    public enum AgencyServices : int
    {
        [CustomDescription("None", "N")]
        None = 0,
        [CustomDescription("Home Health", "HH")]
        HomeHealth = 1,
        [CustomDescription("Private Duty", "PD")]
        PrivateDuty = 2,
        //[CustomDescription("Therapy", "T")]
        //Therapy = 4
    }

    [Flags]
    public enum AgencyServicesBoundary : int
    {
        [CustomDescription("None", "N")]
        None = 0,
        [CustomDescription("All Services", "All")]
        All = 8191,
    }
}
