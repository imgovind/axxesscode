﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Extension
{
    public static class BooleanExtensions
    {
        public static int ToInteger(this bool value)
        {
            return value ? 1 : 0;
        }
    }
}
