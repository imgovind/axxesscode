﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Extension
{
    public static class DictionaryExtensions
    {
        public static TEnum GetEnum<TEnum>(this IDictionary<string, object> instance, string key) where TEnum : struct, IComparable, IConvertible, IFormattable
        {
            TEnum value = default(TEnum);
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }
            else
            {
                object temp = null;
                if (instance.TryGetValue(key, out temp))
                {
                    value = temp.ToString().ToEnum<TEnum>(default(TEnum));
                }
            }
            return value;
        }

        public static T GetOrDefault<T>(this IDictionary<string, T> instance, string key, T defaultValue)
        {
            T value = default(T);
            instance.TryGetValue(key, out value);
            if (EqualityComparer<T>.Default.Equals(value, default(T)))
            {
                return defaultValue;
            }
            return value;
        }

        public static V GetOrDefault<K, V>(this IDictionary<K, V> instance, K key)
        {
            V value = default(V);
            instance.TryGetValue(key, out value);
            if (EqualityComparer<V>.Default.Equals(value, default(V)))
            {
                return default(V);
            }
            return value;
        }

        public static V GetOrDefault<K, V>(this IDictionary<K, V> instance, K key, V defaultValue)
        {
            V value = default(V);
            instance.TryGetValue(key, out value);
            if (EqualityComparer<V>.Default.Equals(value, default(V)))
            {
                return defaultValue;
            }
            return value;
        }

        public static T Get<T>(this IDictionary<string, T> instance, string key)
        {
            T value = default(T);
            instance.TryGetValue(key, out value);
            return value;
        }

        public static string Get(this IDictionary<string, string> instance, string key)
        {
            string value = string.Empty;
            instance.TryGetValue(key, out value);
            return value;
        }

        public static string Get(this IDictionary<Guid, string> instance, Guid key)
        {
            string value = string.Empty;
            instance.TryGetValue(key, out value);
            return value;
        }

        public static void Set<T>(this IDictionary<string, object> instance, string key, T value)
        {
            instance[key] = value;
        }

        public static bool IsNotNullOrEmpty<K, V>(this Dictionary<K, V> dictionary)
        {
            return dictionary != null ? dictionary.Count > 0 : false;
        }

        public static void Merge<TKey, TValue>(this IDictionary<TKey, TValue> first, IDictionary<TKey, TValue> second)
        {
            if (second == null) return;
            if (first == null) { first = new Dictionary<TKey, TValue>(); }

            foreach (var item in second)
            {
                if (!first.ContainsKey(item.Key))
                {
                    first.Add(item.Key, item.Value);
                }
            }
        }


        public static IDictionary<TKey, TValue> MergeAndReturn<TKey, TValue>(this IDictionary<TKey, TValue> first, IDictionary<TKey, TValue> second)
        {
            first.Merge(second);
            return first;
        }

        public static void RemoveAll<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, Func<TValue, bool> predicate)
        {
            var keys = dictionary.Keys.Where(k => predicate(dictionary[k])).ToList();
            foreach (var key in keys)
	        {
                dictionary.Remove(key);
	        }
        }
    }
}
