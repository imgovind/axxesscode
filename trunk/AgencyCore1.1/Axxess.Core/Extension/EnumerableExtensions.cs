﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Linq;
    using System.Diagnostics;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public static class EnumerableExtensions
    {
        [DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (T item in enumerable)
            {
                action(item);
            }
        }

        [DebuggerStepThrough]
        public static void ForEach<K, V>(this IDictionary<K, V> enumerable, Action<K, V> action)
        {
            foreach (KeyValuePair<K, V> item in enumerable)
            {
                action(item.Key, item.Value);
            }
        }

        [DebuggerStepThrough]
        public static List<T> ToList<T>(this SafeList<T> safeList) where T : class
        {
            var list = new List<T>();
            safeList.ForEach(list.Add);
            return list;
        }

        [DebuggerStepThrough]
        public static string ToCommaSeperatedList<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
        }

        [DebuggerStepThrough]
        public static string ToCommaSeperatedNoQuote<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Select(s =>s.ToString()).ToArray().Join(", ");
        }

        [DebuggerStepThrough]
        public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable != null && enumerable.Any();
        }

        [DebuggerStepThrough]
        public static bool IsNotNullOrEmpty<T>(this IList<T> enumerable)
        {
            return enumerable != null && enumerable.Count > 0;
        }

        [DebuggerStepThrough]
        public static bool IsNullOrEmpty<T>(this IList<T> enumerable)
        {
            return enumerable == null || enumerable.Count == 0;
        }

        [Obsolete("This method possibly enumerates multiple times. If this method is needed, it must be rewritten.")]
        [DebuggerStepThrough]
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int size)
        {
            Check.Argument.IsNotNull(source, "source");
            Check.Argument.IsNotNegativeOrZero(size, "size");

            int index = 1;
            IEnumerable<T> partition = source.Take(size).AsEnumerable();

            while (partition.Any())
            {
                yield return partition;
                partition = source.Skip(index++ * size).Take(size).AsEnumerable();
            }
        }

        [DebuggerStepThrough]
        public static IEnumerable<T> Dequeue<T>(this Queue<T> queue, int limit)
        {
            do
            {
                if (queue.Count == 0)
                {
                    yield break;
                }
                yield return queue.Dequeue();
                limit--;
            } while (limit > 0);
        }

        public static List<SelectListItem> GenerateSelectListItems<T>(this IList<T> items, Func<T, string> textFunc, Func<T, string> valueFunc, Func<T, bool> whereFunc, string value, bool isDefaultSelectIncluded, string title, string defaultValue)
        {
            var selectListItems=items.IsNotNullOrEmpty() ? items.Where(whereFunc).Select(item => new SelectListItem { Text = textFunc.Invoke(item), Value = valueFunc.Invoke(item), Selected = value != null && value.IsEqual(valueFunc.Invoke(item)) }).ToList() : new List<SelectListItem>();
             if (isDefaultSelectIncluded)
            {
                selectListItems.Insert(0, new SelectListItem { Text = title, Value = defaultValue });
            }
             return selectListItems;
        }

        public static int CombineValues(this IList<int> items)
        {
            int value = 0;
            if (items.IsNotNullOrEmpty())
            {
                items.ForEach(i => value += i);
            }
            return value;
        }

    }

}
