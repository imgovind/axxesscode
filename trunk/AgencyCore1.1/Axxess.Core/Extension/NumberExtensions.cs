﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Extension
{
    public static class NumberExtensions
    {
        public static bool IsPowerOfTwo(this int x)
        {
            return (x > 0) && ((x & (x - 1)) == 0);
        }

        public static bool IsPowerOfTwo(this long x)
        {
            return (x > 0) && ((x & (x - 1)) == 0);
        }
    }
}
