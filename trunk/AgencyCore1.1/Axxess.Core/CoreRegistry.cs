﻿namespace Axxess.Core
{
    using System;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Core.Infrastructure;

    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            //Uncomment the code below for and in the web.config to enable logging for the memcache
            //log4net.Config.XmlConfigurator.Configure();
            For<IWebConfiguration>().Use<WebConfiguration>();
            For<ICache>().Use<MembaseCache>().Named("MembaseCache");
            For<ICache>().Use<HttpRuntimeCache>().Named("HttpRuntimeCache");
            For<INotification>().Use<EmailNotification>();
            For<ISessionStore>().Use<HttpContextSession>();
            For<ICryptoProvider>().Use<RijndaelProvider>();
            For<IDatabaseAdministration>().Use<MySqlAdministration>();
        }
    }
}
