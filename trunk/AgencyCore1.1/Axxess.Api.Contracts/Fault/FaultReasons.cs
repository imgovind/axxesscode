﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ComponentModel;

    public enum FaultReasons
    {
        [Description("User Singleton Class Error")]
        UserCacheFault = 1,
        [Description("Physician Singleton Class Error")]
        PhysicianCacheFault = 2,
        [Description("Agency Singleton Class Error")]
        AgencyCacheFault = 3
    }
}
