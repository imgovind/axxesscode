﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Diagnostics;

    public static class Windows
    {
        private static EventLog eventLog;
        public static EventLog EventLog
        {
            get
            {
                if (eventLog == null)
                {
                    InitializeEventLog();
                }
                return eventLog;
            }
        }

        private static string log = "Application";
        private static string source = "Axxess.Api.Services";

        static void InitializeEventLog()
        {
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, log);
            }
            eventLog = new EventLog();
            eventLog.Log = log;
            eventLog.Source = source;
        }
    }
}
