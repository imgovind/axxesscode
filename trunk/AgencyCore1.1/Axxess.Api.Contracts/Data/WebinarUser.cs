﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "http://api.axxessweb.com/Authentication/2011/07/")]
    public class WebinarUser
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string AgencyName { get; set; }
    }
}
