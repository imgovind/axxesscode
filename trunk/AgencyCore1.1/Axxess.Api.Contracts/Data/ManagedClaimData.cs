﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2012/08/")]
    public class ManagedClaimData
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid PatientId { get; set; }
        [DataMember]
        public DateTime EpisodeStartDate { get; set; }
        [DataMember]
        public DateTime EpisodeEndDate { get; set; }

        public string VerifiedVisits { get; set; }
    }
}
