﻿//namespace Axxess.Api.Contracts
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Runtime.Serialization;

//    [DataContract(Namespace = "http://api.axxessweb.com/EpisodeDetail/2012/03/")]
//    public class EpisodeDetail
//    {
//        public EpisodeDetail()
//        {
//            this.Assets = new List<Guid>();
//        }

//        [DataMember]
//        public string Comments { get; set; }
//        [DataMember]
//        public List<Guid> Assets { get; set; }
//        [DataMember]
//        public string Therapist { get; set; }
//        [DataMember]
//        public string CaseManager { get; set; }
//        [DataMember]
//        public string PrimaryDiagnosis { get; set; }
//        [DataMember]
//        public string SecondaryDiagnosis { get; set; }
//        [DataMember]
//        public string TriageCode { get; set; }
//        [DataMember]
//        public string FrequencyList { get; set; }
//        [DataMember]
//        public string PrimaryInsurance { get; set; }
//        [DataMember]
//        public string SecondaryInsurance { get; set; }
//        [DataMember]
//        public string PrimaryPhysician { get; set; }
//        [DataMember]
//        public string SurchargePayroll { get; set; }
//    }
//}
