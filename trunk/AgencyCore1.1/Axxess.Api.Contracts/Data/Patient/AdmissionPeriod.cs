﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/AdmissionPeriod/2012/03/")]
    public class AdmissionPeriod
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid AgencyId { get; set; }
        [DataMember]
        public Guid PatientId { get; set; }
        [DataMember]
        public DateTime StartOfCareDate { get; set; }
        [DataMember]
        public DateTime DischargedDate { get; set; }
        [DataMember]
        public string PatientData { get; set; }
        [DataMember]
        public string ProfileData { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsDeprecated { get; set; }
        [DataMember]
        public DateTime Created { get; set; }
    }
}
