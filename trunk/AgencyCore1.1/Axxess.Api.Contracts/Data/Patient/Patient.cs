﻿//namespace Axxess.Api.Contracts
//{
//    using System;
//    using System.Runtime.Serialization;

//    [DataContract(Namespace = "http://api.axxessweb.com/Patient/2012/03/")]
//    public class Patient
//    {
//        [DataMember]
//        public Guid Id { get; set; }
//        [DataMember]
//        public Guid AgencyId { get; set; }
//        [DataMember]
//        public Guid UserId { get; set; }
//        [DataMember]
//        public Guid AgencyLocationId { get; set; }
//        [DataMember]
//        public string PatientIdNumber { get; set; }
//        [DataMember]
//        public string MedicareNumber { get; set; }
//        [DataMember]
//        public string MedicaidNumber { get; set; }
//        [DataMember]
//        public string FirstName { get; set; }
//        [DataMember]
//        public string LastName { get; set; }
//        [DataMember]
//        public string MiddleInitial { get; set; }
//        [DataMember]
//        public string SSN { get; set; }
//        [DataMember]
//        public DateTime DOB { get; set; }
//        [DataMember]
//        public string Gender { get; set; }
//        [DataMember]
//        public float Height { get; set; }
//        [DataMember]
//        public int HeightMetric { get; set; }
//        [DataMember]
//        public float Weight { get; set; }
//        [DataMember]
//        public int WeightMetric { get; set; }
//        [DataMember]
//        public string Ethnicities { get; set; }
//        [DataMember]
//        public string MaritalStatus { get; set; }
//        [DataMember]
//        public string AddressLine1 { get; set; }
//        [DataMember]
//        public string AddressLine2 { get; set; }
//        [DataMember]
//        public string AddressCity { get; set; }
//        [DataMember]
//        public string AddressStateCode { get; set; }
//        [DataMember]
//        public string AddressZipCode { get; set; }
//        [DataMember]
//        public string PhoneHome { get; set; }
//        [DataMember]
//        public string PhoneMobile { get; set; }
//        [DataMember]
//        public string EmailAddress { get; set; }
//        [DataMember]
//        public DateTime StartofCareDate { get; set; }
//        [DataMember]
//        public string PharmacyName { get; set; }
//        [DataMember]
//        public string PharmacyPhone { get; set; }
//        [DataMember]
//        public string PrimaryInsurance { get; set; }
//        [DataMember]
//        public string SecondaryInsurance { get; set; }
//        [DataMember]
//        public string TertiaryInsurance { get; set; }
//        [DataMember]
//        public string PrimaryHealthPlanId { get; set; }
//        [DataMember]
//        public string SecondaryHealthPlanId { get; set; }
//        [DataMember]
//        public string TertiaryHealthPlanId { get; set; }
//        [DataMember]
//        public string PrimaryGroupName { get; set; }
//        [DataMember]
//        public string SecondaryGroupName { get; set; }
//        [DataMember]
//        public string TertiaryGroupName { get; set; }
//        [DataMember]
//        public string PrimaryGroupId { get; set; }
//        [DataMember]
//        public string SecondaryGroupId { get; set; }
//        [DataMember]
//        public string TertiaryGroupId { get; set; }

//        [DataMember]
//        public string DME { get; set; }
//        [DataMember]
//        public string OtherDME { get; set; }
//        [DataMember]
//        public string Payer { get; set; }
//        [DataMember]
//        public string ServicesRequired { get; set; }
//        [DataMember]
//        public string PaymentSource { get; set; }
//        [DataMember]
//        public string OtherPaymentSource { get; set; }
//        [DataMember]
//        public string AdmissionSource { get; set; }
//        [DataMember]
//        public Guid ReferrerPhysician { get; set; }
//        [DataMember]
//        public DateTime ReferralDate { get; set; }
//        [DataMember]
//        public string OtherReferralSource { get; set; }
//        [DataMember]
//        public Guid InternalReferral { get; set; }
//        [DataMember]
//        public Guid CaseManagerId { get; set; }
//        [DataMember]
//        public int Triage { get; set; }
//        [DataMember]
//        public string EvacuationZone { get; set; }
//        [DataMember]
//        public Guid PhotoId { get; set; }
//        [DataMember]
//        public int Status { get; set; }
//        [DataMember]
//        public DateTime Created { get; set; }
//        [DataMember]
//        public DateTime Modified { get; set; }
//        [DataMember]
//        public DateTime NonAdmissionDate { get; set; }
//        [DataMember]
//        public string NonAdmissionReason { get; set; }
//        [DataMember]
//        public string Comments { get; set; }
//        [DataMember]
//        public bool IsDeprecated { get; set; }
//        [DataMember]
//        public bool IsHospitalized { get; set; }
//        [DataMember]
//        public Guid HospitalizationId { get; set; }
//        [DataMember]
//        public DateTime DischargeDate { get; set; }
//        [DataMember]
//        public string DischargeReason { get; set; }
//        [DataMember]
//        public DateTime LastEligibilityCheck { get; set; }
//        [DataMember]
//        public Guid AdmissionId { get; set; }
//    }
//}
