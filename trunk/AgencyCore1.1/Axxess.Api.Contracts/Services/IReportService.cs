﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2011/06/")]
    public interface IReportService : IService
    {
        [OperationContract]
        List<List<string>> CahpsExport(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear);
        [OperationContract]
        List<List<string>> CahpsExportByPaymentSources(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear, List<int> paymentSources);
        [OperationContract]
        List<Dictionary<string, string>> PPSEpisodeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<Dictionary<string, string>> PPSVisitInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<Dictionary<string, string>> PPSChargeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<Dictionary<string, string>> PPSPaymentInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<Dictionary<string, string>> TherapyManagement(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<Dictionary<string, string>> HHRGReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<Dictionary<string, string>> UnbilledVisitsForManagedClaims(Guid agencyId, Guid agencyLocationId, int insurance, int status, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<PatientsAndVisitsByAgeResult> PatientsAndVisitsByAge(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<DischargeByReasonResult> DischargesByReason(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<PrimaryPaymentSourceResult> VisitsByPrimaryPaymentSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<VisitByStaffTypeResult> VisitsByStaffType(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<ReferralSourceResult> AdmissionsByReferralSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        List<PrincipalDiagnosisResult> PatientsVisitsByPrincipalDiagnosis(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
        [OperationContract]
        CostReportResult CostReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate);
    }
}
