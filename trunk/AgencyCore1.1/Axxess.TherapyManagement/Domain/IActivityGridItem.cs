﻿namespace Axxess.TherapyManagement.Domain
{
    using System;
    interface IActivityGridItem
    {
        string Type { get; set; }
        string Summary { get; set; }
        DateTime Date { get; set; }
    }
}
