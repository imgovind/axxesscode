﻿
namespace Axxess.TherapyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using System.Runtime.Serialization;

    public class AgencyReferralPatient : IActivityGridItem
    {
        public string Type { get; set; }
        public string Summary { get; set; }
        public DateTime Date { get; set; }
        public Guid Id { get; set; }
        public Guid TherapyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid AgencyId {get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public int Status { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime SignDate { get; set; }
        public string Comments { get; set; }


        #region Domain
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        [SubSonicIgnore]
        public string Age
        {
            get
            {
                int age = DateTime.Now.Year - this.DOB.Year;
                if (this.DOB > DateTime.Now.AddYears(-age))
                    age--;
                return age.ToString();
            }
        }

        [SubSonicIgnore]
        public string AgencyName { get; set; }

        [SubSonicIgnore]
        public string DOBFormat
        {
            get
            {
                if (this.DOB != null)
                    return this.DOB.ToShortDateString();
                else
                    return "";
            }
        }
        #endregion
    }
}
