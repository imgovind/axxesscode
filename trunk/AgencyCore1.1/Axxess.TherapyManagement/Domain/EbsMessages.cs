﻿namespace Axxess.TherapyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using System.Runtime.Serialization;
    using Axxess.TherapyManagement.Enums;

    public class EbsMessages
    {
        public int Id { get; set; }
        public string Message{get;set;}
        public string Title { get; set; }
        public DateTime TimeArrived { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public bool IsDeprecated { get; set; }
        public Guid AgencyId { get; set; }
        public Guid TherapyId { get; set; }
        public string Comment { get; set; }

        #region Domain
        [SubSonicIgnore]
        public string TypeDisplay
        {
            get
            {
                return ((EbsMessageType)this.Type).ToString();
            }
        }

        [SubSonicIgnore]
        public string Url { get; set; }

        [SubSonicIgnore]
        public string ActionUrl { get; set; }

        [SubSonicIgnore]
        public string Action
        {
            get
            {
                string action = string.Empty;
                switch (this.Type)
                {
                    case (int)EbsMessageType.Referral:
                        action = string.Format("<a href=\"javascript:void(0);\" onclick=\"Agency.SignReferral('{0}',true);\">Accept</a> | <a href=\"javascript:void(0);\" onclick=\"Agency.SignReferral('{0}',false);\">Decline</a>", this.Id); 
                        break;
                    case (int)EbsMessageType.Message:
                        action = string.Format("<a href=\"javascript:void(0);\" onclick=\"Agency.ViewNotice('{0}');\">View</a>", this.Id);
                        break;
                }
                return action;
            }
        }

        #endregion
    }
}
