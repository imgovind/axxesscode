﻿namespace Axxess.TherapyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using System.Runtime.Serialization;
    using Axxess.TherapyManagement.Enums;

    public class Notice
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }
        public bool Viewed { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
