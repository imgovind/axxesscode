﻿
namespace Axxess.TherapyManagement.Repositories
{
    using Axxess.TherapyManagement.Domain;
    using System;
    using System.Collections.Generic;

    public interface IAgencyReferralRepository
    {
        List<AgencyReferralPatient> GetReferralPatients(Guid therapyId, Guid agencyId, int status);
        AgencyReferralPatient GetReferralPatientById(Guid id);
        bool UpdateReferralPatient(AgencyReferralPatient referral);
    }
}
