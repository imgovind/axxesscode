﻿namespace Axxess.TherapyManagement.Repositories.Implementations
{
    using Axxess.TherapyManagement.Domain;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;

    public class AgencyCenterRepository : IAgencyCenterRepository
    {
        private readonly SimpleRepository database;
        public AgencyCenterRepository(SimpleRepository db)
        {
            this.database = db;
        }

        public List<EbsMessages> GetMessagesByStatus(Guid therapyId, Guid agencyId, int status)
        {
            var query = new QueryBuilder("select ebsmessages.Id, ebsmessages.Title, ebsmessages.TimeArrived, ebsmessages.Type, ebsmessages.Comment from ebsmessages");
            query.Where("ebsmessages.TherapyId=@therapyId").And("ebsmessages.AgencyId=@agencyId").And("ebsmessages.Status=@status").And("ebsmessages.IsDeprecated=0");
            return new FluentCommand<EbsMessages>(query.Build())
            .SetConnection("TherapyManagementConnectionString")
            .AddGuid("therapyId", therapyId)
            .AddGuid("agencyId", agencyId)
            .AddInt("status", status)
            .SetMap(reader => new EbsMessages
            {
                Id = reader.GetInt("Id"),
                Title = reader.GetString("Title"),
                TimeArrived = reader.GetDateTime("TimeArrived"),
                Type = reader.GetInt("Type"),
                Comment = reader.GetString("Comment")
            })
            .AsList();
        }

        public List<EbsMessages> GetMessagesByTypeAndStatus(Guid therapyId, Guid agencyId, int type, int status)
        {
            var script = @"SELECT ebsmessages.Id, ebsmessages.Title, ebsmessages.TimeArrived, ebsmessages.Type, ebsmessages.Comment"+
                " FROM ebsmessages"+
            " WHERE ebsmessages.TherapyId=@therapyId AND ebsmessages.AgencyId=@agencyId AND ebsmessages.Status=@status AND ebsmessages.IsDeprecated=0";
            if(type!=0)
            {
                script+=" AND ebsmessages.Type=@type";
            }
            var list = new List<EbsMessages>();
            using (var cmd = new FluentCommand<EbsMessages>(script))
            {
                list = cmd.SetConnection("TherapyManagementConnectionString")
                .AddGuid("therapyId", therapyId)
                .AddGuid("agencyId", agencyId)
                .AddInt("type", type)
                .AddInt("status", status)
                .SetMap(reader => new EbsMessages
                {
                    Id = reader.GetInt("Id"),
                    Title = reader.GetString("Title"),
                    TimeArrived = reader.GetDateTime("TimeArrived"),
                    Type = reader.GetInt("Type"),
                    Comment = reader.GetString("Comment")
                })
                .AsList();
            }
            return list;
        }

        public EbsMessages GetMessagesById(int id)
        {
            return database.Single<EbsMessages>(e => e.Id == id && e.IsDeprecated == false);
        }

        public bool AddNotice(Notice notice)
        {
            try
            {
                database.Add<Notice>(notice);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateMessage(EbsMessages ebs)
        {
            try
            {
                database.Update<EbsMessages>(ebs);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
