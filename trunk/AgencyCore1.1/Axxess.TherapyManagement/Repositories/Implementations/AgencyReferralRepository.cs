﻿

namespace Axxess.TherapyManagement.Repositories.Implementations
{
    using Axxess.TherapyManagement.Domain;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    public class AgencyReferralRepository : IAgencyReferralRepository
    {
        private readonly SimpleRepository database;
        public AgencyReferralRepository(SimpleRepository db)
        {
            this.database = db;
        }

        public List<AgencyReferralPatient> GetReferralPatients(Guid therapyId, Guid agencyId, int status)
        {
            var query = new QueryBuilder("select agencyreferalpatients.Id, agencyreferalpatients.LastName, agencyreferalpatients.FirstName, agencyreferalpatients.Gender, agencyreferalpatients.DOB, agencyreferalpatients.RequestDate from agencyreferalpatients");
            query.Where("agencyreferalpatients.TherapyId=@therapyId").And("agencyreferalpatients.AgencyId=@agencyId").And("agencyreferalpatients.Status=@status");
            return new FluentCommand<AgencyReferralPatient>(query.Build())
            .SetConnection("TherapyManagementConnectionString")
            .AddGuid("therapyId", therapyId)
            .AddGuid("agencyId",agencyId)
            .AddInt("status",status)
            .SetMap(reader => new AgencyReferralPatient
            {
                Id = reader.GetGuid("Id"),
                LastName = reader.GetString("LastName"),
                FirstName = reader.GetString("FirstName"),
                Gender=reader.GetString("Gender"),
                DOB=reader.GetDateTime("DOB"),
                RequestDate = reader.GetDateTime("RequestDate")
            })
            .AsList();
        }

        public AgencyReferralPatient GetReferralPatientById(Guid id)
        {
            return database.Single<AgencyReferralPatient>(a => a.Id == id);
        }

        public bool UpdateReferralPatient(AgencyReferralPatient referral)
        {
            bool result = false;
            try
            {
                if (referral != null)
                {
                    referral.SignDate = DateTime.Now;
                    this.database.Update<AgencyReferralPatient>(referral);
                    result = true;
                }
            }
            catch (Exception)
            {
                
                return false;
            }
            return result;
        }
    }
}
