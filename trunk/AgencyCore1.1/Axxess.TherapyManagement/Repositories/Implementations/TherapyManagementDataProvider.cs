﻿
namespace Axxess.TherapyManagement.Repositories.Implementations
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    
    using SubSonic.Repository;

    public class TherapyManagementDataProvider: ITherapyManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public TherapyManagementDataProvider()
        {
            this.database = new SimpleRepository("TherapyManagementConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        private IAgencyReferralRepository agencyReferralRepository;
        public IAgencyReferralRepository AgencyReferralRepository
        {
            get
            {
                if (agencyReferralRepository == null)
                {
                    agencyReferralRepository = new AgencyReferralRepository(database);
                }
                return agencyReferralRepository;
            }
        }

        private IAgencyCenterRepository agencyCenterRepository;
        public IAgencyCenterRepository AgencyCenterRepository
        {
            get
            {
                if (agencyCenterRepository == null)
                {
                    agencyCenterRepository = new AgencyCenterRepository(database);
                }
                return agencyCenterRepository;
            }
        }
    }
}
