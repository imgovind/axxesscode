﻿namespace Axxess.TherapyManagement.Repositories
{
    using Axxess.TherapyManagement.Domain;
    using System;
    using System.Collections.Generic;

    public interface IAgencyCenterRepository
    {
        List<EbsMessages> GetMessagesByStatus(Guid therapyId, Guid agencyId, int status);
        List<EbsMessages> GetMessagesByTypeAndStatus(Guid therapyId, Guid agencyId, int type, int status);
        EbsMessages GetMessagesById(int id);
        bool AddNotice(Notice notice);
        bool UpdateMessage(EbsMessages ebs);
    }
}
