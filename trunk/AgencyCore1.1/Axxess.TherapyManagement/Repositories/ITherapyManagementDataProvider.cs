﻿

namespace Axxess.TherapyManagement.Repositories
{
    public interface ITherapyManagementDataProvider
    {
        IAgencyReferralRepository AgencyReferralRepository { get; }
        IAgencyCenterRepository AgencyCenterRepository { get; }
    }
}
