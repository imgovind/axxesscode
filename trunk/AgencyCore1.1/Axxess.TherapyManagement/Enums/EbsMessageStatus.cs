﻿namespace Axxess.TherapyManagement.Enums
{
    public enum EbsMessageStatus:int
    {
        unopened = 0,
        opened = 1
    }
}
