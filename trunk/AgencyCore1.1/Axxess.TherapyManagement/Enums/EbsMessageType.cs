﻿namespace Axxess.TherapyManagement.Enums
{
    public enum EbsMessageType : int
    {
        All = 0,
        Referral = 1,
        Message = 2,
        Contract = 3
    }
}
