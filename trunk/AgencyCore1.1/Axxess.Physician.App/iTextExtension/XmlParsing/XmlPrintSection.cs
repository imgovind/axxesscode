﻿namespace Axxess.Physician.App.iTextExtension.XmlParsing {
    using System;
    using System.Text;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    class XmlPrintSection : XmlElement {
        public int Cols;
        public String Label;
        public String Style;
        public String Type;
        public List<XmlPrintSection> Subsection;
        public List<XmlPrintQuestion> Question;
        public XmlPrintSection(BaseXml xml, XElement section) : base(xml, section) {
            this.Cols = this.GetIntAttribute("cols");
            this.Label = this.GetAttribute("label");
            this.Style = this.GetAttribute("style");
            this.Type = this.GetAttribute("type");
            this.Subsection = this.GetSections();
            this.Question = this.GetQuestions();
        }
        public override string GetJson() {
            StringBuilder Json = new StringBuilder();
            Json.Append("{");
            if (this.Label.IsNotNullOrEmpty()) Json.Append("'Title':'" + this.CleanForJson(this.Label) + "',");
            if (this.Subsection.Count > 0) {
                Json.Append("'Section':[");
                for (int i = 0; i < this.Subsection.Count; i++) {
                    if (i != 0 && i % this.Cols == 0) Json.Append(",");
                    Json.Append("[" + this.Subsection[i].GetJson() + "]");
                }
                Json.Append("]");
            } else if (this.Question.Count > 0) {
                Json.Append("'Content':[");
                for (int i = 0; i < this.Question.Count; i++) {
                    if (i != 0 && i % this.Cols == 0) Json.Append(",");
                    Json.Append("[\"" + this.Question[i].GetJson() + "\"]");
                }
                Json.Append("]");
            }
            Json.Replace("][","],[");
            Json.Append("}");
            return Json.ToString();
        }
    }
}