﻿namespace Axxess.Physician.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public class AgencyPatientViewData
    {
        public Guid PatientId { get; set; }
        public string Name { get; set; }
    }
}
