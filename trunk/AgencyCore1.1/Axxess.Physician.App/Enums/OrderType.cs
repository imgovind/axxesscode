﻿namespace Axxess.Physician.App.Enums
{
    using Axxess.Core.Infrastructure;
    using System.ComponentModel;

    public enum OrderType : byte
    {
        [Description("Physician Order")]
        PhysicianOrder = 1,
        [Description("485 Plan of Care (From Assessment)")]
        HCFA485 = 2,
        [Description("486 Plan Of Care")]
        HCFA486 = 3,
        [Description("485 Plan of Treatment/Care")]
        HCFA485StandAlone = 4,
        [Description("Physician Face-to-face Encounter")]
        FaceToFaceEncounter = 5,
        [Description("PT Evaluation")]
        PtEvaluation = 6,
        [Description("PT Re-Evaluation")]
        PtReEvaluation = 7,
        [Description("OT Evaluation")]
        OtEvaluation = 8,
        [Description("OT Re-Evaluation")]
        OtReEvaluation = 9,
        [Description("ST Evaluation")]
        StEvaluation = 10,
        [Description("ST Re-Evaluation")]
        StReEvaluation = 11,
        [Description("MSW Evaluation")]
        MSWEvaluation = 12,
        [Description("PT Discharge")]
        PTDischarge = 13,
        [Description("SixtyDaySummary")]
        SixtyDaySummary=14
    }
}
